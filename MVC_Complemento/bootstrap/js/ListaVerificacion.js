 $(document).ready(function() {		
			//Por defecto	
			$('#Actualizar1').hide();
			$('#Actualizar2').hide();
	  		$('#Actualizar3').hide();
			$('#Actualizar4').hide();	
			
			$('#Limpiar1').hide();
			$('#Limpiar2').hide();
			$('#Limpiar3').hide();
			$('#Limpiar4').hide();
			$('#Limpiar5').hide();
			$('#Limpiar6').hide();
			$('#Limpiar7').hide();
			
			$(".nav .desabilitar>a").on("click", function(e) {
				$(".nav-tabs .desabilitar").addClass('disabled');
				e.preventDefault();
				return false;
			});			
			
			 function Buscar(valor,TipoBusqueda){
				 $.ajax({
                        url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarPaciente",
                        type: "POST",
                        data: {
                            
                            valor: valor,
                            TipoBusqueda:TipoBusqueda
							/*NOMBRES: $("#NOMBRES").val(), 
                             CIUDADES: $("#CIUDADES").val() */
                        },
                        dataType: "JSON",
                        success: function (jsonStr) {							
                            $('#DatosPaciente').css("display", "block"); 
                            $('#IdPaciente').val(jsonStr[0].IdPaciente);
							$('#Paciente').html(jsonStr[0].Paciente);
							$('#DNI').html(jsonStr[0].NroDocumento);
							$('#nrohiscli').html(jsonStr[0].NroHistoriaClinica);
							$('#FechaNac').html(jsonStr[0].FechaNacimiento);
							$('#Edad').html(jsonStr[0].Edad);
							$('#Sexo').html(jsonStr[0].Sexo);
							$('#dnival').val(valor); //valida que exista el paciente
							$('#Domicilio').html(jsonStr[0].Domicilio);
							$('#LugarNacim').html(jsonStr[0].LugarNacim);
							
                        },
                        error: function () {
                            //alert("Mensaje del Sistema: No se encontro Paciente");
							//jAlert('Mensaje del Sistema: No se encontro Paciente', 'SIGESA');
							//alertify.log("Esto es una notificación cualquiera.");
							$('#dnival').val(""); //valida que exista el paciente
							$('#DatosPaciente').css("display", "none");  
							alertify.alert("<b>Mensaje del Sistema: </b> <h1>No se encontro Paciente.</h1>", function () {  });
                        }
                    });
				 }


            //$('#NroHistoriaClinica').on('keypress', function (e) {
			  $( "#NroHistoriaClinica" ).change(function() {
				var NroHistoriaClinica= $('#NroHistoriaClinica').val();				
                //if (e.which === 13) {
          		
						if(NroHistoriaClinica==""){						 
							 //$('#Mensaje').html('Ingrese Nro. Historia Clinica');
							 //$('#Mensaje').show();
							 
							 alertify.alert("<b>Mensaje del Sistema: </b> <h1>Ingrese Nro. Historia Clinica</h1>", function () {				
							});
							
							 
							}else{
							 $("#Mensaje").hide();							 	
							 Buscar($('#NroHistoriaClinica').val(),'NroHistoriaClinica');								 
							 //$("#Datospaciente").show();
						     $("#DatosPaciente").css("display", "block"); 
							}
                    return false;
				//}
            });
			
			 //$('#NroDocumento').on('keypress', function (e) {
			   $( "#NroDocumento" ).change(function() {
				var NroDocumento= $('#NroDocumento').val();				
                //if ((e.keyCode || e.which)  === 13) {
          		
						if(NroDocumento==""){						 
							 //$('#Mensaje').html('Ingrese Nro. DNI');
							 //$('#Mensaje').show();
							  alertify.alert("<b>Mensaje del Sistema: </b> <h1>Ingrese Nro. DNI</h1>", function () {				
							});
							 
							 
							}else{
							 $("#Mensaje").hide();	
							 Buscar($('#NroDocumento').val(),'NroDocumento');	
							 //$("#Datospaciente").show();
						     $("#DatosPaciente").css("display", "block"); 							 
							}
                    return false;
				//}
            });
			
			/*$( "#NroDocumento" ).change(function() {
  alert("The paragraph was clicked.");
});*/
			
			$('#ApellidosNombrespaciente').on('keypress', function (e) {
				var ApellidosNombrespaciente= $('#ApellidosNombrespaciente').val();				
                if (e.which === 13) {
          		
						if(ApellidosNombrespaciente==""){						 
							 //$('#Mensaje').html('Ingrese Nro. DNI');
							 //$('#Mensaje').show();
							  alertify.alert("<b>Mensaje del Sistema: </b> <h1>Ingrese Datos Paciente</h1>", function () {				
							});
							 
							 
							}else{
							 $("#Mensaje").hide();	
							 Buscar($('#ApellidosNombrespaciente').val(),'ApellidosNombrespaciente');	
							 //$("#Datospaciente").show();
						     $("#DatosPaciente").css("display", "block"); 							 
							}
                    return false;
				}
            });
			
			
			 
 /*function Limpiar(){
	  
						    
						    $('#NroDocumento').val('');
	  						$('#NroHistoriaClinica').val('');	
							$('#ApellidosNombrespaciente').val('');			
                         	$('#IdPaciente').html('');
							$('#Paciente').html('');
							$('#DNI').html('');
							$('#nrohiscli').html('');
							$('#FechaNac').html('');
							$('#Edad').html('');
							$('#Sexo').html('');
							$("#Datospaciente").hide();	
	 }*/	
	 
	
$("#ApellidosNombrespaciente").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarPaciente",
            dataType: "json",
            data: {
                valor: request.term,
				TipoBusqueda:'ApellidosNombrespaciente'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Paciente,
                        value        : item.Paciente,
						IdPaciente   : item.IdPaciente,
                        Paciente  	 : item.Paciente,
						NroDocumento  	 : item.NroDocumento,
						NroHistoriaClinica : item.NroHistoriaClinica,
						FechaNacimiento    : item.FechaNacimiento,
						Edad    : item.Edad,
						Sexo    : item.Sexo,
						Domicilio    : item.Domicilio,
						LugarNacim    : item.LugarNacim
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);		
		//$("#Anestesiologo").focus();	
		$("#DatosPaciente").css("display", "block"); 
		$('#IdPaciente').val(ui.item.IdPaciente);
		$('#Paciente').html(ui.item.Paciente);
		$('#DNI').html(ui.item.NroDocumento);
		$('#nrohiscli').html(ui.item.NroHistoriaClinica);
		$('#FechaNac').html(ui.item.FechaNacimiento);
		$('#Edad').html(ui.item.Edad);
		$('#Sexo').html(ui.item.Sexo);
		$('#dnival').val(ui.item.NroHistoriaClinica); //valida que exista el paciente ya que todos los pacientes tienen numero de historia
		$('#Domicilio').html(ui.item.Domicilio);
		$('#LugarNacim').html(ui.item.LugarNacim);
		$("#ApellidosNombrespaciente").val('');	 
        return false;
    }
});
	
	
	 
/***Autocompetado***/ 
$("#Cirujano").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '1',
				IdEspecialidad: $("#IdEspecialidad").val()
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar1").show();
		$("#CirujanoMedico").html(ui.item.Medico);		 
		$("#CirujanoColegiatura").html(ui.item.Colegiatura);		
		$("#CirujanoTiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Cirujanorne").html(ui.item.rne);
		$("#CirujanoDNI").html(ui.item.DNI);		 
		$("#IdCirujano").val(ui.item.IdMedico);
		$("#Cirujano").val('');
		$("#IdCirujano").val(ui.item.IdMedico);
		$("#Anestesiologo").focus();		 
        return false;
    }
});


$("#Anestesiologo").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '2'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar2").show();
		$("#AnestesiologoMedico").html(ui.item.Medico);		 
		$("#AnestesiologoColegiatura").html(ui.item.Colegiatura);		
		$("#AnestesiologoTiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Anestesiologorne").html(ui.item.rne);
		$("#AnestesiologoDNI").html(ui.item.DNI);		 
		$("#IdAnestesiologo").val(ui.item.IdMedico);
		$("#Anestesiologo").val('');
		$("#Instrumentista").focus();		 
        return false;
    }
});
 



$("#Instrumentista").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '3'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar3").show();
		$("#InstrumentistaMedico").html(ui.item.Medico);		 
		$("#InstrumentistaColegiatura").html(ui.item.Colegiatura);		
		$("#InstrumentistaTiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Instrumentistarne").html(ui.item.rne);
		$("#InstrumentistaDNI").html(ui.item.DNI);		 
		$("#IdInstrumentista").val(ui.item.IdMedico);
		$("#Instrumentista").val('');
		$("#Asistente1").focus();		 
        return false;
    }
});




$("#Asistente1").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '4'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar4").show();
		$("#Asistente1Medico").html(ui.item.Medico);		 
		$("#Asistente1Colegiatura").html(ui.item.Colegiatura);		
		$("#Asistente1TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Asistente1rne").html(ui.item.rne);
		$("#Asistente1DNI").html(ui.item.DNI);		 
		$("#IdAsistente1").val(ui.item.IdMedico);
		$("#Asistente1").val('');
		$("#Asistente2").focus();		 
        return false;
    }
});


$("#Asistente2").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '4'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar5").show();
		$("#Asistente2Medico").html(ui.item.Medico);		 
		$("#Asistente2Colegiatura").html(ui.item.Colegiatura);		
		$("#Asistente2TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Asistente2rne").html(ui.item.rne);
		$("#Asistente2DNI").html(ui.item.DNI);		 
		$("#IdAsistente2").val(ui.item.IdMedico);
		$("#Asistente2").val('');
		$("#Asistente3").focus();		 
        return false;
    }
});


$("#Asistente3").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '4'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar6").show();
		$("#Asistente3Medico").html(ui.item.Medico);		 
		$("#Asistente3Colegiatura").html(ui.item.Colegiatura);		
		$("#Asistente3TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Asistente3rne").html(ui.item.rne);
		$("#Asistente3DNI").html(ui.item.DNI);		 
		$("#IdAsistente3").val(ui.item.IdMedico);
		$("#Asistente3").val('');
		$("#EnfermeraCirculante1").focus();		 
        return false;
    }
});

/*Cirujano
Anestesiologo
Instrumentista
Asistente1
Asistente2
Asistente3
EnfermeraCirculante1
EnfermeraCirculante2*/


$("#EnfermeraCirculante1").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '5'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar7").show();
		$("#EnfermeraCirculante1Medico").html(ui.item.Medico);		 
		$("#EnfermeraCirculante1Colegiatura").html(ui.item.Colegiatura);		
		$("#EnfermeraCirculante1TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#EnfermeraCirculante1rne").html(ui.item.rne);
		$("#EnfermeraCirculante1DNI").html(ui.item.DNI);		 
		$("#IdEnfermeraCirculante1").val(ui.item.IdMedico);
		$("#EnfermeraCirculante1").val('');
		//$("#EnfermeraCirculante2").focus();		 
        return false;
    }
});

$("#EnfermeraCirculante2").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term,
				TipoProfesional: '5'
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    /*focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },*/
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Limpiar8").show();
		$("#EnfermeraCirculante2Medico").html(ui.item.Medico);		 
		$("#EnfermeraCirculante2Colegiatura").html(ui.item.Colegiatura);		
		$("#EnfermeraCirculante2TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#EnfermeraCirculante2rne").html(ui.item.rne);
		$("#EnfermeraCirculante2DNI").html(ui.item.DNI);		 
		$("#IdEnfermeraCirculante2").val(ui.item.IdMedico);
		$("#EnfermeraCirculante2").val('');
		$("#EnfermeraCirculante2").focus();		 
        return false;
    }
});

/**Salas**/

//TipoSala/*

//cuando refrescamos la pagina y hemos seleccionado un radio TipoSala
		//obtener el valor del radio TipoSala seleccionado	
		var resultado="ninguno";   
        var TipoSala=document.getElementsByName("TipoSala");
        for(var i=0;i<TipoSala.length;i++)
        {
            if(TipoSala[i].checked)
                resultado=TipoSala[i].value;
        } 	
		//buscamos las salas del TipoSala seleccionado	 	
		$.getJSON("../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarSalas&TipoSala="+resultado, function(json){
				$('#Sala').empty();
				$('#Sala').append($('<option>').text("Select"));
				$.each(json, function(i, obj){
						$('#Sala').append($('<option>').text(obj.text).attr('value', obj.val));
				});
		}); 
		
//cuando cambiamos de radio TipoSala
$('input:radio[name="TipoSala"]').change(function(){   
	var Sala=$(this).val();	 	
	 $.getJSON("../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarSalas&TipoSala="+Sala, function(json){
            $('#Sala').empty();
            $('#Sala').append($('<option>').text("Select"));
            $.each(json, function(i, obj){
                    $('#Sala').append($('<option>').text(obj.text).attr('value', obj.val));
            });
    });	
});

/*$("#Servicio").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarServicios",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Servicios,
                        value        : item.IdServicio,
						IdServicio        : item.IdServicio,
						TiposServicio:item.TiposServicio, 
                        IdEspecialidad:item.IdEspecialidad 
 
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		
		//alert(ui.item.label+' - '+ui.item.IdServicio);
		
		
		$("#ServicioNombre").html(ui.item.label);
		$("#ServicioTipo").html(ui.item.TiposServicio);	
		//$("#IdEspecialidad").html(ui.item.IdEspecialidad);	
		$("#Servicio").val();	
		$("#Cirujano").focus();	
			 
					 
		
		
        return false;
    }
});*/


 
 
 function Cancelar(){
     //$(location).attr('index.php'); 
	 //window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio";
 }

 
	//guardar Identificacion paciente
	$('#FormLisVer').submit(function(event) {       	
		
		var TipoSala = $("#TipoSala").val();
		var Sala     = $("#Sala").val(); //$('select[name=Sala]').val() ;
		var Servicio = $("#Servicio").val();
		var dnival = $("#dnival").val(); 
		//RECUPERAR PARA GUARDAR
		var IdPaciente = $("#IdPaciente").val();  
		var IdEmpleado = $("#IdEmpleado").val();  
		var IdCirujano = $("#IdCirujano").val(); 
		var IdAnestesiologo = $("#IdAnestesiologo").val();  
		var IdInstrumentista = $("#IdInstrumentista").val();  
		var IdAsistente1 = $("#IdAsistente1").val();  
		var IdAsistente2 = $("#IdAsistente2").val();  
		var IdAsistente3 = $("#IdAsistente3").val();  
		var IdEnfermeraCirculante1 = $("#IdEnfermeraCirculante1").val(); 
		
		var accion1 = $("#accion1").val();
		var IdListVerAtenciones = $("#IdListVerAtenciones").val();  
		 
		var formData = 'TipoSala='+ TipoSala + '&Servicio='+ Servicio + '&Sala='+ Sala + '&dnival='+ dnival + '&IdPaciente='+ IdPaciente + '&IdEmpleado='+ IdEmpleado + '&IdCirujano='+ IdCirujano + '&IdAnestesiologo='+ IdAnestesiologo + '&IdInstrumentista='+ IdInstrumentista + '&IdAsistente1='+ IdAsistente1 + '&IdAsistente2='+ IdAsistente2 + '&IdAsistente3='+ IdAsistente3 + '&IdEnfermeraCirculante1='+ IdEnfermeraCirculante1 + '&accion1='+accion1+'&IdListVerAtenciones='+IdListVerAtenciones ;
       
	    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Guardar', // the url where we want to POST
            data        :  formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      :  true,
			
        })
            // using the done promise callback
            .done(function(data) {                
               console.log(data); 				
					
					//alert(data);					
					var TipoSala = document.getElementsByName('TipoSala');
					var ischecked_method = false;
					for ( var i = 0; i < TipoSala.length; i++) {
						if(TipoSala[i].checked) {
							ischecked_method = true;
							break;
						}
					}					
									
			if (!data.success) {
            	
				  if (data.errors.dnival) {
					   $("#error-paciente").addClass('has-error'); 
					   $("#dnival").focus();  
					   //alert(data.errors.dnival);
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.dnival+"</h1>");
					   
			      }else if(!ischecked_method)   { //payment method button is not checked
				  	   $("#error-paciente").removeClass('has-error');//remover clase error
					   $("#TipoSala-group").addClass('has-error'); 
					   $("#TipoSala").focus();  
					   //alert("Seleccionar Tipo Sala");
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>Seleccionar Tipo Sala</h1>");
					   
				   }else if (data.errors.Sala) {
					   $("#TipoSala-group").removeClass('has-error');//remover clase error
					   $("#Sala-group").addClass('has-error'); 
					   $("#Sala").focus();  
					   //alert(data.errors.Sala);
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.Sala+"</h1>");
					   
			      }else if (data.errors.Servicio) {
					   $("#Sala-group").removeClass('has-error');//remover clase error
					   $("#Servicio-group").addClass('has-error'); 
					   $("#Servicio").focus();  
					   //alert(data.errors.Servicio);	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.Servicio+"</h1>");
					   				   					   
			      }else if (data.errors.IdCirujano) {
					   $("#Servicio-group").removeClass('has-error');//remover clase error
					   $("#IdCirujano-group").addClass('has-error'); 
					   $("#IdCirujano").focus();  
					   //alert(data.errors.IdCirujano);	
					    alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdCirujano+"</h1>");
										   					   
			      }else if (data.errors.IdAnestesiologo) {
					   $("#IdCirujano-group").removeClass('has-error');//remover clase error
					   $("#IdAnestesiologo-group").addClass('has-error'); 
					   $("#IdAnestesiologo").focus();  
					   //alert(data.errors.IdAnestesiologo);
					    alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdAnestesiologo+"</h1>");
											   					   
			      }else if (data.errors.IdEnfermeraCirculante1) {
					   $("#IdAnestesiologo-group").removeClass('has-error');//remover clase error
					   $("#IdEnfermeraCirculante1-group").addClass('has-error'); 
					   $("#IdEnfermeraCirculante1").focus();  
					   //alert(data.errors.IdEnfermeraCirculante1);	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdEnfermeraCirculante1+"</h1>");				   					   
			      }	
           
		    }else{
				
				 	 $("#error-paciente").removeClass('has-error');//remover clase error
				 	 $("#TipoSala-group").removeClass('has-error');//remover clase error
					 $("#Sala-group").removeClass('has-error');//remover clase error
					 $("#Servicio-group").removeClass('has-error');//remover clase error
					 
					 $("#IdCirujano-group").removeClass('has-error');
					 $("#IdAnestesiologo-group").removeClass('has-error');
					 $("#IdEnfermeraCirculante1-group").removeClass('has-error');
					 
					  //$('input[type="submit"], input[type="button"], button').disable(true);
					 $('#Guardar1').hide(); //muestro mediante clase   show
				 	 $('#Actualizar1').show();	
				 	 $('#accion1').val('A');	
				 	 $('#IdListVerAtenciones').val(data.IdListVerAtenciones);	    
			
					alertify.success("<b>Paciente Guardado </b> <h1>Continue con el Registro</h1>"); 
					
                	$(".nav-tabs a[href='#tab2primary']").attr('data-toggle', 'tab').off('click');		
					$(".nav-tabs a[href='#tab2primary']").tab('show');	
										
					$('#IdListVerAtenciones2').val(data.IdListVerAtenciones);
					
					var hoy = new Date();
					var dd = hoy.getDate();
					var mm = hoy.getMonth()+1; //hoy es 0!
					var yyyy = hoy.getFullYear();
					
					var h=hoy.getHours();
					var i=hoy.getMinutes();
					var s=hoy.getSeconds();
					
					if(dd<10) {
						dd='0'+dd
					} 			
					if(mm<10) {
						mm='0'+mm
					} 
					$("#FechaInicioEntrada").val(yyyy+"-"+mm+"-"+dd+' '+h+':'+i+':'+s);
					return false;			
		
        		}
					
           });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();		
    });
	//fin guardar Identificacion paciente
	
	//guardar Inicio	
	$('#FormInicio').submit(function(event) { 
	      	
		var FechaInicioEntrada = $("#FechaInicioEntrada").val();
		var IdListVerAtenciones2 = $("#IdListVerAtenciones2").val();
		
		var IdAlternativasChk = $("#IdAlternativasChk").val();	
		var IdAlternativasRad = $("#IdAlternativasRad").val();							
		 
		var formData = 'FechaInicioEntrada='+FechaInicioEntrada+ '&IdListVerAtenciones2='+IdListVerAtenciones2+ '&IdAlternativasChk='+IdAlternativasChk+ '&IdAlternativasRad='+IdAlternativasRad;
       
	    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_GuardarPreguntasInicio', // the url where we want to POST
            data        :  formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      :  true,
			
        })			
			// using the done promise callback
            .done(function(data) {                
               console.log(data); 
			   								
			if (!data.success) {            	
				  if (data.errors.IdListVerAtenciones2) {  
					   //alert(data.errors.IdListVerAtenciones2);
					    alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdListVerAtenciones2+"</h1>");
											   
			      }else if (data.errors.FechaInicioEntrada) {					    
					   //alert(data.errors.FechaInicioEntrada);	
					    alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.FechaInicioEntrada+"</h1>");
										   
			      }else if (data.errors.IdAlternativas) {  
					   //alert(data.errors.IdAlternativas);	
					   //alertify.success("<b> Mensaje del Sistema: </b> <h1>data.errors.IdAlternativas</h1>");
					   //alertify.error("<b>No se encontro Impresora.</b>");
					   //alertify.alert("<h1>Se Guardo la Lista Correctamente.</h1>", function () {});	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdAlternativas+"</h1>");	
					    				   
			      }else if (data.errors.AlternativasCheck) {  
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasCheck+"</h1>");	 				   
			      }else if (data.errors.AlternativasRad) { 
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasRad+"</h1>");	 				   
			      }
           
		    }else{
				
				$('#Guardar2').hide(); //muestro mediante clase   show
				$('#Actualizar2').show();					
				$('#IdListVerAtenciones2').val(IdListVerAtenciones2);				   
				alertify.success("<b> Inicio </b> <h1>Continue con el Registro</h1>"); 
				
				$(".nav-tabs a[href='#tab3primary']").attr('data-toggle', 'tab').off('click');
				$(".nav-tabs a[href='#tab3primary']").tab('show');				
				
				$('#IdListVerAtenciones3').val(IdListVerAtenciones2);			
				var hoy = new Date();
				var dd = hoy.getDate();
				var mm = hoy.getMonth()+1; //hoy es 0!
				var yyyy = hoy.getFullYear();
				
				var h=hoy.getHours();
				var i=hoy.getMinutes();
				var s=hoy.getSeconds();
				
				if(dd<10) {
					dd='0'+dd
				} 
				
				if(mm<10) {
					mm='0'+mm
				} 
				$("#FechaInicioPausa").val(yyyy+"-"+mm+"-"+dd+' '+h+':'+i+':'+s);
				return false;
			  }
					
           });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();		
		
		
    });
	//fin guardar Inicio
	
	//guardar Pausa	
	$('#FormPausa').submit(function(event) { 
	      	
		var FechaInicioPausa = $("#FechaInicioPausa").val();
		var IdListVerAtenciones3 = $("#IdListVerAtenciones3").val();
		
		var IdAlternativasChk = $("#IdAlternativasChkP").val();	
		var IdAlternativasRad = $("#IdAlternativasRadP").val();							
		 
		var formData = 'FechaInicioPausa='+FechaInicioPausa+ '&IdListVerAtenciones3='+IdListVerAtenciones3+ '&IdAlternativasChk='+IdAlternativasChk+ '&IdAlternativasRad='+IdAlternativasRad;
       
	    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_GuardarPreguntasPausa', // the url where we want to POST
            data        :  formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      :  true,
			
        })			
			// using the done promise callback
            .done(function(data) {                
               console.log(data); 
			   								
			if (!data.success) {            	
				  if (data.errors.IdListVerAtenciones3) {  
					   alert(data.errors.IdListVerAtenciones3);	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdListVerAtenciones3+"</h1>");	
					   					   
			      }else if (data.errors.FechaInicioPausa) {					    
					   alert(data.errors.FechaInicioPausa);	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.FechaInicioPausa+"</h1>");
					   				   
			      }else if (data.errors.IdAlternativas) {  
					   //alert(data.errors.IdAlternativas);	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdAlternativas+"</h1>");	
					   			   
			      }else if (data.errors.AlternativasCheck9) {  
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasCheck9+"</h1>");					   	 				   
			      }else if (data.errors.AlternativasCheck11) {  
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasCheck11+"</h1>");				   	 				   
			      }else if (data.errors.AlternativasCheck) {  
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasCheck+"</h1>");					    				   
			      }else if (data.errors.AlternativasRad) { 
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasRad+"</h1>");	 				   
			      }
           
		    }else{
				
				$('#Guardar3').hide(); //muestro mediante clase   show
				$('#Actualizar3').show();				
				$('#IdListVerAtenciones3').val(IdListVerAtenciones3);				   
				alertify.success("<b> Pausa </b> <h1>Continue con el Registro</h1>"); 
				
				$(".nav-tabs .desabilitar").removeClass('disabled');
				$(".nav-tabs a[href='#tab4primary']").attr('data-toggle', 'tab').off('click');
				$(".nav-tabs a[href='#tab4primary']").tab('show');				
				
				$('#IdListVerAtenciones4').val(IdListVerAtenciones3);			
				var hoy = new Date();
				var dd = hoy.getDate();
				var mm = hoy.getMonth()+1; //hoy es 0!
				var yyyy = hoy.getFullYear();
				
				var h=hoy.getHours();
				var i=hoy.getMinutes();
				var s=hoy.getSeconds();
				
				if(dd<10) {
					dd='0'+dd
				} 				
				if(mm<10) {
					mm='0'+mm
				} 
				
				if(h<10) {
					h='0'+h
				}
				if(i<10) {
					i='0'+i
				}
				if(s<10) {
					s='0'+s
				}
				$("#FechaInicioSalida").val(yyyy+"-"+mm+"-"+dd+' '+h+':'+i+':'+s);
				return false;
			  }
					
           });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();		
		
		
    });
	//fin guardar Pausa
	
	//guardar Salida	
	$('#FormSalida').submit(function(event) { 
	      	
		var FechaInicioSalida = $("#FechaInicioSalida").val();
		var IdListVerAtenciones4 = $("#IdListVerAtenciones4").val();
		
		var IdAlternativasChk = $("#IdAlternativasChkS").val();	
		var IdAlternativasRad = $("#IdAlternativasRadS").val();	
		var Observacion = $("#Observacion").val();							
		 
		var formData = 'FechaInicioSalida='+FechaInicioSalida+ '&IdListVerAtenciones4='+IdListVerAtenciones4+ '&IdAlternativasChk='+IdAlternativasChk+ '&IdAlternativasRad='+IdAlternativasRad+ '&Observacion='+Observacion;
       
	    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_GuardarPreguntasSalida', // the url where we want to POST
            data        :  formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      :  true,
			
        })			
			// using the done promise callback
            .done(function(data) {                
               console.log(data); 
			   								
			if (!data.success) {            	
				  if (data.errors.IdListVerAtenciones4) {  
					   //alert(data.errors.IdListVerAtenciones4);	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdListVerAtenciones4+"</h1>");
					   				   
			      }else if (data.errors.FechaInicioSalida) {					     
					   //alert(data.errors.FechaInicioSalida);
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.FechaInicioSalida+"</h1>");
											   
			      }else if (data.errors.IdAlternativas) {  
					   //alert(data.errors.IdAlternativas);	
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdAlternativas+"</h1>");				   
			      
				  }else if (data.errors.AlternativasCheck16) {  
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasCheck16+"</h1>");				   	 				   
			      }else if (data.errors.AlternativasCheck) {  
					   alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.AlternativasCheck+"</h1>");	 				   
			      }
           
		    }else{
				
				$('#Guardar4').hide(); //muestro mediante clase   show
				$('#Actualizar4').show();	
				$('#IdListVerAtenciones4').val(IdListVerAtenciones4);				   
				//alertify.success("<b> Salida </b> <h1>Datos Guardados Correctamente</h1>"); 				
				//alertify.alert("<h1>Se Guardo la Lista Correctamente.</h1>", function () {});				
				var codigo_exp = IdListVerAtenciones4;
				var IdEmpleado =  $("#IdEmpleado").val();
				//window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio&IdEmpleado="+IdEmpleado;
				//window.open('../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo='+codigo_exp+"&IdEmpleado="+IdEmpleado,'_blank');	
				    alertify.confirm("Datos Guardados Correctamente. ¿Desea Imprimir? ", function (e) {
						if (e) {							
							//Redireccionamos si das a aceptar
					 		window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio&IdEmpleado="+IdEmpleado;
							window.open('../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo='+codigo_exp+"&IdEmpleado="+IdEmpleado,'_blank'); 
						} else {
							//alertify.alert("Successful AJAX after Cancel");
							window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio&IdEmpleado="+IdEmpleado;
						}
					});						
				return false;
			  }
					
           });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();		
    });
	//fin guardar Salida
	
});	
	
	 /*function Siguentepagina3(){		 
		 	var validarcheckbox = document.getElementsByClassName('validarcheckbox3');
			var ischecked_chk = false;
			for ( var i = 0; i < validarcheckbox.length; i++) {
				if(validarcheckbox[i].checked) {
					ischecked_chk = true;
					break;
				}
			}	    
			
			var validarradio = document.getElementsByClassName('validarradio3');
			var ischecked_radio = false;
			for ( var i = 0; i < validarradio.length; i++) {
				if(validarradio[i].checked) {
					ischecked_radio = true;
					break;
				}
			}
			
			if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
			}else if(!ischecked_radio && validarradio.length>0)   { //payment method button is not checked
				alert("Porfavor Marcar una opcion de cada Pregunta");
			}else{	
				alertify.alert("<h1>Se Guardo la Lista Correctamente.</h1>", function () { });
				alertify.error("<b>No se encontro Impresora.</b>");				
				var codigo_exp = '7'; 
				window.location.href = "../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+codigo_exp;
				//window.open('../../MVC_Vista/Emergencia/Mascara_2_pdf.php?codigo='+codigo_exp,'_blank');
				return false;
			}			
	 }*/