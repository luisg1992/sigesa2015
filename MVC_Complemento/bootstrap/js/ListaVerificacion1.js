 $(document).ready(function() {
	  
			 function Buscar(valor,TipoBusqueda){
				 $.ajax({
                        url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarPaciente",
                        type: "POST",
                        data: {
                            
                            valor: valor,
                            TipoBusqueda:TipoBusqueda
							/*NOMBRES: $("#NOMBRES").val(), 
                             CIUDADES: $("#CIUDADES").val() */
                        },
                        dataType: "JSON",
                        success: function (jsonStr) {
							
							$('#dnival').html(jsonStr[0].NroDocumento); //valida que exista el paciente
                            $("#DatosPaciente").css("display", "block"); 
                            $('#IdPaciente').html(jsonStr[0].IdPaciente);
							$('#Paciente').html(jsonStr[0].Paciente);
							$('#DNI').html(jsonStr[0].NroDocumento);
							$('#nrohiscli').html(jsonStr[0].NroHistoriaClinica);
							$('#FechaNac').html(jsonStr[0].FechaNacimiento);
							$('#Edad').html(jsonStr[0].Edad);
							$('#Sexo').html(jsonStr[0].Sexo);
							
                        },
                        error: function () {
                            //alert("Error: No se encontro Paciente");
							//jAlert('Error: No se encontro Paciente', 'SIGESA');
							//alertify.log("Esto es una notificación cualquiera.");
							$("#DatosPaciente").css("display", "none");  
							alertify.alert("<b>Error: </b> <h1>No se encontro Paciente.</h1>", function () {				
							});
                        }
                    });
				 }


            $('#NroHistoriaClinica').on('keypress', function (e) {
				var NroHistoriaClinica= $('#NroHistoriaClinica').val();				
                if (e.which === 13) {
          		
						if(NroHistoriaClinica==""){						 
							 //$('#Mensaje').html('Ingrese Nro. Historia Clinica');
							 //$('#Mensaje').show();
							 
							 alertify.alert("<b>Error: </b> <h1>Ingrese Nro. Historia Clinica</h1>", function () {				
							});
							
							 
							}else{
							 $("#Mensaje").hide();							 	
							 Buscar($('#NroHistoriaClinica').val(),'NroHistoriaClinica');								 
							 //$("#Datospaciente").show();
						     $("#DatosPaciente").css("display", "block"); 
							}
                    return false;
				}
            });
			
			 $('#NroDocumento').on('keypress', function (e) {
				var NroDocumento= $('#NroDocumento').val();				
                if (e.which === 13) {
          		
						if(NroDocumento==""){						 
							 //$('#Mensaje').html('Ingrese Nro. DNI');
							 //$('#Mensaje').show();
							  alertify.alert("<b>Error: </b> <h1>Ingrese Nro. DNI</h1>", function () {				
							});
							 
							 
							}else{
							 $("#Mensaje").hide();	
							 Buscar($('#NroDocumento').val(),'NroDocumento');							 							 
							 //document.getElementById("NroDocumento").disabled = true;								 
							 //$("#Datospaciente").show();
						     $("#DatosPaciente").css("display", "block"); 							 
							}
                    return false;
				}
            });
			
			
			 
 function Limpiar(){
	  
						    
						    $('#NroDocumento').val('');
	  						$('#NroHistoriaClinica').val('');			
                         	$('#IdPaciente').html('');
							$('#Paciente').html('');
							$('#DNI').html('');
							$('#nrohiscli').html('');
							$('#FechaNac').html('');
							$('#Edad').html('');
							$('#Sexo').html('');
							$("#Datospaciente").hide();	
	 }	
	 
	 
/***Autocompetado***/
 

$("#Cirujano").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#CirujanoDiv").show();
		$("#CirujanoMedico").html(ui.item.Medico);		 
		$("#CirujanoColegiatura").html(ui.item.Colegiatura);		
		$("#CirujanoTiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Cirujanorne").html(ui.item.rne);
		$("#CirujanoDNI").html(ui.item.DNI);		 
		$("#IdCirujano").val(ui.item.IdMedico);
		$("#Cirujano").val('');
		$("#Anestesiologo").focus();		 
        return false;
    }
});


$("#Anestesiologo").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#AnestesiologoDiv").show();
		$("#AnestesiologoMedico").html(ui.item.Medico);		 
		$("#AnestesiologoColegiatura").html(ui.item.Colegiatura);		
		$("#AnestesiologoTiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Anestesiologorne").html(ui.item.rne);
		$("#AnestesiologoDNI").html(ui.item.DNI);		 
		$("#IdAnestesiologo").val(ui.item.IdMedico);
		$("#Anestesiologo").val('');
		$("#Instrumentista").focus();		 
        return false;
    }
});
 



$("#Instrumentista").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#InstrumentistaDiv").show();
		$("#InstrumentistaMedico").html(ui.item.Medico);		 
		$("#InstrumentistaColegiatura").html(ui.item.Colegiatura);		
		$("#InstrumentistaTiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Instrumentistarne").html(ui.item.rne);
		$("#InstrumentistaDNI").html(ui.item.DNI);		 
		$("#IdInstrumentista").val(ui.item.IdMedico);
		$("#Instrumentista").val('');
		$("#Asistente1").focus();		 
        return false;
    }
});




$("#Asistente1").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Asistente1Div").show();
		$("#Asistente1Medico").html(ui.item.Medico);		 
		$("#Asistente1Colegiatura").html(ui.item.Colegiatura);		
		$("#Asistente1TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Asistente1rne").html(ui.item.rne);
		$("#Asistente1DNI").html(ui.item.DNI);		 
		$("#IdAsistente1").val(ui.item.IdMedico);
		$("#Asistente1").val('');
		$("#Asistente2").focus();		 
        return false;
    }
});


$("#Asistente2").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Asistente2Div").show();
		$("#Asistente2Medico").html(ui.item.Medico);		 
		$("#Asistente2Colegiatura").html(ui.item.Colegiatura);		
		$("#Asistente2TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Asistente2rne").html(ui.item.rne);
		$("#Asistente2DNI").html(ui.item.DNI);		 
		$("#IdAsistente2").val(ui.item.IdMedico);
		$("#Asistente2").val('');
		$("#Asistente3").focus();		 
        return false;
    }
});


$("#Asistente3").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#Asistente3Div").show();
		$("#Asistente3Medico").html(ui.item.Medico);		 
		$("#Asistente3Colegiatura").html(ui.item.Colegiatura);		
		$("#Asistente3TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#Asistente3rne").html(ui.item.rne);
		$("#Asistente3DNI").html(ui.item.DNI);		 
		$("#IdAsistente3").val(ui.item.IdMedico);
		$("#Asistente3").val('');
		$("#EnfermeraCirculante1").focus();		 
        return false;
    }
});

/*Cirujano
Anestesiologo
Instrumentista
Asistente1
Asistente2
Asistente3
EnfermeraCirculante1
EnfermeraCirculante2*/


$("#EnfermeraCirculante1").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#EnfermeraCirculante1Div").show();
		$("#EnfermeraCirculante1Medico").html(ui.item.Medico);		 
		$("#EnfermeraCirculante1Colegiatura").html(ui.item.Colegiatura);		
		$("#EnfermeraCirculante1TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#EnfermeraCirculante1rne").html(ui.item.rne);
		$("#EnfermeraCirculante1DNI").html(ui.item.DNI);		 
		$("#IdEnfermeraCirculante1").val(ui.item.IdMedico);
		$("#EnfermeraCirculante1").val('');
		$("#EnfermeraCirculante2").focus();		 
        return false;
    }
});

$("#EnfermeraCirculante2").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarProfesional",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Medico,
                        value        : item.Medico,
                        Medico       : item.Medico,
                        IdMedico  	 : item.IdMedico,
						IdEmpleado	 : item.IdEmpleado,
						Colegiatura  : item.Colegiatura,												
						rne          : item.rne,
						TiposEmpleado: item.TiposEmpleado,
						DNI          : item.DNI
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		$("#EnfermeraCirculante2Div").show();
		$("#EnfermeraCirculante2Medico").html(ui.item.Medico);		 
		$("#EnfermeraCirculante2Colegiatura").html(ui.item.Colegiatura);		
		$("#EnfermeraCirculante2TiposEmpleado").html(ui.item.TiposEmpleado);
		$("#EnfermeraCirculante2rne").html(ui.item.rne);
		$("#EnfermeraCirculante2DNI").html(ui.item.DNI);		 
		$("#IdEnfermeraCirculante2").val(ui.item.IdMedico);
		$("#EnfermeraCirculante2").val('');
		$("#EnfermeraCirculante2").focus();		 
        return false;
    }
});

/**Salas**/
//TipoSala/*
 
	 
 
 
  

$('input:radio[name="TipoSala"]').change(function(){
   
	var Sala=$(this).val();
	 
	
	 $.getJSON("../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarSalas&TipoSala="+Sala, function(json){
            $('#Sala').empty();
            $('#Sala').append($('<option>').text("Select"));
            $.each(json, function(i, obj){
                    $('#Sala').append($('<option>').text(obj.text).attr('value', obj.val));
            });
    });
	
});

 



$("#Servicio").autocomplete({
    source: function ( request, response ) {
        $.ajax({
            
			url: "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarServicios",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function (data) {
                response($.map(data,function(item){
                    return {
						label        : item.Servicios,
                        value        : item.IdServicio,
						IdServicio        : item.IdServicio,
						TiposServicio:item.TiposServicio 
                         
 
						};
                }));
            }
        });
    },
    minLength: 2,
    focus: function (event, ui) {
        $(event.target).val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        $(event.target).val(ui.item.label);
		
		//alert(ui.item.label+' - '+ui.item.IdServicio);
		
		
		$("#ServicioNombre").html(ui.item.label);
		$("#ServicioTipo").html(ui.item.TiposServicio);	
		$("#Servicio").val();	
		$("#Cirujano").focus();	
			 
					 
		
		
        return false;
    }
});


 
 
 function Cancelar(){
     //$(location).attr('index.php'); 
	 window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio";
	 }

 
	
	 $('#FormLisVer').submit(function(event) {
       			
		var TipoSala = $('input:radio[name=TipoSala]:checked').val();
		var sala     = $("#Sala option:selected" ).val(); //$('select[name=sala]').val() ;
		var Servicio = $("#Servicio").val(); 
		var dnival = $("#dnival").val();  
		var formData = 'TipoSala='+ TipoSala + '&Servicio='+ Servicio + '&sala='+ sala + '&dnival='+ dnival;
       
	    $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Guardar', // the url where we want to POST
            data        :  formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      :  true,
			
        })
            // using the done promise callback
            .done(function(data) {                
               console.log(data); 				
					
					//alert(data);		
					
			if (!data.success) {
            	
				  if (data.errors.Servicio) {
					   $('#Servicio-group').addClass('has-error'); 
					   $("#Servicio").focus();  
					   alert(data.errors.Servicio);
			      }else if (data.errors.dnival) {
					   $('#error-paciente').addClass('has-error'); 
					   $("#dnival").focus();  
					   alert(data.errors.dnival);
			      }
			
           
		    }else{

          // $('FormLisVer').append('<div class="alert alert-success">' + data.message + '</div>');

           // alert('success'+data.message); // for now we'll just alert the user	
			
			alertify.success("<b>Paciente Guardado </b> <h1>Continue con el Registro</h1>"); 
			//alertify.success("Visita nuestro <a href=\"http://blog.reaccionestudio.com/\" style=\"color:white;\" target=\"_blank\"><b>BLOG.</b></a>"); 
			$(".nav-tabs a[href='#tab2primary']").tab('show');
			return false;
				
			//alertify.alert("<b>Paciente Guardado </b> <h1>Continue con el registro</h1>", function(){});
			//$(".nav-tabs a[href='#tab2primary']").tab('show');
			
			
							
							
			

        		}
					
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
		
		
    });
	
   });
	
	
 function Siguentepagina1(){
	 
	        alertify.success("<b> Inicio </b> <h1>Continue con el Registro</h1>"); 
			//alertify.success("Visita nuestro <a href=\"http://blog.reaccionestudio.com/\" style=\"color:white;\" target=\"_blank\"><b>BLOG.</b></a>"); 
			$(".nav-tabs a[href='#tab3primary']").tab('show');
			return false;
	 }
	
 function Siguentepagina2(){
	 
	        alertify.success("<b>Pausa Guardado </b> <h1>Continue con el Registro</h1>"); 
			//alertify.success("Visita nuestro <a href=\"http://blog.reaccionestudio.com/\" style=\"color:white;\" target=\"_blank\"><b>BLOG.</b></a>"); 
			$(".nav-tabs a[href='#tab4primary']").tab('show');
			return false;
	 }
	
	 function Siguentepagina3(){
	 		
			alertify.alert("<b>Error: </b> <h1>Se Guardo la Lista Corectamente.</h1>", function () {				
							});

			alertify.error("<b>No se encontro Impresora.</h1>"); 
			//alertify.success("Visita nuestro <a href=\"http://blog.reaccionestudio.com/\" style=\"color:white;\" target=\"_blank\"><b>BLOG.</b></a>"); 
			//$(".nav-tabs a[href='#tab5primary']").tab('show');
			window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio";
			return false;
			
	 }