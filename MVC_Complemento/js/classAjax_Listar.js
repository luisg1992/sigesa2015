function classAjax_Listar() {
    this.loadCmbJson_L=loadCmbJson_Listar;
}
/**
*Carga un select con los datos de un JSON
*@param url @type String: Direccion para obtener el JSON
*@param data @type String: datos de envio
*@param cmb @type String: Nombre del Objeto Select
*/
function loadCmbJson_Listar(url,data,cmb){
    //invoca el metodo obtener Json por Get
    $.getJSON(url,data, function(json){

        //borramos el contenido de los option del select
        $("#"+cmb).html('');

        //recorremos todas las filas del resultado del proceso que obtenemos en Json
        $.each(json.resultado, function(i,item){
		//introducimos los option del Json obtenido
            $("#"+cmb).append("<option value='" + item.value + "'>" + item.nombre + "</option>");
        });
    });
}