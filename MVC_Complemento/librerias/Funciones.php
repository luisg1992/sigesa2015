<?php

/******************************Funciones para cambiar la Hora de Programacion **********************************/

function Obtener_Minutos($hora_programada){
    $minutos=substr($hora_programada, -2); 
    return $minutos;
}

//Substraer los digitos solo de la hora de la hora programada
function Obtener_Hora($hora_programada){
    $hora=substr($hora_programada, 0, 2); 
    return $hora;
}

//Funcion para reemplazar el 00 por el 60min para poder realizar la resta respectiva
function Validar_Minutos($minutos){
    switch ($minutos) {
        case '00':
            return '60';
            break;
        case '01':
            return '61';
            break;
       /* case '02':
            return '62';
            break;
        case '03':
            return '63';
            break;*/
        default:
            return $minutos;
    }
}

//Funcion para Revisar si solo tiene un solo caracter se le agrega el 0 en la parte izquierda
function Retornar_Caracteres($minutos){
    if(strlen($minutos)==1)
    {
        return '0'.$minutos;
    }
    else
    {
        return $minutos;
    }
}

//Funcion que retorna los valores como se necesita para poder realizar la actualizacion 
function Retornar_Formato_Hora_Programada($hora,$minutos){
    return Retornar_Caracteres($hora).':'.Retornar_Caracteres($minutos);
}


function Calcular_Hora_minutos($hora_cita,$cupos,$minutos_atencion){
    $min=Obtener_Minutos($hora_cita);
    $hora=Obtener_Hora($hora_cita);
    for ($i = 1; $i <= $cupos; $i++) {
        $min=Validar_Minutos($min);
        if($min>=60){
        $hora=$hora-1;
        }
        $min=$min-$minutos_atencion;
    }
    return Retornar_Formato_Hora_Programada($hora,$min);
}





   //ver fecha del formato aaa-mm-dd  a dd/mm/aaaa
	function Devuelve_Sexo($Sexo){
		if($Sexo==2)		
		{
		return 'F';
		}
		else
		{
		return 'M';
		}
	}
   

   // funcion para rellenar de 0  las horas menores a 10
	function  rellena_ceros($digito)
	{
		if(strlen($digito)==1)
		{
			return '0'.$digito;
		}
		else
		{
			return $digito;	
		}
	}
	// funcion para calcular la Hora de Inicio
	function  Calcular_HoraInicio($hora,$minutos,$tiempo,$cantidad_cupos)
	{
		for($i=1;$i<=$cantidad_cupos;$i++){
		if(strlen(abs($minutos))==1)
		{
		$hora_restadas=$hora_restadas+1;
		$minutos='6'.abs($minutos);	
		}
		$minutos=$minutos-$tiempo;	
		}
		$hora=$hora-$hora_restadas;
		echo rellena_ceros($hora).':'.rellena_ceros($minutos);
	}
   
   
   
    // funcion para dividir dos valores
	function Division($a,$b)
	{
		if($a==0 or $b==0)		
		{
		return 0;
		}
		else
		{
		return number_format($a/$b,2);
		}
	}
 
	  //funcion nombre de la PC
     function NombrePC(){
	 $direccionIP = $_SERVER ['REMOTE_ADDR'];
	 return $ips= @gethostbyaddr($direccionIP);
	 
	 }
	 
	 //Ip de la  pc
	 function IPPC(){
	 return $direccionIP = $_SERVER ['REMOTE_ADDR'];
	  $ips= @gethostbyaddr($direccionIP);
	 
	 }
	 
	
	 //Retornar la Hora en Formato Deseado
	function retornar_hora($h)
	{
	  if('A'==substr($h, -2, -1))
	  {
	  $hora=substr($h, 0, 5);
	  }
	  else
	  {
	  $suma=12+substr($h, 0,2);
	  $hora=substr($h, 2,3);
	  $hora=$suma.$hora;
	  }
	  return $hora;
	}
	  
	//Retornar la Fecha  JLCH
	function Devolver_Fecha_Formato($fechahora){
		 
		$newfecha=substr($fechahora,0,10);
		if($newfecha!=NULL)
		{
		list($anio,$mes,$dia)=explode("-",$newfecha);
		return $dia."/".$mes."/".$anio.'  '.substr($fechahora, 10, 9);
		}	
	}	
	
	
	
	//ip Linux  
	function IPLinux ($ip) {
	
	 $host = `host $ip`;
	 return (($host ? end ( explode (' ', $host)) : $ip));
	}
	
	//ip Windows
	
	function IPWindows ($ip) {
	 $host = split('Name:',`nslookup $ip`);
	 return ( trim (isset($host[1]) ? str_replace ("\n".'Address:  '.$ip, '', $host[1]) : $ip));
	}
	


    //direccion MAC
	
	function DireccionMAC($ip){
	
	    $comando=`/usr/sbin/arp $ip`;
		$comando=`arp -a $ip`;
        ereg(".{1,2}-.{1,2}-.{1,2}-.{1,2}-.{1,2}-.{1,2}|.{1,2}:.{1,2}:.{1,2}:.{1,2}:.{1,2}:.{1,2}", $comando, $mac);

		return  $mac[0]; 	
			
  	}
	
 
			   function Terminal(){
     		   $ipx=IPPC();
   			   $macx=DireccionMAC($ipx);
			   $nompcx=NombrePC();   
			   //$terminalx=$ipx."|".$macx."|".$nompcx;
			   $terminalx=$nompcx;	   	   
			 return $terminalx;
			   }


function gfecha($fecha){
    list($anio,$mes,$dia)=explode("/",$fecha);
    return $dia."-".$mes."-".$anio;
}
//echo gfecha('11/12/2010'); Res : 2010-12-11



// funcion que retorna como necesita sql server  recogiendo del easydate del easyui
function sqlfecha_devolver($fecha){
	 
	$arreglo = explode("-", $fecha);
	$fecha=$arreglo[2].'-'.$arreglo[1].'-'.$arreglo[0];	
	return   $fecha; 	
}



	
//ver fecha del formato aaa-mm-dd  a dd/mm/aaaa
function vfecha($fecha){
    list($anio,$mes,$dia)=explode("-",$fecha);
    return $dia."/".$mes."/".$anio;
}

function vfechahora($fechahora){
	// 2011-06-01 20:00:00
	  $newfecha=substr($fechahora,0,10);
	if($newfecha!=NULL){
    list($anio,$mes,$dia)=explode("-",$newfecha);
	
    return $dia."/".$mes."/".$anio." ".$hora=substr($fechahora,11,5);
		}
} 
function gfechahora($fechahora){
	 
	  $newfecha=substr($fechahora,0,10);
	if($newfecha!=NULL){
    list($anio,$mes,$dia)=explode("/",$newfecha);
	
    return $dia."-".$mes."-".$anio." ".$hora=substr($fechahora,11,5);
		}
		
	}
  //////////////////////////////
  function gfechatoño($fechahora){
  // 2011-06-01 20:00:00
    $newfecha=substr($fechahora,0,10);
  if($newfecha!=NULL){
    list($anio,$mes,$dia)=explode("-",$newfecha);
  
    return $dia."/".$mes."/".$anio." ";
    }}
    function gfechahoratoño($fechahora){
   
    $newfecha=substr($fechahora,0,10);
  if($newfecha!=NULL){
    list($anio,$mes,$dia)=explode("/",$newfecha);
  
    return $dia."".$mes."".$anio." ".$hora=substr($fechahora,11,5);
    }
    
  }
  //////////////////////////////
 // Funcion Para separar Nombres
 
 function SepararApellidosNombres($full_name){
  /* separar el nombre completo en espacios */
   $tokens = explode(' ', trim($full_name));
  /* arreglo donde se guardan las "palabras" del nombre */
  $names = array();
  /* palabras de apellidos (y nombres) compuetos */
  $special_tokens = array('da', 'de', 'del', 'la', 'las', 'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa');
  
  $prev = "";
  foreach($tokens as $token) {
      $_token = strtolower($token);
      if(in_array($_token, $special_tokens)) {
          $prev .= "$token ";
      } else {
          $names[] = $prev. $token;
          $prev = "";
      }
  }
  
  $num_nombres = count($names);
   
               
  switch ($num_nombres) {
      case 0:
           $apellidos1  = $names[0];
          break;
      case 1: 
          $apellidos1=$names[0];
		  //$nombres = $names[0];
          break;
      case 2:
          $apellidos1  = $names[0];
		  $apellidos2  = $names[1];
		  $nombres1  = $names[2];
		  
          break;
      case 3:

		  $apellidos1  = $names[0];
		  $apellidos2  = $names[1];
		  $nombres1  = $names[2];
		  $nombres2  = $names[2];
		  
		  
      default:
	      $apellidos1  = $names[0];
		  $apellidos2  = $names[1];
		  $nombres1  = $names[2];
		  $nombres2  = $names[3];
		  
          unset($names[0]);
          unset($names[1]);
          
          $nombres = implode(' ', $names);
          break;
  }
  
$apellidos1  = mb_convert_case($apellidos1, MB_CASE_TITLE, 'UTF-8');
$apellidos2  = mb_convert_case($apellidos2, MB_CASE_TITLE, 'UTF-8');
$nombres1    = mb_convert_case($nombres1, MB_CASE_TITLE, 'UTF-8');
$nombres2    = mb_convert_case($nombres2, MB_CASE_TITLE, 'UTF-8');
return  $nombreapellidos =$apellidos1.'|'.$apellidos2.'|'.$nombres1.'|'.$nombres2;
}

function fecha_hoy()
	{
	$fecha=date("d-m-Y H:i:s");
	$dia=substr($fecha, 0, 2);  
	$mes=substr($fecha, 3, 2);  
	$anio=substr($fecha, 6, 4); 
	$hoy=$dia.'/'.$mes.'/'.$anio;
	return $hoy;
	}

function sqlfecha($fechahora){
	 
	$newfecha=substr($fechahora,0,10);
	if($newfecha!=NULL){
    list($anio,$mes,$dia)=explode("/",$newfecha);
	
    return $dia."-".$mes."-".$anio;
		}
		
	}
	
	
 
function tiempo_transcurrido($fecha_nacimiento, $fecha_control)
{
   $fecha_actual = $fecha_control;
   
   if(!strlen($fecha_actual))
   {
      $fecha_actual = date('d/m/Y');
   }

   // separamos en partes las fechas 
   $array_nacimiento = explode ( "/", $fecha_nacimiento ); 
   $array_actual = explode ( "/", $fecha_actual ); 

   $anos =  $array_actual[2] - $array_nacimiento[2]; // calculamos años 
   $meses = $array_actual[1] - $array_nacimiento[1]; // calculamos meses 
   $dias =  $array_actual[0] - $array_nacimiento[0]; // calculamos días 

   //ajuste de posible negativo en $días 
   if ($dias < 0) 
   { 
      --$meses; 

      //ahora hay que sumar a $dias los dias que tiene el mes anterior de la fecha actual 
      switch ($array_actual[1]) { 
         case 1: 
            $dias_mes_anterior=31;
            break; 
         case 2:     
            $dias_mes_anterior=31;
            break; 
         case 3:  
            if (bisiesto($array_actual[2])) 
            { 
               $dias_mes_anterior=29;
               break; 
            } 
            else 
            { 
               $dias_mes_anterior=28;
               break; 
            } 
         case 4:
            $dias_mes_anterior=31;
            break; 
         case 5:
            $dias_mes_anterior=30;
            break; 
         case 6:
            $dias_mes_anterior=31;
            break; 
         case 7:
            $dias_mes_anterior=30;
            break; 
         case 8:
            $dias_mes_anterior=31;
            break; 
         case 9:
            $dias_mes_anterior=31;
            break; 
         case 10:
            $dias_mes_anterior=30;
            break; 
         case 11:
            $dias_mes_anterior=31;
            break; 
         case 12:
            $dias_mes_anterior=30;
            break; 
      } 

      $dias=$dias + $dias_mes_anterior;

      if ($dias < 0)
      {
         --$meses;
         if($dias == -1)
         {
            $dias = 30;
         }
         if($dias == -2)
         {
            $dias = 29;
         }
      }
   }

   //ajuste de posible negativo en $meses 
   if ($meses < 0) 
   { 
      --$anos; 
      $meses=$meses + 12; 
   }

   $tiempo[0] = $anos;
   $tiempo[1] = $meses;
   $tiempo[2] = $dias;

   return $tiempo;
}

function bisiesto($anio_actual){ 
   $bisiesto=false; 
   //probamos si el mes de febrero del año actual tiene 29 días 
     if (checkdate(2,29,$anio_actual)) 
     { 
      $bisiesto=true; 
   } 
   return $bisiesto; 
}
/*********************************************************************************FUNCION TEMPORA PARA ELIMINAR SIMPRE Q SE HAYA TERMINADO TODO  LO QUE ES DE HIS*****/

// Calcular Fecha	
	function  CalcularEdad($init)
{
     $diferencia = strtotime(date("Y/m/d")) - strtotime($init);

    //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
    //floor devuelve el número entero anterior, si es 5.7 devuelve 5
    if($diferencia < 60){
        $tiempo = " " . floor($diferencia) . "<BR> Se";
    }else if($diferencia > 60 && $diferencia < 3600){
        $tiempo = " " . floor($diferencia/60) . "<BR> Mi'";
    }else if($diferencia > 3600 && $diferencia < 86400){
        $tiempo = " " . floor($diferencia/3600) . "<BR> H";
    }else if($diferencia > 86400 && $diferencia < 2592000){
        $tiempo = " " . floor($diferencia/86400) . "<BR> D";
    }else if($diferencia > 2592000 && $diferencia < 31104000){
        $tiempo = " " . floor($diferencia/2592000) . "<BR> M";
    }else if($diferencia > 31104000){
        $tiempo = " " . floor($diferencia/31104000) . "<BR> A";
    }else{
        $tiempo = " ";
    }
    return $tiempo;
}	   

	function CalcularEdadTonno($fecha)
  {
    $anionaz = date("Y",strtotime($fecha)); 
    $anioactual = date("Y");
    $mesnaz = date("n",strtotime($fecha));
    $mesact = date("n");
    $dianaz = date("j",strtotime($fecha));
    $diaact = date("j");
    $edad = $anioactual-$anionaz;;
    if($mesact == $mesnaz && $dianaz > $diaact)
      {$edad = $anioactual-$anionaz-1;}
    if($mesact < $mesnaz)
      {$edad = $anioactual-$anionaz-1;}
    
    return $edad;
  } 
		
		
		// calcular solo fecha 
		
		function CalcularAnios($init)
{
    //formateamos las fechas a segundos tipo 1374998435
    $diferencia = strtotime(date("Y/m/d")) - strtotime($init);

    //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
    //floor devuelve el número entero anterior, si es 5.7 devuelve 5
    if($diferencia < 60){
        $tiempo = "SE" . floor($diferencia) ;
    }else if($diferencia > 60 && $diferencia < 3600){
        $tiempo = "" . floor($diferencia/60) ;
    }else if($diferencia > 3600 && $diferencia < 86400){
        $tiempo = "MI" . floor($diferencia/3600) ;
    }else if($diferencia > 86400 && $diferencia < 2592000){
        $tiempo = "D" . floor($diferencia/86400) ;
    }else if($diferencia > 2592000 && $diferencia < 31104000){
        $tiempo = "M" . floor($diferencia/2592000) ;
    }else if($diferencia > 31104000){
        $tiempo = "A" . floor($diferencia/31104000) ;
    }else{
        $tiempo = "0";
    }
    return $tiempo;
}

function TipodeSeguro($valor){
	
	switch ($valor) {
    case 1:
        echo "1";
        break;
    case 2:
        echo "4";
        break;
    case 3:
        echo "";
        break;		
	}
	
}	
	
//FUNCION PARA MHR
/*function nombremes($mes){
	setlocale(LC_TIME, 'spanish');  
	$nombremes=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
	return $nombremes;
}*/
	 
//INICIO PARA BANCO DE SANGRE Por Mahali
 function compararFechas($primera, $segunda)//'d/m/Y'
 { 
  $valoresPrimera = explode ("/", $primera);   
  $valoresSegunda = explode ("/", $segunda); 
  $diaPrimera    = $valoresPrimera[0];  
  $mesPrimera  = $valoresPrimera[1];  
  $anyoPrimera   = $valoresPrimera[2]; 
  $diaSegunda   = $valoresSegunda[0];  
  $mesSegunda = $valoresSegunda[1];  
  $anyoSegunda  = $valoresSegunda[2];
  $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
  $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     
  if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
    // "La fecha ".$primera." no es válida";
    return 0;
  }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
    // "La fecha ".$segunda." no es válida";
    return 0;
  }else{
    return  $diasPrimeraJuliano - $diasSegundaJuliano;
  } 
}

function nombremes($mes){
	setlocale(LC_TIME, 'spanish');  
	$nombremes=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
	return $nombremes;
} 

function getUltimoDiaMes($elAnio,$elMes) {
  return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
}
	
function dias_transcurridos($fecha_i,$fecha_f) //dias_transcurridos('2012-07-01','2012-07-18');
{
	$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias 	= abs($dias); $dias = floor($dias);		
	return $dias;
}

function CalcularHoraSalida($hora_ingreso,$jornal) { //10:55:55 , 00:09:53       
        $hora_ingreso=split(":",$hora_ingreso);//10:55:55
        $jornal=split(":",$jornal); //00:09:53
        $horas=(int)$hora_ingreso[0]+(int)$jornal[0];//10
        $minutos=(int)$hora_ingreso[1]+(int)$jornal[1];//64
		$segundos=(int)$hora_ingreso[2]+(int)$jornal[2];//108
        $horas+=(int)($minutos/60);//10+(64/60)=11
		//$minutos=$minutos%60;(minutos si no hay segundos); 
        $minutos=(int)($segundos/60)+(int)$minutos%60;//(108/60)+(residuo64/60=4)=1+4				
		$segundos=$segundos%60; //residuo 108/60=48
        if($minutos<10)$minutos="0".$minutos ;//05
		if($segundos<10)$segundos="0".$segundos ;//48
        return $hora_salida = $horas.":".$minutos.":".$segundos; //11:05:48
} 
//FIN PARA BANCO DE SANGRE

function tiempo_transcurridos($interval,$fecha_i,$fecha_f) //('2012-07-01 02:00','2012-07-18 01:00');
{
  if($interval=='anno'){ //anno
    $tiempo = (strtotime($fecha_i)-strtotime($fecha_f))/(86400*365);
    $tiempo   = abs($tiempo); $tiempo = floor($tiempo); //floor(3.6)=3

  }else if($interval=='mes'){ //mes
    $tiempo = (strtotime($fecha_i)-strtotime($fecha_f))/(86400*30);
    $tiempo   = abs($tiempo); $tiempo = floor($tiempo);  

  }else if($interval=='dia'){ //dia
    $tiempo = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
    $tiempo   = abs($tiempo); $tiempo = floor($tiempo); 

  }else if($interval=='hora'){ //hora
    $tiempo = (strtotime($fecha_i)-strtotime($fecha_f))/3600;
    $tiempo   = abs($tiempo); $tiempo = round($tiempo);  //round(3.6)=4

  }else if($interval=='minuto'){ //minuto
    $tiempo = (strtotime($fecha_i)-strtotime($fecha_f))/60;
    $tiempo   = abs($tiempo); $tiempo = round($tiempo); 

  }else if($interval=='segundo'){ //segundo
    $tiempo = (strtotime($fecha_i)-strtotime($fecha_f));
    $tiempo   = abs($tiempo); $tiempo = round($tiempo); 
  }
   
  return $tiempo;
}

function DescripcionEstadoHemocomponente($Estado){ //SIGESA_BSD_MovSangreDetalle
    if($Estado=='0'){       
        $EstadoDescripcion='<font color="#FF0000">Eliminado</font>';
        
    }else if($Estado=='1'){
      $EstadoDescripcion='<font color="#0000FF">Disponible</font>';
      
    }else if($Estado=='2'){
      $EstadoDescripcion='<font color="#FF9900">Reservado</font>';  
            
    }else if($Estado=='3'){ //NO ES UN INGRESO, ES TIPO F
      $EstadoDescripcion='<font color="#0000FF">Fracción Pediátrica</font>';
      
    }else if($Estado=='4'){
      $EstadoDescripcion='<font color="#FF9900">Custodia</font>';
      
    }else if($Estado=='5'){
      $EstadoDescripcion='<font color="#8080FF">Tranferido otro Hospital</font>';     

    }else if($Estado=='6'){
      $EstadoDescripcion='<font color="#8080FF">Transfundido Paciente</font>';       
    }

     return   $EstadoDescripcion;  //strip_tags($EstadoDescripcion)
}

function DescripcionEstadoSolicitud($Estado){ //SIGESA_BSD_SolicitudSangreDetalle
    if($Estado=='0'){       
        $EstadoDescripcion='<font color="#FF0000">Eliminado</font>';
        
    }else if($Estado=='1'){
      $EstadoDescripcion='<font color="#0000FF">Registrado</font>';
        
      
    }else if($Estado=='2'){
      $EstadoDescripcion='<font color="#FF9900">Reservado</font>';
        
            
    }else if($Estado=='3'){ 
      $EstadoDescripcion='<font color="#0000FF">Despachado</font>';
      
    }else if($Estado=='4'){
      $EstadoDescripcion='<font color="#FF9900">Facturado</font>';
      
    }else if($Estado=='5'){
      $EstadoDescripcion='<font color="#FF9900">Cerrado</font>';
      
    }

     return   $EstadoDescripcion;  //strip_tags($EstadoDescripcion)
}  

?>