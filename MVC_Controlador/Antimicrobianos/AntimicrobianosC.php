<?php
ini_set('memory_limit','1024M');
?>
<?php
	include("../../MVC_Modelo/AntimicrobianosM.php"); 
	include('../../MVC_Modelo/LaboratorioM.php');
	include('../../MVC_Complemento/librerias/Funciones.php');
	include('../../MVC_Modelo/SistemaM.php');	
		if($_REQUEST["acc"] == "Exportar_Reporte_Excel") {
		$FechaInicio=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$FechaFinal=sqlfecha_devolver($_REQUEST["Fecha_Final"]);		
		$data = Mostrar_Lista_Solicitudes_Antimicrobianos($FechaInicio,$FechaFinal);	
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<title>Reporte de Emergencia</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<body>";
			$contenido .= "<font size='4'>";
			$contenido .= "<table width='100%' > ";
			$contenido .= "<tr>";
			$contenido .= "<td colspan='8'><label>HOSPITAL NACIONAL DANIEL ALCIDES CARRION</label></td>";
			$contenido .= "<td>&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "<table width='100%' border='0'>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td colspan='8'  style='text-align:center;font-size:50px !important'><H2><STRONG>ATM - Lista de Solicitudes (   Del ".$_REQUEST["Fecha_Inicial"]."   al  ".$_REQUEST["Fecha_Final"]. "  &nbsp;)&nbsp;    </STRONG></H2></td>";
			$contenido .= "<td>&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr colspan='8'>";
			$contenido .= "<td>&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "<table width='100%' border='1'>";
			$contenido .= "<tr>";
			$contenido .= "<td>&nbsp;<strong>FechaRegistro</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Estado</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Cuenta</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Historia Clinica</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>DNI</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Nombre de Paciente</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Datos de Atencion</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Funciones Vitales</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Antimicrobianos Solicitados</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Antimicrobianos Previos</strong>&nbsp;</td>";
			$contenido .= "</tr>";
			for ($i=0; $i < count($data); $i++) {			
			$contenido .= "<tr>";	
			$contenido .= "<td>&nbsp;".substr($data[$i]['FechaRegistro'],0,19)."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['Estado']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['IdCuentaAtencion']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroHistoriaClinica']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroDocumento']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['ApellidoPaterno']." ".$data[$i]['ApellidoMaterno']." ".$data[$i]['PrimerNombre']."&nbsp;</td>";
			$contenido .= "<td text-align='center'>";
			$contenido .= "<table>";
			$arreglo_SERV = MostrarCabeceraAntimicrobiano($data[$i]['IdCabeceraAntimicrobiano']);
			$contenido .= "<tr bgcolor='#000   !important'>";
			$contenido .= "<td>&nbsp;<strong> Sexo</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> Servicio</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> Fecha Ingreso</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> Hora Ingreso</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> Cama</strong>&nbsp;</td>";
			$contenido .= "</tr>";
			for ($x=0; $x < count($arreglo_SERV); $x++) {			
			$contenido .= "<tr>";
			$contenido .= "<td>&nbsp;".Devuelve_Sexo($arreglo_SERV[$x]['IdTipoSexo'])."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_SERV[$x]['Nombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".substr(vfechahora($arreglo_SERV[$x]['FechaIngreso']),0, 10)."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_SERV[$x]['HoraIngreso']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_SERV[$x]['CamaPaciente']."&nbsp;</td>";
			$contenido .= "</tr>";
			}
			$contenido .= "</table>";
			$contenido .= "</td>";
			// Datos de funciones Vitales
			$contenido .= "<td text-align='center'>";
			$contenido .= "<table>";
			$arreglo_FV = MostrarFuncionesVitalesAntimicrobiano($data[$i]['IdCabeceraAntimicrobiano']);
			$contenido .= "<tr bgcolor='#000   !important'>";
			$contenido .= "<td><strong> FR</strong></td>";
			$contenido .= "<td>&nbsp;<strong> FC</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> PAS</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> PAD</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> Glasgon</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> Peso</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong> Temperatura</strong>&nbsp;</td>";
			$contenido .= "</tr>";
			for ($x=0; $x < count($arreglo_FV); $x++) {			
			$contenido .= "<tr>";
			$contenido .= "<td>&nbsp;".$arreglo_FV[$x]['Valor_FR']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_FV[$x]['Valor_FC']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_FV[$x]['Valor_PAS']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_FV[$x]['Valor_PAD']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_FV[$x]['Valor_Glasgon']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_FV[$x]['Valor_Peso']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_FV[$x]['Valor_Temperatura']."&nbsp;</td>";
			$contenido .= "</tr>";
			}
			$contenido .= "</table>";
			$contenido .= "</td>";
			// Datos de Antimicrobianos Solicitados
			$contenido .= "<td text-align='center'>";
			$contenido .= "<table>";
			$arreglo_AS = Mostrar_Lista_Detalle_Antimicrobianos($data[$i]['IdCabeceraAntimicrobiano']);
			$contador=1;
			$contenido .= "<tr>";
			$contenido .= "<td><strong>Id&nbsp;</strong></td>";
			$contenido .= "<td>&nbsp;<strong>Codigo</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Nombre</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Dosis</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Frecuencia</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Dias</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Total</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Descripcion</strong>&nbsp;</td>";
			$contenido .= "</tr>";
			for ($j=0; $j < count($arreglo_AS); $j++) {			
			$contenido .= "<tr>";
			$contenido .= "<td><strong>&nbsp;".$contador++."&nbsp;.-</strong></td>";
			$contenido .= "<td>&nbsp;".$arreglo_AS[$j]['Codigo']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_AS[$j]['Nombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_AS[$j]['Dosis']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_AS[$j]['Frecuencia']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_AS[$j]['Dias']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_AS[$j]['Total']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_AS[$j]['Descripcion']."&nbsp;</td>";
			$contenido .= "</tr>";
			}
			$contenido .= "</table>";
			$contenido .= "</td>";
			// Datos de Antimicrobianos Previos
			$contenido .= "<td text-align='center'>";
			$contenido .= "<table>";
			$contenido .= "<tr>";
			$contenido .= "<td><strong>Id&nbsp;</strong></td>";
			$contenido .= "<td>&nbsp;<strong>Codigo</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Nombre</strong>&nbsp;</td>";
			$contenido .= "<td>&nbsp;<strong>Dias</strong>&nbsp;</td>";
			$contenido .= "</tr>";
			$arreglo_PR = MostrarPreviosAntimicrobiano($data[$i]['IdCabeceraAntimicrobiano']);
			$contador=1;
			for ($k=0; $k < count($arreglo_PR); $k++) {			
			$contenido .= "<tr>";
			$contenido .= "<td><strong>&nbsp;".$contador++."&nbsp;.-</strong></td>";
			$contenido .= "<td>&nbsp;".$arreglo_PR[$k]['Codigo']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_PR[$k]['Nombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$arreglo_PR[$k]['Dias']."&nbsp;</td>";
			$contenido .= "</tr>";
			}
			$contenido .= "</table>";
			$contenido .= "</td>";
			$contenido .= "</tr>";	
			}
			$contenido .= "</table>"; 
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";	

			echo json_encode($contenido);
			exit();
		
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	if($_GET["acc"] == "Atm_administrar")
	{
   	include('../../MVC_Vista/Antimicrobianos/Administrar_Antimicrobianos.php');
	}
	
	
	if($_GET["acc"] == "Atm_Examenes")
	{
   	include('../../MVC_Vista/Antimicrobianos/Administrar_Examenes.php');
	}
	
	
	if($_GET["acc"] == "Atm_Lista_Solicitud")
	{
		
	$IdEmpleado=$_REQUEST["IdEmpleado"];
	$IdListItem=$_REQUEST["IdListItem"];	
	$ListarPermisos=Listar_Permisos_Usuario_M($IdEmpleado,$IdListItem);
   	include('../../MVC_Vista/Antimicrobianos/Lista_Solicitudes.php');
	}
	

	if($_GET["acc"] == "Atm_reporte")
	{
   	include('../../MVC_Vista/Antimicrobianos/Reporte_Antimicrobianos.php');
	}

	
	
	if($_GET["acc"] == "Atm_Registro_Solicitud")
	{
   	include('../../MVC_Vista/Antimicrobianos/Registro_Solicitud_Antimicrobianos.php');
	}
	

	// MOSTRAR: Lista de los Pacientes
	
	
	
	if($_REQUEST["acc"] == "MostrarPaciente")
	{
	if($_REQUEST["NroHistoriaClinica"]!=NULL){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='';}
	if($_REQUEST["NroDocumento"]!=NULL){$NroDocumento=$_REQUEST["NroDocumento"];}else{$NroDocumento='';}
	if($_REQUEST["ApellidoPaterno"]!=NULL){$ApellidoPaterno=$_REQUEST["ApellidoPaterno"];}else{$ApellidoPaterno='%';}
	if($_REQUEST["ApellidoMaterno"]!=NULL){$ApellidoMaterno=$_REQUEST["ApellidoMaterno"];}else{$ApellidoMaterno='%';}
	$data = Mostrar_Lista_de_Pacientes_M($NroDocumento,$ApellidoPaterno,$ApellidoMaterno,$NroHistoriaClinica);
		for ($i=0; $i < count($data); $i++) { 
			$Pacientes[$i] = array(
				'ApellidoPaterno'	=> $data[$i]['ApellidoPaterno'],
				'ApellidoMaterno'			=> $data[$i]['ApellidoMaterno'],
				'PrimerNombre'	=> $data[$i]['PrimerNombre'],
				'SegundoNombre'	=> strtoupper($data[$i]['SegundoNombre']),
				'FechaNacimiento'	=> Devolver_Fecha_Formato($data[$i]['FechaNacimiento']),
				'NroDocumento'	=> $data[$i]['NroDocumento'],
				'IdTipoSexo'	=> $data[$i]['IdTipoSexo'],
				'NroHistoriaClinica'	=> $data[$i]['NroHistoriaClinica'],
				'IdPaciente'	=> $data[$i]['IdPaciente'],
				'Edad'	=>	CalcularEdadTonno($data[$i]['FechaNacimiento'])
				);
		}
		echo json_encode($Pacientes);
		exit();
 
	}
	

	
	if ($_REQUEST["acc"] == "MostrarAtencionesporIdPaciente") {
		$IdPaciente=$_REQUEST["IdPaciente"];	
		$data = Mostrar_Lista_Atenciones_IdPaciente($IdPaciente);
		for ($i=0; $i < count($data); $i++) { 
			$Atenciones[$i] = array(
				'IdCuentaAtencion'	=> $data[$i]['IdCuentaAtencion'],
				'FechaIngreso'			=> Devolver_Fecha_Formato($data[$i]['FechaIngreso']),
				'HoraIngreso'			=> $data[$i]['HoraIngreso'],
				'Nombre'			=> $data[$i]['Nombre'],
				'FuenteFinanciamiento'			=> $data[$i]['FuenteFinanciamiento'],
				'TipoServicio'			=> $data[$i]['TipoServicio'],
				'Cama'			=> $data[$i]['Cama']
				);
		}
		echo json_encode($Atenciones);
		exit();
	}
	
	
	
	
	if ($_REQUEST["acc"] == "MostrarSolicitudesAntimicrobianosGrilla") {
		$FechaInicio=sqlfecha_devolver($_REQUEST["FechaInicio"]);
		$FechaFinal=sqlfecha_devolver($_REQUEST["FechaFinal"]);			
		$data = Mostrar_Lista_Solicitudes_Antimicrobianos($FechaInicio,$FechaFinal);
		for ($i=0; $i < count($data); $i++) {		
			$Antimicrobianos[$i] = array(
				'FechaRegistro'					=> vfechahora($data[$i]['FechaRegistro']),
				'ApellidoPaterno'				=> $data[$i]['ApellidoPaterno'],
				'ApellidoMaterno'				=> $data[$i]['ApellidoMaterno'],
				'PrimerNombre'					=> $data[$i]['PrimerNombre'],
				'SegundoNombre'					=> $data[$i]['SegundoNombre'],
				'NroDocumento'					=> $data[$i]['NroDocumento'],
				'NroHistoriaClinica'			=> $data[$i]['NroHistoriaClinica'],
				'IdPaciente'					=> $data[$i]['IdPaciente'],
				'FechaNacimiento'				=>substr($data[$i]['FechaNacimiento'], 0, 10),
				'IdCuentaAtencion'				=> $data[$i]['IdCuentaAtencion'],
				'IdCabeceraAntimicrobiano'		=> $data[$i]['IdCabeceraAntimicrobiano'],
				'IdEstadoAntimicrobiano'		=> $data[$i]['IdEstadoAntimicrobiano'],
				'Estado'						=> $data[$i]['Estado']
				);
		}
		echo json_encode($Antimicrobianos);
		exit();
	}


	if ($_REQUEST["acc"] == "MostrarFuncionesVitalesAntimicrobiano") {
		$IdCabeceraAntimicrobiano=$_REQUEST["IdCabeceraAntimicrobiano"];
		$data = MostrarFuncionesVitalesAntimicrobiano($IdCabeceraAntimicrobiano);
		if (count($data) >= 1) 
		{
			for ($i=0; $i < count($data); $i++){		
			$FuncionesVitales[$i] = array(
				'Valor_FR'		=> $data[$i]['Valor_FR'],
				'Valor_FC'		=> $data[$i]['Valor_FC'],
				'Valor_PAS'		=> $data[$i]['Valor_PAS'],
				'Valor_PAD'		=> $data[$i]['Valor_PAD'],
				'Valor_Glasgon'		=> $data[$i]['Valor_Glasgon'],
				'Valor_Peso'		=> $data[$i]['Valor_Peso'],
				'Valor_Temperatura'			=> $data[$i]['Valor_Temperatura']
			);
			}
		}
		else
		{
			$FuncionesVitales = array();
		}
		echo json_encode($FuncionesVitales);
		exit();
	}
		
	if ($_REQUEST["acc"] == "MostrarPreviosAntimicrobiano") {
		$IdCabeceraAntimicrobiano=$_REQUEST["IdCabeceraAntimicrobiano"];
		$data = MostrarPreviosAntimicrobiano($IdCabeceraAntimicrobiano);
		for ($i=0; $i < count($data); $i++) {		
			$Previos[$i] = array(
				'Codigo'		=> $data[$i]['Codigo'],
				'Nombre'		=> $data[$i]['Nombre'],
				'Dias'		=> $data[$i]['Dias']
				);
		}
		echo json_encode($Previos);
		exit();
	}	
		
	if ($_REQUEST["acc"] == "MostrarDatosCabeceraAntimicrobiano") {
		$IdCabeceraAntimicrobiano=$_REQUEST["IdCabeceraAntimicrobiano"];
		$data = MostrarCabeceraAntimicrobiano($IdCabeceraAntimicrobiano);
		for ($i=0; $i < count($data); $i++) {		
			$Antimicrobianos[$i] = array(
				'IdCabeceraAntimicrobiano'	=> $data[$i]['IdCabeceraAntimicrobiano'],
				'ApellidoPaterno'		=> $data[$i]['ApellidoPaterno'],
				'ApellidoMaterno'		=> $data[$i]['ApellidoMaterno'],
				'PrimerNombre'			=> $data[$i]['PrimerNombre'],
				'SegundoNombre'			=> strtoupper($data[$i]['SegundoNombre']),
				'FechaNacimiento'		=> Devolver_Fecha_Formato($data[$i]['FechaNacimiento']),
				'NroDocumento'			=> $data[$i]['NroDocumento'],
				'IdTipoSexo'			=> $data[$i]['IdTipoSexo'],
				'NroHistoriaClinica'	=> $data[$i]['NroHistoriaClinica'],
				'IdPaciente'			=> $data[$i]['IdPaciente'],
				'IdTipoSexo'			=> $data[$i]['IdTipoSexo'],
				'Edad'					=> CalcularEdadTonno($data[$i]['FechaNacimiento']),
				'Estado'				=> $data[$i]['Estado'],
				'Nombre'				=> $data[$i]['Nombre'],
				'CamaPaciente'				=> $data[$i]['CamaPaciente'],
				'IdCuentaAtencion'		=> $data[$i]['IdCuentaAtencion'],
				'Infeccion_IntraHospitalaria'		=> $data[$i]['Infeccion_IntraHospitalaria'],
				'Descripcion_Intrahospitalaria'		=> $data[$i]['Descripcion_Intrahospitalaria'],
				'HoraIngreso'			=> $data[$i]['HoraIngreso'],
				'FechaIngreso'			=> Devolver_Fecha_Formato($data[$i]['FechaIngreso']),
				'Descripcion'			=> $data[$i]['Descripcion'],
				'Justificacion'			=> $data[$i]['Justificacion'],
				'Evaluacion'			=> $data[$i]['Evaluacion']
				);
		}
		echo json_encode($Antimicrobianos);
		exit();
	}
	
	if ($_REQUEST["acc"] == "MostrarDiagnosticosCabeceraAntimicrobiano") {
		$IdCabeceraAntimicrobiano=$_REQUEST["IdCabeceraAntimicrobiano"];
		$data = MostrarDiagnosticosAntimicrobiano($IdCabeceraAntimicrobiano);
		for ($i=0; $i < count($data); $i++) {		
			$Diagnosticos[$i] = array(
				'DescripcionA'		=> $data[$i]['DescripcionA'],
				'CodigoA'		=> $data[$i]['CodigoA'],
				'DescripcionB'		=> $data[$i]['DescripcionB'],
				'CodigoB'			=> $data[$i]['CodigoB'],
				'DescripcionC'		=> $data[$i]['DescripcionC'],
				'CodigoC'		=> $data[$i]['CodigoC']
				);
		}
		echo json_encode($Diagnosticos);
		exit();
	}
	
	if ($_REQUEST["acc"] == "MostrarDetalleAntimicrobianos") {
		$Id_Formulario_Antimicrobiano = $_REQUEST["IdCabeceraAntimicrobiano"];
		$data = Mostrar_Lista_Detalle_Antimicrobianos($Id_Formulario_Antimicrobiano);
		if (count($data) >= 1) 
		{
			for ($i=0; $i < count($data); $i++) {		
				$Antimicrobianos[$i] = array(
					'Dosis'			=> $data[$i]['Dosis'],
					'Frecuencia'		=> $data[$i]['Frecuencia'],
					'Dias'		=> $data[$i]['Dias'],
					'Total'			=> $data[$i]['Total'],
					'Nombre'			=> $data[$i]['Nombre'],
					'Codigo'			=> $data[$i]['Codigo'],
					'IdEstadoAntimicrobiano'			=> $data[$i]['Estado'],
					'IdCabeceraAntimicrobiano'			=> $data[$i]['IdCabeceraAntimicrobiano'],
					'Id_Antimicrobiano'			=> $data[$i]['Id_Antimicrobiano'],
					'Descripcion'			=> $data[$i]['Descripcion'],
					'FechaCalificacion'			=> vfechahora($data[$i]['FechaCalificacion'])
					);
			}
		}
		else
		{
			$Antimicrobianos = array();
		}
		echo json_encode($Antimicrobianos);
		exit();
	
	}

	
	if ($_REQUEST["acc"] == "MostrarAntimicrobianosGrilla") {

		$data = Mostrar_Lista_de_Antimicrobianos('%%');
		for ($i=0; $i < count($data); $i++) { 
			$Antimicrobianos[$i] = array(
				'Codigo'	=> $data[$i]['Codigo'],
				'Nombre'			=> $data[$i]['Nombre'],
				'IdProducto'	=> $data[$i]['IdProducto'],
				'IdTipoAntimicrobiano'	=> $data[$i]['IdTipoAntimicrobiano'],
				'Descripcion'	=> $data[$i]['Descripcion']
				);
		}
		echo json_encode($Antimicrobianos);
		exit();
	}
	
	function Retornar_Stock_Medicamento($IdProducto)
	{		
		$data = Mostrar_Stock_Medicamento($IdProducto);
		if (count($data) >= 1) 
		{
		$Retornar_Stock=$data[0]['Cant'];	
		return number_format($Retornar_Stock,0);	
		}
		else
		{
		return  '0.00';
		}
	}
	
	
	if ($_REQUEST["acc"] == "MostrarAntimicrobianosControladosGrilla") {

		$data = Mostrar_Lista_de_Antimicrobianos('%2%');
		for ($i=0; $i < count($data); $i++) { 
			$Antimicrobianos[$i] = array(
				'Codigo'	=> $data[$i]['Codigo'],
				'Nombre'			=> $data[$i]['Nombre'],
				'IdProducto'	=> $data[$i]['IdProducto'],
				'IdTipoAntimicrobiano'	=> $data[$i]['IdTipoAntimicrobiano'],
				'Descripcion'	=> $data[$i]['Descripcion'],
				'Stock'	=> Retornar_Stock_Medicamento($data[$i]['IdProducto'])
				);
		}
		echo json_encode($Antimicrobianos);
		exit();
	}
	
	
	if ($_REQUEST["acc"] == "MostrarMedicamentosGrilla") {

		$data = Mostrar_Lista_de_Medicamentos();
		for ($i=0; $i < count($data); $i++) { 
			$Medicamentos[$i] = array(
				'IdProducto'	=> $data[$i]['IdProducto'],
				'Codigo'	=> $data[$i]['Codigo'],
				'Nombre'			=> $data[$i]['Nombre']
				);
		}
		echo json_encode($Medicamentos);
		exit();
	}
	


	if ($_REQUEST["acc"] == "MostrarDiagnosticosGrilla") 
	{
		$data = Mostrar_Lista_Diagnosticos();
		for ($i=0; $i < count($data); $i++) { 
			$Lista_Diagnosticos[$i] = array(
				'CodigoCIE10'	=> $data[$i]['CodigoCIE10'],
				'Descripcion'	=> $data[$i]['Descripcion'],
				'IdDiagnostico'	=> $data[$i]['IdDiagnostico']
				);
		}
		echo json_encode($Lista_Diagnosticos);
		exit();
	}


	
	if ($_REQUEST["acc"] == "Agregar_Cabecera_Antimicrobiano") {
		$FechaRegistro=date("Y-m-d H:i:s");
		$IdPaciente=$_REQUEST["IdPaciente"];
		$IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];

		if (empty($_REQUEST["IdDiagnosticoPatologico"]) or $_REQUEST["IdDiagnosticoPatologico"]=='') { 
		$IdDiagnosticoPatologico='0';
		$IdDiagnosticoPatologico='0';
		} 
		else {
		$IdDiagnosticoPatologico=$_REQUEST["IdDiagnosticoPatologico"];
		}

		if (empty($_REQUEST["IdDiagnosticoInfeccioso"]) or $_REQUEST["IdDiagnosticoInfeccioso"]=='') { 
		$IdDiagnosticoInfeccioso='0';
		$IdDiagnosticoInfeccioso='0';
		} 
		else {
		$IdDiagnosticoInfeccioso=$_REQUEST["IdDiagnosticoInfeccioso"];
		}
		
		if (empty($_REQUEST["IdDiagnosticoOtro"]) or $_REQUEST["IdDiagnosticoOtro"]=='') { 
		$IdDiagnosticoOtro='0';
		$IdDiagnosticoOtro='0';
		} 
		else {
		$IdDiagnosticoOtro=$_REQUEST["IdDiagnosticoOtro"];
		}
	
		$Infeccion_IntraHospitalaria=$_REQUEST["Infeccion_IntraHospitalaria"];
		if($Infeccion_IntraHospitalaria==0)
		{
		$Descripcion_IntraHospitalaria='';
		}
		{
		$Descripcion_IntraHospitalaria=trim ($_REQUEST["Descripcion_IntraHospitalaria"]);	
		}
		
		$Justificacion=trim ($_REQUEST["Justificacion"]);
		$Estado=$_REQUEST["Estado"];
		$data =  Agregar_Cabecera_Antimicrobianos_M($FechaRegistro,$IdPaciente,$IdCuentaAtencion,$IdDiagnosticoPatologico,$IdDiagnosticoInfeccioso,
		$IdDiagnosticoOtro,$Infeccion_IntraHospitalaria,$Descripcion_IntraHospitalaria,$Justificacion,$Estado);
		echo json_encode($data);
		exit();
		}

		if ($_REQUEST["acc"] == "Agregar_Previo_Antimicrobiano") {
		
			$IdCabeceraAntimicrobiano = $_REQUEST["IdCabeceraAntimicrobiano"];
			$IdAntimicrobiano = $_REQUEST["IdAntimicrobiano"];
			$Dias = $_REQUEST["Dias"];
			$data = Agregar_Previo_Antimicrobiano_M($IdCabeceraAntimicrobiano,$IdAntimicrobiano,$Dias);
			echo json_encode($data);
			exit();
		}
	
		if ($_REQUEST["acc"] == "Agregar_Detalle_Antimicrobiano") {
		
			$IdCabeceraAntimicrobiano = $_REQUEST["IdCabeceraAntimicrobiano"];
			$IdAntimicrobiano = $_REQUEST["IdAntimicrobiano"];
			$Dosis = $_REQUEST["Dosis"];
			$Frecuencia = $_REQUEST["Frecuencia"];
			$Dias = $_REQUEST["Dias"];
			$Total = $_REQUEST["Total"];
			$Estado = $_REQUEST["Estado"];
			$data = Agregar_Detalle_Antimicrobianos_M($IdCabeceraAntimicrobiano,$IdAntimicrobiano,$Dosis,$Frecuencia,$Dias,$Total,$Estado);
			echo json_encode($data);
			exit();
		}
		
	
		if ($_REQUEST["acc"] == "Agregar_Funciones_Vitales") {
			
		$Id_Formulario_Antimicrobiano = $_REQUEST["Id_Formulario_Antimicrobiano"];
		$FuncionV_FR = $_REQUEST["FuncionV_FR"];
		$FuncionV_FC = $_REQUEST["FuncionV_FC"];
		$FuncionV_PAS = $_REQUEST["FuncionV_PAS"];
		$FuncionV_PAD = $_REQUEST["FuncionV_PAD"];
		$FuncionV_Glasgon = $_REQUEST["FuncionV_Glasgon"];
		$FuncionV_Peso = $_REQUEST["FuncionV_Peso"];
		$FuncionV_Temperatura = $_REQUEST["FuncionV_Temperatura"];
		$data = Agregar_Funciones_Vitales_M($Id_Formulario_Antimicrobiano,$FuncionV_FR,$FuncionV_FC,$FuncionV_PAS,$FuncionV_PAD,$FuncionV_Glasgon,$FuncionV_Peso,$FuncionV_Temperatura);
		echo json_encode($data);
		exit();
	    }
	
	
	

	
	if ($_REQUEST["acc"] == "ResultadosDetallados") {
	$idOrden = $_REQUEST["idorden"];
	$idProductoCpt = $_REQUEST["idproductocpt"];

	$data = SigesaLaboratorioMostrarDetalleOrdenResultadoParaImprimir_M($idOrden,$idProductoCpt);

	for ($i=0; $i < count($data); $i++) { 
		$resultadodetallado_json[$i] = array(
			'GRUPO'				=> $data[$i]["Grupo"],
			'ITEM'				=> $data[$i]["Item"],
			'VALORNUMERO'		=> number_format($data[$i]["ValorNumero"],3),	
			'VALORTEXTO'		=> $data[$i]["ValorTexto"],	
			'VALORCOMBO'		=> $data[$i]["ValorCombo"],
			'VALORCHECK'		=> $data[$i]["ValorCheck"],	
			'VALORREFERENCIAL'	=> $data[$i]["ValorReferencial"],
			'METODO'			=> $data[$i]["Metodo"],
			'REALIZAP'			=> $data[$i]["REALIZAP"],
			'FECHA'				=> $data[$i]["Fecha"],
			'IDPRODCPT'			=> $data[$i]["idProductoCpt"],
			'ORDENXRESULTADO'	=> $data[$i]["ordenXresultado"],
			'SOLONUMERO'		=> $data[$i]["SoloNumero"],
			'SOLOTEXTO'			=> $data[$i]["SoloTexto"]
			);
	}

	echo json_encode($resultadodetallado_json);
	exit();
	}
	
	
	if ($_REQUEST["acc"] == "ModificarAntimicrobiano") {
		
		$IdProducto = $_REQUEST["IdProducto"];
		$IdTipoAntimicrobiano = $_REQUEST["IdTipoAntimicrobiano"];
		$data = Modificar_Antimicrobiano($IdProducto,$IdTipoAntimicrobiano);
		echo json_encode($data);
		exit();
	}
	
	
	
	if ($_REQUEST["acc"] == "AgregarAntimicrobiano") {
	
		$IdProducto = $_REQUEST["IdProducto"];
		$data = Agregar_Antimicrobiano($IdProducto) ;
		echo json_encode($data);
		exit();
	}
	
	
	

	if ($_REQUEST["acc"] == "EliminarAntimicrobiano") {
	
		$IdProducto = $_REQUEST["IdProducto"];
		$data = Eliminar_Antimicrobiano($IdProducto);
		echo json_encode($data);
		exit();
	}
	
	if ($_REQUEST["acc"] == "ModificarCabecerasDetalle") {

		$IdCabeceraAntimicrobiano = $_REQUEST["IdCabeceraAntimicrobiano"];
		$Id_Antimicrobiano = $_REQUEST["Id_Antimicrobiano"];
		$Dias = $_REQUEST["Dias"];
		$Total = $_REQUEST["Total"];
		$IdEstadoAntimicrobiano = $_REQUEST["IdEstadoAntimicrobiano"];
		$FechaModifica=gfechahora(date("d/m/Y H:i:s"));	
		$data = Modificar_Cabecera_Detalle_Antimicrobiano($IdCabeceraAntimicrobiano,$Id_Antimicrobiano,$Dias,$Total,$IdEstadoAntimicrobiano,$FechaModifica);
		echo json_encode($data);
		exit();
	}

	if ($_REQUEST["acc"] == "ModificarCabecerasEvaluacion") {

		$IdCabeceraAntimicrobiano = $_REQUEST["IdCabeceraAntimicrobiano"];
		$Evaluacion = $_REQUEST["Evaluacion"];
		$IdEstadoAntimicrobiano='5';
		$data = Modificar_Cabecera_Evaluacion_Antimicrobiano($IdCabeceraAntimicrobiano,$Evaluacion,$IdEstadoAntimicrobiano);
		echo json_encode($data);
		exit();
	}

	
	if ($_REQUEST["acc"] == "MostrarExamenes") {
		$data = Mostrar_Lista_Examenes();
		for ($i=0; $i < count($data); $i++) { 
			$Atenciones[$i] = array(
				'IdProducto'	=> $data[$i]['IdProducto'],
				'Nombre'			=> $data[$i]['Nombre'],
				'Codigo'			=> $data[$i]['Codigo']
				);
		}
		echo json_encode($Atenciones);
		exit();
	}
		
	
	
	if ($_REQUEST["acc"] == "MostrarAntimicrobianosExamenes") {
		$data = Mostrar_Lista_Antimicrobianos_Examenes();
		for ($i=0; $i < count($data); $i++) { 
			$Atenciones[$i] = array(
				'IdProducto'	=> $data[$i]['IdProducto'],
				'Nombre'			=> $data[$i]['Nombre'],
				'Codigo'			=> $data[$i]['Codigo']
				);
		}
		echo json_encode($Atenciones);
		exit();
	}
	
		

		
	if ($_REQUEST["acc"] == "AgregarExamenAntimicrobiano") {
	
		$IdProducto = $_REQUEST["IdProducto"];
		$data = Agregar_Examen_Antimicrobiano($IdProducto) ;
		echo json_encode($data);
		exit();
	}
	
	
	

	if ($_REQUEST["acc"] == "EliminarExamenAntimicrobiano") {
	
		$IdProducto = $_REQUEST["IdProducto"];
		$data = Eliminar_Examen_Antimicrobiano($IdProducto);
		echo json_encode($data);
		exit();
	}
	
	if ($_REQUEST["acc"] == "EliminarSolicitudRegistroAntimicrobiano") {
	
		$IdCabeceraAntimicrobiano = $_REQUEST["IdCabeceraAntimicrobiano"];
		$data = Eliminar_Solicitud_Registro_Antimicrobiano($IdCabeceraAntimicrobiano);
		echo json_encode($data);
		exit();
	}

	
?>