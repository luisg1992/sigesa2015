<?php
	include('../../MVC_Modelo/SistemaM.php');
	include("../../MVC_Modelo/ArchivoClinicoM.php");
	include("../../MVC_Modelo/ServiciosM.php");
	include("../../MVC_Modelo/PacientesM.php");
	include("../../MVC_Modelo/EmpleadosM.php");
	include('../../MVC_Complemento/librerias/Funciones.php');
	$ListarFechaServidor=RetornaFechaServidorSQL_C();
	$FechaServidor = $ListarFechaServidor[0][0];

    if ($_POST["acc"] == "Insertar_Retorno_General_Array")
	{	
	$IdPaciente = $_POST['IdPaciente'];
	$Observacion = $_POST['Observacion'];
	$IdEmpleado = $_POST['IdEmpleado'];
	$FechaCita='';
	$array_paciente = explode("/", $IdPaciente);
	$array_observacion = explode("/", $Observacion);
	for($i = 0; $i <count($array_paciente); $i++) 
	{   
	   // $Insertar_Retorno_General_Array=Seguimiento_Historia_Clinica_Agregar_C(gfecha($FechaCita),$array_servicio[$i],$array_paciente[$i],$comentario[$i],$IdEmpleado);
		$Insertar_Retorno_General_Array=Insertar_Retorno_General_Array_C(gfecha($FechaCita),$array_paciente[$i],$array_observacion[$i],$IdEmpleado);
	}


	}
	
	
	if ($_POST["acc"] == "Historia_Clinica_Salida_Validacion")
	{	
	$NroHistoriaClinica = $_POST['NroHistoriaClinica'];
	$Fecha_Inicio = gfecha($_POST['Fecha_Inicio']);
	$Fecha_Final = gfecha($_POST['Fecha_Final']);	
	$data = Listar_Citas_de_Paciente_por_Historia_Clinica_C($NroHistoriaClinica,$Fecha_Inicio,$Fecha_Final);
	echo count($data);
	exit();
	}
	
	
			
	if($_POST["acc"] == "Verificar_Salida_HistoriaClinica")
	{
	$NroHistoriaClinica = $_POST['NroHistoriaClinica'];
	$Fecha_Inicio = gfecha($_POST['Fecha_Inicio']);
	$Fecha_Final = gfecha($_POST['Fecha_Final']);
	$Verificar_Salida_HC_Check=Verificar_Salida_HistoriaClinica_C($NroHistoriaClinica,$Fecha_Inicio,$Fecha_Final);	
	}
		



	
	if ($_REQUEST["acc"] == "ConserjeMostrarListaCitas_Salida") {
		$HistoriaClinica =  trim($_REQUEST["HistoriaClinica"]);
		$FechaCita =sqlfecha_devolver($_REQUEST["FechaCita"]);
		$data = Conserje_Listar_Citas_Paciente_Salida_C($HistoriaClinica,$FechaCita);
			if (count($data) == 0) 
			{
			$arreglo = "M_0";
			$datos[0] = array(
				'variable' 					=> $arreglo  
				);
			}
			if (count($data) >= 1) 
			{
			$arreglo = "M_1";
			for ($i=0; $i < count($data); $i++) { 
			$datos[$i] = array(
				'IdAtencion'				=> $data[$i]['IdAtencion'],
				'HoraInicio'				=> $data[$i]['HoraInicio'],
				'NroHistoriaClinica'		=> $data[$i]['NroHistoriaClinica'],
				'NombrePaciente'			=> $data[$i]['NombreCompleto'],
				'IdPaciente'				=> $data[$i]['IdPaciente'] ,
				'NombreServicio'			=> $data[$i]['Nombre'] ,
				'FechaSolicitud'			=> $data[$i]['FechaSolicitud'] ,
				'Descripcion'				=> $data[$i]['Descripcion'] ,
				'variable' 					=> $arreglo  
				);
			}
			}
			
			
		echo json_encode($datos);
		exit();
	}
	
	if ($_REQUEST["acc"] == "ConserjeMostrarListaCitas_Retorno") {
		$HistoriaClinica =  trim($_REQUEST["HistoriaClinica"]);
		$FechaCita =sqlfecha_devolver($_REQUEST["FechaCita"]);
		$data = Conserje_Listar_Citas_Paciente_Retorno_C($HistoriaClinica,$FechaCita);
			if (count($data) == 0) 
			{
			$arreglo = "M_0";
			$datos[0] = array(
				'variable' 					=> $arreglo  
				);
			}
			if (count($data) >= 1) 
			{
			$arreglo = "M_1";
			for ($i=0; $i < count($data); $i++) { 
			$datos[$i] = array(
				'IdAtencion'				=> $data[$i]['IdAtencion'],
				'HoraInicio'				=> $data[$i]['HoraInicio'],
				'HoraFin'					=> $data[$i]['HoraFin'],
				'NroHistoriaClinica'		=> $data[$i]['NroHistoriaClinica'],
				'NombrePaciente'			=> $data[$i]['NombreCompleto'],
				'IdPaciente'				=> $data[$i]['IdPaciente'] ,
				'NombreServicio'			=> $data[$i]['Nombre'] ,
				'FechaSolicitud'			=> $data[$i]['FechaSolicitud'] ,
				'Descripcion'				=> $data[$i]['Descripcion'] ,
				'variable' 					=> $arreglo  
				);
			}
			}
			
			
		echo json_encode($datos);
		exit();
	}
	
	
	
	//Conserje  06-08-2018
	// Accion para Generar lista de Atenciones de Emergencia 
	
	if ($_REQUEST["acc"] == "MostrarAtencionesEmergencia_Retorno") {
		$HistoriaClinica =  trim($_REQUEST["HistoriaClinica"]);
		$data = Conserje_Listar_Atenciones_Emergencia_Paciente_Retorno_C($HistoriaClinica);
			if (count($data) == 0) 
			{
			$arreglo = "M_0";
			$datos[0] = array(
				'variable' 					=> $arreglo  
				);
			}
			if (count($data) >= 1) 
			{
			$arreglo = "M_1";
			for ($i=0; $i < count($data); $i++) { 
			$datos[$i] = array(
				'IdAtencion'				=> $data[$i]['IdAtencion'],
				'HoraInicio'				=> $data[$i]['HoraInicio'],
				'HoraFin'					=> $data[$i]['HoraFin'],
				'NroHistoriaClinica'		=> $data[$i]['NroHistoriaClinica'],
				'NombrePaciente'			=> $data[$i]['NombreCompleto'],
				'IdPaciente'				=> $data[$i]['IdPaciente'] ,
				'NombreServicio'			=> $data[$i]['Nombre'] ,
				'FechaSolicitud'			=> $data[$i]['FechaSolicitud'] ,
				'Descripcion'				=> $data[$i]['Descripcion'] ,
				'variable' 					=> $arreglo  
				);
			}
			}
			
			
		echo json_encode($datos);
		exit();
	}

	
	//Conserje  06-08-2018
	// Accion para Generar lista de Atenciones de Emergencia 
	
	if ($_REQUEST["acc"] == "MostrarHC_sin_Retornar_Constancias_Medicas") {
		$FechaInicio =  sqlfecha_devolver(trim($_REQUEST["FechaInicio"]));
		$FechaFinal  =  sqlfecha_devolver(trim($_REQUEST["FechaFinal"]));
		$IdServicio  =  '333';
		$data = Archivo_Clinico_Mostrar_Lista_Historias_Segun_Servicio_sin_Salir_C($FechaInicio,$FechaFinal,$IdServicio);
			if (count($data) == 0) 
			{
			$arreglo = "M_0";
			$datos[0] = array(
				'variable' 					=> $arreglo  
				);
			}
			if (count($data) >= 1) 
			{
			$arreglo = "M_1";
			for ($i=0; $i < count($data); $i++) { 
			$datos[$i] = array(
				'HistoriaClinica'				=> $data[$i]['HistoriaClinica'],
				'NombrePaciente'				=> $data[$i]['NombrePaciente'],
				'Observacion_Salida'			=> $data[$i]['Observacion_Salida'],
				'F_cita'						=> $data[$i]['F_cita'],
				'Fecha_Salida'					=> $data[$i]['Fecha_Salida'],
				'Hora_Salida'					=> $data[$i]['Hora_Salida'],
				'variable' 						=> $arreglo  
				);
			}
			}
			
			
		echo json_encode($datos);
		exit();
	}
	
	
	//Conserje  14-08-2018
	// Opcion para Imprimir 
	if($_REQUEST["acc"] == "ImprimirReporteConstanciasMedicas") {
	$FechaInicio =  sqlfecha_devolver(trim($_REQUEST["FechaInicio"]));
	$FechaFinal  =  sqlfecha_devolver(trim($_REQUEST["FechaFinal"]));
	$IdServicio  =  '333';
	$data = Archivo_Clinico_Mostrar_Lista_Historias_Segun_Servicio_sin_Salir_C($FechaInicio,$FechaFinal,$IdServicio);
	$contenido  = "<html>";
	$contenido .= "<head>";
	$contenido .= "<style>";
	$contenido .= "*{";
	$contenido .= "font-family: 'CALIBRI';";
	$contenido .= "font-size: 12px;";
	$contenido .= "margin:0;";
    $contenido .= "padding: 0;";
    $contenido .= "}";
    $contenido .= "@media print{
    	font-family: 'CALIBRI';
		font-size: 12px;
		margin:0;
        padding: 0;
    }";
	$contenido .= "</style>";
	$contenido .= "<title>CONSOLIDADO ESTADO DE CUENTA PACIENTE</title>";
	$contenido .= "</head>";
	$contenido .= "<body>";
	$contenido .= "<table width='50%'>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>ID</strong></td>";
    $contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>HISTORIA CLINICA</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>NOMBRE DE PACIENTE</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>OBSERVACIONES</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>FECHA DE SALIDA</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>HORA DE SALIDA</strong></td>";
	$contenido .= "</tr>";
	for ($i=1; $i < count($data); $i++) { 
	$contenido .= "<tr>";
	$contenido .= "<td>".$i."</td>";	
	$contenido .= "<td align='center'>".$data[$i]['HistoriaClinica']."</td>";
	$contenido .= "<td>".$data[$i]['NombrePaciente']."</td>";
	$contenido .= "<td>".$data[$i]['Observacion_Salida']."</td>";
	$contenido .= "<td align='center'>".$data[$i]['Fecha_Salida']."</td>";
	$contenido .= "<td align='center'>".$data[$i]['Hora_Salida']."</td>";
	$contenido .= "</tr>";	
	}
	$contenido .= "</table>";
	$contenido .= "</body>";
	$contenido .= "</html>";
	echo json_encode($contenido);
	exit();

	}
	
	// Opcion para Imprimir 
	if($_REQUEST["acc"] == "ImprimirReporteConstanciasMedicas_2") {
	$FechaInicio =  gfecha(trim($_REQUEST["FechaInicio"]));
	$FechaFinal  =  gfecha(trim($_REQUEST["FechaFinal"]));
	$IdServicio  =  '333';
	$data = Archivo_Clinico_Mostrar_Lista_Historias_Segun_Servicio_sin_Salir_C($FechaInicio,$FechaFinal,$IdServicio);
	$contenido  = "<html>";
	$contenido .= "<head>";
	$contenido .= "<style>";
	$contenido .= "*{";
	$contenido .= "font-family: 'CALIBRI';";
	$contenido .= "font-size: 12px;";
	$contenido .= "margin:0;";
    $contenido .= "padding: 0;";
    $contenido .= "}";
    $contenido .= "@media print{
    	font-family: 'CALIBRI';
		font-size: 12px;
		margin:0;
        padding: 0;
    }";
	$contenido .= "</style>";
	$contenido .= "<title>CONSOLIDADO ESTADO DE CUENTA PACIENTE</title>";
	$contenido .= "</head>";
	$contenido .= "<body>";
	$contenido .= "<table width='50%'>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>ID</strong></td>";
    $contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>HISTORIA CLINICA</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>NOMBRE DE PACIENTE</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>OBSERVACIONES</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>FECHA DE SALIDA</strong></td>";
	$contenido .= "<td align='center' style='border-top:1px dotted black;border-bottom:1px dotted black;'><strong>HORA DE SALIDA</strong></td>";
	$contenido .= "</tr>";
	for ($i=1; $i < count($data); $i++) { 
	$contenido .= "<tr>";
	$contenido .= "<td>".$i."</td>";	
	$contenido .= "<td align='center'>".$data[$i]['HistoriaClinica']."</td>";
	$contenido .= "<td>".$data[$i]['NombrePaciente']."</td>";
	$contenido .= "<td>".$data[$i]['Observacion_Salida']."</td>";
	$contenido .= "<td align='center'>".$data[$i]['Fecha_Salida']."</td>";
	$contenido .= "<td align='center'>".$data[$i]['Hora_Salida']."</td>";
	$contenido .= "</tr>";	
	}
	$contenido .= "</table>";
	$contenido .= "</body>";
	$contenido .= "</html>";
	echo json_encode($contenido);
	exit();

	}
	

	
	//Conserje  25-04-2018
	// Accion para Generar las Salidas de un Conserje
	if($_REQUEST["acc"] == "Generar_Salida_Conserje_Arreglo")
	{ 
	$IdAtencion =  $_REQUEST["IdAtencion"];
	$IdEmpleado =  $_REQUEST["IdEmpleado"];
	$Retorno_General_HC=Generar_Salida_Conserje_C($IdAtencion,$IdEmpleado);
	}
	
	
	
	//Conserje  25-04-2018
	// Accion para Generar las Salidas de un Conserje
	if($_REQUEST["acc"] == "Generar_Retorno_Conserje_Arreglo")
	{ 
	$IdAtencion =  $_REQUEST["IdAtencion"];
	$IdEmpleado =  $_REQUEST["IdEmpleado"];
	$Retorno_General_HC=Generar_Retorno_Conserje_C($IdAtencion,$IdEmpleado);
	}
	
	if ($_REQUEST["acc"] == "MostrarTipoDeEstadoCivil") {
		$data = MostrarTipoEstadoCivil();

		for ($i=0; $i < count($data); $i++) { 
			$estadocivil[$i] = array(
				'id'	=> 	$data[$i]['IDESCIVIL'],
				'text'	=>	$data[$i]['ESTADOCIVIL']
				);
		}

		echo json_encode($estadocivil);
		exit();
	}

	
	//MOSTRAR: Item de Reporte General	Salida por Digito Terminal
	if($_GET["acc"] == "Reporte_Salida_Digito_Terminal")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
   	include('../../MVC_Vista/Archivo_Clinico/Reporte_Salida_Digito_Terminal.php');
	}
	
	
	//MOSTRAR: Item de Reporte General	Salida por Digito Terminal
	
	if($_GET["acc"] == "Reporte_Constancias_Medicas")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
   	include('../../MVC_Vista/Archivo_Clinico/Reporte_Constancias_Medicas.php');
	}
	
	//MOSTRAR: Item de Reporte General	Salida por Digito Terminal Detalle
	
	if($_GET["acc"] == "Salida_Digito_Terminal_Detalle")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
   	include('../../MVC_Vista/Archivo_Clinico/Reporte_Salida_Digito_Terminal_Detalle.php');
	}
	
	
	//MOSTRAR: Item de Reporte General	Salida por Fuente de Financiamiento
	
	if($_GET["acc"] == "Salida_Fuente_Financiamiento")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
   	include('../../MVC_Vista/Archivo_Clinico/Reporte_Salida_Fuente_Financiamiento.php');
	}
	
	
	//MOSTRAR: Item de Reporte General Historias Clinicas sin Retornar
	
	if($_GET["acc"] == "Reporte_HC_sin_Retornar")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
   	include('../../MVC_Vista/Archivo_Clinico/Reporte_Pendientes_Retorno.php');
	}
	

	//MOSTRAR: Item de Reporte General Historias Clinicas sin Retornar
	
	if($_GET["acc"] == "Salida_Consultorio")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
   	include('../../MVC_Vista/Archivo_Clinico/Reporte_Salida_Consultorio.php');
	}
	
	
	//MOSTRAR: Item de Reporte General Historias Clinicas sin Retornar
	
	if($_GET["acc"] == "Salida_Consultorio_Detalle")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
   	include('../../MVC_Vista/Archivo_Clinico/Reporte_Salida_Consultorio_Detalle.php');
	}
	

	// MOSTRAR: Permite Salida de historias CLinicas por Check
	
	if($_POST["acc"] == "Registrar_Salida_Check")
	{
		
	$IdAtencion = $_POST['IdAtencion'];
	$IdEmpleado = $_POST['Id_Empleado'];
	$Registrar_Salida_HC_Check=Registrar_Salida_HC_Check_C($IdAtencion,$IdEmpleado);

	}
	
	// MOSTRAR: Permite Retorna la Salida de historias CLinicas por Check
	
	if($_POST["acc"] == "Registrar_Retorno_de_Salida_Check")
	{
		
	$IdAtencion = $_POST['IdAtencion'];
	$Registrar_Salida_HC_Check=Registrar_Retorno_de_Salida_HC_Check_C($IdAtencion);

	}
	
	if($_POST["acc"] == "Registrar_Retorno_Check")// MOSTRAR: Formulario Nuevo Registro
	{
	$IdAtencion = $_POST['IdAtencion'];
	$IdEmpleado = $_POST['Id_Empleado'];
	$Registrar_Retorno_HC_Check=Registrar_Retorno_HC_Check_C($IdAtencion,$IdEmpleado);
	}
	
	
	
	if($_POST["acc"] == "Verificar_Salida_Check")
	{
	$IdAtencion = $_POST['IdAtencion'];
	$Verificar_Salida_HC_Check=Verificar_Salida_Check_C($IdAtencion);	
	}
		
	if($_POST["acc"] == "Cargar_Paciente")// MOSTRAR: Formulario Nuevo Registro
	{
	if($_REQUEST["NroHistoriaClinica"]!=NULL){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='%';}
	$Mostrar_Pacientes=Mostrar_Pacientes_C($NroHistoriaClinica);
	}
	
	
	// Accion para Cargar el Formulario de un Conserje
	
	if($_GET["acc"] == "Conserje")
	{	 
	include('../../MVC_Vista/Archivo_Clinico/Conserje.php');
	}
	

	// Accion para Cargar los datos de una HC cargada por el Conserje		
	
	if($_POST["acc"] == "Cargar_Ingreso_Conserje")
	{
	if($_REQUEST["NroHistoriaClinica"]!=NULL){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='%';}
	if($_REQUEST["Fecha"]!=NULL){$Fecha=$_REQUEST["Fecha"];}else{$Fecha='%';}
	$Mostrar_Pacientes=Mostrar_Salida_Conserje_C($NroHistoriaClinica,gfecha($Fecha));
	}
	
	
	// Accion para Generar las Salidas de un Conserje
	
	if($_POST["acc"] == "Generar_Salida_Conserje")
	{ 
		if($_REQUEST["valor"]!=NULL){$valor=$_REQUEST["valor"];}else{$valor='%';}
		if($_REQUEST["IdEmpleado"]!=NULL){$IdEmpleado=$_REQUEST["IdEmpleado"];}else{$IdEmpleado='%';}
		$array = explode(",", $valor);
		foreach ($array as &$valor) 
		{
			$Retorno_General_HC=Generar_Salida_Conserje_C($valor,$IdEmpleado);
		}
	}

		
		
		
	// Accion para Mostrar la Ultima HC Generada
	
	if($_POST["acc"] == "Mostrar_Ultima_HC")
	{ 

		$Mostrar_Ultima_HC=Mostrar_Ultima_HC_C();
		  if($Mostrar_Ultima_HC != NULL)
		  { 
			  foreach($Mostrar_Ultima_HC as $item)
			  {
				$salida=$item["UltimoHC"];
			  }
			  echo $salida;  
		  }
			
	}
	
	// Accion para Mostrar la Cantidad de Historias Clinicas Pendientes
	
	if($_POST["acc"] == "Mostrar_Cantidad_HC")
	{ 
		if($_REQUEST["Digito_Inicial"]!=NULL){$Digito_Inicial=$_REQUEST["Digito_Inicial"];}else{$Digito_Inicial='%';}
		if($_REQUEST["Digito_Final"]!=NULL){$Digito_Final=$_REQUEST["Digito_Final"];}else{$Digito_Final='%';}
		if($_REQUEST["Turno"]!=NULL){$Turno=$_REQUEST["Turno"];}else{$Turno='%';}
		$Mostrar_Cantidad_HC=Archivo_Clinico_Mostrar_Cantidad_Historias_Pendientes_C($Digito_Inicial,$Digito_Final,$Turno);
		  if($Mostrar_Cantidad_HC != NULL)
		  { 
			  foreach($Mostrar_Cantidad_HC as $item)
			  {
				$salida=$item["Cantidad"];
			  }
			  echo $salida;  
		  }
			
	}
	
	
	// Accion para Agregar o Modificar la Observacion de una Salida de Historia Clinica
	
	if($_POST["acc"] == "Agregar_Observacion_Salida")
	{    
		$Observacion = $_POST['Observacion'];
		$IdAtencion = $_POST['IdAtencion'];
		$Agregar_Observacion_Salida=Agregar_Observacion_Salida_C($Observacion,$IdAtencion);	
	}
	
	
	// Accion para Agregar o Modificar la Observacion de un Retorno de Historia Clinica
	
	if($_POST["acc"] == "Agregar_Observacion_Retorno")
	{    
		$Observacion = $_POST['Observacion'];
		$IdAtencion = $_POST['IdAtencion'];
		$Agregar_Observacion_Retorno=Agregar_Observacion_Retorno_C($Observacion,$IdAtencion);	
	}
	
	
	// Accion para Modificar Digito Terminales por IdEmpleado
	
	if($_POST["acc"] == "Modificar_DigitoTerminal")
	{    
		$Digito_Inicial = $_POST['Digito_Inicial'];
		$Digito_Final = $_POST['Digito_Final'];
		$Id_Empleado = $_POST['Id_Empleado'];
		$Modificar_DigitoTerminal=Modificar_DigitoTerminal_C($Digito_Inicial,$Digito_Final,$Id_Empleado);	
	}
						
		
	if($_GET["acc"] == "Archivero")// MOSTRAR: Formulario Nuevo Registro
	{	 
	include('../../MVC_Vista/Archivo_Clinico/Archivero.php');
	}
	
		
	
	if($_POST["acc"] == "Combo_Dependencia")// MOSTRAR: Formulario Nuevo Registro
	{		
	$IdTipoServicio=$_POST["IdTipoServicio"];		
	$ServiciosSeleccionarPorTipo=ServiciosSeleccionarPorTipo_C($IdTipoServicio);
		  if($ServiciosSeleccionarPorTipo != NULL)
		  { 
			  foreach($ServiciosSeleccionarPorTipo as $item)
			  {
				$salida.= "<option  value='".$item["IdServicio"]."'>".$item["Servicio"]."</option>";
			  }
			  echo $salida;  
		  }     
	}
	
	
	if($_GET["acc"] == "Salida_Dependencias_HC")// MOSTRAR: Formulario Nuevo Registro
	{
		
	$TiposServicio=TiposServicioSeleccionar_C();
	$Motivos_Movimiento_HC=Motivos_Movimiento_Historia_Todos_C();
	include('../../MVC_Vista/Archivo_Clinico/Salida_Dependencias.php');
	
	}
	
	
	
	
	if($_GET["acc"] == "Reporte_Dependencia")// MOSTRAR: Formulario Nuevo Registro
	{
		
	$ServiciosSeleccionarPorTipo=ServiciosSeleccionarPorTipo_C('1');
	include('../../MVC_Vista/Archivo_Clinico/Reporte_Dependencia.php');
	}
	
	
	
		
	if($_GET["acc"] == "Reportes_Generales")// MOSTRAR: Formulario de Reportes Generales
	{
	if($_REQUEST["IdEmpleado"]!=NULL){$IdEmpleado=$_REQUEST["IdEmpleado"];} 	
	$ServiciosSeleccionarPorTipo=ServiciosSeleccionarPorTipo_C('1');
	include('../../MVC_Vista/Archivo_Clinico/Reporte_Generales.php');
	}
	
	
	
	
	
	
	
    if ($_POST["acc"] == "Insertar_Dependencia")
	{	
	$IdPaciente = $_POST['IdPaciente'];
	$IdServicio = $_POST['IdServicio'];
	$FechaCita = $_POST['FechaCita'];
	$comentario = $_POST['Comentario'];
	$IdEmpleado = $_POST['IdEmpleado'];
	$array_paciente = explode(",", $IdPaciente);
	$array_servicio = explode(",", $IdServicio);
		for($i = 0; $i < count($array_paciente); $i++) 
		{   
	    $Seguimiento_Historia_Clinica_Agregar=Seguimiento_Historia_Clinica_Agregar_C(gfecha($FechaCita),$array_servicio[$i],$array_paciente[$i],$comentario[$i],$IdEmpleado);
		}	
	}
	
	
	
	
	if($_GET["acc"] == "Retorno_General_HC")// MOSTRAR: Retorno General
	{ 
		
	if($_REQUEST["IdEmpleado"]!=NULL){$IdEmpleado=$_REQUEST["IdEmpleado"];} 
	$ListarDigitoTerminales=Archivo_Clinico_Digito_Terminal_IdEmpleado_C($IdEmpleado);
	include('../../MVC_Vista/Archivo_Clinico/Retorno_General_HC.php');	
	}
	
	
	
	if($_POST["acc"] == "Generar_Retorno_General")// MOSTRAR: Retorno General
	{ 
		if($_REQUEST["valor"]!=NULL){$valor=$_REQUEST["valor"];}else{$valor='%';}
		$array = explode(",", $valor);
		foreach ($array as &$valor) 
		{
			$Retorno_General_HC=Retorno_General_HC_C($valor);
		}
	}
	

	// MOSTRAR: Formulario para Generar Reporte Digito Terminal
	
	if($_GET["acc"] == "Reporte_HC")
	{
	if($_REQUEST["IdEmpleado"]!=NULL){$IdEmpleado=$_REQUEST["IdEmpleado"];} 
	$ListarDigitoTerminales=Archivo_Clinico_Digito_Terminal_IdEmpleado_C($IdEmpleado);
	include('../../MVC_Vista/Archivo_Clinico/Reporte_Digito_Terminal.php');
	}
	
	
	// MOSTRAR: Formulario para Generar Reporte de Historia Clinica Nueva
	
	if($_GET["acc"] == "HistoriaClinica")
	{
	include('../../MVC_Vista/Archivo_Clinico/Reporte_HistoriaClinica_Nueva.php');
	}
	
	// MOSTRAR: Formulario para Generar Reporte Consolidado
	
	if($_GET["acc"] == "Reporte_Consolidado")
	{
	if($_REQUEST["IdEmpleado"]!=NULL){$IdEmpleado=$_REQUEST["IdEmpleado"];} 
	$ListarDigitoTerminales=Archivo_Clinico_Digito_Terminal_IdEmpleado_C($IdEmpleado);
	include('../../MVC_Vista/Archivo_Clinico/Consolidado.php');
	}
	
	
	
	// MOSTRAR: Formulario para Generar Salida de las Historias Clinicas
		
	if($_GET["acc"] == "Salida_HC")
	{
	if($_REQUEST["IdEmpleado"]!=NULL){$IdEmpleado=$_REQUEST["IdEmpleado"];} 
	$ListarDigitoTerminales=Archivo_Clinico_Digito_Terminal_IdEmpleado_C($IdEmpleado);
	$EsSupervisor=Mostrar_si_es_Supervisor_Archivo_Clinico_C($IdEmpleado);
		//Validar si es Supervisor
		if($EsSupervisor != NULL)
		{
		$permiso='';	
		}
		else
		{
		$permiso='display:none;';	
		}
		//Listar Digito de Terminal
		if($ListarDigitoTerminales != NULL)
		{
				include('../../MVC_Vista/Archivo_Clinico/Salida_HC.php');				
		}
		else
		{		
				$mensaje = "Archivero_sin_Digitos_Terminales";
				include("../../MVC_Vista/Archivo_Clinico/Emergentes_Mensajes.php");
		}
	}
	
	
	
	// MOSTRAR: Lista de las Historias Clinicas para Salida por Rango de Fecha , Digito Terminal y Turno
		
    if($_POST["acc"] == "Listar_Salida_HC")
	{
		
	if($_REQUEST["Digito_Inicial"]!=NULL){$Digito_Inicial=$_REQUEST["Digito_Inicial"];}else{$Digito_Inicial='%';}
	if($_REQUEST["Digito_Final"]!=NULL){$Digito_Final=$_REQUEST["Digito_Final"];}else{$Digito_Final='%';}
	if($_REQUEST["Fecha_Inicio"]!=NULL){$Fecha_Inicio=$_REQUEST["Fecha_Inicio"];}else{$Fecha_Inicio='%';}
	if($_REQUEST["Fecha_Final"]!=NULL){$Fecha_Final=$_REQUEST["Fecha_Final"];}else{$Fecha_Final='%';}
	if($_REQUEST["Turno"]!=NULL){$Turno=$_REQUEST["Turno"];}else{$Turno='%';}
	if($_REQUEST["Tipo_estado"]!=NULL){$Tipo_estado=$_REQUEST["Tipo_estado"];}else{$Tipo_estado='%';}
	$Listar_Salida_HC=Listar_Salida_HC_C($Digito_Inicial,$Digito_Final,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno,$Tipo_estado);
    include("../../MVC_Vista/Archivo_Clinico/Lista_Salida_HC.php");
	}
	
	
	
	// MOSTRAR: Lista de las Historias Clinicas para Salida por Rango de Fecha, Digito Terminal y Turno como Carga de Salida HC
	
	
	
	if($_POST["acc"] == "Cargar_Salida_HC")
	{	
	$Fecha_Movimiento=date('Y-m-d H:i:s');	
	if($_REQUEST["Digito_Inicial"]!=NULL){$Digito_Inicial=$_REQUEST["Digito_Inicial"];}else{$Digito_Inicial='%';}
	if($_REQUEST["Digito_Final"]!=NULL){$Digito_Final=$_REQUEST["Digito_Final"];}else{$Digito_Final='%';}
	if($_REQUEST["Fecha_Inicio"]!=NULL){$Fecha_Inicio=$_REQUEST["Fecha_Inicio"];}else{$Fecha_Inicio='%';}
	if($_REQUEST["Fecha_Final"]!=NULL){$Fecha_Final=$_REQUEST["Fecha_Final"];}else{$Fecha_Final='%';}
	if($_REQUEST["Turno"]!=NULL){$Turno=$_REQUEST["Turno"];}else{$Turno='';}
	if($_REQUEST["Tipo_estado"]!=NULL){$Tipo_estado=$_REQUEST["Tipo_estado"];}else{$Tipo_estado='';}
	if($_REQUEST["Empleado"]!=NULL){$Empleado=$_REQUEST["Empleado"];}else{$Empleado='';}
	if($_REQUEST["NroHistoriaClinica"]!=NULL)
	{
	$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
	if (is_numeric($NroHistoriaClinica)){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='';}		
	}
	else
	{$NroHistoriaClinica='';
	}
	
	$MostrarAtenciones_Dia_HistoriaClinica=Mostrar_Atenciones_DiaxHistoriaClinica_C($NroHistoriaClinica,gfecha($Fecha_Inicio));
		if($MostrarAtenciones_Dia_HistoriaClinica != NULL)
		{ 
			foreach($MostrarAtenciones_Dia_HistoriaClinica as $item)
			{
				$idFuenteFinanciamiento=$item["idFuenteFinanciamiento"];
				$IdEstadoCita=$item["IdEstadoCita"];					
				$Generar_salida_HC=Generar_salida_HC_C($NroHistoriaClinica,$Digito_Inicial,$Digito_Final,$Fecha_Movimiento,$Empleado,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno);	

			}	
		}
	$Listar_Salida_HC=Listar_Salida_HC_C($Digito_Inicial,$Digito_Final,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno,$Tipo_estado);
    include("../../MVC_Vista/Archivo_Clinico/Lista_Salida_HC.php");
	}
	
	
	
	
	
	
	
	
	
	// MOSTRAR: Lista de las Historias Clinicas para Salida por Rango de Fecha, Digito Terminal y Turno como Carga de Salida HC
	
	/*
	if($_POST["acc"] == "Cargar_Salida_HC")
	{	
	$Fecha_Movimiento=date('Y-m-d H:i:s');	
	if($_REQUEST["Digito_Inicial"]!=NULL){$Digito_Inicial=$_REQUEST["Digito_Inicial"];}else{$Digito_Inicial='%';}
	if($_REQUEST["Digito_Final"]!=NULL){$Digito_Final=$_REQUEST["Digito_Final"];}else{$Digito_Final='%';}
	if($_REQUEST["Fecha_Inicio"]!=NULL){$Fecha_Inicio=$_REQUEST["Fecha_Inicio"];}else{$Fecha_Inicio='%';}
	if($_REQUEST["Fecha_Final"]!=NULL){$Fecha_Final=$_REQUEST["Fecha_Final"];}else{$Fecha_Final='%';}
	if($_REQUEST["Turno"]!=NULL){$Turno=$_REQUEST["Turno"];}else{$Turno='';}
	if($_REQUEST["Tipo_estado"]!=NULL){$Tipo_estado=$_REQUEST["Tipo_estado"];}else{$Tipo_estado='';}
	if($_REQUEST["Empleado"]!=NULL){$Empleado=$_REQUEST["Empleado"];}else{$Empleado='';}
	if($_REQUEST["NroHistoriaClinica"]!=NULL)
	{
	$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
	if (is_numeric($NroHistoriaClinica)){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='';}		
	}
	else
	{$NroHistoriaClinica='';
	}
	
	$MostrarAtenciones_Dia_HistoriaClinica=Mostrar_Atenciones_DiaxHistoriaClinica_C($NroHistoriaClinica,gfecha($Fecha_Inicio));
		if($MostrarAtenciones_Dia_HistoriaClinica != NULL)
		{ 
			foreach($MostrarAtenciones_Dia_HistoriaClinica as $item)
			{
				$idFuenteFinanciamiento=$item["idFuenteFinanciamiento"];
				$IdEstadoCita=$item["IdEstadoCita"];
				$Clave=$item["Clave"];
				
					if($IdEstadoCita=='1' and $idFuenteFinanciamiento=='1' and $Clave!='Pro.')
					{
					$mensaje = "Historia_Clinica_Sin_Pagar";
					include("../../MVC_Vista/Archivo_Clinico/Emergentes_Mensajes.php");	
					}
					else
					{
					$Generar_salida_HC=Generar_salida_HC_C($NroHistoriaClinica,$Digito_Inicial,$Digito_Final,$Fecha_Movimiento,$Empleado,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno);	
					}
			}	
		}
	$Listar_Salida_HC=Listar_Salida_HC_C($Digito_Inicial,$Digito_Final,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno,$Tipo_estado);
    include("../../MVC_Vista/Archivo_Clinico/Lista_Salida_HC.php");
	}
	
	*/
	
	
	// MOSTRAR: Formulario para Generar Retorno de las Historias Clinicas
	
	if($_GET["acc"] == "Retorno_HC")
	{
	if($_REQUEST["IdEmpleado"]!=NULL){$IdEmpleado=$_REQUEST["IdEmpleado"];} 
	$ListarDigitoTerminales=Archivo_Clinico_Digito_Terminal_IdEmpleado_C($IdEmpleado);

		if($ListarDigitoTerminales != NULL)
		{
				include('../../MVC_Vista/Archivo_Clinico/Retorno_HC.php');				
		}
		else
		{		
				$mensaje = "Archivero_sin_Digitos_Terminales";
				include("../../MVC_Vista/Archivo_Clinico/Emergentes_Mensajes.php");
		}
	}
	
		
		
	// MOSTRAR: Lista de las Historias Clinicas para Retorno por Rango de Fecha , Digito Terminal y Turno
		
    if($_POST["acc"] == "Listar_Retorno_HC")
	{
		
	if($_REQUEST["Digito_Inicial"]!=NULL){$Digito_Inicial=$_REQUEST["Digito_Inicial"];}else{$Digito_Inicial='%';}
	if($_REQUEST["Digito_Final"]!=NULL){$Digito_Final=$_REQUEST["Digito_Final"];}else{$Digito_Final='%';}
	if($_REQUEST["Fecha_Inicio"]!=NULL){$Fecha_Inicio=$_REQUEST["Fecha_Inicio"];}else{$Fecha_Inicio='%';}
	if($_REQUEST["Fecha_Final"]!=NULL){$Fecha_Final=$_REQUEST["Fecha_Final"];}else{$Fecha_Final='%';}
	if($_REQUEST["Turno"]!=NULL){$Turno=$_REQUEST["Turno"];}else{$Turno='%';}
	if($_REQUEST["Tipo_estado"]!=NULL){$Tipo_estado=$_REQUEST["Tipo_estado"];}else{$Tipo_estado='%';}
	$Listar_Retorno_HC=Listar_Retorno_HC_C($Digito_Inicial,$Digito_Final,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno,$Tipo_estado);
    include("../../MVC_Vista/Archivo_Clinico/Lista_Retorno_HC.php");
	}
	

	
	
		
	// MOSTRAR: Lista de las Historias Clinicas para Salida por Rango de Fecha, Digito Terminal y Turno como Carga de Retorno HC
	
	if($_POST["acc"] == "Cargar_Retorno_HC")
	{	
	$Fecha_Movimiento=date('Y-m-d H:i:s');	
	if($_REQUEST["Digito_Inicial"]!=NULL){$Digito_Inicial=$_REQUEST["Digito_Inicial"];}else{$Digito_Inicial='%';}
	if($_REQUEST["Digito_Final"]!=NULL){$Digito_Final=$_REQUEST["Digito_Final"];}else{$Digito_Final='%';}
	if($_REQUEST["Fecha_Inicio"]!=NULL){$Fecha_Inicio=$_REQUEST["Fecha_Inicio"];}else{$Fecha_Inicio='%';}
	if($_REQUEST["Fecha_Final"]!=NULL){$Fecha_Final=$_REQUEST["Fecha_Final"];}else{$Fecha_Final='%';}
	if($_REQUEST["Turno"]!=NULL){$Turno=$_REQUEST["Turno"];}else{$Turno='';}
	if($_REQUEST["Tipo_estado"]!=NULL){$Tipo_estado=$_REQUEST["Tipo_estado"];}else{$Tipo_estado='';}
	if($_REQUEST["Empleado"]!=NULL){$Empleado=$_REQUEST["Empleado"];}else{$Empleado='';}
	if($_REQUEST["NroHistoriaClinica"]!=NULL)
	{
	$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
	if (is_numeric($NroHistoriaClinica)){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='';}		
	}
	else
	{$NroHistoriaClinica='';
	}
	
	$Generar_retorno_HC=Generar_retorno_HC_C($NroHistoriaClinica,$Digito_Inicial,$Digito_Final,$Fecha_Movimiento,$Empleado,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno);					
	$Listar_Retorno_HC=Listar_Retorno_HC_C($Digito_Inicial,$Digito_Final,gfecha($Fecha_Inicio),gfecha($Fecha_Final),$Turno,$Tipo_estado);
    include("../../MVC_Vista/Archivo_Clinico/Lista_Retorno_HC.php");
	}
	
	
	
	// MOSTRAR: Lista de los Archiveros
	
	if($_POST["acc"] == "ListarArchiveros")
	{
	 
	if($_REQUEST["NroDocumento"]!=NULL){$NroDocumento=$_REQUEST["NroDocumento"];}else{$NroDocumento='%';}
	if($_REQUEST["Nombres"]!=NULL){$Nombres=$_REQUEST["Nombres"];}else{$Nombres='%';}
	if($_REQUEST["ApellidoPaterno"]!=NULL){$ApellidoPaterno=$_REQUEST["ApellidoPaterno"];}else{$ApellidoPaterno='%';}
	if($_REQUEST["ApellidoMaterno"]!=NULL){$ApellidoMaterno=$_REQUEST["ApellidoMaterno"];}else{$ApellidoMaterno='%';}

	
	$ListarArchiveros=Listar_Archivero_C($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno);
    include("../../MVC_Vista/Archivo_Clinico/ListaArchivero.php");
 
	}
	

	//MOSTRAR: Formulario de Movimiento de una Historia  Clinica
	
	if ($_GET["acc"] == "MovimientoHistoria")
	{
	include('../../MVC_Vista/Archivo_Clinico/MovimientoHistoria.php');
	}
	
	//MOSTRAR: Formulario de Movimiento de una Historia  Clinica
	
	if ($_GET["acc"] == "Detalle_Historia")
	{
	include('../../MVC_Vista/Archivo_Clinico/Detalle_historia.php');
	}
	
	
	
	//MOSTRAR: Los Movimientos de una Historia Clinica dentro de los Servicios
	
	if($_POST["acc"] == "Movimiento_Detalle_Historia_Clinica")
	{
	 
	if($_REQUEST["NroHistoriaClinica"]!=NULL){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='%';}

	$Movimiento_Historia_Clinica=Movimiento_Detalle_Historia_Clinica_C($NroHistoriaClinica);
    include("../../MVC_Vista/Archivo_Clinico/Lista_Movimiento_Detalle_Historia_Clinica.php");
 
	}
	
	//MOSTRAR: Los Datos del Paciente que se observara sus Movimientos
	
	if($_POST["acc"] == "Movimiento_Historia_Clinica")
	{
	 
	if($_REQUEST["NroHistoriaClinica"]!=NULL){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='%';}
	$PacientesDatos=PacientesSeleccionarPorHistoriaClinica_C($NroHistoriaClinica);
    include("../../MVC_Vista/Archivo_Clinico/Paciente_Datos.php");
 
	}
	
	//MOSTRAR: Los Movimientos de una Historia Clinica dentro de los Servicios
	
	if($_POST["acc"] == "Movimiento_Historia_Clinica")
	{
	 
	if($_REQUEST["NroHistoriaClinica"]!=NULL){$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];}else{$NroHistoriaClinica='%';}

	$Movimiento_Historia_Clinica=Movimiento_Historia_Clinica_C($NroHistoriaClinica);
    include("../../MVC_Vista/Archivo_Clinico/Lista_Movimiento_Historia_Clinica.php");
 
	}
	
	
	if ($_POST["acc"] == "Mostrar_Empleado")
	{
	$NroDocumento=$_REQUEST['nro_documento'];
    $Encontrar_Empleado=Mostrar_Empleado_C($NroDocumento);
	}

	
	
	//MOSTRAR: Accion para crear un Archivero
    if ($_POST["acc"] == "Agregar_Archivero")
	{
	
	if(!is_numeric($_REQUEST['Digito_Inicial']) or !is_numeric($_REQUEST['Digito_Final']) ) 
	{
				$mensaje = "Digito_Terminal_No_Valido";
				include("../../MVC_Vista/Archivo_Clinico/Emergentes_Mensajes.php");		
		
	}		
		
    if(is_numeric($_REQUEST['Digito_Inicial']) and is_numeric($_REQUEST['Digito_Final']) ) 
	{
	   	$Digito_Inicial=$_REQUEST['Digito_Inicial'];
		$Digito_Final=$_REQUEST['Digito_Final'];
		$Id_Empleado=$_REQUEST['Id_Empleado'];
		$Mostrar_Archivero_Id=Mostrar_Archivero_Id_C($Id_Empleado);
		if($Mostrar_Archivero_Id != NULL)
		{
				$mensaje = "Archivero_Existente";
				include("../../MVC_Vista/Archivo_Clinico/Emergentes_Mensajes.php");	
		}
		else
		{
		$Agregar_Archivero=Agregar_Archivero_C($Digito_Inicial,$Digito_Final,$Id_Empleado);	
				$mensaje = "Archivero_Agregado";
				include("../../MVC_Vista/Archivo_Clinico/Emergentes_Mensajes.php");			
		}	
	}		
	
	}
	

	//MOSTRAR: Accion para Eliminar un Archivero
	
	if ($_POST["acc"] == "Eliminar_Archivero")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
    $Eliminar_Archivero=Eliminar_Archivero_Id_C($IdEmpleado);
	}

	
	
	
	//MOSTRAR: Accion para Modificar el Digito Terminal Inicial del Archivero
	
	if ($_POST["acc"] == "Modificar_Digito_Inicial")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
	$Digito_Inicial=$_REQUEST['Digito_Inicial'];
    $Modificar_Digito_Inicial=Modificar_Digito_Inicial_C($IdEmpleado,$Digito_Inicial);
	}

	
	//MOSTRAR: Accion para Modificar el Digito Terminal Final del Archivero
	
	if ($_POST["acc"] == "Modificar_Digito_Final")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
	$Digito_Final=$_REQUEST['Digito_Final'];
    $Modificar_Digito_Final=Modificar_Digito_Final_C($IdEmpleado,$Digito_Final);
	}
	
	
	
	//MOSTRAR: Formulario para Generar Reporte de Historia Clinica por Digito Terminal
	
    if ($_POST["acc"] == "Generar_Reporte_HC")
	{
	$Fecha_Citas=$_REQUEST['Fecha_Citas'];
	$Tipo_cita=$_REQUEST['Tipo_cita'];
	$Turno=$_REQUEST['Turno'];
    $Tipo_estado=$_REQUEST['Tipo_estado'];
	include("../../MVC_Vista/Archivo_Clinico/Reporte_HC_Salida.php");
	}
	
	
	//MOSTRAR: Formulario para Generar Reporte Consolidado de Historia Clinica
	
	if ($_POST["acc"] == "Generar_Reporte_Consolidado_HC")
	{
	$Fecha_Citas=$_REQUEST['Fecha_Citas'];
	$Tipo_cita=$_REQUEST['Tipo_cita'];
	$Turno=$_REQUEST['Turno'];
	include("../../MVC_Vista/Archivo_Clinico/Reporte_HC_Consolidado.php");
	}

	
	 
	/***** FUNCIONES  DE PACIENTE******/
	

	function PacientesSeleccionarPorHistoriaClinica_C($NroHistoriaClinica){
	return PacientesSeleccionarPorHistoriaClinica_M($NroHistoriaClinica);
	}	
 
	/***** FUNCIONES  DE ARCHIVO CLINICO******/
	
	function RetornaFechaServidorSQL_C(){	
	return RetornaFechaServidorSQL_M(); 
	}
	
	function Registrar_Salida_HC_Check_C($IdAtencion,$IdEmpleado){
	return Registrar_Salida_HC_Check_M($IdAtencion,$IdEmpleado);
	}
	

	function Registrar_Retorno_HC_Check_C($IdAtencion,$IdEmpleado){
	return Registrar_Retorno_HC_Check_M($IdAtencion,$IdEmpleado);
	}
			
		
		
	function Listar_Archivero_C($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno){
	return Listar_Archivero_M($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno);
	}
	
	function Mostrar_Empleado_C($NroDocumento){
	return Mostrar_Empleado_M($NroDocumento);
	}
	

 	function Movimiento_Historia_Clinica_C($NroHistoriaClinica){
	return Movimiento_Historia_Clinica_M($NroHistoriaClinica);
	}
	
	function Movimiento_Detalle_Historia_Clinica_C($NroHistoriaClinica){
	return Movimiento_Detalle_Historia_Clinica_M($NroHistoriaClinica);
	}
	 
  	function Archivo_Clinico_Digito_Terminal_IdEmpleado_C($Id_Empleado){
	return Archivo_Clinico_Digito_Terminal_IdEmpleado_M($Id_Empleado);
	}
	
	function Listar_Salida_HC_C($Digito_Inicial,$Digito_Final,$Fecha_Inicio,$Fecha_Final,$Turno,$Tipo_estado){
	return Listar_Salida_HC_M($Digito_Inicial,$Digito_Final,$Fecha_Inicio,$Fecha_Final,$Turno,$Tipo_estado);
	}
	
	
	function Listar_Retorno_HC_C($Digito_Inicial,$Digito_Final,$Fecha_Inicio,$Fecha_Final,$Turno,$Tipo_estado){
	return Listar_Retorno_HC_M($Digito_Inicial,$Digito_Final,$Fecha_Inicio,$Fecha_Final,$Turno,$Tipo_estado);
	}
		
		
	function Mostrar_Pacientes_C($NroHistoriaClinica){
	return Mostrar_Pacientes_M($NroHistoriaClinica);
	}

	function Retorno_General_HC_C($IdPaciente){
	return Retorno_General_HC_M($IdPaciente);
	}
	
	function TiposServicioSeleccionar_C(){
	return TiposServicioSeleccionar_M();
	}
	
	function Agregar_Observacion_Salida_C($Observacion,$IdAtencion){
	return Agregar_Observacion_Salida_M($Observacion,$IdAtencion);
	}

		
	function Agregar_Observacion_Retorno_C($Observacion,$IdAtencion){
	return Agregar_Observacion_Retorno_M($Observacion,$IdAtencion);
	}

	function Generar_salida_HC_C($NroHistoriaClinica,$Digito_Inicial,$Digito_Final,$Fecha_Movimiento,$Empleado,$FechaInicio,$FechaFinal,$Turno){
	return  Generar_salida_HC_M($NroHistoriaClinica,$Digito_Inicial,$Digito_Final,$Fecha_Movimiento,$Empleado,$FechaInicio,$FechaFinal,$Turno);
	}
	
	
	function Generar_retorno_HC_C($NroHistoriaClinica,$Digito_Inicial,$Digito_Final,$Fecha_Movimiento,$Empleado,$FechaInicio,$FechaFinal,$Turno){
	return  Generar_retorno_HC_M($NroHistoriaClinica,$Digito_Inicial,$Digito_Final,$Fecha_Movimiento,$Empleado,$FechaInicio,$FechaFinal,$Turno);
	}

	function Mostrar_Atenciones_DiaxHistoriaClinica_C($NroHistoriaClinica,$Fecha){
	return  Mostrar_Atenciones_DiaxHistoriaClinica_M($NroHistoriaClinica,$Fecha);
	}
	
	function Mostrar_Atenciones_xIdAtencion_C($IdAtencion){
	return  Mostrar_Atenciones_xIdAtencion_M($IdAtencion);
	}
	
	
	function ServiciosSeleccionarPorTipo_C($IdTipoServicio){
	return ServiciosSeleccionarPorTipo_M($IdTipoServicio);
	}
	
	function Motivos_Movimiento_Historia_Todos_C(){
	return Motivos_Movimiento_Historia_Todos_M();
	}	
	
	function Seguimiento_Historia_Clinica_Agregar_C($FechaCita,$Servicio,$Paciente,$comentario,$IdEmpleado){
	return Seguimiento_Historia_Clinica_Agregar_M($FechaCita,$Servicio,$Paciente,$comentario,$IdEmpleado);
	}
	
	
		
	// Funciones de Archivero 
	
    function Agregar_Archivero_C($Digito_Inicial,$Digito_Final,$Id_Empleado){
	return Agregar_Archivero_M($Digito_Inicial,$Digito_Final,$Id_Empleado);
	}
 
    function Mostrar_Archivero_Id_C($IdEmpleado){
	return Mostrar_Archivero_Id_M($IdEmpleado);
	}
		
    function Eliminar_Archivero_Id_C($IdEmpleado){
	return Eliminar_Archivero_Id_M($IdEmpleado);
	}
	
	function Modificar_Digito_Inicial_C($IdEmpleado,$Digito_Inicial){
	return Modificar_Digito_Inicial_M($IdEmpleado,$Digito_Inicial);
	}
	
	function Modificar_Digito_Final_C($IdEmpleado,$Digito_Final){
	return Modificar_Digito_Final_M($IdEmpleado,$Digito_Final);
	}
	
	function Modificar_DigitoTerminal_C($Digito_Inicial,$Digito_Final,$Id_Empleado){
	return Modificar_DigitoTerminal_M($Digito_Inicial,$Digito_Final,$Id_Empleado);
	}
	
	// Funciones de Conserje 
				
	function Mostrar_Salida_Conserje_C($NroHistoriaClinica,$Fecha){
	return Mostrar_Salida_Conserje_M($NroHistoriaClinica,$Fecha);
	}

	function Generar_Salida_Conserje_C($valor,$IdEmpleado){
	return Generar_Salida_Conserje_M($valor,$IdEmpleado);
	}
	
	
	function Generar_Retorno_Conserje_C($valor,$IdEmpleado){
	return Generar_Retorno_Conserje_M($valor,$IdEmpleado);
	}
	

	// Funciones de Conserje 
				
	function Mostrar_Ultima_HC_C(){
	return Mostrar_Ultima_HC_M();
	}

	// Funcion que verifica si la cita ah sido pagada por IdAtencion
	
	function Verificar_Salida_Check_C($IdAtencion){
	return Verificar_Estado_Cita_xIdAtencion_M($IdAtencion);
	}
	
	
	// Funcion que verifica si la cita ah sido pagada  por Historia Clinica
	function Verificar_Salida_HistoriaClinica_C($NroHistoriaClinica,$Fecha_Inicio,$Fecha_Final){
	return Verificar_Estado_Cita_xNroHistoriaClinica_M($NroHistoriaClinica,$Fecha_Inicio,$Fecha_Final);
	}
	
	// Funciones de Cantidad de Historias Pendientes 09/12/2016
				
	function Archivo_Clinico_Mostrar_Cantidad_Historias_Pendientes_C($Digito_Inicial,$Digito_Final,$Turno){
	return Archivo_Clinico_Mostrar_Cantidad_Historias_Pendientes_M($Digito_Inicial,$Digito_Final,$Turno);
	}

	
	
	// Funciones de Conserje   18/04/2018
	
	function Conserje_Listar_Citas_Paciente_Salida_C($NroHistoriaClinica,$FechaCita){
	return Conserje_Listar_Citas_Paciente_Salida_M($NroHistoriaClinica,$FechaCita);
	}
	
		
	// Funciones de Conserje   18/04/2018
	
	function Conserje_Listar_Citas_Paciente_Retorno_C($NroHistoriaClinica,$FechaCita){
	return Conserje_Listar_Citas_Paciente_Retorno_M($NroHistoriaClinica,$FechaCita);
	}
	
	
	
	// Funciones de Lista de Atenciones de Emergencia  06/08/2018
	function Conserje_Listar_Atenciones_Emergencia_Paciente_Retorno_C($NroHistoriaClinica){
	return Conserje_Listar_Atenciones_Emergencia_Paciente_Retorno_M($NroHistoriaClinica);
	}
	
	
	// Funciones de Lista de Historias sin Retornar de Constancias Medicas 07/08/2018
	function Archivo_Clinico_Mostrar_Lista_Historias_Segun_Servicio_sin_Salir_C($FechaInicio,$FechaFinal,$IdServicio){
	return Archivo_Clinico_Mostrar_Lista_Historias_Segun_Servicio_sin_Salir_M($FechaInicio,$FechaFinal,$IdServicio);
	}
	
	
	
	function Mostrar_si_es_Supervisor_Archivo_Clinico_C($Id_Empleado){
	return Mostrar_si_es_Supervisor_Archivo_Clinico_M($Id_Empleado);
	}
	
    // Funcion para Modificar el Retorno General de las Historias Clinicas

    function Insertar_Retorno_General_Array_C($FechaCita,$IdPaciente,$Observacion,$IdEmpleado){
    return Insertar_Retorno_General_Array_M($FechaCita,$IdPaciente,$Observacion,$IdEmpleado);	
    }

	
	
	function Listar_Citas_de_Paciente_por_Historia_Clinica_C($HistoriaClinica,$Fecha_Inicio,$Fecha_Final){
    return Listar_Citas_de_Paciente_por_Historia_Clinica_M($HistoriaClinica,$Fecha_Inicio,$Fecha_Final);	
    }

		
 ?>