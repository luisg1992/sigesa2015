<?php 
include('../../MVC_Modelo/SistemaM.php');
include('../../MVC_Modelo/AtencionesCertificadosM.php');
include('../../MVC_Modelo/MaestrosM.php');
include('../../MVC_Complemento/librerias/Funciones.php');
date_default_timezone_set('America/Bogota');
//$IdEmpleado = $_REQUEST["IdEmpleado"];
if($_REQUEST["acc"] == "Registro") // MOSTRAR: Formulario InicioExportarTxt
{  
	echo '<br>';
	include('../../MVC_Vista/AtencionesCertificados/RegistroAC.php'); 

}
if($_REQUEST["acc"] == "Consulta") // MOSTRAR: Formulario InicioExportarTxt
{  
	echo '<br>';
	include('../../MVC_Vista/AtencionesCertificados/ConsultaAC.php'); 

}
if ($_REQUEST["acc"] == "BusquedaDatosPaciente") {
	
	$tipo_busqueda = $_REQUEST["tipobusuqeda"]; // tipo busqueda 1=HISTORIA CLINICA 2=NRO DE DCTO DNI
	$dato = $_REQUEST["dato"]; //valor de busqueda
	
	$RetornoData = SIGESA_CertificadoMedico_BuscarPaciente_LCO($dato,$tipo_busqueda);
			$data_paciente = array(
					'IdPaciente' => $RetornoData[0]["IdPaciente"],
					'ApellidoPaterno' => mb_strtoupper($RetornoData[0]["ApellidoPaterno"]),
					'ApellidoMaterno' => $RetornoData[0]["ApellidoMaterno"],
					'Nombres' => mb_strtoupper($RetornoData[0]["PrimerNombre"]).' '.mb_strtoupper($RetornoData[0]["SegundoNombre"]),
					'FechaNacimiento' => vfecha(substr($RetornoData[0]["FechaNacimiento"],0,10)),
					'IdTipoSexo' => $RetornoData[0]["IdTipoSexo"],
					'NomDist' => mb_strtoupper($RetornoData[0]["NomDist"]),
					'NomProv' => mb_strtoupper($RetornoData[0]["NomProv"]),
					'NomDep' => mb_strtoupper($RetornoData[0]["NomDep"]),
					'DireccionDomicilio' => mb_strtoupper($RetornoData[0]["DireccionDomicilio"]),
					'NroDocumento' => $RetornoData[0]["NroDocumento"],
					'NroHistoriaClinica' => $RetornoData[0]["NroHistoriaClinica"],
					'edadpaciente'=>CalcularEdadTonno(substr($RetornoData[0]["FechaNacimiento"],0,10)),
					'IdDistritoDomicilio'=>$RetornoData[0]["IdDistritoDomicilio"],
					
					);
				echo json_encode($data_paciente);
				exit();	
}


if ($_REQUEST["acc"] == "Mostrar_AtencionesCertificados") {
		$data = SIGESA_CertificadoMedico_Listar_LCO();
		for ($i=0; $i < count($data); $i++) {
				$input=$data[$i]['NroCertificado'];
				$NroCert= str_pad($input, 5, "0", STR_PAD_LEFT); 
				 $Lista[$i] = array(
				'anio'					=> $data[$i]['AnnioCertificado'],
				'ndoc'					=> $NroCert,
				'paciente'				=> mb_strtoupper(($data[$i]["ApellidoPaterno"])).' '.mb_strtoupper(($data[$i]["ApellidoMaterno"])).' '.mb_strtoupper(($data[$i]["PrimerNombre"])).' '.mb_strtoupper(($data[$i]["SegundoNombre"])),
				'dni'					=> $data[$i]['NroDocumento'],
				'femision'				=> vfecha(substr($data[$i]["FechaEmision"],0,10)),
				'IdAtencionCertMed'		=> $data[$i]['IdAtencionCertMed'],
				'NroImpresion'			=> $data[$i]['NroImpresion']
				);
		}
		echo json_encode($Lista);
		exit();
}



if ($_REQUEST["acc"] == "BusquedaDiagnosticos") {
	$sexo='0';
	$edad='0';
	//$cie10=$_REQUEST["cie10"];
	//$valor =utf8_decode($_REQUEST["q"]);
	$Filtro = $_REQUEST["q"];
		if ($Filtro != '') {
				$RetornaData=SIGESA_CertificadoMedico_BuscarDiagnosticos_LCO($sexo,$edad,$Filtro); 
			//$RetornaData=SIGESA_CertificadoMedico_BuscarDiagnosticos_LCO($Filtro);
	 				for ($i = 0; $i < count($RetornaData); $i++) {
							$resultadodetallado_json[$i] = array(
							//'IdDiagnostico'=>$RetornaData[$i]["IdDiagnostico"],
							'CodigoCIE10' => $RetornaData[$i]["IdDiagnostico"],
							'Descripcion' => mb_strtoupper($RetornaData[$i]["Descripcion"])
            			);
       				 }
			if ($resultadodetallado_json != NULL)
				echo json_encode($resultadodetallado_json);
			exit();
		}
}

if ($_REQUEST["acc"] == "BusquedaDiagnosticosx") {
		$IdTipoSexo=$_REQUEST["IdTipoSexo"];
		$edadpaciente=$_REQUEST["edadpaciente"];
		$Filtro=$_REQUEST["q"];
		$data = SIGESA_CertificadoMedico_BuscarDiagnosticos_LCO($IdTipoSexo,$edadpaciente,$Filtro); 

		for ($i=0; $i < count($data); $i++) { 
			$data[$i] = array(

				
				'CodigoCIE10' => $data[$i]["IdDiagnostico"],
				'Descripcion' => mb_strtoupper($data[$i]["Descripcion"])
				
				);
		}
		
		
		echo json_encode($data);
		exit();
}



if ($_REQUEST["acc"] == "GuardarCertificadoMedicos"){
   $errors = array();      // array to hold validation errors
    $data = array();
//PARAMETROS 
 $anio_actual=date('Y');
 $mes_actual=date('n'); //dial mes sin ceros
	
			//$IdAtencionCertMed='';
//OBTENER EL NRO CORRELATIVO DEL CERTIFICADO MEDICO SEGUN EL AÑO ACTUAL			
			$GenerarCorrelativo=SIGESA_CertificadoMedico_GeneraNroCert_LCO_C($anio_actual);
			$NroCertificado=$GenerarCorrelativo[0]['NroDocumento'];
			$AnnioCertificado=$GenerarCorrelativo[0]['AnnioDocumento'];			
//RECUPERA LOS DATOS DE LA VISTA.		
			
			
			$IdPaciente=$_REQUEST["IdPaciente"];      
			$EdadPaciente=$_REQUEST["EdadPaciente"];        
			$FechaEmision=gfecha($_REQUEST["FechaEmision"]);  
			$IdDistritoDomicilio =$_REQUEST["IdDistritoDomicilio"];     
			$DireccionDomicilio=$_REQUEST["DireccionDomicilio"];  
			$NaturalezaIncapacidad=$_REQUEST["NaturalezaIncapacidad"];      
			//$GradoIncapacidad=$_REQUEST["GradoIncapacidad"];      
			
			if(isset($_REQUEST['GradoIncapacidad'])) { $GradoIncapacidad=$_REQUEST["GradoIncapacidad"];}else{ $GradoIncapacidad=0;} 
			
			$MenoscaboCombinado=$_REQUEST["MenoscaboCombinado"];      
			$TipoActividad=$_REQUEST["TipoActividad"];      
			$PosibilidadReubicaLaboral=$_REQUEST["PosibilidadReubicaLaboral"];      
			$Edad=$_REQUEST["Edad"];      
			$MenoscaboGlobal=$_REQUEST["MenoscaboGlobal"]; 
			
			if(isset($_REQUEST['SwPrecisable'])) { $SwPrecisable=$_REQUEST["SwPrecisable"];}else{ $SwPrecisable=0;}     
          
		  
		  
		
			 
			 if($_REQUEST['FechaInicioIncapacidad']!=NULL){
				 $FechaInicioIncapacidad=gfecha($_REQUEST["FechaInicioIncapacidad"]);
				 }else{$FechaInicioIncapacidad=NULL;}
	
			
			if(isset($_REQUEST['Observaciones'])) { $Observaciones=$_REQUEST["Observaciones"];}else{ $Observaciones=NULL;}             
			$dataResultado = $_REQUEST["dataResultado"];  // Datos del Datagrid
			  
//RECUPERAMOS A LOS MIEMBROS DEL CMCI CORRESPONDIENTE AL AÑO Y MES EN CURSO.
			//$ObtenerCMIC=SIGESA_CertificadoMedico_ListaMiembrosCMI_LCO($mes_actual,$anio_actual);		 NO SE USA X REGULARIZAR PROCESO 12/01/2017 
			$CodigoCMCI='0';//$ObtenerCMIC[0]['codigo'];      
			$IdEmpleadoCrea=$_REQUEST["IdEmpleado"];     
			$FechaCrea=gfechahora(date("d/m/Y H:i:s"));
			
			$NroDocumento=$_REQUEST['NroDocumento'];
			
			 try{
					//guarda certificado
					
					SIGESA_CertificadoMedico_ActualizaPacienteMaestro_LCO($DireccionDomicilio,$IdDistritoDomicilio,$NroDocumento,$IdPaciente );
					
					
					 
				 $IdCertificadoMed=SIGESA_CertificadoMedico_Guardar_LCO($IdPaciente,$EdadPaciente,$AnnioCertificado,$NroCertificado,$FechaEmision,$IdDistritoDomicilio,$DireccionDomicilio,$NaturalezaIncapacidad,$GradoIncapacidad,$MenoscaboCombinado,$TipoActividad,$PosibilidadReubicaLaboral,$Edad,$MenoscaboGlobal,$SwPrecisable,$FechaInicioIncapacidad,$Observaciones,
$CodigoCMCI,$IdEmpleadoCrea,$FechaCrea);

				 
			} catch (Exception $e) {
				echo 'Caught Exception: ', $e->getMessage(), "\n";
           }
//GUARDA LOS DIAGNOSTICOS DEL CERTIFICADO

				for ($i = 0; $i < $dataResultado['total']; $i++) {
                            try {
								$IdAtencionCertMed=$IdCertificadoMed;
								$IdClasificacionDx=8;
                                $IdDiagnostico = $dataResultado['rows'][$i]["x1CodigoCIE10"];
                                SIGESA_CertificadoMedico_GuardarDiagnosticos_LCO($IdAtencionCertMed,$IdClasificacionDx,$IdDiagnostico);
                            } catch (Exception $e) {
                                echo 'Caught Exception: ', $e->getMessage(), "\n";
                            }
                        }
		   
			
	if (!empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $data['success'] = true;
        $data['message'] = 'Success!';
		$data['message'] = $IdCertificadoMed;	
		$data['NroCertificado'] = $IdCertificadoMed;	
		
    }

    echo json_encode($data);	
}
if ($_REQUEST["acc"] == "ImprimeCertificadoMedicoConsulta") {	
		$Annio=$_REQUEST['txtanio'];
		$Nro=$_REQUEST['txtnro'];
		$ImprimirCertificado=SIGESA_CertificadoMedico_Imprimir_VP_LCO($Annio,$Nro);		
		include('../../MVC_Vista/AtencionesCertificados/ImprimirACConsulta.php'); 	
}

if ($_REQUEST["acc"] == "ImprimeCertificadoMedico") {	
	$IdAtencionCertMed=$_REQUEST['NroDocumentoCertificado'];
	$ImprimirCertificado=SIGESA_CertificadoMedico_Imprimir_LCO($IdAtencionCertMed);
	include('../../MVC_Vista/AtencionesCertificados/ImprimirAC.php'); 	
}


if ($_REQUEST["acc"] == "ImprimeCertificadoMedicopdf") {
	$IdAtencionCertMed=$_REQUEST['NroDocumentoCertificado'];
	$ImprimirCertificado=SIGESA_CertificadoMedico_Imprimir_LCO($IdAtencionCertMed);
	include('../../MVC_Vista/AtencionesCertificados/PdfAC.php'); 
}
////////////// LISTADO 
if($_REQUEST["acc"] == "Certificados") // MOSTRAR: LISTA DE CERTIFICADOS EMITIDOS
{   echo '<br>';
	$IdEmpleado=$_REQUEST["IdEmpleado"];
	$IdListItem=$_REQUEST["IdListItem"];	
	$ListarPermisos=Listar_Permisos_Usuario_M($IdEmpleado,$IdListItem);
	include('../../MVC_Vista/AtencionesCertificados/ListaAC.php'); 
}
if($_REQUEST["acc"] == "Actualizar") // MOSTRAR: LISTA DE CERTIFICADOS EMITIDOS

{   echo '<br>';
	$IdAtencionCertMed=$_REQUEST['NroDocumentoCertificado'];
	$ImprimirCertificado=SIGESA_CertificadoMedico_Imprimir_LCO($IdAtencionCertMed);
	include('../../MVC_Vista/AtencionesCertificados/ActualizarAC.php'); 
}
///////////////////////////// ACTUALIZA CERTIFICADO MEDICO
if ($_REQUEST["acc"] == "ActualizaCertificadoMedicos"){
		    $errors = array();      // array to hold validation errors
    		$data = array();
//PARAMETROS 
			$anio_actual=date('Y');
			$mes_actual=date('n'); //dial mes sin ceros
			$IdAtencionCertMed=$_REQUEST["IdAtencionCertMed"]; ;
			//$IdPaciente=$_REQUEST["IdPaciente"];      
			$EdadPaciente=$_REQUEST["EdadPaciente"];        
			//$FechaEmision=gfecha($_REQUEST["FechaEmision"]);  
			$IdDistritoDomicilio =$_REQUEST["IdDistritoDomicilio"];     
			$DireccionDomicilio=$_REQUEST["DireccionDomicilio"];  
			$NaturalezaIncapacidad=$_REQUEST["NaturalezaIncapacidad"];      
//			$GradoIncapacidad=$_REQUEST["GradoIncapacidad"];      
			if(isset($_REQUEST['GradoIncapacidad'])) { $GradoIncapacidad=$_REQUEST["GradoIncapacidad"];}else{ $GradoIncapacidad=0;} 
			$MenoscaboCombinado=$_REQUEST["MenoscaboCombinado"];      
			$TipoActividad=$_REQUEST["TipoActividad"];      
			$PosibilidadReubicaLaboral=$_REQUEST["PosibilidadReubicaLaboral"];      
			$Edad=$_REQUEST["Edad"];      
			$MenoscaboGlobal=$_REQUEST["MenoscaboGlobal"]; 			
			if(isset($_REQUEST['SwPrecisable'])) { $SwPrecisable=$_REQUEST["SwPrecisable"];}else{ $SwPrecisable=0;}               
			if($_REQUEST['FechaInicioIncapacidad']!=NULL){
				 $FechaInicioIncapacidad=gfecha($_REQUEST["FechaInicioIncapacidad"]);
				 }else{$FechaInicioIncapacidad=NULL;
			}			
			if(isset($_REQUEST['Observaciones'])) { $Observaciones=$_REQUEST["Observaciones"];}else{ $Observaciones=NULL;}             
			$dataResultado = $_REQUEST["dataResultado"];  // Datos del Datagrid			  
//RECUPERAMOS A LOS MIEMBROS DEL CMCI CORRESPONDIENTE AL AÑO Y MES EN CURSO.
			//$ObtenerCMIC=SIGESA_CertificadoMedico_ListaMiembrosCMI_LCO($mes_actual,$anio_actual); NO SE USA X REGULARIZAR PROCESO 12/01/2017 
			$CodigoCMCI='0'; 		     
			$IdEmpleadoModifica=$_REQUEST["IdEmpleado"];     
			$FechaModifica=gfechahora(date("d/m/Y H:i:s"));			
			 try{
					//guarda certificado					 
				 $GIdCertificadoMed=SIGESA_CertificadoMedico_Actualizar_LCO($IdAtencionCertMed,$EdadPaciente,$IdDistritoDomicilio,$DireccionDomicilio,$NaturalezaIncapacidad,$GradoIncapacidad,$MenoscaboCombinado,$TipoActividad,$PosibilidadReubicaLaboral,$Edad,$MenoscaboGlobal,$SwPrecisable,$FechaInicioIncapacidad,$Observaciones,
$CodigoCMCI,$IdEmpleadoModifica,$FechaModifica);			 
			} catch (Exception $e) {
				echo 'Caught Exception: ', $e->getMessage(), "\n";
           }
//GUARDA LOS DIAGNOSTICOS DEL CERTIFICADO

				 SIGESA_CertificadoMedico_EliminarDiagnosticos_LCO($IdAtencionCertMed);
				for ($i = 0; $i < $dataResultado['total']; $i++) {
                            try {
								//$IdAtencionCertMed=$IdCertificadoMed;
								$IdClasificacionDx=8;
                                $IdDiagnostico = $dataResultado['rows'][$i]["x1CodigoCIE10"];
                                SIGESA_CertificadoMedico_GuardarDiagnosticos_LCO($IdAtencionCertMed,$IdClasificacionDx,$IdDiagnostico);
                            } catch (Exception $e) {
                                echo 'Caught Exception: ', $e->getMessage(), "\n";
                            }
                        }
	if (!empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $data['success'] = true;
        $data['message'] = 'Success!';
		$data['message'] = $IdAtencionCertMed;	
		$data['NroCertificado'] = $IdAtencionCertMed;			
    }
    echo json_encode($data);	
}


///////////////////
if ($_REQUEST["acc"] == 'MostrarDepartamentoCombo') {
		$data = DepartamentosSeleccionarTodos();

		for ($i=0; $i < count($data); $i++) { 
			$departamentos[$i] = array(
				'id'	=>	$data[$i]["IdDepartamento"],
				'text'	=>	$data[$i]["Nombre"]
				);
		}
		echo json_encode($departamentos);
		exit();
	}

	if ($_REQUEST["acc"] == 'MostrarProvinciasCombo') {
		$idDepartamento = $_REQUEST["idDepartamento"];
		$data = ProvinciasSeleccionarPorDepartamento($idDepartamento);

		for ($i=0; $i < count($data); $i++) { 
			$provinvias[$i] = array(
				'IdProvincia'	=>	$data[$i]["IdProvincia"],
				'NomProvincia'	=>	(mb_strtoupper($data[$i]["Nombre"]))
				);
		}
		echo json_encode($provinvias);
		exit();
	}

	if ($_REQUEST["acc"] == 'MostrarDistritosCombo') {
		$idProvincias = $_REQUEST["idProvincias"];
		$data = DistritosSeleccionarPorProvincia($idProvincias);

		for ($i=0; $i < count($data); $i++) { 
			$distritos[$i] = array(
				'IdDistrito'	=>	$data[$i]["IdDistrito"],
				'NomDistrito'	=>	(mb_strtoupper($data[$i]["Nombre"]))
				);
		}
		echo json_encode($distritos);
		exit();
	}
function SIGESA_CertificadoMedico_GeneraNroCert_LCO_C($anio_actual){
    return
            SIGESA_CertificadoMedico_GeneraNroCert_LCO($anio_actual);
}
if ($_REQUEST["acc"] == "BuscarPacientes") {
   $valor=$_REQUEST["q"];
   $TipoBusqueda='ApellidosNombrespaciente';
   $data = SIGESA_ListVer_Buscarpaciente_M($valor,$TipoBusqueda);
		   $data = SIGESA_ListVer_Buscarpaciente_M($valor,$TipoBusqueda);

		for ($i=0; $i < count($data); $i++) { 
		$Paciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].' '.$data[$i]["PrimerNombre"];
			$data[$i] = array(
				'IdPaciente' => $data[$i]["NroHistoriaClinica"],
				'Paciente' => $Paciente//mb_strtoupper($data[$i]["Descripcion"])
				
				);
		}
		echo json_encode($data);
		exit();
}
?>