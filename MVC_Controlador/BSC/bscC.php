<?php 

ini_set('memory_limit', '1024M');

include_once("../../MVC_Modelo/bscM.php");
include_once('../../MVC_Modelo/SistemaM.php');
include_once('../../MVC_Modelo/ServiciosM.php');

	#PARA REDIRECCIONAR A LA VISTA
	if($_REQUEST["acc"] == "Indicador De Gestion"){
		include("../../MVC_Vista/BSC/InterfazBSC.php");
	}
	
	if($_REQUEST["acc"] == "Indicador De Servicios") 
	{  
 		include("../../MVC_Vista/BSC/Atenciones_Citas.php");
	}

	if($_REQUEST["acc"] == "Indicadores  Economicos") 
	{  
 		include("../../MVC_Vista/BSC/ieconomicos.php");
	}	 	
	
	if($_REQUEST["acc"] == "Reporte  Generales")
	{
		include("../../MVC_Vista/BSC/reporte_generales.php");
	}	

	
	if($_REQUEST["acc"] == "Cuadros_Estadisticos")
	{
		include("../../MVC_Vista/Estadisticas/EstadisticaV.php");
	}	
	#PARA MOSTRAR REPORTE GENERAL
	if ($_REQUEST["acc"] == "Pruebas_BSC") {		
		$data_citas_temp = Mostrar_Citas_x_Medico_M();
		$data_horas_temp = Mostrar_Horas_x_Medico_M();
		//print_r($data_citas_temp);
		$indicadores = Reporte_x_Meses($data_citas_temp,$data_horas_temp);
		echo json_encode($indicadores);
		exit();
	}

	#DEPARTAMENTOS HOSPITAL
	if ($_REQUEST["acc"] == "Departamentos_Hospital") {
		$data_horas_temp = Mostrar_Horas_x_Medico_M();
		for ($i=0; $i < count($data_horas_temp); $i++) { 
			$departamentos[]  	= array(
									'id' 	=> $data_horas_temp[$i]["IDDPTO"],
									'text'	=> utf8_decode(strtoupper(utf8_encode($data_horas_temp[$i]["DEPARTAMENTO"])))
									);
		}
		$departamentos  = array_values(array_unique2($departamentos));
		//$departamentos = array_unique(array_values($departamentos));
		echo json_encode($departamentos);
		exit();		
	}

	#ESPECIALIDADES HOSPITAL POR DEPARTAMENTO
	if ($_REQUEST["acc"] == "Especialidades_Hospital") {
		$id_dpto = $_REQUEST["id_dpto"];
		$especialidades = MostrarEspecialidadxDepartamento($id_dpto);
		echo json_encode($especialidades);
		exit();	
	}

	#PARA MOSTRAR REPORTES POR DEPARTAMENTOS
	if ($_REQUEST["acc"] == "Reportes_x_Departamentos") {
		
		#OBTENIENDO ID DE DEPARTAMENTO
		$dpto = $_REQUEST["id_dpto"];

		#OBTENER LAS ESPECIALIDADES
		$especialidades = MostrarEspecialidadxDepartamento($dpto);

			for ($i=0; $i < count($especialidades); $i++) { 
				$id_especialidades[] 		= $especialidades[$i]["id"];
				$nombres_especialidades[] 	= $especialidades[$i]["text"];
			}

		#ARMANDO ARRAY CON LOS VALORES DE LOS INDICADORES
		$i = 0;
		while ($i < count($id_especialidades)):
    		
    		#PARA CITAS
    		$citas_temp 		= Mostrar_Citas_x_Medico_Esp_Dpto_M($dpto,$id_especialidades[$i]);
    		$suma_citas_temp 	= 0;

    		for ($x=0; $x < count($citas_temp); $x++) { 
    			$suma_citas_temp = $citas_temp[$x][1] + $citas_temp[$x][2] + $citas_temp[$x][3] + $citas_temp[$x][4] + $citas_temp[$x][5] + $citas_temp[$x][6] + $citas_temp[$x][7] + $citas_temp[$x][8] + $citas_temp[$x][9] + $citas_temp[$x][10] + $citas_temp[$x][11] + $citas_temp[$x][12] + $suma_citas_temp;
    		}

    		#PARA HORAS
    		$horas_temp 		= Mostrar_Horas_x_Medico_Esp_Dpto_M($dpto,$id_especialidades[$i]);
    		$suma_horas_temp 	= 0;

    		for ($y=0; $y < count($horas_temp); $y++) { 
    			$suma_horas_temp = $horas_temp[$y][1] + $horas_temp[$y][2] + $horas_temp[$y][3] + $horas_temp[$y][4] + $horas_temp[$y][5] + $horas_temp[$y][6] + $horas_temp[$y][7] + $horas_temp[$y][8] + $horas_temp[$y][9] + $horas_temp[$y][10] + $horas_temp[$y][11] + $horas_temp[$y][12] + $suma_horas_temp;
    		} 

    		#INDICADORES
    		if ($suma_horas_temp == 0) {
    			$indicadores[$i] = 0;
    		}
    		else {
    			$indicadores[$i] = ($suma_citas_temp / $suma_horas_temp);    			
    		}			
			$i++;

		endwhile;

		echo json_encode(array('ESPECILIDADES' => $nombres_especialidades, 'INDICADORES' => $indicadores));
		exit();
	}

	#PARA MOSTRAR REPORTES POR ESPECIALIDADES
	if ($_REQUEST["acc"] == "Reportes_x_Especialidades") {
		
		#OBTENIENDO DEPARTAMENTO Y ESPECIALIDAD
		$dpto 	= $_REQUEST["id_dpto"];
		$esp 	= $_REQUEST["especia"];

		#EXTRAYENDO DATA
		$data_citas_temp = Mostrar_Citas_x_Medico_Esp_Dpto_M($dpto,$esp);
		$data_horas_temp = Mostrar_Horas_x_Medico_Esp_Dpto_M($dpto,$esp);
		$indicadores = Reporte_x_Meses($data_citas_temp,$data_horas_temp);
		
		#DESGREGANDO LA INFORMACION
		foreach ($indicadores as $key => $value) {
			$meses[] = $key;
			$indicador[] = $value;
		}

		#ENVIANDO DATOS A VISTA
		echo json_encode(array('MESES' => $meses, 'INDICADORES' => $indicador));
		exit();
	}

	#MEDICOS 
	if ($_REQUEST["acc"] == "Mostrar_Medicos") {
		$data_horas_temp = Mostrar_Horas_x_Medico_M();
		for ($i=0; $i < count($data_horas_temp); $i++) { 
			$medicos[]  	= array(
									'id' 	=> $data_horas_temp[$i]["IdMedico"],
									'text'	=> utf8_decode(strtoupper(utf8_encode($data_horas_temp[$i]["Nombre"])))
									);
		}
		//print_r($medicos);exit();
		$medicos  = array_values(array_unique2($medicos));
		//$medicos = array_values($medicos);
		echo json_encode($medicos);
		exit();		
	}

	#REPORTES POR ID DE MEDICO
	if ($_REQUEST["acc"] == "Reportes_x_Id_Medico") {
		$id_med = $_REQUEST["id_medico"];

		$data_citas_temp = Mostrar_Citas_x_Id_Medico_M($id_med);
		$data_horas_temp = Mostrar_Horas_x_Id_Medico_M($id_med);
		$indicadores = Reporte_x_Meses($data_citas_temp,$data_horas_temp);

		#DESGREGANDO INFORMACION
		foreach ($indicadores as $key => $value) {
			$meses[] = $key;
			$indicador[] = $value;
		}

		echo json_encode(array('MESES' => $meses, 'INDICADORES' => $indicador, 'ESPECIALIDAD_MEDICO' => $data_citas_temp[0]["ESPECIALIDAD"]));
		exit();

	}
	
	#ATENDIDOS POR EMERGENCIA / CITAS POR CONSULTA EXTERNA
	if ($_REQUEST["acc"] == "Atendidos_x_Emergencia") {
		$data_aten_emergencia 	= Mostrar_Atendidos_x_Emergencia_M();
		$data_citas_temp 		= Mostrar_Citas_x_Medico_M();

		$indicadores = Reporte_x_Meses($data_aten_emergencia,$data_citas_temp);
		echo json_encode($indicadores);
		exit();
	}
	
	#ATENCIONES / CITAS GRAFICO A DOS BARRA
	if ($_REQUEST["acc"] == "Atenciones_y_Citados_DB") {
		#INCIO DE EJECUCION
		$inicio = microtime(true);

		$data_citas_temp = Mostrar_Citas_x_Medico_M();
		$data_atenc_temp = Mostrar_Atendidos_M();

		$data_citas 	 = Cantidad_x_Mes($data_citas_temp);
		$data_atenciones = Cantidad_x_Mes($data_atenc_temp);

		#DESGREGANDO INFORMACION
		foreach ($data_citas as $key => $value) {
			$meses[] = $key;
			$cantidad_citas[] = $value;
		}

		foreach ($data_atenciones as $key => $value) {
			#$meses[] = $key;
			$cantidad_atenciones[] = $value;
		}

		$series[0] = array(
			'name' => 'Atenciones',
			'data' => $cantidad_citas,
			'color' => '#0174DF'
			);

		$series[1] = array(
			'name' => 'Atendidos',
			'data' => $cantidad_atenciones,
			'color' => '#81DAF5'
			);

		
		#FINAL DE EJECUCION
		$final = microtime(true);
		$tiempo = $final - $inicio;

		$maximo = max($cantidad_citas);

		#IMPRIMIENDO INFORMACION
		echo json_encode(array('MESES' => $meses, 'SERIES' => $series, 'TIEMPO' => $tiempo, 'MAX' => round($maximo)));
		exit();
	}
	
	#REPORTE DE ATENCIONES Y ATENDIDOS POR EMERGENCIA
	if ($_REQUEST["acc"] == "Atenciones_Atendidos_Emergencia") {
		#INCIO DE EJECUCION
		$inicio = microtime(true);
		
		$data_atenciones_emergencia_temp = Mostrar_Atenciones_x_Emergencia_Serv_M();
		$data_atendidos_emergencia_temp  = Mostrar_Atendidos_x_Emergencia_Serv_M();

		$data_atenciones_emergencia = Cantidad_x_Mes($data_atenciones_emergencia_temp);
		$data_atendidos_emergencia  = Cantidad_x_Mes($data_atendidos_emergencia_temp);

		#DESGREGANDO INFORMACION
		foreach ($data_atenciones_emergencia as $key => $value) {
			$meses[] = $key;
			$atenciones[] = $value;
		}

		foreach ($data_atendidos_emergencia as $key => $value) {
			#$meses[] = $key;
			$atendidos[] = $value;
		}

		#ARMANDO LA SERIE PARA HIGHCHARTS
		$series[0] = array(
			'name' => 'Atenciones por Emergencia',
			'data' => $atenciones,
			'color' => '#0174DF'
			);

		$series[1] = array(
			'name' => 'Atendidos por Emergencia',
			'data' => $atendidos,
			'color' => '#81DAF5'
			);

		#FINAL DE EJECUCION
		$final = microtime(true);
		$tiempo = $final - $inicio;

		$maximo = max($atenciones);

		echo json_encode(array('MESES' => $meses, 'SERIES' => $series, 'TIEMPO' => $tiempo, 'MAX' => round($maximo)));
		exit();
	}

	#REPORTE DE INGRESO POR CENTRO DE COSTO
	if ($_REQUEST["acc"] == "Ingreso_x_Centro_Costo") {
		
		#RECIBIMOS EL ANIO
		#$anio=$_REQUEST["anio"];

		$anio = date("Y");
		$primer_dia = $anio."-01-01";
		$ultimo_dia = $anio."-12-31";

		$data = Mostrar_Ingreso_x_Centro_Costo_M($primer_dia,$ultimo_dia);

		#VERIFICANDO LA EXISTENCIA DE DATOS
		if ($data != NULL) {
			
			for ($i=0; $i < count($data); $i++) { 
				$categorias[]    = strtoupper(utf8_decode(utf8_encode($data[$i]["Descripcion"])));
				$totales[]       = floatval($data[$i]["Total"]);
				$exoneracion[]   = floatval($data[$i]["Exoneracion"]);
				$precios[]       = floatval($data[$i]["Precio"]);
			}

			$series[0] = array(
				'name'  => 'Totales',
				'data'  => $totales,
				'color' => '#0080FF'
				);
			

			$series[1] = array(
				'name'  => 'Exoneracion',
				'data'  => $exoneracion,
				'color' => '#FF0000'
				);

			$series[2] = array(
				'name'  => 'Subtotal',
				'data'  => $precios,
				'color' => '#64FE2E'
				);

			
			$maximo = max($totales);
		}

		echo json_encode(array('CATEGORIAS' => $categorias, 'SERIES' => $series, 'MAX' => round($maximo)));
		exit();
	}

	#REPORTE DE INGRESO POR CENTRO DE COSTO - POR ID DE CENTRO DE COSTO
	if ($_REQUEST["acc"] == "Ingreso_x_Centro_Costo_x_IdCC") {
		#RECIBIMOS EL ANIO
		$anio=$_REQUEST["anio"];
		$idcentrocosto=$_REQUEST["idcc"];
		$idmes = $_REQUEST["mes"];

		if ($idmes == 0) {
			$primer_dia = $anio."-01-01";
			$ultimo_dia = $anio."-12-31";
			$data = Mostrar_Ingreso_x_Centro_Costo_x_idcc_M($primer_dia,$ultimo_dia,$idcentrocosto);
		}
		
		else {
			$primer_dia = Primer_Dia_Mes($idmes);
			$ultimo_dia = Ultima_Dia_Mes($idmes);
			$data = Mostrar_Ingreso_x_Centro_Costo_x_idcc_M($primer_dia,$ultimo_dia,$idcentrocosto);
		}
		

		#EXTRAYENDO INFORMACION
		for ($i=0; $i < count($data); $i++) { 
			$datos[] 			 = floatval($data[$i]["Precio"]);
			$datos[] 			 = floatval($data[$i]["Exoneracion"]);
			$datos[] 			 = floatval($data[$i]["Total"]);
			$nombre_centro_costo = strtoupper(utf8_decode(utf8_encode($data[$i]["Descripcion"])));
		}

		$series[0] = array(
				'name' => 'Soles',
				'data' => $datos,
				'color' => '#ACFA58'
				);

		$maximo = max($datos);
		$categorias = array("SUBTOTAL","EXONERACION","TOTAL");
		$nombre_mes = ObtenerNombreMes($idmes);

		echo json_encode(array('CATEGORIAS' => $categorias, 'DATA' => $series, 'CENTRO_COSTO' => $nombre_centro_costo, 'MAX' => round($maximo), 'MES' => $nombre_mes));
		exit();
	}

	#CENTROS DE COSTOS
	if ($_REQUEST["acc"] == "Centros_Costos_Listado") {
		
		$anio = date("Y");
		$primer_dia = $anio."-01-01";
		$ultimo_dia = $anio."-12-31";

		$data = Mostrar_Ingreso_x_Centro_Costo_M($primer_dia,$ultimo_dia);
		for ($i=0; $i < count($data); $i++) { 
			if($i == 0)
			{
				$centros_costos[] = array(
					'id'   => $data[$i]["IdCentroCosto"],
					'text' => strtoupper(utf8_decode(utf8_encode($data[$i]["Descripcion"]))),
					'selected' =>true
				);
			}
			else
			{$centros_costos[] = array(
					'id'   => $data[$i]["IdCentroCosto"],
					'text' => strtoupper(utf8_decode(utf8_encode($data[$i]["Descripcion"])))
				);
			}
		}

		echo json_encode($centros_costos);
		exit();
	}
	
	#REPORTE DE ATENCIONES POR PACIENTE - BUSQUEDA POR NUMERO DE HISTORIA O DNI
	if ($_REQUEST["acc"] == "Mostrar_Reporte_Paciente_x_NumHis_C") {
		$variable_busqueda = $_REQUEST["var_des"];
		$tipo = $_REQUEST["tipo_b"];

		//echo $variable_busqueda . " " . $tipo;exit();
		switch ($tipo) {
			
			#BUSQUEDA POR NUMERO DE HISTORIA
			case 1:
				$respuesta = Mostrar_Reporte_Paciente_x_NumHis_M($variable_busqueda);
				#print_r($respuesta);return false;
				if ($respuesta != NULL) {
					#CREANDO TABLA
					$tabla = '<h3>PACIENTE :'.$respuesta[0]["DATOSPACIENTES"].' &nbsp;&nbsp;DNI: '.$respuesta[0]["DNI"].' &nbsp;&nbsp;NUMERO HISTORIA: '.$variable_busqueda.'</h3>';
					$tabla .= '<table class="easyui-datagrid" title="" style="width:100%;"
                    data-options="singleSelect:true,collapsible:true" id="tabla_pacientes">
                        <thead>
                            <tr>
                            	<th field="cuenta" resizable:true>CUENTA</th> 
                                <th field="fecha" resizable:true>FECHA</th>                                
                                <th field="Tipo" resizable:true>TIPO AFILIACION</th>
                                <th field="Tipo" resizable:true>TIPO SERVICIO</th>
                                <th field="Departamento" resizable:true>DEPARTAMENTO</th>
                                <th field="Especialidad" resizable:true>ESPECIALIDAD</th>
                                <th field="Consultorio" resizable:true>CONSULTORIO</th>
                                <th field="Medico" resizable:true>MEDICO</th>
                                <th field="Diagnostico" resizable:true>DIAGNOSTICO</th>
                                <th field="Codigo" resizable:true>COD. DIAGNOSTICO</th>
                            </tr>
                        </thead>';
					for ($i=0; $i < count($respuesta); $i++) { 
							$tabla .= "<tr>";
							$tabla .= "<td>" . $respuesta[$i]["NCUENTA"] . "</td>";
							$tabla .= "<td>" . substr($respuesta[$i]["FECHA"], 0, 10) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["TIPO"]))) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["TIPOSERVICIO"]))) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["DEPARTAMENTO"]))) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["ESPECIALIDAD"]))) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["CONSULTORIO"]))) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["DATOSMEDICO"]))) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["NOMBREDX"]))) . "</td>";
							$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["DIAGCIEX"]))) . "</td>";	
							$tabla .= "</tr>";						
					}
					
					$tabla .= "</table>";
					echo json_encode(array('tabla_paciente' => $tabla));
				}
				else {
					echo json_encode(array('respuesta' => "R_1"));
				}
				break;
			
			#BUSQUEDA POR NUMERO DE DNI
			case 2:
				$respuesta = Mostrar_Reporte_Paciente_x_NumDni_M($variable_busqueda);
				if ($respuesta != NULL) {
					#CREANDO TABLA
					$tabla = '<h3>PACIENTE :'.$respuesta[0]["DATOSPACIENTES"].' &nbsp;&nbsp;DNI: '.$variable_busqueda.' &nbsp;&nbsp;NUMERO HISTORIA: '.$respuesta[0]["HISTORIANUM"].'</h3>';
					$tabla .= '<table class="easyui-datagrid" title="" style="width:100%;"
                    data-options="singleSelect:true,collapsible:true" id="tabla_pacientes">
                        <thead>
                            <tr>
                            	<th field="cuenta" resizable:true>CUENTA</th> 
                                <th field="fecha" resizable:true>FECHA</th>                                
                                <th field="Tipo" resizable:true>TIPO AFILIACION</th>
                                <th field="Tipo" resizable:true>TIPO SERVICIO</th>
                                <th field="Departamento" resizable:true>DEPARTAMENTO</th>
                                <th field="Especialidad" resizable:true>ESPECIALIDAD</th>
                                <th field="Consultorio" resizable:true>CONSULTORIO</th>
                                <th field="Medico" resizable:true>MEDICO</th>
                                <th field="Diagnostico" resizable:true>DIAGNOSTICO</th>
                                <th field="Codigo" resizable:true>COD. DIAGNOSTICO</th>
                            </tr>
                        </thead>';
					for ($i=0; $i < count($respuesta); $i++) { 
						$tabla .= "<tr>";
						$tabla .= "<td>" . $respuesta[$i]["NCUENTA"] . "</td>";
						$tabla .= "<td>" . substr($respuesta[$i]["FECHA"], 0, 10) . "</td>";						
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["TIPO"]))) . "</td>";
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["TIPOSERVICIO"]))) . "</td>";
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["DEPARTAMENTO"]))) . "</td>";
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["ESPECIALIDAD"]))) . "</td>";
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["CONSULTORIO"]))) . "</td>";
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["DATOSMEDICO"]))) . "</td>";
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["NOMBREDX"]))) . "</td>";
						$tabla .= "<td>" . strtoupper(utf8_decode(utf8_encode($respuesta[$i]["DIAGCIEX"]))) . "</td>";	
						$tabla .= "</tr>";
					}
					
					$tabla .= "</table>";
					echo json_encode(array('tabla_paciente' => $tabla));					
				}
				else {
					echo json_encode(array('respuesta' => "R_1"));
				}
				break;
		}

		
		exit();
	}
	
	#MOSTRAR MEDICOS PARA COMBOBOX
	if ($_REQUEST["acc"] == "Mostrar_Medicos_Reportes_Generales") {
		$data_medicos = Mostrar_Medicos_M();
		for ($i=0; $i < count($data_medicos); $i++) { 
			if ($i == 0) {
				$medicos[]  	= array(
									'id' 	=> $data_medicos[$i]["ID_MEDICO"],
									'text'	=> utf8_decode(strtoupper(utf8_encode($data_medicos[$i]["DATOSMEDICO"]))),
									'selected' =>true
									);
			}
			else{
			$medicos[]  	= array(
									'id' 	=> $data_medicos[$i]["ID_MEDICO"],
									'text'	=> utf8_decode(strtoupper(utf8_encode($data_medicos[$i]["DATOSMEDICO"])))
									);
			}
		}
		$medicos  = array_values(array_unique2($medicos));
		echo json_encode($medicos);
		exit();		
	}

	#PROGRAMACION MEDICA POR MES
	if($_REQUEST["acc"] == "Programacion_Medica_Por_Mes") {
		$idmedico 	= $_REQUEST["idmedico"];
		$mes 		= $_REQUEST["mes"];
		$anio 		= $_REQUEST["anio"];
		$meses 		= array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SETIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
		$diaSemana	= date("w",mktime(0,0,0,$mes,1,$anio))+7;
		$primer_dia = Primer_Dia_Mes($mes,$anio);
		$ultimo_dia = Ultima_Dia_Mes($mes,$anio);

		$data_general = Mostrar_Programacion_x_Mes_M($primer_dia,$ultimo_dia,$idmedico);
		
		if ($data_general != NULL) {
			for ($i=0; $i < count($data_general); $i++) { 
			$dias_programados[] = substr($data_general[$i]["Fecha"], 0, 10);
		}

		$dias_programados_temp = array_values(array_unique2($dias_programados));



		#echo "<pre>";
		#print_r($data_general);
		#echo "</pre>";exit();

		for ($i=0; $i < count($dias_programados_temp); $i++) { 
			$dias[] = array(
				'fecha' => $dias_programados_temp[$i],
				'numero_dia' => substr($dias_programados_temp[$i], 8)
				);
		}

		
		#echo "<pre>";
		#print_r($dias);
		#echo "</pre>";exit();
		
		$calendario .= '<table class="calendario">';
			$calendario .= '<caption >'.$meses[$mes-1]. ' ' .$anio.'</caption>';
			$calendario .= '<tr>';
				$calendario .= '<th>Lu</th><th>Ma</th><th>Mi</th><th>Ju</th>';
				$calendario .= '<th>Vi</th><th>Sa</th><th>Do</th>';
			$calendario .= '</tr>';
			$calendario .= '<tr>';
				
				$last_cell=$diaSemana+$ultimo_dia;
				// hacemos un bucle hasta 42, que es el m�ximo de valores que puede
				// haber... 6 columnas de 7 dias
				$x = 0;
				for($i=1;$i<=42;$i++)
				{
					if($i==$diaSemana)
					{
						// determinamos en que dia empieza
						$day=1;						
					}
					if($i<$diaSemana || $i>=$last_cell)
					{
						// celca vacia
						$calendario .= "<td>&nbsp;</td>";
					}else{
						
						// mostramos el dia
						if($day==$dias[$x]["numero_dia"]){
									$fecha = '"'.$dias[$x]["fecha"].'"';
									$calendario .= "<td style='background:#2ECCFA;padding:10px;color:#fff;' id='dia_seleccionado_cc' onclick='MostrarContenido(".$fecha.",".$idmedico.",this)'> ".$day."</td>";															
							$x = $x +1;}
						else{
							$calendario .= "<td>$day</td>";
						}
						
						
						$day++;
					}
					
					// cuando llega al final de la semana, iniciamos una columna nueva
					if($i%7==0)
					{
						$calendario .= "</tr><tr>\n";
					}

				}
			
		$calendario .= '</tr>';
		$calendario .= '</table>';
		echo json_encode(array('calendario' => $calendario));
		exit();
		}

		else {
			echo json_encode(array('calendario' => 'R_1'));
				exit();
		}
		
	}

	#PROGRAMACION MEDICA DETALLADA
	if ($_REQUEST["acc"] == "Programacion_Medica_detallada") {
		$fecha = $_REQUEST["fecha"];
		$idmedico = $_REQUEST["idmedico"];

		#ARMANDO CABECERA
		$data_cabecera = Detalle_Programacion_x_Dia_x_Medico_M($fecha,$idmedico);

		for ($i=0; $i < count($data_cabecera); $i++) { 
			$fecha_cabecera    = substr($data_cabecera[$i]["FECHA"], 0, 10);
			$horas_programadas = $data_cabecera[$i]["HORASPROGRAMADAS"];
			$turno_cabecera    = utf8_decode(utf8_encode($data_cabecera[$i]["TURNO"]));
			$especialidad_cabecera = strtoupper(utf8_decode(utf8_encode($data_cabecera[$i]["ESPECIALIDAD"])));	
			$medico = utf8_decode(utf8_encode($data_cabecera[$i]["DATOSMEDICO"]));
		}

		$cabecera  = "<br><label style='background-color:#01A9DB;color:#fff;padding:1%;'><strong>DIA: ".$fecha_cabecera."</strong></label><br><br>";
		$cabecera .= "<label><strong>MEDICO:</strong> ".$medico."</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$cabecera .= "<label><strong>ESPECIALIDAD:</strong> ".$especialidad_cabecera."</label><br><br>";
		
		$cabecera .= "<label><strong>TURNO:</strong> ".$turno_cabecera."</label>&nbsp;&nbsp;&nbsp;";
		$cabecera .= "<label><strong>HORAS PROGRAMADAS </strong>: ".$horas_programadas." HORAS</label><br><br>";

		$data_detalle = Detalle_Programacion_x_Dia_x_Medico_Completo_M($fecha,$idmedico);

		$sumatoria_asignados = 0;
		$sumatoria_adicional = 0;

		$tabla = "<table style='border:1px solid #d3d3d3;border-collapse: collapse;'>";
		$tabla .= "<tr><td style='border:1px solid #d3d3d3;background-color:#eee;padding:5px;'>His.Cli.</td><td style='border:1px solid #d3d3d3;background-color:#eee;padding:5px;'>HORA INICIO</td><td style='border:1px solid #d3d3d3;background-color:#eee;padding:5px;'>HORA FIN</td><td style='border:1px solid #d3d3d3;background-color:#eee;padding:5px;'>PACIENTE</td><td style='border:1px solid #d3d3d3;background-color:#eee;padding:5px;'>TIPO</td></tr>";
		for ($i=0; $i < count($data_detalle); $i++) { 
			$adicional_var = $data_detalle[$i]["ADICIONAL"];

				if ($adicional_var == 0) {
					$sumatoria_asignados = $sumatoria_asignados + 1;
					$texto_adicional = "<td style='border:1px dotted #d3d3d3;padding:5px;'>PROGRAMADA</td>";
				}

				if ($adicional_var == 1) {
					$sumatoria_adicional = $sumatoria_adicional + 1;
					$texto_adicional = "<td style='border:1px dotted #d3d3d3;padding:5px;color:#74DF00;font-weight: bolder'>ADICIONAL</td>";
				}

			$tabla .= "<tr>";
				$tabla .= "<td style='border-bottom:1px dotted #d3d3d3;border-left:1px dotted #d3d3d3;padding:5px;' align='center'>".$data_detalle[$i]["NroHistoriaClinica"]."</td>";
				$tabla .= "<td style='border-bottom:1px dotted #d3d3d3;border-left:1px dotted #d3d3d3;padding:5px;' align='center'>".$data_detalle[$i]["HORAINICIO"]."</td>";
				$tabla .= "<td style='border-bottom:1px dotted #d3d3d3;border-left:1px dotted #d3d3d3;padding:5px;' align='center'>".$data_detalle[$i]["HORAFIN"]."</td>";
				$tabla .= "<td style='border-bottom:1px dotted #d3d3d3;border-left:1px dotted #d3d3d3;padding:5px;'>".$data_detalle[$i]["PACIENTE"]."</td>";
				$tabla .= $texto_adicional;
				
				

			$tabla .= "</tr>";
		}
		$tabla .= "</table><br>";

		$cupos_programados = $horas_programadas*4;
		$cupos_vacios = $cupos_programados - $sumatoria_asignados;

		$pie  = "<label><strong>CUPOS PROGRAMADOS :</strong> " . $cupos_programados . "</label>&nbsp;&nbsp;&nbsp;";
		$pie .= "<label><strong>CUPOS ASIGNADOS   :</strong> " . $sumatoria_asignados . "</label>&nbsp;&nbsp;&nbsp;";
		$pie .= "<label><strong>CUPOS VACIOS      :</strong> " . $cupos_vacios . "</label>&nbsp;&nbsp;&nbsp;";
		$pie .= "<label><strong>ADICIONALES       :</strong> " . $sumatoria_adicional . "</label><br><br>";

		echo json_encode(array(
			'CABECERA' => $cabecera,
			'TABLA'    => $tabla,
			'PIE'      => $pie
			));
		exit();
	}

	#REPORTE CENTRO DE COSTO DESGREGADO
	if ($_REQUEST["acc"] == "Centro_Costo_x_Id_fi_ff_C") {
		$idcc = $_REQUEST["idcc"];
		
		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechainicio"]);
		$fechainicio = $anio."-".$mes."-".$dia;

		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
		$fechafin = $anio."-".$mes."-".$dia;



		$data_general = Centro_Costo_x_Id_fi_ff_M($idcc,$fechainicio,$fechafin);

		for ($i=0; $i < count($data_general); $i++) { 
			$data_cc[] = array(
				'CODIGO' => $data_general[$i]["Codigo"],
				'NOMBRES' => strtoupper(utf8_decode(utf8_encode($data_general[$i]["Nombre"]))),
				'CANTIDAD' => $data_general[$i]["Cantidad"],
				'SUB TOTAL' => $data_general[$i]["SubTotal"],
				'EXONERACIONES' => $data_general[$i]["Exoneraciones"],
				'TOTAL' => $data_general[$i]["Total"]
				);
		}

		echo json_encode($data_cc);
		exit();
	}

	#REPORTES / DATOS EMERGENCIA
	if ($_REQUEST["acc"] == "Datos_Emergencia") {
		
		#OBTENIENDO EL RANGO DE FECHA
		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechainicio"]);
		$fechainicio = $anio."-".$mes."-".$dia;

		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
		$fechafin = $anio."-".$mes."-".$dia;

		#TIPO DE REPORTE
		$reporte = $_REQUEST["reporte"];

		switch ($reporte) {
			
			#DATA GENERAL
			case 'RDG':
				$data_general = Data_General_Emergencia_M($fechainicio,$fechafin);
				if (!$data_general) {
					echo json_encode('R_1'); 
					exit();
				}
				else
				{	
					$cantidad 	= 	count($data_general);
					for ($i=0; $i < $cantidad; $i++) { 
						$fecha_temp = substr($data_general[$i]["FECHA"],0,10);
						list($anio,$mes,$dia) = explode("-",$fecha_temp);
						$fecha = $dia."/".$mes."/".$anio;
						$data_general_tb[] = array(
							'IDATENCION'   					=>	$data_general[$i]["IDATENCION"],
							'HISTORIA_CLINICA'				=>	$data_general[$i]["HISTORIA_CLINICA"],
							'EDAD'							=>	$data_general[$i]["EDAD"],
							'TIPO'							=>	utf8_decode(utf8_encode($data_general[$i]["TIPO"])),
							'EDADHIS'						=>	$data_general[$i]["EDADHIS"],
							'SUSALUD'						=>	$data_general[$i]["EDAD SUSALUD"],
							'SEXO'							=>	$data_general[$i]["SEXO"],
							'PACIENTE'						=>	$data_general[$i]["PACIENTE"],
							'DNI'							=>	$data_general[$i]["DNI"],
							'DISTRITO'						=>	$data_general[$i]["DISTRITO"],
							'IDDISTRITO'					=>	$data_general[$i]["IDDISTRITO"],
							'FECHA'							=>	$fecha,
							'INGRESO'						=>	$data_general[$i]["INGRESO"],
							'SALIDA'						=>	$data_general[$i]["SALIDA"],
							'SEGURO'						=>	$data_general[$i]["SEGURO"],
							'TOPICO'						=>	$data_general[$i]["TOPICO"],
							'SERVICIO_EGRESO'				=>	$data_general[$i]["SERVICIO_EGRESO"],
							'CAUSA_EXTERNA'					=>	$data_general[$i]["CAUSA_EXTERNA"],
							'PRIORIDAD DE ATENCION'			=>	$data_general[$i]["PRIORIDAD DE ATENCION"],
							'DIA_DIG'						=>	$data_general[$i]["DIA_DIG"],
							'HORA_DIG'						=>	$data_general[$i]["HORA_DIG"],
							'DESCRIPCION MOTIVO INGRESO'	=>	$data_general[$i]["DESCRIPCION MOTIVO INGRESO"],
							'DESCRIPCION DE PRIORIDAD'		=>	$data_general[$i]["DESCRIPCION DE PRIORIDAD"],
							'DESCRIPCION_CAUSA_EXTERNA'		=>	$data_general[$i]["DESCRIPCION_CAUSA_EXTERNA"],
							'DX1'							=>	$data_general[$i]["DX1"],
							'DIAGNOSTICO1'					=>	$data_general[$i]["DIAGNOSTICO1"],
							'DX2'							=>	$data_general[$i]["DX2"],
							'DIAGNOSTICO2'					=>	$data_general[$i]["DIAGNOSTICO2"],
							'DX3'							=>	$data_general[$i]["DX3"],
							'DIAGNOSTICO'					=>	$data_general[$i]["DIAGNOSTICO"]
							);
					}
					echo json_encode($data_general_tb);
					exit();
				}
				
				break;

			#ACCIDENTES DE TRANSITO
			case 'RAT':
				# code...
				break;

			#AGRESIONES
			case 'RA':
				# code...
				break;

			#POR TOPICOS
			case 'RPT':
				# code...
				break;
		}

	}

	#FUNCIONES GENERALES
	function Reporte_x_Meses($data_citas,$data_horas) {
		
		#CANTIDAD DE CITAS POR MESES
		for ($i=0; $i < count($data_citas); $i++) { 
			$enero_citas 		= $enero_citas 		+ $data_citas[$i][1];
			$febrero_citas 		= $febrero_citas 	+ $data_citas[$i][2];
			$marzo_citas 		= $marzo_citas 		+ $data_citas[$i][3];
			$abril_citas 		= $abril_citas 		+ $data_citas[$i][4];
			$mayo_citas 		= $mayo_citas 		+ $data_citas[$i][5];
			$junio_citas 		= $junio_citas 		+ $data_citas[$i][6];
			$julio_citas 		= $julio_citas 		+ $data_citas[$i][7];
			$agosto_citas 		= $agosto_citas 	+ $data_citas[$i][8];
			$setiembre_citas 	= $setiembre_citas 	+ $data_citas[$i][9];
			$obtubre_citas 		= $obtubre_citas 	+ $data_citas[$i][10];
			$noviembre_citas 	= $noviembre_citas 	+ $data_citas[$i][11];
			$diciembre_citas 	= $diciembre_citas 	+ $data_citas[$i][12];
		}

		#CANTIDAD DE HORAS PROGRAMADAS POR MESES
		for ($i=0; $i < count($data_horas); $i++) { 
			$enero_horas 		= $enero_horas 		+ $data_horas[$i][1];
			$febrero_horas 		= $febrero_horas 	+ $data_horas[$i][2];
			$marzo_horas 		= $marzo_horas 		+ $data_horas[$i][3];
			$abril_horas 		= $abril_horas 		+ $data_horas[$i][4];
			$mayo_horas 		= $mayo_horas 		+ $data_horas[$i][5];
			$junio_horas 		= $junio_horas 		+ $data_horas[$i][6];
			$julio_horas 		= $julio_horas 		+ $data_horas[$i][7];
			$agosto_horas 		= $agosto_horas 	+ $data_horas[$i][8];
			$setiembre_horas 	= $setiembre_horas 	+ $data_horas[$i][9];
			$obtubre_horas 		= $obtubre_horas 	+ $data_horas[$i][10];
			$noviembre_horas 	= $noviembre_horas 	+ $data_horas[$i][11];
			$diciembre_horas 	= $diciembre_horas 	+ $data_horas[$i][12];
		}

		#INDICADORES GENERALES POR MES
		if ($enero_horas == 0) { $ind_enero = 0;}
		else {$ind_enero	= ($enero_citas / $enero_horas);}

		if ($febrero_horas == 0) { $ind_febrero = 0;}
		else {$ind_febrero	= ($febrero_citas / $febrero_horas);}

		if ($marzo_horas == 0) { $ind_marzo = 0;}
		else {$ind_marzo	= ($marzo_citas / $marzo_horas);}

		if ($abril_horas == 0) { $ind_abril = 0;}
		else {$ind_abril	= ($abril_citas / $abril_horas);}

		if ($mayo_horas == 0) { $ind_mayo = 0;}
		else {$ind_mayo	= ($mayo_citas / $mayo_horas);}

		if ($junio_horas == 0) { $ind_junio = 0;}
		else {$ind_junio	= ($junio_citas / $junio_horas);}

		if ($julio_horas == 0) { $ind_julio = 0;}
		else {$ind_julio	= ($julio_citas / $julio_horas);}

		if ($agosto_horas == 0) { $ind_agosto = 0;}
		else {$ind_agosto	= ($agosto_citas / $agosto_horas);}

		if ($setiembre_horas == 0) { $ind_setiembre = 0;}
		else {$ind_setiembre	= ($setiembre_citas / $setiembre_horas);}

		if ($obtubre_horas == 0) { $ind_obtubre = 0;}
		else {$ind_obtubre	= ($obtubre_citas / $obtubre_horas);}
				
		if ($noviembre_horas == 0) { $ind_noviembre = 0;}
		else {$ind_noviembre	= ($noviembre_citas / $noviembre_horas);}
		
		if ($diciembre_horas == 0) { $ind_diciembre = 0;}
		else {$ind_diciembre = ($diciembre_citas / $diciembre_horas);}
		

		$indicadores = array(
			'ENERO'			=> $ind_enero, 
			'FEBRERO'		=> $ind_febrero,
			'MARZO'			=> $ind_marzo,
			'ABRIL'			=> $ind_abril,
			'MAYO'			=> $ind_mayo,
			'JUNIO'			=> $ind_junio,
			'JULIO'			=> $ind_julio,
			'AGOSTO'		=> $ind_agosto,
			'SETIEMBRE'		=> $ind_setiembre,
			'OCTUBRE'		=> $ind_obtubre,
			'NOVIEMBRE'		=> $ind_noviembre,
			'DICIEMBRE'		=> $ind_diciembre
			);

		return $indicadores;
	}

	function MostrarEspecialidadxDepartamento($id_dpto) {
		$data_horas_temp = Mostrar_Horas_x_Medico_M();
		for ($i=0; $i < count($data_horas_temp); $i++) { 
			$especialidades_temp[] = array(
									'IDDPTOP'		 => $data_horas_temp[$i]["IDDPTO"],
									'ESPECIALIDADES' => utf8_decode(strtoupper(utf8_encode($data_horas_temp[$i]["ESPECIALIDAD"]))),
									'IDESPECIALIDAD' => $data_horas_temp[$i]["IDESP"]
									);
		}
		//print_r($especialidades_temp);exit();
		$especialidades_temp  = array_values(array_unique2($especialidades_temp));
		
		for ($i=0; $i < count($especialidades_temp); $i++) { 
			if($id_dpto == $especialidades_temp[$i]["IDDPTOP"]){
				$especialidades[]  	= array(
									'id' 	=> $especialidades_temp[$i]["IDESPECIALIDAD"],
									'text'	=> $especialidades_temp[$i]["ESPECIALIDADES"]
									);
			}
		}

		return $especialidades;
	}

	function array_unique2($array) {
		$container = array();
		$i = 0;
		foreach ($array as $a=>$b)
		if (!in_array($b,$container)){
			$container[$i]=$b;
			$i++;
		}
		return $container;
	} 
	
	function Cantidad_x_Mes($data_temp) {

		#CANTIDAD DE temp POR MESES
		for ($i=0; $i < count($data_temp); $i++) { 
			$enero_temp 		= $enero_temp 		+ $data_temp[$i][1];
			$febrero_temp 		= $febrero_temp 	+ $data_temp[$i][2];
			$marzo_temp 		= $marzo_temp 		+ $data_temp[$i][3];
			$abril_temp 		= $abril_temp 		+ $data_temp[$i][4];
			$mayo_temp 			= $mayo_temp 		+ $data_temp[$i][5];
			$junio_temp 		= $junio_temp 		+ $data_temp[$i][6];
			$julio_temp 		= $julio_temp 		+ $data_temp[$i][7];
			$agosto_temp 		= $agosto_temp 		+ $data_temp[$i][8];
			$setiembre_temp 	= $setiembre_temp 	+ $data_temp[$i][9];
			$obtubre_temp 		= $obtubre_temp 	+ $data_temp[$i][10];
			$noviembre_temp 	= $noviembre_temp 	+ $data_temp[$i][11];
			$diciembre_temp 	= $diciembre_temp 	+ $data_temp[$i][12];
		}

		$temporal = array(
			'ENERO'			=> $enero_temp, 
			'FEBRERO'		=> $febrero_temp,
			'MARZO'			=> $marzo_temp,
			'ABRIL'			=> $abril_temp,
			'MAYO'			=> $mayo_temp,
			'JUNIO'			=> $junio_temp,
			'JULIO'			=> $julio_temp,
			'AGOSTO'		=> $agosto_temp,
			'SETIEMBRE'		=> $setiembre_temp,
			'OCTUBRE'		=> $obtubre_temp,
			'NOVIEMBRE'		=> $noviembre_temp,
			'DICIEMBRE'		=> $diciembre_temp
			);

		return $temporal;
	}

	#ULTIMO DIA DEL MES
  	function Ultima_Dia_Mes($month,$year) { 
    	//$year = date('Y');
      	$day = date("d", mktime(0,0,0, $month+1, 0, $year)); 
      	return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
  	};
 
 	#PRIMER DIA DEL MES
  	function Primer_Dia_Mes($month,$year) {
     	//$year = date('Y');
      	return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
  	}

  	#CANTIDAD DE DIAS EN UN MES
  	function Cantidad_Dias_Mes($Month, $Year)
	{
	    if( is_callable("cal_days_in_month"))
	   	{
			return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
	   	}
	   	
	   	else
	   	{
	    	return date("d",mktime(0,0,0,$Month+1,0,$Year));
	   	}
	}

	#NOMBRE DE MES
	function ObtenerNombreMes($idmes){
		$meses = array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SETIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
		if ($idmes == 0) {
			return " ";
		}
		else {
			return $meses[$idmes-1];
		}
	}

	#EMERGENCIA POR TOPICOS DERIVADOS - REPORTES DE EMERGENCIA
	function EmergenciaxTopicosC($fechainicio,$fechafin)
	{
		$data_topicos 	= Data_Topicos_M($fechainicio,$fechafin);
		$contador 		= count($data_topicos);
		
		#for ($i=0; $i < $contador; $i++) { 
		#	echo '<td>'.$data_topicos[$i][0].'</td>';
		#}
		#echo '</tr>';
		return $data_topicos;
	}

 ?>