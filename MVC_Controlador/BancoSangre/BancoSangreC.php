<?php 
//ini_set('error_reporting',0);//para xamp
  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 0);
include('../../MVC_Modelo/SistemaM.php');
include('../../MVC_Modelo/BancoSangreM.php');

include('../../MVC_Modelo/FacturacionM.php');
//include('../../MVC_Modelo/MaestrosM.php');
include('../../MVC_Complemento/librerias/Funciones.php');
date_default_timezone_set('America/Bogota');

/***************************/
/*** Variables Glovales ***/
/**************************/
 $ListarFechaServidor=RetornaFechaServidorSQL_M();
 $FechaServidor = $ListarFechaServidor[0][0];
 
 //INICIO MAESTROS
 
   /////INICIO COMBO DEPARTAMENTOS-PROVINCIAS-DISTRITOS
   if ($_REQUEST["acc"] == 'MostrarDepartamentoCombo') {
		$data = DepartamentosSeleccionarTodos();

		for ($i=0; $i < count($data); $i++) { 
			$departamentos[$i] = array(
				'id'	=>	$data[$i]["IdDepartamento"],
				'text'	=>	$data[$i]["Nombre"]
				);
		}
		echo json_encode($departamentos);
		exit();
	}

	if ($_REQUEST["acc"] == 'MostrarProvinciasCombo') {
		$idDepartamento = $_REQUEST["idDepartamento"];
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; 
		$data = ProvinciasSeleccionarPorDepartamento($idDepartamento,$texto);

		for ($i=0; $i < count($data); $i++) { 
			$provinvias[$i] = array(
				'IdProvincia'	=>	$data[$i]["IdProvincia"],
				'NomProvincia'	=>	(mb_strtoupper($data[$i]["Nombre"]))
				);
		}
		echo json_encode($provinvias);
		exit();
	}

	if ($_REQUEST["acc"] == 'MostrarDistritosCombo') {
		$idProvincias = $_REQUEST["idProvincias"];
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; 
		$data = DistritosSeleccionarPorProvincia($idProvincias,$texto);

		for ($i=0; $i < count($data); $i++) { 
			$distritos[$i] = array(
				'IdDistrito'	=>	$data[$i]["IdDistrito"],
				'NomDistrito'	=>	(mb_strtoupper($data[$i]["Nombre"]))
				);
		}
		echo json_encode($distritos);
		exit();
	}
	
	if ($_REQUEST["acc"] == 'MostrarCentroPobladoCombo') { 
		$idDistritos = $_REQUEST["idDistritos"];		
		//$texto =utf8_decode($_REQUEST["q"]);	
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; 		
		$data = CentroPobladoSeleccionarPorDistrito($idDistritos,$texto);

		for ($i=0; $i < count($data); $i++) { 
			$distritos[$i] = array(
				'IdCentroPoblado'	=>	$data[$i]["IdCentroPoblado"],
				'NomCentroPoblado'	=>	(mb_strtoupper($data[$i]["Nombre"]))
				);
		}
		echo json_encode($distritos);
		exit();
	}
	
	/////FIN COMBO DEPARTAMENTOS-PROVINCIAS-DISTRITOS
	
 //FIN MAESTROS 
 
if($_REQUEST["acc"] == "RecepcionPostulante") 
{	
	include('../../MVC_Vista/BancoSangre/RegPostulante.php');	
}

function BuscarPostulantesRegistrados($data){  
   /* //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();
    
   //recuperando el valor del DNI	
   //$valor=str_pad($_REQUEST["valor"], 8, "0", STR_PAD_LEFT); 
   $valor=$_REQUEST["valor"]; 
   
   //METODO buscar postulantes registrados en el sistema
   $data=SIGESA_BuscarpostulanteM($valor,'NroDocumento');
   */      
	for ($i=0; $i < count($data); $i++) {
	
		$IdDocIdentidad=$data[$i]["IdDocIdentidad"];
		//$NroDocumento=str_pad($data[$i]["NroDocumento"], 8, "0", STR_PAD_LEFT);
		$NroDocumento=$data[$i]["NroDocumento"];	
		$ApellidoPaterno=$data[$i]["ApellidoPaterno"];
		$ApellidoMaterno=$data[$i]["ApellidoMaterno"];
		$PrimerNombre=$data[$i]["PrimerNombre"];
		$SegundoNombre=$data[$i]["SegundoNombre"];
		$TercerNombre=$data[$i]["TercerNombre"];
		
		$FechaNacimientox = time() - strtotime($data[$i]['FechaNacimiento']);
		$edad = floor((($FechaNacimientox / 3600) / 24) / 360);	
		
		$FechaNacimientoPostulante = trim($data[$i]['FechaNacimiento']);	
		if($FechaNacimientoPostulante!=	NULL){
			$FechaNacimientoPostulante=vfecha(substr($data[$i]['FechaNacimiento'],0,10));
		}else{
			$FechaNacimientoPostulante="";
		}
		
		$IdTipoSexo=$data[$i]["IdTipoSexo"];
		$IdEstadoCivil=$data[$i]["IdEstadoCivil"];
		$IdTipoOcupacion=$data[$i]["IdTipoOcupacion"];
		$Telefono=$data[$i]["Telefono"];
		$IdGrupoSanguineo=$data[$i]["IdGrupoSanguineo"];
		$Email=$data[$i]["Email"]; 
	  
		$IdPaisDomicilio=$data[$i]["IdPaisDomicilio"];	
		$IdPaisProcedencia=$data[$i]["IdPaisProcedencia"];	
		$IdPaisNacimiento=$data[$i]["IdPaisNacimiento"];		  
		$IdDepartamentoProcedencia=$data[$i]["IdDepartamentoProcedencia"];	
		$IdDepartamentoDomicilio=$data[$i]["IdDepartamentoDomicilio"];	
		$IdDepartamentoNacimiento=$data[$i]["IdDepartamentoNacimiento"];	
		$IdProvinciaProcedencia=$data[$i]["IdProvinciaProcedencia"];	
		$IdProvinciaDomicilio=$data[$i]["IdProvinciaDomicilio"];	
		$IdProvinciaNacimiento=$data[$i]["IdProvinciaNacimiento"];	  
		$IdDistritoProcedencia=$data[$i]["IdDistritoProcedencia"];	
		$IdDistritoDomicilio=$data[$i]["IdDistritoDomicilio"];	
		$IdDistritoNacimiento=$data[$i]["IdDistritoNacimiento"];	
		$IdCentroPobladoNacimiento=$data[$i]["IdCentroPobladoNacimiento"];	
		$IdCentroPobladoDomicilio=$data[$i]["IdCentroPobladoDomicilio"];	
		$IdCentroPobladoProcedencia=$data[$i]["IdCentroPobladoProcedencia"];	  
		$DireccionDomicilio=$data[$i]["DireccionDomicilio"];	
		
		$IdTipoDonacion=$data[$i]["IdTipoDonacion"];	
		$IdTipoDonante=$data[$i]["IdTipoDonante"];	
		$IdTipoRelacion=$data[$i]["IdTipoRelacion"];		
		$IdCondicionDonante=$data[$i]["IdCondicionDonante"];	
		$Observaciones=$data[$i]["Observaciones"];		
		
		if($data[$i]["FechaDonacion"]!=NULL){
			$FechaDonacion=vfecha(substr($data[$i]["FechaDonacion"],0,10)); //01/04/2017
			$diasdif=compararFechas (date('d/m/Y'),$FechaDonacion); //numero de dias transcurridos
		}else{
			$FechaDonacion='';
			$diasdif='';
		}
		
		if($data[$i]["FechaMovi"]!=NULL){
			$FechaMovi=vfecha(substr($data[$i]["FechaMovi"],0,10)); //01/04/2017
			$HoraMovi=substr($data[$i]["FechaMovi"],11,8);
		}else{
			$FechaMovi='';
			$HoraMovi='';
		}		
		 
			//OBTENER DATOS PARAMETROS
			$TIMIN=SIGESA_ListarParametros_M('TIMIN');
			$TiempoMinimoDonacion=$TIMIN[0]["Valor1"]; //60 dias OK														
			$CAMAXV=SIGESA_ListarParametros_M('CAMAXV');
			$CantidadMaximaDonacionesVaron=$CAMAXV[0]["Valor1"]; //4 veces						
			$CAMAXM=SIGESA_ListarParametros_M('CAMAXM');
			$CantidadMaximaDonacionesMujer=$CAMAXM[0]["Valor1"]; //3 veces
			
			$ODonacionesEnUltimoAnno=ObtenerCantidadDonacionesEnUltimoAnno_M($data[$i]["IdPostulante"]);	
			$DonacionesEnUltimoAnno=$ODonacionesEnUltimoAnno[0]["cantidad"];				
			
			$Postulante=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].' '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];	

			//Agregado el 13-06-2018			 			
		 	$BuscarDonante=SIGESA_BSD_ListarDonantesEstadoDefinitivoM($NroDocumento);
		 	$EstadoDefinitivo=$BuscarDonante[0]["EstadoDefinitivo"]; //APTO, NO APTO						
			 
			if($data[$i]["EstadoAptoTamizaje"]=='' && $data[$i]["DesMotivoElimMuestra"]=='ZONA GRIS'){ //si el postulante tiene zona gris 
				$resultadodetallado_json=array(
					'IdMensaje'		 	=> 'PostulanteGris',
					'Mensaje'		 	=> 'Avisar al Médico, Zona Gris en donación Previa'		
				); 
				 
			}else if($data[$i]["EstadoAptoTamizaje"]=='0' && $EstadoDefinitivo=='NO APTO'){ //si el postulante es REACTIVO en el tamizaje Y $EstadoDefinitivo=='NO APTO' ES NO APTO DEFINITIVO		
					$resultadodetallado_json=array(
						'IdMensaje'		 	=> 'PostulanteReactivo',
						'Mensaje'		 	=> 'No Apto Definitivo, consultar donaciones previas y avisar al Médico'		
					); 				
				 
			}else if($diasdif!='' && $diasdif<=$TiempoMinimoDonacion && $TIMIN!=NULL){ //si donó antes o en 60 dias (2 MESES)
				$resultadodetallado_json=array(
					'IdMensaje'		 	=> 'Postulantedonoantes',
					'Mensaje'		 	=> $Postulante.' donó antes de '.$TiempoMinimoDonacion.' dias. El '.vfecha(substr($data[$i]["FechaDonacion"],0,10))		
				); 
				 
			}else if($DonacionesEnUltimoAnno!=NULL && $DonacionesEnUltimoAnno>$CantidadMaximaDonacionesVaron && $IdTipoSexo=='1'){ //si es VARON
				$resultadodetallado_json=array(
					'IdMensaje'		 	=> 'donoantesAnnoVaron',
					'Mensaje'		 	=> $Postulante.' donó más de '.$CantidadMaximaDonacionesVaron.' veces el último año'		
				); 
				 
			}else if($DonacionesEnUltimoAnno!=NULL && $DonacionesEnUltimoAnno>$CantidadMaximaDonacionesMujer && $IdTipoSexo=='2'){ //si es MUJER
				$resultadodetallado_json=array(
					'IdMensaje'		 	=> 'donoantesAnnoMUJER',
					'Mensaje'		 	=> $Postulante.' donó más de '.$CantidadMaximaDonacionesMujer.' veces el último año'		
				); 
				 
			}else if($data[$i]["EstadoApto"]=='0'){ //POSTULANTE NO APTO INDEFINIDAMENTE
				$motivo=$data[$i]["MotivoRechazo"]; 
				if($motivo==NULL){$motivo='Consumir '.$data[$i]["Medicacion"];}								 
				$resultadodetallado_json=array(
					'IdMensaje'		 	=> 'PostulanteNuncaPodraDonar',
					'Mensaje'		 	=> 'El Postulante está NO APTO por '.$motivo
				); 
			 
			}else if($data[$i]["EstadoMov"]=='1' ){ //EstadoApto Tiene una Recepcion Pendiente
				$resultadodetallado_json=array(
					'IdMensaje'		 	=> 'Recepcionpendiente'						
				);				 
			 
			}else{			
			$resultadodetallado_json = array(
			'IdMensaje'		 	=> '',
			'IdDocIdentidad'		 	=> $IdDocIdentidad,			
			'NroDocumento' 			=> $NroDocumento,
			'CodigoPostulante' 	=> $data[$i]["CodigoPostulante"],
			'IdPostulante' 	=> $data[$i]["IdPostulante"],
			
			'ApellidoPaterno'	 			=> $ApellidoPaterno,
			'ApellidoMaterno'	 			=> $ApellidoMaterno,
			'PrimerNombre'	 			=> $PrimerNombre,
			'SegundoNombre'	 			=> $SegundoNombre,
			'TercerNombre'	 			=> $TercerNombre,			
			'FechaNacimiento' 		=> $FechaNacimientoPostulante,
			'Edad' 					=> $edad,			
			'IdTipoSexo' 			=> $IdTipoSexo,				
			'IdEstadoCivil'	 			=> $IdEstadoCivil,
			'IdTipoOcupacion'	 		=> $IdTipoOcupacion,			
			'Telefono' 	=> $Telefono,
			'IdGrupoSanguineo' 	=> $IdGrupoSanguineo,
			'Email' 	=> $Email,
			
			'IdPaisDomicilio' 	=> $IdPaisDomicilio,
			'IdPaisProcedencia' 	=> $IdPaisProcedencia,
			'IdPaisNacimiento' 	=> $IdPaisNacimiento,
			'IdDepP' 	=> $IdDepartamentoProcedencia,
			'IdDepD' 	=> $IdDepartamentoDomicilio,
			'IdDepN' 	=> $IdDepartamentoNacimiento,
			'IdProvP' 	=> $IdProvinciaProcedencia,
			'IdProvD' 	=> $IdProvinciaDomicilio,
			'IdProvN' 	=> $IdProvinciaNacimiento,
			'IdDistritoProcedencia' 	=> $IdDistritoProcedencia,
			'IdDistritoDomicilio' 	=> $IdDistritoDomicilio,
			'IdDistritoNacimiento' 	=> $IdDistritoNacimiento,			
			'IdCentroPobladoNacimiento' 	=> $IdCentroPobladoNacimiento,
			'IdCentroPobladoDomicilio' 	=> $IdCentroPobladoDomicilio,
			'IdCentroPobladoProcedencia' 	=> $IdCentroPobladoProcedencia,
			'DireccionDomicilio' 	=> $DireccionDomicilio,			
			'IdTipoDonacion' 	=> $IdTipoDonacion,
			'IdTipoDonante' 	=> $IdTipoDonante,
			'IdTipoRelacion' 	=> $IdTipoRelacion,
			'IdCondicionDonante' 	=> $IdCondicionDonante,
						
			'Observaciones' 	=> $Observaciones,
			'FechaDonacion' 	=> $FechaDonacion,
			'FechaMovi' 	=> $FechaMovi,
			'HoraMovi' 	=> $HoraMovi,
			'EstadoApto' 	=> $data[$i]["EstadoApto"]
			);
		}
	}//END FOR	
		echo json_encode($resultadodetallado_json); 
		exit(); 	
}
///
 
 if ($_REQUEST["acc"] == 'BuscarPostulantesPorDNI') { 
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();
    
   //recuperando el valor del DNI	
   //$valor=str_pad($_REQUEST["valor"], 8, "0", STR_PAD_LEFT); 
   $valor=$_REQUEST["valor"];  
   
   //METODO buscar postulantes registrados en el sistema
   $data=SIGESA_BuscarpostulanteM($valor,'NroDocumento');
   
   if( ($data!=NULL && $data[0]['FechaNacimiento']!=NULL) || ($data[0]['EstadoAptoTamizaje']=='0') ){ //si existe postulante con el DNI en la tabla Postulantes (NOTA: los que no tienen FechaNacimiento son recepciones registrados manualmente)
		 BuscarPostulantesRegistrados($data);   
			
   }else{    //RENIEC
	    //DNI		 
		$strDNIAuto="";//NO		
		//header('Location: http://wsminsa.minsa.gob.pe/WSRENIEC_DNI/SerDNI.asmx/GetReniec?strDNIAuto='.$strDNIAuto.'.&strDNICon='.$valor); 		
	    $xmlstr = 'http://wsminsa.minsa.gob.pe/WSRENIEC_DNI/SerDNI.asmx/GetReniec?strDNIAuto='.$strDNIAuto.'.&strDNICon='.$valor; 
		
		//try{
		   $articulos = simplexml_load_string(file_get_contents($xmlstr));
		   //print_r($articulos);
		   //echo $articulos->string[1];
		   $arrayap=explode(':', ($articulos->string[0]));
		   $codigo=$arrayap[0];
		   
		   $arrayap=explode(':', ($articulos->string[1]));
		   $ApellidoPaterno=$arrayap[0];
		   
		   $arrayam=explode(':', ($articulos->string[2]));
		   $ApellidoMaterno=$arrayam[0];
		   
		   $arraynom=explode(':', ($articulos->string[3]));
		   $Nombres=$arraynom[0];
		   
		   $arrayn=explode(' ', $Nombres);
		   $cantn=count($arrayn);
		   if($cantn=='1'){
			    $PrimerNombre=$arrayn[0];
		   		$SegundoNombre='';
		   		$TercerNombre='';
		   }else if($cantn=='2'){		   
		   		$PrimerNombre=$arrayn[0];
		  		$SegundoNombre=$arrayn[1];
		  		$TercerNombre='';
		   }else if($cantn=='3'){
		   		$PrimerNombre=$arrayn[0];
		   		$SegundoNombre=$arrayn[1];
		   		$TercerNombre=$arrayn[2];			   
		   }		   
		  	   
		   $arrayidp=explode(':', ($articulos->string[5]));
		   $IdPaisDomicilio=$arrayidp[0];
		   
		   $arrayp=explode(':', ($articulos->string[11]));
		   $NomPaisDomicilio=$arrayp[0];
		   
		   $ObtenerDatosP=ObtenerDatosPais_M($NomPaisDomicilio);
		   		   
		   if($IdPaisDomicilio=='33'){
			   	$IdPaisDomicilio='166';	
				$NomPaisDomicilio=$arrayp[0];	   
		   }else if($ObtenerDatosP!=NULL){
			    $IdPaisDomicilio=$ObtenerDatosP[0]["IdPais"];
				$NomPaisDomicilio=$arrayp[0];
		   }else{
			    $IdPaisDomicilio='0';
				$NomPaisDomicilio="";
		   }
		   
		   $arrayiddep=explode(':', ($articulos->string[6]));
		   $IdDepD=$arrayiddep[0];
		   $arrayidpr=explode(':', ($articulos->string[7]));
		   $IdProvD=$arrayidpr[0];
		   $arrayidd=explode(':', ($articulos->string[8]));
		   $IdDistritoDomicilio=$arrayidd[0];
		   $arrayidcp=explode(':', ($articulos->string[9]));
		   $IdCentroPobladoDomicilio=$arrayidcp[0];
		   ////////////////    
		   
		   //Inicio Obtener Id 
		   $IdReniec=$IdDepD.$IdProvD.$IdDistritoDomicilio;
		   if($IdDistritoDomicilio!="00" && $IdReniec!=""){ //$IdReniec!=""
			   $ObtenerIdDepaReniec=ObtenerIdDepaReniec_M($IdReniec);
			   $IdDepartamentoD=$ObtenerIdDepaReniec[0]['IdDepartamento'];
			   $arraydep=explode(':', ($articulos->string[12]));
			   $NomDepD=$arraydep[0];
			   
			   $IdProvinciaD=$ObtenerIdDepaReniec[0]['IdProvincia'];
			   $arraypr=explode(':', ($articulos->string[13]));
			   $NomProvD=$arraypr[0];
			   
			   $IdDistritoD=$ObtenerIdDepaReniec[0]['IdDistrito'];
			   $arrayd=explode(':', ($articulos->string[14]));
			   $NomDistritoDomicilio=$arrayd[0];
			   
			   $arraycp=explode(':', ($articulos->string[15]));
			   $NomCentroPobladoDomicilio=$arraycp[0];	
		   }else{
			  $IdDepartamentoD='0'; $NomDepD="";
			  $IdProvinciaD='0'; $NomProvD="";
			  $IdDistritoD='0'; $NomDistritoDomicilio=""; 
			  $NomCentroPobladoDomicilio="";
		   }
		   
		   //FIN Obtener Id 
		   $arraydir=explode(':', ($articulos->string[16]));
		   $DireccionDomicilio=$arraydir[0];		   
		   $arrayids=explode(':', ($articulos->string[17]));
		   $IdTipoSexo=$arrayids[0];
		   $arrayfn=explode(':', ($articulos->string[18])); 
		   $FechaNacimientox=$arrayfn[0];//19920512
		   $FechaNacimiento=substr($FechaNacimientox, 6, 2).'/'.substr($FechaNacimientox, 4, 2).'/'.substr($FechaNacimientox, 0, 4);
		   
		   $NroDocumentoReniec=explode(':', ($articulos->string[21]));
		   $IdDocIdentidad='1';		   
		   	   
		   $resultadodetallado_json= array(
		   				'IdMensaje'		 	=> '',	
		   				'CodigoPostulante' 	=> '',
						'IdPostulante' 	=> '',
											
						'IdDocIdentidad'	=>  $IdDocIdentidad,
						'NroDocumento'	=> $NroDocumentoReniec,						
						'ApellidoPaterno'	=>  $ApellidoPaterno,
						'ApellidoMaterno'	=> $ApellidoMaterno,
						'PrimerNombre'	=> $PrimerNombre,
						'SegundoNombre'	=> $SegundoNombre,
						'TercerNombre'	=> $TercerNombre,
						
						'FechaNacimiento'	=> $FechaNacimiento,
						'IdTipoSexo'	=> $IdTipoSexo,							
						'IdEstadoCivil'	 => '0',
						'IdTipoOcupacion'=> '0',			
						'Telefono' 		 => '',
						'IdGrupoSanguineo' => '0',
						'Email' 	=> '',
						
						'IdPaisDomicilio'	=> $IdPaisDomicilio,
						'IdDepD'	=> $IdDepartamentoD,
						'IdProvD'	=> $IdProvinciaD,
						'IdDistritoDomicilio'	=> $IdDistritoD,
						'IdCentroPobladoDomicilio'	=> $IdCentroPobladoDomicilio,
						'NomPaisDomicilio'	=> $NomPaisDomicilio,
						'NomDepD'	=> $NomDepD,
						'NomProvD'	=> $NomProvD,
						'NomDistritoDomicilio'	=> $NomDistritoDomicilio,
						'NomCentroPobladoDomicilio'	=> $NomCentroPobladoDomicilio,						
						'DireccionDomicilio'	=> $DireccionDomicilio						
																	
			);
			if($resultadodetallado_json!=NULL && $IdReniec!=""){ 
				echo json_encode($resultadodetallado_json);
			}else{
				$resultadodetallado_json = array();
				echo json_encode($resultadodetallado_json);
			}
			//exit();		   
		/*}catch (Exception $e) {
		   //manejamos error de lectura
		   echo 'XML no valido';
		}*/
   } 	
}
 

	
if($_REQUEST["acc"] == "BSD_BuscarPaciente") // BUSCAR: Buscar Pacientes
{  
   $data = array();
   $resultadodetallado_json = array();
   
   $valor=$_REQUEST["valor"];
   $TipoBusqueda=$_REQUEST["TipoBusqueda"];    
   $data=SIGESA_BSD_Buscarpaciente_M($valor,$TipoBusqueda);	
   
   //RECEPTOR REGISTRADO   	
	   //$BuscarReceptor=SIGESA_BSD_BuscarReceptor_M($data[0]["NroHistoriaClinica"],'NroHistoriaClinica');//Obtener IdGrupoSanguineo del RECEPTOR REGISTRADO
	   $BuscarReceptor=SIGESA_BSD_BuscarReceptorFiltro_M($data[0]["IdPaciente"],'IdPaciente');
	   if($BuscarReceptor!=NULL){
		  $IdGrupoSanguineo=$BuscarReceptor[0]["IdGrupoSanguineo"];
	   }else{
		  $IdGrupoSanguineo='0';
	   }	
	//FIN RECEPTOR REGISTRADO
  	if($data!=NULL){
		for ($i=0; $i < count($data); $i++) {		
			
			$Paciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].', '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];		
			$ApellidoPaterno=$data[$i]["ApellidoPaterno"];
			$ApellidoMaterno=$data[$i]["ApellidoMaterno"];
			$PrimerNombre=$data[$i]["PrimerNombre"];
			$SegundoNombre=$data[$i]["SegundoNombre"];
			$TercerNombre=$data[$i]["TercerNombre"];
			
			$FechaNacimientox = time() - strtotime($data[$i]['FechaNacimiento']);
			$edad = floor((($FechaNacimientox / 3600) / 24) / 360);
			
			$Domicilio=$data[$i]["departamentodomicilio"].' - '.$data[$i]["provinciadomicilio"].' - '.$data[$i]["distritodomicilio"];
			$LugarNacim=$data[$i]["departamentonac"].' - '.$data[$i]["provincianac"].' - '.$data[$i]["distritonac"];
			
			if($data[$i]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($data[$i]["IdTipoSexo"]==2){$Sexo='Femenino';}    
	   			
		    $resultadodetallado_json = array(
			'IdPaciente'		 	=> $data[$i]["IdPaciente"],			
			'NroDocumento' 			=> $data[$i]["NroDocumento"],
			'NroHistoria' 	=> $data[$i]["NroHistoriaClinica"],
			'Paciente'	 			=> $Paciente,
			'ApellidoPaterno'	 			=> $ApellidoPaterno,
			'ApellidoMaterno'	 			=> $ApellidoMaterno,
			'PrimerNombre'	 			=> $PrimerNombre,
			'SegundoNombre'	 			=> $SegundoNombre,
			'TercerNombre'	 			=> $TercerNombre,
			
			'FechaNacimiento' 		=> vfecha(substr($data[$i]["FechaNacimiento"],0,10)),
			'IdTipoSexo' 			=> $data[$i]["IdTipoSexo"],
			'Sexo' 			=> $Sexo, 
			'Edad' 					=> $edad,			
			'Domicilio'	 			=> $Domicilio,
			'LugarNacim'	 		=> $LugarNacim,
			'IdGrupoSanguineo' 	=> $IdGrupoSanguineo			
			/*'IdServicio' 	=> $IdServicio,
			'numerocama' 	=> $numerocama,
			'Medico' 	=> $Medico,
			'diagnostico' 	=> $diagnostico*/			
			);
	    }
	}	
	if($resultadodetallado_json!=NULL){ 
		echo json_encode($resultadodetallado_json);
	}else{
		$resultadodetallado_json = array();
		echo json_encode($resultadodetallado_json);
	}
	//exit();
}
	
		
		if($_REQUEST["acc"] == "GuardarRecepcion")
		{  				
			//GUARDAR DATOS DEL POSTULANTE
				//INICIO GENERAR CODIGO
				$data=ListarMaxCodigoPostulanteM();
				for ($i=0; $i < count($data); $i++) {	     
					$maxcodigo=$data[$i]["maxcodigo"]; //000054
				}		
				if($maxcodigo!=NULL){
					//$codigo='P'.date('Y').str_pad($maxcodigo+1, 5, "0", STR_PAD_LEFT);
					//$codigo='P'.str_pad($maxcodigo+1, 6, "0", STR_PAD_LEFT); //PDAC000055
					$codigo='P'.($maxcodigo+1); //P851
				}else{
					//$codigo='P'.date('Y').'00001'; //P201700001
					//$codigo='P'.'000001'; //P000001
					$codigo='P'.'1';
				}
				//FIN GENERAR CODIGO
		
	   			//guardar tabla SIGESA_BSD_Postulante 
				
				//METODO buscar postulantes registrados en el sistema
				$Buscarpostulante = array();
			    $Buscarpostulante=SIGESA_BuscarpostulanteM($_REQUEST['NroDocumento'],'NroDocumento');
				if($Buscarpostulante!=""){
					$CodigoPostulanteExiste=$Buscarpostulante[0]["CodigoPostulante"];	
					$IdPostulanteExiste=$Buscarpostulante[0]["IdPostulante"];
				}else if(trim($_REQUEST['IdPostulante'])!=""){ ///
					$CodigoPostulanteExiste=trim($_REQUEST['CodigoPostulante']);	
					$IdPostulanteExiste=trim($_REQUEST['IdPostulante']);
				}else{
					$CodigoPostulanteExiste=$codigo;	
					$IdPostulanteExiste="";
				}			
				
			 	//$IdPostulante='1';//autoincrementar					
				$CodigoPostulante = $CodigoPostulanteExiste; 							 
				$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
				$UsuReg=$_REQUEST['IdEmpleado'];
				$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo'];
				$ApellidoPaterno=trim(mb_strtoupper($_REQUEST['ApellidoPaterno']));
				$ApellidoMaterno=trim(mb_strtoupper($_REQUEST['ApellidoMaterno']));
				$PrimerNombre=trim(mb_strtoupper($_REQUEST['PrimerNombre']));
				$SegundoNombre=trim(mb_strtoupper($_REQUEST['SegundoNombre']));
				$TercerNombre=trim(mb_strtoupper($_REQUEST['TercerNombre']));
				$FechaNacimiento=gfecha($_REQUEST['FechaNacimiento']);			
				//$NroDocumento=str_pad($_REQUEST["NroDocumento"], 8, "0", STR_PAD_LEFT);
				$NroDocumento=$_REQUEST["NroDocumento"];   				
				$Telefono=$_REQUEST['Telefono'];
				$DireccionDomicilio=trim(mb_strtoupper($_REQUEST['DireccionDomicilio']));				
				$IdTipoSexo=$_REQUEST['IdTipoSexo'];
				$IdEstadoCivil=$_REQUEST['IdEstadoCivil'];
				$IdDocIdentidad=$_REQUEST['IdDocIdentidad'];
				$IdTipoOcupacion=$_REQUEST['IdTipoOcupacion'];				
				$IdCentroPobladoNacimiento = isset($_REQUEST['IdCentroPobladoNacimiento']) ? $_REQUEST['IdCentroPobladoNacimiento'] : 0;				
				$IdCentroPobladoDomicilio = isset($_REQUEST['IdCentroPobladoDomicilio']) ? $_REQUEST['IdCentroPobladoDomicilio'] : 0;				
				$IdCentroPobladoProcedencia = isset($_REQUEST['IdCentroPobladoProcedencia']) ? $_REQUEST['IdCentroPobladoProcedencia'] : 0;				
				$Observacion='';
				$IdPaisDomicilio=$_REQUEST['IdPaisDomicilio'];
				$IdPaisProcedencia=$_REQUEST['IdPaisProcedencia'];
				$IdPaisNacimiento=$_REQUEST['IdPaisNacimiento'];				
				$IdDistritoProcedencia = isset($_REQUEST['IdDistritoProcedencia']) ? $_REQUEST['IdDistritoProcedencia'] : 0;				
				$IdDistritoDomicilio = isset($_REQUEST['IdDistritoDomicilio']) ? $_REQUEST['IdDistritoDomicilio'] : 0;				
				$IdDistritoNacimiento = isset($_REQUEST['IdDistritoNacimiento']) ? $_REQUEST['IdDistritoNacimiento'] : 0;				
				$Email=$_REQUEST['Email'];
				
				$AutoIdPostulante=GuardarPostulanteM($CodigoPostulante,$FechaReg,$UsuReg,$IdGrupoSanguineo,$ApellidoPaterno,$ApellidoMaterno,$PrimerNombre,$SegundoNombre,$TercerNombre,$FechaNacimiento,$NroDocumento,$Telefono,$DireccionDomicilio,$IdTipoSexo,$IdEstadoCivil,$IdDocIdentidad,$IdTipoOcupacion,$IdCentroPobladoNacimiento,$IdCentroPobladoDomicilio,$IdCentroPobladoProcedencia,$Observacion,$IdPaisDomicilio,$IdPaisProcedencia,$IdPaisNacimiento,$IdDistritoProcedencia,$IdDistritoDomicilio,$IdDistritoNacimiento,$Email);	
				
				$NewIdPostulante = ($IdPostulanteExiste!="") ? $IdPostulanteExiste : $AutoIdPostulante; 
									
			//FIN GUARDAR DATOS DEL POSTULANTE
				  
			//GUARDAR RECEPTOR
			//$IdReceptor='1';//AUTOINCREMENT 			
			//$NroDocumento=str_pad($_REQUEST["NroDocumentoRec"], 8, "0", STR_PAD_LEFT); 
			$NroDocumento=$_REQUEST["NroDocumentoRec"];  			
			$IdPaciente=$_REQUEST['IdPaciente'];			
			$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineoRec'];	
			$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
			$UsuReg=$_REQUEST['IdEmpleado'];	
			
			if($_REQUEST['NroHistoria']==""){
				$NroHistoria=0;
			}else{
				$NroHistoria=$_REQUEST['NroHistoria'];
			}	

			if($_REQUEST['IdPaciente']==""){
				$IdPaciente=0;
			}else{
				$IdPaciente=$_REQUEST['IdPaciente'];
			}			
			
			//$BuscarReceptor=SIGESA_BSD_BuscarReceptor_M($NroHistoria,'NroHistoriaClinica');
			$BuscarReceptor=SIGESA_BSD_BuscarReceptorFiltro_M($IdPaciente,'IdPaciente');
			if($BuscarReceptor!=NULL){
				$NewIdReceptor=$BuscarReceptor[0]["IdReceptor"];
				GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg);
			}else{
				//$NewIdReceptor=$AutoIdReceptor;
				$NewIdReceptor=GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg);				
			}	
			$GrupoSanguineo=$_REQUEST['GrupoSanguineoRec'];//text A+,A-...
			UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo);		
			//FIN GUARDAR DATOS DEL RECEPTOR	   		
			
			//GUARDAR MOVIMIENTOS			   
			if($NewIdPostulante!=NULL && $NewIdReceptor!=NULL){
				//echo "POSTULANTE Y RECEPTOR REGISTRADO";						
				//$IdMovimiento=$_REQUEST['IdMovimiento'];
				//INICIO GENERAR NroMovimiento
				$ListarMaxNroMovimiento=ListarMaxNroMovimientoM();					     
				$MaxNroMovimiento=$ListarMaxNroMovimiento[0]["maxcodigo"]; //2						
				if($MaxNroMovimiento!=NULL){					
					$NroMovimiento=$MaxNroMovimiento+1; //3
				}else{					
					$NroMovimiento=1; //1
				}
				//FIN GENERAR NroMovimiento
				$Estado='1'; //REGISTRADO PENDIENTE PARA ANALISIS PRE-DONACION
				$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
				$UsuReg=$_REQUEST['IdEmpleado'];
				$FechaMovi=gfecha($_REQUEST['FechaMovi']).' '.$_REQUEST['HoraMovi'];
				$NroDonacion=NULL;
				//$FechaDonacion=NULL;
				$IdReceptor=$NewIdReceptor;
				$IdCondicionDonante=$_REQUEST['IdCondicionDonante'];
				$IdTipoDonacion=$_REQUEST['IdTipoDonacion'];
				$IdTipoDonante=$_REQUEST['IdTipoDonante'];
				$IdPostulante=$NewIdPostulante;
				$IdTipoRelacion=$_REQUEST['IdTipoRelacion'];
				
				$IdHospital=$_REQUEST['IdHospital'];
				$IdServicioHospital=$_REQUEST['IdServicioHospital'];
				//$NroCama=$_REQUEST['NroCama'];			
				//$IdMedico=NULL;
				//$Medico=$_REQUEST['Medico'];
				//$IdDiagnostico=NULL;
				//$CodigoCIE10Diagnostico=$_REQUEST['CodigoCIE10Diagnostico'];				
				//$EstadoAlta = isset($_REQUEST['EstadoAlta']) ? $_REQUEST['EstadoAlta'] : '0'; 							
				$Observaciones=$_REQUEST['Observaciones'];
				
				$NewIdMovimiento=GuardarMovimientosM($Estado, $FechaReg, $UsuReg, $FechaMovi, $NroDonacion, $IdReceptor, $IdCondicionDonante, $IdTipoDonacion, $IdTipoDonante, $IdPostulante, $IdTipoRelacion, $Observaciones, $IdHospital, $IdServicioHospital, $NroMovimiento); //, $NroCama, $IdMedico, $Medico, $IdDiagnostico, $CodigoCIE10Diagnostico, $EstadoAlta,	 	   
				if($NewIdMovimiento!=NULL){
					$mensaje="Recepción registrada correctamente PDAC".$NroMovimiento;
					print "<script>alert('$mensaje')</script>";
				}else{
					$mensaje="Recepción NO REGISTRADA";
					print "<script>alert('$mensaje')</script>";
					//echo "MOVIMIENTOS NO REGISTRADO";
				}
				
			}//FIN GUARDAR MOVIMIENTOS					
				
			include('../../MVC_Vista/BancoSangre/RegPostulante.php');
		}
		
	
if($_REQUEST["acc"] == "ExamenMedicoPostulante") 
{  		
	include('../../MVC_Vista/BancoSangre/EvaluacionPredonacionPostulante.php');
}	

if($_REQUEST["acc"] == "RegEvaluacionPredonacionPostulante") 
{	
	include('../../MVC_Vista/BancoSangre/RegEvaluacionPredonacionPostulante.php');
}

if($_REQUEST["acc"] == "UpdEvaluacionPredonacionPostulante") 
{	
	include('../../MVC_Vista/BancoSangre/UpdEvaluacionPredonacionPostulante.php');
}

if($_REQUEST["acc"] == "UpdPostulante") 
{  	
	include('../../MVC_Vista/BancoSangre/UpdPostulante.php');
}

if($_REQUEST["acc"] == "Tamizaje") 
{ 	
	include('../../MVC_Vista/BancoSangre/Tamizaje.php');
}

if($_REQUEST["acc"] == "RegTamizaje") 
{ 
	include('../../MVC_Vista/BancoSangre/RegTamizaje.php');
}

if($_REQUEST["acc"] == "Extraccion") 
{ 		
	include('../../MVC_Vista/BancoSangre/ExtraccionSangre.php');
}

if($_REQUEST["acc"] == "RegExtraccionSangre") 
{ 	
	//obtener Nro Donación
	/*$ObtenerMaxNroDonacion=ObtenerMaxNroDonacionExtraccion_M();
	if($ObtenerMaxNroDonacion!=NULL && $ObtenerMaxNroDonacion[0]["maxNroDonacion"]!=NULL){
		$NroDonacion=($ObtenerMaxNroDonacion[0]["maxNroDonacion"])+1;
	}else{
		$NroDonacion=substr(date('Y'),2,2).'00001'; //1700001;
	}*/
	include('../../MVC_Vista/BancoSangre/RegExtraccionSangre.php');
}

if($_REQUEST["acc"] == "UpdExtraccionSangre") 
{ 	
	include('../../MVC_Vista/BancoSangre/UpdExtraccionSangre.php');
}

if($_REQUEST["acc"] == "FiltrarPostulante") {
			
		$data = array();
		$DatosPostulantes = array();
		
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; // the request parameter
		if(trim($texto)!="" && is_numeric($texto)){
			$data=SIGESA_BuscarpostulanteFiltroM($texto,'NroDocumento');			
		}else if(trim($texto)!=""){
			$data=SIGESA_BuscarpostulanteFiltroM($texto,'ApellidosNombres');		
		}else{
			/*$mensaje="Ingrese Apellidos y Nombres del Postulante";
			print "<script>alert('$mensaje')</script>";exit();*/
		}
				
		for ($i=0; $i < count($data); $i++) { 
			$NombresPostulante=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].' '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];		
			$DatosPostulantes[$i] = array(
				'CodigoPostulante'	=>	$data[$i]["CodigoPostulante"],
				'ApellidoPaterno'	=>	mb_strtoupper($data[$i]["ApellidoPaterno"]),
				'NombresPostulante'	=>	mb_strtoupper($NombresPostulante),
				'NroDocumento'	=>	$data[$i]["NroDocumento"],
				'GrupoSanguineoPostulante'	=>	$data[$i]["GrupoSanguineoPostulante"]
				);
		}
		
		echo json_encode($DatosPostulantes);
		exit();		
	
}

if($_REQUEST["acc"] == "FiltrarPaciente") {
			
		$data = array();
		$DatosPacientes = array();
		
		/*$texto = isset($_POST['q']) ? $_POST['q'] : ''; // the request parameter
		if(trim($texto)!=""){	   			            
   			$data=SIGESA_BSD_Buscarpaciente_M($texto,'ApellidosNombrespaciente');
		}else{
			//$mensaje="Ingrese Apellidos y Nombres del Paciente";
			//print "<script>alert('$mensaje')</script>";exit();
		}*/
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; // the request parameter
		if(trim($texto)!="" && is_numeric($texto)){
			$data=SIGESA_BSD_Buscarpaciente_M($texto,'NroHistoriaClinica');			
		}else if(trim($texto)!=""){
			$data=SIGESA_BSD_Buscarpaciente_M($texto,'ApellidosNombrespaciente');		
		}else{
			/*$mensaje="Ingrese Apellidos y Nombres del Paciente";
			  print "<script>alert('$mensaje')</script>";exit();*/
		}
				
		for ($i=0; $i < count($data); $i++) { 
			$NombresPaciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].' '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];		
			$DatosPacientes[$i] = array(
				'NroHistoriaClinica'	=>	$data[$i]["NroHistoriaClinica"],
				'ApellidoPaterno'	=>	mb_strtoupper($data[$i]["ApellidoPaterno"]),
				'NombresPaciente'	=>	mb_strtoupper($NombresPaciente),
				'GrupoSanguineo'	=>	$data[$i]["GrupoSanguineo"],
				);
		}
		
		echo json_encode($DatosPacientes);
		exit();		
}

if($_REQUEST["acc"] == "FiltrarReceptor") {
			
		$data = array();
		$DatosPacientes = array();		
		
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; // the request parameter
		if(trim($texto)!="" && is_numeric($texto)){
			$data=SIGESA_BSD_BuscarReceptorFiltro_M($texto,'NroHistoriaClinica');			
		}else if(trim($texto)!=""){
			$data=SIGESA_BSD_BuscarReceptorFiltro_M($texto,'ApellidosNombrespaciente');		
		}else{
			/*$mensaje="Ingrese Apellidos y Nombres del Paciente";
			  print "<script>alert('$mensaje')</script>";exit();*/
		}
				
		for ($i=0; $i < count($data); $i++) { 
			$NombresPaciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].' '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];		
			$DatosPacientes[$i] = array(
				'NroHistoriaClinica'	=>	$data[$i]["NroHistoriaClinica"],
				'ApellidoPaterno'	=>	mb_strtoupper($data[$i]["ApellidoPaterno"]),
				'NombresPaciente'	=>	mb_strtoupper($NombresPaciente),
				'GrupoSanguineo'	=>	$data[$i]["GrupoSanguineoRec"],
				'IdPaciente'	=>	$data[$i]["IdPaciente"],
				'IdTipoSexo'	=>	$data[$i]["IdTipoSexo"],
				'IdGrupoSanguineo' =>	$data[$i]["IdGrupoSanguineo"]
				);
		}
		
		echo json_encode($DatosPacientes);
		exit();		
}

if($_REQUEST["acc"] == "GuardarExamenMedico") {
	//$IdExamenMedico=$_REQUEST['IdExamenMedico'];
	$IdLugarColecta=$_REQUEST['IdLugarColecta'];	
	$FechaEvaluacion=gfecha($_REQUEST['FechaEvaluacion']).' '.$_REQUEST['HoraEvaluacion'];	
	$IdEntrevistador=$_REQUEST['IdEntrevistador'];
	$Peso = ($_REQUEST['Peso']!=NULL) ? ($_REQUEST['Peso']) : 0;
	$Talla = ($_REQUEST['Talla']!=NULL) ? ($_REQUEST['Talla']) : 0;	
	$PresionSistolica = ($_REQUEST['PresionSistolica']!=NULL) ? ($_REQUEST['PresionSistolica']) : 0;	
	$PresionDiastolica = ($_REQUEST['PresionDiastolica']!=NULL) ? ($_REQUEST['PresionDiastolica']) : 0;	
	$Pulso = ($_REQUEST['Pulso']!=NULL) ? ($_REQUEST['Pulso']) : 0;	
	$Temperatura = ($_REQUEST['Temperatura']!=NULL) ? ($_REQUEST['Temperatura']) : 0;	
	$Hemoglobina = ($_REQUEST['Hemoglobina']!=NULL) ? ($_REQUEST['Hemoglobina']) : 0;	
	$Hematocrito = ($_REQUEST['Hematocrito']!=NULL) ? ($_REQUEST['Hematocrito']) : 0;
	
	$Observacion=$_REQUEST['Observacion'];
	$NroCuestionario='';
	$Calificacion=0;
	$IdMedicacion=$_REQUEST['IdMedicacion'];
	$EstadoApto=$_REQUEST['EstadoApto'];
	$IdMotivoRechazo=$_REQUEST['IdMotivoRechazo'];
	$IdMovimiento=$_REQUEST['IdMovimiento'];
	$UsuReg=$_REQUEST['IdEmpleado'];
	$FecReg=$FechaServidor;//date('Y-m-d H:i:s');
	
	$FechaMedicacion = ($_REQUEST['FechaMedicacion']!='') ? gfecha($_POST['FechaMedicacion']) : NULL; //NULL manda vacio
	$FechaEspera = ($_REQUEST['FechaEspera']!='') ? gfecha($_POST['FechaEspera']) : NULL;
	
	$Turno=$_REQUEST['Turno'];
	$MetodologiaHb=$_REQUEST['MetodologiaHb'];
	$MetodologiaHcto=$_REQUEST['MetodologiaHcto'];
	$IdResponsableEv=$_REQUEST['IdResponsableEv'];
	
	$FechaInicioRechazo = ($_REQUEST['FechaInicioRechazo']!=NULL) ? gfecha($_REQUEST['FechaInicioRechazo']) : NULL;	
	$FechaFinRechazo = ($_REQUEST['FechaFinRechazo']!=NULL) ? gfecha($_REQUEST['FechaFinRechazo']) : NULL;

	$newIdExamenMedico=GuardarExamenMedicoM($IdLugarColecta, $FechaEvaluacion, $IdEntrevistador, $Peso, $Talla, $PresionSistolica, $PresionDiastolica, $Pulso, $Temperatura, $Hemoglobina, $Hematocrito, $NroCuestionario, $Calificacion, $IdMedicacion, $FechaMedicacion, $FechaEspera, $EstadoApto, $IdMotivoRechazo, $IdMovimiento, $UsuReg, $FecReg, $Turno, $MetodologiaHb, $MetodologiaHcto, $IdResponsableEv, $FechaInicioRechazo, $FechaFinRechazo, $Observacion);
	
	//ACTUALIZAR IdGrupoSanguineo POSTULANTE
	$IdPostulante=$_REQUEST['IdPostulante'];
	$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo'];
	SIGESA_UpdGrupoSanguineoPostulante_M($IdPostulante, $IdGrupoSanguineo);
	
	if($newIdExamenMedico!=NULL){
		$mensaje="Evaluación Predonación registrada correctamente";
		print "<script>alert('$mensaje')</script>";	
	}else{
		$mensaje="Evaluación Predonación YA registrada";
		print "<script>alert('$mensaje')</script>";	
	}
	
	include('../../MVC_Vista/BancoSangre/EvaluacionPredonacionPostulante.php');
	
}


if($_REQUEST["acc"] == "GuardarUpdExamenMedico") {
	$IdExamenMedico=$_REQUEST['IdExamenMedico'];
	//$IdLugarColecta=$_REQUEST['IdLugarColecta'];	
	//$FechaEvaluacion=gfecha($_REQUEST['FechaEvaluacion']).' '.$_REQUEST['HoraEvaluacion'];	
	//$IdEntrevistador=$_REQUEST['IdEntrevistador'];
	$Peso = ($_REQUEST['Peso']!=NULL) ? ($_REQUEST['Peso']) : 0;
	$Talla = ($_REQUEST['Talla']!=NULL) ? ($_REQUEST['Talla']) : 0;
	$PresionSistolica = ($_REQUEST['PresionSistolica']!=NULL) ? ($_REQUEST['PresionSistolica']) : 0;		
	$PresionDiastolica = ($_REQUEST['PresionDiastolica']!=NULL) ? ($_REQUEST['PresionDiastolica']) : 0;	
	$Pulso = ($_REQUEST['Pulso']!=NULL) ? ($_REQUEST['Pulso']) : 0;	
	$Temperatura = ($_REQUEST['Temperatura']!=NULL) ? ($_REQUEST['Temperatura']) : 0;	
	$Hemoglobina = ($_REQUEST['Hemoglobina']!=NULL) ? ($_REQUEST['Hemoglobina']) : 0;		
	$Hematocrito = ($_REQUEST['Hematocrito']!=NULL) ? ($_REQUEST['Hematocrito']) : 0;
	
	$Observacion=$_REQUEST['Observacion'];
	$NroCuestionario='';
	$Calificacion=0;
	$IdMedicacion=$_REQUEST['IdMedicacion'];
	$EstadoApto=$_REQUEST['EstadoApto'];
	$IdMotivoRechazo=$_REQUEST['IdMotivoRechazo'];
	$IdMovimiento=$_REQUEST['IdMovimiento'];
	$UsuUpd=$_REQUEST['IdEmpleado'];
	$FecUpd=$FechaServidor;//date('Y-m-d H:i:s');
	
	$FechaMedicacion = ($_REQUEST['FechaMedicacion']!='') ? gfecha($_POST['FechaMedicacion']) : NULL; //NULL manda vacio
	$FechaEspera = ($_REQUEST['FechaEspera']!='') ? gfecha($_POST['FechaEspera']) : NULL;
	
	//$Turno=$_REQUEST['Turno'];
	$MetodologiaHb=$_REQUEST['MetodologiaHb'];
	$MetodologiaHcto=$_REQUEST['MetodologiaHcto'];
	//$IdResponsableEv=$_REQUEST['IdResponsableEv'];
	
	$FechaInicioRechazo = ($_REQUEST['FechaInicioRechazo']!=NULL) ? gfecha($_REQUEST['FechaInicioRechazo']) : NULL;	
	$FechaFinRechazo = ($_REQUEST['FechaFinRechazo']!=NULL) ? gfecha($_REQUEST['FechaFinRechazo']) : NULL;

	GuardarUpdExamenMedicoM($Peso, $Talla, $PresionSistolica, $PresionDiastolica, $Pulso, $Temperatura, $Hemoglobina, $Hematocrito, $NroCuestionario, $Calificacion, $IdMedicacion, $FechaMedicacion, $FechaEspera, $EstadoApto, $IdMotivoRechazo, $IdMovimiento, $UsuUpd, $FecUpd, $MetodologiaHb, $MetodologiaHcto, $FechaInicioRechazo, $FechaFinRechazo, $Observacion, $IdExamenMedico);
	
	//ACTUALIZAR IdGrupoSanguineo POSTULANTE
	$IdPostulante=$_REQUEST['IdPostulante'];
	$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo'];
	SIGESA_UpdGrupoSanguineoPostulante_M($IdPostulante, $IdGrupoSanguineo);
	
	//INICIO AGREGADO 14-09-2017 POR MAHALI	
	if($_REQUEST['NroDonacionAct']!=NULL && trim($EstadoApto)!=2){ //2=SI-Apto,1=NO-No Apto Temporal,0=NO-No Apto Definitivo		
		$NroDonacion=NULL;	
		$EstadoDonacion=NULL; //NroDonacion NO registrado
		$FechaDonacion=NULL; 
		SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $_REQUEST['IdMovimiento']);		
	}//FIN AGREGADO 14-09-2017 POR MAHALI
	
	$mensaje="Evaluación Predonación actualizado correctamente";
	print "<script>alert('$mensaje')</script>";	
	
	include('../../MVC_Vista/BancoSangre/ExtraccionSangre.php');
	
}

if($_REQUEST["acc"] == "GuardarExtraccionSangre") {
	//$IdExtraccion=$_REQUEST['IdExtraccion'];
	$IdExamenMedico=$_REQUEST['IdExamenMedico'];
	$IdResponsableIni=$_REQUEST['IdResponsableIni'];
	$IdResponsableFin=$_REQUEST['IdResponsableFin'];
	$IdTipoDonacion=$_REQUEST['IdTipoDonacion']; ///
	
	if($IdTipoDonacion=='1'){ //1=Sangre Total
		$IdBolsaColectora=$_REQUEST['IdBolsaColectora'];
		$IdMezcladorBasculante=$_REQUEST['IdMezcladorBasculante'];
		$SerieHemobascula=$_REQUEST['SerieHemobascula'];
		$DuracionColeccion=$_REQUEST['DuracionColeccion'];
		$VolColeccionPre=$_REQUEST['VolColeccionPre'];
		$VolColeccionReal=$_REQUEST['VolColeccionReal'];
		$AlarmaOcurrida=$_REQUEST['AlarmaOcurrida'];	
		$FlujoAlto=isset($_REQUEST['FlujoAlto']) ? $_REQUEST['FlujoAlto'] : 0; 
		$FlujoBajo=isset($_REQUEST['FlujoBajo']) ? $_REQUEST['FlujoBajo'] : 0; 
		$ConInterrupcion=isset($_REQUEST['ConInterrupcion']) ? $_REQUEST['ConInterrupcion'] : 0; 		
		$TipoReg=$_REQUEST['TipoReg'];
		
		$IdEquipoAferesis='0';
		$IdSetAferesis='0';
		$SerieEquipo='';		
		$VolProcesado='';
		$VolUsado='';
		$VolPlaquetas='';
		$NroCiclos=0;
		$RendimientoEstimado=0;
		$RendimientoObjetivo=0;
		
	}else if($IdTipoDonacion=='2'){ //2=AFERESIS
		$IdBolsaColectora='0';
		$IdMezcladorBasculante='0';
		$SerieHemobascula='';
		$DuracionColeccion='';
		$VolColeccionPre='';
		$VolColeccionReal='';
		$AlarmaOcurrida='';
		$FlujoAlto='';
		$FlujoBajo=''; 
		$ConInterrupcion='';
		$TipoReg='';
		
		$IdEquipoAferesis=$_REQUEST['IdEquipoAferesis'];
		$IdSetAferesis=$_REQUEST['IdSetAferesis'];
		$SerieEquipo=$_REQUEST['SerieEquipo'];
		$DuracionColeccion=$_REQUEST['DuracionColeccion2'];
		$VolProcesado=$_REQUEST['VolProcesado'];
		$VolUsado=$_REQUEST['VolUsado'];
		$VolPlaquetas=$_REQUEST['VolPlaquetas'];
		$NroCiclos=$_REQUEST['NroCiclos'];
		$RendimientoEstimado=$_REQUEST['RendimientoEstimado'];
		$RendimientoObjetivo=$_REQUEST['RendimientoObjetivo'];
	}
	//
	$IdMotivoRechazoDurante=$_REQUEST['IdMotivoRechazoDurante'];
	$IdMotivoRechazoDespues=$_REQUEST['IdMotivoRechazoDespues'];
	$FechaExtraccion=gfecha($_REQUEST['FechaExtraccion']).' '.$_REQUEST['HoraInicio'];
	$HoraInicio=$_REQUEST['HoraInicio'];
	$HoraFin=$_REQUEST['HoraFin'];	
	
	$Observaciones=$_REQUEST['Observaciones'];	
	$FechaReg=$FechaServidor;
	$UsuReg=$_REQUEST['IdEmpleado'];
	//	
	
	$NroLote=$_REQUEST['NroLote'];	
	$Estado='1';	
	$EstadoApto=$_REQUEST['Anular']; //0=Si ANULADO= NO APTO , 1=NO ANULADO= (APTO)	
	
	if($EstadoApto=='0'){ //SI ANULADO
		//DATOS para actualizar datos de Donacion al Movimiento(Recepcion)
		$NroDonacion=NULL;
		$EstadoDonacion='0'; //No apto
		$FechaDonacion=NULL;	
		$IdMovimiento=$_REQUEST['IdMovimiento'];
		
	}else if($EstadoApto=='1'){ //NO ANULADO
		//DATOS para actualizar datos de Donacion al Movimiento(Recepcion)
		$NroDonacion=$_REQUEST['NroDonacion'];			
		$EstadoDonacion='2'; //apto
		$FechaDonacion=gfecha($_REQUEST['FechaExtraccion']).' '.$_REQUEST['HoraInicio'];
		$IdMovimiento=$_REQUEST['IdMovimiento'];		
	}
	
	//INICIO CONDICIONES PARA GUARDAR EXTRACCION		
	if($_REQUEST['NroDonacionAnt']==$NroDonacion){ //NO SE HA CAMBIADO EL NroDonacion Y $NroDonacion NO ESTA ANULADO	
			//validar que no exista EL NroDonacion
		$ValidarNroDonacionExtraccion=ValidarNroDonacionExtraccion_M($NroDonacion);				
		if($ValidarNroDonacionExtraccion!=NULL){
			$mensaje="Extracción YA REGISTRADA...";
			print "<script>alert('$mensaje')</script>";					
			include('../../MVC_Vista/BancoSangre/ExtraccionSangre.php');
		}else{
			//guardar	
			$NewIdExtraccion=GuardarExtraccionM($IdExamenMedico, $IdResponsableIni, $IdResponsableFin, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $FechaExtraccion, $HoraInicio, $HoraFin, $IdBolsaColectora, $IdMezcladorBasculante, $SerieHemobascula, $DuracionColeccion, $VolColeccionPre, $VolColeccionReal, $AlarmaOcurrida, $FlujoAlto, $FlujoBajo, $ConInterrupcion, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaReg, $UsuReg, $TipoReg, $IdMovimiento, $EstadoDonacion);//El SP ademas actualiza EstadoApto='2' en SIGESA_BSD_Movimientos
			//actualizar datos de Donacion al Movimiento(Recepcion)
			//SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento);	
			$mensaje="Extracción registrada correctamente";
			print "<script>alert('$mensaje')</script>";			
			include('../../MVC_Vista/BancoSangre/ExtraccionSangre.php');		
		}
	}else if($NroDonacion==NULL){ //$NroDonacion ESTA ANULADO
			//guardar	
			$NewIdExtraccion=GuardarExtraccionM($IdExamenMedico, $IdResponsableIni, $IdResponsableFin, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $FechaExtraccion, $HoraInicio, $HoraFin, $IdBolsaColectora, $IdMezcladorBasculante, $SerieHemobascula, $DuracionColeccion, $VolColeccionPre, $VolColeccionReal, $AlarmaOcurrida, $FlujoAlto, $FlujoBajo, $ConInterrupcion, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaReg, $UsuReg, $TipoReg, $IdMovimiento, $EstadoDonacion);	
		//actualizar datos de Donacion al Movimiento(Recepcion)
		SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento);
		$mensaje="Extracción registrada correctamente y Nro Donacion Anulado";
		print "<script>alert('$mensaje')</script>";	
		include('../../MVC_Vista/BancoSangre/ExtraccionSangre.php');
		
	}else{ //SE HA CAMBIADO EL NroDonacion
		//validar que no exista EL NroDonacion
		$ValidarNroDonacionMovimiento=ValidarNroDonacionMovimiento_M($NroDonacion,$IdMovimiento);				
		if($ValidarNroDonacionMovimiento!=NULL){
			$mensaje="Nro Donación YA EXISTE...";
			print "<script>alert('$mensaje')</script>";				
			include('../../MVC_Vista/BancoSangre/ExtraccionSangre.php');
		}else{
			//guardar	
			$NewIdExtraccion=GuardarExtraccionM($IdExamenMedico, $IdResponsableIni, $IdResponsableFin, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $FechaExtraccion, $HoraInicio, $HoraFin, $IdBolsaColectora, $IdMezcladorBasculante, $SerieHemobascula, $DuracionColeccion, $VolColeccionPre, $VolColeccionReal, $AlarmaOcurrida, $FlujoAlto, $FlujoBajo, $ConInterrupcion, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaReg, $UsuReg, $TipoReg, $IdMovimiento, $EstadoDonacion);			
			//actualizar nuevos datos de Donacion al Movimiento(Recepcion)
			SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento);
			$mensaje="Extracción registrada correctamente";
			print "<script>alert('$mensaje')</script>";
			//imprimir
			$IdExamenMedico=$_REQUEST['IdExamenMedico'];//no llamaba cuando EstadoApto(Movimiento)=1
			include('../../MVC_Vista/BancoSangre/ImprimirCodigoBarraNroDonacion.php');				
		}//end else	
	}//end else	
}

if($_REQUEST["acc"] == "EliminarExtraccionSangre") 
{  
	$IdExtraccion=$_REQUEST['IdExtraccion'];	
	$IdExamenMedico=$_REQUEST['IdExamenMedico'];
	$IdMovimiento=$_REQUEST['IdMovimiento'];
	$FechaElim=$FechaServidor;
	$UsuElim=$_REQUEST['IdEmpleado'];	
	EliminarExtraccionM($IdExtraccion,$IdExamenMedico,$IdMovimiento,$FechaElim,$UsuElim);	
	include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');
}

if($_REQUEST["acc"] == "GuardarUpdExtraccionSangre") {
	$IdExtraccion=$_REQUEST['IdExtraccion'];
	$IdExamenMedico=$_REQUEST['IdExamenMedico'];
	//$IdResponsableIni=$_REQUEST['IdResponsableIni'];
	//$IdResponsableFin=$_REQUEST['IdResponsableFin'];
	//$FechaExtraccion=gfecha($_REQUEST['FechaExtraccion']).' '.$_REQUEST['HoraInicio'];
	//$HoraInicio=$_REQUEST['HoraInicio'];
	//$HoraFin=$_REQUEST['HoraFin'];
	$IdTipoDonacion=$_REQUEST['IdTipoDonacion']; ///
	
	if($IdTipoDonacion=='1'){ //1=Sangre Total
		/*$IdBolsaColectora=$_REQUEST['IdBolsaColectora'];
		$IdMezcladorBasculante=$_REQUEST['IdMezcladorBasculante'];
		$SerieHemobascula=$_REQUEST['SerieHemobascula'];
		$DuracionColeccion=$_REQUEST['DuracionColeccion'];
		$VolColeccionPre=$_REQUEST['VolColeccionPre'];*/
		$VolColeccionReal=$_REQUEST['VolColeccionReal'];
		/*$AlarmaOcurrida=$_REQUEST['AlarmaOcurrida'];	
		$FlujoAlto=isset($_REQUEST['FlujoAlto']) ? $_REQUEST['FlujoAlto'] : 0; 
		$FlujoBajo=isset($_REQUEST['FlujoBajo']) ? $_REQUEST['FlujoBajo'] : 0; 
		$ConInterrupcion=isset($_REQUEST['ConInterrupcion']) ? $_REQUEST['ConInterrupcion'] : 0; 		
		$TipoReg=$_REQUEST['TipoReg'];*/
		
		$IdEquipoAferesis='0';
		$IdSetAferesis='0';
		$SerieEquipo='';		
		$VolProcesado='';
		$VolUsado='';
		$VolPlaquetas='';
		$NroCiclos=0;
		$RendimientoEstimado=0;
		$RendimientoObjetivo=0;
		
	}else if($IdTipoDonacion=='2'){ //2=AFERESIS
		/*$IdBolsaColectora='0';
		$IdMezcladorBasculante='0';
		$SerieHemobascula='';
		$DuracionColeccion='';
		$VolColeccionPre='';*/
		$VolColeccionReal='';
		/*$AlarmaOcurrida='';
		$FlujoAlto='';
		$FlujoBajo=''; 
		$ConInterrupcion='';
		$TipoReg='';*/
		
		$IdEquipoAferesis=$_REQUEST['IdEquipoAferesis'];
		$IdSetAferesis=$_REQUEST['IdSetAferesis'];
		$SerieEquipo=$_REQUEST['SerieEquipo'];
		$DuracionColeccion=$_REQUEST['DuracionColeccion2'];
		$VolProcesado=$_REQUEST['VolProcesado'];
		$VolUsado=$_REQUEST['VolUsado'];
		$VolPlaquetas=$_REQUEST['VolPlaquetas'];
		$NroCiclos=$_REQUEST['NroCiclos'];
		$RendimientoEstimado=$_REQUEST['RendimientoEstimado'];
		$RendimientoObjetivo=$_REQUEST['RendimientoObjetivo'];
	}
	//
	$IdMotivoRechazoDurante=$_REQUEST['IdMotivoRechazoDurante'];
	$IdMotivoRechazoDespues=$_REQUEST['IdMotivoRechazoDespues'];		
	
	$Observaciones=$_REQUEST['Observaciones'];	
	$FechaUpd=$FechaServidor;
	$UsuUpd=$_REQUEST['IdEmpleado'];
	//	
	
	$NroLote=$_REQUEST['NroLote'];	
	$Estado='1';	
	$EstadoApto=$_REQUEST['Anular']; //0=Si ANULADO= NO APTO , 1=NO ANULADO= (APTO)	
	
	if($EstadoApto=='0'){ //SI ANULADO
		//DATOS para actualizar datos de Donacion al Movimiento(Recepcion)
		$NroDonacion=NULL;
		$EstadoDonacion='0'; //No apto
		$FechaDonacion=NULL;	
		$IdMovimiento=$_REQUEST['IdMovimiento'];
		
	}else if($EstadoApto=='1'){ //NO ANULADO
		//DATOS para actualizar datos de Donacion al Movimiento(Recepcion)
		$NroDonacion=$_REQUEST['NroDonacion'];			
		$EstadoDonacion='2'; //apto
		//$FechaDonacion=gfecha($_REQUEST['FechaExtraccion']).' '.$_REQUEST['HoraInicio'];
		$IdMovimiento=$_REQUEST['IdMovimiento'];		
	}
	
	//INICIO CONDICIONES PARA GUARDAR EXTRACCION		
	if($_REQUEST['NroDonacionAnt']==$NroDonacion){ //NO SE HA CAMBIADO EL NroDonacion Y $NroDonacion NO ESTA ANULADO			
			//guardar	
			/*$NewIdExtraccion=GuardarExtraccionM($IdExamenMedico, $IdResponsableIni, $IdResponsableFin, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $FechaExtraccion, $HoraInicio, $HoraFin, $IdBolsaColectora, $IdMezcladorBasculante, $SerieHemobascula, $DuracionColeccion, $VolColeccionPre, $VolColeccionReal, $AlarmaOcurrida, $FlujoAlto, $FlujoBajo, $ConInterrupcion, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaReg, $UsuReg, $TipoReg, $IdMovimiento, $EstadoDonacion);*///El SP ademas actualiza EstadoApto='2' en SIGESA_BSD_Movimientos
			GuardarUpdExtraccionM($IdExamenMedico, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $VolColeccionReal, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaUpd, $UsuUpd, $IdMovimiento, $EstadoDonacion, $IdExtraccion);
			//actualizar datos de Donacion al Movimiento(Recepcion)
			//SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento);	
			$mensaje="Extracción actualizada correctamente";
			print "<script>alert('$mensaje')</script>";			
			include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');		
		
	}else if($NroDonacion==NULL){ //$NroDonacion ESTA ANULADO
			//guardar	
			GuardarUpdExtraccionM($IdExamenMedico, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $VolColeccionReal, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaUpd, $UsuUpd, $IdMovimiento, $EstadoDonacion, $IdExtraccion);	
		//actualizar datos de Donacion al Movimiento(Recepcion)
		SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento);
		$mensaje="Extracción actualizada correctamente y Nro Donacion Anulado";
		print "<script>alert('$mensaje')</script>";	
		include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');
		
	}else{ //SE HA CAMBIADO EL NroDonacion
		//validar que no exista EL NroDonacion
		$ValidarNroDonacionMovimiento=ValidarNroDonacionMovimiento_M($NroDonacion,$IdMovimiento);				
		if($ValidarNroDonacionMovimiento!=NULL){
			$mensaje="Nro Donación YA EXISTE...";
			print "<script>alert('$mensaje')</script>";				
			include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');
		}else{
			//guardar	
			GuardarUpdExtraccionM($IdExamenMedico, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $VolColeccionReal, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaUpd, $UsuUpd, $IdMovimiento, $EstadoDonacion, $IdExtraccion);		
			//actualizar nuevos datos de Donacion al Movimiento(Recepcion)
			SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento);
			$mensaje="Extracción actualizada correctamente";
			print "<script>alert('$mensaje')</script>";
			//imprimir
			$IdExamenMedico=$_REQUEST['IdExamenMedico'];//no llamaba cuando EstadoApto(Movimiento)=1
			include('../../MVC_Vista/BancoSangre/ImprimirCodigoBarraNroDonacion.php');				
		}//end else	
	}//end else	
}

if($_REQUEST["acc"] == "Configuracion") 
{  	
	include('../../MVC_Vista/BancoSangre/Configuracion.php');
}

if($_REQUEST["acc"] == "GuardarUpdParametros") {
	$Codigo=$_REQUEST['Codigo'];
	$Valor1=$_REQUEST['Valor1'];
	GuardarUpdParametros_M($Codigo,$Valor1);	
	include('../../MVC_Vista/BancoSangre/Configuracion.php');
}

if($_REQUEST["acc"] == "ActivarDesactivarParametros") {
	$Codigo=$_REQUEST['Codigo'];
	$Estado=$_REQUEST['Estado'];
	ActivarDesactivarParametros_M($Codigo,$Estado);	
	include('../../MVC_Vista/BancoSangre/Configuracion.php');
}

if($_REQUEST["acc"] == "Fraccionamiento") {		
	include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');
}

if($_REQUEST["acc"] == "UpdNroDonacionMovimiento") {	
	//actualizar datos de Donacion al Movimiento(Recepcion)
		$NroDonacion=$_REQUEST['NroDonacionCreado'];			
		$EstadoDonacion='1'; //NroDonacion registrado
		$FechaDonacion=$FechaServidor;//FECHA DE GENERACION DE ETIQUETA	
		$IdMovimiento=$_REQUEST['IdMovimiento'];
		
		$ValidarNroDonacionMovimiento=ValidarNroDonacionMovimiento_M($NroDonacion,$IdMovimiento);
				
		if($ValidarNroDonacionMovimiento!=NULL){
			$mensaje="Nro Donación YA EXISTE...";
			print "<script>alert('$mensaje')</script>";					
			include('../../MVC_Vista/BancoSangre/ExtraccionSangre.php');
		}else{
			SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento);		
			$mensaje="Nro Donación Generado correctamente";
			print "<script>alert('$mensaje')</script>";	
			$IdExamenMedico=$_REQUEST['IdExamenMedico']; 
			include('../../MVC_Vista/BancoSangre/ImprimirCodigoBarraNroDonacion.php');
		}	
		
}

if($_REQUEST["acc"] == "ImprimirNroDonacionMovimiento") {
		$NroDonacion=$_REQUEST['NroDonacionCreado'];
		$IdExamenMedico=$_REQUEST['IdExamenMedico'];				
		include('../../MVC_Vista/BancoSangre/ImprimirCodigoBarraNroDonacion.php');
}

if($_REQUEST["acc"] == "ObtenerGrupoSanguineoPaciente") {
		$IdPaciente = isset($_REQUEST['IdPaciente']) ? $_REQUEST['IdPaciente'] : ''; 			
		$data = array();
		$Resultado = array();		
		$data = ObtenerGrupoSanguineoPaciente($IdPaciente);
		for ($i=0; $i < count($data); $i++) { 
			$Resultado[$i] = array(
				'idOrden'	=>	$data[$i]["idOrden"],
				'ValorTexto'	=> $data[$i]["ordenXresultado"].". ".(mb_strtoupper($data[$i]["ValorTexto"])),
				'Fecha'	=>	vfecha(substr($data[$i]["Fecha"],0,10)),
				'RealizaPrueba'	=>	(mb_strtoupper($data[$i]["RealizaPrueba"])),
				);
		}
		echo json_encode($Resultado);
		exit();
}

if ($_REQUEST["acc"] == 'ListarMotivoRechazo') {
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; 	
		$data = SIGESA_ListarMotivoRechazo_M('Evaluacion',$texto); 

		for ($i=0; $i < count($data); $i++) { 
			$Datos=$data[$i]["IdMotivoRechazo"].'|'.$data[$i]["Diferimiento"].'|'.$data[$i]["Tiempo"].'|'.$data[$i]["TipoTiempo"];
			$MotivoRechazo[$i] = array(				
				'Descripcion'	=>	$data[$i]["Descripcion"],
				'Datos'	=>	$Datos
			);
		}
		echo json_encode($MotivoRechazo);
		exit();
}

if ($_REQUEST["acc"] == 'ListarDatosSistemaFraccionamiento') {
	$NroDonacion = $_REQUEST["NroDonacion"];
	$data = array();
	$DatosSistemaFraccionamiento = array();
	$data = ListarDatosSistemaFraccionamiento($NroDonacion);	

	for ($i=0; $i < count($data); $i++) { 
		$DatosSistemaFraccionamiento = array(
			'FechaPrimeraCentrif'	=>	vfecha(substr($data[$i]["FechaPrimeraCentrif"],0,10)), //01/04/2017 
			'HoraPrimeraCentrif'	=>	$data[$i]["HoraPrimeraCentrif"],
			'GR'	=>	$data[$i]["GR"],
			'PLASMA'	=>	$data[$i]["PLASMA"]
			);
	}	
	echo json_encode($DatosSistemaFraccionamiento);	
	exit();
}

//INICIO GUARDAR Fraccionamiento
if($_REQUEST["acc"] == "GuardarTabuladura") {	
	$IdResponsable = $_REQUEST["IdResponsable"];
	$VolumenTotRecol = $_REQUEST["VolumenTotRecol"];
	$NroTabuladora = $_REQUEST["NroTabuladora"];	
	$IdExtraccion = $_REQUEST["IdExtraccion1"];
	$UsuReg = $_REQUEST["IdEmpleado"];
    $FecReg = $FechaServidor; 
	
	$DatosFraccionamiento=SIGESA_ListarExtraccionFraccionamiento_M($IdExtraccion);
	if($DatosFraccionamiento==NULL){
		$NewIdFraccionamiento=GuardarFraccionamientoSangreTabM($IdResponsable,$VolumenTotRecol,$NroTabuladora,$IdExtraccion,$UsuReg,$FecReg); 
		$mensaje="Tubuladura registrada correctamente";
		print "<script>alert('$mensaje')</script>";							
	}else{
		$mensaje="Tubuladura YA registrada";
		print "<script>alert('$mensaje')</script>";
	}		
	include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');   	
}
if($_REQUEST["acc"] == "GuardarPrimeraCentrif") {
	$IdExtraccion = $_REQUEST["IdExtraccion2"];
	$VolumenPaqueteGlobu=$_REQUEST["VolumenPaqueteGlobu"];    
    $VolumenPlasma=$_REQUEST["VolumenPlasma"];      
    $MotivoElimPaqueteGlobu=$_REQUEST["MotivoElimPaqueteGlobu"];     
    $MotivoElimPlasma=$_REQUEST["MotivoElimPlasma"];     
	$FechaPrimeraCentrif = gfecha($_REQUEST["FechaPrimeraCentrif"]).' '.$_REQUEST['HoraPrimeraCentrif'];
	$NroTabuladora = $_REQUEST["ConfirmaTabuladura"];
	GuardarFraccionamientoSangre1CentriM($IdExtraccion,$VolumenPaqueteGlobu,$VolumenPlasma,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$FechaPrimeraCentrif,$NroTabuladora);  	
	$mensaje="Primera Centrifugación registrado correctamente";
	print "<script>alert('$mensaje')</script>";		
	include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');
}
if($_REQUEST["acc"] == "GuardarSegundaCentrif") {
	$IdExtraccion = $_REQUEST["IdExtraccion3"];
	$VolumenPlaquetas = $_REQUEST["VolumenPlaquetas"];
	$MotivoElimPlaquetas = $_REQUEST["MotivoElimPlaquetas"];
	$FechaSegundaCentrif = gfecha($_REQUEST["FechaSegundaCentrif"]).' '.$_REQUEST['HoraSegundaCentrif'];	
	GuardarFraccionamientoSangre2CentriM($IdExtraccion,$VolumenPlaquetas,$MotivoElimPlaquetas,$FechaSegundaCentrif);  
	$mensaje="Segunda Centrifugación registrado correctamente";
	print "<script>alert('$mensaje')</script>";		
	include('../../MVC_Vista/BancoSangre/Fraccionamiento.php');	
}

//FIN GUARDAR Fraccionamiento

if($_REQUEST["acc"] == "GuardarTamizaje") {
	$FechaTamizaje = gfecha($_REQUEST["FechaTamizaje"]).' '.$_REQUEST['HoraTamizaje'];
	$IdResponsable = $_REQUEST["IdResponsable"];
	$NroDonacion = $_REQUEST["NroDonacion"];	
	$FechaReg = $FechaServidor; 	
	$UsuReg = $_REQUEST["IdEmpleado"];
	$quimioVIH = $_REQUEST["quimioVIH"];	
	if($quimioVIH=='1'){ //Reactivo
		$PRquimioVIH = $_REQUEST["PRquimioVIH"];		
	}else{ //NoReactivo
		$PRquimioVIH = 0;
	}
	
	$quimioSifilis = $_REQUEST["quimioSifilis"];
	if($quimioSifilis=='1'){ //Reactivo
		$PRquimioSifilis = $_REQUEST["PRquimioSifilis"];		
	}else{ //NoReactivo
		$PRquimioSifilis = 0;
	}
	
	$quimioHTLV = $_REQUEST["quimioHTLV"];
	if($quimioHTLV=='1'){ //Reactivo
		$PRquimioHTLV = $_REQUEST["PRquimioHTLV"];		
	}else{ //NoReactivo
		$PRquimioHTLV = 0;
	}
	
	$quimioANTICHAGAS = $_REQUEST["quimioANTICHAGAS"];
	if($quimioANTICHAGAS=='1'){ //Reactivo
		$PRquimioANTICHAGAS = $_REQUEST["PRquimioANTICHAGAS"];		
	}else{ //NoReactivo
		$PRquimioANTICHAGAS = 0;
	}
	
	$quimioHBS = $_REQUEST["quimioHBS"];
	if($quimioHBS=='1'){ //Reactivo
		$PRquimioHBS = $_REQUEST["PRquimioHBS"];		
	}else{ //NoReactivo
		$PRquimioHBS = 0;
	}
	
	$quimioVHB = $_REQUEST["quimioVHB"];
	if($quimioVHB=='1'){ //Reactivo
		$PRquimioVHB = $_REQUEST["PRquimioVHB"];		
	}else{ //NoReactivo
		$PRquimioVHB = 0;
	}
	
	$quimioVHC = $_REQUEST["quimioVHC"];
	if($quimioVHC=='1'){ //Reactivo
		$PRquimioVHC = $_REQUEST["PRquimioVHC"];		
	}else{ //NoReactivo
		$PRquimioVHC = 0;
	}
	
	$RealizaNAT = $_REQUEST["RealizaNAT"];
	if($RealizaNAT == '1'){ //SI
		$natVIH = $_REQUEST["natVIH"];
		$natHBV = $_REQUEST["natHBV"];
		$natHCV = $_REQUEST["natHCV"];
		$IdMotivoNoNAT = NULL;
	}else{
		$natVIH = NULL;
		$natHBV = NULL;
		$natHCV = NULL;
		$IdMotivoNoNAT = $_REQUEST["IdMotivoNoNAT"];
		if($IdMotivoNoNAT=='4'){ //4 = OTRO, ESPECIFICAR:
			$MotivoNoNAT = trim(mb_strtoupper($_REQUEST["MotivoNoNAT"]));
			$IdMotivoNoNAT=GuardarMotivoNoNATM($MotivoNoNAT);	
		}else{
			$IdMotivoNoNAT = $_REQUEST["IdMotivoNoNAT"];
		}
	}
	
	
	$IdExtraccion = $_REQUEST["IdExtraccion"];
	$IdMovimiento = $_REQUEST["IdMovimiento"];
	$Observacion = $_REQUEST["Observacion"];	
	
	//CAMPOS NUEVOS
	$SNCS = $_REQUEST["SNCS"];
	$MotivoElimMuestra = $_REQUEST["MotivoElimMuestra"];
	
	if($quimioVIH=='1' || $quimioSifilis=='1' || $quimioHTLV=='1' || $quimioANTICHAGAS=='1' || $quimioHBS=='1' || $quimioVHB=='1' || $quimioVHC=='1' || $natVIH=='1' || $natHBV=='1' || $natHCV=='1' ){
		$EstadoApto = '0';
	}else if(trim($MotivoElimMuestra)!=''){		
		$quimioVIH=NULL; $quimioSifilis=NULL; $quimioHTLV=NULL; $quimioANTICHAGAS=NULL; $quimioHBS=NULL; $quimioVHB=NULL; $quimioVHC=NULL;
		$PRquimioVIH=0; $PRquimioSifilis=0; $PRquimioHTLV=0; $PRquimioANTICHAGAS=0; $PRquimioHBS=0; $PRquimioVHB=0; $PRquimioVHC=0; 		
		$EstadoApto = NULL;
	}else{
		$EstadoApto = '1';
	}
	//$Estado = '1';	
	
	$ValidarRepiteSNCS=ValidarRepiteSNCS_M($SNCS, '');
	
	if($ValidarRepiteSNCS==''){
		$NewIdTamizaje=GuardarTamizajeM($FechaTamizaje, $IdResponsable, $NroDonacion, $FechaReg, $UsuReg, 
		$quimioVIH, $quimioSifilis, $quimioHTLV, $quimioANTICHAGAS, $quimioHBS, $quimioVHB, $quimioVHC, 
		$PRquimioVIH, $PRquimioSifilis, $PRquimioHTLV, $PRquimioANTICHAGAS, $PRquimioHBS, $PRquimioVHB, $PRquimioVHC,
		$natVIH, $natHBV, $natHCV, $IdExtraccion, $Observacion, $EstadoApto,$IdMovimiento, $RealizaNAT, $IdMotivoNoNAT, $SNCS,$MotivoElimMuestra);
				
		$ObtenerMaximoSNCS=ObtenerMaximoSNCS_M();
		$Valor1=$ObtenerMaximoSNCS[0]['Valor1'];
		if((int)$SNCS>$Valor1){
			UpdateMaximoSNCS_M($SNCS);	
		}
		
		$mensaje="Tamizaje registrado correctamente";
		print "<script>alert('$mensaje')</script>";				
					
	}else{
		$mensaje="Nº de Sello Nacional de Calidad YA EXISTE";
		print "<script>alert('$mensaje')</script>";	
	}
	
	include('../../MVC_Vista/BancoSangre/Tamizaje.php');
	
}


if($_REQUEST["acc"] == "Reportes_BSD") {
	include('../../MVC_Vista/BancoSangre/Reportes.php');	
}

if($_REQUEST["acc"] == "ReporteExcel1") {
	$fecha_inicio=gfecha($_REQUEST["fecha_inicio"]);
	$fecha_fin=gfecha($_REQUEST["fecha_fin"]);
	include('../../MVC_Vista/BancoSangre/TablaExcelReporte1.php');		
}

if($_REQUEST["acc"] == "GuardarUpdRecepcion") {				
		
			//guardar Datos Personales del Postulante (tabla SIGESA_BSD_Postulante)			
			$CodigoPostulanteExiste=trim($_REQUEST['CodigoPostulante']);	
			$IdPostulanteExiste=trim($_REQUEST['IdPostulante']);		
								
			$CodigoPostulante = $CodigoPostulanteExiste; 							 
			$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
			$UsuReg=$_REQUEST['IdEmpleado'];
			$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo'];
			//--no actualiza 8campos y NroDocumento
			$ApellidoPaterno='';
			$ApellidoMaterno='';
			$PrimerNombre='';
			$SegundoNombre='';
			$TercerNombre='';
			$FechaNacimiento='';				
			$IdTipoSexo=''; 
			$IdDocIdentidad='';
			$NroDocumento='';				 				
			$Telefono=$_REQUEST['Telefono'];
			$DireccionDomicilio=trim(mb_strtoupper($_REQUEST['DireccionDomicilio']));				
			$IdEstadoCivil=$_REQUEST['IdEstadoCivil'];				
			$IdTipoOcupacion=$_REQUEST['IdTipoOcupacion'];				
			$IdCentroPobladoNacimiento = isset($_REQUEST['IdCentroPobladoNacimiento']) ? $_REQUEST['IdCentroPobladoNacimiento'] : 0;				
			$IdCentroPobladoDomicilio = isset($_REQUEST['IdCentroPobladoDomicilio']) ? $_REQUEST['IdCentroPobladoDomicilio'] : 0;				
			$IdCentroPobladoProcedencia = isset($_REQUEST['IdCentroPobladoProcedencia']) ? $_REQUEST['IdCentroPobladoProcedencia'] : 0;				
			$Observacion='';
			$IdPaisDomicilio=$_REQUEST['IdPaisDomicilio'];
			$IdPaisProcedencia=$_REQUEST['IdPaisProcedencia'];
			$IdPaisNacimiento=$_REQUEST['IdPaisNacimiento'];				
			$IdDistritoProcedencia = isset($_REQUEST['IdDistritoProcedencia']) ? $_REQUEST['IdDistritoProcedencia'] : 0;				
			$IdDistritoDomicilio = isset($_REQUEST['IdDistritoDomicilio']) ? $_REQUEST['IdDistritoDomicilio'] : 0;				
			$IdDistritoNacimiento = isset($_REQUEST['IdDistritoNacimiento']) ? $_REQUEST['IdDistritoNacimiento'] : 0;				
			$Email=$_REQUEST['Email'];
			
			GuardarPostulanteM($CodigoPostulante,$FechaReg,$UsuReg,$IdGrupoSanguineo,$ApellidoPaterno,$ApellidoMaterno,$PrimerNombre,$SegundoNombre,$TercerNombre,$FechaNacimiento,$NroDocumento,$Telefono,$DireccionDomicilio,$IdTipoSexo,$IdEstadoCivil,$IdDocIdentidad,$IdTipoOcupacion,$IdCentroPobladoNacimiento,$IdCentroPobladoDomicilio,$IdCentroPobladoProcedencia,$Observacion,$IdPaisDomicilio,$IdPaisProcedencia,$IdPaisNacimiento,$IdDistritoProcedencia,$IdDistritoDomicilio,$IdDistritoNacimiento,$Email);								
			$NewIdPostulante = $IdPostulanteExiste; 	
				  
			//GUARDAR SIGESA_BSD_Receptor						
			//$NroDocumento=str_pad($_REQUEST["NroDocumentoRec"], 8, "0", STR_PAD_LEFT);  
			$NroDocumento=$_REQUEST["NroDocumentoRec"]; 			
			$IdPaciente=$_REQUEST['IdPaciente'];			
			$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineoRec'];	
			$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
			$UsuReg=$_REQUEST['IdEmpleado'];	
			
			if($_REQUEST['NroHistoria']==""){
				$NroHistoria=0;
			}else{
				$NroHistoria=$_REQUEST['NroHistoria'];
			}	

			if($_REQUEST['IdPaciente']==""){
				$IdPaciente=0;
			}else{
				$IdPaciente=$_REQUEST['IdPaciente'];
			}			
			
			//$BuscarReceptor=SIGESA_BSD_BuscarReceptor_M($NroHistoria,'NroHistoriaClinica');
			$BuscarReceptor=SIGESA_BSD_BuscarReceptorFiltro_M($IdPaciente,'IdPaciente');
			if($BuscarReceptor!=NULL){
				$NewIdReceptor=$BuscarReceptor[0]["IdReceptor"];
				GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg);
			}else{
				//$NewIdReceptor=$AutoIdReceptor;
				$NewIdReceptor=GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg);				
			}	
			$GrupoSanguineo=$_REQUEST['GrupoSanguineoRec'];//text A+,A-...
			UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo);		
			//FIN GUARDAR DATOS DEL RECEPTOR	   		
			
			//GUARDAR MOVIMIENTOS			   
			if($NewIdPostulante!=NULL && $NewIdReceptor!=NULL){
				//echo "POSTULANTE Y RECEPTOR REGISTRADO";						
				$IdMovimiento=$_REQUEST['IdMovimiento'];
				//$Estado='1'; //REGISTRADO PENDIENTE PARA ANALISIS PRE-DONACION
				$FechaUpd=$FechaServidor;//date('Y-m-d H:i:s');
				$UsuUpd=$_REQUEST['IdEmpleado'];
				//$FechaMovi=gfecha($_REQUEST['FechaMovi']).' '.$_REQUEST['HoraMovi'];
				$NroDonacion=NULL;				
				$IdReceptor=$NewIdReceptor;
				$IdCondicionDonante=$_REQUEST['IdCondicionDonante'];
				$IdTipoDonacion=$_REQUEST['IdTipoDonacion'];
				$IdTipoDonante=$_REQUEST['IdTipoDonante'];
				$IdPostulante=$NewIdPostulante;
				$IdTipoRelacion=$_REQUEST['IdTipoRelacion'];
				
				$IdHospital=$_REQUEST['IdHospital'];
				$IdServicioHospital=$_REQUEST['IdServicioHospital'];											
				$Observaciones=$_REQUEST['Observaciones'];				
				GuardarUpdMovimientosM($FechaUpd, $UsuUpd, $NroDonacion, $IdReceptor, $IdCondicionDonante, $IdTipoDonacion, $IdTipoDonante, $IdPostulante, $IdTipoRelacion, $Observaciones, $IdHospital, $IdServicioHospital, $IdMovimiento); //, $NroCama, $IdMedico, $Medico, $IdDiagnostico, $CodigoCIE10Diagnostico, $EstadoAlta,				
					$mensaje="Recepción actualizada correctamente";
					print "<script>alert('$mensaje')</script>";	
				
			}//FIN GUARDAR MOVIMIENTOS					
				
			include('../../MVC_Vista/BancoSangre/EvaluacionPredonacionPostulante.php');
}  

if($_REQUEST["acc"] == "ObtenerDatosReniec") {
		$IdReniec = isset($_REQUEST['IdReniec']) ? $_REQUEST['IdReniec'] : ''; 	
		$data = array();
		$Resultado = array();		
		$data = ObtenerIdDepaReniec_M($IdReniec);
		for ($i=0; $i < count($data); $i++) { 
			$Resultado = array(
				'IdDistrito'	=>	$data[$i]["IdDistrito"],
				'IdProvincia'	=> $data[$i]["IdProvincia"],
				'IdDepartamento'	=>	$data[$i]["IdDepartamento"]
			);
		}
		echo json_encode($Resultado);
		exit();
}

if($_REQUEST["acc"] == "Consultas") {
	include('../../MVC_Vista/BancoSangre/Consultas.php');	
}
if($_REQUEST["acc"] == "ConsultarPostulantes") {
	include('../../MVC_Vista/BancoSangre/ConsultarPostulantes.php');	
}
if($_REQUEST["acc"] == "ConsultarReceptores") {
	include('../../MVC_Vista/BancoSangre/ConsultarReceptores.php');	
}
if($_REQUEST["acc"] == "ReportePostulantes")
{ 	
    $data = array();
	$ListarResultado= array();	
		 
		 $NroDocumento=$_REQUEST["NroDocumento"];
		 
		 //Estado Donante Prueba Confirmatoria
		 $BuscarDonante=SIGESA_BSD_ListarDonantesEstadoDefinitivoM($NroDocumento);
		 $EstadoDefinitivo=$BuscarDonante[0]["EstadoDefinitivo"];		
		 
		 $ListarResultado= ReportePostulantesM($NroDocumento);		
	       	
		for ($i = 0; $i < count($ListarResultado); $i++) {	
				 $EstadoApto=$ListarResultado[$i]["EstadoApto"]; //EstadoApto SIGESA_BSD_ExamenMedico				 
				 if($EstadoApto==''){
					 $DiferidoPredonacion='F';
				 }else if($EstadoApto=='2'){
					 $DiferidoPredonacion='NO';
				 }else{
					$DiferidoPredonacion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $IdMotivoRechazoDurante=$ListarResultado[$i]["IdMotivoRechazoDurante"];
				 $IdMotivoRechazoDespues=$ListarResultado[$i]["IdMotivoRechazoDespues"];
				 $IdMotivoRechazoExtraccion=$IdMotivoRechazoDurante.' '.$IdMotivoRechazoDespues;
				 if(trim($IdMotivoRechazoExtraccion)==''){
					 $DiferidoExtraccion='F';
				 }else if($IdMotivoRechazoDurante=='0' && $IdMotivoRechazoDespues=='0'){
					 $DiferidoExtraccion='NO';
				 }else{
					$DiferidoExtraccion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $EstadoAptoTamizaje=$ListarResultado[$i]["EstadoAptoTamizaje"];
				 if(trim($EstadoAptoTamizaje)=='' && $ListarResultado[$i]["IdTamizaje"]!=''){
					$Tamizaje='F - '.$ListarResultado[$i]["DesMotivoElimMuestra"];
				 }else if($EstadoAptoTamizaje=='0'){ //0 NO apto tamizaje
					$Tamizaje='<font color="#FF0000">Conversar con Médico</font>';  
				 }else if($EstadoAptoTamizaje=='1'){//1 APTO
					$Tamizaje='Apto'; 
				 }else{//AUN NO SE REGISTRA
					$Tamizaje=''; 
				 }			 	
				 						
                $data[$i] = array(
                    'Nro' => $i+1,					
					'NroMovimiento' => 'PDAC'.$ListarResultado[$i]["NroMovimiento"],
                    'FechaMovi' => vfecha(substr($ListarResultado[$i]['FechaMovi'],0,10)).' '.substr($ListarResultado[$i]['FechaMovi'],11,8),					
					'NroDocumento' => $ListarResultado[$i]["NroDocumento"],
					'Postulante' => strtoupper($ListarResultado[$i]["ApellidosPostulante"].' '.$ListarResultado[$i]["NombresPostulante"]),
                    'NroDonacion' => $ListarResultado[$i]["NroDonacion"],
                    'TipoDonacion' => $ListarResultado[$i]["TipoDonacion"],
					'DiferidoPredonacion' => $DiferidoPredonacion, 
					'DiferidoExtraccion' => $DiferidoExtraccion,   
					       
					'Paciente' => $ListarResultado[$i]["ApellidoPaternoPaciente"].' '.$ListarResultado[$i]["ApellidoMaternoPaciente"].' '.$ListarResultado[$i]["NombresPaciente"],
					'GrupoSanguineoPaciente' => $ListarResultado[$i]["GrupoSanguineoPaciente"],
					'HospitalProcedencia' => $ListarResultado[$i]["HospitalProcedencia"],
					'Tamizaje' => $Tamizaje,
					'Confirmatoria' => $EstadoDefinitivo
                );
				
            }//fin for
            echo json_encode($data);
            exit();	
	 //include('../../MVC_Vista/Emergencia/ListaAdmisionEmerg.php');
}

if($_REQUEST["acc"] == "ReporteReceptores")
{ 	
    $data = array();
	$ListarResultado= array();	
		 
		 $NroHistoria=$_REQUEST["NroHistoria"];
		 //$ApellidoPaterno=$_REQUEST["ApellidoPaterno"];	
		 
		 $ListarResultado= ReporteReceptoresM($NroHistoria);		
	       	
		for ($i = 0; $i < count($ListarResultado); $i++) {	
				 $EstadoApto=$ListarResultado[$i]["EstadoApto"]; //EstadoApto SIGESA_BSD_ExamenMedico				 
				 if($EstadoApto==''){
					 $DiferidoPredonacion='F';
				 }else if($EstadoApto=='2'){
					 $DiferidoPredonacion='NO';
				 }else{
					$DiferidoPredonacion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $IdMotivoRechazoDurante=$ListarResultado[$i]["IdMotivoRechazoDurante"];
				 $IdMotivoRechazoDespues=$ListarResultado[$i]["IdMotivoRechazoDespues"];
				 $IdMotivoRechazoExtraccion=$IdMotivoRechazoDurante.' '.$IdMotivoRechazoDespues;
				 if(trim($IdMotivoRechazoExtraccion)==''){
					 $DiferidoExtraccion='F';
				 }else if($IdMotivoRechazoDurante=='0' && $IdMotivoRechazoDespues=='0'){
					 $DiferidoExtraccion='NO';
				 }else{
					$DiferidoExtraccion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $EstadoAptoTamizaje=$ListarResultado[$i]["EstadoAptoTamizaje"];
				 if(trim($EstadoAptoTamizaje)=='' && $ListarResultado[$i]["IdTamizaje"]!=''){
					$Tamizaje='F - '.$ListarResultado[$i]["DesMotivoElimMuestra"];
				 }else if($EstadoAptoTamizaje=='0'){ //0 NO apto tamizaje
					$Tamizaje='<font color="#FF0000">Conversar con Médico</font>';  
				 }else if($EstadoAptoTamizaje=='1'){//1 APTO
					$Tamizaje='Apto'; 
				 }else{//AUN NO SE REGISTRA
					$Tamizaje=''; 
				 }	 
				 						
                $data[$i] = array(
                    'Nro' => $i+1,					
					'NroMovimiento' => 'PDAC'.$ListarResultado[$i]["NroMovimiento"],
                    'FechaMovi' => vfecha(substr($ListarResultado[$i]['FechaMovi'],0,10)).' '.substr($ListarResultado[$i]['FechaMovi'],11,8),					
					'NroHistoria' => $ListarResultado[$i]["NroHistoria"],
					'Postulante' => strtoupper($ListarResultado[$i]["ApellidosPostulante"].' '.$ListarResultado[$i]["NombresPostulante"]),
                    'NroDonacion' => $ListarResultado[$i]["NroDonacion"],
                    'TipoDonacion' => $ListarResultado[$i]["TipoDonacion"],
					'DiferidoPredonacion' => $DiferidoPredonacion, 
					'DiferidoExtraccion' => $DiferidoExtraccion,   
					       
					'Paciente' => $ListarResultado[$i]["ApellidoPaternoPaciente"].' '.$ListarResultado[$i]["ApellidoMaternoPaciente"].' '.$ListarResultado[$i]["NombresPaciente"],
					'GrupoSanguineoPaciente' => $ListarResultado[$i]["GrupoSanguineoPaciente"],
					'HospitalProcedencia' => $ListarResultado[$i]["HospitalProcedencia"],
					'Tamizaje' => $Tamizaje
                );
				
            }//fin for
            echo json_encode($data);
            exit();	
	 //include('../../MVC_Vista/Emergencia/ListaAdmisionEmerg.php');
}


if($_REQUEST["acc"] == "BuscarPacientes")
{ 	
    $data = array();
	$ListarResultado= array();	
		 
		 $NroDocumento=$_REQUEST["NroDocumento"];
		 //$ApellidoPaterno=$_REQUEST["ApellidoPaterno"];	
		 
		 $ListarResultado= ReportePostulantesM($NroDocumento);		
	       	
		for ($i = 0; $i < count($ListarResultado); $i++) {	
				 $EstadoApto=$ListarResultado[$i]["EstadoApto"]; //EstadoApto SIGESA_BSD_ExamenMedico				 
				 if($EstadoApto==''){
					 $DiferidoPredonacion='F';
				 }else if($EstadoApto=='2'){
					 $DiferidoPredonacion='NO';
				 }else{
					$DiferidoPredonacion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $IdMotivoRechazoDurante=$ListarResultado[$i]["IdMotivoRechazoDurante"];
				 $IdMotivoRechazoDespues=$ListarResultado[$i]["IdMotivoRechazoDespues"];
				 $IdMotivoRechazoExtraccion=$IdMotivoRechazoDurante.' '.$IdMotivoRechazoDespues;
				 if(trim($IdMotivoRechazoExtraccion)==''){
					 $DiferidoExtraccion='F';
				 }else if($IdMotivoRechazoDurante=='0' && $IdMotivoRechazoDespues=='0'){
					 $DiferidoExtraccion='NO';
				 }else{
					$DiferidoExtraccion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $EstadoAptoTamizaje=$ListarResultado[$i]["EstadoAptoTamizaje"];
				 if($EstadoAptoTamizaje==''){
					 $Tamizaje='F';
				 }else if($EstadoAptoTamizaje=='0'){ //NO apto tamizaje
					$Tamizaje='<font color="#FF0000">Conversar con Médico</font>';  
				 }else{ //1
					$Tamizaje='Apto'; 
				 }
				 						
                $data[$i] = array(
                    'Nro' => $i+1,					
					'NroMovimiento' => 'PDAC'.$ListarResultado[$i]["NroMovimiento"],
                    'FechaMovi' => vfecha(substr($ListarResultado[$i]['FechaMovi'],0,10)).' '.substr($ListarResultado[$i]['FechaMovi'],11,8),					
					'NroDocumento' => $ListarResultado[$i]["NroDocumento"],
					'Postulante' => strtoupper($ListarResultado[$i]["ApellidosPostulante"].' '.$ListarResultado[$i]["NombresPostulante"]),
                    'NroDonacion' => $ListarResultado[$i]["NroDonacion"],
                    'TipoDonacion' => $ListarResultado[$i]["TipoDonacion"],
					'DiferidoPredonacion' => $DiferidoPredonacion, 
					'DiferidoExtraccion' => $DiferidoExtraccion,   
					       
					'Paciente' => $ListarResultado[$i]["ApellidoPaternoPaciente"].' '.$ListarResultado[$i]["ApellidoMaternoPaciente"].' '.$ListarResultado[$i]["NombresPaciente"],
					'GrupoSanguineoPaciente' => $ListarResultado[$i]["GrupoSanguineoPaciente"],
					'HospitalProcedencia' => $ListarResultado[$i]["HospitalProcedencia"],
					'Tamizaje' => $Tamizaje
                );
				
            }//fin for
            echo json_encode($data);
            exit();	
	 //include('../../MVC_Vista/Emergencia/ListaAdmisionEmerg.php');
}

if($_REQUEST["acc"] == "ModificarFT") 
{  	
	include('../../MVC_Vista/BancoSangre/ModificarFT.php');
}

if($_REQUEST["acc"] == "GuardarUpdFraccionamiento") {
	
	$IdFraccionamiento = $_REQUEST["IdFraccionamiento"];		
	$UsuUpd = $_REQUEST["IdEmpleado"];
    $FecUpd = $FechaServidor; 
	
	$NroTabuladora = $_REQUEST["NroTabuladora"];
	$VolumenPaqueteGlobu=$_REQUEST["VolumenPaqueteGlobu"];    
    $VolumenPlasma=$_REQUEST["VolumenPlasma"];      
    $MotivoElimPaqueteGlobu=$_REQUEST["MotivoElimPaqueteGlobu"];     
    $MotivoElimPlasma=$_REQUEST["MotivoElimPlasma"];
	$VolumenPlaquetas = $_REQUEST["VolumenPlaquetas"];
	$MotivoElimPlaquetas = $_REQUEST["MotivoElimPlaquetas"];
	
	GuardarUpdFraccionamientoM($IdFraccionamiento,$UsuUpd,$FecUpd,$NroTabuladora,$VolumenPaqueteGlobu,$VolumenPlasma,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$VolumenPlaquetas,$MotivoElimPlaquetas);	
	$mensaje="Fraccionamiento actualizado correctamente";
	print "<script>alert('$mensaje')</script>";	
	include('../../MVC_Vista/BancoSangre/ModificarFT.php');	
}

if($_REQUEST["acc"] == "GuardarUpdTamizaje") {
	
	//$FechaTamizaje = gfecha($_REQUEST["FechaTamizaje"]);
	//$IdResponsable = $_REQUEST["IdResponsable"];
	//$NroDonacion = $_REQUEST["NroDonacion"];	
	$FechaUpd = $FechaServidor; 	
	$UsuUpd = $_REQUEST["IdEmpleado"];
	//$quimioVIH = $_REQUEST["quimioVIH"];
	$quimioVIH=isset($_REQUEST['quimioVIH']) ? $_REQUEST['quimioVIH'] : NULL;	
	if($quimioVIH=='1'){ //Reactivo
		$PRquimioVIH = $_REQUEST["PRquimioVIH"];		
	}else{ //NoReactivo
		$PRquimioVIH = 0;
	}
	
	//$quimioSifilis = $_REQUEST["quimioSifilis"];
	$quimioSifilis=isset($_REQUEST['quimioSifilis']) ? $_REQUEST['quimioSifilis'] : NULL;
	if($quimioSifilis=='1'){ //Reactivo
		$PRquimioSifilis = $_REQUEST["PRquimioSifilis"];		
	}else{ //NoReactivo
		$PRquimioSifilis = 0;
	}
	
	//$quimioHTLV = $_REQUEST["quimioHTLV"];
	$quimioHTLV=isset($_REQUEST['quimioHTLV']) ? $_REQUEST['quimioHTLV'] : NULL;
	if($quimioHTLV=='1'){ //Reactivo
		$PRquimioHTLV = $_REQUEST["PRquimioHTLV"];		
	}else{ //NoReactivo
		$PRquimioHTLV = 0;
	}
	
	//$quimioANTICHAGAS = $_REQUEST["quimioANTICHAGAS"];
	$quimioANTICHAGAS=isset($_REQUEST['quimioANTICHAGAS']) ? $_REQUEST['quimioANTICHAGAS'] : NULL;
	if($quimioANTICHAGAS=='1'){ //Reactivo
		$PRquimioANTICHAGAS = $_REQUEST["PRquimioANTICHAGAS"];		
	}else{ //NoReactivo
		$PRquimioANTICHAGAS = 0;
	}
	
	//$quimioHBS = $_REQUEST["quimioHBS"];
	$quimioHBS=isset($_REQUEST['quimioHBS']) ? $_REQUEST['quimioHBS'] : NULL;
	if($quimioHBS=='1'){ //Reactivo
		$PRquimioHBS = $_REQUEST["PRquimioHBS"];		
	}else{ //NoReactivo
		$PRquimioHBS = 0;
	}
	
	//$quimioVHB = $_REQUEST["quimioVHB"];
	$quimioVHB=isset($_REQUEST['quimioVHB']) ? $_REQUEST['quimioVHB'] : NULL;
	if($quimioVHB=='1'){ //Reactivo
		$PRquimioVHB = $_REQUEST["PRquimioVHB"];		
	}else{ //NoReactivo
		$PRquimioVHB = 0;
	}
	
	//$quimioVHC = $_REQUEST["quimioVHC"];
	$quimioVHC=isset($_REQUEST['quimioVHC']) ? $_REQUEST['quimioVHC'] : NULL;
	if($quimioVHC=='1'){ //Reactivo
		$PRquimioVHC = $_REQUEST["PRquimioVHC"];		
	}else{ //NoReactivo
		$PRquimioVHC = 0;
	}
	
	$RealizaNAT = $_REQUEST["RealizaNAT"];
	if($RealizaNAT == '1'){ //SI
		$natVIH = $_REQUEST["natVIH"];
		$natHBV = $_REQUEST["natHBV"];
		$natHCV = $_REQUEST["natHCV"];
		$IdMotivoNoNAT = NULL;
	}else{
		$natVIH = NULL;
		$natHBV = NULL;
		$natHCV = NULL;
		$IdMotivoNoNAT = $_REQUEST["IdMotivoNoNAT"];
		if($IdMotivoNoNAT=='4'){ //4 = OTRO, ESPECIFICAR:
			$MotivoNoNAT = trim(mb_strtoupper($_REQUEST["MotivoNoNAT"]));
			$IdMotivoNoNAT=GuardarMotivoNoNATM($MotivoNoNAT);	
		}else{
			$IdMotivoNoNAT = $_REQUEST["IdMotivoNoNAT"];
		}
	}	
	
	$IdExtraccion = $_REQUEST["IdExtraccion"];
	$IdMovimiento = $_REQUEST["IdMovimiento"];
	//$Observacion = "Actualizado";
	$Observacion = $_REQUEST["Observacion"];
	
	//CAMPOS NUEVOS
	$SNCS = $_REQUEST["SNCS"];
	$MotivoElimMuestra = $_REQUEST["MotivoElimMuestra"];	
	
	if($quimioVIH=='1' || $quimioSifilis=='1' || $quimioHTLV=='1' || $quimioANTICHAGAS=='1' || $quimioHBS=='1' || $quimioVHB=='1' || $quimioVHC=='1' || $natVIH=='1' || $natHBV=='1' || $natHCV=='1' ){
		$EstadoApto = '0';
	}else if(trim($MotivoElimMuestra)!=''){		
		$quimioVIH=NULL; $quimioSifilis=NULL; $quimioHTLV=NULL; $quimioANTICHAGAS=NULL; $quimioHBS=NULL; $quimioVHB=NULL; $quimioVHC=NULL;
		$PRquimioVIH=0; $PRquimioSifilis=0; $PRquimioHTLV=0; $PRquimioANTICHAGAS=0; $PRquimioHBS=0; $PRquimioVHB=0; $PRquimioVHC=0; 		
		$EstadoApto = NULL;
	}else{
		$EstadoApto = '1';
	}
	//$Estado = '1';	
	$IdTamizaje=$_REQUEST["IdTamizaje"];	
	
	$ValidarRepiteSNCS=ValidarRepiteSNCS_M($SNCS, $IdTamizaje);
	
	if($ValidarRepiteSNCS==''){
		GuardarUpdTamizajeM($FechaUpd, $UsuUpd, 
		$quimioVIH, $quimioSifilis, $quimioHTLV, $quimioANTICHAGAS, $quimioHBS, $quimioVHB, $quimioVHC, 
		$PRquimioVIH, $PRquimioSifilis, $PRquimioHTLV, $PRquimioANTICHAGAS, $PRquimioHBS, $PRquimioVHB, $PRquimioVHC,
		$natVIH, $natHBV, $natHCV, $IdExtraccion, $Observacion, $EstadoApto,$IdMovimiento, $RealizaNAT, $IdMotivoNoNAT, $IdTamizaje, $SNCS,$MotivoElimMuestra);
		
		$ObtenerMaximoSNCS=ObtenerMaximoSNCS_M();
		$Valor1=$ObtenerMaximoSNCS[0]['Valor1'];
		if((int)$SNCS>$Valor1){
			UpdateMaximoSNCS_M($SNCS);	
		}		
		$mensaje="Tamizaje actualizado correctamente";
		print "<script>alert('$mensaje')</script>";		
					
	}else{
		$mensaje="Nº de Sello Nacional de Calidad YA EXISTE";
		print "<script>alert('$mensaje')</script>";	
	}
	
	include('../../MVC_Vista/BancoSangre/ModificarFT.php');	
		
}

//INICIO FUNCIONES PARA Nº SNCS y NO ingresar ningún resultado de tamizaje
if($_REQUEST["acc"] == "ReporteSNCS") 
{  	
	include('../../MVC_Vista/BancoSangre/ReporteSNCS.php');
}

if($_REQUEST["acc"] == "ArchivarTamizaje") 
{  	
	$EstadoArchivado='1';
	$FechaArchiva=$FechaServidor;
	$UsuArchiva=$_REQUEST["IdEmpleado"];	
	
	$FechaTamizajeIni=gfecha($_REQUEST["FechaReg_Inicio"]);
	$FechaTamizajeFin=gfecha($_REQUEST["FechaReg_Final"]);
	
	$RealizaNAT=$_REQUEST["RealizaNAT"];

	$NroDonacionBus=$_REQUEST["NroDonacionBus"];
	
	ArchivarTamizaje_M($EstadoArchivado,$FechaArchiva,$UsuArchiva,$FechaTamizajeIni,$FechaTamizajeFin,$RealizaNAT,$NroDonacionBus);	
	
	//Registrar Examenes Donantes Pendientes
	if($NroDonacionBus!=""){
		$TodosNroDonacion=$_REQUEST["NroDonacionBus"];//1900017
	}else{
		$TodosNroDonacion=$_REQUEST["TodosNroDonacion"].'0';//1805299,1805300,1805301,1805302,0
	}	
	SIGESA_BSD_EnviarDonantesTablaEnvio($TodosNroDonacion);	  

	$mensaje="Tamizajes archivados correctamente";
	print "<script>alert('$mensaje')</script>";	
	include('../../MVC_Vista/BancoSangre/ReporteSNCS.php');
}
//FIN FUNCIONES PARA Nº SNCS y NO ingresar ningún resultado de tamizaje

//Inicio PRUEBA CONFIRMATORIA
if($_REQUEST["acc"] == "PruebaConfirmatoria") 
{  	
	include('../../MVC_Vista/BancoSangre/PruebaConfirmatoria.php');
}

if($_REQUEST["acc"] == "FiltrarDonante") {
			
		$data = array();
		$DatosPostulantes = array();
		
		$texto = isset($_POST['q']) ? $_POST['q'] : ''; // the request parameter
		if(trim($texto)!="" && is_numeric($texto)){
			$data=SIGESA_BuscarDonantesFiltroM($texto,'NroDocumento');			
		}else if(trim($texto)!=""){
			$data=SIGESA_BuscarDonantesFiltroM($texto,'ApellidosNombres');		
		}else{
			/*$mensaje="Ingrese Apellidos y Nombres del Postulante";
			print "<script>alert('$mensaje')</script>";exit();*/
		}
				
		for ($i=0; $i < count($data); $i++) { 
			$NombresPostulante=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].' '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];	
			$EstadoAptoTamizaje=$data[$i]["EstadoAptoTamizaje"]; // 0 1 ""
			if(trim($EstadoAptoTamizaje)==""){
				$DescripcionEApto='<font color="#00FF00">FALTA</font>'; 
				$DescripcionEApto2='FALTA'; 
			}else if($EstadoAptoTamizaje==0){
				$DescripcionEApto='<font color="#FF0000">NO APTO</font>'; 
				$DescripcionEApto2='NO APTO'; 
			}else if($EstadoAptoTamizaje==1){
				$DescripcionEApto='<font color="#0000FF">APTO</font>';
				$DescripcionEApto2='APTO';  
			}

			$DatosPostulantes[$i] = array(
				'CodigoPostulante'	=>	$data[$i]["CodigoPostulante"],
				'ApellidoPaterno'	=>	mb_strtoupper($data[$i]["ApellidoPaterno"]),
				'NombresPostulante'	=>	mb_strtoupper($NombresPostulante),
				'NroDocumento'	=>	$data[$i]["NroDocumento"],
				'GrupoSanguineoPostulante'	=>	$data[$i]["GrupoSanguineoPostulante"],
				'EstadoAptoTamizaje'	=>	$data[$i]["EstadoAptoTamizaje"],
				'DescripcionEApto'	=>	$DescripcionEApto,
				'DescripcionEApto2' =>	$DescripcionEApto2
				);
		}
		
		echo json_encode($DatosPostulantes);
		exit();		
	
}

if($_REQUEST["acc"] == "ReporteDonantesReactivos")
{ 	
    $data = array();
	$ListarResultado= array();	
		 
		 $NroDocumento=$_REQUEST["NroDocumento"];		 	

		 //Estado Donante
		 $BuscarDonante=SIGESA_BSD_ListarDonantesEstadoDefinitivoM($NroDocumento);
		 $EstadoDefinitivo=$BuscarDonante[0]["EstadoDefinitivo"];
		 
		 $ListarResultado= ReporteDonantesReactivosM($NroDocumento);		
	       	
		for ($i = 0; $i < count($ListarResultado); $i++) {
			$PRUEBAREACTIVO=$ListarResultado[$i]["PRUEBAREACTIVO"];
			$BuscarPruebaConfirmatoria=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PRUEBAREACTIVO);

			if($BuscarPruebaConfirmatoria!=NULL){				

				$FechaPrueba=$BuscarPruebaConfirmatoria[0]["FechaPrueba"];
				if(trim($FechaPrueba)!=""){				
					$FechaPrueba=vfecha(substr($FechaPrueba,0,10)); 
				}else{				 
					$FechaPrueba=''; 
				}

				$IdResponsablePrueba=$BuscarPruebaConfirmatoria[0]["IdResponsablePrueba"];
				$ResponsablePrueba=$BuscarPruebaConfirmatoria[0]["ResponsablePrueba"];				

				$ResultadoFinal=$BuscarPruebaConfirmatoria[0]["ResultadoFinal"];
				if(trim($ResultadoFinal)==""){				
					$DescripcionRFinal=''; 
				}else if($ResultadoFinal==0){				 
					//$DescripcionRFinal='NO REACTIVO';
					$DescripcionRFinal='<font color="#0000FF">NO REACTIVO</font>'; 
				}else if($ResultadoFinal==1){			
					//$DescripcionRFinal='REACTIVO';  
					$DescripcionRFinal='<font color="#FF0000">REACTIVO</font>'; 
				}

				$Comentario=$BuscarPruebaConfirmatoria[0]["Comentario"];
				$NombreDoc=$BuscarPruebaConfirmatoria[0]["NombreDoc"];				
				$IdPruebaConfirmatoria=$BuscarPruebaConfirmatoria[0]["IdPruebaConfirmatoria"];

			}else{
				$FechaPrueba=''; 
				$IdResponsablePrueba='';
				$ResponsablePrueba='';	
				$DescripcionRFinal=''; 
				$Comentario='';
				$NombreDoc='';				
				$IdPruebaConfirmatoria='';				
			}
						 					
                $data[$i] = array(
                    'Nro' => $i+1,	
                    'NroDocumento' => $ListarResultado[$i]["NroDocumento"],                    				
					'FechaTamizaje' => vfecha(substr($ListarResultado[$i]['FechaTamizaje'],0,10)).' '.substr($ListarResultado[$i]['FechaTamizaje'],11,8),
                    'FechaExtraccion' => vfecha(substr($ListarResultado[$i]['FechaExtraccion'],0,10)).' '.substr($ListarResultado[$i]['FechaExtraccion'],11,8),	
                    'SNCS' => $ListarResultado[$i]["SNCS"],	
                    'NroDonacion' => $ListarResultado[$i]["NroDonacion"],                    
					'GrupoSanguineoPostulante' => $ListarResultado[$i]["GrupoSanguineoPostulante"], 
					'ObservacionTamizaje' => $ListarResultado[$i]["ObservacionTamizaje"],  
					'PruebaReactivo' => $ListarResultado[$i]["PRUEBAREACTIVO"],
					'IdTamizaje' => $ListarResultado[$i]["IdTamizaje"],

					'FechaPrueba' => $FechaPrueba, 
					'IdResponsablePrueba' => $IdResponsablePrueba,
					'ResponsablePrueba' => $ResponsablePrueba,
					'NombreDoc' => $NombreDoc,
					'ResultadoFinal' => $DescripcionRFinal,
					'Comentario'  => $Comentario,
					'IdPruebaConfirmatoria' => $IdPruebaConfirmatoria,
					'EstadoDefinitivo'=> $EstadoDefinitivo,


                );				
            }//fin for
            echo json_encode($data);
            exit();	
	 
}

if($_REQUEST["acc"] == "VerPdf")
{
	$IdPruebaConfirmatoria=$_REQUEST["IdPruebaConfirmatoria"];
 	$BuscarPruebaConfirmatoriaPorId=SIGESA_BuscarPruebaConfirmatoriaPorIdM($IdPruebaConfirmatoria);
 	$DocPruebaConfirma=$BuscarPruebaConfirmatoriaPorId[0]["DocPruebaConfirma"];		
 	
	header("Content-type: application/pdf");//image/jpeg
	//$NombreDoc=$BuscarPruebaConfirmatoriaPorId[0]["NombreDoc"];	
	//header('Content-Disposition: attachment; filename="'.$NombreDoc.'"');//filename="downloaded.pdf"');	
	echo trim($DocPruebaConfirma);
}

function mssql_escape($data) {
    if(is_numeric($data))
     return $data;
    $unpacked = unpack('H*hex', $data);
    return '0x' . $unpacked['hex'];   
}

if($_REQUEST["acc"] == "GuardarPruebaConfirmatoria")
{

//$IdPruebaConfirmatoria=$_REQUEST['IdPruebaConfirmatoria'];
$NroDocumento=$_REQUEST['NroDocumentoPostulante'];
$IdTamizaje=$_REQUEST['IdTamizaje'];
$NroDonacion=$_REQUEST['NroDonacion'];
$FechaPrueba=gfecha($_REQUEST['FechaPrueba']);
$IdResponsablePrueba=$_REQUEST['IdResponsablePrueba'];
$PruebaReactivo=$_REQUEST['PruebaReactivo'];

$ResultadoFinal=$_REQUEST['ResultadoFinal'];
$ValorMedicion=$_REQUEST['ValorMedicion'];
$Comentario=$_REQUEST['Comentario'];

$FechaReg=$FechaServidor;  
$UsuReg=$_REQUEST['IdEmpleado'];   

//documentos
$NombreDoc=$_FILES["DocPruebaConfirma"]["name"];
//$DocPruebaConfirma=$_REQUEST['DocPruebaConfirma'];

//Mover el archivo a una ubicación
	//move_uploaded_file($_FILES["DocPruebaConfirma"]["tmp_name"],"temp/" . $_FILES["DocPruebaConfirma"]["name"]);

//Processes your file here
//FORMA 1 varbinary(MAX)
$UbicacionDoc = $_FILES["DocPruebaConfirma"]["tmp_name"];
$data = fopen($UbicacionDoc, "rb");
$content =fread($data,filesize($UbicacionDoc));
$DocPruebaConfirma=mssql_escape($content);//Call mssql_escape function

//FORMA 2 varchar(MAX)
//$DocPruebaConfirma = base64_encode(addslashes(fread(fopen($UbicacionDoc, "rb"), filesize($UbicacionDoc))));
	
GuardarPruebaConfirmatoriaM($NroDocumento, $IdTamizaje, $NroDonacion, $FechaPrueba, $IdResponsablePrueba, $PruebaReactivo, $DocPruebaConfirma, $NombreDoc, $ResultadoFinal, $ValorMedicion, $Comentario, $FechaReg, $UsuReg);	
	$mensaje="Prueba Confirmatoria REGISTRADO correctamente";
	print "<script>alert('$mensaje')</script>";	

	include('../../MVC_Vista/BancoSangre/PruebaConfirmatoria.php');		
}

//Fin PRUEBA CONFIRMATORIA


//INICIO ETIQUETAS falta modi en servidor
if($_REQUEST["acc"] == "ImpresionEtiquetas")
{
	include('../../MVC_Vista/BancoSangre/Etiquetas.php');	
}

if($_REQUEST["acc"] == "ImprimirEtiquetaAlicuota") {
		$NroDonacion=trim($_REQUEST['NroDonacion']);
		$data=SIGESA_BSD_ListarEtiquetasAlicuotaM($NroDonacion);

		if($data!=NULL){
			include('../../MVC_Vista/BancoSangre/ImprimirEtiquetaAlicuota.php');
		}else{
			$mensaje="Nro Donacion ".$NroDonacion." NO EXISTE o FALTA Registrar Fraccionamiento";
			print "<script>alert('$mensaje')</script>";	
			include('../../MVC_Vista/BancoSangre/Etiquetas.php');
		}
}

if($_REQUEST["acc"] == "ImprimirEtiquetaReceptor") {
		$NroHistoriaClinica=trim($_REQUEST['NroHistoriaClinica']);
		$data=SIGESA_BSD_ListarEtiquetasReceptorM($NroHistoriaClinica);

		if($data!=NULL){
			include('../../MVC_Vista/BancoSangre/ImprimirEtiquetaReceptor.php');
		}else{
			$mensaje="Paciente con Nro de Historia Clinica ".$NroHistoriaClinica." NO EXISTE";
			print "<script>alert('$mensaje')</script>";	
			include('../../MVC_Vista/BancoSangre/Etiquetas.php');
		}
}

if($_REQUEST["acc"] == "OrdenesLaboratorio_ImprimirOrden"){ 
	          
	    $IdOrden=$_REQUEST["IdOrden"];         	
		$ListarDatosMoviLab=SigesaLaboratorioXidOrden_M($IdOrden);
	 
	    $Paciente=$ListarDatosMoviLab[0][2];
		$NroHistoriaClinica=$ListarDatosMoviLab[0]["NroHistoriaClinica"];
	    $NroDocumento=$ListarDatosMoviLab[0]["NroDocumento"];
	    $FechaHoraRealizaCpt=$ListarDatosMoviLab[0][1];
		  
	    $dataPaciente=SIGESA_BSD_ListarEtiquetasReceptorM($NroHistoriaClinica);		  
		  
		if($ListarDatosMoviLab!=NULL){
			 include('../../MVC_Vista/BancoSangre/ImprimirEtiquetaSolicitudAnalisis.php');
		}else{
			$mensaje="Nro de Orden ".$IdOrden." NO EXISTE";
			print "<script>alert('$mensaje')</script>";	
			include('../../MVC_Vista/BancoSangre/Etiquetas.php');
		}	
  
 }
//FIN ETIQUETAS

//PARTE 2

if($_REQUEST["acc"] == "VerificacionAptos"){ 
	          
	   include('../../MVC_Vista/BancoSangre/VerificacionAptos.php');
  
 }

 if($_REQUEST["acc"] == "RegVerificacionAptos") 
{ 
	include('../../MVC_Vista/BancoSangre/RegVerificacionAptos.php');
}

if($_REQUEST["acc"] == "GuardarVerificacionAptos") 
{ 
	///////
	//$IdVerifica=$_REQUEST['IdVerifica'];
	$NroDonacion=$_REQUEST['NroDonacion'];
	//$SNCS=$_REQUEST['SNCS'];
	$SNCS_PG=$_REQUEST['SNCS_PG'];
	$SNCS_PFC=$_REQUEST['SNCS_PFC'];
	$SNCS_PQ=$_REQUEST['SNCS_PQ'];

	$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo'];
	$IdGrupoSanguineoAnterior=$_REQUEST["IdGrupoSanguineoAnterior"];
	$FechaVerifica=gfecha($_REQUEST['FechaVerifica']).' '.$_REQUEST['HoraVerifica'];
	$IdResponsable=$_REQUEST['IdResponsable'];
	$DeteccionCI=$_REQUEST['DeteccionCI'];
	$CoombsDirecto=isset($_REQUEST['CoombsDirecto']) ? $_REQUEST['CoombsDirecto'] : '';

	$DVI=isset($_REQUEST['DVI']) ? $_REQUEST['DVI'] : ''; 

	$FenC=isset($_REQUEST['FenC']) ? $_REQUEST['FenC'] : ''; 
	$FenE=isset($_REQUEST['FenE']) ? $_REQUEST['FenE'] : ''; 
	$Fencc=isset($_REQUEST['Fencc']) ? $_REQUEST['Fencc'] : ''; 
	$Fenee=isset($_REQUEST['Fenee']) ? $_REQUEST['Fenee'] : ''; 
	$FenCw=isset($_REQUEST['FenCw']) ? $_REQUEST['FenCw'] : ''; 
	$FenK=isset($_REQUEST['FenK']) ? $_REQUEST['FenK'] : ''; 
	$Fenkk=isset($_REQUEST['Fenkk']) ? $_REQUEST['Fenkk'] : ''; 
	$Fencontrol=isset($_REQUEST['Fencontrol']) ? $_REQUEST['Fencontrol'] : ''; 

	$Fenotipo=$_REQUEST['Fenotipo'];
	$Observacion=$_REQUEST['Observacion'];
	$EstadoHemo='1';

	$IdFraccionamiento=$_REQUEST['IdFraccionamiento'];	
	$UsuReg=$_REQUEST['IdEmpleado'];	
	$FechaReg=$FechaServidor;	
	$IdPostulante=$_REQUEST['IdPostulante'];	
	
	//INICIO ACTUALIZAR Fraccionamiento
	$IdFraccionamiento=$_REQUEST['IdFraccionamiento'];		
	if($CoombsDirecto=='+'){ // Eliminar todos los hemocomponentes por "COOMBS DIRECTO"
		$MotivoElimPaqueteGlobu=44;
		$MotivoElimPlasma=45;
		$MotivoElimPlaquetas=46;		
		$SNCS_PG='';
		$SNCS_PFC='';
		$SNCS_PQ='';
		$EstadoHemo='0';//Todos los hemocomponentes eliminados
		UpdFraccionamientoM($IdFraccionamiento,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$MotivoElimPlaquetas);	

	}else if($DeteccionCI=='+'){ // PFC y PQ eliminado por "ANTICUERPOS IRREGULARES"
		if(trim($SNCS_PG)!=''){//SI PG AUN NO HA SIDO ELIMINADO POR FRACCIONAMIENTO
			$MotivoElimPaqueteGlobu=0;
		}else{//SI ESTÁ ELIMINADO POR OTRO MOTIVO
			$MotivoElimPaqueteGlobu='';//EL MISMO MOTIVO
			$EstadoHemo='0';
		}
		$MotivoElimPlasma=47;
		$MotivoElimPlaquetas=48;			
		$SNCS_PFC='';
		$SNCS_PQ='';
		UpdFraccionamientoM($IdFraccionamiento,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$MotivoElimPlaquetas);	
	}
		
	//FIN ACTUALIZAR Fraccionamiento

	//INICIO ACTUALIZAR GS
	if($IdGrupoSanguineo!=$IdGrupoSanguineoAnterior){
		$IdPostulante=$_REQUEST["IdPostulante"];
		$IdGrupoSanguineo=$_REQUEST["IdGrupoSanguineo"];
		//echo "SIGESA_UpdGrupoSanguineoPostulante_M";
		SIGESA_UpdGrupoSanguineoPostulante_M($IdPostulante, $IdGrupoSanguineo);
	}
	//FIN ACTUALIZAR GS

	GuardarVerificacionAptosM($NroDonacion, $SNCS_PG, $SNCS_PFC, $SNCS_PQ, $IdGrupoSanguineo, $IdGrupoSanguineoAnterior, $FechaVerifica, $IdResponsable, $DeteccionCI, $CoombsDirecto, $DVI, /*$FenC, $FenE, $Fencc, $Fenee, $FenCw, $FenK, $Fenkk,*/ $Fenotipo, $Observacion, $EstadoHemo, $IdFraccionamiento, $UsuReg, $FechaReg, $IdPostulante);	

	//GUARDAR Fenotipo
	//$IdFenotipo=$_REQUEST['IdFenotipo'];
	$TipoPersona='D';//D donante,R Receptor
	$IdPostulante=$_REQUEST['IdPostulante'];
	$IdPaciente=0;	
	$Observacion='';
	$Estado='1';
	$FechaReg=$FechaServidor;
	$UsuReg=$_REQUEST['IdEmpleado'];

	if(trim($Fenotipo)!=''){
		GuardarUpdFenotipoPersonaM($TipoPersona, $IdPostulante, $IdPaciente, $Fenotipo);
	}

	if($FenC!=''){
		$TipoFenotipo='cmayor';
		$ValorFenotipo=$_REQUEST['FenC'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($FenE!=''){
		$TipoFenotipo='emayor';
		$ValorFenotipo=$_REQUEST['FenE'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fencc!=''){
		$TipoFenotipo='cmenor';
		$ValorFenotipo=$_REQUEST['Fencc'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fenee!=''){
		$TipoFenotipo='emenor';
		$ValorFenotipo=$_REQUEST['Fenee'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($FenCw!=''){
		$TipoFenotipo='Cw';
		$ValorFenotipo=$_REQUEST['FenCw'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($FenK!=''){
		$TipoFenotipo='kmayor';
		$ValorFenotipo=$_REQUEST['FenK'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fenkk!=''){
		$TipoFenotipo='kmenor';
		$ValorFenotipo=$_REQUEST['Fenkk'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fencontrol!=''){
		$TipoFenotipo='control';
		$ValorFenotipo=$_REQUEST['Fencontrol'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	$mensaje="Verificación guardada correctamente";
	print "<script>alert('$mensaje')</script>";	
	include('../../MVC_Vista/BancoSangre/VerificacionAptos.php');
}

if($_REQUEST["acc"] == "LiberarCuarentena"){           
	  $ComponentesEnvio=$_REQUEST["ComponentesEnvio"]; //PG-0314587<br>PG-0314589<br>PG-0314588<br>PG-0314586<br>
	  $arrayComponentesEnvio=explode("<br>", $ComponentesEnvio);

	   //ver si es PG,PFC o PQ	  	
	  	$he=$arrayComponentesEnvio[0]; //OBTENER EL PRIMER ELEMENTO ejem: PQ-0314585
	  	$arrayhe=explode("-", $he);
	  	$tipohe=$arrayhe[0]; //PG,PFC o PQ

	    //Datos GuardarCabecera
	 	//$MovNumero=$_REQUEST['MovNumero'];//
		$MovTipo='I';
		$TipoHem=$tipohe; 
		$IdEstablecimientoIngreso='6216'; //6216	06218	Hosp. Nac.daniel A. Carrion
		$IdEstablecimientoEgreso='';
		$UsuarioRecibe=$_REQUEST['UsuarioRecibe'];
		$FechaRecibe=gfecha($_REQUEST['FechaRecibe']);
		$IdPaciente='';
		$UsuarioSalida='';
		$FechaSalida='';
		//$FechaPreparaCrio=NULL;
		$FechaPreparaFraccion=NULL;
		$MetodoPreparaFraccion='';
		$Estado='1';
		$Observacion='';
		$UsuReg=$_REQUEST['IdEmpleado'];	
		$FecReg=$FechaServidor;

		$MovNumero=GuardarMovSangreCabeceraM($MovTipo, $TipoHem, $IdEstablecimientoIngreso, $IdEstablecimientoEgreso, $UsuarioRecibe, $FechaRecibe, $IdPaciente, $UsuarioSalida, $FechaSalida, $FechaPreparaFraccion, $MetodoPreparaFraccion, $Estado, $Observacion, $UsuReg, $FecReg);


	  for($i=0;$i<(count($arrayComponentesEnvio)-1);$i++){	 
	  	
		$he_SNCS=$arrayComponentesEnvio[$i];//PQ-0314585
		$arrayhem=explode("-", $he_SNCS);
	  	$tipohem=$arrayhem[0]; //PG,PFC o PQ
	  	$SNCS=$arrayhem[1];//0314585

	  	//Inicio Obtener Datos Donacion con $NumSNCS
	  	$ObtenerDatosDonacion=ObtenerDatosDonacionConSNCS_M($SNCS);
	  	$IdGrupoSanguineo=$ObtenerDatosDonacion[0]["IdGrupoSanguineo"];
	  	$IdTipoDonacion=$ObtenerDatosDonacion[0]["IdTipoDonacion"];
	  	$IdTipoDonante=$ObtenerDatosDonacion[0]["IdTipoDonante"];

	  	$NroDonacion=$ObtenerDatosDonacion[0]["NroDonacion"];
	  	$NroTabuladora=$ObtenerDatosDonacion[0]["NroTabuladora"];
	  	$FechaExtraccion=$ObtenerDatosDonacion[0]["FechaExtraccion"];

	  	$VolumenPaqueteGlobu=$ObtenerDatosDonacion[0]["VolumenPaqueteGlobu"];
	  	$VolumenPlasma=$ObtenerDatosDonacion[0]["VolumenPlasma"];
	  	$VolumenPlaquetas=$ObtenerDatosDonacion[0]["VolumenPlaquetas"];	  	
	  	//Fin Obtener Datos Donacion con $NumSNCS	

	  	//Datos GuardarDetalle
	  	//$IdMovDetalle=$_REQUEST['IdMovDetalle'];
		$MovNumero=$MovNumero;
		$MovTipo='I';
		$IdGrupoSanguineo=$IdGrupoSanguineo;//
		$NroDonacion=$NroDonacion;
		$NroTabuladora=$NroTabuladora;
		$TipoHem=$tipohem;
		$SNCS=$SNCS;
		$FechaExtraccion=$FechaExtraccion;

		//
		$ListarParametrosTIDPG=SIGESA_ListarParametros_M('TIDPG');
	    $TIDPG=$ListarParametrosTIDPG[0]["Valor1"];// 42

	    $ListarParametrosTIDPFC=SIGESA_ListarParametros_M('TIDPFC');
	    $TIDPFC=$ListarParametrosTIDPFC[0]["Valor1"];// 365

	    $ListarParametrosTIDPQ=SIGESA_ListarParametros_M('TIDPQ');
	    $TIDPQ=$ListarParametrosTIDPQ[0]["Valor1"];// 5

		$nuevafecha1 = strtotime ( '+'.$TIDPG.'day' , strtotime ( $FechaExtraccion ) ) ;//42
		$nuevafecha1 = date ( 'Y-m-j' , $nuevafecha1 );
		$FechaVencPG=substr($nuevafecha1,0,10);//vfecha(substr($nuevafecha1,0,10));
		
		$nuevafecha2 = strtotime ( '+'.$TIDPQ.'day' , strtotime ( $FechaExtraccion ) ) ;//5
		$nuevafecha2 = date ( 'Y-m-j' , $nuevafecha2 );
		$FechaVencPlaqueta=substr($nuevafecha2,0,10);//vfecha(substr($nuevafecha2,0,10));
		
		$nuevafecha3 = strtotime ( '+'.$TIDPFC.'day' , strtotime ( $FechaExtraccion ) ) ;//365
		$nuevafecha3 = date ( 'Y-m-j' , $nuevafecha3 );
		$FechaVencPlasma=substr($nuevafecha3,0,10);//vfecha(substr($nuevafecha3,0,10));
		//

		if($tipohem=='PG'){	  		
	  		$FechaVencimiento=$FechaVencPG; 
	  		$VolumenTotRecol=$VolumenPaqueteGlobu;

	  	}else if($tipohem=='PFC'){	  		
	  		$FechaVencimiento=$FechaVencPlasma;
	  		$VolumenTotRecol=$VolumenPlasma; 

	  	}else if($tipohem=='PQ'){	  		
	  		$FechaVencimiento=$FechaVencPlaqueta;
	  		$VolumenTotRecol=$VolumenPlaquetas;
	  	}	
		
		$VolumenFraccion=0;
		$VolumenRestante=$VolumenTotRecol;

		$IdTipoDonacion=$IdTipoDonacion;
		$IdTipoDonante=$IdTipoDonante;
		if($IdTipoDonante==3 || $IdTipoDonante==5 || $IdTipoDonacion==2){ //IdTipoDonante(3=Autólogo,5=Dirigido) --IdTipoDonacion(2=AFERESIS)
			$swReserva='1';//RESERVADO			
			$IdPacienteReserva=$ObtenerDatosDonacion[0]["IdPaciente"];	
			$Estado='2';//RESERVADO  
		}else{
			$swReserva='0';//NO RESERVADO			
			$IdPacienteReserva='0';
			$Estado='1';//DISPONIBLE
		}		

		$FechaReserva='';
		$UsuReserva='';		
		$MotivoEstado='';
		$UsuMotivo='';
		$FecMotivo='';
		$ResultadoCustodia='';
		$Observacion='';
		$UsuReg=$_REQUEST['IdEmpleado'];	
		$FecReg=$FechaServidor;

	  	GuardarMovSangreDetalleM($MovNumero, $MovTipo, $IdGrupoSanguineo, $NroDonacion, $NroTabuladora, $TipoHem, $SNCS, $FechaExtraccion, $FechaVencimiento, $VolumenTotRecol, $VolumenFraccion, $VolumenRestante, $IdTipoDonacion, $IdTipoDonante, $swReserva, $IdPacienteReserva, $FechaReserva, $UsuReserva, $Estado, $MotivoEstado, $UsuMotivo, $FecMotivo, $ResultadoCustodia, $Observacion, $UsuReg, $FecReg); 		 

	  	//Actualizar Verificacion Aptos
	  	if($tipohem=='PG'){
	  		$SNCS_PG=$arrayComponentesEnvio[$i];//PG-0314585
	  		$SNCS_PFC='';
	  		$SNCS_PQ='';
	  	}else if($tipohem=='PFC'){
	  		$SNCS_PG='';
	  		$SNCS_PFC=$arrayComponentesEnvio[$i];//PFC-0314585
	  		$SNCS_PQ='';
	  	}else if($tipohem=='PQ'){
	  		$SNCS_PG='';
	  		$SNCS_PFC='';
	  		$SNCS_PQ=$arrayComponentesEnvio[$i];//PQ-0314585
	  	}	  	
	 	SIGESA_BSD_UpdVerificacionAptos_M($SNCS_PG, $SNCS_PFC, $SNCS_PQ);	 	
	 	
	  }


  	  $mensaje="Hemocomponentes liberados correctamente";
	  print "<script>alert('$mensaje')</script>";	
	  include('../../MVC_Vista/BancoSangre/VerificacionAptos.php');	
 }

if($_REQUEST["acc"] == "StockSangre") 
{ 		
	include('../../MVC_Vista/BancoSangre/StockSangre.php');
}

if($_REQUEST["acc"] == "EnviarHemocomponentes") 
{ 
	$IdEstablecimientoEgreso=isset($_REQUEST['IdEstablecimientoEgresoB']) ? $_REQUEST['IdEstablecimientoEgresoB'] : '';

	$FechaSalidaB=isset($_REQUEST['FechaSalidaB']) ? $_REQUEST['FechaSalidaB'] :  date('d/m/Y');
	$FechaSalida=gfecha($FechaSalidaB);
	//$BuscarSNCSEnviado=BuscarSNCSEnviado_M($IdEstablecimientoEgreso,$FechaSalida);
	include('../../MVC_Vista/BancoSangre/EnviarHemocomponentes.php');
}

if($_REQUEST["acc"] == "GuardarEnviarHemocomponentes") 
{ 
	$TipoHem=$_REQUEST['TipoHem'];
	$SNCS=$_REQUEST['SNCS'];
	$BuscarSNCS=BuscarSNCSDisponible_M($TipoHem,$SNCS);

	if($BuscarSNCS!=NULL){

		$XFechaVencimiento=$BuscarSNCS[0]["FechaVencimiento"];//2018-05-08 00:00:00.000

		if(substr($XFechaVencimiento,11,5)=='00:00'){
			$FechaVencimiento=substr($XFechaVencimiento,0,10).' 23:59';
		}else{
			$FechaVencimiento=$XFechaVencimiento;
		}

		$fechaActual=$FechaServidor; //2018-07-12 11:43:56.137
		if($FechaVencimiento<$fechaActual){
			 $mensaje="Hemocomponente ".$TipoHem."-".$SNCS." Venció el ".$FechaVencimiento;
	  		 print "<script>alert('$mensaje')</script>";	
		}else{			
			//Datos GuardarCabecera
		 	//$MovNumero=$_REQUEST['MovNumero'];//
			$MovTipo='E';
			$TipoHem=$TipoHem; 
			$IdEstablecimientoIngreso=''; //6216	06218	Hosp. Nac.daniel A. Carrion
			$IdEstablecimientoEgreso=$_REQUEST['IdEstablecimientoEgreso'];
			$UsuarioRecibe='';
			$FechaRecibe='';
			$IdPaciente='';
			$UsuarioSalida=$_REQUEST['UsuarioSalida'];
			$FechaSalida=gfecha($_REQUEST['FechaSalida']);
			//$FechaPreparaCrio=NULL;
			$FechaPreparaFraccion=NULL;
			$MetodoPreparaFraccion='';
			$Estado='1';
			$Observacion='';
			$UsuReg=$_REQUEST['IdEmpleado'];	
			$FecReg=$FechaServidor;

			$MovNumero=GuardarMovSangreCabeceraM($MovTipo, $TipoHem, $IdEstablecimientoIngreso, $IdEstablecimientoEgreso, $UsuarioRecibe, $FechaRecibe, $IdPaciente, $UsuarioSalida, $FechaSalida, $FechaPreparaFraccion, $MetodoPreparaFraccion, $Estado, $Observacion, $UsuReg, $FecReg);		
		  	
		  	//Inicio Obtener Datos Donacion con $NumSNCS
		  	$IdGrupoSanguineo=$BuscarSNCS[0]["IdGrupoSanguineo"];
		  	$IdTipoDonacion=$BuscarSNCS[0]["IdTipoDonacion"];
		  	$IdTipoDonante=$BuscarSNCS[0]["IdTipoDonante"];

		  	$NroDonacion=$BuscarSNCS[0]["NroDonacion"];
		  	$NroTabuladora=$BuscarSNCS[0]["NroTabuladora"];
		  	$FechaExtraccion=$BuscarSNCS[0]["FechaExtraccion"];

		  	$FechaVencimiento=$BuscarSNCS[0]["FechaVencimiento"];

		  	$VolumenTotRecol=$BuscarSNCS[0]["VolumenTotRecol"]; 	
			$VolumenFraccion=$BuscarSNCS[0]["VolumenFraccion"];  	
			$VolumenRestante=$BuscarSNCS[0]["VolumenRestante"];  	
		  	//Fin Obtener Datos Donacion con $NumSNCS

		  	//Datos GuardarDetalle
		  	//$IdMovDetalle=$_REQUEST['IdMovDetalle'];
			$MovNumero=$MovNumero;
			$MovTipo='E';
			$IdGrupoSanguineo=$IdGrupoSanguineo;//
			$NroDonacion=$NroDonacion;
			$NroTabuladora=$NroTabuladora;
			$TipoHem=$TipoHem;
			$SNCS=$SNCS;
			$FechaExtraccion=$FechaExtraccion;
		
			$FechaVencimiento=$FechaVencimiento;

			$VolumenTotRecol=$VolumenTotRecol;		
			$VolumenFraccion=$VolumenFraccion;
			$VolumenRestante=$VolumenRestante;

			$IdTipoDonacion=$IdTipoDonacion;
			$IdTipoDonante=$IdTipoDonante;

			$swReserva='0';//NO RESERVADO
			$IdPacienteReserva='0';
			$Estado='1';//DISPONIBLE		

			$FechaReserva='';
			$UsuReserva='';		
			$MotivoEstado='';
			$UsuMotivo='';
			$FecMotivo='';
			$ResultadoCustodia='';
			$Observacion='';
			$UsuReg=$_REQUEST['IdEmpleado'];	
			$FecReg=$FechaServidor;

		  	GuardarMovSangreDetalleM($MovNumero, $MovTipo, $IdGrupoSanguineo, $NroDonacion, $NroTabuladora, $TipoHem, $SNCS, $FechaExtraccion, $FechaVencimiento, $VolumenTotRecol, $VolumenFraccion, $VolumenRestante, $IdTipoDonacion, $IdTipoDonante, $swReserva, $IdPacienteReserva, $FechaReserva, $UsuReserva, $Estado, $MotivoEstado, $UsuMotivo, $FecMotivo, $ResultadoCustodia, $Observacion, $UsuReg, $FecReg); 	

		  	$EstadoActual='5';//DONADO
		  	//$IdMovDetalle=$BuscarSNCS[0]["IdMovDetalle"];
		  	//CambiarEstadoMovSangreDetalle($IdMovDetalle,$EstadoActual, $MovNumero);

		  	$MovNumeroIng=$BuscarSNCS[0]["MovNumero"];
		  	CambiarEstadoMovSangreDetalle($TipoHem, $SNCS, $EstadoActual, $MovNumeroIng);
		  	$mensaje="Hemocomponente Enviado correctamente";
		 	print "<script>alert('$mensaje')</script>";
		}
  		
  	}else{
  	  $mensaje="Hemocomponente NO Existe O NO está Disponible";
	  print "<script>alert('$mensaje')</script>";	  
  	}

  	$IdEstablecimientoEgreso=isset($_REQUEST['IdEstablecimientoEgreso']) ? $_REQUEST['IdEstablecimientoEgreso'] : '';

  	$FechaSalidaB=isset($_REQUEST['FechaSalida']) ? $_REQUEST['FechaSalida'] :  date('d/m/Y');
	$FechaSalida=gfecha($FechaSalidaB);	
	//$BuscarSNCSEnviado=BuscarSNCSEnviado_M($IdEstablecimientoEgreso,$FechaSalida);
	include('../../MVC_Vista/BancoSangre/EnviarHemocomponentes.php');  
}

if($_REQUEST["acc"] == "ImprimirEnvio") 
{ 
	include('../../MVC_Vista/BancoSangre/ImprimirEnvio_pdf.php'); 
}

if($_REQUEST["acc"] == "RecibirHemocomponentes") 
{ 
	$IdEstablecimientoIngreso=isset($_REQUEST['IdEstablecimientoIngresoB']) ? $_REQUEST['IdEstablecimientoIngresoB'] : '';

	$FechaRecibeB=isset($_REQUEST['FechaRecibeB']) ? $_REQUEST['FechaRecibeB'] :  date('d/m/Y');
	$FechaRecibe=gfecha($FechaRecibeB);			
	//$BuscarSNCSEnviado=BuscarSNCSEnviado_M($IdEstablecimientoEgreso,$FechaSalida);
	//
		$ListarParametrosTIDPG=SIGESA_ListarParametros_M('TIDPG');
	    $TIDPG=$ListarParametrosTIDPG[0]["Valor1"];// 42

	    $ListarParametrosTIDPFC=SIGESA_ListarParametros_M('TIDPFC');
	    $TIDPFC=$ListarParametrosTIDPFC[0]["Valor1"];// 365

	    $ListarParametrosTIDPQ=SIGESA_ListarParametros_M('TIDPQ');
	    $TIDPQ=$ListarParametrosTIDPQ[0]["Valor1"];// 5		
	//
	include('../../MVC_Vista/BancoSangre/RecibirHemocomponentes.php');
}

if($_REQUEST["acc"] == "GuardarRecibirHemocomponentes") 
{ 
	$TipoHem=$_REQUEST['TipoHem'];
	$SNCS=$_REQUEST['SNCS'];
	
	//VALIDAR QUE NO SE INGRESEN HEMOCOMPONENTES CON SNCS IGUAL QUE ESTADO<>1(DISPONIBLE), 2(RESERVADO),3(FRACCION),4(CUSTODIA),6(TRANFUNDIDO PACIENTE)
	$ValidarStock=BuscarSNCSTodos_M($TipoHem,$SNCS,'V');
	//Obtener ultimo estado del hemocomponente:		
	$POSult=count($ValidarStock)-1;//ULTIMO REGISTRO
	$Estado=$ValidarStock[$POSult]["Estado"];
		
	if($Estado=='1' || $Estado=='2' || $Estado=='3' || $Estado=='4' || $Estado=='6'){ //$Estado=='5' || 		
		$EstadoDescripcion=strip_tags(DescripcionEstadoHemocomponente($Estado));
	 	$mensaje="Ya existe el mismo Hemocomponente con estado ".$EstadoDescripcion. " en el Sistema";
	 	print "<script>alert('$mensaje')</script>";	

	}else{	

		//Datos GuardarCabecera
	 	//$MovNumero=$_REQUEST['MovNumero'];//
		$MovTipo='I';
		$TipoHem=$TipoHem; 
		$IdEstablecimientoIngreso=$_REQUEST['IdEstablecimientoIngreso']; //6216	06218	Hosp. Nac.daniel A. Carrion
		$IdEstablecimientoEgreso='';
		$UsuarioRecibe=$_REQUEST['UsuarioRecibe'];
		$FechaRecibe=gfecha($_REQUEST['FechaRecibe']);
		$IdPaciente='';
		$UsuarioSalida='';
		$FechaSalida=NULL;
		//$FechaPreparaCrio=NULL;
		$FechaPreparaFraccion=NULL;
		$MetodoPreparaFraccion='';
		$Estado='1';
		$Observacion='';
		$UsuReg=$_REQUEST['IdEmpleado'];	
		$FecReg=$FechaServidor;

		$MovNumero=GuardarMovSangreCabeceraM($MovTipo, $TipoHem, $IdEstablecimientoIngreso, $IdEstablecimientoEgreso, $UsuarioRecibe, $FechaRecibe, $IdPaciente, $UsuarioSalida, $FechaSalida, $FechaPreparaFraccion, $MetodoPreparaFraccion, $Estado, $Observacion, $UsuReg, $FecReg);	  	

	  	//Datos GuardarDetalle
	  	//$IdMovDetalle=$_REQUEST['IdMovDetalle'];
		$MovNumero=$MovNumero;
		$MovTipo='I';
		$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo'];//
		$NroDonacion=$_REQUEST['NroDonacion'];
		$NroTabuladora=$_REQUEST['NroTabuladora'];
		$TipoHem=$TipoHem;
		$SNCS=$SNCS;
		$FechaExtraccion=gfecha($_REQUEST['FechaExtraccion']);

		$FechaVencimiento=gfecha($_REQUEST['FechaVencimiento']);
		
		$VolumenTotRecol=$_REQUEST['VolumenTotRecol'];		
		$VolumenFraccion=0;
		$VolumenRestante=$_REQUEST['VolumenTotRecol'];

		$IdTipoDonacion='';
		$IdTipoDonante='';

		$swReserva='0';//NO RESERVADO
		$IdPacienteReserva='0';
		$Estado='1';//DISPONIBLE		

		$FechaReserva='';
		$UsuReserva='';		
		$MotivoEstado='';
		$UsuMotivo='';
		$FecMotivo='';
		$ResultadoCustodia='';
		$Observacion='';
		$UsuReg=$_REQUEST['IdEmpleado'];	
		$FecReg=$FechaServidor;

	  	GuardarMovSangreDetalleM($MovNumero, $MovTipo, $IdGrupoSanguineo, $NroDonacion, $NroTabuladora, $TipoHem, $SNCS, $FechaExtraccion, $FechaVencimiento, $VolumenTotRecol, $VolumenFraccion, $VolumenRestante, $IdTipoDonacion, $IdTipoDonante, $swReserva, $IdPacienteReserva, $FechaReserva, $UsuReserva, $Estado, $MotivoEstado, $UsuMotivo, $FecMotivo, $ResultadoCustodia, $Observacion, $UsuReg, $FecReg); 
	  	
	  	$mensaje="Hemocomponente Recibido correctamente";
	 	print "<script>alert('$mensaje')</script>";  	
	}   

	$IdEstablecimientoIngreso=isset($_REQUEST['IdEstablecimientoIngreso']) ? $_REQUEST['IdEstablecimientoIngreso'] : '';

  	$FechaRecibeB=isset($_REQUEST['FechaRecibe']) ? $_REQUEST['FechaRecibe'] :  date('d/m/Y');
	$FechaRecibe=gfecha($FechaRecibeB);	
	//$BuscarSNCSRecibido=BuscarSNCSRecibido_M($IdEstablecimientoEgreso,$FechaSalida);
	//
		$ListarParametrosTIDPG=SIGESA_ListarParametros_M('TIDPG');
	    $TIDPG=$ListarParametrosTIDPG[0]["Valor1"];// 42

	    $ListarParametrosTIDPFC=SIGESA_ListarParametros_M('TIDPFC');
	    $TIDPFC=$ListarParametrosTIDPFC[0]["Valor1"];// 365

	    $ListarParametrosTIDPQ=SIGESA_ListarParametros_M('TIDPQ');
	    $TIDPQ=$ListarParametrosTIDPQ[0]["Valor1"];// 5		
	// 
	include('../../MVC_Vista/BancoSangre/RecibirHemocomponentes.php');  	
}

if($_REQUEST["acc"] == "EliminarHemocomponentes") 
{ //Eliminar y convertir a crio
	$TipoHem=isset($_REQUEST['TipoHemB']) ? $_REQUEST['TipoHemB'] : '';
	$SNCS=isset($_REQUEST['SNCSB']) ? trim($_REQUEST['SNCSB']) : '';
	//BuscarSNCSDisponible_M($TipoHem,$SNCS);
	include('../../MVC_Vista/BancoSangre/EliminarHemocomponentes.php');
}

if($_REQUEST["acc"] == "GuardarEliminarHemocomponentes") 
{ 
	$TipoHem=$_REQUEST['TipoHem'];
	$SNCS=$_REQUEST['SNCS'];
	$FecMotivo=gfecha($_REQUEST['FecMotivo']).' '.$_REQUEST['HoraMotivo'];
	$UsuMotivo=$_REQUEST['UsuMotivo'];
	$MotivoEstado=$_REQUEST['MotivoEstado'];
	$Estado=0; //ELIMINA
	$MovNumero=$_REQUEST['MovNumero'];
	GuardarEliminarHemocomponentesM($TipoHem,$SNCS,$FecMotivo,$UsuMotivo,$MotivoEstado,$Estado,$MovNumero);

	//BuscarSNCSDisponible_M($TipoHem,$SNCS);
	include('../../MVC_Vista/BancoSangre/EliminarHemocomponentes.php');
}

if($_REQUEST["acc"] == "GuardarCrioHemocomponentes") 
{ 
	$TipoHem=$_REQUEST['TipoHem2'];
	$SNCS=$_REQUEST['SNCS2'];
	$FechaConvirtio=gfecha($_REQUEST['FechaConvirtio']);
	$UsuConvirtio=$_REQUEST['UsuConvirtio'];
	//$MotivoEstado='61';//PFC se convirtio a CRIO
	//$Estado=0; //ELIMINA
	$MovNumero=$_REQUEST['MovNumero2'];		
	$VolumenRestante=$_REQUEST['VolumenRestante2'];	
	GuardarCrioHemocomponentesM($TipoHem,$SNCS,$FechaConvirtio,$UsuConvirtio,$MovNumero,$VolumenRestante);//CONVERTIR PFC

	//BuscarSNCSDisponible_M($TipoHem,$SNCS);
	include('../../MVC_Vista/BancoSangre/EliminarHemocomponentes.php');
}

if($_REQUEST["acc"] == "FraccionarHemocomponentes") 
{ 
	$TipoHem=isset($_REQUEST['TipoHemB']) ? $_REQUEST['TipoHemB'] : 'PG';
	$SNCS=isset($_REQUEST['SNCSB']) ? trim($_REQUEST['SNCSB']) : '';
	//BuscarSNCSDisponible_M($TipoHem,$SNCS);
	include('../../MVC_Vista/BancoSangre/FraccionarHemocomponentes.php');
}

if($_GET["acc"] == "GuardarFraccionarHemocomponentes"){	

	$TipoHem=$_REQUEST['TipoHem'];
	$SNCS=$_REQUEST['SNCS'];	

	$BuscarSNCSDisponible=BuscarSNCSDisponible_M($TipoHem,$SNCS);//detalles disponibles
		//Datos GuardarCabecera
	 	//$MovNumero=$_REQUEST['MovNumero'];//
		$MovTipo='F';//Fracción Pediátrica	
		$TipoHem=$TipoHem; 
		$IdEstablecimientoIngreso=$BuscarSNCSDisponible[0]['IdEstablecimientoIngreso']; //6216	06218	Hosp. Nac.daniel A. Carrion
		$IdEstablecimientoEgreso='';
		$UsuarioRecibe='';
		$FechaRecibe='';
		$IdPaciente='';
		$UsuarioSalida='';
		$FechaSalida='';
		//$FechaPreparaCrio=NULL;
		$FechaPreparaFraccion=gfecha($_REQUEST['FechaPreparaFraccion']).' '.$_REQUEST['HoraPreparaFraccion'];
		$MetodoPreparaFraccion=$_REQUEST['MetodoPreparaFraccion'];
		$Estado='1';
		
		$Observacion='';
		$UsuReg=$_REQUEST['IdEmpleado'];	
		$FecReg=$FechaServidor;

		$MovNumero=GuardarMovSangreCabeceraM($MovTipo, $TipoHem, $IdEstablecimientoIngreso, $IdEstablecimientoEgreso, $UsuarioRecibe, $FechaRecibe, $IdPaciente, $UsuarioSalida, $FechaSalida, $FechaPreparaFraccion, $MetodoPreparaFraccion, $Estado, $Observacion, $UsuReg, $FecReg);	  	

	  	//Datos GuardarDetalle
	  	//$IdMovDetalle=$_REQUEST['IdMovDetalle'];
		$MovNumero=$MovNumero;
		$MovTipo='F';//Fracción Pediátrica	
		$IdGrupoSanguineo=$BuscarSNCSDisponible[0]['IdGrupoSanguineo'];
		$NroDonacion=$BuscarSNCSDisponible[0]['NroDonacion'];
		$NroTabuladora=$BuscarSNCSDisponible[0]['NroTabuladora'];
		$TipoHem=$TipoHem;
		$SNCS=$SNCS;
		$FechaExtraccion=$BuscarSNCSDisponible[0]['FechaExtraccion'];

		$FechaVencimiento=gfecha($_REQUEST['FechaVencimiento']).' '.$_REQUEST['HoraVencimiento'];
		
		$VolumenTotRecol=$_REQUEST['VolumenFraccion'];		
		$VolumenFraccion=0;
		$VolumenRestante=$_REQUEST['VolumenFraccion'];

		$IdTipoDonacion='';
		$IdTipoDonante='';

		$swReserva='0';//NO RESERVADO
		$IdPacienteReserva='0';
		$Estado='3'; //Fracción Pediátrica	

		$FechaReserva='';
		$UsuReserva='';		
		$MotivoEstado='';
		$UsuMotivo='';
		$FecMotivo='';
		$ResultadoCustodia='';
		$Observacion='';
		$UsuReg=$_REQUEST['IdEmpleado'];	
		$FecReg=$FechaServidor;

	  	GuardarMovSangreDetalleM($MovNumero, $MovTipo, $IdGrupoSanguineo, $NroDonacion, $NroTabuladora, $TipoHem, $SNCS, $FechaExtraccion, $FechaVencimiento, $VolumenTotRecol, $VolumenFraccion, $VolumenRestante, $IdTipoDonacion, $IdTipoDonante, $swReserva, $IdPacienteReserva, $FechaReserva, $UsuReserva, $Estado, $MotivoEstado, $UsuMotivo, $FecMotivo, $ResultadoCustodia, $Observacion, $UsuReg, $FecReg); 
	  	
	  	UpdateVolumenPorFraccion($_REQUEST['IdMovDetalle'],$_REQUEST['VolumenFraccion'],$FechaVencimiento);

	  	$mensaje="Hemocomponente Fraccionado correctamente";
	 	print "<script>alert('$mensaje')</script>";    	

	$TipoHem=isset($_REQUEST['TipoHem']) ? $_REQUEST['TipoHem'] : 'PG';
	$SNCS=isset($_REQUEST['SNCS']) ? trim($_REQUEST['SNCS']) : '';
	//BuscarSNCSDisponible_M($TipoHem,$SNCS);
	include('../../MVC_Vista/BancoSangre/FraccionarHemocomponentes.php');
}

if($_GET["acc"] == "verdetalleFraccion"){		
	$IdMovDetalle=$_GET['IdMovDetalle']; 	
	$ObtenerDatosIdMovDetalle=ObtenerDatosIdMovDetalle_M($IdMovDetalle);	
	$SNCS=$ObtenerDatosIdMovDetalle[0]['SNCS']; 
	$TipoHem=$ObtenerDatosIdMovDetalle[0]['TipoHem'];	
	$BuscarSNCSFraccion=BuscarSNCSFraccion_M($TipoHem,$SNCS);

	if($BuscarSNCSFraccion!=NULL){
		echo json_encode($BuscarSNCSFraccion);
	}else{
		$mensaje="Hemocomponente NO ha sido Fraccionado";
	 	print "<script>alert('$mensaje')</script>"; 
	}
}

if($_REQUEST["acc"] == "CustodiaHemocomponentes") 
{ 
	$TipoHem=isset($_REQUEST['TipoHemB']) ? $_REQUEST['TipoHemB'] : '';
	$SNCS=isset($_REQUEST['SNCSB']) ? trim($_REQUEST['SNCSB']) : '';

	$EstadoB=isset($_REQUEST['EstadoB']) ? trim($_REQUEST['EstadoB']) : 'V';//Por Defecto Todos
	//$BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);
	include('../../MVC_Vista/BancoSangre/CustodiaHemocomponentes.php');
}

if($_REQUEST["acc"] == "GuardarCustodiaHemocomponentes") 
{ 
	$TipoHem=$_REQUEST['TipoHem'];
	$SNCS=$_REQUEST['SNCS'];
	$FecMotivo=gfecha($_REQUEST['FecMotivo']).' '.$_REQUEST['HoraMotivo'];
	$UsuMotivo=$_REQUEST['UsuMotivo'];
	$MotivoEstado=$_REQUEST['MotivoEstado'];
	$Estado=4; //CUSTODIA
	$MovNumero=$_REQUEST['MovNumero'];
	GuardarEliminarHemocomponentesM($TipoHem,$SNCS,$FecMotivo,$UsuMotivo,$MotivoEstado,$Estado,$MovNumero);

	$EstadoB=isset($_REQUEST['EstadoB']) ? trim($_REQUEST['EstadoB']) : 'V';//Por Defecto Todos
	//$BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);	
	include('../../MVC_Vista/BancoSangre/CustodiaHemocomponentes.php');
}

if($_REQUEST["acc"] == "GuardarResultadoCustodia") 
{ 
	$TipoHem=$_REQUEST['TipoHem2'];
	$SNCS=$_REQUEST['SNCS2'];
	$FecResultado=gfecha($_REQUEST['FecResultado']).' '.$_REQUEST['HoraResultado'];
	$UsuResultado=$_REQUEST['UsuResultado'];
	$ResultadoCustodia=$_REQUEST['ResultadoCustodia'];
	$Estado=$_REQUEST['Estado2']; //0=ELIMINADO O 1=DISPONIBLE
	$MovNumero=$_REQUEST['MovNumero2'];
	GuardarResultadoCustodia_M($TipoHem,$SNCS,$FecResultado,$UsuResultado,$ResultadoCustodia,$Estado,$MovNumero);
	
	$EstadoB=isset($_REQUEST['EstadoB']) ? trim($_REQUEST['EstadoB']) : 'V';//Por Defecto Todos
	//$BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);	
	include('../../MVC_Vista/BancoSangre/CustodiaHemocomponentes.php');
}

if($_REQUEST["acc"] == "AgregarQuitarReserva") 
{ 
	$TipoHem=isset($_REQUEST['TipoHemB']) ? $_REQUEST['TipoHemB'] : '';
	$SNCS=isset($_REQUEST['SNCSB']) ? trim($_REQUEST['SNCSB']) : '';

	$EstadoB=isset($_REQUEST['EstadoB']) ? trim($_REQUEST['EstadoB']) : 'V';//Por Defecto Todos
	//$BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);
	include('../../MVC_Vista/BancoSangre/AgregarQuitarReserva.php');
}

if($_REQUEST["acc"] == "GuardarAgregarReserva") 
{ 
	$TipoHem=$_REQUEST['TipoHem'];
	$SNCS=$_REQUEST['SNCS'];

	$IdMovDetalle=$_REQUEST['IdMovDetalle'];
	$MovNumero=$_REQUEST['MovNumero'];
	$IdPacienteReserva=$_REQUEST['IdPaciente'];
	$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineoRec'];
	$UsuReserva=$_REQUEST['UsuReserva'];
	$FechaReserva=gfecha($_REQUEST['FechaReserva']).' '.$_REQUEST['HoraReserva'];
	$MotivoAgregarReserva=$_REQUEST['MotivoAgregarReserva'];
	$Estado='2'; //2=RESERVADA
	GuardarAgregarReserva_M($TipoHem,$SNCS,$IdMovDetalle,$MovNumero,$IdPacienteReserva,$UsuReserva,$FechaReserva,$MotivoAgregarReserva,$Estado);

	//INICIO GUARDAR DATOS DEL RECEPTOR
	$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
	$UsuReg=$_REQUEST['IdEmpleado'];
	$IdPaciente=$_REQUEST['IdPaciente'];
	$NroHistoria=$_REQUEST['NroHistoria'];
	$NroDocumento=$_REQUEST['NroDocumento'];
	//$BuscarReceptor=SIGESA_BSD_BuscarReceptor_M($NroHistoria,'NroHistoriaClinica');
	$BuscarReceptor=SIGESA_BSD_BuscarReceptorFiltro_M($IdPaciente,'IdPaciente');
	if($BuscarReceptor!=NULL){ //ACTUALIZAR RECEPTOR
		$NewIdReceptor=$BuscarReceptor[0]["IdReceptor"];
		GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg);

	}else{  //REGISTRAR RECEPTOR
		//$NewIdReceptor=$AutoIdReceptor;
		$NewIdReceptor=GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg);				
	}	
	$GrupoSanguineo=$_REQUEST['GrupoSanguineoRec'];//text A+,A-...
	UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo);		
	//FIN GUARDAR DATOS DEL RECEPTOR	

	$EstadoB=isset($_REQUEST['EstadoB']) ? trim($_REQUEST['EstadoB']) : 'V';//Por Defecto Todos
	//$BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);
	include('../../MVC_Vista/BancoSangre/AgregarQuitarReserva.php');
}

if($_REQUEST["acc"] == "GuardarQuitarReserva") 
{ 
	$TipoHem=$_REQUEST['TipoHem2'];
	$SNCS=$_REQUEST['SNCS2'];

	$IdMovDetalle=$_REQUEST['IdMovDetalle2'];
	$MovNumero=$_REQUEST['MovNumero2'];
	//$IdPacienteReserva=$_REQUEST['IdPaciente'];
	//$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineoRec'];
	$UsuQuitarReserva=$_REQUEST['UsuQuitarReserva'];
	$FecQuitarReserva=gfecha($_REQUEST['FecQuitarReserva']).' '.$_REQUEST['HoraQuitarReserva'];
	$MotivoQuitarReserva=$_REQUEST['MotivoQuitarReserva'];
	$Estado=$_REQUEST['Estado2']; //1=DISPONIBLE
	GuardarQuitarReserva_M($TipoHem,$SNCS,$IdMovDetalle,$MovNumero,$UsuQuitarReserva,$FecQuitarReserva,$MotivoQuitarReserva,$Estado);

	$EstadoB=isset($_REQUEST['EstadoB']) ? trim($_REQUEST['EstadoB']) : 'V';//Por Defecto Todos
	//$BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);
	include('../../MVC_Vista/BancoSangre/AgregarQuitarReserva.php');
}

if ($_REQUEST["acc"] == 'ListarEstablecimientos') {		

		$data = array();
		$Establecimientos=array();

		$texto = isset($_POST['q']) ? $_POST['q'] : ''; 
		$data = ListarEstablecimientos_M(0,$texto);		

		for ($i=0; $i < count($data); $i++) { 
			//$Datos=$data[$i]["IdMotivoRechazo"].'|'.$data[$i]["Diferimiento"].'|'.$data[$i]["Tiempo"].'|'.$data[$i]["TipoTiempo"];
			$Establecimientos[$i] = array(				
				'IdEstablecimiento'	=>	$data[$i]["IdEstablecimiento"],
				'Codigo' =>	$data[$i]["Codigo"],
				'Nombre'	=>	$data[$i]["Nombre"]
			);
		}

		if($Establecimientos!=NULL){
			echo json_encode($Establecimientos);
		}else{
			$mensaje="Establecimiento NO existe";
		 	print "<script>alert('$mensaje')</script>"; 
		}
		//echo json_encode($Establecimientos);
		//exit();
}


if($_REQUEST["acc"] == "VerDetalleStock") {

	$TipoHem=$_REQUEST["TipoHem"];
	$IdGrupoSanguineo=isset($_REQUEST['IdGrupoSanguineo']) ? $_REQUEST['IdGrupoSanguineo'] : '0'; 
	$Estado=$_REQUEST["Estado"];
	$Vencido=$_REQUEST["Vencido"];
	$SIGESA_BSD_BuscarStock=SIGESA_BSD_BuscarStock_M($TipoHem,$IdGrupoSanguineo,$Estado,$Vencido);
	include('../../MVC_Vista/BancoSangre/VerDetalleStock.php');
}

///////////////////////////////////

if($_REQUEST["acc"] == "RegistrarSolicitud") 
{ 
	include('../../MVC_Vista/BancoSangre/RegistrarSolicitud.php');
}

if($_REQUEST["acc"] == "GuardarSolicitudSangre") 
{
	
	//VALIDAR QUE NO TENGA OTRA SOLICITUD PENDIENTE
	$SolicitudesPendientes=ListarCabeceraSolicitudes_M($_REQUEST['NroHistoriaClinica']);			
	if($SolicitudesPendientes!=NULL){		
		$mensaje="El Paciente TIENE Pendiente la Solicitud N° ".$SolicitudesPendientes[0]["NroSolicitud"];
		print "<script>alert('$mensaje')</script>"; 
	}else{	

		//INICIO GUARDAR DATOS DEL RECEPTOR
		$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
		$UsuReg=$_REQUEST['IdEmpleado'];
		$IdPaciente=$_REQUEST['IdPaciente'];
		$NroHistoria=$_REQUEST['NroHistoriaClinica'];
		$NroDocumento=$_REQUEST['NroDocumento'];
		//$BuscarReceptor=SIGESA_BSD_BuscarReceptor_M($NroHistoria,'NroHistoriaClinica');
		$BuscarReceptor=SIGESA_BSD_BuscarReceptorFiltro_M($IdPaciente,'IdPaciente');
		if($BuscarReceptor!=NULL){ //ACTUALIZAR RECEPTOR
			$NewIdReceptor=$BuscarReceptor[0]["IdReceptor"];
			GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $_REQUEST['IdGrupoSanguineoRec'], $FechaReg, $UsuReg);

		}else{  //REGISTRAR RECEPTOR			
			$NewIdReceptor=GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $_REQUEST['IdGrupoSanguineoRec'], $FechaReg, $UsuReg);				
		}	
		$GrupoSanguineo=$_REQUEST['GrupoSanguineoRec'];//text A+,A-...
		UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo);		
		//FIN GUARDAR DATOS DEL RECEPTOR

		//Datos Cabecera SIGESA_BSD_SolicitudSangreCabecera 
		//$IdSolicitudSangre=$_REQUEST['IdSolicitudSangre'];
		$IdReceptor=$NewIdReceptor;
		$IdPaciente=$_REQUEST['IdPaciente'];
		$NroHistoria=$_REQUEST['NroHistoriaClinica'];
		$IdCuentaAtencion=$_REQUEST['IdCuentaAtencion'];
		$IdAtencion=$_REQUEST['IdAtencion'];
		$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineoRec'];

		$FechaSolicitud=gfecha($_REQUEST['FechaSolicitud']).' '.$_REQUEST['HoraSolicitud'];
		$IdTipoServicio=$_REQUEST['IdTipoServicio'];
		$IdServicio=$_REQUEST['IdServicioHospital'];
		$CodigoCama=$_REQUEST['CodigoCama'];
		
		$UsuReg=$_REQUEST['IdEmpleado'];
		$FecReg=$FechaServidor;//date('Y-m-d H:i:s');
		$Estado='1';

		$IdSolicitudSangre=GuardarSolicitudSangreCabeceraM($IdReceptor, $IdPaciente, $NroHistoria, $IdCuentaAtencion, $IdAtencion, $IdGrupoSanguineo, $FechaSolicitud, $IdTipoServicio, $IdServicio, $CodigoCama,/* $IdDiagnostico, $swAnemia1, $swAnemia2, $Hemoglobina, $Hematocrito, $Plaquetas, $swTransPre, $swReacTrans,
		$EmbarazoPrevio, $Aborto, $IncompatibilidadMF, $swConsentimiendoInformado, $MotivoNOConsentimiendo, $MedicoSolicitante, $UsuTomoMuestraPaciente,*/
		$UsuReg, $FecReg, $Estado);

		//MAESTROS
		$SepararPG=SIGESA_ListarParametros_M('SPG');
		$ValorPG=$SepararPG[0]["Valor1"]; //SI 

		$SepararGRlav=SIGESA_ListarParametros_M('SGRlav');
		$ValorGRlav=$SepararGRlav[0]["Valor1"]; //SI 

		//Datos Detalle SIGESA_BSD_SolicitudSangreDetalle
		//$IdDetSolicitudSangre=$_REQUEST['IdDetSolicitudSangre'];
		$IdSolicitudSangre=$IdSolicitudSangre;
		$UsuReg=$_REQUEST['IdEmpleado'];
		$FecReg=$FechaServidor;//date('Y-m-d H:i:s');*/

		$CantidadST=$_REQUEST['CantidadST'];
		if(trim($CantidadST)!="" && trim($CantidadST)!="0"){
			$CodigoComponente=$_REQUEST['CodigoST'];
			$Cantidad=$_REQUEST['CantidadST'];
			$TipoRequerimiento=$_REQUEST['TipoReqST'];			
			$FechaReservaOper=empty($_REQUEST['FechaReservaOperPST']) ? '' : gfecha($_REQUEST['FechaReservaOperST']).' '.$_REQUEST['HoraReservaOperST']; 
			$FechaProg=empty($_REQUEST['FechaProgST']) ? '' : gfecha($_REQUEST['FechaProgST']).' '.$_REQUEST['HoraProgST'] ;
			$Volumen=$_REQUEST['VolumenST']; 
			GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);
		}

		$CantidadPG=$_REQUEST['CantidadPG'];
		if(trim($CantidadPG)!="" && trim($CantidadPG)!="0"){
			//$CodigoComponente=$_REQUEST['CodigoPG'];
			//$Cantidad=$_REQUEST['CantidadPG'];			
			$TipoRequerimiento=$_REQUEST['TipoReqPG'];
			$FechaReservaOper=empty($_REQUEST['FechaReservaOperPG']) ? '' : gfecha($_REQUEST['FechaReservaOperPG']).' '.$_REQUEST['HoraReservaOperPG']; 
			$FechaProg=empty($_REQUEST['FechaProgPG']) ? '' : gfecha($_REQUEST['FechaProgPG']).' '.$_REQUEST['HoraProgPG'] ; 
			$Volumen=$_REQUEST['VolumenPG']; 
			if($ValorPG=='SI'){ //separar			
				for($i=1;$i<=$CantidadPG;$i++){	
					$CodigoComponente=$_REQUEST['CodigoPG'].'-'.$i;				
					$Cantidad=1;
					GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);
				}

			}else{	
				$CodigoComponente=$_REQUEST['CodigoPG'];			 
				$Cantidad=$_REQUEST['CantidadPG'];
				GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);			
			}		
		}
		
		$CantidadGRlav=$_REQUEST['CantidadGRlav'];
		if(trim($CantidadGRlav)!="" && trim($CantidadGRlav)!="0"){
			//$CodigoComponente=$_REQUEST['CodigoGRlav'];
			//$Cantidad=$_REQUEST['CantidadGRlav'];
			$TipoRequerimiento=$_REQUEST['TipoReqGRlav'];
			$FechaReservaOper=empty($_REQUEST['FechaReservaOperGRlav']) ? '' : gfecha($_REQUEST['FechaReservaOperGRlav']).' '.$_REQUEST['HoraReservaOperGRlav']; 
			$FechaProg=empty($_REQUEST['FechaProgGRlav']) ? '' : gfecha($_REQUEST['FechaProgGRlav']).' '.$_REQUEST['HoraProgGRlav']; 
			$Volumen=$_REQUEST['VolumenGRlav']; 
			if($ValorGRlav=='SI'){ //separar			
				for($i=1;$i<=$CantidadGRlav;$i++){
					$CodigoComponente=$_REQUEST['CodigoGRlav'].'-'.$i;					
					$Cantidad=1;
					GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);
				}

			}else{	
				$CodigoComponente=$_REQUEST['CodigoGRlav'];			 
				$Cantidad=$_REQUEST['CantidadGRlav'];
				GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);			
			}
		}

		$CantidadPFC=$_REQUEST['CantidadPFC'];
		if(trim($CantidadPFC)!="" && trim($CantidadPFC)!="0"){
			$CodigoComponente=$_REQUEST['CodigoPFC'];
			$Cantidad=$_REQUEST['CantidadPFC'];
			$TipoRequerimiento=$_REQUEST['TipoReqPFC'];
			$FechaReservaOper=empty($_REQUEST['FechaReservaOperPFC']) ? '' : gfecha($_REQUEST['FechaReservaOperPFC']).' '.$_REQUEST['HoraReservaOperPFC']; 
			$FechaProg=empty($_REQUEST['FechaProgPFC']) ? '' : gfecha($_REQUEST['FechaProgPFC']).' '.$_REQUEST['HoraProgPFC']; 
			$Volumen=$_REQUEST['VolumenPFC']; 
			GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);
		}
		$CantidadPQ=$_REQUEST['CantidadPQ'];
		if(trim($CantidadPQ)!="" && trim($CantidadPQ)!="0"){
			$CodigoComponente=$_REQUEST['CodigoPQ'];
			$Cantidad=$_REQUEST['CantidadPQ'];
			$TipoRequerimiento=$_REQUEST['TipoReqPQ'];
			$FechaReservaOper=empty($_REQUEST['FechaReservaOperPQ']) ? '' : gfecha($_REQUEST['FechaReservaOperPQ']).' '.$_REQUEST['HoraReservaOperPQ']; 
			$FechaProg=empty($_REQUEST['FechaProgPQ']) ? '' : gfecha($_REQUEST['FechaProgPQ']).' '.$_REQUEST['HoraProgPQ']; 
			$Volumen=$_REQUEST['VolumenPQ']; 
			GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);
		}
		$CantidadCRIO=$_REQUEST['CantidadCRIO'];
		if(trim($CantidadCRIO)!="" && trim($CantidadCRIO)!="0"){
			$CodigoComponente=$_REQUEST['CodigoCRIO'];
			$Cantidad=$_REQUEST['CantidadCRIO'];
			$TipoRequerimiento=$_REQUEST['TipoReqCRIO'];
			$FechaReservaOper=empty($_REQUEST['FechaReservaOperCRIO']) ? '' : gfecha($_REQUEST['FechaReservaOperCRIO']).' '.$_REQUEST['HoraReservaOperCRIO']; 
			$FechaProg=empty($_REQUEST['FechaProgCRIO']) ? '' : gfecha($_REQUEST['FechaProgCRIO']).' '.$_REQUEST['HoraProgCRIO']; 
			$Volumen=$_REQUEST['VolumenCRIO']; 
			GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen);
		}

		$mensaje="Solicitud Guardada Correctamente";
		print "<script>alert('$mensaje')</script>"; 
	}
	include('../../MVC_Vista/BancoSangre/RegistrarSolicitud.php');
}

if($_REQUEST["acc"] == "GuardarResumenSolicitudSangre") 
{ 
	//INICIO ACTUALIZAR DATOS DEL RECEPTOR
	/*$FechaReg=$FechaServidor;//date('Y-m-d H:i:s');
	$UsuReg=$_REQUEST['IdEmpleado'];
	$IdPaciente=$_REQUEST['IdPaciente'];
	$NroHistoria=$_REQUEST['NroHistoriaClinica'];
	$NroDocumento=$_REQUEST['NroDocumento'];
	
	$BuscarReceptor=SIGESA_BSD_BuscarReceptorFiltro_M($IdPaciente,'IdPaciente');
	$NewIdReceptor=$BuscarReceptor[0]["IdReceptor"];
	GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $_REQUEST['IdGrupoSanguineoRec'], $FechaReg, $UsuReg);

	$GrupoSanguineo=$_REQUEST['GrupoSanguineoRec'];//text A+,A-...
	UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo);*/	
	//FIN ACTUALIZAR DATOS DEL RECEPTOR

	//Datos Cabecera SIGESA_BSD_SolicitudSangreCabecera 
	$IdSolicitudSangre=$_REQUEST['IdSolicitudSangre'];
	/*$IdReceptor=$NewIdReceptor;
	$IdPaciente=$_REQUEST['IdPaciente'];
	$NroHistoria=$_REQUEST['NroHistoriaClinica'];
	$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineoRec'];*/

	$IdDiagnostico1=$_REQUEST['IdDiagnostico'];
	$IdDiagnostico2=$_REQUEST['IdDiagnostico2'];

	$Hemoglobina=$_REQUEST['Hemoglobina'];
	$Hematocrito=$_REQUEST['Hematocrito'];
	$swAnemia1=isset($_REQUEST['swAnemia1']) ? $_REQUEST['swAnemia1'] :  ''; 
	if($swAnemia1=='Otros'){
		$swAnemia1=$_REQUEST['OtroswAnemia1'];
	}	

	$swAnemia2=isset($_REQUEST['swAnemia2']) ? $_REQUEST['swAnemia2'] :  ''; 
	if($swAnemia2=='Otros'){
		$swAnemia2=$_REQUEST['OtroswAnemia2'];
	}
	 
	$Tromboplastina=empty($_REQUEST['Tromboplastina']) ? 0 : $_REQUEST['Tromboplastina'];
	$Protrombina=empty($_REQUEST['Protrombina']) ? 0 : $_REQUEST['Protrombina'];
	$swPlasma1=isset($_REQUEST['swPlasma1']) ? $_REQUEST['swPlasma1'] :  ''; 
	if($swPlasma1=='Otros'){
		$swPlasma1=$_REQUEST['OtroswPlasma1'];
	}	
	
	$Plaquetas=$_REQUEST['Plaquetas'];
	$swPQ1=isset($_REQUEST['swPQ1']) ? $_REQUEST['swPQ1'] :  ''; 
	if($swPQ1=='Otros'){
		$swPQ1=$_REQUEST['OtroswPQ1'];
	}

	$swTransPre=$_REQUEST['swTransPre'];
	$swReacTrans=$_REQUEST['swReacTrans'];
	$EmbarazoPrevio=isset($_REQUEST['EmbarazoPrevio']) ? $_REQUEST['EmbarazoPrevio'] : ''; 
	$Aborto=isset($_REQUEST['Aborto']) ? $_REQUEST['Aborto'] : '';
	$IncompatibilidadMF=isset($_REQUEST['IncompatibilidadMF']) ? $_REQUEST['IncompatibilidadMF'] : '';
	$swConsentimiendoInformado=$_REQUEST['swConsentimiendoInformado'];
	$MotivoNOConsentimiendo=$_REQUEST['MotivoNOConsentimiendo'];
	$MedicoSolicitante=$_REQUEST['MedicoSolicitante'];
	$UsuTomoMuestraPaciente=$_REQUEST['UsuTomoMuestraPaciente'];
	$UsuRegResumen=$_REQUEST['IdEmpleado'];
	$FecRegResumen=$FechaServidor;//date('Y-m-d H:i:s');
	$Estado='2';

	$swReacAler=$_REQUEST['swReacAler'];
	$PesoReceptor=$_REQUEST['PesoReceptor'];
	$INR=$_REQUEST['INR'];

	GuardarSolicitudSangreCabeceraResumenM($IdSolicitudSangre, $IdDiagnostico1, $IdDiagnostico2, $Hemoglobina, $Hematocrito, $swAnemia1, $swAnemia2, $Tromboplastina, $Protrombina, $swPlasma1, $Plaquetas, $swPQ1, 
	$swTransPre, $swReacTrans, $EmbarazoPrevio, $Aborto, $IncompatibilidadMF, $swConsentimiendoInformado, $MotivoNOConsentimiendo, $MedicoSolicitante, $UsuTomoMuestraPaciente, $UsuRegResumen, $FecRegResumen, $Estado, $swReacAler, $PesoReceptor, $INR);	

	$mensaje="Resumen de Solicitud Transfusional Guardada Correctamente";
	print "<script>alert('$mensaje')</script>"; 
	include('../../MVC_Vista/BancoSangre/RegistrarSolicitud.php');
}

if($_REQUEST["acc"] == "BuscarPacienteAtenciones") {
			
		$data = array();
		$DatosPacientes = array();		
		
		$texto = isset($_REQUEST['q']) ? $_REQUEST['q'] : ''; // the request parameter

				
		$data=SIGESA_BSD_BuscarPacienteAtenciones_M($texto,'NroHistoriaClinica');

		//VALIDAR QUE NO TENGA OTRA SOLICITUD PENDIENTE
		$SolicitudesPendientes=ListarCabeceraSolicitudes_M($texto);			
		if($SolicitudesPendientes!=NULL){
			$Paciente=SIGESA_BSD_Buscarpaciente_M($SolicitudesPendientes[0]["IdPaciente"],'IdPaciente');	
			$NombresPaciente=$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"].' '.$Paciente[0]["SegundoNombre"];
			$IdTipoSexo=$Paciente[0]["IdTipoSexo"];
			$DatosPacientes= array(
				'IdPaciente'	=>	$SolicitudesPendientes[0]["IdPaciente"],								
				'NroHistoria'       =>	$SolicitudesPendientes[0]["NroHistoria"],				
				'NombresPaciente'  =>   $NombresPaciente,
				'IdTipoSexo'	=>	$IdTipoSexo,
				'IdGrupoSanguineo'	=>	$SolicitudesPendientes[0]["IdGrupoSanguineo"],

				'IdCuentaAtencion'  =>  $SolicitudesPendientes[0]["IdCuentaAtencion"],
				'IdAtencion'  =>  $SolicitudesPendientes[0]["IdAtencion"],

				'NroSolicitud'	=>	$SolicitudesPendientes[0]["NroSolicitud"],
				'IdSolicitudSangre'	=>	$SolicitudesPendientes[0]["IdSolicitudSangre"],
			);
		}else{			
			
			for ($i=0; $i < count($data); $i++) { 
				$NombresPaciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].' '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];	
				$Edad=CalcularEdad($data[$i]["FechaNacimiento"]);//Y/m/d	
				$DatosPacientes= array(
					'NroHistoriaClinica'	=>	$data[$i]["NroHistoriaClinica"],
					//'ApellidoPaterno'	=>	mb_strtoupper($data[$i]["ApellidoPaterno"]),
					'NombresPaciente'	=>	mb_strtoupper($NombresPaciente),				
					'IdPaciente'	=>	$data[$i]["IdPaciente"],
					'NroDocumento'  =>	$data[$i]["NroDocumento"],
					'IdTipoSexo'	=>	$data[$i]["IdTipoSexo"],
					'GrupoSanguineo'	=>	$data[$i]["GrupoSanguineoRec"],
					'IdGrupoSanguineo' =>	$data[$i]["IdGrupoSanguineo"],
					'FechaNacimiento' =>	vfecha(substr($data[$i]["FechaNacimiento"],0,10)),
					'Edad' 			  => strip_tags($Edad),

					'NroSolicitud'      => '',
					'IdSolicitudSangre'	=> '',	
					/*'IdTipoServicio' =>	$IdTipoServicio,
					'IdServicio' =>	$IdServicio,
					'numerocama' =>	$numerocama,*/
					);
			}
		}
		
		echo json_encode($DatosPacientes);
		exit();		
}

if($_REQUEST["acc"] == "ListarDatosPacientesAtenciones") {
			
		$dataAtencion = array();
		$DatosPacientes = array();	

		$IdPaciente = isset($_REQUEST['IdPaciente']) ? $_REQUEST['IdPaciente'] : '';
				
		//datos atencion
		if($IdPaciente != 'undefined'){
		$dataAtencion=SIGESA_BSD_BuscarPacienteAtenciones_M($IdPaciente,'IdPaciente');//de la tabla atenciones					
			for ($i=0; $i < count($dataAtencion); $i++) {
				 $IdCuentaAtencion=$dataAtencion[$i]["IdCuentaAtencion"];
				 $NombreServicio=$dataAtencion[$i]["NombreServicio"];
				 $FechaIngreso=vfecha(substr($dataAtencion[$i]["FechaIngreso"],0,10));
				 $FuneteFinnac=$dataAtencion[$i]["FuneteFinnac"];
				 $diagnostico=$dataAtencion[$i]["diagnostico"];//cie10 
				 $IdAtencion=$dataAtencion[$i]["IdAtencion"];

				 $DatosPacientes[$i] = array(				
					'IdCuentaAtencion' =>	$IdCuentaAtencion,				
					'NombreServicio' =>	$NombreServicio,
					'FechaIngreso' =>	$FechaIngreso,
					'FuneteFinnac' =>	$FuneteFinnac,				
					'IdAtencion' =>	$IdAtencion,
					'diagnostico'=> $diagnostico,
				 );
			}

		}
		
		echo json_encode($DatosPacientes);
		exit();		
}

if($_REQUEST["acc"] == "ListarDatosPacientesAtencionesEstancia") {//FALTAA
			
		$data = array();
		$DatosPacientes = array();			

		$IdAtencion = isset($_REQUEST['IdAtencion']) ? $_REQUEST['IdAtencion'] : '';

		if($IdAtencion!=NULL){			
			//datos atencion
		    $dataAtencion=SIGESA_BSD_BuscarPacienteAtenciones_M($IdAtencion,'IdAtencion');//de la tabla atenciones
			if($dataAtencion!=NULL){
			   $IdTipoServicio=$dataAtencion[0]["IdTipoServicio"];
			   $IdServicioIngreso=$dataAtencion[0]["IdServicioIngreso"];
			   $numerocama=$dataAtencion[0]["numerocama"];
			   $Medico=$dataAtencion[0]["Medico"];
			   $diagnostico=$dataAtencion[0]["diagnostico"];//cie10 
			   $IdCuentaAtencion=$dataAtencion[0]["IdCuentaAtencion"];
			}else{
			   $IdTipoServicio="";
			   $IdServicioIngreso="";
			   $numerocama="";
			   $Medico="";
			   $diagnostico="";//cie10 
			   $IdCuentaAtencion="";
			}			
		}		
				
		for ($i=0; $i < count($dataAtencion); $i++) { 				
			$DatosPacientes = array(				
				'IdTipoServicio' =>	$IdTipoServicio,
				'IdServicioIngreso' =>	$IdServicioIngreso,
				'numerocama' =>	$numerocama,
				'Medico'=> $Medico,
				'diagnostico'=> $diagnostico,
				'IdCuentaAtencion' => $IdCuentaAtencion,
				'IdAtencion' => $IdAtencion,	
				);
		}
		
		echo json_encode($DatosPacientes);
		exit();		
}

if($_REQUEST["acc"] == "Buscar_Diagnosticos"){  

	$data = array();
	$Datos = array();

    $TipoBusqueda=$_REQUEST["TipoBusqueda"];
	if($TipoBusqueda==1){ //CodigoCIE10 like @variable 
		$variable='%'.strtoupper($_REQUEST["q"]).'%';
		$TipoBusqueda=$_REQUEST["TipoBusqueda"];
	}else{ //Descripcion like @variable
		$variable='%'.strtoupper($_REQUEST["q"]).'%';
		$TipoBusqueda=$_REQUEST["TipoBusqueda"];
	}	 

	$data =Buscar_Diagnosticos_Emergencia_M($variable,$TipoBusqueda);
 	
	for ($i=0; $i < count($data); $i++) {	
		$Datos[$i] = array(				
			'IdDiagnostico' =>	$data[$i]["IdDiagnostico"] ,
			'CodigoCIE2004' =>	$data[$i]["CodigoCIE2004"] ,
			'Descripcion' =>	$data[$i]["Descripcion"] ,
			'CodigoDescripcion' =>	$data[$i]["CodigoCIE2004"].$data[$i]["Descripcion"] ,
		);
	}
	echo json_encode($Datos);
	exit(); 
}	

if($_REQUEST["acc"] == "ListaEspera") {
	//$ListarSolicitudes=ListarSolicitudes_M(0, 'T');
	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');

}

if($_REQUEST["acc"] == "RegFenotipoReceptor") {	
	include('../../MVC_Vista/BancoSangre/RegFenotipoReceptor.php');

}

if($_REQUEST["acc"] == "GuardarUpdSolicitudSangreFenotipo") { //FENOTIPO PACIENTE

	//Actualizar Datos Solicitud     
	$IdSolicitudSangre=$_REQUEST['IdSolicitudSangre'];
	$DeteccionCI=$_REQUEST['DeteccionCI'];
	$CoombsDirecto=isset($_REQUEST['CoombsDirecto']) ? $_REQUEST['CoombsDirecto'] : '';
	$DVI=isset($_REQUEST['DVI']) ? $_REQUEST['DVI'] : ''; 

	$FenC=isset($_REQUEST['FenC']) ? $_REQUEST['FenC'] : ''; 
	$FenE=isset($_REQUEST['FenE']) ? $_REQUEST['FenE'] : ''; 
	$Fencc=isset($_REQUEST['Fencc']) ? $_REQUEST['Fencc'] : ''; 
	$Fenee=isset($_REQUEST['Fenee']) ? $_REQUEST['Fenee'] : ''; 
	$FenCw=isset($_REQUEST['FenCw']) ? $_REQUEST['FenCw'] : ''; 
	$FenK=isset($_REQUEST['FenK']) ? $_REQUEST['FenK'] : ''; 
	$Fenkk=isset($_REQUEST['Fenkk']) ? $_REQUEST['Fenkk'] : ''; 
	$Fencontrol=isset($_REQUEST['Fencontrol']) ? $_REQUEST['Fencontrol'] : ''; 

	$Fenotipo=$_REQUEST['Fenotipo'];
	$UsuFenotipo=$_REQUEST['UsuFenotipo'];
	$UsuRegFenotipo=$_REQUEST['IdEmpleado'];		
	$FecRegFenotipo=gfecha($_REQUEST['FecRegFenotipo']).' '.$_REQUEST['HoraRegFenotipo'];	

	$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo'];
	$IdGrupoSanguineoAnterior=$_REQUEST['IdGrupoSanguineoAnterior'];	

	//INICIO ACTUALIZAR GS
	if($IdGrupoSanguineo!=$IdGrupoSanguineoAnterior){
		$IdPaciente=$_REQUEST['IdPaciente'];		
		GuardarReceptorM('', '', $IdPaciente, $IdGrupoSanguineo, $FechaServidor, $UsuRegFenotipo);

		$ObtenerGrupoSanguineo=ObtenerGrupoSanguineo_M($IdGrupoSanguineo);
		$GrupoSanguineo=$ObtenerGrupoSanguineo[0]["Descripcion"];//text A+,A-...
		UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo);
	}
	//FIN ACTUALIZAR GS
	$EspecifidadAnticuerpo=$_REQUEST['EspecifidadAnticuerpo'];
	$ObsFenotipo=$_REQUEST['ObsFenotipo'];
	GuardarUpdSolicitudSangreFenotipoM($IdSolicitudSangre, $DeteccionCI, $CoombsDirecto, $DVI, $Fenotipo, $UsuFenotipo, $UsuRegFenotipo, $FecRegFenotipo, $IdGrupoSanguineo, $EspecifidadAnticuerpo, $ObsFenotipo);	

	//GUARDAR Fenotipo
	//$IdFenotipo=$_REQUEST['IdFenotipo'];
	$TipoPersona='R';//D donante,R Receptor
	$IdPostulante=0;
	$IdPaciente=$_REQUEST['IdPaciente'];	
	$Observacion='';
	$Estado='1';
	$FechaReg=$FechaServidor;
	$UsuReg=$_REQUEST['IdEmpleado'];

	if(trim($Fenotipo)!=''){
		//Actualizar Fenotipo tabla Receptor o Postulante y poner en Estado 0 todos los Fenotipos
		GuardarUpdFenotipoPersonaM($TipoPersona, $IdPostulante, $IdPaciente, $Fenotipo);
	}	

	if($FenC!=''){
		$TipoFenotipo='cmayor';
		$ValorFenotipo=$_REQUEST['FenC'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($FenE!=''){
		$TipoFenotipo='emayor';
		$ValorFenotipo=$_REQUEST['FenE'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fencc!=''){
		$TipoFenotipo='cmenor';
		$ValorFenotipo=$_REQUEST['Fencc'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fenee!=''){
		$TipoFenotipo='emenor';
		$ValorFenotipo=$_REQUEST['Fenee'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($FenCw!=''){
		$TipoFenotipo='Cw';
		$ValorFenotipo=$_REQUEST['FenCw'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($FenK!=''){
		$TipoFenotipo='kmayor';
		$ValorFenotipo=$_REQUEST['FenK'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fenkk!=''){
		$TipoFenotipo='kmenor';
		$ValorFenotipo=$_REQUEST['Fenkk'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}

	if($Fencontrol!=''){
		$TipoFenotipo='control';
		$ValorFenotipo=$_REQUEST['Fencontrol'];
		GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg );	
	}
	
	$mensaje="Fenotipo del Receptor registrado correctamente";
	print "<script>alert('$mensaje')</script>";
	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');

}

if($_REQUEST["acc"] == "GuardarRecepcionSolicitud") {
	$IdSolicitudSangre=$_REQUEST['IdSolicitudSangre'];	
	$FechaRecepcion=gfecha($_REQUEST['FechaRecepcion']).' '.$_REQUEST['HoraRecepcion'];	
	$UsuRecepcion=$_REQUEST['UsuRecepcion'];	
	$UsuRegRecepcion=$_REQUEST['IdEmpleado'];	

	GuardarRecepcionSolicitudM($IdSolicitudSangre, $FechaRecepcion, $UsuRecepcion, $UsuRegRecepcion);
	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
} 

if($_REQUEST["acc"] == "VerHemocomponentesDisponibles") {
	$TipoHem=$_REQUEST['TipoHem'];	
	if($TipoHem=="GRlav"){
		$TipoHem='PG';
	}

	if($TipoHem=="PQAF"){
		$TipoHem='PQ';
	}

	$IdGrupoSanguineo=$_REQUEST['IdGrupoSanguineo']; //	1O+,	2A+,	3B+,	4AB+,	5O-,	6A-,	7B-,	8AB-

	//REGLAS GRUPO DE SANGRE:
	$filtro="";
	if($TipoHem=='PG'){
		if($IdGrupoSanguineo=='2'){//1. Receptor grupo A+, se permite el ingreso del Donante, si este es A+ (1°), A-(2°), O+(3°), O-(4°)
			$filtro=' and d.IdGrupoSanguineo in (2,6,1,5) ';

		}else if($IdGrupoSanguineo=='3'){//2. Receptor grupo B+, se permite el ingreso del Donante, si este es B+(1°), B-(2°), O+(3°), O-(4°)
			$filtro=' and d.IdGrupoSanguineo in (3,7,1,5) ';

		}else if($IdGrupoSanguineo=='4'){//3. Receptor grupo AB+, se permite el ingreso del Donante, si este es A+(1°), A-(2°), B+(3°). B-(4°), AB+(5°), AB-(6°), O+(7°), O-(8°)
			$filtro=''; //TODOS

		}else if($IdGrupoSanguineo=='1'){//4. Receptor grupo O+, se permite el ingreso del Donante, si este es O+(1°), O-(2°)
			$filtro=' and d.IdGrupoSanguineo in (1,5) ';

		}else if($IdGrupoSanguineo=='6'){//5. Receptor grupo A-, se permite el ingreso del Donante, si este es A-(1°), O-(2°)
			$filtro=' and d.IdGrupoSanguineo in (6,5) ';
			
		}else if($IdGrupoSanguineo=='7'){//6. Receptor grupo B-, se permite el ingreso del Donante, si este es B-(1°), O-(2°)
			$filtro=' and d.IdGrupoSanguineo in (7,5) ';

		}else if($IdGrupoSanguineo=='8'){//7. Receptor grupo AB-, se permite el ingreso del Donante, si este es A-(1°), B-(2°), AB-(3°),  O-(4°)
			$filtro=' and d.IdGrupoSanguineo in (6,7,8,5) ';
			
		}else if($IdGrupoSanguineo=='5'){//8. Receptor grupo O-, se permite el ingreso del Donante, O-(unico)
			$filtro=' and d.IdGrupoSanguineo in (5) ';
		}

	}else if($TipoHem=='PFC'){ //PLASMA
		if($IdGrupoSanguineo=='2'){//1. Receptor grupo A+, se permite el ingreso del Donante, si este es A+ (1°), A-(2°)
			$filtro=' and d.IdGrupoSanguineo in (2,6) ';

		}else if($IdGrupoSanguineo=='3'){//2. Receptor grupo B+, se permite el ingreso del Donante, si este es B+(1°), B-(2°)
			$filtro=' and d.IdGrupoSanguineo in (3,7) ';

		}else if($IdGrupoSanguineo=='4'){//3. Receptor grupo AB+, se permite el ingreso del Donante, si este es AB+(1°), AB-(2°)
			$filtro=' and d.IdGrupoSanguineo in (4,8) '; 

		}else if($IdGrupoSanguineo=='1'){//4. Receptor grupo O+
			$filtro=''; //TODOS

		}else if($IdGrupoSanguineo=='6'){//5. Receptor grupo A-, se permite el ingreso del Donante, si este es A-(1°)
			$filtro=' and d.IdGrupoSanguineo in (6) ';
			
		}else if($IdGrupoSanguineo=='7'){//6. Receptor grupo B-, se permite el ingreso del Donante, si este es B-(1°)
			$filtro=' and d.IdGrupoSanguineo in (7) ';

		}else if($IdGrupoSanguineo=='8'){//7. Receptor grupo AB-, se permite el ingreso del Donante, si este es AB-(1°)
			$filtro=' and d.IdGrupoSanguineo in (8) ';
			
		}else if($IdGrupoSanguineo=='5'){//8. Receptor grupo O-, se permite el ingreso del Donante, O-(1°),A-(2°),B-(3°),AB-(4°)
			$filtro=' and d.IdGrupoSanguineo in (5,6,7,8) ';
		}

	}else{ //PQ y CRIO (SIN REGLAS)
		$filtro='';
	}

	if($_REQUEST['TipoHem']=="PQAF"){ // PLAQUETAS POR AFERESIS
		$filtro.="  or (d.Estado=3 and TipoHem=''PQ'') ";

	}else{
		$filtro.="";
	}


	//REGLAS GRUPO MENOR: TODOS $TipoHem

	$DeteccionCI=isset($_REQUEST['DeteccionCI']) ? $_REQUEST['DeteccionCI'] : ''; 
	$CoombsDirecto=isset($_REQUEST['CoombsDirecto']) ? $_REQUEST['CoombsDirecto'] : ''; 
	$IdPaciente=$_REQUEST["IdPaciente"];

	//Buscar Fenotipo  IdPaciente

	$valorcmayor=0;$valoremayor=0;$valorcmenor=0;$valoremenor=0;$valorCw=0;$valorkmayor=0;$valorkmenor=0;
	if(($DeteccionCI=='-' || $DeteccionCI=='') && ($CoombsDirecto='-' || $CoombsDirecto='')){
		$BuscarFenotipoPaciente1=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'cmayor');
		$FenotipoPaciente1=$BuscarFenotipoPaciente1[0]["ValorFenotipo"];//C+
		$BuscarFenotipoPaciente2=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'emayor');
		$FenotipoPaciente2=$BuscarFenotipoPaciente2[0]["ValorFenotipo"];//E+
		$BuscarFenotipoPaciente3=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'cmenor');
		$FenotipoPaciente3=$BuscarFenotipoPaciente3[0]["ValorFenotipo"];//c-
		$BuscarFenotipoPaciente4=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'emenor');
		$FenotipoPaciente4=$BuscarFenotipoPaciente4[0]["ValorFenotipo"];//e-
		$BuscarFenotipoPaciente5=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'Cw');
		$FenotipoPaciente5=$BuscarFenotipoPaciente5[0]["ValorFenotipo"];//Cw-
		$BuscarFenotipoPaciente6=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'kmayor');
		$FenotipoPaciente6=$BuscarFenotipoPaciente6[0]["ValorFenotipo"];//K-
		$BuscarFenotipoPaciente7=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'kmenor');
		$FenotipoPaciente7=$BuscarFenotipoPaciente7[0]["ValorFenotipo"];//k-

		//Obtener Valores Parametro Puntuacion
		$ListarParametroscmayor=SIGESA_ListarParametros_M('cmayor');
		$valorcmayor=$ListarParametroscmayor[0]["Valor1"]; //0.002

		$ListarParametrosemayor=SIGESA_ListarParametros_M('emayor');
		$valoremayor=$ListarParametrosemayor[0]["Valor1"]; //0.033

		$ListarParametroscmenor=SIGESA_ListarParametros_M('cmenor');
		$valorcmenor=$ListarParametroscmenor[0]["Valor1"]; //0.04

		$ListarParametrosemenor=SIGESA_ListarParametros_M('emenor');
		$valoremenor=$ListarParametrosemenor[0]["Valor1"]; //0.011

		$ListarParametrosCw=SIGESA_ListarParametros_M('Cw');
		$valorCw=$ListarParametrosCw[0]["Valor1"]; //INACTIVO POR AHORA

		$ListarParametroskmayor=SIGESA_ListarParametros_M('kmayor');
		$valorkmayor=$ListarParametroskmayor[0]["Valor1"]; //0.1	

		$ListarParametroskmenor=SIGESA_ListarParametros_M('kmenor');
		$valorkmenor=$ListarParametroskmenor[0]["Valor1"]; //0.015 INACTIVO POR AHORA
	}	

	$HemDisponibles=array();
	$Datos=array();
	$VolumenPediatrico=$_REQUEST['VolumenPediatrico'];
	//$TipoHemSol=$_REQUEST['TipoHem'];
	$HemDisponibles=ListarHemDisponiblesM($TipoHem,$VolumenPediatrico,$filtro);
	
	if($HemDisponibles!=NULL){

		$Puntuacion=0;
		for ($i=0; $i < count($HemDisponibles); $i++) {	
			$IdPostulante=$HemDisponibles[$i]["IdPostulante"];
			if($IdPostulante!=NULL){
				//Buscar Fenotipo  IdPostulante
				$BuscarFenotipoPostulante1=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'cmayor');
				$FenotipoPostulante1=$BuscarFenotipoPostulante1[0]["ValorFenotipo"];//C+
				$BuscarFenotipoPostulante2=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'emayor');
				$FenotipoPostulante2=$BuscarFenotipoPostulante2[0]["ValorFenotipo"];//E+
				$BuscarFenotipoPostulante3=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'cmenor');
				$FenotipoPostulante3=$BuscarFenotipoPostulante3[0]["ValorFenotipo"];//c-
				$BuscarFenotipoPostulante4=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'emenor');
				$FenotipoPostulante4=$BuscarFenotipoPostulante4[0]["ValorFenotipo"];//e-
				$BuscarFenotipoPostulante5=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'Cw');
				$FenotipoPostulante5=$BuscarFenotipoPostulante5[0]["ValorFenotipo"];//Cw-
				$BuscarFenotipoPostulante6=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'kmayor');
				$FenotipoPostulante6=$BuscarFenotipoPostulante6[0]["ValorFenotipo"];//K-
				$BuscarFenotipoPostulante7=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'kmenor');
				$FenotipoPostulante7=$BuscarFenotipoPostulante7[0]["ValorFenotipo"];//k-

				if($FenotipoPaciente1=='C-' && $FenotipoPostulante1=='C+'){//Difiere Fenotipo cmayor
					$Puntuacioncmayor=$valorcmayor;
				}else{
					$Puntuacioncmayor=0;
				}

				if($FenotipoPaciente2=='E-' && $FenotipoPostulante2=='E+'){//Difiere Fenotipo cmayor
					$Puntuacionemayor=$valoremayor;
				}else{
					$Puntuacionemayor=0;
				}

				if($FenotipoPaciente3=='c-' && $FenotipoPostulante3=='c+'){//Difiere Fenotipo cmayor
					$Puntuacioncmenor=$valorcmenor;
				}else{
					$Puntuacioncmenor=0;
				}

				if($FenotipoPaciente4=='e-' && $FenotipoPostulante4=='e+'){//Difiere Fenotipo cmayor
					$Puntuacionemenor=$valoremenor;
				}else{
					$Puntuacionemenor=0;
				}

				if($FenotipoPaciente5=='Cw-' && $FenotipoPostulante5=='Cw+'){//Difiere Fenotipo cmayor
					$PuntuacionCw=$valorCw;
				}else{
					$PuntuacionCw=0;
				}

				if($FenotipoPaciente6=='K-' && $FenotipoPostulante6=='K+'){//Difiere Fenotipo cmayor
					$Puntuacionkmayor=$valorkmayor;
				}else{
					$Puntuacionkmayor=0;
				}

				if($FenotipoPaciente7=='k-' && $FenotipoPostulante7=='k+'){//Difiere Fenotipo cmayor
					$Puntuacionkmenor=$valorkmenor;
				}else{
					$Puntuacionkmenor=0;
				}

				$Puntuacion=$Puntuacioncmayor+$Puntuacionemayor+$Puntuacioncmenor+$Puntuacionemenor+$PuntuacionCw+$Puntuacionkmayor+$Puntuacionkmenor;
			}

			$FechaExtraccion=$HemDisponibles[$i]["FechaExtraccion"];	
			if($FechaExtraccion!=NULL){
				$FechaExtraccion=vfecha(substr($FechaExtraccion,0,10)).' '.substr($FechaExtraccion,11,5);				
			}

			$FechaVencimiento=$HemDisponibles[$i]["FechaVencimiento"];
			if($FechaVencimiento!=NULL){
				$FechaVencimiento=vfecha(substr($FechaVencimiento,0,10)).' '.substr($FechaVencimiento,11,5);				
			}

			//Vencido	
			$XFechaVencimiento=$HemDisponibles[$i]["FechaVencimiento"];
			if(substr($XFechaVencimiento,11,5)=='00:00'){
				$YFechaVencimiento=substr($XFechaVencimiento,0,10).' 23:59';
			}else{
				$YFechaVencimiento=$HemDisponibles[$i]["FechaVencimiento"];
			}

			$datetime1 = new DateTime($YFechaVencimiento);
			$datetime2 = new DateTime(date('Y-m-d H:i:s'));			
			//$diff = $datetime1->diff($datetime2);//siempre es positivo
			//$Resta=$diff->days;//2
			if($datetime2 > $datetime1){
				$Vencido=1;
			}else{
				$Vencido=0;
			}

			$Datos[$i] = array(				
				'TipoHem' =>	$HemDisponibles[$i]["TipoHem"] ,
				'SNCS' =>	$HemDisponibles[$i]["SNCS"] ,
				'NroDonacion' =>	$HemDisponibles[$i]["NroDonacion"] ,
				'GrupoSanguineo' =>	$HemDisponibles[$i]["GrupoSanguineo"] ,
				'Fenotipo' =>	$HemDisponibles[$i]["Fenotipo"] ,
				'FechaExtraccion' =>	$FechaExtraccion ,
				'FechaVencimiento' =>	$FechaVencimiento ,
				'Puntuacion' => round($Puntuacion,4),
				'IdMovDetalle' =>	$HemDisponibles[$i]["IdMovDetalle"] ,
				'IdDetSolicitudSangre' =>	$_REQUEST["IdDetSolicitudSangre"] ,
				'IdPaciente' =>	$_REQUEST["IdPaciente"] ,
				'Vencido' =>	$Vencido ,
				'Volumen' =>	$HemDisponibles[$i]["VolumenRestante"] ,
			);
		}
	}else{
	
			/*$Datos[0] = array(				
			'TipoHem' =>	'' ,
			 );*/
	}

	echo json_encode($Datos);
}

if($_REQUEST["acc"] == "GuardarUpdSepararHemocomponente") { //INCLUYE SEPARAR Y RESERVAR
	
	//$TipoHem=$_REQUEST['TipoHem'];
	//$SNCS=$_REQUEST['SNCS'];

	//$ValidarStock=BuscarSNCSTodos_M($TipoHem,$SNCS,'V');		
	//$POSult=count($ValidarStock)-1;//ULTIMO REGISTRO
	//$Estado=$ValidarStock[$POSult]["Estado"];//ultimo estado del hemocomponente:	

	//$ValidarStock=BuscarSNCSTodos_M($TipoHem,$SNCS,'1');

	$IdMovDetalle=$_REQUEST['IdMovDetalle'];
	$ValidarStock=BuscarMovSangrePorIdMovDetalle_M($IdMovDetalle);
	$TipoHem=$ValidarStock[0]["TipoHem"];
	$SNCS=$ValidarStock[0]["SNCS"];
	$Estado=$ValidarStock[0]["Estado"];

	if($Estado=='1' || $Estado=='3'){ //DISPONIBLE o FRACCIÓN PEDIÁTRICA
		//INICIO RESERVAN EN STOCK		
		$IdMovDetalle=$ValidarStock[0]["IdMovDetalle"];//$_REQUEST['IdMovDetalle'];
		$MovNumero=$ValidarStock[0]["MovNumero"];//$_REQUEST['MovNumero'];
		$IdPacienteReserva=$_REQUEST['IdPaciente'];//		
		$UsuReserva=$_REQUEST['IdEmpleado'];//$_REQUEST['UsuReserva'];
		$FechaReserva=$FechaServidor; //gfecha($_REQUEST['FechaReserva']).' '.$_REQUEST['HoraReserva'];
		$MotivoAgregarReserva='POR SOLICITUD';
		$Estado='2'; //2=RESERVADA
		GuardarAgregarReserva_M($TipoHem,$SNCS,$IdMovDetalle,$MovNumero,$IdPacienteReserva,$UsuReserva,$FechaReserva,$MotivoAgregarReserva,$Estado);
		//FIN RESERVAN EN STOCK		

		//INICIO RESERVAR EN SOLICITUD
		//$TipoHem=$_REQUEST["TipoHem"];
		//$SNCS=$_REQUEST["SNCS"];		
		$FechaReg=$FechaServidor;
		$UsuReg=$_REQUEST['IdEmpleado'];
		$Estado='2';//Reservado
		$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];

		$Puntuacion=$_REQUEST["Puntuacion"];
		$IdReserva=GuardarUpdSepararHemocomponenteM($TipoHem, $SNCS, $FechaReg, $UsuReg, $Estado, $IdDetSolicitudSangre,$IdMovDetalle, $Puntuacion);

		if($IdReserva>0){
			$mensaje="Unidad Reservada correctamente";
			print "<script>alert('$mensaje')</script>";
		}else{
			$mensaje="La cantidad de Unidades requeridas están completas";
			print "<script>alert('$mensaje')</script>"; 
		}
		//FIN RESERVAR EN SOLICITUD

	}else{		
		$EstadoDescripcion=strip_tags(DescripcionEstadoHemocomponente($Estado));
		$mensaje=$TipoHem.'-'.$SNCS." se encuentra en estado ".$EstadoDescripcion. " en el Sistema";
		//$mensaje="El Hemocomponente NO se encuentra DISPONIBLE";
	 	print "<script>alert('$mensaje')</script>";	
	}

	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
}

if($_REQUEST["acc"] == "GuardarDespachoSolicitud") {
	$TipoHem2=$_REQUEST['TipoHem2']; //PG|PG|
	$SNCS2=$_REQUEST['SNCS2']; //0314589|0314586|

	$IdMovDetalle2=$_REQUEST['IdMovDetalle2']; //0314589|0314586|

	//Separar
	$porcionesTipoHem = explode("|", $TipoHem2);
	$porcionesSNCS = explode("|", $SNCS2);
	$porcionesIdMovDetalle = explode("|", $IdMovDetalle2);

	$cantTipoHem=count($porcionesTipoHem);//3
	for($th=0;$th<($cantTipoHem-1);$th++){
		 $TipoHem=$porcionesTipoHem[$th]; 
		 $SNCS=$porcionesSNCS[$th]; 

		 //$ValidarStock=BuscarSNCSTodos_M($TipoHem,$SNCS,'V');		 	
		 //$POSult=count($ValidarStock)-1;//ULTIMO REGISTRO
		 //$Estado=$ValidarStock[$POSult]["Estado"];//ultimo estado del hemocomponente:	

		 //$ValidarStock=BuscarSNCSTodos_M($TipoHem,$SNCS,'2');
		 
			$IdMovDetalle=$porcionesIdMovDetalle[$th]; 
			$ValidarStock=BuscarMovSangrePorIdMovDetalle_M($IdMovDetalle);
			$TipoHem=$ValidarStock[0]["TipoHem"];
			$SNCS=$ValidarStock[0]["SNCS"];
			$Estado=$ValidarStock[0]["Estado"];

		 if($Estado=='2'){ //Reservado
			//INICIO DESPACHAR EN SOLICITUD
			$IdSolicitudSangre=$_REQUEST['IdSolicitudSangre2'];	
			$IdDetSolicitudSangre=$_REQUEST['IdDetSolicitudSangre'];
			$FechaDespacho=gfecha($_REQUEST['FechaDespacho']).' '.$_REQUEST['HoraDespacho'];	
			$UsuDespacho=$_REQUEST['UsuDespacho'];	
			$UsuRegDespacho=$_REQUEST['IdEmpleado'];
			GuardarDespachoSolicitudM($IdSolicitudSangre, $IdDetSolicitudSangre, $FechaDespacho, $UsuDespacho, $UsuRegDespacho, $TipoHem, $SNCS);
			$EstadoActual='6';//Transfundido Paciente en STOCK

			$MovNumero=$ValidarStock[0]["MovNumero"];
			CambiarEstadoMovSangreDetalle($TipoHem, $SNCS, $EstadoActual, $MovNumero);
			$mensaje=$TipoHem.'-'.$SNCS." Despachado correctamente";
			print "<script>alert('$mensaje')</script>";
			//FIN RESERVAR EN SOLICITUD

		 }else{
			$EstadoDescripcion=strip_tags(DescripcionEstadoHemocomponente($Estado));
			$mensaje=$TipoHem.'-'.$SNCS." se encuentra en estado ".$EstadoDescripcion.". Sólo se pueden despachar unidades Reservadas.";
			//$mensaje="El Hemocomponente NO se encuentra RESERVADO";
		 	print "<script>alert('$mensaje')</script>";	
		 }

	}	

	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
} 

if($_REQUEST["acc"] == "FacturacionCargarCatalogos"){
     $ListarSigesDevuelveServicios=SigesDevuelveServiciosQueSonPuntosCarga_M();  
     $IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];
     $TodasLasCuentas=0;// $_REQUEST["TodasLasCuentas"];
	 $valorbuscar=$_REQUEST["IdCuentaAtencion"];
	 $pivotbuscar=2;
	 $IdEmpleado=$_REQUEST["IdEmpleado"];
	//
	$LsitarCuentas=SigesFacturacionCuentasAtencionxIdCuentaAtencion_M($TodasLasCuentas,$IdCuentaAtencion); 
	if($LsitarCuentas[0]["idEstadoAtencion"]==1){ 
		 	
		foreach($LsitarCuentas as $item){
			$NroHistoriaClinica = $item["NroHistoriaClinica"];	
			$Paciente=$item["ApellidoPaterno"]." ".$item["ApellidoMaterno"]." ".$item["PrimerNombre"];
			$IdPaciente = $item["IdPaciente"];
			$idFuenteFinanciamiento = $item["idFuenteFinanciamiento"];
			$GeneraPago = $item["GeneraPago"]; //idTipoFinanciamiento
			$IdServicioIngreso= $item["IdServicioIngreso"];
			$FechaEgresoAdministrativo= $item["FechaEgresoAdministrativo"];
			$HoraEgresoAdministrativo= $item["HoraEgresoAdministrativo"];
			$idEstadoAtencion= $item["idEstadoAtencion"];
			$dFuenteFinanciamiento= $item["dFuenteFinanciamiento"];
			$dtipoFinanciamiento= $item["dtipoFinanciamiento"];
			$NroDocumento= $item["NroDocumento"];
			$IdCuentaAtencion= $item["IdCuentaAtencion"];
			$IdPaciente= $item["IdPaciente"];
			$IdPuntoCarga= $item["IdPuntoCarga"];
			$FinanciamientoPaciente=$dFuenteFinanciamiento.' '.$dtipoFinanciamiento;		
		} 			  
	}

	//$ListaCatalagoX= FactCatalogoPaqueteSeleccionarTodos($GeneraPago,'1');
	$TipoHem=isset($_REQUEST['TipoHem']) ? $_REQUEST['TipoHem'] : '';

	$TipoHemB=isset($_REQUEST['TipoHemB']) ? $_REQUEST['TipoHemB'] : $TipoHem;
	$SNCSB=isset($_REQUEST['SNCSB']) ? $_REQUEST['SNCSB'] : $_REQUEST['SNCS'];

	 //OBTENER PAQUETE POR DEFECTO
	 if($GeneraPago=='1'){
	 	if($TipoHem=='PG' || $TipoHem=='GRlav' || $TipoHem=='PFC'){
		 	$idpaquete=17;
		 	$DescripcionPaquete='TRANSFUSION DE SANGRE GLOBULAR PFC (PARTICULAR)';
		}else if($TipoHem=='PQ' || $TipoHem=='CRIO' || $TipoHem=='PQAF'){
		 	$idpaquete=18;
		 	$DescripcionPaquete='TRANSFUSION DE PLAQUETAS CRIOPRECIPITADO (PARTICULAR)';
		}else{
		 	$idpaquete=isset($_REQUEST['idpaquete']) ? $_REQUEST['idpaquete'] : 0;
		 	$DescripcionPaquete='';
		}

	 }else if($GeneraPago=='2'){
	 	if($TipoHem=='PG' || $TipoHem=='GRlav' || $TipoHem=='PFC'){
		 	$idpaquete=23;
		 	$DescripcionPaquete='TRANSFUSION DE SANGRE GLOBULAR PFC (SIS/CONVENIOS)';
		}else if($TipoHem=='PQ' || $TipoHem=='CRIO' || $TipoHem=='PQAF'){
		 	$idpaquete=24;
		 	$DescripcionPaquete='TRANSFUSION DE PLAQUETAS CRIOPRECIPITADO (SIS/CONVENIOS)';
		}else{
		 	$idpaquete=isset($_REQUEST['idpaquete']) ? $_REQUEST['idpaquete'] : 0;
		 	$DescripcionPaquete='';
		}
	 }else{
	 	if($TipoHem=='PG' || $TipoHem=='GRlav' || $TipoHem=='PFC'){
		 	$idpaquete=26;
		 	$DescripcionPaquete='TRANSFUSION DE SANGRE GLOBULAR PFC (SOAT)';
		}else if($TipoHem=='PQ' || $TipoHem=='CRIO' || $TipoHem=='PQAF'){
		 	$idpaquete=27;
		 	$DescripcionPaquete='TRANSFUSION DE PLAQUETAS CRIOPRECIPITADO (SOAT)';
		}else{
		 	$idpaquete=isset($_REQUEST['idpaquete']) ? $_REQUEST['idpaquete'] : 0;
		 	$DescripcionPaquete='';
		}
	 	
	 }	
      
	 /*$AbirCuenta=$_REQUEST["AbirCuenta"];
		 if($AbirCuenta=="SI"){
		   FacturacionCuentasAtencionAbrir_C($IdCuentaAtencion,$IdEmpleado);
		 }*/
	$listaPaquetes=FacturacionCatalogoPaquetesXidPaquete($idpaquete);    
	$Mensaje='1';
	$FechaDespacho=$_REQUEST["FechaDespacho"].' '.$_REQUEST["HoraDespacho"];

	include('../../MVC_Vista/BancoSangre/ConsumoenelServicioAgregarV.php');
	    
}

if($_GET["acc"] == "GuardarConsumodeServicio") // Guardar: Consumo de Servicio 
{
	
		$DatosReserva=ObtenerDatosIdReservaSangre($_REQUEST["IdReserva"]);
		
		$IdOrden=$DatosReserva[0]["IdOrden"];
		if(trim($IdOrden)!=0){
			$mensaje="El despacho del Hemocomponente ".$DatosReserva[0]["TipoHem"]."-".$DatosReserva[0]["SNCS"]." YA fue Facturado";
			print "<script>alert('$mensaje')</script>"; 
			include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
			exit();
		}
	
		$idOrdenPago='';
		$NombrePC = NombrePC();
		$arreglo=explode('.',$NombrePC); 
		$NombrePC=mb_strtoupper($arreglo[0]);
		
		include('../../MVC_Modelo/EmpleadosM.php');
		//include('../../MVC_Modelo/FactConfigM.php');

		//$FechaDespacho=$FechaServidor;//$_REQUEST["FechaDespacho"];
		$FechaDespachoX=$_REQUEST["FechaDespacho"];//21/08/2018 10:29:06
		$FechaDespacho= gfecha(substr($FechaDespachoX,0,10)).' '.(substr($FechaDespachoX,11,8));
		
		$FechaRegistro=$FechaServidor;//gfechahora($_REQUEST["FechaRegistro"]);
		$IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];
		$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
		$NroDocumento=$_REQUEST["NroDocumento"];
		$Paciente=$_REQUEST["Paciente"];
		$IdPaciente=$_REQUEST["IdPaciente"];
		$IdPuntoCargaSer=$_REQUEST["IdPuntoCargaSer"];
		$IdEmpleado=$_REQUEST["IdEmpleado"];
		$TotalFilas=$_REQUEST["TotalFilas"];
	    $IdPuntoCarga=$_REQUEST["xIdPuntoCarga"];  
	    $idFuenteFinanciamiento=$_REQUEST["xidFuenteFinanciamiento"];
	    $idTipoFinanciamiento=$_REQUEST["xGeneraPago"];
        $IdServicioPaciente=$_REQUEST["IdServicioIngreso"];
        $IdEstadoFacturacion=1;
		$IdProducto =$_REQUEST["IdProducto"];
		$codigo=$_REQUEST["codigo"];
		$descripcion=$_REQUEST["descripcion"]; 
		$Precio=$_REQUEST["precio"];
		$dscto=$_REQUEST["dscto"];
		$Cantidad=$_REQUEST["cantidad"]; 
		$st=$_REQUEST["st"];
		$bi=$_REQUEST["bi"];
		//$Total=$Cantidad*$Precio;
 
	    $ExtraerDatosUsuario=EmpleadosSeleccionarPorId_M($IdEmpleado);
        $Usuario=$ExtraerDatosUsuario[0]["Usuario"]; 
            
 
 	/**************************Paciente SIN SEGURO **************************/
	if($idFuenteFinanciamiento==1 or $idFuenteFinanciamiento==5 ){
		 
	 	try {	   
	
		/*  FacturacionServicioDespachoAgregar    *
			FactOrdenServicioAgregar              *
			FacturacionServicioPagosAgregar       *
			FactOrdenServicioPagosAgregar
			*/			
			
			echo $id=FactOrdenServicioAgregar_M('', 
				 $IdPuntoCarga, 
				 $IdPaciente, 
				 $IdCuentaAtencion,  
				 $IdServicioPaciente, 
				 $idTipoFinanciamiento,  
				 $idFuenteFinanciamiento, 
				 $FechaRegistro, 
				 $IdEmpleado,  
				 $FechaRegistro,  
				 $IdEmpleado, 
				 $IdEstadoFacturacion,  
				 $FechaRegistro, 
				 $IdEmpleado);
				 
				 
		  
			$NumeroOrden=  SigesaFactOrdenServicioExtraerIdorden_M($IdPuntoCarga,$IdPaciente,$IdCuentaAtencion,$IdServicioPaciente,$idTipoFinanciamiento,$idFuenteFinanciamiento,$FechaRegistro,$IdEmpleado);	 
		
			$idOrden=$NumeroOrden[0][0];
			for ($i = 1; $i <= $TotalFilas; $i++) {
				$IdProducto =$_REQUEST["IdProducto".$i];
				$Precio=$_REQUEST["precio".$i];
				$Cantidad=$_REQUEST["cantidad".$i]; 
				$Total=$Cantidad*$Precio;
				if($IdProducto!=''){
				$labConfHIS='';	
				FacturacionServicioDespachoAgregar_M($idOrden,$IdProducto,$Cantidad,$Precio,$Total,$labConfHIS,$IdEmpleado);
				}
			}
			
			FactOrdenServicioPagosAgregar_M('',NULL,$idOrden,$FechaRegistro,$IdEmpleado,1,0.00,0,$IdEmpleado);
												
			$NumeroidOrdenPago=SigesaFactOrdenServicioPagosExtraerIdordenpago_M($idOrden,$FechaRegistro,$IdEmpleado);
			$idOrdenPagos=$NumeroidOrdenPago[0][0];	
			
			for ($r = 1; $r <= $TotalFilas; $r++) {
					$IdProducto =$_REQUEST["IdProducto".$r];
					$Precio=$_REQUEST["precio".$r];
					$Cantidad=$_REQUEST["cantidad".$r]; 
					$Total=$Cantidad*$Precio;	
					
					if($IdProducto!=''){							
					 FacturacionServicioPagosAgregar_M($idOrdenPagos,$IdProducto,$Cantidad,$Precio,$Total,$IdEmpleado);
					}
				}
			 
		} catch (Exception $e) {
			echo 'Excepción capturada: ',  $e->getMessage(), "\n";
		}      
	
		    $ListarFactOrdenServicio=FactOrdenServicioSeleccionarPorIdOrden_M($idOrden); //Cavecera
			$IdPaciente=$ListarFactOrdenServicio[0]["IdPaciente"];		
			$IdCuentaAtencion=$ListarFactOrdenServicio[0]["IdCuentaAtencion"];		
			$IdServicioPaciente=$ListarFactOrdenServicio[0]["IdServicioPaciente"];		
			$idTipoFinanciamiento=$ListarFactOrdenServicio[0]["idTipoFinanciamiento"];		
			$idFuenteFinanciamiento=$ListarFactOrdenServicio[0]["idFuenteFinanciamiento"];		
			$FechaCreacion=vfechahora($ListarFactOrdenServicio[0]["FechaCreacion"]);		
			$IdUsuario=$ListarFactOrdenServicio[0]["IdUsuario"];		
			$FechaDespacho=vfechahora($ListarFactOrdenServicio[0]["FechaDespacho"]);		
			$IdUsuarioDespacho=$ListarFactOrdenServicio[0]["IdUsuarioDespacho"];		
			$IdEstadoFacturacion=$ListarFactOrdenServicio[0]["IdEstadoFacturacion"];		
			$FechaHoraRealizaCpt=$ListarFactOrdenServicio[0]["FechaHoraRealizaCpt"];		
			$ApellidoPaterno=$ListarFactOrdenServicio[0]["ApellidoPaterno"];		
			$ApellidoMaterno=$ListarFactOrdenServicio[0]["ApellidoMaterno"];		
			$PrimerNombre=$ListarFactOrdenServicio[0]["PrimerNombre"];		
			$SegundoNombre=$ListarFactOrdenServicio[0]["SegundoNombre"];		
			$NroHistoriaClinica=$ListarFactOrdenServicio[0]["NroHistoriaClinica"];		
			$dservicio=$ListarFactOrdenServicio[0]["dservicio"];		
			$idTipoServicio=$ListarFactOrdenServicio[0]["idTipoServicio"];	
 	      
		     $ListarFacturacionServicioPagos=FacturacionServicioPagosFiltraPorIdOrden_M($idOrden); //Detalle
			 $idOrdenPago=$ListarFacturacionServicioPagos[0]["idOrdenPago"];
			  $IdEmpleado=$_REQUEST["IdEmpleado"];
		     // echo "Paciente sisn segurpo";
		  	 // include('../../MVC_Vista/BancoSangre/ConsumoenelServicioImprimir_SinSeguro.php');
			 include('../../MVC_Vista/BancoSangre/ConsumoenelServicioImprimi.php');
	 
 	}else{
		/**************************Paciente CON SEGURO **************************/	 
		//FactOrdenServicio
		//FacturacionCuentasAtencionPtos
		//FacturacionServicioDespacho
		//FacturacionServicioFinanciamientos

		try {
			/*FactOrdenServicioAgregar_M($IdOrden, 
		 $IdPuntoCarga, 
		 $IdPaciente, 
		 $IdCuentaAtencion,  
		 $IdServicioPaciente, 
		 $idTipoFinanciamiento,  
		 $idFuenteFinanciamiento, 
		 $FechaCreacion, 
		 $IdUsuario,  
		 $FechaDespacho,  
		 $IdUsuarioDespacho, 
		 $IdEstadoFacturacion,  
		 $FechaHoraRealizaCpt, 
		 $IdUsuarioAuditoria)*/
	    	echo  $id=FactOrdenServicioAgregar_M('', 
			 $IdPuntoCarga, 
			 $IdPaciente, 
			 $IdCuentaAtencion,  
			 $IdServicioPaciente, 
			 $idTipoFinanciamiento,  
			 $idFuenteFinanciamiento, 
			 $FechaRegistro, 
			 $IdEmpleado,  
			 $FechaDespacho,  
			 $IdEmpleado, 
			 $IdEstadoFacturacion,  
			 $FechaRegistro, 
			 $IdEmpleado);
						 
			 $NumeroOrden=  SigesaFactOrdenServicioExtraerIdorden_M($IdPuntoCarga,$IdPaciente,$IdCuentaAtencion,$IdServicioPaciente,$idTipoFinanciamiento,$idFuenteFinanciamiento,$FechaRegistro,$IdEmpleado)	;	 
	
			$idOrden=$NumeroOrden[0][0];
			for ($i = 1; $i <= $TotalFilas; $i++) {
				$IdProducto =$_REQUEST["IdProducto".$i];
				
				$Precio=$_REQUEST["precio".$i];
	            $Cantidad=$_REQUEST["cantidad".$i]; 
				$Total=$Cantidad*$Precio;
					if($IdProducto!=''){
					$labConfHIS='';
					FacturacionServicioDespachoAgregar_M($idOrden,$IdProducto,$Cantidad,$Precio,$Total,$labConfHIS,$IdEmpleado);
					FacturacionServicioFinanciamientosAgregar_M($idOrden ,
															$IdProducto ,
															$idTipoFinanciamiento ,
															$idFuenteFinanciamiento ,
															$Cantidad ,
															$Precio ,
															$Total ,
															$FechaRegistro ,
															$IdEmpleado ,
															$IdEmpleado);	
					}
			}
			
	    } catch (Exception $e) {
    		echo 'Excepción capturada: ',  $e->getMessage(), "\n";
		}  
	    	//echo "Es pacente con seguro ";
	        $ListarFactOrdenServicio=FactOrdenServicioSeleccionarPorIdOrden_M($idOrden); //Cavecera
			$IdPaciente=$ListarFactOrdenServicio[0]["IdPaciente"];		
			$IdCuentaAtencion=$ListarFactOrdenServicio[0]["IdCuentaAtencion"];		
			$IdServicioPaciente=$ListarFactOrdenServicio[0]["IdServicioPaciente"];		
			$idTipoFinanciamiento=$ListarFactOrdenServicio[0]["idTipoFinanciamiento"];		
			$idFuenteFinanciamiento=$ListarFactOrdenServicio[0]["idFuenteFinanciamiento"];		
			$FechaCreacion=vfechahora($ListarFactOrdenServicio[0]["FechaCreacion"]);		
			$IdUsuario=$ListarFactOrdenServicio[0]["IdUsuario"];		
			$FechaDespacho=vfechahora($ListarFactOrdenServicio[0]["FechaDespacho"]);		
			$IdUsuarioDespacho=$ListarFactOrdenServicio[0]["IdUsuarioDespacho"];		
			$IdEstadoFacturacion=$ListarFactOrdenServicio[0]["IdEstadoFacturacion"];		
			$FechaHoraRealizaCpt=$ListarFactOrdenServicio[0]["FechaHoraRealizaCpt"];		
			$ApellidoPaterno=$ListarFactOrdenServicio[0]["ApellidoPaterno"];		
			$ApellidoMaterno=$ListarFactOrdenServicio[0]["ApellidoMaterno"];		
			$PrimerNombre=$ListarFactOrdenServicio[0]["PrimerNombre"];		
			$SegundoNombre=$ListarFactOrdenServicio[0]["SegundoNombre"];		
			$NroHistoriaClinica=$ListarFactOrdenServicio[0]["NroHistoriaClinica"];		
			$dservicio=$ListarFactOrdenServicio[0]["dservicio"];		
			$idTipoServicio=$ListarFactOrdenServicio[0]["idTipoServicio"];	
			
  	        $ListarFacturacionServicioPagos=FacturacionServicioDespachoFiltraPorIdOrden_M($idOrden);			  
			include('../../MVC_Vista/BancoSangre/ConsumoenelServicioImprimi.php');	
	 }
	 
	 $IdReserva=$_REQUEST['IdReserva'];
	 UpdateOrdenReserva($IdReserva,$idOrden);

}


if($_REQUEST["acc"] == "VerFacturacion") {
	$IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];
	$XFechaDespacho=isset($_REQUEST['FechaDespacho']) ? $_REQUEST['FechaDespacho'] : '';
	if(trim($XFechaDespacho)!=''){
		$FechaDespacho=isset($XFechaDespacho) ? (gfecha($XFechaDespacho).' '.$_REQUEST['HoraDespacho']) : '';
	}else{
		$FechaDespacho='';
	}
	$listado=VerFacturacionesBancoDeSangreM($_REQUEST['IdDetSolicitudSangre']);
	if($listado!=''){
		include('../../MVC_Vista/BancoSangre/VerFacturacion.php');
	}else{
		$mensaje="La atencion no tiene Facturaciones en Banco de Sangre";
		print "<script>alert('$mensaje')</script>"; 
		//$ListarSolicitudes=ListarSolicitudes_M(0, 'T');
		include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
	}
}

if($_REQUEST["acc"] == "ConsultarPacientesHemoglobinaMenor7") {

	$fecha = date('Y-m-d');
	$nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
	$FechaI = date ( 'd/m/Y' , $nuevafecha );

	$FechaInicioB= isset($_REQUEST['FechaInicio']) ? ($_REQUEST['FechaInicio']) : $FechaI;	
	$FechaFinalB=  isset($_REQUEST['FechaFinal']) ? ($_REQUEST['FechaFinal']) : date('d/m/Y');	

	include('../../MVC_Vista/BancoSangre/ConsultarPacientesHemoglobinaMenor7.php');
}

if($_REQUEST["acc"] == "OtrosRegistrosDonaciones") {
	include('../../MVC_Vista/BancoSangre/OtrosRegistrosDonaciones.php');
}
	
if($_REQUEST["acc"] == "VerUnidadesReservadas") {
	
	$Datos=array();
	$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
	$Reserva=VerSolicitudSangreDetalleReservaM($IdDetSolicitudSangre);
	
	if($Reserva!=NULL){
		$Puntuacion=0;
		for ($i=0; $i < count($Reserva); $i++) {
			$FechaReg=vfecha(substr($Reserva[$i]["FechaReg"],0,10)).' '.substr($Reserva[$i]["FechaReg"],11,8);	
			
			if($Reserva[$i]["IdEstadoFacturacion"]==9){
				$IdOrdenF='0';	
				$EstadoF='3';
			}else{
				$IdOrdenF=$Reserva[$i]["IdOrden"] ;
				$EstadoF='4';
			}

			$Datos[$i] = array(				
				'TipoHem' =>	$Reserva[$i]["TipoHem"] ,
				'SNCS' =>	$Reserva[$i]["SNCS"] ,
				'FechaReg' =>	$FechaReg , 
				'UsuarioReg' =>	$Reserva[$i]["UsuarioReg"] , 
				'GrupoSanguineo' =>	$Reserva[$i]["GrupoSanguineo"] ,
				'IdOrden' =>	$IdOrdenF ,
				'Estado' =>		$EstadoF ,
				'IdDetSolicitudSangre'=>$Reserva[$i]["IdDetSolicitudSangre"] ,
				'IdReserva'=>$Reserva[$i]["IdReserva"] ,
				'IdMovDetalle'=>$Reserva[$i]["IdMovDetalle"] ,
				'VolumenReserva'=>$Reserva[$i]["VolumenReserva"] ,
			);
		}
	}
	echo json_encode($Datos);
}

if($_REQUEST["acc"] == "VerUnidadesDespachadas") {
	
	$Datos=array();
	$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
	$Reserva=VerSolicitudSangreDetalleReservaM($IdDetSolicitudSangre);
	
	$ObtenerDatosDetS=ObtenerDatosDetSolicitudSangre($IdDetSolicitudSangre);
	$IdPaciente=$ObtenerDatosDetS[0]["IdPaciente"];
	$IdCuentaAtencion=$ObtenerDatosDetS[0]["IdCuentaAtencion"];
	$FechaDespachoX=$ObtenerDatosDetS[0]["FechaDespacho"];
	$FechaDespacho=isset($FechaDespachoX) ? (vfecha(substr($FechaDespachoX,0,10))) : '';	
	$HoraDespacho=isset($FechaDespachoX) ? (substr($FechaDespachoX,11,8)) : '';	
	
	if($Reserva!=NULL){
		$Puntuacion=0;
		for ($i=0; $i < count($Reserva); $i++) {
			//$FechaReg=vfecha(substr($Reserva[$i]["FechaReg"],0,10)).' '.substr($Reserva[$i]["FechaReg"],11,8);	
			$Datos[$i] = array(				
				'TipoHem' =>	$Reserva[$i]["TipoHem"] ,
				'SNCS' =>	$Reserva[$i]["SNCS"] ,
				//'FechaReg' =>	$FechaReg , 
				'UsuarioReg' =>	$Reserva[$i]["UsuarioReg"] ,
				'GrupoSanguineo' =>	$Reserva[$i]["GrupoSanguineo"] ,				
				'IdReserva'=>	$Reserva[$i]["IdReserva"] ,
				'IdOrden'=>	$Reserva[$i]["IdOrden"] ,
								 
				'IdPaciente' =>	$IdPaciente ,
				'IdCuentaAtencion' =>	$IdCuentaAtencion ,
				'FechaDespacho' =>	$FechaDespacho ,
				'HoraDespacho' =>	$HoraDespacho ,
				'FechaHoraDespacho' =>	$FechaDespacho.' '.$HoraDespacho ,
			);
		}
	}
	echo json_encode($Datos);
}

if($_REQUEST["acc"] == "VerHistorialDonaciones") {
	$IdPaciente=$_REQUEST["IdPaciente"];
	include('../../MVC_Vista/BancoSangre/VerHistorialDonaciones.php');
}

if($_REQUEST["acc"] == "RegistroBUT") {
	include('../../MVC_Vista/BancoSangre/RegistroBUT.php');
}

if($_REQUEST["acc"] == "GuardarRegresoBUT") {
	$IdReserva=$_REQUEST["IdReserva"];
	$FechaRecBUT=isset($_REQUEST['FechaRecBUT']) ? (gfecha($_REQUEST['FechaRecBUT']).' '.$_REQUEST['HoraRecBUT']) : '';
	$UsuSerUsuBUT=$_REQUEST["UsuSerUsuBUT"];	
	$UsuRegBUT=$_REQUEST["IdEmpleado"];
	$RegresoBUT=1;
	GuardarRegresoBUT($IdReserva,$FechaRecBUT,$UsuSerUsuBUT,$UsuRegBUT,$RegresoBUT);

	$mensaje="El regreso de la B.U.T fue registrada correctamente";
	print "<script>alert('$mensaje')</script>";
	include('../../MVC_Vista/BancoSangre/RegistroBUT.php');
}

if($_REQUEST["acc"] == "CerrarDetSolicitud") {	

	$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
	$FechaCierre=$FechaServidor;
	$UsuRegCierre=$_REQUEST["IdEmpleado"];
	CerrarDetSolicitud($IdDetSolicitudSangre,$FechaCierre,$UsuRegCierre);

	$mensaje="El detalle de la solicitud fue CERRADA correctamente";
	print "<script>alert('$mensaje')</script>";
	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
}

if($_REQUEST["acc"] == "EliminarDetSolicitud") {	

	$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
	$FechaElim=$FechaServidor;
	$ListarUsuarioxIdemp=ListarUsuarioxIdempleado_M($_REQUEST["IdEmpleado"]);
	$UsuElim=$ListarUsuarioxIdemp[0]["Usuario"];
	$ObsEstado='Eliminado por '.$UsuElim.'. El '.$FechaElim;

	//$ObtenerDatosDetSolicitud=ObtenerDatosDetSolicitudSangre($IdDetSolicitudSangre);
	$VerReservas=VerSolicitudSangreDetalleReservaM($IdDetSolicitudSangre);
	if($VerReservas!=NULL){			
		$mensaje="NO puede Eliminar la Solicitud porque tiene Hemocomponentes";

	}else{
		EliminarDetSolicitud($IdDetSolicitudSangre,$ObsEstado);
		$mensaje="El detalle de la solicitud fue ELIMINADA correctamente";		
	}

	print "<script>alert('$mensaje')</script>";
	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');	
	
}

if($_REQUEST["acc"] == "QuitarReserva") {

	$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
	//$TipoHem=$_REQUEST["TipoHem"];
	//$SNCS=$_REQUEST["SNCS"];
	$IdReserva=$_REQUEST["IdReserva"];
	
	//$ValidarStock=BuscarSNCSTodos_M($TipoHem,$SNCS,'V');
	//$POSult=count($ValidarStock)-1;//ULTIMO REGISTRO
	//$Estado=$ValidarStock[$POSult]["Estado"];//ultimo estado del hemocomponente:

	//$ValidarStock=BuscarSNCSTodos_M($TipoHem,$SNCS,'2');	

	$IdMovDetalle=$_REQUEST["IdMovDetalle"];
	$ValidarStock=BuscarMovSangrePorIdMovDetalle_M($IdMovDetalle);
	$TipoHem=$ValidarStock[0]["TipoHem"];
	$SNCS=$ValidarStock[0]["SNCS"];
	$Estado=$ValidarStock[0]["Estado"];

	if($Estado=='2' || $Estado=='6'){ //RESERVADO y Transfundido Paciente (DESPACHADO)	
		
		$MovNumero=$ValidarStock[0]["MovNumero"];			
		$UsuRegEstado=$_REQUEST['IdEmpleado'];
		$FecRegEstado=$FechaServidor; 
		QuitarReserva($IdDetSolicitudSangre,$TipoHem,$SNCS,$IdReserva,$MovNumero,$UsuRegEstado,$FecRegEstado);

		$mensaje="La reserva fue eliminada correctamente";
		print "<script>alert('$mensaje')</script>";
		
	}else{		
		$EstadoDescripcion=strip_tags(DescripcionEstadoHemocomponente($Estado));
		$mensaje="El Hemocomponente YA se encuentra en estado ".$EstadoDescripcion. " en el Sistema";
		//$mensaje="El Hemocomponente NO se encuentra RESERVADO";
	 	print "<script>alert('$mensaje')</script>";	
	}	

	
	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
}

//INICIO COPIADO SERVIDOR 25-09-2018
if($_REQUEST["acc"] == "ConsultarPacientesDebenSangre") {

	/*$fecha = date('Y-m-d');
	$nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
	$FechaI = date ( 'd/m/Y' , $nuevafecha );

	$FechaInicioB= isset($_REQUEST['FechaInicio']) ? ($_REQUEST['FechaInicio']) : $FechaI;	
	$FechaFinalB=  isset($_REQUEST['FechaFinal']) ? ($_REQUEST['FechaFinal']) : date('d/m/Y');	*/
	$IdServicioHospital=isset($_REQUEST['IdServicioHospital']) ? ($_REQUEST['IdServicioHospital']) : "9999";

	include('../../MVC_Vista/BancoSangre/ConsultarPacientesDebenSangre.php');
}

if($_REQUEST["acc"] == "TablaPacientesHospitalizadosEmergencia") {
		include('../../MVC_Vista/BancoSangre/TablaPacientesHospitalizadosEmergencia.php');
}

if($_REQUEST["acc"] == "LiberarTamizajesArchivados") 
{ 
	$NroDonacion=isset($_POST['NroDonacion']) ? $_POST['NroDonacion'] : '';
	include('../../MVC_Vista/BancoSangre/LiberarTamizajesArchivados.php');
}

if($_REQUEST["acc"] == "DesarchivarTamizaje") 
{  	
	$EstadoArchivado='0';			
	$NroDonacion=$_REQUEST["NroDonacion"];
	
	$ObservacionDesarchiva='Por: '.$_REQUEST["IdEmpleado"].' El: '.$FechaServidor;	
		
	DesarchivarTamizaje_M($EstadoArchivado,$ObservacionDesarchiva,$NroDonacion);
	
	$mensaje="Tamizaje Desarchivados correctamente";
	print "<script>alert('$mensaje')</script>";	
	include('../../MVC_Vista/BancoSangre/LiberarTamizajesArchivados.php');
}
//FIN COPIADO SERVIDOR 25-09-2018

if($_REQUEST["acc"] == "ImprimirEtiquetaTransfusiones") {
		$IdDetSolicitudSangre=$_REQUEST['IdDetSolicitudSangre'];
		$IdPaciente=$_REQUEST['IdPaciente'];
		$data=SIGESA_BSD_ListarEtiquetaTransfusionesM($IdDetSolicitudSangre);

		if($data!=NULL){
			include('../../MVC_Vista/BancoSangre/ImprimirEtiquetaTransfusiones.php');
		}else{
			/*$mensaje="Paciente con Nro de Historia Clinica ".$NroHistoriaClinica." NO EXISTE";
			print "<script>alert('$mensaje')</script>";	
			include('../../MVC_Vista/BancoSangre/Etiquetas.php');*/
		}
}

if($_REQUEST["acc"] == "ConsultarMovimientosUnidadesAptas") 
{ 	
	include('../../MVC_Vista/BancoSangre/ConsultarMovimientosUnidadesAptas.php');
}

if($_REQUEST["acc"] == "ReporteMovimientosUnidadesAptas")
{ 	
    $data = array();
	$ListarResultado= array();	
	$RegresoBUT=0;
		 
		 //$NroDocumento=$_REQUEST["NroDocumento"];
		 $TipoHem=$_REQUEST["TipoHem"];
		 $SNCS=$_REQUEST["SNCS"];			
		 
		 $ListarResultado= ReporteMovimientosUnidadesAptasM($TipoHem, $SNCS);		
	       	
		for ($i = 0; $i < count($ListarResultado); $i++) {
				if($ListarResultado[$i]["EstablecimientoIngreso"]=='6216'){
					$NroDonacion='PDAC'.$ListarResultado[$i]["NroDonacion"];
				}else{
					$NroDonacion=$ListarResultado[$i]["NroDonacion"];
				}	

				$FechaRecibe=$ListarResultado[$i]['FechaRecibe'];	
				if($FechaRecibe==''){
					$FechaRecibe='';
				}else{
					$FechaRecibe=vfecha(substr($FechaRecibe,0,10)).' '.substr($FechaRecibe,11,5);
				}				

				$Observacion='';$FechaMovimiento='';

				$MovTipo=$ListarResultado[$i]['MovTipo'];
				if(count($ListarResultado)>($i+1)){//2>2
					$j=$i+1;
				}else{
					$j=$i;
				}

				if($MovTipo=='I' && $ListarResultado[$j]['MovTipo']=='E'){ //SI EL SIGUIENTE MOV ES UN EGRESO
					$FechaMovimiento=$FechaRecibe;
					$EstadoDescripcion='Disponible';
					
				}else if($MovTipo=='E'){					
					$FechaMovimiento=vfecha(substr($ListarResultado[$i]['FechaSalida'],0,10)).' '.substr($ListarResultado[$i]['FechaSalida'],11,5);
					$EstadoDescripcion='Transferido a ';
					$Observacion=$ListarResultado[$i]["EstablecimientoEgreso"];					

				}else{
					$EstadoDescripcion=strip_tags(DescripcionEstadoHemocomponente($ListarResultado[$i]['Estado']));
				}

				if($ListarResultado[$i]['Estado']==2){ //RESERVA
					$FechaMovimiento=vfecha(substr($ListarResultado[$i]['FechaReserva'],0,10)).' '.substr($ListarResultado[$i]['FechaReserva'],11,5);
					$Paciente=SIGESA_BSD_Buscarpaciente_M($ListarResultado[$i]["IdPacienteReserva"],'IdPaciente');	
					$Observacion=' HC:'.$Paciente[0]["NroHistoriaClinica"].' '.$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"].' '.$Paciente[0]["SegundoNombre"];

				}else if($ListarResultado[$i]['Estado']==6){ //DESPACHADO			
					$FechaMovimiento=vfecha(substr($ListarResultado[$i]['FechaDespacho'],0,10)).' '.substr($ListarResultado[$i]['FechaDespacho'],11,5);
					$Paciente=SIGESA_BSD_Buscarpaciente_M($ListarResultado[$i]["IdPacienteReserva"],'IdPaciente');	
					//$Observacion=$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"].' '.$Paciente[0]["SegundoNombre"];
					$Observacion=' HC:'.$Paciente[0]["NroHistoriaClinica"].' '.$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"].' '.$Paciente[0]["SegundoNombre"];
				}

				if($ListarResultado[$i]['NroSolicitud']){
					$Observacion='Solic:'.$ListarResultado[$i]['NroSolicitud'].$Observacion;
				}

                $data[$i] = array(
                    'Nro' => $i+1,	
                    'SNCS'	=> $TipoHem.'-'.$SNCS,			
					'EstablecimientoIngreso' => $ListarResultado[$i]["EstablecimientoIngreso"],
                    'FechaRecibe' => $FechaRecibe,					
					'EstablecimientoEgreso' => $ListarResultado[$i]["EstablecimientoEgreso"],
					//'Postulante' => strtoupper($ListarResultado[$i]["ApellidosPostulante"].' '.$ListarResultado[$i]["NombresPostulante"]),
                    'NroDonacion' => $NroDonacion,     
                    'GrupoSanguineo'=> $ListarResultado[$i]["GrupoSanguineo"],
                    'NroTabuladora'=> $ListarResultado[$i]["NroTabuladora"],

                    'FechaExtraccion' => vfecha(substr($ListarResultado[$i]['FechaExtraccion'],0,10)).' '.substr($ListarResultado[$i]['FechaExtraccion'],11,8),	
					'FechaVencimiento' => vfecha(substr($ListarResultado[$i]['FechaVencimiento'],0,10)).' '.substr($ListarResultado[$i]['FechaVencimiento'],11,5),	

					'Fenotipo' => $ListarResultado[$i]["Fenotipo"],
					'TipoDonacion' => $ListarResultado[$i]["TipoDonacion"],

					'FechaMovimiento' => $FechaMovimiento,
					'EstadoDescripcion' => $EstadoDescripcion,  
					'VolumenRestante' => $ListarResultado[$i]["VolumenRestante"],
					'Observacion'  => $Observacion,					
                );

                $RegresoBUT=isset($ListarResultado[$i]['RegresoBUT']) ? ($ListarResultado[$i]['RegresoBUT']) : 0;	

				$FechaRecBUTX=$ListarResultado[$i]['FechaRecBUT'];	
				$FechaRecBUT=isset($FechaRecBUTX) ? (vfecha(substr($FechaRecBUTX,0,10)).' '.substr($FechaRecBUTX,11,5)) : '';
				
            }//fin for

            	if($RegresoBUT!=0){
	                $data[$i] = array(
	                    'Nro' => $i+2,	
	                    'SNCS'	=> '',			
						'EstablecimientoIngreso' =>'',
	                    'FechaRecibe' => '',					
						'EstablecimientoEgreso' => '',
						//'Postulante' => strtoupper($ListarResultado[$i]["ApellidosPostulante"].' '.$ListarResultado[$i]["NombresPostulante"]),
	                    'NroDonacion' => '',     
	                    'GrupoSanguineo'=> '',
	                    'NroTabuladora'=> '',

	                    'FechaExtraccion' => '',	
						'FechaVencimiento' => '',	

						'Fenotipo' => '',
						'TipoDonacion' => '',

						'FechaMovimiento' => $FechaRecBUT,
						'EstadoDescripcion' => 'BUT',  
						'Observacion'  => 'RETORNO Y SE ELIMINO',											
	                );
           		 }

            echo json_encode($data);
            exit();	
	 //include('../../MVC_Vista/Emergencia/ListaAdmisionEmerg.php');
}

if($_REQUEST["acc"] == "ImprimirSolicitud") 
{ 		
	//$IdPaciente=$_REQUEST["IdPaciente"];
	$IdSolicitudSangre=$_REQUEST["IdSolicitudSangre"];	
	include('../../MVC_Vista/BancoSangre/ImprimirSolicitud_pdf.php');
}

if($_REQUEST["acc"] == "ImprimirSolicitudBancoSangre") 
{ 	
	include('../../MVC_Vista/BancoSangre/ImprimirSolicitudBancoSangre_pdf.php');
}

if($_REQUEST["acc"] == "ReporteExcel2") 
{ 	
	$fecha_inicio=gfecha($_REQUEST["fecha_inicio2"]);
	$fecha_fin=gfecha($_REQUEST["fecha_fin2"]);
	include('../../MVC_Vista/BancoSangre/TablaExcelReporte2.php');	
}

if($_REQUEST["acc"] == "ConsultarHemocomponentes") 
{ 	
	$MovTipo=isset($_REQUEST['MovTipoB']) ? $_REQUEST['MovTipoB'] : '';
	$TipoHem=isset($_REQUEST['TipoHemB']) ? $_REQUEST['TipoHemB'] : '';
	$Estado=isset($_REQUEST['EstadoB']) ? trim($_REQUEST['EstadoB']) : '';
	//BuscarSNCSDisponible_M($TipoHem,$SNCS);
	include('../../MVC_Vista/BancoSangre/ConsultarHemocomponentes.php');	
}

if($_REQUEST["acc"] == "GuardarOrdenFacturacion") 
{ 			 

	$IdReserva=$_REQUEST['IdReserva'];
	$idOrden=$_REQUEST['idOrdenFac'];
	$FactCabecera=FactOrdenServicioSeleccionarPorIdOrden_M($idOrden);
	$IdPaciente=$FactCabecera[0]["IdPaciente"];
	$IdPacienteVal=$_REQUEST['IdPacienteVal'];

	if($FactCabecera==NULL){
		$mensaje="Nro de Orden NO EXISTE";
		print "<script>alert('$mensaje')</script>";	

	}else if($IdPaciente!=$IdPacienteVal){
		$Paciente=$FactCabecera[0]["ApellidoPaterno"].' '.$FactCabecera[0]["ApellidoMaterno"].' '.$FactCabecera[0]["PrimerNombre"];
		$mensaje="Nro de Orden pertenece a OTRO paciente: ".$Paciente;
		print "<script>alert('$mensaje')</script>";	

	}else if($FactCabecera[0]["IdPuntoCarga"]!=1 && $FactCabecera[0]["IdPuntoCarga"]!=11){		
		$mensaje="Punto de Carga ".$FactCabecera[0]["IdPuntoCarga"].' No pertenece a Consumo en el Servicio';
		print "<script>alert('$mensaje')</script>";	

	}else{
		UpdateOrdenReserva($IdReserva,$idOrden);
		$mensaje="Nro de Orden Relacionado correctamente";
		print "<script>alert('$mensaje')</script>";	
	}

	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
	
}



if($_REQUEST["acc"] == "GuardarOrdenFenotipo") 
{ 			 

	$IdSolicitudSangre=$_REQUEST['IdSolicitudSangre3'];
	$idOrden=$_REQUEST['IdOrdenFen'];
	$FactCabecera=FactOrdenServicioSeleccionarPorIdOrden_M($idOrden);
	$IdPaciente=$FactCabecera[0]["IdPaciente"];
	$IdPacienteVal=$_REQUEST['IdPaciente3'];

	if($FactCabecera==NULL){
		$mensaje="Nro de Orden NO EXISTE";
		print "<script>alert('$mensaje')</script>";	

	}else if($IdPaciente!=$IdPacienteVal){
		$Paciente=$FactCabecera[0]["ApellidoPaterno"].' '.$FactCabecera[0]["ApellidoMaterno"].' '.$FactCabecera[0]["PrimerNombre"];
		$mensaje="Nro de Orden pertenece a OTRO paciente: ".$Paciente;
		print "<script>alert('$mensaje')</script>";	

	}else if($FactCabecera[0]["IdPuntoCarga"]!=1 && $FactCabecera[0]["IdPuntoCarga"]!=11){		
		$mensaje="Punto de Carga ".$FactCabecera[0]["IdPuntoCarga"].' No pertenece a Consumo en el Servicio';
		print "<script>alert('$mensaje')</script>";	

	}else{		
		SIGESA_BSD_EnviarReceptoresTablaEnvio($idOrden);	

		UpdateOrdenFenotipo($IdSolicitudSangre,$idOrden);
		$mensaje="Nro de Orden Guardada correctamente";
		print "<script>alert('$mensaje')</script>";	
	}

	include('../../MVC_Vista/BancoSangre/ListaEsperaSolicitud.php');
	
}


if($_REQUEST["acc"] == "BuscarPacientesDebenSangre") 
{ 	
	$NroHistoriaClinicaBusRec=isset($_REQUEST['NroHistoriaClinica']) ? ($_REQUEST['NroHistoriaClinica']) : "";
	include('../../MVC_Vista/BancoSangre/BuscarPacientesDebenSangre.php');	
}

if($_REQUEST["acc"] == "AbrirSolicitud") 
{ 	
	$NroSolicitud=isset($_REQUEST['NroSolicitudB']) ? ($_REQUEST['NroSolicitudB']) : "";
	include('../../MVC_Vista/BancoSangre/AbrirSolicitud.php');	
}

if($_REQUEST["acc"] == "GuardarAbrirSolicitud") 
{ 	
	$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
	$FechaCierre=$FechaServidor;
	$UsuRegCierre=$_REQUEST["IdEmpleado"];

	$Estado=$_REQUEST["EstadoAnterior"];
	AbrirDetSolicitud($IdDetSolicitudSangre,$FechaCierre,$UsuRegCierre,$Estado);

	$mensaje="El detalle de la solicitud fue ABIERTA correctamente";
	print "<script>alert('$mensaje')</script>";
	$NroSolicitud=isset($_REQUEST['NroSolicitudB']) ? ($_REQUEST['NroSolicitudB']) : "";
	include('../../MVC_Vista/BancoSangre/AbrirSolicitud.php');
}


?>