<?php
include('../../MVC_Modelo/CajaM.php');
include('../../MVC_Modelo/OrdenServicioM.php');
include('../../MVC_Modelo/HistoriaClinicaM.php');
include('../../MVC_Modelo/FinanciamientoM.php');
include('../../MVC_Modelo/ServiciosM.php');
include('../../MVC_Modelo/SistemaM.php');
include('../../MVC_Complemento/librerias/Funciones.php');
include('../../MVC_Modelo/Servicio_SocialM.php'); 
include('../../MVC_Complemento/fpdf/fpdf.php');
/***************************/
/*** Variables Glovales ***/
/**************************/
 $ListarFechaServidor=RetornaFechaServidorSQL_C();
 $FechaServidor = $ListarFechaServidor[0][0];
 $IdEmpleado=$_REQUEST["IdEmpleado"];
 /****************/
/*** ACCIONES ***/
/****************/



		function Retornar_Usuario_Extorno($IdRegistro,$tipo)
		{
			
			$data = Devolver_Usuario_Extorno_M($IdRegistro);
			$ApellidoPaterno=$data[0]['ApellidoPaterno'];
			$ApellidoMaterno=$data[0]['ApellidoMaterno'];
			$Nombres=$data[0]['Nombres'];	
			if($tipo==1)
			{
			return $ApellidoPaterno.' '.$ApellidoMaterno.' '.$Nombres;		
			}
			else
			{
			return substr($ApellidoPaterno,0,1).'.'.substr($ApellidoMaterno,0,1).'.'.substr($Nombres,0,1);	
			}
		}

		function Retornar_Total_Recaudado($IdOrden)
		{
			
			$data = Retornar_Total_Recaudado_M($IdOrden);
			$Total=$data[0]['Total'];
			return number_format($Total,2);		
		}
		
		
		
		function Retornar_Importe_Exonerado_Farmacia($MovNumero,$MovTipo,$IdProducto)
		{
			
			$data = Exoneraciones_FacturacionBienes_Financiamientos_Solo_Exonerados($MovNumero,$MovTipo,$IdProducto);
			if (count($data) >= 1) 
			{
			$TotalFinanciado=$data[0]['totalfinanciado'];	
			return number_format($TotalFinanciado,2);	
			}
			else
			{
			return  number_format('0',2);
			}
		}
		
		
		
		function Retornar_Importe_Exonerado_Servicios($IdOrden,$IdProducto)
		{
			
			$data = Exoneraciones_Facturacion_Servicio_Financiamiento_Buscar($IdOrden,$IdProducto);
			if (count($data) >= 1) 
			{
			$TotalFinanciado=$data[0]['TotalFinanciado'];	
			return number_format($TotalFinanciado,2);	
			}
			else
			{
			return  number_format('0',2);
			}
		}
		function Retornar_Deuda_Total($Idcuenta) {
			 

				$data = Exoneraciones_Grilla_Medicamentos_Ambulatorio_Cuenta($Idcuenta);
				if (count($data) >= 1) 
				{			
					for ($j=0; $j < count($data); $j++) 
					{
					$total=$data[$j]['IMPORTE']-Retornar_Importe_Exonerado_Farmacia($data[$j]['MOVNUMERO'],$data[$j]['MOVTIPO'],$data[$j]['IDPRODUCTO']);

					$suma_exonerado=$suma_exonerado+Retornar_Importe_Exonerado_Farmacia($data[$j]['MOVNUMERO'],$data[$j]['MOVTIPO'],$data[$j]['IDPRODUCTO']);
					
					if($data[$j]['IDPRODUCTO']!=4691 or $data[$j]['IDPRODUCTO']!=6035)
					{
					$total_pagar=$total_pagar+$data[$j]['IMPORTE'];
					}
					
					if($data[$j]['IDESTADOFACTURACION']==4 and  $data[$j]['IDPRODUCTO']==4691)
					{
					$total_pago_cuenta=$total_pago_cuenta+$data[$j]['Importe'];	
					}
					
					if($data[$j]['IDESTADOFACTURACION']==4)
					{
					$total_pagado=$total_pagado+$data[$j]['IMPORTE']-Retornar_Importe_Exonerado_Farmacia($data[$j]['MOVNUMERO'],$data[$j]['MOVTIPO'],$data[$j]['IDPRODUCTO']);	
					}
					
					if($data[$j]['IDESTADOFACTURACION']==1)
					{
					$total_debe_pagar=$total_debe_pagar+$data[$j]['IMPORTE']-Retornar_Importe_Exonerado_Farmacia($data[$j]['MOVNUMERO'],$data[$j]['MOVTIPO'],$data[$j]['IDPRODUCTO']);	
					}
					}
				}
				$data = Exoneraciones_Grilla_Servicios_Ambulatorio_Cuenta($Idcuenta);
				if (count($data) >= 1) 
				{			
					for ($i=0; $i < count($data); $i++) 
					{

					$total=$data[$i]['Importe']-Retornar_Importe_Exonerado_Servicios($data[$i]['IdOrden'],$data[$i]['IdProducto']);
					
					$suma_exonerado=$suma_exonerado+Retornar_Importe_Exonerado_Servicios($data[$i]['IdOrden'],$data[$i]['IdProducto']);	
					
					if($data[$i]['IdProducto']!=4691   and  strlen($data[$i]['IdOrdenPago'])>0)
					{
					$total_pagar=$total_pagar+$data[$i]['Importe'];
					}
					
					if($data[$i]['IdEstadoFacturacion']==4 and  $data[$i]['IdProducto']==4691)
					{
					$total_pago_cuenta=$total_pago_cuenta+$data[$i]['Importe'];	
					}
					
					if($data[$i]['IdEstadoFacturacion']==4 and  $data[$i]['IdProducto']==6035)
					{	
					$total_devolver=$total_devolver+$data[$i]['Importe'];	
					}
					
					if($data[$i]['IdEstadoFacturacion']==4)
					{
					$total_pagado=$total_pagado+$data[$i]['Importe']-Retornar_Importe_Exonerado_Servicios($data[$i]['IdOrden'],$data[$i]['IdProducto']);		
					}
					
					if($data[$i]['IdEstadoFacturacion']==1)
					{
					$total_debe_pagar=$total_debe_pagar+$data[$i]['Importe']-Retornar_Importe_Exonerado_Servicios($data[$i]['IdOrden'],$data[$i]['IdProducto']);		
					}
					
		
					}
					$total_pagado=$total_pagado-$total_devolver;
				}
				
				$data = Exoneraciones_Grilla_dias_Estancia($Idcuenta);
				if (count($data) >= 1) 
				{
		
					for ($r=0; $r < count($data); $r++) 
					{
					$estancia=$estancia+$data[$r]['dias']*$data[$r]['PrecioUnitario'];				
					}
					$estancia=$estancia+$data[$r]['dias']*$data[$r]['PrecioUnitario'];		
					$total_debe_pagar=$total_debe_pagar+$estancia;
					$total_pagar=$total_pagar+$estancia;				
				}
				
				
					$total_debe_pagar=$total_debe_pagar-$total_pago_cuenta+$total_devolver;	
					$total_deuda=$total_pagar-$suma_exonerado;
					if($total_debe_pagar<=0)
					{
					$total_debe_pagar='0.00';	
					}						
			return number_format($total_debe_pagar,2);
		
		}
		
        if($_REQUEST["acc"] == "PDF_Aseguradoras")
		{
			class PDF extends FPDF
			{
				function Header()
				{	
					$this->SetFont('Arial','',9);
					$this->Cell(20);
					$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
					$this->Cell(37);
					$this->setfont('arial','b',12);
					$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
					$this->Cell(70);
					$this->SetFont('Arial','',9); 
					$this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
					$this->Ln(4);
					$this->Cell(22,4,'',0,0,'L');
					$this->Cell(135);
					$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
					$this->Ln(3);
					$this->Cell(49);
					$NumPag=$this->PageNo();
					$this->Cell(80,5,strtoupper('Reporte de estado de cuenta de las empresas aseguradoras'),0,0,'C');	
					$this->Cell(25);
					$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
					$this->Image('../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
					$this->Ln(5);
					$this->setfont('arial','b',12);
					$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
					$this->Ln(8);
				}
				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}

			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();

			$pdf->SetFont('Arial','b',9);

			$pdf->Cell(50,5,'NOMBRE DE SERVICIO',1,0,'C');  
			$pdf->Cell(60,5,'NOMBRE DE PACIENTE',1,0,'C');		
			$pdf->Cell(18,5,'H.CLINICA',1,0,'C');
			$pdf->Cell(18,5,'CUENTA',1,0,'C');				
			$pdf->Cell(20,5,'F.INGRESO',1,0,'C');
			$pdf->Cell(30,5,'MONTO TOTAL',1,0,'C');				
			$ListarReporte=Reporte_Consolidado_por_Aseguradora_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]),$_REQUEST["IdFuenteFinanciamiento"],$_REQUEST["IdTipoServicio"]);
				if($ListarReporte != NULL)	{ 
				$Pagado=0;
				$DEVUELTO=0;
				$Anulado=0;
				$DebueltoDia=0;
				$ToTNumDocDevuelDia=0;
				
			  foreach($ListarReporte as $item){  
						$pdf->ln(5);
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(50,5,utf8_decode(substr($item["ServicioNombre"],0,33)),1,0,'L');  
						$pdf->Cell(60,5,utf8_decode(substr($item["NombrePaciente"],0,35)),1,0,'L'); 
						$pdf->Cell(18,5,utf8_decode(substr($item["NroHistoriaClinica"],0,15)),1,0,'C');	
						$pdf->Cell(18,5,utf8_decode(substr($item["IdCuentaAtencion"],0,10)),1,0,'L');							
						$pdf->Cell(20,5,utf8_decode(substr($item["FechaIngreso"],0,10)),1,0,'L');
						$pdf->Cell(30,5,'s/. '.Retornar_Deuda_Total($item['IdCuentaAtencion']),1,0,'L');
						
				}						
			  }
			$pdf->Output('Aseguradoras.pdf','D');
		}

		if($_REQUEST["acc"]=="PDF_Extornos")
		{
			class PDF extends FPDF
			{
			function Header(){	
					$this->SetFont('Arial','',9);
					$this->Cell(20);
					$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
					$this->Cell(37);
					$this->setfont('arial','b',12);
					$this->Cell(150,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
					$this->Cell(1);
					$this->SetFont('Arial','',9); 
					$this->Cell(30,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
					$this->Ln(4);
					$this->Cell(22,4,'',0,0,'L');
					$this->Cell(193);
					$this->Cell(20,4,'H.Imp: '.date("H:i:s"),0,0,'R');
					$this->Ln(3);
					$this->Cell(49);
					$NumPag=$this->PageNo();
					$this->Cell(161,5,'  REPORTE DE EXTORNOS  ',0,0,'C');	
					$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'L');
					$this->Image('../../MVC_Complemento/img/grcallo.jpg',247,5,18,20);
					$this->Ln(5);
					$this->setfont('arial','b',12);
					$this->Cell(265,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
					$this->Ln(8);
				}
				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}

			$pdf = new PDF('L','mm');
			$pdf->AliasNbPages();
			$pdf->AddPage();

			$pdf->SetFont('Arial','b',9);	
			$pdf->Cell(70,5,'Nombre de Cajero',1,0,'C');
			$pdf->Cell(15,5,'U. Ext.',1,0,'C');
			$pdf->Cell(75,5,'Nombre de Paciente',1,0,'C');			
			$pdf->Cell(25,5,'Historia Clinica',1,0,'C');		
			$pdf->Cell(30,5,'Serie Boleta',1,0,'C');		
			$pdf->Cell(20,5,'Monto',1,0,'C');		
			$pdf->Cell(40,5,'Fecha Cobranza',1,0,'C');	
			$ListarReporte=Reporte_Extornos_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
			 if($ListarReporte != NULL)	{ 
				  $suma=0; 
				  $c=0;
				  foreach($ListarReporte as $item){
							$pdf->ln(5);
							$pdf->SetFont('Arial','',9);
							$pdf->Cell(70,5,utf8_decode(substr($item["NombreCajero"],0,35)),1,0,'L');
							$pdf->Cell(15,5,utf8_decode(Retornar_Usuario_Extorno($item['IdComprobantePago'],2)),1,0,'C'); 							
							$pdf->Cell(75,5,utf8_decode(substr($item["RazonSocial"],0,35)),1,0,'L');  
							$pdf->Cell(25,5,utf8_decode($item["NroHistoriaClinica"]),1,0,'L');  
							$pdf->Cell(30,5,utf8_decode($item["NroSerie"]).' - '.utf8_decode($item["NroDocumento"]),1,0,'C');  
							$pdf->Cell(20,5,' s/. '.number_format($item["Total"], 2),1,0,'L'); 
							$pdf->Cell(40,5,substr($item['FechaCobranza'],0, 19),1,0,'C'); 
							$suma=$suma+$item["Total"];
							$c=$c+1;
							
					}
					
				  }
							$pdf->ln(5);
							$pdf->SetFont('Arial','b',9);
							$pdf->Cell(215,5,'TOTAL ( '.$c.' ) ',1,0,'C');
							$pdf->Cell(60,5,' s/. '.number_format($suma,2),1,0,'L');  				

			$pdf -> Output('Extornos.pdf', 'D');
			
		}
		
        if($_REQUEST["acc"] == "PDF_Serie")
		{
			
			
			class PDF extends FPDF
			{
				function Header()
				{	
					$this->SetFont('Arial','',9);
					$this->Cell(20);
					$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
					$this->Cell(37);
					$this->setfont('arial','b',12);
					$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION'.$mira,0,0,'C');
					$this->Cell(70);
					$this->SetFont('Arial','',9); 
					$this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
					$this->Ln(4);
					$this->Cell(22,4,'',0,0,'L');
					$this->Cell(135);
					$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
					$this->Ln(3);
					$this->Cell(49);
					$NumPag=$this->PageNo();
					$this->Cell(80,5,strtoupper('Reporte de Serie'),0,0,'C');	
					$this->Cell(25);
					$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
					$this->Image('../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
					$this->Ln(5);
					$this->setfont('arial','b',12);
					$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
					$this->Ln(8);
				}
				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}

			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();

			$pdf->SetFont('Arial','b',9);

			$pdf->Cell(25,5,strtoupper('Nro Serie'),1,0,'C');  	
			$pdf->Cell(25,5,strtoupper('Nro Min'),1,0,'C');		
			$pdf->Cell(25,5,strtoupper('Nro Max'),1,0,'C');
			$pdf->Cell(38,5,strtoupper('Monto Total'),1,0,'C');	
		    $pdf->Cell(38,5,strtoupper('Total Extornado'),1,0,'C');				
			$pdf->Cell(38,5,strtoupper('Total Recaudado'),1,0,'C');					
			$ListarReporte=Reporte_Consolidado_Serie_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
			$Listar_Devoluciones=Reporte_Devoluciones_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
						
				if($Listar_Devoluciones != NULL)
				{
					foreach($Listar_Devoluciones as $item)
					{  
							$Total_Devolucion=$Total_Devolucion+$item["Total"];						
					}
				}
			
			
				if($ListarReporte != NULL)
				{ 
				$Pagado=0;
				$DEVUELTO=0;
				$Anulado=0;
				$DebueltoDia=0;
				$ToTNumDocDevuelDia=0;
					foreach($ListarReporte as $item)
					{  
							$pdf->ln(5);
							$pdf->SetFont('Arial','',9);
							$pdf->Cell(25,5,utf8_decode(substr($item["NroSerie"],0,40)),1,0,'C'); 
							$pdf->Cell(25,5,utf8_decode(substr($item["NrodocumentoMax"],0,35)),1,0,'L');
							$pdf->Cell(25,5,utf8_decode(substr($item["NrodocumentoMin"],0,40)),1,0,'L');   						
							$pdf->Cell(38,5,' s/. '.number_format($item["Total"]-$item["Anulado"],2),1,0,'L');
							$pdf->Cell(38,5,' s/. '.number_format($item["Anulado"],2),1,0,'L');
							$pdf->Cell(38,5,' s/. '.number_format($item["Pagado"]-$item["Devuelto"],2),1,0,'L');
							$Total=$Total+$item["Total"]-$item["Anulado"];
							$Pagado=$Pagado+$item["Pagado"]-$item["Devuelto"];	
							$Anulado=$Anulado+$item["Anulado"];	
							$Devuelto=$Devuelto+$item["Devuelto"];							
					}
							$pdf->ln(5);
							$pdf->SetFont('Arial','',10);
							$pdf->Cell(75,5,strtoupper('Total'),1,0,'C'); 						
							$pdf->Cell(38,5,' s/. '.number_format($Total,2),1,0,'L');
							$pdf->Cell(38,5,' s/. '.number_format($Anulado,2),1,0,'L');								
							$pdf->Cell(38,5,' s/. '.number_format($Pagado-$Total_Devolucion,2),1,0,'L');
											
			  }
			$pdf->Output('Serie.pdf','D');	
		}

		
		
		
        if($_REQUEST["acc"] == "PDF_Liquidaciones")
		{
			
			
			class PDF extends FPDF
			{
				function Header()
				{	
					$this->SetFont('Arial','',9);
					$this->Cell(20);
					$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
					$this->Cell(37);
					$this->setfont('arial','b',12);
					$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION'.$mira,0,0,'C');
					$this->Cell(70);
					$this->SetFont('Arial','',9); 
					$this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
					$this->Ln(4);
					$this->Cell(22,4,'',0,0,'L');
					$this->Cell(135);
					$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
					$this->Ln(3);
					$this->Cell(49);
					$NumPag=$this->PageNo();
					$this->Cell(80,5,strtoupper('Reporte de Deudas'),0,0,'C');	
					$this->Cell(25);
					$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
					$this->Image('../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
					$this->Ln(5);
					$this->setfont('arial','b',12);
					$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
					$this->Ln(8);
				}
				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}

			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();

			$pdf->SetFont('Arial','b',9);

			$pdf->Cell(47,5,'NOMBRE SERVICIO',1,0,'C');  
			$pdf->Cell(55,5,'NOMBRE PACIENTE',1,0,'C');	
			$pdf->Cell(20,5,strtoupper('Cuenta'),1,0,'C');		
			$pdf->Cell(20,5,strtoupper('Hc'),1,0,'C');	
			$pdf->Cell(24,5,strtoupper('F. Ingreso'),1,0,'C');
			$pdf->Cell(24,5,strtoupper('Monto'),1,0,'C');	
			$ListarReporte=Reporte_Liquidaciones_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
				if($ListarReporte != NULL)	
				{ 	
				foreach($ListarReporte as $item){  
						$pdf->ln(5);
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(47,5,utf8_decode(substr($item["ServicioNombre"],0,35)),1,0,'L');
						$pdf->Cell(55,5,utf8_decode(substr($item["NombrePaciente"],0,40)),1,0,'L');  
						$pdf->Cell(20,5,utf8_decode(substr($item["IdCuentaAtencion"],0,40)),1,0,'C');	
						$pdf->Cell(20,5,utf8_decode(substr($item["NroHistoriaClinica"],0,40)),1,0,'C');  
						$pdf->Cell(24,5,utf8_decode(substr($item["FechaIngreso"],0,10)),1,0,'C');  			
						$pdf->Cell(24,5,utf8_decode(" s/. ".Retornar_Deuda_Total($item["IdCuentaAtencion"])),1,0,'L');	
				}						
			  }
			$pdf->Output('Liquidaciones.pdf','D');	
		}
		
		
		
				

		
		
		
		

        if($_REQUEST["acc"] == "PDF_Liquidaciones_Hospitalizados")
		{
			
			
			class PDF extends FPDF
			{
				function Header(){	
						$this->SetFont('Arial','',9);
						$this->Cell(20);
						$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
						$this->Cell(37);
						$this->setfont('arial','b',12);
						$this->Cell(150,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
						$this->Cell(1);
						$this->SetFont('Arial','',9); 
						$this->Cell(30,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
						$this->Ln(4);
						$this->Cell(22,4,'',0,0,'L');
						$this->Cell(193);
						$this->Cell(20,4,'H.Imp: '.date("H:i:s"),0,0,'R');
						$this->Ln(3);
						$this->Cell(49);
						$NumPag=$this->PageNo();
						$this->Cell(161,5,'  REPORTE DE LIQUIDACIONES  ',0,0,'C');	
						$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'L');
						$this->Image('../../MVC_Complemento/img/grcallo.jpg',247,5,18,20);
						$this->Ln(5);
						$this->setfont('arial','b',12);
						$this->Cell(265,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
						$this->Ln(8);
				}
				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}
			$pdf = new PDF('L','mm');
			$pdf->AliasNbPages();
			$pdf->AddPage();

			$pdf->SetFont('Arial','b',9);

			$pdf->Cell(50,5,'NOMBRE SERVICIO',1,0,'C');  
			$pdf->Cell(65,5,'NOMBRE PACIENTE',1,0,'C');	
			$pdf->Cell(20,5,strtoupper('Cuenta'),1,0,'C');		
			$pdf->Cell(20,5,strtoupper('Hc'),1,0,'C');	
			$pdf->Cell(25,5,strtoupper('F. Cobranza'),1,0,'C');
			$pdf->Cell(30,5,strtoupper('Monto'),1,0,'C');	
			$ListarReporte=Reporte_Liquidaciones_Hospitalizados_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
				if($ListarReporte != NULL)	
				{ 	
				foreach($ListarReporte as $item){  
						$pdf->ln(5);
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(50,5,utf8_decode(substr($item["ServicioNombre"],0,35)),1,0,'L');
						$pdf->Cell(65,5,utf8_decode(substr($item["NombrePaciente"],0,37)),1,0,'L');  
						$pdf->Cell(20,5,utf8_decode(substr($item["IdCuentaAtencion"],0,40)),1,0,'C');	
						$pdf->Cell(20,5,utf8_decode(substr($item["NroHistoriaClinica"],0,40)),1,0,'C');  
						$pdf->Cell(25,5,utf8_decode(substr($item["FechaCobranza"],0,10)),1,0,'C');  			
						$pdf->Cell(30,5,utf8_decode(" s/. ".number_format($item["Total"],2)),1,0,'L');
						$Total=$Total+$item["Total"];
				}
						$pdf->ln(5);
						$pdf->SetFont('Arial','B',10);
						$pdf->Cell(180,5,strtoupper('Total'),1,0,'C'); 						
						$pdf->Cell(30,5,' s/. '.number_format($Total,2),1,0,'L');				
			  }
			$pdf->Output('Liquidaciones.pdf','D');	
		}
		
		
		
		
		
		if($_REQUEST["acc"] == "PDF_Devoluciones")
		{
			
			
			class PDF extends FPDF
			{
				function Header(){	
						$this->SetFont('Arial','',9);
						$this->Cell(20);
						$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
						$this->Cell(37);
						$this->setfont('arial','b',12);
						$this->Cell(150,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
						$this->Cell(1);
						$this->SetFont('Arial','',9); 
						$this->Cell(30,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
						$this->Ln(4);
						$this->Cell(22,4,'',0,0,'L');
						$this->Cell(193);
						$this->Cell(20,4,'H.Imp: '.date("H:i:s"),0,0,'R');
						$this->Ln(3);
						$this->Cell(49);
						$NumPag=$this->PageNo();
						$this->Cell(161,5,'  REPORTE DEVOLUCIONES DEL DIA ',0,0,'C');	
						$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'L');
						$this->Image('../../MVC_Complemento/img/grcallo.jpg',247,5,18,20);
						$this->Ln(5);
						$this->setfont('arial','b',12);
						$this->Cell(265,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
						$this->Ln(8);
				}
				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}

			$pdf = new PDF('L','mm');
			$pdf->AliasNbPages();
			$pdf->AddPage();

			$pdf->SetFont('Arial','b',9);

			$pdf->Cell(50,5,'NOMBRE AUTORIZA',1,0,'C');  
			$pdf->Cell(55,5,'NOMBRE CAJERO',1,0,'C');	
			$pdf->Cell(20,5,strtoupper('Nro Serie'),1,0,'C');		
			$pdf->Cell(35,5,strtoupper('Nro Documento'),1,0,'C');	
			$pdf->Cell(50,5,strtoupper('Paciente'),1,0,'C');	
			$pdf->Cell(25,5,strtoupper('F. Pagado'),1,0,'C');
			$pdf->Cell(30,5,strtoupper('Monto'),1,0,'C');	
			$ListarReporte=Reporte_Devoluciones_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
				if($ListarReporte != NULL)	
				{ 	
				foreach($ListarReporte as $item){  
						$pdf->ln(5);
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(50,5,utf8_decode(substr($item["NombreAutoriza"],0,35)),1,0,'L');
						$pdf->Cell(55,5,utf8_decode(substr($item["NombreCajero"],0,37)),1,0,'L');  
						$pdf->Cell(20,5,utf8_decode(substr($item["NroSerie"],0,10)),1,0,'C');	
						$pdf->Cell(35,5,utf8_decode(substr($item["NroDocumento"],0,10)),1,0,'C'); 
						$pdf->Cell(50,5,utf8_decode(substr($item["RazonSocial"],0,30)),1,0,'L');  						
						$pdf->Cell(25,5,utf8_decode(substr($item["FechaPagado"],0,10)),1,0,'C');  			
						$pdf->Cell(30,5,utf8_decode(" s/. ".number_format($item["Total"],2)),1,0,'L');
						$Total=$Total+$item["Total"];
				}
						$pdf->ln(5);
						$pdf->SetFont('Arial','B',10);
						$pdf->Cell(235,5,strtoupper('Total'),1,0,'C'); 						
						$pdf->Cell(30,5,' s/. '.number_format($Total,2),1,0,'L');				
			  }
			$pdf->Output('Devoluciones_del_dia.pdf','D');	
		}
		
		
		
		if($_REQUEST["acc"] == "PDF_Devoluciones_II")
		{
			class PDF extends FPDF
			{
				function Header(){	
						$this->SetFont('Arial','',9);
						$this->Cell(20);
						$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
						$this->Cell(37);
						$this->setfont('arial','b',12);
						$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION'.$mira,0,0,'C');
						$this->Cell(70);
						$this->SetFont('Arial','',9); 
						$this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
						$this->Ln(4);
						$this->Cell(22,4,'',0,0,'L');
						$this->Cell(135);
						$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
						$this->Ln(3);
						$this->Cell(49);
						$NumPag=$this->PageNo();
						$this->Cell(80,5,'  REPORTE DEVOLUCIONES  ',0,0,'C');	
						$this->Cell(25);
						$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
						$this->Image('../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
						$this->Ln(5);
						$this->setfont('arial','b',12);
						$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
						$this->Ln(8);
				}
				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}
			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();

			$pdf->SetFont('Arial','b',9);
			$pdf->Cell(55,5,'NOMBRE CAJERO',1,0,'C');	
			$pdf->Cell(15,5,strtoupper('N. Serie'),1,0,'C');		
			$pdf->Cell(25,5,strtoupper('N.Documento'),1,0,'C');	
			$pdf->Cell(50,5,strtoupper('Paciente'),1,0,'C');	
			$pdf->Cell(25,5,strtoupper('F. Cobranza'),1,0,'C');
			$pdf->Cell(25,5,strtoupper('Monto'),1,0,'C');	
			$ListarReporte=Reporte_Devoluciones_II_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]),$_REQUEST["NroSerie"]);
				if($ListarReporte != NULL)	
				{ 	
				foreach($ListarReporte as $item){  
						$pdf->ln(5);
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(55,5,utf8_decode(substr($item["NombreCajero"],0,37)),1,0,'L');  
						$pdf->Cell(15,5,utf8_decode(substr($item["NroSerie"],0,10)),1,0,'C');	
						$pdf->Cell(25,5,utf8_decode(substr($item["NroDocumento"],0,10)),1,0,'C'); 
						$pdf->Cell(50,5,utf8_decode(substr($item["RazonSocial"],0,30)),1,0,'L');  						
						$pdf->Cell(25,5,utf8_decode(substr($item["FechaCobranza"],0,10)),1,0,'C');  			
						$pdf->Cell(25,5,utf8_decode(" s/. ".number_format($item["Total"],2)),1,0,'L');					
						$Total=$Total+$item["Total"];
				}
						$pdf->ln(5);
						$pdf->SetFont('Arial','B',10);
						$pdf->Cell(170,5,strtoupper('Total'),1,0,'C'); 						
						$pdf->Cell(25,5,' s/. '.number_format($Total,2),1,0,'L');				
			  }
			$pdf->Output('Devoluciones.pdf','D');	
		}
		
		
				
		
		if($_REQUEST["acc"] == "PDF_Pendientes_Pago")
		{
			class PDF extends FPDF
			{
				function Header(){	
						$this->SetFont('Arial','',9);
						$this->Cell(20);
						$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
						$this->Cell(37);
						$this->setfont('arial','b',12);
						$this->Cell(150,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
						$this->Cell(1);
						$this->SetFont('Arial','',9); 
						$this->Cell(30,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
						$this->Ln(4);
						$this->Cell(22,4,'',0,0,'L');
						$this->Cell(193);
						$this->Cell(20,4,'H.Imp: '.date("H:i:s"),0,0,'R');
						$this->Ln(3);
						$this->Cell(49);
						$NumPag=$this->PageNo();
						$this->Cell(161,5,'  REPORTE PENDIENTES DE PAGO ',0,0,'C');	
						$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'L');
						$this->Image('../../MVC_Complemento/img/grcallo.jpg',247,5,18,20);
						$this->Ln(5);
						$this->setfont('arial','b',12);
						$this->Cell(265,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
						$this->Ln(8);
				}
				

				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');			
				}
			}
			$pdf = new PDF('L','mm');
			$pdf->AliasNbPages();
			$pdf->AddPage();
			$pdf->SetFont('Arial','b',9);
			$pdf->Cell(20,5,'Orden',1,0,'C');
			$pdf->Cell(35,5,'Apellido Paterno',1,0,'C');
			$pdf->Cell(35,5,'Apellido Materno',1,0,'C');	
			$pdf->Cell(30,5,'Primer Nombre',1,0,'C');
			$pdf->Cell(25,5,'Cuenta',1,0,'C');	
			$pdf->Cell(25,5,'Historia Clinica',1,0,'C');	
			$pdf->Cell(55,5,'Nombre de Cajero',1,0,'C');
			$pdf->Cell(35,5,'F. Registro',1,0,'C');
			$pdf->Cell(21,5,'Total',1,0,'C');					
			$ListarReporte=Mostrar_Lista_Pendientes_pago_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]),$_REQUEST["Cajeros"]);
				if($ListarReporte != NULL)	
				{ 	
				foreach($ListarReporte as $item){
						$pdf->ln(5);
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(20,5,utf8_decode(substr($item["IdOrden"],0,10)),1,0,'C');  		
						$pdf->Cell(35,5,' '.utf8_decode(substr($item["ApellidoPaterno"],0,37)),1,0,'L');  
						$pdf->Cell(35,5,' '.utf8_decode(substr($item["ApellidoMaterno"],0,10)),1,0,'L');	
						$pdf->Cell(30,5,' '.utf8_decode(substr($item["PrimerNombre"],0,10)),1,0,'L'); 
						$pdf->Cell(25,5,utf8_decode(substr($item["IdCuentaAtencion"],0,30)),1,0,'C');  						
						$pdf->Cell(25,5,utf8_decode(substr($item["NroHistoriaClinica"],0,10)),1,0,'C'); 
						$pdf->Cell(55,5,' '.utf8_decode(substr($item["Empleado"],0,40)),1,0,'L');  			
					    $pdf->Cell(35,5,utf8_decode(Devolver_Fecha_Formato($item["FechaCreacion"])),1,0,'L');  							
						$pdf->Cell(21,5,utf8_decode(" s/. ".Retornar_Total_Recaudado($item['IdOrden'])),1,0,'L');				
						$Total=$Total+Retornar_Total_Recaudado($item['IdOrden']);
				}
				        
						$pdf->ln(5);
						$pdf->SetFont('Arial','B',10);
						$pdf->Cell(225,5,strtoupper('Total'),1,0,'C'); 						
						$pdf->Cell(56,5,' s/. '.number_format($Total,2),1,0,'L');				
			  }
			$pdf->Output('PendientesPago.pdf','D');	
		}
		
		

		if($_REQUEST["acc"] == "Selected_Fuente_Financiamiento")
		{   
		$elegido=$_REQUEST["elegido"];								 
		$resultados=Mostrar_Lista_Tipos_Financiamiento_M($elegido);								 
		if($resultados!=NULL)
		{
			for ($i=0; $i < count($resultados); $i++) 
			{	
			$html .= "<option value=".$resultados[$i]["IdServicio"].">".mb_strtoupper($resultados[$i]["Nombre"])."</option>";
			}
		}
		echo $html;
		}
		
		
		if($_REQUEST["acc"] == "Reportes_Caja") 
		{  	
		include('../../MVC_Vista/Caja/ReportesV.php');	
		}


		if ($_REQUEST["acc"] == "Highchart_Consolidado_por_Servicio"){
			
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Consolidado_por_Servicio_M($Fecha_Inicial,$Fecha_Final);
		$a=0;
		$v='"';
		for ($i=0; $i < count($data); $i++) { 
						if($a==0)
						{$contenido.= "{";
						}else
						{
					    $contenido.=",{";
						}
						$contenido.=$v."name".$v.": ".$v.$data[$i]['Nombre'].'<br>s/.'.number_format($data[$i]['SumaTotal'],2).$v.",";
						$contenido.=$v."y".$v.": ".str_replace(',','',number_format($data[$i]['SumaTotal'],2));
						$contenido.="}";
						$a++;

		}

		echo json_encode('['.$contenido.']');
		}
		

		if ($_REQUEST["acc"] == "Lista_Consolidado_Servicios") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Consolidado_por_Servicio_M($Fecha_Inicial,$Fecha_Final);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'Nombre'			=> $data[$i]['Nombre'],
				'SumaTotal'	=> 's/. '.number_format($data[$i]['SumaTotal'], 2)
				);
		}
		echo json_encode($Lista);
		exit();
		}
 
 
 
 		if ($_REQUEST["acc"] == "Lista_Consolidado_por_Aseguradoras") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$IdFuenteFinanciamiento=$_REQUEST["IdFuenteFinanciamiento"];
		$IdTipoServicio=$_REQUEST["IdTipoServicio"];
		$data = Reporte_Consolidado_por_Aseguradora_M($Fecha_Inicial,$Fecha_Final,$IdFuenteFinanciamiento,$IdTipoServicio);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'NombrePaciente'			=> $data[$i]['NombrePaciente'],
				'ServicioNombre'			=> $data[$i]['ServicioNombre'],
				'Edad'			=> $data[$i]['Edad'],
				'NroDocumento'			=> $data[$i]['NroDocumento'],
				'NroHistoriaClinica'			=> $data[$i]['NroHistoriaClinica'],
			    'TipoServicio'			=> $data[$i]['TipoServicio'],
				'FechaIngreso'			=> substr($data[$i]['FechaIngreso'],0,10),
				'IdCuentaAtencion'			=> $data[$i]['IdCuentaAtencion'],
				'TotalPagar'			=> 's/. '.Retornar_Deuda_Total($data[$i]['IdCuentaAtencion'])			
				);
		}
		echo json_encode($Lista);
		exit();
		}
 
 
  		if ($_REQUEST["acc"] == "Listado_Liquidaciones") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Liquidaciones_M($Fecha_Inicial,$Fecha_Final);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'NombrePaciente'			=> $data[$i]['NombrePaciente'],
				'ServicioNombre'			=> $data[$i]['ServicioNombre'],
				'Edad'			=> $data[$i]['Edad'],
				'NroDocumento'			=> $data[$i]['NroDocumento'],
				'NroHistoriaClinica'			=> $data[$i]['NroHistoriaClinica'],
			    'TipoServicio'			=> $data[$i]['TipoServicio'],
				'FechaIngreso'			=> substr($data[$i]['FechaIngreso'],0,10),
				'IdCuentaAtencion'			=> $data[$i]['IdCuentaAtencion'],
				'TotalPagar'			=> 's/. '.Retornar_Deuda_Total($data[$i]['IdCuentaAtencion'])			
				);
		}
		echo json_encode($Lista);
		exit();
		}

		
  		if ($_REQUEST["acc"] == "Lista_Liquidaciones_Hospitalizaciones") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Liquidaciones_Hospitalizados_M($Fecha_Inicial,$Fecha_Final);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'NombrePaciente'			=> $data[$i]['NombrePaciente'],
				'ServicioNombre'			=> $data[$i]['ServicioNombre'],
				'Edad'			=> $data[$i]['Edad'],
				'NroDocumento'			=> $data[$i]['NroDocumento'],
				'NroHistoriaClinica'			=> $data[$i]['NroHistoriaClinica'],
			    'TipoServicio'			=> $data[$i]['TipoServicio'],
				'FechaCobranza'			=> substr($data[$i]['FechaCobranza'],0,10),
				'IdCuentaAtencion'			=> $data[$i]['IdCuentaAtencion'],
				'TotalPagado'			=> 's/. '.number_format($data[$i]['Total'],2),	
				'FechaIngreso'			=> substr($data[$i]['FechaIngreso'],0,10),
				'FechaEgreso'			=> substr($data[$i]['FechaEgreso'],0,10)					
				);
		}
		echo json_encode($Lista);
		exit();
		}
		
		
		
		
 
		if ($_REQUEST["acc"] == "Lista_Consolidado_por_Serie") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Consolidado_Serie_M($Fecha_Inicial,$Fecha_Final);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'NrodocumentoMax'					=> $data[$i]['NrodocumentoMax'],
				'NrodocumentoMin'				=> $data[$i]['NrodocumentoMin'],
				'NroSerie'				=> $data[$i]['NroSerie'],
				'Total'				=> 's/. '.number_format($data[$i]['Total'],2),
				'Anulado'				=> 's/. '.number_format($data[$i]['Anulado'],2),
				'Pagado'				=> 's/. '.number_format($data[$i]['Pagado']-$data[$i]['Devuelto'],2)
				);
		}
		echo json_encode($Lista);
		exit();
		}
 

		if ($_REQUEST["acc"] == "Listado_Boletas") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Liquidaciones_M($Fecha_Inicial,$Fecha_Final);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'NombreCajero'			=> $data[$i]['ApellidoPaterno'].' '.$data[$i]['ApellidoMaterno'].' '.$data[$i]['Nombres'],
				'NroSerie'					=> $data[$i]['NroSerie'],
				'NroDocumento'				=> $data[$i]['NroDocumento'],
				'RazonSocial'				=> $data[$i]['RazonSocial'],
				'Total'						=> 's/. '.number_format($data[$i]['Total'],2),
				'FechaCobranza'				=> $data[$i]['FechaCobranza'],
				'NroHistoriaClinica'		=> $data[$i]['NroHistoriaClinica'],
				'EstadoComprobante'		=> $data[$i]['EstadoComprobante'],
				'IdEstadoComprobante'		=> $data[$i]['IdEstadoComprobante']
				);
		}
		echo json_encode($Lista);
		exit();
		}
 

		if ($_REQUEST["acc"] == "Listado_Extornos") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Extornos_M($Fecha_Inicial,$Fecha_Final);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'NombreCajero'			=> $data[$i]['NombreCajero'],
				'idDevolucion'			=> $data[$i]['idDevolucion'],
				'NroHistoriaClinica'			=> $data[$i]['NroHistoriaClinica'],
				'IdComprobantePago'			=> $data[$i]['IdComprobantePago'],
				'Total'			=> 's/. '.number_format($data[$i]['Total'], 2),
				'FechaCobranza'			=> $data[$i]['FechaCobranza'],
				'RazonSocial'			=> $data[$i]['RazonSocial'],
				'NroSerie'			=> $data[$i]['NroSerie'],
				'NroDocumento'			=> $data[$i]['NroDocumento'],
				'UsuarioExtorno' => Retornar_Usuario_Extorno($data[$i]['IdComprobantePago'],1)
				);
		}
		echo json_encode($Lista);
		exit();
		}

		
		if ($_REQUEST["acc"] == "Lista_Devoluciones") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$data = Reporte_Devoluciones_M($Fecha_Inicial,$Fecha_Final);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'NombreCajero'			=> $data[$i]['NombreCajero'],
				'NombreAutoriza'			=> $data[$i]['NombreAutoriza'],
				'idComprobantePago'			=> $data[$i]['idComprobantePago'],
				'NroHistoriaClinica'			=> $data[$i]['NroHistoriaClinica'],
				'Total'			=> 's/. '.number_format($data[$i]['Total'], 2),
				'RazonSocial'			=> $data[$i]['RazonSocial'],
				'NroSerie'			=> $data[$i]['NroSerie'],
				'NroDocumento'			=> $data[$i]['NroDocumento'],
				'FechaPagado'			=> substr($data[$i]['FechaPagado'],0,10)
				);
		}
		echo json_encode($Lista);
		exit();
		}
		
		if ($_REQUEST["acc"] == "Lista_Devoluciones_II") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$NroSerie=$_REQUEST["NroSerie"];
		$data = Reporte_Devoluciones_II_M($Fecha_Inicial,$Fecha_Final,$NroSerie);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
			
				'Total'					=> 's/. '.number_format($data[$i]['Total'], 2),
				'RazonSocial'			=> $data[$i]['RazonSocial'],
				'NroSerie'				=> $data[$i]['NroSerie'],
				'NroDocumento'			=> $data[$i]['NroDocumento'],
				'Exoneraciones'			=>' s/. '.number_format($data[$i]['Exoneraciones'], 2),
				'FechaCobranza'			=> substr($data[$i]['FechaCobranza'],0,10),
				'NombreCajero'				=> $data[$i]['NombreCajero']
				);
		}
		echo json_encode($Lista);
		exit();
		}
		
	
		if ($_REQUEST["acc"] == "Lista_Pendientes_Pago") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$Cajeros=$_REQUEST["Cajeros"];
		$data = Mostrar_Lista_Pendientes_pago_M($Fecha_Inicial,$Fecha_Final,$Cajeros);
		for ($i=0; $i < count($data); $i++) { 
			$Lista[$i] = array(
				'ApellidoPaterno'			=> $data[$i]['ApellidoPaterno'],
				'ApellidoMaterno'			=> $data[$i]['ApellidoMaterno'],
				'PrimerNombre'			    => $data[$i]['PrimerNombre'],
				'IdCuentaAtencion'			=> $data[$i]['IdCuentaAtencion'],
				'NroHistoriaClinica'		=> $data[$i]['NroHistoriaClinica'],
				'IdOrden'					=> $data[$i]['IdOrden'],
				'Empleado'					=> $data[$i]['Empleado'],
				'FechaCreacion'				=> Devolver_Fecha_Formato($data[$i]['FechaCreacion']),
				'IdOrden'					=> $data[$i]['IdOrden'],
				'Empleado'					=> $data[$i]['Empleado'],
				'Total'					=> 's/. '.Retornar_Total_Recaudado($data[$i]['IdOrden'])
				);
		}
		echo json_encode($Lista);
		exit();
		}

	
		
		if ($_REQUEST["acc"] == "Exportar_Reporte_Consolidado_Servicios") {
			 
			$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
			$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
			$contenido .= "<title>Reporte de Consolidado por Servicio</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<H2  style='margin-left:30px'>";
			$contenido .= "&nbsp;&nbsp;";
			$contenido .= "Reporte de Consolidado por Servicio";
			$contenido .= "</H2>";
			$contenido .= "<H4 style='margin-left:10px'>";
			$contenido .= "&nbsp;Fecha Desde :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Inicial"]."&nbsp;&nbsp;&nbsp;Hasta :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Final"];
			$contenido .= "</H4>";
			$contenido .= "<br>";
			$contenido .= "<table style='border:#727070 2px solid; margin:100px; padding:500px'>";
			$item=Reporte_Consolidado_por_Servicio_M($Fecha_Inicial,$Fecha_Final);
			$contenido .= "<tr>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=450>Nombre de Servicio</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Monto</td>";
			$contenido .= "</tr>";
			
				for ($i=0; $i < count($item); $i++) { 
				$contenido .= "<tr>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['Nombre']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;;text-align:left'>&nbsp;&nbsp;s/. ".number_format($item[$i]['SumaTotal'], 2)."</td>";
				$contenido .= "</tr>";	
				}
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";

			echo json_encode($contenido);
			exit();




		 
		 
		 
		 }
		
				
		if ($_REQUEST["acc"] == "Exportar_Reporte_Extornos") {
			 
			$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
			$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
			$contenido .= "<title>Reporte de Extornos</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<H2  style='margin-left:30px'>";
			$contenido .= "&nbsp;&nbsp;";
			$contenido .= "Reporte de de Extornos";
			$contenido .= "</H2>";
			$contenido .= "<H4 style='margin-left:10px'>";
			$contenido .= "&nbsp;Fecha Desde :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Inicial"]."&nbsp;&nbsp;&nbsp;Hasta :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Final"];
			$contenido .= "</H4>";
			$contenido .= "<br>";
			$contenido .= "<table style='border:#727070 2px solid; margin:100px; padding:500px'>";
			$item=Reporte_Extornos_M($Fecha_Inicial,$Fecha_Final);
			$contenido .= "<tr>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=200>Nombre de Cajero</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=200>Nombres y Apellidos del Paciente</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Historia <br> Clinica</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Nro de <br> Serie</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Nro de <br> Documento</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Monto</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=150>Fecha de<br>Cobranza</td>";
			$contenido .= "</tr>";
				for ($i=0; $i < count($item); $i++) { 
				$contenido .= "<tr>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NombreCajero']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['RazonSocial']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NroHistoriaClinica']."</td>";	
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NroSerie']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NroDocumento']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;;text-align:left'>&nbsp;&nbsp;s/. ".number_format($item[$i]['Total'], 2)."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".substr($item[$i]['FechaCobranza'], 0, 19)."</td>";
				$contenido .= "</tr>";	
				}
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";

			echo json_encode($contenido);
			exit();

		}	
		
						
		if ($_REQUEST["acc"] == "Exportar_Reporte_Devoluciones") {
			 
			$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
			$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
			$contenido .= "<title>Reporte de Extornos</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<H2  style='margin-left:30px'>";
			$contenido .= "&nbsp;&nbsp;";
			$contenido .= "Reporte de de Extornos";
			$contenido .= "</H2>";
			$contenido .= "<H4 style='margin-left:10px'>";
			$contenido .= "&nbsp;Fecha Desde :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Inicial"]."&nbsp;&nbsp;&nbsp;Hasta :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Final"];
			$contenido .= "</H4>";
			$contenido .= "<br>";
			$contenido .= "<table style='border:#727070 2px solid; margin:100px; padding:500px'>";
			$item=Reporte_Extornos_M($Fecha_Inicial,$Fecha_Final);
			$contenido .= "<tr>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=200>Nombre de Cajero</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=200>Nombres y Apellidos del Paciente</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Historia <br> Clinica</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Nro de <br> Serie</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Nro de <br> Documento</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Monto</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=600>Motivo</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Fecha de<br>Devolucion</td>";
			$contenido .= "</tr>";
				for ($i=0; $i < count($item); $i++) { 
				$contenido .= "<tr>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NombreCajero']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['RazonSocial']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NroHistoriaClinica']."</td>";	
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NroSerie']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NroDocumento']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;;text-align:left'>&nbsp;&nbsp;s/. ".number_format($item[$i]['Total'], 2)."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['motivo']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".substr($item[$i]['fechaDevolucion'], 0, 19)."</td>";
				$contenido .= "</tr>";	
				}
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";

			echo json_encode($contenido);
			exit();

		}		
				
		if ($_REQUEST["acc"] == "Exportar_Reporte_Serie") {
			 
			$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
			$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
			$contenido .= "<title>Reporte de Extornos</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<H2  style='margin-left:30px'>";
			$contenido .= "&nbsp;&nbsp;";
			$contenido .= "Reporte de Serie de Boletas";
			$contenido .= "</H2>";
			$contenido .= "<H4 style='margin-left:10px'>";
			$contenido .= "&nbsp;Fecha Desde :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Inicial"]."&nbsp;&nbsp;&nbsp;Hasta :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Final"];
			$contenido .= "</H4>";
			$contenido .= "<br>";
			$contenido .= "<table style='border:#727070 2px solid; margin:100px; padding:500px'>";
			$item=Reporte_Consolidado_Serie_M($Fecha_Inicial,$Fecha_Final);
			$contenido .= "<tr>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=110>Nro <br>Serie</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=120>Nro Documento <br>Min</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=120>Nro Documento <br>Max</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=130>Monto <br> Total</td>";
			$contenido .= "</tr>";
				for ($i=0; $i < count($item); $i++) { 
				$contenido .= "<tr>";
				$contenido .= "<td style='border:#727070 1px solid;text-align:center'>&nbsp;&nbsp;".$item[$i]['NroSerie']."</td>";
			    $contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NrodocumentoMin']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NrodocumentoMax']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;text-align:left'>&nbsp;&nbsp; S/.".number_format($item[$i]['Total'], 2)."</td>";
				$contenido .= "</tr>";	
				}
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";

			echo json_encode($contenido);
			exit();

		}	
		
		
		
		if ($_REQUEST["acc"] == "Exportar_Reporte_Liquidaciones") {
			 
			$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
			$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
			$contenido .= "<title>Reporte de Liquidaciones</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<H2  style='margin-left:800px'>";
			$contenido .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$contenido .= "Reporte de Liquidaciones";
			$contenido .= "</H2>";
			$contenido .= "<H4 style='margin-left:10px'>";
			$contenido .= "Fecha Desde :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Inicial"]."&nbsp;&nbsp;&nbsp;Hasta :&nbsp;&nbsp;&nbsp;".$_REQUEST["Fecha_Final"];
			$contenido .= "</H4>";
			$contenido .= "<br>";
			$contenido .= "<table style='border:#727070 2px solid; margin:100px; padding:500px'>";
			$item=Reporte_Liquidaciones_M($Fecha_Inicial,$Fecha_Final);
			$contenido .= "<tr>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=250>Nombre de Cajero</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Historia Clinica</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=290>Nombre y Apellido del Paciente</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=190>N° Boleta de Venta</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Monto</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=100>Estado<br>Comprobante</td>";
			$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:#ffffff; font-weight:bold; background-color: #808080; text-align:center' width=190>Fecha Cobranza</td>";
			$contenido .= "</tr>";
			
				for ($i=0; $i < count($item); $i++) { 
				$contenido .= "<tr>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['ApellidoPaterno'].'  '.$item[$i]['ApellidoMaterno'].'  '.$item[$i]['Nombres']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['NroHistoriaClinica']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['RazonSocial']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;text-align:center'>&nbsp;&nbsp;".$item[$i]['NroSerie'].' - '.$item[$i]['NroDocumento'].' '."</td>";
				$contenido .= "<td style='border:#727070 1px solid;;text-align:left'>&nbsp;&nbsp;s/. ".number_format($item[$i]['Total'], 2)."</td>";
				$contenido .= "<td style='border:#727070 1px solid;'>&nbsp;&nbsp;".$item[$i]['EstadoComprobante']."</td>";
				$contenido .= "<td style='border:#727070 1px solid;text-align:left''>&nbsp;&nbsp;".substr($item[$i]['FechaCobranza'], 0, 19)."</td>";
				$contenido .= "</tr>";	
				}
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";

			echo json_encode($contenido);
			exit();




		 
		 
		 
		 }

		if($_REQUEST["acc"] == "Devoluciones") 
		{  
		$FechaActual=substr($FechaServidor,0,10);
		
		$ListarMontoRecaudado=SigesaCajaComprobanteMontoxCajero_C($FechaActual.' 00:00',$FechaActual.' 23:59',$IdEmpleado);
		
		    $MontoTots = (float) $ListarMontoRecaudado[0]["Monto"];
		    $MontoDevolus = (float) $ListarMontoRecaudado[0]["MontoDvol"];
            $ModntoRecaudado=number_format($MontoTots-$MontoDevolus,2);
			$ModntoRecaudadoc=$MontoTots-$MontoDevolus;
			
		$ListarDatosCajero=ListarUsuarioxIdempleado_C($IdEmpleado);	
		 
 		$ApellidoPaterno=$ListarDatosCajero[0]["ApellidoPaterno"];	
		$ApellidoMaterno=$ListarDatosCajero[0]["ApellidoMaterno"];
		$Nombres=$ListarDatosCajero[0]["Nombres"];
		$DNI=$ListarDatosCajero[0]["DNI"];	 
		$Usuario=$ListarDatosCajero[0]["Usuario"];
		/*
		include('../../MVC_Vista/Caja/DevolucionesV_.php');
		*/
		include('../../MVC_Vista/Sistema/Desahibilitado.php');
		}
		if($_REQUEST["acc"] == "Buscar_Devoluciones") 
		{
			  
			  $NroSerie=$_REQUEST["NroSerie"];
			  $NroDocumento=$_REQUEST["NroDocumento"];
			  $IdEmpleado=$_REQUEST["IdEmpleado"];
			  $FechaActual=substr($FechaServidor,0,10);
		
		    $ListarMontoRecaudado=SigesaCajaComprobanteMontoxCajero_C($FechaActual.' 00:00',$FechaActual.' 23:59',$IdEmpleado);
 		
		    $MontoTots = (float) $ListarMontoRecaudado[0]["Monto"];
		    $MontoDevolus = (float) $ListarMontoRecaudado[0]["MontoDvol"];
			$ModntoRecaudado=number_format($MontoTots-$MontoDevolus,2);
			$ModntoRecaudadoc=$MontoTots-$MontoDevolus;
		$ListarDatosCajero=ListarUsuarioxIdempleado_C($IdEmpleado);	
		
		 
 		$ApellidoPaterno=$ListarDatosCajero[0]["ApellidoPaterno"];	
		$ApellidoMaterno=$ListarDatosCajero[0]["ApellidoMaterno"];
		$Nombres=$ListarDatosCajero[0]["Nombres"];
		$DNI=$ListarDatosCajero[0]["DNI"];	 
		$Usuario=$ListarDatosCajero[0]["Usuario"];
		
		 $Mostrar_CajaComprobantesPago=CajaComprobantesPago_Mostrar_C($NroSerie,$NroDocumento);
		 $RUC=$row["RUC"];
    				$FechaCobranza=$Mostrar_CajaComprobantesPago[0]["FechaCobranza"];
    				$NroSerie=$Mostrar_CajaComprobantesPago[0]["NroSerie"];
    				$NroDocumento=$Mostrar_CajaComprobantesPago[0]["NroDocumento"];
    				$RazonSocial=$Mostrar_CajaComprobantesPago[0]["RazonSocial"];
    				$SubTotal=$Mostrar_CajaComprobantesPago[0]["SubTotal"];
    				$IGV=$Mostrar_CajaComprobantesPago[0]["Mostrar_CajaComprobantesPago[0]"];
    				$Total=$Mostrar_CajaComprobantesPago[0]["Total"];
					$IdEstadoComprobante=$Mostrar_CajaComprobantesPago[0]["IdEstadoComprobante"];
					
					$IdComprobantePago=$Mostrar_CajaComprobantesPago[0]["IdComprobantePago"];
 
      			/*$FechaCobranza=substr($Mostrar_CajaComprobantesPago[0]["FechaCobranza"],0,11);
      			$TipoCambio=$Mostrar_CajaComprobantesPago[0]["TipoCambio"];
      			$Observaciones=$Mostrar_CajaComprobantesPago[0]["Observaciones"];
      			$IdTipoComprobante=$Mostrar_CajaComprobantesPago[0]["IdTipoComprobante"];
      			$IdCuentaAtencion=$Mostrar_CajaComprobantesPago[0]["IdCuentaAtencion"];
      			$IdEstadoComprobante=$Mostrar_CajaComprobantesPago[0]["IdEstadoComprobante"];
      			$IdGestionCaja=$Mostrar_CajaComprobantesPago[0]["IdGestionCaja"];
      			$IdTipoPago=$Mostrar_CajaComprobantesPago[0]["IdTipoPago"];
      			$IdTipoOrden=$Mostrar_CajaComprobantesPago[0]["IdTipoOrden"];
      			$Dctos=$Mostrar_CajaComprobantesPago[0]["Dctos"];
      			$IdPaciente=$Mostrar_CajaComprobantesPago[0]["IdPaciente"];
      			$IdCajero=$Mostrar_CajaComprobantesPago[0]["IdCajero"];
      			$idTurno=$Mostrar_CajaComprobantesPago[0]["idTurno"];
      			$idCaja=$Mostrar_CajaComprobantesPago[0]["idCaja"];
      			$idFormaPago=$Mostrar_CajaComprobantesPago[0]["idFormaPago"];
      			$idFarmacia=$Mostrar_CajaComprobantesPago[0]["idFarmacia"];
      			$Exoneraciones=$Mostrar_CajaComprobantesPago[0]["Exoneraciones"];
      			$Adelantos=$Mostrar_CajaComprobantesPago[0]["Adelantos"];
      			$idTipoFinanciamiento=$Mostrar_CajaComprobantesPago[0]["idTipoFinanciamiento"];	 
		  */
		  
	 
		  $Lista_OrdenesServicio=FactOrdenServicioPagos_x_ComprobantePago_C($NroSerie,$NroDocumento);	
		  
				include('../../MVC_Vista/Caja/DevolucionesV.php');
		}
		
if($_REQUEST["acc"] == "Guardar_Devoluciones") 
		{  		 
		        $Observaciones=$_REQUEST["Observaciones"];
			    $IdComprobantePago=$_REQUEST["IdComprobantePago"];
			    $montoDevuelto=$_REQUEST["montoDevuelto"];
			    $IdEmpleado=$_REQUEST["IdEmpleado"];
				 
				 $MontoRecaudado=$_REQUEST["MontoRecaudado"];
				 $IdEstadoComprobante=$_REQUEST["IdEstadoComprobante"];
				
				if($IdEstadoComprobante==4){  
		CajaDevolucionesAgregar_C('',$IdComprobantePago,$montoDevuelto,$montoDevuelto,$FechaServidor,$IdEmpleado,$Observaciones,$IdEmpleado);
		
		
		 $Mostrar_CajaComprobantePagoPorIdComprobante=CajaComprobantePagoPorIdComprobante_C($IdComprobantePago);
		 $NroSerie=$Mostrar_CajaComprobantePagoPorIdComprobante[0]["NroSerie"];
         $NroDocumento=$Mostrar_CajaComprobantePagoPorIdComprobante[0]["NroDocumento"];
		 $Lista_OrdenesServicio=FactOrdenServicioPagos_x_ComprobantePago_C($NroSerie,$NroDocumento);
		 
		 }
		include('../../MVC_Vista/Caja/DevolucionesImprimir.php'); 
		 
		}
         
       
		/* Imprimir Devolucion  */
 
		if($_REQUEST["acc"] == "Imprimir_Devolucion") 
		{  
		   $IdEmpleado=$_REQUEST["IdEmpleado"];
		   $IdComprobantePago=$_REQUEST["IdComprobantePago"];
		 $Mostrar_CajaComprobantePagoPorIdComprobante=CajaComprobantePagoPorIdComprobante_C($IdComprobantePago);
		   $NroSerie=$Mostrar_CajaComprobantePagoPorIdComprobante[0]["NroSerie"];
           $NroDocumento=$Mostrar_CajaComprobantePagoPorIdComprobante[0]["NroDocumento"];
		 $Lista_OrdenesServicio=FactOrdenServicioPagos_x_ComprobantePago_C($NroSerie,$NroDocumento);
		include('../../MVC_Vista/Caja/DevolucionesImprimir.php'); 
		}



	    function CajaComprobantesPago_Mostrar_C($NroSerie,$NroDocumento)
		{
				return CajaComprobantesPago_Mostrar_M($NroSerie,$NroDocumento);
		}


		function FactOrdenServicioPagos_x_ComprobantePago_C($NroSerie,$NroDocumento)
		{
				return FactOrdenServicioPagos_x_ComprobantePago_M($NroSerie,$NroDocumento);
		}
		
		
		
		function CajaComprobantePagoPorIdComprobante_C($IdComprobantePagos)
		{
				return CajaComprobantePagoPorIdComprobante_M($IdComprobantePagos);
		}


	    function CajaComprobante_Devolver_ComprobanteYOrdenServicio_C($IdComprobantePagos,$IdEmpleado)
		{
				return CajaComprobante_Devolver_ComprobanteYOrdenServicio_M($IdComprobantePagos,$IdEmpleado);
		}

	 function  CajaDevolucionesAgregar_C($idDevolucion,$idComprobantePago,$montoDevuelto,$montoTotal,$fechaDevolucion,$idUsuario,$motivo,$IdUsuarioAuditoria)
		{
				return  CajaDevolucionesAgregar_M($idDevolucion,$idComprobantePago,$montoDevuelto,$montoTotal,$fechaDevolucion,$idUsuario,$motivo,$IdUsuarioAuditoria);
		}
		  
  function SigesaCajaComprobanteMontoxCajero_C($FechaInicio,$FechaFin,$IdCajero)
		{
				return SigesaCajaComprobanteMontoxCajero_M($FechaInicio,$FechaFin,$IdCajero);
		}

function RetornaFechaServidorSQL_C(){	
		return RetornaFechaServidorSQL_M(); 
		}	
		
	function ListarUsuarioxIdempleado_C($IdEmpleado){
		return ListarUsuarioxIdempleado_M($IdEmpleado); 
		}	
?>