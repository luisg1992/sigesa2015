<?php
session_start();
include('../../MVC_Modelo/ConsultaExternaM.php');
include('../../MVC_Complemento/librerias/Funciones.php');

	//MOSTRAR PROGRAMACION
	if ($_REQUEST["acc"] == "mostrarPacientes") 
	{
		$nroCuenta 	 = $_REQUEST["nroCuenta"];
		$nroDni 	 = $_REQUEST["nroDni"];
		$histClinica = $_REQUEST["histClinica"];
		$apPaterno 	 = $_REQUEST["apPaterno"];
		$apMaterno 	 = $_REQUEST["apMaterno"];
		$fecTriaje 	 = $_REQUEST["fecTriaje"];

		$servicios_temp 	= mostrarPacientes($nroCuenta, $nroDni, $histClinica, $apPaterno, $apMaterno, $fecTriaje);

		#GENERANDO PROGRAMACIÓN
		for ($i=0; $i < count($servicios_temp); $i++) { 
			$lista_triaje[$i] = array(

				'NroCuenta'		  =>  $servicios_temp[$i]['NroCuenta'],
				'NroHistoria'	  =>  $servicios_temp[$i]['NroHistoria'],
				'ApellidoPaterno' =>  $servicios_temp[$i]['ApellidoPaterno'],
				'ApellidoMaterno' =>  $servicios_temp[$i]['ApellidoMaterno'],
				'PrimerNombre'	  =>  $servicios_temp[$i]['PrimerNombre'],
				'TriajeFecha'	  =>  $servicios_temp[$i]['TriajeFecha'],
				'Consultorios'	  =>  $servicios_temp[$i]['Consultorios'],
				'FechaCitas'	  =>  $servicios_temp[$i]['FechaCitas'],
			);
		}
		echo json_encode($lista_triaje);
		exit();
	}
?>