<?php 
 
  ini_set('error_reporting',0);//para xamp
  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 0);
 ?>
<?php
include('../../MVC_Modelo/SistemaM.php');
include('../../MVC_Modelo/EmergenciaM.php');
include('../../MVC_Modelo/EmergenciaV2M.php');
include('../../MVC_Modelo/FacturacionM.php');
include('../../MVC_Modelo/HistoriaClinicaM.php');
include('../../MVC_Modelo/ServiciosM.php');
include('../../MVC_Complemento/librerias/Funciones.php');
include('../../MVC_Complemento/fpdf/fpdf.php');

//$ListarFechaServidor=RetornaFechaServidorSQL_C();
 //$FechaServidor = $ListarFechaServidor[0][0];
//$IdEmpleado=$_REQUEST["IdEmpleado"];


 		/*********** TOÑO ********/
			if($_REQUEST["acc"] == "PacienteEmerg") // MOSTRAR: Formulario Login
			{  
			 
			 include('../../MVC_Vista/Consulta externa/PacientesV.php');
			 
			}

 		/*************************/

		if($_REQUEST["acc"] == "Modificacion_Rotacion_Camas")
		{  
			$IdCama=$_REQUEST["IdCama"];	
			$IdAtencion=$_REQUEST["IdAtencion"];	
			$IdPaciente=$_REQUEST["IdPaciente"];	
			Modificacion_Rotacion_Camas_M($IdCama,$IdAtencion,$IdPaciente);
		}
		

        if($_REQUEST["acc"] == "PDF_CamasEmergencia")
		{

			class PDF extends FPDF
			{


			function Header(){	
						$this->SetFont('Arial','',9);
						$this->Cell(20);
						$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
						$this->Cell(37);
						$this->setfont('arial','b',12);
						$this->Cell(150,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
						$this->Cell(1);
						$this->SetFont('Arial','',9); 
						$this->Cell(30,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
						$this->Ln(4);
						$this->Cell(22,4,'',0,0,'L');
						$this->Cell(193);
						$this->Cell(20,4,'H.Imp: '.date("H:i:s"),0,0,'R');
						$this->Ln(3);
						$this->Cell(49);
						$NumPag=$this->PageNo();
						$this->Cell(161,5,'  REPORTE CAMAS DE OBSERVACION DE EMERGENCIA ',0,0,'C');	
						$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'L');
						$this->Image('../../MVC_Complemento/img/grcallo.jpg',247,5,18,20);
						$this->Ln(5);
						$this->setfont('arial','b',12);
						$this->Cell(265,5,'SERVICIO DE   '.$_REQUEST["NombreServicio"],0,0,'C');
						$this->Ln(8);
				}


				function Footer()
				{
					 $this->SetY(-10);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
					$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
					$this->Ln(5);
					$this->SetFont('Arial','',9);
					$this->Cell(150,5,'Terminal()',0,0,'L');
							
				}
			}

			$pdf = new PDF('L','mm');
			$pdf->AliasNbPages();
			$pdf->AddPage();
			$pdf->SetFont('Arial','b',9);
			$pdf->Cell(15,5,'CAMA',1,0,'C');	
			$pdf->Cell(20,5,strtoupper('H.CLINICA '),1,0,'C');		
			$pdf->Cell(40,5,strtoupper('PRIMER NOMBRE'),1,0,'C');	
			$pdf->Cell(40,5,strtoupper('APELLIDO PATERNO'),1,0,'C');
			$pdf->Cell(40,5,strtoupper('APELLIDO MATERNO '),1,0,'C');
			$pdf->Cell(40,5,strtoupper('FECHA INGRESO '),1,0,'C');
			$pdf->Cell(20,5,strtoupper('ESTANCIA '),1,0,'C');
			$pdf->Cell(15,5,strtoupper('EDAD '),1,0,'C');
			$pdf->Cell(40,5,strtoupper('SERVICIO'),1,0,'C');
			$IdServicio=$_REQUEST["IdServicio"];
			$data = MostrarListadoCamasEmergencia($IdServicio);
			//Inicio de Bucle
			for ($i=0; $i < count($data); $i++) {
						$pdf->ln(5);
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(15,5,utf8_decode($data[$i]['Codigo']),1,0,'C');	
						$pdf->Cell(20,5,utf8_decode(substr($data[$i]['NroHistoriaClinica'],0,40)),1,0,'C');	
						$pdf->Cell(40,5,utf8_decode(substr($data[$i]['PrimerNombre'],0,20)),1,0,'L');  
						$pdf->Cell(40,5,utf8_decode(substr($data[$i]['ApellidoMaterno'],0,20)),1,0,'L');  	
						$pdf->Cell(40,5,utf8_decode(substr($data[$i]['ApellidoPaterno'],0,20)),1,0,'L'); 
						$pdf->Cell(40,5,utf8_decode(substr($data[$i]['FechaIngreso'],0,40)),1,0,'C'); 
						$pdf->Cell(20,5,utf8_decode(substr($data[$i]['Estancia'],0,40)),1,0,'C'); 
						$pdf->Cell(15,5,utf8_decode(substr($data[$i]['Edad'],0,40)),1,0,'C'); 
						$pdf->Cell(40,5,utf8_decode(substr($data[$i]['Nombre'],0,20)),1,0,'L'); 
	
			}
			$pdf->Output('CAMASEMERGENCIA_'.$_REQUEST["NombreServicio"].'.pdf','D');	
		}

		if($_REQUEST["acc"] == "CamasEmergencia") 
		{  
			$ServiciosEmergencia=ServiciosEmergencia_C();
			include('../../MVC_Vista/Emergencia/CamasV.php');
		}



		if($_REQUEST["acc"] == "Camas_Disponibles_Observacion")
		{   
			
			$Servicio=$_REQUEST["Servicio"];	
			$resultados=Camas_Disponibles_Observacion_M($Servicio);	
			$html="<table>";							 
			if($resultados!=NULL)
			{
			for ($i=0; $i < count($resultados); $i++) 
			{	
			$html .= "<tr><td><img src='../../MVC_Complemento/img/cama_disponible.png '  style='width: 24px; height: 26px '></td><td><span style='margin:5px;font-weight:bold;color:green'>".mb_strtoupper($resultados[$i]["Codigo"])."</span></td></tr>";
			}
			$html .="</table>";	
			$html .="<div style='margin:15px 0 0 0;font-weight:bold;border:2px dashed #D6D3D2;width:100px;padding:4px'>Total : ".count($resultados)." Camas</div>";	
			}
			echo $html;	
		}


		if($_REQUEST["acc"] == "Camas_Ocupadas_Observacion")
		{   
			
			$Servicio=$_REQUEST["Servicio"];									 
			$resultados=Camas_Ocupadas_Observacion_M($Servicio);								 
			if($resultados!=NULL)
			{
			$html="<table>";	
			for ($i=0; $i < count($resultados); $i++) 
			{	
			$html .= "<tr><td><img src='../../MVC_Complemento/img/cama_ocupada.png' style='width: 24px; height: 26px '></td><td><span style='margin:5px;font-weight:bold;color:red'>".mb_strtoupper($resultados[$i]["Codigo"])."</span></td></tr>";
			}
			$html .="</table>";
			$html .="<div style='margin:15px 0 0 0;font-weight:bold;border:2px dashed #D6D3D2;width:100px;padding:4px'>Total : ".count($resultados)." Camas</div>";		
			}
			echo $html;	
		}





		if($_REQUEST["acc"] == "Limpiar_Camas") 
		{  

			$IdServicio=$_REQUEST["IdServicio"];
			$Limpiar_Camas=Limpiar_Camas_M($IdServicio);
		}



		if ($_REQUEST["acc"] == "Rotacion_Lista_Disponibles"){
		$IdPaciente =  $_REQUEST["IdPaciente"];
		$IdServicio =  $_REQUEST["IdServicio"];
		$data = Rotacion_por_Paciente_M($IdPaciente,$IdServicio);
		$j=0;
		for ($i=0; $i < count($data); $i++) {
			if($data[$i]['IdCama']!= ''){
				 $Lista[$j] = array(
				'IdCama'				=> $data[$i]['IdCama'],
				'IdAtencion'			=> $data[$i]['IdAtencion'],
				'IdPaciente'			=> $data[$i]['IdPaciente'],
				'ApellidoPaterno'		=> $data[$i]['ApellidoPaterno'],
				'ApellidoMaterno'		=> $data[$i]['ApellidoMaterno'],
				'PrimerNombre'			=> $data[$i]['PrimerNombre'],
				'Cama'					=> $data[$i]['Codigo'],
				'NroHistoriaClinica'	=> $data[$i]['NroHistoriaClinica']
				);
			$j=$j+1;	 
			}
		}
		echo json_encode($Lista);
		exit();
		}


		if ($_REQUEST["acc"] == "Lista_Movimientos_Paciente") {
		$IdPaciente =  $_REQUEST["IdPaciente"];
		$data = Movimientos_Emergencia_Paciente_M($IdPaciente);
		for ($i=0; $i < count($data); $i++) {
				 $Lista[$i] = array(
				'FechaIngreso'		=> substr($data[$i]['FechaIngreso'],0,10),
				'HoraIngreso'		=> $data[$i]['HoraIngreso'],
				'Nombre'			=> $data[$i]['Nombre'],
				'Descripcion'		=> $data[$i]['Descripcion'],
				'IdAtencion'		=> $data[$i]['IdAtencion']
				);
		}
		echo json_encode($Lista);
		exit();
		}
		


		if ($_REQUEST["acc"] == "Lista_Antimicrobianos_Cargados_Paciente") {
		$IdPaciente =  $_REQUEST["IdPaciente"];
		$data = Antimicrobianos_Emergencia_Paciente_M($IdPaciente);
		for ($i=0; $i < count($data); $i++) {
				 $Lista[$i] = array(
				'FechaRegistro'	=> substr($data[$i]['FechaRegistro'],0,10),
				'Nombre'		=> $data[$i]['Nombre'],
				'Dosis'			=> $data[$i]['Dosis'],
				'Frecuencia'	=> $data[$i]['Frecuencia'],
				'Total'			=> $data[$i]['Total'],
				'Estado'		=> $data[$i]['Descripcion'],
				'Dias'			=> $data[$i]['Dias']
				);
		}
		echo json_encode($Lista);
		exit();
		}



		if ($_REQUEST["acc"] == "Diagnosticos_Atencion") {
		$IdAtencion =  $_REQUEST["IdAtencion"];
		$data = Diagnosticos_Atencion_M($IdAtencion);
		for ($i=0; $i < count($data); $i++) {
				 $Lista[$i] = array(
				'Descripcion'		=> $data[$i]['Descripcion'],
				'CodigoCIE10'		=> $data[$i]['CodigoCIE10'],
				'Clasificacion'		=> $data[$i]['Clasificacion']
				);
		}
		echo json_encode($Lista);
		exit();
		}
		


		if ($_REQUEST["acc"] == "FarmaciaEstadoCuenta") {
		$IdCuentaAtencion = $_REQUEST["IdCuentaAtencion"];
		$respuesta = SigesaEstadoCuentaFarmaciaxNumeroCuenta($IdCuentaAtencion);
		if (!$respuesta) {
			echo "R_1";
		}
		else
		{	
				for ($i=0; $i < count($respuesta); $i++) { 
					$data_general[$i] = array(
				'FechaHoraPrescribe'=> vfecha(substr($respuesta[$i]["FechaHoraPrescribe"],0,10)),	
				'Cuenta' 	=> $respuesta[$i]["idCuentaAtencion"],	
				'DocumentoNumero' 	=> $respuesta[$i]["DocumentoNumero"],	
				'Codigo' 			=> $respuesta[$i]["Codigo"],	
				'Nombre' 			=> $respuesta[$i]["Nombre"],	
				'cantidad' 			=> $respuesta[$i]["cantidad"],	
				'precio' 			=> 's/. '.number_format($respuesta[$i]["precio"], 2, '.', ''),	
				'total' 			=> 's/. '.number_format($respuesta[$i]["total"], 2, '.', ''),	
				'descripcion' 		=> $respuesta[$i]["descripcion"],
				'TipoProducto' 		=> $respuesta[$i]["TipoProducto"]
		);
		}
		echo json_encode($data_general);
		exit();				
		}	
		}


		if ($_REQUEST["acc"] == "Imagenologia") {
		$IdCuentaAtencion = $_REQUEST["IdCuentaAtencion"];
		$respuesta = Imagenologia_por_Cuenta_M($IdCuentaAtencion);
		if (!$respuesta) {
			echo "R_1";
		}
		else
		{	
				for ($i=0; $i < count($respuesta); $i++) { 
				$data_general[$i] = array(
				'codigo' 					=> $respuesta[$i]["codigo"],
				'Nombre' 					=> $respuesta[$i]["Nombre"],
				'ResultInformeFormato_RIS' 	=> $respuesta[$i]["ResultInformeFormato_RIS"],
				'ResultInformeImagen_PACS' 	=> $respuesta[$i]["ResultInformeImagen_PACS"],
				'idcuentaatencion' 			=> $respuesta[$i]["idcuentaatencion"]	
				);

		}
		echo json_encode($data_general);
		exit();				
		}	
		}


		if ($_REQUEST["acc"] == "Lista_Camas_Pacientes_Emergencia") {
		$IdServicio =  $_REQUEST["IdServicio"];
		$data = MostrarListadoCamasEmergencia($IdServicio);
		for ($i=0; $i < count($data); $i++) {
				 $Lista[$i] = array(
				'IdPaciente'			=> $data[$i]['IdPaciente'],
				'IdCama'				=> $data[$i]['IdCama'],
				'ApellidoPaterno'		=> $data[$i]['ApellidoPaterno'],
				'ApellidoMaterno'		=> $data[$i]['ApellidoMaterno'],
				'PrimerNombre'	    	=> $data[$i]['PrimerNombre'],
				'SegundoNombre'			=> $data[$i]['SegundoNombre'],
				'HistoriaClinica'		=> $data[$i]['NroHistoriaClinica'],
				'IdAtencion'			=> $data[$i]['IdAtencion'],
				'Edad'					=> $data[$i]['Edad'],
				'Codigo'				=> $data[$i]['Codigo'],
				'FechaOcupacion'		=> substr($data[$i]['FechaOcupacion'],0,10),
				'Estancia'				=> $data[$i]['Estancia'],
				'Nombre'				=> $data[$i]['Nombre'],
				'IdCuentaAtencion'		=> $data[$i]['IdCuentaAtencion']
				);
		}
		echo json_encode($Lista);
		exit();
		}
		
		
		
		if ($_REQUEST["acc"] == "Lista_Servicios_por_Cama_Paciente_Emergencia") {
		$IdAtencion =  $_REQUEST["IdAtencion"];
		$data = MostrarListadoServiciosporPacienteEmergencia($IdAtencion);
		for ($i=0; $i < count($data); $i++) {
				 $Lista[$i] = array(
				'Secuencia'		=> $data[$i]['Secuencia'],
				'FechaOcupacion'		=> $data[$i]['FechaOcupacion'],
				'HoraOcupacion'	    	=> $data[$i]['HoraOcupacion'],
				'FechaDesocupacion'	    => $data[$i]['FechaDesocupacion'],
				'Codigo'	    		=> $data[$i]['Codigo'],
				'Estancia'	    		=> $data[$i]['Estancia'],
				'Nombre'	    		=> $data[$i]['Nombre'],
				'IdCuentaAtencion'	    => $data[$i]['IdCuentaAtencion']
				);
		}
		echo json_encode($Lista);
		exit();
		}


		if($_REQUEST["acc"] == "RecetasE") 
		{  
			//include('../../MVC_Vista/Emergencia/LoginEmergencia.php');
		}

		if($_REQUEST["acc"] == "AdmisionConsultorioEmerg") // MOSTRAR: Admision de Consultorio de Emergencia
		{  
		$ServiciosEmergencia=ServiciosEmergencia_C();
		include('../../MVC_Vista/Emergencia/EmergenciaV.php');
		}
		
		if($_REQUEST["acc"] == "ReporteEmergencia") // MOSTRAR: Admision de Consultorio de Emergencia
		{  
		$ServiciosEmergencia=ServiciosEmergencia_C();
		include('../../MVC_Vista/Emergencia/Reportes_Emergencia.php');
		}
		
		if($_REQUEST["acc"] == "Aperturar_Cuentas_Emergencia")
		{  
			$Fecha_Apertura = sqlfecha_devolver($_REQUEST["Fecha_Apertura"]);
			Apertura_Cuenta_Emergencia($Fecha_Apertura);
		}
		
		if($_REQUEST["acc"] == "Buscar_Emergencia_Pacientes")
		{    
		     /* Mostrar todos los pacientes de Emergencia */
			 $HistoriaClinica=$_REQUEST["HistoriaClinica"];
			 $IdCuenta=$_REQUEST["IdCuenta"];
			 $IdServicio=$_REQUEST["IdServicio"];
			 $Dni=$_REQUEST["Dni"];
			 $ApellidoPaterno=$_REQUEST["ApellidoPaterno"];

			 $variable='';
			 if(!empty($HistoriaClinica))
				{
						$variable=$HistoriaClinica;
						$busqueda='2';
				}
			if(!empty($IdCuenta))
				{
						$variable=$IdCuenta;
						$busqueda='1';
				}
			if(!empty($Dni))
				{
						$variable=$Dni;
						$busqueda='3';
				}
			if(empty($IdCuenta) and  empty($HistoriaClinica) and  empty($Dni))
				{
						$busqueda='4';
			 	}
				
			 if(!empty($ApellidoPaterno))
				{
						$variable=$ApellidoPaterno;
						$busqueda='5';
				}
				
			 $FechaInicio=sqlfecha($_REQUEST["FechaInicio"]);
			 $FechaFin=sqlfecha($_REQUEST["FechaFin"]);
			 if($IdServicio=='0')
				 {
				   $ListarPacientesEmergencia= Mostrar_Pacientes_Emergencia_Todos_Servicios_C($variable,$FechaInicio,$FechaFin,$busqueda);
				 }
			 else
				 {
				   $ListarPacientesEmergencia= Mostrar_Pacientes_Emergencia_C($variable,$FechaInicio,$FechaFin,$IdServicio,$busqueda);		
				 }
			 include('../../MVC_Vista/Emergencia/Lista_emergencia_pacientes.php');
		}	

		///////////////INICIO DAR ALTA MEDICA AGREGADO EL 02-06-2018

		/*if($_REQUEST["acc"] == "Buscar_Emergencia_Pacientes_SinAlta"){ //POR MAHALI  		    					
			 $FechaInicio=sqlfecha($_REQUEST["FechaInicio"]);
			 $FechaFin=sqlfecha($_REQUEST["FechaFin"]);
			 $IdServicio=$_REQUEST["IdServicio"];			
			 
			 $ListarPacientesEmergencia=SIGESA_Emergencia_ListarPacientesSinAlta_M($FechaInicio,$FechaFin,$IdServicio);
			 include('../../MVC_Vista/Emergencia/Lista_emergencia_pacientes_sinalta.php');
		}*/

		if($_REQUEST["acc"] == "Buscar_Establecimientos"){ //POR MAHALI 
		
		$lcFiltro=' order by Establecimientos.Nombre';				
	    $EstablecimientosFiltrar=EstablecimientosFiltrar_M($lcFiltro);
		
			 if($EstablecimientosFiltrar!=NULL)	{			
				foreach ($EstablecimientosFiltrar as $item){				
					$codigo= $item["codigo"];
					$Nombre= mb_strtoupper($item["Nombre"]);
					$Ubicacion='Ubicación: '.$item["Distrito"].' '.$item["Provincia"].' '.$item["Departamento"];					

					$fintrado=$codigo.' '.$Nombre.' '.$Ubicacion;
					$IdEstablecimiento=$item["IdEstablecimiento"];

					echo "$fintrado|$Nombre|$IdEstablecimiento|$codigo\n";
				}
			 }	
			
		}

		if($_REQUEST["acc"] == "Listado_Establecimientos"){ //POR MAHALI 
		
		$nombres = utf8_decode($_REQUEST["q"]);		
		$lcFiltro="WHERE Establecimientos.Nombre LIKE '" . $nombres . "%' ";
		$lcFiltro.=' order by Establecimientos.Nombre';				
	    $EstablecimientosFiltrar=EstablecimientosFiltrar_M($lcFiltro);
		
			 if($EstablecimientosFiltrar!=NULL)	{	
				echo "<table id='tableEstablecimiento' border='1' cellpadding='0' cellspacing='0'><tr><th><b>Codigo</b></th><th><b>Nombre</b></th><th><b>Distrito</b></th></tr>";
				 $registros='';
				foreach ($EstablecimientosFiltrar as $item){			
					//$contenido .= "<td>&nbsp;".strstr($data[$i]['TipoGravedad'], '(', true)."&nbsp;</td>";
					$IdEstablecimiento=$item["IdEstablecimiento"];
					$Nombre=$item["Nombre"];
					$codigo=$item["codigo"];					
					//$registros.= "<tr id='".$item["IdEstablecimiento"]."' onClick='mostrarPersona(".$IdEstablecimiento.")'; >";
					$registros.= "<tr id='".$item["IdEstablecimiento"]."' onClick='LlenarDatosEstablecimiento($IdEstablecimiento,\"$Nombre\",\"$codigo\");' >";
					
	   $registros.= "<td>".$item["codigo"]."</td>";
			   //LO SGTE PARA OBTENER LA PORSION DE TEXTO QUE COINCIDE Y CAMBIARLE DE ESTILO, $cadena2 -> está variable contiene el valor q coincide, al cual lo ubico en una etiqueta span para cambiarle de estilo.
				$posicion  = stripos($item["Nombre"], $nombres);
				if($posicion>-1){
					$cadena1 = substr($item["Nombre"], 0, $posicion);
					$cadena2 = substr($item["Nombre"], $posicion, strlen($nombres));
					$cadena3 = substr($item["Nombre"], ($posicion + strlen($nombres)));
					
					$dato = $cadena1.'<span>'.$cadena2.'</span>'.$cadena3;
					$registros.= "<td>".$dato."</td>";
				}else{
					$registros.= "<td>".$item["Nombre"]."</td>";
					}					
					$registros.= "<td>".utf8_decode($item["Distrito"])."</td>";
	   $registros.= "</tr>";
				}
				echo utf8_encode($registros);
				echo "</table>"; 
			 }				
		}

if($_REQUEST["acc"] == "Listado_EpisodioClinico"){ //POR MAHALI 
		
		$IdPaciente = $_REQUEST["IdPaciente"];		
	    $EpisodioClinicoFiltrar=AtencionesEpisodiosDetalleSeleccionarXpaciente_M($IdPaciente);
		
		//$tablaEpisodioClinico='';
			 if($EpisodioClinicoFiltrar!=NULL)	{		 
				   		 
				
				echo "<table id='tablaEpisodioClinico' border='1' cellpadding='0' cellspacing='0'><tr bgcolor='#00FFFF'><th><b>Id</b></th><th><b>F.Apertura</b></th><th><b>F.Cierre</b></th><th><b>Diágnostico</b></th><th><b>T.Servicio</b></th><th><b>Servicio/Consultorio</b></th><th><b>F.Cuenta</b></th><th><b>N°Cuenta</b></th></tr>";
				 $registros='';
				foreach ($EpisodioClinicoFiltrar as $item){	
					
					$NroEpisodio=$item["idEpisodio"];
					if($item["FechaApertura"]!=""){
						$FechaApertura=vfecha(substr($item["FechaApertura"],0,10));
					}else{
						$FechaApertura="";
					}
					
					if($item["FechaCierre"]!=""){
						$FechaCierre=vfecha(substr($item["FechaCierre"],0,10));	
					}else{
						$FechaCierre="";
					}
					
					//Diagnostico mo_AdminAdmision1.AtencionesDiagnosticosSeleccionarPorAtencion(lnIdAtencion, IIf(lnIdTipoServicio = 1, 1, 3))
					if($item["IdTipoServicio"]=="1"){
						$TipoDiagnostico="1"; //1	Atencion CE o Consultorio de Emergencia
						$IdTipoDiagnostico="102";	//102	D	Definitivo	1	1
					}else{
						$TipoDiagnostico="3"; //3	Egreso hospitalizacion u Obs. emergencia
						$IdTipoDiagnostico="301";	//301	P	Principal	3	3
					}	

					$AtencionesDiagnosticos=AtencionesDiagnosticosSeleccionarPorAtencion_M($item["IdAtencion"],$TipoDiagnostico);			
					if($AtencionesDiagnosticos!=NULL){			
						foreach ($AtencionesDiagnosticos as $itemDx){
							if($itemDx["IdTipoDiagnostico"]==$IdTipoDiagnostico){
								$dx=trim(utf8_decode($itemDx["CodigoCIE2004"]." ".$itemDx["Descripcion"])); 
							}else{
								$dx="";
							}
						}
					}else{
						$dx="";
					}

					//SI NO TIENE DIAGNOSTICO DE EGRESO, PERO TIENE DE FALLECIDO
					if($dx!=""){
						$dx=$dx;	
					}else if($item["IdTipoServicio"] != 1){
						$DiagnosticosFallecidos=AtencionesDiagnosticosSeleccionarPorAtencion_M($item["IdAtencion"],4);
						if($DiagnosticosFallecidos!=NULL){			
							foreach ($DiagnosticosFallecidos as $itemFall){
								if($itemFall["IdTipoDiagnostico"]=="303"){
									$dx=trim(utf8_decode($itemFall["CodigoCIE2004"]." ".$itemFall["Descripcion"])); 
								}else{
									$dx="";
								}
							}
						}
					}
																														 
					$TipoServicio=$item["TipoServicio"];																							 
					
					if($item["IdTipoServicio"]=='1'){
						$Servicio=$item["ServIng"];	
					}else{
						$Servicio=$item["Servicio"];	
					}						
					
					$CuentaFecha=vfecha(substr($item["FechaIngreso"],0,10));	
					$CuentaNro=$item["IdCuentaAtencion"];			
					
					$registros.= "<tr id='".$NroEpisodio."' onClick='LlenarDatosEpisodioidEpisodio($NroEpisodio,\"$dx\",\"$FechaCierre\");' >";			
	   				$registros.= "<td>".$NroEpisodio."</td> <td>".$FechaApertura."</td> <td>".$FechaCierre."</td><td>".$dx."</td>";	
					$registros.= "<td>".utf8_decode($TipoServicio)."</td>";
					$registros.= "<td>".utf8_decode($Servicio)."</td>";
					$registros.= "<td>".$CuentaFecha."</td>";
					$registros.= "<td>".$CuentaNro."</td>";
					
	   $registros.= "</tr>";
				}
				echo utf8_encode($registros);
				echo "</table>"; 
				
			 }	
			 
		}
		

		if($_REQUEST["acc"] == "Buscar_Medicos"){ //POR MAHALI 
			
			$IdEspecialidad=$_REQUEST["IdEspecialidad"];
				
			$variable=strtoupper($_REQUEST["q"]).'%';	    
			$lcFiltro= ' where Especialidades.IdEspecialidad='.$_REQUEST["IdEspecialidad"].' and ApellidoPaterno like '."'".$variable."'";			
			$lcFiltro.=' order by ApellidoPaterno';	
				
		    $MedicosFiltrar=MedicosFiltrar_M($lcFiltro);
		
			if($MedicosFiltrar!=NULL){
				foreach ($MedicosFiltrar as $item)
				{				
					$ApellidoPaterno= $item["ApellidoPaterno"];
					$ApellidoMaterno= $item["ApellidoMaterno"];
					$Nombres= $item["Nombres"] ;				
					$Trabajador=mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"]);	
					$CodigoPlanilla= $item["CodigoPlanilla"] ;			
					
					$fintrado=$Trabajador;
					$IdMedico=$item["IdMedico"];
					
				    echo "$fintrado|$Trabajador|$IdMedico|$CodigoPlanilla\n";
			   
				}
			}			
		}
		
		if($_REQUEST["acc"] == "Ingreso_Emergencia_Alta"){ //POR MAHALI       

		     $IdAtencion=$_REQUEST["IdAtencion"];
			 $IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];
			 $ApellidoPaterno=$_REQUEST["ApellidoPaterno"];
			 $ApellidoMaterno=$_REQUEST["ApellidoMaterno"];
			 $PrimerNombre=$_REQUEST["PrimerNombre"];
			 $NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
			 $FechaNacimiento=$_REQUEST["FechaNacimiento"];

			 $FechaIngreso=$_REQUEST["FechaIngreso"];
			 $HoraIngreso=$_REQUEST["HoraIngreso"];
			 
			 $Consultorio=$_REQUEST["Consultorio"];
			 $FuenteFinanciamiento=$_REQUEST["FuenteFinanciamiento"];
			 $Sexo=$_REQUEST["Sexo"];
			 $IdServicio=$_REQUEST["IdServicio"];
			 $TipoServicio=$_REQUEST["TipoServicio"];
			 $TipoOrigen=$_REQUEST["TipoOrigen"];
			 $TipoGravedad=$_REQUEST["TipoGravedad"];
			 $FechaEgresoAdministrativo=$_REQUEST["FechaEgresoAdministrativo"];	
			 $IdServicioEgreso=$_REQUEST["IdServicioEgreso"];			
			 
			 //$ObtenerCodigoCama=ObtenerCodigoCama_M($IdAtencion);			 
			 $CodigoCamaEgreso=$_REQUEST["cama"];				 
			 
			 $ObtenerDatosServicio=ObtenerDatosServicio_M($IdServicioEgreso);
			 $CodigoServicioEgreso=$ObtenerDatosServicio[0]['Codigo'];
			 $NombreServicioEgreso=$ObtenerDatosServicio[0]['Nombre'];
			 $IdEspecialidadServicioEgreso=$ObtenerDatosServicio[0]['IdEspecialidad'];		 
			
			 $IdPaciente=$_REQUEST["IdPaciente"]; 
			 $fechaEgresoBD =isset($_POST['FechaEgreso']) ? $_POST['FechaEgreso'] : '';
			 
			 $wxParametro324=ParametrosSeleccionarPorId_M(324); //N° horas máximas que un paciente puede estar en Emergencia
			 $HoraMaxEmerg=$wxParametro324[0]["ValorTexto"];

			 $ListarDatosAtencion=ListarDatosAtencion_M($IdAtencion);

			 $IdDestinoAtencion=$ListarDatosAtencion[0]["IdDestinoAtencion"];
			 $IdTipoAlta=$ListarDatosAtencion[0]["IdTipoAlta"];
			 $IdCondicionAlta=$ListarDatosAtencion[0]["IdCondicionAlta"];

			 include('../../MVC_Vista/Emergencia/EmergenciaDarAlta.php');			 
		}

		if($_REQUEST["acc"] == "VerSolicAntim"){ //POR MAHALI	    
			
			 $NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"]; 
			 include('../../MVC_Vista/Emergencia/Ver_SolicAntim.php');			 
		}

if ($_REQUEST["acc"] == "ExamenesAntimicrobianos") {
	 $IdCuentaAtencion = $_REQUEST["IdCuentaAtencion"];
	 $FactOrdenServicio=SigesaFactOrdenServicio($IdCuentaAtencion);
	 $ExamenesAntimicrobianos=Mostrar_Lista_Antimicrobianos_Examenes_IdProducto();
	 $array = array();
	 for ($x=0; $x < count($ExamenesAntimicrobianos); $x++) 
	 {
	 $array[] = $ExamenesAntimicrobianos[$x]["IdProducto"];
	 }
 	 $c = 0;
	 for ($h=0; $h < count($FactOrdenServicio); $h++) 
	 {
		$idOrden = $FactOrdenServicio[$h]["IdOrden"];
		$FechaDespacho = $FactOrdenServicio[$h]["FechaDespacho"];
		$ordenes_general = SigesaLaboratorioMostrarOrdenDetalladaxIdOrden_M($idOrden);
		$ordenes_resultado = SigesaLaboratorioMMostrarOrdenConResultado_M($idOrden);
		for ($i=0; $i < count($ordenes_general); $i++){
			if (in_array($ordenes_general[$i]["idProductoCPT"], $array)) 
			{
			$tipo='1';
			}
			else
			{
			$tipo='0';				
			}	
			$resultado = 'NO';
			
			for ($j=0; $j < count($ordenes_resultado); $j++) { 				
				if ($ordenes_general[$i]["idProductoCPT"] == $ordenes_resultado[$j]["idProductoCpt"]) {
					$resultado = 'SI';
				}				
			}

			$indice = $i + 1;
			$resultado_json[] = array(
				'POSICION'	=>	$c,
				'NUMERO'	=>	$indice,
				'ORDEN'	=>	$FactOrdenServicio[$h]["IdOrden"],
				'ITEMS'	=>	count($ordenes_general),
				'FECHADESPACHO'	=>	vfechahora($FactOrdenServicio[$h]["FechaDespacho"]),
				'CODIGO'	=>	$ordenes_general[$i]["Codigo"],
				'NOMBRE'	=>	$ordenes_general[$i]["Nombre"],
				'CANTIDAD'	=>	$ordenes_general[$i]["cantidad"],
				'PRECIO'	=>	$ordenes_general[$i]["precio"],
				'TOTAL'		=>	$ordenes_general[$i]["importe"],
				'IDPRODCPT' => 	$ordenes_general[$i]["idProductoCPT"],
				'PUNTOCARGA' => 	$ordenes_general[$i]["Descripcion"],
				'RESULTAD'	=>	$resultado,
				'ORDEN'		=>  $idOrden,
				'TIPO'		=> $tipo,
				'IdCuentaAtencion' => $FactOrdenServicio[$h]["IdCuentaAtencion"],
				);
			$c = $c + 1;			
			
		}	
	}
echo json_encode($resultado_json);
exit();
		 
}


if ($_REQUEST["acc"] == "datosCabecerasDetalle") {
	$idOrden = $_REQUEST["idorden"];
	$idProductoCpt = $_REQUEST["idproductocpt"];

	$data = SigesaLaboratorioMostrarDetalleOrdenResultadoParaImprimir_M($idOrden,$idProductoCpt);

	$realiza_prueba = $data[0]["REALIZAP"];
	$fecha_prueba   = $data[0]["Fecha"];

	echo json_encode(array('REALIZAP' => $realiza_prueba, 'FECHA' => $fecha_prueba));
	exit();
}


		if($_REQUEST["acc"] == "DarAlta_Emergencia"){ //POR MAHALI   
			
			//Obtener Datos		
			
			$IdDestinoAtencion = $_REQUEST["IdDestinoAtencion"];
			$FechaEgreso = $_REQUEST["FechaEgreso"];
			$HoraEgreso = $_REQUEST["HoraEgreso"];
			$IdUsuarioAuditoria = $_REQUEST["IdEmpleado"];

			//$FechaEgresoAdministrativo = $_REQUEST["FechaEgresoAdministrativo"];
			//$HoraEgresoAdministrativo = $_REQUEST["HoraEgresoAdministrativo"];
			$IdCamaEgreso = $_REQUEST["IdCamaEgreso"];
			$IdCondicionAlta = $_REQUEST["IdCondicionAlta"];

			$IdServicioEgreso = $_REQUEST["IdServicioEgreso"];

			$IdTipoAlta = $_REQUEST["IdTipoAlta"];
			$IdMedicoEgreso = $_REQUEST["IdMedicoEgreso"];

			//$IdMedicoRespNacimiento = $_REQUEST["IdMedicoRespNacimiento"];	
			$IdTipoReferenciaDestino =isset($_REQUEST['TipoReferencia']) ? $_REQUEST['TipoReferencia'] : '';

			if($IdTipoReferenciaDestino == 1){
				$idEstablecimientoDestino = $_REQUEST["IdEstablecimientoDestino"]; 
				$IdEstablecimientoNoMinsaDestino = 0;
			}else{
				$idEstablecimientoDestino = 0; 
				$IdEstablecimientoNoMinsaDestino = $_REQUEST["IdEstablecimientoDestino"]; 
			}
			
			$NroReferenciaDestino = $_REQUEST["NroReferencia"]; 	
			
			$IdTipoServicio=$_REQUEST["IdTipoServicio"];	
			$IdPaciente=$_REQUEST["IdPaciente"];
			$IdAtencion=$_REQUEST["IdAtencion"];		
			$IdEspecialidadMedico=$_REQUEST["IdEspecialidadServicioEgreso"];//
			$IdEmpleado=$_REQUEST["IdEmpleado"];	
			
			$idEstadoAtencion='1';
			$IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];		
			
			//INICIO METODOS DE SISGALENPLUS			
				//CalculaEstanciaParaPacienteConAltaMedica///////////		
				//ModificarDatos = mo_AdminAdmision.AdmisionHospModEGRESO			
				include('ModificarAtenciones.php');	

				//mo_ReglasFacturacion.FacturacionCuentasAtencionPtosActualizar mo_Atenciones.idCuentaAtencion, False, 0	           
	            include('FacturacionCuentasAtencionPtosActualizar.php'); 

            //FIN METODOS DE SISGALENPLUS
             
			$SeleccionaFilaParametro245=ParametrosSeleccionarPorId_M(302); 
			$wxParametro302 = $SeleccionaFilaParametro245[0]["ValorTexto"]; //S

			$ListarDatosAtencionX=ListarDatosAtencion_M($IdAtencion);
			$idFuenteFinanciamiento=$ListarDatosAtencionX[0]["idFuenteFinanciamiento"];

			if($FechaEgreso!=''){				 				
				if ($wxParametro302 == "S" And $idFuenteFinanciamiento==3){
				   //mo_ReglasSISgalenhos.SisFuaAtencionActualizaDatosDesdeHospEmegCE mo_Atenciones.idCuentaAtencion, _																		  mo_Atenciones.idTipoServicio, mo_Atenciones.IdAtencion, _																		  mo_lnIdTablaLISTBARITEMS, ml_IdUsuario

				}   
				$mensaje="Se registró el alta del paciente satisfactoriamente";
				print "<script>alert('$mensaje')</script>";	
				
			}else{
				$mensaje="Los datos se modificaron correctamente, para la Cuenta N° ".$IdCuentaAtencion;
				print "<script>alert('$mensaje')</script>";	
			}			
			
			
		}		


		/////////////FIN ALTA MEDICA
		

		if ($_REQUEST["acc"] == "Lista_Pacientes_Emergencia") {
			
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$Servicio=$_REQUEST["Servicio"];
		$Tipo=$_REQUEST["Tipo"];
		$Turno=$_REQUEST["Turno"];
		if($Turno=='1')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		if($Turno=='2')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_Diurno_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		if($Turno=='3')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_Nocturno_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}

		for ($i=0; $i < count($data); $i++) {
				 $Lista[$i] = array(
				'IdCuentaAtencion'		=> $data[$i]['IdCuentaAtencion'],
				'ApellidoMaterno'		=> $data[$i]['ApellidoMaterno'],
				'ApellidoPaterno'		=> $data[$i]['ApellidoPaterno'],
				'PrimerNombre'			=> $data[$i]['PrimerNombre'],
				'Nombre'				=> $data[$i]['Nombre'],
				'FechaTurno2'			=> $data[$i]['FechaIngreso2'],
				'HoraIngreso'			=> $data[$i]['HoraIngreso'],
				'NroHistoriaClinica'	=> $data[$i]['NroHistoriaClinica'],
				'TipoGravedad'			=> strstr($data[$i]['TipoGravedad'],'(', true) ,
				'Turno'					=> $data[$i]['Turno'],
				'FechaTurno'					=> $data[$i]['FechaTurno2'],
				'FechaIngreso'					=> $data[$i]['FechaIngreso2'],
				'NombreDiagnos'					=> $data[$i]['NombeDiagnos']

				);
		}
		echo json_encode($Lista);
		exit();
		}

		
		
		
		if($_REQUEST["acc"] == "Imprimir_Lista_Pacientes_Emergencia") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$Servicio=$_REQUEST["Servicio"];
		$Tipo=$_REQUEST["Tipo"];
		$Turno=$_REQUEST["Turno"];
		if($Turno=='1')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		if($Turno=='2')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_Diurno_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		if($Turno=='3')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_Nocturno_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		
		if($Tipo=='3' or $Tipo=='4')
		{
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<title>Reporte de Emergencia</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "@media all {
			   div.saltopagina{
				  display: none;
				background:black   !important;
			   }
			}
			   
			@media print{
			   div.saltopagina{ 
				  display:block; 
				  page-break-before:always;
			   }
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<body>";
			$contenido .= "<font size='4'>";
			$contenido .= "<table width='100%' > ";
			$contenido .= "<tr>";
			$contenido .= "<td><label>HOSPITAL NACIONAL DANIEL ALCIDES CARRION</label></td>";
			$contenido .= "<td><label>" . date("d/m/Y") . "</label></td>";
			$contenido .= "<td>&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$IdServicio=$data[$i]['IdServicio'];
			$salto=0;
			$cont=0;
			for ($i=0; $i < count($data); $i++) {
			if($IdServicio != $data[$i]['IdServicio'])
			{
			$salto=$salto+1;
			if($salto>1)
			{
			$contenido .= "<tr>";
			$contenido .= "<td style='text-align:left' colspan='3'><strong>Total de Pacientes : ".$count."</strong></td>";			
			$contenido .= "</tr>";
			}
			$count=0;
			$contenido .= "<table  width='100%' border='0'>";
			$contenido .= "<tr width='900px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='900px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='900px'>";
			$contenido .= "<td colspan='12'  style='text-align:center;font-size:80px !important'><H1><STRONG>PACIENTES DE EMERGENCIA POR SERVICIOS</STRONG></H1></td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='900px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='900px' style='text-transform: uppercase;'>";	
			$contenido .= "<td colspan='12'  style='text-align:center;'><strong>".$data[$i]['Nombre']."</strong></td>";
			$contenido .= "</tr>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";			
			$contenido .= "<tr style='text-transform: uppercase;'>";	
			$contenido .= "<td><strong>Id</strong></td>";
			$contenido .= "<td><strong>Cuenta</strong></td>";
			$contenido .= "<td><strong>Nro. HC</strong></td>";
			$contenido .= "<td><strong>DNI</strong></td>";
			$contenido .= "<td><strong>Apellidos y Nombres</strong></td>";
			$contenido .= "<td><strong>Prioridad</strong></td>";
			$contenido .= "<td><strong>Turno</strong></td>";
			$contenido .= "<td><strong>F. Turno</strong></td>";
			$contenido .= "<td><strong>F. Ingreso - Hora</strong></td>";
			$contenido .= "<td><strong>Diag. Ingreso</strong></td>";
			$contenido .= "<td><strong>Verifica</strong></td>";
			$contenido .= "<td><strong>Observacion</strong></td>";
			$contenido .= "</tr>";
			$IdServicio=$data[$i]['IdServicio'];
			}		
			$count=$count+1;
			$contenido .= "<tr>";
			$contenido .= "<td><strong>".$count.".-</strong></td>";		
			$contenido .= "<td>&nbsp;".$data[$i]['IdCuentaAtencion']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroHistoriaClinica']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroDocumento']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['ApellidoPaterno']." ".$data[$i]['ApellidoMaterno']." ".$data[$i]['PrimerNombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['TipoGravedad']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['Turno']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaTurno2']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaIngreso2'].' '.$data[$i]['HoraIngreso']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NombeDiagnos']."&nbsp;</td>";
			$contenido .= "<td text-align='center'><div style='width:18px;heigth:8px;border: 2px solid #483E3C;'>&nbsp;</div></td>";
			$contenido .= "</tr>";
			if($salto>1)
			{
			$contenido .= "<div class='saltopagina'></div>";
			}
			
			}
			$contenido .= "</table>"; 
			$contenido .= "<tr>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";

		}
		
		
		
		// if($Tipo=='3' or $Tipo=='4')
		// {
			
			// $contenido  = "<html>";
			// $contenido .= "<head>";
			// $contenido .= "<title>Reporte de Emergencia</title>";
			// $contenido .= "<style>";
			// $contenido .= "*{";
			// $contenido .= "font-family: 'CALIBRI';";
			// $contenido .= "font-size: 12px;";
			// $contenido .= "margin:0;";
			// $contenido .= "padding: 0;";
			// $contenido .= "}";
			// $contenido .= "@media print{
				// font-family: 'CALIBRI';
				// font-size: 12px;
				// margin:0;
				// padding: 0;
			// }";
			// $contenido .= "</style>";
			// $contenido .= "</head>";
			// $contenido .= "<body>";
			// $contenido .= "<font size='4'>";
			// $contenido .= "<table width='100%' > ";
			// $contenido .= "<tr>";
			// $contenido .= "<td><label>HOSPITAL NACIONAL DANIEL ALCIDES CARRION</label></td>";
			// $contenido .= "<td><label>" . date("d/m/Y") . "</label></td>";
			// $contenido .= "<td>&nbsp;</td>";
			// $contenido .= "</tr>";
			// $contenido .= "<tr>";
			// $contenido .= "</tr>";
			// $contenido .= "</table>";
			// $contenido .= "<table width='100%' border='0'>";
			// $contenido .= "<tr width='500px'>";
			// $contenido .= "<td colspan='8'  style='text-align:center;font-size:50px !important'><H1><STRONG>PACIENTES DE EMERGENCIA POR SERVICIOS</STRONG></H1></td>";
			// $contenido .= "</tr>";
			// $contenido .= "<tr width='500px'>";
			// $contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			// $contenido .= "</tr>";
			// $IdServicio=$data[$i]['IdServicio'];
			// $salto=0;
			// for ($i=0; $i < count($data); $i++) {
			// if($IdServicio != $data[$i]['IdServicio'])
			// {
			// $salto=$salto+1;
			// if($Servicio=='' and  $salto>1)
			// {
			// $contenido .= "<tr width='500px'>";
			// $contenido .= "<td style='text-align:left' colspan='3'><strong>Total de Pacientes : ".$count."</strong></td>";			
			// $contenido .= "</tr>";
			// }
			// $count=0;
			// $contenido .= "<tr width='500px'>";
			// $contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			// $contenido .= "</tr>";
			// $contenido .= "<tr width='500px'>";
			// $contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			// $contenido .= "</tr>";
			// $contenido .= "<tr style='text-transform: uppercase;'>";	
			// $contenido .= "<td colspan='8'><strong>".$data[$i]['Nombre']."</strong></td>";
			// $contenido .= "</tr>";	
			// $contenido .= "<tr style='text-transform: uppercase;'>";	
			// $contenido .= "<td><strong>Id</strong></td>";
			// $contenido .= "<td><strong>Cuenta</strong></td>";
			// $contenido .= "<td><strong>Nro. HC</strong></td>";
			// $contenido .= "<td><strong>DNI</strong></td>";
			// $contenido .= "<td><strong>Apellidos y Nombres</strong></td>";
			// $contenido .= "<td><strong>Prioridad</strong></td>";
			// $contenido .= "<td><strong>Turno</strong></td>";
			// $contenido .= "<td><strong>F. Turno</strong></td>";
			// $contenido .= "<td><strong>F. Ingreso - Hora</strong></td>";
			// $contenido .= "<td><strong>Diag. Ingreso</strong></td>";
			// $contenido .= "<td><strong>Verifica</strong></td>";
			// $contenido .= "<td><strong>Observacion</strong></td>";
			// $contenido .= "</tr>";
			// $IdServicio=$data[$i]['IdServicio'];
			// }		
			// $count=$count+1;
			// $contenido .= "<tr>";
			// $contenido .= "<td><strong>".$count.".-</strong></td>";		
			// $contenido .= "<td>&nbsp;".$data[$i]['IdCuentaAtencion']."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".$data[$i]['NroHistoriaClinica']."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".$data[$i]['NroDocumento']."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".$data[$i]['ApellidoPaterno']." ".$data[$i]['ApellidoMaterno']." ".$data[$i]['PrimerNombre']."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".strstr($data[$i]['TipoGravedad'], '(', true)."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".$data[$i]['Turno']."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".$data[$i]['FechaTurno2']."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".$data[$i]['FechaIngreso2'].' '.$data[$i]['HoraIngreso']."&nbsp;</td>";
			// $contenido .= "<td>&nbsp;".$data[$i]['NombeDiagnos']."&nbsp;</td>";
			// $contenido .= "<td text-align='center'><div style='width:18px;heigth:8px;border: 2px solid #483E3C;'>&nbsp;</div></td>";
			// $contenido .= "</tr>";	
			// }
			// $contenido .= "</table>"; 
			// $contenido .= "<tr>";
			// $contenido .= "</tr>";
			// $contenido .= "</table>";
			// $contenido .= "</font>";
			// $contenido .= "</body>";
			// $contenido .= "</html> ";
			
			
			
		// }
		
		
		if($Tipo=='1' or $Tipo=='2')
		{
			
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<title>Reporte de Emergencia</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<body>";
			$contenido .= "<font size='4'>";
			$contenido .= "<table width='100%' > ";
			$contenido .= "<tr>";
			$contenido .= "<td><label>HOSPITAL NACIONAL DANIEL ALCIDES CARRION</label></td>";
			$contenido .= "<td><label>" . date("d/m/Y") . "</label></td>";
			$contenido .= "<td>&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "<table width='100%' border='0'>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td colspan='8'  style='text-align:center;font-size:50px !important'><H1><STRONG>PACIENTES DE EMERGENCIA POR SERVICIOS</STRONG></H1></td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr style='text-transform: uppercase;'>";	
			$contenido .= "<td><strong>Cuenta</strong></td>";
			$contenido .= "<td><strong>Nro. HC</strong></td>";
			$contenido .= "<td><strong>DNI</strong></td>";
			$contenido .= "<td><strong>Apellidos y Nombres</strong></td>";
			$contenido .= "<td><strong>Servicio</strong></td>";
			$contenido .= "<td><strong>Prioridad</strong></td>";
			$contenido .= "<td><strong>Turno</strong></td>";
			$contenido .= "<td><strong>F. Turno</strong></td>";
			$contenido .= "<td><strong>F. Ingreso - Hora</strong></td>";
			$contenido .= "<td><strong>Diag. Ingreso</strong></td>";
			$contenido .= "<td><strong>Verifica</strong></td>";
			$contenido .= "<td><strong>Observacion</strong></td>";
			$contenido .= "</tr>";
			for ($i=0; $i < count($data); $i++) {			
			$contenido .= "<tr>";	
			$contenido .= "<td>&nbsp;".$data[$i]['IdCuentaAtencion']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroHistoriaClinica']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroDocumento']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['ApellidoPaterno']." ".$data[$i]['ApellidoMaterno']." ".$data[$i]['PrimerNombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['Nombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".strstr($data[$i]['TipoGravedad'], '(', true)."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['Turno']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaTurno2']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaIngreso2'].' '.$data[$i]['HoraIngreso']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NombeDiagnos']."&nbsp;</td>";
			$contenido .= "<td text-align='center'><div style='width:18px;heigth:8px;border: 2px solid #483E3C;'>&nbsp;</div></td>";
			$contenido .= "</tr>";	
			}
			$contenido .= "</table>"; 
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";	
			
			
		}
		
		
		echo json_encode($contenido);
		exit();
		
		}
		

		
		if($_REQUEST["acc"] == "Exportar_Reporte_Excel") {
		$Fecha_Inicial=sqlfecha_devolver($_REQUEST["Fecha_Inicial"]);
		$Fecha_Final=sqlfecha_devolver($_REQUEST["Fecha_Final"]);
		$Servicio=$_REQUEST["Servicio"];
		$Tipo=$_REQUEST["Tipo"];
		$Turno=$_REQUEST["Turno"];
		if($Turno=='1')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		if($Turno=='2')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_Diurno_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		if($Turno=='3')
		{
		$data = Reporte_Lista_Pacientes_Emergencia_Nocturno_2_M($Fecha_Inicial,$Fecha_Final,$Servicio,$Tipo);	
		}
		
		if($Tipo=='3' or $Tipo=='4')
		{
			
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<title>Reporte de Emergencia</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<body>";
			$contenido .= "<font size='4'>";
			$contenido .= "<table width='100%' > ";
			$contenido .= "<tr>";
			$contenido .= "<td><label>HOSPITAL NACIONAL DANIEL ALCIDES CARRION</label></td>";
			$contenido .= "<td><label>" . date("d/m/Y") . "</label></td>";
			$contenido .= "<td>&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "<table width='100%' border='0'>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td colspan='8'  style='text-align:center;font-size:50px !important'><H1><STRONG>PACIENTES DE EMERGENCIA POR SERVICIOS</STRONG></H1></td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$IdServicio=$data[$i]['IdServicio'];
			for ($i=0; $i < count($data); $i++) {
			if($IdServicio != $data[$i]['IdServicio'])
			{
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr style='text-transform: uppercase;'>";	
			$contenido .= "<td colspan='8'><strong>".$data[$i]['Nombre']."</strong></td>";
			$contenido .= "</tr>";	
			$contenido .= "<tr style='text-transform: uppercase;'>";	
			$contenido .= "<td><strong>Cuenta</strong></td>";
			$contenido .= "<td><strong>Nro. HC</strong></td>";
			$contenido .= "<td><strong>DNI</strong></td>";
			$contenido .= "<td><strong>Apellidos y Nombres</strong></td>";
			$contenido .= "<td><strong>Prioridad</strong></td>";
			$contenido .= "<td><strong>Turno</strong></td>";
			$contenido .= "<td><strong>F. Turno</strong></td>";
			$contenido .= "<td><strong>F. Ingreso - Hora</strong></td>";
			$contenido .= "<td><strong>Diag. Ingreso</strong></td>";
			$contenido .= "<td><strong>Verifica</strong></td>";
			$contenido .= "<td><strong>Observacion</strong></td>";
			$contenido .= "</tr>";
			$IdServicio=$data[$i]['IdServicio'];
			}			
			$contenido .= "<tr>";	
			$contenido .= "<td>&nbsp;".$data[$i]['IdCuentaAtencion']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroHistoriaClinica']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroDocumento']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['ApellidoPaterno']." ".$data[$i]['ApellidoMaterno']." ".$data[$i]['PrimerNombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".strstr($data[$i]['TipoGravedad'], '(', true)."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['Turno']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaTurno2']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaIngreso2'].' '.$data[$i]['HoraIngreso']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NombeDiagnos']."&nbsp;</td>";
			$contenido .= "<td text-align='center'><div style='width:18px;heigth:8px;border: 2px solid #483E3C;'>&nbsp;</div></td>";
			$contenido .= "</tr>";	
			}
			$contenido .= "</table>"; 
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";
			
			
			
		}
		
		
		if($Tipo=='1' or $Tipo=='2')
		{
			
			$contenido  = "<html>";
			$contenido .= "<head>";
			$contenido .= "<title>Reporte de Emergencia</title>";
			$contenido .= "<style>";
			$contenido .= "*{";
			$contenido .= "font-family: 'CALIBRI';";
			$contenido .= "font-size: 12px;";
			$contenido .= "margin:0;";
			$contenido .= "padding: 0;";
			$contenido .= "}";
			$contenido .= "@media print{
				font-family: 'CALIBRI';
				font-size: 12px;
				margin:0;
				padding: 0;
			}";
			$contenido .= "</style>";
			$contenido .= "</head>";
			$contenido .= "<body>";
			$contenido .= "<font size='4'>";
			$contenido .= "<table width='100%' > ";
			$contenido .= "<tr>";
			$contenido .= "<td><label>HOSPITAL NACIONAL DANIEL ALCIDES CARRION</label></td>";
			$contenido .= "<td><label>" . date("d/m/Y") . "</label></td>";
			$contenido .= "<td>&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "<table width='100%' border='0'>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td colspan='8'  style='text-align:center;font-size:50px !important'><H1><STRONG>PACIENTES DE EMERGENCIA POR SERVICIOS</STRONG></H1></td>";
			$contenido .= "</tr>";
			$contenido .= "<tr width='500px'>";
			$contenido .= "<td style='text-align:center'>&nbsp;&nbsp;</td>";
			$contenido .= "</tr>";
			$contenido .= "<tr style='text-transform: uppercase;'>";	
			$contenido .= "<td><strong>Cuenta</strong></td>";
			$contenido .= "<td><strong>Nro. HC</strong></td>";
			$contenido .= "<td><strong>DNI</strong></td>";
			$contenido .= "<td><strong>Apellidos y Nombres</strong></td>";
			$contenido .= "<td><strong>Servicio</strong></td>";
			$contenido .= "<td><strong>Prioridad</strong></td>";
			$contenido .= "<td><strong>Turno</strong></td>";
			$contenido .= "<td><strong>F. Turno</strong></td>";
			$contenido .= "<td><strong>F. Ingreso - Hora</strong></td>";
			$contenido .= "<td><strong>Diag. Ingreso</strong></td>";
			$contenido .= "<td><strong>Verifica</strong></td>";
			$contenido .= "<td><strong>Observacion</strong></td>";
			$contenido .= "</tr>";
			for ($i=0; $i < count($data); $i++) {			
			$contenido .= "<tr>";	
			$contenido .= "<td>&nbsp;".$data[$i]['IdCuentaAtencion']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroHistoriaClinica']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NroDocumento']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['ApellidoPaterno']." ".$data[$i]['ApellidoMaterno']." ".$data[$i]['PrimerNombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['Nombre']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".strstr($data[$i]['TipoGravedad'], '(', true)."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['Turno']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaTurno2']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['FechaIngreso2'].' '.$data[$i]['HoraIngreso']."&nbsp;</td>";
			$contenido .= "<td>&nbsp;".$data[$i]['NombeDiagnos']."&nbsp;</td>";
			$contenido .= "<td text-align='center'><div style='width:18px;heigth:8px;border: 2px solid #483E3C;'>&nbsp;</div></td>";
			$contenido .= "</tr>";	
			}
			$contenido .= "</table>"; 
			$contenido .= "<tr>";
			$contenido .= "</tr>";
			$contenido .= "</table>";
			$contenido .= "</font>";
			$contenido .= "</body>";
			$contenido .= "</html> ";	
			
			
		}
		
		
		echo json_encode($contenido);
		exit();
		
		}
		
		
		if($_REQUEST["acc"] == "Ingreso_Emergencia_Modificar")
		{    

		     $IdAtencion=$_REQUEST["IdAtencion"];
			 $IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];
			 $ApellidoPaterno=$_REQUEST["ApellidoPaterno"];
			 $ApellidoMaterno=$_REQUEST["ApellidoMaterno"];
			 $PrimerNombre=$_REQUEST["PrimerNombre"];
			 $NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
			 $FechaNacimiento=$_REQUEST["FechaNacimiento"];
			 //$FechaTurno2=$_REQUEST["FechaTurno2"];
			 $FechaIngreso=$_REQUEST["FechaIngreso"];
			 $HoraIngreso=$_REQUEST["HoraIngreso"];
			 $Consultorio=$_REQUEST["Consultorio"];
			 $FuenteFinanciamiento=$_REQUEST["FuenteFinanciamiento"];
			 $Sexo=$_REQUEST["Sexo"];
			 $IdServicio=$_REQUEST["IdServicio"];
			 $TipoServicio=$_REQUEST["TipoServicio"];
			 $TipoOrigen=$_REQUEST["TipoOrigen"];
			 $TipoGravedad=$_REQUEST["TipoGravedad"];
			 $ServiciosEmergencia=ServiciosEmergencia_C();
			 $TiposServiciosEmergencia=TiposServiciosEmergencia_C();
			 $OrigenesEmergencia=Mostrar_Origenes_Emergencia_C();
			 $GravedadEmergencia=Mostrar_Gravedad_Emergencia_C();
			 $Mostrar_Causas_Morbilidad=Mostrar_Causas_Morbilidad_C();
			 $ListaDiagnostico_idatencion=Mostrar_Diagnosticos_Emergencia_IdAtencion_C($IdAtencion);
			 $Mostrar_Triaje=Mostrar_Triaje_Paciente_C($IdAtencion);
			 $Mostrar_Causa_Morbilidad_X_IdAtencion=Mostrar_Causa_Morbilidad_X_IdAtencion_C($IdAtencion);
			 
			 include('../../MVC_Vista/Emergencia/EmergenciaAgregarV.php');
			 
		}	
		
		
		
		if($_REQUEST["acc"] == "MostrarDiagnosticos")
		{    
			$ListaDiagnosticosEmergencia=Mostrar_Diagnosticos_Emergencia_C();
			$IdAtencion=$_REQUEST["IdAtencion"];
			include('../../MVC_Vista/Emergencia/MostrarDiagnosticos.php');

		}



        /*Eliminar el Diagnostico de la Atencion de Emergencia  */
		
		if($_REQUEST["acc"] == "Eliminar_Diagnostico")
		{    

			$IdDiagnostico_Atencion=$_REQUEST["IdDiagnostico_Atencion"];
			$IdAtencion=$_REQUEST["IdAtencion"];
			$Eliminar_Diagnostico_Atencion_Emergencia=Eliminar_Diagnostico_Atencion_Emergencia_C($IdDiagnostico_Atencion);
			$ListaDiagnostico_idatencion=Mostrar_Diagnosticos_Emergencia_IdAtencion_C($IdAtencion);
			include('../../MVC_Vista/Emergencia/Lista_Diagnosticos.php');
		}
	
	
		if($_REQUEST["acc"] == "AgregarDiagnostico")
		{    
			$IdDiagnostico=$_REQUEST["IdDiagnostico"];
			$IdAtencion=$_REQUEST["IdAtencion"];
			$Verificar_Diagnosticos_Emergencia=Verificar_Diagnosticos_Emergencia_C($IdDiagnostico,$IdAtencion);
			if($Verificar_Diagnosticos_Emergencia=='0')
			{
			$Agregar_Diagnosticos_Emergencia=Agregar_Diagnosticos_Emergencia_C($IdDiagnostico,$IdAtencion);
			}				
		}	


		if($_REQUEST["acc"] == "Verificar_Diagnostico")
		{    
			$IdDiagnostico=$_REQUEST["IdDiagnostico"];
			$IdAtencion=$_REQUEST["IdAtencion"];
			$Verificar_Diagnosticos_Emergencia=Verificar_Diagnosticos_Emergencia_C($IdDiagnostico,$IdAtencion);
			if($Verificar_Diagnosticos_Emergencia=='1')
			{
			include('../../MVC_Vista/Emergencia/Mensaje_Diagnostico_Existente_Emergencia.php');
			}
			else
			{
			include('../../MVC_Vista/Emergencia/Mensaje_Diagnostico_Agregado_Emergencia.php');	
			}				
		}	





		if($_REQUEST["acc"] == "Triaje")
		{    

			$idatencion_triaje=$_REQUEST["idatencion_triaje"];
			$accion_triaje=$_REQUEST["accion_triaje"];		
			$presion=$_REQUEST["presion"];
			$talla=$_REQUEST["talla"];
			$temperatura=$_REQUEST["temperatura"];
			$peso=$_REQUEST["peso"];
			$pulso=$_REQUEST["pulso"];
			$frecuencia_cardiaca=$_REQUEST["frecuencia_cardiaca"];
			$frecuencia_respiratoria=$_REQUEST["frecuencia_respiratoria"];
			$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
			

			if($accion_triaje == "insert")
			{
			$Agregar_Triaje_Paciente=Agregar_Triaje_Paciente_C($idatencion_triaje,$NroHistoriaClinica,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor);
			}

			if($accion_triaje == "update")
			{
			$Modificar_Triaje_Paciente=Modificar_Triaje_Paciente_C($idatencion_triaje,$NroHistoriaClinica,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor);
			}

		}		
		


		if($_REQUEST["acc"] == "Mostrar_Triaje")
		{    

			$idatencion_triaje=$_REQUEST["idatencion_triaje"];
			$accion_triaje=$_REQUEST["accion_triaje"];		
			$presion=$_REQUEST["presion"];
			$talla=$_REQUEST["talla"];
			$temperatura=$_REQUEST["temperatura"];
			$peso=$_REQUEST["peso"];
			$pulso=$_REQUEST["pulso"];
			$frecuencia_cardiaca=$_REQUEST["frecuencia_cardiaca"];
			$frecuencia_respiratoria=$_REQUEST["frecuencia_respiratoria"];
			include('../../MVC_Vista/Emergencia/Mostrar_Triaje.php');
		}




		if($_REQUEST["acc"] == "Refrescar_Diagnostico")
		{    
		     $IdAtencion=$_REQUEST["IdAtencion"];
			 $ListaDiagnostico_idatencion=Mostrar_Diagnosticos_Emergencia_IdAtencion_C($IdAtencion);
			 include('../../MVC_Vista/Emergencia/Lista_Diagnosticos.php');
		}	



        
		if($_REQUEST["acc"] == "Modificar_Emergencia")
		{    
		     
			 
			$accion_triaje=$_REQUEST["accion_triaje"];		
			$IdAtencion=$_REQUEST["IdAtencion"];
			$IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];
			$NroHistoriaClinica=$_REQUEST["NroHistoriaClinica"];
		 
			/*///*/
		 	  $IdTipoServicio=$_REQUEST["IdTipoServicio"];
			 $IdOrigenAtencion=$_REQUEST["IdOrigenAtencion"];
			 $IdServicio=$_REQUEST["IdServicio"];
			 $IdTipoGravedad=$_REQUEST["IdTipoGravedad"];
			 $IdCausaExternaMorbilidad=$_REQUEST["IdCausaExternaMorbilidad"];
			
		    
			 $idatencion_triaje=$_REQUEST["idatencion_triaje"];
		    $IdCausaExternaMorbilidad=$_REQUEST["IdCausaExternaMorbilidad"];
			
			/*Triaje*/
			
			 $pulso=$_REQUEST["TriajePulso"];
			 $temperatura=$_REQUEST["TriajeTemperatura"];
			 $presion_s=$_REQUEST["presion_s"];
			 $presion_d=$_REQUEST["presion_d"];
			 $frecuencia_cardiaca=$_REQUEST["TriajeFrecCardiaca"];
			 $frecuencia_respiratoria=$_REQUEST["TriajeFrecRespiratoria"];
			 $peso=$_REQUEST["TriajePeso"];
			 $talla=$_REQUEST["TriajeTalla"];
			  $TriajePerimCefalico=$_REQUEST["TriajePerimCefalico"];
			
			$mensaje_triaje=$_REQUEST["mensaje_triaje"];
			$accion_triaje=$_REQUEST["accion_triaje"];
			$IdEmpleado=$_REQUEST["IdEmpleado"];
			
			 
			$presion=$presion_s.'/'.$presion_d;
			$ip = $_SERVER['REMOTE_ADDR'];
			$NombrePC = NombrePC();
			$arreglo=explode('.',$NombrePC); 
			$hostname=mb_strtoupper($arreglo[0]);
			 
			/*Diagnostico*/
			Modificar_Emergencia_Datos_Basicos_M($IdAtencion,$IdTipoGravedad,$IdServicio,$IdOrigenAtencion,$IdTipoServicio,$IdEmpleado,$hostname);
			Modificar_Causa_Morbilidad_X_IdAtencion_C($IdCausaExternaMorbilidad,$IdAtencion);
			Eliminar_Diagnostico_Atencion_Emergencia_C($IdAtencion);
			
			$contadorCE=$_REQUEST["contadorCE"];
			for ($i = 1; $i <= $contadorCE; $i++){
			 $IdDiagnostico=$_REQUEST["IdDiagnostico".$i];
					 
			  Agregar_Diagnosticos_Emergencia_C($IdDiagnostico,$IdAtencion);
			
			}
			  
			if($accion_triaje == "insert"){
			Agregar_Triaje_Paciente_C($IdAtencion,$NroHistoriaClinica,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor,$TriajePerimCefalico);
			}

			if($accion_triaje == "update"){
			Modificar_Triaje_Paciente_C($IdAtencion,$NroHistoriaClinica,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor,$TriajePerimCefalico);
			}
			 
			
		}	






		if($_REQUEST["acc"] == "Cargar_Topicos")
		{    
			 $ServiciosEmergencia=ServiciosEmergencia_C();
			 include('../../MVC_Vista/Emergencia/Selected_topico.php');
		}	
		
		
		
		if($_REQUEST["acc"] == "Buscar_Diagnosticos")
		{     
		         $TipoBusqueda=$_REQUEST["TipoBusqueda"];
			if($TipoBusqueda==1)
				{
						  $variable='%'.strtoupper($_REQUEST["q"]).'%';
						  $TipoBusqueda=$_REQUEST["TipoBusqueda"];
				}
			else{
				          $variable='%'.strtoupper($_REQUEST["q"]).'%';
						  $TipoBusqueda=$_REQUEST["TipoBusqueda"];
				}
			
			 
	    
		$ListarAutocompletar =Buscar_Diagnosticos_Emergencia_C($variable,$TipoBusqueda);
		
		 if($ListarAutocompletar!=NULL)
		{
			foreach ($ListarAutocompletar as $item)
			{
				
				
				$IdDiagnostico= $item["IdDiagnostico"] ;
				$CodigoCIE2004= $item["CodigoCIE2004"] ;
				$Descripcion= $item["Descripcion"] ;
	
				$fintrado =$item["CodigoCIE2004"].' '.$item["Descripcion"];	
			    echo "$fintrado|$IdDiagnostico|$CodigoCIE2004|$Descripcion\n";
		   
			}
		}
			
			
			//include('../../MVC_Vista/Emergencia/Selected_topico.php');
		}	
		
		
		
		//autocompletado de pacientes

		if($_REQUEST["acc"] == "Buscar_Pacientes_LV_C")
		{     
		$NumHis = $_REQUEST["numhis"];   	    
		$ListarAutocompletar =Buscar_Pacientes_LV_C($NumHis);
		
		 if($ListarAutocompletar!=NULL)
		{
			foreach ($ListarAutocompletar as $item)
			{	
				$IDPACIENTE = $item['IdPaciente'];
				$PATERNO =strtoupper(utf8_encode($item['PATERNO']));	
				$MATERNO =strtoupper(utf8_encode($item['MATERNO']));
				$NOMBRES =strtoupper(utf8_encode($item['NOMBRES']));
			}

			echo json_encode(
			array(
				'IDPACIENTE' => $IDPACIENTE,
				'PATERNO' => utf8_decode($PATERNO),
				'MATERNO' => utf8_decode($MATERNO),
				'NOMBRES' => utf8_decode($NOMBRES)
				)
			);
			exit();
		}

		else {
			echo json_encode(array('dato' => 'bad'));
			exit();
		}	
		
		}

		// AUTOCOMPLETADO DE ESPECIALIDADES
		if($_REQUEST["acc"] == "Buscar_Especialidad_lv_C")
		{     
		    $TipoBusqueda=$_REQUEST["TipoBusqueda"];
			if($TipoBusqueda==1)
				{
					$variable='%'.strtoupper($_REQUEST["q"]).'%';
					$TipoBusqueda=$_REQUEST["TipoBusqueda"];
				}
			else{
				    $variable='%'.strtoupper($_REQUEST["q"]).'%';
					$TipoBusqueda=$_REQUEST["TipoBusqueda"];
				}	 
	    
		$ListarAutocompletar =Buscar_Especialidad_lv_C($variable,$TipoBusqueda);


			if($ListarAutocompletar!=NULL)
			{
				foreach ($ListarAutocompletar as $item)
				{
					$datos_json[] = array('id' => $item["IdEspecialidad"], 'especilidades' => $item["Nombre"]);			   
				}

			}
				echo json_encode($datos_json);
	            exit();
		}

		//listar modulos de lista de verificacion
		if ($_REQUEST["acc"] == "Listar_Modulos_LV_C") {
			$modulos = Listar_Modulos_LV_C();

			if($modulos!=NULL) {
				foreach ($modulos as $item)
				{	
					$IDMODULO[] = $item['IdModulo'];
					$MODULO[] =strtoupper(utf8_decode($item['Nombre']));				
				}

				//retornando valores

				echo json_encode(array('IDMODULO' => $IDMODULO,'MODULO' => $MODULO));
				exit(); 
			}


		}

		//especialidad
		if ($_REQUEST["acc"] == "Listar_Especialidad_Id_LV_C") {
			$idespecilidad = $_REQUEST["idespecilidad"];
			$especialidad = Listar_Especialidad_Id_LV_C($idespecilidad);

			if($especialidad!=NULL) {
				foreach ($especialidad as $item)
				{	
					$IdEspecialidad = $item['IdEspecialidad'];
					$NOMBRESESPE =utf8_encode($item['NOMBRESESPE']);				
				}

				//retornando valores

				echo json_encode(array('IdEspecialidad' => $IdEspecialidad,'NOMBRESESPE' => utf8_decode($NOMBRESESPE)));
				exit(); 
			}


		}

		//tipos de sala lista
		if ($_REQUEST["acc"] == "Listar_Salas_LV_C") {
			$idtiposala = $_REQUEST["idtiposala"];

			$salas = Listar_Salas_LV_C($idtiposala);
			if($salas!=NULL) {
				foreach ($salas as $item)
				{	
					$array[] = array(
						'id'   => $item['IdSalas'],
						'text' => $item['Nombre']
						);
					//$IdSalas[] = ;
					//$Nombre[] =;
				}

				//retornando valores

				echo json_encode($array);
				exit(); 
			}
		}

		//insertando inicio
		if ($_REQUEST["acc"] == "Insertando_Inicio_LV_C") {
			$estado = 1; 
			$especialidad_id = $_REQUEST["especialidad_id"];
			$tipo_sala_id = $_REQUEST["tipo_sala_id"];
			$numhis = $_REQUEST["numhis"];

			$hora_inicio = date("Y-m-d H:i:s");
			
			$respuesta = Insertando_Inicio_LV_C($numhis,$tipo_sala_id,$especialidad_id,'2015-04-22',$estado);

			echo json_encode(array('respuesta' => $respuesta));
			exit();
		}

		//buscando ultimo registro para mostrar cabecera
		if ($_REQUEST["acc"] == "Buscando_Ultimo_LV_C") {
			$idpaciente = $_REQUEST["idpaciente"];
			$especialidad = $_REQUEST["especialidad"];
			$sala = $_REQUEST["sala"];

			$datos = Buscando_Ultimo_LV_C($idpaciente,$especialidad,$sala);

			if ($datos!=NULL) {
				
				foreach($datos as $item){
					$CODIGO = $item["CODIGO"];
					$NUMHIS = $item["NUMHIS"];
					$ESPECIALIDAD = $item["ESPECIALIDAD"];
					$SALA = $item["SALA"];
				}

				echo json_encode(array('CODIGO' => $CODIGO,'NUMHIS' => $NUMHIS,'ESPECIALIDAD' => $ESPECIALIDAD,'SALA' => $SALA));
				exit();
			}


		}

		// mostrando empleados para los miembros del equipo
		if ($_REQUEST["acc"] == "Buscando_Empleado_LV_C") {
			$empleados = Buscando_Empleado_LV_C();
			if($empleados!=NULL) {
				foreach ($empleados as $item)
				{	
					$array[] = array(
						'id'   => $item['IDEMPLEADO'],
						'text' => strtoupper($item['NOMBRESEMPLEADOS'])
						);
				}
				//retornando valores
				echo json_encode($array);
				exit(); 
			}
		}

		// mostrando medicos para los miembros del equipo
		if ($_REQUEST["acc"] == "Buscando_Medicos_LV_C") {
			$empleados = Buscando_Medicos_LV_M();
			if($empleados!=NULL) {
				foreach ($empleados as $item)
				{	
					$array[] = array(
						'id'   => $item['IDEMPLEADO'],
						'text' => strtoupper($item['MEDICO'])
						);
				}
				//retornando valores
				echo json_encode($array);
				exit(); 
			}
		}
		
		//insertando entrada
		if ($_REQUEST["acc"] == "Insertando_Entrada_LV_C") {
			$estado = 2;
			$combinaciones_entrada = $_REQUEST["array_combinaciones_entrada"];
			if ($combinaciones_entrada == NULL) {
				echo json_encode(array('respuesta' => 'M_2'));
				exit();
			}
			$codigo_documento = $_REQUEST["codigo_documento"];
			$hora_inicio = date("Y-m-d H:i:s");

			$combinacion_final = " ";
			foreach($combinaciones_entrada as $combinaciones_entrada_temp){
				$combinacion_final .= $combinaciones_entrada_temp["value"] . "-";
			}

			$respuesta = Insertando_Entrada_LV_C($codigo_documento,$combinacion_final,$estado,$hora_inicio);
			echo json_encode(array('respuesta' => $respuesta));
			exit();
		}

		//insertando pausa quirurgica
		if ($_REQUEST["acc"] == "Insertando_Pausa_LV_C") {
			$estado = 3;
			$combinaciones_pausa = $_REQUEST["array_combinaciones_pausa"];
			if ($combinaciones_pausa == NULL) {
				echo json_encode(array('respuesta' => 'M_2'));
				exit();
			}
			$codigo_documento = $_REQUEST["codigo_documento"];
			$hora_inicio = date("Y-m-d H:i:s");

			//trayendo la antigua combinaciones
			$combinacion_antigua = Buscando_Combinacion_Antigua_LV_M($codigo_documento);

			foreach ($combinacion_antigua as $item) {
				$combinacion_antigua_f = $item["Combinaciones"];
			}

			$combinacion_final = " ";
			foreach($combinaciones_pausa as $combinaciones_pausa_temp){
				$combinacion_final .= $combinaciones_pausa_temp["value"] . "-";
			}

			$combinacion_nueva = $combinacion_antigua_f.$combinacion_final;

			$respuesta = Insertando_Pausa_LV_C($codigo_documento,$combinacion_nueva,$estado,$hora_inicio);
			echo json_encode(array('respuesta' => $respuesta));
			exit();
		}

		//insertando salida
		if ($_REQUEST["acc"] == "Insertando_Salida_LV_C") {
			$estado = 4;
			$combinaciones_salida = $_REQUEST["array_combinaciones_salida"];
			if ($combinaciones_salida == NULL) {
				echo json_encode(array('respuesta' => 'M_2'));
				exit();
			}
			$codigo_documento = $_REQUEST["codigo_documento"];
			$hora_inicio = date("Y-m-d H:i:s");

			//trayendo la antigua combinaciones
			$combinacion_antigua = Buscando_Combinacion_Antigua_LV_M($codigo_documento);

			foreach ($combinacion_antigua as $item) {
				$combinacion_antigua_f = $item["Combinaciones"];
			}

			$combinacion_final = " ";
			foreach($combinaciones_salida as $combinaciones_salida_temp){
				$combinacion_final .= $combinaciones_salida_temp["value"] . "-";
			}

			$combinacion_nueva = $combinacion_antigua_f.$combinacion_final;

			$respuesta = Insertando_Salida_LV_C($codigo_documento,$combinacion_nueva,$estado,$hora_inicio);
			echo json_encode(array('respuesta' => $respuesta));
			exit();
		}

		//actualizando observaciones
		if ($_REQUEST["acc"] == "Actualizando_Observaciones_LV_C") {
			$codigo_documento = $_REQUEST["codigo_documento"];
			$observaciones = $_REQUEST["observaciones"];
			$observacion_antigua = Buscando_Observacion_Antigua_LV_C($codigo_documento);

			foreach ($observacion_antigua as $item) {
				$observacion_antigua_f = $item["observaciones"];
			}

			if ($observacion_antigua_f==" " || $observacion_antigua_f==NULL) {
				//echo "aqui toy";exit();
				$observaciones_actualizar = $observaciones;
				$respuesta = Actualizando_Observaciones_LV_C($codigo_documento,$observaciones_actualizar);
				echo json_encode(array('respuesta' => $respuesta));
				exit();
			}

			else {
				//echo "aqui vacio";exit();
				$observaciones_actualizar = $observacion_antigua_f."-*-".$observaciones;
				$respuesta = Actualizando_Observaciones_LV_C($codigo_documento,$observaciones_actualizar);	
				echo json_encode(array('respuesta' => $respuesta));			
				exit();
			}
		}

		if ($_REQUEST["acc"] == "Actualizando_Observaciones_Final_LV_C"){
			$codigo_documento = $_REQUEST["codigo_documento"];
			$observaciones = $_REQUEST["observaciones"];
			$respuesta = Actualizando_Observaciones_LV_C($codigo_documento,strtoupper($observaciones));
			echo json_encode(array('respuesta' => $respuesta));
			exit();
		}

		//ingresar miembros del equipo
		if ($_REQUEST["acc"] == "Ingresar_Miembros_Equipos_LV_C") {
			$codigo_documento = $_REQUEST["codigo_documento"];
			$cirujano_medico_id = $_REQUEST["cirujano_medico_id"];
			$anestesiologo_medico_id = $_REQUEST["anestesiologo_medico_id"];
			$instrumentista_id = $_REQUEST["instrumentista_id"];
			$asistente_uno_id = $_REQUEST["asistente_uno_id"];
			$asistente_dos_id = $_REQUEST["asistente_dos_id"];
			$asistente_tres_id = $_REQUEST["asistente_tres_id"];
			$enf_circulantes_id = $_REQUEST["enf_circulantes_id"];

			if(strlen($cirujano_medico_id) == 0 ) {
				$cirujano_medico_id = "NULL";
			}
			if(strlen($anestesiologo_medico_id) == 0 ) {
				$anestesiologo_medico_id = "NULL";
			}
			if(strlen($instrumentista_id) == 0 ) {
				$instrumentista_id = "NULL";
			}
			if(strlen($asistente_uno_id) == 0 ) {
				$asistente_uno_id = "NULL";
			}
			if(strlen($asistente_dos_id) == 0 ) {
				$asistente_dos_id = "NULL";
			}
			if(strlen($asistente_tres_id) == 0 ) {
				$asistente_tres_id = "NULL";
			}
			if(strlen($enf_circulantes_id) == 0 ) {
				$enf_circulantes_id = "NULL";
			}

			$respuesta = Ingresar_Miembros_Equipos_LV_C($codigo_documento,$cirujano_medico_id,$anestesiologo_medico_id,$instrumentista_id,$asistente_uno_id,$asistente_dos_id,$asistente_tres_id,$enf_circulantes_id);
			echo json_encode(array('respuesta' => $respuesta));			
			exit();
		}

		//mostrando observaciones
		if ($_REQUEST["acc"] == "Mostrar_Observaciones_LV_C") {
			$codigo_documento = $_REQUEST["codigo_documento"];
			$observaciones_mostrar = Mostrar_Observaciones_LV_C($codigo_documento);

			if ($observaciones_mostrar!=NULL) {
				$texto_mostrar = " ";
				foreach($observaciones_mostrar as $item){
					$observaciones = explode("-*-",strtoupper(utf8_encode($item["observaciones"])));
						
						for ($i=0; $i < count($observaciones); $i++) { 
							$texto_mostrar .= utf8_decode($observaciones[$i])." ";
						}					
				}
				echo json_encode(array('texto_mostrar' => $texto_mostrar));			
				exit();
			}
		}

		//mostrando miembros para su validacion
		if ($_REQUEST["acc"] == "Lista_Miembros_LV_C") {
			$codigo_documento = $_REQUEST["codigo_documento"];

			$lista_ids = Mostrar_Miembros_LV_M($codigo_documento);

			if ($lista_ids!=NULL) {
				
				foreach($lista_ids as $lista_id){
					
					$IDCIRUJANO = $lista_id["IDCIRUJANO"];
					$IDANES 	= $lista_id["IDANES"];
					$IDINS 		= $lista_id["IDINS"];
					$IDAU 		= $lista_id["IDAU"];
					$IDAD 		= $lista_id["IDAD"];
					$IDAT 		= $lista_id["IDAT"];
					$IDEC 		= $lista_id["IDEC"];

				}

				if(strlen($IDCIRUJANO) > 2) {
					$id_empleado_array[] = array(
						'id' => $IDCIRUJANO, 
						'cargo' => 'CIRUJANO'
						);
				}

				if(strlen($IDANES) > 2) {
					$id_empleado_array[] = array(
						'id' => $IDANES, 
						'cargo' => 'ANESTESIOLOGO'
						);
				}

				if(strlen($IDINS) > 2) {
					$id_empleado_array[] = array(
						'id' => $IDINS, 
						'cargo' => 'ENF. INSTRUMENTISTA'
						);
				}

				if(strlen($IDAU) > 2) {
					$id_empleado_array[] = array(
						'id' => $IDAU, 
						'cargo' => 'ASISTENTE 1'
						);
				}

				if(strlen($IDAD) > 2) {
					$id_empleado_array[] = array(
						'id' => $IDAD, 
						'cargo' => 'ASISTENTE 2'
						);
				}

				if(strlen($IDAT) > 2) {
					$id_empleado_array[] = array(
						'id' => $IDAT, 
						'cargo' => 'ASISTENTE 3'
						);
				}

				if(strlen($IDEC) > 2) {
					$id_empleado_array[] = array(
						'id' => $IDEC, 
						'cargo' => 'ENF. CIRCULANTE'
						);
				}

				$tabla_empleados = Mostrar_Datos_Empleado_LV_M($codigo_documento);
				
						foreach($tabla_empleados as $empleados){

							$array_empleados[] = array(
								'ID_EMPLEADO'	=> $empleados['IDEMPLEADO'],
								'CARGO'			=> utf8_encode(utf8_decode($empleados['CARGO'])),
								'NOMBRE'		=> utf8_encode(utf8_decode($empleados['NOMBRES'])),
								'VALIDADO'		=> utf8_encode(utf8_decode($empleados['VALIDADO']))
								);
						}
				

			//mostrando datos
			echo json_encode($array_empleados);
			exit();
			}
			
		}

		if ($_REQUEST["acc"] == "Validar_MIembros_LV_C") {
			
			$idempleado 		= 	$_REQUEST["idempleado"];
			$contrasena 		= 	md5($_REQUEST["contrasena"]);
			$cargo 				= 	$_REQUEST["cargo_empleado_validacion"];
			$codigo_documento 	= 	$_REQUEST["codigo_documento"];
			$respuesta 			= 	Validar_MIembros_LV_C($idempleado,$contrasena);
			$fechaValida 		= 	date("Y-m-d H:i:s");
			
			//derivando el cargo
			switch ($cargo) {
				case 'CIRUJANO':
					$cargo = 'CodValidaMC';
					break;
				
				case 'ANESTESIOLOGO':
					$cargo = 'CodValidaMA';
					break;

				case 'ENF. INSTRUMENTISTA':
					$cargo = 'CodValidaEI';
					break;

				case 'ASISTENTE 1':
					$cargo = 'CodValidaAU';
					break;

				case 'ASISTENTE 2':
					$cargo = 'CodValidaAD';
					break;

				case 'ASISTENTE 3':
					$cargo = 'CodValidaAT';
					break;

				case 'ENF. CIRCULANTE':
					$cargo = 'CodValidaEC';
					break;
			}

			if ($respuesta!=NULL) {
				$respuesta_final = Insertar_CodValida_LV_M($cargo,$contrasena,$codigo_documento,$fechaValida);
				echo json_encode(array('respuesta' => $respuesta_final));
				exit();
			}

			else {
				echo json_encode(array('respuesta' => 'M_2'));
				exit();	
			}
		}		

		if ($_REQUEST["acc"] == "Mostrar_Listas_General_LV_C") {
			$inicio = microtime(true);
			$tabla_listas = Mostrar_Listas_General_LV_C();

			////////////////////////////////////////////////////////////////
			$listas_creadas = Mostrar_Listas_General_LV_M();
			foreach ($listas_creadas as $item) {
				$verificacion = Validacion_x_Estado_LV_M($item["CODIGO"]);

				if ($verificacion!=NULL AND $item["ESTADO"]==4) {
					//echo $item["CODIGO"].": validado<br>";
					Actualizando_Estado_LV_M($item["CODIGO"],5);
				}

				else{
					//echo $item["CODIGO"].": sin validar<br>";
				}

			}//PARA VALIDADO
			/////////////////////////////////////////////////////////////////
			if ($tabla_listas!=NULL) {
				foreach($tabla_listas as $item){
					//echo $item["CODIGO"]."\t".$item["IDPACIENTE"]."\t".strtoupper(utf8_decode($item["NOMBRES"]))."\t".$item["ESTADO"]."<br>";
					switch ($item["ESTADO"]) {
						case 1:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$salida 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 2:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$salida 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 3:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$salida 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 4:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$salida 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 5:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$salida 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$validado 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							break;	
					}

					$array_lista_verificacion[] = array(
						'CODIGO'		=> 	$item["CODIGO"],
						'IDPACIENTE'	=> 	$item["IDPACIENTE"],
						'PACIENTES'		=> 	strtoupper(utf8_decode($item["NOMBRES"])),
						'INICIO'		=> 	$inicio,
						'ENTRADA'		=> 	$entrada,
						'PAUSA'			=> 	$pausa,
						'SALIDA'		=> 	$salida,
						'VALIDAR'		=> 	$validado,
						'NUMHIS'		=>	$item["NUMHIS"]
						);
				}
			$final = microtime(true);
			$tiempo = $final - $inicio;
			echo json_encode($array_lista_verificacion);
			#echo $tiempo;
			exit();
			}
			
		}

		if ($_REQUEST["acc"] == "Mostrar_Listas_Validado_LV_C") {
			$tabla_listas = Mostrar_Listas_Validado_LV_C();

			if ($tabla_listas!=NULL) {
				foreach($tabla_listas as $item){
					
					switch ($item["ESTADO"]) {
						case 1:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$salida 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 2:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$salida 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 3:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$salida 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 4:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$salida 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$validado 	= "<img src='../../MVC_Complemento/img/no.png' width='15' heigth='15'>";
							break;
						case 5:
							$inicio  	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$entrada 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$pausa 		= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$salida 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							$validado 	= "<img src='../../MVC_Complemento/img/ok.png' width='20' heigth='20'>";
							break;	
					}

					$array_lista_verificacion[] = array(
						'CODIGO'		=> $item["CODIGO"],
						'IDPACIENTE'	=> $item["IDPACIENTE"],
						'PACIENTES'		=> strtoupper(utf8_decode($item["NOMBRES"])),
						'INICIO'		=> $inicio,
						'ENTRADA'		=> $entrada,
						'PAUSA'			=> $pausa,
						'SALIDA'		=> $salida,
						'VALIDAR'		=> $validado,
						'NUMHIS'		=> $item["NUMHIS"]
						);
				}

			echo json_encode($array_lista_verificacion);
			exit();
			}
		}

		if ($_REQUEST["acc"] == "Mostrar_Estado_LV_C") {
			$codigo_documento = $_REQUEST["codigo_documento"];

			$datos = Mostrar_Estado_LV_C($codigo_documento);
			
			if ($datos!=NULL) {
				foreach($datos as $item) {
					$estado = $item["estado"];
					$numhis = $item["numhis"];
					$especialidad = strtoupper(utf8_decode($item["especialidad"]));
					$sala = strtoupper(utf8_decode($item["sala"]));
					$codigo = $item["codigo"];
				}

				echo json_encode(
					array(
						'estado' 		=> $estado,
						'numhis' 		=> $numhis,
						'especialidad' 	=> utf8_encode($especialidad),
						'sala' 			=> $sala, 
						'codigo' 		=> $codigo
						)
					);
				exit();
			}
			
		}

		if ($_REQUEST["acc"] == "Generando_Reportes_Pruebas") {
			$anno_buscar 	= 	$_REQUEST["anno_buscar"];
			$mes_buscar		=	$_REQUEST["mes_buscar"];
			$especialidad   = 	$_REQUEST["especialidad"];

			$data = Cantidad_Doc_x_Mes_M($anno_buscar,$mes_buscar,$especialidad);

			$cantidad_datos = count($data);			//CANTIDAD DE REGISTROS DEVUELTOS
			
			$combinaciones_general = " ";
			foreach ($data as $datos) {
				$combinaciones_general .= $datos["CLAVES"];
			}

			//PASANDO COMBINACIONES A UN ARRAY
			$tr = count(explode("-", $combinaciones_general))-1;
			for ($i=0; $i < $tr; $i++) { 
				$combinaciones_temp = explode("-", $combinaciones_general);
				$combinaciones[]    = $combinaciones_temp[$i];
			}

			
			
		}
		
		
		
///-----------------------REPORTES----------------------
//        RESPORTES EMERGENCIA
//------------------------------------------------------				

if($_REQUEST["acc"] == "Reportes")
		{    
			 //$ServiciosEmergencia=ServiciosEmergencia_C();
			  $ServiciosEmergencia=ServiciosEmergencia_C();
			  $ListarTiposServicio= Sigesa_RP_TiposServicio_C();
			 include('../../MVC_Vista/Emergencia/ReportesV.php');
		}	
if($_REQUEST["acc"] == "Reportes_Exel")
		{    
			 //$ServiciosEmergencia=ServiciosEmergencia_C();
			 include('../../MVC_Vista/Emergencia/Reporte_IngresoxEmergencia.php');
		}	

			//----------------------------//
			// FUNCIONES   EMERGENCIA   //
			//--------------------------//
			function Mostrar_Pacientes_Emergencia_C($variable,$FechaInicio,$FechaFin,$IdServicio,$busqueda)
			{
				return Mostrar_Pacientes_Emergencia_M($variable,$FechaInicio,$FechaFin,$IdServicio,$busqueda);
			}
			

			function Mostrar_Pacientes_Emergencia_Todos_Servicios_C($variable,$FechaInicio,$FechaFin,$busqueda)
			{
				return  Mostrar_Pacientes_Emergencia_Todos_Servicios_M($variable,$FechaInicio,$FechaFin,$busqueda);
			}


			function Mostrar_Diagnosticos_Emergencia_IdAtencion_C($IdAtencion)
			{
				return Mostrar_Diagnosticos_Emergencia_IdAtencion_M($IdAtencion);
			}
			

			function ServiciosEmergencia_C()
			{
				return ServiciosEmergencia_M();
			}
			

			function TiposServiciosEmergencia_C()
			{
				return TiposServiciosEmergencia_M();
			}
			

			function Mostrar_Origenes_Emergencia_C()
			{
				return Mostrar_Origenes_Emergencia_M();
			}
			

			function Mostrar_Gravedad_Emergencia_C()
			{
				return Mostrar_Gravedad_Emergencia_M();
			}
			
			
			function Mostrar_Diagnosticos_Emergencia_C()
			{
				return Mostrar_Diagnosticos_Emergencia_M();
			}


			function Verificar_Diagnosticos_Emergencia_C($IdDiagnostico,$IdAtencion)
			{
				return Verificar_Diagnosticos_Emergencia_M($IdDiagnostico,$IdAtencion);
			}


			function Agregar_Diagnosticos_Emergencia_C($IdDiagnostico,$IdAtencion)
			{
				return Agregar_Diagnosticos_Emergencia_M($IdDiagnostico,$IdAtencion);
			}

			function Mostrar_Triaje_Paciente_C($IdAtencion)
			{
				return Mostrar_Triaje_Paciente_M($IdAtencion);
			}


			function Agregar_Triaje_Paciente_C($idatencion_triaje,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor,$TriajePerimCefalico)
			{
				return Agregar_Triaje_Paciente_M($idatencion_triaje,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor,$TriajePerimCefalico);
			}


			function Modificar_Triaje_Paciente_C($idatencion_triaje,$NroHistoriaClinica,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor,$TriajePerimCefalico)
			{
				return Modificar_Triaje_Paciente_M($idatencion_triaje,$NroHistoriaClinica,$presion,$talla,$temperatura,$peso,$pulso,$frecuencia_cardiaca,$frecuencia_respiratoria,$FechaServidor,$TriajePerimCefalico);
			}

		    
		    /*function Modificar_Emergencia_Datos_Basicos_C($idatencion_triaje,$IdTipoGravedad,$IdServicio,$IdOrigenAtencion,$IdTipoServicio,$Idempleado)
			{
				return  Modificar_Emergencia_Datos_Basicos_M($idatencion_triaje,$IdTipoGravedad,$IdServicio,$IdOrigenAtencion,$IdTipoServicio,$Idempleado);
			}*/
			

			function Mostrar_Causas_Morbilidad_C()
			{
				return Mostrar_Causas_Morbilidad_M();
			}



			function Mostrar_Causa_Morbilidad_X_IdAtencion_C($IdAtencion)
			{
				return Mostrar_Causa_Morbilidad_X_IdAtencion_M($IdAtencion);
			}



			function Modificar_Causa_Morbilidad_X_IdAtencion_C($IdCausaExternaMorbilidad,$IdAtencion)
			{
				return Modificar_Causa_Morbilidad_X_IdAtencion_M($IdCausaExternaMorbilidad,$IdAtencion);
			}



			function Eliminar_Diagnostico_Atencion_Emergencia_C($IdDiagnostico_Atencion)
			{
				return Eliminar_Diagnostico_Atencion_Emergencia_M($IdDiagnostico_Atencion);
			}


		function RetornaFechaServidorSQL_C(){	
		return RetornaFechaServidorSQL_M(); 
		}
		
		function Buscar_Diagnosticos_Emergencia_C($variable,$tipo_busqueda)		{	
		return  Buscar_Diagnosticos_Emergencia_M($variable,$tipo_busqueda);
		}	

        function Sigesa_ReporteIngresoxEmergencia_SIS_SOAT_C($FechaInicio,$FechaFin){
    	return   Sigesa_ReporteIngresoxEmergencia_SIS_SOAT_M($FechaInicio,$FechaFin);
     	}
        function Sigesa_EmergenciaPC_Digitacion_C($IdAtencion){
			return Sigesa_EmergenciaPC_Digitacion_M($IdAtencion);
			}
		function Buscar_Pacientes_LV_C($NumHis)
		{
		return Buscar_Pacientes_LV_M($NumHis);
		}

		function Buscar_Especialidad_lv_C($variable,$tipo_busqueda)		{	
		return  Buscar_Especialidad_lv_M($variable,$tipo_busqueda);
		}

		function Listar_Modulos_LV_C(){
		return Listar_Modulos_LV_M();
		}

		function Listar_Especialidad_Id_LV_C($idespecilidad){
		return Listar_Especialidad_Id_LV_M($idespecilidad);
		}

		function Listar_Salas_LV_C($idtiposala){
		return Listar_Salas_LV_M($idtiposala);
		}

		function Insertando_Inicio_LV_C($especialidad_id,$tipo_sala_id,$numhis,$hora_inicio,$estado){
		return Insertando_Inicio_LV_M($especialidad_id,$tipo_sala_id,$numhis,$hora_inicio,$estado);
		}

		function Buscando_Ultimo_LV_C($idpaciente,$especialidad,$sala){
		return Buscando_Ultimo_LV_M($idpaciente,$especialidad,$sala);
		}

		function Buscando_Empleado_LV_C(){
		return Buscando_Empleado_LV_M();
		}

		function Insertando_Entrada_LV_C($codigo_documento,$combinacion_final,$estado,$hora_inicio) {
		return Insertando_Entrada_LV_M($codigo_documento,$combinacion_final,$estado,$hora_inicio);
		}

		function Insertando_Pausa_LV_C($codigo_documento,$combinacion_nueva,$estado,$hora_inicio) {
		return Insertando_Pausa_LV_M($codigo_documento,$combinacion_nueva,$estado,$hora_inicio);
		}

		function Insertando_Salida_LV_C($codigo_documento,$combinacion_nueva,$estado,$hora_inicio) {
		return Insertando_Salida_LV_M($codigo_documento,$combinacion_nueva,$estado,$hora_inicio);
		}

		function Buscando_Observacion_Antigua_LV_C($codigo_documento){
		return Buscando_Observacion_Antigua_LV_M($codigo_documento);
		}

		function Actualizando_Observaciones_LV_C($codigo_documento,$observaciones_nueva) {
		return Actualizando_Observaciones_LV_M($codigo_documento,$observaciones_nueva);
		}

		function Ingresar_Miembros_Equipos_LV_C($codigo_documento,$cirujano_medico_id,$anestesiologo_medico_id,$instrumentista_id,$asistente_uno_id,$asistente_dos_id,$asistente_tres_id,$enf_circulantes_id){
		return Ingresar_Miembros_Equipos_LV_M($codigo_documento,$cirujano_medico_id,$anestesiologo_medico_id,$instrumentista_id,$asistente_uno_id,$asistente_dos_id,$asistente_tres_id,$enf_circulantes_id);
		}

		function Mostrar_Observaciones_LV_C($codigo_documento){
		return Buscando_Observacion_Antigua_LV_M($codigo_documento);
		}

		function Validar_MIembros_LV_C($idempleado,$contrasena){
		return Validar_MIembros_LV_M($idempleado,$contrasena);
		}

		function Mostrar_Listas_General_LV_C(){
		return Mostrar_Listas_General_LV_M();
		}

		function Mostrar_Estado_LV_C($codigo_documento){
		return Mostrar_Estado_LV_M($codigo_documento);
		}

		function Mostrar_Listas_Validado_LV_C(){
		return Mostrar_Listas_Validado_LV_M();
		}
		function Sigesa_RP_TiposServicio_C(){
				return  Sigesa_RP_TiposServicio_M();
				}
				
				
//////25/01/2017
if($_REQUEST["acc"] == "ImprimeHojaEmergenciapdf")
		{  
 include('../../MVC_Vista/Emergencia/HojaClinicaPDF.php');
		}
				
?>