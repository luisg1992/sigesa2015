<?php
ini_set('memory_limit','1024M');
?>

<?php

	include("../../MVC_Modelo/FarmaciaM.php"); 
	include('../../MVC_Complemento/librerias/Funciones.php');
	include('../../MVC_Modelo/SistemaM.php');
	
	
	 
	function Retornar_Tipo_Medicamento($TipoProductoSismed)
	{
		$data = Filtrar_TiposProducto_M($TipoProductoSismed);
		if (count($data) >= 1) 
		{
		$TiposProducto=$data[0]['Descripcion'];	
		return $TiposProducto;	
		}
		else
		{
		return  'No Definido';
		}
	}
	
	
	
	// Vista de Catologo de Bienes
	if($_GET["acc"] == "CatalogoBienes")
	{
   	include('../../MVC_Vista/Farmacia/Administrar_Medicamentos.php');
	}
	

	//Mostrar la Grilla de Medicamentos
	if ($_REQUEST["acc"] == "MostrarMedicamentosGrilla") {
		$data = Mostrar_Lista_de_Medicamentos_2();
		for ($i=0; $i < count($data); $i++) { 
			$Medicamentos[$i] = array(
				'IdProducto'	=> $data[$i]['IdProducto'],
				'Codigo'	=> $data[$i]['Codigo'],
				'Nombre'			=> $data[$i]['Nombre'],
				'TipoProductoSismed'			=> $data[$i]['TipoProductoSismed'],
				'TipoProducto'=> $data[$i]['Descripcion']
				);
		}
		echo json_encode($Medicamentos);
		exit();
	}
	 	
	//Modificar el Tipo de Medicamento    --29-10-2017
	if ($_REQUEST["acc"] == "ModificarMedicamento") {
		$IdProducto = $_REQUEST["IdProducto"];
		$TipoProductoSismed = $_REQUEST["TipoProductoSismed"];
		$data = Modificar_Tipo_Medicamento($TipoProductoSismed,$IdProducto);
		echo json_encode($data);
		exit();
	}
 
		

	
	
	
?>