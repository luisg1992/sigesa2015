<?php 
	
	include '../../MVC_Modelo/CarM.php';

	if ($_REQUEST["acc"] == "IngresarEspecialidad") {
		$nombreEspecialidad = $_REQUEST["nombreEspecialidad"];
		$piso = $_REQUEST["piso"];
		$respuesta = IngresarEspecialidadM($nombreEspecialidad,$piso);
		if ($respuesta == 'M_1') {
			echo json_encode($respuesta);
			exit();
		}
		else
		{
			echo json_encode('M_0');
			exit();
		}
	}

	#LISTAR ESPECIALIDADES
	if ($_REQUEST["acc"] == 'ListarEspecialidadesCombo') {
		$piso = $_REQUEST["piso"];
		$data = ListarEspecialidades($piso);

		for ($i=0; $i < count($data); $i++) { 
			$especialidades[$i] = array(
				'id'	=>	$data[$i]["IdEspecialidad"],
				'text'	=>	$data[$i]["NombreEspecialidad"]
				);
		}
		echo json_encode($especialidades);
		exit();
	}

	#INGRESAR INFORME OPERACIONAL
	if ($_REQUEST["acc"] == "IngresarNuevoInformeOperacional") {
		$diagnostico = $_REQUEST["diagnostico"];
		$procedimiento = $_REQUEST["procedimiento"];
		$tiempo_promedio = $_REQUEST["tiempo_promedio"];
		$numero_cirugias = $_REQUEST["numero_cirugias"];
		$idEspecialidad = $_REQUEST["idEspecialidad"];
		$mes = $_REQUEST["mes"];
		$anio = $_REQUEST["anio"];

		$respuesta = IngresarInformeOperacional($idEspecialidad,$diagnostico,$procedimiento,$tiempo_promedio,$numero_cirugias,$mes,$anio);
		if ($respuesta == 'M_1') {
			echo json_encode($respuesta);
			exit();
		}
		else
		{
			echo json_encode('M_0');
			exit();
		}
	}

	#TABLA INFORME OPERACIONAL
	if ($_REQUEST["acc"] == "TablaInfrOperacional") {
		$IdEspecialidad = $_REQUEST["IdEspecialidad"];
		$mes = $_REQUEST["mes"];
		$anio = $_REQUEST["anio"];
		$data = TablaInformeOperacional($IdEspecialidad,$mes,$anio);

		for ($i=0; $i < count($data); $i++) { 
			$tabla[$i] = array(
				    'DIAGNOSTICO' => $data[$i]['DIAGNOSTICO'],
					'PROCEDIMIENTO' => $data[$i]['PROCEDIMIENTO'],
					'TPSALA' => $data[$i]['TIEMPO'],
					'NCIRUGIAS' => $data[$i]['NROCIRUGIA']
				);
		}

		echo json_encode($tabla);
		exit();	
	}

	#MOSTRAR INGRESOS
	if ($_REQUEST["acc"] == "ListaInformeOperacionalxEspecialidad") {
		$idEspecialidad = $_REQUEST["idEspecialidad"];
		
		$primer_dia = Primer_Dia_Mes($_REQUEST["mes"],$_REQUEST["anio"]);
		$ultimo_dia = Ultima_Dia_Mes($_REQUEST["mes"],$_REQUEST["anio"]);

		$data = $listarInformeOperacionalM($idEspecialidad,$primer_dia,$ultimo_dia);

		if (count($data) == 0) {
			echo json_encode('M_0');
			exit();
		}
		else
		{
			for ($i=0; $i < count($data); $i++) { 
				$listas[$i] = array(
					'DIAGNOSTICO'	=> $data[$i][""],
					'DIAGNOSTICO'	=> $data[$i][""],
					'DIAGNOSTICO'	=> $data[$i][""],
					'DIAGNOSTICO'	=> $data[$i][""],
					);
			}

			echo json_encode($listas);
			exit();
		}
	}

	#INGRESAR OPERACIONES PROGRAMADAS, GRADO DE CUMPLIMIENTO, UTILIZACION DE SALA POR MES
	if ($_REQUEST["acc"] == "ingOperacionesProgramadas") {
		//echo "hola";exit();
		$OpeProgramadas = $_REQUEST["OpeProgramadas"];
		$OpeSuspendidas = $_REQUEST["OpeSuspendidas"];
		$OpeProgRealizadas = $_REQUEST["OpeProgRealizadas"];
		$CoberturaProgramada = $_REQUEST["CoberturaProgramada"];
		$Aniadidas = $_REQUEST["Aniadidas"];
		$Reoperados = $_REQUEST["Reoperados"];
		$TOR = $_REQUEST["TOR"];
		$HorasOferSala = $_REQUEST["HorasOferSala"];
		$HorasOperProgra = $_REQUEST["HorasOperProgra"];
		$HorasOpera = $_REQUEST["HorasOpera"];
		$HorasAnestesicas = $_REQUEST["HorasAnestesicas"];
		$HorasEfectivas = $_REQUEST["HorasEfectivas"];
		$NroMes = $_REQUEST["NroMes"];
		$anio = $_REQUEST["anio"];
		$Piso = $_REQUEST["Piso"];

		$data = IngresarOperacionesProgramadas($OpeProgramadas,$OpeSuspendidas,$OpeProgRealizadas,$CoberturaProgramada,$Aniadidas,$Reoperados,$TOR,$HorasOferSala,$HorasOperProgra,$HorasOpera,$HorasAnestesicas,$HorasEfectivas,$NroMes,$anio,$Piso);
		if ($data == 'M_1') {
			echo json_encode($data);
			exit();
		}
		else
		{
			echo json_encode('M_0');
			exit();
		}
	}

	#INSERTAR TIPO ANESTESIA
	if ($_REQUEST["acc"] == "InsertarTIpoAnestesiaMes") {
		$AgeneralBalanceada = $_REQUEST["AgeneralBalanceada"];
		$AgeneralInhalatoria = $_REQUEST["AgeneralInhalatoria"];
		$AgeneralEndovenosa = $_REQUEST["AgeneralEndovenosa"];
		$AperiduralContinua = $_REQUEST["AperiduralContinua"];
		$AperiduralSimple = $_REQUEST["AperiduralSimple"];
		$Araquidea = $_REQUEST["Araquidea"];
		$Alocal = $_REQUEST["Alocal"];
		$sedacion = $_REQUEST["sedacion"];
		$total = $_REQUEST["total"];
		$NroMes = $_REQUEST["NroMes"];
		$Anio = $_REQUEST["Anio"];
		$Piso = $_REQUEST["Piso"];
		$porc_AgeneralBalanceada = $_REQUEST["porc_AgeneralBalanceada"];
		$porc_AgeneralInhalatoria = $_REQUEST["porc_AgeneralInhalatoria"];
		$porc_AgeneralEndovenosa = $_REQUEST["porc_AgeneralEndovenosa"];
		$porc_AperdurialContinua = $_REQUEST["porc_AperdurialContinua"];
		$porc_AperdurialSimple = $_REQUEST["porc_AperdurialSimple"];
		$porc_Araquidea = $_REQUEST["porc_Araquidea"];
		$porc_Alocal = $_REQUEST["porc_Alocal"];
		$porc_Sedacion = $_REQUEST["porc_Sedacion"];

		$data = IngresarTipoAnestesia($AgeneralBalanceada, $AgeneralInhalatoria, $AgeneralEndovenosa, $AperiduralContinua, $AperiduralSimple, $Araquidea, $Alocal, $sedacion, $total, $NroMes, $Anio, $Piso, $porc_AgeneralBalanceada, $porc_AgeneralInhalatoria, $porc_AgeneralEndovenosa, $porc_AperdurialContinua, $porc_AperdurialSimple, $porc_Araquidea, $porc_Alocal, $porc_Sedacion);
		if ($data == 'M_1') {
			echo json_encode($data);
			exit();
		}
		else
		{
			echo json_encode('M_0');
			exit();
		}
	}

	if ($_REQUEST["acc"] == "InsertarProcedimientosInvasivos") {
		$totalAppiProgramadas = $_REQUEST["totalAppiProgramadas"];
		$totalAppiRealizada = $_REQUEST["totalAppiRealizada"];
		$coberturaAppiProgramada = $_REQUEST["coberturaAppiProgramada"];
		$totalAppiAñadida = $_REQUEST["totalAppiAñadida"];
		$totalAppiSuspendida = $_REQUEST["totalAppiSuspendida"];
		$totalHorasProcedimiento = $_REQUEST["totalHorasProcedimiento"];
		$totalHorasAnestesia = $_REQUEST["totalHorasAnestesia"];
		$totalHorasEfectivas = $_REQUEST["totalHorasEfectivas"];
		$NroMes = $_REQUEST["NroMes"];
		$Anio = $_REQUEST["Anio"];
		$Piso = $_REQUEST["Piso"];

		$data = InsertarProcedimientosInvasivos($totalAppiProgramadas, $totalAppiRealizada, $coberturaAppiProgramada, $totalAppiAñadida, $totalAppiSuspendida, $totalHorasProcedimiento, $totalHorasAnestesia, $totalHorasEfectivas, $NroMes, $Anio, $Piso);
		if ($data == 'M_1') {
			echo json_encode($data);
			exit();
		}
		else
		{
			echo json_encode('M_0');
			exit();
		}
	}

	if ($_REQUEST["acc"] == "InsertarConsultoriosAtenciones") {
		$consultasConsul1 = $_REQUEST["consultasConsul1"];
		$consultalConsul2 = $_REQUEST["consultalConsul2"];
		$interconsultas = $_REQUEST["interconsultas"];
		$total = $_REQUEST["total"];
		$NroMes = $_REQUEST["NroMes"];
		$Anio = $_REQUEST["Anio"];
		$Piso = $_REQUEST["Piso"];

		$data = InsertarConsultorioAtenciones($consultasConsul1, $consultalConsul2, $interconsultas, $total, $NroMes, $Anio, $Piso);

		if ($data == 'M_1') {
			echo json_encode($data);
			exit();
		}
		else
		{
			echo json_encode('M_0');
			exit();
		}
	}

	if ($_REQUEST["acc"] == "InsertarComplicacionesMuertes") {
		$ComplicacionesAnestesicas = $_REQUEST["ComplicacionesAnestesicas"];
		$MuertesSalasOperaciones = $_REQUEST["MuertesSalasOperaciones"];
		$MuertesUrpa = $_REQUEST["MuertesUrpa"];
		$NroMes = $_REQUEST["NroMes"];
		$Anio = $_REQUEST["Anio"];
		$Piso = $_REQUEST["Piso"];

		$data = InsertarComplicacionesMuertesM($ComplicacionesAnestesicas,$MuertesSalasOperaciones,$MuertesUrpa,$NroMes,$Anio,$Piso);

		if ($data == 'M_1') {
			echo json_encode($data);
			exit();
		}
		else
		{
			echo json_encode('M_0');
			exit();
		}

	}

	?>