<?php 
include('../../MVC_Modelo/FarmaciaM.php');
include('../../MVC_Modelo/EmergenciaM.php');
include('../../MVC_Modelo/SistemaM.php');
include('../../MVC_Modelo/ServiciosM.php');
include('../../MVC_Complemento/librerias/Funciones.php');
$ListarFechaServidor=RetornaFechaServidorSQL_C();
 $FechaServidor = $ListarFechaServidor[0][0];

	#REDIRECCION FARMACIA REPORTES
	if ($_REQUEST["acc"] == 'Reportes Farmacia') {
		include('../../MVC_Vista/Farmacia/reportes_farmacia.php');
	}
	
	#GRILLA CON STOCK 25-10-2018
	if ($_REQUEST["acc"] == "MostrarMedicamentosGrillaconStock") {
		$data = Mostrar_Lista_de_Medicamentos_con_Stock_M();
		for ($i=0; $i < count($data); $i++) { 
			$Medicamentos[$i] = array(
				'Codigo'	=> $data[$i]['Codigo'],
				'Nombre'			=> $data[$i]['Nombre'],
				'SaldoTotal'	=> number_format($data[$i]['SaldoTotal'],0)
				);
		}
		echo json_encode($Medicamentos);
		exit();
	}


	#GRILLA  STOCK POR CODIGO 25-10-2018
	if ($_REQUEST["acc"] == "MostrarStockporCodigo") {
		$Codigo=$_REQUEST["Codigo"];
		$data = Mostrar_Stock_Medicamento_por_Codigo($Codigo);
		for ($i=0; $i < count($data); $i++) {		
			$StockporProducto[$i] = array(
				'Descripcion'	=> $data[$i]['Descripcion'],
				'cantidad'		=> number_format($data[$i]['cantidad'],0),
				'precio'		=> 's/. '.number_format($data[$i]['precio'],2)
				);
		}
		echo json_encode($StockporProducto);
		exit();
	}




	#GRAFICO DE FARMACIA POR ALMACEN 25-10-2018
	if ($_REQUEST["acc"] == "Highchart_Farmacia_Almacenes") {
		$Codigo=$_REQUEST["Codigo"];
		$data = Mostrar_Stock_Medicamento_por_Codigo($Codigo);
		if($data != NULL)
		{
						$a=0;
						$cero='0';
						$v='"';
						$contenido.=" ";
						foreach($data as $item)
						{
						if($a==0)
						{$contenido.= "{";
						}else
						{
					    $contenido.=",{";
						}
						$contenido.=$v."name".$v.": ".$v.$item['Descripcion'].$v.",";
						$contenido.=$v."y".$v.": ".str_replace(",", "", number_format($item['cantidad'],0)).' ';
						$contenido.="}";
						$a++;
						}
						$contenido.="";
						
		}
		
		echo json_encode('['.$contenido.']');	
	}






	#REDIRECCIONANDO NUEVOS REPORTES FARMACIA
	if($_REQUEST["acc"] == "Reportes")
		{    
			 //$ServiciosEmergencia=ServiciosEmergencia_C();
			$AlmacenesFarmacia=Listado_Farmacias_M();
			$AniosFarmacia=Listar_Anios_Farmacia_M();
			 //$ListarTiposServicio= Sigesa_RP_TiposServicio_C();
			//$ListarServicios=Listar_Servicios_Farmacia_M();

			 include('../../MVC_Vista/Farmacia/VentaxDiariaFarmacia.php');
		}	

	#REDIRECCIONANDO KARDEX
	if($_REQUEST["acc"] == "Kardex")
		{    
			 //$ServiciosEmergencia=ServiciosEmergencia_C();
			$AlmacenesFarmacia=Listado_Farmacias_M();
			$AniosFarmacia=Listar_Anios_Farmacia_M();
			 //$ListarTiposServicio= Sigesa_RP_TiposServicio_C();
			 include('../../MVC_Vista/Farmacia/VentaxDiariaFarmacia2.php');
		}	

	if($_REQUEST["acc"] == "Ventas")
	{    
		 //$ServiciosEmergencia=ServiciosEmergencia_C();
	//	$AlmacenesFarmacia=Listado_Farmacias_M();
	//	$AniosFarmacia=Listar_Anios_Farmacia_M();
		 //$ListarTiposServicio= Sigesa_RP_TiposServicio_C();
		 include('../../MVC_Vista/Farmacia/Ventas_1/VentasFarmaciaV_1.php');
	}

	#REDIRECCIONAR BUSQUEDA DE EXPENDEDORES
		if ($_REQUEST["acc"] == "BuscadorxDocumento") 
	{
			include('../../MVC_Vista/Farmacia/BuscarPorDocumento/BuscarXDocumento_v2.php');
	}

	#OPERACIONES
	#===========
	

	#LISTADO DE FARMACIAS
	if ($_REQUEST["acc"] == 'ListarFarmacias') 
	{
		$data = Listado_Farmacias_M();

		for ($i=0; $i < count($data); $i++) { 
			$farmacias[$i] = array(
				'id'	=> 	$data[$i]["IDALMACEN"],
				'text'	=>	$data[$i]["NOMBRE"]
				);
		}

		echo json_encode($farmacias);
		exit();
	}

	#CONSUMO TOTAL - MEDICAMENTOS,INSUMOS
	if ($_REQUEST["acc"] == "MedicamentosInsumos") 
	{
		$fechaini 	= $_REQUEST["fechaini"];
		$fechafin 	= $_REQUEST["fechafin"];
		$idalmacen 	= $_REQUEST["idalmacen"];

		$medicamentos_temp 	= ConsumoNormalMedicamentos_M($fechaini,$fechafin,$idalmacen);
		$insumos_temp 		= ConsumoNormalInsumos_M($fechaini,$fechafin,$idalmacen);

		#GENERANDO LISTA DE MEDICAMENTOS
		for ($i=0; $i < count($medicamentos_temp); $i++) { 
			$lista_medicamentos[$i] = array(
				'CODIGO'	=> 	$medicamentos_temp[$i]['CODIGO'],
				'NOMBRE'	=> 	$medicamentos_temp[$i]['NOMBRE'],
				'UNIDAD'	=> 	$medicamentos_temp[$i]['UNIDAD']
				);
		}

		$lista_medicamentos = array_unique2($lista_medicamentos);

		#GENERANDO LISTAS DE INSUMOS
		for ($i=0; $i < count($insumos_temp); $i++) { 
			$lista_insumos[$i] = array(
				'CODIGO'	=> 	$insumos_temp[$i]['CODIGO'],
				'NOMBRE'	=> 	$insumos_temp[$i]['NOMBRE'],
				'UNIDAD'	=> 	$insumos_temp[$i]['UNIDAD']
				);
		}

		$lista_insumos = array_unique2($lista_insumos);

		/*echo '<pre>';
		print_r($lista_medicamentos);
		echo '</pre>';
		exit();*/


		#GENERANDO STOCK PARA MEDICAMENTOS
		for ($i=0; $i < count($lista_medicamentos); $i++) { 
			for ($j=0; $j < count($medicamentos_temp); $j++) { 
				if ($lista_medicamentos[$i]["CODIGO"] == $medicamentos_temp[$j]["CODIGO"]) {
					$stock_temporal = $medicamentos_temp[$j]["STOCK"];
				}
			}

			$listas_stocks_medicamentos[$i] = round($stock_temporal);
		}

		#GENERANDO STOCK PARA INSUMOS
		for ($i=0; $i < count($lista_insumos); $i++) { 
			for ($j=0; $j < count($insumos_temp); $j++) { 
				if ($lista_insumos[$i]["CODIGO"] == $insumos_temp[$j]["CODIGO"]) {
					$stock_temporal = $insumos_temp[$j]["STOCK"];
				}
			}

			$listas_stocks_insumos[$i] = round($stock_temporal);
		}

		#GENERANDO CANTIDADES PARA INSUMOS
		$cantidad_temporal = 0;
		for ($i=0; $i < count($lista_insumos); $i++) { 
			for ($j=0; $j < count($insumos_temp); $j++) { 
				if ($lista_insumos[$i]["CODIGO"] == $insumos_temp[$j]["CODIGO"]) {
					$cantidad_temporal = $insumos_temp[$j]["CANTIDAD"] + $cantidad_temporal;
				}
			}

			$listas_cantidades_insumos[$i] = $cantidad_temporal;
			$cantidad_temporal = 0;
		}

		#GENERANDO CANTIDADES PARA MEDICAMENTOS
		$cantidad_temporal = 0;
		for ($i=0; $i < count($lista_medicamentos); $i++) { 
			for ($j=0; $j < count($medicamentos_temp); $j++) { 
				if ($lista_medicamentos[$i]["CODIGO"] == $medicamentos_temp[$j]["CODIGO"]) {
					$cantidad_temporal = $medicamentos_temp[$j]["CANTIDAD"] + $cantidad_temporal;
				}
			}

			$listas_cantidades_medicamentos[$i] = $cantidad_temporal;
			$cantidad_temporal = 0;
		}


		#MEDICAMENTOS
		for ($i=0; $i < count($lista_medicamentos); $i++) { 
			$medicamentos[$i] = array(
				'CODIGO'		=> $lista_medicamentos[$i]['CODIGO'],
				'DESCRIPCION'	=> $lista_medicamentos[$i]['NOMBRE'],
				'UMED'			=> $lista_medicamentos[$i]['UNIDAD'],
				'CONSUMO'		=> $listas_cantidades_medicamentos[$i],
				'STOCK'			=> $listas_stocks_medicamentos[$i]
				);
		}

		#INSUMOS
		for ($i=0; $i < count($lista_insumos); $i++) { 
			$insumos[$i] = array(
				'CODIGO'		=> $lista_insumos[$i]['CODIGO'],
				'DESCRIPCION'	=> $lista_insumos[$i]['NOMBRE'],
				'UMED'			=> $lista_insumos[$i]['UNIDAD'],
				'CONSUMO'		=> $listas_cantidades_insumos[$i],
				'STOCK'			=> $listas_stocks_insumos[$i]
				);
		}

		#IMPRIMIENDO RESULTADO
		echo json_encode(
				array(
					'MEDICAMENTOS' 	=> $medicamentos,
					'INSUMOS'		=> $insumos
				)
			);
		exit();
	}

	#FUNCIONES
	function array_unique2($array) {
	$container = array();
	$i = 0;
	
	foreach ($array as $a=>$b)
		if (!in_array($b,$container)){
			$container[$i]=$b;
			$i++;
		}
			
	return $container;
	}

	#BUSCAR MOVIMIENTOS DE FARMACIA X PACIENTE 
	
			if($_REQUEST["acc"] == "Buscar_Farmacia_Pacientes")
		{    
		     /* Mostrar todos los pacientes de Farmacia */
			 $HistoriaClinica=$_REQUEST["HistoriaClinica"];
			 $Dni=$_REQUEST["Dni"];
			 $ApellidoPaterno=$_REQUEST["ApellidoPaterno"];

			 if(!empty($HistoriaClinica))
				{
						$variable=$HistoriaClinica;
						$busqueda='1';
				}
			if(!empty($Dni))
				{
						$variable=$Dni;
						$busqueda='2';
				}
			 if(!empty($ApellidoPaterno))
				{
						$variable=$ApellidoPaterno;
						$busqueda='3';
				}
				
			if(empty($ApellidoPaterno) and  empty($HistoriaClinica) and  empty($Dni))
				{
						$busqueda='4';
			 	}	
				
			 $FechaInicio=sqlfecha($_REQUEST["FechaInicio"]);
			 $FechaFin=sqlfecha($_REQUEST["FechaFin"]);
		
				   $ListarPacientesFarmacia= Buscar_Farmacia_Pacientes_C($variable,$FechaInicio,$FechaFin,$busqueda);
		
				 
			 include('../../MVC_Vista/Farmacia/Listar_farmacia_pacientes.php');
		}

	#MOVIMIENTOS DETALLADOS POR PACIENTE
	if($_REQUEST["acc"] == "DetalleFarmaciaPacientes")
		{    
			 $IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];	    
			 $ListarMovimientoxPaciente=SigesaFarmaciaListarMovimientoxPaciente_C($IdCuentaAtencion);
			   
			 include('../../MVC_Vista/Farmacia/Lista_farmacia_bienes_x_paciente.php');
			 
		}	

	
	#SIGESA 2016 ESTADO DE CUENTA
if ($_REQUEST["acc"] == "Medicamentos") {
	$dato = $_REQUEST["dato"];

	
			$respuesta = SigesaBuscarMedicamento($dato);
			if (!$respuesta) {
				echo "R_1";
			}
			else
			{	
				$data_general = array(
					'NOMBRE' 			=> $respuesta[0]["Nombre"],
					);
				echo json_encode($data_general);
				exit();				
			}
			
	}
if($_REQUEST["acc"] == "VentasFarmaciaAgregar") // MOSTRAR: OrdenesLaboratorio
{  
  
 //$LsitarPuntosCargaSeleccionar=FactPuntosCargaSeleccionarTodos_C();
  include('../../MVC_Vista/Farmcia/Ventas/VentasFarmaciaAgregarV.php');

}

if($_REQUEST["acc"] == "ConsumoenelServicioAgregar") // MOSTRAR: OrdenesLaboratorio
{  
  
 //$LsitarPuntosCargaSeleccionar=FactPuntosCargaSeleccionarTodos_C();
  include('../../MVC_Vista/Farmacia/Ventas/VentasFarmaciaAgregarV.php');

}

if($_GET["acc"] == "BuscarCPTCodigo") // BUSCAR: Autocompletado Busca Por Apellidos 
{
              
	      
		    // $IdPuntoCarga=$_REQUEST["IdPuntoCarga"];
		     $idFuenteFinanciamiento=$_REQUEST["idFuenteFinanciamiento"];
		   
		     $Filtro =strtoupper($_REQUEST["q"]);
          
                // @TipoServicioOfrecido 0->solo Insumos,      1->solo CPT,        2->ambos
		 
	    
		  //$ListarAutocompletar =SigesFactCatalogoServiciosHospFiltraPorPuntoCargaTipoFinanciamientoxCodigo_C($IdPuntoCarga,$idFuenteFinanciamiento,1,$Filtro,1);
		     $ListarAutocompletar =SigesFactCatalogoBienesInsumosHospFiltraPorPuntoCargaTipoFinanciamientoxCodigo_C($idFuenteFinanciamiento,1,$Filtro);
		
		 if($ListarAutocompletar!=NULL)
		{
			foreach ($ListarAutocompletar as $item)
			{
 				$IdProducto = $item["IdProducto"];	
				$Codigo = $item["Codigo"];	
				$Nombre = $item["Nombre"];	
				$PrecioUnitario = number_format($item["PrecioUnitario"], 2, '.', '');	
				$Activo = $item["Activo"];	
				//$idPuntoCarga = $item["idPuntoCarga"];	
				//$SeUsaSinPrecio = $item["SeUsaSinPrecio"];	
				//$NombreProducto = $item["NombreProducto"];
			 
				$fintrado =$item["Codigo"].' '.$item["Nombre"];	
				

           echo "$fintrado|$Codigo|$Nombre|$PrecioUnitario|$IdProducto\n";
		        //  0          1      2          3             5
		
				
			}

		} 

}
if($_GET["acc"] == "BuscarCPTNombre") // BUSCAR: Autocompletado Busca Por Apellidos 
{
            	      
		    // $IdPuntoCarga=$_REQUEST["IdPuntoCarga"];
		     $idFuenteFinanciamiento=$_REQUEST["idFuenteFinanciamiento"];
		     $Filtro =strtoupper($_REQUEST["q"]);
             // @TipoServicioOfrecido 0->solo Insumos,      1->solo CPT,        2->ambos
		 
	     $ListarAutocompletar =FactCatalogoBienesInsumosHospFiltraPorPuntoCargaTipoFinanciamiento_C($idFuenteFinanciamiento,2,$Filtro);
		
		 if($ListarAutocompletar!=NULL)
		{
			foreach ($ListarAutocompletar as $item)
			{
 				$IdProducto = $item["IdProducto"];	
				$Codigo = $item["Codigo"];	
				$Nombre = $item["Nombre"];	
				$PrecioUnitario = number_format($item["PrecioUnitario"], 2, '.', '');		
				$Activo = $item["Activo"];	
				$idPuntoCarga = $item["idPuntoCarga"];	
				$SeUsaSinPrecio = $item["SeUsaSinPrecio"];	
				$NombreProducto = $item["NombreProducto"];
				
				$fintrado =$item["Codigo"].' '.$item["Nombre"];	
				

           echo "$fintrado|$Codigo|$Nombre|$PrecioUnitario|$IdProducto\n";
		        //  0          1      2          3             5
		
				
			}
		} 

}

if($_GET["acc"] == "ConsumoCuentasdeAtencion") // IMPRIMIR: 
{    
     
     $valor=$_REQUEST["valorbuscar"];
	 $pivot=$_REQUEST["pivotbuscar"];
	 $IdEmpleado=$_REQUEST["IdEmpleado"];
	 
	  if($_REQUEST["valorbuscar"]==NULL){
	   $ListarCuentasAtencion=Cosnumo_Servicios_Cuentas_C(0,1);
		  }else{
    	$ListarCuentasAtencion=Cosnumo_Servicios_Cuentas_C($valor,$pivot);		  
			  }
	 
	 
  include('../../MVC_Vista/Farmacia/Ventas/ConsumoEnFarmaciaCuentasV.php');

	}
 if($_REQUEST["acc"] == "FacturacionBuscarCuentas") // MOSTRAR: Buscar Cuentas
 
{  
  
  
     $IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];
     $TodasLasCuentas=0;// $_REQUEST["TodasLasCuentas"];
	 $valorbuscar=$_REQUEST["valorbuscar"];
	 $pivotbuscar=$_REQUEST["pivotbuscar"];
	 $IdEmpleado=$_REQUEST["IdEmpleado"];
     
	 $AbirCuenta=$_REQUEST["AbirCuenta"];
	 if($AbirCuenta=="SI"){
		   FacturacionCuentasAtencionAbrir_C($IdCuentaAtencion,$IdEmpleado);
		 }
	 
	 
    $LsitarCuentas=SigesFacturacionCuentasAtencionxIdCuentaAtencion_C($TodasLasCuentas,$IdCuentaAtencion);
  
 //include('../../MVC_Vista/Facturacion/ConsumoenelServicioTraerDatosxCuentaV.php');
 $Mensaje='1';
 include('../../MVC_Vista/Farmacia/Ventas/VentasFarmaciaAgregarV.php');
  
  if($LsitarCuentas!=NULL){
 echo "<script languaje='javascript'>document.getElementById('BuscarCPT').focus();BuscarCuentas();</script>"; 

  }else{
	 echo "<script languaje='javascript'>document.getElementById('NroHistoriaClinica').focus();BuscarCuentas();</script>";   
  }

}

#BUSCAR MOVIMIENTOS DE FARMACIA X PACIENTE 
	
			if($_REQUEST["acc"] == "BuscarDocumento")
		{    
		     /* Mostrar todos los pacientes de Farmacia */
			 $NroDocumento=$_REQUEST["NroDocumento"];
			 $variable=$_REQUEST["Variable"];


			   $ListarDocumentos= BuscarDocumento_C($NroDocumento,$variable);
		
				 
			 include('../../MVC_Vista/Farmacia/BuscarPorDocumento/ListarDocumentos.php');
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			if($_REQUEST["acc"] == "BuscarDocumentos")
		{    
		     /* Mostrar todos los pacientes de Farmacia */
			 $NroDocumento=$_REQUEST["NroDocumento"];

			   $ListarDocumentos= BuscarDocumentos_C($NroDocumento);
		
				 
			 include('../../MVC_Vista/Farmacia/BuscarPorDocumento/ListarDocumentos.php');
		}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	if($_REQUEST["acc"] == "BuscarMovimiento")
		{    
		     /* Mostrar todos los pacientes de Farmacia */
			 $NroMovimiento=$_REQUEST["NroMovimiento"];
			 $variable=$_REQUEST["Variable"];

			   $ListarDocumentos= BuscarMovimiento_C($NroMovimiento,$variable);
		
				 
			 include('../../MVC_Vista/Farmacia/BuscarPorDocumento/ListarDocumentos.php');
		}

if($_REQUEST["acc"] == "ReporteConsumoxServicioFarmacia")
		{    
		
		$FechaInicio=gfechahora($_REQUEST["FechaInicio"]);
		$FechaFinal=gfechahora($_REQUEST["FechaFinal"]);
		$IdServicio=$_REQUEST["ServiciosFarmacia"];
		$IdTipoServicio=$_REQUEST["TiposServicioFarmacia"];
		$IdAlmacenFarmacia=$_REQUEST["AlmacenFarmacia"];
			
				 
				 
					 
					 
				 	
			
             
			
     	    
	    
			
			  echo '
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/imprimir.css"/>
<title></title>
 
<style type="text/css">
.botonExcel{cursor:pointer;}

@media all {
   div.saltopagina{
      display: none;
   }
}
   
@media print{
   div.saltopagina{
      display:block;
      page-break-before:always;
   }
} 
</style>
<style type="text/css" media="print">
.nover {display:none}
</style>
</head>
<body  onLoad="window.print();">
 
 

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="4" align="center"><FONT FACE="Calibri" size="+1"><strong>Consumo por Servicios- Emergencia / Hospitalización <br>
	
	Fecha: '.$_REQUEST["FechaInicio"].' Al '.$_REQUEST["FechaFinal"].'
        
    </strong>    </FONT></td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  ';       
  $CabeceraFarmacia= SIGESA_Farmacia_ReporteConsuumoxServcio_C($FechaInicio,$FechaFinal,$IdServicio,$IdTipoServicio);
			
	  foreach($CabeceraFarmacia as $item){
		  
 
   echo '<tr>
    <td colspan="4"><FONT FACE="Calibri" >Servicio: '.$item['Servicios'].'-'.$item['TiposServicio'].':</FONT></td>
  </tr>
  
  <tr>
    <td width="4">&nbsp;</td>
    <td width="57"  ><FONT FACE="Calibri"  ><strong>Codigo</strong></FONT></td>
    <td width="242" align="center"><FONT FACE="Calibri"   ><strong>Descripcion</strong></FONT></td>
    <td width="46" align="center"><FONT FACE="Calibri"   ><strong>Cant</strong>.</FONT></td>
  </tr>
  ';
 $DetalleFarmacia =SIGESA_Farmacia_ReporteConsuumoxServicioDetalle_C($FechaInicio,$FechaFinal,$item['IdServicio'],$IdAlmacenFarmacia);	
				 foreach($DetalleFarmacia as $item1){
                              
					 
 		
echo'  <tr>
    <td>&nbsp;</td>
    <td    valign="top">'.$item1['Codigo'].'</td>
    <td   valign="top"><FONT FACE="Calibri" >'.$item1['Nombre'].' </FONT></td>
    <td  align="center" valign="top">'.$item1['Cantidad'].'</td>
  </tr>
 	';
	 }	 	
			}
  echo'
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><font face="Calibri" >Terminal: '.Terminal().'</font></td>
    <td align="right"><font face="Calibri" >Fecha Imp: '. vfechahora($FechaServidor).'</font></td>
  </tr>
</table>
 ';
		}	


////////////////////////////////////////////////////////////////////TOÑOOOOOOOOOOOOOOOO////////////////////////////////////////////////////////////////

		if ($_REQUEST["acc"] == 'MostrarTiposServiciosCombo') {
		$data = MostrarTiposServicios();

		for ($i=0; $i < count($data); $i++) { 
			$departamentos[$i] = array(
				'id'	=>	$data[$i]["IDTIPOSERVICIO"],
				'text'	=>	$data[$i]["DESCRIPCION"]
				);
		}
		echo json_encode($departamentos);
		exit();
	}

	if ($_REQUEST["acc"] == 'MostrarServiciosCombo') {
		$idDepartamento = $_REQUEST["idDepartamento"];
		$data = MostrarServicios($idDepartamento);

		for ($i=0; $i < count($data); $i++) { 
			$provinvias[$i] = array(
				'id'	=>	$data[$i]["IDSERVICIO"],
				'text'	=>	$data[$i]["NOMBRE"]
				);
		}
		echo json_encode($provinvias);
		exit();
	}

	if ($_REQUEST["acc"] == 'MostrarFarmaciasCombo') {
		$data = Listado_Farmacias_M();

		for ($i=0; $i < count($data); $i++) { 
			$provinvias[$i] = array(
				'id'	=>	$data[$i]["IDALMACEN"],
				'text'	=>	$data[$i]["NOMBRE"]
				);
		}
		echo json_encode($provinvias);
		exit();
	}


if ($_REQUEST["acc"] == 'ReporteIndicadorDosisUnitaria') { //POR MAHALI	 
	  $FechaInicio=gfechahora($_REQUEST["FechaInicio"]);
	  $FechaFinal=gfechahora($_REQUEST["FechaFinal"]);
	  $IdServicio=$_REQUEST["ServiciosFarmacia"];
	  $IdAlmacenFarmacia=$_REQUEST["AlmacenFarmacia"];
	
	$IdTipoServicio=$_REQUEST["TiposServicioFarmacia"];		
	$resultados=SIGESA_Farmacia_ReporteIndicadorDosisUnitaria_M($FechaInicio,$FechaFinal,$IdServicio,$IdAlmacenFarmacia,$IdTipoServicio);
	include('../../MVC_Vista/Farmacia/Reporte_IndicadorDosisUnitariaExcel.php');
	 
}








	function RetornaFechaServidorSQL_C(){	
		return RetornaFechaServidorSQL_M(); 
		}
		
	function Buscar_Farmacia_Pacientes_C($variable,$FechaInicio,$FechaFin,$busqueda)
			{
				return Buscar_Farmacia_Pacientes_M($variable,$FechaInicio,$FechaFin,$busqueda);
			}
	function SigesaFarmaciaListarMovimientoxPaciente_C($IdCuentaAtencion)
			{
				return 	SigesaFarmaciaListarMovimientoxPaciente_M($IdCuentaAtencion);
			}
	function SigesFactCatalogoBienesInsumosHospFiltraPorPuntoCargaTipoFinanciamientoxCodigo_C($idTipoFinanciamiento,$idFiltroTipo,$Filtro)
			{
			    return  SigesFactCatalogoBienesInsumosHospFiltraPorPuntoCargaTipoFinanciamientoxCodigo_M($idTipoFinanciamiento,$idFiltroTipo,$Filtro);
			}				
	 function FactCatalogoBienesInsumosHospFiltraPorPuntoCargaTipoFinanciamiento_C($idTipoFinanciamiento,$idFiltroTipo,$Filtro){
		 return   FactCatalogoBienesInsumosHospFiltraPorPuntoCargaTipoFinanciamiento_M($idTipoFinanciamiento,$idFiltroTipo,$Filtro);
	 }
	 	function Cosnumo_Servicios_Cuentas_C($valor,$pivot){
		return Cosnumo_Servicios_Cuentas_M($valor,$pivot);
	}	
	function SigesFacturacionCuentasAtencionxIdCuentaAtencion_C($TodasLasCuentas,$IdCuentaAtencion){
	return SigesFacturacionCuentasAtencionxIdCuentaAtencion_M($TodasLasCuentas,$IdCuentaAtencion);
	}
		function BuscarDocumento_C($NroDocumento,$variable)
			{
			    return  BuscarDocumento_M($NroDocumento,$variable);
			}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function BuscarDocumentos_C($NroDocumento)
			{
			    return  BuscarDocumentos_M($NroDocumento);
			}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function BuscarMovimiento_C($NroMovimiento,$variable)
			{
			    return  BuscarMovimiento_M($NroMovimiento,$variable);
			}	
function SIGESA_Farmacia_ReporteConsuumoxServcio_C($FechaInicio,$FechaFinal,$IdServicio,$IdTipoServicio){	
return   SIGESA_Farmacia_ReporteConsuumoxServcio_M($FechaInicio,$FechaFinal,$IdServicio,$IdTipoServicio); 		 
		}
function SIGESA_Farmacia_ReporteConsuumoxServicioDetalle_C($FechaInicio,$FechaFinal,$IdServicio,$IdAlmacenFarmacia){
return SIGESA_Farmacia_ReporteConsuumoxServicioDetalle_M($FechaInicio,$FechaFinal,$IdServicio,$IdAlmacenFarmacia);
 		}	
 ?>