<?php
//include('../../MVC_Modelo/SistemaM.php');
//include('../../MVC_Modelo/DatosInstitucionesM.php');
include('../../MVC_Modelo/LaboratorioM.php');
include('../../MVC_Modelo/HistoriaClinicaM.php');
//include('../../MVC_Modelo/FactConfig.php');
include('../../MVC_Complemento/librerias/Funciones.php');
include('../../MVC_Modelo/CuposAdicionalesM.php');

/***************************/
/*** Variables Globales ***/
/**************************/

   

/****************/
/*** ACCIONES ***/
/****************/
 
//VISTAS


if($_REQUEST["acc"] == "Retornar_HoraFin")
{   
		$IdProgramacion=$_REQUEST["IdProgramacion"];									 
		$resultados=BuscaUltimaCitaporServicio_M($IdProgramacion);								 
		if($resultados!=NULL)
		{
			for ($i=0; $i < count($resultados); $i++) 
			{	
			$HoraFinal=$resultados[$i]["HoraFinal"];
			}
		}
		$data = EditarHoraFinal_M($IdProgramacion, $HoraFinal);
		echo json_encode($HoraFinal);
		exit();	
}	

if ($_REQUEST["acc"]=="GenerarFlujoProgramacion"){
		$contenido=''; 
		$a=0;
		$id = $_REQUEST["id"];
		$fecha = $_REQUEST["fecha"];
		$data = BuscarProgramacion_M($id, $fecha);
		if($data!=null){	
		for ($i=0; $i < count($data); $i++)
		{			
		$v='"';
		$fin='end';
		$colores = array('rgb(249, 231, 159)','rgb(213, 245, 227)','rgb(250, 219, 216)','rgb(214, 234, 248)','rgb(232, 218, 239)','rgb(212, 239, 223)');
		if($a==0)
		{$contenido.= "{";
		}else
		{
		$contenido.=",{";
		}
		$contenido.= $v."start".$v.":".$v.substr($data[$i]["Fecha"],0,10)."T".$data[$i]["HoraInicio"].$v.",";
		$contenido.= $v."".$fin."".$v.":".$v.substr($data[$i]["Fecha"],0,10)."T".$data[$i]["HoraFin"].$v.",";
		$contenido.=$v."title".$v.":".$v.$data[$i]["Medico"].$v.",";
		$contenido.=$v."color".$v.":".$v.$colores[array_rand($colores)].$v;
		$contenido.="}";
		$a++;
		}
		}
		else 
		{
		$contenido.= '{"start": "2017-12-12T02:00","end": "2017-12-12T12:00","title": "","color": "rgb(149, 203, 255)"}';	 
		}
	
		//echo json_encode('['.$contenido.']');
		//exit();
		echo json_encode('['.$contenido.']');
		exit();
}


if($_REQUEST["acc"] == "ColocarHoraInicio")
{   
	$hora=$_REQUEST["hora"];
	$minutos=$_REQUEST["minutos"];
	$tiempo=$_REQUEST["tiempo"];
	$cantidad_cupos=$_REQUEST["cantidad_cupos"];
	Calcular_HoraInicio($hora,$minutos,$tiempo,$cantidad_cupos);	
}



if($_REQUEST["acc"] == "Cupos Adicionales") // MOSTRAR: OrdenesLaboratorio
{  
 	include("../../MVC_Vista/Cupos adicionales/CuposAdicionalesV.php");
} 			



//Vista de Apertura de Cuentas
if($_REQUEST["acc"] == "Aperturar_Cuentas")
{  	
 	include("../../MVC_Vista/Sistema/Aperturar_Cuentas.php");
} 


//ACCIONES

if ($_REQUEST["acc"]=="ListarMedicos"){	
		$data = ListarMedicos_M();

		for ($i=0; $i < count($data); $i++) { 
			$medico[$i] = array(
				'IDMEDICO'	=> 	$data[$i]["IdMedico"],
				'MEDICO'	=>	strtoupper($data[$i]["ApellidoPaterno"]." ".$data[$i]["ApellidoMaterno"].", ".$data[$i]["nombres"])
				);
		}
		echo json_encode($medico);
		exit();
}

if ($_REQUEST["acc"]=="ListarServicio"){	
		$data = ListarServicio_M();

		for ($i=0; $i < count($data); $i++) { 
			$servicio[$i] = array(
				'IDSERVICIO'	=> 	$data[$i]["IdServicio"],
				'SERVICIO'	=>	$data[$i]["Servicio"]
				);
		}
		echo json_encode($servicio);
		exit();
}	

if ($_REQUEST["acc"]=="BuscarProgramacion"){

		$id = $_REQUEST["id"];
	
		$fecha = $_REQUEST["fecha"];

		$data = BuscarProgramacion_M($id, $fecha);

		if($data!=null){
		for ($i=0; $i < count($data); $i++) { 
			$programacion[$i] = array(
				'MEDICO'	=> 	$data[$i]["Medico"],
				'IDPROG'	=> 	$data[$i]["IdProgramacion"],
				'FECHAPROG'	=>	Devolver_Fecha_Formato($data[$i]["Fecha"]),
				'HI'	=>	$data[$i]["HoraInicio"],
				'HF'	=>	$data[$i]["HoraFin"],
				'TPROMEDIO'	=>	$data[$i]["TiempoPromedioAtencion"]
				);
		}
		echo json_encode($programacion);
		exit();
		}
		else {
			echo json_encode(array('dato' => 'bad'));
			exit();
		}
}	

if ($_REQUEST["acc"]=="BuscarProgramacionxMedico"){

		$id = $_REQUEST["id"];
	
		$fecha = $_REQUEST["fecha"];

		$data = BuscarProgramacionxMedico_M($id, $fecha);

		if($data!=null){
		for ($i=0; $i < count($data); $i++) { 
			$programacion[$i] = array(
				'IDPROG'	=> 	$data[$i]["IdProgramacion"],
				'CONSULTORIO' => strtoupper($data[$i]["nombre"]),
				'FECHAPROG'	=>	$data[$i]["Fecha"],
				'HI'	=>	$data[$i]["HoraInicio"],
				'HF'	=>	$data[$i]["HoraFin"],
				'TPROMEDIO'	=>	$data[$i]["TiempoPromedioAtencion"]
				);
		}
		echo json_encode($programacion);
		exit();
		}
		else {
			echo json_encode(array('dato' => 'bad'));
			exit();
		}
}	


if ($_REQUEST["acc"]=="EditarHoraInicial"){
		$id = $_REQUEST["id"];
		$nhi = $_REQUEST["nhi"];
		EditarHoraInicial_M($id, $nhi);
		exit();
}	

if ($_REQUEST["acc"]=="EditarHoraFinal"){
		$id = $_REQUEST["id"];
		$nhf = $_REQUEST["nhf"];
		EditarHoraFinal_M($id, $nhf);
		exit();
}	



if ($_REQUEST["acc"]=="EditarHorarioCitas"){
		$id = $_REQUEST["id"];
		$nhi = $_REQUEST["nhi"];
		$tiempoPromedio = $_REQUEST["tiempoPromedio"];
		$cantidad_cupos = $_REQUEST["cantidad_cupos"];
		$data_citas = Buscar_Citas_por_IdProgramacion_M($id);
		for ($i=0; $i < count($data_citas); $i++) 
		{ 
		$HoraInicio=$data_citas[$i]["HoraInicio"];
		$HoraFin=$data_citas[$i]["HoraFin"];
		$IdProgramacion=$data_citas[$i]["IdProgramacion"];
		$IdAtencion=$data_citas[$i]["IdAtencion"];
		$HoraInicio_Mod=Calcular_Hora_minutos($HoraInicio,$cantidad_cupos,$tiempoPromedio);
		$HoraFin_Mod=Calcular_Hora_minutos($HoraFin,$cantidad_cupos,$tiempoPromedio);
		$EditarHoraInicial_Atencion=Editar_HoraIngreso_Atencion($HoraInicio_Mod,$IdAtencion);
		$Editar_HoraInicio_HoraFinal=Editar_HoraInicio_HoraFinal_IdProgramacion_M($HoraInicio, $HoraFin,$IdProgramacion,$HoraInicio_Mod,$HoraFin_Mod);
		}
		exit();
}	

?>