﻿<?php

//ini_set('error_reporting',0);//para xamp
include('../../MVC_Modelo/SistemaM.php');
include('../../MVC_Modelo/ImagenologiaM.php');
include('../../MVC_Modelo/AntimicrobianosM.php');
include('../../MVC_Modelo/HistoriaClinicaM.php');
include('../../MVC_Modelo/FactConfigM.php');
include('../../MVC_Complemento/librerias/Funciones.php');

/***************************/
/*** Variables Globales ***/
/**************************/
 $ListarFechaServidor=RetornaFechaServidorSQL_M();
 $FechaServidor = $ListarFechaServidor[0][0];

function ObtenerIdPuntoCarga($IdListItem){
	if($IdListItem=='1317'){   
		$IdPuntoCarga='20';//20	Ecografía General  

	}else if($IdListItem=='1318'){
		$IdPuntoCarga='21';//21	Rayos X 

	}else if($IdListItem=='1319'){
		$IdPuntoCarga='22';//22	Tomografía 
		
	}else if($IdListItem=='1320'){
		$IdPuntoCarga='23';//23	Pro.Ecografia.01 (CE) (Ecografía Obstétrica)
	}

	return $IdPuntoCarga;
}


   
if($_REQUEST["acc"] == "ImagEcografiaG" || $_REQUEST["acc"] == "ImagRayosX" || $_REQUEST["acc"] == "ImagTomografia" || $_REQUEST["acc"] == "ImagEcografiaO") // MOSTRAR: Consulta_Resultados
{  
 	$IdListItem=$_REQUEST["IdListItem"];
	$IdPuntoCarga=ObtenerIdPuntoCarga($IdListItem); 

  	$LsitarPuntosCargaSeleccionar= FactPuntosCargaSeleccionarPorId_M($IdPuntoCarga);
  	$DescripcionPC=$LsitarPuntosCargaSeleccionar[0]["Descripcion"];
  	include('../../MVC_Vista/Imagenologia/consulta_Resultados.php'); 
}

if ($_REQUEST["acc"] == "MostrarMascaraBody") {

	$buscar_fi= gfechahora($_REQUEST["buscar_fi"]);
	$buscar_ff=gfechahora($_REQUEST["buscar_ff"]);

	$IdListItem=$_REQUEST["IdListItem"];
	$IdPuntoCarga=ObtenerIdPuntoCarga($IdListItem); 

	$data_mascara = SigesaLaboratorioMostrarMascara_M($buscar_fi,$buscar_ff,$IdPuntoCarga);

	if (!$data_mascara) {
		# SI NO EXISTEN DATOS...
	}
	else
	{
		for ($i=0; $i < count($data_mascara); $i++) { 
			
			$fecha = time() - strtotime($data_mascara[$i]['FNACIMIENTO']);
			$edad=CalcularEdadTonno($data_mascara[$i]['FNACIMIENTO']);
			$FechaNacimiento=gfechatoño($data_mascara[$i]['FNACIMIENTO']);
			$FechaRegistro=gfechatoño($data_mascara[$i]['FREGISTRO']);

			$data[$i] = array(
				'IDMOVIMIENTO' 	=> $data_mascara[$i]['IDMOVIMIENTO'],
				'NCUENTA' 		=> $data_mascara[$i]['NCUENTA'],
				'NHISCLI' 		=> $data_mascara[$i]['NHISCLI'],
				'PACIENTE' 		=> $data_mascara[$i]['PACIENTE'],
				'FREGISTRO' 	=> $FechaRegistro,
				'ORDENPUEBRA' 	=> $data_mascara[$i]['ORDENPUEBRA'],
				'FNACIMIENTO' 	=> $FechaNacimiento,
				'SEXO' 			=> $data_mascara[$i]['SEXO'],
				'EDAD'			=> $edad,
				'IDPACIENTE'	=> $data_mascara[$i]['IDPACIENTE'],
				'ORDEN'			=> $data_mascara[$i]['ORDEN'],
				'CAMA' 			=> $data_mascara[$i]['CAMA']
				);
		}

		echo json_encode($data);
		exit();
	}
}


if ($_REQUEST["acc"] == "DetalleOrdenxIdOrden") {

	$IdListItem=$_REQUEST["IdListItem"];
	$IdPuntoCarga=ObtenerIdPuntoCarga($IdListItem); 

	$idOrden = $_REQUEST["idorden"];

	$ordenes_general = SigesaLaboratorioMostrarOrdenDetalladaxIdOrden_M($idOrden,$IdPuntoCarga);
	$ordenes_resultado = SigesaLaboratorioMMostrarOrdenConResultado_M($idOrden);
	
	for ($i=0; $i < count($ordenes_general); $i++) { 
		$resultado = 'NO'; $Informe_Risc = 'NO'; $Imagen_Pacs = 'NO';
		for ($j=0; $j < count($ordenes_resultado); $j++) { 
			
			if ($ordenes_general[$i]["Codigo"] == $ordenes_resultado[$j]["CodigoCpt"]) {
				$resultado = 'SI';
			}

			if ($ordenes_general[$i]["Codigo"] == $ordenes_resultado[$j]["CodigoCpt"] && $ordenes_resultado[$j]["ResultInformeFormato_RIS"]!='') {
				$Informe_Risc = '<img src="../../MVC_Complemento/img/Informe-ris.jpg" width="39" height="50" />';
				$ResultInformeFormato_RIS= $ordenes_resultado[$j]["ResultInformeFormato_RIS"];
			}

			if ($ordenes_general[$i]["Codigo"] == $ordenes_resultado[$j]["CodigoCpt"] && $ordenes_resultado[$j]["ResultInformeImagen_PACS"]!='') {
				$Imagen_Pacs = '<img src="../../MVC_Complemento/img/Imegen-pacs.jpg" width="74" height="50" />';
				$ResultInformeImagen_PACS= $ordenes_resultado[$j]["ResultInformeImagen_PACS"];
			}			
			
		}			

		$indice = $i + 1;
		$resultado_json[$i] = array(
			'NUMERO'	=>	$indice,
			'CODIGO'	=>	$ordenes_general[$i]["Codigo"],
			'NOMBRE'	=>	$ordenes_general[$i]["Nombre"],
			'CANTIDAD'	=>	$ordenes_general[$i]["cantidad"],
			'PRECIO'	=>	$ordenes_general[$i]["precio"],
			'TOTAL'		=>	$ordenes_general[$i]["importe"],
			'IDPRODCPT' => 	$ordenes_general[$i]["idProductoCPT"],
			'PUNTOCARGA' => 	$ordenes_general[$i]["Descripcion"],
			'RESULTAD'	=>	$resultado,
			'ORDEN'		=>  $idOrden,

			'Informe_Risc'		=>  $Informe_Risc,
			'Imagen_Pacs'		=>  $Imagen_Pacs,

			'ResultInformeFormato_RIS'		=>  $ResultInformeFormato_RIS,
			'ResultInformeImagen_PACS'		=>  $ResultInformeImagen_PACS,
			);

		
	}

	echo json_encode($resultado_json);
	exit();
	
}


/*if ($_REQUEST["acc"] == "datosCabecerasDetalle") {
	$idOrden = $_REQUEST["idorden"];
	$idProductoCpt = $_REQUEST["idproductocpt"];

	$data = SigesaLaboratorioMostrarDetalleOrdenResultadoParaImprimir_M($idOrden,$idProductoCpt);

	$realiza_prueba = $data[0]["REALIZAP"];
	$fecha_prueba   = $data[0]["Fecha"];

	echo json_encode(array('REALIZAP' => $realiza_prueba, 'FECHA' => $fecha_prueba));
	exit();
}*/


//BUSQUEDAS

if ($_REQUEST["acc"] == "BusquedaxNumeroHistoriaCLinica") {
	$numerohistoria 	= $_REQUEST["numerohistoria"];
	$FechaInicio_temp 	= $_REQUEST["fecha_inicio"];
	$FechaFinal_temp 	= $_REQUEST["fecha_fin"];

	#CONFIGURANDO FECHAS
	$fechaini = gfecha($_REQUEST["fecha_inicio"]);
	$fechafin = gfecha($_REQUEST["fecha_fin"]);

	$IdListItem=$_REQUEST["IdListItem"];
	$IdPuntoCarga=ObtenerIdPuntoCarga($IdListItem); 
	
	$data_general = SigesaLaboratorioBusquedaxNumeroHistoria_M($fechaini,$fechafin,$numerohistoria,$IdPuntoCarga);

	for ($i=0; $i < count($data_general); $i++) { 
		
		/*AGREGADO POR LGNA 21/07/2017*/
		$edad=CalcularEdadTonno($data_general[$i]['FNACIMIENTO']);		
		
		$FechaNacimiento=gfechatoño($data_general[$i]['FNACIMIENTO']);
		$FechaRegistro=gfechatoño($data_general[$i]['FREGISTRO']);		
		
			$data[$i] = array(
				'IDMOVIMIENTO' 	=> $data_general[$i]['IDMOVIMIENTO'],
				'NCUENTA' 		=> $data_general[$i]['NCUENTA'],
				'NHISCLI' 		=> $data_general[$i]['NHISCLI'],
				'PACIENTE' 		=> $data_general[$i]['PACIENTE'],
				'FREGISTRO' 	=> $FechaRegistro,
				'ORDENPUEBRA' 	=> $data_general[$i]['ORDENPUEBRA'],
				'FNACIMIENTO' 	=> $FechaNacimiento,
				'SEXO' 			=> $data_general[$i]['SEXO'],
				'EDAD'			=> $edad,
				'IDPACIENTE'	=> $data_general[$i]['IDPACIENTE'],
				'ORDEN'			=> $data_general[$i]['ORDEN'],
				'CAMA'			=> $data_general[$i]['CAMA']
				);
	}

	echo json_encode($data);
	exit();
}

if ($_REQUEST["acc"] == "BusquedaxNumeroCuenta") {
	$numerocuenta 	= $_REQUEST["numerocuenta"];
	$FechaInicio_temp 	= $_REQUEST["fecha_inicio"];
	$FechaFinal_temp 	= $_REQUEST["fecha_fin"];

	#CONFIGURANDO FECHAS
	$fechaini = gfecha($_REQUEST["fecha_inicio"]);
	$fechafin = gfecha($_REQUEST["fecha_fin"]);	

	$IdListItem=$_REQUEST["IdListItem"];
	$IdPuntoCarga=ObtenerIdPuntoCarga($IdListItem); 

	$data_general = SigesaLaboratorioBusquedaxNumeroCuenta_M($fechaini,$fechafin,$numerocuenta,$IdPuntoCarga);

	for ($i=0; $i < count($data_general); $i++) { 
		$fecha = time() - strtotime($data_general[$i]['FNACIMIENTO']);
		$edad=CalcularEdadTonno($data_general[$i]['FNACIMIENTO']);
		$FechaNacimiento=gfechatoño($data_general[$i]['FNACIMIENTO']);
		$FechaRegistro=gfechatoño($data_general[$i]['FREGISTRO']);

			$data[$i] = array(
				'IDMOVIMIENTO' 	=> $data_general[$i]['IDMOVIMIENTO'],
				'NCUENTA' 		=> $data_general[$i]['NCUENTA'],
				'NHISCLI' 		=> $data_general[$i]['NHISCLI'],
				'PACIENTE' 		=> $data_general[$i]['PACIENTE'],
				'FREGISTRO' 	=> $FechaRegistro,
				'ORDENPUEBRA' 	=> $data_general[$i]['ORDENPUEBRA'],
				'FNACIMIENTO' 	=> $FechaNacimiento,
				'SEXO' 			=> $data_general[$i]['SEXO'],
				'EDAD'			=> $edad,
				'IDPACIENTE'	=> $data_general[$i]['IDPACIENTE'],
				'ORDEN'			=> $data_general[$i]['ORDEN'],
				'CAMA'			=> $data_general[$i]['CAMA']
				);
	}

	echo json_encode($data);
	exit();
}

if ($_REQUEST["acc"] == "BusquedaxNumeroMovimiento") {
	$numeromovimi 		= $_REQUEST["numeromovimi"];
	$FechaInicio_temp 	= $_REQUEST["fecha_inicio"];
	$FechaFinal_temp 	= $_REQUEST["fecha_fin"];

	#CONFIGURANDO FECHAS
	$fechaini = gfecha($_REQUEST["fecha_inicio"]);
	$fechafin = gfecha($_REQUEST["fecha_fin"]);	

    $IdListItem=$_REQUEST["IdListItem"];
	$IdPuntoCarga=ObtenerIdPuntoCarga($IdListItem); 

	$data_general = SigesaLaboratorioBusquedaxNumeroMovimiento_M($fechaini,$fechafin,$numeromovimi,$IdPuntoCarga);

	for ($i=0; $i < count($data_general); $i++) { 	
	
		$edad=CalcularEdadTonno($data_general[$i]['FNACIMIENTO']);		
		$FechaNacimiento=gfechatoño($data_general[$i]['FNACIMIENTO']);
		$FechaRegistro=gfechatoño($data_general[$i]['FREGISTRO']);

			$data[$i] = array(
				'IDMOVIMIENTO' 	=> $data_general[$i]['IDMOVIMIENTO'],
				'NCUENTA' 		=> $data_general[$i]['NCUENTA'],
				'NHISCLI' 		=> $data_general[$i]['NHISCLI'],
				'PACIENTE' 		=> $data_general[$i]['PACIENTE'],
				'FREGISTRO' 	=> $FechaRegistro,
				'ORDENPUEBRA' 	=> $data_general[$i]['ORDENPUEBRA'],
				'FNACIMIENTO' 	=> $FechaNacimiento,
				'SEXO' 			=> $data_general[$i]['SEXO'],
				'EDAD'			=> $edad,
				'IDPACIENTE'	=> $data_general[$i]['IDPACIENTE'],
				'ORDEN'			=> $data_general[$i]['ORDEN'],
				'CAMA'			=> $data_general[$i]['CAMA']
				);
	}

	echo json_encode($data);
	exit();
}

if ($_REQUEST["acc"] == "BusquedaxNombrePaciente") {
	$paciente 			= $_REQUEST["paciente"];
	$FechaInicio_temp 	= $_REQUEST["fecha_inicio"];
	$FechaFinal_temp 	= $_REQUEST["fecha_fin"];

	#CONFIGURANDO FECHAS
	$fechaini = gfecha($_REQUEST["fecha_inicio"]);
	$fechafin = gfecha($_REQUEST["fecha_fin"]);	

	$IdListItem=$_REQUEST["IdListItem"];
	$IdPuntoCarga=ObtenerIdPuntoCarga($IdListItem); 

	$data_general = SigesaLaboratorioBusquedaxNombrePaciente_M($fechaini,$fechafin,$paciente,$IdPuntoCarga);

	for ($i=0; $i < count($data_general); $i++) { 
		$fecha = time() - strtotime($data_general[$i]['FNACIMIENTO']);
		$edad=CalcularEdadTonno($data_general[$i]['FNACIMIENTO']);

			$data[$i] = array(
				'IDMOVIMIENTO' 	=> $data_general[$i]['IDMOVIMIENTO'],
				'NCUENTA' 		=> $data_general[$i]['NCUENTA'],
				'NHISCLI' 		=> $data_general[$i]['NHISCLI'],
				'PACIENTE' 		=> $data_general[$i]['PACIENTE'],
				'FREGISTRO' 	=> $data_general[$i]['FREGISTRO'],
				'ORDENPUEBRA' 	=> $data_general[$i]['ORDENPUEBRA'],
				'FNACIMIENTO' 	=> $data_general[$i]['FNACIMIENTO'],
				'SEXO' 			=> $data_general[$i]['SEXO'],
				'EDAD'			=> $edad,
				'IDPACIENTE'	=> $data_general[$i]['IDPACIENTE'],
				'ORDEN'			=> $data_general[$i]['ORDEN'],
				'CAMA'			=> $data_general[$i]['CAMA']
				);
	}

	echo json_encode($data);
	exit();
}

?>