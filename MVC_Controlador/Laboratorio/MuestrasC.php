<?php
	include('../../MVC_Modelo/AnatomiaPatologicaM.php');	

	if ($_REQUEST["acc"]=="ListarMedicos"){	
		$data = ListarMedicos_M();

		for ($i=0; $i < count($data); $i++) { 
			$medicos[$i] = array(
				'IDMEDICO'	=> 	$data[$i]["IdMedico"],
				'NOMBREMEDICO'	=>	$data[$i]["NombreMedico"]
				);
		}
		echo json_encode($medicos);
		exit();
	}

	if ($_REQUEST["acc"]=="ListarSalaPiso"){	
		$data = ListarSalaPiso_M();

		for ($i=0; $i < count($data); $i++) { 
			$salapiso[$i] = array(
				'IDSALAPISO'	=> 	$data[$i]["IdSalaPiso"],
				'NOMBRESALAPISO'	=>	$data[$i]["NombreSalaPiso"]
				);
		}
		echo json_encode($salapiso);
		exit();
	}	

	if ($_REQUEST["acc"]=="ListarCalidad"){	
		$data = ListarCalidad_M();

		for ($i=0; $i < count($data); $i++) { 
			$calidad[$i] = array(
				'IDCALIDAD'	=> 	$data[$i]["IdCalidad"],
				'NOMBRECALIDAD'	=>	$data[$i]["NombreCalidad"]
				);
		}
		echo json_encode($calidad);
		exit();
	}

	if ($_REQUEST["acc"]=="ListarMotivo"){	
		$data = ListarMotivo_M();

		for ($i=0; $i < count($data); $i++) { 
			$motivo[$i] = array(
				'IDMOTIVO'	=> 	$data[$i]["IdMotivo"],
				'NOMBREMOTIVO'	=>	$data[$i]["NombreMotivo"]
				);
		}
		echo json_encode($motivo);
		exit();
	}

	if ($_REQUEST["acc"]=="ListarEndo"){	
		$data = ListarEndo_M();

		for ($i=0; $i < count($data); $i++) { 
			$endo[$i] = array(
				'IDENDO'	=> 	$data[$i]["IdEndo"],
				'NOMBREENDO'	=>	$data[$i]["NombreEndo"]
				);
		}
		echo json_encode($endo);
		exit();
	}

	if ($_REQUEST["acc"]=="ListarCate"){	
		$data = ListarCate_M();

		for ($i=0; $i < count($data); $i++) { 
			$cate[$i] = array(
				'IDCATE'	=> 	$data[$i]["IdCate"],
				'NOMBRECATE'	=>	$data[$i]["NombreCate"]
				);
		}
		echo json_encode($cate);
		exit();
	}

	if ($_REQUEST["acc"]=="EliminarMuestra"){

		$IM   = $_REQUEST["IdMuestra"];

		$RC   =   RetornarCodigoAP($IM);
		
		if($RC!=NULL){
			$query = EliminarxC_M($IM);			
			echo json_encode($query);
			exit();  
		}
		else{			
			echo json_encode(array('dato' => 'bad'));
			exit();
			} 			
	}

	if ($_REQUEST["acc"]=="DatosxHC"){

		$HC = $_REQUEST["HC"];

		$data = MostrarxHC_M($HC);

		if($data!=NULL){
		for ($i=0; $i < count($data); $i++) { 
			$resultado[$i] = array(
				'IDPACIENTE'		=> 	$data[$i]["IdPaciente"],
				'IDCUENTAATENCION'	=>  $data[$i]["IdCuentaAtencion"],
				'NOMBREPACIENTE'	=>	$data[$i]["NombrePaciente"],
				'EDAD'				=> 	$data[$i]["Edad"],
				'DESCRIPCION'		=>	$data[$i]["Descripcion"],
				'DIRECCION'			=> 	$data[$i]["DireccionDomicilio"],
				'FECHANACIMIENTO'	=>	$data[$i]["FechaNacimiento"]
				);
		}
		echo json_encode($resultado);
		//echo $json['NOMBREPACIENTE'];
		exit();
		}
		else {
			echo json_encode(array('dato' => 'bad'));
			exit();
		}	
	}

	if ($_REQUEST["acc"]=="DatosxCuenta"){

		$Cuenta = $_REQUEST["Cuenta"];

		$data = MostrarxCuenta_M($Cuenta);

		if($data!=NULL){
		for ($i=0; $i < count($data); $i++) { 
			$resultado[$i] = array(
				'IDPACIENTE'			=> 	$data[$i]["IdPaciente"],
				'NROHISTORIACLINICA'	=>  $data[$i]["NroHistoriaClinica"],
				'NOMBREPACIENTE'		=>	$data[$i]["NombrePaciente"],
				'EDAD'					=> 	$data[$i]["Edad"],
				'DESCRIPCION'			=>	$data[$i]["Descripcion"],
				'DIRECCION'				=> 	$data[$i]["DireccionDomicilio"],
				'FECHANACIMIENTO'		=>	$data[$i]["FechaNacimiento"]
				);
		}
		echo json_encode($resultado);
		//echo $json['NOMBREPACIENTE'];
		exit();
		}
		else {
			echo json_encode(array('dato' => 'bad'));
			exit();
		}
	}

	//Registrar
	if($_REQUEST["acc"] == "RegistrarMuestra"){ 

	$CodigoAP						=	$_REQUEST["CodigoAP"];
	$data 							=   RetornarCodigoAP($CodigoAP);

	if ($data==NULL){
		$IdTipoMuestra				=	$_REQUEST["TipoMuestra"];
		$IdTipoEspecifico			=	$_REQUEST["TipoEspecifico"];  
		$NumCuenta					=	$_REQUEST["NumCuenta"];
		$NombrePaciente				=	$_REQUEST["NombrePaciente"];
		$Sexo						=	$_REQUEST["Sexo"];
		$Edad						=	$_REQUEST["Edad"];
		$temp_FechaRegistro			=	$_REQUEST["FechaRegistro"];		
		
		$time = strtotime($temp_FechaRegistro);
		$FechaRegistro = date('Y-m-d',$time);

		$FechaCompletado			=	$_REQUEST["FechaCompletado"];
		$IdPaciente					=	$_REQUEST["IdPaciente"];
		$HC 						=	$_REQUEST["HC"];
		$Especimen 					=	$_REQUEST["Especimen"];
		$IdServicios				=	$_REQUEST["IdServicios"];	    	    
		$IdMedico1 					=	$_REQUEST["IdMedico1"];
		$IdMedico2 					=	$_REQUEST["IdMedico2"];
		$Endocervicales				=	$_REQUEST["Endocervicales"];
		$Categorizacion				=	$_REQUEST["Categorizacion"]; 
		$CalidadMuestra				=	$_REQUEST["CalidadMuestra"];   
		$Combinaciones				=	$_REQUEST["Combinaciones"];	    	    
		$OtrosCitologico			=	$_REQUEST["OtrosCitologico"];
		$DiagnosticoCitologico		=	$_REQUEST["DiagnosticoCitologico"];
		$DescripcionCitologico		=	$_REQUEST["DescripcionCitologico"];
		$DiagnosticoQuirurgico 		=	$_REQUEST["DiagnosticoQuirurgico"];
		$DescripcionQuirurgico 		=	$_REQUEST["DescripcionQuirurgico"];
		$Motivo 					=	$_REQUEST["Motivo"];

		
		if($Motivo ==' ' or $Motivo == NULL){  
				$Motivo = 'NULL';
		}

		if($Especimen=='' or $Especimen == NULL){
			$Especimen='NULL';
		}

		if($HC=='' or $HC == NULL){
			$HC='NULL';
		}

		if($NumCuenta=='' or $NumCuenta == NULL){
			$NumCuenta='NULL';
		}

		if($IdPaciente=='' or $IdPaciente == NULL){
			$IdPaciente='NULL';
		}

		    if($NombrePaciente==' ' or $NombrePaciente == NULL){
				$NombrePaciente= 'NULL';
			}

			if($Sexo==' ' or $Sexo == NULL){
				$Sexo= 'NULL';
			}

			if($Edad==' ' or $Edad == NULL){
				$Edad= 'NULL';
			}
			
			if($FechaRegistro==' ' or $FechaRegistro == NULL){
				$FechaRegistro= 'NULL';
			}

			if($FechaCompletado==' ' or $FechaCompletado == NULL){ 
				$FechaRegistro= 'NULL';
			}	

			if($IdEspecimen==' ' or $IdEspecimen == NULL){ 
				$IdEspecimen= 'NULL';
				}

			if($IdServicios==' ' or $IdServicios == NULL){  
				$IdServicios= 'NULL';
				}

			if($IdMedico1==' ' or $IdMedico1 == NULL){  
				$IdMedico1= 'NULL';
				}

			if($IdMedico2==' ' or $IdMedico2 == NULL){  
				$IdMedico2= 'NULL';
				}

			if($Endocervicales==' ' or $Endocervicales == NULL){  
				$Endocervicales= 'NULL';
				}

			if($Categorizacion==' ' or $Categorizacion == NULL){ 
				$Categorizacion= 'NULL';
				}

			if($CalidadMuestra==' ' or $CalidadMuestra == NULL){  
				$CalidadMuestra= 'NULL';
				}

			if($Combinaciones==' ' or $Combinaciones == NULL or $Combinaciones == 'NULL'){  
				$CValores= 'NULL';
				}
			else{
			$CValores = $Combinaciones[0];
			    for ($i=1; $i < count($Combinaciones); $i++) { 
			    	$CValores .= "-".$Combinaciones[$i];
			    }
			}
			if($OtrosCitologico==' ' or $OtrosCitologico == NULL){  
				$OtrosCitologico= 'NULL';
				}

			if($DiagnosticoCitologico==' ' or $DiagnosticoCitologico == NULL){  
				$DiagnosticoCitologico= 'NULL';
				}

			if($DescripcionCitologico==' ' or $DescripcionCitologico == NULL){  
				$DescripcionCitologico= 'NULL';
				}

			if($DiagnosticoQuirurgico==' ' or $DiagnosticoQuirurgico == NULL){  
				$DiagnosticoQuirurgico= 'NULL';
				}

			if($DescripcionQuirurgico==' ' or $DescripcionQuirurgico == NULL){  
				$DescripcionQuirurgico= 'NULL';
				}

			if($FechaCompletado==' ' or $FechaCompletado == NULL){  
				$FechaCompletado= 'NULL';
				}

			$query = RegistrarMuestra_M($IdTipoMuestra,$IdTipoEspecifico,$HC,$NumCuenta,$CodigoAP,$NombrePaciente,$Sexo,$Edad,$FechaRegistro,$FechaCompletado,$IdPaciente,$IdServicios,$Especimen,$IdMedico1,$IdMedico2,$Endocervicales,$Categorizacion,$CalidadMuestra,$CValores,$OtrosCitologico,$DiagnosticoCitologico,$DescripcionCitologico,$DiagnosticoQuirurgico,$DescripcionQuirurgico,$Motivo);	
			echo json_encode($query);
			exit(); 
		
	}	
	else{		
		echo json_encode(array('dato' => 'bad'));
		exit();	  
			}
	}

	if ($_REQUEST["acc"] == "BuscarxTipo") {
		
	#TIPO DE BUSQUEDA: 1 POR NOMBRE, 2 POR NUMCUENTA,3 POR CODIGO,4 POR HC
	$var = $_REQUEST["busqueda"];
	
	$tipo_b = $_REQUEST["tipo"];

	switch ($tipo_b) {
		case 1:
			$resultados = BuscarxNombre_M($var);
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {
					$sala = 'NULL';
					$especimen = 'NULL';
					$id_paciente = 'NULL';
					$nhc = 'NULL';
					$temp_paciente = $resultados[$i]["IdPaciente"];
					$temp_nhc = $resultados[$i]["HC"];
					$temp_sala = $resultados[$i]["Servicio"];
					$temp_especimen = $resultados[$i]["Especimenes"];
					if ($temp_especimen == NULL or $temp_especimen == 'NULL'){
						$especimen = 'No Registrado';
					} 
					else{
						$especimen = $resultados[$i]["Especimenes"];
					}
					if ($temp_sala == NULL or $temp_sala == 'NULL'){
						$sala = 'No Registrado';
					} 
					else{
						$sala = $resultados[$i]["Servicio"];
					}
					if ($temp_paciente == NULL or $temp_paciente == 'NULL'){
						$id_paciente = 'Particular';
					} 
					else{
						$id_paciente = $resultados[$i]["IdPaciente"];
					}
					if ($temp_nhc == NULL or $temp_nhc == 'NULL'){
						$nhc = 'Particular';
					} 
					else{
						$nhc = $resultados[$i]["HC"];
					}  
					$resultado[$i] = array(
						'CODIGOAP'				=> 	$resultados[$i]["CodigoAP"],
						'NROHISTORIACLINICA'	=>  $nhc,
						'NOMBREPACIENTE'		=>	$resultados[$i]["NombrePaciente"],
						'TIPO'					=>	$resultados[$i]["Tipo"],
						'TIPOE'					=>	$resultados[$i]["TipoEspecifico"],
						'ESPECIMENES'			=> 	$especimen,
						'FECHAREGISTRO'			=>	$resultados[$i]["FechaRegistro"],
						'FECHACOMPLETADO'		=> 	$resultados[$i]["FechaCompletado"],
						'SERVICIO'				=>	$sala,
						'IDPACIENTE'			=>	$id_paciente,
						'M1'					=>	$resultados[$i]["M1"],
						'M2'					=>	$resultados[$i]["M2"],
						'ENDO'					=>	$resultados[$i]["Endocervicales"],
						'CG'					=>	$resultados[$i]["Categorizacion"],
						'CM'					=>	$resultados[$i]["CalidadMuestra"],
						'COMBINACIONES'			=>	$resultados[$i]["Combinaciones"],
						'OTROS'					=>  $resultados[$i]["OtrosCitologico"],
						'DIC'					=>	$resultados[$i]["DiagnosticoCitologico"],
						'DEC'					=>	$resultados[$i]["DescripcionCitologico"],
						'DIQ'					=>	$resultados[$i]["DiagnosticoQuirurgico"],
						'DEQ'					=>	$resultados[$i]["DescripcionQuirurgico"],
						'MOTIVO'				=>	$resultados[$i]["Motivo"],
						);
				}
				echo json_encode($resultado);
			}
			else {
				echo json_encode(array('dato' => 'bad'));
			exit();
		}
		break;
		case 2:
			$resultados = BuscarxNumCuenta_M($var);
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {
					$sala = 'NULL';
					$especimen = 'NULL'; 
					$id_paciente = 'NULL';
					$nhc = 'NULL';
					$temp_paciente = $resultados[$i]["IdPaciente"];
					$temp_nhc = $resultados[$i]["HC"];
					$temp_sala = $resultados[$i]["Servicio"];
					$temp_especimen = $resultados[$i]["Especimenes"];
					if ($temp_especimen == NULL or $temp_especimen == 'NULL'){
						$especimen = 'No Registrado';
					} 
					else{
						$especimen = $resultados[$i]["Especimenes"];
					}
					if ($temp_sala == NULL or $temp_sala == 'NULL'){
						$sala = 'No Registrado';
					} 
					else{
						$sala = $resultados[$i]["Servicio"];
					}
					if ($temp_paciente == NULL or $temp_paciente == 'NULL'){
						$id_paciente = 'Particular';
					} 
					else{
						$id_paciente = $resultados[$i]["IdPaciente"];
					}
					if ($temp_nhc == NULL or $temp_nhc == 'NULL'){
						$nhc = 'Particular';
					} 
					else{
						$nhc = $resultados[$i]["HC"];
					} 
					$resultado[$i] = array(
						'CODIGOAP'				=> 	$resultados[$i]["CodigoAP"],
						'NROHISTORIACLINICA'	=>  $nhc,
						'NOMBREPACIENTE'		=>	$resultados[$i]["NombrePaciente"],
						'EDAD'					=>	$resultados[$i]["Edad"],
						'SEXO'					=>	$resultados[$i]["Sexo"],
						'TIPO'					=>	$resultados[$i]["Tipo"],
						'TIPOE'					=>	$resultados[$i]["TipoEspecifico"],
						'ESPECIMENES'			=> 	$especimen,
						'FECHAREGISTRO'			=>	$resultados[$i]["FechaRegistro"],
						'FECHACOMPLETADO'		=> 	$resultados[$i]["FechaCompletado"],
						'SERVICIO'				=>	$sala,
						'IDPACIENTE'			=>	$id_paciente,
						'M1'					=>	$resultados[$i]["M1"],
						'M2'					=>	$resultados[$i]["M2"],
						'ENDO'					=>	$resultados[$i]["Endocervicales"],
						'CG'					=>	$resultados[$i]["Categorizacion"],
						'CM'					=>	$resultados[$i]["CalidadMuestra"],
						'COMBINACIONES'			=>	$resultados[$i]["Combinaciones"],
						'OTROS'					=>  $resultados[$i]["OtrosCitologico"],
						'DIC'					=>	$resultados[$i]["DiagnosticoCitologico"],
						'DEC'					=>	$resultados[$i]["DescripcionCitologico"],
						'DIQ'					=>	$resultados[$i]["DiagnosticoQuirurgico"],
						'DEQ'					=>	$resultados[$i]["DescripcionQuirurgico"],						
						'MOTIVO'				=>	$resultados[$i]["Motivo"],
						);
				}
				echo json_encode($resultado);
			}
			else {
				echo json_encode(array('dato' => 'bad'));
			exit();
		}
		break;
		case 3:
			$resultados = BuscarxCodigo_M($var);
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) { 
					$sala = 'NULL';
					$especimen = 'NULL';
					$id_paciente = 'NULL';
					$nhc = 'NULL';
					$temp_paciente = $resultados[$i]["IdPaciente"];
					$temp_nhc = $resultados[$i]["HC"];
					$temp_sala = $resultados[$i]["Servicio"];
					$temp_especimen = $resultados[$i]["Especimenes"];
					if ($temp_especimen == NULL or $temp_especimen == 'NULL'){
						$especimen = 'No Registrado';
					} 
					else{
						$especimen = $resultados[$i]["Especimenes"];
					}
					if ($temp_sala == NULL or $temp_sala == 'NULL'){
						$sala = 'No Registrado';
					} 
					else{
						$sala = $resultados[$i]["Servicio"];
					}
					if ($temp_paciente == NULL or $temp_paciente == 'NULL'){
						$id_paciente = 'Particular';
					} 
					else{
						$id_paciente = $resultados[$i]["IdPaciente"];
					}
					if ($temp_nhc == NULL or $temp_nhc == 'NULL'){
						$nhc = 'Particular';
					} 
					else{
						$nhc = $resultados[$i]["HC"];
					}

					$resultado[$i] = array(
						'CODIGOAP'				=> 	$resultados[$i]["CodigoAP"],
						'NROHISTORIACLINICA'	=>  $nhc,
						'NOMBREPACIENTE'		=>	$resultados[$i]["NombrePaciente"],
						'EDAD'					=>	$resultados[$i]["Edad"],
						'SEXO'					=>	$resultados[$i]["Sexo"],
						'TIPO'					=>	$resultados[$i]["Tipo"],
						'TIPOE'					=>	$resultados[$i]["TipoEspecifico"],
						'ESPECIMENES'			=> 	$especimen,
						'FECHAREGISTRO'			=>	$resultados[$i]["FechaRegistro"],
						'FECHACOMPLETADO'		=> 	$resultados[$i]["FechaCompletado"],
						'SERVICIO'				=>	$sala,
						'IDPACIENTE'			=>	$id_paciente,
						'M1'					=>	$resultados[$i]["M1"],
						'M2'					=>	$resultados[$i]["M2"],
						'ENDO'					=>	$resultados[$i]["Endocervicales"],
						'CG'					=>	$resultados[$i]["Categorizacion"],
						'CM'					=>	$resultados[$i]["CalidadMuestra"],
						'COMBINACIONES'			=>	$resultados[$i]["Combinaciones"],
						'OTROS'					=>  $resultados[$i]["OtrosCitologico"],
						'DIC'					=>	$resultados[$i]["DiagnosticoCitologico"],
						'DEC'					=>	$resultados[$i]["DescripcionCitologico"],
						'DIQ'					=>	$resultados[$i]["DiagnosticoQuirurgico"],
						'DEQ'					=>	$resultados[$i]["DescripcionQuirurgico"],
						'MOTIVO'				=>	$resultados[$i]["Motivo"],
						);
				}
				echo json_encode($resultado);
			}
			else {
				echo json_encode(array('dato' => 'bad'));
			exit();
		}
		break;
		case 4:
			$resultados = BuscarxHC_M($var);
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {
				$sala = 'NULL';
				$especimen = 'NULL';
				$id_paciente = 'NULL';
				$nhc = 'NULL';
				$temp_paciente = $resultados[$i]["IdPaciente"];
				$temp_nhc = $resultados[$i]["HC"];
				$temp_sala = $resultados[$i]["Servicio"];
				$temp_especimen = $resultados[$i]["Especimenes"];
				if ($temp_especimen == NULL or $temp_especimen == 'NULL'){
					$especimen = 'No Registrado';
				} 
				else{
					$especimen = $resultados[$i]["Especimenes"];
				}
				if ($temp_sala == NULL or $temp_sala == 'NULL'){
					$sala = 'No Registrado';
				} 
				else{
					$sala = $resultados[$i]["Servicio"];
				}
				if ($temp_paciente == NULL or $temp_paciente == 'NULL'){
					$id_paciente = 'Particular';
				} 
				else{
					$id_paciente = $resultados[$i]["IdPaciente"];
				}
				if ($temp_nhc == NULL or $temp_nhc == 'NULL'){
					$nhc = 'Particular';
				} 
				else{
					$nhc = $resultados[$i]["HC"];
				} 
					$resultado[$i] = array(
						'CODIGOAP'				=> 	$resultados[$i]["CodigoAP"],
						'NROHISTORIACLINICA'	=>  $nhc,
						'NOMBREPACIENTE'		=>	$resultados[$i]["NombrePaciente"],
						'EDAD'					=>	$resultados[$i]["Edad"],
						'SEXO'					=>	$resultados[$i]["Sexo"],
						'TIPO'					=>	$resultados[$i]["Tipo"],
						'TIPOE'					=>	$resultados[$i]["TipoEspecifico"],
						'ESPECIMENES'			=> 	$especimen,
						'FECHAREGISTRO'			=>	$resultados[$i]["FechaRegistro"],
						'FECHACOMPLETADO'		=> 	$resultados[$i]["FechaCompletado"],
						'SERVICIO'				=>	$sala,
						'IDPACIENTE'			=>	$id_paciente,
						'M1'					=>	$resultados[$i]["M1"],
						'M2'					=>	$resultados[$i]["M2"],
						'ENDO'					=>	$resultados[$i]["Endocervicales"],
						'CG'					=>	$resultados[$i]["Categorizacion"],
						'CM'					=>	$resultados[$i]["CalidadMuestra"],
						'COMBINACIONES'			=>	$resultados[$i]["Combinaciones"],
						'OTROS'					=>  $resultados[$i]["OtrosCitologico"],
						'DIC'					=>	$resultados[$i]["DiagnosticoCitologico"],
						'DEC'					=>	$resultados[$i]["DescripcionCitologico"],
						'DIQ'					=>	$resultados[$i]["DiagnosticoQuirurgico"],
						'DEQ'					=>	$resultados[$i]["DescripcionQuirurgico"],						
						'MOTIVO'				=>	$resultados[$i]["Motivo"],
						);
				}
				echo json_encode($resultado);
			}
			else {
				echo json_encode(array('dato' => 'bad'));
			exit();
		}
		break;
	}
}


	if ($_REQUEST["acc"]=="ListarRegistro"){

		$data = ListarRegistro_M();

		for ($i=0; $i < count($data); $i++) {
			$sala = 'NULL';
			$especimen = 'NULL';
			$id_paciente = 'NULL';
			$nhc = 'NULL';
			$temp_paciente = $data[$i]["IdPaciente"];
			$temp_nhc = $data[$i]["HC"];
			$temp_sala = $data[$i]["Servicio"];
			$temp_especimen = $data[$i]["Especimenes"];
			if ($temp_especimen == NULL or $temp_especimen == 'NULL'){
				$especimen = 'No Registrado';
			} 
			else{
				$especimen = $data[$i]["Especimenes"];
			}
			if ($temp_sala == NULL or $temp_sala == 'NULL'){
				$sala = 'No Registrado';
			} 
			else{
				$sala = $data[$i]["Servicio"];
			}
			if ($temp_paciente == NULL or $temp_paciente == 'NULL'){
				$id_paciente = 'Particular';
			} 
			else{
				$id_paciente = $data[$i]["IdPaciente"];
			}
			if ($temp_nhc == NULL or $temp_nhc == 'NULL'){
				$nhc = 'Particular';
			} 
			else{
				$nhc = $data[$i]["HC"];
			}
			$resultado[$i] = array(
				'CODIGOAP'				=> 	$data[$i]["CodigoAP"],				
				'NROHISTORIACLINICA'	=>  $nhc,
				'NOMBREPACIENTE'		=>	$data[$i]["NombrePaciente"],
				'EDAD'					=>	$data[$i]["Edad"],
				'SEXO'					=>	$data[$i]["Sexo"],
				'TIPO'					=>	$data[$i]["Tipo"],
				'TIPOE'					=>	$data[$i]["TipoEspecifico"],
				'ESPECIMENES'			=> 	$especimen,
				'FECHAREGISTRO'			=>	$data[$i]["FechaRegistro"],
				'FECHACOMPLETADO'		=> 	$data[$i]["FechaCompletado"],
				'SERVICIO'				=>	$sala,				
				'IDPACIENTE'			=>  $id_paciente,				
				'M1'					=>	$data[$i]["M1"],
				'M2'					=>	$data[$i]["M2"],
				'ENDO'					=>	$data[$i]["Endocervicales"],
				'CG'					=>	$data[$i]["Categorizacion"],
				'CM'					=>	$data[$i]["CalidadMuestra"],
				'COMBINACIONES'			=>	$data[$i]["Combinaciones"],
				'OTROS'					=>  $data[$i]["OtrosCitologico"],
				'DIC'					=>	$data[$i]["DiagnosticoCitologico"],
				'DEC'					=>	$data[$i]["DescripcionCitologico"],
				'DIQ'					=>	$data[$i]["DiagnosticoQuirurgico"],
				'DEQ'					=>	$data[$i]["DescripcionQuirurgico"],				
				'MOTIVO'				=>	$data[$i]["Motivo"],
				);				
		}
		echo json_encode($resultado);
		exit();
	}

	if($_REQUEST["acc"] == "ModificarMuestra"){  					

			$Codigo						=	$_REQUEST["Codigo"];
			$RC 						=   RetornarCodigoAP($Codigo);

			if($RC!=NULL){

			$HC     					=	$_REQUEST["HC"];
			$NumCuenta					=	$_REQUEST["NumCuenta"];
			$NombrePaciente				=	$_REQUEST["NombrePaciente"];
			$Sexo						=	$_REQUEST["Sexo"];
			$Edad						=	$_REQUEST["Edad"];
			$IdPaciente     			=	$_REQUEST["IdPaciente"];
			$Especimen     				=	$_REQUEST["Especimen"];
		    $IdServicios				=	$_REQUEST["IdServicios"];	    	    
		    $IdMedico1 					=	$_REQUEST["IdMedico1"];
		    $IdMedico2 					=	$_REQUEST["IdMedico2"];
			$CalidadMuestra				=	$_REQUEST["CalidadMuestra"];
			$Endocervicales				=	$_REQUEST["Endocervicales"];  
	    	$Categorizacion				=	$_REQUEST["Categorizacion"];    
	    	$Combinaciones				=	$_REQUEST["Combinaciones"];
	    	$Motivo						=	$_REQUEST["Motivo"];

	    	$CValores = $Combinaciones[0];
	    		for ($i=1; $i < count($Combinaciones); $i++) { 
	    		$CValores .= "-".$Combinaciones[$i];
	    		}

	    	$OtrosCitologico			=	$_REQUEST["OtrosCitologico"];
	    	$DiagnosticoCitologico		=	$_REQUEST["DiagnosticoCitologico"];
	    	$DescripcionCitologico		=	$_REQUEST["DescripcionCitologico"];
	    	$DiagnosticoQuirurgico 		=	$_REQUEST["DiagnosticoQuirurgico"];
	    	$DescripcionQuirurgico 		=	$_REQUEST["DescripcionQuirurgico"];
	    	$FechaCompletado			=	$_REQUEST["FechaCompletado"];

	    	for ($i=0; $i < count($RC); $i++) {				
				$temp_salapiso = $RC[$i]["Servicio"];
				$temp_m1 = $RC[$i]["M1"];
				$temp_m2 = $RC[$i]["M2"];

				if ($IdServicios == NULL or $IdServicios == ' '){
					$IdServicios = $RC[$i]["Servicio"];;
				} 
				else{
					$IdServicios = $_REQUEST["IdServicios"];
				}
				if ($IdMedico1 == NULL or $IdMedico1 == ' '){
					$IdMedico1 = $RC[$i]["M1"];
				} 
				else{
					$IdMedico1 = $_REQUEST["IdMedico1"];
				}
				if ($IdMedico2 == NULL or $IdMedico2 == ' '){
					$IdMedico2 = $RC[$i]["M2"];
				} 
				else{
					$IdMedico2 = $_REQUEST["IdMedico2"];
				}					
			}

	    	if($Motivo ==' ' or $Motivo == NULL){  
				$Motivo = 'NULL';
				}

	    	if($HC ==' ' or $HC == NULL){  
				$HC = 'NULL';
				}

			if($NumCuenta==' ' or $NumCuenta == NULL){
				$NumCuenta= 'NULL';
			}	

			if($NombrePaciente==' ' or $NombrePaciente == NULL){
				$NombrePaciente= 'NULL';
			}

			if($Sexo==' ' or $Sexo == NULL){
				$Sexo= 'NULL';
			}

			if($Edad==' ' or $Edad == NULL){
				$Edad= 'NULL';
			}

	    	if($IdEspecimen ==' ' or $IdEspecimen == NULL){  
				$IdEspecimen = 'NULL';
				}	    				

			if($IdServicios==' ' or $IdServicios == NULL){  
				$IdServicios= 'NULL';
				}

			if($IdMedico1==' ' or $IdMedico1 == NULL){  
				$IdMedico1= 'NULL';
				}			

			if($IdMedico2==' ' or $IdMedico2 == NULL){  
				$IdMedico2= 'NULL';
				}			

			if($Endocervicales==' ' or $Endocervicales == NULL){  
				$Endocervicales= 'NULL';
				}
			

			if($Categorizacion==' ' or $Categorizacion == NULL){ 
				$Categorizacion= 'NULL';
				}

			if($CalidadMuestra==' ' or $CalidadMuestra == NULL){  
				$CalidadMuestra= 'NULL';
				}

			if($Combinaciones==' ' or $Combinaciones == NULL){  
				$CValores= 'NULL';
				}
			$CValores = $Combinaciones[0];
			    for ($i=1; $i < count($Combinaciones); $i++) { 
			    	$CValores .= "-".$Combinaciones[$i];
			    }

			if($OtrosCitologico==' ' or $OtrosCitologico == NULL){  
				$OtrosCitologico= 'NULL';
				}

			if($DiagnosticoCitologico==' ' or $DiagnosticoCitologico == NULL){  
				$DiagnosticoCitologico= 'NULL';
				}

			if($DescripcionCitologico==' ' or $DescripcionCitologico == NULL){  
				$DescripcionCitologico= 'NULL';
				}				

			if($DiagnosticoQuirurgico==' ' or $DiagnosticoQuirurgico == NULL){  
				$DiagnosticoQuirurgico= 'NULL';
				}

			if($DescripcionQuirurgico==' ' or $DescripcionQuirurgico == NULL){  
				$DescripcionQuirurgico= 'NULL';
				}
	    	
	    	$query = ModificarMuestra_M($Codigo,$IdPaciente,$Sexo,$Edad,$NombrePaciente,$HC,$NumCuenta,$IdServicios,$Especimen,$IdMedico1,$IdMedico2,$CalidadMuestra,$Endocervicales,$Categorizacion,$CValores,$OtrosCitologico,$DiagnosticoCitologico,$DescripcionCitologico,$DiagnosticoQuirurgico,$DescripcionQuirurgico,$FechaCompletado,$Motivo);
			
			echo json_encode($query);
			exit();  
			}
			else {
				echo json_encode(array('dato' => 'bad'));
				exit();
			} 
	}
?>