<?php 
//session_start();
//include('../../MVC_Modelo/SistemaM.php');
//include('../../MVC_Modelo/DatosInstitucionesM.php');
 ini_set('error_reporting',0);//para xamp
 date_default_timezone_set('America/Bogota');
 include('../../MVC_Modelo/Lista_VerificacionM.php');
 include('../../MVC_Modelo/SistemaM.php');
 include('../../MVC_Complemento/librerias/Funciones.php');

/***************************/
/*** Variables Glovales ***/
/**************************/
 $ListarFechaServidor=RetornaFechaServidorSQL_M();
 $FechaServidor = $ListarFechaServidor[0][0]; 

/****************/
/*** ACCIONES ***/
/****************/
if($_REQUEST["acc"] == "CambiarClave") 
{        
	$listarUsuario=ListarUsuarioxIdempleado_M($_REQUEST["IdEmpleado"]);
	$listarUsuario[0][0];
	$listarUsuario[0][0];
	
	$DNI=$listarUsuario[0]["DNI"];
	$ApellidoPaterno=$listarUsuario[0]["ApellidoPaterno"];
	$ApellidoMaterno=$listarUsuario[0]["ApellidoMaterno"];
	$ApellidoMaterno=$listarUsuario[0]["ApellidoMaterno"];
	$Nombres=$listarUsuario[0]["Nombres"];
	$Usuario=$listarUsuario[0]["Usuario"];   
	include('../../MVC_Vista/ListaVerificacion/CambiarClave.php');
}
 
if($_REQUEST["acc"] == "CambiarContrasennaActualizar") { // Cambiar Contraseña  
 
	SigesaUsuarioActualizarClave_M($_REQUEST["IdEmpleado"],$_REQUEST["CONTRA_REPITA"]); 	
	include('../../MVC_Vista/ListaVerificacion/LoginV.php');
 	//echo "<meta http-equiv='Refresh' content='0;url=../../index.html' target='_parent'>";

} 
 
if($_REQUEST["acc"] == "VerPreguntas") 
{       
              
       include('../../MVC_Vista/ListaVerificacion/LV_RepPreguntas.php');
}

 
if($_REQUEST["acc"] == "Reportes") // MOSTRAR: Formulario Listado
{
	$IdEmpleado=$_GET['IdEmpleado'];
	include('../../MVC_Vista/ListaVerificacion/Reportes.php');
}
 
if($_REQUEST["acc"] == "LV_Inicio") // MOSTRAR: Formulario Listado
{ 
	$Listarpacientes=SIGESA_ListVer_PacientesAtendidos_C();
	include('../../MVC_Vista/ListaVerificacion/Inicio.php');
}

if($_REQUEST["acc"] == "LV_Nuevo") // MOSTRAR: Formulario Nuevo
{   
	    
  $ListarEntrada=SIGESA_ListVer_ListarPreguntas_C('1');  
  $ListarPausa=SIGESA_ListVer_ListarPreguntas_C('2');
  $ListarSalida=SIGESA_ListVer_ListarPreguntas_C('3'); 
  include('../../MVC_Vista/ListaVerificacion/LV_Nuevo.php');
}

if($_REQUEST["acc"] == "LV_Modificar") // MOSTRAR: Formulario Modificar
{ 
  $IdListVerAtenciones=$_REQUEST["IdListVerAtenciones"];
  $ListVerModi=Sigesa_ListVer_ModificarM($IdListVerAtenciones);
  	    
  $ListarEntrada=SIGESA_ListVer_ListarPreguntas_C('1');  
  $ListarPausa=SIGESA_ListVer_ListarPreguntas_C('2');
  $ListarSalida=SIGESA_ListVer_ListarPreguntas_C('3'); 
  include('../../MVC_Vista/ListaVerificacion/LV_Modificar.php');
}

if($_REQUEST["acc"] == "LV_Validar") // MOSTRAR: Formulario Modificar
{ 
  $IdListVerAtenciones=$_REQUEST["IdListVerAtenciones"];
  $ListVerModi=Sigesa_ListVer_ModificarM($IdListVerAtenciones);
  	    
  $ListarEntrada=SIGESA_ListVer_ListarPreguntas_C('1');  
  $ListarPausa=SIGESA_ListVer_ListarPreguntas_C('2');
  $ListarSalida=SIGESA_ListVer_ListarPreguntas_C('3'); 
  include('../../MVC_Vista/ListaVerificacion/LV_Validar.php');
}



if($_REQUEST["acc"] == "LV_BuscarPaciente") // BUSCAR: Buscar Pacientes
{  
   $valor=$_REQUEST["valor"];
   $TipoBusqueda=$_REQUEST["TipoBusqueda"];
   $data = SIGESA_ListVer_Buscarpaciente_C($valor,$TipoBusqueda);
  
	for ($i=0; $i < count($data); $i++) { 
	
	    $FechaNacimiento = time() - strtotime($data[$i]['FechaNacimiento']);
		$edad = floor((($FechaNacimiento / 3600) / 24) / 360);
		$Paciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].', '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];
		$Domicilio=$data[$i]["departamentodomicilio"].' - '.$data[$i]["provinciadomicilio"].' - '.$data[$i]["distritodomicilio"];
		$LugarNacim=$data[$i]["departamentonac"].' - '.$data[$i]["provincianac"].' - '.$data[$i]["distritonac"];
		
   	   if($data[$i]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($data[$i]["IdTipoSexo"]==2){$Sexo='Femenino';}
				
		$resultadodetallado_json[$i] = array(
			'IdPaciente'		 	=> $data[$i]["IdPaciente"],			
			'NroDocumento' 			=> $data[$i]["NroDocumento"],
			'NroHistoriaClinica' 	=> $data[$i]["NroHistoriaClinica"],
			'FechaNacimiento' 		=> vfecha(substr($data[$i]["FechaNacimiento"],0,10)),
			'Sexo' 			=> $Sexo, 
			'Edad' 					=> $edad,
			'Paciente'	 			=> $Paciente,
			'Domicilio'	 			=> $Domicilio,
			'LugarNacim'	 		=> $LugarNacim
			);
	}
	if($resultadodetallado_json!=NULL)
	echo json_encode($resultadodetallado_json); 
	exit(); 

}

if($_REQUEST["acc"] == "LV_BuscarProfesional") // BUSCAR: Buscar Profesional
{  
   $valor=$_REQUEST["term"];
   $TipoProfesional=$_REQUEST["TipoProfesional"];//1 cirujano, 2 anestesiologo, 3 Instrumentista(enfermera), 4 asistente, 5 enfermera  
   
   $IdEspecialidad=$_REQUEST["IdEspecialidad"];   
	
    /*if($IdEspecialidad=='38'){ //general
		$IdEspecialidad='Medico Cirujano';  //$IdEspecialidad='101' or $IdEspecialidad='102'  //Tipo Empleado

	}else if($IdEspecialidad=='35'){ //torax y cardio
		$IdEspecialidad='Medico Cirujano Torax/cardiova';  //$IdEspecialidad='104' //Tipo Empleado
	
	}else if($IdEspecialidad=='127'){ //pediatrica
		$IdEspecialidad='Medico Cirujano Pediatrico';  //$IdEspecialidad='103' //Tipo Empleado
	
	}else if($IdEspecialidad=='129'){ //gineco
		$IdEspecialidad='Medico Gineco-obstetra';  //$IdEspecialidad='109' //Tipo Empleado
	
	}else if($IdEspecialidad=='43'){ //neuro cirujano
		$IdEspecialidad='Medico Neurocirujano';  //$IdEspecialidad='112' //Tipo Empleado
	
	}else if($IdEspecialidad=='39'){ //oncologia
		$IdEspecialidad='Medico Oncologo';  //$IdEspecialidad='115' //Tipo Empleado
	
	}else{
		$IdEspecialidad='Medico Cirujano'; //$IdEspecialidad='101' or $IdEspecialidad='102'  //Tipo Empleado
	}*/   
   
   $data = SIGESA_ListVer_BuscarProfesional_M($valor,$TipoProfesional,$IdEspecialidad);
  
	for ($i=0; $i < count($data); $i++) { 	
	    // $FechaNacimiento = time() - strtotime($data[$i]['FechaNacimiento']);
	    //$edad = floor((($FechaNacimiento / 3600) / 24) / 360);
		//$Paciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].', '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];
   	  //if($data[$i]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($data[$i]["IdTipoSexo"]==0){$Sexo='Femenino';}
		$Medico=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].', '.$data[$i]["Nombres"];
		$resultadodetallado_json[$i] = array(
			'IdEmpleado'		  => $data[$i]["IdEmpleado"],	
			'Medico'		 	  => $Medico,			 
			'IdMedico'		 	  => $data[$i]["IdMedico"],	
			'Colegiatura'		  => $data[$i]["Colegiatura"],
			'rne'		 	      => $data[$i]["rne"],
			'TiposEmpleado'		  => $data[$i]["TiposEmpleado"],
			'DNI'		  => $data[$i]["DNI"]				
		 );			
			
		//	IdEmpleado	ApellidoPaterno	ApellidoMaterno	Nombres	DNI	IdMedico	Colegiatura	rne	TiposEmpleado
     }
	if($resultadodetallado_json!=NULL)
	echo json_encode($resultadodetallado_json); 
	exit(); 

}


if($_REQUEST["acc"] == "LV_BuscarSalas") // BUSCAR: Buscar Pacientes
{  
   $valor=$_REQUEST["TipoSala"];
  //$TipoBusqueda=$_REQUEST["TipoBusqueda"];
   $data = SIGESA_ListVer_BuscarSalaXTipo_C($valor);
     
	for ($i=0; $i < count($data); $i++) { 				
		$resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdSala"],				 
			'text'	=> $data[$i]["Descripcion"]	
		);		 	
	}
	
	if($resultadodetallado_json!=NULL)
	echo json_encode($resultadodetallado_json); 
	exit();
}


if($_REQUEST["acc"] == "LV_BuscarServicios") // BUSCAR: Buscar Pacientes
{  
   $valor=$_REQUEST["Servicio"]; 
   $data = SIGESA_ListVer_BuscarServicio_C($valor);
  
	for ($i=0; $i < count($data); $i++) {	     
		$Servicio=$data[$i]["Servicios"].' - '.$data[$i]["TiposServicio"];
		$resultadodetallado_json[$i] = array(		
			'IdServicio'		  => $data[$i]["IdServicio"],			 		 
			'Servicios'		 	  => $Servicio, 
			'TiposServicio'		  => $data[$i]["TiposServicio"],		 
			'IdEspecialidad'	  => $data[$i]["IdEspecialidad"]
		 );				
		//	IdEmpleado	ApellidoPaterno	ApellidoMaterno	Nombres	DNI	IdMedico	Colegiatura	rne	TiposEmpleado		
	}
	
	if($resultadodetallado_json!=NULL)
	echo json_encode($resultadodetallado_json); 
	exit(); 

}

if($_REQUEST["acc"] == "LV_Guardar") // BUSCAR: Buscar Profesional de Salud
{	  
	$errors         = array();      // array to hold validation errors
	$data           = array();      // array to pass back data
	
	//validar antes de guardar (recuperar datos en ListaVerificacion.js en FormLisVer)	
	if (empty($_POST['dnival'])){
        $errors['dnival'] = 'Falta Buscar Paciente';				
	}else if ($_POST['Sala']=='Select'){
        $errors['Sala'] = 'Seleccionar Sala';		
    }else if (empty($_POST['Servicio'])){
        $errors['Servicio'] = 'Seleccionar Servicio';
	
	}else if (empty($_POST['IdCirujano'])){
        $errors['IdCirujano'] = 'Falta Buscar Cirujano';
	}else if (empty($_POST['IdAnestesiologo'])){
        $errors['IdAnestesiologo'] = 'Falta Buscar Anestesiologo';
	}else if (empty($_POST['IdEnfermeraCirculante1'])){
        $errors['IdEnfermeraCirculante1'] = 'Falta Buscar Enf.Circulante';
	}else{
	
	//GUARDAR DATOS
		//INICIO GENERAR CODIGO
		$data=ListarMaxCodigoListVer_AtencionesM();
		for ($i=0; $i < count($data); $i++) {	     
			$maxcodigo=$data[$i]["maxcodigo"];	
		}		
		if($maxcodigo!=NULL){
			$codigo=$maxcodigo+1;
		}else{
			$codigo=substr((date('Y')),2,2).'00001'; //1700001
		}	
		//FIN GENERAR CODIGO
		
	   //guardar tabla SIGESA_ListVer_Atenciones
	   $CodigoListVerAtenciones=$codigo;	  
	   $IdPaciente=$_REQUEST['IdPaciente'];
	   $IdSala=$_REQUEST['Sala'];
	   $IdServicio=$_REQUEST['Servicio'];
	   $UsuRegistra=$_REQUEST['IdEmpleado'];
	   $FechaRegistro=$FechaServidor;		  
	   /*$FechaInicioEntrada=NULL;
	   $FechaFinEntrada=NULL;
	   $FechaInicioPausa=NULL;
	   $FechaFinPausa=NULL;
	   $FechaInicioSalida=NULL;
	   $FechaFinSalida=NULL;*/
	   $Observacion=NULL;
	   $Estado='1';
	   $accion1=$_REQUEST['accion1'];
	   
	   $AntIdListVerAtenciones=$_REQUEST['IdListVerAtenciones'];
	   $NewIdListVerAtenciones=SIGESA_RegistrarListVer_AtencionesM($CodigoListVerAtenciones,$IdPaciente,$IdSala,$IdServicio,$UsuRegistra,$FechaRegistro,$Observacion,$Estado,$accion1,$AntIdListVerAtenciones);
	   
	   if($accion1=='R'){
		  $IdListVerAtenciones=$NewIdListVerAtenciones; 
	   }else if($accion1=='A'){
		  $IdListVerAtenciones=$AntIdListVerAtenciones; 
	   } 
		
		//guardar tabla SIGESA_ListVer_Responsables	
		//$IdResponsables//autoincrement		
		$FechaRegistro=$FechaServidor;	
		//$Validacion='0';//0		
		//$FechaValida=NULL;
		$Observacion=NULL;
		$Estado='1';			
		$IdCirujano=$_REQUEST['IdCirujano'];		
		$IdAnestesiologo=$_REQUEST['IdAnestesiologo'];
		$IdInstrumentista=$_REQUEST['IdInstrumentista'];
		$IdAsistente1=$_REQUEST['IdAsistente1'];
		$IdAsistente2=$_REQUEST['IdAsistente2'];
		$IdAsistente3=$_REQUEST['IdAsistente3'];
		$IdEnfermeraCirculante1=$_REQUEST['IdEnfermeraCirculante1'];		
		
		SIGESA_RegistrarListVer_ResponsablesM($IdCirujano,'1',$FechaRegistro,$Observacion,$IdListVerAtenciones,$Estado);
	
		SIGESA_RegistrarListVer_ResponsablesM($IdAnestesiologo,'2',$FechaRegistro,$Observacion,$IdListVerAtenciones,$Estado);
	
		SIGESA_RegistrarListVer_ResponsablesM($IdInstrumentista,'3',$FechaRegistro,$Observacion,$IdListVerAtenciones,$Estado);
	
		SIGESA_RegistrarListVer_ResponsablesM($IdAsistente1,'4',$FechaRegistro,$Observacion,$IdListVerAtenciones,$Estado);
	
		SIGESA_RegistrarListVer_ResponsablesM($IdAsistente2,'5',$FechaRegistro,$Observacion,$IdListVerAtenciones,$Estado);
	
		SIGESA_RegistrarListVer_ResponsablesM($IdAsistente3,'6',$FechaRegistro,$Observacion,$IdListVerAtenciones,$Estado);
	
		SIGESA_RegistrarListVer_ResponsablesM($IdEnfermeraCirculante1,'7',$FechaRegistro,$Observacion,$IdListVerAtenciones,$Estado);		
	//FIN GUARDAR DATOS	
	}		
	
    if ( ! empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {		
		$data['success'] = true;
        $data['message'] = 'Success!';
		$data['IdListVerAtenciones'] = $IdListVerAtenciones;
    }	
    echo json_encode($data);	 	 
}

if($_REQUEST["acc"] == "LV_GuardarValidar") // BUSCAR: Buscar Profesional de Salud
{	  
	$errors         = array();      // array to hold validation errors
	$data           = array();      // array to pass back data
	
	//validar antes de guardar (recuperar datos en ListaVerificacion.js en FormLisVer)	
	/*if (empty($_POST['IdCirujano'])){
        $errors['IdCirujano'] = 'Falta Buscar Cirujano';
	}else if (empty($_POST['IdAnestesiologo'])){
        $errors['IdAnestesiologo'] = 'Falta Buscar Anestesiologo';
	}else if (empty($_POST['IdEnfermeraCirculante1'])){
        $errors['IdEnfermeraCirculante1'] = 'Falta Buscar Enf.Circulante';
	}else{*/
				
		//guardar tabla SIGESA_ListVer_Responsables	
		//$IdResponsables//autoincrement		
		//$Observacion=NULL;
		//$Estado='1';	
		$IdListVerAtenciones=$_REQUEST['IdListVerAtenciones'];		
		$IdCirujano=$_REQUEST['IdCirujano'];		
		$IdAnestesiologo=$_REQUEST['IdAnestesiologo'];
		$IdInstrumentista=$_REQUEST['IdInstrumentista'];
		$IdAsistente1=$_REQUEST['IdAsistente1'];
		$IdAsistente2=$_REQUEST['IdAsistente2'];
		$IdAsistente3=$_REQUEST['IdAsistente3'];
		$IdEnfermeraCirculante1=$_REQUEST['IdEnfermeraCirculante1'];	
		
		$FechaValida=$FechaServidor;
		
		$Cirujano=$_REQUEST['Cirujano'];
		if ( $Cirujano=="VALIDADO") {
			$ValidacionCirujano='1';
			SIGESA_RegistrarListVer_ValidarResponsablesM($IdCirujano,1,$ValidacionCirujano,$FechaValida,$IdListVerAtenciones);			
		}else{		
			$ValidacionCirujano='0';
		}		
		$Anestesiologo=$_REQUEST['Anestesiologo'];
		if ( $Anestesiologo=="VALIDADO") {
			$ValidacionAnestesiologo='1';
			SIGESA_RegistrarListVer_ValidarResponsablesM($IdAnestesiologo,'2',$ValidacionAnestesiologo,$FechaValida,$IdListVerAtenciones);
		}else{		
			$ValidacionAnestesiologo='0';
		}
		$Instrumentista=$_REQUEST['Instrumentista'];
		if ( $Instrumentista=="VALIDADO") {
			$ValidacionInstrumentista='1';
			SIGESA_RegistrarListVer_ValidarResponsablesM($IdInstrumentista,'3',$ValidacionInstrumentista,$FechaValida,$IdListVerAtenciones);
		}else{		
			$ValidacionInstrumentista='0';
		}
		$Asistente1=$_REQUEST['Asistente1'];
		if ( $Asistente1=="VALIDADO") {
			$ValidacionAsistente1='1';
			SIGESA_RegistrarListVer_ValidarResponsablesM($IdAsistente1,'4',$ValidacionAsistente1,$FechaValida,$IdListVerAtenciones);
		}else{		
			$ValidacionAsistente1='0';
		}
		$Asistente2=$_REQUEST['Asistente2'];
		if ( $Asistente2=="VALIDADO") {
			$ValidacionAsistente2='1';
			SIGESA_RegistrarListVer_ValidarResponsablesM($IdAsistente2,'5',$ValidacionAsistente2,$FechaValida,$IdListVerAtenciones);
		}else{		
			$ValidacionAsistente2='0';
		}
		$Asistente3=$_REQUEST['Asistente3'];
		if ( $Asistente3=="VALIDADO") {
			$ValidacionAsistente3='1';
			SIGESA_RegistrarListVer_ValidarResponsablesM($IdAsistente3,'6',$ValidacionAsistente3,$FechaValida,$IdListVerAtenciones);
		}else{		
			$ValidacionAsistente3='0';
		}
		$EnfermeraCirculante1=$_REQUEST['EnfermeraCirculante1'];
		if ( $EnfermeraCirculante1=="VALIDADO") {
			$ValidacionEnfermeraCirculante1='1';
			SIGESA_RegistrarListVer_ValidarResponsablesM($IdEnfermeraCirculante1,'7',$ValidacionEnfermeraCirculante1,$FechaValida,$IdListVerAtenciones);
		}else{		
			$ValidacionEnfermeraCirculante1='0';
		}		
			
	//FIN GUARDAR DATOS	
	//}		
	
    if ( ! empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {		
		$data['success'] = true;
        $data['message'] = 'Success!';
		//$data['IdListVerAtenciones'] = $IdListVerAtenciones;
    }	
    echo json_encode($data);	 	 
}


if($_REQUEST["acc"] == "LV_GuardarPreguntasInicio") //Guardar Entrada
{
	$errors         = array();      // array to hold validation errors
	$data           = array();      // array to pass back data
	
	//
		$IdAlternativasChk = $_POST['IdAlternativasChk'];
        $porcionesa = explode("|", $IdAlternativasChk);
		
		$IdAlternativasRad = $_POST['IdAlternativasRad'];
        $porcionesb = explode("|", $IdAlternativasRad); 	
	//
	
	//validar antes de guardar (recuperar datos en ListaVerificacion.js en FormLisVer)	
	if (empty($_POST['FechaInicioEntrada'])){
        $errors['FechaInicioEntrada'] = 'Primero debe Identificar al Paciente';
				
	}else if ($_POST['IdListVerAtenciones2']==''){
        $errors['IdListVerAtenciones2'] = 'Primero debe Identificar al Paciente';
		
    }/*else if ( empty($_POST['IdAlternativasChk']) && empty($_POST['IdAlternativasRad']) ){
        $errors['IdAlternativas'] = 'Responder al menos una Pregunta';	
		
    }*/else if ( count($porcionesa)-1 < 4  ){
        $errors['AlternativasCheck'] = 'Debe marcar las 4 opciones de la Pregunta 1';	
		
    }else if ( count($porcionesb)-1 < 6 ){		
        $errors['AlternativasRad'] = 'Falta Responder '.(6-(count($porcionesb)-1)).' Pregunta(s)';
				
    }else{	
		//guardar tabla SIGESA_ListVer_Respuesta
		//$IdRespuesta //autoincrement
		$Valor=NULL;
		$Observacion=NULL;
		$FechaRegistro=$FechaServidor;	
		$IdListVerAtenciones=$_REQUEST['IdListVerAtenciones2'];		
		
		//INICIO OBTENER MAX ALTERNATIVA
		/*$data=SIGESA_ObtenerMaxIdAlternativaM();
		for ($i=0; $i < count($data); $i++) {	     
			$codigo=$data[$i]["maxalternativa"];	
		}*/				
		//FIN OBTENER MAX ALTERNATIVA		
				
		$IdAlternativasChk = $_REQUEST['IdAlternativasChk'];
        $porcionesa = explode("|", $IdAlternativasChk);
		SIGESA_EliminarListVer_RespuestaM($IdListVerAtenciones,'V','I');
        for ($i = 0; $i < count($porcionesa)-1; $i++) {
             $IdAlternativas = $porcionesa[$i]; 			
			 SIGESA_RegistrarListVer_RespuestaM($Valor,$Observacion,$FechaRegistro,$IdListVerAtenciones,$IdAlternativas,'V','I');            
        }
		
		$IdAlternativasRad = $_REQUEST['IdAlternativasRad'];
        $porcionesb = explode("|", $IdAlternativasRad);
		SIGESA_EliminarListVer_RespuestaM($IdListVerAtenciones,'U','I');
        for ($i = 0; $i < count($porcionesb)-1; $i++) {
             $IdAlternativas = $porcionesb[$i]; 			
			 SIGESA_RegistrarListVer_RespuestaM($Valor,$Observacion,$FechaRegistro,$IdListVerAtenciones,$IdAlternativas,'U','I');            
        }	
		
		//update tabla SIGESA_ListVer_Atenciones
		$IdListVerAtenciones=$_REQUEST['IdListVerAtenciones2'];		
		$FechaInicio=$_REQUEST['FechaInicioEntrada'];
		$FechaFin=$FechaServidor;	
		SIGESA_UpdateListVer_AtencionesM($IdListVerAtenciones,$FechaInicio,$FechaFin,'Entrada');		
		   	  
		//FIN GUARDAR DATOS	
	}
		
	
	if ( ! empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {		
		$data['success'] = true;
        $data['message'] = 'Success!';		
    }
	
    echo json_encode($data);	 
}


if($_REQUEST["acc"] == "LV_GuardarPreguntasPausa") //Guardar Pausa
{
	$errors         = array();      // array to hold validation errors
	$data           = array();      // array to pass back data
	
	//
		$IdAlternativasChk = $_POST['IdAlternativasChk'];
        $porcionesa = explode("|", $IdAlternativasChk);
		
		$IdAlternativasRad = $_POST['IdAlternativasRad'];
        $porcionesb = explode("|", $IdAlternativasRad); 	
	//
	
	//validar antes de guardar (recuperar datos en ListaVerificacion.js en FormLisVer)	
	if (empty($_POST['FechaInicioPausa'])){
        $errors['FechaInicioPausa'] = 'Primero debe Identificar al Paciente';
				
	}else if ($_POST['IdListVerAtenciones3']==''){
        $errors['IdListVerAtenciones3'] = 'Primero debe Identificar al Paciente';
		
    /*}else if ( empty($_POST['IdAlternativasChk']) && empty($_POST['IdAlternativasRad']) ){
        $errors['IdAlternativas'] = 'Responder al menos una Pregunta';		
    */    
	}else if ( !in_array("19", $porcionesa) || !in_array("20", $porcionesa) || !in_array("21", $porcionesa)  ){
        $errors['AlternativasCheck9'] = 'Debe marcar las 3 opciones de la Pregunta 9';	
		
    }else if ( !in_array("25", $porcionesa) || !in_array("26", $porcionesa) || !in_array("27", $porcionesa)  ){
        $errors['AlternativasCheck9'] = 'Debe marcar las 3 opciones de la Pregunta 11';	
		
    }else if ( count($porcionesa)-1 < 8  ){
        $errors['AlternativasCheck'] = 'Falta Responder '.(12-(count($porcionesa)-1)-(count($porcionesb)-1)).' Pregunta(s)';	
	
	}else if ( count($porcionesb)-1 < 4 ){		
        $errors['AlternativasRad'] = 'Falta Responder '.(4-(count($porcionesb)-1)).' Pregunta(s)';
				
    }else{	
		//guardar tabla SIGESA_ListVer_Respuesta
		//$IdRespuesta //autoincrement
		$Valor=NULL;
		$Observacion=NULL;
		$FechaRegistro=$FechaServidor;	
		$IdListVerAtenciones=$_REQUEST['IdListVerAtenciones3'];		
		
		//INICIO OBTENER MAX ALTERNATIVA
		/*$data=SIGESA_ObtenerMaxIdAlternativaM();
		for ($i=0; $i < count($data); $i++) {	     
			$codigo=$data[$i]["maxalternativa"];	
		}*/				
		//FIN OBTENER MAX ALTERNATIVA		
				
		$IdAlternativasChk = $_REQUEST['IdAlternativasChk'];
        $porcionesa = explode("|", $IdAlternativasChk);
		SIGESA_EliminarListVer_RespuestaM($IdListVerAtenciones,'V','P');
        for ($i = 0; $i < count($porcionesa)-1; $i++) {
             $IdAlternativas = $porcionesa[$i]; 			
			 SIGESA_RegistrarListVer_RespuestaM($Valor,$Observacion,$FechaRegistro,$IdListVerAtenciones,$IdAlternativas,'V','P');            
        }
		
		$IdAlternativasRad = $_REQUEST['IdAlternativasRad'];
        $porcionesb = explode("|", $IdAlternativasRad);
		SIGESA_EliminarListVer_RespuestaM($IdListVerAtenciones,'U','P');
        for ($i = 0; $i < count($porcionesb)-1; $i++) {
             $IdAlternativas = $porcionesb[$i]; 			
			 SIGESA_RegistrarListVer_RespuestaM($Valor,$Observacion,$FechaRegistro,$IdListVerAtenciones,$IdAlternativas,'U','P');            
        }
		
		//update tabla SIGESA_ListVer_Atenciones
		$IdListVerAtenciones=$_REQUEST['IdListVerAtenciones3'];		
		$FechaInicio=$_REQUEST['FechaInicioPausa'];
		$FechaFin=$FechaServidor;	
		SIGESA_UpdateListVer_AtencionesM($IdListVerAtenciones,$FechaInicio,$FechaFin,'Pausa');
			   	  
		//FIN GUARDAR DATOS	
	}		
	
	if ( ! empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {		
		$data['success'] = true;
        $data['message'] = 'Success!';		
    }
	
    echo json_encode($data);	 
}

if($_REQUEST["acc"] == "LV_GuardarPreguntasSalida") //Guardar Salida
{
	$errors         = array();      // array to hold validation errors
	$data           = array();      // array to pass back data
	
	//
		$IdAlternativasChk = $_REQUEST['IdAlternativasChk'];
        $porcionesa = explode("|", $IdAlternativasChk);
		
		/*$IdAlternativasRad = $_POST['IdAlternativasRad'];
        $porcionesb = explode("|", $IdAlternativasRad);*/ 	
	//
	
	//validar antes de guardar (recuperar datos en ListaVerificacion.js en FormLisVer)	
	if (empty($_POST['FechaInicioSalida'])){
        $errors['FechaInicioSalida'] = 'Primero debe Identificar al Paciente';
				
	}else if ($_POST['IdListVerAtenciones4']==''){
        $errors['IdListVerAtenciones4'] = 'Primero debe Identificar al Paciente';
		
    /*}else if ( empty($_POST['IdAlternativasChk']) && empty($_POST['IdAlternativasRad']) ){
        $errors['IdAlternativas'] = 'Responder al menos una Pregunta';		
    */    
	}else if ( !in_array("37", $porcionesa) || !in_array("38", $porcionesa) || !in_array("39", $porcionesa) || !in_array("40", $porcionesa)  ){
        $errors['AlternativasCheck16'] = 'Debe marcar las 4 opciones de la Pregunta 16';	
		
    }else if ( count($porcionesa)-1 < 5  ){
        $errors['AlternativasCheck'] = 'Falta Responder '.(5-(count($porcionesa)-1)).' Pregunta(s)';	
		
    }else{	
		//guardar tabla SIGESA_ListVer_Respuesta
		//$IdRespuesta //autoincrement
		$Valor=NULL;
		$Observacion=NULL;
		$FechaRegistro=$FechaServidor;	
		$IdListVerAtenciones=$_REQUEST['IdListVerAtenciones4'];		
		
		//INICIO OBTENER MAX ALTERNATIVA
		/*$data=SIGESA_ObtenerMaxIdAlternativaM();
		for ($i=0; $i < count($data); $i++) {	     
			$codigo=$data[$i]["maxalternativa"];	
		}*/				
		//FIN OBTENER MAX ALTERNATIVA		
				
		$IdAlternativasChk = $_REQUEST['IdAlternativasChk'];
		if($IdAlternativasChk!=""){
			$porcionesa = explode("|", $IdAlternativasChk);
			SIGESA_EliminarListVer_RespuestaM($IdListVerAtenciones,'V','S');
			for ($i = 0; $i < count($porcionesa)-1; $i++) {
				 $IdAlternativas = $porcionesa[$i]; 			
				 SIGESA_RegistrarListVer_RespuestaM($Valor,$Observacion,$FechaRegistro,$IdListVerAtenciones,$IdAlternativas,'V','S');            
			}
		}
		
		$IdAlternativasRad = $_REQUEST['IdAlternativasRad'];
		if($IdAlternativasRad!=""){
			$porcionesb = explode("|", $IdAlternativasRad);
			SIGESA_EliminarListVer_RespuestaM($IdListVerAtenciones,'U','S');
			for ($i = 0; $i < count($porcionesb)-1; $i++) {
				 $IdAlternativas = $porcionesb[$i]; 			
				 SIGESA_RegistrarListVer_RespuestaM($Valor,$Observacion,$FechaRegistro,$IdListVerAtenciones,$IdAlternativas,'U','S');            
			}
		}
		
		//update tabla SIGESA_ListVer_Atenciones
		$IdListVerAtenciones=$_REQUEST['IdListVerAtenciones4'];		
		$FechaInicio=$_REQUEST['FechaInicioSalida'];
		$FechaFin=$FechaServidor;	
		SIGESA_UpdateListVer_AtencionesM($IdListVerAtenciones,$FechaInicio,$FechaFin,'Salida');
		
		$Observacion=$_REQUEST['Observacion'];
		SIGESA_UpdateListVer_AtencionesObserM($IdListVerAtenciones,$Observacion);		
		//FIN GUARDAR DATOS	
		
	}		
	
	if ( ! empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {		
		$data['success'] = true;
        $data['message'] = 'Success!';		
    }
	
    echo json_encode($data);	
    	 
}
 
/*if($_REQUEST["acc"] == "LV_Modificar") // MOSTRAR: Formulario Login
{  
	include('../../MVC_Vista/ListaVerificacion/Inicio.php');
}*/

/*if($_REQUEST["acc"] == "LV_Imprimir") // MOSTRAR: Formulario Login
{  
	include('../../MVC_Vista/ListaVerificacion/Inicio.php');
}*/ 

if($_REQUEST["acc"] == "LV_Eliminar") // MOSTRAR: Formulario Login
{ 
	$IdListVerAtenciones = $_REQUEST['IdListVerAtenciones'];
	$IdEmpleado=$_REQUEST['IdEmpleado'];
	
	$listarUsuario=ListarUsuarioxIdempleado_M($IdEmpleado);   
	/*$DNI=$listarUsuario[0]["DNI"];
	$ApellidoPaterno=$listarUsuario[0]["ApellidoPaterno"];
	$ApellidoMaterno=$listarUsuario[0]["ApellidoMaterno"];
	$ApellidoMaterno=$listarUsuario[0]["ApellidoMaterno"];
	$Nombres=$listarUsuario[0]["Nombres"];*/
	$Usuario=$listarUsuario[0]["Usuario"];
	
	$Observacion="Eliminado el ".date('d-m-Y H:i:s').' Por '.$Usuario;	
	SIGESA_EliminarListVer_AtencionesM($IdListVerAtenciones,$Observacion);
	//VOLVER 
	$Listarpacientes=SIGESA_ListVer_PacientesAtendidos_C();
	include('../../MVC_Vista/ListaVerificacion/Inicio.php');
}

if($_REQUEST["acc"] == "CambiarClaveResponsablesLV") // MOSTRAR: Formulario CambiarClaveResponsablesLV 
{ 
	$IdEmpleado=$_GET["IdEmpleado"];
	include('../../MVC_Vista/ListaVerificacion/CambiarClaveResponsablesLV.php');
}

///---------------------------- FUNCIONES ---------------------

function Login_ValidarUsuario_C($usuario,$password){
	return  Login_ValidarUsuario_M($usuario,$password);
}

function SIGESA_ListVer_ListarPreguntas_C($IdEstaoPreg){
	return SIGESA_ListVer_ListarPreguntas_M($IdEstaoPreg);
}	

function SIGESA_ListVer_ListarAlternativas_C($IdPreguntas){
	return SIGESA_ListVer_ListarAlternativas_M($IdPreguntas);
}

function SIGESA_ListVer_Buscarpaciente_C($valor,$Pivotx){
	return SIGESA_ListVer_Buscarpaciente_M($valor,$Pivotx);
}

function SIGESA_ListVer_BuscarSalaXTipo_C($TipoSala){
	return   SIGESA_ListVer_BuscarSalaXTipo_M($TipoSala);
}

function SIGESA_ListVer_BuscarServicio_C($valor){
	return  SIGESA_ListVer_BuscarServicio_M($valor);
}	

function SIGESA_ListVer_PacientesAtendidos_C(){
	return  SIGESA_ListVer_PacientesAtendidos_M();
}	

?>