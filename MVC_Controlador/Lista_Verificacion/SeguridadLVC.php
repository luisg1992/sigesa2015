<?php
	
	include('../../MVC_Modelo/SeguridadLVM.php');	
	include('../../MVC_Complemento/librerias/Funciones.php');

	//MOSTRAR: Vista para Editar un Empleado	
	
	if($_REQUEST["acc"] == "EditarEmpleado")
	{
		$IdEmpleado = $_GET['IdEmpleado'];
		$Datos_Empleado=Mostrar_Empleado_IdEmpleado_M($IdEmpleado);
    	include("../../MVC_Vista/ListaVerificacion/Seguridad_ModificarEmpleado.php");
	}
	
	//MOSTRAR: Accion para Modificar la Contraseña de un Empleado
    if ($_REQUEST["acc"] == "Modificar_Empleado")
	{		
		if($_REQUEST['Clave']!=NULL ) 
		{
			$Clave=$_REQUEST['Clave'];
			$Id_Empleado=$_REQUEST['IdEmpleado'];
			$Modificar_Empleado=Modificar_Empleado_Sigesa_M($Clave,$Id_Empleado);	
			$mensaje = "Empleado_Modificado";			
			print "<script>alert('$mensaje')</script>";					
		}		
	}		
	
	//MOSTRAR: Accion para Resetear un Empleado
	
	if ($_REQUEST["acc"] == "Resetear_Empleado")
	{
	$IdEmpleadoResetear=$_REQUEST['IdEmpleadoResetear'];
    $Resetear_Empleado=Resetear_Empleado_Id_M($IdEmpleadoResetear);
	}	
	
	//MOSTRAR: Accion para Eliminar un Empleado
	
	if ($_REQUEST["acc"] == "Eliminar_Empleado")
	{
	$IdEmpleadoEliminar=$_REQUEST['IdEmpleadoEliminar'];
    $Eliminar_Empleado=Eliminar_Empleado_Id_M($IdEmpleadoEliminar);
	}
	
	//MOSTRAR: Datos de un Medico	
	if ($_REQUEST["acc"] == "Mostrar_Empleado")
	{
	$NroDocumento=$_REQUEST['nro_documento'];
    $Encontrar_Empleado=Mostrar_EmpleadoMedico_M($NroDocumento);		
	}	
	
	//MOSTRAR: Accion para crear un Empleado Sigesa
    if ($_REQUEST["acc"] == "Agregar_Empleado")
	{		
		
		if($_REQUEST['Clave']!=NULL ) 
		{
			$Clave=$_REQUEST['Clave'];
			$Id_Empleado=$_REQUEST['Id_Empleado'];
			$Mostrar_Empleado_Id=Mostrar_Empleado_Sigesa_IdEmpleado_M($Id_Empleado);
			if($Mostrar_Empleado_Id != NULL)
			{
					$mensaje = "Empleado_Existente";
					print "<script>alert('$mensaje')</script>";	
			}else{
				
					$Agregar_Empleado=Agregar_Empleado_Sigesa_M($Clave,$Id_Empleado);	
					$mensaje = "Empleado_Agregado";
					print "<script>alert('$mensaje')</script>";			
			}	
		}		
	
	}		
	
	
?>