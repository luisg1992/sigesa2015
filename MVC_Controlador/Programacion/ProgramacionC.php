<?php
session_start();
include('../../MVC_Modelo/ProgramacionM.php');
include('../../MVC_Complemento/librerias/Funciones.php');

	//LISTAR CONSULTORIOS
	 if ($_REQUEST["acc"] == "mostrarServicios") {
		$data = ListarServicios_C();
		for ($i=0; $i < count($data); $i++) { 
			$servicio[$i] = array(
					'id'	=> $data[$i]["NOMBRE"],
					'text'  => $data[$i]["NOMBRE"]
				);
		}
		echo json_encode($servicio);
		exit();
	}

	//MOSTRAR PROGRAMACION
	if ($_REQUEST["acc"] == "mostrarProgramacion") 
	{
		$fecha 	= $_REQUEST["fecha"];
		$Servicio 	= $_REQUEST["Servicio"];

		$servicios_temp 	= MostrarProgramacion_C($fecha, $Servicio);

		#GENERANDO PROGRAMACIÓN
		for ($i=0; $i < count($servicios_temp); $i++) { 
			$lista_programacion[$i] = array(

				'IDPROGRAMACION'=> 	$servicios_temp[$i]['IDPROGRAMACION'],
				'CONSULTORIO'	=> 	$servicios_temp[$i]['CONSULTORIO'],
				'MEDICO'		=> 	$servicios_temp[$i]['MEDICO'],
				'HORAINICIO'	=> 	$servicios_temp[$i]['HORAINICIO'],
				'HORAFIN'		=> 	$servicios_temp[$i]['HORAFIN'],
				'IDSERVICIO'	=> 	$servicios_temp[$i]['IDSERVICIO'],
				'PrimerPaciente'	=> 	$servicios_temp[$i]['PrimerPaciente'],
				'UltimoPaciente'	=> 	$servicios_temp[$i]['UltimoPaciente'],
				'TotPaciente'	=> 	$servicios_temp[$i]['TotPaciente'],
				
			);
		}
		echo json_encode($lista_programacion);
		exit();
	}


	//ACTUALIZAR PROGRAMACION
	if ($_REQUEST["acc"] == "actualizarProgramacion")
	{
		$IdProgramacion = $_REQUEST["IDPROGRAMACION"];
		$horaInicio = $_REQUEST["HORAINICIO"];
		$horaFin = $_REQUEST["HORAFIN"];

		$query = ActualizarHoraInicioFin($IdProgramacion, $horaInicio, $horaFin);

		echo json_encode($query);
		exit();
	}
	
	/**FUNCIONES**/
	
	function MostrarProgramacion_C($fecha, $Servicio){
	 return  MostrarProgramacion_M($fecha, $Servicio);
	
  	}

	 function ListarServicios_C(){
		 return  ListarServicios_M();
	}
	
///////////////////// 27/02/2017
	if ($_REQUEST["acc"] == "Medico")
	{
		SIGESA_Programacion_ActualizarTablaEspecialidadesCE_LCO();
		include("../../MVC_Vista/Programacion/ActualizaEspecialidadMedicos.php");
		
	}


if ($_REQUEST["acc"] == "BuscarMedicos") {
   $valor=$_REQUEST["q"];
   $TipoBusqueda='ApellidosNombresmedicos';
   $data = SIGESA_Programacion_Buscarmedico_LCO($valor,$TipoBusqueda);
		   $data = SIGESA_Programacion_Buscarmedico_LCO($valor,$TipoBusqueda);

		for ($i=0; $i < count($data); $i++) { 
		$Medico=$data[$i]["Medico"];
			$data[$i] = array(

				
				'IdMedico' => $data[$i]["IdMedico"],
				'Medico' => $Medico//mb_strtoupper($data[$i]["Descripcion"])
				
				);
		}
		
		
		echo json_encode($data);
		exit();
}


if ($_REQUEST["acc"] == "BusquedaMedicosEspecialidad") {


 $IdMedico = $_GET["IdMedico"];

$data_medicos = SIGESA_Programacion_ListaMedicoxEspecialidades_LCO($IdMedico);


		for ($i=0; $i < count($data_medicos); $i++) { 
			
			if($data_medicos[$i]["Estado"]=='1'){$estado=1;}else{$estado=0;}

			$data[$i] = array(
			'xIdMedico' => $data_medicos[$i]["IdMedico"],	
			'xId' => $data_medicos[$i]["IdMedicoEspecialidad"],
           // 'xColegiatura' => $data_medicos[$i]["Colegiatura"],
            'xMedico' => $data_medicos[$i]["ApellidoPaterno"].' '.$data_medicos[$i]["ApellidoMaterno"].' '.$data_medicos[$i]["Nombres"],
            'xDepartamento' => $data_medicos[$i]["departamento"],
            'xEspecialidad' => $data_medicos[$i]["especialidad"],
			'xEstado' => $estado
				);
		}

		echo json_encode($data);
		exit();

	
}
if ($_REQUEST["acc"] == "GuardarActualizaEspecialidades") {
	
	$errors = array();      // array to hold validation errors
    $data = array();
	$dataResultado = $_REQUEST["dataResultado"];  
				for ($i = 0; $i < $dataResultado['total']; $i++) {
                            try {
								
                                $IdEspMedico = $dataResultado['rows'][$i]["xId"];
								$Estado = $dataResultado['rows'][$i]["xEstado"];
								if($Estado=='1'){
									$EstadoPrim='1';
									}else{
									$EstadoPrim='null';	
										}
                               SIGESA_Programacion_ActualizarEspecialidades_LCO($IdEspMedico,$EstadoPrim);
                            } catch (Exception $e) {
                                echo 'Caught Exception: ', $e->getMessage(), "\n";
                            }
                        }
		   
			
	if (!empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $data['success'] = true;
        $data['message'] = 'Success!';	
    }
    echo json_encode($data);
	
	
	
	}


if ($_REQUEST["acc"] == "ActualizarEspecialidadesCE") {
	
	$errors = array();      // array to hold validation errors
    $data = array();
	$dataResultado = $_REQUEST["dataResultado"];  
				
                            try {
                               SIGESA_Programacion_ActualizarTablaEspecialidadesCE_LCO();
                            } catch (Exception $e) {
                                echo 'Caught Exception: ', $e->getMessage(), "\n";
                            }
                       
		   
			
	if (!empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $data['success'] = true;
        $data['message'] = 'Success!';	
    }
    echo json_encode($data);
	
	
	
	}

	
?>