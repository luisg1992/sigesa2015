<?php 
//session_start();
//include('../../MVC_Modelo/SistemaM.php');
//include('../../MVC_Modelo/DatosInstitucionesM.php');
 ini_set('error_reporting',0);//para xamp
 date_default_timezone_set('America/Bogota');
 include('../../MVC_Modelo/RecetasMedicasM.php');
 include('../../MVC_Modelo/SistemaM.php');
 include('../../MVC_Complemento/librerias/Funciones.php');

/***************************/
/*** Variables Glovales ***/
/**************************/
 $ListarFechaServidor=RetornaFechaServidorSQL_M();
 $FechaServidor = $ListarFechaServidor[0][0]; 

/****************/
/*** ACCIONES ***/
/****************/
if($_REQUEST["acc"] == "CambiarClave") 
{        
	$listarUsuario=ListarUsuarioxIdempleado_M($_REQUEST["IdEmpleado"]);
	$listarUsuario[0][0];
	$listarUsuario[0][0];
	
	$DNI=$listarUsuario[0]["DNI"];
	$ApellidoPaterno=$listarUsuario[0]["ApellidoPaterno"];
	$ApellidoMaterno=$listarUsuario[0]["ApellidoMaterno"];
	$ApellidoMaterno=$listarUsuario[0]["ApellidoMaterno"];
	$Nombres=$listarUsuario[0]["Nombres"];
	$Usuario=$listarUsuario[0]["Usuario"];   
	include('../../MVC_Vista/RecetasMedicas/CambiarClave.php');
}
 
if($_REQUEST["acc"] == "CambiarContrasennaActualizar") { // Cambiar Contraseña  
 
	SigesaUsuarioActualizarClave_M($_REQUEST["IdEmpleado"],$_REQUEST["CONTRA_REPITA"]); 	
	include('../../MVC_Vista/RecetasMedicas/LoginRecetas.php');
} 
 
if($_REQUEST["acc"] == "Inicio") 
{ 
	if($_GET['IdEmpleado']==''){
		$IdEmpleado=$_SESSION['IdEmpleado'];	
	}else{
		$IdEmpleado=$_GET['IdEmpleado'];	
	}
	$idTipoServicio=$_REQUEST["idTipoServicio"];
	 		
	include('../../MVC_Vista/RecetasMedicas/Inicio.php');
}

if($_REQUEST["acc"] == "BusquedaPacientesNuevaReceta") 
{ 
	if($_GET['IdEmpleado']==''){
		$IdEmpleado=$_SESSION['IdEmpleado'];	
	}else{
		$IdEmpleado=$_GET['IdEmpleado'];	
	} 		
	include('../../MVC_Vista/RecetasMedicas/BusquedaPacientesNuevaReceta.php');
}

if($_REQUEST["acc"] == "ListarServiciosProgramacionMedicos") 
{  
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();
   
   $FechaIngreso=gfecha($_REQUEST["FechaIngreso"]);
   $IdMedico=$_REQUEST["IdMedico"];
   $data = ListarServiciosProgramacionMedicos_M($IdMedico,$FechaIngreso);
   
   for ($i=0; $i < count($data); $i++) { 	    
				
		$resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdServicio"],				 
			'text'	=> $data[$i]["IdServicio"].'='.$data[$i]["Nombre"]	
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "ListarServiciosEmergencia") 
{  
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();   
   
   //$IdTipoServicio=$_REQUEST["IdTipoServicio"];
   $data = ListarServiciosEmergencia_M();
   
   for ($i=0; $i < count($data); $i++) { 	    
				
		$resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdServicio"],				 
			'text'	=> $data[$i]["IdServicio"].'='.$data[$i]["Nombre"]	
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "ListarServiciosHospitalizacion") 
{  
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();   
   
   //$IdTipoServicio=$_REQUEST["IdTipoServicio"];
   $data = ListarServiciosHospitalizacion_M();
   
   for ($i=0; $i < count($data); $i++) { 	    
				
		$resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdServicio"],				 
			'text'	=> $data[$i]["IdServicio"].'='.$data[$i]["Nombre"]	
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "ListarPacientesProgramacionMedicos") 
{  
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();   
   
   $FechaIngreso=gfecha($_REQUEST["FechaIngreso"]);
   $IdMedico=$_REQUEST["IdMedico"];
   $IdServicio=$_REQUEST["IdServicio"];
   $data = ListarPacientesProgramacionMedicos_M($IdMedico,$FechaIngreso,$IdServicio);   
   
   for ($i=0; $i < count($data); $i++) { 
	   
		$Paciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].', '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];	
		
	    //VALIDAR CUENTA ABIERTA
	    $AtencionesSelecionarPorCuenta=AtencionesSelecionarPorCuenta($data[$i]["IdCuentaAtencion"]); 
	    $IdEstado=$AtencionesSelecionarPorCuenta[0]; //<> 1 cuenta no abierta
	    $estadoCta=$AtencionesSelecionarPorCuenta[1];
				
		$resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdPaciente"].'|'.$data[$i]["IdCuentaAtencion"].'|'.$data[$i]["idFuenteFinanciamiento"].'|'.$IdEstado.'|'.$estadoCta,					 
			'text'	=> $Paciente,
			'DescripcionServicio' => $data[$i]["DescripcionServicio"],
			'IdProgramacion'	=> $data[$i]["IdProgramacion"]
				
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "ListarPacientesEmergencia") 
{ 
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();  
    
   $FechaIngreso=gfecha($_REQUEST["FechaIngreso"]);
   //$IdMedico=$_REQUEST["IdMedico"];
   $IdServicio=$_REQUEST["IdServicio"];
   $data = ListarPacientesEmergencia_M($FechaIngreso,$IdServicio); 
   
   for ($i=0; $i < count($data); $i++) { 		
	   $Paciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].', '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];
	   
	   //VALIDAR CUENTA ABIERTA
	   $AtencionesSelecionarPorCuenta=AtencionesSelecionarPorCuenta($data[$i]["IdCuentaAtencion"]); 
	   $IdEstado=$AtencionesSelecionarPorCuenta[0]; //<> 1 cuenta no abierta
	   $estadoCta=$AtencionesSelecionarPorCuenta[1];
				
	   $resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdPaciente"].'|'.$data[$i]["IdCuentaAtencion"].'|'.$data[$i]["idFuenteFinanciamiento"].'|'.$IdEstado.'|'.$estadoCta,				 
			'text'	=> $Paciente,
			'DescripcionServicio' => $data[$i]["DescripcionServicio"]
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "ListarPacientesHospitalizacion") 
{ 
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();  
   
   $FechaIngreso=gfecha($_REQUEST["FechaIngreso"]);
   //$IdMedico=$_REQUEST["IdMedico"];
   $IdServicio=$_REQUEST["IdServicio"];
   $data = ListarPacientesHospitalizacion_M($FechaIngreso,$IdServicio);
   
   for ($i=0; $i < count($data); $i++) { 		
	   $Paciente=$data[$i]["ApellidoPaterno"].' '.$data[$i]["ApellidoMaterno"].', '.$data[$i]["PrimerNombre"].' '.$data[$i]["SegundoNombre"];
	   
	   //VALIDAR CUENTA ABIERTA
	   $AtencionesSelecionarPorCuenta=AtencionesSelecionarPorCuenta($data[$i]["IdCuentaAtencion"]); 
	   $IdEstado=$AtencionesSelecionarPorCuenta[0]; //<> 1 cuenta no abierta
	   $estadoCta=$AtencionesSelecionarPorCuenta[1];
				
	   $resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdPaciente"].'|'.$data[$i]["IdCuentaAtencion"].'|'.$data[$i]["idFuenteFinanciamiento"].'|'.$IdEstado.'|'.$estadoCta,					 
			'text'	=> $Paciente,
			'DescripcionServicio' => $data[$i]["DescripcionServicio"]
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "AceptarBusquedaPacientes") 
{ 
   $idTipoServicio=$_REQUEST["TipoServicio"];	  
   $Servicio=$_REQUEST["Servicio"];	
   $DescripcionServicio=$_REQUEST["DescripcionServicio"];	
   $FechaIngreso=$_REQUEST["FechaIngreso"];
   $IdMedico=$_REQUEST["IdMedico"];
   
   $IdPaciente=$_REQUEST["IdPaciente"];
   $IdCuentaAtencion=$_REQUEST["IdCuentaAtencion"];	
   $idFuenteFinanciamiento=$_REQUEST["idFuenteFinanciamiento"];	
   
   include('../../MVC_Vista/RecetasMedicas/RegistrarReceta.php');
}

if($_REQUEST["acc"] == "verProductos") 
{    
   if($_REQUEST['criterio']!=NULL){	
   		$IdTipoFinanciamiento=$_REQUEST['IdTipoFinanciamiento'];    
		$Codigo=$_REQUEST['codigo'];
		$Nombre=$_REQUEST['criterio'];
		$ProductoBuscar=ListarCatalogoBienesInsumosM($IdTipoFinanciamiento, $Codigo, $Nombre);		
	}
	include('../../MVC_Vista/RecetasMedicas/verProductos.php');	
}

if($_REQUEST["acc"] == "verServicios") 
{    
   if($_REQUEST['criterio']!=NULL){
	    $IdTipoFinanciamiento=$_REQUEST['IdTipoFinanciamiento'];
		$Codigo=$_REQUEST['codigo'];
		$Nombre=$_REQUEST['criterio'];
		$ServicioBuscar=ListarCatalogoServiciosM($IdTipoFinanciamiento, $Codigo, $Nombre);		
	}
	include('../../MVC_Vista/RecetasMedicas/verServicios.php');	
}

if($_REQUEST["acc"] == "verPaquetes") 
{    
   //if($_REQUEST['criterio']!=NULL){
	    $IdTipoFinanciamiento=$_REQUEST['IdTipoFinanciamiento'];
		$Codigo=$_REQUEST['codigo'];
		$Nombre=$_REQUEST['criterio'];
		$PaqueteBuscar=ListarCatalogoCatalogoPaquetesM($IdTipoFinanciamiento, $Codigo, $Nombre);		
	//}
	include('../../MVC_Vista/RecetasMedicas/verPaquetes.php');	
}

if($_REQUEST["acc"] == "ListarRecetaViaAdministracion") 
{  
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();
   
   $data = ListarRecetaViaAdministracion_M();
   
   for ($i=0; $i < count($data); $i++) { 	    
				
		$resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["IdViaAdministracion"],				 
			'text'	=> $data[$i]["Descripcion"]	
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "ListarRecetaDosis") 
{  
   //inicializando las variables 
   $data = array();
   $resultadodetallado_json = array();
   
   $data = ListarRecetaDosis_M();
   
   for ($i=0; $i < count($data); $i++) { 	    
				
		$resultadodetallado_json[$i] = array(
			'val'	=> $data[$i]["idDosis"],				 
			'text'	=> $data[$i]["NumeroDosis"]	
		);
	}	
	echo json_encode($resultadodetallado_json); 
	exit();		
}

//FILTROS
if($_REQUEST["acc"] == "FiltroProductos") 
{       
	$IdTipoFinanciamiento=$_REQUEST['IdTipoFinanciamiento'];    
	$Codigo=$_REQUEST['codigo'];
	$Nombre=$_REQUEST['criterio'];
	$data=ListarCatalogoBienesInsumosM($IdTipoFinanciamiento, $Codigo, $Nombre);	
		
	for ($i=0; $i < count($data); $i++) { 		
	$PrecioX=$data[$i]['PrecioUnitario']; 
	$Precio=number_format( $PrecioX , 2 , "." , " " ); 	
					
		$resultadodetallado_json[$i] = array(
			'Codigo'		 	=> $data[$i]['Codigo'],			
			'Nombre' 			=> $data[$i]["Nombre"],
			'IdProducto' 	=> $data[$i]["IdProducto"],
			'Precio' 			=> $Precio
		);
	}
	if($resultadodetallado_json!=NULL)
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "FiltroServicios") 
{    
  
	$IdTipoFinanciamiento=$_REQUEST['IdTipoFinanciamiento'];
	$Codigo=$_REQUEST['codigo'];
	$Nombre=$_REQUEST['criterio'];
	$data=ListarCatalogoServiciosM($IdTipoFinanciamiento, $Codigo, $Nombre);		
	
	for ($i=0; $i < count($data); $i++) { 		
	$PrecioX=$data[$i]['PrecioUnitario']; 
	$Precio=number_format( $PrecioX , 2 , "." , " " ); 	
					
		$resultadodetallado_json[$i] = array(
			'Codigo'		 	=> $data[$i]['Codigo'],			
			'Nombre' 			=> $data[$i]["Nombre"],
			'IdProducto' 	=> $data[$i]["IdProducto"],
			'Precio' 			=> $Precio
		);
	}
	if($resultadodetallado_json!=NULL)
	echo json_encode($resultadodetallado_json); 
	exit();		
}

if($_REQUEST["acc"] == "GuardarReceta") 
{
	//RecetaCabecera
	//$idReceta=$_REQUEST['idReceta']; //autoincrement 
		
	if($_REQUEST['FechaReceta']==''){
		$FechaReceta=NULL;
		$FechaRecetaAuditoria=NULL;
	}else{
		$FechaReceta=gfecha($_REQUEST['FechaReceta']).' '.$_REQUEST['HoraReceta'];
		$FechaRecetaAuditoria=$_REQUEST['FechaReceta'].' '.substr($_REQUEST['HoraReceta'],0,5);
	}
	
	$idCuentaAtencion=$_REQUEST['idCuentaAtencion'];
	$idServicioReceta=$_REQUEST['Servicio'];
	$idEstado='1';
	$DocumentoDespacho=NULL;
	$idComprobantePago=NULL;
	$idMedicoReceta=$_REQUEST['IdMedico'];
	$fechaVigencia=gfecha($_REQUEST['fechaVigencia']);
	
	//GUARDAR DATOS AUDITORIA
	$IdEmpleado=$_REQUEST['IdEmpleado'];$Accion='A';//(A=AGREGAR, M=MODIFICAR, E=ELIMINAR)
	$IdRegistro=$_REQUEST['idCuentaAtencion'];$Tabla='RecetaCabecera';
	$idTipoServicio=$_REQUEST['idTipoServicio'];
	if($idTipoServicio=='1'){ //Consulta Externa
		$idListItem='1366'; //RecetasCE
	}else if($idTipoServicio=='2' || $idTipoServicio=='4'){ //Emergencia
		$idListItem='1343'; //RecetasE
	}else if($idTipoServicio=='3'){ //Hospitalizacion
		$idListItem='1344'; //RecetasH
	}	
	$nombrePC=$_REQUEST['nombrePC'];
	$observaciones='('.$FechaRecetaAuditoria.') Paciente : '.$_REQUEST['NroHistoriaClinica'].' - '.$_REQUEST['Paciente'].' (Edad:'.$_REQUEST['EDAD'].')';//EJEMPLO: (22/08/2017 10:57) Paciente : 355187 - CANALES MOLERO MAXIMO (Edad: 66 Años)  
	GuardarAuditoriaAgregarV($IdEmpleado,$Accion,$IdRegistro,$Tabla,$idListItem,$nombrePC,$observaciones); 	
	
	//guardar cabecera Medicamento/Insumo	
	if($_REQUEST['IdProducto1'] != ""){
		$IdPuntoCarga='5'; //Medicamento/Insumo	
		$idRecetaFarmacia=GuardarRecetaCabeceraM($IdPuntoCarga, $FechaReceta, $idCuentaAtencion, $idServicioReceta, $idEstado, $DocumentoDespacho, $idComprobantePago, $idMedicoReceta, $fechaVigencia);			
	}	
	
	//detalle Medicamento/Insumo	
	$n=0;
	for($i=1;$i<=50;$i++){
		$idItem=$_REQUEST['IdProducto'.$i];
		$CantidadPedida = $_REQUEST['CantidadPedida'.$i];		
		$Precio=$_REQUEST['Precio'.$i];
		$Total=$CantidadPedida*$Precio;
		$SaldoEnRegistroReceta=0;
		$SaldoEnDespachoReceta=NULL;
		$CantidadDespachada=NULL;
		$idDosisRecetada=$_REQUEST['idDosisRecetada'.$i];
		$idEstadoDetalle=NULL;
		$MotivoAnulacionMedico=NULL;
		$observaciones=$_REQUEST['Frecuencia'.$i];
		$IdViaAdministracion=$_REQUEST['IdViaAdministracion'.$i];
		
		if($_REQUEST['IdProducto'.$i] != ""){
			$n++;
			//$n_itemdet = $n;
			GuardarRecetaDetalleM($idRecetaFarmacia, $idItem, $CantidadPedida, $Precio, $Total, $SaldoEnRegistroReceta, $SaldoEnDespachoReceta, $CantidadDespachada, $idDosisRecetada, $idEstadoDetalle, $MotivoAnulacionMedico, $observaciones, $IdViaAdministracion);
		}
	}//fin detalle farmacia
	
	//detalle servicio
	$m=0;$todoidRecetaServicio='';
	for($j=1;$j<=50;$j++){
		$idItemS=$_REQUEST['IdProductoS'.$j];
		$CantidadPedidaS = $_REQUEST['CantidadPedidaS'.$j];		
		$PrecioS=$_REQUEST['PrecioS'.$j];
		$TotalS=$CantidadPedidaS*$PrecioS;
		$SaldoEnRegistroRecetaS=0;
		$SaldoEnDespachoRecetaS=NULL;
		$CantidadDespachadaS=NULL;
		$idDosisRecetadaS=$_REQUEST['idDosisRecetada'.$j];
		$idEstadoDetalleS='1';
		$MotivoAnulacionMedicoS=NULL;
		$observacionesS=$_REQUEST['FrecuenciaS'.$j];
		$IdViaAdministracionS=$_REQUEST['IdViaAdministracionS'.$j];
		
		if($_REQUEST['IdProductoS'.$j] != ""){
			$m++;
			//$n_itemdet = $m;
			//guardar cabecera Servicio
			if($_REQUEST['IdPuntoCarga'.$j]!=(isset($_REQUEST['IdPuntoCarga'.($j-1)]) ? $_REQUEST['IdPuntoCarga'.($j-1)] : '') ){ 
				$IdPuntoCarga=$_REQUEST['IdPuntoCarga'.$j]; //Servicios
				$idRecetaServicio=GuardarRecetaCabeceraM($IdPuntoCarga, $FechaReceta, $idCuentaAtencion, $idServicioReceta, $idEstado, $DocumentoDespacho, $idComprobantePago, $idMedicoReceta, $fechaVigencia);
				$todoidRecetaServicio=$todoidRecetaServicio.$idRecetaServicio.'|';
			}	
					
			GuardarRecetaDetalleM($idRecetaServicio, $idItemS, $CantidadPedidaS, $PrecioS, $TotalS, $SaldoEnRegistroRecetaS, $SaldoEnDespachoRecetaS, $CantidadDespachadaS, $idDosisRecetadaS, $idEstadoDetalleS, $MotivoAnulacionMedicoS, $observacionesS, $IdViaAdministracionS);
		}
	}//fin detalle servicio	
	
	/*$mensaje="Receta guardada correctamente";
	print "<script>alert('$mensaje')</script>";							
	include('../../MVC_Vista/RecetasMedicas/Inicio.php');*/
	$IdRecetas=$idRecetaFarmacia.'|'.$todoidRecetaServicio;						
	//include('../../MVC_Vista/RecetasMedicas/ImprimirReceta.php');
	header("location:../../MVC_Vista/RecetasMedicas/ImprimirReceta.php?IdEmpleado=".$_REQUEST['IdEmpleado']."&IdRecetas=".$IdRecetas);	

}

function AtencionesSelecionarPorCuenta($idCuentaAtencion)
{  
   $data = array();
   $AtencionesSelecionarPorCuenta=AtencionesSelecionarPorCuentaM($idCuentaAtencion);
   if($AtencionesSelecionarPorCuenta!=""){
	  $IdEstado=$AtencionesSelecionarPorCuenta[0]['IdEstado']; //<> 1 cuenta no abierta
	  $estadoCta=$AtencionesSelecionarPorCuenta[0]['estadoCta']; 	  
	  $data=array($IdEstado,$estadoCta);
   }
   return $data;   
}

if($_REQUEST["acc"] == "ModificarReceta") 
{     
   include('../../MVC_Vista/RecetasMedicas/ModificarReceta.php');
}

if($_REQUEST["acc"] == "GuardarUpdReceta") 
{  
	$IdReceta=$_REQUEST['IdReceta'];
	$IdPuntoCargaReg=$_REQUEST['IdPuntoCargaReg'];
	/*NOTAS:
		*La cabecera no se elimina, los detalles se eliminan	
	*/
	
	//1. eliminar detalle Receta
	EliminarDetalleReceta($IdReceta);
	
	//Obtener Datos Cabecera 
	if($_REQUEST['FechaReceta']==''){
		$FechaReceta=NULL;
		$FechaRecetaAuditoria=NULL;
	}else{
		$FechaReceta=gfecha($_REQUEST['FechaReceta']).' '.$_REQUEST['HoraReceta'];
		$FechaRecetaAuditoria=$_REQUEST['FechaReceta'].' '.substr($_REQUEST['HoraReceta'],0,5);
	}
	
	$idCuentaAtencion=$_REQUEST['idCuentaAtencion'];
	$idServicioReceta=$_REQUEST['Servicio'];
	$idEstado='1';
	$DocumentoDespacho=NULL;
	$idComprobantePago=NULL;
	$idMedicoReceta=$_REQUEST['IdMedico'];
	$fechaVigencia=gfecha($_REQUEST['fechaVigencia']);	
	
	//2. guardar cabecera Medicamento/Insumo si IdPuntoCargaReg no es igual a 5 	
	if($_REQUEST['IdProducto1'] != "" && $_REQUEST['IdPuntoCargaReg'] != "5"){
		$IdPuntoCarga='5'; //Medicamento/Insumo	
		$idRecetaFarmacia=GuardarRecetaCabeceraM($IdPuntoCarga, $FechaReceta, $idCuentaAtencion, $idServicioReceta, $idEstado, $DocumentoDespacho, $idComprobantePago, $idMedicoReceta, $fechaVigencia);			
	}else{
		$idRecetaFarmacia=$_REQUEST['IdReceta'];
	}
	
	//detalle Medicamento/Insumo	
	$n=0;
	for($i=1;$i<=50;$i++){
		$idItem=$_REQUEST['IdProducto'.$i];
		$CantidadPedida = $_REQUEST['CantidadPedida'.$i];		
		$Precio=$_REQUEST['Precio'.$i];
		$Total=$CantidadPedida*$Precio;
		$SaldoEnRegistroReceta=0;
		$SaldoEnDespachoReceta=NULL;
		$CantidadDespachada=NULL;
		$idDosisRecetada=$_REQUEST['idDosisRecetada'.$i];
		$idEstadoDetalle=NULL;
		$MotivoAnulacionMedico=NULL;
		$observaciones=$_REQUEST['Frecuencia'.$i];
		$IdViaAdministracion=$_REQUEST['IdViaAdministracion'.$i];
		
		if($_REQUEST['IdProducto'.$i] != ""){
			$n++;
			//$n_itemdet = $n;
			//3. guardar detalle Medicamento/Insumo	
			GuardarRecetaDetalleM($idRecetaFarmacia, $idItem, $CantidadPedida, $Precio, $Total, $SaldoEnRegistroReceta, $SaldoEnDespachoReceta, $CantidadDespachada, $idDosisRecetada, $idEstadoDetalle, $MotivoAnulacionMedico, $observaciones, $IdViaAdministracion);
		}
	}//fin detalle farmacia
	
	//detalle servicio
	$m=0;$todoidRecetaServicio='';
	for($j=1;$j<=50;$j++){
		$idItemS=$_REQUEST['IdProductoS'.$j];
		$CantidadPedidaS = $_REQUEST['CantidadPedidaS'.$j];		
		$PrecioS=$_REQUEST['PrecioS'.$j];
		$TotalS=$CantidadPedidaS*$PrecioS;
		$SaldoEnRegistroRecetaS=0;
		$SaldoEnDespachoRecetaS=NULL;
		$CantidadDespachadaS=NULL;
		$idDosisRecetadaS=$_REQUEST['idDosisRecetada'.$j];
		$idEstadoDetalleS='1';
		$MotivoAnulacionMedicoS=NULL;
		$observacionesS=$_REQUEST['FrecuenciaS'.$j];
		$IdViaAdministracionS=$_REQUEST['IdViaAdministracionS'.$j];		
		
		if($_REQUEST['IdProductoS'.$j] != ""){
			$m++;
			//$n_itemdet = $m;			
			//4. guardar cabecera Servicio si IdPuntoCargaReg no es igual a  $_REQUEST['IdPuntoCarga'.$j]
			if( ($_REQUEST['IdPuntoCarga'.$j]!=(isset($_REQUEST['IdPuntoCarga'.($j-1)]) ? $_REQUEST['IdPuntoCarga'.($j-1)] : '')) && $_REQUEST['IdPuntoCargaReg']!=$_REQUEST['IdPuntoCarga'.$j] ){ 
				$IdPuntoCarga=$_REQUEST['IdPuntoCarga'.$j]; //Servicios
				$idRecetaServicio=GuardarRecetaCabeceraM($IdPuntoCarga, $FechaReceta, $idCuentaAtencion, $idServicioReceta, $idEstado, $DocumentoDespacho, $idComprobantePago, $idMedicoReceta, $fechaVigencia);
				$todoidRecetaServicio=$todoidRecetaServicio.$idRecetaServicio.'|';
			}else{				
				$idRecetaServicio=$_REQUEST['IdReceta'];
			}	
			
			//5. guardar detalle servicio		
			GuardarRecetaDetalleM($idRecetaServicio, $idItemS, $CantidadPedidaS, $PrecioS, $TotalS, $SaldoEnRegistroRecetaS, $SaldoEnDespachoRecetaS, $CantidadDespachadaS, $idDosisRecetadaS, $idEstadoDetalleS, $MotivoAnulacionMedicoS, $observacionesS, $IdViaAdministracionS);
		}
	}//fin detalle servicio			
	
	//6. GUARDAR DATOS AUDITORIA MODIFICAR
	$IdEmpleado=$_REQUEST['IdEmpleado'];$Accion='M';//(A=AGREGAR, M=MODIFICAR, E=ELIMINAR)	
	$IdRegistro=$_REQUEST['idCuentaAtencion'];$Tabla='RecetaCabecera';
	$idTipoServicio=$_REQUEST['idTipoServicio'];	
	if($idTipoServicio=='1'){ //Consulta Externa
		$idListItem='1366'; //RecetasCE
	}else if($idTipoServicio=='2' || $idTipoServicio=='4'){ //Emergencia
		$idListItem='1343'; //RecetasE
	}else if($idTipoServicio=='3'){ //Hospitalizacion
		$idListItem='1344'; //RecetasH
	}	
	$nombrePC=gethostbyaddr($_SERVER['REMOTE_ADDR']);
	$observaciones='('.$_REQUEST['NombrePuntoCarga'].': '.$IdReceta.') Paciente : '.$_REQUEST['NroHistoriaClinica'].' - '.$_REQUEST['Paciente'].' (Edad:'.$_REQUEST['EDAD'].')';//EJEMPLO: (Pat.Cli: 33Farm: 34) Paciente : 1120552 - DUEÑAS SANCHEZ SARITA (Edad: 38 Años)    	
	GuardarAuditoriaAgregarV($IdEmpleado,$Accion,$IdRegistro,$Tabla,$idListItem,$nombrePC,$observaciones);	
	
		
	/*$mensaje="Receta Modificada Correctamente";
	print "<script>alert('$mensaje')</script>";
	include('../../MVC_Vista/RecetasMedicas/Inicio.php');*/	
	$IdRecetas=$idRecetaFarmacia.'|'.$todoidRecetaServicio;						
	//include('../../MVC_Vista/RecetasMedicas/ImprimirReceta.php');
	header("location:../../MVC_Vista/RecetasMedicas/ImprimirReceta.php?IdEmpleado=".$_REQUEST['IdEmpleado']."&IdRecetas=".$IdRecetas);	
}

if($_REQUEST["acc"] == "EliminarReceta") 
{ 
	$IdReceta=$_REQUEST['IdReceta'];	  
	$ListarRecetaCabecera=ListarRecetaCabeceraSeleccionarPorIdReceta($IdReceta);	
	$IdPaciente=$ListarRecetaCabecera[0]['IdPaciente'];	
	
	//DATOS CABECERA RECETA		
	$NombrePuntoCarga=$ListarRecetaCabecera[0]['NombrePuntoCarga'];
	
	//DATOS ATENCION
	$IdCuentaAtencion=$ListarRecetaCabecera[0]['idCuentaAtencion'];	
	$idTipoServicio=$ListarRecetaCabecera[0]['IdTipoServicio'];
	
	//DATOS PACIENTE
	$ObtenerDatosPacientes=ObtenerDatosPacientes_M($IdPaciente);
	$NroHistoriaClinica=$ObtenerDatosPacientes[0]["NroHistoriaClinica"];	
	$Paciente=$ObtenerDatosPacientes[0]["ApellidoPaterno"].' '.$ObtenerDatosPacientes[0]["ApellidoMaterno"].', '.$ObtenerDatosPacientes[0]["PrimerNombre"].' '.$ObtenerDatosPacientes[0]["SegundoNombre"];
	$EDAD=str_replace('<BR> A',' AÑOS',CalcularEdad($ObtenerDatosPacientes[0]['FechaNacimiento'])) ;
	$EDAD=str_replace('<BR> M',' MESES',$EDAD) ;
	$EDAD=str_replace('<BR> D',' DIAS',$EDAD) ;
	$EDAD=str_replace('<BR> H',' HORAS',$EDAD) ; 
	
	//GUARDAR DATOS AUDITORIA
	$IdEmpleado=$_REQUEST['IdEmpleado'];$Accion='E';//(A=AGREGAR, M=MODIFICAR, E=ELIMINAR)	
	$IdRegistro=$IdCuentaAtencion;$Tabla='RecetaCabecera';	
	if($idTipoServicio=='1'){ //Consulta Externa
		$idListItem='1366'; //RecetasCE
	}else if($idTipoServicio=='2' || $idTipoServicio=='4'){ //Emergencia
		$idListItem='1343'; //RecetasE
	}else if($idTipoServicio=='3'){ //Hospitalizacion
		$idListItem='1344'; //RecetasH
	}	
	$nombrePC=gethostbyaddr($_SERVER['REMOTE_ADDR']);
	$observaciones='('.$NombrePuntoCarga.': '.$IdReceta.') Paciente : '.$NroHistoriaClinica.' - '.$Paciente.' (Edad:'.$EDAD.')';//EJEMPLO: (Pat.Cli: 33Farm: 34) Paciente : 1120552 - DUEÑAS SANCHEZ SARITA (Edad: 38 Años)    	
	GuardarAuditoriaAgregarV($IdEmpleado,$Accion,$IdRegistro,$Tabla,$idListItem,$nombrePC,$observaciones);
	
	EliminarReceta($IdReceta); 		
	$mensaje="Receta Eliminada Correctamente";
	print "<script>alert('$mensaje')</script>";							
	include('../../MVC_Vista/RecetasMedicas/Inicio.php');
}


?>