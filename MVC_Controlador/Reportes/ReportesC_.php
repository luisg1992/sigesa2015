	<?php
include('../../MVC_Modelo/SistemaM.php');
include('../../MVC_Modelo/ReportesM.php');
include('../../MVC_Modelo/DatosInstitucionesM.php');
include('../../MVC_Modelo/EstadisticasM.php');
 include('../../MVC_Complemento/librerias/Funciones.php');

$ListarFechaServidor=RetornaFechaServidorSQL_C();
 $FechaServidor = $ListarFechaServidor[0][0];
 

/***************************/
/*** Variables Glovales ***/
/**************************/

  // echo $_REQUEST["acc"];

/****************/
/*** ACCIONES ***/
/****************/

		if($_REQUEST["acc"] == "Productividad") 
		{  
		include('../../MVC_Vista/Reportes/ProductividadV.php');
		}

	    if ($_REQUEST["acc"] == "MostrarProductividad") {
		$Anio=$_REQUEST["Anio"];
		$Mes=$_REQUEST["Mes"];
		$Atenciones = SigesaProductividadHoraMedica_Atenciones_M($Anio,$Mes);
		$Horas = SigesaProductividadHoraMedica_Horas_M($Anio,$Mes);
		for ($i=0; $i < count($Atenciones); $i++) { 
			$Productividad[$i] = array(
				'IdDepartamento'	=> $Atenciones[$i]['IdDepartamento'].'C',
				'Especialidad'	=> $Atenciones[$i]['Especialidad'],
				'jan_a'			=> $Atenciones[$i]['jan'],
				'feb_a'			=> $Atenciones[$i]['feb'],
				'mar_a'			=> $Atenciones[$i]['mar'],
				'apr_a'			=> $Atenciones[$i]['apr'],
				'may_a'			=> $Atenciones[$i]['may'],
				'jun_a'			=> $Atenciones[$i]['jun'],
				'jul_a'			=> $Atenciones[$i]['jul'],
				'aug_a'			=> $Atenciones[$i]['aug'],
				'sep_a'			=> $Atenciones[$i]['sep'],
				'oct_a'			=> $Atenciones[$i]['oct'],
				'nov_a'			=> $Atenciones[$i]['nov'],
				'dec_a'			=> $Atenciones[$i]['dec'],
				'jan_h'			=> $Horas[$i]['jan'],
				'feb_h'			=> $Horas[$i]['feb'],
				'mar_h'			=> $Horas[$i]['mar'],
				'apr_h'			=> $Horas[$i]['apr'],
				'may_h'			=> $Horas[$i]['may'],
				'jun_h'			=> $Horas[$i]['jun'],
				'jul_h'			=> $Horas[$i]['jul'],
				'aug_h'			=> $Horas[$i]['aug'],
				'sep_h'			=> $Horas[$i]['sep'],
				'oct_h'			=> $Horas[$i]['oct'],
				'nov_h'			=> $Horas[$i]['nov'],
				'dec_h'			=> $Horas[$i]['dec'],
				'jan_p'			=> Division($Atenciones[$i]['jan'],$Horas[$i]['jan']),
				'feb_p'			=> Division($Atenciones[$i]['feb'],$Horas[$i]['feb']),
				'mar_p'			=> Division($Atenciones[$i]['mar'],$Horas[$i]['mar']),
				'apr_p'			=> Division($Atenciones[$i]['apr'],$Horas[$i]['apr']),
				'may_p'			=> Division($Atenciones[$i]['may'],$Horas[$i]['may']),
				'jun_p'			=> Division($Atenciones[$i]['jun'],$Horas[$i]['jun']),
				'jul_p'			=> Division($Atenciones[$i]['jul'],$Horas[$i]['jul']),
				'aug_p'			=> Division($Atenciones[$i]['aug'],$Horas[$i]['aug']),
				'sep_p'			=> Division($Atenciones[$i]['sep'],$Horas[$i]['sep']),
				'oct_p'			=> Division($Atenciones[$i]['oct'],$Horas[$i]['oct']),
				'nov_p'			=> Division($Atenciones[$i]['nov'],$Horas[$i]['nov']),
				'dec_p'			=> Division($Atenciones[$i]['dec'],$Horas[$i]['dec']),
				'Total_Atenciones'	=> $Atenciones[$i]['jan']+$Atenciones[$i]['feb']+$Atenciones[$i]['mar']+$Atenciones[$i]['apr']+$Atenciones[$i]['may']+$Atenciones[$i]['jun']+$Atenciones[$i]['jul']+$Atenciones[$i]['aug']+$Atenciones[$i]['sep']+$Atenciones[$i]['oct']+$Atenciones[$i]['nov']+$Atenciones[$i]['dec'],
				'Total_Horas'			=> $Horas[$i]['jan']+$Horas[$i]['feb']+$Horas[$i]['mar']+$Horas[$i]['apr']+$Horas[$i]['may']+$Horas[$i]['jun']+$Horas[$i]['jul']+$Horas[$i]['aug']+$Horas[$i]['sep']+$Horas[$i]['oct']+$Horas[$i]['nov']+$Horas[$i]['dec']
				);
		}
		
		$Atenciones_Departamento = SigesaProductividadHoraMedica_Atenciones_Departamento_M($Anio,$Mes);
		$Horas_Departamento = SigesaProductividadHoraMedica_Departamento_M($Anio,$Mes);
		for ($i=0; $i < count($Atenciones_Departamento); $i++) { 
			$Productividad_Departamento[$i] = array(
				'IdDepartamento'	=> $Atenciones_Departamento[$i]['IdDepartamento'].'A',
				'Especialidad'	=> strtoupper('Departamento de <br>'.$Atenciones_Departamento[$i]['Departamento']),
				'jan_a'			=> $Atenciones_Departamento[$i]['jan'],
				'feb_a'			=> $Atenciones_Departamento[$i]['feb'],
				'mar_a'			=> $Atenciones_Departamento[$i]['mar'],
				'apr_a'			=> $Atenciones_Departamento[$i]['apr'],
				'may_a'			=> $Atenciones_Departamento[$i]['may'],
				'jun_a'			=> $Atenciones_Departamento[$i]['jun'],
				'jul_a'			=> $Atenciones_Departamento[$i]['jul'],
				'aug_a'			=> $Atenciones_Departamento[$i]['aug'],
				'sep_a'			=> $Atenciones_Departamento[$i]['sep'],
				'oct_a'			=> $Atenciones_Departamento[$i]['oct'],
				'nov_a'			=> $Atenciones_Departamento[$i]['nov'],
				'dec_a'			=> $Atenciones_Departamento[$i]['dec'],
				'jan_h'			=> $Horas_Departamento[$i]['jan'],
				'feb_h'			=> $Horas_Departamento[$i]['feb'],
				'mar_h'			=> $Horas_Departamento[$i]['mar'],
				'apr_h'			=> $Horas_Departamento[$i]['apr'],
				'may_h'			=> $Horas_Departamento[$i]['may'],
				'jun_h'			=> $Horas_Departamento[$i]['jun'],
				'jul_h'			=> $Horas_Departamento[$i]['jul'],
				'aug_h'			=> $Horas_Departamento[$i]['aug'],
				'sep_h'			=> $Horas_Departamento[$i]['sep'],
				'oct_h'			=> $Horas_Departamento[$i]['oct'],
				'nov_h'			=> $Horas_Departamento[$i]['nov'],
				'dec_h'			=> $Horas_Departamento[$i]['dec'],
				'jan_p'			=> Division($Atenciones_Departamento[$i]['jan'],$Horas_Departamento[$i]['jan']),
				'feb_p'			=> Division($Atenciones_Departamento[$i]['feb'],$Horas_Departamento[$i]['feb']),
				'mar_p'			=> Division($Atenciones_Departamento[$i]['mar'],$Horas_Departamento[$i]['mar']),
				'apr_p'			=> Division($Atenciones_Departamento[$i]['apr'],$Horas_Departamento[$i]['apr']),
				'may_p'			=> Division($Atenciones_Departamento[$i]['may'],$Horas_Departamento[$i]['may']),
				'jun_p'			=> Division($Atenciones_Departamento[$i]['jun'],$Horas_Departamento[$i]['jun']),
				'jul_p'			=> Division($Atenciones_Departamento[$i]['jul'],$Horas_Departamento[$i]['jul']),
				'aug_p'			=> Division($Atenciones_Departamento[$i]['aug'],$Horas_Departamento[$i]['aug']),
				'sep_p'			=> Division($Atenciones_Departamento[$i]['sep'],$Horas_Departamento[$i]['sep']),
				'oct_p'			=> Division($Atenciones_Departamento[$i]['oct'],$Horas_Departamento[$i]['oct']),
				'nov_p'			=> Division($Atenciones_Departamento[$i]['nov'],$Horas_Departamento[$i]['nov']),
				'dec_p'			=> Division($Atenciones_Departamento[$i]['dec'],$Horas_Departamento[$i]['dec']),
				'Total_Atenciones'	=> $Atenciones_Departamento[$i]['jan']+$Atenciones_Departamento[$i]['feb']+$Atenciones_Departamento[$i]['mar']+$Atenciones_Departamento[$i]['apr']+$Atenciones_Departamento[$i]['may']+$Atenciones_Departamento[$i]['jun']+$Atenciones_Departamento[$i]['jul']+$Atenciones_Departamento[$i]['aug']+$Atenciones_Departamento[$i]['sep']+$Atenciones_Departamento[$i]['oct']+$Atenciones_Departamento[$i]['nov']+$Atenciones_Departamento[$i]['dec'],
				'Total_Horas'			=> $Horas_Departamento[$i]['jan']+$Horas_Departamento[$i]['feb']+$Horas_Departamento[$i]['mar']+$Horas_Departamento[$i]['apr']+$Horas_Departamento[$i]['may']+$Horas_Departamento[$i]['jun']+$Horas_Departamento[$i]['jul']+$Horas_Departamento[$i]['aug']+$Horas_Departamento[$i]['sep']+$Horas_Departamento[$i]['oct']+$Horas_Departamento[$i]['nov']+$Horas_Departamento[$i]['dec']
				);
		}
		
		$result = array_merge($Productividad, $Productividad_Departamento);
		
	   function cmp_function($a, $b) {
		  if ($a == $b) return 0;
		  return ($a < $b) ? -1 : 1;
	   }
   
	   usort($result, "cmp_function");
		echo json_encode($result);
		exit();
	}


	

if($_REQUEST["acc"] == "GestionCaja") 
{  
include('../../MVC_Vista/Caja/CajaV.php');
}
		

if($_REQUEST["acc"] == "VistaReporte") 
		{   
		     
              
			 $Usuario=$_REQUEST["Usuario"];
			  $IdReporte=$_REQUEST["IdReporte"];
		     $NombreReporte=$_REQUEST["NombreReporte"];
		$ListarCajeros= SigesaListarCajeros_C();  
		
		include('../../MVC_Vista/Reportes/ReporteVisualizar.php');
		}



if ($_REQUEST["acc"] == "Busqueda Paciente X Servicio") {
	include("../../MVC_Vista/BSC/pacientes.php");
}

if ($_REQUEST["acc"] == "Programacion Medica") {
	include("../../MVC_Vista/BSC/medicos.php");	
}
if ($_REQUEST["acc"] == "Detalle X Centro de Costo") {
	include("../../MVC_Vista/BSC/r_centro_costo.php");
}


if ($_REQUEST["acc"] == "Datos Emergencia") {
	include("../../MVC_Vista/BSC/ReporteFuas.php");
}
 
if ($_REQUEST["acc"] == "SOAT") {

	$TiposServicios=ListarTiposServicios_C();
	$FuentesFinanciamiento=ListarFuentesFinanciamiento_C();
	include("../../MVC_Vista/Reportes/ReporteSoat.php");
 }
 
if($_REQUEST["acc"] == "Selected_IdDepartamento")
{   
	
		$elegido=$_REQUEST["elegido"];
		echo "<option value=''>Todos</option>"; 									 
		$resultados=ListarEspecialidadesM($elegido);								 
		if($resultados!=NULL)
		{
			for ($i=0; $i < count($resultados); $i++) 
			{	
			$html .= "<option value=".$resultados[$i]["IdEspecialidad"].">".mb_strtoupper($resultados[$i]["Nombre"])."</option>";
			}
		}
		
		echo $html;
		
}

	 
	 
if ($_REQUEST["acc"] == "Highchart_Generar_Hora_Medico")
{
	$contenido=''; 
	$IdEspecialidad=$_REQUEST["IdEspecialidad"];
	$Anio=$_REQUEST["Anio"];
	$Mes=$_REQUEST["Mes"];
	$TipoBusqueda=$_REQUEST["Tipo_Busqueda"];
	$Listar_Hora_Medico=Hora_Medico_M($IdEspecialidad,$Anio,$Mes,$TipoBusqueda);
		if($Listar_Hora_Medico != NULL)
		{
						$a=0;
						$cero='0';
						$v='"';
						$contenido.=" ";
						foreach($Listar_Hora_Medico as $item)
						{
						if($a==0)
						{$contenido.= "[";
						}else
						{
					    $contenido.=",[";
						}
						$contenido.=$v.$item['Medico'].$v.",";
						$contenido.=$item['HoraMedico'].' ';
						$contenido.="]";
						$a++;
						}
						$contenido.="";
						
		}
		
		echo json_encode(' ['.$contenido.'] ');	
}	 
	 
	 
	 
	 
	 
if ($_REQUEST["acc"] == "Highchart_Citas_Vendidas_Citas_Ofertadas_Periodo")
{
	$contenido=''; 
	$Fecha_Inicial_Reporte=sqlfecha_devolver($_REQUEST["Fecha_Inicial_Reporte"]);
	$Fecha_Final_Reporte=sqlfecha_devolver($_REQUEST["Fecha_Final_Reporte"]);
	$ListarCitasxFuenteFinanciamiento=Citas_Vendidas_Citas_Ofertadas_Periodo_M($Fecha_Inicial_Reporte,$Fecha_Final_Reporte);
		if($ListarCitasxFuenteFinanciamiento != NULL)
		{
						$a=0;
						$cero='0';
						$v='"';
						foreach($ListarCitasxFuenteFinanciamiento as $item)
						{
						if($a==0)
						{$contenido.= "{";
						}else
						{
					    $contenido.=",{";
						}
						$contenido.=$v."name".$v.": ".$v.$item['Registro'].$v.",";
						$contenido.=$v."data".$v.":[ ";
						$contenido.= $item['1'].',';
						$contenido.= $item['2'].',';
						$contenido.= $item['3'].',';
						$contenido.= $item['4'].',';
						$contenido.= $item['5'].',';
						$contenido.= $item['53'].',';
						$contenido.= $item['55'].',';
						$contenido.= $item['56'].',';
						$contenido.= $item['57'].'';
						$contenido.="]}";
						$a++;
						}
						
		}
		
		echo json_encode('['.$contenido.']');	
}


 if ($_REQUEST["acc"] == "Highchart_VS_Citas") {
    $contenido=''; 
	$IdDepartamento=$_REQUEST["IdDepartamento"];
	$IdEspecialidad=$_REQUEST["IdEspecialidad"];
	$Anio=$_REQUEST["Anio"];
	$Mes=$_REQUEST["Mes"];
	$ListarCitas_vs_Citas=CitasVendidas_VS_Citas_M($IdDepartamento,$IdEspecialidad,$Anio,$Mes);
					if($ListarCitas_vs_Citas != NULL)
					{         
					$a=0;
					$cero='0';
					$v='"';
						foreach($ListarCitas_vs_Citas as $item)
						{
						if($a==0)
						{$contenido.= "{";
						}else
						{
					    $contenido.=",{";
						}
						$contenido.=$v."name".$v.": ".$v.$item['Registro'].$v.",";
						$contenido.=$v."data".$v.":[ ";
						if($item['1']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['1'].',';
						}
						if($item['2']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['2'].',';
						}
						if($item['3']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['3'].',';
						}
						if($item['4']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['4'].',';
						}
						if($item['5']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['5'].',';
						}
						if($item['6']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['6'].',';
						}
						if($item['7']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['7'].' , ';
						}
						if($item['8']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['8'].',';
						}
						if($item['9']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['9'].',';
						}
						if($item['10']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['10'].',';
						}
						if($item['11']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['11'].',';
						}
						if($item['12']==NULL)
						{
						$contenido.= $cero." ]";
						}
						else{
						$contenido.= $item['12'].' ]';
						}
						$contenido.="}";
						$a++;
						}
					 }
					 else
					{	
						$contenido.= '{ "name": "-", "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},{"name": "-", "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},{"name": "-", "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}';	 
					}
					 
					 
					echo json_encode('['.$contenido.']');
					
	
 }








 
 if ($_REQUEST["acc"] == "Highchart_Mes") {
    $contenido=''; 
	$IdDepartamento=$_REQUEST["IdDepartamento"];
	$IdEspecialidad=$_REQUEST["IdEspecialidad"];
	$Anio=$_REQUEST["Anio"];
	$Mes=$_REQUEST["Mes"];
	$ListarCitasxFuenteFinanciamiento=CitasVendidasxFuenteFinaciamiento_M($IdDepartamento,$IdEspecialidad,$Anio,$Mes);
					if($ListarCitasxFuenteFinanciamiento != NULL)
					{         
					$a=0;
					$cero='0';
					$v='"';
						foreach($ListarCitasxFuenteFinanciamiento as $item)
						{
						if($a==0)
						{$contenido.= "{";
						}else
						{
					    $contenido.=",{";
						}
						$contenido.=$v."name".$v.": ".$v.$item['Descripcion'].$v.",";
						$contenido.=$v."data".$v.":[ ";
						if($item['1']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['1'].',';
						}
						if($item['2']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['2'].',';
						}
						if($item['3']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['3'].',';
						}
						if($item['4']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['4'].',';
						}
						if($item['5']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['5'].',';
						}
						if($item['6']==NULL)
						{
						$contenido.= $cero.",";
						}
						else{
						$contenido.= $item['6'].',';
						}
						if($item['7']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['7'].' , ';
						}
						if($item['8']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['8'].',';
						}
						if($item['9']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['9'].',';
						}
						if($item['10']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['10'].',';
						}
						if($item['11']==NULL)
						{
						$contenido.= $cero." , ";
						}
						else{
						$contenido.= $item['11'].',';
						}
						if($item['12']==NULL)
						{
						$contenido.= $cero." ]";
						}
						else{
						$contenido.= $item['12'].' ]';
						}
						$contenido.="}";
						$a++;
						}
					 }
					 else
					{	
						$contenido.= '{ "name": "PARTICULAR", "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},{"name": "SIS", "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},{"name": "SOAT", "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}';	 
					}
					 
					 
					echo json_encode('['.$contenido.']');
					
	
 }
 
 
  if ($_REQUEST["acc"] == "Exportar_CitasVendidasxFuenteFinaciamiento") {
	 
	$IdDepartamento=$_REQUEST["IdDepartamento"];
	$IdEspecialidad=$_REQUEST["IdEspecialidad"];
	$Anio=$_REQUEST["Anio"];
	$Mes=$_REQUEST["Mes"];	
	$contenido  = "<html>";
	$contenido .= "<head>";
	$contenido .= "<title>CONSOLIDADO ESTADO DE CUENTA PACIENTE</title>";
	$contenido .= "<style>";
	$contenido .= "*{";
	$contenido .= "font-family: 'CALIBRI';";
	$contenido .= "font-size: 12px;";
	$contenido .= "margin:0;";
    $contenido .= "padding: 0;";
    $contenido .= "}";
    $contenido .= "@media print{
    	font-family: 'CALIBRI';
		font-size: 12px;
		margin:0;
        padding: 0;
    }";
	$contenido .= "</style>";
	$contenido .= "</head>";
	$contenido .= "<br>";
	$contenido .= "<br>";
	$contenido .= "<div style='padding:500px;background:black'>";
	$contenido .= "<table style='border:#727070 2px solid; margin:100px; padding:500px'>";
	$item=CitasVendidasxFuenteFinaciamiento_M($IdDepartamento,$IdEspecialidad,$Anio,$Mes);
	$contenido .= "<tr>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Seguro</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Enero</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Febrero</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Marzo</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Abril</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Mayo</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Junio</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Julio</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Agosto</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Setiembre</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Octubre</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Noviembre</td>";
	$contenido .= "<td  style='border:#727070 1px solid; font-size:14px; color:black; font-weight:bold; text-align:center' width=90>Diciembre</td>";
	$contenido .= "</tr>";
	
		for ($i=0; $i < count($item); $i++) { 
		$contenido .= "<tr>";
		$contenido .= "<td  style='border:#727070 1px solid;'>".$item[$i]['Descripcion']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['1']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['2']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['3']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['4']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['5']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['6']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['7']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['8']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['9']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['10']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['11']."</td><td  style='border:#727070 1px solid;'>".$item[$i]['12']."</td>";
		$contenido .= "</tr>";	
		}
	$contenido .= "</table>";
	$contenido .= "</div>";
	$contenido .= "</font>";
	$contenido .= "</body>";
	$contenido .= "</html> ";

	echo json_encode($contenido);
	exit();
 
 }
/****************/
/*** FUNCIONES ***/
/****************/

 function SigesaCajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas_C($FechaInicio,$FechaFin){
	return SigesaCajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas_M($FechaInicio,$FechaFin);
	  }
	  
	  	function RetornaFechaServidorSQL_C(){	
		return RetornaFechaServidorSQL_M(); 
		}
		
function EmpleadosSeleccionarPorIdEmpleado_C($IdEmpleado){
	return EmpleadosSeleccionarPorIdEmpleado_M($IdEmpleado);
	}
	
function SigesaListarCajeros_C(){
	return SigesaListarCajeros_M();
	}

function ListarTiposServicios_C(){
	return ListarTiposServicios_M();
}

function ListarFuentesFinanciamiento_C(){
	return ListarFuentesFinanciamiento_M();
}

?>