<?php

	include('../../MVC_Modelo/LaboratorioM.php');
	include('../../MVC_Modelo/HistoriaClinicaM.php');
	include('../../MVC_Modelo/EmpleadosM.php');
	include('../../MVC_Modelo/FactConfig.php');
	include('../../MVC_Complemento/librerias/Funciones.php');

	/***************************/
	/*** Variables Glovales ***/
	/**************************/

   

	/****************/
	/*** ACCIONES ***/
	/****************/
	 
	if($_REQUEST["acc"] == "Empleado") // MOSTRAR: OrdenesLaboratorio
	{  
		include('../../MVC_Vista/Seguridad/Empleados.php');
	 
	}


	
	// MOSTRAR: Lista de los Empleados
	
	if($_POST["acc"] == "ListarEmpleados")
	{
	 
	if($_REQUEST["NroDocumento"]!=NULL){$NroDocumento=$_REQUEST["NroDocumento"];}else{$NroDocumento='%';}
	if($_REQUEST["Nombres"]!=NULL){$Nombres=$_REQUEST["Nombres"];}else{$Nombres='%';}
	if($_REQUEST["ApellidoPaterno"]!=NULL){$ApellidoPaterno=$_REQUEST["ApellidoPaterno"];}else{$ApellidoPaterno='%';}
	if($_REQUEST["ApellidoMaterno"]!=NULL){$ApellidoMaterno=$_REQUEST["ApellidoMaterno"];}else{$ApellidoMaterno='%';}

	
	$ListarEmpleados=Listar_Empleado_C($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno);
    include("../../MVC_Vista/Seguridad/ListaEmpleado.php");
 
	}

	
	// MOSTRAR: Lista de los Empleados Pendientes
	
	if($_POST["acc"] == "ListarEmpleadosPendientes")
	{
	 
	if($_REQUEST["NroDocumento"]!=NULL){$NroDocumento=$_REQUEST["NroDocumento"];}else{$NroDocumento='%';}
	if($_REQUEST["Nombres"]!=NULL){$Nombres=$_REQUEST["Nombres"];}else{$Nombres='%';}
	if($_REQUEST["ApellidoPaterno"]!=NULL){$ApellidoPaterno=$_REQUEST["ApellidoPaterno"];}else{$ApellidoPaterno='%';}
	if($_REQUEST["ApellidoMaterno"]!=NULL){$ApellidoMaterno=$_REQUEST["ApellidoMaterno"];}else{$ApellidoMaterno='%';}

	
	$Listar_Empleado_Pendientes=Listar_Empleado_Pendientes_C($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno);
	include("../../MVC_Vista/Seguridad/ListaEmpleadoPendientes.php");
 
	}
	
	
	
	
	
	
	
		//MOSTRAR: Accion para crear un Empleado Sigesa
    if ($_POST["acc"] == "Agregar_Empleado")
	{
			
		
    if($_REQUEST['Clave']!=NULL ) 
	{
	   	$Clave=$_REQUEST['Clave'];
		$Id_Empleado=$_REQUEST['Id_Empleado'];
		$Mostrar_Empleado_Id=Mostrar_Empleado_Sigesa_IdEmpleado_C($Id_Empleado);
		if($Mostrar_Empleado_Id != NULL)
		{
				$mensaje = "Empleado_Existente";
				include("../../MVC_Vista/Seguridad/Emergentes_Mensajes.php");	
		}
		else
		{
		$Agregar_Empleado=Agregar_Empleado_Sigesa_C($Clave,$Id_Empleado);	
				$mensaje = "Empleado_Agregado";
				include("../../MVC_Vista/Seguridad/Emergentes_Mensajes.php");			
		}	
	}		
	
	}
	
	
	
	//MOSTRAR: Accion para Modificar la Contraseña de un Empleado
    if ($_POST["acc"] == "Modificar_Empleado")
	{		
		if($_REQUEST['Clave']!=NULL ) 
		{
			$Clave=$_REQUEST['Clave'];
			$Id_Empleado=$_REQUEST['IdEmpleado'];
			$Modificar_Empleado=Modificar_Empleado_Sigesa_C($Clave,$Id_Empleado);	
			$mensaje = "Empleado_Modificado";
			include("../../MVC_Vista/Seguridad/Emergentes_Mensajes.php");			
		}		
	}
	
	
	

	//MOSTRAR: Accion para Editar un Empleado	
	
	if($_GET["acc"] == "EditarEmpleado")
	{
	$IdEmpleado = $_GET['IdEmpleado'];
	$Datos_Empleado=Mostrar_Empleado_IdEmpleado_C($IdEmpleado);
    include("../../MVC_Vista/Seguridad/Modificar_Empleado.php");
	}
	

	//MOSTRAR: Accion para Eliminar un Empleado
	
	if ($_POST["acc"] == "Eliminar_Empleado")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
    $Eliminar_Empleado=Eliminar_Empleado_Id_C($IdEmpleado);
	}
	
	//MOSTRAR: Accion para Eliminar un Empleado
	
	if ($_POST["acc"] == "Resetear_Empleado")
	{
	$IdEmpleado=$_REQUEST['IdEmpleado'];
    $Resetear_Empleado=Resetear_Empleado_Id_C($IdEmpleado);
	}
	
	

	function Listar_Empleado_C($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno){
	return Listar_Empleado_M($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno);
	}
	
	function Listar_Empleado_Pendientes_C($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno){
	return Listar_Empleado_Pendientes_M($NroDocumento,$Nombres,$ApellidoPaterno,$ApellidoMaterno);
	}

	function Agregar_Empleado_Sigesa_C($Clave,$Id_Empleado){
	return Agregar_Empleado_Sigesa_M($Clave,$Id_Empleado);
	}

	
	function Modificar_Empleado_Sigesa_C($Clave,$Id_Empleado){
	return Modificar_Empleado_Sigesa_M($Clave,$Id_Empleado);
	}

	
	function Mostrar_Empleado_IdEmpleado_C($IdEmpleado){
	return Mostrar_Empleado_IdEmpleado_M($IdEmpleado);
	}

	
	
	function Mostrar_Empleado_Sigesa_IdEmpleado_C($IdEmpleado){
	return Mostrar_Empleado_Sigesa_IdEmpleado_M($IdEmpleado);
	}
	
	function Eliminar_Empleado_Id_C($IdEmpleado){
	return Eliminar_Empleado_Id_M($IdEmpleado);
	}	
	
	function Resetear_Empleado_Id_C($IdEmpleado){
	return Resetear_Empleado_Id_M($IdEmpleado);
	}	
	
	
	
	
	
?>