<?php
//ini_set('error_reporting',0);//para xamp
include('../../MVC_Modelo/SusaludM.php');
include('../../MVC_Complemento/librerias/Funciones.php');
include('ClassArchivo.php');
/***************************/
/*** Variables Glovales ***/
/**************************/

   

/****************/
/*** ACCIONES ***/
/****************/

if($_REQUEST["acc"] == "Exportar_txt") // MOSTRAR: Formulario InicioExportarTxt
{  
	include('../../MVC_Vista/Susalud/InicioExportarTxt.php'); 
}

if($_REQUEST["acc"] == "VerReporte") // MOSTRAR: Formulario VerReporte segun el tipo de Reporte
{  
	$tiporep=$_REQUEST["tiporep"];
	$Mes=$_REQUEST["Mes"];
	$Anio=$_REQUEST["Anio"];	
	include('../../MVC_Vista/Susalud/VerReporte'.$tiporep.'.php'); 	
}


if($_REQUEST["acc"] == "VistaPrevia") 
{ 
	 //RECUPERAR DATOS
	 $tiporep=$_REQUEST["tiporep"]; 
	 if($tiporep!='ACCIDENTESTRANSITO' && $tiporep!='HORAMEDICO'  && $tiporep!='CITAS' && $tiporep!='HOSPIFINDEMES'){
	 	$Anio=$_REQUEST["Anio"];
	 	$Mes=$_REQUEST["Mes"];	 
	 	$CodIpress=$_REQUEST["CodIpress"]; 
	 	$CodUgiPres=$_REQUEST["CodUgiPres"]; 
	 }
	 
	 if($tiporep=='A0'){
		 $NroConsFisFunc=$_REQUEST["NroConsFisFunc"]; 
		 $TotAxu=$_REQUEST["TotAxu"]; 
		 $TotOtrosProf=$_REQUEST["TotOtrosProf"]; 
		 $TotAmbOper=$_REQUEST["TotAmbOper"];
		 
	 }else if($tiporep=='OTRO2'){
		  $IdServicio=$_REQUEST["IdServicio"];
		  $IdEspecialidad=$_REQUEST["IdEspecialidad"];
		   
	 }else if($tiporep=='HOSPIFINDEMES'){
		   $fecha_fin=gfecha($_REQUEST["fecha_fin"]);
		   
	 }else if($tiporep=='CONSUMOSERVICIOS'){
		 $fecha_inicio=gfecha($_REQUEST["fecha_inicio"]);
		 $fecha_fin=gfecha($_REQUEST["fecha_fin"]); 
		 $IdServicio=$_REQUEST["IdServicio"];
		 $IdCentroCosto=$_REQUEST["IdCentroCosto"];
		 $codigoCPT=$_REQUEST["codigoCPT"];
		 
	 }else if($tiporep=='ACCIDENTESTRANSITO'){
		 $fecha_inicio=gfecha($_REQUEST["fecha_inicio"]);
		 $fecha_fin=gfecha($_REQUEST["fecha_fin"]); 
		 
	 }else if($tiporep=='HORAMEDICO'){
		 $fecha_inicio=gfecha($_REQUEST["fecha_inicio"]);
		 $fecha_fin=gfecha($_REQUEST["fecha_fin"]); 
		 $IdDepartamentoChk=$_REQUEST["IdDepartamentoChk"]; 
		 $IdEspecialidadChk=$_REQUEST["IdEspecialidadChk"];
		 $IdServicioChk=$_REQUEST["IdServicioChk"];
		 
	 }else if($tiporep=='CITAS'){
		 $fecha_inicio=gfecha($_REQUEST["fecha_inicio"]);
		 $fecha_fin=gfecha($_REQUEST["fecha_fin"]); 		
	 }
	 	 
	//TIPO LINK vistaprevia O exportartxt
	
	if($_REQUEST["tipolink"]=='vistaprevia'){		
		include('../../MVC_Vista/Susalud/VistaPrevia'.$tiporep.'.php');
		 
	}else if($_REQUEST["tipolink"]=='exportarexcelEspecialidad'){
		if($IdEspecialidad=='todos'){
			require '../../MVC_Vista/Susalud/TablaExcelIngEgrHospEspe.php';
		}else{
			$name = $Anio.'_'.$Mes.'_'.'TA'.'Hospitalizacion'.$IdEspecialidad;		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			require '../../MVC_Vista/Susalud/TablaExcelIngEgrHospEspeSele.php';
		}							
		 
	}else if($_REQUEST["tipolink"]=='exportarexcel'){
		
		if($tiporep=='OTRO1'){			
			$name = $Anio.'_'.$Mes.'_'.'TA'.'EmergenciaSEM';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			//echo $_REQUEST['datos_a_enviar'];
			require '../../MVC_Vista/Susalud/TablaExcelEmergencia.php';
			
		}else if($tiporep=='EMERGENCIAMENORES29DIAS'){			
			$name = $Anio.'_'.$Mes.'_'.'TA'.'EmergenciaMenores29Dias';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			//echo $_REQUEST['datos_a_enviar'];
			//require '../../MVC_Vista/Susalud/TablaExcelEmergenciaMenores29Diasex.php';	
			require '../../MVC_Vista/Susalud/TablaExcelEmergenciaMenores29Dias.php';
					
		}else if($tiporep=='OTRO2'){			
			$name = $Anio.'_'.$Mes.'_'.'TA'.'Hospitalizacion'.$IdServicio;		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			//echo $_REQUEST['datos_a_enviar'];
			if($IdServicio=='todos'){
				require '../../MVC_Vista/Susalud/TablaExcelIngEgrHospServ.php';	
			}else{
				require '../../MVC_Vista/Susalud/TablaExcelIngEgrHospServSele.php';
			}
					
		}else if($tiporep=='HOSPIFINDEMES'){			
			require '../../MVC_Vista/Susalud/TablaExcelHospiFindeMes.php';	
					
		}else if($tiporep=='DIAGHOSP'){			
			$name = $Anio.'_'.$Mes.'_'.'TA'.'DiagnosticosHospitalizacion';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			//echo $_REQUEST['datos_a_enviar'];
			require '../../MVC_Vista/Susalud/TablaExcelDiagHosp.php';
						
		}else if($tiporep=='CONSUMOSERVICIOS'){						
			/*$name = $Anio.'_'.$Mes.'_'.'TA'.'ConsumoServicios';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");*/			
			require '../../MVC_Vista/Susalud/TablaExcelConsumoServicios.php';	
					 	
		}else if($tiporep=='ACCIDENTESTRANSITO'){
		    $name = $fecha_inicio.'_'.$fecha_fin.'_'.'TA'.'AccidentesTransito';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			require '../../MVC_Vista/Susalud/TablaExcelAccidentesTransito.php';	
				
	 	}else if($tiporep=='HORAMEDICO'){
		    $name = $fecha_inicio.'_'.$fecha_fin.'_'.'TA'.'HoraMedico';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			require '../../MVC_Vista/Susalud/TablaExcelHoraMedico.php';		
			
	 	}else if($tiporep=='CITAS'){
		    /*$name = $fecha_inicio.'_'.$fecha_fin.'_'.'TA'.'CITAS';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");*/
			require '../../MVC_Vista/Susalud/TablaExcelCITAS.php';	
				
	 	}else if($tiporep=='IngresosHospitalizadosUCIPediatricos'){
		    /*$name = $fecha_inicio.'_'.$fecha_fin.'_'.'TA'.'CITAS';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");*/
			require '../../MVC_Vista/Susalud/TablaExcelIngresosHospitalizadosUCIPediatricos.php';	
				
	 	}
		
	}else if($_REQUEST["tipolink"]=='exportarexcelEstadistica'){
		
		if($tiporep=='OTRO1'){			
			$name = $Anio.'_'.$Mes.'_'.'TA'.'EmergenciaEstadistica';		
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: filename=$name.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			//echo $_REQUEST['datos_a_enviar'];
			require '../../MVC_Vista/Susalud/TablaExcelEmergenciaEstadistica.php';			
		}
		
	}else if($_REQUEST["tipolink"]=='exportarDBF'){
		
		if($tiporep=='OTRO1'){			
			require '../../MVC_Vista/Susalud/ArrayDBFEmergenciaSEM.php';
					
		}else if($tiporep=='DIAGHOSP'){	
			$RENIPRESS = $_REQUEST["RENIPRESS"];			
			$E_UBIG = $_REQUEST["E_UBIG"];			
			$E_CDPTO = substr($E_UBIG,0,2);
			$E_CPROV = substr($E_UBIG,2,2);
			$E_CDIST = substr($E_UBIG,4,2);				
			$COD_DISA = $_REQUEST["COD_DISA"];
			$COD_RED = $_REQUEST["COD_RED"];
			$COD_MRED = $_REQUEST["COD_MRED"];		
			require '../../MVC_Vista/Susalud/ArrayDBFHospitalizacion.php';
						
		}
		
	}else if($_REQUEST["tipolink"]=='exportartxt'){
				
		$le=$CodIpress.'_'.$Anio.'_'.$Mes.'_'.'TA'.$tiporep.'.txt';
		$fa=fopen($le, "w+");
		fwrite($fa,"");
		fclose($fa);
		$mensajes = Archivo::getInstancia($le); //Direcion y Nombre Del Archivo
		
		 if($tiporep=='A0'){
			 $resultados = ListarRecursosSaludM($Anio, $Mes, $CodIpress, $CodUgiPres, $NroConsFisFunc, $TotAxu, $TotOtrosProf, $TotAmbOper);
			 if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {									
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["NroConsultFisico"];
					$cinco = $resultados[$i]["NroConsultFisicoFunc"];
					$seis = $resultados[$i]["TotCamas"];
					$siete = $resultados[$i]["TotMed"];					
					$ocho = $resultados[$i]["TotMedSerums"];
					$nueve = $resultados[$i]["TotMedRes"];
					$diez = $resultados[$i]["TotEnf"];
					$once = $resultados[$i]["TotOdon"];
					$doce = $resultados[$i]["TotPsco"];
					$trece = $resultados[$i]["TotNut"];
					$catorce = $resultados[$i]["TotTecMed"];
					$quince = $resultados[$i]["TotObst"];
					$dieciseis = $resultados[$i]["TotFarmc"];
					$diecisiete = $resultados[$i]["TotAux"];					
					$dieciocho = $resultados[$i]["TotOtrosProf"];
					$diecinueve = $resultados[$i]["NroAmbOper"];			
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);
					$mensajes->grabar('|');
					$mensajes->grabar($ocho);		
					$mensajes->grabar('|');				
					$mensajes->grabar($nueve);
					$mensajes->grabar('|'); 
					$mensajes->grabar($diez);
					$mensajes->grabar('|');
					$mensajes->grabar($once);
					$mensajes->grabar('|'); 
					$mensajes->grabar($doce);
					$mensajes->grabar('|');
					$mensajes->grabar($trece);
					$mensajes->grabar('|');
					$mensajes->grabar($catorce);
					$mensajes->grabar('|');
					$mensajes->grabar($quince);
					$mensajes->grabar('|');
					$mensajes->grabar($dieciseis);		
					$mensajes->grabar('|');	
					$mensajes->grabar($diecisiete);
					$mensajes->grabar('|');
					$mensajes->grabar($dieciocho);
					$mensajes->grabar('|');
					$mensajes->grabar($diecinueve);		
					//$mensajes->grabar('|');		
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='A0' 
			else if($tiporep=='B1'){
				 $resultados = ListarConsolidadoGeneralM($Anio, $Mes, $CodIpress, $CodUgiPres);
			 if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {									
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["IdTipoSexo"];
					$cinco = $resultados[$i]["edad"];
					
					$edad=$cinco;
					$IdTipoSexo=$cuatro;
					
					if($edad=='1'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasBebesM($Anio, $Mes, $IdTipoSexo);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasBebesM($Anio, $Mes, $IdTipoSexo);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosBebesM($Anio, $Mes, $IdTipoSexo);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='2'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,1,4);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,1,4);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,1,4);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='3'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,5,9);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,5,9);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,5,9);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='4'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,10,14);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,10,14);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,10,14);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='5'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,15,19);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,15,19);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,15,19);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='6'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,20,24);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,20,24);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,20,24);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='7'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,25,29);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,25,29);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,25,29);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='8'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,30,34);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,30,34);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,30,34);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='9'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,35,39);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,35,39);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,35,39);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='10'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,40,44);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,40,44);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,40,44);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='11'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,45,49);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,45,49);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,45,49);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='12'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,50,54);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,50,54);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,50,54);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='13'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,55,59);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,55,59);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,55,59);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='14'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,60,64);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,60,64);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,60,64);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='15'){
						$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,65,150);
						$TotalAtenMedicas=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenMedicas=$TotalAtenMedicas+$Total;								
							}
						}
						$resultdetNOMedicas=ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, 1, $IdTipoSexo,65,150);
						$TotalAtenNOMedicas=0;
						if($resultdetNOMedicas!=NULL){							
							for ($k=0; $k < count($resultdetNOMedicas); $k++) {
								$Total = $resultdetNOMedicas[$k]["Total"];	
								$TotalAtenNOMedicas=$TotalAtenNOMedicas+$Total;								
							}
						}
						$resultdetAtendidos=ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,65,150);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else{
						$TotalAtenMedicas=0;
						$TotalAtenNOMedicas=0;
						$TotalAtendidos=0;
					}	
					
					$seis=$TotalAtenMedicas;
					$siete=$TotalAtenNOMedicas;
					$ocho=$TotalAtendidos;				
								
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);
					$mensajes->grabar('|');
					$mensajes->grabar($ocho);
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
				
			}//end if $tiporep=='B1' 
			else if($tiporep=='B2'){
				 $IdEmpleado=$_GET['IdEmpleado'];
				 //$ip_pc='192.168.0.5';
				 $ip = $_SERVER['REMOTE_ADDR'];
				 $ip_pc = gethostbyaddr($ip);
				 Registrartmp_Consolidado_MorbilidadCita_M($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc);
				 $resultados = ListarConsolidado_MorbilidadCitaM($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc);
			 if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {									
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["IdTipoSexo"];
					$cinco = $resultados[$i]["edad"];									
					$seis = trim($resultados[$i]["CodigoCIE10"]);
					$siete=$resultados[$i]["Total"];							
								
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='B2' 
			
			else if($tiporep=='C1'){
				$resultados = ListarConsolidadoGeneralM($Anio, $Mes, $CodIpress, $CodUgiPres);
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {										
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["IdTipoSexo"];
					$cinco = $resultados[$i]["edad"];
					
					$edad=$cinco;
					$IdTipoSexo=$cuatro;
					
					if($edad=='1'){
						$resultdet=ListarConsolidado_AtEmergenciaBebesM($Anio, $Mes, $IdTipoSexo);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosBebesM($Anio, $Mes, $IdTipoSexo);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='2'){
						//$resultdet=ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, 1 , $IdTipoSexo,1,4);
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1, $IdTipoSexo,1,4);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,1,4);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='3'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,5,9);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,5,9);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='4'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,10,14);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,10,14);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='5'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,15,19);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,15,19);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='6'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,20,24);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,20,24);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='7'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,25,29);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,25,29);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='8'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,30,34);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,30,34);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='9'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,35,39);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,35,39);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='10'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,40,44);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,40,44);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='11'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,45,49);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,45,49);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='12'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,50,54);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,50,54);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='13'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,55,59);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,55,59);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='14'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,60,64);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}																	
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,60,64);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else if($edad=='15'){
						$resultdet=ListarConsolidado_AtEmergenciaM($Anio, $Mes, 1 , $IdTipoSexo,65,150);
						$TotalAtenciones=0;
						if($resultdet!=NULL){							
							for ($j=0; $j < count($resultdet); $j++) {
								$Total = $resultdet[$j]["Total"];	
								$TotalAtenciones=$TotalAtenciones+$Total;								
							}
						}						
						$resultdetAtendidos=ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, 1, $IdTipoSexo,65,150);	
						$TotalAtendidos=0;					
						if($resultdetAtendidos!=NULL){							
							for ($m=0; $m < count($resultdetAtendidos); $m++) {
								$TotalAtendidos = $resultdetAtendidos[$m]["Total"];																
							}
						}
					}else{
						$TotalAtenciones=0;
						$TotalAtendidos=0;
					}				
			 									
					$seis = $TotalAtenciones;
					$siete=$TotalAtendidos;							
								
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='C1'
			else if($tiporep=='C2'){
				 $IdEmpleado=$_GET['IdEmpleado'];
				 //$ip_pc='192.168.0.5';
				 $ip = $_SERVER['REMOTE_ADDR'];
				 $ip_pc = gethostbyaddr($ip);
				 Registrartmp_Consolidado_MorbilidadEmergenciaM($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc);
				 $resultados = ListarConsolidado_MorbilidadEmergenciaM($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc);
			 if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {									
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["IdTipoSexo"];
					$cinco = $resultados[$i]["edad"];									
					$seis = trim($resultados[$i]["CodigoCIE10"]);
					$siete=$resultados[$i]["Total"];							
								
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='C2' 
			else if($tiporep=='D1'){				
				$resultados = ListarConsolidado_HospitalizacionM($Anio, $Mes, $CodIpress, $CodUgiPres);			 
			    if($resultados!=NULL){
				  for ($i=0; $i < count($resultados); $i++) {										
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["codigoz"];
					$cinco = $resultados[$i]["Ing"];	
					$seis = $resultados[$i]["Egr"];	
					$estancia = $resultados[$i]["estancia"];
					if($estancia!=NULL){
					  $siete = $resultados[$i]["estancia"];
					}else{
					  $siete = '0';	
					}
					
					$ocho = $resultados[$i]["totpaciente"];
					$nueve = $resultados[$i]["totcamas"];
					$diez = $resultados[$i]["totcamasdispo"];
					$once = $resultados[$i]["totfacellidos"];			
						
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);
					$mensajes->grabar('|');
					$mensajes->grabar($ocho);		
					$mensajes->grabar('|');				
					$mensajes->grabar($nueve);
					$mensajes->grabar('|'); 
					$mensajes->grabar($diez);
					$mensajes->grabar('|');
					$mensajes->grabar($once); 
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='D1' 
			
			else if($tiporep=='D2'){				
				$resultados = ListarConsolidado_MorbilidadHospitalizacionM($Anio, $Mes, $CodIpress, $CodUgiPres);		 
			    if($resultados!=NULL){
				  for ($i=0; $i < count($resultados); $i++) {										
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["IdTipoSexo"];
					$cinco = $resultados[$i]["edad"];					
					$seis = trim($resultados[$i]["CodigoCIE10"]);
					$resultdet = ListarConsolidado_MorbilidadHospitalizacionTotalesM($Anio, $Mes, $cinco, $cuatro, $seis);			 
					if($resultdet!=NULL){
						for ($j=0; $j < count($resultdet); $j++) {	
							$siete = $resultdet[$j]["Total"];	
						}
					}else{
						$siete = '0';
					}		
						
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);					
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='D2'
			
			else if($tiporep=='G0'){				
				$resultados = ListarConsolidado_ProcedimientosM($Anio, $Mes, $CodIpress, $CodUgiPres);		 
			    if($resultados!=NULL){
				  for ($i=0; $i < count($resultados); $i++) {										
					$uno = $resultados[$i]["AnioMes"];
					$dos = $resultados[$i]["CodIPress"];
					$tres = $resultados[$i]["CodUgiPress"];
					$cuatro = $resultados[$i]["codigo"];
					$cinco = $resultados[$i]["cantidad"];					
					$seis = trim($resultados[$i]["servicio"]);						
						
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);				
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='G0'
			
			else if($tiporep=='OTRO1'){				
				$uno=$_REQUEST["CodIpress"];
				$dos=$_REQUEST["CodUgiPres"];
				$tres=$_REQUEST["CodIgss"];
				$cuatro=$_REQUEST["CodRed"];
				$cinco=$_REQUEST["CodMicrored"];
				//ElimTemp_IngresosEgresosEmergenciaM();
				//RegTemp_IngresosEgresosEmergenciaM($Anio, $Mes);	
				$resultados = ListarIngresosEgresosEmergenciaM($Anio, $Mes); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {											
					$seis = $resultados[$i]["FECHAING"];
					$siete = $resultados[$i]["HORAING"];
					$ocho = $resultados[$i]["HISTORIACLINICA"];
					
					$tipodoc=$resultados[$i]["TIPODOC"];
					$doc = $resultados[$i]["NDOC"];
					if(trim($doc)!="" && (int)$doc!=0){
					$NDOC = $tipodoc.$doc;
					}else{
					$NDOC = "0";	
					}
					$nueve = $NDOC;
					
					$diez = trim($resultados[$i]["ETNIA"]);					
					$once = $resultados[$i]["SEGURO"];
					$doce = $resultados[$i]["SEXO"];
					$trece = $resultados[$i]["EDAD"];
					$catorce = $resultados[$i]["TIPOEDAD"];
					
					$nopermitidos = array( "'" , '"', "," , ";" , "&" , "+" );
					//$nopermitidos = array( "" );
											
					$NOMBRESPACIENTE = trim($resultados[$i]["NOMBRESPACIENTE"]);
					$quince = str_replace($nopermitidos, "", $NOMBRESPACIENTE);	
									
					$APELLIDOSPACIENTE = trim($resultados[$i]["APELLIDOSPACIENTE"]);
					$dieciseis = str_replace($nopermitidos, "", $APELLIDOSPACIENTE);
					
					$DIRECCION=trim($resultados[$i]["DIRECCION"]);
					$diecisiete = str_replace($nopermitidos, "", $DIRECCION);//17					
					
					$dieciocho=$resultados[$i]["IDDISTRITODOMICILIO"];//18
					$diecinueve=$resultados[$i]["IDDISTRITOPROCEDENCIA"];//19
					
					$NOMBREACOMPA=$resultados[$i]["NOMBREACOMPA"];
					$veinte = trim(str_replace($nopermitidos, "", $NOMBREACOMPA));//20
					
					$veinti1=trim($resultados[$i]["DNIACOMPA"]);//21
					$veinti2=trim($resultados[$i]["MOTIVOATCEMERGENCIA"]);//22
					$veinti3=trim($resultados[$i]["UBIGEOSITIOOCURRENCIA"]);//23
					
					$SERVICIOATENCION=$resultados[$i]["SERVICIOATENCION"];
					$veinti4 = str_replace($nopermitidos, "", $SERVICIOATENCION);//24
					
					$veinti5=trim($resultados[$i]["DX_ING1"]);//25					
					$veinti6=trim($resultados[$i]["TIPODIAG1"]);//26	
					if($resultados[$i]["TIPODIAG1"]==NULL && $resultados[$i]["DX_ING1"]!=NULL){		
						$veinti6='D';				
					}else{
						$veinti6=$resultados[$i]["TIPODIAG1"];
					}
									
					$veinti7=trim($resultados[$i]["DX_ING2"]);//27
					$veinti8=trim($resultados[$i]["TIPODIAG2"]);//28	
					if($resultados[$i]["TIPODIAG2"]==NULL && $resultados[$i]["DX_ING2"]!=NULL){		
						$veinti8='D';				
					}else{
						$veinti8=$resultados[$i]["TIPODIAG2"];
					}
									
					$veinti9=trim($resultados[$i]["DX_ING3"]);//29
					$treinta=trim($resultados[$i]["TIPODIAG3"]);//30
					if($resultados[$i]["TIPODIAG3"]==NULL && $resultados[$i]["DX_ING3"]!=NULL){		
						$treinta='D';				
					}else{
						$treinta=$resultados[$i]["TIPODIAG3"];
					}
					
					$treinta1=trim($resultados[$i]["DX_ING4"]);//31
					$treinta2=trim($resultados[$i]["TIPODIAG4"]);//32
					if($resultados[$i]["TIPODIAG4"]==NULL && $resultados[$i]["DX_ING4"]!=NULL){		
						$treinta2='D';				
					}else{
						$treinta2=$resultados[$i]["TIPODIAG4"];
					}
					
					$treinta3=$resultados[$i]["OTROPROCE_CPT1"];//33
					$treinta4=$resultados[$i]["OTROPROCE_CPT2"];//34
					$treinta5=$resultados[$i]["OTROPROCE_CPT3"];//35
					$treinta6=$resultados[$i]["OTROPROCE_CPT4"];//36					
					$treinta7=$resultados[$i]["CONDICIONSALIDA"];//37
					$treinta8=$resultados[$i]["FECHAEGRESO"];//38
					$treinta9=$resultados[$i]["HORAEGRESO"];//39
					$cuarenta=$resultados[$i]["DESTINOATENCION"];//40
					
					$cuarenta1="";//$resultados[$i]["CODEESSDESTINO"];//41
					$cuarenta2="";//$resultados[$i]["UPSSIESHOSPITALIZADO"];//42					
					$cuarenta3="";//$resultados[$i]["MEDICO"];//43
					$cuarenta4="";//$resultados[$i]["PASOOBSERVACION"];//44
					$cuarenta5="";//$resultados[$i]["FECHAINOBS"];//45
					$cuarenta6="";//$resultados[$i]["HORAINOBS"];//46					
					$cuarenta7="";//$resultados[$i]["FECHAOUTOBS"];//47
					$cuarenta8="";//$resultados[$i]["HORAOUTOBS"];//48
					$cuarenta9="";//$resultados[$i]["TOTALESTAENOBS"];//49
					$cincuenta="";//$resultados[$i]["NCAMAENOBS"];//50
					
					$cincuenta1="";//$resultados[$i]["MEDICOOBS"];//51
					$cincuenta2="";//$resultados[$i]["CODIGO1OBS"];//52					
					$cincuenta3="";//$resultados[$i]["CODIGO2OBS"];//53
					$cincuenta4="";//$resultados[$i]["FECHAREGISTRO"];//54
					$cincuenta5="";//$resultados[$i]["ESTADOREGISTRO"];//55
												
										
						
					//llenar txt
					$mensajes->grabar($uno);
					$mensajes->grabar('|'); //Separador de Columnas
					$mensajes->grabar($dos);
					$mensajes->grabar('|');
					$mensajes->grabar($tres);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuatro);
					$mensajes->grabar('|');
					$mensajes->grabar($cinco);
					$mensajes->grabar('|');
					$mensajes->grabar($seis);
					$mensajes->grabar('|');
					$mensajes->grabar($siete);	
					$mensajes->grabar('|');	
					$mensajes->grabar($ocho);		
					$mensajes->grabar('|');				
					$mensajes->grabar($nueve);
					$mensajes->grabar('|'); 
					$mensajes->grabar($diez);
					$mensajes->grabar('|');
					$mensajes->grabar($once);
					$mensajes->grabar('|'); 
					$mensajes->grabar($doce);
					$mensajes->grabar('|');
					$mensajes->grabar($trece);
					$mensajes->grabar('|');
					$mensajes->grabar($catorce);
					$mensajes->grabar('|');
					$mensajes->grabar($quince);
					$mensajes->grabar('|');
					$mensajes->grabar($dieciseis);					
					$mensajes->grabar('|');
					$mensajes->grabar($diecisiete);
					$mensajes->grabar('|');
					$mensajes->grabar($dieciocho);
					$mensajes->grabar('|');
					$mensajes->grabar($diecinueve);	
					$mensajes->grabar('|');
					$mensajes->grabar($veinte);	
					
					$mensajes->grabar('|');
					$mensajes->grabar($veinti1);
					$mensajes->grabar('|'); 
					$mensajes->grabar($veinti2);
					$mensajes->grabar('|');
					$mensajes->grabar($veinti3);
					$mensajes->grabar('|');
					$mensajes->grabar($veinti4);
					$mensajes->grabar('|');
					$mensajes->grabar($veinti5);
					$mensajes->grabar('|');
					$mensajes->grabar($veinti6);					
					$mensajes->grabar('|');
					$mensajes->grabar($veinti7);
					$mensajes->grabar('|');
					$mensajes->grabar($veinti8);
					$mensajes->grabar('|');
					$mensajes->grabar($veinti9);	
					$mensajes->grabar('|');
					$mensajes->grabar($treinta);
					
					$mensajes->grabar('|');
					$mensajes->grabar($treinta1);
					$mensajes->grabar('|'); 
					$mensajes->grabar($treinta2);
					$mensajes->grabar('|');
					$mensajes->grabar($treinta3);
					$mensajes->grabar('|');
					$mensajes->grabar($treinta4);
					$mensajes->grabar('|');
					$mensajes->grabar($treinta5);
					$mensajes->grabar('|');
					$mensajes->grabar($treinta6);					
					$mensajes->grabar('|');
					$mensajes->grabar($treinta7);
					$mensajes->grabar('|');
					$mensajes->grabar($treinta8);
					$mensajes->grabar('|');
					$mensajes->grabar($treinta9);	
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta);
					
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta1);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cuarenta2);
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta3);
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta4);
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta5);
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta6);					
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta7);
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta8);
					$mensajes->grabar('|');
					$mensajes->grabar($cuarenta9);	
					$mensajes->grabar('|');
					$mensajes->grabar($cincuenta);
					
					$mensajes->grabar('|');
					$mensajes->grabar($cincuenta1);
					$mensajes->grabar('|'); 
					$mensajes->grabar($cincuenta2);
					$mensajes->grabar('|');
					$mensajes->grabar($cincuenta3);
					$mensajes->grabar('|');
					$mensajes->grabar($cincuenta4);
					$mensajes->grabar('|');
					$mensajes->grabar($cincuenta5);										
							
					$mensajes->grabar('||'); //Salto de Linea   
					}
				}
			}//end if $tiporep=='OTRO1'
			
			else if($tiporep=='OTRO2'){	
						
				//if($_REQUEST["tipolink"]=='exportarexcel')
								
			}//end if $tiporep=='OTRO2' 
			
		$mensa ='./'.$le; 	
		header("Content-Description: File Transfer"); 
		header( "Content-Disposition: filename=".basename($mensa) ); 
		header("Content-Length: ".filesize($mensa)); 
		header("Content-Type: application/force-download"); 
		@readfile($mensa);
					
	}//end else if	exportar txt	
}
	
?>