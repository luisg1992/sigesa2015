<?php
	
	function ListarMedicos_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPListarMedicos";			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function ListarSalaPiso_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPListarSalaPiso";			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}		

	function ListarCalidad_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPListarCalidad";			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function ListarMotivo_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPListarMotivo";			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function ListarEndo_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPListarEndocervicales";	

		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function ListarCate_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPListarCategorizacion";			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}	

	function MostrarxHC_M($HC){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPDatosPacientexHC ".$HC;
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function MostrarxCuenta_M($Cuenta){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPDatosPacientexCuenta ".$Cuenta;
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}


	//RegistrarMuestra

	function RegistrarMuestra_M($IdTipoMuestra,$IdTipoEspecifico,$HC,$NumCuenta,$CodigoAP,$NombrePaciente,$Sexo,$Edad,$FechaRegistro,$FechaCompletado,$IdPaciente,$IdServicios,$Especimen,$IdMedico1,$IdMedico2,$Endocervicales,$Categorizacion,$CalidadMuestra,$CValores,$OtrosCitologico,$DiagnosticoCitologico,$DescripcionCitologico,$DiagnosticoQuirurgico,$DescripcionQuirurgico,$Motivo){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPRegistrarMuestra ".$IdTipoMuestra.",".$IdTipoEspecifico.",".$HC.",".$NumCuenta.",'".$CodigoAP."','".$NombrePaciente."','".$Sexo."',".$Edad.",'".$FechaRegistro."','".$FechaCompletado."',".$IdPaciente.",".$IdServicios.",'".$Especimen."',".$IdMedico1.",".$IdMedico2.",".$Endocervicales.",".$Categorizacion.",".$CalidadMuestra.",'".$CValores."','".$OtrosCitologico."','".$DiagnosticoCitologico."','".$DescripcionCitologico."','".$DiagnosticoQuirurgico."','".$DescripcionQuirurgico."','".$Motivo."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 
			
	}		

	function BuscarxHC_M($Historia){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPBuscarxHC '".$Historia."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function BuscarxCodigo_M($Codigo){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPBuscarxCodigo '".$Codigo."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function BuscarxNumCuenta_M($Cuenta){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPBuscarxNumCuenta '".$Cuenta."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function EliminarxC_M($IM){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPEliminarMuestra '".$IM."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 	
	}

	

	function BuscarxNombre_M($Paciente){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPBuscarxNombrePaciente '".$Paciente."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}	

	function ListarRegistro_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPListarRegistros";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function ListarRegistroExcel_M(){
		 
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaAPExcel";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function RetornarCodigoAP($CodigoAP){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPRetornarCodigo '".$CodigoAP."'";					 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}

	function ModificarMuestra_M($Codigo,$IdPaciente,$Sexo,$Edad,$NombrePaciente,$HC,$NumCuenta,$IdServicios,$Especimen,$IdMedico1,$IdMedico2,$CalidadMuestra,$Endocervicales,$Categorizacion,$CValores,$OtrosCitologico,$DiagnosticoCitologico,$DescripcionCitologico,$DiagnosticoQuirurgico,$DescripcionQuirurgico,$FechaCompletado,$Motivo){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAPModificarMuestra '".$Codigo."',".$IdPaciente.",'".$Sexo."',".$Edad.",'".$NombrePaciente."',".$HC.",".$NumCuenta.",".$IdServicios.",'".$Especimen."',".$IdMedico1.",".$IdMedico2.",".$CalidadMuestra.",".$Endocervicales.",".$Categorizacion.",'".$CValores."','".$OtrosCitologico."','".$DiagnosticoCitologico."','".$DescripcionCitologico."','".$DiagnosticoQuirurgico."','".$DescripcionQuirurgico."','".$FechaCompletado."','".$Motivo."'";					 
		//echo $$IdEspecimen,exit();
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 
			
	}
?>