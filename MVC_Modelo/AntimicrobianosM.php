<?php





   	function Mostrar_Stock_Medicamento($IdProducto)
  	{
	require('cn/cn_sqlserver_server_locla.php');
		$sql = "select SUM(cantidad)   as Cant  from FarmSaldo
		  where idProducto='".$IdProducto."' ";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	
	
	

  function MostrarFuncionesVitalesAntimicrobiano($IdCabeceraAntimicrobiano)
  	{
	require('cn/cn_sqlserver_server_locla.php');
		$sql = "select  Valor_FR,
			Valor_FC,
			Valor_PAS,
			Valor_PAD,
			Valor_Glasgon,
			Valor_Peso,
			Valor_Temperatura  from AntimicrobianosFuncionesVitales
			where IdCabeceraAntimicrobiano='".$IdCabeceraAntimicrobiano."'
			 ";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}




  function MostrarPreviosAntimicrobiano($IdCabeceraAntimicrobiano)
  	{
	require('cn/cn_sqlserver_server_locla.php');
		$sql = "select  FactCatalogoBienesInsumos.Codigo,FactCatalogoBienesInsumos.Nombre, 
		AntimicrobianosPrevios.Dias  
		from AntimicrobianosPrevios
		inner join FactCatalogoBienesInsumos
		on FactCatalogoBienesInsumos.IdProducto=AntimicrobianosPrevios.Id_Antimicrobiano
		where AntimicrobianosPrevios.IdCabeceraAntimicrobiano='".$IdCabeceraAntimicrobiano."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}








  function MostrarCabeceraAntimicrobiano($IdCabeceraAntimicrobiano)
  	{
	require('cn/cn_sqlserver_server_locla.php');
		$sql = "select 
			AntimicrobianosCabecera.IdCabeceraAntimicrobiano,
			Pacientes.PrimerNombre,
			Pacientes.SegundoNombre,
			Pacientes.ApellidoPaterno,
			Pacientes.ApellidoMaterno,
			Pacientes.NroHistoriaClinica,
			Pacientes.FechaNacimiento,
			Atenciones.IdCuentaAtencion,
			Servicios.Nombre,
			Atenciones.HoraIngreso,
			Atenciones.FechaIngreso,
			Pacientes.IdTipoSexo,
			FuentesFinanciamiento.Descripcion,
			AntimicrobianosCabecera.Justificacion,
			AntimicrobianosCabecera.Evaluacion,
			AntimicrobianosCabecera.Infeccion_IntraHospitalaria,
			AntimicrobianosCabecera.Descripcion_Intrahospitalaria,
			Camas.Codigo as CamaPaciente
			from AntimicrobianosCabecera
			inner join Pacientes
			on Pacientes.IdPaciente=AntimicrobianosCabecera.IdPaciente
			inner join Atenciones
			on Atenciones.IdCuentaAtencion=AntimicrobianosCabecera.IdCuentaAtencion
			inner join Servicios
			on Servicios.IdServicio=Atenciones.IdServicioIngreso
			inner join FuentesFinanciamiento
			on FuentesFinanciamiento.idFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento
			left join Camas
			on Camas.IdCama=Atenciones.IdCamaIngreso
			 where AntimicrobianosCabecera.IdCabeceraAntimicrobiano='".$IdCabeceraAntimicrobiano."'
			 ";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}


	//Mostrar Diagnosticos de la Cabecera
	function MostrarDiagnosticosAntimicrobiano($IdCabeceraAntimicrobiano)
  	{
	require('cn/cn_sqlserver_server_locla.php');
		$sql = "select DiagnosticosA.Descripcion as DescripcionA,
				DiagnosticosA.CodigoCIE10  as CodigoA ,
				DiagnosticosB.Descripcion as DescripcionB,
				DiagnosticosB.CodigoCIE10  as CodigoB ,
				DiagnosticosC.Descripcion as DescripcionC,
				DiagnosticosC.CodigoCIE10  as CodigoC 
				from AntimicrobianosCabecera
				left join Diagnosticos DiagnosticosA
				on DiagnosticosA.IdDiagnostico=AntimicrobianosCabecera.IdDiagnosticoInfeccioso
				left join Diagnosticos DiagnosticosB
				on DiagnosticosB.IdDiagnostico=AntimicrobianosCabecera.IdDiagnosticoPatologico
				left join Diagnosticos DiagnosticosC
				on DiagnosticosC.IdDiagnostico=AntimicrobianosCabecera.IdDiagnosticoOtro
				where IdCabeceraAntimicrobiano='".$IdCabeceraAntimicrobiano."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}


	
	//Mostrar  Antimicrobianos 
	function Mostrar_Lista_de_Antimicrobianos($TipoBusqueda)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAntimicrobianosLista  '".$TipoBusqueda."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	//Mostrar  Tipos de Antimicrobianos 
	function Mostrar_Lista_de_Tipos_Antimicrobianos()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = " select   * from TiposAntimicrobiano ";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
	//Mostrar  Tipos de Antimicrobianos 
	function Mostrar_Lista_Solicitudes_Antimicrobianos($FechaInicio,$FechaFinal)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = " select  
				AntimicrobianosEstados.Descripcion as Estado,
				AntimicrobianosCabecera.IdEstadoAntimicrobiano,
				AntimicrobianosCabecera.FechaRegistro,
				Pacientes.IdPaciente,
				Pacientes.ApellidoPaterno,
				Pacientes.ApellidoMaterno,
				Pacientes.PrimerNombre,
				Pacientes.SegundoNombre,
				Pacientes.NroDocumento,
				Pacientes.NroHistoriaClinica,
				Pacientes.FechaNacimiento,
				AntimicrobianosCabecera.IdCabeceraAntimicrobiano,
				AntimicrobianosCabecera.IdCuentaAtencion
				from AntimicrobianosCabecera
				inner join Pacientes
				on Pacientes.IdPaciente=AntimicrobianosCabecera.IdPaciente
				inner join AntimicrobianosEstados
				on AntimicrobianosEstados.IdEstadoAntimicrobiano=AntimicrobianosCabecera.IdEstadoAntimicrobiano
				where cast(AntimicrobianosCabecera.FechaRegistro as date)  between '".$FechaInicio."' and '".$FechaFinal."'
				order by AntimicrobianosCabecera.IdCabeceraAntimicrobiano desc";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
	function Mostrar_Lista_Detalle_Antimicrobianos($Id_Formulario_Antimicrobiano)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select  
				AntimicrobianosDetalle.Dosis,
				AntimicrobianosDetalle.Frecuencia,
				AntimicrobianosDetalle.Dias,
				AntimicrobianosDetalle.Total,
				AntimicrobianosDetalle.FechaCalificacion,
				FactCatalogoBienesInsumos.Nombre,
				FactCatalogoBienesInsumos.Codigo,
				AntimicrobianosDetalle.Estado,
				AntimicrobianosDetalle.IdCabeceraAntimicrobiano,
				AntimicrobianosDetalle.Id_Antimicrobiano,
				AntimicrobianosEstados.Descripcion
				from AntimicrobianosDetalle
				inner join FactCatalogoBienesInsumos
				on FactCatalogoBienesInsumos.IdProducto=AntimicrobianosDetalle.Id_Antimicrobiano
				inner join AntimicrobianosEstados
				on AntimicrobianosEstados.IdEstadoAntimicrobiano=AntimicrobianosDetalle.Estado
				where AntimicrobianosDetalle.IdCabeceraAntimicrobiano='".$Id_Formulario_Antimicrobiano."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	

	
	//Mostrar  Pacientes 
	function Mostrar_Lista_de_Pacientes_M($NroDocumento,$ApellidoPaterno,$ApellidoMaterno,$NroHistoriaClinica)
	{
		
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  [SigesaListapacientes]  '".$NroDocumento."','".$ApellidoPaterno."','".$ApellidoMaterno."','".$NroHistoriaClinica."'";
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
		
	//Mostrar  Pacientes 
	function Mostrar_Lista_Atenciones_IdPaciente($IdPaciente)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select Atenciones.FechaIngreso,
					Atenciones.HoraIngreso,
					Servicios.Nombre,
					Atenciones.IdCuentaAtencion,
					FuentesFinanciamiento.Descripcion as FuenteFinanciamiento,
					TiposServicio.Descripcion TipoServicio,
					Camas.Codigo as Cama
					from Atenciones 
					inner join Servicios
					on Servicios.IdServicio=Atenciones.IdServicioIngreso
					inner join TiposServicio
					on TiposServicio.IdTipoServicio=Atenciones.IdTipoServicio
					inner join FuentesFinanciamiento
					on FuentesFinanciamiento.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento
					left join Camas
					on Camas.idCama=Atenciones.IDcamaEgreso
					where Atenciones.IdPaciente='".$IdPaciente."'
					and Atenciones.IdTipoServicio!='1'
					order by Atenciones.HoraIngreso+Atenciones.FechaIngreso desc";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	


	//Agregar Cabecera de Antimicrobiano
	function Agregar_Cabecera_Antimicrobianos_M($FechaRegistro,$IdPaciente,$IdCuentaAtencion,$IdDiagnosticoPatologico,$IdDiagnosticoInfeccioso,
		$IdDiagnosticoOtro,$Infeccion_IntraHospitalaria,$Descripcion_IntraHospitalaria,$Justificacion,$Estado)
	{			
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SigesaAntimicrobianosCabeceraAgregar]  '".$FechaRegistro."',".$IdPaciente.",'".$IdCuentaAtencion."','".$IdDiagnosticoPatologico."','".$IdDiagnosticoInfeccioso."','".$IdDiagnosticoOtro."','".$Infeccion_IntraHospitalaria."','".$Descripcion_IntraHospitalaria."','".$Justificacion."',".$Estado." ";
		$results = $conn->query($sql);
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
		$validar=    trim($row[0]);
		}
		$stmt = null;
		$conn = null;
		return $validar;		
	} 

	
	
	//Agregar Previo de Antimicrobiano
	function Agregar_Previo_Antimicrobiano_M($IdCabeceraAntimicrobiano,$Id_Antimicrobiano,$Dias){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = " exec [SigesaAntimicrobianosPrevioAgregar]  '".$IdCabeceraAntimicrobiano."','".$Id_Antimicrobiano."','".$Dias."'"; 
		$conn->query($sql);
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	
	
	//Agregar Detalle de Antimicrobiano
	function Agregar_Detalle_Antimicrobianos_M($IdCabeceraAntimicrobiano,$Id_Antimicrobiano,$Dosis,$Frecuencia,$Dias,$Total,$Estado){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = " exec [SigesaAntimicrobianosDetalleAgregar]  '".$IdCabeceraAntimicrobiano."','".$Id_Antimicrobiano."','".$Dosis."','".$Frecuencia."','".$Dias."','".$Total."','".$Estado."' "; 
		$conn->query($sql);
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	//Agregar Funciones Vitales
	function Agregar_Funciones_Vitales_M($Id_Formulario_Antimicrobiano,$FuncionV_FR,$FuncionV_FC,$FuncionV_PAS,$FuncionV_PAD,$FuncionV_Glasgon,$FuncionV_Peso,$FuncionV_Temperatura)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = " exec [SigesaAntimicrobianosFuncionesVitalesAgregar]  '".$Id_Formulario_Antimicrobiano."','".$FuncionV_FR."','".$FuncionV_FC."','".$FuncionV_PAS."','".$FuncionV_PAD."','".$FuncionV_Glasgon."','".$FuncionV_Peso."','".$FuncionV_Temperatura."' "; 
		$conn->query($sql);
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	

	//Mostrar  Medicamentos 
	function Mostrar_Lista_de_Medicamentos()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaFactCatalogoBienesInsumos ";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
	
	//Mostrar  Diagnosticos 
	function Mostrar_Lista_Diagnosticos()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [DiagnosticosSeleccionarTodos]";
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

			


	
	//Agregar datos
	function Agregar_Antimicrobiano($IdProducto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "insert into Antimicrobianos values ( '". $IdProducto."', getdate() )";
		//echo $sql;exit();            
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
		

		
	//Agregar datos
	function Modificar_Antimicrobiano($IdProducto,$IdTipoAntimicrobiano)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "update Antimicrobianos set IdTipoAntimicrobiano='".$IdTipoAntimicrobiano."'   where IdProducto='".$IdProducto."'";
		//echo $sql;exit();            
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
			
		
	//Agregar datos
	function Modificar_Cabecera_Detalle_Antimicrobiano($IdCabeceraAntimicrobiano,$Id_Antimicrobiano,$Dias,$Total,$IdEstadoAntimicrobiano,$FechaModifica)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "update [AntimicrobianosDetalle]  set Total='".$Total."',Dias='".$Dias."',Estado='".$IdEstadoAntimicrobiano."',FechaCalificacion='".$FechaModifica."'   where  IdCabeceraAntimicrobiano='".$IdCabeceraAntimicrobiano."' and Id_Antimicrobiano='".$Id_Antimicrobiano."'";           
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
	

	//Agregar datos
	function Modificar_Cabecera_Evaluacion_Antimicrobiano($IdCabeceraAntimicrobiano,$Evaluacion,$IdEstadoAntimicrobiano)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "update [AntimicrobianosCabecera]  set Evaluacion='".$Evaluacion."',IdEstadoAntimicrobiano='".$IdEstadoAntimicrobiano."'  where  IdCabeceraAntimicrobiano='".$IdCabeceraAntimicrobiano."'";           
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
		

	
		
	//Eliminar datos
	function Eliminar_Antimicrobiano($IdProducto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "delete from Antimicrobianos where IdProducto= '". $IdProducto."' ";
		//echo $sql;exit();            
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
		
		
		
	
	
	//Agregar datos
	function Agregar_Examen_Antimicrobiano($IdProducto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "insert into AntimicrobianosExamenes values ( '". $IdProducto."', getdate())";
		//echo $sql;exit();            
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
			
		
		
			
	//Eliminar datos
	function Eliminar_Examen_Antimicrobiano($IdProducto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "delete from AntimicrobianosExamenes where IdProducto= '". $IdProducto."' ";
		//echo $sql;exit();            
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
		

				
	//Eliminar datos
	function Eliminar_Solicitud_Registro_Antimicrobiano($IdCabeceraAntimicrobiano)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql  = "delete from AntimicrobianosCabecera where IdCabeceraAntimicrobiano='". $IdCabeceraAntimicrobiano."' ";
		//echo $sql;exit();            
		$resultat = $conn->query($sql);
		$stmt = null;
		$conn = null;
		return "M_1";
	}	
			
		
		
		
	//Mostrar  Tipos de Antimicrobianos 
	function Mostrar_Lista_Examenes()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select  IdProducto,Nombre,Codigo from FactCatalogoServicios";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
			
	//Mostrar  Tipos de Antimicrobianos 
	function Mostrar_Lista_Antimicrobianos_Examenes()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select  FactCatalogoServicios.IdProducto,Nombre,Codigo from FactCatalogoServicios
			inner join AntimicrobianosExamenes
			on FactCatalogoServicios.IdProducto=AntimicrobianosExamenes.IdProducto
			";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	
				
	//Mostrar  Tipos de Antimicrobianos 
	function Mostrar_Lista_Antimicrobianos_Examenes_IdProducto()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdProducto from AntimicrobianosExamenes";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

?>