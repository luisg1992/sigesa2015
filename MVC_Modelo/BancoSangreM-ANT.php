<?php

//MAESTROS

 function SIGESA_Emergencia_ListarTipoDocumento_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdDocIdentidad,Descripcion from TiposDocIdentidad";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
 }
 
function ListarPaises_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdPais,Codigo,Nombre FROM Paises order by Nombre";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function ObtenerDatosPais_M($Nombre){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdPais,Codigo,Nombre FROM Paises where Nombre='$Nombre'";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function CentroPobladoSeleccionarPorDistrito($IdDistrito,$texto){
		require('cn/cn_sqlserver_server_locla.php');				
	  	$sql="exec SIGESA_BSD_CentroPobladoSeleccionarPorDistrito_MHR '".$IdDistrito."','".$texto."'";		
		$results = $conn->query($sql);	
		
		$validar=NULL;	
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
	}

function DepartamentosSeleccionarTodos(){
	require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec DepartamentosSeleccionarTodos ";	 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
function ProvinciasSeleccionarPorDepartamento($IdDepartamento,$texto){
	require('cn/cn_sqlserver_server_locla.php');		
		if($IdDepartamento!='0'){
	  	$sql="exec SIGESA_BSD_ProvinciasSeleccionarPorDepartamento_MHR '".$IdDepartamento."','".$texto."'";	
		}else{
		$sql="exec ProvinciasSeleccionarTodo ";	
			}
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
function DistritosSeleccionarPorProvincia($idProvincias,$texto){
	require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SIGESA_BSD_DistritosSeleccionarPorProvincia_MHR '".$idProvincias."','".$texto."'";		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

//FIN MAESTROS


function ObtenerIdDepaReniec_M($IdReniec){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdDistrito,Distritos.IdReniec as IdDiReniec,
				Provincias.IdProvincia,Provincias.IdReniec as IdProReniec,
				Departamentos.IdDepartamento,Departamentos.IdReniec as IdDepReniec
				from Distritos 
				inner join Provincias on Distritos.IdProvincia=Provincias.IdProvincia
				inner join Departamentos on Departamentos.IdDepartamento=Provincias.IdDepartamento
				where Distritos.IdReniec=$IdReniec";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

	function ObtenerDatosDistrito_M($IdDistrito){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select t1.IdDistrito,t1.Nombre as Distrito,
				t2.IdProvincia,t2.Nombre as Provincia,
				t3.IdDepartamento,t3.Nombre as Departamento 
				from Distritos t1
				inner join Provincias t2 on t1.IdProvincia=t2.IdProvincia
				inner join Departamentos t3 on t2.IdDepartamento=t3.IdDepartamento
				where t1.IdDistrito='$IdDistrito' ";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
  }
 
  function SIGESA_Emergencia_ListarTiposSexo_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoSexo,Descripcion from TiposSexo";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
 }
 
 function SIGESA_Emergencia_ListarEstadosCiviles_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdEstadoCivil,Descripcion from TiposEstadoCivil";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
 } 
 
 
 function SIGESA_Emergencia_ListarTiposOcupacion_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoOcupacion,descripcion as Descripcion from TiposOcupacion order by descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
  function SIGESA_BSD_ListarTipoDonante_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoDonante,Descripcion from SIGESA_BSD_TipoDonante_MHR  where Estado='A' ";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function SIGESA_BSD_ListarTipoDonacion_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoDonacion,Descripcion from SIGESA_BSD_TipoDonacion_MHR  where Estado='A' ";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function SIGESA_BSD_ListarCondicionDonante_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdCondicionDonante,Descripcion from SIGESA_BSD_CondicionDonante_MHR  where Estado='A' order by Descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
  function SIGESA_BSD_ListarHospital_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdHospital,Descripcion from SIGESA_BSD_Hospital_MHR  where Estado='A'";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function SIGESA_BSD_ListarGrupoSanguineo_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdGrupoSanguineo,Descripcion from SIGESA_BSD_GrupoSanguineo_MHR  where Estado='A'";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function ListarTiposServicio_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdTipoServicio,Descripcion FROM TiposServicio where IdTipoServicio not in (6,10,11) ORDER BY Descripcion";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}
 
 function ListarServiciosHospitalM(){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select IdServicio,Servicios.Nombre,TiposServicio.Descripcion as DescripcionTipoServicio,
					Servicios.IdTipoServicio from Servicios 
					inner join TiposServicio on TiposServicio.IdTipoServicio=Servicios.IdTipoServicio
					where Servicios.IdTipoServicio not in (5,6,7,10,11) and Servicios.IdServicio not in(425) 
					order by TiposServicio.Descripcion,Servicios.Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

 function ListarTipoRelacionM(){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select IdTipoRelacion,Descripcion from dbo.SIGESA_BSD_TipoRelacion
					where Estado<>'0'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function SIGESA_ListarMotivoRechazo_M($TipoMotivo,$texto){ //1 Evaluacion, 2 Extraccion, 3 Tamizaje
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT * FROM SIGESA_BSD_MotivoRechazo 
				WHERE Estado<>'0' AND $TipoMotivo='1' AND Descripcion LIKE '%$texto%' order by Descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}
 
function SIGESA_ListarMedicacion_M($IdMedicacion){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdMedicacion,Descripcion,Diferimiento,Tiempo,TipoTiempo FROM dbo.SIGESA_BSD_Medicacion 
			    WHERE Estado<>'0' and (IdMedicacion='$IdMedicacion' or '$IdMedicacion'='') order by Descripcion";  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_ListarMetodologia_M($tipo){  //$tipo=cw_hb,cw_hcto
		 
		require('cn/cn_sqlserver_server_locla.php');
		echo $sql = "select * from SIGESA_BSD_Metodologia where Estado='1' and $tipo='1' order by Descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		echo $sql = "select e.IdEmpleado,rtrim(DNI)as DNI,ApellidoPaterno,ApellidoMaterno,Nombres,Usuario from Empleados e
					inner join EmpleadosLugarDeTrabajo on e.IdEmpleado=EmpleadosLugarDeTrabajo.idEmpleado
					where idLaboraArea=3 and idLaboraSubArea=38 and esActivo=1 
					order by ApellidoPaterno,ApellidoMaterno"; //and e.idEmpleado<>'1890' 
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_ListarEquipoExtraccion_M($CodTipoEquipo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		echo $sql = "SELECT * FROM dbo.SIGESA_BSD_EquipoExtraccion_MHR WHERE Estado='1' and CodTipoEquipo='$CodTipoEquipo'";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

 ///////////////////////////// 
function SIGESA_BSD_BuscarReceptor_M($valor,$TipoBusqueda){
		 
		require('cn/cn_sqlserver_server_locla.php');
		//$sql = "SELECT IdReceptor,IdGrupoSanguineo FROM SIGESA_BSD_Receptor where Estado='1' and (NroDocumento='$valor' or NroHistoria='$valor')";
		/*$sql = "SELECT * FROM SIGESA_BSD_Receptor RE where Estado='1' and 
				(CONVERT(VARCHAR(20),RE.NroDocumento)='$valor' or CONVERT(VARCHAR(20),RE.NroHistoria)='$valor' or (select ApellidoPaterno+' '+ApellidoMaterno+' '+PrimerNombre+' '+SegundoNombre from Pacientes where Pacientes.IdPaciente=RE.IdPaciente) like '$valor%')";	*/	
		if($TipoBusqueda=='NroDocumento'){
			$sql = "SELECT * FROM SIGESA_BSD_Receptor RE where Estado='1' and 
					CONVERT(VARCHAR(20),RE.NroDocumento)='$valor' ";		
		}else if($TipoBusqueda=='NroHistoriaClinica'){
			$sql = "SELECT * FROM SIGESA_BSD_Receptor RE where Estado='1' and
 					CONVERT(VARCHAR(20),RE.NroHistoria)='$valor'  ";
		}else{
			$sql = "SELECT * FROM SIGESA_BSD_Receptor RE where Estado='1' and  
				    (select ApellidoPaterno+' '+ApellidoMaterno+' '+PrimerNombre+' '+SegundoNombre from Pacientes 
				    where Pacientes.IdPaciente=RE.IdPaciente) like '$valor%' ";
		}
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_BSD_BuscarPacienteAtenciones_M($valor,$Pivotx){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPacienteAtenciones_MHR '".$valor."','".$Pivotx."' ";				  
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BSD_BuscarReceptorFiltro_M($valor,$Pivotx){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarReceptorFiltro_MHR '".$valor."','".$Pivotx."' ";				  
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BSD_Buscarpaciente_M($valor,$Pivotx){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPaciente_MHR '".$valor."','".$Pivotx."' ";				  
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function ListarMaxCodigoPostulanteM(){	//obtener maximo codigo	 
		require('cn/cn_sqlserver_server_locla.php');
			/*$sql =   "SELECT SUBSTRING(max(CodigoPostulante),6,5) as maxcodigo
					FROM SIGESA_BSD_Postulante 
					where SUBSTRING(CodigoPostulante ,2 , 4 )=YEAR(getdate())";*/
			/*$sql =   "SELECT SUBSTRING(max(CodigoPostulante),2,6) as maxcodigo
					FROM SIGESA_BSD_Postulante";*/	
			$sql =   "SELECT MAX(CONVERT(int, SUBSTRING(CodigoPostulante,2,8))) as maxcodigo
					FROM SIGESA_BSD_Postulante";	
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function ListarMaxNroMovimientoM(){	//obtener maximo codigo	 
		require('cn/cn_sqlserver_server_locla.php');
			$sql =   "select MAX(NroMovimiento) as maxcodigo from SIGESA_BSD_Movimientos";		
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function GuardarPostulanteM($CodigoPostulante,$FechaReg,$UsuReg,$IdGrupoSanguineo,$ApellidoPaterno,$ApellidoMaterno,$PrimerNombre,$SegundoNombre,$TercerNombre,$FechaNacimiento,$NroDocumento,$Telefono,$DireccionDomicilio,$IdTipoSexo,$IdEstadoCivil,$IdDocIdentidad,$IdTipoOcupacion,$IdCentroPobladoNacimiento,$IdCentroPobladoDomicilio,$IdCentroPobladoProcedencia,$Observacion,$IdPaisDomicilio,$IdPaisProcedencia,$IdPaisNacimiento,$IdDistritoProcedencia,$IdDistritoDomicilio,$IdDistritoNacimiento,$Email){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarPostulante_MHR] '".$CodigoPostulante."','".$FechaReg."','".$UsuReg."','".$IdGrupoSanguineo."','".$ApellidoPaterno."','".$ApellidoMaterno."','".$PrimerNombre."','".$SegundoNombre."','".$TercerNombre."','".$FechaNacimiento."','".$NroDocumento."','".$Telefono."','".$DireccionDomicilio."','".$IdTipoSexo."','".$IdEstadoCivil."','".$IdDocIdentidad."','".$IdTipoOcupacion."','".$IdCentroPobladoNacimiento."','".$IdCentroPobladoDomicilio."','".$IdCentroPobladoProcedencia."','".$Observacion."','".$IdPaisDomicilio."','".$IdPaisProcedencia."','".$IdPaisNacimiento."','".$IdDistritoProcedencia."','".$IdDistritoDomicilio."','".$IdDistritoNacimiento."','".$Email."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}	

function GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarReceptor_MHR] '".$NroDocumento."','".$NroHistoria."','".$IdPaciente."','".$IdGrupoSanguineo."','".$FechaReg."','".$UsuReg."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarMovimientosM($Estado, $FechaReg, $UsuReg, $FechaMovi, $NroDonacion, $IdReceptor, $IdCondicionDonante, $IdTipoDonacion, $IdTipoDonante, $IdPostulante, $IdTipoRelacion, $Observaciones, $IdHospital, $IdServicioHospital, $NroMovimiento){ //, $NroCama, $IdMedico, $Medico, $IdDiagnostico, $CodigoCIE10Diagnostico, $EstadoAlta, 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarMovimientos_MHR] '".$Estado."','".$FechaReg."','".$UsuReg."','".$FechaMovi."','".$NroDonacion."','".$IdReceptor."','".$IdCondicionDonante."','".$IdTipoDonacion."','".$IdTipoDonante."','".$IdPostulante."','".$IdTipoRelacion."','".$Observaciones."','".$IdHospital."','".$IdServicioHospital."','".$NroMovimiento."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function GuardarUpdMovimientosM($FechaUpd, $UsuUpd, $NroDonacion, $IdReceptor, $IdCondicionDonante, $IdTipoDonacion, $IdTipoDonante, $IdPostulante, $IdTipoRelacion, $Observaciones, $IdHospital, $IdServicioHospital, $IdMovimiento){ //, $NroCama, $IdMedico, $Medico, $IdDiagnostico, $CodigoCIE10Diagnostico, $EstadoAlta, 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdMovimientos_MHR] '".$FechaUpd."','".$UsuUpd."','".$NroDonacion."','".$IdReceptor."','".$IdCondicionDonante."','".$IdTipoDonacion."','".$IdTipoDonante."','".$IdPostulante."','".$IdTipoRelacion."','".$Observaciones."','".$IdHospital."','".$IdServicioHospital."','".$IdMovimiento."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function Listar_MovimientosRecepcionM($IdMovimiento){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarMovimientosRecepcion_MHR '".$IdMovimiento."' ";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function Listar_ExamenMedicoM($IdExamenMedico){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarExamenMedico_MHR '".$IdExamenMedico."' ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function Listar_TamizajePendientesM($NroDonacion){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarTamizajePendientes_MHR '".$NroDonacion."' ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function SIGESA_BuscarpostulanteM($valor,$Pivotx){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPostulante_MHR '".$valor."','".$Pivotx."' ";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BuscarpostulanteFiltroM($valor,$Pivotx){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPostulanteFiltro_MHR '".$valor."','".$Pivotx."' ";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BSD_MotivoNoNAT_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT * FROM SIGESA_BSD_MotivoNoNAT WHERE Estado='1'";	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;
	
}	

function GuardarExamenMedicoM($IdLugarColecta, $FechaEvaluacion, $IdEntrevistador, $Peso, $Talla, $PresionSistolica, $PresionDiastolica, $Pulso, $Temperatura, $Hemoglobina, $Hematocrito, $NroCuestionario, $Calificacion, $IdMedicacion, $FechaMedicacion, $FechaEspera, $EstadoApto, $IdMotivoRechazo, $IdMovimiento, $UsuReg, $FecReg,  $Turno, $MetodologiaHb, $MetodologiaHcto, $IdResponsableEv, $FechaInicioRechazo, $FechaFinRechazo,  $Observacion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarExamenMedico_MHR] '".$IdLugarColecta."','". $FechaEvaluacion."','". $IdEntrevistador."','". $Peso."','". $Talla."','". $PresionSistolica."','". $PresionDiastolica."','". $Pulso."','". $Temperatura."','". $Hemoglobina."','". $Hematocrito."','". $NroCuestionario."','". $Calificacion."','". $IdMedicacion."','". $FechaMedicacion."','". $FechaEspera."','". $EstadoApto."','". $IdMotivoRechazo."','". $IdMovimiento."','". $UsuReg."','". $FecReg."','".$Turno."','".$MetodologiaHb."','".$MetodologiaHcto."','".$IdResponsableEv."','".$FechaInicioRechazo."','".$FechaFinRechazo."','".$Observacion."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarUpdExamenMedicoM($Peso, $Talla, $PresionSistolica, $PresionDiastolica, $Pulso, $Temperatura, $Hemoglobina, $Hematocrito, $NroCuestionario, $Calificacion, $IdMedicacion, $FechaMedicacion, $FechaEspera, $EstadoApto, $IdMotivoRechazo, $IdMovimiento, $UsuUpd, $FecUpd, $MetodologiaHb, $MetodologiaHcto, $FechaInicioRechazo, $FechaFinRechazo, $Observacion, $IdExamenMedico){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdExamenMedico_MHR] '". $Peso."','". $Talla."','". $PresionSistolica."','". $PresionDiastolica."','". $Pulso."','". $Temperatura."','". $Hemoglobina."','". $Hematocrito."','". $NroCuestionario."','". $Calificacion."','". $IdMedicacion."','". $FechaMedicacion."','". $FechaEspera."','". $EstadoApto."','". $IdMotivoRechazo."','". $IdMovimiento."','". $UsuUpd."','". $FecUpd."','".$MetodologiaHb."','".$MetodologiaHcto."','".$FechaInicioRechazo."','".$FechaFinRechazo."','".$Observacion."','".$IdExamenMedico."'";
		$results = $conn->query($sql);				
		return $results;		
}

function SIGESA_UpdGrupoSanguineoPostulante_M($IdPostulante, $IdGrupoSanguineo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Postulante set IdGrupoSanguineo='$IdGrupoSanguineo'
				where IdPostulante='$IdPostulante'"; 
		$results = $conn->query($sql);		
		return $results;	
}

function GuardarExtraccionM($IdExamenMedico, $IdResponsableIni, $IdResponsableFin, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $FechaExtraccion, $HoraInicio, $HoraFin, $IdBolsaColectora, $IdMezcladorBasculante, $SerieHemobascula, $DuracionColeccion, $VolColeccionPre, $VolColeccionReal, $AlarmaOcurrida, $FlujoAlto, $FlujoBajo, $ConInterrupcion, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaReg, $UsuReg, $TipoReg, $IdMovimiento, $EstadoDonacion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarExtraccion_MHR] '".$IdExamenMedico."','". $IdResponsableIni."','". $IdResponsableFin."','". $IdTipoDonacion."','". $NroLote."','". $Estado."','". $EstadoApto."','". $IdMotivoRechazoDurante."','". $IdMotivoRechazoDespues."','". $FechaExtraccion."','". $HoraInicio."','". $HoraFin."','". $IdBolsaColectora."','". $IdMezcladorBasculante."','". $SerieHemobascula."','". $DuracionColeccion."','". $VolColeccionPre."','". $VolColeccionReal."','". $AlarmaOcurrida."','". $FlujoAlto."','". $FlujoBajo."','". $ConInterrupcion."','". $NroDonacion."','". $Observaciones."','". $IdEquipoAferesis."','". $IdSetAferesis."','". $SerieEquipo."','". $VolProcesado."','". $VolUsado."','". $VolPlaquetas."','". $NroCiclos."','". $RendimientoEstimado."','". $RendimientoObjetivo."','". $FechaReg."','". $UsuReg."','". $TipoReg."','". $IdMovimiento."','". $EstadoDonacion."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarUpdExtraccionM($IdExamenMedico, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $VolColeccionReal, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaUpd, $UsuUpd, $IdMovimiento, $EstadoDonacion, $IdExtraccion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdExtraccion_MHR] '".$IdExamenMedico."','". $IdTipoDonacion."','". $NroLote."','". $Estado."','". $EstadoApto."','". $IdMotivoRechazoDurante."','". $IdMotivoRechazoDespues."','". $VolColeccionReal."','". $NroDonacion."','". $Observaciones."','". $IdEquipoAferesis."','". $IdSetAferesis."','". $SerieEquipo."','". $VolProcesado."','". $VolUsado."','". $VolPlaquetas."','". $NroCiclos."','". $RendimientoEstimado."','". $RendimientoObjetivo."','". $FechaUpd."','". $UsuUpd."','". $IdMovimiento."','". $EstadoDonacion."','". $IdExtraccion."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_UpdDatosDonacionRecepcion_MHR] '".$NroDonacion."','". $EstadoDonacion."','". $FechaDonacion."','". $IdMovimiento."' ";
		//$sql = "update SIGESA_BSD_Movimientos set NroDonacion='$NroDonacion', EstadoDonacion='$EstadoDonacion', FechaDonacion='$FechaDonacion' where IdMovimiento='$IdMovimiento'"; 
		$results = $conn->query($sql);		
		return $results;		
}

//MANTENIMENTO CONFIGURACION

function SIGESA_ListarParametros_M($Codigo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT * FROM SIGESA_BSD_Parametro_MHR WHERE ( (Codigo='$Codigo' AND Estado<>'0') or '$Codigo'='1' )"; // Estado='1' 
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function GuardarUpdParametros_M($Codigo,$Valor1){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Parametro_MHR set Valor1='$Valor1' WHERE Codigo='$Codigo' and  Estado='1'";  
		$results = $conn->query($sql);	
		return $results;	
}

function ActivarDesactivarParametros_M($Codigo,$Estado){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Parametro_MHR set Estado='$Estado' WHERE Codigo='$Codigo'";  
		$results = $conn->query($sql);		
		return $results;	
}

//////EXTRACCION SANGRE
function ObtenerMaxNroDonacionExtraccion_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		//$sql =   "SELECT MAX(NroDonacion)as maxNroDonacion FROM SIGESA_BSD_ExtraccionSangre where Estado<>'0'";
		$sql =   "SELECT MAX(NroDonacion)as maxNroDonacion FROM SIGESA_BSD_Movimientos 
				  where Estado<>'0' AND SUBSTRING(NroDonacion,1,2)=SUBSTRING(CONVERT(VARCHAR(4), YEAR(GETDATE())),3,2)"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function Listar_ExtraccionSangreM($IdExtraccion){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarExtraccionSangre_MHR '".$IdExtraccion."' ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ObtenerDatosSistExtraccion_M($DonorFileNumber){		 
		require('cn/cn_access_sistema_extraccion.php');
		$sql =   "SELECT StartOperatorIdentity as IdResponsableIni, BagLot as NroLote, MixerIdentity as SerieHemobascula, StartingCollectionTime as HoraInicio, CollectionDuration as DuracionColeccion, PresetVolume as VolColeccionPre, CollectedVolume as VolColeccionReal, StopOperatorIdentity as IdResponsableFin, OverFlow as FlujoAlto,  UnderFlow as FlujoBajo,  CollectionInterrupted as ConInterrupcion 
		from  Donations where DonorFileNumber='$DonorFileNumber'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		$results = null;
		$conn = null;
		return $validar;	
}

function ValidarNroDonacionMovimiento_M($NroDonacion,$IdMovimiento){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "SELECT * FROM SIGESA_BSD_Movimientos where NroDonacion='$NroDonacion' and IdMovimiento<>'$IdMovimiento'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ValidarNroDonacionExtraccion_M($NroDonacion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "SELECT * FROM SIGESA_BSD_ExtraccionSangre WHERE Estado<>'0' and NroDonacion='$NroDonacion' ";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ObtenerCantidadDonacionesEnUltimoAnno_M($IdPostulante){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "SELECT COUNT(*)as cantidad
				 FROM SIGESA_BSD_Movimientos 
				 WHERE  DATEDIFF(day, FechaDonacion, CONVERT(DATE, GETDATE()))<365 AND IdPostulante='$IdPostulante'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ObtenerGrupoSanguineoPaciente($IdPaciente){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select distinct t1.IdPaciente,t1.idOrden,t2.ordenXresultado,t2.ValorTexto,t2.Fecha,
				(select ApellidoPaterno+' '+ApellidoMaterno+' '+Nombres from Empleados where idempleado=t2.realizaAnalisis)as RealizaPrueba
				from FactOrdenServicio t1
				inner join LabResultadoPorItems t2 on t1.IdOrden=t2.idOrden
				where t1.IdPaciente IS NOT NULL AND t2.idProductoCpt=3842 AND t2.ValorTexto<>''
				and t1.IdPaciente='$IdPaciente'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ListarMotivoElimina($Tipo){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select * from SIGESA_BSD_MotivoElimina where Estado='1' and Tipo='$Tipo'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ListarDatosSistemaFraccionamiento($Donante){		 
		require('cn/cn_sqlserver_sistema_fraccionamiento.php');
		/*$sql = "SELECT Donante as NroDonacion,Fecha as FechaPrimeraCentrif,HoraInicio as HoraPrimeraCentrif,
				NOMCORTO as Componente,Volumen 
				FROM T_Interface where Donante='$Donante' ";*/
		$sql = "SELECT Donante as NroDonacion,Fecha as FechaPrimeraCentrif,HoraInicio as HoraPrimeraCentrif,[GR],[PLASMA] 
				FROM (select Donante,Fecha,HoraInicio,VOLUMEN,NOMCORTO from T_Interface)AS SourceTable
				PIVOT( AVG(VOLUMEN) FOR NOMCORTO IN ([GR],[PLASMA]))AS PivotTable 
				where Donante='$Donante' ";		
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function SIGESA_ListarExtraccionFraccionamiento_M($IdExtraccion){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select * from SIGESA_BSD_FraccionamientoSangre where Estado<>'0' AND IdExtraccion='$IdExtraccion' ";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function GuardarFraccionamientoSangreTabM($IdResponsable,$VolumenTotRecol,$NroTabuladora,$IdExtraccion,$UsuReg,$FecReg){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarFraccionamientoSangreTab_MHR] '".$IdResponsable."','".$VolumenTotRecol."','".$NroTabuladora."','".$IdExtraccion."','". $UsuReg."','". $FecReg."'";
		$results = $conn->query($sql);			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarFraccionamientoSangre1CentriM($IdExtraccion,$VolumenPaqueteGlobu,$VolumenPlasma,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$FechaPrimeraCentrif,$NroTabuladora){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarFraccionamientoSangre1Centri_MHR] '".$IdExtraccion."','".$VolumenPaqueteGlobu."','".$VolumenPlasma."','".$MotivoElimPaqueteGlobu."','".$MotivoElimPlasma."','".$FechaPrimeraCentrif."','".$NroTabuladora."'"; 
		$results = $conn->query($sql);	
		return $results;	
}

function GuardarFraccionamientoSangre2CentriM($IdExtraccion,$VolumenPlaquetas,$MotivoElimPlaquetas,$FechaSegundaCentrif){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarFraccionamientoSangre2Centri_MHR] '".$IdExtraccion."','".$VolumenPlaquetas."','".$MotivoElimPlaquetas."','".$FechaSegundaCentrif."'";  
		$results = $conn->query($sql);	
		return $results;	
}

function GuardarTamizajeM($FechaTamizaje, $IdResponsable, $NroDonacion, $FechaReg, $UsuReg, 
$quimioVIH, $quimioSifilis, $quimioHTLV, $quimioANTICHAGAS, $quimioHBS, $quimioVHB, $quimioVHC, 
$PRquimioVIH, $PRquimioSifilis, $PRquimioHTLV, $PRquimioANTICHAGAS, $PRquimioHBS, $PRquimioVHB, $PRquimioVHC,
$natVIH, $natHBV, $natHCV, $IdExtraccion, $Observacion, $EstadoApto,$IdMovimiento, $RealizaNAT, $IdMotivoNoNAT, $SNCS,$MotivoElimMuestra){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarTamizaje_MHR] '".$FechaTamizaje."','".$IdResponsable."','".$NroDonacion."','".$FechaReg."','".$UsuReg."',
		'".$quimioVIH."','".$quimioSifilis."','".$quimioHTLV."','".$quimioANTICHAGAS."','".$quimioHBS."','".$quimioVHB."','".$quimioVHC."',
		'".$PRquimioVIH."','".$PRquimioSifilis."','".$PRquimioHTLV."','".$PRquimioANTICHAGAS."','".$PRquimioHBS."','".$PRquimioVHB."','".$PRquimioVHC."',
		'".$natVIH."','".$natHBV."','".$natHCV."','".$IdExtraccion."','".$Observacion."','".$EstadoApto."','".$IdMovimiento."','".$RealizaNAT."','".$IdMotivoNoNAT."','".$SNCS."','".$MotivoElimMuestra."'";
		$results = $conn->query($sql);			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarMotivoNoNATM($Descripcion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarMotivoNoNAT_MHR] '".$Descripcion."'";
		$results = $conn->query($sql);			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

/*function UpdateExtraccionTamizaje_M($IdExtraccion,$IdMovimiento){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Tamizaje set IdExtraccion='$IdExtraccion' WHERE IdMovimiento='$IdMovimiento' ";  
		$results = $conn->query($sql);	
		return $results;	
} */

function ReporteGeneralM($fecha_inicio,$fecha_fin){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ReporteGeneral_MHR'".$fecha_inicio."','".$fecha_fin."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ReportePostulantesM($NroDocumento){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ReportePostulantes_MHR'".$NroDocumento."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function ReporteReceptoresM($NroHistoria){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ReporteReceptores_MHR]'".$NroHistoria."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_UpdGrupoSanguineoPaciente_MHR] '".$IdPaciente."','".$GrupoSanguineo."'";  
		$results = $conn->query($sql);	
		return $results;	
}

function EliminarExtraccionM($IdExtraccion,$IdExamenMedico,$IdMovimiento,$FechaElim,$UsuElim){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_EliminarExtraccion_MHR] '".$IdExtraccion."','".$IdExamenMedico."','".$IdMovimiento."','".$FechaElim."','".$UsuElim."'";  
		$results = $conn->query($sql);	
		return $results;	
}


function ListarFraccionamientoM($IdFraccionamiento){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ListarFraccionamiento_MHR]'".$IdFraccionamiento."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function ListarTamizajeM($IdTamizaje){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ListarTamizaje_MHR]'".$IdTamizaje."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}


function GuardarUpdFraccionamientoM($IdFraccionamiento,$UsuUpd,$FecUpd,$NroTabuladora,$VolumenPaqueteGlobu,$VolumenPlasma,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$VolumenPlaquetas,$MotivoElimPlaquetas){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdFraccionamiento_MHR] '".$IdFraccionamiento."','".$UsuUpd."','".$FecUpd."','".$NroTabuladora."','".$VolumenPaqueteGlobu."','".$VolumenPlasma."','".$MotivoElimPaqueteGlobu."','".$MotivoElimPlasma."','".$VolumenPlaquetas."','".$MotivoElimPlaquetas."'";  
		$results = $conn->query($sql);	
		return $results;	
}

function GuardarUpdTamizajeM($FechaUpd, $UsuUpd, 
	$quimioVIH, $quimioSifilis, $quimioHTLV, $quimioANTICHAGAS, $quimioHBS, $quimioVHB, $quimioVHC, 
	$PRquimioVIH, $PRquimioSifilis, $PRquimioHTLV, $PRquimioANTICHAGAS, $PRquimioHBS, $PRquimioVHB, $PRquimioVHC,
	$natVIH, $natHBV, $natHCV, $IdExtraccion, $Observacion, $EstadoApto,$IdMovimiento, $RealizaNAT, $IdMotivoNoNAT, $IdTamizaje, $SNCS,$MotivoElimMuestra){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdTamizaje_MHR] '".$FechaUpd."','".$UsuUpd."',
		'".$quimioVIH."','".$quimioSifilis."','".$quimioHTLV."','".$quimioANTICHAGAS."','".$quimioHBS."','".$quimioVHB."','".$quimioVHC."',
		'".$PRquimioVIH."','".$PRquimioSifilis."','".$PRquimioHTLV."','".$PRquimioANTICHAGAS."','".$PRquimioHBS."','".$PRquimioVHB."','".$PRquimioVHC."',
		'".$natVIH."','".$natHBV."','".$natHCV."','".$IdExtraccion."','".$Observacion."','".$EstadoApto."','".$IdMovimiento."','".$RealizaNAT."','".$IdMotivoNoNAT."','".$IdTamizaje."','".$SNCS."','".$MotivoElimMuestra."'";
		$results = $conn->query($sql);			
		return $results;		
}

function RolesItemSeleccionarPermisosPorIdEmpleadoYIdListItem_M($IdEmpleado,$IdListItem){
		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql = "exec [RolesItemSeleccionarPermisosPorIdEmpleadoYIdListItem] '".$IdEmpleado."','".$IdListItem."'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;
	
}

//INICIO FUNCIONES PARA Nº SNCS y NO ingresar ningún resultado de tamizaje
function ObtenerMaximoSNCS_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql =   "SELECT CONVERT(INT,Valor1) AS Valor1 FROM SIGESA_BSD_Parametro_MHR WHERE codigo='MAXSNC'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ValidarRepiteSNCS_M($SNCS, $IdTamizaje){		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql =   "SELECT * FROM SIGESA_BSD_Tamizaje WHERE CONVERT(INT,SNCS)='$SNCS' AND SNCS<>'' AND (IdTamizaje<>'$IdTamizaje')"; 
		$results = $conn->query($sql);
	  	$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function UpdateMaximoSNCS_M($Valor1){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "update SIGESA_BSD_Parametro_MHR set Valor1='$Valor1' WHERE codigo='MAXSNC'";
		$results = $conn->query($sql);		
		return $results;	
}

function ReporteSNCS_M($FechaTamizajeIni,$FechaTamizajeFin,$RealizaNAT){
		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql = "exec [SIGESA_BSD_ReporteSNCS_MHR] '".$FechaTamizajeIni."','".$FechaTamizajeFin."','".$RealizaNAT."'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ArchivarTamizaje_M($EstadoArchivado,$FechaArchiva,$UsuArchiva,$FechaTamizajeIni,$FechaTamizajeFin,$RealizaNAT){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =  "exec [SIGESA_BSD_ArchivarTamizaje_MHR] '".$EstadoArchivado."','".$FechaArchiva."','".$UsuArchiva."','".$FechaTamizajeIni."','".$FechaTamizajeFin."','".$RealizaNAT."'";
		$results = $conn->query($sql);		
		return $results;	
}
//FIN FUNCIONES PARA Nº SNCS y NO ingresar ningún resultado de tamizaje
		
?>