<?php

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0);

//MAESTROS

 function SIGESA_Emergencia_ListarTipoDocumento_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdDocIdentidad,Descripcion from TiposDocIdentidad";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
 }
 
function ListarPaises_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdPais,Codigo,Nombre FROM Paises order by Nombre";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function ObtenerDatosPais_M($Nombre){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdPais,Codigo,Nombre FROM Paises where Nombre='$Nombre'";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function CentroPobladoSeleccionarPorDistrito($IdDistrito,$texto){
		require('cn/cn_sqlserver_server_locla.php');				
	  	$sql="exec SIGESA_BSD_CentroPobladoSeleccionarPorDistrito_MHR '".$IdDistrito."','".$texto."'";		
		$results = $conn->query($sql);	
		
		$validar=NULL;	
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
	}
      

function DepartamentosSeleccionarTodos(){
	require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec DepartamentosSeleccionarTodos ";	 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
function ProvinciasSeleccionarPorDepartamento($IdDepartamento,$texto){
	require('cn/cn_sqlserver_server_locla.php');		
		if($IdDepartamento!='0'){
	  	$sql="exec SIGESA_BSD_ProvinciasSeleccionarPorDepartamento_MHR '".$IdDepartamento."','".$texto."'";	
		}else{
		$sql="exec ProvinciasSeleccionarTodo ";	
			}
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
function DistritosSeleccionarPorProvincia($idProvincias,$texto){
	require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SIGESA_BSD_DistritosSeleccionarPorProvincia_MHR '".$idProvincias."','".$texto."'";		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

//FIN MAESTROS


function ObtenerIdDepaReniec_M($IdReniec){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdDistrito,Distritos.IdReniec as IdDiReniec,
				Provincias.IdProvincia,Provincias.IdReniec as IdProReniec,
				Departamentos.IdDepartamento,Departamentos.IdReniec as IdDepReniec
				from Distritos 
				inner join Provincias on Distritos.IdProvincia=Provincias.IdProvincia
				inner join Departamentos on Departamentos.IdDepartamento=Provincias.IdDepartamento
				where Distritos.IdReniec=$IdReniec";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

	function ObtenerDatosDistrito_M($IdDistrito){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select t1.IdDistrito,t1.Nombre as Distrito,
				t2.IdProvincia,t2.Nombre as Provincia,
				t3.IdDepartamento,t3.Nombre as Departamento 
				from Distritos t1
				inner join Provincias t2 on t1.IdProvincia=t2.IdProvincia
				inner join Departamentos t3 on t2.IdDepartamento=t3.IdDepartamento
				where t1.IdDistrito='$IdDistrito' ";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
  }
 
  function SIGESA_Emergencia_ListarTiposSexo_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoSexo,Descripcion from TiposSexo";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
 }
 
 function SIGESA_Emergencia_ListarEstadosCiviles_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdEstadoCivil,Descripcion from TiposEstadoCivil";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
 } 
 
 
 function SIGESA_Emergencia_ListarTiposOcupacion_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoOcupacion,descripcion as Descripcion from TiposOcupacion order by descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
  function SIGESA_BSD_ListarTipoDonante_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoDonante,Descripcion from SIGESA_BSD_TipoDonante_MHR  where Estado='A' ";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function SIGESA_BSD_ListarTipoDonacion_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoDonacion,Descripcion from SIGESA_BSD_TipoDonacion_MHR  where Estado='A' ";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function SIGESA_BSD_ListarCondicionDonante_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdCondicionDonante,Descripcion from SIGESA_BSD_CondicionDonante_MHR  where Estado='A' order by Descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
  function SIGESA_BSD_ListarHospital_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdHospital,Descripcion from SIGESA_BSD_Hospital  where Estado='A'";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function SIGESA_BSD_ListarGrupoSanguineo_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdGrupoSanguineo,Descripcion from SIGESA_BSD_GrupoSanguineo_MHR  where Estado='A'";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
 }
 
 function ListarTiposServicio_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdTipoServicio,Descripcion FROM TiposServicio where IdTipoServicio not in (6,10,11) ORDER BY Descripcion";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}
 
 function ListarServiciosHospitalM(){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select IdServicio,Servicios.Nombre,TiposServicio.Descripcion as DescripcionTipoServicio,
					Servicios.IdTipoServicio from Servicios 
					inner join TiposServicio on TiposServicio.IdTipoServicio=Servicios.IdTipoServicio
					where Servicios.IdTipoServicio not in (5,6,7,10,11) and Servicios.IdServicio not in(425) and Servicios.idEstado='1'
					order by TiposServicio.Descripcion,Servicios.Nombre ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

 function ListarTipoRelacionM(){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select IdTipoRelacion,Descripcion from dbo.SIGESA_BSD_TipoRelacion
					where Estado<>'0'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function SIGESA_ListarMotivoRechazo_M($TipoMotivo,$texto){ //1 Evaluacion, 2 Extraccion, 3 Tamizaje
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT * FROM SIGESA_BSD_MotivoRechazo 
				WHERE Estado<>'0' AND $TipoMotivo='1' AND Descripcion LIKE '%$texto%' order by Descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}
 
function SIGESA_ListarMedicacion_M($IdMedicacion){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdMedicacion,Descripcion,Diferimiento,Tiempo,TipoTiempo FROM dbo.SIGESA_BSD_Medicacion 
			    WHERE Estado<>'0' and (IdMedicacion='$IdMedicacion' or '$IdMedicacion'='') order by Descripcion";  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_ListarMetodologia_M($tipo){  //$tipo=cw_hb,cw_hcto
		 
		require('cn/cn_sqlserver_server_locla.php');
		echo $sql = "select * from SIGESA_BSD_Metodologia where Estado='1' and $tipo='1' order by Descripcion";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select e.IdEmpleado,rtrim(DNI)as DNI,ApellidoPaterno,ApellidoMaterno,Nombres,Usuario from Empleados e
					inner join EmpleadosLugarDeTrabajo on e.IdEmpleado=EmpleadosLugarDeTrabajo.idEmpleado
					where idLaboraArea=3 and idLaboraSubArea=38 and esActivo=1 
					order by ApellidoPaterno,ApellidoMaterno"; //and e.idEmpleado<>'1890' 
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_ListarEquipoExtraccion_M($CodTipoEquipo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		echo $sql = "SELECT * FROM dbo.SIGESA_BSD_EquipoExtraccion_MHR WHERE Estado='1' and CodTipoEquipo='$CodTipoEquipo'";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

 ///////////////////////////// 
function SIGESA_BSD_BuscarPacienteAtenciones_M($valor,$Pivotx){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPacienteAtenciones_MHR '".$valor."','".$Pivotx."' ";				  
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

/*function SIGESA_BSD_BuscarReceptor_M($valor,$TipoBusqueda){
		 
		require('cn/cn_sqlserver_server_locla.php');		
		$sql = "SELECT IdReceptor,NroDocumento,NroHistoria,IdPaciente,IdGrupoSanguineo FROM SIGESA_BSD_Receptor RE 
				where Estado='1' and CONVERT(VARCHAR(20),RE.NroHistoria)='$valor' ";
	  //$sql = "SELECT * FROM SIGESA_BSD_Receptor RE where Estado='1' and (select ApellidoPaterno+' '+ApellidoMaterno+' '+PrimerNombre+' '+SegundoNombre from Pacientes where Pacientes.IdPaciente=RE.IdPaciente) like '$valor%' ";		
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}*/

function SIGESA_BSD_BuscarReceptorFiltro_M($valor,$Pivotx){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarReceptorFiltro_MHR '".$valor."','".$Pivotx."' ";				  
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BSD_Buscarpaciente_M($valor,$Pivotx){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPaciente_MHR '".$valor."','".$Pivotx."' ";				  
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function ListarMaxCodigoPostulanteM(){	//obtener maximo codigo	 
		require('cn/cn_sqlserver_server_locla.php');				
			$sql =   "SELECT MAX(CONVERT(int, SUBSTRING(CodigoPostulante,2,8))) as maxcodigo
					FROM SIGESA_BSD_Postulante";	
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function ListarMaxNroMovimientoM(){	//obtener maximo codigo	 
		require('cn/cn_sqlserver_server_locla.php');
			$sql =   "select MAX(NroMovimiento) as maxcodigo from SIGESA_BSD_Movimientos";		
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function GuardarPostulanteM($CodigoPostulante,$FechaReg,$UsuReg,$IdGrupoSanguineo,$ApellidoPaterno,$ApellidoMaterno,$PrimerNombre,$SegundoNombre,$TercerNombre,$FechaNacimiento,$NroDocumento,$Telefono,$DireccionDomicilio,$IdTipoSexo,$IdEstadoCivil,$IdDocIdentidad,$IdTipoOcupacion,$IdCentroPobladoNacimiento,$IdCentroPobladoDomicilio,$IdCentroPobladoProcedencia,$Observacion,$IdPaisDomicilio,$IdPaisProcedencia,$IdPaisNacimiento,$IdDistritoProcedencia,$IdDistritoDomicilio,$IdDistritoNacimiento,$Email){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarPostulante_MHR] '".$CodigoPostulante."','".$FechaReg."','".$UsuReg."','".$IdGrupoSanguineo."','".$ApellidoPaterno."','".$ApellidoMaterno."','".$PrimerNombre."','".$SegundoNombre."','".$TercerNombre."','".$FechaNacimiento."','".$NroDocumento."','".$Telefono."','".$DireccionDomicilio."','".$IdTipoSexo."','".$IdEstadoCivil."','".$IdDocIdentidad."','".$IdTipoOcupacion."','".$IdCentroPobladoNacimiento."','".$IdCentroPobladoDomicilio."','".$IdCentroPobladoProcedencia."','".$Observacion."','".$IdPaisDomicilio."','".$IdPaisProcedencia."','".$IdPaisNacimiento."','".$IdDistritoProcedencia."','".$IdDistritoDomicilio."','".$IdDistritoNacimiento."','".$Email."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}	

function GuardarReceptorM($NroDocumento, $NroHistoria, $IdPaciente, $IdGrupoSanguineo, $FechaReg, $UsuReg){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarReceptor_MHR] '".$NroDocumento."','".$NroHistoria."','".$IdPaciente."','".$IdGrupoSanguineo."','".$FechaReg."','".$UsuReg."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarMovimientosM($Estado, $FechaReg, $UsuReg, $FechaMovi, $NroDonacion, $IdReceptor, $IdCondicionDonante, $IdTipoDonacion, $IdTipoDonante, $IdPostulante, $IdTipoRelacion, $Observaciones, $IdHospital, $IdServicioHospital, $NroMovimiento){ //, $NroCama, $IdMedico, $Medico, $IdDiagnostico, $CodigoCIE10Diagnostico, $EstadoAlta, 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarMovimientos_MHR] '".$Estado."','".$FechaReg."','".$UsuReg."','".$FechaMovi."','".$NroDonacion."','".$IdReceptor."','".$IdCondicionDonante."','".$IdTipoDonacion."','".$IdTipoDonante."','".$IdPostulante."','".$IdTipoRelacion."','".$Observaciones."','".$IdHospital."','".$IdServicioHospital."','".$NroMovimiento."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function GuardarUpdMovimientosM($FechaUpd, $UsuUpd, $NroDonacion, $IdReceptor, $IdCondicionDonante, $IdTipoDonacion, $IdTipoDonante, $IdPostulante, $IdTipoRelacion, $Observaciones, $IdHospital, $IdServicioHospital, $IdMovimiento){ //, $NroCama, $IdMedico, $Medico, $IdDiagnostico, $CodigoCIE10Diagnostico, $EstadoAlta, 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdMovimientos_MHR] '".$FechaUpd."','".$UsuUpd."','".$NroDonacion."','".$IdReceptor."','".$IdCondicionDonante."','".$IdTipoDonacion."','".$IdTipoDonante."','".$IdPostulante."','".$IdTipoRelacion."','".$Observaciones."','".$IdHospital."','".$IdServicioHospital."','".$IdMovimiento."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function Listar_MovimientosRecepcionM($IdMovimiento){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarMovimientosRecepcion_MHR '".$IdMovimiento."' ";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function Listar_ExamenMedicoM($IdExamenMedico){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarExamenMedico_MHR '".$IdExamenMedico."' ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function Listar_TamizajePendientesM($NroDonacion){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarTamizajePendientes_MHR '".$NroDonacion."' ";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function SIGESA_BuscarpostulanteM($valor,$Pivotx){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPostulante_MHR '".$valor."','".$Pivotx."' ";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BuscarpostulanteFiltroM($valor,$Pivotx){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarPostulanteFiltro_MHR '".$valor."','".$Pivotx."' ";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BSD_MotivoNoNAT_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT * FROM SIGESA_BSD_MotivoNoNAT WHERE Estado='1'";	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;
	
}	

function GuardarExamenMedicoM($IdLugarColecta, $FechaEvaluacion, $IdEntrevistador, $Peso, $Talla, $PresionSistolica, $PresionDiastolica, $Pulso, $Temperatura, $Hemoglobina, $Hematocrito, $NroCuestionario, $Calificacion, $IdMedicacion, $FechaMedicacion, $FechaEspera, $EstadoApto, $IdMotivoRechazo, $IdMovimiento, $UsuReg, $FecReg,  $Turno, $MetodologiaHb, $MetodologiaHcto, $IdResponsableEv, $FechaInicioRechazo, $FechaFinRechazo,  $Observacion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarExamenMedico_MHR] '".$IdLugarColecta."','". $FechaEvaluacion."','". $IdEntrevistador."','". $Peso."','". $Talla."','". $PresionSistolica."','". $PresionDiastolica."','". $Pulso."','". $Temperatura."','". $Hemoglobina."','". $Hematocrito."','". $NroCuestionario."','". $Calificacion."','". $IdMedicacion."','". $FechaMedicacion."','". $FechaEspera."','". $EstadoApto."','". $IdMotivoRechazo."','". $IdMovimiento."','". $UsuReg."','". $FecReg."','".$Turno."','".$MetodologiaHb."','".$MetodologiaHcto."','".$IdResponsableEv."','".$FechaInicioRechazo."','".$FechaFinRechazo."','".$Observacion."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarUpdExamenMedicoM($Peso, $Talla, $PresionSistolica, $PresionDiastolica, $Pulso, $Temperatura, $Hemoglobina, $Hematocrito, $NroCuestionario, $Calificacion, $IdMedicacion, $FechaMedicacion, $FechaEspera, $EstadoApto, $IdMotivoRechazo, $IdMovimiento, $UsuUpd, $FecUpd, $MetodologiaHb, $MetodologiaHcto, $FechaInicioRechazo, $FechaFinRechazo, $Observacion, $IdExamenMedico){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdExamenMedico_MHR] '". $Peso."','". $Talla."','". $PresionSistolica."','". $PresionDiastolica."','". $Pulso."','". $Temperatura."','". $Hemoglobina."','". $Hematocrito."','". $NroCuestionario."','". $Calificacion."','". $IdMedicacion."','". $FechaMedicacion."','". $FechaEspera."','". $EstadoApto."','". $IdMotivoRechazo."','". $IdMovimiento."','". $UsuUpd."','". $FecUpd."','".$MetodologiaHb."','".$MetodologiaHcto."','".$FechaInicioRechazo."','".$FechaFinRechazo."','".$Observacion."','".$IdExamenMedico."'";
		$results = $conn->query($sql);				
		return $results;		
}

function SIGESA_UpdGrupoSanguineoPostulante_M($IdPostulante, $IdGrupoSanguineo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Postulante set IdGrupoSanguineo='$IdGrupoSanguineo'
				where IdPostulante='$IdPostulante'"; 
		$results = $conn->query($sql);		
		return $results;	
}

function GuardarExtraccionM($IdExamenMedico, $IdResponsableIni, $IdResponsableFin, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $FechaExtraccion, $HoraInicio, $HoraFin, $IdBolsaColectora, $IdMezcladorBasculante, $SerieHemobascula, $DuracionColeccion, $VolColeccionPre, $VolColeccionReal, $AlarmaOcurrida, $FlujoAlto, $FlujoBajo, $ConInterrupcion, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaReg, $UsuReg, $TipoReg, $IdMovimiento, $EstadoDonacion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarExtraccion_MHR] '".$IdExamenMedico."','". $IdResponsableIni."','". $IdResponsableFin."','". $IdTipoDonacion."','". $NroLote."','". $Estado."','". $EstadoApto."','". $IdMotivoRechazoDurante."','". $IdMotivoRechazoDespues."','". $FechaExtraccion."','". $HoraInicio."','". $HoraFin."','". $IdBolsaColectora."','". $IdMezcladorBasculante."','". $SerieHemobascula."','". $DuracionColeccion."','". $VolColeccionPre."','". $VolColeccionReal."','". $AlarmaOcurrida."','". $FlujoAlto."','". $FlujoBajo."','". $ConInterrupcion."','". $NroDonacion."','". $Observaciones."','". $IdEquipoAferesis."','". $IdSetAferesis."','". $SerieEquipo."','". $VolProcesado."','". $VolUsado."','". $VolPlaquetas."','". $NroCiclos."','". $RendimientoEstimado."','". $RendimientoObjetivo."','". $FechaReg."','". $UsuReg."','". $TipoReg."','". $IdMovimiento."','". $EstadoDonacion."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarUpdExtraccionM($IdExamenMedico, $IdTipoDonacion, $NroLote, $Estado, $EstadoApto, $IdMotivoRechazoDurante, $IdMotivoRechazoDespues, $VolColeccionReal, $NroDonacion, $Observaciones, $IdEquipoAferesis, $IdSetAferesis, $SerieEquipo, $VolProcesado, $VolUsado, $VolPlaquetas, $NroCiclos, $RendimientoEstimado, $RendimientoObjetivo, $FechaUpd, $UsuUpd, $IdMovimiento, $EstadoDonacion, $IdExtraccion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdExtraccion_MHR] '".$IdExamenMedico."','". $IdTipoDonacion."','". $NroLote."','". $Estado."','". $EstadoApto."','". $IdMotivoRechazoDurante."','". $IdMotivoRechazoDespues."','". $VolColeccionReal."','". $NroDonacion."','". $Observaciones."','". $IdEquipoAferesis."','". $IdSetAferesis."','". $SerieEquipo."','". $VolProcesado."','". $VolUsado."','". $VolPlaquetas."','". $NroCiclos."','". $RendimientoEstimado."','". $RendimientoObjetivo."','". $FechaUpd."','". $UsuUpd."','". $IdMovimiento."','". $EstadoDonacion."','". $IdExtraccion."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function SIGESA_UpdDatosDonacionRecepcion_M($NroDonacion, $EstadoDonacion, $FechaDonacion, $IdMovimiento){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_UpdDatosDonacionRecepcion_MHR] '".$NroDonacion."','". $EstadoDonacion."','". $FechaDonacion."','". $IdMovimiento."' ";
		//$sql = "update SIGESA_BSD_Movimientos set NroDonacion='$NroDonacion', EstadoDonacion='$EstadoDonacion', FechaDonacion='$FechaDonacion' where IdMovimiento='$IdMovimiento'"; 
		$results = $conn->query($sql);		
		return $results;		
}

//MANTENIMENTO CONFIGURACION

function SIGESA_ListarParametros_M($Codigo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT * FROM SIGESA_BSD_Parametro WHERE ( (Codigo='$Codigo' AND Estado<>'0') or '$Codigo'='1' )"; // Estado='1' 
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function GuardarUpdParametros_M($Codigo,$Valor1){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Parametro set Valor1='$Valor1' WHERE Codigo='$Codigo' and  Estado='1'";  
		$results = $conn->query($sql);	
		return $results;	
}

function ActivarDesactivarParametros_M($Codigo,$Estado){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Parametro set Estado='$Estado' WHERE Codigo='$Codigo'";  
		$results = $conn->query($sql);		
		return $results;	
}

//////EXTRACCION SANGRE
function ObtenerMaxNroDonacionExtraccion_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		//$sql =   "SELECT MAX(NroDonacion)as maxNroDonacion FROM SIGESA_BSD_ExtraccionSangre where Estado<>'0'";
		$sql =   "SELECT MAX(NroDonacion)as maxNroDonacion FROM SIGESA_BSD_Movimientos 
				  where Estado<>'0' AND SUBSTRING(NroDonacion,1,2)=SUBSTRING(CONVERT(VARCHAR(4), YEAR(GETDATE())),3,2)"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function Listar_ExtraccionSangreM($IdExtraccion){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ListarExtraccionSangre_MHR '".$IdExtraccion."' ";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ObtenerDatosSistExtraccion_M($DonorFileNumber){		 
		require('cn/cn_access_sistema_extraccion.php');
		$sql =   "SELECT StartOperatorIdentity as IdResponsableIni, BagLot as NroLote, MixerIdentity as SerieHemobascula, StartingCollectionTime as HoraInicio, CollectionDuration as DuracionColeccion, PresetVolume as VolColeccionPre, CollectedVolume as VolColeccionReal, StopOperatorIdentity as IdResponsableFin, OverFlow as FlujoAlto,  UnderFlow as FlujoBajo,  CollectionInterrupted as ConInterrupcion 
		from  Donations where DonorFileNumber='$DonorFileNumber'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		$results = null;
		$conn = null;
		return $validar;	
}

function ValidarNroDonacionMovimiento_M($NroDonacion,$IdMovimiento){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "SELECT * FROM SIGESA_BSD_Movimientos where NroDonacion='$NroDonacion' and IdMovimiento<>'$IdMovimiento'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ValidarNroDonacionExtraccion_M($NroDonacion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "SELECT * FROM SIGESA_BSD_ExtraccionSangre WHERE Estado<>'0' and NroDonacion='$NroDonacion' ";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ObtenerCantidadDonacionesEnUltimoAnno_M($IdPostulante){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "SELECT COUNT(*)as cantidad
				 FROM SIGESA_BSD_Movimientos 
				 WHERE  DATEDIFF(day, FechaDonacion, CONVERT(DATE, GETDATE()))<365 AND IdPostulante='$IdPostulante'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ObtenerGrupoSanguineoPaciente($IdPaciente){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select distinct t1.IdPaciente,t1.idOrden,t2.ordenXresultado,t2.ValorTexto,t2.Fecha,
				(select ApellidoPaterno+' '+ApellidoMaterno+' '+Nombres from Empleados where idempleado=t2.realizaAnalisis)as RealizaPrueba
				from FactOrdenServicio t1
				inner join LabResultadoPorItems t2 on t1.IdOrden=t2.idOrden
				where t1.IdPaciente IS NOT NULL AND t2.idProductoCpt=3842 AND t2.ValorTexto<>''
				and t1.IdPaciente='$IdPaciente'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ListarMotivoElimina($Tipo){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select * from SIGESA_BSD_MotivoElimina where Estado='1' and Tipo='$Tipo'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function ListarDatosSistemaFraccionamiento($Donante){		 
		require('cn/cn_sqlserver_sistema_fraccionamiento.php');
		/*$sql = "SELECT Donante as NroDonacion,Fecha as FechaPrimeraCentrif,HoraInicio as HoraPrimeraCentrif,
				NOMCORTO as Componente,Volumen 
				FROM T_Interface where Donante='$Donante' ";*/
		$sql = "SELECT Donante as NroDonacion,Fecha as FechaPrimeraCentrif,HoraInicio as HoraPrimeraCentrif,[GR],[PLASMA] 
				FROM (select Donante,Fecha,HoraInicio,VOLUMEN,NOMCORTO from T_Interface)AS SourceTable
				PIVOT( AVG(VOLUMEN) FOR NOMCORTO IN ([GR],[PLASMA]))AS PivotTable 
				where Donante='$Donante' ";		
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		} 
		return $validar;	
}

function SIGESA_ListarExtraccionFraccionamiento_M($IdExtraccion){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select * from SIGESA_BSD_FraccionamientoSangre where Estado<>'0' AND IdExtraccion='$IdExtraccion' ";	  
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function GuardarFraccionamientoSangreTabM($IdResponsable,$VolumenTotRecol,$NroTabuladora,$IdExtraccion,$UsuReg,$FecReg){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarFraccionamientoSangreTab_MHR] '".$IdResponsable."','".$VolumenTotRecol."','".$NroTabuladora."','".$IdExtraccion."','". $UsuReg."','". $FecReg."'";
		$results = $conn->query($sql);			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarFraccionamientoSangre1CentriM($IdExtraccion,$VolumenPaqueteGlobu,$VolumenPlasma,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$FechaPrimeraCentrif,$NroTabuladora){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarFraccionamientoSangre1Centri_MHR] '".$IdExtraccion."','".$VolumenPaqueteGlobu."','".$VolumenPlasma."','".$MotivoElimPaqueteGlobu."','".$MotivoElimPlasma."','".$FechaPrimeraCentrif."','".$NroTabuladora."'"; 
		$results = $conn->query($sql);	
		return $results;	
}

function GuardarFraccionamientoSangre2CentriM($IdExtraccion,$VolumenPlaquetas,$MotivoElimPlaquetas,$FechaSegundaCentrif){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarFraccionamientoSangre2Centri_MHR] '".$IdExtraccion."','".$VolumenPlaquetas."','".$MotivoElimPlaquetas."','".$FechaSegundaCentrif."'";  
		$results = $conn->query($sql);	
		return $results;	
}

function GuardarTamizajeM($FechaTamizaje, $IdResponsable, $NroDonacion, $FechaReg, $UsuReg, 
$quimioVIH, $quimioSifilis, $quimioHTLV, $quimioANTICHAGAS, $quimioHBS, $quimioVHB, $quimioVHC, 
$PRquimioVIH, $PRquimioSifilis, $PRquimioHTLV, $PRquimioANTICHAGAS, $PRquimioHBS, $PRquimioVHB, $PRquimioVHC,
$natVIH, $natHBV, $natHCV, $IdExtraccion, $Observacion, $EstadoApto,$IdMovimiento, $RealizaNAT, $IdMotivoNoNAT, $SNCS,$MotivoElimMuestra){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarTamizaje_MHR] '".$FechaTamizaje."','".$IdResponsable."','".$NroDonacion."','".$FechaReg."','".$UsuReg."',
		'".$quimioVIH."','".$quimioSifilis."','".$quimioHTLV."','".$quimioANTICHAGAS."','".$quimioHBS."','".$quimioVHB."','".$quimioVHC."',
		'".$PRquimioVIH."','".$PRquimioSifilis."','".$PRquimioHTLV."','".$PRquimioANTICHAGAS."','".$PRquimioHBS."','".$PRquimioVHB."','".$PRquimioVHC."',
		'".$natVIH."','".$natHBV."','".$natHCV."','".$IdExtraccion."','".$Observacion."','".$EstadoApto."','".$IdMovimiento."','".$RealizaNAT."','".$IdMotivoNoNAT."','".$SNCS."','".$MotivoElimMuestra."'";
		$results = $conn->query($sql);			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

function GuardarMotivoNoNATM($Descripcion){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarMotivoNoNAT_MHR] '".$Descripcion."'";
		$results = $conn->query($sql);			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;		
}

/*function UpdateExtraccionTamizaje_M($IdExtraccion,$IdMovimiento){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update SIGESA_BSD_Tamizaje set IdExtraccion='$IdExtraccion' WHERE IdMovimiento='$IdMovimiento' ";  
		$results = $conn->query($sql);	
		return $results;	
} */

function ReporteGeneralM($fecha_inicio,$fecha_fin){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ReporteGeneral_MHR'".$fecha_inicio."','".$fecha_fin."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ReportePostulantesM($NroDocumento){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ReportePostulantes_MHR'".$NroDocumento."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function ReporteReceptoresM($NroHistoria){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ReporteReceptores_MHR]'".$NroHistoria."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function UpdGrupoSanguineoPacienteM($IdPaciente,$GrupoSanguineo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_UpdGrupoSanguineoPaciente_MHR] '".$IdPaciente."','".$GrupoSanguineo."'";  
		$results = $conn->query($sql);	
		return $results;	
}

function EliminarExtraccionM($IdExtraccion,$IdExamenMedico,$IdMovimiento,$FechaElim,$UsuElim){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_EliminarExtraccion_MHR] '".$IdExtraccion."','".$IdExamenMedico."','".$IdMovimiento."','".$FechaElim."','".$UsuElim."'";  
		$results = $conn->query($sql);	
		return $results;	
}


function ListarFraccionamientoM($IdFraccionamiento){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ListarFraccionamiento_MHR]'".$IdFraccionamiento."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function ListarTamizajeM($IdTamizaje){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ListarTamizaje_MHR]'".$IdTamizaje."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}


function GuardarUpdFraccionamientoM($IdFraccionamiento,$UsuUpd,$FecUpd,$NroTabuladora,$VolumenPaqueteGlobu,$VolumenPlasma,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$VolumenPlaquetas,$MotivoElimPlaquetas){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdFraccionamiento_MHR] '".$IdFraccionamiento."','".$UsuUpd."','".$FecUpd."','".$NroTabuladora."','".$VolumenPaqueteGlobu."','".$VolumenPlasma."','".$MotivoElimPaqueteGlobu."','".$MotivoElimPlasma."','".$VolumenPlaquetas."','".$MotivoElimPlaquetas."'";  
		$results = $conn->query($sql);	
		return $results;	
}

function GuardarUpdTamizajeM($FechaUpd, $UsuUpd, 
	$quimioVIH, $quimioSifilis, $quimioHTLV, $quimioANTICHAGAS, $quimioHBS, $quimioVHB, $quimioVHC, 
	$PRquimioVIH, $PRquimioSifilis, $PRquimioHTLV, $PRquimioANTICHAGAS, $PRquimioHBS, $PRquimioVHB, $PRquimioVHC,
	$natVIH, $natHBV, $natHCV, $IdExtraccion, $Observacion, $EstadoApto,$IdMovimiento, $RealizaNAT, $IdMotivoNoNAT, $IdTamizaje, $SNCS,$MotivoElimMuestra){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdTamizaje_MHR] '".$FechaUpd."','".$UsuUpd."',
		'".$quimioVIH."','".$quimioSifilis."','".$quimioHTLV."','".$quimioANTICHAGAS."','".$quimioHBS."','".$quimioVHB."','".$quimioVHC."',
		'".$PRquimioVIH."','".$PRquimioSifilis."','".$PRquimioHTLV."','".$PRquimioANTICHAGAS."','".$PRquimioHBS."','".$PRquimioVHB."','".$PRquimioVHC."',
		'".$natVIH."','".$natHBV."','".$natHCV."','".$IdExtraccion."','".$Observacion."','".$EstadoApto."','".$IdMovimiento."','".$RealizaNAT."','".$IdMotivoNoNAT."','".$IdTamizaje."','".$SNCS."','".$MotivoElimMuestra."'";
		$results = $conn->query($sql);			
		return $results;		
}

function RolesItemSeleccionarPermisosPorIdEmpleadoYIdListItem_M($IdEmpleado,$IdListItem){
		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql = "exec [RolesItemSeleccionarPermisosPorIdEmpleadoYIdListItem] '".$IdEmpleado."','".$IdListItem."'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;
	
}

//INICIO FUNCIONES PARA Nº SNCS y NO ingresar ningún resultado de tamizaje
function ObtenerMaximoSNCS_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql =   "SELECT CONVERT(INT,Valor1) AS Valor1, (LEN(Valor1)) AS LengValor1 FROM SIGESA_BSD_Parametro WHERE codigo='MAXSNC'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ValidarRepiteSNCS_M($SNCS, $IdTamizaje){		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql =   "SELECT * FROM SIGESA_BSD_Tamizaje WHERE CONVERT(INT,SNCS)='$SNCS' AND SNCS<>'' AND (IdTamizaje<>'$IdTamizaje')"; 
		$results = $conn->query($sql);
	  	$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function UpdateMaximoSNCS_M($Valor1){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =   "update SIGESA_BSD_Parametro set Valor1='$Valor1' WHERE codigo='MAXSNC'";
		$results = $conn->query($sql);		
		return $results;	
}

function ReporteSNCS_M($FechaTamizajeIni,$FechaTamizajeFin,$RealizaNAT,$NroDonacionBus){
		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql = "exec [SIGESA_BSD_ReporteSNCS_MHR] '".$FechaTamizajeIni."','".$FechaTamizajeFin."','".$RealizaNAT."','".$NroDonacionBus."'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ArchivarTamizaje_M($EstadoArchivado,$FechaArchiva,$UsuArchiva,$FechaTamizajeIni,$FechaTamizajeFin,$RealizaNAT,$NroDonacionBus){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =  "exec [SIGESA_BSD_ArchivarTamizaje_MHR] '".$EstadoArchivado."','".$FechaArchiva."','".$UsuArchiva."','".$FechaTamizajeIni."','".$FechaTamizajeFin."','".$RealizaNAT."','".$NroDonacionBus."'";
		$results = $conn->query($sql);		
		return $results;	
}
//FIN FUNCIONES PARA Nº SNCS y NO ingresar ningún resultado de tamizaje

//Inicio PRUEBA CONFIRMATORIA
function ReporteDonantesReactivosM($NroDocumento){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_BSD_ReporteDonantesReactivos_MHR'".$NroDocumento."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BuscarDonantesFiltroM($valor,$Pivotx){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_BuscarDonantes_MHR '".$valor."','".$Pivotx."' ";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT NroDocumento,IdPruebaConfirmatoria,FechaPrueba,IdResponsablePrueba,DocPruebaConfirma,NombreDoc,ResultadoFinal,ValorMedicion,Comentario,PruebaReactivo,
				(SELECT Usuario FROM Empleados WHERE DNI=IdResponsablePrueba)AS ResponsablePrueba
  				FROM SIGESA_BSD_PruebaConfirmatoria WHERE NroDocumento='$NroDocumento' and PruebaReactivo='$PruebaReactivo'";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		//$stmt = null;
		//$conn = null; 
		return $validar;	
}

function SIGESA_BuscarPruebaConfirmatoriaPorIdM($IdPruebaConfirmatoria){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT DocPruebaConfirma,NombreDoc
				FROM SIGESA_BSD_PruebaConfirmatoria WHERE IdPruebaConfirmatoria='$IdPruebaConfirmatoria'";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function SIGESA_BSD_ListarDonantesEstadoDefinitivoM($NroDocumento){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_BSD_ListarDonantesEstadoDefinitivo_MHR '".$NroDocumento."'";
	 	
		$validar=NULL;	  
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function GuardarPruebaConfirmatoriaM($NroDocumento, $IdTamizaje, $NroDonacion, $FechaPrueba, $IdResponsablePrueba, $PruebaReactivo, $DocPruebaConfirma, $NombreDoc, $ResultadoFinal, $ValorMedicion, $Comentario, $FechaReg, $UsuReg){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =  "exec [SIGESA_BSD_GuardarPruebaConfirmatoria_MHR] '".$NroDocumento."','".$IdTamizaje."','".$NroDonacion."','".$FechaPrueba."','".$IdResponsablePrueba."','".$PruebaReactivo."',".$DocPruebaConfirma.",'".$NombreDoc."','".$ResultadoFinal."','".$ValorMedicion."','".$Comentario."','".$FechaReg."','".$UsuReg."'";
		$results = $conn->query($sql);		
		return $results;	
}

//Fin PRUEBA CONFIRMATORIA

//INICIO ETIQUETAS falta modi en servidor
function SIGESA_BSD_ListarEtiquetasAlicuotaM($NroDonacion){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ListarEtiquetasAlicuota_MHR]'".$NroDonacion."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function SIGESA_BSD_ListarEtiquetasReceptorM($NroHistoriaClinica){	
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SIGESA_BSD_ListarEtiquetasReceptor_MHR]'".$NroHistoriaClinica."'"; 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

/*function SigesaLaboratorioMostarxGrupo_M($IdOrden){
		 
		require('cn/cn_sqlserver_server_locla.php');
	      $sql = "exec [SigesaLaboratorioMostarxGrupo]'".$IdOrden."'";
		  
		  
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}*/	

function SigesaLaboratorioXidOrden_M($IdOrden)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SigesaLaboratorioXidOrden '".$IdOrden."'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
	}	
	
function SigesaLaboratorioMostarxGrupoDetalle_M($IdOrden){
		 
		require('cn/cn_sqlserver_server_locla.php');
	      $sql = "select
				 LabMovimientoLaboratorio.IdOrden,
					(SELECT  
					CASE WHEN tubos.Siglas is not null THEN  tubos.Siglas COLLATE DATABASE_DEFAULT 
					ELSE FactCatalogoServicios.Nombre END)as Nombrefinal
				 from  LabMovimientoLaboratorio
				 inner join LabMovimientoCPT on LabMovimientoCPT.idMovimiento=LabMovimientoLaboratorio.IdMovimiento
				 inner join FactCatalogoServicios on FactCatalogoServicios.IdProducto=LabMovimientoCPT.idProductoCPT
				 left join SigesLaboratorioTubosGrupo tubos on tubos.idProducto= LabMovimientoCPT.idProductoCPT
				 where LabMovimientoLaboratorio.IdOrden='$IdOrden'"; 		  
		 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
	
	}
//FIN ETIQUETAS

//PARTE 2

function ReporteSNCSDinamico_M($RealizaNAT,$TieneSNCS,$lcFiltroComun,$lcFiltro){
		 
		require('cn/cn_sqlserver_server_locla.php');
		   $sql = "exec [SIGESA_BSD_ReporteSNCSDinamico_MHR] '".$RealizaNAT."','".$TieneSNCS."','".$lcFiltroComun."','".$lcFiltro."'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ObtenerGrupoSanguineo_M($IdGrupoSanguineo)	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select * from SIGESA_BSD_GrupoSanguineo_MHR where IdGrupoSanguineo='$IdGrupoSanguineo'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
	}	


/*function ObtenerDatosAtencionPaciente_M($IdPaciente)	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "SELECT top 1 ate.IdTipoServicio,(SELECT Descripcion FROM TiposServicio tips WHERE tips.IdTipoServicio=ate.IdTipoServicio )AS DescripcionTiposServicio,
				atehos.IdServicio,(SELECT Nombre FROM Servicios ser WHERE ser.IdServicio=atehos.IdServicio)AS NombreServicio,
				(select camas.Codigo  from camas where camas.IdCama =atehos.IdCama )as cama
				FROM Atenciones ate inner join AtencionesEstanciaHospitalaria atehos on ate.IdAtencion=atehos.IdAtencion				
				WHERE ate.idEstadoAtencion=1 and IdTipoServicio in (2,3,4)
				and ate.IdPaciente='$IdPaciente'
				order by IdCuentaAtencion desc,Secuencia desc"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
	}*/	

function GuardarVerificacionAptosM($NroDonacion, $SNCS_PG, $SNCS_PFC, $SNCS_PQ, $IdGrupoSanguineo, $IdGrupoSanguineoAnterior, $FechaVerifica, $IdResponsable, $DeteccionCI, $CoombsDirecto, $DVI, /*$FenC, $FenE, $Fencc, $Fenee, $FenCw, $FenK, $Fenkk,*/ $Fenotipo, $Observacion, $EstadoHemo, $IdFraccionamiento, $UsuReg, $FechaReg, $IdPostulante){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =  "exec [SIGESA_BSD_GuardarVerificacionAptos_MHR] '".$NroDonacion."','".$SNCS_PG."','".$SNCS_PFC."','".$SNCS_PQ."','".$IdGrupoSanguineo."','".$IdGrupoSanguineoAnterior."','".$FechaVerifica."','".$IdResponsable."','".$DeteccionCI."','".$CoombsDirecto."','".$DVI."','".$Fenotipo."','".$Observacion."','".$EstadoHemo."','".$IdFraccionamiento."','".$UsuReg."','".$FechaReg."','".$IdPostulante."'";
		$results = $conn->query($sql);		
		return $results;	
}

function UpdFraccionamientoM($IdFraccionamiento,$MotivoElimPaqueteGlobu,$MotivoElimPlasma,$MotivoElimPlaquetas){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql =  "exec [SIGESA_BSD_UpdFraccionamiento_MHR] '".$IdFraccionamiento."','".$MotivoElimPaqueteGlobu."','".$MotivoElimPlasma."','".$MotivoElimPlaquetas."'";
		$results = $conn->query($sql);		
		return $results;	
}

function ObtenerDatosVerificacionAptos_M($IdFraccionamiento){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select * from SIGESA_BSD_VerificacionAptos where IdFraccionamiento='$IdFraccionamiento'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function SIGESA_BSD_ReporteStock_M($IdGrupoSanguineo){
		require('cn/cn_sqlserver_server_locla.php');
	   $sql =  "exec [SIGESA_BSD_ReporteStock_MHR] '".$IdGrupoSanguineo."'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function SIGESA_BSD_ReporteStockCuarentena_M($IdGrupoSanguineo){
		require('cn/cn_sqlserver_server_locla.php');
	   $sql =  "exec [SIGESA_BSD_ReporteStockCuarentena_MHR] '".$IdGrupoSanguineo."'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function SIGESA_BSD_UpdVerificacionAptos_M($SNCS_PG, $SNCS_PFC, $SNCS_PQ){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_UpdVerificacionAptos_MHR] '".$SNCS_PG."','".$SNCS_PFC."','".$SNCS_PQ."'";
		$results = $conn->query($sql);		
		return $results;
}

function GuardarMovSangreCabeceraM($MovTipo, $TipoHem, $IdEstablecimientoIngreso, $IdEstablecimientoEgreso, $UsuarioRecibe, $FechaRecibe, $IdPaciente, $UsuarioSalida, $FechaSalida, $FechaPreparaFraccion, $MetodoPreparaFraccion, $Estado, $Observacion, $UsuReg, $FecReg){ 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarMovSangreCabecera_MHR] '".$MovTipo."','".$TipoHem."','".$IdEstablecimientoIngreso."','".$IdEstablecimientoEgreso."','".$UsuarioRecibe."','".$FechaRecibe."','".$IdPaciente."','".$UsuarioSalida."','".$FechaSalida."','".$FechaPreparaFraccion."','".$MetodoPreparaFraccion."','".$Estado."','".$Observacion."','".$UsuReg."','".$FecReg."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function GuardarMovSangreDetalleM($MovNumero, $MovTipo, $IdGrupoSanguineo, $NroDonacion, $NroTabuladora, $TipoHem, $SNCS, $FechaExtraccion, $FechaVencimiento, $VolumenTotRecol, $VolumenFraccion, $VolumenRestante, $IdTipoDonacion, $IdTipoDonante, $swReserva, $IdPacienteReserva, $FechaReserva, $UsuReserva, $Estado, $MotivoEstado, $UsuMotivo, $FecMotivo, $ResultadoCustodia, $Observacion, $UsuReg, $FecReg){ 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarMovSangreDetalle_MHR] '".$MovNumero."','".$MovTipo."','".$IdGrupoSanguineo."','".$NroDonacion."','".$NroTabuladora."','".$TipoHem."','".$SNCS."','".$FechaExtraccion."','".$FechaVencimiento."','".$VolumenTotRecol."','".$VolumenFraccion."','".$VolumenRestante."','".$IdTipoDonacion."','".$IdTipoDonante."','".$swReserva."','".$IdPacienteReserva."','".$FechaReserva."','".$UsuReserva."','".$Estado."','".$MotivoEstado."','".$UsuMotivo."','".$FecMotivo."','".$ResultadoCustodia."','".$Observacion."','".$UsuReg."','".$FecReg."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function ObtenerDatosDonacionConSNCS_M($SNCS){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "SELECT SNCS,pos.IdGrupoSanguineo,mov.IdTipoDonacion,mov.IdTipoDonante,
				ext.NroDonacion,frac.NroTabuladora,ext.FechaExtraccion,frac.VolumenPaqueteGlobu,frac.VolumenPlasma,frac.VolumenPlaquetas,
				rec.IdPaciente
				 FROM SIGESA_BSD_Tamizaje tam 
					INNER JOIN SIGESA_BSD_ExtraccionSangre ext on tam.NroDonacion=ext.NroDonacion
					INNER JOIN SIGESA_BSD_FraccionamientoSangre frac on ext.IdExtraccion=frac.IdExtraccion
					INNER JOIN SIGESA_BSD_Movimientos mov on mov.IdMovimiento=tam.IdMovimiento
					INNER JOIN SIGESA_BSD_Postulante pos on pos.IdPostulante=mov.IdPostulante
					LEFT JOIN SIGESA_BSD_Receptor rec on rec.IdReceptor=mov.IdReceptor
				where SNCS='$SNCS'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function ListarEstablecimientos_M($IdEstablecimiento,$texto){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "SELECT '0' IdEstablecimiento, '' Codigo, 
				''CodigoNombre,' Todos' Nombre,
				''Distrito,	''Provincia,	''IdDepartamento,	''Departamento
				
				UNION ALL				
				SELECT '10000' IdEstablecimiento, 'SAMU' Codigo, 
				'SAMU'CodigoNombre,'SAMU' Nombre,
				''Distrito,	''Provincia,	''IdDepartamento,	''Departamento

				UNION ALL
				Select Establecimientos.IdEstablecimiento,Establecimientos.Codigo,
				UPPER(Codigo+' - '+Establecimientos.Nombre)AS CodigoNombre,Establecimientos.Nombre,
				Distritos.Nombre as Distrito,Provincias.Nombre as Provincia,
				Departamentos.IdDepartamento,Departamentos.Nombre as Departamento
				from Establecimientos 
				inner join Distritos on Establecimientos.IdDistrito=Distritos.IdDistrito
				inner join Provincias on Distritos.IdProvincia=Provincias.IdProvincia
				inner join Departamentos on Provincias.IdDepartamento=Departamentos.IdDepartamento
				where Departamentos.IdDepartamento in (7,15) and Establecimientos.IdTipo=1 
				and (Establecimientos.IdEstablecimiento='$IdEstablecimiento' or $IdEstablecimiento=0) AND Establecimientos.Nombre LIKE '%$texto%'
				order by Nombre"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function BuscarSNCSDisponible_M($TipoHem,$SNCS){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select cab.IdEstablecimientoIngreso,det.*,
		(select Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=det.IdGrupoSanguineo)as GrupoSanguineo
	    from SIGESA_BSD_MovSangreCabecera cab
	    inner join SIGESA_BSD_MovSangreDetalle det on cab.MovNumero=det.MovNumero
	    where (det.Estado=1 AND (DATEDIFF(day,GETDATE(),det.FechaVencimiento))>=0) and det.MovTipo='I' and det.TipoHem='$TipoHem' and det.SNCS='$SNCS' "; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function BuscarSNCSTodos_M($TipoHem,$SNCS,$Estado){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_BuscarMovSangreTodos_MHR] '".$TipoHem."','".$SNCS."','".$Estado."'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function BuscarMovSangrePorIdMovDetalle_M($IdMovDetalle){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_BuscarMovSangrePorIdMovDetalle_MHR] '".$IdMovDetalle."'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function CambiarEstadoMovSangreDetalle($TipoHem, $SNCS, $EstadoActual, $MovNumero){
	require('cn/cn_sqlserver_server_locla.php');
	    //$sql =  "update SIGESA_BSD_MovSangreDetalle set Estado='$EstadoActual' where IdMovDetalle='$IdMovDetalle'";
		$sql =  "exec [SIGESA_BSD_CambiarEstadoMovSangreDetalle_MHR] '".$TipoHem."','".$SNCS."','".$EstadoActual."','".$MovNumero."'";
		$results = $conn->query($sql);		
		return $results;
}

function BuscarSNCSEnviado_M($IdEstablecimientoEgreso,$FechaSalida){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select cab.IdEstablecimientoEgreso,(select Nombre from Establecimientos est where est.IdEstablecimiento=cab.IdEstablecimientoEgreso)as EstablecimientoEgreso, det.*,
	    		(select Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=det.IdGrupoSanguineo)as GrupoSanguineo
	            from SIGESA_BSD_MovSangreCabecera cab
				inner join SIGESA_BSD_MovSangreDetalle det on cab.MovNumero=det.MovNumero
				where cab.Estado=1 and cab.MovTipo='E' and (IdEstablecimientoEgreso='$IdEstablecimientoEgreso' or '$IdEstablecimientoEgreso'='0') and FechaSalida='$FechaSalida'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function BuscarSNCSRecibido_M($IdEstablecimientoIngreso,$FechaRecibe){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select cab.IdEstablecimientoIngreso,(select Nombre from Establecimientos est where est.IdEstablecimiento=cab.IdEstablecimientoIngreso)as EstablecimientoIngreso, det.*,
	    		(select Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=det.IdGrupoSanguineo)as GrupoSanguineo
	            from SIGESA_BSD_MovSangreCabecera cab
				inner join SIGESA_BSD_MovSangreDetalle det on cab.MovNumero=det.MovNumero
				where cab.Estado=1 and cab.MovTipo='I' and (IdEstablecimientoIngreso='$IdEstablecimientoIngreso' or '$IdEstablecimientoIngreso'='0') and IdEstablecimientoIngreso<>'6216' and FechaRecibe='$FechaRecibe'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function GuardarEliminarHemocomponentesM($TipoHem,$SNCS,$FecMotivo,$UsuMotivo,$MotivoEstado,$Estado,$MovNumero){
	require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_GuardarEliminarHemocomponentes_MHR] '".$TipoHem."','".$SNCS."','".$FecMotivo."','".$UsuMotivo."','".$MotivoEstado."','".$Estado."','".$MovNumero."'";
		$results = $conn->query($sql);		
		return $results;
}

function GuardarCrioHemocomponentesM($TipoHem,$SNCS,$FechaConvirtio,$UsuConvirtio,$MovNumero,$VolumenRestante){
	require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_GuardarCrioHemocomponentes_MHR] '".$TipoHem."','".$SNCS."','".$FechaConvirtio."','".$UsuConvirtio."','".$MovNumero."','".$VolumenRestante."'";
		$results = $conn->query($sql);		
		return $results;
}

//Para Ver Fracciones
function ObtenerDatosIdMovDetalle_M($IdMovDetalle){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select * from SIGESA_BSD_MovSangreDetalle det 
				where det.Estado=1 and det.MovTipo='I' and IdMovDetalle='$IdMovDetalle'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function BuscarSNCSFraccion_M($TipoHem,$SNCS){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select cab.MetodoPreparaFraccion,cab.FechaPreparaFraccion,det.*,
		(select Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=det.IdGrupoSanguineo)as GrupoSanguineo
	    from SIGESA_BSD_MovSangreCabecera cab
	    inner join SIGESA_BSD_MovSangreDetalle det on cab.MovNumero=det.MovNumero
	    where det.Estado=3 and det.MovTipo='F' and det.TipoHem='$TipoHem' and det.SNCS='$SNCS' "; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function UpdateVolumenPorFraccion($IdMovDetalle,$VolumenFraccion,$FechaVencimiento){
	require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "update SIGESA_BSD_MovSangreDetalle set VolumenFraccion=VolumenFraccion+'$VolumenFraccion', VolumenRestante=VolumenRestante-$VolumenFraccion, FechaVencimiento='$FechaVencimiento' 
	    where IdMovDetalle='$IdMovDetalle'";
		$results = $conn->query($sql);		
		return $results;
}

//Custodia
function GuardarResultadoCustodia_M($TipoHem,$SNCS,$FecResultado,$UsuResultado,$ResultadoCustodia,$Estado,$MovNumero){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_GuardarResultadoCustodia_MHR] '".$TipoHem."','".$SNCS."','".$FecResultado."','".$UsuResultado."','".$ResultadoCustodia."','".$Estado."','".$MovNumero."'";
		$results = $conn->query($sql);		
		return $results;
}

function GuardarAgregarReserva_M($TipoHem,$SNCS,$IdMovDetalle,$MovNumero,$IdPacienteReserva,$UsuReserva,$FechaReserva,$MotivoAgregarReserva,$Estado){
	require('cn/cn_sqlserver_server_locla.php');
    $sql =  "exec [SIGESA_BSD_GuardarAgregarReserva_MHR] '".$TipoHem."','".$SNCS."','".$IdMovDetalle."','".$MovNumero."','".$IdPacienteReserva."','".$UsuReserva."','".$FechaReserva."','".$MotivoAgregarReserva."','".$Estado."'";
	$results = $conn->query($sql);		
	return $results;
}

function GuardarQuitarReserva_M($TipoHem,$SNCS,$IdMovDetalle,$MovNumero,$UsuQuitarReserva,$FecQuitarReserva,$MotivoQuitarReserva,$Estado){
	require('cn/cn_sqlserver_server_locla.php');
    $sql =  "exec [SIGESA_BSD_GuardarQuitarReserva_MHR] '".$TipoHem."','".$SNCS."','".$IdMovDetalle."','".$MovNumero."','".$UsuQuitarReserva."','".$FecQuitarReserva."','".$MotivoQuitarReserva."','".$Estado."'";
	$results = $conn->query($sql);		
	return $results;
}

function ObtenerDatosVerificacionAptosXSNCS_M($TipoHemSNCS){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "SELECT * from SIGESA_BSD_VerificacionAptos WHERE EstadoHemo=1 and (SNCS_PG='$TipoHemSNCS' OR SNCS_PFC='$TipoHemSNCS' OR	SNCS_PQ='$TipoHemSNCS') "; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function SIGESA_BSD_BuscarStock_M($TipoHem,$IdGrupoSanguineo,$Estado,$Vencido){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_BuscarStock_MHR] '".$TipoHem."','".$IdGrupoSanguineo."','".$Estado."','".$Vencido."'"; 	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
}

function Buscar_Diagnosticos_Emergencia_M($variable,$tipo_busqueda){	 
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec [SigesaDiagnosticosSeleccionarTodos] '".$variable."',".$tipo_busqueda."";	 	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}	
	return $validar;
}

//SOLICITUD SANGRE
function ListarMedicos_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select DISTINCT
e.IdEmpleado,	
e.ApellidoPaterno,	
e.ApellidoMaterno	,
e.Nombres	,
e.DNI	,
e.CodigoPlanilla	,
e.FechaIngreso,	
e.Usuario	,
e.loginPC,
s.Clave1,
s.IdServicio,m.IdMedico
from Empleados e  inner join SigesaUsuario s on  e.IdEmpleado=s.IdEmpleado
left join UsuariosRoles ur on ur.IdEmpleado=e.IdEmpleado
left join Medicos m on m.IdEmpleado=e.IdEmpleado
where (m.IdMedico is not null)";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

function GuardarSolicitudSangreCabeceraM($IdReceptor, $IdPaciente, $NroHistoria, $IdCuentaAtencion, $IdAtencion, $IdGrupoSanguineo, $FechaSolicitud, $IdTipoServicio, $IdServicio, $CodigoCama,/* $IdDiagnostico, $swAnemia1, $swAnemia2, $Hemoglobina, $Hematocrito, $Plaquetas, $swTransPre, $swReacTrans,
	$EmbarazoPrevio, $Aborto, $IncompatibilidadMF, $swConsentimiendoInformado, $MotivoNOConsentimiendo, $MedicoSolicitante, $UsuTomoMuestraPaciente,*/
	$UsuReg, $FecReg, $Estado){ 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarSolicitudSangreCabecera_MHR] '".$IdReceptor."','".$IdPaciente."','".$NroHistoria."','".$IdCuentaAtencion."','".$IdAtencion."','".$IdGrupoSanguineo."','".$FechaSolicitud."','".$IdTipoServicio."','".$IdServicio."','".$CodigoCama."','".//$IdDiagnostico."','".$swAnemia1."','".$swAnemia2."','".$Hemoglobina."','".$Hematocrito."','".$Plaquetas."','".$swTransPre."','".$swReacTrans."','".$EmbarazoPrevio."','".$Aborto."','".$IncompatibilidadMF."','".$swConsentimiendoInformado."','".$MotivoNOConsentimiendo."','".$MedicoSolicitante."','".$UsuTomoMuestraPaciente."','".
		$UsuReg."','".$FecReg."','".$Estado."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function GuardarSolicitudSangreDetalleM($IdSolicitudSangre, $CodigoComponente, $Cantidad, $TipoRequerimiento, $UsuReg, $FecReg, $FechaReservaOper, $FechaProg, $Volumen){ 
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarSolicitudSangreDetalle_MHR] '".$IdSolicitudSangre."','".$CodigoComponente."','".$Cantidad."','".$TipoRequerimiento."','".$UsuReg."','".$FecReg."','".$FechaReservaOper."','".$FechaProg."','".$Volumen."'";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function GuardarSolicitudSangreCabeceraResumenM($IdSolicitudSangre, $IdDiagnostico1, $IdDiagnostico2, $Hemoglobina, $Hematocrito, $swAnemia1, $swAnemia2, $Tromboplastina, $Protrombina, $swPlasma1, $Plaquetas, $swPQ1, 
	$swTransPre, $swReacTrans, $EmbarazoPrevio, $Aborto, $IncompatibilidadMF, $swConsentimiendoInformado, $MotivoNOConsentimiendo, $MedicoSolicitante, $UsuTomoMuestraPaciente, $UsuRegResumen, $FecRegResumen, $Estado, $swReacAler, $PesoReceptor, $INR){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarSolicitudSangreCabeceraResumen_MHR] '".$IdSolicitudSangre."','".$IdDiagnostico1."','".$IdDiagnostico2."','".$Hemoglobina."','".$Hematocrito."','".$swAnemia1."','".$swAnemia2."','".$Tromboplastina."','".$Protrombina."','".$swPlasma1."','".$Plaquetas."','".$swPQ1."','".$swTransPre."','".$swReacTrans."','".$EmbarazoPrevio."','".$Aborto."','".$IncompatibilidadMF."','".$swConsentimiendoInformado."','".$MotivoNOConsentimiendo."','".$MedicoSolicitante."','".$UsuTomoMuestraPaciente."','".
		$UsuRegResumen."','".$FecRegResumen."','".$Estado."','".$swReacAler."','".$PesoReceptor."','".$INR."'";

		$results = $conn->query($sql);		
		return $results;
}

function ListarCabeceraSolicitudes_M($NroHistoria){ //Solicitudes Pendientes
		 
		require('cn/cn_sqlserver_server_locla.php');
		 $sql = "SELECT cab.*
				 FROM SIGESA_BSD_SolicitudSangreCabecera cab 
				 WHERE ('$NroHistoria'='0' or  cab.NroHistoria='$NroHistoria') and cab.Estado not in (0,5) ";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ListarSolicitudes_M($NroHistoria, $Estado){ //0,T Todos
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT --det.IdSolicitudSangre, 
				NroSolicitud, IdReceptor, IdPaciente, NroHistoria, IdCuentaAtencion, IdAtencion, IdGrupoSanguineo, 
				FechaSolicitud, IdTipoServicio, IdServicio, CodigoCama, --UsuReg, FecReg, 
				IdDiagnostico1, IdDiagnostico2, Hemoglobina, Hematocrito, swAnemia1, Tromboplastina, Protrombina, swPlasma1, Plaquetas, swPQ1, swTransPre, 
				swReacTrans, EmbarazoPrevio, Aborto, IncompatibilidadMF, swConsentimiendoInformado, MotivoNOConsentimiendo, 
				MedicoSolicitante, UsuTomoMuestraPaciente, UsuRegResumen, FecRegResumen, --Estado, 
				FechaRecepcion, UsuRecepcion, UsuRegRecepcion, DeteccionCI, CoombsDirecto, DVI, Fenotipo, UsuFenotipo, UsuRegFenotipo, FecRegFenotipo, IdOrdenFen,
				(select Servicios.Nombre from Servicios where Servicios.IdServicio=cab.IdServicio)as Servicio,
				(select grupo.Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=cab.IdGrupoSanguineo)as GrupoSanguineo,
				(select emp.Usuario from Empleados emp where emp.IdEmpleado=cab.UsuReg)as UsuarioReg, cab.Estado as EstadoCab,
				 det.*
		 		 FROM SIGESA_BSD_SolicitudSangreCabecera cab
				 inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdSolicitudSangre=cab.IdSolicitudSangre
				 WHERE ('$NroHistoria'='0' or  cab.NroHistoria='$NroHistoria') and ('$Estado'='T' or det.Estado='$Estado') and det.Estado not in (0,5) order by cab.NroSolicitud";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function GuardarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo, $ValorFenotipo, $Observacion, $Estado, $FechaReg, $UsuReg ){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarFenotipo_MHR] '".$TipoPersona."','".$IdPostulante."','".$IdPaciente."','".$TipoFenotipo."','".$ValorFenotipo."','".$Observacion."','".$Estado."','".$FechaReg."','".$UsuReg."'";

		$results = $conn->query($sql);		
		return $results;
}

function GuardarUpdFenotipoPersonaM($TipoPersona, $IdPostulante, $IdPaciente, $Fenotipo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdFenotipoPersona_MHR] '".$TipoPersona."','".$IdPostulante."','".$IdPaciente."','".$Fenotipo."'";
		$results = $conn->query($sql);		
		return $results;	
}

function GuardarUpdSolicitudSangreFenotipoM($IdSolicitudSangre, $DeteccionCI, $CoombsDirecto, $DVI, $Fenotipo, $UsuFenotipo, $UsuRegFenotipo, $FecRegFenotipo, $IdGrupoSanguineo, $EspecifidadAnticuerpo, $ObsFenotipo){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarUpdSolicitudSangreFenotipo_MHR] '".$IdSolicitudSangre."','".$DeteccionCI."','".$CoombsDirecto."','".$DVI."','".$Fenotipo."','".$UsuFenotipo."','".$UsuRegFenotipo."','".$FecRegFenotipo."','".$IdGrupoSanguineo."','".$EspecifidadAnticuerpo."','".$ObsFenotipo."'";
		$results = $conn->query($sql);		
		return $results;	
}

function GuardarRecepcionSolicitudM($IdSolicitudSangre, $FechaRecepcion, $UsuRecepcion, $UsuRegRecepcion){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarRecepcionSolicitud_MHR] '".$IdSolicitudSangre."','".$FechaRecepcion."','".$UsuRecepcion."','".$UsuRegRecepcion."'";
		$results = $conn->query($sql);		
		return $results;	
}

function GuardarDespachoSolicitudM($IdSolicitudSangre, $IdDetSolicitudSangre, $FechaDespacho, $UsuDespacho, $UsuRegDespacho, $TipoHem, $SNCS){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_GuardarDespachoSolicitud_MHR] '".$IdSolicitudSangre."','".$IdDetSolicitudSangre."','".$FechaDespacho."','".$UsuDespacho."','".$UsuRegDespacho."','".$TipoHem."','".$SNCS."'";
		$results = $conn->query($sql);		
		return $results;	
}

function ListarHemDisponiblesM($TipoHem,$VolumenPediatrico,$Filtro){

	require('cn/cn_sqlserver_server_locla.php');
		 //$sql = "SELECT d.*,(select Descripcion from SIGESA_BSD_GrupoSanguineo_MHR g where g.IdGrupoSanguineo=d.IdGrupoSanguineo)as GrupoSanguineo,
				//v.Fenotipo,v.IdPostulante FROM SIGESA_BSD_MovSangreDetalle d
				//left join SIGESA_BSD_VerificacionAptos v on v.NroDonacion=d.NroDonacion
				//where (Estado='1' AND (DATEDIFF(day,GETDATE(),FechaVencimiento))>=0) AND MovTipo='I' AND TipoHem='$TipoHem' ".$filtro;
	    $sql = "exec [SIGESA_BSD_ListarHemocomponentesDisponibles_MHR] '".$TipoHem."','".$VolumenPediatrico."','".$Filtro."'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function SIGESA_BSD_BuscarFenotipoM($TipoPersona, $IdPostulante, $IdPaciente, $TipoFenotipo){
	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_BuscarFenotipo_MHR] '".$TipoPersona."','".$IdPostulante."','".$IdPaciente."','".$TipoFenotipo."'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function GuardarUpdSepararHemocomponenteM($TipoHem, $SNCS, $FechaReg, $UsuReg, $Estado, $IdDetSolicitudSangre, $IdMovDetalle, $Puntuacion){ 		 
		require('cn/cn_sqlserver_server_locla.php');
		//$sql = "update SIGESA_BSD_SolicitudSangreDetalle set TipoHem='$TipoHem', SNCS='$SNCS' where IdDetSolicitudSangre='$IdDetSolicitudSangre' ";
		$sql = "exec [SIGESA_BSD_GuardarUpdSepararHemocomponente_MHR] '".$TipoHem."','".$SNCS."','".$FechaReg."','".$UsuReg."','".$Estado."','".$IdDetSolicitudSangre."','".$IdMovDetalle."','".$Puntuacion."'";		
	
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;
}

function SigesDevuelveServiciosQueSonPuntosCarga_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SigesDevuelveServiciosQueSonPuntosCarga] ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}	

function SigesaFactOrdenServicioExtraerIdorden_M($IdPuntoCarga ,
$IdPaciente ,
$IdCuentaAtencion ,
$IdServicioPaciente ,
$idTipoFinanciamiento ,
$idFuenteFinanciamiento ,
$FechaCreacion ,
$IdUsuario ){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SigesaFactOrdenServicioExtraerIdorden] '".$IdPuntoCarga."' ,'".$IdPaciente."' ,'".$IdCuentaAtencion."' ,'".$IdServicioPaciente."' ,'".$idTipoFinanciamiento."' ,'".$idFuenteFinanciamiento."' ,'".$FechaCreacion."' ,'".$IdUsuario."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

function FactOrdenServicioSeleccionarPorIdOrden_M($idOrden){
		 
		require('cn/cn_sqlserver_server_locla.php');
		 $sql = "exec [FactOrdenServicioSeleccionarPorIdOrden] '".$idOrden."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

function SIGESA_BSD_ConsultarPacientesHemoglobinaMenorSiete_M($FechaInicio,$FechaFinal){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_ConsultarPacientesHemoglobinaMenorSiete_MHR] '".$FechaInicio."','".$FechaFinal."'";
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;	
}

function VerFacturacionesBancoDeSangreM($IdDetSolicitudSangre){

	require('cn/cn_sqlserver_server_locla.php');
		/*$sql = "SELECT   --dbo.FacturacionServicioDespacho.*, dbo.FactCatalogoServicios.Codigo, dbo.FactCatalogoServicios.Nombre, 
		EstadosFacturacion.IdEstadoFacturacion,EstadosFacturacion.Descripcion as EstadoFacturacion, --IdEstadoFacturacion 1 atendido,4 Pagado,9 Anulado
		dbo.FactOrdenServicio.IdOrden, dbo.FactOrdenServicio.IdCuentaAtencion, 
		dbo.FactOrdenServicio.idTipoFinanciamiento, dbo.TiposFinanciamiento.Descripcion AS dfinanciamiento, 
		dbo.FactOrdenServicio.FechaCreacion, dbo.FactOrdenServicio.FechaDespacho,  
		dbo.FactOrdenServicio.IdEstadoFacturacion, dbo.FactOrdenServicio.idFuenteFinanciamiento, dbo.FactOrdenServicio.IdPuntoCarga, 
		dbo.FactOrdenServicio.IdServicioPaciente, (select SUM(Total) from FacturacionServicioDespacho d where d.idOrden=FactOrdenServicio.IdOrden)as TotalOrden,
		(select Servicios.Nombre from Servicios where Servicios.IdServicio=dbo.FactOrdenServicio.IdServicioPaciente) as NombreServicio
		FROM  dbo.FactOrdenServicio  
		--INNER JOIN dbo.FacturacionServicioDespacho ON dbo.FacturacionServicioDespacho.idOrden = dbo.FactOrdenServicio.IdOrden --LEFT OUTER JOIN
		LEFT OUTER JOIN dbo.TiposFinanciamiento ON dbo.FactOrdenServicio.idTipoFinanciamiento = dbo.TiposFinanciamiento.IdTipoFinanciamiento 
		LEFT OUTER JOIN dbo.EstadosFacturacion ON dbo.FactOrdenServicio.IdEstadoFacturacion = dbo.EstadosFacturacion.IdEstadoFacturacion 
		--LEFT OUTER JOIN dbo.FactCatalogoServicios ON dbo.FacturacionServicioDespacho.IdProducto = dbo.FactCatalogoServicios.IdProducto
		WHERE dbo.FactOrdenServicio.IdCuentaAtencion='$IdCuentaAtencion' and Idpuntocarga IN (1,11) AND CONVERT(DATE,FechaDespacho)=CONVERT(DATE,'$FechaDespacho')";
		//AND CONVERT(DATE,FechaDespacho)=CONVERT(DATE,'$FechaDespacho') NO, xq en f guarda la fecha servidor en el sistema*/
		
		$sql = "SELECT  
		EstadosFacturacion.IdEstadoFacturacion,EstadosFacturacion.Descripcion as EstadoFacturacion, --IdEstadoFacturacion 1 atendido,4 Pagado,9 Anulado
		dbo.FactOrdenServicio.IdOrden, dbo.FactOrdenServicio.IdCuentaAtencion, 
		dbo.FactOrdenServicio.idTipoFinanciamiento, dbo.TiposFinanciamiento.Descripcion AS dfinanciamiento, 
		dbo.FactOrdenServicio.FechaCreacion, dbo.FactOrdenServicio.FechaDespacho,  
		dbo.FactOrdenServicio.IdEstadoFacturacion, dbo.FactOrdenServicio.idFuenteFinanciamiento, dbo.FactOrdenServicio.IdPuntoCarga, 
		dbo.FactOrdenServicio.IdServicioPaciente, (select SUM(Total) from FacturacionServicioDespacho d where d.idOrden=FactOrdenServicio.IdOrden)as TotalOrden,
		(select Servicios.Nombre from Servicios where Servicios.IdServicio=dbo.FactOrdenServicio.IdServicioPaciente) as NombreServicio
		FROM  dbo.FactOrdenServicio  
		LEFT OUTER JOIN dbo.TiposFinanciamiento ON dbo.FactOrdenServicio.idTipoFinanciamiento = dbo.TiposFinanciamiento.IdTipoFinanciamiento 
		LEFT OUTER JOIN dbo.EstadosFacturacion ON dbo.FactOrdenServicio.IdEstadoFacturacion = dbo.EstadosFacturacion.IdEstadoFacturacion 
		INNER JOIN SIGESA_BSD_SolicitudSangreDetalleReserva res ON res.IdOrden=FactOrdenServicio.IdOrden
		where res.IdDetSolicitudSangre='$IdDetSolicitudSangre'";
		
		
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function VerSolicitudSangreDetalleReservaM($IdDetSolicitudSangre){

	require('cn/cn_sqlserver_server_locla.php');		
		$sql = "select r.*,d.IdGrupoSanguineo,(select Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo 
				where grupo.IdGrupoSanguineo=d.IdGrupoSanguineo) as GrupoSanguineo, d.NroDonacion, d.VolumenRestante as VolumenReserva,
				(select cabmov.IdEstablecimientoIngreso from SIGESA_BSD_MovSangreCabecera cabmov where cabmov.MovNumero=d.MovNumero)as IdEstablecimientoIngreso,
				(select emp.Usuario from Empleados emp where emp.IdEmpleado=r.UsuReg)as UsuarioReg,
				FactOrdenServicio.IdEstadoFacturacion
				from SIGESA_BSD_SolicitudSangreDetalleReserva r	
				INNER JOIN SIGESA_BSD_MovSangreDetalle d on d.IdMovDetalle=r.IdMovDetalle
				left join FactOrdenServicio on FactOrdenServicio.IdOrden=r.IdOrden
				where r.IdDetSolicitudSangre='$IdDetSolicitudSangre' and r.Estado<>0 ";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

/*function VerFacturacionM($IdOrden){

	require('cn/cn_sqlserver_server_locla.php');		
		$sql = "select * from FactOrdenServicio where FactOrdenServicio.IdOrden='$IdOrden' and FactOrdenServicio.IdEstadoFacturacion<>9";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}*/

function HistorialDonacionesM($IdPaciente){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_ReporteSolicitudesDespachadas_MHR] '".$IdPaciente."'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ListarSolicitudesPendientesBUT(){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_ListarSolicitudesPendientesBUT_MHR]";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ObtenerDatosDetSolicitudSangre($IdDetSolicitudSangre){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT cab.IdPaciente,cab.IdCuentaAtencion,
				det.FechaDespacho,det.Estado as EstadoDet
				FROM SIGESA_BSD_SolicitudSangreCabecera cab  
				inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdSolicitudSangre=cab.IdSolicitudSangre
				where IdDetSolicitudSangre='$IdDetSolicitudSangre'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function GuardarRegresoBUT($IdReserva,$FechaRecBUT,$UsuSerUsuBUT,$UsuRegBUT,$RegresoBUT){
	require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "update SIGESA_BSD_SolicitudSangreDetalleReserva 
				 set FechaRecBUT='$FechaRecBUT', UsuSerUsuBUT='$UsuSerUsuBUT', UsuRegBUT='$UsuRegBUT',RegresoBUT='$RegresoBUT' 
				 where IdReserva='$IdReserva'";
		$results = $conn->query($sql);		
		return $results;
}

function ObtenerDatosIdReservaSangre($IdReserva){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT r.IdOrden,r.TipoHem,r.SNCS 
				from SIGESA_BSD_SolicitudSangreDetalleReserva r
				where r.IdReserva='$IdReserva'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function UpdateOrdenReserva($IdReserva,$idOrden){ 
	require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec [SIGESA_BSD_GuardarUpdOrdenReserva_MHR] '".$IdReserva."','".$idOrden."'";
		$results = $conn->query($sql);		
		return $results;
}

function UpdateOrdenFenotipo($IdSolicitudSangre,$idOrden){ 
	require('cn/cn_sqlserver_server_locla.php');
	    $sql = "update SIGESA_BSD_SolicitudSangreCabecera set IdOrdenFen='$idOrden' where IdSolicitudSangre='$IdSolicitudSangre'";
		$results = $conn->query($sql);		
		return $results;
}

function CerrarDetSolicitud($IdDetSolicitudSangre,$FechaCierre,$UsuRegCierre){ 
	require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec [SIGESA_BSD_CerrarDetSolicitud_MHR] '".$IdDetSolicitudSangre."','".$FechaCierre."','".$UsuRegCierre."'";
		$results = $conn->query($sql);		
		return $results;
}

function EliminarDetSolicitud($IdDetSolicitudSangre,$ObsEstado){ 
	require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec [SIGESA_BSD_EliminarDetSolicitud_MHR] '".$IdDetSolicitudSangre."','".$ObsEstado."'";
		$results = $conn->query($sql);		
		return $results;
} 

function QuitarReserva($IdDetSolicitudSangre,$TipoHem,$SNCS,$IdReserva,$MovNumero,$UsuRegEstado,$FecRegEstado){ 
	require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec [SIGESA_BSD_QuitarReserva_MHR] '".$IdDetSolicitudSangre."','".$TipoHem."','".$SNCS."','".$IdReserva."','".$MovNumero."','".$UsuRegEstado."','".$FecRegEstado."'";
		$results = $conn->query($sql);		
		return $results;
}

//INICIO COPIADO SERVIDOR 25-09-2018
function ConsultarPacientesHospitalizadosRecibieronSangre($IdServicio){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_ConsultarPacientesHospitalizadosRecibieronSangre_MHR] '".$IdServicio."'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ObtenerCantidadTipoHem($IdPaciente,$TipoHem){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "Select COUNT(*)as Cantidad
				from  SIGESA_BSD_SolicitudSangreCabecera solicitud 
				inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdSolicitudSangre=solicitud.IdSolicitudSangre 
				inner join SIGESA_BSD_SolicitudSangreDetalleReserva res on res.IdDetSolicitudSangre=det.IdDetSolicitudSangre 
				where res.Estado in (3,4,5) AND solicitud.IdPaciente='$IdPaciente' and res.TipoHem='$TipoHem'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function PacientesDebenSangre($NroHistoriaClinica){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "Select distinct  MAX(det.FechaDespacho) as FechaUltDespacho, 
				Pacientes.IdPaciente,Pacientes.NroHistoriaClinica,
				(CASE WHEN dbo.Pacientes.ApellidoPaterno	IS NULL THEN ' ' ELSE dbo.Pacientes.ApellidoPaterno END + ' ' +
				CASE WHEN dbo.Pacientes.ApellidoMaterno	IS NULL THEN ' ' ELSE dbo.Pacientes.ApellidoMaterno END + ' ' +
				CASE WHEN dbo.Pacientes.PrimerNombre		IS NULL THEN ' ' ELSE dbo.Pacientes.PrimerNombre END + ' ' +
				CASE WHEN dbo.Pacientes.SegundoNombre		IS NULL THEN ' ' ELSE dbo.Pacientes.SegundoNombre END + ' ' +
				CASE WHEN dbo.Pacientes.TercerNombre	IS NULL THEN ' ' ELSE dbo.Pacientes.TercerNombre END) AS Paciente,
				(select grupo.Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo
				inner join SIGESA_BSD_Receptor rec on rec.IdGrupoSanguineo=grupo.IdGrupoSanguineo
				where rec.IdPaciente=Pacientes.IdPaciente)as GrupoSanguineoPaciente
				 
				from  SIGESA_BSD_SolicitudSangreCabecera solicitud 
				inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdSolicitudSangre=solicitud.IdSolicitudSangre 
				inner join SIGESA_BSD_SolicitudSangreDetalleReserva res on res.IdDetSolicitudSangre=det.IdDetSolicitudSangre 
				inner join dbo.Pacientes ON solicitud.IdPaciente = dbo.Pacientes.IdPaciente 
				left join SIGESA_BSD_Receptor rec ON rec.IdPaciente=solicitud.IdPaciente
				where res.Estado in (3,4,5) AND (NroHistoriaClinica='$NroHistoriaClinica' or $NroHistoriaClinica=0)
				group by Pacientes.IdPaciente,Pacientes.NroHistoriaClinica,Pacientes.ApellidoPaterno,Pacientes.ApellidoMaterno,
				Pacientes.PrimerNombre,Pacientes.SegundoNombre,Pacientes.TercerNombre";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ObtenerCantidadDonantesTraidos($IdPaciente){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "Select COUNT(*)as Cantidad from SIGESA_BSD_Movimientos mov 
				inner join SIGESA_BSD_Receptor rec on rec.IdReceptor=mov.IdReceptor
				inner join SIGESA_BSD_ExtraccionSangre ext on ext.NroDonacion=mov.NroDonacion
				where ext.Estado<>'0' and ext.EstadoApto='1' and rec.IdPaciente='$IdPaciente'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function SIGESA_BSD_ListarEtiquetaTransfusionesM($IdDetSolicitudSangre){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT
				 cab.NroSolicitud, cab.IdReceptor, cab.IdPaciente, 				
				(select grupo.Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=cab.IdGrupoSanguineo)as GrupoSanguineo,	
				 pac.NroHistoriaClinica,pac.NroDocumento,pac.ApellidoPaterno,pac.ApellidoMaterno,pac.PrimerNombre,pac.SegundoNombre,pac.TercerNombre,pac.FechaNacimiento,sex.Descripcion as Sexo,			
				 det.IdDetSolicitudSangre,det.CodigoComponente,cab.FechaRecepcion
		 		 FROM SIGESA_BSD_SolicitudSangreCabecera cab
				 inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdSolicitudSangre=cab.IdSolicitudSangre
				 inner join Pacientes pac on pac.IdPaciente=cab.IdPaciente
				 inner join TiposSexo sex on sex.IdTipoSexo=pac.IdTipoSexo
				 WHERE det.IdDetSolicitudSangre='$IdDetSolicitudSangre' and det.Estado<>0";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ListarServiciosHospitalizacionEmergenciaM(){		 
	require('cn/cn_sqlserver_server_locla.php');
	  $sql = "select * from Servicios where  IdTipoServicio in (2,3,4) order by Nombre";
	$results = $conn->query($sql);
	
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
		  $validar[] = $row;
	}
		
	$stmt = null;
	$conn = null; 
	return $validar;	
}

function ReporteArchivoDefinitivo_M($NroDonacion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		    $sql = "exec [SIGESA_BSD_ReporteArchivoDefinitivo_MHR] '".$NroDonacion."'";
		$results = $conn->query($sql);
	  
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		 
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function DesarchivarTamizaje_M($EstadoArchivado,$ObservacionDesarchiva,$NroDonacion){
		require('cn/cn_sqlserver_server_locla.php');
		$sql =  "exec [SIGESA_BSD_DesarchivarTamizaje_MHR] '".$EstadoArchivado."','".$ObservacionDesarchiva."','".$NroDonacion."'";
		$results = $conn->query($sql);		
		return $results;
}
//FIN COPIADO SERVIDOR 25-09-2018

function ReporteMovimientosUnidadesAptasM($TipoHem, $SNCS){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_BSD_ReporteMovimientosUnidadesAptas_MHR] '".$TipoHem."','".$SNCS."'";		
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ImprimirSolicitud($IdSolicitudSangre,$IdDetSolicitudSangre){

	require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT cab.IdSolicitudSangre, 
				NroSolicitud, IdReceptor, IdPaciente, NroHistoria, IdCuentaAtencion, IdAtencion, IdGrupoSanguineo, 
				FechaSolicitud, IdTipoServicio, IdServicio, CodigoCama, --UsuReg, FecReg, 
				IdDiagnostico1, (select CodigoCIE2004+' '+Descripcion from Diagnosticos Di where Di.IdDiagnostico=IdDiagnostico1)as Diagnostico1,
				IdDiagnostico2, (select CodigoCIE2004+' '+Descripcion from Diagnosticos Di where Di.IdDiagnostico=IdDiagnostico2)as Diagnostico2,
				Hemoglobina, Hematocrito, swAnemia1, swAnemia2, Tromboplastina, Protrombina, INR, swPlasma1, Plaquetas, swPQ1,
				swTransPre, swReacTrans, swReacAler, EmbarazoPrevio, Aborto, IncompatibilidadMF, 
				swConsentimiendoInformado, MotivoNOConsentimiendo, 
				MedicoSolicitante,FechaSolicitud, UsuTomoMuestraPaciente, UsuRecepcion, FechaRecepcion, UsuRegRecepcion, 
				(select Colegiatura from Medicos m where m.IdEmpleado=cab.MedicoSolicitante )AS  cmpSolicitante,				
				UsuRegResumen, FecRegResumen, --Estado, 				
				DeteccionCI, CoombsDirecto, DVI, Fenotipo, UsuFenotipo, UsuRegFenotipo, FecRegFenotipo,	EspecifidadAnticuerpo, ObsFenotipo,			
				(select Servicios.Nombre from Servicios where Servicios.IdServicio=cab.IdServicio)as Servicio,
				(select grupo.Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=cab.IdGrupoSanguineo)as GrupoSanguineo,
				(select emp.Usuario from Empleados emp where emp.IdEmpleado=cab.UsuReg)as UsuarioReg,PesoReceptor,
				det.CodigoComponente,det.Cantidad,det.Volumen,det.TipoRequerimiento,det.FechaProg,det.IdDetSolicitudSangre,det.Estado
				 						
		 		 FROM SIGESA_BSD_SolicitudSangreCabecera cab
				 inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdSolicitudSangre=cab.IdSolicitudSangre
				 WHERE cab.IdSolicitudSangre='$IdSolicitudSangre' and (IdDetSolicitudSangre='$IdDetSolicitudSangre' or $IdDetSolicitudSangre=0)";		
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	

}

function ImprimirSolicitudDatosDonante($IdSolicitudSangre,$IdDetSolicitudSangre){

	require('cn/cn_sqlserver_server_locla.php');

		$sql = "SELECT det.IdSolicitudSangre,det.IdDetSolicitudSangre,det.FechaDespacho,det.UsuDespacho,
				detr.FechaReg as FechaReserva,detr.UsuReg as UsuReserva,
				(select Colegiatura from Medicos m where m.IdEmpleado=detr.UsuReg )AS  cmpReserva,	
				detr.Estado,detr.IdOrden,detr.RegresoBUT, 
				p.IdPostulante,(
				CASE WHEN p.PrimerNombre			IS NULL THEN ' ' ELSE p.PrimerNombre    END + ' ' +
				CASE WHEN p.SegundoNombre			IS NULL THEN ' ' ELSE p.SegundoNombre   END + ' ' +
				CASE WHEN p.TercerNombre			IS NULL THEN ' ' ELSE p.TercerNombre    END
				) AS NombresPostulante,			
				p.ApellidoPaterno+ ' ' +p.ApellidoMaterno AS ApellidosPostulante,		
				(select Nombre from Establecimientos e inner join SIGESA_BSD_MovSangreCabecera c on c.IdEstablecimientoIngreso=e.IdEstablecimiento 
				where c.MovNumero=d.MovNumero )as HospitalOrigen,
				d.NroDonacion,MovTipo, 
				d.TipoHem, d.SNCS,d.Observacion,
				(select Descripcion from SIGESA_BSD_GrupoSanguineo_MHR g where g.IdGrupoSanguineo=d.IdGrupoSanguineo)as GrupoSanguineo,
				d.VolumenRestante,d.NroTabuladora					
				--v.Fenotipo						
				FROM SIGESA_BSD_MovSangreDetalle d						
				inner join SIGESA_BSD_SolicitudSangreDetalleReserva detr on detr.IdMovDetalle=d.IdMovDetalle
				inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdDetSolicitudSangre=detr.IdDetSolicitudSangre 
				
				left join SIGESA_BSD_VerificacionAptos v on v.NroDonacion=d.NroDonacion --NO HAY DATOS PARA LOS INGRESOS DE OTRO HOSPITAL
				left join SIGESA_BSD_Postulante p on p.IdPostulante=v.IdPostulante
				where MovTipo in ('I','F') AND d.Estado='6' and det.Estado in (3,4,5) --d.Estado='6' despachado y det.Estado in (3,4,5) despachado, facturado y cerrado	
				and det.IdSolicitudSangre='$IdSolicitudSangre' and (det.IdDetSolicitudSangre='$IdDetSolicitudSangre' or '$IdDetSolicitudSangre'=0)";


		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	

}

function ReporteGeneralTransfusionesM($fecha_inicio,$fecha_fin){	
	require('cn/cn_sqlserver_server_locla.php');	
	 $sql = "exec [SIGESA_BSD_ReporteGeneralTransfusiones_MHR] '".$fecha_inicio."','".$fecha_fin."'";	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function SigesaFactOrdenServicioPagosExtraerIdordenpago_M($idOrden,$FechaCreacion,$IdUsuario){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SigesaFactOrdenServicioPagosExtraerIdordenpago] '".$idOrden."' ,'".$FechaCreacion."' ,'".$IdUsuario."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

function FacturacionServicioPagosFiltraPorIdOrden_M($idOrden){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FacturacionServicioPagosFiltraPorIdOrden] '".$idOrden."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

function ConsultarHemocomponentes($MovTipo,$Estado,$TipoHem){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_ConsultarHemocomponentes_MHR] '".$MovTipo."','".$Estado."','".$TipoHem."'";
		$results = $conn->query($sql);		
		return $results;
}

function ObtenerUltFenotipoPacienteSolicitud($IdPaciente, $IdSolicitudSangre){

	require('cn/cn_sqlserver_server_locla.php');

		$sql = "select TOP 1 Fenotipo
				from Pacientes pos
				inner join SIGESA_BSD_SolicitudSangreCabecera mov on mov.IdPaciente=pos.IdPaciente
				where mov.Fenotipo<>'' and pos.IdPaciente='$IdPaciente'  and mov.IdSolicitudSangre<>'$IdSolicitudSangre'
				order by mov.IdSolicitudSangre desc";

		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;
}

function SIGESA_BSD_EnviarDonantesTablaEnvio($NroDonanteTodos){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_EnviarDonantesTablaEnvio_MHR] '".$NroDonanteTodos."'";
		$results = $conn->query($sql);		
		return $results;
}

function SIGESA_BSD_EnviarReceptoresTablaEnvio($IdOrdenFen){
		require('cn/cn_sqlserver_server_locla.php');
	    $sql =  "exec [SIGESA_BSD_EnviarReceptoresTablaEnvio_MHR] '".$IdOrdenFen."'";
		$results = $conn->query($sql);		
		return $results;
}

function BuscarSolicitudesCerradas_M($NroSolicitud){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT --det.IdSolicitudSangre, 
				NroSolicitud, IdReceptor, IdPaciente, NroHistoria, IdCuentaAtencion, IdAtencion, IdGrupoSanguineo, 
				FechaSolicitud, IdTipoServicio, IdServicio, CodigoCama, --UsuReg, FecReg, 
				IdDiagnostico1, IdDiagnostico2, Hemoglobina, Hematocrito, swAnemia1, Tromboplastina, Protrombina, swPlasma1, Plaquetas, swPQ1, swTransPre, 
				swReacTrans, EmbarazoPrevio, Aborto, IncompatibilidadMF, swConsentimiendoInformado, MotivoNOConsentimiendo, 
				MedicoSolicitante, UsuTomoMuestraPaciente, UsuRegResumen, FecRegResumen, --Estado, 
				FechaRecepcion, UsuRecepcion, UsuRegRecepcion, DeteccionCI, CoombsDirecto, DVI, Fenotipo, UsuFenotipo, UsuRegFenotipo, FecRegFenotipo, IdOrdenFen,
				(select Servicios.Nombre from Servicios where Servicios.IdServicio=cab.IdServicio)as Servicio,
				(select grupo.Descripcion from SIGESA_BSD_GrupoSanguineo_MHR grupo where grupo.IdGrupoSanguineo=cab.IdGrupoSanguineo)as GrupoSanguineo,
				(select emp.Usuario from Empleados emp where emp.IdEmpleado=cab.UsuReg)as UsuarioReg, cab.Estado as EstadoCab,
				(select emp.Usuario from Empleados emp where emp.IdEmpleado=det.UsuRegCierre)as UsuarioRegCierre,
				 det.*
		 		 FROM SIGESA_BSD_SolicitudSangreCabecera cab
				 inner join SIGESA_BSD_SolicitudSangreDetalle det on det.IdSolicitudSangre=cab.IdSolicitudSangre
				 WHERE (cab.NroSolicitud='$NroSolicitud') and det.Estado=5 order by cab.NroSolicitud,det.CodigoComponente";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function AbrirDetSolicitud($IdDetSolicitudSangre,$FechaCierre,$UsuRegCierre,$Estado){ 
	require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec [SIGESA_BSD_AbrirDetSolicitud_MHR] '".$IdDetSolicitudSangre."','".$FechaCierre."','".$UsuRegCierre."','".$Estado."'";
		$results = $conn->query($sql);		
		return $results;
}

function BuscarDatosRecepcionEquiposBSD_M($NroDonante,$idProductoCPT){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "  SELECT NroDonante,idProductoCPT,Valor_Examen,EstadoExamen,'Por '+EquipoMedico AS Observacion
				  FROM SIGESA_BANCODESANGRE_Recepcion
				  WHERE NroDonante='$NroDonante' and idProductoCPT='$idProductoCPT'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function BuscarDatosPacientesEquiposBSD_M($ID_Orden,$idProductoCPT){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "  SELECT NroDonante,idProductoCPT,Valor_Examen,EstadoExamen,'Por '+EquipoMedico AS Observacion,ID_Orden
				  FROM SIGESA_BANCODESANGRE_Recepcion
				  WHERE ID_Orden='$ID_Orden' and idProductoCPT='$idProductoCPT'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}

function ObtenerDatosGrupoSanguineo_M($DescripcionBIORAD){ 
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select * from [192.168.10.12].[SIGH_HL7].[dbo].SIGESA_BANCODESANGRE_GrupoSanguineo
				where DescripcionBIORAD='$DescripcionBIORAD'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}		
		return $validar;	
}


?>