<?php
		//Reporte de Liquidaciones	
		function Retornar_Total_Recaudado_M($IdOrden)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select  SUM(total)  as Total
		from FacturacionServicioDespacho
		where idOrden='".$IdOrden."'
		group by idOrden";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		//Lista de Pendientes de Pago	
		function Mostrar_Lista_Pendientes_pago_M($Fecha_Inicial,$Fecha_Final,$Cajeros)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReportePendientesPago '".$Fecha_Inicial."', '".$Fecha_Final."','".$Cajeros."'";   
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}


		//Lista de Cajeros	
		function Mostrar_Lista_Cajeros_M()
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "  select  Empleados.ApellidoPaterno,
					Empleados.ApellidoMaterno,
				    Empleados.Nombres,
					Empleados.IdEmpleado
					from Empleados where IdEmpleado in (
					select   distinct(IdUsuario) from FactOrdenServicio
					where idpuntocarga='1'
					and idTipoFinanciamiento='1' and idFuenteFinanciamiento='1') 
					and IdEmpleado not in (1893,1890,2022)
					order by Empleados.ApellidoPaterno asc";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}




		//Lista de Serie	
		function Mostrar_Lista_Serie_M()
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SigesaCaja]";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}


		//Reporte de Aseguradora	
		function Reporte_Consolidado_por_Aseguradora_M($FechaInicio,$FechaFin,$IdFuenteFinanciamiento,$IdTipoServicio)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaAseguradora '".$FechaInicio."', '".$FechaFin."','".$IdFuenteFinanciamiento."','".$IdTipoServicio."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
		


	//Reporte de Monto Recaudado
		function Reporte_Consolidado_Monto_Recaudado_por_Cajero_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaMontoRecaudadoporCajero '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		



		//Reporte de Liquidaciones	
		function Devolver_Usuario_Extorno_M($IdRegistro)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaUsuarioRealizoExtorno '".$IdRegistro."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
		
		
		//Reporte de Liquidaciones	
		function Reporte_Liquidaciones_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaLiquidaciones '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		

		//Reporte de Liquidaciones	
		function Reporte_Liquidaciones_Economia_M($FechaInicio,$FechaFin,$IdFuenteFinanciamiento,$IdTipoServicio)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaLiquidaciones_Economia '".$FechaInicio."', '".$FechaFin."','".$IdFuenteFinanciamiento."', '".$IdTipoServicio."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}



		//Reporte de Liquidaciones	
		function Reporte_Liquidaciones_Economia_Total_M($FechaInicio,$FechaFin,$IdTipoServicio)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaLiquidaciones_Total_Economia '".$FechaInicio."', '".$FechaFin."', '".$IdTipoServicio."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}


		//Reporte de Liquidaciones	
		function Reporte_Liquidaciones_Hospitalizados_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaLiquidaciones_Hospitalizados '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}




		//Reporte de Liquidaciones	
		function Reporte_Liquidaciones_Hospitalizados_II_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaLiquidacionesHospitalizacionyEmergencia '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}

		//Reporte de Extornos	
		function Reporte_Extornos_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaExtornos '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}


		
		//Reporte de Extornos	
		function Reporte_Devoluciones_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaDevoluciones '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}

		
		//Reporte de Extornos	
		function Reporte_Devoluciones_II_M($FechaInicio,$FechaFin,$NroSerie)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaDevolucionesII '".$FechaInicio."', '".$FechaFin."', '".$NroSerie."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
		
		//Reporte de Consolidado por Serie 
			
		function Reporte_Consolidado_Serie_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaSerie '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
		
		
		//Reporte de Consolidado por Servicio 
			
		function Reporte_Consolidado_por_Servicio_M($FechaInicio,$FechaFin)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaReporteCajaConsolidadoServicio '".$FechaInicio."', '".$FechaFin."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
		//Mostrar  los Datos de la CajaComprobantePago por NroSerie y Nro.Documento
			
		function CajaComprobantesPago_Mostrar_M($NroSerie,$NroDocumento)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  CajaComprobantesPagoSeleccionarPorNroSerieNroDocumento '".$NroSerie."','".$NroDocumento."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}


		//Modificar CajaComprobantesPago para devolver el monto 
			
		function CajaComprobante_Devolver_ComprobanteYOrdenServicio_M($IdComprobantePago,$IdUsuarioAuditoria)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaCajaComprobantesDevolverComprobanteYOrdenServicio '".$IdComprobantePago."','".$IdUsuarioAuditoria."'";		   	 
		//$sql = "exec  CajaDevolucionesAgregar '".$IdComprobantePago."','".$IdUsuarioAuditoria."'";		   	 
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null;
		}



		//Mostrar datos de  CajaComprobantesPago por IdComprobantePago
			
		function CajaComprobantePagoPorIdComprobante_M($IdComprobantePago)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "exec  SigesaCajaComprobantexIdComprobante '".$IdComprobantePago."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}


		//Agregar un registro en la tabla FacturacionServicioDevoluciones
			
		function FacturacionServicioDevolucionAgregar_M($IdComprobantePago,$IdEstadoDevolucion,$IdUsuarioAutoriza)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaAgregarDevoluciones '".$IdComprobantePago."','".$IdEstadoDevolucion."','".$IdUsuarioAutoriza."'";	
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null;
		}



 
		function CajaDevolucionesAgregar_M($idDevolucion,$idComprobantePago,$montoDevuelto,$montoTotal,$fechaDevolucion,$idUsuario,$motivo,$IdUsuarioAuditoria)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec CajaDevolucionesAgregar '".$idDevolucion."','".$idComprobantePago."','".$montoDevuelto."','".
		$montoTotal."','".$fechaDevolucion."','".$idUsuario."','".$motivo."','".$IdUsuarioAuditoria."'";	
				$results = $conn->query($sql);
				$stmt = null;
				$conn = null;
		}
		
		
		function SigesaCajaComprobanteMontoxCajero_M($FechaInicio,$FechaFin,$IdCajero)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaCajaComprobanteMontoxCajero '".$FechaInicio."','".$FechaFin."','".$IdCajero."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
?>