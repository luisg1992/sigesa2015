<?php 
	
	#ingresar especialidades
	function IngresarEspecialidadM($nombreEspecialidad,$piso)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaCARIngresarEspecialidades '" . $nombreEspecialidad . "', " . $piso;
		#echo $sql;exit();
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null; 
		return 'M_1';
	}

	#listar especialidades
	function ListarEspecialidades($piso)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaCARListarEspecialidades " . $piso;		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MostarItemsDeinformeOperacional
	function TablaInformeOperacional($IdEspecialidad,$mes,$anio)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaCARMostrarEspecialidades " . $IdEspecialidad . "," . $mes . "," . $anio;		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#ingresar informe operacional 
	function IngresarInformeOperacional($IdEspecialidad,$diagnostico,$procedimiento,$tiempo,$NumCirugias,$NroMes,$Anio)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaCARIngresarInfoOperacional " . $IdEspecialidad . ", '" . $diagnostico . "', '" . $procedimiento . "', '" . $tiempo . "', " . $NumCirugias . "," . $NroMes . ", " . $Anio;
		//echo $sql;exit();
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null; 
		return 'M_1';
	}

	#INGRESAR OPERACIONES PROGRAMADAS
	function IngresarOperacionesProgramadas($OpeProgramadas,$OpeSuspendidas,$OpeProgRealizadas,$CoberturaProgramada,$Aniadidas,$Reoperados,$TOR,$HorasOferSala,$HorasOperProgra,$HorasOpera,$HorasAnestesicas,$HorasEfectivas,$NroMes,$anio,$Piso)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaCARInsertarOperacionesProgramadas " . $OpeProgramadas . "," . $OpeSuspendidas . "," . $OpeProgRealizadas . ", '" .$CoberturaProgramada . "' ," . $Aniadidas . "," . $Reoperados . "," . $TOR . ", '" . $HorasOferSala . "' , '" . $HorasOperProgra . "', '" . $HorasOpera . "', '" . $HorasAnestesicas . "', '" . $HorasEfectivas . "' , " . $NroMes . ", " . $anio . "," . $Piso;
		#echo $sql;exit();
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null; 
		return 'M_1';
	}

	#INGRESA TIPO ANESTESIA
	function IngresarTipoAnestesia($AgeneralBalanceada, $AgeneralInhalatoria, $AgeneralEndovenosa, $AperiduralContinua, $AperiduralSimple, $Araquidea, $Alocal, $sedacion, $total, $NroMes, $Anio, $Piso, $porc_AgeneralBalanceada, $porc_AgeneralInhalatoria, $porc_AgeneralEndovenosa, $porc_AperdurialContinua, $porc_AperdurialSimple, $porc_Araquidea, $porc_Alocal, $porc_Sedacion)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec sigesaCARInsertarTipoAnestesia " . $AgeneralBalanceada . ", " . $AgeneralInhalatoria . ", " . $AgeneralEndovenosa . ", " . $AperiduralContinua . ", " . $AperiduralSimple . ", " . $Araquidea . ", " . $Alocal . ", " . $sedacion . ", " . $total . ", " . $NroMes . ", " . $Anio . ", " . $Piso . ", '" . $porc_AgeneralBalanceada . "', '" . $porc_AgeneralInhalatoria . "', '" . $porc_AgeneralEndovenosa . "', '" . $porc_AperdurialContinua . "', '" . $porc_AperdurialSimple . "', '" . $porc_Araquidea . "', '" . $porc_Alocal . "', '" . $porc_Sedacion . "'";
		#echo $sql;exit();
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null; 
		return 'M_1';
	}

	#INSERTAR PROCEDIMIENTOS INVASIVOS
	function InsertarProcedimientosInvasivos($totalAppiProgramadas, $totalAppiRealizada, $coberturaAppiProgramada, $totalAppiAñadida, $totalAppiSuspendida, $totalHorasProcedimiento, $totalHorasAnestesia, $totalHorasEfectivas, $NroMes, $Anio, $Piso)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec sigesaCARInsertarAppi " . $totalAppiProgramadas . "," .  $totalAppiRealizada . "," . $coberturaAppiProgramada . "," . $totalAppiAñadida . "," . $totalAppiSuspendida . ",'" . $totalHorasProcedimiento . "','" . $totalHorasAnestesia . "','" . $totalHorasEfectivas . "'," . $NroMes . "," . $Anio . "," . $Piso;
		#echo $sql;exit();
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null; 
		return 'M_1';
	}

	#INSERTAR CONSULTORISO ATENCIONES
	function InsertarConsultorioAtenciones($consultasConsul1, $consultalConsul2, $interconsultas, $total, $NroMes, $Anio, $Piso)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec sigesaCARInsertarAtConsultorioAnest " . $consultasConsul1 . "," . $consultalConsul2 . "," . $interconsultas . "," . $total . "," . $NroMes . "," . $Anio . "," . $Piso;
		#echo $sql;exit();
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null; 
		return 'M_1';
	}

	#INSRTAR COMPLICACIONES Y MUERTES
	function InsertarComplicacionesMuertesM($ComplicacionesAnestesicas,$MuertesSalasOperaciones,$MuertesUrpa,$NroMes,$Anio,$Piso)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaInsertarCARCompliMuertes " . $ComplicacionesAnestesicas . "," . $MuertesSalasOperaciones . "," . $MuertesUrpa .  "," . $NroMes . "," . $Anio . "," . $Piso;
		#echo $sql;exit();
		$results = $conn->query($sql);
		$stmt = null;
		$conn = null; 
		return 'M_1';
	}
 ?>