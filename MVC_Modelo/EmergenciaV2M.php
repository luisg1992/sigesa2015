<?php

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 0);

//AGREGADO POR MAHALI 26-01-2017
/*function SIGESA_Emergencia_ListarPacientesSinAlta_M($fecha_inicio,$fecha_fin,$IdServicio){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Emergencia_ListarPacientesSinAlta_MHR '".$fecha_inicio."','".$fecha_fin."','".$IdServicio."'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;
}*/

function TiposDestinoAtencionEmergencia_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec TiposDestinoAtencionSeleccionarDestinosDeConsultorioEmergencia";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function SubclasificacionDiagnosticosSeleccionarDxHospEgreso_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SubclasificacionDiagnosticosSeleccionarDxHospEgreso";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;
}

function SubclasificacionDiagnosticosSeleccionarDxHospMortalidad_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SubclasificacionDiagnosticosSeleccionarDxHospMortalidad";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function SubclasificacionDiagnosticosSeleccionarDxHospMuerteFetal_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SubclasificacionDiagnosticosSeleccionarDxHospMuerteFetal";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function TiposAltaSeleccionarTodos_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec TiposAltaSeleccionarTodos";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function TiposCondicionAltaSeleccionarTodos_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec TiposCondicionAltaSeleccionarTodos";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function ObtenerCodigoCama_M($IdAtencion){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT TOP 1 IdAtencion, MAX(Secuencia)AS maxsecuencia,Secuencia,AtencionesEstanciaHospitalaria.IdCama,Codigo
				FROM AtencionesEstanciaHospitalaria
				inner join Camas on Camas.IdCama=AtencionesEstanciaHospitalaria.IdCama
				WHERE AtencionesEstanciaHospitalaria.IdAtencion='$IdAtencion'
				GROUP BY IdAtencion,Secuencia,AtencionesEstanciaHospitalaria.IdCama,Codigo
				order by Secuencia DESC";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function ObtenerDatosServicio_M($IdServicio){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdServicio,Nombre,Codigo,IdEspecialidad
				from Servicios
				WHERE IdServicio='$IdServicio'";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function MedicosFiltrar_M($lcFiltro){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = 'exec MedicosFiltrar "'.$lcFiltro.'"';
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function TiposReferenciaSeleccionarTodos_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec TiposReferenciaSeleccionarTodos";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function EstablecimientosFiltrar_M($lcFiltro){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = 'exec EstablecimientosFiltrar "'.$lcFiltro.'"';
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function AtencionesEpisodiosDetalleSeleccionarXpaciente_M($idPaciente){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = 'exec AtencionesEpisodiosDetalleSeleccionarXpaciente '.$idPaciente;
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function AtencionesDiagnosticosSeleccionarPorAtencion_M($IdAtencion,$TipoDiagnostico){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = 'exec AtencionesDiagnosticosSeleccionarPorAtencion "'.$IdAtencion.'","'.$TipoDiagnostico.'"';
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function TiposCondicionRecienNacido_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = 'select IdCondicionRN,Descripcion from TiposCondicionRecienNacido';
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function TiposSexo_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdTipoSexo,Descripcion from TiposSexo";
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function TiposDocIdentidad_M(){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdDocIdentidad,Descripcion from TiposDocIdentidad";	
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function UltimaAtencionesEstanciaHospitalaria_M($IdAtencion){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT top 1 * FROM AtencionesEstanciaHospitalaria WHERE IdAtencion='$IdAtencion' order by Secuencia desc";	
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}

function AtencionesNacimientosSeleccionarPorAtencion_M($IdAtencion){
	  require('cn/cn_sqlserver_server_locla.php');
		$sql = 'exec AtencionesNacimientosSeleccionarPorAtencion "'.$IdAtencion.'"'; 	
		$results = $conn->query($sql);		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;	
}


/*function ObtenerDiferenciaDateTime_M($interval,$fechainicio,$fechafinal){
    require('cn/cn_sqlserver_server_locla.php');
          //select DATEDIFF(day, '2005-12-01 13:59', '2005-12-02 11:00');
    $sql = "select DATEDIFF($interval, '$fechainicio', '$fechafinal') as nro";
    $results = $conn->query($sql);    
    $validar=NULL;
    while ($row = $results->fetch(PDO::FETCH_UNIQUE))
    {
        $validar[] = $row;
    }
    return $validar;  
}*/


//Mostrar  Tipos de Antimicrobianos 
	function Mostrar_Lista_Solicitudes_Antimicrobianos($IdPaciente)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = " select  
				AntimicrobianosEstados.Descripcion as Estado,
				AntimicrobianosCabecera.IdEstadoAntimicrobiano,
				AntimicrobianosCabecera.FechaRegistro,
				AntimicrobianosCabecera.IdPaciente,
				Pacientes.ApellidoPaterno,
				Pacientes.ApellidoMaterno,
				Pacientes.PrimerNombre,
				Pacientes.SegundoNombre,
				Pacientes.NroDocumento,
				Pacientes.NroHistoriaClinica,
				Pacientes.FechaNacimiento,
				AntimicrobianosCabecera.IdCabeceraAntimicrobiano,
				AntimicrobianosCabecera.IdCuentaAtencion
				from AntimicrobianosCabecera
				inner join Pacientes
				on Pacientes.IdPaciente=AntimicrobianosCabecera.IdPaciente
				inner join AntimicrobianosEstados
				on AntimicrobianosEstados.IdEstadoAntimicrobiano=AntimicrobianosCabecera.IdEstadoAntimicrobiano				
				where AntimicrobianosCabecera.IdPaciente='$IdPaciente'
				order by AntimicrobianosCabecera.IdPaciente,
				AntimicrobianosCabecera.IdCabeceraAntimicrobiano desc";	  	 
		$results = $conn->query($sql);

		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

//MANTENIMIENTOS

function AtencionesModificar($HoraIngreso, $FechaIngreso, $IdTipoServicio , $IdPaciente , $IdAtencion , $IdTipoCondicionALEstab , $FechaEgresoAdministrativo , $IdCamaEgreso , $IdCamaIngreso , $IdServicioEgreso , $IdTipoAlta , $IdCondicionAlta , $IdTipoEdad , $IdOrigenAtencion , $IdDestinoAtencion , $HoraEgresoAdministrativo , $IdTipoCondicionAlServicio , $HoraEgreso , $FechaEgreso , $IdMedicoEgreso , $Edad , $IdEspecialidadMedico , $IdMedicoIngreso , $IdServicioIngreso , $IdTipoGravedad , $IdCuentaAtencion , $idFormaPago, $IdUsuarioAuditoria, $idFuenteFinanciamiento, $idEstadoAtencion, $EsPacienteExterno, $idSunasaPacienteHistorico){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec AtencionesModificar '".$HoraIngreso."','".$FechaIngreso."','".$IdTipoServicio."','".$IdPaciente."','".$IdAtencion."','".$IdTipoCondicionALEstab."','".$FechaEgresoAdministrativo."','".$IdCamaEgreso."','".$IdCamaIngreso."','".$IdServicioEgreso."','".$IdTipoAlta."','".$IdCondicionAlta."','".$IdTipoEdad."','".$IdOrigenAtencion."','".$IdDestinoAtencion."','".$HoraEgresoAdministrativo."','".$IdTipoCondicionAlServicio."','".$HoraEgreso."','".$FechaEgreso."','".$IdMedicoEgreso."','".$Edad."','".$IdEspecialidadMedico."','".$IdMedicoIngreso."','".$IdServicioIngreso."','".$IdTipoGravedad."','".$IdCuentaAtencion."','".$idFormaPago."','". $IdUsuarioAuditoria."','".$idFuenteFinanciamiento."','".$idEstadoAtencion."','".$EsPacienteExterno."','".$idSunasaPacienteHistorico."'";
	$results = $conn->query($sql);	
	return $results;
}

function GuardarAuditoriaAgregarV($IdEmpleado,$Accion,$IdRegistro,$Tabla,$idListItem,$nombrePC,$observaciones){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [AuditoriaAgregarV] '".$IdEmpleado."','".$Accion."','".$IdRegistro."','".$Tabla."','".$idListItem."','".$nombrePC."','".$observaciones."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function AltaMedica_AtencionesModificar_M($IdTipoServicio,$IdPaciente, $IdAtencion, $IdCamaEgreso, $IdServicioEgreso , $IdTipoAlta , $IdCondicionAlta, $IdDestinoAtencion, $HoraEgreso, $FechaEgreso , $IdMedicoEgreso, $IdEspecialidadMedico ,$idEstadoAtencion, $lnDiasEstancia){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [AltaMedica_AtencionesModificar_MHR] '".$IdTipoServicio."','".$IdPaciente."','".$IdAtencion."','".$IdCamaEgreso."','".$IdServicioEgreso ."','".$IdTipoAlta ."','".$IdCondicionAlta."','".$IdDestinoAtencion."','".$HoraEgreso."','".$FechaEgreso ."','".$IdMedicoEgreso ."','".$IdEspecialidadMedico."','".$idEstadoAtencion."','".$lnDiasEstancia."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function FacturacionCuentasAtencionModificar($TotalPorPagar, $IdEstado , $TotalPagado , $TotalAsegurado , $TotalExonerado , $HoraCierre , $FechaCierre , $HoraApertura , $FechaApertura , $IdPaciente , $IdCuentaAtencion , $FechaCreacion, $IdUsuarioAuditoria){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FacturacionCuentasAtencionModificar] '".$TotalPorPagar."','".$IdEstado."','".$TotalPagado."','".$TotalAsegurado."','".$TotalExonerado."','". $HoraCierre."','".$FechaCierre."','".$HoraApertura."','".$FechaApertura."','".$IdPaciente."','". $IdCuentaAtencion."','".$FechaCreacion."','".$IdUsuarioAuditoria."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function FacturacionCuentasAtencionModificar_MHR($IdEstado, $IdCuentaAtencion, $IdUsuarioAuditoria){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [AltaMedica_FacturacionCuentasAtencionModificar_MHR] '".$IdEstado."','".$IdCuentaAtencion."','".$IdUsuarioAuditoria."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function CamasLimpiaIdPaciente($lIdPaciente){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [CamasLimpiaIdPaciente] '".$lIdPaciente."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function CamasActualizaIdPaciente($lIdPaciente, $lIdCama){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [CamasActualizaIdPaciente] '".$lIdPaciente."','".$lIdCama."'";		
		$results = $conn->query($sql);	
		return $results;	
}

//Diagnosticos
function AtencionesDiagnosticosEliminarDxSinIngresoXIdAtencion($lIdAtencion){
	 	require('cn/cn_sqlserver_server_locla.php');
		//$sql = "exec [AtencionesDiagnosticosEliminarXIdAtencion] '".$lIdAtencion."'";		
		 $sql = "delete from AtencionesDiagnosticos where IdClasificacionDx<>'2' and IdAtencion = '".$lIdAtencion."'";		 
		$results = $conn->query($sql);	
		return $results;	
}

function AtencionesDiagnosticosAgregar($IdSubclasificacionDx, $IdClasificacionDx, $IdDiagnostico, $IdAtencionDiagnostico, $IdAtencion, $labConfHIS, $grupoHIS, $subGrupoHIS, $IdUsuarioAuditoria){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [AtencionesDiagnosticosAgregar] '".$IdSubclasificacionDx."','".$IdClasificacionDx."','".$IdDiagnostico."','".$IdAtencionDiagnostico."','". $IdAtencion."','".$labConfHIS."','".$grupoHIS."','".$subGrupoHIS."','".$IdUsuarioAuditoria."'";				
		$results = $conn->query($sql);	
		return $results;	
}

//Nacimientos
function AtencionesNacimientosEliminarXIdAtencion($lIdAtencion){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [AtencionesNacimientosEliminarXIdAtencion] '".$lIdAtencion."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function AtencionesNacimientosAgregar($IdAtencion, $IdCondicionRN, $IdTipoSexo, $Peso, $Talla, $EdadSemanas, $FechaNacimiento, $IdNacimiento, $apgar_1 , $apgar_5 , $ClamplajeFecha , $NroOrdenHijoEnParto , $NroOrdenHijo, $idPacienteNacido, $docIdentidad, $IdDocIdentidad , $IdUsuarioAuditoria){
	 	require('cn/cn_sqlserver_server_locla.php');
		echo $sql = "exec [AtencionesNacimientosAgregar] '".$IdAtencion."','".$IdCondicionRN."','".$IdTipoSexo."','".$Peso."','".$Talla."','".$EdadSemanas."','". $FechaNacimiento."','". $IdNacimiento."','".$apgar_1."','".$apgar_5."','".$ClamplajeFecha."','".$NroOrdenHijoEnParto."','".$NroOrdenHijo."','".$idPacienteNacido."','". $docIdentidad."','".$IdDocIdentidad."','".$IdUsuarioAuditoria."'";				
		$results = $conn->query($sql);	
		return $results;	
}

function ObtenerCodigoDeConsultaDeEmergencia_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec ObtenerCodigoDeConsultaDeEmergencia";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}
     
function CajaComprobantesPagoChequeaSiYaPago_M($IdCuentaAtencion, $lnIdServicio){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec CajaComprobantesPagoChequeaSiYaPago'".$IdCuentaAtencion."','".$lnIdServicio."'";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function ParametrosSeleccionarPorId_M($IdParametro){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec ParametrosSeleccionarPorId'".$IdParametro."'";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,$IdClasificacionDx)
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  AltaMedica_AtencionesDiagnosticosSeleccionarTodosPorIdAtencion_MHR '".$IdAtencion."','".$IdClasificacionDx."'";		   	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}

function FacturacionCuentasAtencionSeleccionarPorId_M($IdCuentaAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionCuentasAtencionSeleccionarPorId '".$IdCuentaAtencion."'";		   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}
	
function ListarSisFuaAtencion_M($IdCuentaAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "select * from SIGH_EXTERNA.dbo.SisFuaAtencion WHERE idCuentaAtencion='$IdCuentaAtencion'";		   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

function ListarDatosAtencion_M($IdAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "select idFuenteFinanciamiento,IdFormaPago,IdDestinoAtencion,IdMedicoEgreso,IdTipoAlta,IdCondicionAlta from Atenciones where IdAtencion='$IdAtencion'";		   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

/*function FactCatalogoServiciosXidTipoFinanciamiento_M($idProducto,$IdTipoFinanciamiento){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FactCatalogoServiciosXidTipoFinanciamiento '".$idProducto."','".$IdTipoFinanciamiento."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}*/

function FactCatalogoServiciosSeleccionarPorId_M($IdProducto){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FactCatalogoServiciosSeleccionarPorId '".$IdProducto."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}
	
/*function FacturacionCuentasAtencionxIdCuentaAtencion_M($TodasLasCuentas, $IdCuentaAtencion){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionCuentasAtencionxIdCuentaAtencion '".$TodasLasCuentas."','".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}*/

/*function FacturacionServicioPagosPorCuentaTodos_M($IdCuentaAtencion){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionServicioPagosPorCuentaTodos '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}*/

function FacturacionServicioFinanciamientosConsultarXIdCuentaAtencion_M($IdCuentaAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionServicioFinanciamientosConsultarXIdCuentaAtencion '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

/*function FacturacionCuentasAtencionPtosXIdCuenta_M($IdCuentaAtencion){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionCuentasAtencionPtosXIdCuenta '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}*/

/*function FacturacionBienesPagosSeleccionarPorCuentaTodosOconexion_M($IdCuentaAtencion){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionBienesPagosSeleccionarPorCuentaTodosOconexion '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}*/

function FacturacionfarmMovimientoVentasConsultarXIdCuentaAtencion_M($IdCuentaAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionfarmMovimientoVentasConsultarXIdCuentaAtencion '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

/*function FacturacionCajaComprobantesPagoConsultarXIdCuentaAtencion_M($IdCuentaAtencion){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionCajaComprobantesPagoConsultarXIdCuentaAtencion '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}*/

/*function FacturacionServiciosPagosSeleccionarPorIdComprobantePago_M($IdComprobantePago){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionServiciosPagosSeleccionarPorIdComprobantePago '".$IdComprobantePago."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}*/

function FacturacionServicioFinanciamientosConsultarX_M($IdOrden, $IdProducto){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionServicioFinanciamientosConsultarX@IdOrdenIdProducto '".$IdOrden."','".$IdProducto."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

function FacturacionReembolsosXcuentaAtencion_M($IdCuentaAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionReembolsosXcuentaAtencion '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

function ReembolsoDetalleSeleccionaPorIdComprobantePagoIdCuenta_M($IdComprobantePago , $IdCuentaAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec ReembolsoDetalleSeleccionaPorIdComprobantePagoIdCuenta '".$IdComprobantePago."','".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

function ReembolsoDetalleItemSeleccionarPorIdCuenta_M($IdCuentaAtencion){
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec ReembolsoDetalleItemSeleccionarPorIdCuenta '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE))
	{
		  $validar[] = $row;
	}		
	$stmt = null;
	$conn = null; 
	return $validar;
}

/*function FacturacionCuentasAtencionPtosEliminarXcuenta_M($IdCuentaAtencion){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionCuentasAtencionPtosEliminarXcuenta '".$IdCuentaAtencion."'";	   	 
	$results = $conn->query($sql);	
	return $results;
}*/	

/*function FacturacionCuentasAtencionPtosActualizar_M($lbNueva,$IdCuentaAtencion,$idPuntoCarga,$TotalConsumos, $TotalPagos, $TotalPagosReembolso){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionCuentasAtencionPtosActualizar '".$lbNueva."','".$IdCuentaAtencion."','".$idPuntoCarga."','".$TotalConsumos."','".$TotalPagos."','".$TotalPagosReembolso."'";	   	 
	$results = $conn->query($sql);	
	return $results;
}*/	

/*function FacturacionCuentasAtencionActualizaImportes_M($IdCuentaAtencion,$TotalExonerado, $TotalAsegurado, $TotalPagado, $totalPorPagar){ //esta en FacturacionM
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec FacturacionCuentasAtencionActualizaImportes '".$IdCuentaAtencion."','".$TotalExonerado."','".$TotalAsegurado."','".$TotalPagado."','".$totalPorPagar."'";	   	 
	$results = $conn->query($sql);	
	return $results;
}*/

/////
/*  1602845 */
	function SigesaFactOrdenServicio($IdCuentaAtencion)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "select   * from FactOrdenServicio where IdCuentaAtencion='".$IdCuentaAtencion."' and IdPuntoCarga='2' order by  IdOrden desc"; 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	function SigesaAtencionesDatosAtencion($NroHistoriaClinica)
	{
		require('cn/cn_sqlserver_server_locla.php');	   
		$sql = "select distinct 
				--FactOrdenServicio.
				IdCuentaAtencion,FechaIngreso,HoraIngreso,
				(select Descripcion from TiposServicio ts where ts.IdTipoServicio=Atenciones.IdTipoServicio) as TipoServicio,
				(select Nombre from Servicios ts where ts.IdServicio=Atenciones.IdServicioIngreso) as ServicioIngreso,
				(select Descripcion from FuentesFinanciamiento ff where ff.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento ) as FuentesFinanciamiento,
				Pacientes.*
				from Pacientes
				inner join Atenciones on Atenciones.IdPaciente=Pacientes.IdPaciente	
				WHERE Pacientes.NroHistoriaClinica='$NroHistoriaClinica' 
				order by Atenciones.IdCuentaAtencion desc"; 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	/*function SigesaFactOrdenServicioDatosAtencion($NroHistoriaClinica)
	{
		require('cn/cn_sqlserver_server_locla.php');	   
		$sql = "select distinct 
				FactOrdenServicio.IdCuentaAtencion,FechaIngreso,HoraIngreso,
				(select Descripcion from TiposServicio ts where ts.IdTipoServicio=Atenciones.IdTipoServicio) as TipoServicio,
				(select Nombre from Servicios ts where ts.IdServicio=Atenciones.IdServicioIngreso) as ServicioIngreso,
				(select Descripcion from FuentesFinanciamiento ff where ff.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento ) as FuentesFinanciamiento,
				Pacientes.*
				from LabMovimiento
				left join	LabMovimientoLaboratorio on LabMovimientoLaboratorio.IdMovimiento=LabMovimiento.IdMovimiento
				--left join	LabMovimientoCPT	on LabMovimientoCPT.idMovimiento=LabMovimiento.IdMovimiento
				--left join	FactCatalogoServicios	on FactCatalogoServicios.IdProducto=LabMovimientoCPT.idProductoCPT
				--inner join FactPuntosCarga on LabMovimiento.IdPuntoCarga=FactPuntosCarga.IdPuntoCarga
				inner join FactOrdenServicio on FactOrdenServicio.IdOrden=LabMovimientoLaboratorio.IdOrden
				inner join Pacientes on Pacientes.IdPaciente=FactOrdenServicio.IdPaciente
				inner join Atenciones on Atenciones.IdCuentaAtencion=FactOrdenServicio.IdCuentaAtencion	
				WHERE FactOrdenServicio.IdPuntoCarga='2' and Pacientes.NroHistoriaClinica='$NroHistoriaClinica' order by FactOrdenServicio.IdCuentaAtencion desc"; 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}*/

	function SigesaLaboratorioMostrarOrdenDetalladaxIdOrden_M($IdOrden)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SigesaLaboratorioMostrarDetalleOrden_MHR " . $IdOrden; 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	function SigesaLaboratorioMMostrarOrdenConResultado_M($IdOrden)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SigesaLaboratorioMostrarDetalleOrdenConResultado " . $IdOrden; 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}	

	//Mostrar  Tipos de Antimicrobianos 
	function Mostrar_Lista_Antimicrobianos_Examenes_IdProducto()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdProducto from AntimicrobianosExamenes";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}	

	function AtencionesEpisodiosCabeceraSeleccionarPorId($idPaciente){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec AtencionesEpisodiosCabeceraSeleccionarPorId '".$idPaciente."'";	   	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}
		return $validar;
	}	

	function AtencionesEpisodiosCabeceraAgregar($idPaciente, $idEpisodio, $FechaApertura, $FechaCierre, $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		if(trim($FechaCierre)!=""){
			$sql = "exec AtencionesEpisodiosCabeceraAgregar '".$idPaciente."','".$idEpisodio."','".$FechaApertura."','".$FechaCierre."','".$IdUsuarioAuditoria."'";	 
		}else{
			$sql = "exec AtencionesEpisodiosCabeceraAgregar '".$idPaciente."','".$idEpisodio."','".$FechaApertura."',NULL,'".$IdUsuarioAuditoria."'";	 
		}  	 
		$results = $conn->query($sql);	
		return $results;
	}	

	function AtencionesEpisodiosCabeceraModificar($idPaciente, $idEpisodio, $FechaApertura, $FechaCierre, $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		if(trim($FechaCierre)!=""){
			$sql = "exec AtencionesEpisodiosCabeceraModificar '".$idPaciente."','".$idEpisodio."','".$FechaApertura."','".$FechaCierre."','".$IdUsuarioAuditoria."'";
		}else{
			$sql = "exec AtencionesEpisodiosCabeceraModificar '".$idPaciente."','".$idEpisodio."','".$FechaApertura."',NULL,'".$IdUsuarioAuditoria."'";	 
		} 	   	 
		$results = $conn->query($sql);	
		return $results;
	}

	function AtencionesEpisodiosCabeceraEliminar($idPaciente, $idEpisodio, $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec AtencionesEpisodiosCabeceraEliminar '".$idPaciente."','".$idEpisodio."','".$IdUsuarioAuditoria."'";	   	 
		$results = $conn->query($sql);	
		return $results;
	}

	function AtencionesEpisodiosDetalleAgregar($idPaciente, $idEpisodio, $idAtencion, $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec AtencionesEpisodiosDetalleAgregar '".$idPaciente."','".$idEpisodio."','".$idAtencion."','".$IdUsuarioAuditoria."'";	   	 
		$results = $conn->query($sql);	
		return $results;
	}

	function AtencionesEpisodiosDetalleModificar($idPaciente, $idEpisodio, $idAtencion, $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec AtencionesEpisodiosDetalleModificar '".$idPaciente."','".$idEpisodio."','".$idAtencion."','".$IdUsuarioAuditoria."'";	   	 
		$results = $conn->query($sql);	
		return $results;
	}

	function AtencionesEpisodiosDetalleEliminar($idPaciente, $idEpisodio, $idAtencion, $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec AtencionesEpisodiosDetalleEliminar '".$idPaciente."','".$idEpisodio."','".$idAtencion."','".$IdUsuarioAuditoria."'";	   	 
		$results = $conn->query($sql);	
		return $results;
	}

	function FactOrdenServicioFiltraPorIdCuenta($idCuentaAtencion){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec FactOrdenServicioFiltraPorIdCuenta '".$idCuentaAtencion."'";	   	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}
		return $validar;
	}

	function FacturacionServicioFinanciamientosSeleccionarPorId($idOrden){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec FacturacionServicioFinanciamientosSeleccionarPorId '".$idOrden."'";	   	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}
		return $validar;
	}

	function FacturacionServicioFinanciamientosEliminar($idOrden, $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec FacturacionServicioFinanciamientosEliminar '".$idOrden."','".$IdUsuarioAuditoria."'";	   	 
		$results = $conn->query($sql);	
		return $results;
	}

	function FactOrdenServicioModificar($IdOrden, $IdUsuarioAuditoria, $IdEstadoFacturacion){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update FactOrdenServicio
				set 
				IdEstadoFacturacion='$IdEstadoFacturacion'				
				where IdOrden = '$IdOrden'";	   	 
		$results = $conn->query($sql);	
		return $results;
	}

	function TiposFinanciamientoSeleccionarPorId($IdTipoFinanciamiento){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec TiposFinanciamientoSeleccionarPorId '".$IdTipoFinanciamiento."'";	   	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}
		return $validar;
	}

	function CamasSeleccionarPorId($IdCama){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec CamasSeleccionarPorId '".$IdCama."'";	   	 
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}
		return $validar;
	}

	function CamasModificar($Y,$X,$IdPaciente,$IdServicioUbicacionActual,$Codigo,$IdEstadoCama,$IdCondicionOcupacion,$IdTiposCama,$IdServicioPropietario,$IdCama,$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec CamasModificar '".$Y."','".$X."','".$IdPaciente."','".$IdServicioUbicacionActual."','".$Codigo."','".$IdEstadoCama."','".$IdCondicionOcupacion."','".$IdTiposCama."','".$IdServicioPropietario."','".$IdCama."','".$IdUsuarioAuditoria."'";	     	 
		$results = $conn->query($sql);	
		return $results;
	}

	function CamasMovimientosAgregar($IdMovimiento,$IdCama,$IdServicio,$fechaIngreso,$fechaSalida,$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec CamasMovimientosAgregar '".$IdMovimiento."','".$IdCama."','".$IdServicio."','".$fechaIngreso."','".$fechaSalida."','".$IdUsuarioAuditoria."'";	     	 
		$results = $conn->query($sql);	
		return $results;
	}

	function FactOrdenServicioAgregar2_M(
		 $IdOrden, 
		 $IdPuntoCarga, 
		 $IdPaciente, 
		 $IdCuentaAtencion,  
		 $IdServicioPaciente, 
		 $idTipoFinanciamiento,  
		 $idFuenteFinanciamiento, 
		 $FechaCreacion, 
		 $IdUsuario,  
		 $FechaDespacho,  
		 $IdUsuarioDespacho, 
		 $IdEstadoFacturacion,  
		 $FechaHoraRealizaCpt, 
		 $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
//		  $sql = "exec [FactOrdenServicioAgregar] '',	1,752914,239053,273,2,3,'2015-05-11 16:17',1893,'2015-05-11 16:17',1893,1,'2015-05-11 16:17',181";
		  $sql = "exec [FactOrdenServicioAgregar] '".$IdOrden."','".$IdPuntoCarga."','".$IdPaciente."','".$IdCuentaAtencion."','".$IdServicioPaciente."','".$idTipoFinanciamiento."','".$idFuenteFinanciamiento."','".$FechaCreacion."','".$IdUsuario."','".$FechaDespacho."','".$IdUsuarioDespacho."','".$IdEstadoFacturacion."','".$FechaHoraRealizaCpt."','".$IdUsuarioAuditoria."'";
		  
		    $resultat = $conn->query($sql);
			 
			$sqlid = "SELECT @@identity AS id";
			$query = $conn->query($sqlid);
			while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
				$validar=    trim($row[0]);
			}
			return $validar;	
	}

	function FactOrdenServicioPagosAgregar2_M($idOrdenPago,
											$idComprobantePago,
											$idOrden,
											$FechaCreacion,
											$IdUsuario,
											$IdEstadoFacturacion,
											$ImporteExonerado,
											$idUsuarioExonera,
											$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FactOrdenServicioPagosAgregar] '".$idOrdenPago."',NULL,
'".$idOrden."',
'".$FechaCreacion."',
'".$IdUsuario."',
'".$IdEstadoFacturacion."',
'".$ImporteExonerado."',
'".$idUsuarioExonera."',
'".$IdUsuarioAuditoria."'";
		               
		$resultat = $conn->query($sql);
		
		$sqlid = "SELECT @@identity AS id";
			$query = $conn->query($sqlid);
			while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
				$validar=    trim($row[0]);
			}
			return $validar;

	}

	function FacturacionServiciosPagosSeleccionarPorIdComprobantePago2_M($IdComprobantePago){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionServiciosPagosSeleccionarPorIdComprobantePago] '".$IdComprobantePago."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

/*function ObtenerIdDepaReniec_M($IdReniec){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select IdDistrito,Distritos.IdReniec as IdDiReniec,
				Provincias.IdProvincia,Provincias.IdReniec as IdProReniec,
				Departamentos.IdDepartamento,Departamentos.IdReniec as IdDepReniec
				from Distritos 
				inner join Provincias on Distritos.IdProvincia=Provincias.IdProvincia
				inner join Departamentos on Departamentos.IdDepartamento=Provincias.IdDepartamento
				where Distritos.IdReniec=$IdReniec";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function ListarPaises_M(){		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "SELECT IdPais,Codigo,Nombre FROM Paises order by Nombre";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		return $validar;
}

function CentroPobladoSeleccionarPorDistrito($IdDistrito,$texto){
		require('cn/cn_sqlserver_server_locla.php');				
	  	$sql="exec CentroPobladoSeleccionarPorDistrito '".$IdDistrito."','".$texto."'";		
		$results = $conn->query($sql);	
		
		$validar=NULL;	
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		return $validar;
	}

//

function SIGESA_Emergencia_ListarPacientesSinAlta_M($variable,$fecha_inicio,$fecha_fin,$IdServicio,$tipo_busqueda)
 		{		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Emergencia_ListarPacientesSinAlta_MHR '".$variable."','".$fecha_inicio."','".$fecha_fin."','".$IdServicio."',".$tipo_busqueda."";
		$results = $conn->query($sql);
		
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}
		return $validar;
}
*/
 
		
?>