<?php

   //11-06-2018

	function Mostrar_Datos_de_Paciente($NroHistoriaClinica)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select  ApellidoPaterno,ApellidoMaterno,PrimerNombre,SegundoNombre,
					NroDocumento,NroHistoriaClinica
				 	FROM PACIENTES WHERE NROHISTORIACLINICA='". $NroHistoriaClinica."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	//  07-06-2018
	function Lista_Atenciones_de_Pacientes($NroHistoriaClinica)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select   Atenciones.IdAtencion,Servicios.Nombre,Atenciones.FechaIngreso,
				Atenciones.IdCuentaAtencion,Pacientes.NroDocumento,TiposServicio.Descripcion  from atenciones
				inner join Pacientes
				on  Pacientes.IdPaciente=atenciones.idpaciente
				inner join Servicios
				on Servicios.IdServicio=atenciones.IdServicioIngreso
				inner join TiposServicio
				on TiposServicio.IdTipoServicio=Servicios.IdTipoServicio
				where Pacientes.NroHistoriaClinica='". $NroHistoriaClinica."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	//  07-06-2018
	function Mostrar_Datos_SisFiliaciones_por_Paciente($DocumentoNumero)
	{
		require('cn/cn_sqlserver_server_sigh_externa.php');
		$sql = "select Codigo,CodigoEstablAdscripcion,idSiasis  from sisfiliaciones where  afiliacionnroformato='".$DocumentoNumero."' or documentonumero='".$DocumentoNumero."'";		   	 
		$results = $conn_sigh_externa->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return  $validar;
	}



	//  07-06-2018
	function Modificar_Datos_Adicionales_Atenciones($IdSiaSis,$SisCodigo,$IdEstablecimientoOrigen,$IdAtencion)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "update AtencionesDatosAdicionales set IdSiaSis='".$IdSiaSis."',SisCodigo='".$SisCodigo."',IdEstablecimientoOrigen='".$IdEstablecimientoOrigen."'  where idatencion='".$IdAtencion."'";	   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}


	//16-02-2018   Funcion que Busca si Tiene una Atencion con Fallecimiento
    function FactBuscarSiExisteFallecimientodePaciente_HC_M($Dato){ 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select   *  from atenciones
				inner join Pacientes
				on Pacientes.IdPaciente=atenciones.idpaciente
				where Pacientes.NroHistoriaClinica='".$Dato."'
				and Atenciones.IdCondicionAlta='4'";
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}	
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	
	
	//16-02-2018   Funcion que Busca si Tiene una Atencion con Fallecimiento
    function FactBuscarSiExisteFallecimientodePaciente_DNI_M($Dato){ 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "select   *  from atenciones
				inner join Pacientes
				on Pacientes.IdPaciente=atenciones.idpaciente
				where Pacientes.NroDocumento='".$Dato."'
				and Atenciones.IdCondicionAlta='4'";
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}	
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	
	
	function FactOrdenServicioAgregar_M(
		 $IdOrden, 
		 $IdPuntoCarga, 
		 $IdPaciente, 
		 $IdCuentaAtencion,  
		 $IdServicioPaciente, 
		 $idTipoFinanciamiento,  
		 $idFuenteFinanciamiento, 
		 $FechaCreacion, 
		 $IdUsuario,  
		 $FechaDespacho,  
		 $IdUsuarioDespacho, 
		 $IdEstadoFacturacion,  
		 $FechaHoraRealizaCpt, 
		 $IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
//		  $sql = "exec [FactOrdenServicioAgregar] '',	1,752914,239053,273,2,3,'2015-05-11 16:17',1893,'2015-05-11 16:17',1893,1,'2015-05-11 16:17',181";
		  $sql = "exec [FactOrdenServicioAgregar] '".$IdOrden."','".$IdPuntoCarga."','".$IdPaciente."','".$IdCuentaAtencion."','".$IdServicioPaciente."','".$idTipoFinanciamiento."','".$idFuenteFinanciamiento."','".$FechaCreacion."','".$IdUsuario."','".$FechaDespacho."','".$IdUsuarioDespacho."','".$IdEstadoFacturacion."','".$FechaHoraRealizaCpt."','".$IdUsuarioAuditoria."'";
		  
		 $resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;


	}
	
 
		 
		 	
	function FactOrdenServicioPagosAgregar_M($idOrdenPago,
											$idComprobantePago,
											$idOrden,
											$FechaCreacion,
											$IdUsuario,
											$IdEstadoFacturacion,
											$ImporteExonerado,
											$idUsuarioExonera,
											$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FactOrdenServicioPagosAgregar] '".$idOrdenPago."',NULL,
'".$idOrden."',
'".$FechaCreacion."',
'".$IdUsuario."',
'".$IdEstadoFacturacion."',
'".$ImporteExonerado."',
'".$idUsuarioExonera."',
'".$IdUsuarioAuditoria."'";
		               
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}
	
	
	



	
	function FacturacionServicioDespachoAgregar_M($idOrden,$IdProducto,$Cantidad,$Precio,$Total,$labConfHIS,$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "exec [FacturacionServicioDespachoAgregar]'".$idOrden."','".$IdProducto."','".$Cantidad."','".$Precio."','".$Total."','".$labConfHIS."','','','".$IdUsuarioAuditoria."'";
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}
	
	 


	function FacturacionServicioPagosAgregar_M($idOrdenPago,$idProducto,$Cantidad,$Precio,$Total,$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FacturacionServicioPagosAgregar] '".$idOrdenPago."','".$idProducto."','".$Cantidad."','".$Precio."','".$Total."','".$IdUsuarioAuditoria."'";
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}

 
function FacturacionCuentasAtencionxIdCuentaAtencion_M($TodasLasCuentas,$IdCuentaAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionCuentasAtencionxIdCuentaAtencion] '".$TodasLasCuentas."','".$IdCuentaAtencion."'";
				 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
function SigesFacturacionCuentasAtencionxIdCuentaAtencion_M($TodasLasCuentas,$IdCuentaAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [SigesFacturacionCuentasAtencionxIdCuentaAtencion] '".$TodasLasCuentas."','".$IdCuentaAtencion."'";
				 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
 
	
 
	

	
		
 function EstadosFacturacionSeleccionarTodos_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [EstadosFacturacionSeleccionarTodos] ";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
 
 function FactCatalogoServiciosHospFiltraPorPuntoCargaTipoFinanciamiento_M($idPuntoCarga,$idTipoFinanciamiento,$idFiltroTipo,$Filtro,$TipoServicioOfrecido){
		 
		require('cn/cn_sqlserver_server_locla.php');
		        $sql = "exec [FactCatalogoServiciosHospFiltraPorPuntoCargaTipoFinanciamiento] '".$idPuntoCarga."','".$idTipoFinanciamiento."','".$idFiltroTipo."','".$Filtro."','".$TipoServicioOfrecido."' ";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	

	
	 function SigesFactCatalogoServiciosHospFiltraPorPuntoCargaTipoFinanciamientoxCodigo_M($idPuntoCarga,$idTipoFinanciamiento,$idFiltroTipo,$Filtro,$TipoServicioOfrecido){
		 
		require('cn/cn_sqlserver_server_locla.php');
		        $sql = "exec [SigesFactCatalogoServiciosHospFiltraPorPuntoCargaTipoFinanciamientoxCodigo] '".$idPuntoCarga."','".$idTipoFinanciamiento."','".$idFiltroTipo."','".$Filtro."','".$TipoServicioOfrecido."' ";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	
	
function FactCatalogoServiciosXidTipoFinanciamiento_M($idProducto,$IdTipoFinanciamiento){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FactCatalogoServiciosXidTipoFinanciamiento] '".$idProducto."','".$IdTipoFinanciamiento."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
 function FacturacionCuentasAtencionPtosEliminarXcuenta_M($IdCuentaAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionCuentasAtencionPtosEliminarXcuenta] '".$IdCuentaAtencion."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	
 
 function FacturacionServicioPagosPorCuentaTodos_M($idCuentaAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionServicioPagosPorCuentaTodos] '".$idCuentaAtencion."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

 


 function FacturacionCuentasAtencionPtosXIdCuenta_M($idCuenta){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionCuentasAtencionPtosXIdCuenta] '".$idCuenta."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	



		 	 
	function FacturacionCuentasAtencionPtosActualizar_M($lbNueva,$IdCuentaAtencion,$idPuntoCarga,$TotalConsumos,$TotalPagos,$TotalPagosReembolso){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FacturacionCuentasAtencionPtosActualizar] '".$lbNueva."','".$IdCuentaAtencion."','".$idPuntoCarga."','".$TotalConsumos."','".$TotalPagos."','".$TotalPagosReembolso."'";
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}
	
 
 

function FactOrdenServicioPorFechas_M($FechaInicio,$FechaFin,$idPuntoCarga,$idServicioPaciente){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FactOrdenServicioPorFechas] '".$FechaInicio."','".$FechaFin."','".$idPuntoCarga."','".$idServicioPaciente."'";
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
		}
	
	
 

function FacturacionBienesPagosSeleccionarPorCuentaTodosOconexion_M($Cuenta){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionBienesPagosSeleccionarPorCuentaTodosOconexion] '".$Cuenta."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}

 

function FacturacionCajaComprobantesPagoConsultarXIdCuentaAtencion_M($IdCuentaAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionCajaComprobantesPagoConsultarXIdCuentaAtencion] '".$IdCuentaAtencion."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
 

function FacturacionServiciosPagosSeleccionarPorIdComprobantePago_M($IdComprobantePago){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionCajaComprobantesPagoConsultarXIdCuentaAtencion] '".$IdComprobantePago."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	
 

   function FacturacionServicioFinanciamientosConsultarXIdOrdenIdProducto_M($IdOrden,$IdOrden){
		 
		require('cn/cn_sqlserver_server_locla.php');
		     $sql = "exec [FacturacionServicioFinanciamientosConsultarX@IdOrdenIdProducto] '".$IdComprobantePago."','".$IdProducto."'";
		  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
 

	function FacturacionCuentasAtencionActualizaImportes_M($IdCuentaAtencion,$TotalExonerado,$TotalAsegurado,$TotalPagado,$totalPorPagar){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FacturacionCuentasAtencionActualizaImportes] '".$IdCuentaAtencion."','".$TotalExonerado."','".$TotalAsegurado."','".$TotalPagado."','".$totalPorPagar."'";
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}
	
	

  function SigesFactOrdenServicioFiltrar_M($IdOrden ,$IdCuentaAtencion ,$NroHistoriaClinica, $idPuntoCarga ,$idServicioPaciente){
		 
		require('cn/cn_sqlserver_server_locla.php');
//		     $sql = "exec [SigesFactOrdenServicioFiltrar]  '".$IdOrden."','".$IdCuentaAtencion."','".$NroHistoriaClinica."','".$idPuntoCarga."','".$idServicioPaciente."'";
		
	  	$sql="SELECT     dbo.FactOrdenServicio.IdOrden, 
	           dbo.FactOrdenServicio.FechaCreacion, 
	           dbo.FactOrdenServicio.IdCuentaAtencion, 
	           dbo.Pacientes.NroHistoriaClinica, 
	           dbo.Pacientes.ApellidoPaterno, 
	           dbo.Pacientes.ApellidoMaterno, 
	           dbo.Pacientes.PrimerNombre, 
	           dbo.EstadosFacturacion.Descripcion AS EstadoOrden, 
	           dbo.FactOrdenServicio.IdPuntoCarga, 
	           dbo.FactOrdenServicio.IdEstadoFacturacion,
	           FechaDespacho,
	           dbo.FactOrdenServicio.IdPaciente
	FROM       dbo.FactOrdenServicio 
	           LEFT OUTER JOIN dbo.Pacientes ON dbo.FactOrdenServicio.IdPaciente = dbo.Pacientes.IdPaciente 
	           LEFT OUTER JOIN dbo.EstadosFacturacion ON dbo.FactOrdenServicio.IdEstadoFacturacion = dbo.EstadosFacturacion.IdEstadoFacturacion
	WHERE     
 	            dbo.FactOrdenServicio.IdOrden like '".$IdOrden."'
	           and dbo.FactOrdenServicio.IdCuentaAtencion like '".$IdCuentaAtencion."'
	           and dbo.Pacientes.NroHistoriaClinica like '".$NroHistoriaClinica."'
	ORDER BY dbo.FactOrdenServicio.fechaCreacion desc,
	         dbo.FactOrdenServicio.IdOrden desc";  //dbo.FactOrdenServicio.idPuntoCarga='".$idPuntoCarga."' and
	           
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
 

	function FacturacionServicioFinanciamientosAgregar_M($idOrden ,
$idProducto ,
$IdTipoFinanciamiento ,
$IdFuenteFinanciamiento ,
$CantidadFinanciada ,
$PrecioFinanciado ,
$TotalFinanciado ,
$FechaAutoriza ,
$IdUsuarioAutoriza ,
$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [FacturacionServicioFinanciamientosAgregar] '".$idOrden."' ,
'".$idProducto."' ,
'".$IdTipoFinanciamiento."' ,
'".$IdFuenteFinanciamiento."' ,
'".$CantidadFinanciada."' ,
'".$PrecioFinanciado."' ,
'".$TotalFinanciado."' ,
'".$FechaAutoriza."' ,
'".$IdUsuarioAutoriza."' ,
'".$IdUsuarioAuditoria."'";
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}
	
	
	
	

  function FactOrdenServicioPagosSeleccionarPorIdOrden_M($idOrden){
		 
		require('cn/cn_sqlserver_server_locla.php');
		
	  	$sql="exec FactOrdenServicioPagosSeleccionarPorIdOrden '".$idOrden."'";  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	
	
	function SigesaFactOrdenServicioEliminar_M($idOrden){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SigesaFactOrdenServicioEliminar] '".$idOrden."'";
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}
	
	
	
	function FacturacionServicioDespachoFiltraPorIdOrden_M($idOrden){
		 
		require('cn/cn_sqlserver_server_locla.php');
		
	  	$sql="exec FacturacionServicioDespachoFiltraPorIdOrden '".$idOrden."'";  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
		function ConsumoMedicamentosPorIdCuentaConSeguroYparticular_M($IdCuentaAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		
	    	$sql="exec ConsumoMedicamentosPorIdCuentaConSeguroYparticular '".$IdCuentaAtencion."'";  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}





	
			function Atencion_Edad_IdCuentaAtencion_M($IdCuentaAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		
	  	$sql="select Atenciones.Edad,TiposEdad.Descripcion from Atenciones inner join TiposEdad on TiposEdad.IdTipoEdad=Atenciones.IdTipoEdad
where IdCuentaAtencion='".$IdCuentaAtencion."'";  
		 
		 
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
 
	
		 function Cosnumo_Servicios_Cuentas_M($valor,$pivot){
		 	require('cn/cn_sqlserver_server_locla.php');
		    	$sql="exec SIGESA_CuentasAtencionXDNI_HISCLI_CUNETA '".$valor."',$pivot";  
			$results = $conn->query($sql);
	
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
	 

	function FacturacionCuentasAtencionAbrir_M($IdCuentaAtencion,$IdUsuarioAuditoria){
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "exec [FacturacionCuentasAtencionAbrir] ".$IdCuentaAtencion.",".$IdUsuarioAuditoria."";
		$resultat = $conn->query($sql);
		//return      $conn->insert_id;
		$stmt = null;
		$conn = null; 
		//return  $validar;

	}
	
	#SIGESA 2016 ESTADO DE CUENTA RODOLFO CRISANTO
	
	function SigesaEstadoCuentaBusquedaxNumeroCuenta($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SegesaEstadoCuentaBusquedaxNumCuenta " . $numcuenta; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	function SigesaEstadoCuentaBusquedaxNumeroHistoria($historiaclinica)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SegesaEstadoCuentaBusquedaxNumHistoriaClinica " . $historiaclinica; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
	function SigesaEstadoCuentaBusquedaxNumeroDni($dni)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SegesaEstadoCuentaBusquedaxNumDni '" . $dni."'"; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	

	function SigesaEstadoCuentaServiciosxNumeroCuenta($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec [SigesaEstadoCuentaServiciosListaxCuenta_V2]  '" .$numcuenta. "'"; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	/********************/
		function SigesaEstadoCuentaServiciosConsolidadoxNumeroCuenta($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SigesaEstadoCuentaServiciosConsolidadoListaxCuenta " . $numcuenta; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	 
	function SigesaEstadoCuentaServiConsoliCentroCostoxNumroCuenta($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SigesaEstadoCuentaServiConsolCentroCostoxCuenta " . $numcuenta; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	 function SigesaEstadoCuentaServiConsolCentroCostoxNumroCuenta($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SigesaEstadoCuentaServiFarmConsolCentroCostoxCuenta '" . $numcuenta."'";	 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
 
	
	
	function SigesaEstadoCuentaFarmaciaxNumeroCuenta($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SigesaEstadoCuentaFarmaciaListaxCuenta_V2  '" . $numcuenta."'"; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
	function SigesaEstadoCuentaFarmaciaConsolidadoxNumeroCuenta($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec SigesaEstadoCuentaFarmaciaConsolidadoListaxCuenta " . $numcuenta; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
 

	function SigesaEstadoCuentaUsuarioMostrarImprimir_M($idempleado)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SigesaLaboratorioImpresionUsuario " . $idempleado; 
		#echo $sql; exit();
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
		function SIGESA_DiadnosticosxAtencion_M($IdAtencion)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SIGESA_DiadnosticosxAtencion " . $IdAtencion; 
		#echo $sql; exit();
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
	
			function SIGESA_DatosFUA_IdcuentaAtencion_M($idCuentaAtencion)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SIGESA_DatosFUA_IdcuentaAtencion " . $idCuentaAtencion; 
		#echo $sql; exit();
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}



	function SigesaEstadoCuentaServiciosConsolidadoListaxCuentaMultiples($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	    $sql="exec SigesaEstadoCuentaServiciosConsolidadoListaxCuentaMultiples '".$numcuenta."'"; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	function SigesaEstadoCuentaFarmaciaConsolidadoListaxCuentaMultiples($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	  $sql="exec SigesaEstadoCuentaFarmaciaConsolidadoListaxCuentaMultiples '".$numcuenta."'"; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	function SigesaEstadoCuentaServiConsolCentroCostoxCuentaMultiple($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	  $sql="exec SigesaEstadoCuentaServiConsolCentroCostoxCuentaMultiple '".$numcuenta."'"; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
		
	function SegesaEstadoCuentaBusquedaxNumCuentaMultiples($numcuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	  $sql="exec SegesaEstadoCuentaBusquedaxNumCuentaMultiples '".$numcuenta."'" ; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	function SIGESA_DatosFUA_IdcuentaAtencionMultiples_M($idCuentaAtencion)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SIGESA_DatosFUA_IdcuentaAtencionMultiples '".$idCuentaAtencion."'" ; 
		#echo $sql; exit();
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}
  
  		function SIGESA_DiadnosticosxAtencionMultiples_M($IdAtencion)
	{
		require('cn/cn_sqlserver_server_locla.php');
	    $sql = "exec SIGESA_DiadnosticosxAtencionMultiples '".$IdAtencion."'" ; 
		#echo $sql; exit();
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	//////////////////////////////////////////////////
	
	
	
//--	EXEC [SIGESA_FactCatalogoPaqueteSeleccionarTodos] 1,2
	function FactCatalogoPaqueteSeleccionarTodos($idTipoFinanciamiento,$TipoPaquete)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	  $sql="exec SIGESA_FactCatalogoPaqueteSeleccionarTodos $idTipoFinanciamiento,$TipoPaquete "; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}	
	
	function FacturacionCatalogoPaquetesXidPaquete($idpaquete)
	{
		require('cn/cn_sqlserver_server_locla.php');		
	  	$sql="exec FacturacionCatalogoPaquetesXidPaquete ".$idpaquete; 		 
		$results = $conn->query($sql);		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}			
		$stmt = null;
		$conn = null; 
		return $validar;
	}		
	
	
	function ExoneracionesFacturacionServicioFinanciamientoBuscar($IdOrden,$IdProducto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  FacturacionServicioFinanciamientosConsultarX@IdOrdenIdProducto "."'".$IdOrden."','".$IdProducto."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
		
			
	function ExoneracionesFacturacionBienesFinanciamientoBuscar($MovNumero,$MovTipo,$IdProducto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  facturacionBienesFinanciamientosSoloExonerados "."'".$MovNumero."','".$MovTipo."','".$IdProducto."'";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	
?>
