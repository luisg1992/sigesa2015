<?php

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 0);

function ListarServiciosHospitalizacionM(){		 
	require('cn/cn_sqlserver_server_locla.php');
	  $sql = "select * from Servicios where  IdTipoServicio='3' order by Nombre";
	$results = $conn->query($sql);
	
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
		  $validar[] = $row;
	}
		
	$stmt = null;
	$conn = null; 
	return $validar;	
}

function ListarPacientesHospitalizadosM($IdServicio){		 
	require('cn/cn_sqlserver_server_locla.php');
	$sql = "exec SIGESA_Hospitalizacion_ListarPacientesHospitalizados_MHR '".$IdServicio."'";
	$results = $conn->query($sql);
	
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
		  $validar[] = $row;
	}
		
	$stmt = null;
	$conn = null; 
	return $validar;	
}



function ListarServiciosCamasDisponiblesyOcupadasM(){		 
	require('cn/cn_sqlserver_server_locla.php');
	  $sql = "select Servicios.Nombre,Servicios.IdServicio, sum(case when Camas.idestadocama=1 then  1 else 0 end) as Libres ,
			  sum(case when Camas.idestadocama=3 then  1 else 0 end) as Ocupadas 
			   from Servicios
			  inner join camas 
			  on camas.IdServicioPropietario=Servicios.IdServicio
			  where Servicios.IdTipoServicio='3'
			  group by Servicios.Nombre, Servicios.IdServicio";
	$results = $conn->query($sql);
	
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
		  $validar[] = $row;
	}
		
	$stmt = null;
	$conn = null; 
	return $validar;	
}




function ListarDepartamentosCamasDisponiblesyOcupadasM(){		 
	require('cn/cn_sqlserver_server_locla.php');
	  $sql = "select DepartamentosHospital.Nombre, sum(case when Camas.idestadocama=1 then  1 else 0 end) as Libres ,
			  sum(case when Camas.idestadocama=3 then  1 else 0 end) as Ocupadas 
			   from Servicios
			  inner join camas 
			  on camas.IdServicioPropietario=Servicios.IdServicio
			  inner join Especialidades
			  on Especialidades.IdEspecialidad=Servicios.IdEspecialidad
			  inner join DepartamentosHospital
			  on DepartamentosHospital.IdDepartamento=Especialidades.IdDepartamento
			  where Servicios.IdTipoServicio='3'
			  group by DepartamentosHospital.Nombre";
	$results = $conn->query($sql);
	
	$validar=NULL;
	while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
		  $validar[] = $row;
	}
		
	$stmt = null;
	$conn = null; 
	return $validar;	
}




		
?>