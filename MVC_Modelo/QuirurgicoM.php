<?php

function Buscar_Pacientes_M($NroDocumento,$NroHistoriaClinica,$ApellidoPaterno,$ApellidoMaterno){
		 
		require('cn/cn_sqlserver_server_locla.php');
		echo    $sql = "
select Atenciones.FechaIngreso, Atenciones.HoraIngreso, Pacientes.ApellidoPaterno, Pacientes.ApellidoMaterno, Pacientes.PrimerNombre, Pacientes.SegundoNombre, 
Pacientes.NroHistoriaClinica, Atenciones.Edad, TiposSexo.Descripcion, Empleados.ApellidoPaterno, Empleados.ApellidoMaterno, Empleados.Nombres, 
DATEDIFF (MINUTE, Atenciones.HoraIngreso, Atenciones.HoraEgreso) AS 'TIEMPO OPERAT.', Atenciones.HoraEgreso
from Citas
inner join Atenciones on Atenciones.IdAtencion=Citas.IdAtencion
inner join AtencionesDiagnosticos on AtencionesDiagnosticos.IdAtencion = Atenciones.IdAtencion
inner join Diagnosticos on Diagnosticos.IdDiagnostico = AtencionesDiagnosticos.IdDiagnostico
inner join Medicos on Medicos.IdMedico = Citas.IdMedico
inner join Empleados on	Empleados.IdEmpleado = Medicos.IdEmpleado
inner join Pacientes on Pacientes.IdPaciente = Citas.IdPaciente
inner join Especialidades on Especialidades.IdEspecialidad = Citas.IdEspecialidad
inner join DepartamentosHospital on DepartamentosHospital.IdDepartamento = Especialidades.IdDepartamento
inner join TiposSexo on TiposSexo.IdTipoSexo = Pacientes.IdTipoSexo
WHERE DepartamentosHospital.IdDepartamento='4' AND Pacientes.NroHistoriaClinica like '".$NroHistoriaClinica."'
ORDER BY Atenciones.FechaEgreso desc, Atenciones.HoraEgreso desc
";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	
function Buscar_Pacientes_Agregar_M($NroHistoriaClinica){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "
select Atenciones.FechaIngreso, Atenciones.HoraIngreso, Pacientes.ApellidoPaterno, Pacientes.ApellidoMaterno, Pacientes.PrimerNombre, Pacientes.SegundoNombre, 
Pacientes.NroHistoriaClinica, Atenciones.Edad, TiposSexo.Descripcion, Empleados.ApellidoPaterno as ApellidoPaternoDoctor, Empleados.ApellidoMaterno as ApellidoMaternoDoctor, Empleados.Nombres, 
DATEDIFF (MINUTE, Atenciones.HoraIngreso, Atenciones.HoraEgreso) AS 'TIEMPO OPERAT.', Atenciones.HoraEgreso
from Citas
inner join Atenciones on Atenciones.IdAtencion=Citas.IdAtencion
inner join AtencionesDiagnosticos on AtencionesDiagnosticos.IdAtencion = Atenciones.IdAtencion
inner join Diagnosticos on Diagnosticos.IdDiagnostico = AtencionesDiagnosticos.IdDiagnostico
inner join Medicos on Medicos.IdMedico = Citas.IdMedico
inner join Empleados on	Empleados.IdEmpleado = Medicos.IdEmpleado
inner join Pacientes on Pacientes.IdPaciente = Citas.IdPaciente
inner join Especialidades on Especialidades.IdEspecialidad = Citas.IdEspecialidad
inner join DepartamentosHospital on DepartamentosHospital.IdDepartamento = Especialidades.IdDepartamento
inner join TiposSexo on TiposSexo.IdTipoSexo = Pacientes.IdTipoSexo
WHERE DepartamentosHospital.IdDepartamento='4' AND Pacientes.NroHistoriaClinica = '".$NroHistoriaClinica."'
ORDER BY Atenciones.FechaEgreso desc, Atenciones.HoraEgreso desc
";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
	}
	

	
              
	/******************************************************************************************************************************/
	function DiagnosticosSeleccionarTodos_M()
		{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_DiagnosticosSeleccionarTodos] ";	 	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
	
	function OpcsSeleccionarTodo_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_OpcsSeleccionarTodo] ";	 	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
	function MedicosSeleccionarPorDepartamento_M($IdDepartamento)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_MedicosSeleccionarPorDepartmaento] '".$IdDepartamento."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
			}
			
	function EspecialidadesSeleccionarTodos_M()
	{
		{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_EspecialidadesSeleccionarTodos] ";	 	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
		}
		
	function CentroQuirurgicoBuscarXHistoria_M($NroHistoriaClini)
	{
		
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_CentroQuirurgicoBuscarXHistoria] '".$NroHistoriaClini."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
		
		}
		
	function AsaSeleccionarTodos_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_AsaSeleccionarTodos]";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
	function AnestesiasSeleccionarTodas_M(){
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_AnestesiasSeleccionarTodos]";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
		}
		
		 
		
?>