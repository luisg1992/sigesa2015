<?php
/*function ValidarProgramacionMedicos_M($IdMedico,$Fecha){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select *
			from ProgramacionMedica 
			where IdMedico='$IdMedico' and convert(date,Fecha)=convert(date,'$Fecha') ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
}*/
	
function ListarServiciosProgramacionMedicos_M($IdMedico,$Fecha){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select distinct Servicios.IdServicio,Servicios.IdTipoServicio,Servicios.Nombre,Fecha
				  from ProgramacionMedica pro inner join Servicios on pro.IdServicio=Servicios.IdServicio
				  where IdMedico='$IdMedico' and convert(date,Fecha)=convert(date,'$Fecha') AND Servicios.Nombre  not like 'Pro.%'  ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}	

function ListarServiciosEmergencia_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select * from Servicios where IdTipoServicio='2' and idEstado='1' order by EsObservacionEmergencia,Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ListarServiciosHospitalizacion_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select * from Servicios where IdTipoServicio='3' and idEstado='1' order by Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ListarPacientesProgramacionMedicos_M($IdMedico,$FechaIngreso,$IdServicio){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql =  "select Pacientes.*,Citas.IdProgramacion,Atenciones.IdCuentaAtencion,Atenciones.idFuenteFinanciamiento,
				   (select Nombre from Servicios where Servicios.IdServicio=Citas.IdServicio)as DescripcionServicio
				   from Citas
				   inner join Pacientes on Pacientes.IdPaciente=Citas.IdPaciente
				   inner join Atenciones on Atenciones.IdAtencion=Citas.IdAtencion
				   where IdMedico='$IdMedico' and convert(date,Fecha)=convert(date,'$FechaIngreso') and IdServicio='$IdServicio' 
				   order by ApellidoPaterno,ApellidoMaterno";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ListarPacientesEmergencia_M($FechaIngreso,$IdServicio){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select Pacientes.*,IdCuentaAtencion,idFuenteFinanciamiento,
				(select Nombre from Servicios where Servicios.IdServicio=Atenciones.IdServicioIngreso)as DescripcionServicio
		  		from Atenciones 
				inner join Pacientes on Pacientes.IdPaciente=Atenciones.IdPaciente
				where IdTipoServicio=2 and idEstadoAtencion<>'0' and FechaEgreso is null and IdServicioIngreso='$IdServicio' and convert(date,FechaIngreso)=convert(date,'$FechaIngreso')
				order by ApellidoPaterno,ApellidoMaterno";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ListarPacientesHospitalizacion_M($FechaIngreso,$IdServicio){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select Pacientes.*,IdCuentaAtencion,idFuenteFinanciamiento,
				(select Nombre from Servicios where Servicios.IdServicio=Atenciones.IdServicioIngreso)as DescripcionServicio 
				from Atenciones 
				inner join Pacientes on Pacientes.IdPaciente=Atenciones.IdPaciente
				where IdTipoServicio=3 and idEstadoAtencion<>'0' and FechaEgreso is null and IdServicioIngreso='$IdServicio' and convert(date,FechaIngreso)=convert(date,'$FechaIngreso')
				order by ApellidoPaterno,ApellidoMaterno";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}


function ObtenerDatosPacientes_M($IdPaciente){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select Pacientes.* from Pacientes
			where IdPaciente='$IdPaciente'
			order by ApellidoPaterno,ApellidoMaterno";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ObtenerDatosFuenteFinanciamiento_M($idFuenteFinanciamiento){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "SELECT IdFuenteFinanciamiento,Descripcion,IdTipoFinanciamiento FROM FuentesFinanciamiento
  WHERE IdFuenteFinanciamiento='$idFuenteFinanciamiento' ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ListarRecetaViaAdministracion_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select * from RecetaViaAdministracion";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ListarRecetaDosis_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select idDosis,NumeroDosis from RecetaDosis";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function ListarPuntosCargaServicios_M(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "SELECT * FROM FactPuntosCarga WHERE IdPuntoCarga IN (2,3,11,20,21,22,23)";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}
	
function ListarCatalogoBienesInsumosM($IdTipoFinanciamiento, $Codigo, $Nombre)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Recetas_ListarCatalogoBienesInsumos_MHR '".$IdTipoFinanciamiento."','".$Codigo."','".$Nombre."' ";	
		/*$Filtro='';
		$Order=' order by Nombre';	
		$sql = "exec CatalogoBienesInsumosResumenSeleccionarPorFiltro '".$Filtro."','".$Order."' ";	*/
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
}	

function ListarCatalogoServiciosM($IdTipoFinanciamiento, $Codigo, $Nombre)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Recetas_ListarCatalogoServicios_MHR '".$IdTipoFinanciamiento."','".$Codigo."','".$Nombre."' ";		
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
}	

function ListarCatalogoCatalogoPaquetesM($IdTipoFinanciamiento, $Codigo, $Nombre)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Recetas_CatalogoPaquetes_MHR '".$IdTipoFinanciamiento."','".$Codigo."','".$Nombre."' ";//FactCatalogoPaqueteSeleccionarTodos	
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
}	

function ListarCatalogoPaquetesXpaqueteM($IdFactPaquete)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Recetas_CatalogoPaquetesXpaquete_MHR '".$IdFactPaquete."' ";//FacturacionCatalogoPaquetesXpaquete	
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
} 

function RecetaFiltrar($Filtro)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Recetas_RecetaFiltrar_MHR '".$Filtro."' ";//RecetaFiltrar	
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
} 

function AtencionesSelecionarPorCuentaM($idCuentaAtencion)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec AtencionesSelecionarPorCuenta '".$idCuentaAtencion."' ";//AtencionesSelecionarPorCuenta	
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
}

function ListarRecetaCabeceraSeleccionarPorIdReceta($idReceta)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Recetas_RecetaCabeceraSeleccionarPorIdReceta_MHR '".$idReceta."' ";//RecetaCabeceraSeleccionarPorIdCuentaAtencion	
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
}

function ListarRecetasDevuelveDatosDelDetalle($IdReceta,$IdPuntoCarga)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SIGESA_Recetas_RecetasDevuelveDatosDelDetalle_MHR '".$IdReceta."','".$IdPuntoCarga."' ";//RecetasDevuelveDatosDelDetalle	
		$results = $conn->query($sql);
		$validar = NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}			 
		return $validar;
}

//MANTENIMENTOS
function GuardarRecetaCabeceraM($IdPuntoCarga, $FechaReceta, $idCuentaAtencion, $idServicioReceta, $idEstado, $DocumentoDespacho, $idComprobantePago, $idMedicoReceta, $fechaVigencia){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_Recetas_GuardarRecetaCabecera_MHR] '".$IdPuntoCarga."','".$FechaReceta."','".$idCuentaAtencion."','".$idServicioReceta."','".$idEstado."','".$DocumentoDespacho."','".$idComprobantePago."','".$idMedicoReceta."','".$fechaVigencia."' ";
		$results = $conn->query($sql);
			 
		$sqlid = "SELECT @@identity AS id";
		$query = $conn->query($sqlid);
		while ($row = $query->fetch(PDO::FETCH_UNIQUE)) {
			$validar=    trim($row[0]);
		}
		return $validar;	
}

function GuardarRecetaDetalleM($idReceta, $idItem, $CantidadPedida, $Precio, $Total, $SaldoEnRegistroReceta, $SaldoEnDespachoReceta, $CantidadDespachada, $idDosisRecetada, $idEstadoDetalle, $MotivoAnulacionMedico, $observaciones, $IdViaAdministracion){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_Recetas_GuardarRecetaDetalle_MHR] '".$idReceta."','".$idItem."','".$CantidadPedida."','".$Precio."','".$Total."','".$SaldoEnRegistroReceta."','".$SaldoEnDespachoReceta."','".$CantidadDespachada."','".$idDosisRecetada."','".$idEstadoDetalle."','".$MotivoAnulacionMedico."','".$observaciones."','".$IdViaAdministracion."' ";		
		$results = $conn->query($sql);	
		return $results;	
}

function GuardarAuditoriaAgregarV($IdEmpleado,$Accion,$IdRegistro,$Tabla,$idListItem,$nombrePC,$observaciones){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [AuditoriaAgregarV] '".$IdEmpleado."','".$Accion."','".$IdRegistro."','".$Tabla."','".$idListItem."','".$nombrePC."','".$observaciones."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function EliminarReceta($IdReceta){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_Recetas_EliminarReceta_MHR] '".$IdReceta."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function EliminarDetalleReceta($IdReceta){
	 	require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_Recetas_EliminarDetalleReceta_MHR] '".$IdReceta."'";		
		$results = $conn->query($sql);	
		return $results;	
}

function RecetaCabeceraPoridRecetaM($lnidReceta){
		 
		require('cn/cn_sqlserver_server_locla.php');
		 $sql = "exec [RecetaCabeceraPoridReceta] '".$lnidReceta."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
}

function RecetaDetalleServicioPorIdRecetaM($idReceta,$IdPuntoCarga){
		 
		require('cn/cn_sqlserver_server_locla.php');
		 $sql = "exec [SIGESA_Recetas_RecetaDetalleServicioPorIdReceta_MHR] '".$idReceta."','".$IdPuntoCarga."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
}

function AtencionesDatosAdicionalesSeleccionarPorIdM($idAtencion){
		 
		require('cn/cn_sqlserver_server_locla.php');
		 $sql = "exec [AtencionesDatosAdicionalesSeleccionarPorId] '".$idAtencion."'";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function AtencionesDiagnosticosSeleccionarPorAtencionM($IdAtencion,$TipoDiagnostico){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec [SIGESA_Recetas_AtencionesDiagnosticosSeleccionarPorAtencion_MHR] '".$IdAtencion."','".$TipoDiagnostico."'";//AtencionesDiagnosticosSeleccionarPorAtencion
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
}

function  SisFuaAtencionSeleccionarPorIdM($idCuentaAtencion)
 		{		 
		require('cn/cn_sqlserver_server_sigh_externa.php');
		$sql = "exec SisFuaAtencionSeleccionarPorId '".$idCuentaAtencion."' ";
		$results = $conn_sigh_externa->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn_sigh_externa = null; 
		return $validar;
}

function  SisFiltraPacientesAfiliadosM($filtro)
 		{		 
		require('cn/cn_sqlserver_server_sigh_externa.php');
		$sql = "exec SisFiltraPacientesAfiliados '".$filtro."' ";
		$results = $conn_sigh_externa->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			  $validar[] = $row;
		}		
		$stmt = null;
		$conn_sigh_externa = null; 
		return $validar;
}


		
?>