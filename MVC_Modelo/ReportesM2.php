<?php

		

		//Consulta para  Departamento 
		
		function SigesaProductividadHoraMedica_Atenciones_Departamento_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "SELECT *
				FROM (
					SELECT IdDepartamento,					
					Departamento,
					left(datename(month,Fecha),3) as [Mes], 
					CitasAtendidas as Atenciones 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(Atenciones)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY IdDepartamento ASC	";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		

		//Consulta para  Departamento 
						
		function SigesaProductividadHoraMedica_Departamento_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "SELECT *
				FROM (
					SELECT IdDepartamento,
					Departamento,
					left(datename(month,Fecha),3) as [Mes], 
					TotalHorasPro as HorasProgramadas 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas	
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(HorasProgramadas)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt
				Order BY IdDepartamento ASC	";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
						
		//Consulta de Servicio - Version 2
        
		function SigesaProductividadHoraMedica_Atenciones2_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "	
				SELECT *
				FROM (
					SELECT 
					case when Idespecialidad='31' then '30' 
					when Idespecialidad='30' then '30'
					when Idespecialidad='15' then '39' 
					when Idespecialidad='39' then '39'
					when Idespecialidad='9' then '10' 
					when Idespecialidad='10' then '10'
					when Idespecialidad='25' then '25' 
					when Idespecialidad='14' then '25'
					else  Idespecialidad end IdEspecialidad,					
					case
					when Especialidad='Medicina  Interna I' then '1-Medicina Interna' 
					when Especialidad='Medicina Interna II' then '1-Medicina Interna' 
					when Especialidad='Neurología' then '1-Neurología'
					when Especialidad='Neurología Pediátrica' then '1-Neurología'
					when Especialidad='Ginecología' then '3-Ginecología-Obstetricia'  
					when Especialidad='Obstetricia' then '3-Ginecología-Obstetricia'  
					when Especialidad='Cirugía Oncológica' then '53-Oncología' 
					when Especialidad='Oncología Médica' then '53-Oncología' 
					else cast(IdDepartamento  as varchar(20))+'-'+Especialidad end as [Especialidad],
					left(datename(month,Fecha),3) as [Mes], 
					CitasAtendidas as Atenciones 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(Atenciones)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY Especialidad ASC";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		

		//Consulta de Servicio - Version 2
		function SigesaProductividadHoraMedica_Horas2_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "SELECT *
				FROM (
					SELECT 
					case when Idespecialidad='31' then '30' 
					when Idespecialidad='30' then '30'
					when Idespecialidad='15' then '39' 
					when Idespecialidad='39' then '39'
					when Idespecialidad='9' then '10' 
					when Idespecialidad='10' then '10'
					when Idespecialidad='25' then '25' 
					when Idespecialidad='14' then '25'
					else  Idespecialidad end IdEspecialidad,					
					case
					when Especialidad='Medicina  Interna I' then '1-Medicina Interna' 
					when Especialidad='Medicina Interna II' then '1-Medicina Interna' 
					when Especialidad='Neurología' then '1-Neurología'
					when Especialidad='Neurología Pediátrica' then '1-Neurología'
					when Especialidad='Ginecología' then '3-Ginecología-Obstetricia'  
					when Especialidad='Obstetricia' then '3-Ginecología-Obstetricia'  
					when Especialidad='Cirugía Oncológica' then '53-Oncología' 
					when Especialidad='Oncología Médica' then '53-Oncología' 
					else cast(IdDepartamento  as varchar(20))+'-'+Especialidad end as [Especialidad],
					left(datename(month,Fecha),3) as [Mes], 
					TotalHorasPro as HorasProgramadas 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(HorasProgramadas)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY Especialidad ASC	";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		

		
		


		//Consulta para  Servicio 
		function SigesaProductividadHoraMedica_Atenciones_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "	SELECT *
				FROM (
					SELECT IdDepartamento,					
					case
					when Especialidad='Medicina  Interna I' then 'Medicina Interna' 
					when Especialidad='Medicina Interna II' then 'Medicina Interna' 
					when Especialidad='Neurología Pediátrica' then 'Neurología'
					when Especialidad='Ginecología' then 'Ginecología-Obstetricia'  
					when Especialidad='Obstetricia' then 'Ginecología-Obstetricia'  
					when Especialidad='Cirugía Oncológica' then 'Oncología' 
					when Especialidad='Oncología Médica' then 'Oncología' 
					else Especialidad end as [Especialidad],
					left(datename(month,Fecha),3) as [Mes], 
					CitasAtendidas as Atenciones 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(Atenciones)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY IdDepartamento,Especialidad ASC";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}

		//Consulta para  Servicio 
		function SigesaProductividadHoraMedica_Horas_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "SELECT *
				FROM (
					SELECT IdDepartamento,
					case
					when Especialidad='Medicina  Interna I' then 'Medicina Interna' 
					when Especialidad='Medicina Interna II' then 'Medicina Interna' 
					when Especialidad='Neurología Pediátrica' then 'Neurología'
					when Especialidad='Ginecología' then 'Ginecología-Obstetricia'  
					when Especialidad='Obstetricia' then 'Ginecología-Obstetricia'  
					when Especialidad='Cirugía Oncológica' then 'Oncología' 
					when Especialidad='Oncología Médica' then 'Oncología' 
					else Especialidad end as [Especialidad],
					left(datename(month,Fecha),3) as [Mes], 
					TotalHorasPro as HorasProgramadas 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(HorasProgramadas)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY IdDepartamento,Especialidad ASC	";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		

		
	   //Consulta de Medico  - Version 2
		function SigesaProductividadHoraMedica_Atenciones_Medico2_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "	SELECT *
				FROM (
					SELECT 
					case when Idespecialidad='31' then '30' 
					when Idespecialidad='30' then '30'
					when Idespecialidad='15' then '39' 
					when Idespecialidad='39' then '39'
					when Idespecialidad='9' then '10' 
					when Idespecialidad='10' then '10'
					when Idespecialidad='25' then '25' 
					when Idespecialidad='14' then '25'
					else  Idespecialidad end IdEspecialidad,				
					cast(IdDepartamento  as varchar(20))+'-'+Medico as [Medico],
					left(datename(month,Fecha),3) as [Mes], 
					CitasAtendidas as Atenciones 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(Atenciones)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY IdEspecialidad,Medico ASC";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
		
		//Consulta de Medico  - Version 2
		function SigesaProductividadHoraMedica_Horas_Medico2_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "SELECT *
				FROM (
					SELECT 
					case when Idespecialidad='31' then '30' 
					when Idespecialidad='30' then '30'
					when Idespecialidad='15' then '39' 
					when Idespecialidad='39' then '39'
					when Idespecialidad='9' then '10' 
					when Idespecialidad='10' then '10'
					when Idespecialidad='25' then '25' 
					when Idespecialidad='14' then '25'
					else  Idespecialidad end IdEspecialidad,
					cast(IdDepartamento  as varchar(20))+'-'+Medico as [Medico],
					left(datename(month,Fecha),3) as [Mes], 
					TotalHorasPro as HorasProgramadas 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(HorasProgramadas)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY IdEspecialidad,Medico ASC	";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}

		
		
		
		
		//Consulta de Medico
		function SigesaProductividadHoraMedica_Atenciones_Medico_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "	SELECT *
				FROM (
					SELECT IdDepartamento,					
					Medico,
					left(datename(month,Fecha),3) as [Mes], 
					CitasAtendidas as Atenciones 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(Atenciones)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY IdDepartamento,Medico ASC";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}

		//Consulta de Medico
		function SigesaProductividadHoraMedica_Horas_Medico_M($Anio,$Mes){
		require('cn/cn_sqlserver_server_sigh.php');
		      $sql = "SELECT *
				FROM (
					SELECT IdDepartamento,
					Medico,
					left(datename(month,Fecha),3) as [Mes], 
					TotalHorasPro as HorasProgramadas 
					FROM SIGESA_Citas_Ofertadas_Vendidas_Atendidas		
					where datepart(year,Fecha)='".$Anio."'
					and datepart(month,Fecha)<='".$Mes."'
					and IdEspecialidad not in (134,133,54,128,52,53)
					and CitasAtendidas!=0	
				) as s
				PIVOT
				(
					SUM(HorasProgramadas)
					FOR [Mes] IN (jan, feb, mar, apr, 
					may, jun, jul, aug, sep, oct, nov, dec)
				)AS pvt	
				Order BY IdDepartamento,Medico ASC	";							  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}

		
		
		
		



	function ReporteIngreso_emergencia01_M($FechaInicio,$FechaFin,$Topico){
			
			require('cn/cn_sqlserver_server_sigh.php');
				  $sql = "select top 10 Atenciones.IdAtencion,Atenciones.IdCuentaAtencion,Pacientes.ApellidoPaterno,Pacientes.ApellidoMaterno,Pacientes.PrimerNombre,
		Pacientes.NroHistoriaClinica,Pacientes.FechaNacimiento,convert(varchar(150),Atenciones.FechaIngreso,103) as FechaIngreso,
		atenciones.HoraIngreso,servicios.Nombre,FuentesFinanciamiento.Descripcion,Servicios.IdServicio,Atenciones.IdTipoServicio,Atenciones.IdOrigenAtencion,Atenciones.IdTipoGravedad,TiposSexo.Descripcion,
		TiposGravedadAtencion.Descripcion as TipoGravedad,
			(select top 1 CodigoCIE10  from AtencionesDiagnosticos inner join Diagnosticos on AtencionesDiagnosticos.IdDiagnostico=Diagnosticos.IdDiagnostico where IdAtencion=Atenciones.IdAtencion)as NombeDiagnos
	from Atenciones  	
	inner join Pacientes on Pacientes.IdPaciente=Atenciones.IdPaciente
	inner join AtencionesEmergencia	on AtencionesEmergencia.IdAtencion=Atenciones.IdAtencion
	inner join Servicios on servicios.IdServicio=Atenciones.IdServicioIngreso
	inner join FuentesFinanciamiento on FuentesFinanciamiento.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento
	inner join TiposSexo on Pacientes.IdTipoSexo=TiposSexo.IdTipoSexo
	left join TiposGravedadAtencion on TiposGravedadAtencion.IdTipoGravedad=Atenciones.IdTipoGravedad";
				  
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;
		
			}


//-- CajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas($NroSerie,$NroDocumento,$FechaInicio,$FechaFin)
 
 function CajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas_M($NroSerie,$NroDocumento,$FechaInicio,$FechaFin){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec CajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas '".$NroSerie."','".$NroDocumento."','".$FechaInicio."','".$FechaFin."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
 
 function SigesaCajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas_M($FechaInicio,$FechaFin){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaCajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas '".$FechaInicio."','".$FechaFin."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
 
 
 
  function SigesaCajaComprobantePagONumeroDocumentoPorRangoFechas_M($FechaInicio,$FechaFin){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaCajaComprobantePagONumeroDocumentoPorRangoFechas '".$FechaInicio."','".$FechaFin."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
		
 
		
		  function SigesaFactReportexCentroCosto_M($FechaInicio,$FechaFin){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaFactReportexCentroCosto '".$FechaInicio."','".$FechaFin."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
			  function SigesaFactReportexCentroCosto_Farmaci_M($FechaInicio,$FechaFin){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaFactReportexCentroCosto_Farmacia '".$FechaInicio."','".$FechaFin."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
		
		
		 function SigesaFactReportexCentroCosto_FarmaciaRedondeado_M($FechaInicio,$FechaFin){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaFactReportexCentroCosto_FarmaciaRedondeado '".$FechaInicio."','".$FechaFin."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
 
 
 		
		  function SigesAFactReporteCentrocostroTotales_M($FechaInicio,$FechaFin){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesAFactReporteCentrocostroTotales '".$FechaInicio."','".$FechaFin."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
 

     function SigesAFactReporteCentrocostroTotalesXCajero($FechaInicio,$FechaFin,$IdCajero){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesAFactReporteCentrocostroTotalesXCajero '".$FechaInicio."','".$FechaFin."','".$IdCajero."'";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
		
		 function SigesAFactReporteCentrocostroxFechayUsuario_M($FechaInicio,$FechaFin,$idUsuario){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesAFactReporteCentrocostroxFechayUsuario  '".$FechaInicio."','".$FechaFin."','".$idUsuario."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
// exec [SigesaCajaDevolucionesxFechayUsuario]'2015-08-26 00:00', '2015-08-26 23:59' ,'1890'
 
 
 	  function SigesaCajaDevolucionesxFechayUsuario_M($FechaInicio,$FechaFin,$idUsuario){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaCajaDevolucionesxFechayUsuario '".$FechaInicio."','".$FechaFin."','".$idUsuario."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
 	  function SigesaCajaxFechayCentrodeCosto_M($FechaInicio,$FechaFin,$idUsuario){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaCajaxFechayCentrodeCosto '".$FechaInicio."','".$FechaFin."','".$idUsuario."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
	 
		
		 	  function SigesaCajaxFechayCatalogodeServicios_M($FechaInicio,$FechaFin,$IdCentroCosto){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaCajaxFechayCatalogodeServicios '".$FechaInicio."','".$FechaFin."','".$IdCentroCosto."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
		 function SigesaCajaComprobantePagoBoletaxFecahcxCajero_M($FechaInicio,$FechaFin,$IdCajero){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaCajaComprobantePagoBoletaxFecahcxCajero '".$FechaInicio."','".$FechaFin."','".$IdCajero."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
		
		
 
	 function sigesaCajaDevolucionesPorFechasUsuario_M($FechaInicio,$FechaFin,$IdCajero){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec sigesaCajaDevolucionesPorFechasUsuario '".$FechaInicio."','".$FechaFin."','".$IdCajero."' ";
			  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}	 
 
 		 
	function BuscarPacientesxNroHistoriaClinica_M($NroHistoriaClinica){
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "select
				Atenciones.FechaIngreso AS FECHA,  
				DepartamentosHospital.Nombre AS DEPARTAMENTO,
				Especialidades.Nombre AS ESPECIALIDAD,
				TiposServicio.Descripcion AS TIPOSERVICIO,
				Servicios.Nombre AS CONSULTORIO,
				Pacientes.ApellidoPaterno+' '+pacientes.ApellidoMaterno+' '+pacientes.PrimerNombre AS DATOSPACIENTES,
				FuentesFinanciamiento.Descripcion AS TIPO,
				TiposServicio.Descripcion AS TIPOSERVICIO,
				Empleados.ApellidoPaterno+' '+Empleados.ApellidoMaterno+' '+Empleados.Nombres AS DATOSMEDICO,
				/** EstadosAtencion.Descripcion AS ESTADOATENCION, **/
				Diagnosticos.Descripcion as NOMBREDX,
				Diagnosticos.CodigoCIE10 AS DIAGCIEX

				From	Pacientes	inner join Atenciones				on Atenciones.IdPaciente=pacientes.IdPaciente
							inner join Servicios				on Servicios.IdServicio=atenciones.IdServicioIngreso
							inner join Especialidades			on Servicios.Idespecialidad=Especialidades.IdEspecialidad
							inner join DepartamentosHospital	on Especialidades.IdDepartamento=DepartamentosHospital.IdDepartamento
							inner join TiposServicio            on TiposServicio.IdTipoServicio=Atenciones.IdTipoServicio
							inner join Medicos					on Medicos.IdMedico=Atenciones.IdMedicoIngreso
							inner join Empleados				on Empleados.IdEmpleado=Medicos.IdEmpleado
							inner join EstadosAtencion			on EstadosAtencion.IdEstadoAtencion=Atenciones.idEstadoAtencion
							inner join FuentesFinanciamiento	on FuentesFinanciamiento.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento 
							LEFT join AtencionesDiagnosticos	on AtencionesDiagnosticos.IdAtencion=Atenciones.IdAtencion
							LEFT join Diagnosticos				on Diagnosticos.IdDiagnostico=AtencionesDiagnosticos.IdDiagnostico
				where NroHistoriaClinica='" . $NroHistoriaClinica . "' order by fecha    desc";

		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $data[] = $row;
		}

		$stmt = null;
		$conn = null; 
		return $data;
	}

	function BuscarPacientesxNroDni_M($NroDni){
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "select
				Atenciones.FechaIngreso AS FECHA,  
				DepartamentosHospital.Nombre AS DEPARTAMENTO,
				Especialidades.Nombre AS ESPECIALIDAD,
				TiposServicio.Descripcion AS TIPOSERVICIO,
				Servicios.Nombre AS CONSULTORIO,
				Pacientes.ApellidoPaterno+' '+pacientes.ApellidoMaterno+' '+pacientes.PrimerNombre AS DATOSPACIENTES,
				FuentesFinanciamiento.Descripcion AS TIPO,
				TiposServicio.Descripcion AS TIPOSERVICIO,
				Empleados.ApellidoPaterno+' '+Empleados.ApellidoMaterno+' '+Empleados.Nombres AS DATOSMEDICO,
				/** EstadosAtencion.Descripcion AS ESTADOATENCION, **/
				Diagnosticos.Descripcion as NOMBREDX,
				Diagnosticos.CodigoCIE10 AS DIAGCIEX

				From	Pacientes	inner join Atenciones				on Atenciones.IdPaciente=pacientes.IdPaciente
							inner join Servicios				on Servicios.IdServicio=atenciones.IdServicioIngreso
							inner join Especialidades			on Servicios.Idespecialidad=Especialidades.IdEspecialidad
							inner join DepartamentosHospital	on Especialidades.IdDepartamento=DepartamentosHospital.IdDepartamento
							inner join TiposServicio            on TiposServicio.IdTipoServicio=Atenciones.IdTipoServicio
							inner join Medicos					on Medicos.IdMedico=Atenciones.IdMedicoIngreso
							inner join Empleados				on Empleados.IdEmpleado=Medicos.IdEmpleado
							inner join EstadosAtencion			on EstadosAtencion.IdEstadoAtencion=Atenciones.idEstadoAtencion
							inner join FuentesFinanciamiento	on FuentesFinanciamiento.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento 
							LEFT join AtencionesDiagnosticos	on AtencionesDiagnosticos.IdAtencion=Atenciones.IdAtencion
							LEFT join Diagnosticos				on Diagnosticos.IdDiagnostico=AtencionesDiagnosticos.IdDiagnostico
				where pacientes.NroDocumento='" . $NroDni . "' order by fecha    desc";
		
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $data[] = $row;
		}

		$stmt = null;
		$conn = null; 
		return $data;
	}
 	
	function RetornarIdMedicoxNombre($IdMedico){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "select Empleados.ApellidoPaterno+' '+Empleados.ApellidoMaterno+' '+Empleados.Nombres AS DATOSMEDICO
				from Empleados
				inner join Medicos on Medicos.IdEmpleado=Empleados.IdEmpleado
				where Medicos.IdMedico = '" . $IdMedico . "'";
		//echo($sql);
		//exit();	  
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}

	function CitadosAtendidosxConsultorios($nuevafecha,$IdMedico)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "SELECT 
				Citas.HoraInicio AS HORAINICIO,
				Citas.HoraFin AS HORAFIN,
				Citas.Fecha AS FECHA,
				pacientes.ApellidoPaterno+' '+pacientes.ApellidoMaterno+' '+pacientes.PrimerNombre AS DATOSPACIENTES,
				FuentesFinanciamiento.Descripcion AS TIPO ,
				Servicios.Nombre AS CONSULTORIO,
				Especialidades.Nombre AS ESPECIALIDAD,
				Empleados.ApellidoPaterno+' '+Empleados.ApellidoMaterno+' '+Empleados.Nombres AS DATOSMEDICO,
				Diagnosticos.Descripcion as NOMBREDX,
				Diagnosticos.CodigoCIE10 AS DIAGCIEX,
				Pacientes.NroHistoriaClinica AS NHIS,
				Pacientes.NroDocumento AS NDOC
				FROM Citas 
				inner join Pacientes				on Pacientes.IdPaciente=Citas.IdPaciente
				inner join Servicios				on Servicios.IdServicio=Citas.IdServicio
				inner join Especialidades			on Servicios.Idespecialidad=Especialidades.IdEspecialidad
				inner join DepartamentosHospital	on Especialidades.IdDepartamento=DepartamentosHospital.IdDepartamento
				inner join Medicos					on Medicos.IdMedico=Citas.IdMedico
				inner join Empleados				on Empleados.IdEmpleado=Medicos.IdEmpleado
				inner join Atenciones				on Citas.IdAtencion=Atenciones.IdAtencion
				inner join FuentesFinanciamiento	on FuentesFinanciamiento.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento
				left join AtencionesDiagnosticos	on AtencionesDiagnosticos.IdAtencion=Atenciones.IdAtencion
				left join Diagnosticos				on Diagnosticos.IdDiagnostico=AtencionesDiagnosticos.IdDiagnostico
				 
			WHERE Citas.IdMedico='" . $IdMedico . "'";
		
		$sql .= "AND Citas.Fecha = '" . $nuevafecha . "'";		
		$sql .= " ORDER BY Citas.Fecha DESC";
		//echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $data[] = $row;
		}

		$stmt = null;
		$conn = null; 
		return $data;
	}	

	function AtendidosxConsultorios($nuevafecha,$IdMedico)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "SELECT 
				Citas.HoraInicio AS HORAINICIO,
				Citas.HoraFin AS HORAFIN,
				Citas.Fecha AS FECHA,
				pacientes.ApellidoPaterno+' '+pacientes.ApellidoMaterno+' '+pacientes.PrimerNombre AS DATOSPACIENTES,
				FuentesFinanciamiento.Descripcion AS TIPO ,
				Servicios.Nombre AS CONSULTORIO,
				Especialidades.Nombre AS ESPECIALIDAD,
				Empleados.ApellidoPaterno+' '+Empleados.ApellidoMaterno+' '+Empleados.Nombres AS DATOSMEDICO,
				Diagnosticos.Descripcion as NOMBREDX,
				Diagnosticos.CodigoCIE10 AS DIAGCIEX,
				Pacientes.NroHistoriaClinica AS NHIS,
				Pacientes.NroDocumento AS NDOC
				FROM Citas 
				inner join Pacientes				on Pacientes.IdPaciente=Citas.IdPaciente
				inner join Servicios				on Servicios.IdServicio=Citas.IdServicio
				inner join Especialidades			on Servicios.Idespecialidad=Especialidades.IdEspecialidad
				inner join DepartamentosHospital	on Especialidades.IdDepartamento=DepartamentosHospital.IdDepartamento
				inner join Medicos					on Medicos.IdMedico=Citas.IdMedico
				inner join Empleados				on Empleados.IdEmpleado=Medicos.IdEmpleado
				inner join Atenciones				on Citas.IdAtencion=Atenciones.IdAtencion
				inner join FuentesFinanciamiento	on FuentesFinanciamiento.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento
				left join AtencionesDiagnosticos	on AtencionesDiagnosticos.IdAtencion=Atenciones.IdAtencion
				left join Diagnosticos				on Diagnosticos.IdDiagnostico=AtencionesDiagnosticos.IdDiagnostico
				 
			WHERE Citas.IdMedico='" . $IdMedico . "'";
		
		$sql .= "AND Citas.Fecha = '" . $nuevafecha . "'";	
		$sql .= "AND Diagnosticos.Descripcion != 'NULL'";	
		$sql .= " ORDER BY Citas.Fecha DESC";
		//echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $data[] = $row;
		}

		$stmt = null;
		$conn = null; 
		return $data;
	}

		function ListarTiposServicios_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  Sigesa_RP_TiposServicio ";
		#ECHO $sql;
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	function ListarFuentesFinanciamiento_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  FuentesFinanciamientoSeleccionarTodos ";
		#ECHO $sql;
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	////////////////////////////////

	function ListarHospitalizados_M($TiposServico,$FuenteFin,$FechaInicio,$FechaFin,$EstadosCuenta)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaReportesListarHospitalizados '".$TiposServico."', '".$FuenteFin."','".$FechaInicio."','".$FechaFin."',".$EstadosCuenta;
		#ECHO $sql;
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
///AGREGADO POR LUIS CRUZADO  17-07-2017
     function SigesaNotasCreditoporRangoFechas_LCO_M($FechaInicio,$FechaFin,$IdCajero){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaNotasCreditoporRangoFechas_LCO '".$FechaInicio."','".$FechaFin."','".$IdCajero."'";
			  
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
     function SigesaNotasCreditoporRangoFechasConsolidado_LCO_M($FechaInicio,$FechaFin,$IdCajero){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaNotasCreditoporRangoFechasConsolidado_LCO '".$FechaInicio."','".$FechaFin."','".$IdCajero."'";
			  
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}
     function SigesaNotasCreditoObtenerDevolucion_LCO_M($IdComprobantePago){
		
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaNotasCreditoObtenerDevolucion_LCO ".$IdComprobantePago;
			  
		$results = $conn->query($sql);
		$validar=NULL;
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;
	
		}		
// FIN LUIS CRUZADO	
?>