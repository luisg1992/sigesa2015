<?php

  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 0);
 
  function ListarDepartamentosHospitalM(){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select IdDepartamento,Nombre from DepartamentosHospital order by Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	function ListarParametrosHospitalM($Codigo){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select * from Parametros
					where Tipo='DATOS_GENERALES' and Codigo='$Codigo'
					order by Tipo";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	
   function ListarEspecialidadM(){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select IdEspecialidad,Nombre,IdDepartamento from Especialidades 
					where Nombre  not like 'Proce%' 
					AND IdEspecialidad not in(93,104,58,62, 222,221,94,112,59,120,115,72, 18,118,117,33, 114,2,230, 74, 125,71,105, 60,106)
					order by Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	function ListarServiciosEspecialidadM(){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select IdServicio,Servicios.Nombre,Servicios.IdEspecialidad,IdTipoServicio 
					from Servicios inner join Especialidades on Especialidades.IdEspecialidad=Servicios.IdEspecialidad 
					where Especialidades.Nombre  not like 'Proce%' 
					AND Especialidades.IdEspecialidad not in(93,104,58,62, 222,221,94,112,59,120,115,72, 18,118,117,33, 114,2,230, 74, 125,71,105, 60,106)
					AND Servicios.Nombre  not like 'Pro.%'  
					AND Servicios.IdServicio not in(782) AND IdTipoServicio='1'
					order by Nombre  ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
 
  function ListarProgramacionHoraMedico($fecha_inicio, $fecha_fin, $IdDepartamentoChk, $IdEspecialidadChk, $IdServicioChk){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "Select  
 DepartamentosHospital.Nombre as Departamento, 
 Especialidades.Nombre as Especialidad, 
 Servicios.Nombre as Servicios,
 convert(varchar,ProgramacionMedica.Fecha,103) as Fecha, 
 Empleados.ApellidoPaterno+' '+Empleados.ApellidoMaterno+' '+Empleados.Nombres as Medico,
 Empleados.DNI, 
 TiposEmpleado.Descripcion as TiposEmpleado, 
 Turnos.Descripcion as Turnos,
 RefConTurnosUPS.Turno as TipoTurno,
(DATEDIFF(HOUR,Turnos.HoraInicio,Turnos.HoraFin)) AS TotalHorasPro
from ProgramacionMedica                          
             inner join DepartamentosHospital on DepartamentosHospital.IdDepartamento=ProgramacionMedica.IdDepartamento
             inner join Especialidades on Especialidades.IdEspecialidad=ProgramacionMedica.IdEspecialidad
             inner join Servicios on Servicios.IdServicio =ProgramacionMedica.IdServicio         
             inner join Medicos on ProgramacionMedica.IdMedico=Medicos.IdMedico
             inner join Empleados on Empleados.IdEmpleado=Medicos.IdEmpleado
             inner join TiposEmpleado on Empleados.IdTipoEmpleado=TiposEmpleado.IdTipoEmpleado
             inner join Turnos on ProgramacionMedica.IdTurno=Turnos.IdTurno
			 inner join TiposCondicionTrabajo on TiposCondicionTrabajo.IdCondicionTrabajo=Empleados.IdCondicionTrabajo
			 INNER JOIN RefConTurnosUPS on RefConTurnosUPS.IdTurno=Turnos.IdTipoTurnoRef			 
WHERE 
ProgramacionMedica.Fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
AND ProgramacionMedica.IdDepartamento IN ($IdDepartamentoChk)
AND ProgramacionMedica.IdEspecialidad IN ($IdEspecialidadChk)
AND ProgramacionMedica.IdServicio IN ($IdServicioChk)
order by 
DepartamentosHospital.Nombre ,Especialidades.Nombre ,Servicios.Nombre ,ProgramacionMedica.Fecha";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
 
 function ListarAtencionesAccidentesTransito($fecha_inicio, $fecha_fin, $IdServicio1,$IdServicio2,$IdFuenteFinanciamiento){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select  COUNT(*) as cantidad from Atenciones 
					inner join AtencionesEmergencia on Atenciones.IdAtencion=AtencionesEmergencia.IdAtencion
					inner join FuentesFinanciamiento on FuentesFinanciamiento.IdFuenteFinanciamiento=Atenciones.idFuenteFinanciamiento
					inner join AtencionesEstanciaHospitalaria on AtencionesEstanciaHospitalaria.IdAtencion=Atenciones.IdAtencion
					inner join Servicios on Servicios.IdServicio=AtencionesEstanciaHospitalaria.IdServicio 
					where AtencionesEstanciaHospitalaria.FechaOcupacion between '$fecha_inicio' and '$fecha_fin' 
					and Servicios.IdServicio in ($IdServicio1,$IdServicio2) 
					AND (FuentesFinanciamiento.IdFuenteFinanciamiento=$IdFuenteFinanciamiento or $IdFuenteFinanciamiento='0')";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
 
 function ListarFuentesFinanciamientoM(){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select IdFuenteFinanciamiento,Descripcion 
				  from FuentesFinanciamiento ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
 
 function ListarServiciosHospitalizacionM(){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select * from Servicios where  IdTipoServicio='3' order by Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
 function ListarEspecialidadesHospitalizacionM(){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "select Especialidades.IdEspecialidad, Especialidades.Nombre from Servicios 
					inner join Especialidades on Servicios.IdEspecialidad=Especialidades.IdEspecialidad
					where  IdTipoServicio='3' 
					group by Especialidades.IdEspecialidad, Especialidades.Nombre
					order by Especialidades.Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	function ListarServiciosM(){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select * from Servicios order by Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	function ListarCentroCostosM(){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select * from CentrosCosto ORDER BY Descripcion";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	function ListarCatalogoServiciosM(){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select IdProducto,Codigo,Nombre,IdCentroCosto from FactCatalogoServicios ORDER BY Nombre";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	///////////////////////PROCEDURE

 function ListarRecursosSaludM($Anio, $Mes, $CodIpress, $CodUgiPres, $NroConsFisFunc, $TotAxu, $TotOtrosProf, $TotAmbOper) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Recursos_Salud '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."','".$NroConsFisFunc."','".$TotAxu."','".$TotOtrosProf."','".$TotAmbOper."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsolidadoGeneralM($Anio, $Mes, $CodIpress, $CodUgiPres) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}	
	
	function ListarConsAsistAmbulatoriaAtMedicasM($Anio, $Mes, $IdTipoEdad, $IdTipoSexo,$edad1,$edad2) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtMedicas '".$Anio."','".$Mes."','".$IdTipoEdad."','".$IdTipoSexo."','".$edad1."','".$edad2."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsAsistAmbulatoriaAtMedicasBebesM($Anio, $Mes, $IdTipoSexo) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtMedicasBebes '".$Anio."','".$Mes."','".$IdTipoSexo."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}		
	
	function ListarConsAsistAmbulatoriaAtNOMedicasM($Anio, $Mes, $IdTipoEdad, $IdTipoSexo,$edad1,$edad2) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtNOMedicas '".$Anio."','".$Mes."','".$IdTipoEdad."','".$IdTipoSexo."','".$edad1."','".$edad2."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsAsistAmbulatoriaAtNOMedicasBebesM($Anio, $Mes, $IdTipoSexo) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtNOMedicasBebes '".$Anio."','".$Mes."','".$IdTipoSexo."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsAsistAmbulatoriaAtendidosM($Anio, $Mes, $IdTipoEdad, $IdTipoSexo,$edad1,$edad2) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_Atendidos '".$Anio."','".$Mes."','".$IdTipoEdad."','".$IdTipoSexo."','".$edad1."','".$edad2."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsAsistAmbulatoriaAtendidosBebesM($Anio, $Mes, $IdTipoSexo) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtendidosBebes '".$Anio."','".$Mes."','".$IdTipoSexo."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function Registrartmp_Consolidado_MorbilidadCita_M($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec usp_Susalud_Reporte_Registrartmp_Consolidado_MorbilidadCita '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."',".$IdEmpleado.",'".$ip_pc."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 
			
	}	
	
	function ListarConsolidado_MorbilidadCitaM($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_MorbilidadCita '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."','".$IdEmpleado."','".$ip_pc."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsolidado_AtEmergenciaM($Anio, $Mes, $IdTipoEdad, $IdTipoSexo,$edad1,$edad2) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtEmergencia '".$Anio."','".$Mes."','".$IdTipoEdad."','".$IdTipoSexo."','".$edad1."','".$edad2."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsolidado_AtEmergenciaAtendidosM($Anio, $Mes, $IdTipoEdad, $IdTipoSexo,$edad1,$edad2) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtEmergenciaAtendidos '".$Anio."','".$Mes."','".$IdTipoEdad."','".$IdTipoSexo."','".$edad1."','".$edad2."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	
	function ListarConsolidado_AtEmergenciaBebesM($Anio, $Mes, $IdTipoSexo) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtEmergenciaBebes '".$Anio."','".$Mes."','".$IdTipoSexo."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsolidado_AtEmergenciaAtendidosBebesM($Anio, $Mes, $IdTipoSexo) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_AtEmergenciaAtendidosBebes '".$Anio."','".$Mes."','".$IdTipoSexo."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function Registrartmp_Consolidado_MorbilidadEmergenciaM($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec usp_Susalud_Reporte_Registrartmp_Consolidado_MorbilidadEmergencia '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."',".$IdEmpleado.",'".$ip_pc."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 
			
	}	
	
	function ListarConsolidado_MorbilidadEmergenciaM($Anio, $Mes, $CodIpress, $CodUgiPres,$IdEmpleado,$ip_pc) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_MorbilidadEmergencia '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."','".$IdEmpleado."','".$ip_pc."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
		
	function ListarConsolidado_HospitalizacionM($Anio, $Mes, $CodIpress, $CodUgiPres) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_Hospitalizacion '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsolidado_MorbilidadHospitalizacionM($Anio, $Mes, $CodIpress, $CodUgiPres) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_MorbilidadHospitalizacion '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsolidado_MorbilidadHospitalizacionTotalesM($Anio, $Mes, $IdTipoEdad, $IdTipoSexo, $Cie) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_MorbilidadHospitalizacionTotales '".$Anio."','".$Mes."',".$IdTipoEdad.",".$IdTipoSexo.",'".$Cie."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ElimTemp_IngresosEgresosEmergenciaM(){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec usp_Susalud_ElimTemp_IngresosEgresosEmergencia";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 
			
	}
	
	function RegTemp_IngresosEgresosEmergenciaM($Anio, $Mes){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec usp_Susalud_RegTemp_IngresosEgresosEmergencia '".$Anio."','".$Mes."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 
			
	}
	
	function ListarIngresosEgresosEmergenciaM($Anio, $Mes) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_IngresosEgresosEmergencia '".$Anio."','".$Mes."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsolidado_ProcedimientosM($Anio, $Mes, $CodIpress, $CodUgiPres) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Consolidado_Procedimientos '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}	
	
	function ListarDiasHospitalizadosOcuM($Anio, $Mes, $CodIpress, $CodUgiPres, $IdServicio) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarDiasHospitalizadosOcu '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."','".$IdServicio."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarDiasHospitalizadosDesocuM($Anio, $Mes, $CodIpress, $CodUgiPres, $IdServicio) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarDiasHospitalizadosDesocu '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."','".$IdServicio."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar;  
	}	
	
	function ListarIngresosHospitalizadosM($Fecha, $IdServicio){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarIngresosHospitalizados '".$Fecha."','".$IdServicio."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar;  
	
	}
	
	function ListarEgresosHospitalizadosM($Fecha, $IdServicio){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarEgresosHospitalizados '".$Fecha."','".$IdServicio."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar; 	
	}
	
	function ObtenerCamasOcupadasDisponiblesAntM($Fecha, $IdServicio){		 
		require('cn/cn_sqlserver_server_locla.php');
		  $sql = "select Fecha,Ocupadas,Disponible,IdServicio from SIGESA_CamasOcupadasDisponibles where Fecha='$Fecha' and IdServicio='$IdServicio' ";
		$results = $conn->query($sql);
		
		while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
			  $validar[] = $row;
		}
			
		$stmt = null;
		$conn = null; 
		return $validar;	
	}
	
	function RegTemp_IngresosEgresosHospitalizacionM($Anio, $Mes){
		 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec usp_Susalud_RegTemp_IngresosEgresosHospitalizacion '".$Anio."','".$Mes."'";
		//echo $sql;exit();			 
		$results = $conn->query($sql);
	
		$stmt = null;
		$conn = null; 
			
	}
	
	function ListarIngresosEgresosHospitalizacionM($Anio, $Mes) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_IngresosEgresosHospitalizacion '".$Anio."','".$Mes."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarIngresosEgresosEmergenciaMenores29DiasM($Anio, $Mes) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_IngresosEgresosEmergenciaMenores29Dias '".$Anio."','".$Mes."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}
	
	function ListarConsumoServiciosM($fecha_inicio, $fecha_fin, $IdServicio, $IdCentroCosto, $codigoCPT) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarConsumoServicios '".$fecha_inicio."','".$fecha_fin."','".$IdServicio."','".$IdCentroCosto."','".$codigoCPT."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar;  
	}	
	
	function ListarDiasHospitalizadosOcu_EspeM($Anio, $Mes, $CodIpress, $CodUgiPres, $IdEspecialidad) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarDiasHospitalizadosOcu_EspeMHR '".$Anio."','".$Mes."','".$CodIpress."','".$CodUgiPres."','".$IdEspecialidad."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar;  
	}	
	
	function ListarIngresosHospitalizados_EspeM($Fecha, $IdEspecialidad){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarIngresosHospitalizados_EspeMHR '".$Fecha."','".$IdEspecialidad."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar;  
	
	}
	
	function ListarEgresosHospitalizados_EspeM($Fecha, $IdEspecialidad){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarEgresosHospitalizados_EspeMHR '".$Fecha."','".$IdEspecialidad."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar; 	
	}
	
	function ListarCitasM($FechaInicio, $FechaFinal) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_Citas '".$FechaInicio."','".$FechaFinal."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			//$stmt = null;
			$conn = null; 
			return $validar;  
	}	
	
	function ListarAtencionesHospServicioDesocupacionM($Fecha, $IdServicio){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarAtencionesHospServicioDesocupacionMHR '".$Fecha."','".$IdServicio."'";					 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}			
			$conn = null; 
			return $validar; 	
	}
	
	function ListarAtencionesHospMaximoSecuenciaM($IdAtencion){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarAtencionesHospMaximoSecuenciaMHR '".$IdAtencion."'";					 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}			
			$conn = null; 
			return $validar; 	
	}
	
	function ListarAtencionesHospEspeDesocupacionM($Fecha, $IdEspecialidad){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarAtencionesHospEspeDesocupacionMHR '".$Fecha."','".$IdEspecialidad."'";					 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}			
			$conn = null; 
			return $validar; 	
	}

	function ListarIngresosEgresosHospitalizacionFindeMesM($FechaFinal) {
        
			require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_IngresosEgresosHospitalizacionFindeMes '".$FechaFinal."'";
			//echo $sql;exit();			 
			$results = $conn->query($sql);
			
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}
				
			$stmt = null;
			$conn = null; 
			return $validar;  
	}	
	
	function ListarIngresosHospitalizadosUCIPediatricosM($Anio, $Mes){		 
		require('cn/cn_sqlserver_server_locla.php');
			$sql = "exec usp_Susalud_Reporte_ListarIngresosHospitalizadosUCIPediatricosMHR '".$Anio."','".$Mes."'";					 
			$results = $conn->query($sql);
			$validar=NULL;
			while ($row = $results->fetch(PDO::FETCH_UNIQUE)){
				  $validar[] = $row;
			}			
			$conn = null; 
			return $validar; 	
	}
	
	
?>