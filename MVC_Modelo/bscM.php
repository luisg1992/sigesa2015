<?php 
	/**
 * @author Rodolfo Esteban Crisanto Rosas <[<crisanto.rosas.22@gmail.com>]>
 */
	#MOSTRANDO CITAS CON MEDICO
	function Mostrar_Citas_x_Medico_M()
	{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaBSCCitasxMedico";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}


	#MOSTRANDO HORAS DE PROGRAMACION
	function Mostrar_Horas_x_Medico_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaBSCHorasxMedico";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO HORAS POR MEDICO POR DEPARTAMENTOS
	function Mostrar_Horas_x_Medico_Dpto_M($dpto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaBSCHorasxMedicoxDpto " . $dpto;		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO CITAS POR MEDICO POR DEPARTAMENTOS
	function Mostrar_Citas_x_Medico_Dpto_M($dpto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaBSCCitasxMedicoxDpto " . $dpto;		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO HORAS PROGRAMADAS POR ESPECIALIDAD Y DEPARTAMENTO
	function Mostrar_Horas_x_Medico_Esp_Dpto_M($dpto,$esp)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCHorasxEspecialidad ".$dpto.", ".$esp;	
    	#echo $sql;exit();	   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO CITAS POR ESPECILIDAD Y DEPARTAMENTO
	function Mostrar_Citas_x_Medico_Esp_Dpto_M($dpto,$esp)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCCitasxEspecialidad ".$dpto.", ".$esp;
		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO CITAS POR MEDICO
	function Mostrar_Citas_x_Id_Medico_M($id_med)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCCitasxIdMedico " . $id_med;
		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO HORAS POR MEDICO
	function Mostrar_Horas_x_Id_Medico_M($id_med)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCHorasxIdMedico " . $id_med;
		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO ATENDIDOS POR EMERGENCIA
	function Mostrar_Atendidos_x_Emergencia_M()
	{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaBSCAtendidosEmergencia";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#INDICADORES DE SERVICIOS
	#========================

	#MOSTRANDO ATENDIDOS - ATEN/CITAS
	function Mostrar_Atendidos_M()
	{	 
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec  SigesaBSCAtencionesxMedico";		   	 
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO ATENCIONES POR EMERGENCIA
	function Mostrar_Atenciones_x_Emergencia_Serv_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCAtencionesxEmergencia";
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO ATENDIDOS POR EMERGENCIA
	function Mostrar_Atendidos_x_Emergencia_Serv_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCAtendidosxEmergencia";
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO INGRESO POR CENTRO DE COSTO
	function Mostrar_Ingreso_x_Centro_Costo_M($primer_dia,$ultimo_dia)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCIngresoxCentroCosto '" . $primer_dia . "', '" . $ultimo_dia . "'";
		#echo $sql; exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MOSTRANDO INGRESO POR CENTRO DE COSTOS POR ID DE CENTRO COSTO	
	function Mostrar_Ingreso_x_Centro_Costo_x_idcc_M($primer_dia,$ultimo_dia,$idcentrocosto)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCIngresoxCentroCostoxIdCC '" . $primer_dia . "', '" . $ultimo_dia . "', " . $idcentrocosto;
		#echo $sql; exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#REPORTES GENERALES 
	#==================
	
	
	#REPORTE DE PACIENTES Y SUS ATENCIONES POR NUMERO DE HISTORIA
	function Mostrar_Reporte_Paciente_x_NumHis_M($numhis)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCReportePacientexNumHis " . $numhis;
		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#REPORTE DE PACIENTES Y SUS ATENCIONES POR NUMERO DE DNI
	function Mostrar_Reporte_Paciente_x_NumDni_M($numdni)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCReportePacientexNumDni " . $numdni;
		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#MEDICOS DE LA BASE DE DATOS
	function Mostrar_Medicos_M()
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCMostrarMedicos";
		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#REPORTE DE MEDICO POR ID POR MES
	function Mostrar_Programacion_x_Mes_M($fechainico,$fechafin,$idmedico)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCRPCuposxMedicoxMes '" . $fechainico ."' , '" . $fechafin . "', ".$idmedico;		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#REPORTE PROGRAMACION POR DIA POR MEDICO
	function Detalle_Programacion_x_Dia_x_Medico_M($fecha,$idmedico)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCMostrarProgramacionDetallexIdMedxfecha " . $idmedico . ", '" . $fecha . "'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#REPORTE PROGRAMACION POR DIA POR MEDICO COMPLETO
	function Detalle_Programacion_x_Dia_x_Medico_Completo_M($fecha,$idmedico)
	{
		require('cn/cn_sqlserver_server_locla.php');
		$sql = "exec SigesaBSCMostrarProgramacionxDiaxidMed " . $idmedico . ", '" . $fecha . "'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#REPORTE CENTRO DE COSTO DESGREGADO
	function Centro_Costo_x_Id_fi_ff_M($idcc,$fechainicio,$fechafin)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaBSCCentroCostoDesgregado ".$idcc.", '".$fechainicio."', '".$fechafin."'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#REPORTES - DATOS EMERGENCIA
	#===========================
	
	#EMERGENCIA - DATA GENERAL
	function Data_General_Emergencia_M($fechainicio,$fechafin)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaBSCDataEmergenciaGeneralxRangoFecha '".$fechainicio."', '".$fechafin."'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
	
	#EPIDEMIO - DATA GENERAL
	function Data_General_Epidemio_M($fechainicio,$fechafin)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaBSCEpidemioGeneralRangFecha '".$fechainicio."', '".$fechafin."'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#ACCIDENTES DE TRANSITO - DATA GENERAL
	function Data_General_Transtio_M($fechainicio,$fechafin)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaBSCAccidentesTransitoRangFecha '".$fechainicio."', '".$fechafin."'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#AGRESIONES - DATA GENERAL
	function Data_General_Agresiones_M($fechainicio,$fechafin)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaBSCAgresionesRangFecha '".$fechainicio."', '".$fechafin."'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}

	#TOPICOS - DATA GENERAL
	function Data_Topicos_M($fechainicio,$fechafin)
	{
		require('cn/cn_sqlserver_server_sigh.php');
		$sql = "exec SigesaBSCEmergenciaxTopicos '".$fechainicio."', '".$fechafin."'";		
		#echo $sql;exit();
		$results = $conn->query($sql);
		while ($row = $results->fetch(PDO::FETCH_UNIQUE))
		{
			$validar[] = $row;
		}		
		$stmt = null;
		$conn = null; 
		return $validar;
	}
 ?>