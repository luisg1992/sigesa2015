    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Antimicrobianos</title>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

		</style>
		</head>
		<body>
		<script type="text/javascript">	
		$(document).ready(function(){
		$('#Contenedor_Editor').dialog('close');
		
		});
		
		function Cancelar_Actualizacion(){
		$('#Contenedor_Editor').dialog('close');	
		}

		
		function Ejecutar_Actualizacion(){	
		$('#Contenedor_Editor').dialog('close');
		var IdProducto=$("#IdProducto").val();  		
		var IdTipoAntimicrobiano=$( "#Tipo_Antimicrobiano option:selected" ).val();
		var parametros =
			{
			"IdProducto" : IdProducto,
			"IdTipoAntimicrobiano" : IdTipoAntimicrobiano
			};
																
			$.ajax({
				data:  parametros,
				url:   '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=ModificarAntimicrobiano',
				type:  'post'
			});
			
			$.messager.alert('SIGESA','Se modifico Antimicrobiano Satisfactoriamente');	
			$('#Antimicrobianos').datagrid('reload');
		}
		
		</script>
		<div class="easyui-layout" style="width:100%;height:100%;">
	
            <div data-options="region:'south',split:true" style="height:3%;">
			</div>
            <div data-options="region:'west',split:true,iconCls:'icon-ok'" title="Lista de Medicamentos" style="width:50%;">
					<div style="margin-left:10px;float:left; width:150px">
					<p>Lista de Medicamentos disponibles</p>
					</div>
					<div style="float:left;margin:20px 0;padding-top:2px">
						<a href="#" class="easyui-linkbutton" onclick="Agregar_Antimicrobiano()" data-options="iconCls:'icon-add'">Agregar</a>
					</div>
					<div style="clear:both">
					</div>
				    <table id="Medicamentos" class="easyui-datagrid"  style="width:650px;height:610px"
					data-options="rownumbers:true,singleSelect:true,pageSize:20,pagination: true,url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarMedicamentosGrilla',method:'get'">
					<thead>
						<tr>
							<th data-options="field:'Codigo',width:80">Codigo</th>
							<th data-options="field:'Nombre',width:535">Nombre de Producto</th>
						</tr>
					</thead>
					</table>
					</div>
            <div data-options="region:'center',title:'Lista de Antimicrobianos',iconCls:'icon-ok'"  style="width:50%;">
					<div style="margin-left:10px;float:left; width:150px">
					<p>Lista de Antimicrobianos disponibles</p>
					</div>
					<div style="float:left;margin:20px 0;padding-top:2px">
						<a href="#" class="easyui-linkbutton"  onclick="$('#Contenedor_Editor').window('open')" data-options="iconCls:'icon-save'">Editar</a>
						<a href="#" class="easyui-linkbutton" onclick="Eliminar_Antimicrobiano()" data-options="iconCls:'icon-cut'">Eliminar</a>
					</div>
					
					<div id="Contenedor_Editor" class="easyui-window" title="Editar  Antimicrobiano" data-options="iconCls:'icon-save'" style="width:500px;height:180px;padding:10px;">
						<div style="float:left;margin-left:15px;">
							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px;margin-top:1px">						
							<label for="message">Codigo: </label>
							</div>
							<div style="float:left;margin-bottom:7px;margin-left:12px">
							<input type="hidden"  size="6" id="IdProducto"   readonly="readonly">
							<input type="text"  size="6" id="Codigo"   readonly="readonly">
							</div>
							</div>

							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px">						
							<label for="message">Descripción: </label>
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px">
							<input type="text"  size="37" id="Nombre"   readonly="readonly">
							</div>
							</div>	

							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px">						
							<label for="message">Tipo Antimicrobiano: </label>
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px;padding:10px 0px 0px 0px">

								<select  name="Tipo_Antimicrobiano" style="width:250px" id="Tipo_Antimicrobiano" >
									<?php 
									$resultados=Mostrar_Lista_de_Tipos_Antimicrobianos();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["IdTipoAntimicrobiano"] ?>"><?php echo mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
									<?php 
									}}
									?>                   
								</select>
								
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px">						
							&nbsp;&nbsp;
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px;padding:10px 0px 0px 0px">
								<a href="#" class="easyui-linkbutton"   onclick="Ejecutar_Actualizacion()" data-options="iconCls:'icon-save'">Aceptar</a>
								<a href="#" class="easyui-linkbutton"  onclick="Cancelar_Actualizacion()"  data-options="iconCls:'icon-cancel'">Cancelar</a>
							</div>
							</div>								
						</div>
					</div>
					<div style="clear:both">
					</div>
				    <table id="Antimicrobianos" class="easyui-datagrid"  style="width:650px;height:610px;"
					data-options="rownumbers:true,singleSelect:true,pagination: true,remoteSort:false,multiSort:true,pageSize:20,
					url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAntimicrobianosGrilla',method:'get',
					onSelect:function(index,row){
											$('#IdProducto').val(row.IdProducto);
											$('#Codigo').val(row.Codigo);
											$('#Nombre').val(row.Nombre);
											$('#PrimerNombre').val(row.PrimerNombre);
										   	$('#Tipo_Antimicrobiano').val(row.IdTipoAntimicrobiano);}
											">
					<thead>
						<tr>
							<th data-options="field:'ck',checkbox:true"></th>
							<th data-options="field:'Codigo',width:80">Codigo</th>
							<th data-options="field:'Nombre',width:402">Nombre de Producto</th>
							<th data-options="field:'Descripcion',width:105,styler:Estilo_Datagrid,sortable:true">Tipo de <br>Antimicrobiano</th>
						</tr>
					</thead>
					</table>
            </div>
        </div>
		
		
		    <script type="text/javascript">
				function Estilo_Datagrid(value,row,index){
						return 'background-color:#F6E3CE;color:#8A0808;font-weight:bold';
				}
			</script>
		
		
		    <script type="text/javascript">	
			$(function(){
				/* Aqui te Permite Seleccionar mas de un Item Selected   */
				$('#Medicamentos').datagrid({singleSelect:(0)});
				/* Habilita el Grid para poder buscar por filtro   */
				var Medicamentos = $('#Medicamentos').datagrid();
				    Medicamentos.datagrid('enableFilter');
				/* Habilita el Grid para poder buscar por filtro   */
				var Antimicrobianos = $('#Antimicrobianos').datagrid();
				    Antimicrobianos.datagrid('enableFilter');
			 });	
			</script>


			<script type="text/javascript">

				function Agregar_Antimicrobiano(){
					var rows = $('#Medicamentos').datagrid('getSelections');
					for(var i=0; i<rows.length; i++){
						var row = rows[i];
						$.post( "../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=AgregarAntimicrobiano", { 'IdProducto': row.IdProducto  } );
					}
					$('#Antimicrobianos').datagrid('reload');
				}

				
				
				function Eliminar_Antimicrobiano(){
					var rows = $('#Antimicrobianos').datagrid('getSelections');
					$.messager.confirm('Confirmar','Estas seguro que quieres eliminar Registro',function(r){
						if (r)
						{
							for(var i=0; i<rows.length; i++){
							var row = rows[i];
							$.post( "../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=EliminarAntimicrobiano", { 'IdProducto': row.IdProducto  } );
							}
							$('#Antimicrobianos').datagrid('reload');
						}
					});
				}
			</script>			
     
    </body>
    </html>