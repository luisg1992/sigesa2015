    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Antimicrobianos</title>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

		</style>
		</head>
		<body>
		<div class="easyui-layout" style="width:100%;height:100%;">
	
            <div data-options="region:'south',split:true" style="height:3%;">
			</div>
            <div data-options="region:'west',split:true,iconCls:'icon-ok'" title="Lista de Examenes" style="width:50%;">
					<div style="margin-left:10px;float:left; width:150px">
					<p>Lista de Examenes disponibles</p>
					</div>
					<div style="float:left;margin:20px 0;padding-top:2px">
						<a href="#" class="easyui-linkbutton" onclick="Agregar_Antimicrobiano()" data-options="iconCls:'icon-add'">Agregar</a>
					</div>
					<div style="clear:both">
					</div>
				    <table id="FactCatalogoServicios" class="easyui-datagrid" style="width:750px;height:610px"
					data-options="rownumbers:true,singleSelect:true,pagination: true,pageSize:20,url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarExamenes',method:'get'">
					<thead>
						<tr>
							<th data-options="field:'Codigo',width:80">Codigo</th>
							<th data-options="field:'Nombre',width:630">Nombre de Servicio</th>
						</tr>
					</thead>
					</table>
			</div>
			
            <div data-options="region:'center',title:'Lista de Examenes',iconCls:'icon-ok'"  style="width:50%;">
					<div style="margin-left:10px;float:left; width:150px">
					<p>Lista de Examenes para Antimicrobianos disponibles</p>
					</div>
					<div style="float:left;margin:20px 0;padding-top:2px">
						<a href="#" class="easyui-linkbutton"  onclick="$('#Contenedor_Editor').window('open')" data-options="iconCls:'icon-save'">Editar</a>
						<a href="#" class="easyui-linkbutton" onclick="Eliminar_Antimicrobiano()" data-options="iconCls:'icon-cut'">Eliminar</a>
					</div>
					<div style="clear:both">
					</div>
				    <table id="ExamenesAntimicrobianos" class="easyui-datagrid"  style="width:650px;height:610px"
					data-options="rownumbers:true,singleSelect:true,pagination: true,remoteSort:false,multiSort:true,pageSize:20,
					url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAntimicrobianosExamenes',method:'get'">
					<thead>
						<tr>
							<th data-options="field:'ck',checkbox:true"></th>
							<th data-options="field:'Codigo',width:80">Codigo</th>
							<th data-options="field:'Nombre',width:500">Nombre de Servicio</th>
						</tr>
					</thead>
					</table>
            </div>
        </div>
		
		
		    <script type="text/javascript">
				function Estilo_Datagrid(value,row,index){
						return 'background-color:#F6E3CE;color:#8A0808;font-weight:bold';
				}
			</script>
		
		
		    <script type="text/javascript">	
			$(function(){
				var FactCatalogoServicios = $('#FactCatalogoServicios').datagrid();
				    FactCatalogoServicios.datagrid('enableFilter');
				/* Habilita el Grid para poder buscar por filtro   */
				var ExamenesAntimicrobianos = $('#ExamenesAntimicrobianos').datagrid();
				    ExamenesAntimicrobianos.datagrid('enableFilter');
			 });	
			</script>

			<script type="text/javascript">

				function Agregar_Antimicrobiano(){
					var rows = $('#FactCatalogoServicios').datagrid('getSelections');
					for(var i=0; i<rows.length; i++){
						var row = rows[i];
						$.post( "../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=AgregarExamenAntimicrobiano", { 'IdProducto': row.IdProducto  } );
					}
					$('#ExamenesAntimicrobianos').datagrid('reload');
				}

				
				
				function Eliminar_Antimicrobiano(){
					var rows = $('#ExamenesAntimicrobianos').datagrid('getSelections');
					$.messager.confirm('Confirmar','Estas seguro que quieres eliminar Registro',function(r){
						if (r)
						{
							for(var i=0; i<rows.length; i++){
							var row = rows[i];
							$.post( "../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=EliminarExamenAntimicrobiano", { 'IdProducto': row.IdProducto  } );
							}
							$('#ExamenesAntimicrobianos').datagrid('reload');
						}
					});
				}
			</script>			
     
    </body>
    </html>