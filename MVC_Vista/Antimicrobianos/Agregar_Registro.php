			<style>
			html,body { 
				padding: 0px;
				margin: 0px;
				height: 100%;
				font-family: 'Helvetica'; 			
			}

			.mayus>input{
				text-transform: capitalize;
			}		
			.letras_peque�as{
				font-size:9px;
				font-weight:bold;
				color:red;
			}
			
			.letras_peque�as_2{
				font-size:8px;
				font-weight:bold;
				color:red;
				
			}
			
			.error{
				background-color: #BC1010;
				border-radius: 4px 4px 4px 4px;
				color: white;
				font-weight: bold;
				margin-left: 16px;
				margin-top: 6px;
				padding: 6px 12px;
				position: absolute;
			}
			.error:before{
				border-color: transparent #BC1010 transparent transparent;
				border-style: solid;
				border-width: 6px 8px;
				content: "";
				display: block;
				height: 0;
				left: -16px;
				position: absolute;
				top: 8px;
				width: 0;
			}
			.result_fail{
				background: none repeat scroll 0 0 #BC1010;
				border-radius: 20px 20px 20px 20px;
				color: white;
				font-weight: bold;
				padding: 10px 20px;
				text-align: center;
			}
			.result_ok{
				background: none repeat scroll 0 0 #1EA700;
				border-radius: 20px 20px 20px 20px;
				color: white;
				font-weight: bold;
				padding: 10px 20px;
				text-align: center;
			}

			fieldset, legend ,#contenido{
				border: 1px solid #ddd;
				background-color: #eee;
				-moz-border-radius-topleft: 5px;
				border-top-left-radius: 5px;
				-moz-border-radius-topright: 5px;
				border-top-right-radius: 5px;
				margin-top:2px;
				}
			legend {
				font-weight: normal;
				font-size: 1 em;
				text-shadow: #fff 1px 1px 1px; 
				font-weight: bold;
				color :#777;
				}
			fieldset ,#contenido{
				background-color: #f7f7f7;
				-moz-border-radius-bottomleft: 5px;
				border-bottom-left-radius: 5px;
				-moz-border-radius-bottomright: 5px;
				border-bottom-right-radius: 5px; }			
			
			#contenido{
				margin:6px;
			}
		</style>
	
	<div class="easyui-layout" style="width:100%;height:100%">
        <div data-options="region:'center',iconCls:'icon-ok'" style="width:100%;height:0%">
			<div style="margin:5px;width:950px">
			<div style="width:928px">
			<fieldset>
			<legend align="left">Datos de Busqueda</legend>
					<div style="clear:both;padding-top:2px">
						<div style="float:left;width:40px">						
						<label for="message">Dni :</label>
						</div>
						<div style="float:left;">
							<input type="text"  size="6" id="b_Dni">
						</div>
						<div style="float:left;width:50px;margin-left:10px">						
						<label for="message">Historia Clinica :</label>
						</div>
						<div style="float:left;">
						<input type="text"  size="8" id="b_HistoriaClinica">
						</div>
						<div style="float:left;width:50px;;margin-left:10px">						
						<label for="message">Apellido Paterno :</label>
						</div>
						<div style="float:left;">
						<input type="text"  size="17" id="b_ApellidoPaterno">
						</div>
						<div style="float:left;width:55px;margin-left:10px">						
						<label for="message">Apellido Materno :</label>
						</div>
						<div style="float:left;">
						<input type="text"  size="15" id="b_ApellidoMaterno">
						</div>
						<div style="float:left;width:100px;margin-left:10px">						
						<a id="Busqueda_Paciente" href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:96px;height: 30px;">BUSCAR</a>
						</div>
						<div style="float:left;margin-bottom:0px">
						<a id="Limpiar_Busqueda" href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:96px;height: 30px;">LIMPIAR</a>
						</div>
					</div>
			</fieldset>
			</div>		
			<div id="Lista_Pacientes" class="easyui-dialog" title="Lista de Pacientes" data-options="iconCls:'icon-save'" 
						style="width:700px;height:430px;padding:10px">
						<table id="Lista"></table>
			</div>
			<div id="Lista_Atenciones" class="easyui-dialog" title="Lista de Atenciones" data-options="iconCls:'icon-save'" 
						style="width:750px;height:450px;padding:10px">
						<table id="Lista_Atenciones_Paciente"></table>
			</div>
			<div style="clear:both;margin-top:10x">
				<div style="float:left;width:650px;">
				<fieldset>
				<legend align="left">Datos Personales de Paciente</legend>
						<div style="float:left; ">
							<div style="float:left;width:130px">						
							<label for="message">Primer Nombre :</label>
							</div>
							<div style="float:left;margin-bottom:15px">
							<input type="text"  size="20" id="PrimerNombre">
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Segundo Nombre :</label>
							</div>
							<div style="float:left;margin-bottom:15px">
							<input type="text"  size="20" id="SegundoNombre">
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Apellido Paterno :</label>
							</div>
							<div style="float:left;margin-bottom:15px">
							<input type="text"  size="20" id="ApellidoPaterno">
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Apellido Materno  :</label>
							</div>
							<div style="float:left;margin-bottom:1px">
							<input type="text"  size="20" id="ApellidoMaterno">
							</div>
							</div>
						</div>
						<div style="float:left;margin-left:15px">
							<div style="float:left;width:130px">						
							<label for="message">Fecha Nacimiento :</label>
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="20" id="FechaNacimiento">
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Historia Clinica :</label>
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="20" id="NroHistoriaClinica">
							<input type="hidden"  size="10" id="IdPaciente">
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:60px">						
							<label for="message">Edad :</label>
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="8" id="Edad">
							</div>
							<div style="float:left;width:50px;margin-left:15px">						
							<label for="message">Sexo :</label>
							</div>
							<div style="float:left;margin-right:5px;">
							<label class="radio inline"> 
							<input type="radio" name="Sexo" id="Sexo_Femenino" value="Sexo_Femenino" >
							<span>Femenino </span>
							</label>
							<br>
							<label class="radio inline"> 
							<input type="radio" name="Sexo" id="Sexo_Masculino" value="Sexo_Masculino" >
							<span>Masculino </span>
							</label>
							</div>
							</div>
						</div>	
				</fieldset>
				</div>
				<div style="float:left;width:280px;margin-left:0px">
				<fieldset>
				<legend align="left">Datos de Atencion</legend>
						<div style="float:left;width:140px">
							<div style="clear:both">
							<div style="float:left;width:105px;margin-left:12px">						
							<label for="message">F. Financiamiento : </label>
							</div>
							<div style="float:left;margin-bottom:7px;margin-left:12px">
							<input type="text"  size="15" id="FuenteFinanciamiento"   readonly="readonly">
							</div>
							</div>
							
							
							<div style="clear:both">
							<div style="float:left;width:95px;margin-left:12px">						
							<label for="message">Fecha Ingreso : </label>
							</div>
							<div style="float:left;margin-bottom:10px;margin-left:12px">
							<input type="text"  size="15" id="FechaIngreso"   readonly="readonly">
							</div>
							</div>
						</div>
						<div style="float:left;margin-left:15px;width:100px">
							<div style="clear:both">
							<div style="float:left;width:50px;margin-left:12px;margin-top:1px">						
							<label for="message">Cuenta: </label>
							</div>
							<div style="float:left;margin-bottom:7px;margin-left:12px">
							<input type="text"  size="6" id="IdCuentaAtencion"   readonly="readonly">
							</div>
							</div>

							<div style="clear:both">
							<div style="float:left;width:50px;margin-left:12px">						
							<label for="message">H.Ingreso: </label>
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px">
							<input type="text"  size="6" id="HoraIngreso"   readonly="readonly">
							</div>
							</div>							
						</div>
						<div style="clear:both">
							<div style="float:left;width:95px;margin-left:12px;margin-bottom:3px">						
							<label for="message">Servicio : </label>
							</div>
							<div style="float:left;margin-bottom:3px;margin-left:12px">
							<input type="text"  size="30" id="Servicio"   readonly="readonly">
							</div>
						</div>
				</fieldset>
				</div>
			</div>
			
			<div style="width:100%;clear:both;margin-bottom:60px">	
					<div style="float:left;width:790px">					
					<fieldset>
					<legend align="left">Funciones Vitales Actuales</legend>
							<div style="clear:both;padding-top:2px">
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">FR :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_FR">
								</div>								
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">FC :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_FC">
								</div>								
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">PAS :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_PAS">
								</div>
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">PAD :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_PAD">
								</div>
								<div style="float:left;width:70px;margin-left:10px">						
								<label for="message">Glasgow :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_Glasgon">
								</div>
								<div style="float:left;width:30px;margin-left:10px">						
								<label for="message">T&#176; :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_Temperatura">
								</div>
								<div style="float:left;width:60px;margin-left:10px">						
								<label for="message">Peso <span style="font-size:10px  !important;font-weight:bold">(kg)</span>  :</label>
								</div>
								<div style="float:left;margin-left:10px">
									<input type="text"  size="3" id="FuncionV_Peso">
								</div>
							</div>
					</fieldset>
					</div>
					<div style="float:left;width:140px">					
					<fieldset>
					<legend align="left">Datos Adicionales</legend>
							<div style="clear:both;padding-top:2px">
								<div style="float:left;width:50px;margin-left:1px">						
								<label for="message">Cama :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="4" id="Cama"   readonly="readonly">
								</div>
							</div>
					</fieldset>
					</div>
			</div>
			</div>		
		<!-- Aqui se mandan los valores --> 
		<div id="contenido_agregar" class="easyui-tabs" style="width:930px;height:350px;clear:both">
			<div title="1.- Diagnosticos y Otros" style="padding:25px;background:#FAFAFA" >
			<div style="height:50px">		
						<div id="Lista_Diagnosticos_IV" class="easyui-dialog" title="Lista de Diagnosticos" data-options="iconCls:'icon-save'" 
						style="width:590px;height:440px;padding:10px">
						
						<table id="Lista_Diagnosticos_Antecedentes" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDiagnosticosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Lista_Diagnosticos_IV').dialog('close');
										$('#codigo_dx_Antecedentes').val(row.CodigoCIE10);
										$('#dx_Antecedentes').val(row.Descripcion);
										$('#IdDiagnosticoPatologico').val(row.IdDiagnostico);
										}">
							<thead>
								<tr>
									<th data-options="field:'CodigoCIE10',width:80">Codigo</th>
									<th data-options="field:'Descripcion',width:600">Nombre de Diagnostico</th>
								</tr>
							</thead>
						</table>
						</div>
						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Anteced. Patol&oacute;gico :</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="codigo_dx_Antecedentes">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="45" id="dx_Antecedentes">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="45" id="IdDiagnosticoPatologico">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Diagnosticos_Antecedentes">...</a>
							</div>
						</div>
						


						<div id="Lista_Diagnosticos_II" class="easyui-dialog" title="Lista de Diagnosticos" data-options="iconCls:'icon-save'" 
						style="width:590px;height:440px;padding:10px">
						
						<table id="Lista_Diagnosticos_Infeccioso" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDiagnosticosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Lista_Diagnosticos_II').dialog('close');
										$('#codigo_dx_infeccioso').val(row.CodigoCIE10);
										$('#dx_infeccioso').val(row.Descripcion);
										$('#IdDiagnosticoInfeccioso').val(row.IdDiagnostico);
										}">
							<thead>
								<tr>
									<th data-options="field:'CodigoCIE10',width:80">Codigo</th>
									<th data-options="field:'Descripcion',width:600">Nombre de Diagnostico</th>
								</tr>
							</thead>
						</table>
						</div>
						
						
						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">DX Infeccioso :</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="codigo_dx_infeccioso">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="45" id="dx_infeccioso">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="5" id="IdDiagnosticoInfeccioso">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Diagnosticos_Infeccioso">...</a>
							</div>
						</div>
						
						
											
						
						<div id="Lista_Diagnosticos_III" class="easyui-dialog" title="Lista de Diagnosticos" data-options="iconCls:'icon-save'" 
						style="width:590px;height:440px;padding:10px">
						
						<table id="Lista_Diagnosticos_Otro" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDiagnosticosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Lista_Diagnosticos_III').dialog('close');
										$('#codigo_dx_otro').val(row.CodigoCIE10);
										$('#dx_otro').val(row.Descripcion);
										$('#IdDiagnosticoOtro').val(row.IdDiagnostico);
										}">
							<thead>
								<tr>
									<th data-options="field:'CodigoCIE10',width:80">Codigo</th>
									<th data-options="field:'Descripcion',width:600">Nombre de Diagnostico</th>
								</tr>
							</thead>
						</table>
						</div>
						
						
						
						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Otro Diagn&oacute;stico :</label>
							</div>
							<div style="float:left;margin-bottom:10px;margin-right:5px">
							<input type="text"  size="5" id="codigo_dx_otro">
							</div>
							<div style="float:left;margin-bottom:10px">
							<input type="text"  size="45" id="dx_otro">
							</div>
							<div style="float:left;margin-bottom:10px;margin-right:5px">
							<input type="hidden"  size="45" id="IdDiagnosticoOtro">
							</div>
							<div style="float:left;margin-bottom:10px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Diagnosticos_Otro">...</a>
							</div>
						</div>
						

						
						
						<div style="clear:both;margin-bottom:25px">
							<div style="float:left;">
								<div style="float:left;width:100px">						
								<label for="message">Infecci&oacute;n IntraHospitalaria Actual :</label>
								</div>
								<div style="float:left;margin-bottom:10px;margin-left:35px;">
								<div class="maxl">
								  <label class="radio inline"> 
									   <input type="radio" name="Infeccion_IntraHospitalaria" id="Infeccion_SI"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="1">
									  <span> Si </span> 
								   </label>
								  <label class="radio inline"> 
										<input type="radio" name="Infeccion_IntraHospitalaria" id="Infeccion_NO"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="0" checked>
									  <span>No </span> 
								  </label>
								</div>
								</div>
							</div>
						</div>

						
						<div style="clear:both;margin-bottom:25px">
							<div id="Contenedor_Infeccion" style="display:none;float:left;width:850px;height:30px;margin-bottom:40px;">
								<div style="clear:both;margin-bottom:5px">
								<div style="float:left;width:100px;margin-top:50px;">						
								<label for="message">Descripci&oacute;n IntraHospitalaria :</label>
								</div>
								<div style="float:left;margin-right:5px;margin-left:30px">
								<textarea rows="3" cols="50" id="Descripcion_IntraHospitalaria" name="Descripcion_IntraHospitalaria">
								</textarea> 
								</div>
								</div>
							</div>	
						</div>

			</div>
			</div>

			<div title="2.- Antimicrobianos Previos" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
			<div style="height:100px">
						<div id="Antimicrobianos_I" class="easyui-dialog" title="Lista de Antimicrobianos Previos" data-options="iconCls:'icon-save'" 
						style="width:590px;height:440px;padding:10px">
						<table id="Lista_Antimicrobianos_I" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAntimicrobianosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Antimicrobianos_I').dialog('close');
										$('#Codigo_Antimicrobiano_Previo_I').val(row.Codigo);
										$('#Antimicrobiano_Previo_I').val(row.Nombre);
										$('#IdAntimicrobianoPrevio_I').val(row.IdProducto);
										$( '#AntimicrobianoDia_I').focus();
										}">
							<thead>
								<tr>
									<th data-options="field:'Codigo',width:80">Codigo</th>
									<th data-options="field:'Nombre',width:467">Nombre de Producto</th>
								</tr>
							</thead>
						</table>
						</div>

						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Antimicrobianos<br>Previos :</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="Codigo_Antimicrobiano_Previo_I">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="50" id="Antimicrobiano_Previo_I">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="5" id="IdAntimicrobianoPrevio_I">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Antimicrobiano_Previo_I">...</a>
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:15px">
								<input type="text"  size="3" id="AntimicrobianoDia_I"   class="AntimicrobianoDia">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<span class="letras_peque�as" >(Dias) </span>
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"  id="Agregar_Anti_Previo">Agregar</a>
							</div>
						</div>
						
						<div style="clear:both;height:250px">
						<table id="AntimicrobianoPrevio"></table>
						</div>
						
			</div>
			</div>

						
			<div title="3.- Antimicrobianos Controlados" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
			
			
			<div id="Contenedor_Antimicrobiano_Solicitado_I" class="easyui-dialog" title="Lista de Antimicrobianos Controlados" data-options="iconCls:'icon-save'" 
				style="width:620px;height:440px;padding:10px">
				<table id="Lista_Antimicrobiano_Solicitado_I" class="easyui-datagrid"  style="width:585px;height:380px"
				data-options="rowtexts:true,singleSelect:true,pagination: true,
				url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAntimicrobianosControladosGrilla',method:'get',
				rowStyler: function(index,row){
                if (row.Stock=='0'){
                return 'color:#ff3333;';
                }
                },
				onSelect:function(index,row){
					if(row.Stock!='0'){
						$('#Contenedor_Antimicrobiano_Solicitado_I').dialog('close');
						$('#Id_Antimicrobiano_Solicitado').val(row.IdProducto);
						$('#Codigo_Antimicrobiano_Solicitado').val(row.Codigo);
						$('#Descripcion_Antimicrobiano_Solicitado').val(row.Nombre);
						$('#Dosis_Antimicrobiano_Solicitado').focus();
					}
					else
					{
					$.messager.alert('SIGESA','El Antimicrobiano : <strong>'+row.Nombre+'</strong> no cuenta con Stock');		
					}				
				}">
				<thead>
				<tr>
				<th data-options="field:'Codigo',width:80">Codigo</th>
				<th data-options="field:'Stock',width:80">Stock</th>
				<th data-options="field:'Nombre',width:423">Nombre de Producto</th>
				</tr>
				</thead>
				</table>
			</div>
						
						

			
			
			<div style="height:290px">
						<div style="clear:both;">
							<div style="float:left;width:100px">						
							<label for="message">Solicitar Antimicrobianos:</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="5" id="Id_Antimicrobiano_Solicitado">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="Codigo_Antimicrobiano_Solicitado" readonly="readonly">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="50" id="Descripcion_Antimicrobiano_Solicitado" readonly="readonly">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Antimicrobiano_Solicitado_I">...</a>
							</div>
							
							
							<div style="float:left;margin-bottom:20px;margin-left:10px">
							<div style="position:relative;margin-left:4px;">
							<input type="text"  size="4" id="Dosis_Antimicrobiano_Solicitado">
							</div>
							<div style="position:relative;margin-top:2px;">
								<span class="letras_peque�as" style="margin-left:15px;">DOSIS</span>
								<BR> 
								<span class="letras_peque�as_2" style="margin-left:15px;">( mg ) </span>
								 
							</div>
							</div>

							<div style="float:left;margin-bottom:20px;margin-left:10px;">
							<div style="position:relative;margin-left:8px;">
							<input type="text"  size="4" id="Frecuencia_Antimicrobiano_Solicitado">
							</div>
							<div style="position:relative;margin-top:2px;">
							  <span class="letras_peque�as" style="margin-left:10px;">FRECUENCIA</span>
								<BR>
							  <span class="letras_peque�as_2" style="margin-left:20px;">( horas )</span>
							</div>
							</div>


							<div style="float:left;margin-bottom:20px;margin-left:9px;margin-top:-4px">
								<a href="#" class="easyui-linkbutton"   data-options="iconCls:'icon-add'" id="Agregar_Antimicrobiano">Agregar</a>
							</div>
							
						</div>
						
						<div style="clear:both;height:220px">
						<table id="AntimicrobianoSolicitados"></table>
						</div>
	
			</div>
			</div>
									
			<div title="4.- Justificaci&oacute;n" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
						<div style="clear:both">
							<div style="float:left;width:100px;margin-top:15px">						
							<label for="message">Justificaci&oacute;n :</label>
							</div>
							<div style="float:left;margin-bottom:10px;margin-right:5px;margin-top:15px;margin-left:5px">
							<textarea rows="4" cols="80" id="Justificacion"  name="Justificacion" style="width: 700px; height: 180px;">
							</textarea> 
							</div>
						</div>
			</div>
		</div>
		</div>
		<div data-options="region:'south',split:true" style="width:250px;height:9%;padding-left:70px;padding-top:1px;">
		  	<table cellpadding="5">
			<tr>
			<td><a href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:110px;height:40px;"   id="Guardar_Cambios_Agregar"     onclick="Guardar_Cambios_Agregar()">GUARDAR</a></td>
			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:110px;height:40px;"  id="Cerrar_Contenedor_Agregar"   onclick="Cerrar_Contenedor_Agregar()">SALIR</a></td>
			</tr>
			</table>
		</div>
	</div>