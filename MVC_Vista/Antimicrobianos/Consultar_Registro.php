								<!--Vista para Consulta de Registro de Solicitud de Antimicrobiano -->
								<div class="easyui-layout" style="width:100%;height:100%">
								<div data-options="region:'center',iconCls:'icon-ok'" style="width:900px;height:0%">									
									<div style="margin:5px;width:940px">
									<div style="clear:both;margin-top:10x">
										<div style="float:left;width:650px;">
										<fieldset>
										<legend align="left">Datos Personales de Paciente</legend>
												<div style="float:left; ">
													<div style="float:left;width:120px">						
													<label for="message">Primer Nombre :</label>
													</div>
													<div style="float:left;margin-bottom:15px">
													<input type="text"  size="20" class="PrimerNombre"   readonly="readonly">
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Segundo Nombre :</label>
													</div>
													<div style="float:left;margin-bottom:15px">
													<input type="text"  size="20" class="SegundoNombre"  readonly="readonly">
													</div>
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Apellido Paterno :</label>
													</div>
													<div style="float:left;margin-bottom:15px">
													<input type="text"  size="20" class="ApellidoPaterno" readonly="readonly">
													</div>
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Apellido Materno  :</label>
													</div>
													<div style="float:left;margin-bottom:1px">
													<input type="text"  size="20" class="ApellidoMaterno" readonly="readonly">
													</div>
													</div>
												</div>
												
												
												<div style="float:left;margin-left:15px">
													<div style="float:left;width:120px">						
													<label for="message">Fecha Nacimiento :</label>
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="20" class="FechaNacimiento" readonly="readonly">
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Historia Clinica :</label>
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="20" class="NroHistoriaClinica" readonly="readonly">
													<input type="hidden"  size="10" class="IdPaciente">
													</div>
													</div>
													<div style="clear:both">
													<div style="float:left;width:60px">						
													<label for="message">Edad :</label>
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="3" class="Edad" readonly="readonly">
													</div>
													<div style="float:left;width:50px;margin-left:15px">						
													<label for="message">Sexo :</label>
													</div>
													<div style="float:left;margin-right:5px;">
													<label class="radio inline"> 
													<input type="radio" name="Sexo" id="Sexo_Femenino_Consultar" value="Sexo_Femenino" >
													<span>Femenino </span>
													</label>
													<br>
													<label class="radio inline"> 
													<input type="radio" name="Sexo" id="Sexo_Masculino_Consultar" value="Sexo_Masculino" >
													<span>Masculino </span>
													</label>
													</div>
													</div>
												</div>
												
												
												
										</fieldset>
										</div>
										
										<div style="float:left;width:280px;margin-left:2px">
										<fieldset>
										<legend align="left">Datos de Atencion</legend>
												<div style="float:left;width:140px">
													<div style="clear:both">
													<div style="float:left;width:105px;margin-left:12px">						
													<label for="message">F. Financiamiento : </label>
													</div>
													<div style="float:left;margin-bottom:7px;margin-left:12px">
													<input type="text"  size="15" class="FuenteFinanciamiento"   readonly="readonly">
													</div>
													</div>
													
													
													<div style="clear:both">
													<div style="float:left;width:95px;margin-left:12px">						
													<label for="message">Fecha Ingreso : </label>
													</div>
													<div style="float:left;margin-bottom:10px;margin-left:12px">
													<input type="text"  size="15" class="FechaIngreso"   readonly="readonly">
													</div>
													</div>
												</div>
												
												
												<div style="float:left;margin-left:15px;width:100px">
													<div style="clear:both">
													<div style="float:left;width:50px;margin-left:12px;margin-top:1px">						
													<label for="message">Cuenta: </label>
													</div>
													<div style="float:left;margin-bottom:7px;margin-left:12px">
													<input type="text"  size="6" class="IdCuentaAtencion"   readonly="readonly">
													</div>
													</div>

													<div style="clear:both">
													<div style="float:left;width:50px;margin-left:12px">						
													<label for="message">H.Ingreso: </label>
													</div>
													<div style="float:left;margin-bottom:2px;margin-left:12px">
													<input type="text"  size="6" class="HoraIngreso"   readonly="readonly">
													</div>
													</div>							
												</div>
												
												
												<div style="clear:both">
													<div style="float:left;width:95px;margin-left:12px;margin-bottom:3px">						
													<label for="message">Servicio : </label>
													</div>
													<div style="float:left;margin-bottom:3px;margin-left:12px">
													<input type="text"  size="30" class="Servicio"   readonly="readonly">
													</div>
												</div>
										</fieldset>
										</div>
									</div>
									
									<div style="width:935px;clear:both;margin-bottom:65px">	
											<div style="float:left;width:790px">					
											<fieldset>
											<legend align="left">Funciones Vitales Actuales</legend>
													<div style="clear:both;padding-top:2px">
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">FR :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_FR" readonly="readonly">
														</div>								
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">FC :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_FC" readonly="readonly">
														</div>								
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">PAS :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_PAS" readonly="readonly">
														</div>
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">PAD :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_PAD" readonly="readonly">
														</div>
														<div style="float:left;width:60px;margin-left:10px">						
														<label for="message">Glasgow :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_Glasgon" readonly="readonly">
														</div>
														<div style="float:left;width:30px;margin-left:10px">						
														<label for="message">T&#176; :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_Temperatura" readonly="readonly">
														</div>
														<div style="float:left;width:60px;margin-left:10px">						
														<label for="message">Peso <span style="font-size:10px  !important;font-weight:bold">(kg)</span>  :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_Peso" readonly="readonly">
														</div>
													</div>
											</fieldset>
											</div>
											
											
											
											<div style="float:left;width:140px">					
											<fieldset>
											<legend align="left">Datos Adicionales</legend>
													<div style="clear:both;padding-top:2px">
														<div style="float:left;width:50px;margin-left:1px">						
														<label for="message">Cama :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="4" class="Cama" readonly="readonly">
														</div>
													</div>
											</fieldset>
											</div>
									</div>
									

									
								<div id="contenido" class="easyui-tabs" style="width:925px;height:410px;">
									<div title="1.- Antimicrobianos Solicitados" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
										<div style="height:325px">
											<table id="DetalleAntimicrobianos_Visualizar"></table>	
										</div>
									</div>
									
																		
									<div title="2.- Antimicrobianos Previos" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
										<div style="height:325px">
										<table id="PreviosAntimicrobianos_Visualizar"></table>	
										</div>
									</div>
									
									<div title="3.- Diag. y Otros" style="padding:25px;background:#FAFAFA" >
									<div style="height:50px">		

												<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Anteced. Patol&oacute;gico :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="text"  size="5" class="codigo_dx_Antecedentes">
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="45" class="dx_Antecedentes">
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="hidden"  size="45" class="IdDiagnosticoPatologico">
													</div>
													<div style="float:left;margin-bottom:20px;margin-left:5px">
														<a href="javascript:void(0)" class="easyui-linkbutton"  class="Ventana_Diagnosticos_Antecedentes">...</a>
													</div>
												</div>
												<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">DX Infeccioso :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="text"  size="5" class="codigo_dx_infeccioso">
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="45" class="dx_infeccioso">
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="hidden"  size="5" class="IdDiagnosticoInfeccioso">
													</div>
													<div style="float:left;margin-bottom:20px;margin-left:5px">
														<a href="javascript:void(0)" class="easyui-linkbutton"  class="Ventana_Diagnosticos_Infeccioso">...</a>
													</div>
												</div>
												<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Otro Diagn&oacute;stico :</label>
													</div>
													<div style="float:left;margin-bottom:10px;margin-right:5px">
													<input type="text"  size="5" class="codigo_dx_otro">
													</div>
													<div style="float:left;margin-bottom:10px">
													<input type="text"  size="45" class="dx_otro">
													</div>
													<div style="float:left;margin-bottom:10px;margin-right:5px">
													<input type="hidden"  size="45" class="IdDiagnosticoOtro">
													</div>
													<div style="float:left;margin-bottom:10px;margin-left:5px">
														<a href="javascript:void(0)" class="easyui-linkbutton"  class="Ventana_Diagnosticos_Otro">...</a>
													</div>
												</div>
												<div style="clear:both;margin-bottom:25px">
													<div style="float:left;">
														<div style="float:left;width:100px">						
														<label for="message">Infecci&oacute;n IntraHospitalaria Actual :</label>
														</div>
														<div style="float:left;margin-bottom:10px;margin-left:35px;">
														<div class="maxl">
														  <label class="radio inline"> 
															   <input type="radio" name="Infeccion_IntraHospitalaria_Consultar" id="Infeccion_SI_Consultar"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="1">
															  <span> Si </span> 
														   </label>
														  <label class="radio inline"> 
																<input type="radio" name="Infeccion_IntraHospitalaria_Consultar" id="Infeccion_NO_Consultar"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="0" checked>
															  <span>No </span> 
														  </label>
														</div>
														</div>
													</div>
												</div>
												<div style="clear:both;margin-bottom:25px">
													<div id="Contenedor_Infeccion_Consultar" style="display:none;float:left;width:850px;height:30px;margin-bottom:40px;">
														<div style="clear:both;margin-bottom:5px">
														<div style="float:left;width:100px;margin-top:50px;">							
														<label for="message">Descripci&oacute;n IntraHospitalaria :</label>
														</div>
														<div style="float:left;margin-right:5px;margin-left:20px">
														<textarea rows="3" cols="50" id="Descripcion_IntraHospitalaria_Consultar" name="Descripcion_IntraHospitalaria_Consultar"  readonly="readonly">
														</textarea> 
														</div>
														</div>
													</div>	
												</div>

									</div>
									</div>									
									<div title="4.- Examenes de Laboratorio" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
										<table id="Examenes_Antimicrobianos_Visualizar"></table>
									</div>
	
									
																					
									<div title="5.- Justificaci&oacute;n" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
									<div style="height:325px">
										<div style="clear:both">
													<div style="float:left;width:100px;margin-top:15px">						
													<label for="message">Justificaci&oacute;n :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px;margin-top:15px;margin-left:5px">
													<textarea rows="5" cols="80"   name="Justificacion_Consultar" style="width: 700px; height: 210px;">
													</textarea> 
													</div>
										</div>
									</div>
									</div>																				
									<div title="6.- Evaluaci&oacute;n" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
									<div style="height:325px">
										<div style="clear:both">
													<div style="float:left;width:100px;margin-top:15px">						
													<label for="message">Evaluaci&oacute;n :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px;margin-top:15px;margin-left:5px">
													<textarea rows="5" cols="80" name="Evaluacion_Consultar" style="width: 700px; height: 210px;">
													</textarea> 
													</div>
										</div>
									</div>
									</div>
								</div>
								</div>		
								</div>
								<div data-options="region:'south',split:true" style="width:250px;height:9%;padding-left:70px;padding-top:1px;">
									<table cellpadding="5">
									<tr>
									<td><a href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:110px;height: 40px;" id="Guardar_Cambios_Consultar"   onclick="Cerrar_Contenedor_Consultar()">GUARDAR</a></td>
									<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:110px;height: 40px;" id="Cancelar_Cambios_Consultar" onclick="Cerrar_Contenedor_Consultar()">SALIR</a></td>
									</tr>
									</table>
								</div>			
							</div>