	<html>
    <head>
        <meta charset="UTF-8">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/js/jquery.numeric.js"></script>
		<script src="../../MVC_Complemento/tinymce/tinymce.min.js"></script>
		<script src="../../MVC_Complemento/tinymce/langs/es.js"></script>
		<script src="../../MVC_Vista/Antimicrobianos/js/Basico.js"></script>
		<script src="../../MVC_Vista/Antimicrobianos/js/Consultar.js"></script>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<link rel="stylesheet" href="../../MVC_Complemento/css/antimicrobianos_estilo.css">
		<script>tinymce.init({ selector:'textarea' ,
							   language : 'es', 
							   menubar: false,
							   plugins: [
								'advlist autolink lists  charmap print preview anchor textcolor',
								'searchreplace visualblocks code fullscreen',
								'insertdatetime media table contextmenu paste code help'],
							   branding: false});
		</script>
		<style>
			html,body { 
				padding: 0px;
				margin: 0px;
				height: 100%;
				width: 100%;
				font-family: 'Helvetica'; 			
			}
			.mayus>input{
				text-transform: capitalize;
			}		
		</style>
	  </head>
	  <body>
				<div class="easyui-layout"  style="width:100%;height:100%">
					<div data-options="region:'north'" style="height:90px" title="Busqueda de Solicitudes">
						 <div style="margin:20px">
						 <!--
						 <div style="float:left; width:60px"><strong>Apellido Paterno :</strong></div>
						 <div style="float:left; width:70px;margin-left:12px"><input id="ApellidoPaterno_Buscar" type="text"   size="12"></div>
						 <div style="float:left; width:60px;margin-left:12px"><strong>Historia Clinica :</strong></div>
						 <div style="float:left; width:100px;margin-left:12px"><input id="HistoriaClinica_Buscar" type="text"   size="8"></div>
						 -->
						 <div style="float:left; width:40px"><strong>Fecha Inicio  : </strong></div>
						 <div style="float:left; width:105px;margin-left:12px"><input id="Fecha_Inicial_Antimicrobiano" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;" value="*"></div>
						 <div style="float:left; width:40px;margin-left:12px"><strong>Fecha Final  : </strong></div>
						 <div style="float:left; width:105px;margin-left:12px"><input id="Fecha_Final_Antimicrobiano" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;"  value="*"></div>	
						 <div style="float:left; width:90px;margin-left:12px"><a id="Busqueda_Lista_Solicitudes"  onclick="cargar_lista_paciente()" href="#" class='easyui-linkbutton' data-options="iconCls:'icon-search'" style="width:96px;height: 30px;">BUSCAR</a></div>	
						 <div style="float:left; width:90px;margin-left:12px"><a id="Limpiar_Busqueda_Lista_Solicitudes" href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:96px;height: 30px;">LIMPIAR</a></div>	
						<div style="float:left; width:90px;margin-left:12px"><a id="Reporte_Antimicrobiano" href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:96px;height: 30px;">REPORTE</a></div>	
						</div> 
					</div>
					<div data-options="region:'center',iconCls:'icon-ok'"  style="width:100%;height:100%">
						<?php
						  if($ListarPermisos != NULL)
						  {
							  foreach($ListarPermisos as $Permiso)
							  { 
							  $Agregar=$Permiso["Agregar"];
							  $Modificar=$Permiso["Modificar"];
							  $Eliminar=$Permiso["Eliminar"];
							  $Consultar=$Permiso["Consultar"];
							  }
						?>
						<?php						
						if($Consultar==0){?>
						<script>$(function(){ $('#Consultar').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php if($Modificar==0){?>
						<script>$(function(){ $('#Modificar').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php if($Eliminar==0){?>
						<script>$(function(){ $('#Eliminar').linkbutton({disabled:true});});</script>			
						<script>$(function(){ $('#Reporte_Antimicrobiano').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php if($Agregar==0){?>
						<script>$(function(){ $('#Agregar').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php
						}						
						?>
					
						<div  id="opciones">
						<p>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true"  onclick="Consultar()"  id="Consultar">CONSULTAR</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" 	onclick="Agregar()"    style="color:red !important"  id="Agregar">AGREGAR NUEVA SOLICITUD</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true"	onclick="Modificar()"  id="Modificar" >MODIFICAR</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" 	onclick="Eliminar()"   id="Eliminar">ELIMINAR</a>
						</p>
						</div>
						<table id="Antimicrobianos"></table>
					</div>
				</div>

				<!-- Contenedor de Resultados por Examen CPT  -->
				<div id="Detalle_Orden" class="easyui-window" data-options="modal:true,iconCls:'icon-man',footer:'#Pie_Detalle'" style="width:650px;height:405px;margin:0;">
					<div class="easyui-panel" style="width:620px;height:360px;margin:0;">
							<div class="easyui-layout" data-options="fit:true">
							<div data-options="region:'center',split:true" >
								<div id="tabla_resultados_kk">
								</div>
							</div>
							</div>
					</div>
				</div>

				<div id="Contenedor_Consultar" class="easyui-window" title="Consultar Solicitud de Antimicrobiano" data-options="iconCls:'icon-save'" style="width:975px;height:770px;">	
				<?php   include'../../MVC_Vista/Antimicrobianos/Consultar_Registro.php';  ?>
				</div>
				<div id="Contenedor_Agregar" class="easyui-window" title="Agregar Solicitud de Antimicrobiano" data-options="iconCls:'icon-save'" style="width:975px;height:770px;">	
				<?php   include'../../MVC_Vista/Antimicrobianos/Agregar_Registro.php';  ?>
				</div>	
				<div id="Contenedor_Modificar" class="easyui-window" title="Modificar Solicitud de Antimicrobiano" data-options="iconCls:'icon-save'" style="width:975px;height:770px;">	
				<?php   include'../../MVC_Vista/Antimicrobianos/Modificar_Registro.php';  ?>
				</div>
					
				<script>

				$(document).ready(function(){
				function Exportar_Reporte_Excel(Fecha_Inicial_Reporte,Fecha_Final_Reporte)
				{
				$.ajax({
				url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Exportar_Reporte_Excel',
				type: 'POST',
				dataType: 'json',
				data: {
				Fecha_Inicial:Fecha_Inicial_Reporte,
				Fecha_Final:Fecha_Final_Reporte
				},
				success:function(impresion)
				{
				window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
				e.preventDefault();
				alert(impresion);
				}
				});
				}
				$("#Reporte_Antimicrobiano").click(function(event) {
				var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Antimicrobiano').datebox('getValue');
				var Fecha_Final_Reporte= $('#Fecha_Final_Antimicrobiano').datebox('getValue');
				Exportar_Reporte_Excel(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
				});
					
				 cargar_lista_paciente();
				 });
				 
				 //Funcion para cargar la Lista de Pacientes
				function cargar_lista_paciente()
				{
				var Fecha_Inicial = $('#Fecha_Inicial_Antimicrobiano').datebox('getValue');
				var Fecha_Final= $('#Fecha_Final_Antimicrobiano').datebox('getValue');	
				var dg =$('#Antimicrobianos').datagrid({
				iconCls:'icon-edit',
				rownumbers:true,
				collapsible:true,
				pagination: true,
				remoteSort:false,
				multiSort:true,
				toolbar: '#opciones',
				pageSize:20,
				url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarSolicitudesAntimicrobianosGrilla&FechaInicio='+Fecha_Inicial+'&FechaFinal='+Fecha_Final,
				columns:[[
				{field:'Estado',title:'Estado',width:70},			
				{field:'FechaRegistro',title:'Fecha<br>Registro',width:140,sortable:true},
				{field:'ApellidoPaterno',title:'Apellido <br>Paterno',width:100,sortable:true},
				{field:'ApellidoMaterno',title:'Apellido <br>Materno',width:100,sortable:true},
				{field:'PrimerNombre',title:'Primer <br>Nombre',width:100,sortable:true},
				{field:'SegundoNombre',title:'Segundo <br>Nombre',width:100,sortable:true},
				{field:'NroDocumento',title:'NroDocumento',width:100,sortable:true},
				{field:'NroHistoriaClinica',title:'Historia <br> Clinica',width:70,sortable:true},
				{field:'FechaNacimiento',title:'Fecha de <br>Nacimiento',width:100,sortable:true}	
				]]		
				});
				var Antimicrobianos = $('#Antimicrobianos').datagrid();
				Antimicrobianos.datagrid('enableFilter');
				$('#Antimicrobianos').datagrid({singleSelect:(1)});
				}		
				</script>

    </body>
    </html>