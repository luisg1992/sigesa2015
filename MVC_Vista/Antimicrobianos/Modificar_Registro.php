						<div class="easyui-layout" style="width:960px;height:100%">
								<div data-options="region:'center',iconCls:'icon-ok'" style="width:900px;">									
									<div style="margin:5px 5px 0px 5px;width:940px">
									<div style="clear:both;margin-top:10x">
										<input type="hidden"  size="20" id="IdCabecera"   style="border: 0px; border-radius: 4px;  font-weight:bold">
										<div style="float:left;width:650px;">
										<fieldset>
										<legend align="left">Datos Personales de Paciente</legend>
												<div style="float:left; ">
													<div style="float:left;width:120px">						
													<label for="message">Primer Nombre :</label>
													</div>
													<div style="float:left;margin-bottom:15px">
													<input type="text"  size="20" class="PrimerNombre">
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Segundo Nombre :</label>
													</div>
													<div style="float:left;margin-bottom:15px">
													<input type="text"  size="20" class="SegundoNombre">
													</div>
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Apellido Paterno :</label>
													</div>
													<div style="float:left;margin-bottom:15px">
													<input type="text"  size="20" class="ApellidoPaterno">
													</div>
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Apellido Materno  :</label>
													</div>
													<div style="float:left;margin-bottom:1px">
													<input type="text"  size="20" class="ApellidoMaterno">
													</div>
													</div>
												</div>
												
												
												<div style="float:left;margin-left:15px">
													<div style="float:left;width:120px">						
													<label for="message">Fecha Nacimiento :</label>
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="20" class="FechaNacimiento">
													</div>
													<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Historia Clinica :</label>
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="20" class="NroHistoriaClinica">
													<input type="hidden"  size="10" class="IdPaciente">
													</div>
													</div>
													<div style="clear:both">
													<div style="float:left;width:60px">						
													<label for="message">Edad :</label>
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="3" class="Edad">
													</div>
													<div style="float:left;width:50px;margin-left:15px">						
													<label for="message">Sexo :</label>
													</div>
													<div style="float:left;margin-right:5px;">
													<label class="radio inline"> 
													<input type="radio" name="Sexo" class="Sexo_Femenino" value="Sexo_Femenino" >
													<span>Femenino </span>
													</label>
													<br>
													<label class="radio inline"> 
													<input type="radio" name="Sexo" class="Sexo_Masculino" value="Sexo_Masculino" >
													<span>Masculino </span>
													</label>
													</div>
													</div>
												</div>
												
												
												
										</fieldset>
										</div>
										
										<div style="float:left;width:280px;margin-left:2px">
										<fieldset>
										<legend align="left">Datos de Atencion</legend>
												<div style="float:left;width:140px">
													<div style="clear:both">
													<div style="float:left;width:105px;margin-left:12px">						
													<label for="message">F. Financiamiento : </label>
													</div>
													<div style="float:left;margin-bottom:7px;margin-left:12px">
													<input type="text"  size="15" class="FuenteFinanciamiento"   readonly="readonly">
													</div>
													</div>
													
													
													<div style="clear:both">
													<div style="float:left;width:95px;margin-left:12px">						
													<label for="message">Fecha Ingreso : </label>
													</div>
													<div style="float:left;margin-bottom:10px;margin-left:12px">
													<input type="text"  size="15" class="FechaIngreso"   readonly="readonly">
													</div>
													</div>
												</div>
												
												
												<div style="float:left;margin-left:15px;width:100px">
													<div style="clear:both">
													<div style="float:left;width:50px;margin-left:12px;margin-top:1px">						
													<label for="message">Cuenta: </label>
													</div>
													<div style="float:left;margin-bottom:7px;margin-left:12px">
													<input type="text"  size="6" class="IdCuentaAtencion"   readonly="readonly">
													</div>
													</div>

													<div style="clear:both">
													<div style="float:left;width:50px;margin-left:12px">						
													<label for="message">H.Ingreso: </label>
													</div>
													<div style="float:left;margin-bottom:2px;margin-left:12px">
													<input type="text"  size="6" class="HoraIngreso"   readonly="readonly">
													</div>
													</div>							
												</div>
												
												
												<div style="clear:both">
													<div style="float:left;width:95px;margin-left:12px;margin-bottom:3px">						
													<label for="message">Servicio : </label>
													</div>
													<div style="float:left;margin-bottom:3px;margin-left:12px">
													<input type="text"  size="30" class="Servicio"   readonly="readonly">
													</div>
												</div>
										</fieldset>
										</div>
									</div>
									
									<div style="width:935px;clear:both;margin-bottom:65px">	
											<div style="float:left;width:790px">					
											<fieldset>
											<legend align="left">Funciones Vitales Actuales</legend>
													<div style="clear:both;padding-top:2px">
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">FR :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_FR">
														</div>								
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">FC :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_FC">
														</div>								
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">PAS :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_PAS">
														</div>
														<div style="float:left;width:40px;margin-left:10px">						
														<label for="message">PAD :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_PAD">
														</div>
														<div style="float:left;width:60px;margin-left:10px">						
														<label for="message">Glasgow :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_Glasgon">
														</div>
														<div style="float:left;width:30px;margin-left:10px">						
														<label for="message">T&#176; :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_Temperatura">
														</div>
														<div style="float:left;width:60px;margin-left:10px">						
														<label for="message">Peso <span style="font-size:10px  !important;font-weight:bold">(kg)</span>  :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="3" class="FuncionV_Peso">
														</div>
													</div>
											</fieldset>
											</div>
											
											
											
											<div style="float:left;width:140px">					
											<fieldset>
											<legend align="left">Datos Adicionales</legend>
													<div style="clear:both;padding-top:2px">
														<div style="float:left;width:50px;margin-left:1px">						
														<label for="message">Cama :</label>
														</div>
														<div style="float:left;">
															<input type="text"  size="4" class="Cama">
														</div>
													</div>
											</fieldset>
											</div>
									</div>
									


						<div id="contenido_modificar" class="easyui-tabs" style="width:925px;height:410px;">
									
									<div title="1.- Diag. y Otros" style="padding:25px;background:#FAFAFA" >
									<div style="height:50px">		

												<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Anteced. Patol&oacute;gico :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="text"  size="5" class="codigo_dx_Antecedentes">
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="45" class="dx_Antecedentes">
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="hidden"  size="45" class="IdDiagnosticoPatologico">
													</div>
													<div style="float:left;margin-bottom:20px;margin-left:5px">
														<a href="javascript:void(0)" class="easyui-linkbutton"  class="Ventana_Diagnosticos_Antecedentes">...</a>
													</div>
												</div>
												<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">DX Infeccioso :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="text"  size="5" class="codigo_dx_infeccioso">
													</div>
													<div style="float:left;margin-bottom:20px">
													<input type="text"  size="45" class="dx_infeccioso">
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px">
													<input type="hidden"  size="5" class="IdDiagnosticoInfeccioso">
													</div>
													<div style="float:left;margin-bottom:20px;margin-left:5px">
														<a href="javascript:void(0)" class="easyui-linkbutton"  class="Ventana_Diagnosticos_Infeccioso">...</a>
													</div>
												</div>
												<div style="clear:both">
													<div style="float:left;width:120px">						
													<label for="message">Otro Diagn&oacute;stico :</label>
													</div>
													<div style="float:left;margin-bottom:10px;margin-right:5px">
													<input type="text"  size="5" class="codigo_dx_otro">
													</div>
													<div style="float:left;margin-bottom:10px">
													<input type="text"  size="45" class="dx_otro">
													</div>
													<div style="float:left;margin-bottom:10px;margin-right:5px">
													<input type="hidden"  size="45" class="IdDiagnosticoOtro">
													</div>
													<div style="float:left;margin-bottom:10px;margin-left:5px">
														<a href="javascript:void(0)" class="easyui-linkbutton"  class="Ventana_Diagnosticos_Otro">...</a>
													</div>
												</div>
												<div style="clear:both;margin-bottom:25px">
													<div style="float:left;">
														<div style="float:left;width:100px">						
														<label for="message">Infecci&oacute;n IntraHospitalaria Actual :</label>
														</div>
														<div style="float:left;margin-bottom:10px;margin-left:35px;">
														<div class="maxl">
														  <label class="radio inline"> 
															   <input type="radio" name="Infeccion_IntraHospitalaria" class="Infeccion_SI"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="1">
															  <span> Si </span> 
														   </label>
														  <label class="radio inline"> 
																<input type="radio" name="Infeccion_IntraHospitalaria" class="Infeccion_NO"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="0" checked>
															  <span>No </span> 
														  </label>
														</div>
														</div>
													</div>
												</div>
												<div style="clear:both;margin-bottom:25px">
													<div class="Contenedor_Infeccion" style="display:none;float:left;width:850px;height:30px;margin-bottom:40px;">
														<div style="clear:both;margin-bottom:5px">
														<div style="float:left;width:100px;margin-top:50px;">							
														<label for="message">Descripci&oacute;n IntraHospitalaria :</label>
														</div>
														<div style="float:left;margin-right:5px;margin-left:20px">
														<textarea rows="3" cols="50"  name="Descripcion_IntraHospitalaria_Modificar">
														</textarea> 
														</div>
														</div>
													</div>	
												</div>

									</div>
									</div>
	
									<div title="2.- Examenes de Laboratorio" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
										<table id="Examenes_Antimicrobianos"></table>
									</div>

									<div title="3.- Antimicrobianos Previos" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
										<div style="height:325px">
										<table id="PreviosAntimicrobianos"></table>	
										</div>
									</div>
									
									<div title="4.- Antimicrobianos Solicitados" style="padding:0px 25px 25px 25px;background:#FAFAFA;">
											<div style="float:left;margin-bottom:0px;margin-left:5px">
											<p>
											<a href="javascript:void(0)"  id="Ventana_Sugerencia_Antimicrobiano"  onclick="$('#dlg').dialog('open')"><strong>¿ Desea Agregar un Antimicrobiano Alternativo ?</strong></a>
											</p>
											</div>
											<div style="height:280px;clear:both">
												<table id="DetalleAntimicrobianos"></table>	
											</div>
									</div>	
									
									<div title="5.- Justificaci&oacute;n" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
									<div style="height:325px">
										<div style="clear:both">
													<div style="float:left;width:100px;margin-top:15px">						
													<label for="message">Justificaci&oacute;n :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px;margin-top:15px;margin-left:5px">
													<textarea rows="5" cols="80"   name="Justificacion_Modificar"  style="width: 700px; height: 210px;">
													</textarea> 
													</div>
										</div>
									</div>
									</div>	
									
									<div title="6.- Evaluaci&oacute;n" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
									<div style="height:325px">
										<div style="clear:both">
													<div style="float:left;width:100px;margin-top:15px">						
													<label for="message">Evaluaci&oacute;n :</label>
													</div>
													<div style="float:left;margin-bottom:20px;margin-right:5px;margin-top:15px;margin-left:5px">
													<textarea rows="5" cols="80" name="Evaluacion_Modificar"  style="width: 700px; height: 210px;">
													</textarea> 
													</div>
										</div>
									</div>
									</div>
						</div>	
						</div>		
						</div>
														
						<div data-options="region:'south',split:true" style="width:250px;height:9%;padding-left:70px;padding-top:1px;">
									<table cellpadding="5">
									<tr>
									<td><a href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:110px;height: 40px;" id="Guardar_Cambios_Modificar"   onclick="Guardar_Cambios_Modificar()"  >GUARDAR</a></td>
									<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:110px;height: 40px;" id="Cerrar_Contenedor_Modificar" onclick="Cerrar_Contenedor_Modificar()"  >SALIR</a></td>
									</tr>
									</table>
						</div>

						
						
						<div id="dlg" class="easyui-dialog" title="Agregar Antimicrobiano Alternativo" data-options="iconCls:'icon-save'" style="width:480px;height:250px;padding:10px">
								<div style="float:left;width:100px;margin-bottom:10px"><strong>Nombre :</strong></div>
								
								<div style="float:left;margin-bottom:10px;margin-right:5px">
								<input type="hidden"  id="Id_Antimicrobiano_Sugerido"    class="antimicrobiano_sugerido" size="5">
								<input type="text"  id="Codigo_Antimicrobiano_Sugerido"  class="antimicrobiano_sugerido" size="8"></div>
								<div style="float:left;margin-bottom:10px"><input type="text"  id="Descripcion_Antimicrobiano_Sugerido" class="antimicrobiano_sugerido" size="35"></div>
								<div style="float:left;margin-bottom:10px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"  id="Agregar_Sugerencia_Antimicrobiano"></a>
							    </div>
								<div  style="float:left;width:270px">
									<div style="float:left;clear:both;;width:100px;margin-bottom:10px"><strong>Dosis :</strong></div><div style="float:left;margin-bottom:10px">     <input type="text"   class="antimicrobiano_sugerido"   id="Dosis_Sugerencia" size="10"  onkeyup="Calculo_Antimicrobiano();"></div>
									
									<div style="float:left;clear:both;;width:100px;margin-bottom:10px"><strong>Frecuencia :</strong></div><div style="float:left;margin-bottom:10px"><input type="text"  class="antimicrobiano_sugerido"  id="Frecuencia_Sugerencia" size="10"  onkeyup="Calculo_Antimicrobiano();"></div>
									
									<div style="float:left;clear:both;;width:100px;margin-bottom:10px"><strong>Dias :</strong></div><div style="float:left;margin-bottom:10px">      <input type="text"   class="antimicrobiano_sugerido"  id="Dias_Sugerencia" size="10"  onkeyup="Calculo_Antimicrobiano();"></div>
									
									<div style="clear:both;">
									<table cellpadding="5">
									<tr>
									<td><a href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:110px;height: 40px;" id="Guardar_Cambios_Sugerencia"   onclick="Guardar_Cambios_Sugerencia()"  >GUARDAR</a></td>
									<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:110px;height: 40px;" id="Cerrar_Contenedor_Sugerencia" onclick="Cerrar_Contenedor_Sugerencia()"  >SALIR</a></td>
									</tr>
									</table>
									</div>
								</div>
								<div  style="float:left;width:120px;height:150px;">
								<br>
									<div id="contenido_total">
									<span id="spTotal"></span>
									</div>
								</div> 	
					   </div>
					   
					   
					   	<div id="Contenedor_Antimicrobiano_Sugerencia" class="easyui-dialog" title="Lista de Antimicrobianos Controlados" data-options="iconCls:'icon-save'" 
							style="width:620px;height:440px;padding:10px">
							<table id="Lista_Antimicrobiano_Sugerencia" class="easyui-datagrid"  style="width:585px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAntimicrobianosControladosGrilla',method:'get',
							rowStyler: function(index,row){
							if (row.Stock=='0'){
							return 'color:#ff3333;';
							}
							},
							onSelect:function(index,row){
									$('#Contenedor_Antimicrobiano_Sugerencia').dialog('close');
									$('#Id_Antimicrobiano_Sugerido').val(row.IdProducto);
									$('#Codigo_Antimicrobiano_Sugerido').val(row.Codigo);
									$('#Descripcion_Antimicrobiano_Sugerido').val(row.Nombre);
											
							}">
							<thead>
							<tr>
							<th data-options="field:'Codigo',width:80">Codigo</th>
							<th data-options="field:'Stock',width:80">Stock</th>
							<th data-options="field:'Nombre',width:423">Nombre de Producto</th>
							</tr>
							</thead>
							</table>
						</div>
					   