    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Antimicrobianos</title>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<link rel="stylesheet" href="../../MVC_Complemento/css/antimicrobianos_estilo.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/js/jquery.numeric.js"></script>
		<script src="../../MVC_Complemento/tinymce/tinymce.min.js"></script>
		<script src="../../MVC_Complemento/tinymce/langs/es.js"></script>
		<script>tinymce.init({ selector:'textarea' ,
							   language : 'es', 
							   menubar: false,
							   plugins: [
								'advlist autolink lists  charmap print preview anchor textcolor',
								'searchreplace visualblocks code fullscreen',
								'insertdatetime media table contextmenu paste code help'],
							   branding: false});
		</script>
		<style>
			html,body { 
				padding: 0px;
				margin: 0px;
				height: 100%;
				font-family: 'Helvetica'; 			
			}

			.mayus>input{
				text-transform: capitalize;
			}		
			.letras_pequeñas{
				font-size:9px;
				font-weight:bold;
				color:red;
			}
			
			.letras_pequeñas_2{
				font-size:8px;
				font-weight:bold;
				color:red;
				
			}
			
			.error{
				background-color: #BC1010;
				border-radius: 4px 4px 4px 4px;
				color: white;
				font-weight: bold;
				margin-left: 16px;
				margin-top: 6px;
				padding: 6px 12px;
				position: absolute;
			}


			.error:before{
				border-color: transparent #BC1010 transparent transparent;
				border-style: solid;
				border-width: 6px 8px;
				content: "";
				display: block;
				height: 0;
				left: -16px;
				position: absolute;
				top: 8px;
				width: 0;
			}
			.result_fail{
				background: none repeat scroll 0 0 #BC1010;
				border-radius: 20px 20px 20px 20px;
				color: white;
				font-weight: bold;
				padding: 10px 20px;
				text-align: center;
			}
			.result_ok{
				background: none repeat scroll 0 0 #1EA700;
				border-radius: 20px 20px 20px 20px;
				color: white;
				font-weight: bold;
				padding: 10px 20px;
				text-align: center;
			}

			fieldset, legend ,#contenido{
				border: 1px solid #ddd;
				background-color: #eee;
				-moz-border-radius-topleft: 5px;
				border-top-left-radius: 5px;
				-moz-border-radius-topright: 5px;
				border-top-right-radius: 5px;
				margin-top:2px;
				}
			legend {
				font-weight: normal;
				font-size: 1 em;
				text-shadow: #fff 1px 1px 1px; 
				font-weight: bold;
				color :#777;
				}
			fieldset ,#contenido{
				background-color: #f7f7f7;
				-moz-border-radius-bottomleft: 5px;
				border-bottom-left-radius: 5px;
				-moz-border-radius-bottomright: 5px;
				border-bottom-right-radius: 5px; }			
			
			#contenido{
				margin:6px;
			}
		</style>
		
		
		<script type="text/javascript">
		function Mostrar_Contenido_Infeccion() {
			element = document.getElementById("Contenedor_Infeccion");
			check_Si = document.getElementById("Infeccion_SI");
			check_No = document.getElementById("Infeccion_NO");
			if (check_Si.checked) {
				element.style.display='block';
			}
			if (check_No.checked) {
				element.style.display='none';
			}
		}
		</script>
		
		
		<script type="text/javascript">
		
		// Al Inicializar
		$(document).ready(function(){
			//Poner por defecto foco en busqueda de Historia Clinica
			//$( "#b_HistoriaClinica" ).focus();
			
			//Formato de Busqueda
			$('#b_Dni').numeric();
			$('#b_HistoriaClinica').numeric();
			
			//Funciones Vitales  
			$('#FuncionV_FR,#FuncionV_FC,#FuncionV_PAS,#FuncionV_PAD,#FuncionV_Glasgon,#FuncionV_Peso,#FuncionV_Temperatura').numeric(",");

			//Antimicrobianos Previos
			$('#AntimicrobianoDia_I').numeric(",");
			$('#Dias').numeric();
			
			//Antimicrobianos Solicitados
			$('#Dosis_Antimicrobiano_Solicitado').numeric(","); 
			$('#Frecuencia_Antimicrobiano_Solicitado').numeric(); 
			
			//Limpiar datos de Busqueda
			$("#Limpiar_Busqueda").click(function(){
				$("#b_ApellidoPaterno").val("");
				$("#b_ApellidoMaterno").val("");
				$("#b_HistoriaClinica").val("");
				$("#b_Dni").val("");
			});


			$("#AntimicrobianoDia_I,#FuncionV_FR,#FuncionV_FC,#FuncionV_PAS,#FuncionV_PAD,#FuncionV_Glasgon,#FuncionV_Peso,#FuncionV_Temperatura").bind('blur keyup', function(){  
				if ($(this).val() != "") {  			
					$('.error').fadeOut();
					return false;  
				}  
			});	
		});
		</script>
		

		
		<script type="text/javascript">
		
		$(document).ready(function(){
			
			    //Focus en la caja de Texto
				$("#b_Dni,#b_HistoriaClinica,#b_ApellidoMaterno,#b_ApellidoPaterno").focus(function(){
					//Al hacer Enter ejecutara el buscador de Paciente
					$(document).keypress(function(e) {
						if(e.which == 13) {
						if($("#b_Dni").val().length!=0 || $("#b_HistoriaClinica").val().length!=0)
							{
								Buscar_Paciente();
							}
						}
					});
					
				});

				
				
				//Al hacer Click en el Boton de Busqueda ejecutara el buscador de Paciente
				$('#Busqueda_Paciente').click(function() {
					Buscar_Paciente();			
				});	

				$("#Generar_Formulario_Antimicrobiano").click(function(){
					
							$(".error").fadeOut().remove();
							if ($("#IdPaciente").val() == ""  ||   $("#IdCuentaAtencion").val() == "") {
							$.messager.alert('SIGESA','Por favor Seleccione Paciente y Atención');					
							return false;  
							} 

							var IdDiagnosticoInfeccioso=$("#IdDiagnosticoInfeccioso").val();
							var IdDiagnosticoOtro=$("#IdDiagnosticoOtro").val();
							var IdDiagnosticoPatologico=$("#IdDiagnosticoPatologico").val();
							var Diag1=IdDiagnosticoInfeccioso.length;
							var Diag2=IdDiagnosticoOtro.length;
							var Diag3=IdDiagnosticoPatologico.length;
							var Contador_Diagnosticos=Diag1+Diag2+Diag3;
	
							// Valida que se Ingrese Funcion Vital

							if ($("#FuncionV_FR").val() == "") {  
									$("#FuncionV_FR").focus().after('<span class="error">Ingrese F. Vital FR</span>');  
									return false;  
							} 
							
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_FC").val() == "") {  
									$("#FuncionV_FC").focus().after('<span class="error">Ingrese F. Vital FC</span>');  
									return false;  
							} 

										
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_PAS").val() == "") {  
									$("#FuncionV_PAS").focus().after('<span class="error">Ingrese F. Vital PAS</span>');  
									return false;  
							} 
							
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_PAD").val() == "") {  
									$("#FuncionV_PAD").focus().after('<span class="error">Ingrese F. Vital PAD</span>');  
									return false;   
							} 
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_Glasgon").val() == "") {  
									$("#FuncionV_Glasgon").focus().after('<span class="error">Ingrese F. Vital Glasgon</span>');  
									return false;  
							} 
							
							
						    // Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_Temperatura").val() == "") {  
									$("#FuncionV_Temperatura").focus().after('<span class="error">Ingrese F. Vital Temperatura</span>');  
									return false;  
							} 
							
							
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_Peso").val() == "") {  
									$("#FuncionV_Peso").focus().after('<span class="error">Ingrese F. Vital Peso</span>');  
									return false;  
							} 

							
							// Validacion de Diagnosticos
							if (Contador_Diagnosticos == 0) {  
								$.messager.alert('SIGESA','Por favor ingrese minimo un diagnostico');
								return false;  				
							} 
							
							
							// Valida que se Ingrese Antimicrobiano Previos
							if(Previos.length==0)
							{
							$.messager.alert('SIGESA','Por favor ingrese minimo un Antimicrobiano Previos');
							return false; 	
							}
							
							
							// Valida que se Ingrese Antimicrobiano Solicitados
							if(solicitados.length==0)
							{
							$.messager.alert('SIGESA','Por favor ingrese minimo un Antimicrobiano Controlado');
							return false; 	
							}
							
							// Valida una Justificacion 
							if (tinymce.get('Justificacion').getContent() == "") {  
							$.messager.alert('SIGESA','Por favor ingrese una Justificación');
							return false;  
							} 
							

							var FechaRegistro=$("#FechaRegistro").val(); 
							var IdPaciente=$("#IdPaciente").val(); 
							var IdCuentaAtencion=$("#IdCuentaAtencion").val(); 
							var IdDiagnosticoPatologico=$("#IdDiagnosticoPatologico").val(); 	
							var IdDiagnosticoInfeccioso=$("#IdDiagnosticoInfeccioso").val(); 
							var IdDiagnosticoOtro=$("#IdDiagnosticoOtro").val(); 
							var Infeccion_IntraHospitalaria=$('input:radio[name=Infeccion_IntraHospitalaria]:checked').val();
							var Descripcion_IntraHospitalaria =tinymce.get('Descripcion_IntraHospitalaria').getContent();
							var Justificacion =tinymce.get('Justificacion').getContent();
							var Estado='1';
							
							$.ajax({
							url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Cabecera_Antimicrobiano',
							type: 'POST',
							dataType: 'json',
							data: 
							{
							"FechaRegistro" : FechaRegistro,
							"IdPaciente" : IdPaciente,
							"IdCuentaAtencion" : IdCuentaAtencion,
							"IdDiagnosticoPatologico" : IdDiagnosticoPatologico,
							"IdDiagnosticoInfeccioso" : IdDiagnosticoInfeccioso,
							"IdDiagnosticoOtro" : IdDiagnosticoOtro,
							"Infeccion_IntraHospitalaria" : Infeccion_IntraHospitalaria,
							"Descripcion_IntraHospitalaria" : Descripcion_IntraHospitalaria,
							"Justificacion" : Justificacion,
							"Estado" : Estado
							},
							success: function(data)
							{
								
								/*Agregar Datos  */
								Agregar_Funciones_Vitales(data);
								Agregar_Previos_Antimicrobiano(data);
								Agregar_Detalle_Antimicrobiano(data);	
								$.messager.alert('SIGESA','Solicitud Ingresada Satisfactoriamente');
								NuevoRegistro();							
							}
						});
										
				});

				
				// Agregar Previos Antimicrobiano
				function Agregar_Previos_Antimicrobiano(IdCabeceraAntimicrobianoAntimicrobiano){
				var rows=$('#AntimicrobianoPrevio').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var IdCabeceraAntimicrobiano=row.IdCabeceraAntimicrobiano;
				var IdAntimicrobiano=row.IdAntimicrobiano;
				var Dias=row.Dias;
				$.ajax({
					url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Previo_Antimicrobiano',
					type: 'POST',
					dataType: 'json',
					data: {
					IdCabeceraAntimicrobiano:IdCabeceraAntimicrobianoAntimicrobiano,
					IdAntimicrobiano:IdAntimicrobiano,
					Dias   : Dias
					}
					});
					}
				}

				// Agregar Detalle Antimicrobiano
				function Agregar_Detalle_Antimicrobiano(IdCabeceraAntimicrobianoAntimicrobiano){
				var rows=$('#AntimicrobianoSolicitados').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var Codigo=row.Codigo;
				var Nombre=row.Nombre;
				var Dosis=row.Dosis;
				var Frecuencia=row.Frecuencia;
				var IdAntimicrobiano=row.IdAntimicrobiano;
				var IdCabeceraAntimicrobiano=row.IdCabeceraAntimicrobiano;
				$.ajax({
					url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Detalle_Antimicrobiano',
					type: 'POST',
					dataType: 'json',
					data: {
					IdCabeceraAntimicrobiano:IdCabeceraAntimicrobianoAntimicrobiano,
					IdAntimicrobiano:IdAntimicrobiano,
					Frecuencia:Frecuencia,
					Dosis  :Dosis,
					Dias   : '',
					Total  : '',
					Estado : '1'
					}
					});
					}
				}


				// Agregar Funciones Vitales
				function Agregar_Funciones_Vitales(IdCabeceraAntimicrobianoAntimicrobiano){
					var FuncionV_FR=$("#FuncionV_FR").val(); 
					var FuncionV_FC=$("#FuncionV_FC").val(); 
					var FuncionV_PAS=$("#FuncionV_PAS").val(); 
					var FuncionV_PAD=$("#FuncionV_PAD").val(); 
					var FuncionV_Glasgon=$("#FuncionV_Glasgon").val(); 
					var FuncionV_Peso=$("#FuncionV_Peso").val(); 
					var FuncionV_Temperatura=$("#FuncionV_Temperatura").val(); 
					$.ajax({
					url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Funciones_Vitales',
					type: 'POST',
					dataType: 'json',
					data: 
					{
					"Id_Formulario_Antimicrobiano" :IdCabeceraAntimicrobianoAntimicrobiano,	
					"FuncionV_FR" : FuncionV_FR,
					"FuncionV_FC" : FuncionV_FC,
					"FuncionV_PAS" : FuncionV_PAS,
					"FuncionV_PAD" : FuncionV_PAD,
					"FuncionV_Glasgon" : FuncionV_Glasgon,
					"FuncionV_Peso" : FuncionV_Peso,
					"FuncionV_Temperatura" : FuncionV_Temperatura
					},
					success: function(data)
					{
					$("#FuncionV_FR").val("");
					$("#FuncionV_FC").val("");
					$("#FuncionV_PAS").val("");
					$("#FuncionV_PAD").val("");
					$("#FuncionV_Glasgon").val("");
					$("#FuncionV_Peso").val("");
					$("#FuncionV_Temperatura").val("");
					}
					});		
				}

				function NuevoRegistro()
				{	
				
					//Limpia las cajas de Tipo Texto
					$(":text").each(function(){	
							$($(this)).val('');
					});
					
					//Descripcion_IntraHospitalaria
					tinymce.get('Descripcion_IntraHospitalaria').setContent('');
								
					//Poner por defecto en blanco la Justificacion
					tinymce.get('Justificacion').setContent('');
	
								
					// Limpiar Datagrid de EasyUI
					$('#AntimicrobianoSolicitados').datagrid('loadData',[]);
					$('#AntimicrobianoPrevio').datagrid('loadData',[]);

					
					// Limpiar Array
					solicitados.splice(0, solicitados.length);		
					Previos.splice(0, Previos.length);	
					
				}
				
				
			    /* No Cargara por defecto las Ventanas  */
				
				$('#Lista_Pacientes').dialog('close');
				$('#Lista_Atenciones').dialog('close');
				$('#Lista_Diagnosticos_II').dialog('close');
				$('#Lista_Diagnosticos_III').dialog('close');
				$('#Lista_Diagnosticos_IV').dialog('close');
				$('#Antimicrobianos_I').dialog('close');
				$('#Contenedor_Antimicrobiano_Solicitado_I').dialog('close');


				
				/* Da Formato correcto al easyui  */
				$('.easyui-datebox').datebox({
					formatter : function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
					},
					parser : function(s){

						if (!s) return new Date();
						var ss = s.split('-');
						var y = parseInt(ss[2],10);
						var m = parseInt(ss[1],10);
						var d = parseInt(ss[0],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d)
						} else {
							return new Date();
						}
					}

				});	

				
				$('#Ventana_Diagnosticos_Infeccioso').click(function() {
					$('#Lista_Diagnosticos_II').dialog('open');
				});

				$('#Ventana_Diagnosticos_Otro').click(function() {
					$('#Lista_Diagnosticos_III').dialog('open');
				});
				
				
				$('#Ventana_Diagnosticos_Antecedentes').click(function() {
					$('#Lista_Diagnosticos_IV').dialog('open');
				});
				
				
				
				
				function Buscar_Paciente(){
					$("#b_Dni,#b_HistoriaClinica,#b_ApellidoMaterno,#b_ApellidoPaterno").blur();
					var NroHistoriaClinica=$("#b_HistoriaClinica").val();
					var NroDocumento=$("#b_Dni").val();
					var ApellidoPaterno=$("#b_ApellidoPaterno").val();
					var ApellidoMaterno=$("#b_ApellidoMaterno").val();
					var n1=NroHistoriaClinica.length;
					var n2=NroDocumento.length;
					var n3=ApellidoPaterno.length;
					var n4=ApellidoMaterno.length;
					var total=0;
					total=n1+n2+n3+n4;
					if(total==0)
					{
					$.messager.alert('SIGESA','Por favor Ingrese alguno de los Filtros <br>(Ap. Paterno,Ap. Materno,DNI o Nro Historia Clinica)');							
					return false;	
					}
					if(total>0 && n2!=8 && n2>0)
					{
					$.messager.alert('SIGESA','Ingrese numero de DNI correcto');							
					return false;	
					}
					
					$('#Lista_Pacientes').dialog('open');
						var dg =$('#Lista').datagrid({
											iconCls:'icon-edit',
											singleSelect:true,
											rownumbers:true,
											rowtexts:true,
											pagination: true,
											remoteSort:false,
											multiSort:true,
											url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarPaciente&NroHistoriaClinica='+NroHistoriaClinica+'&NroDocumento='+NroDocumento+'&ApellidoPaterno='+ApellidoPaterno+'&ApellidoMaterno='+ApellidoMaterno,
											columns:[[
												{field:'ApellidoPaterno',title:'Apellido Paterno',width:130,sortable:true},
												{field:'ApellidoMaterno',title:'Apellido Materno',width:130,sortable:true},
												{field:'PrimerNombre',title:'Primer Nombre',width:120,sortable:true},
												{field:'SegundoNombre',title:'Segundo Nombre',width:120,sortable:true},
												{field:'NroDocumento',title:'DNI',width:120}
											]],
											onSelect:function(index,row){
											$('#Lista_Pacientes').dialog('close');
											$('#ApellidoPaterno').val(row.ApellidoPaterno);
											$('#ApellidoMaterno').val(row.ApellidoMaterno);
											$('#PrimerNombre').val(row.PrimerNombre);
											$('#SegundoNombre').val(row.SegundoNombre);
											$('#NroDocumento').val(row.NroDocumento);
											$('#IdPaciente').val(row.IdPaciente);
											$('#FechaNacimiento').val(row.FechaNacimiento);
											$('#Edad').val(row.Edad);
											$('#NroHistoriaClinica').val(row.NroHistoriaClinica);
											if(row.IdTipoSexo==1)
												{
												$("#Sexo_Masculino").prop("checked", true);												
												}
											else
												{
												$("#Sexo_Femenino").prop("checked", true);													
												}
												Ventana_Progresiva(row.IdPaciente);	
												//Limpiar Campos  
												//Datos de Busqueda
											   $("#b_Dni").val("");
											   $("#b_HistoriaClinica").val("");
											   $("#b_ApellidoPaterno").val("");
											   $("#b_ApellidoMaterno").val("");												
											}
						});	
						
					}

				
				 function Ventana_Progresiva(IdPaciente){
					var win = $.messager.progress({
						title:'Atenciones de Paciente',
						msg:'Cargando Atenciones...'
					});
					setTimeout(function(){
						$.messager.progress('close');
						Lista_Atenciones(IdPaciente);
					},600)
				}
				
				
				function  Lista_Atenciones(IdPaciente){
						$('#Lista_Atenciones').dialog('open');
						var dg =$('#Lista_Atenciones_Paciente').datagrid({
											iconCls:'icon-edit',
											singleSelect:true,
											rownumbers:true,
											rowtexts:true,
											pagination: true,
											url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAtencionesporIdPaciente&IdPaciente='+IdPaciente,
											columns:[[
												{field:'Nombre',title:'Servicio',width:160},
												{field:'TipoServicio',title:'Tipo de <br> Servicio',width:180},
												{field:'FechaIngreso',title:'Fecha de <br> Ingreso',width:75},
												{field:'HoraIngreso',title:'Hora de <br> Ingreso',width:60},
												{field:'IdCuentaAtencion',title:'Cuenta de <br> Atencion',width:70},
												{field:'FuenteFinanciamiento',title:'Fuente de <br>  Financiamiento',width:120},

											]],
											onSelect:function(index,row){
											$('#Lista_Atenciones').dialog('close');
											$('#FuenteFinanciamiento').val(row.FuenteFinanciamiento);
											$('#Servicio').val(row.Nombre);
											$('#FechaIngreso').val(row.FechaIngreso);
											$('#HoraIngreso').val(row.HoraIngreso);	
											$('#FechaIngreso').val(row.FechaIngreso);
											$('#IdCuentaAtencion').val(row.IdCuentaAtencion);	
											$('#Cama').val(row.Cama);
											}
				
						});
				}
				

				$('#Ventana_Antimicrobiano_Previo_I').click(function() {
					$('#Antimicrobianos_I').dialog('open');
				});
				
				
				$('#Ventana_Antimicrobiano_Solicitado_I').click(function() {
					$('#Contenedor_Antimicrobiano_Solicitado_I').dialog('open');
				});
				


			});
			

		
		</script>

		 <script type="text/javascript">	
			$(function(){
			
				var Lista_Pacientes = $('#Lista').datagrid();
				    Lista_Pacientes.datagrid('enableFilter');	

				var Lista_Atenciones = $('#Lista_Atenciones_Paciente').datagrid();
				Lista_Atenciones.datagrid('enableFilter');

					
				var Lista_Antimicrobianos_I = $('#Lista_Antimicrobianos_I').datagrid();
				    Lista_Antimicrobianos_I.datagrid('enableFilter');
					

				var Lista_Antimicrobiano_Solicitado_I = $('#Lista_Antimicrobiano_Solicitado_I').datagrid();
				    Lista_Antimicrobiano_Solicitado_I.datagrid('enableFilter');	

					
				var Lista_Diagnosticos_Infeccioso = $('#Lista_Diagnosticos_Infeccioso').datagrid();
				    Lista_Diagnosticos_Infeccioso.datagrid('enableFilter');
					
				var Lista_Diagnosticos_Otro = $('#Lista_Diagnosticos_Otro').datagrid();
				    Lista_Diagnosticos_Otro.datagrid('enableFilter');	

				var Lista_Diagnosticos_Antecedentes = $('#Lista_Diagnosticos_Antecedentes').datagrid();
				    Lista_Diagnosticos_Antecedentes.datagrid('enableFilter');
					
			 });	
			 
			 
		</script>
					
		
					
		<script type="text/javascript">
		
				$(document).ready(function(){
				var Contenedor = $('#campos'); 
				$(Contenedor).on('click', '.Remover', function(e){ 
					e.preventDefault();
					$(this).parent('div').remove(); 
				});
				});
		</script>
		

		<script type="text/javascript">
		var Previos = [];
		$(document).ready(function(){
		$("#Agregar_Anti_Previo").click(function(){
		if($("#IdAntimicrobianoPrevio_I").val().length==0 ||  $("#AntimicrobianoDia_I").val().length==0)
		{
			$.messager.alert('SIGESA','Por favor Ingrese Antimicrobiano Previo y Dias Respectivamente');	
			return false;  
		}
		for(var i = 0; i < Previos.length; i++) {
	      if(Previos[i].IdAntimicrobiano == $("#IdAntimicrobianoPrevio_I").val()) 
			{
			$.messager.alert('SIGESA','Antimicrobiano Previo ya ah sido Ingresado Anteriormente : <strong>'+$("#Antimicrobiano_Previo_I").val()+'</strong>');
			return false;  			
			}
		}
		var valueToPush = [];
		valueToPush["IdAntimicrobiano"] = $("#IdAntimicrobianoPrevio_I").val();
		valueToPush["Codigo"] = $("#Codigo_Antimicrobiano_Previo_I").val();
		valueToPush["Descripcion"] = $("#Antimicrobiano_Previo_I").val();
		valueToPush["Dias"] = $("#AntimicrobianoDia_I").val();
		
		//Agregar Valores a Arreglo
		Previos.push(valueToPush);
		
		// Se limpia los Datos  
		$("#Antimicrobiano_Previo_I,#Codigo_Antimicrobiano_Previo_I,#IdAntimicrobianoPrevio_I,#AntimicrobianoDia_I").val("");

		var dg = $('#AntimicrobianoPrevio').datagrid();
		dg.datagrid({
		fit: true,
		pagination: true,
		singleSelect: true,
		striped: true,
		rownumbers: true,
		fitColumns: true,
		data: Previos,
		columns: [[
		 {
		 field: 'Codigo',
		 width: 80,
		 title: 'Codigo'
		 }, 
		 {
		 field: 'Descripcion',
		 width: 350,
		 title: 'Descripcion'
		 }, 
		 {
		 field: 'Dias',
		 width: 20,
		 align: 'right',
		 title: 'Dias',
		 editor:{type:'numberbox',options:{precision:1}}
		 }
		 ]
		 ],
		 onSelect: function (rowIndex, rowData) {
				 $.messager.confirm('Confirmar','Estas seguro que quieres eliminar el Antimicrobiano <strong>'+rowData.Descripcion+'</strong> ?',function(r){
                if (r){
					$('#AntimicrobianoPrevio').datagrid('deleteRow', rowIndex);
                    }
                });
		 }
		 });
		 });
		 });
		 </script>
		 
		<script type="text/javascript">
				
		var solicitados = [];
		$(document).ready(function(){
		$("#Agregar_Antimicrobiano").click(function(){
		if($("#Dosis_Antimicrobiano_Solicitado").val().length==0 ||  $("#Frecuencia_Antimicrobiano_Solicitado").val().length==0 ||  $("#Dosis_Antimicrobiano_Solicitado").val().length==0  || $("#Id_Antimicrobiano_Solicitado").val().length==0    || $("#Descripcion_Antimicrobiano_Solicitado").val().length==0)
		{
			$.messager.alert('SIGESA','Por favor Ingrese Antimicrobiano Controlado ,Dosis y Frecuencia Respectivamente');	
			return false;  
		}
		for(var i = 0; i < solicitados.length; i++) {
	      if(solicitados[i].IdAntimicrobiano == $("#Id_Antimicrobiano_Solicitado").val()) 
			{
			$.messager.alert('SIGESA','Antimicrobiano Controlado ya ah sido Ingresado Anteriormente : <strong>'+$("#Descripcion_Antimicrobiano_Solicitado").val()+'</strong>');
			return false;  			
			}
		}
		
		var valueToPush = [];									
		valueToPush["IdAntimicrobiano"] = $("#Id_Antimicrobiano_Solicitado").val();
		valueToPush["Codigo"] = $("#Codigo_Antimicrobiano_Solicitado").val();
		valueToPush["Descripcion"] = $("#Descripcion_Antimicrobiano_Solicitado").val();
		valueToPush["Dosis"] = $("#Dosis_Antimicrobiano_Solicitado").val();
		valueToPush["Frecuencia"] = $("#Frecuencia_Antimicrobiano_Solicitado").val();
		solicitados.push(valueToPush);
		
		
		// Se limpia los Datos  
		$("#Id_Antimicrobiano_Solicitado,#Codigo_Antimicrobiano_Solicitado,#Descripcion_Antimicrobiano_Solicitado,#Dosis_Antimicrobiano_Solicitado,#Frecuencia_Antimicrobiano_Solicitado").val("");

		//Cargar Datagrid 
		var dg = $('#AntimicrobianoSolicitados').datagrid();
		dg.datagrid({
		fit: true,
		pagination: true,
		singleSelect: true,
		striped: true,
		rownumbers: true,
		fitColumns: true,
		data: solicitados,
		columns: [[
		 {
		 field: 'Codigo',
		 width: 50,
		 title: 'Codigo'
		 }, 
		 {
		 field: 'Descripcion',
		 width: 350,
		 title: 'Descripcion'
		 }, 
		 {
		 field: 'Dosis',
		 width: 30,
		 align: 'right',
		 title: 'Dosis'
		 }, 
		 {
		 field: 'Frecuencia',
		 width: 50,
		 align: 'right',
		 title: 'Frecuencia'
		 }
		 ]
		 ],
		 onSelect: function (rowIndex, rowData) {
				 $.messager.confirm('Confirmar','Estas seguro que quieres eliminar el Antimicrobiano <strong>'+rowData.Descripcion+'</strong> ?',function(r){
                if (r){
					$('#AntimicrobianoSolicitados').datagrid('deleteRow', rowIndex);
                    }
                });
		 }
		 });
		 });
		 });
		 </script>
		 
		 
    </head>
    <body>
	<div class="easyui-layout" style="width:100%;height:100%">
        <div data-options="region:'center',title:'FORMULARIO JUSTIFICACION DE ANTIMICROBIANOS SUJETO A CONTROL',iconCls:'icon-ok'" style="width:100%;height:0%">
			<div style="margin:5px;width:950px">
			<div style="width:950px">
			<fieldset>
			<legend align="left">Datos de Busqueda</legend>
					<div style="clear:both;padding-top:2px">
						<div style="float:left;width:40px">						
						<label for="message">Dni :</label>
						</div>
						<div style="float:left;">
							<input type="text"  size="7" id="b_Dni">
						</div>
						<div style="float:left;width:50px;margin-left:10px">						
						<label for="message">Historia Clinica :</label>
						</div>
						<div style="float:left;">
						<input type="text"  size="9" id="b_HistoriaClinica">
						</div>
						<div style="float:left;width:50px;;margin-left:10px">						
						<label for="message">Apellido Paterno :</label>
						</div>
						<div style="float:left;">
						<input type="text"  size="18" id="b_ApellidoPaterno">
						</div>
						<div style="float:left;width:55px;margin-left:10px">						
						<label for="message">Apellido Materno :</label>
						</div>
						<div style="float:left;">
						<input type="text"  size="17" id="b_ApellidoMaterno">
						</div>
						<div style="float:left;width:100px;margin-left:10px">						
						<a id="Busqueda_Paciente" href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:96px;height: 30px;">BUSCAR</a>
						</div>
						<div style="float:left;margin-bottom:0px">
						<a id="Limpiar_Busqueda" href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:96px;height: 30px;">LIMPIAR</a>
						</div>

					</div>
			</fieldset>
			</div>
					
			<div id="Lista_Pacientes" class="easyui-dialog" title="Lista de Pacientes" data-options="iconCls:'icon-save'" 
						style="width:700px;height:430px;padding:10px">
						<table id="Lista"></table>
			</div>

			
			
			<div id="Lista_Atenciones" class="easyui-dialog" title="Lista de Atenciones" data-options="iconCls:'icon-save'" 
						style="width:750px;height:450px;padding:10px">
						<table id="Lista_Atenciones_Paciente"></table>
			</div>

			
			
			
			<div style="clear:both;margin-top:10x">
				<div style="float:left;width:650px;">
				<fieldset>
				<legend align="left">Datos Personales de Paciente</legend>
						<div style="float:left; ">
							<div style="float:left;width:130px">						
							<label for="message">Primer Nombre :</label>
							</div>
							<div style="float:left;margin-bottom:15px">
							<input type="text"  size="20" id="PrimerNombre">
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Segundo Nombre :</label>
							</div>
							<div style="float:left;margin-bottom:15px">
							<input type="text"  size="20" id="SegundoNombre">
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Apellido Paterno :</label>
							</div>
							<div style="float:left;margin-bottom:15px">
							<input type="text"  size="20" id="ApellidoPaterno">
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Apellido Materno  :</label>
							</div>
							<div style="float:left;margin-bottom:1px">
							<input type="text"  size="20" id="ApellidoMaterno">
							</div>
							</div>
						</div>
						
						
						<div style="float:left;margin-left:15px">
							<div style="float:left;width:130px">						
							<label for="message">Fecha Nacimiento :</label>
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="20" id="FechaNacimiento">
							</div>
							<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Historia Clinica :</label>
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="20" id="NroHistoriaClinica">
							<input type="hidden"  size="10" id="IdPaciente">
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:60px">						
							<label for="message">Edad :</label>
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="8" id="Edad">
							</div>
							<div style="float:left;width:50px;margin-left:15px">						
							<label for="message">Sexo :</label>
							</div>
							<div style="float:left;margin-right:5px;">
							<label class="radio inline"> 
							<input type="radio" name="Sexo" id="Sexo_Femenino" value="Sexo_Femenino" >
							<span>Femenino </span>
							</label>
							<br>
							<label class="radio inline"> 
							<input type="radio" name="Sexo" id="Sexo_Masculino" value="Sexo_Masculino" >
							<span>Masculino </span>
							</label>
							</div>
							</div>
						</div>	
				</fieldset>
				</div>
				
				
				<div style="float:left;width:280px;margin-left:20px">
				<fieldset>
				<legend align="left">Datos de Atencion</legend>
						<div style="float:left;width:140px">
							<div style="clear:both">
							<div style="float:left;width:105px;margin-left:12px">						
							<label for="message">F. Financiamiento : </label>
							</div>
							<div style="float:left;margin-bottom:7px;margin-left:12px">
							<input type="text"  size="15" id="FuenteFinanciamiento"   readonly="readonly">
							</div>
							</div>
							
							
							<div style="clear:both">
							<div style="float:left;width:95px;margin-left:12px">						
							<label for="message">Fecha Ingreso : </label>
							</div>
							<div style="float:left;margin-bottom:10px;margin-left:12px">
							<input type="text"  size="15" id="FechaIngreso"   readonly="readonly">
							</div>
							</div>
						</div>
						
						
						<div style="float:left;margin-left:15px;width:100px">
							<div style="clear:both">
							<div style="float:left;width:50px;margin-left:12px;margin-top:1px">						
							<label for="message">Cuenta: </label>
							</div>
							<div style="float:left;margin-bottom:7px;margin-left:12px">
							<input type="text"  size="6" id="IdCuentaAtencion"   readonly="readonly">
							</div>
							</div>

							<div style="clear:both">
							<div style="float:left;width:50px;margin-left:12px">						
							<label for="message">H.Ingreso: </label>
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px">
							<input type="text"  size="6" id="HoraIngreso"   readonly="readonly">
							</div>
							</div>							
						</div>
						<div style="clear:both">
							<div style="float:left;width:95px;margin-left:12px;margin-bottom:3px">						
							<label for="message">Servicio : </label>
							</div>
							<div style="float:left;margin-bottom:3px;margin-left:12px">
							<input type="text"  size="30" id="Servicio"   readonly="readonly">
							</div>
						</div>
				</fieldset>
				</div>
			</div>
			
			<div style="width:100%;clear:both;margin-bottom:60px">	
					<div style="float:left;width:810px">					
					<fieldset>
					<legend align="left">Funciones Vitales Actuales</legend>
							<div style="clear:both;padding-top:2px">
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">FR :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_FR">
								</div>								
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">FC :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_FC">
								</div>								
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">PAS :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_PAS">
								</div>
								<div style="float:left;width:40px;margin-left:10px">						
								<label for="message">PAD :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_PAD">
								</div>
								<div style="float:left;width:70px;margin-left:10px">						
								<label for="message">Glasgow :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_Glasgon">
								</div>
								<div style="float:left;width:30px;margin-left:10px">						
								<label for="message">T° :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="3" id="FuncionV_Temperatura">
								</div>
								<div style="float:left;width:60px;margin-left:10px">						
								<label for="message">Peso <span style="font-size:10px  !important;font-weight:bold">(kg)</span>  :</label>
								</div>
								<div style="float:left;margin-left:10px">
									<input type="text"  size="3" id="FuncionV_Peso">
								</div>
							</div>
					</fieldset>
					</div>
					

					<div style="float:left;width:140px">					
					<fieldset>
					<legend align="left">Datos Adicionales</legend>
							<div style="clear:both;padding-top:2px">
								<div style="float:left;width:50px;margin-left:1px">						
								<label for="message">Cama :</label>
								</div>
								<div style="float:left;">
									<input type="text"  size="4" id="Cama"   readonly="readonly">
								</div>
							</div>
					</fieldset>
					</div>
			</div>
			
			
			
			</div>		
					

		<!-- Aqui se mandan los valores --> 
		<div id="contenido" class="easyui-tabs" style="width:948px;height:400px;clear:both">
			
			<div title="Diagnosticos y Otros" style="padding:25px;background:#FAFAFA" >
			<div style="height:50px">		
						<div id="Lista_Diagnosticos_IV" class="easyui-dialog" title="Lista de Diagnosticos" data-options="iconCls:'icon-save'" 
						style="width:580px;height:430px;padding:10px">
						
						<table id="Lista_Diagnosticos_Antecedentes" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDiagnosticosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Lista_Diagnosticos_IV').dialog('close');
										$('#codigo_dx_Antecedentes').val(row.CodigoCIE10);
										$('#dx_Antecedentes').val(row.Descripcion);
										$('#IdDiagnosticoPatologico').val(row.IdDiagnostico);
										}">
							<thead>
								<tr>
									<th data-options="field:'CodigoCIE10',width:80">Codigo</th>
									<th data-options="field:'Descripcion',width:600">Nombre de Diagnostico</th>
								</tr>
							</thead>
						</table>
						</div>
						
						
						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Anteced. Patológico :</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="codigo_dx_Antecedentes">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="45" id="dx_Antecedentes">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="45" id="IdDiagnosticoPatologico">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Diagnosticos_Antecedentes">...</a>
							</div>
						</div>
						


						<div id="Lista_Diagnosticos_II" class="easyui-dialog" title="Lista de Diagnosticos" data-options="iconCls:'icon-save'" 
						style="width:580px;height:430px;padding:10px">
						
						<table id="Lista_Diagnosticos_Infeccioso" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDiagnosticosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Lista_Diagnosticos_II').dialog('close');
										$('#codigo_dx_infeccioso').val(row.CodigoCIE10);
										$('#dx_infeccioso').val(row.Descripcion);
										$('#IdDiagnosticoInfeccioso').val(row.IdDiagnostico);
										}">
							<thead>
								<tr>
									<th data-options="field:'CodigoCIE10',width:80">Codigo</th>
									<th data-options="field:'Descripcion',width:600">Nombre de Diagnostico</th>
								</tr>
							</thead>
						</table>
						</div>
						
						
						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">DX Infeccioso :</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="codigo_dx_infeccioso">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="45" id="dx_infeccioso">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="5" id="IdDiagnosticoInfeccioso">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Diagnosticos_Infeccioso">...</a>
							</div>
						</div>
						
						
											
						
						<div id="Lista_Diagnosticos_III" class="easyui-dialog" title="Lista de Diagnosticos" data-options="iconCls:'icon-save'" 
						style="width:580px;height:430px;padding:10px">
						
						<table id="Lista_Diagnosticos_Otro" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDiagnosticosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Lista_Diagnosticos_III').dialog('close');
										$('#codigo_dx_otro').val(row.CodigoCIE10);
										$('#dx_otro').val(row.Descripcion);
										$('#IdDiagnosticoOtro').val(row.IdDiagnostico);
										}">
							<thead>
								<tr>
									<th data-options="field:'CodigoCIE10',width:80">Codigo</th>
									<th data-options="field:'Descripcion',width:600">Nombre de Diagnostico</th>
								</tr>
							</thead>
						</table>
						</div>
						
						
						
						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Otro Diagnóstico :</label>
							</div>
							<div style="float:left;margin-bottom:10px;margin-right:5px">
							<input type="text"  size="5" id="codigo_dx_otro">
							</div>
							<div style="float:left;margin-bottom:10px">
							<input type="text"  size="45" id="dx_otro">
							</div>
							<div style="float:left;margin-bottom:10px;margin-right:5px">
							<input type="hidden"  size="45" id="IdDiagnosticoOtro">
							</div>
							<div style="float:left;margin-bottom:10px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Diagnosticos_Otro">...</a>
							</div>
						</div>
						

						
						
						<div style="clear:both;margin-bottom:25px">
							<div style="float:left;">
								<div style="float:left;width:100px">						
								<label for="message">Infección IntraHospitalaria Actual :</label>
								</div>
								<div style="float:left;margin-bottom:10px;margin-left:35px;">
								<div class="maxl">
								  <label class="radio inline"> 
									   <input type="radio" name="Infeccion_IntraHospitalaria" id="Infeccion_SI"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="1">
									  <span> Si </span> 
								   </label>
								  <label class="radio inline"> 
										<input type="radio" name="Infeccion_IntraHospitalaria" id="Infeccion_NO"   onChange="javascript:Mostrar_Contenido_Infeccion()"  value="0" checked>
									  <span>No </span> 
								  </label>
								</div>
								</div>
							</div>
						</div>

						
						<div style="clear:both;margin-bottom:25px">
							<div id="Contenedor_Infeccion" style="display:none;float:left;width:850px;height:30px;margin-bottom:40px;">
								<div style="clear:both;margin-bottom:5px">
								<div style="float:left;width:100px;margin-top:50px;">						
								<label for="message">Descripción IntraHospitalaria :</label>
								</div>
								<div style="float:left;margin-right:5px;margin-left:30px">
								<textarea rows="3" cols="50" id="Descripcion_IntraHospitalaria" name="Descripcion_IntraHospitalaria">
								</textarea> 
								</div>
								</div>
							</div>	
						</div>

			</div>
			</div>

			<div title="Antimicrobianos Previos" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
			<div style="height:325px">
						<div id="Antimicrobianos_I" class="easyui-dialog" title="Lista de Antimicrobianos Previos" data-options="iconCls:'icon-save'" 
						style="width:590px;height:440px;padding:10px">
						<table id="Lista_Antimicrobianos_I" class="easyui-datagrid"  style="width:550px;height:380px"
							data-options="rowtexts:true,singleSelect:true,pagination: true,
							url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAntimicrobianosGrilla',method:'get',
								onSelect:function(index,row){
										$('#Antimicrobianos_I').dialog('close');
										$('#Codigo_Antimicrobiano_Previo_I').val(row.Codigo);
										$('#Antimicrobiano_Previo_I').val(row.Nombre);
										$('#IdAntimicrobianoPrevio_I').val(row.IdProducto);
										$( '#AntimicrobianoDia_I').focus();
										}">
							<thead>
								<tr>
									<th data-options="field:'Codigo',width:80">Codigo</th>
									<th data-options="field:'Nombre',width:467">Nombre de Producto</th>
								</tr>
							</thead>
						</table>
						</div>

						<div style="clear:both">
							<div style="float:left;width:130px">						
							<label for="message">Antimicrobianos<br>Previos :</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="Codigo_Antimicrobiano_Previo_I">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="60" id="Antimicrobiano_Previo_I">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="5" id="IdAntimicrobianoPrevio_I">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Antimicrobiano_Previo_I">...</a>
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:15px">
								<input type="text"  size="3" id="AntimicrobianoDia_I"   class="AntimicrobianoDia">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<span class="letras_pequeñas" >(Dias) </span>
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'"  id="Agregar_Anti_Previo">Agregar</a>
							</div>
						</div>
						
						<div style="clear:both;height:300px">
						<table id="AntimicrobianoPrevio"></table>
						</div>
						
			</div>
			</div>

						
			<div title="Antimicrobianos Controlados" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
			
			
			<div id="Contenedor_Antimicrobiano_Solicitado_I" class="easyui-dialog" title="Lista de Antimicrobianos Controlados" data-options="iconCls:'icon-save'" 
				style="width:620px;height:440px;padding:10px">
				<table id="Lista_Antimicrobiano_Solicitado_I" class="easyui-datagrid"  style="width:585px;height:380px"
				data-options="rowtexts:true,singleSelect:true,pagination: true,
				url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAntimicrobianosControladosGrilla',method:'get',
				rowStyler: function(index,row){
                if (row.Stock=='0'){
                return 'color:#ff3333;';
                }
                },
				onSelect:function(index,row){
						$('#Contenedor_Antimicrobiano_Solicitado_I').dialog('close');
						$('#Id_Antimicrobiano_Solicitado').val(row.IdProducto);
						$('#Codigo_Antimicrobiano_Solicitado').val(row.Codigo);
						$('#Descripcion_Antimicrobiano_Solicitado').val(row.Nombre);
						$('#Dosis_Antimicrobiano_Solicitado').focus();				
				}">
				<thead>
				<tr>
				<th data-options="field:'Codigo',width:80">Codigo</th>
				<th data-options="field:'Stock',width:80">Stock</th>
				<th data-options="field:'Nombre',width:423">Nombre de Producto</th>
				</tr>
				</thead>
				</table>
			</div>
						
						

			
			
			<div style="height:325px">
						<div style="clear:both;">
							<div style="float:left;width:100px">						
							<label for="message">Solicitar Antimicrobianos:</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="hidden"  size="5" id="Id_Antimicrobiano_Solicitado">
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px">
							<input type="text"  size="5" id="Codigo_Antimicrobiano_Solicitado" readonly="readonly">
							</div>
							<div style="float:left;margin-bottom:20px">
							<input type="text"  size="60" id="Descripcion_Antimicrobiano_Solicitado" readonly="readonly">
							</div>
							<div style="float:left;margin-bottom:20px;margin-left:5px">
								<a href="javascript:void(0)" class="easyui-linkbutton"  id="Ventana_Antimicrobiano_Solicitado_I">...</a>
							</div>
							
							
							<div style="float:left;margin-bottom:20px;margin-left:10px">
							<div style="position:relative;margin-left:4px;">
							<input type="text"  size="4" id="Dosis_Antimicrobiano_Solicitado">
							</div>
							<div style="position:relative;margin-top:2px;">
								<span class="letras_pequeñas" style="margin-left:15px;">DOSIS</span>
								<BR> 
								<span class="letras_pequeñas_2" style="margin-left:15px;">( mg ) </span>
								 
							</div>
							</div>

							<div style="float:left;margin-bottom:20px;margin-left:10px;">
							<div style="position:relative;margin-left:8px;">
							<input type="text"  size="4" id="Frecuencia_Antimicrobiano_Solicitado">
							</div>
							<div style="position:relative;margin-top:2px;">
							  <span class="letras_pequeñas" style="margin-left:10px;">FRECUENCIA</span>
								<BR>
							  <span class="letras_pequeñas_2" style="margin-left:20px;">( horas )</span>
							</div>
							</div>


							<div style="float:left;margin-bottom:20px;margin-left:9px;margin-top:-4px">
								<a href="#" class="easyui-linkbutton"   data-options="iconCls:'icon-add'" id="Agregar_Antimicrobiano">Agregar</a>
							</div>
							
						</div>
						
						<div style="clear:both;height:250px">
						<table id="AntimicrobianoSolicitados"></table>
						</div>
	
			</div>
			</div>
									
			<div title="Justificación" style="padding:17px 25px 25px 25px;background:#FAFAFA;">
						<div style="clear:both">
							<div style="float:left;width:100px;margin-top:15px">						
							<label for="message">Justificación :</label>
							</div>
							<div style="float:left;margin-bottom:20px;margin-right:5px;margin-top:15px;margin-left:5px">
							<textarea rows="5" cols="80" id="Justificacion"  name="Justificacion" style="width: 700px; height: 210px;">
							</textarea> 
							</div>
						</div>
			</div>
		</div>
		
		
		 <div data-options="region:'south',split:true" style="width:950px;height:11%;padding-left:70px;padding-top:10px;">
		  	<table cellpadding="5">
			<tr>
			<td><a href="#" class='easyui-linkbutton' data-options="iconCls:'icon-save'" style="width:96px;height: 50px;" id="Generar_Formulario_Antimicrobiano">GUARDAR</a></td>
			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" >SALIR</a></td>
			</tr>
			</table>
		</div>

		</div>
	</div>
    </body>
    </html>