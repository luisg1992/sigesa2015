    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Antimicrobianos</title>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

		</style>
    </head>
    <body>
	
		<script>
				$(document).ready(function() {
						$("#Generar_Reporte_por_Periodo").click(function(event) {	
						var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte').datebox('getValue');
						var Fecha_Final_Reporte= $('#Fecha_Final_Reporte').datebox('getValue');
							if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
							{
								$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
								return false;
							}
							else{
								
									if (Fecha_Inicial_Reporte < Fecha_Final_Reporte)
									{
									Highchart_Columnas_por_Periodo(Fecha_Inicial_Reporte,Fecha_Final_Reporte);	
									}
									else
									{
									$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
									}
							}	
					});	
				});				
		</script>
		<div class="easyui-layout" style="width:100%;height:100%;">
		
			<!--
            <div data-options="region:'north'" style="height:50px">
			</div>
			-->
			
            <div data-options="region:'south',split:true" style="height:30%;">
			</div>
			<!--
            <div data-options="region:'east',split:true" title="East" style="width:100px;">
			</div>
			-->
			
            <div data-options="region:'west',split:true,iconCls:'icon-ok'" title="Reporte de Antimicrobianos" style="width:20%;">

					<div title="Por Rango de Fechas" data-options="iconCls:'icon-ok'" style="overflow:auto;padding-left:45px;clear:both;padding-right:45px;padding-top:15px;">
								<p style="padding:10px"><strong>Reporte de Antimicrobianos Solicitados</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:90px" id="Generar_Reporte_por_Periodo">Generar Reporte</a>
								</div>									
						</div>
			</div>
			
            <div data-options="region:'center',title:'Lista',iconCls:'icon-ok'"  style="width:80%;">

					
            </div>
        </div>
    </body>
    </html>