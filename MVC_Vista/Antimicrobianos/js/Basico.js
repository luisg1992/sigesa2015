
		
		
	
		//Script para cargar  Antimicrobianos  Solicitados							
		function AntimicrobianoPrevios(IdCabeceraAntimicrobiano)
		{
		var dg =$('#PreviosAntimicrobianos').datagrid({
		iconCls:'icon-edit',
		singleSelect:true,
		rownumbers:true,
		pagination:true,
		pageSize:10,
		url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarPreviosAntimicrobiano&IdCabeceraAntimicrobiano='+IdCabeceraAntimicrobiano,
		columns:[[
		{field:'Codigo',title:'Codigo',width:80},
		{field:'Nombre',title:'Nombre',width:450},
		{field:'Dias',title:'Dias',width:60}]]
		});
		}
		

		
		//Script para Varilidar que cargue valor no null 
		function ValidarVacio(valor){
				if(valor==null)
				{
				return '';
				}
				else
				{
				return valor;	
				}	
		}
		
		
		
		
		
		
		//Script para Cargar Datos Personales de Paciente  - Modificar
		function Cargar_Datos_Personales(IdCabeceraAntimicrobiano){	
				$.post('../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php', {acc: 'MostrarDatosCabeceraAntimicrobiano',IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano}, function(htmlexterno){
				var Datos=JSON.parse(htmlexterno.replace("[", "").replace("]", ""));
				//Datos Personales de Paciente
				$('#IdCabecera').val(Datos['IdCabeceraAntimicrobiano']);
				$('.PrimerNombre').val(Datos['PrimerNombre']);
				$('.SegundoNombre').val(Datos['SegundoNombre']);
				$('.ApellidoPaterno').val(Datos['ApellidoPaterno']);
				$('.ApellidoMaterno').val(Datos['ApellidoMaterno']);
				$('.FechaNacimiento').val(Datos['FechaNacimiento']);
				$('.NroHistoriaClinica').val(Datos['NroHistoriaClinica']);
				$('.Edad').val(Datos['Edad']);
				if(Datos['IdTipoSexo']==1)
				{
				$(".Sexo_Masculino").prop("checked", true);												
				}
				else
				{
				$(".Sexo_Femenino").prop("checked", true);													
				}
				
				if(Datos['Infeccion_IntraHospitalaria']==0)
				{
				$(".Infeccion_NO").prop("checked", true);			
				$(".Contenedor_Infeccion").hide();
				}
				else
				{
				$(".Infeccion_SI").prop("checked", true);		
				$(".Contenedor_Infeccion").show();
				tinymce.get('Descripcion_IntraHospitalaria_Modificar').setContent(Datos['Descripcion_Intrahospitalaria']);				
				}
				//Datos de Atencion
				$('.FuenteFinanciamiento').val(Datos['Descripcion']);
				$('.IdCuentaAtencion').val(Datos['IdCuentaAtencion']);
				$('.FechaIngreso').val(Datos['FechaIngreso']);
				$('.HoraIngreso').val(Datos['HoraIngreso']);
				$('.Servicio').val(Datos['Nombre']);
				//Datos Adicionales
				$('.Cama').val(Datos['CamaPaciente']);
				//Datos Detalle
				var	Just = tinymce.get('Justificacion_Modificar').setContent(ValidarVacio(Datos['Justificacion']));
				var	Eva = tinymce.get('Evaluacion_Modificar').setContent(ValidarVacio(Datos['Evaluacion']));
				});
			}

			
			
			
			
		//Script para Cargar Datos Personales de Paciente  - Consultar 		
		function Cargar_Datos_Personales_Consultar(IdCabeceraAntimicrobiano){	
				$.post('../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php', {acc: 'MostrarDatosCabeceraAntimicrobiano',IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano}, function(htmlexterno){
				var Datos=JSON.parse(htmlexterno.replace("[", "").replace("]", ""));
				//Datos Personales de Paciente
				$('#IdCabecera').val(Datos['IdCabeceraAntimicrobiano']);
				$('.PrimerNombre').val(Datos['PrimerNombre']);
				$('.SegundoNombre').val(Datos['SegundoNombre']);
				$('.ApellidoPaterno').val(Datos['ApellidoPaterno']);
				$('.ApellidoMaterno').val(Datos['ApellidoMaterno']);
				$('.FechaNacimiento').val(Datos['FechaNacimiento']);
				$('.NroHistoriaClinica').val(Datos['NroHistoriaClinica']);
				$('.Edad').val(Datos['Edad']);
				if(Datos['IdTipoSexo']==1)
				{
				$("#Sexo_Masculino_Consultar").prop("checked", true);												
				}
				else
				{
				$("#Sexo_Femenino_Consultar").prop("checked", true);													
				}
				
				if(Datos['Infeccion_IntraHospitalaria']==0)
				{
				$("#Infeccion_NO_Consultar").prop("checked", true);			
				$("#Contenedor_Infeccion_Consultar").hide();
				}
				else
				{
				$("#Infeccion_SI_Consultar").prop("checked", true);		
				$("#Contenedor_Infeccion_Consultar").show();
				tinymce.get('Descripcion_IntraHospitalaria_Consultar').setContent(Datos['Descripcion_Intrahospitalaria']);				
				}
				//Datos de Atencion
				$('.FuenteFinanciamiento').val(Datos['Descripcion']);
				$('.IdCuentaAtencion').val(Datos['IdCuentaAtencion']);
				$('.FechaIngreso').val(Datos['FechaIngreso']);
				$('.HoraIngreso').val(Datos['HoraIngreso']);
				$('.Servicio').val(Datos['Nombre']);
				//Datos Adicionales
				$('.Cama').val(Datos['CamaPaciente']);
				//Datos Detalle
				var	Just = tinymce.get('Justificacion_Consultar').setContent(ValidarVacio(Datos['Justificacion']));
				var	Eva = tinymce.get('Evaluacion_Consultar').setContent(ValidarVacio(Datos['Evaluacion']));
				});
			}
		
		
		
		
		
		    //Script para Cargar Diagnosticos  de Paciente 
			function Cargar_Diagnosticos(IdCabeceraAntimicrobiano){
				$.post('../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php', {acc: 'MostrarDiagnosticosCabeceraAntimicrobiano',IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano}, function(htmlexterno){
				var Datos=JSON.parse(htmlexterno.replace("[", "").replace("]", ""));
				//Diagnosticos
				$('.dx_Antecedentes').val(Datos['DescripcionA']);
				$('.codigo_dx_Antecedentes').val(Datos['CodigoA']);
				$('.dx_infeccioso').val(Datos['DescripcionB']);
				$('.codigo_dx_infeccioso').val(Datos['CodigoB']);
				$('.dx_otro').val(Datos['DescripcionC']);
				$('.codigo_dx_otro').val(Datos['CodigoC']);
				});		
			}


			
			
			
			
		   //Script para Cargar Funciones Vitales de Paciente 	
			function Cargar_Funciones_Vitales(IdCabeceraAntimicrobiano){
				$.post('../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php', {acc: 'MostrarFuncionesVitalesAntimicrobiano',IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano}, function(htmlexterno){
				var Datos=JSON.parse(htmlexterno.replace("[", "").replace("]", ""));
				//Funciones Vitales
				$('.FuncionV_FR').val(Datos['Valor_FR']);
				$('.FuncionV_FC').val(Datos['Valor_FC']);
				$('.FuncionV_PAS').val(Datos['Valor_PAS']);
				$('.FuncionV_PAD').val(Datos['Valor_PAD']);
				$('.FuncionV_Glasgon').val(Datos['Valor_Glasgon']);
				$('.FuncionV_Temperatura').val(Datos['Valor_Temperatura']);
				$('.FuncionV_Peso').val(Datos['Valor_Peso']);
				});
			}
		

		
		
		
		
		   //Script Detalle de Antimicrobianos realiza el calculo 
			$(function(){
				var lastIndex;
				$('#DetalleAntimicrobianos').datagrid({
					onClickRow:function(rowIndex){
						if (lastIndex != rowIndex){
							$(this).datagrid('endEdit', lastIndex);
							$(this).datagrid('beginEdit', rowIndex);
						}
						lastIndex = rowIndex;
					},
					onBeginEdit:function(rowIndex){
						var editors = $('#DetalleAntimicrobianos').datagrid('getEditors', rowIndex);
						var DosisEditor = $(editors[0].target);
						var FrecuenciaEditor = $(editors[1].target);
						var DiasEditor = $(editors[2].target);
						var TotalEditor = $(editors[3].target);
						DiasEditor.numberbox({
							onChange:function(){
								var Total = DosisEditor.numberbox('getValue')* 0.001 *DiasEditor.numberbox('getValue')*(24/FrecuenciaEditor.numberbox('getValue')).toFixed(1);
								TotalEditor.numberbox('setValue',Total);
							}
						})
					}
				});
			});
		
			


			
		
		// Script para cargar los Resultados del Examen 	
		function IngresarResultadosKK(orden,iprod)
		{
			$.ajax({
                url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ResultadosDetallados',
                type: 'POST',
                dataType: 'json',
                data: {
                    idorden: orden,
                    idproductocpt:iprod
                },
                success:function(data)
                {   
                	$("#tabla_resultados_kk").html(' ');
                	var valor = 1;
                    var content = "<table><tr><th style='width:1%;'>Grupo</th><th style='width:15%;'>Item</th><th style='width:5%;'>Valor Numero</th><th style='width:35%;'>Valor Texto</th><th style='width:11%;'>Valor Combo</th><th style='width:11%;'>Valor Check</th><th style='width:11%;'>Valor Referencial</th><th style='width:11%;'>Metodo</th><th style='display:none;''>idproducto</th><th style='display:none;'>ordenxresultado</th> </tr><tbody>";
                    
                    $.each(data, function(index, val) {
                        
                        if(data[index].GRUPO == null){data[index].GRUPO = " ";}
                        if(data[index].ITEM == null){data[index].ITEM = " ";}
                        if(data[index].VALORNUMERO == null){data[index].VALORNUMERO = " ";}
                        if(data[index].VALORTEXTO == null){data[index].VALORTEXTO = " ";}
                        if(data[index].VALORCOMBO == null){data[index].VALORCOMBO = " ";}
                        if(data[index].VALORCHECK == null){data[index].VALORCHECK = " ";}
                        if(data[index].VALORREFERENCIAL == null){data[index].VALORREFERENCIAL = " ";}
                        if(data[index].METODO == null){data[index].METODO = " ";}
                        
                        content += '<tr>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].GRUPO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].ITEM+'" disabled/></td>';
                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLONUMERO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }

                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLOTEXTO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCOMBO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCHECK+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORREFERENCIAL+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].METODO+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].IDPRODCPT+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].ORDENXRESULTADO+'" disabled/></td>';
                        content += '</tr>';
                    }); 
                    content += '</tbody></table>';
                    $("#tabla_resultados_kk").append(content); 
                }
            });
		}

	
	    //Script para cargar los Examenes de Antimicrobianos  							
		//function Cargar_Servicios(IdCuentaAtencion)
		function Cargar_Servicios(IdPaciente)
		{								
		var dg =$('#Examenes_Antimicrobianos').datagrid({
		iconCls:'icon-edit',
		singleSelect:true,
		url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ExamenesAntimicrobianosxIdPaciente&IdPaciente='+IdPaciente,
		//url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ExamenesAntimicrobianos&IdCuentaAtencion='+IdCuentaAtencion,
		columns:[[
		{field:'ORDEN',title:'N&#176; <br> Orden',width:80,styler:Estilo_Basico},
		{field:'NUMERO',title:'N&#176;',width:40},
		{field:'CODIGO',title:'Codigo',width:60},
		{field:'NOMBRE',title:'Nombre',width:280},
		{field:'CANTIDAD',title:'Cant.',width:40},
		{field:'RESULTAD',title:'Resultado',width:70,sortable:true,align:'center',styler:Resultado_Estilo},
		{field:'FECHADESPACHO',title:'Fecha <br> Despacho',width:122}		
		]],
		onLoadSuccess: CombinarCeldas,
		onClickRow:function(row){
		var row = $('#Examenes_Antimicrobianos').datagrid('getSelected');            		
		if(row.RESULTAD == 'SI')
		{
		$('#Detalle_Orden').window('setTitle',row.CODIGO+' - '+row.NOMBRE);
		IngresarResultadosKK(row.ORDEN,row.IDPRODCPT);
		var idorden = row.ORDEN;
		var idproductocpt = row.IDPRODCPT;
		$.ajax({
		url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=datosCabecerasDetalle',
		type: 'POST',
		dataType: 'json',
		data: {
		idorden:idorden,
		idproductocpt:idproductocpt
		}
		});
		$('#Detalle_Orden').window('open');
		}
		else
		{
		$.messager.alert('SIGESA','No cuenta con Resultado');
		}
		},
		rowStyler: function(index,row){
		   if (row.TIPO ==1){
			 return 'font-weight:bold';
		  }
		}
		});
		}
		
	
	
	
	
	    //Script para cargar los Examenes de Antimicrobianos  							
		//function Cargar_Servicios_Visualizar(IdCuentaAtencion)
		function Cargar_Servicios_Visualizar(IdPaciente)
		{								
		var dg =$('#Examenes_Antimicrobianos_Visualizar').datagrid({
		iconCls:'icon-edit',
		singleSelect:true,
		/*url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ExamenesAntimicrobianos&IdCuentaAtencion='+IdCuentaAtencion,*/
		url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ExamenesAntimicrobianosxIdPaciente&IdPaciente='+IdPaciente,
		columns:[[
		{field:'ORDEN',title:'N&#176; <br> Orden',width:80,styler:Estilo_Basico},
		{field:'NUMERO',title:'N&#176;',width:40},
		{field:'CODIGO',title:'Codigo',width:60},
		{field:'NOMBRE',title:'Nombre',width:280},
		{field:'CANTIDAD',title:'Cant.',width:40},
		{field:'RESULTAD',title:'Resultado',width:70,sortable:true,align:'center',styler:Resultado_Estilo},
		{field:'FECHADESPACHO',title:'Fecha <br> Despacho',width:122}		
		]],
		onLoadSuccess: CombinarCeldas_Visualizar,
		onClickRow:function(row){
		var row = $('#Examenes_Antimicrobianos_Visualizar').datagrid('getSelected');            		
		if(row.RESULTAD == 'SI')
		{
		$('#Detalle_Orden').window('setTitle',row.CODIGO+' - '+row.NOMBRE);
		IngresarResultadosKK(row.ORDEN,row.IDPRODCPT);
		var idorden = row.ORDEN;
		var idproductocpt = row.IDPRODCPT;
		$.ajax({
		url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=datosCabecerasDetalle',
		type: 'POST',
		dataType: 'json',
		data: {
		idorden:idorden,
		idproductocpt:idproductocpt
		}
		});
		$('#Detalle_Orden').window('open');
		}
		else
		{
		$.messager.alert('SIGESA','No cuenta con Resultado');
		}
		},
		rowStyler: function(index,row){
		   if (row.TIPO ==1){
			 return 'font-weight:bold';
		  }
		}
		});
		}

		
		
		//Script para combinar celdas de la tabla de Examenes  
		function CombinarCeldas_Visualizar(data){
				var rows=$('#Examenes_Antimicrobianos_Visualizar').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var Posicion=row.POSICION;
				var Cantidad=row.ITEMS;
				if(row.NUMERO==1)
				{
				$(this).datagrid('mergeCells',{
				index: Posicion,
				field: 'ORDEN',
				rowspan: Cantidad
				});		
				}				
			}									
		}	
		
		
		
		
	    // Script para combinar celdas de la tabla de Examenes  
		function CombinarCeldas(data){
				var rows=$('#Examenes_Antimicrobianos').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var Posicion=row.POSICION;
				var Cantidad=row.ITEMS;
				if(row.NUMERO==1)
				{
				$(this).datagrid('mergeCells',{
				index: Posicion,
				field: 'ORDEN',
				rowspan: Cantidad
				});		
				}				
			}									
		}
	

	
	

	  // Script para cargar  Antimicrobianos  Solicitados							
		function AntimicrobianoSolicitados(IdCabeceraAntimicrobiano)
		{
		var dg =$('#DetalleAntimicrobianos').datagrid({
		iconCls:'icon-edit',
		singleSelect:true,
		rownumbers:true,
		pagination:true,
		pageSize:10,
		url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDetalleAntimicrobianos&IdCabeceraAntimicrobiano='+IdCabeceraAntimicrobiano,
		columns:[[
		{field:'Codigo',title:'Codigo',width:50},
		{field:'Nombre',title:'Nombre',width:330},
		{field:'Dosis',title:'Dosis',width:60,editor:{type:'numberbox',options:{precision:1}}},
		{field:'Frecuencia',title:'Frecuencia',width:70,editor:{type:'numberbox',options:{precision:0}}},
		{field:'Dias',align:'center',title:'Dias',width:40,sortable:true,align:'center',editor:{type:'numberbox',options:{precision:0}}},
		{field:'Total',align:'center',title:'Total',width:80,editor:{type:'numberbox',options:{precision:1}},styler:Estilo_Columna},
		{field:'IdEstadoAntimicrobiano',align:'center',title:'Estado',width:105,
		formatter:function(value,row){
		return row.Descripcion || value;
		},
		editor:{
		type:'combobox',
		options:{
		valueField:'IdEstadoAntimicrobiano',
		textField:'Descripcion',
		data:products,
		required:true
		}
		}
		}
		,
		{field:'action',title:'Accion',width:100,align:'center',
							formatter:function(value,row,index){
								if (row.editing){
									var s = '<a href="javascript:void(0)" onclick="saverow(this)">Guardar</a> ';
									var c = '<a href="javascript:void(0)" onclick="cancelrow(this)">Cancelar</a>';
									return s+c;
								} else {
									var e = '<a href="javascript:void(0)" onclick="editrow(this)">Editar</a> ';
									var d = '<a href="javascript:void(0)" onclick="deleterow(this)">Eliminar</a>';
									return e+d;
								}
							}
		}
		]],
		onEndEdit:function(index,row){
		var ed = $(this).datagrid('getEditor', {
				index: index,
				field: 'IdEstadoAntimicrobiano'
		});
		row.Descripcion = $(ed.target).combobox('getText');
		}
		,
					onBeforeEdit:function(index,row){
						row.editing = true;
						$(this).datagrid('refreshRow', index);
					},
					onAfterEdit:function(index,row){
						row.editing = false;
						$(this).datagrid('refreshRow', index);
					},
					onCancelEdit:function(index,row){
						row.editing = false;
						$(this).datagrid('refreshRow', index);
					}
		
		});
		}
		
		
		function getRowIndex(target){
			var tr = $(target).closest('tr.datagrid-row');
			return parseInt(tr.attr('datagrid-row-index'));
		}
		function editrow(target){
			$('#DetalleAntimicrobianos').datagrid('beginEdit', getRowIndex(target));
		}
		function deleterow(target){
			$.messager.confirm('Confirm','Are you sure?',function(r){
				if (r){
					$('#DetalleAntimicrobianos').datagrid('deleteRow', getRowIndex(target));
				}
			});
		}
	
	
	   function saverow(target){
			$('#DetalleAntimicrobianos').datagrid('endEdit', getRowIndex(target));
	   }
		
	
	
	  // Script de Estilo para columna de resultado de Examenes   
		var products = [
		    {IdEstadoAntimicrobiano:'1',Descripcion:'Pendiente'},
		    {IdEstadoAntimicrobiano:'2',Descripcion:'Aprobado'},
		    {IdEstadoAntimicrobiano:'3',Descripcion:'Desaprobado'}
		];
   
   
   
	 //Script Estilo para Resultados  	
	  function Resultado_Estilo(value,row,index){
			if (row.RESULTAD == 'NO'){
			return 'color:red;font-weight:bold';
			}
			else{
			return 'color:green;font-weight:bold';
			}
	  }
	
	
	
	// Script de Estilo para columna de resultado de Examenes   
	function Estilo_Datagrid(value,row,index){
	return 'background-color:#F6E3CE;color:#8A0808;font-weight:bold !important';
	}
	
	
	
	
	// Script de Estilo para columna   
	function Estilo_Columna(value,row,index){
	return 'background-color:#F6E3CE; color:red !important';
	}
	

	
	// Script de Estilo para columna   
	function Estilo_Basico(value,row,index){
	return 'background-color:white   !important;';
	}
	
	
	
	 //Funcion para cerrar el contenedor de Consultar
	function Consultar(){
	var row = $('#Antimicrobianos').datagrid('getSelected');
	$('#Contenedor_Consultar').dialog('open');
	Cargar_Datos_Personales_Consultar(row.IdCabeceraAntimicrobiano);
	//Cargar_Servicios_Visualizar(row.IdCuentaAtencion);
	Cargar_Servicios_Visualizar(row.IdPaciente);
	AntimicrobianoPrevios_Visualizar(row.IdCabeceraAntimicrobiano);
	AntimicrobianoSolicitados_Visualizar(row.IdCabeceraAntimicrobiano);
	Cargar_Diagnosticos(row.IdCabeceraAntimicrobiano);
	Cargar_Funciones_Vitales(row.IdCabeceraAntimicrobiano);
	}
					
					
	//Funcion para Agregar un nuevo Registro
	function Agregar(){
	$('#Contenedor_Agregar').dialog('open');
	}
					
				

				
					
	//Funcion para Modificar los Datos de Registro
	function Modificar(){
	var row = $('#Antimicrobianos').datagrid('getSelected');
	$('#Contenedor_Modificar').dialog('open');
	Cargar_Datos_Personales(row.IdCabeceraAntimicrobiano);
	Cargar_Servicios(row.IdPaciente);
	//Cargar_Servicios(row.IdCuentaAtencion);
	AntimicrobianoPrevios(row.IdCabeceraAntimicrobiano);
	AntimicrobianoSolicitados(row.IdCabeceraAntimicrobiano);
	Cargar_Diagnosticos(row.IdCabeceraAntimicrobiano);
	Cargar_Funciones_Vitales(row.IdCabeceraAntimicrobiano);
	}
			
		
			
	function Eliminar(){
      $.messager.confirm('SIGESA', 'Deseas eliminar el Registro?', function(r){
      if (r){
	   var row = $('#Antimicrobianos').datagrid('getSelected');
	   var parametros = {
                "acc" : 'EliminarSolicitudRegistroAntimicrobiano',
                "IdCabeceraAntimicrobiano" : row.IdCabeceraAntimicrobiano
       };
       $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?', //archivo que recibe la peticion
                type:  'post' //método de envio
        });
	   $('#Antimicrobianos').datagrid('reload');
       }
      });
	}
		
		
	function Generar_Sugerencia_Antimicrobiano(){	
	$('#DetalleAntimicrobianos').datagrid('reload');
	};	 
		 
		 
	//Funcion para cerrar el contenedor de Consultar
	function Cerrar_Contenedor_Consultar()
	{
	$('#Contenedor_Consultar').dialog('close');
	}
	
	
	//Funcion para cerrar el contenedor de Consultar
	function Cerrar_Contenedor_Modificar()
	{
	$('#Contenedor_Modificar').dialog('close');
	}

	//Funcion para cerrar el contenedor de Agregar
	function Cerrar_Contenedor_Sugerencia()
	{
	$('#dlg').dialog('close');
	}

	//Funcion para cerrar el contenedor de Consultar
	function Cerrar_Contenedor_Agregar()
	{
	$('#Contenedor_Agregar').dialog('close');
	}
	
	
	function Mostrar_Contenido_Infeccion() {
			element = document.getElementById("Contenedor_Infeccion");
			check_Si = document.getElementById("Infeccion_SI");
			check_No = document.getElementById("Infeccion_NO");
			if (check_Si.checked) {
				element.style.display='block';
			}
			if (check_No.checked) {
				element.style.display='none';
			}
	}
		
		
		

		
	// Script que se ejecutaran al cargar la Pagina  
	$(document).ready(function(){		
			//Formato de Busqueda
			$('#b_Dni').numeric();
			$('#b_HistoriaClinica').numeric();
			
			//Funciones Vitales  
			$('#FuncionV_FR,#FuncionV_FC,#FuncionV_PAS,#FuncionV_PAD,#FuncionV_Glasgon,#FuncionV_Peso,#FuncionV_Temperatura').numeric(",");

			//Antimicrobianos Previos
			$('#AntimicrobianoDia_I').numeric(",");
			$('#Dias').numeric();
			
			
			//Antimicrobianos Alternativo
			$('#Dosis_Sugerencia').numeric();
			$('#Frecuencia_Sugerencia').numeric();
			$('#Dias_Sugerencia').numeric();
			
			
			//Antimicrobianos Solicitados
			$('#Dosis_Antimicrobiano_Solicitado').numeric(","); 
			$('#Frecuencia_Antimicrobiano_Solicitado').numeric(); 
			
			//Limpiar datos de Busqueda
			$("#Limpiar_Busqueda").click(function(){
				$("#b_ApellidoPaterno").val("");
				$("#b_ApellidoMaterno").val("");
				$("#b_HistoriaClinica").val("");
				$("#b_Dni").val("");
			});


			$("#AntimicrobianoDia_I,#FuncionV_FR,#FuncionV_FC,#FuncionV_PAS,#FuncionV_PAD,#FuncionV_Glasgon,#FuncionV_Peso,#FuncionV_Temperatura").bind('blur keyup', function(){  
				if ($(this).val() != "") {  			
					$('.error').fadeOut();
					return false;  
				}  
			});	
		
		   //Contenedores de Vistas
			$('#Detalle_Orden').dialog('close');
			$('#Contenedor_Editor').dialog('close');
			$('#Contenedor_Consultar').window('close');
			$('#Contenedor_Agregar').window('close');
			$('#Contenedor_Modificar').window('close');
			$('#dlg').dialog('close');
			
			//Deshabilitar el boton de Guardar para la Vista Consultar
		    $('#Guardar_Cambios_Consultar').linkbutton({disabled:true});
		});
		
		


		$(document).ready(function(){
			
				//Script para Guardar los Cambios Realizados  
				$('#Guardar_Cambios_Modificar').click(function() {
				 var rows=$('#DetalleAntimicrobianos').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var Codigo=row.Codigo;
				var Nombre=row.Nombre;
				var Dosis=row.Dosis;
				var Frecuencia=row.Frecuencia;
				var Dias=row.Dias;
				var Total=row.Total;
				var Id_Antimicrobiano=row.Id_Antimicrobiano;
				var IdCabeceraAntimicrobiano=row.IdCabeceraAntimicrobiano;
				var IdEstadoAntimicrobiano=row.IdEstadoAntimicrobiano;
				$.ajax({
				url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=ModificarCabecerasDetalle',
				type: 'POST',
				dataType: 'json',
				data: {
				IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano,
				Id_Antimicrobiano:Id_Antimicrobiano,
				Dias:Dias,
				Total:Total,
				IdEstadoAntimicrobiano:IdEstadoAntimicrobiano 
				}
				});
				}
				var IdCabecera=$("#IdCabecera").val();
				var Evaluacion =tinymce.get('Evaluacion_Modificar').getContent();
				$.ajax({
				url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=ModificarCabecerasEvaluacion',
				type: 'POST',
				dataType: 'json',
				data: {
				IdCabeceraAntimicrobiano:IdCabecera,
				Evaluacion:Evaluacion
				}
				});
				$('#Contenedor_Modificar').dialog('close');
				$('#Antimicrobianos').datagrid('reload');				
				});	
					

					
			
			    //Focus en la caja de Texto
				$("#b_Dni,#b_HistoriaClinica,#b_ApellidoMaterno,#b_ApellidoPaterno").focus(function(){
					//Al hacer Enter ejecutara el buscador de Paciente
					$(document).keypress(function(e) {
						if(e.which == 13) {
						if($("#b_Dni").val().length!=0 || $("#b_HistoriaClinica").val().length!=0)
							{
								Buscar_Paciente();
							}
						}
					});
					
				});

				
				//Al hacer Click en el Boton de Busqueda ejecutara el buscador de Paciente
				$('#Busqueda_Paciente').click(function() {
					Buscar_Paciente();			
				});	

				$("#Guardar_Cambios_Agregar").click(function(){
					
							$(".error").fadeOut().remove();
							if ($("#IdPaciente").val() == ""  ||   $("#IdCuentaAtencion").val() == "") {
							$.messager.alert('SIGESA','Por favor Seleccione Paciente y Atención');					
							return false;  
							} 

							var IdDiagnosticoInfeccioso=$("#IdDiagnosticoInfeccioso").val();
							var IdDiagnosticoOtro=$("#IdDiagnosticoOtro").val();
							var IdDiagnosticoPatologico=$("#IdDiagnosticoPatologico").val();
							var Diag1=IdDiagnosticoInfeccioso.length;
							var Diag2=IdDiagnosticoOtro.length;
							var Diag3=IdDiagnosticoPatologico.length;
							var Contador_Diagnosticos=Diag1+Diag2+Diag3;
	
							// Valida que se Ingrese Funcion Vital

							if ($("#FuncionV_FR").val() == "") {  
									$("#FuncionV_FR").focus().after('<span class="error">Ingrese F. Vital FR</span>');  
									return false;  
							} 
							
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_FC").val() == "") {  
									$("#FuncionV_FC").focus().after('<span class="error">Ingrese F. Vital FC</span>');  
									return false;  
							} 

										
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_PAS").val() == "") {  
									$("#FuncionV_PAS").focus().after('<span class="error">Ingrese F. Vital PAS</span>');  
									return false;  
							} 
							
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_PAD").val() == "") {  
									$("#FuncionV_PAD").focus().after('<span class="error">Ingrese F. Vital PAD</span>');  
									return false;   
							} 
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_Glasgon").val() == "") {  
									$("#FuncionV_Glasgon").focus().after('<span class="error">Ingrese F. Vital Glasgon</span>');  
									return false;  
							} 
							
							
						    // Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_Temperatura").val() == "") {  
									$("#FuncionV_Temperatura").focus().after('<span class="error">Ingrese F. Vital Temperatura</span>');  
									return false;  
							} 
							
							
							
							// Valida que se Ingrese Funcion Vital
							if ($("#FuncionV_Peso").val() == "") {  
									$("#FuncionV_Peso").focus().after('<span class="error">Ingrese F. Vital Peso</span>');  
									return false;  
							} 

							
							// Validacion de Diagnosticos
							if (Contador_Diagnosticos == 0) {  
								$.messager.alert('SIGESA','Por favor ingrese minimo un diagnostico');
								return false;  				
							} 
							
							
							// Valida que se Ingrese Antimicrobiano Previos
							if(Previos.length==0)
							{
							$.messager.alert('SIGESA','Por favor ingrese minimo un Antimicrobiano Previos');
							return false; 	
							}
							
							
							// Valida que se Ingrese Antimicrobiano Solicitados
							if(solicitados.length==0)
							{
							$.messager.alert('SIGESA','Por favor ingrese minimo un Antimicrobiano Controlado');
							return false; 	
							}
							
							// Valida una Justificacion 
							if (tinymce.get('Justificacion').getContent() == "") {  
							$.messager.alert('SIGESA','Por favor ingrese una Justificación');
							return false;  
							} 
							

							var FechaRegistro=$("#FechaRegistro").val(); 
							var IdPaciente=$("#IdPaciente").val(); 
							var IdCuentaAtencion=$("#IdCuentaAtencion").val(); 
							var IdDiagnosticoPatologico=$("#IdDiagnosticoPatologico").val(); 	
							var IdDiagnosticoInfeccioso=$("#IdDiagnosticoInfeccioso").val(); 
							var IdDiagnosticoOtro=$("#IdDiagnosticoOtro").val(); 
							var Infeccion_IntraHospitalaria=$('input:radio[name=Infeccion_IntraHospitalaria]:checked').val();
							var Descripcion_IntraHospitalaria =tinymce.get('Descripcion_IntraHospitalaria').getContent();
							var Justificacion =tinymce.get('Justificacion').getContent();
							var Estado='1';
							
							$.ajax({
							url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Cabecera_Antimicrobiano',
							type: 'POST',
							dataType: 'json',
							data: 
							{
							"FechaRegistro" : FechaRegistro,
							"IdPaciente" : IdPaciente,
							"IdCuentaAtencion" : IdCuentaAtencion,
							"IdDiagnosticoPatologico" : IdDiagnosticoPatologico,
							"IdDiagnosticoInfeccioso" : IdDiagnosticoInfeccioso,
							"IdDiagnosticoOtro" : IdDiagnosticoOtro,
							"Infeccion_IntraHospitalaria" : Infeccion_IntraHospitalaria,
							"Descripcion_IntraHospitalaria" : Descripcion_IntraHospitalaria,
							"Justificacion" : Justificacion,
							"Estado" : Estado
							},
							success: function(data)
							{

								/*Agregar Datos  */
								Agregar_Funciones_Vitales(data);
								Agregar_Previos_Antimicrobiano(data);
								Agregar_Detalle_Antimicrobiano(data);	
								$.messager.alert('SIGESA','Solicitud Ingresada Satisfactoriamente');
								NuevoRegistro();							
							}
						});
										
				});

				
				// Agregar Previos Antimicrobiano
				function Agregar_Previos_Antimicrobiano(IdCabeceraAntimicrobianoAntimicrobiano){
				var rows=$('#AntimicrobianoPrevio').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var IdCabeceraAntimicrobiano=row.IdCabeceraAntimicrobiano;
				var IdAntimicrobiano=row.IdAntimicrobiano;
				var Dias=row.Dias;
				$.ajax({
					url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Previo_Antimicrobiano',
					type: 'POST',
					dataType: 'json',
					data: {
					IdCabeceraAntimicrobiano:IdCabeceraAntimicrobianoAntimicrobiano,
					IdAntimicrobiano:IdAntimicrobiano,
					Dias   : Dias
					}
					});
					}
				}

				// Agregar Detalle Antimicrobiano
				function Agregar_Detalle_Antimicrobiano(IdCabeceraAntimicrobianoAntimicrobiano){
				var rows=$('#AntimicrobianoSolicitados').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var Codigo=row.Codigo;
				var Nombre=row.Nombre;
				var Dosis=row.Dosis;
				var Frecuencia=row.Frecuencia;
				var IdAntimicrobiano=row.IdAntimicrobiano;
				var IdCabeceraAntimicrobiano=row.IdCabeceraAntimicrobiano;
				$.ajax({
					url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Detalle_Antimicrobiano',
					type: 'POST',
					dataType: 'json',
					data: {
					IdCabeceraAntimicrobiano:IdCabeceraAntimicrobianoAntimicrobiano,
					IdAntimicrobiano:IdAntimicrobiano,
					Frecuencia:Frecuencia,
					Dosis  :Dosis,
					Dias   : '',
					Total  : '',
					Estado : '1'
					}
					});
					}
				}


				// Agregar Funciones Vitales
				function Agregar_Funciones_Vitales(IdCabeceraAntimicrobianoAntimicrobiano){
					var FuncionV_FR=$("#FuncionV_FR").val(); 
					var FuncionV_FC=$("#FuncionV_FC").val(); 
					var FuncionV_PAS=$("#FuncionV_PAS").val(); 
					var FuncionV_PAD=$("#FuncionV_PAD").val(); 
					var FuncionV_Glasgon=$("#FuncionV_Glasgon").val(); 
					var FuncionV_Peso=$("#FuncionV_Peso").val(); 
					var FuncionV_Temperatura=$("#FuncionV_Temperatura").val(); 
					$.ajax({
					url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Funciones_Vitales',
					type: 'POST',
					dataType: 'json',
					data: 
					{
					"Id_Formulario_Antimicrobiano" :IdCabeceraAntimicrobianoAntimicrobiano,	
					"FuncionV_FR" : FuncionV_FR,
					"FuncionV_FC" : FuncionV_FC,
					"FuncionV_PAS" : FuncionV_PAS,
					"FuncionV_PAD" : FuncionV_PAD,
					"FuncionV_Glasgon" : FuncionV_Glasgon,
					"FuncionV_Peso" : FuncionV_Peso,
					"FuncionV_Temperatura" : FuncionV_Temperatura
					},
					success: function(data)
					{
					$("#FuncionV_FR").val("");
					$("#FuncionV_FC").val("");
					$("#FuncionV_PAS").val("");
					$("#FuncionV_PAD").val("");
					$("#FuncionV_Glasgon").val("");
					$("#FuncionV_Peso").val("");
					$("#FuncionV_Temperatura").val("");
					}
					});		
				}
				
				
				
				//Funcion para limpiar todos los datos despues de grabar
				function NuevoRegistro()
				{	
				
					//Limpia las cajas de Tipo Texto
					$(":text").each(function(){	
							$($(this)).val('');
					});
					
					//Descripcion_IntraHospitalaria
					tinymce.get('Descripcion_IntraHospitalaria').setContent('');
								
					//Poner por defecto en blanco la Justificacion
					tinymce.get('Justificacion').setContent('');
	
								
					// Limpiar Datagrid de EasyUI
					$('#AntimicrobianoSolicitados').datagrid('loadData',[]);
					$('#AntimicrobianoPrevio').datagrid('loadData',[]);

					
					// Limpiar Array
					solicitados.splice(0, solicitados.length);		
					Previos.splice(0, Previos.length);	
					
				}
				
				
			    /* No Cargara por defecto las Ventanas  */	
				$('#Lista_Pacientes').dialog('close');
				$('#Lista_Atenciones').dialog('close');
				$('#Lista_Diagnosticos_II').dialog('close');
				$('#Lista_Diagnosticos_III').dialog('close');
				$('#Lista_Diagnosticos_IV').dialog('close');
				$('#Antimicrobianos_I').dialog('close');
				$('#Contenedor_Antimicrobiano_Solicitado_I').dialog('close');
				$('#Contenedor_Antimicrobiano_Sugerencia').dialog('close');
				


				
				/* Da Formato correcto al easyui  */
				$('.easyui-datebox').datebox({
					formatter : function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
					},
					parser : function(s){

						if (!s) return new Date();
						var ss = s.split('-');
						var y = parseInt(ss[2],10);
						var m = parseInt(ss[1],10);
						var d = parseInt(ss[0],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d)
						} else {
							return new Date();
						}
					}

				});	

				
				$('#Ventana_Diagnosticos_Infeccioso').click(function() {
					$('#Lista_Diagnosticos_II').dialog('open');
				});

				$('#Ventana_Diagnosticos_Otro').click(function() {
					$('#Lista_Diagnosticos_III').dialog('open');
				});
				
				
				$('#Ventana_Diagnosticos_Antecedentes').click(function() {
					$('#Lista_Diagnosticos_IV').dialog('open');
				});
				
				
				
				
				function Buscar_Paciente(){
					$("#b_Dni,#b_HistoriaClinica,#b_ApellidoMaterno,#b_ApellidoPaterno").blur();
					var NroHistoriaClinica=$("#b_HistoriaClinica").val();
					var NroDocumento=$("#b_Dni").val();
					var ApellidoPaterno=$("#b_ApellidoPaterno").val();
					var ApellidoMaterno=$("#b_ApellidoMaterno").val();
					var n1=NroHistoriaClinica.length;
					var n2=NroDocumento.length;
					var n3=ApellidoPaterno.length;
					var n4=ApellidoMaterno.length;
					var total=0;
					total=n1+n2+n3+n4;
					if(total==0)
					{
					$.messager.alert('SIGESA','Por favor Ingrese alguno de los Filtros <br>(Ap. Paterno,Ap. Materno,DNI o Nro Historia Clinica)');							
					return false;	
					}
					if(total>0 && n2!=8 && n2>0)
					{
					$.messager.alert('SIGESA','Ingrese numero de DNI correcto');							
					return false;	
					}
					
					$('#Lista_Pacientes').dialog('open');
						var dg =$('#Lista').datagrid({
											iconCls:'icon-edit',
											singleSelect:true,
											rownumbers:true,
											rowtexts:true,
											pagination: true,
											remoteSort:false,
											multiSort:true,
											url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarPaciente&NroHistoriaClinica='+NroHistoriaClinica+'&NroDocumento='+NroDocumento+'&ApellidoPaterno='+ApellidoPaterno+'&ApellidoMaterno='+ApellidoMaterno,
											columns:[[
												{field:'ApellidoPaterno',title:'Apellido Paterno',width:130,sortable:true},
												{field:'ApellidoMaterno',title:'Apellido Materno',width:130,sortable:true},
												{field:'PrimerNombre',title:'Primer Nombre',width:120,sortable:true},
												{field:'SegundoNombre',title:'Segundo Nombre',width:120,sortable:true},
												{field:'NroDocumento',title:'DNI',width:120}
											]],
											onSelect:function(index,row){
											$('#Lista_Pacientes').dialog('close');
											$('#ApellidoPaterno').val(row.ApellidoPaterno);
											$('#ApellidoMaterno').val(row.ApellidoMaterno);
											$('#PrimerNombre').val(row.PrimerNombre);
											$('#SegundoNombre').val(row.SegundoNombre);
											$('#NroDocumento').val(row.NroDocumento);
											$('#IdPaciente').val(row.IdPaciente);
											$('#FechaNacimiento').val(row.FechaNacimiento);
											$('#Edad').val(row.Edad);
											$('#NroHistoriaClinica').val(row.NroHistoriaClinica);
											if(row.IdTipoSexo==1)
												{
												$("#Sexo_Masculino").prop("checked", true);												
												}
											else
												{
												$("#Sexo_Femenino").prop("checked", true);													
												}
												Ventana_Progresiva(row.IdPaciente);	
												//Limpiar Campos  
												//Datos de Busqueda
											   $("#b_Dni").val("");
											   $("#b_HistoriaClinica").val("");
											   $("#b_ApellidoPaterno").val("");
											   $("#b_ApellidoMaterno").val("");												
											}
						});	
						
					}

				
				 function Ventana_Progresiva(IdPaciente){
					var win = $.messager.progress({
						title:'Atenciones de Paciente',
						msg:'Cargando Atenciones...'
					});
					setTimeout(function(){
						$.messager.progress('close');
						Lista_Atenciones(IdPaciente);
					},600)
				}
				
				
				function  Lista_Atenciones(IdPaciente){
						$('#Lista_Atenciones').dialog('open');
						var dg =$('#Lista_Atenciones_Paciente').datagrid({
											iconCls:'icon-edit',
											singleSelect:true,
											rownumbers:true,
											rowtexts:true,
											pagination: true,
											url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarAtencionesporIdPaciente&IdPaciente='+IdPaciente,
											columns:[[
												{field:'Nombre',title:'Servicio',width:160},
												{field:'TipoServicio',title:'Tipo de <br> Servicio',width:180},
												{field:'FechaIngreso',title:'Fecha de <br> Ingreso',width:75},
												{field:'HoraIngreso',title:'Hora de <br> Ingreso',width:60},
												{field:'IdCuentaAtencion',title:'Cuenta de <br> Atencion',width:70},
												{field:'FuenteFinanciamiento',title:'Fuente de <br>  Financiamiento',width:120},

											]],
											onSelect:function(index,row){
											$('#Lista_Atenciones').dialog('close');
											$('#FuenteFinanciamiento').val(row.FuenteFinanciamiento);
											$('#Servicio').val(row.Nombre);
											$('#FechaIngreso').val(row.FechaIngreso);
											$('#HoraIngreso').val(row.HoraIngreso);	
											$('#FechaIngreso').val(row.FechaIngreso);
											$('#IdCuentaAtencion').val(row.IdCuentaAtencion);	
											$('#Cama').val(row.Cama);
											}
				
						});
				}
				

				$('#Ventana_Antimicrobiano_Previo_I').click(function() {
					$('#Antimicrobianos_I').dialog('open');
				});
				$('#Ventana_Antimicrobiano_Solicitado_I').click(function() {
					$('#Contenedor_Antimicrobiano_Solicitado_I').dialog('open');
				});
				
				$('#Agregar_Sugerencia_Antimicrobiano').click(function() {
					$('#Contenedor_Antimicrobiano_Sugerencia').dialog('open');
				});
				
			});

		 	
		$(function(){
			
				var Antimicrobianos = $('#Antimicrobianos').datagrid();
				Antimicrobianos.datagrid('enableFilter');	
			
				var Lista_Pacientes = $('#Lista').datagrid();
				    Lista_Pacientes.datagrid('enableFilter');	

				var Lista_Atenciones = $('#Lista_Atenciones_Paciente').datagrid();
				Lista_Atenciones.datagrid('enableFilter');
					
				var Lista_Antimicrobianos_I = $('#Lista_Antimicrobianos_I').datagrid();
				    Lista_Antimicrobianos_I.datagrid('enableFilter');
					
				var Lista_Antimicrobiano_Solicitado_I = $('#Lista_Antimicrobiano_Solicitado_I').datagrid();
				    Lista_Antimicrobiano_Solicitado_I.datagrid('enableFilter');	
					
				var Lista_Diagnosticos_Infeccioso = $('#Lista_Diagnosticos_Infeccioso').datagrid();
				    Lista_Diagnosticos_Infeccioso.datagrid('enableFilter');
					
				var Lista_Diagnosticos_Otro = $('#Lista_Diagnosticos_Otro').datagrid();
				    Lista_Diagnosticos_Otro.datagrid('enableFilter');	

				var Lista_Diagnosticos_Antecedentes = $('#Lista_Diagnosticos_Antecedentes').datagrid();
				    Lista_Diagnosticos_Antecedentes.datagrid('enableFilter');
					
				var Lista_Antimicrobiano_Sugerencia = $('#Lista_Antimicrobiano_Sugerencia').datagrid();
				    Lista_Antimicrobiano_Sugerencia.datagrid('enableFilter');
		
		});	

		//Script para Cargar Antimicrobiano Previo
		var Previos = [];
		$(document).ready(function(){
		$("#Agregar_Anti_Previo").click(function(){
		if($("#IdAntimicrobianoPrevio_I").val().length==0 ||  $("#AntimicrobianoDia_I").val().length==0)
		{
			$.messager.alert('SIGESA','Por favor Ingrese Antimicrobiano Previo y Dias Respectivamente');	
			return false;  
		}
		for(var i = 0; i < Previos.length; i++) {
	      if(Previos[i].IdAntimicrobiano == $("#IdAntimicrobianoPrevio_I").val()) 
			{
			$.messager.alert('SIGESA','Antimicrobiano Previo ya ah sido Ingresado Anteriormente : <strong>'+$("#Antimicrobiano_Previo_I").val()+'</strong>');
			return false;  			
			}
		}
		var valueToPush = [];
		valueToPush["IdAntimicrobiano"] = $("#IdAntimicrobianoPrevio_I").val();
		valueToPush["Codigo"] = $("#Codigo_Antimicrobiano_Previo_I").val();
		valueToPush["Descripcion"] = $("#Antimicrobiano_Previo_I").val();
		valueToPush["Dias"] = $("#AntimicrobianoDia_I").val();
		
		//Agregar Valores a Arreglo
		Previos.push(valueToPush);
		
		// Se limpia los Datos  
		$("#Antimicrobiano_Previo_I,#Codigo_Antimicrobiano_Previo_I,#IdAntimicrobianoPrevio_I,#AntimicrobianoDia_I").val("");

		var dg = $('#AntimicrobianoPrevio').datagrid();
		dg.datagrid({
		fit: true,
		pagination: true,
		singleSelect: true,
		striped: true,
		rownumbers: true,
		fitColumns: true,
		data: Previos,
		columns: [[
		 {
		 field: 'Codigo',
		 width: 80,
		 title: 'Codigo'
		 }, 
		 {
		 field: 'Descripcion',
		 width: 350,
		 title: 'Descripcion'
		 }, 
		 {
		 field: 'Dias',
		 width: 20,
		 align: 'right',
		 title: 'Dias',
		 editor:{type:'numberbox',options:{precision:1}}
		 }
		 ]
		 ],
		 onSelect: function (rowIndex, rowData) {
				 $.messager.confirm('Confirmar','Estas seguro que quieres eliminar el Antimicrobiano <strong>'+rowData.Descripcion+'</strong> ?',function(r){
                if (r){
					$('#AntimicrobianoPrevio').datagrid('deleteRow', rowIndex);
                    }
                });
		 }
		 });
		 });
		 });
		 
		 
		
		//Script para Cargar Antimicrobiano Solicitado		
		var solicitados = [];
		$(document).ready(function(){
		$("#Agregar_Antimicrobiano").click(function(){
		if($("#Dosis_Antimicrobiano_Solicitado").val().length==0 ||  $("#Frecuencia_Antimicrobiano_Solicitado").val().length==0 ||  $("#Dosis_Antimicrobiano_Solicitado").val().length==0  || $("#Id_Antimicrobiano_Solicitado").val().length==0    || $("#Descripcion_Antimicrobiano_Solicitado").val().length==0)
		{
			$.messager.alert('SIGESA','Por favor Ingrese Antimicrobiano Controlado ,Dosis y Frecuencia Respectivamente');	
			return false;  
		}
		for(var i = 0; i < solicitados.length; i++) {
	      if(solicitados[i].IdAntimicrobiano == $("#Id_Antimicrobiano_Solicitado").val()) 
			{
			$.messager.alert('SIGESA','Antimicrobiano Controlado ya ah sido Ingresado Anteriormente : <strong>'+$("#Descripcion_Antimicrobiano_Solicitado").val()+'</strong>');
			return false;  			
			}
		}
		
		var valueToPush = [];									
		valueToPush["IdAntimicrobiano"] = $("#Id_Antimicrobiano_Solicitado").val();
		valueToPush["Codigo"] = $("#Codigo_Antimicrobiano_Solicitado").val();
		valueToPush["Descripcion"] = $("#Descripcion_Antimicrobiano_Solicitado").val();
		valueToPush["Dosis"] = $("#Dosis_Antimicrobiano_Solicitado").val();
		valueToPush["Frecuencia"] = $("#Frecuencia_Antimicrobiano_Solicitado").val();
		solicitados.push(valueToPush);
		
		
		// Se limpia los Datos  
		$("#Id_Antimicrobiano_Solicitado,#Codigo_Antimicrobiano_Solicitado,#Descripcion_Antimicrobiano_Solicitado,#Dosis_Antimicrobiano_Solicitado,#Frecuencia_Antimicrobiano_Solicitado").val("");

		//Cargar Datagrid 
		var dg = $('#AntimicrobianoSolicitados').datagrid();
		dg.datagrid({
		fit: true,
		pagination: true,
		singleSelect: true,
		striped: true,
		rownumbers: true,
		fitColumns: true,
		data: solicitados,
		columns: [[
		 {
		 field: 'Codigo',
		 width: 50,
		 title: 'Codigo'
		 }, 
		 {
		 field: 'Descripcion',
		 width: 350,
		 title: 'Descripcion'
		 }, 
		 {
		 field: 'Dosis',
		 width: 30,
		 align: 'right',
		 title: 'Dosis'
		 }, 
		 {
		 field: 'Frecuencia',
		 width: 50,
		 align: 'right',
		 title: 'Frecuencia'
		 }
		 ]
		 ],
		 onSelect: function (rowIndex, rowData) {
				 $.messager.confirm('Confirmar','Estas seguro que quieres eliminar el Antimicrobiano <strong>'+rowData.Descripcion+'</strong> ?',function(r){
                if (r){
					$('#AntimicrobianoSolicitados').datagrid('deleteRow', rowIndex);
                    }
                });
		 }
		 });
		 });
		 });
		 
		 
		//Calculo de Antimicrobiano Sugerido 
		function Calculo_Antimicrobiano() {
			var total = 0;
			var Dosis_Sugerencia=document.getElementById("Dosis_Sugerencia").value;
			var Frecuencia_Sugerencia=document.getElementById("Frecuencia_Sugerencia").value;
			var Dias_Sugerencia=document.getElementById("Dias_Sugerencia").value;
			total=(Dosis_Sugerencia*0.001*Dias_Sugerencia*(24/Frecuencia_Sugerencia)).toFixed(1);
			document.getElementById('spTotal').innerHTML = total;
		}
		
		
		
		// Agregar Detalle Antimicrobiano  por Sugerencia
		function Guardar_Cambios_Sugerencia(){
				var Dosis_Sugerencia=document.getElementById("Dosis_Sugerencia").value;
				var Frecuencia_Sugerencia=document.getElementById("Frecuencia_Sugerencia").value;
				var Dias_Sugerencia=document.getElementById("Dias_Sugerencia").value;
				var IdAntimicrobiano=document.getElementById("Id_Antimicrobiano_Sugerido").value;
				var IdCabeceraAntimicrobiano=document.getElementById("IdCabecera").value;
				var total=(Dosis_Sugerencia*0.001*Dias_Sugerencia*(24/Frecuencia_Sugerencia)).toFixed(1);
				$.ajax({
					url: '../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=Agregar_Detalle_Antimicrobiano',
					type: 'POST',
					dataType: 'json',
					data: {
					IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano,
					IdAntimicrobiano:IdAntimicrobiano,
					Frecuencia:Frecuencia_Sugerencia,
					Dosis  :Dosis_Sugerencia,
					Dias   :Dias_Sugerencia,
					Total  :total,
					Estado :'4'
					}
					});
					//Limpia las cajas de Tipo Texto
					$(".antimicrobiano_sugerido").each(function(){	
							$($(this)).val('');
					});			
				$('#DetalleAntimicrobianos').datagrid('reload');
				$('#dlg').dialog('close');

		}
		
		
		
		
		
