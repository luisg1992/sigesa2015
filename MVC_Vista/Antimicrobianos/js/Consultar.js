
//Funcion para cargar los Antimicrobianos Previos del Paciente
function AntimicrobianoPrevios_Visualizar(IdCabeceraAntimicrobiano)
{
var dg =$('#PreviosAntimicrobianos_Visualizar').datagrid({
iconCls:'icon-edit',
singleSelect:true,
rownumbers:true,
pagination:true,
pageSize:10,
url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarPreviosAntimicrobiano&IdCabeceraAntimicrobiano='+IdCabeceraAntimicrobiano,
columns:[[
		{field:'Codigo',title:'Codigo',width:80},
		{field:'Nombre',title:'Nombre',width:450},
		{field:'Dias',title:'Dias',width:60}]]
});
}


//Funcion para cargar Antimicrobianos Solicitados para el Paciente
function AntimicrobianoSolicitados_Visualizar(IdCabeceraAntimicrobiano)
{
var dg =$('#DetalleAntimicrobianos_Visualizar').datagrid({
iconCls:'icon-edit',
singleSelect:true,
rownumbers:true,
pagination:true,
pageSize:10,
url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDetalleAntimicrobianos&IdCabeceraAntimicrobiano='+IdCabeceraAntimicrobiano,
rowStyler: function(index,row){
   if (row.IdEstadoAntimicrobiano ==1){
     return 'color:#ff8c1a;';
   }
   if (row.IdEstadoAntimicrobiano ==2){
     return 'color:#009900;';
   }
   if (row.IdEstadoAntimicrobiano ==3){
	return 'color:#ff0000;';
   }
},
columns:[[
		{field:'Codigo',title:'Codigo',width:50},
		{field:'Nombre',title:'Nombre',width:330},
		{field:'Dosis',title:'Dosis',width:60},
		{field:'Frecuencia',title:'Frecuencia',width:70},
		{field:'Dias',align:'center',title:'Dias',width:40,sortable:true,align:'center'},
		{field:'Total',align:'center',title:'Total',width:80},
		{field:'Descripcion',align:'center',title:'Estado',width:105},
		{field:'FechaCalificacion',align:'center',title:'Fecha',width:105}]]
});
}	


