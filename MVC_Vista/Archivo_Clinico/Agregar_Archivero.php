<html>
    <head>
		<link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
	
		<script type="text/javascript">
		$(function(){								
			$('#btn_cargar_datos').click(function(){
					$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php', 
						{ 
						 nro_documento: $('#nro_documento').val(),
						acc: 'Mostrar_Empleado'
						},
						 function(res)
						{
							parsedRes = $.parseJSON(res);
							var parsedRes = $.parseJSON(res);  	
							$( '#Apellido_Paterno' ).val( parsedRes.ApellidoPaterno);
							$( '#Apellido_Materno' ).val( parsedRes.ApellidoMaterno);
							$( '#Nombre' ).val( parsedRes.Nombres);	
							$( '#Empleado_Id' ).val( parsedRes.IdEmpleado);							
						}										
						);	
			});
			
		
			$("#btn_Agregar_Archivero").click(function(){
				
							var Apellido_Paterno=$("#Apellido_Paterno").val(); 
							var Apellido_Materno=$("#Apellido_Materno").val(); 
							var Nombre=$("#Nombre").val(); 
							var Digito_Inicial=$("#Digito_Inicial").val(); 
							var Digito_Final=$("#Digito_Final").val();
							var Id_Empleado=$("#Empleado_Id").val();
														
							if ($("#nro_documento").val() == "") {  
								$("#nro_documento").focus().after('<span class="error">Ingrese DNI</span>');  
								return false;  
							}
								
							if ($("#Digito_Inicial").val() == "") {  
								$("#Digito_Inicial").focus().after('<span class="error">Ingrese Digito Inicial</span>');  
								return false;  
							}
							
							if ($("#Digito_Final").val() == "") {  
								$("#Digito_Final").focus().after('<span class="error">Ingrese Digito Final</span>');  
								return false;  
							}  								
							
							var parametros =
							{
							"Digito_Inicial" : Digito_Inicial,
							"Digito_Final" : Digito_Final,
							"Id_Empleado" : Id_Empleado,
							"acc" : 'Agregar_Archivero'
							};
			
							$.ajax({
											data:  parametros,
											url:   '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
											type:  'post',
											success:  function (response) {
													$("#resultado").html(response);
											
											}
							
							
							}); 
			});
			
			
			$("#btn_limpiar").click(function(){
				$('#nro_documento').val("");
				$('#Apellido_Paterno').val("");
				$('#Apellido_Materno').val("");
				$('#Nombre').val("");
				$('#Digito_Inicial').val("");
				$('#Digito_Final').val("");

			});

			
		});
		</script>
		</head>
		<body>
		<div style="margin:15px !important">
		<!--------Aqui apareceran los Alert de Jquery------------>
		<div id="resultado">
		</div>
		<!-------------------->
		<h2 >Agregar Archivero</h2>	
			<hr/> 	
			<table>
			<tr>
				<td>DNI</td>
				<td>
				<input type='text' name='nro_documento' id='nro_documento'  required>
				</td>
				<td>
				&nbsp; 
				</td>
				<td>
				<input type="button" class="button" value="Cargar Datos"   name="btn_cargar_datos" id="btn_cargar_datos" />
				</td>
				<td>
				&nbsp; 
				</td>
				<td>
				<input type="button" class="button" value="Limpiar"   name="btn_limpiar" id="btn_limpiar" />
				</td>
			</tr>
			<tr>
				<td>Apellido Paterno</td>
				<td colspan="2">
				<input type='text' name='Apellido_Paterno' id='Apellido_Paterno'   readonly='readonly'    required>
				</td>
			</tr>
			<tr>
				<td>Apellido Materno</td>
				<td colspan="2" ><input type='text' name='Apellido_Materno' id='Apellido_Materno'  readonly='readonly'  required></td>
			</tr>
			<tr>
				<td>Nombres</td>
				<td><input type='text' name='Nombres' id='Nombre'  readonly='readonly'  required></td>
			</tr>
			<tr>
				<td>Digito Inicial</td>
				<td><input type='text' name='Digito_Inicial' id='Digito_Inicial' size="4"  maxlength="2" min=00 max=99    placeholder='00-99' required></td>
			</tr>
			<tr>
				<td>Digito Final</td>
				<td><input type='text' name='Digito_Final' id='Digito_Final' size="4" maxlength="2" min=00 max=99  placeholder='00-99' required></td>
			</tr>
			<tr style="display:none">
				<td>Id Empleado</td>
				<td><input type='text' name='Empleado_Id' id='Empleado_Id'  readonly='readonly'  required></td>	
			</tr>
			<tr>
				<td colspan="2">
				<input type="button" class="buttonS" value="Guardar"   name="btn_cargar_datos" id="btn_Agregar_Archivero" />			
				</td>
			</tr>
		</table>
		</div>
    </body>
</html>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	