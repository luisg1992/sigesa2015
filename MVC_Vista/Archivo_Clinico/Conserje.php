<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<style>
		html,body { 
        	height: 100%;
        	font-family: Helvetica; 

        }
	</style>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.numeric.js"></script>
    <script>
	var salida = [];
	var retorno = [];
	var retorno_emergencia = [];
	
	
	
	//Jquery Inicializacion
    $(document).ready(function() {
		//Funcion para Imprimir Lista de Historias Solicitadas desde una Matricial
		function ImprimirEstadoCuentaConsolidado()
		{
			var FechaInicio = $('#Fecha_Reporte_Inicio').datebox('getValue');
			var FechaFinal = $('#Fecha_Reporte_Final').datebox('getValue');	
			if (FechaInicio == ""   || FechaFinal == "") {  
			$.messager.alert('SIGESA','Ingrese Fecha Validas');
				return false;  
			} 
			var newWin = window.open();
			$.ajax({
				url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=ImprimirReporteConstanciasMedicas',
				type: 'POST',
				dataType: 'json',
				data: {
					FechaInicio 					:FechaInicio,
					FechaFinal						:FechaFinal
				},
				success:function(impresion)
				{
					newWin.document.write(impresion);
				    newWin.document.close();
				    newWin.focus();
				    newWin.print();
				    newWin.close();
				}
			});
			
		}
		
		//Boton para imprimir de Impresora Matricial
		$("#Generar_Matricial_Reporte_Constancias_Medicas").click(function(e) {
		ImprimirEstadoCuentaConsolidado();
		});
		
		
		
		//  Boton para Generar Excel de Reporte
		$("#Generar_Excel_Reporte_Constancias_Medicas").click(function(e) {
		var htmltable= document.getElementById('Contenido_Excel');
		var html = htmltable.outerHTML;
		window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
		});
		

		
		
		/* Da Formato correcto al easyui  */
		$('.easyui-datebox').datebox({
			formatter : function(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
			},
			parser : function(s){
			if (!s) return new Date();
			var ss = s.split('-');
			var y = parseInt(ss[2],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[0],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d)
			} else {
			return new Date();
			}
			}
		});	

    	//CARGANDO LAS CONFIGURACIONES POR DEFECTO
		$('#Numero_Historia_Clinica_Salida').textbox('clear').textbox('textbox').focus();
		$('#lista_pacientes_salida').dialog('close');
		$('#lista_pacientes_retorno').dialog('close');
		$('#lista_pacientes_retorno_emergencia').dialog('close');
		$("#Numero_Historia_Clinica_Salida").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				Cargar_Citas_Historia_Clinica_Salida();
			}
		});
		$("#Numero_Historia_Clinica_Retorno").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				Cargar_Citas_Historia_Clinica_Retorno();
			}
		});
		
		function Cargar_Pacientes_en_Lista_Retorno(){	
		var dg = $('#ListaPacientes_Retorno').datagrid();
			dg.datagrid({
			fit: true,
			pagination: true,
			singleSelect: true,
			fitColumns: true,
			data: retorno,
			columns: [[
			 {
			 field: 'FechaSolicitud',
			 width: 60,
			 title: 'Fecha de <br>Solicitud'
			 },
			 {
			 field: 'HoraInicio',
			 width: 45,
			 title: 'Hora Inicio'
			 },
			 {
			 field: 'Descripcion',
			 width: 110,
			 title: 'Tipo de Servicio'
			 },
			 {
			 field: 'NombreServicio',
			 width: 150,
			 title: 'Servicio'
			 },
			 {
			 field: 'NroHistoriaClinica',
			 width: 90,
			 title: 'Nro Historia Clinica'
			 },
			 {
			 field: 'NombrePaciente',
			 width: 500,
			 title: 'Nombre de Paciente'
			 }
			 ]]
			 });			
		}
		
		
		
		
		function Cargar_Pacientes_en_Lista_Retorno_Emergencia(){	
		var dg = $('#ListaPacientes_Retorno_Emergencia').datagrid();
			dg.datagrid({
			fit: true,
			pagination: true,
			singleSelect: true,
			fitColumns: true,
			data: retorno_emergencia,
			columns: [[
			 {
			 field: 'FechaSolicitud',
			 width: 60,
			 title: 'Fecha de <br>Solicitud'
			 },
			 {
			 field: 'HoraInicio',
			 width: 45,
			 title: 'Hora Inicio'
			 },
			 {
			 field: 'Descripcion',
			 width: 110,
			 title: 'Tipo de Servicio'
			 },
			 {
			 field: 'NombreServicio',
			 width: 150,
			 title: 'Servicio'
			 },
			 {
			 field: 'NroHistoriaClinica',
			 width: 90,
			 title: 'Nro Historia Clinica'
			 },
			 {
			 field: 'NombrePaciente',
			 width: 500,
			 title: 'Nombre de Paciente'
			 }
			 ]]
			 });			
		}
		
		
		function Cargar_Pacientes_en_Lista_Salida(){	
		var dg = $('#ListaPacientes_Salida').datagrid();
			dg.datagrid({
			fit: true,
			pagination: true,
			singleSelect: true,
			fitColumns: true,
			data: salida,
			columns: [[
			{
			 field: 'FechaSolicitud',
			 width: 60,
			 title: 'Fecha de <br>Solicitud'
			 },
			 {
			 field: 'HoraInicio',
			 width: 45,
			 title: 'Hora Inicio'
			 },
			 {
			 field: 'Descripcion',
			 width: 110,
			 title: 'Tipo de Servicio'
			 },
			 {
			 field: 'NombreServicio',
			 width: 150,
			 title: 'Servicio'
			 },
			 {
			 field: 'NroHistoriaClinica',
			 width: 90,
			 title: 'Nro Historia Clinica'
			 },
			 {
			 field: 'NombrePaciente',
			 width: 500,
			 title: 'Nombre de Paciente'
			 }
			 ]]
			 });			
		}
		//Funcion para Cargar los Datos basicos del Paciente					
		function Cargar_Citas_Historia_Clinica_Salida(){
		if ($("#Numero_Historia_Clinica_Salida").val() == "") {  
			$.messager.alert('SIGESA','Ingrese Numero de Historia Clinica');
			return false;  
		} 	
		var HistoriaClinica=$("#Numero_Historia_Clinica_Salida").val();
		var FechaCita = $('#Fecha_Cita_Salida').datebox('getValue');		
		$.ajax({
		url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=ConserjeMostrarListaCitas_Salida',
		type: 'POST',
		dataType: 'json',
		data: 
		{
			HistoriaClinica: HistoriaClinica,
			FechaCita:FechaCita
		},
		success: function(resultado_t)
		{
			//Pregunta si hay data en el arreglo	
			if (resultado_t[0].variable == 'M_1') 
			{	
			$('#lista_pacientes_salida').dialog('open');
			var dg =$('#lista_cuentas_paciente_salida').datagrid({
			singleSelect:true,
			rownumbers:true,
			url:'../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=ConserjeMostrarListaCitas_Salida&HistoriaClinica='+HistoriaClinica+'&FechaCita='+FechaCita,
			columns:[[
			{field:'FechaSolicitud',title:'Fecha de <br>Solicitud',width:90},
			{field:'HoraInicio',title:'Hora<br>Inicio',width:70},	
			{field:'Descripcion',title:'Tipo<br>Servicio',width:90},				
			{field:'NombreServicio',title:'Servicio',width:180,sortable:true},
			{field:'NroHistoriaClinica',title:'Nro Historia<br>Clinica',width:80},
			{field:'NombrePaciente',title:'Nombre de <br>Paciente',width:300,sortable:true}
			]],
			onSelect:function(index,row){
				$.messager.confirm('Conserje', 'Estas seguro que desea cargar la Atencion?', function(r){
                if (r)
				{
					$('#lista_pacientes_salida').dialog('close');
					var valueToPush = [];
					valueToPush["FechaSolicitud"]=row.FechaSolicitud;					
					valueToPush["HoraInicio"]=row.HoraInicio;
					valueToPush["NombreServicio"]=row.NombreServicio;
					valueToPush["Descripcion"]=row.Descripcion;
					valueToPush["NroHistoriaClinica"]=row.NroHistoriaClinica;
					valueToPush["NombrePaciente"]=row.NombrePaciente;
					valueToPush["TipoCita"]=row.TipoCita;	
					valueToPush["IdAtencion"]=row.IdAtencion;
					for(var i = 0; i < salida.length; i++) {
					  if(salida[i].IdAtencion == row.IdAtencion) 
						{
						$.messager.alert('SIGESA','Usted ya registro este movimiento');
						return false;  			
						}
					}
					salida.push(valueToPush);
					Cargar_Pacientes_en_Lista_Salida();
					$('#Numero_Historia_Clinica_Salida').textbox('clear').textbox('textbox').focus();
                }
				else
				{
					$('#lista_pacientes_salida').dialog('close');
					$('#Numero_Historia_Clinica_Salida').textbox('clear').textbox('textbox').focus();
				}
				});
			}
			});		
			}
			else
			{
			$.messager.alert('SIGESA','No existen datos');
			}
		}
		});								
		}
		

		//Funcion para Cargar los Datos basicos del Paciente					
		function Vista_Reporte_Constancias_Medicas(){
		var FechaInicio = $('#Fecha_Reporte_Inicio').datebox('getValue');
		var FechaFinal = $('#Fecha_Reporte_Final').datebox('getValue');	
		if (FechaInicio == ""   || FechaFinal == "") {  
		$.messager.alert('SIGESA','Ingrese Fecha Validas');
			return false;  
		} 
		
		$.ajax({
		url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=MostrarHC_sin_Retornar_Constancias_Medicas',
		type: 'POST',
		dataType: 'json',
		data: 
		{
			FechaInicio: FechaInicio,
			FechaFinal:FechaFinal
		},

		success: function(resultado_t)
		{
			//Pregunta si hay data en el arreglo	
			if (resultado_t[0].variable == 'M_1') 
			{	
			var dg =$('#ListaPacientes_Retorno_Emergencia').datagrid({
			singleSelect:true,
			url:'../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=MostrarHC_sin_Retornar_Constancias_Medicas&FechaInicio='+FechaInicio+'&FechaFinal='+FechaFinal,
			columns:[[
			{field:'HistoriaClinica',title:'Historia<br>Clinica',width:80},
			{field:'NombrePaciente',title:'Nombre de <br>Paciente',width:300,sortable:true},
			{field:'Fecha_Salida',title:'Fecha de <br>Salida',width:75,sortable:true},
			{field:'Hora_Salida',title:'Hora de <br>Salida',width:60},		
			{field:'Observacion_Salida',title:'Observaciones de <br>Salida',width:550}
			]]
			});		
			}
			else
			{
			$.messager.alert('SIGESA','No existen datos');
			}
		}
		});	
		}
		
		
		
		//Funcion para Cargar los Datos basicos del Paciente					
		function Cargar_Citas_Historia_Clinica_Retorno(){
		if ($("#Numero_Historia_Clinica_Retorno").val() == "") {  
			$.messager.alert('SIGESA','Ingrese Numero de Historia Clinica');
			return false;  
		} 	
		var HistoriaClinica=$("#Numero_Historia_Clinica_Retorno").val();
		var FechaCita = $('#Fecha_Cita_Retorno').datebox('getValue');		
		$.ajax({
		url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=ConserjeMostrarListaCitas_Retorno',
		type: 'POST',
		dataType: 'json',
		data: 
		{
			HistoriaClinica: HistoriaClinica,
			FechaCita:FechaCita
		},
		success: function(resultado_t)
		{
			//Pregunta si hay data en el arreglo	
			if (resultado_t[0].variable == 'M_1') 
			{	
			$('#lista_pacientes_retorno').dialog('open');
			var dg =$('#lista_cuentas_paciente_retorno').datagrid({
			singleSelect:true,
			rownumbers:true,
			url:'../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=ConserjeMostrarListaCitas_Retorno&HistoriaClinica='+HistoriaClinica+'&FechaCita='+FechaCita,
			columns:[[
			{field:'FechaSolicitud',title:'Fecha de  <br>Solicitud',width:90},
			{field:'HoraInicio',title:'Hora<br>Inicio',width:70},	
			{field:'Descripcion',title:'Tipo<br>Servicio',width:90},				
			{field:'NombreServicio',title:'Servicio',width:180,sortable:true},
			{field:'NroHistoriaClinica',title:'Nro Historia<br>Clinica',width:80},
			{field:'NombrePaciente',title:'Nombre de <br>Paciente',width:300,sortable:true}
			]],
			onSelect:function(index,row){
				$.messager.confirm('Conserje', 'Estas seguro que desea cargar la Atencion ?', function(r){
                if (r)
				{
					$('#lista_pacientes_retorno').dialog('close');
					var valueToPush = [];	
					valueToPush["FechaSolicitud"]=row.FechaSolicitud;						
					valueToPush["HoraInicio"]=row.HoraInicio;
					valueToPush["Descripcion"]=row.Descripcion;
					valueToPush["NombreServicio"]=row.NombreServicio;
					valueToPush["NroHistoriaClinica"]=row.NroHistoriaClinica;
					valueToPush["NombrePaciente"]=row.NombrePaciente;
					valueToPush["TipoCita"]=row.TipoCita;	
					valueToPush["IdAtencion"]=row.IdAtencion;
					for(var i = 0; i < retorno.length; i++) {
					  if(retorno[i].IdAtencion == row.IdAtencion) 
						{
						$.messager.alert('SIGESA','Usted ya registro este movimiento');
						return false;  			
						}
					}
					retorno.push(valueToPush);
					Cargar_Pacientes_en_Lista_Retorno();
					$('#Numero_Historia_Clinica_Retorno').textbox('clear').textbox('textbox').focus();
                }
				else
				{
					$('#lista_pacientes_retorno').dialog('close');
					$('#Numero_Historia_Clinica_Retorno').textbox('clear').textbox('textbox').focus();
				}
				});
			}
			});		
			}
			else
			{
			$.messager.alert('SIGESA','No existen datos');
			}
		}
		});								
		}
		
		
		//09-08-2018  Imprimir Reporte - Lista de Historias Clinicas ( Constancias Medicas )
		
		function Generar_Word_Reporte_Constancias_Medicas()
		{   var  HistoriaClinica='555';
			var newWin = window.open();
			$.ajax({
				url: '../../MVC_Controlador/Facturacion/Archivo_ClinicoC.php?acc=ImprimirReporteConstanciasMedicas',
				type: 'POST',
				dataType: 'json',
				data: {
					HistoriaClinica:HistoriaClinica
				},
				success:function(impresion)
				{
					newWin.document.write(impresion);
				    newWin.document.close();
				    newWin.focus();
				    newWin.print();
				    newWin.close();
				}
			});	
		}

		//Funcion para Cargar los Datos basicos del Paciente					
		function Cargar_Citas_Historia_Clinica_Retorno_Emergencia(){
		if ($("#Numero_Historia_Clinica_Retorno_Emergencia").val() == "") {  
			$.messager.alert('SIGESA','Ingrese Numero de Historia Clinica');
			return false;  
		} 	
		var HistoriaClinica=$("#Numero_Historia_Clinica_Retorno_Emergencia").val();	
		$.ajax({
		url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=MostrarAtencionesEmergencia_Retorno',
		type: 'POST',
		dataType: 'json',
		data: 
		{
			HistoriaClinica: HistoriaClinica
		},
		success: function(resultado_t)
		{
			//Pregunta si hay data en el arreglo	
			if (resultado_t[0].variable == 'M_1') 
			{	
			$('#lista_pacientes_retorno_emergencia').dialog('open');
			var dg =$('#lista_atenciones_paciente_retorno_emergencia').datagrid({
			singleSelect:true,
			rownumbers:true,
			url:'../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=MostrarAtencionesEmergencia_Retorno&HistoriaClinica='+HistoriaClinica,
			columns:[[
			{field:'FechaSolicitud',title:'Fecha de  <br>Solicitud',width:90},
			{field:'HoraInicio',title:'Hora<br>Inicio',width:60},			
			{field:'NombreServicio',title:'Servicio',width:220,sortable:true},
			{field:'NroHistoriaClinica',title:'Nro Historia<br>Clinica',width:80},
			{field:'NombrePaciente',title:'Nombre de <br>Paciente',width:300,sortable:true}
			]],
			onSelect:function(index,row){
				$.messager.confirm('Conserje', 'Estas seguro que desea cargar la Atencion ?', function(r){
                if (r)
				{
					$('#lista_pacientes_retorno_emergencia').dialog('close');
					var valueToPush = [];	
					valueToPush["FechaSolicitud"]=row.FechaSolicitud;						
					valueToPush["HoraInicio"]=row.HoraInicio;
					valueToPush["Descripcion"]=row.Descripcion;
					valueToPush["NombreServicio"]=row.NombreServicio;
					valueToPush["NroHistoriaClinica"]=row.NroHistoriaClinica;
					valueToPush["NombrePaciente"]=row.NombrePaciente;
					valueToPush["TipoCita"]=row.TipoCita;	
					valueToPush["IdAtencion"]=row.IdAtencion;
					for(var i = 0; i < retorno_emergencia.length; i++) {
					  if(retorno_emergencia[i].IdAtencion == row.IdAtencion) 
						{
						$.messager.alert('SIGESA','Usted ya registro este movimiento');
						return false;  			
						}
					}
					retorno_emergencia.push(valueToPush);
					Cargar_Pacientes_en_Lista_Retorno_Emergencia();
					$('#Numero_Historia_Clinica_Retorno').textbox('clear').textbox('textbox').focus();
                }
				else
				{
					$('#lista_pacientes_retorno_emergencia').dialog('close');
					$('#Numero_Historia_Clinica_Retorno').textbox('clear').textbox('textbox').focus();
				}
				});
			}
			});		
			}
			else
			{
			$.messager.alert('SIGESA','No existen datos');
			}
		}
		});								
		}
		
		
		//BOTON AGREGAR MOVIMIENTO DE CONSERJE SALIDA
		$("#AceptarMovimientoConserje_Salida").click(function(event) {
			Cargar_Citas_Historia_Clinica_Salida();
		});
		
		
		

		

		//BOTON AGREGAR MOVIMIENTO DE CONSERJE RETORNO
		$("#AceptarMovimientoConserje_Retorno").click(function(event) {
			Cargar_Citas_Historia_Clinica_Retorno();
		});
		
		
		
		//BOTON AGREGAR MOVIMIENTO DE CONSERJE RETORNO EMERGENCIA
		$("#AceptarMovimientoConserje_Retorno_Emergencia").click(function(event) {
			Cargar_Citas_Historia_Clinica_Retorno_Emergencia();
		});
		

		//BOTON CARGAR MOVIMIENTO DE CONSERJE SALIDA
		$("#CargarMovimientoConserje_Salida").click(function(event) {
			Agregar_Movimientos_Conserje_Salida();
		});
		

		//BOTON CARGAR MOVIMIENTO DE CONSERJE RETORNO
		$("#CargarMovimientoConserje_Retorno").click(function(event) {
			Agregar_Movimientos_Conserje_Retorno();
		});
		
		//BOTON CARGAR MOVIMIENTO DE CONSERJE RETORNO
		$("#Generar_Vista_Reporte_Constancias_Medicas").click(function(event) {
			Vista_Reporte_Constancias_Medicas();
		});
		
		
				
		//BOTON AGREGAR MOVIMIENTO DE CONSERJE SALIDA
		$("#Generar_Word_Reporte_Constancias_Medicas").click(function(event) {
			alert('saas');
			Generar_Word_Reporte_Constancias_Medicas();
		});
		
		
		// 
		$("#AceptarMovimientoConserje_Salida").click(function(event) {
			Cargar_Citas_Historia_Clinica_Salida();
		});
		
		
		// Agregar Detalle Antimicrobiano
		function Agregar_Movimientos_Conserje_Salida(){
		if(salida.length==0)
		{
			$.messager.alert('SIGESA','No existe registros');
			false;
		}
		else
		{
			var rows=$('#ListaPacientes_Salida').datagrid('getRows');
			for(i=0;i<rows.length;i++)
			{
			var row = rows[i];
			var IdAtencion=row.IdAtencion;
			var	IdEmpleado=$("#IdEmpleado").val();
			$.ajax({
			url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=Generar_Salida_Conserje_Arreglo',
			type: 'POST',
			dataType: 'json',
			data: {
			IdEmpleado:IdEmpleado,
			IdAtencion:IdAtencion
			}
			});
			}
			//Limpiar los Arreglos
			$('#ListaPacientes_Salida').datagrid('loadData',[]);
			salida.length=0;
			$('#Numero_Historia_Clinica_Salida').textbox('clear').textbox('textbox').focus();
			$.messager.alert('SIGESA','Se generaron las Salidas Satisfactoriamente');	
		}
		}



		// Agregar Detalle Antimicrobiano
		function Agregar_Movimientos_Conserje_Retorno(){
		if(retorno.length==0)
		{
			$.messager.alert('SIGESA','No existe registros');
			false;
		}
		else{
			var rows=$('#ListaPacientes_Retorno').datagrid('getRows');
			for(i=0;i<rows.length;i++)
			{
			var row = rows[i];
			var IdAtencion=row.IdAtencion;
			var	IdEmpleado=$("#IdEmpleado").val();
			$.ajax({
			url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php?acc=Generar_Retorno_Conserje_Arreglo',
			type: 'POST',
			dataType: 'json',
			data: {
			IdEmpleado:IdEmpleado,
			IdAtencion:IdAtencion
			}
			});
			}
			$('#ListaPacientes_Retorno').datagrid('loadData',[]);
			retorno.length=0;
			$('#Numero_Historia_Clinica_Retorno').textbox('clear').textbox('textbox').focus();
			$.messager.alert('SIGESA','Se generaron los Retornos  Satisfactoriamente');	
		}		
		}		
		});
		
	
		function cellStyler(value,row,index){
			return 'background-color:#ffee00;color:red;';
		}
    </script>
</head>
<body>

    <div class="easyui-tabs" style="width:100%;height:100%">
				<div title="Salida de Conserje">
				<div class="easyui-panel"  style="width:100%;height:700px;">
					<div class="easyui-layout" data-options="fit:true">
								<!-- CONTENEDOR DE OPERACIONES -->
								<div data-options="region:'north',split:true" style="height:11%;">
									<div class="easyui-layout" data-options="fit:true">
										<!-- CONTENEDOR: BUSQUEDAS -->
										<div data-options="region:'west'" style="width:100%;height:100%;padding:1%;" >
											<table>
												<tr style="display:none">
												<td><input  type="hidden"  name="IdEmpleado"  id="IdEmpleado" value="<?php echo $_REQUEST["IdEmpleado"]; ?>"></td>
												</tr>
												<tr>
													<td>N° Historia Clinica</td>
													<td>&nbsp;</td>
													<td><input id="Numero_Historia_Clinica_Salida" class="easyui-textbox" type="text"  data-options="prompt:'N° HISTORIA CLINICA'"></td>
													<td>&nbsp;</td>
													<td>Fecha de Cita</td>
													<td>&nbsp;</td>
													<td><input id="Fecha_Cita_Salida" 	class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;"></td>
													<td>&nbsp;</td>
													<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:110px" id="AceptarMovimientoConserje_Salida">Aceptar</a></td>
													<td>&nbsp;</td>
													<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:180px" id="CargarMovimientoConserje_Salida">Generar Salida de HC</a></td>
													<td>&nbsp;</td>
												</tr>
											</table>		
										</div>
									</div>
								</div>
								<!-- CUENTAS A SELECCIONAR -->
								<div data-options="region:'center'"  style="width:70%;height:100%;padding:1%;" >
									<div style="clear:both;height:500px;width:1500px;">
									<table id="ListaPacientes_Salida"></table>
									</div>
								</div>
						
					</div>
				</div>
				<div id="lista_pacientes_salida" class="easyui-dialog" title="Lista de Pacientes" data-options="iconCls:'icon-save'" style="width:800px;height:500px;padding:10px">
					<table id="lista_cuentas_paciente_salida"></table>
				</div>
				</div>
		
		        <div title="Retorno de Conserje">
            		<div class="easyui-panel"  style="width:100%;height:700px;">
					<div class="easyui-layout" data-options="fit:true">
								<div data-options="region:'north',split:true" style="height:11%;">
									<div class="easyui-layout" data-options="fit:true">
										<!-- CONTENEDOR: BUSQUEDAS -->
										<div data-options="region:'west'" style="width:100%;height:100%;padding:1%;" >
											<table>
												<tr style="display:none">
												</tr>
												<tr>
													<td>N° Historia Clinica</td>
													<td>&nbsp;</td>
													<td><input id="Numero_Historia_Clinica_Retorno" class="easyui-textbox" type="text"  data-options="prompt:'N° HISTORIA CLINICA'"></td>
													<td>&nbsp;</td>
													<td>Fecha de Cita</td>
													<td>&nbsp;</td>
													<td><input id="Fecha_Cita_Retorno" 	class="easyui-datebox" ></td>
													<td>&nbsp;</td>
													<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:110px" id="AceptarMovimientoConserje_Retorno">Aceptar</a></td>
													<td>&nbsp;</td>
													<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:180px" id="CargarMovimientoConserje_Retorno">Generar Retorno de HC</a></td>
													<td>&nbsp;</td>
												</tr>
											</table>		
										</div>
									</div>
								</div>
								<!-- CUENTAS A SELECCIONAR -->
								<div data-options="region:'center'"  style="width:70%;height:100%;padding:1%;" >
									<div style="clear:both;height:500px;width:1500px;">
									<table id="ListaPacientes_Retorno"></table>
									</div>
								</div>	
					</div>
				</div>
				<div id="lista_pacientes_retorno" class="easyui-dialog" title="Lista de Pacientes" data-options="iconCls:'icon-save'" style="width:800px;height:500px;padding:10px">
					<table id="lista_cuentas_paciente_retorno"></table>
				</div>
        </div>
		
		
		
		 <div title="Reporte de Retorno - Constancias Medicas ">
            		<div class="easyui-panel"  style="width:100%;height:700px;">
					<div class="easyui-layout" data-options="fit:true">
								<div data-options="region:'north',split:true" style="height:11%;">
									<div class="easyui-layout" data-options="fit:true">
										<!-- CONTENEDOR: BUSQUEDAS -->
										<div data-options="region:'west'" style="width:100%;height:100%;padding:1%;" >
											<table>
												<tr style="display:none">
												</tr>
												<tr>
													<td>Fecha Inicio</td>
													<td>&nbsp;</td>
													<td><input id="Fecha_Reporte_Inicio" class="easyui-datebox" ></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>Fecha Final</td>
													<td>&nbsp;</td>
													<td><input id="Fecha_Reporte_Final" class="easyui-datebox" ></td>
													<td>&nbsp;</td>
													<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:110px" id="Generar_Vista_Reporte_Constancias_Medicas">Vista Previa</a></td>
													<td>&nbsp;</td>
													<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:130px" id="Generar_Excel_Reporte_Constancias_Medicas">Generar Excel</a></td>
													<td>&nbsp;</td>
													<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:130px" id="Generar_Matricial_Reporte_Constancias_Medicas">Generar Reporte</a></td>
													<td>&nbsp;</td>
												</tr>
												
											</table>											
										</div>
									</div>
								</div>
								<!-- CUENTAS A SELECCIONAR -->
								<div data-options="region:'center'"  style="width:70%;height:100%;padding:1%;" >
									<div style="clear:both;height:500px;width:1500px;">
									<div  id="Contenido_Excel">
									<table id="ListaPacientes_Retorno_Emergencia"></table>
									</div>
									</div>
								</div>	
					</div>
				</div>	
        </div>
		
		
		
    </div>

</body>
</html>