<html>
<head>
<meta charset="utf-8">
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
<!--------  Jquery Alert Dialog------------>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
<!------------------------------>
<!-------- Css Alert Dialog------------>
<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
<!------------------------------>
&nbsp;
<?php
switch ($mensaje) {
    case "Archivero_Existente":
			?>
				<script>
					$.alertx('Informacion Archivo Clinico','El Personal Ingresado ya es Archivero');
				</script>
		   <?php
        break;
    case "Archivero_Agregado":
			?>
				<script>
					$.alertx('Informacion Archivo Clinico','Archivero Registrado Satisfactoriamente');
				</script>
		   <?php
        break;
    case "Digito_Terminal_No_Valido":
     		?>
				<script>
					$.alertx('Informacion Archivo Clinico','Ingreso Digito Terminales no Validos');
				</script>
		   <?php
		break;
	case "Archivero_sin_Digitos_Terminales":
     		?>
				<script>
					$.alertx('Informacion Archivo Clinico','Usuario debe ser Registrado como Archivero');
				</script>
		   <?php
		break;
	case "Historia_Clinica_Sin_Pagar":
     		?>
				<script>
					$.alertx('Informacion Archivo Clinico','Esta Cita no ah sido Pagada ');
				</script>
		   <?php
	break;
    default:
        	?>
				<script>
					$.alertx('Info Title','This is an alert message');
				</script>
		   <?php
}
?>
