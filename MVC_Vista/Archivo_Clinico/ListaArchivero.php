<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>

<!--------  Jquery Alert Dialog------------>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
<!------------------------------>
<!-------- Css Alert Dialog------------>
<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
<!------------------------------>

	<script type="text/javascript">
	$(function(){
		<!--   Funcion para eliminar un archivero de la Lista  -->
		$(".Eliminar_Archivero").click(function()
		{
			var id = $(this).attr("id");
			var IdEmpleado = id;
			var Accion = 'Eliminar_Archivero';
			var parent = $(this).parent("td").parent("tr");		
			$.confirmx('Confirmacion','Esta Seguro que eliminara Registro?',function(dialogx)
			{
			console.log('Confirm');
				$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php', {'IdEmpleado':IdEmpleado,'acc':Accion}, function(data)
				{
					parent.fadeOut('slow');
				});	
			return true;
			},function(dialogx)
			{ console.log('Cancelar'); return true; });
			
			return false;		
		});	
	});
	</script>
<body>
   <table width="940" border="1" cellpadding="0" cellspacing="0" class="data">
     <tr>
       <th width="10" align="center">N&deg; Id</th>
       <th width="140" align="center">Nº DNI</th>
       <th width="220" align="center">Apellido Paterno</th>
       <th width="220" align="center">Apellido Materno</th>
       <th width="220" align="center">Nombres</th>     
       <th width="50" align="center">Digito Inicial</th>
       <th width="50" align="center">Digito Final</th>       
       <th width="50" align="center">Opcion</th>
     </tr>   
    <?php 
	  if($ListarArchiveros != NULL){ 
	  foreach($ListarArchiveros as $item){
      
    ?>
     <tr> 
       <td><a><?php echo  $item["Correlativo"];?></a></td>
	   <td align="center"><?php echo  $item["DNI"];?></td>
	   <td align="center"><?php echo  $item["ApellidoPaterno"];?></td>
       <td align="center"><?php echo  $item["ApellidoMaterno"];?></td>
       <td align="center"><?php echo  $item["Nombres"];?></td>
	   <td align="center">
	    <div class="grid_content editable">
		<span style="Color:red; font-weight:bold;font-size:16px"><?php echo $item["digito_inicial"]; ?></span>
		<input  size="4" type="text" class="digito_inicial" name="<?php echo $item["IdEmpleado"]; ?>" value="<?php echo $item["digito_inicial"]; ?>" />
		</div>
	   </td>
	   <td align="center">
	    <div class="grid_content editable">
		<span style="Color:red; font-weight:bold;font-size:16px"><?php echo $item["digito_final"]; ?></span>
		<input  size="4" type="text" class="digito_final" name="<?php echo $item["IdEmpleado"]; ?>" value="<?php echo $item["digito_final"]; ?>" />
		</div>
	   </td>
	   <td align="center"> <a href="#"  id="<?php echo $item['IdEmpleado']; ?>" class="Eliminar_Archivero"><img src="../../MVC_Complemento/img/close.png" width="16" height="16" /></a></td>	   
	   </tr>
	   <?php } }else{ ?>
	   <tr>
	   <td colspan="7" align="center" bgcolor="#FFFFFF" class="alert_error">NO SE ENCONTRÓ NINGÚN REGISTRO </td>
	   </tr>
  <?php 
	}  
  ?>       
   </table>
</body>
</html>