<script type="text/javascript" src="../../MVC_Complemento/js/title.js"></script>
<body> 
	<table width="950" border="1"  class="data" id="mytable">
     <?php 
	  if($Listar_Salida_HC != NULL){ 
	  foreach($Listar_Salida_HC as $item)
	   {
			$checked = '';
			if(!empty($item["Salida_HC"])) 
			{
			 $checked = 'checked disabled';
			 $color="black";
			}
			else
			{
			 $color="red";
			}
      ?>
	<tr> 
       <td align="center" style="color:<?php echo  $color;?>">
	   <span>
	   <b><?php echo  $item["Correlativo"];?></b>
	   </span>
	   </td>
	   <td align="center" style="color:<?php echo  $color;?>">
	   <span>
	   <?php echo  $item["Nombre"]."<b> ( ".substr($item["Descripcion"], 0, 4)." )</b>";?></span>
	   </td>
       <td style="color:<?php echo  $color;?>">
	   <span>
	   <a rel="tooltip" title="<font color='red'><b><i>Fuente de Financiamiento :</i></b></font><br>
	   <?php echo  $item["Descripcion"];?><br>
	   <font color='red'><b><i>Nro de DNI :</i></b></font><br>
	   <?php echo  $item["NroDocumento"];?><br>
	   <font color='red'><b><i>Fecha de Cita :</i></b></font><br>
	   <?php echo  $item["Fecha"];?><br>
	   <font color='red'><b><i>Fecha de Solicitud :</i></b></font><br>
	   <?php echo  $item["Fecha_Solicitud"];?><br>
	   <font color='red'><b><i>Hora de Solicitud :</i></b></font><br>
	   <?php echo  $item["Hora_Solicitud"];?><br>
	   <font color='red'><b><i>Estado de Cita:</i></b></font><br>
	   <?php echo  strtoupper($item["EstadoCita"]);?><br>
	   <font color='red'><b><i>Nombre de Medico:</i></b></font><br>
	   <?php echo  strtoupper($item["NombreMedico"]);?><br>
	   ">
	   <center><b><?php echo  $item["NroHistoriaClinica"];?></b></center>
	   </a>
	   </span>
	   </td>
       <td align="center"  style="color:<?php echo  $color;?>">
	   <span class="nombre">
	   <?php echo  $item["NombreCompleto"];?>
	   </span>
	   </td>
	   <td align="center" >
	   <input type="checkbox" value="<?php echo $item["IdAtencion"];?>"   id="active_<?php echo $item["IdAtencion"];?>"   class="IdAtencion"
	   <?php echo $checked; ?> />
	   </td>        
	   <td align="center" style="width:300px;color:<?php echo  $color;?>">
	   <span>
	    <div class="grid_content editable">
		<span><?php echo $item["Observacion_Salida"]; ?></span>
		<input  style="width:200px" type="text" class="gridder_input" name="<?php echo $item["IdAtencion"]; ?>" value="<?php echo $item["Observacion_Salida"]; ?>" />
		</div>
	   </span>
	   </td>
       
    </tr>
      <?php } }else{ ?>
     <tr>
       <td colspan="7" align="center" bgcolor="#FFFFFF" class="alert_error">NO SE ENCONTRÓ NINGÚN REGISTRO </td>
     </tr>
  <?php 
}  
  ?>      
      
   </table>
</body>
</html>