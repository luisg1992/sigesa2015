<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	
	
	 <!-- Script de Validacion  --->
	 <link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
     <script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	 
	 <!-- Fin Script de Validacion  --->
	 
	 
	 <!-- Script de Archivo Clinico  --->
	 <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
	 <!-- Fin Script de Archivo Clinico  --->
	 
     <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">

	<script type="text/javascript">
	
				$( function()
				{
				//Al hacer Click en el Boton Cargar_Dato llamara a la funcion Cargar_Movimientos
					$( '#Movimiento_Historia_Clinica' ).click(function()
					{
					Cargar_Movimientos(); 
					$("#NroHistoriaClinica").val("");						
					});
				});

				$(document).keypress(function(e) 
				{
				//Al hacer Enter llamara a la funcion v	
				if(e.which == 13) 
					{
					Cargar_Movimientos(); 
					$("#NroHistoriaClinica").val("");	
					}
				});
				
				function Cargar_Movimientos(){
					
				if ($("#NroHistoriaClinica").val() == "") 
				{  
				$("#NroHistoriaClinica").focus().after('<span class="error">Ingrese Historia Clinica</span>');  
				return false;  
				}
				
				var gridder = $('#lineaResultado');
				var UrlToPass = {
						NroHistoriaClinica: $('#NroHistoriaClinica').val(),
						acc : 'Movimiento_Historia_Clinica'
				};

				$.ajax({
					url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
					type : 'POST',
					data : UrlToPass,
					success: function(responseText) {
						gridder.html(responseText);
					}
				});	
									
				}
	
	
				$(function(){

					$("#Agregar_Archivero").click(function Agregar_Archivero()
					{

						 $("#lista_archiveros").hide();
						 $("#contenedor").load('../../MVC_Vista/Archivo_Clinico/Agregar_Archivero.php');
					});
	
	});
	
	

	function Limpiar()
	{

				document.getElementById("NroHistoriaClinica").value="";
				document.getElementById("NroHistoriaClinica").focus();
	}
		

	/*	
	function recargar()
	{	
		var acc='Mostrar_Ultima_HC';
		$("#recargado").fadeOut(function() {
			$.post("../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php", { acc: acc }, function(data){	
			$("#recargado").html(data).fadeIn();
			});			
		});
	}
	$actual=0;
	timer = setInterval("recargar()", 2000);
	
	*/
		
	$(window).load(function() {
	 $("#NroHistoriaClinica").focus();
	});
	

	</script>


 </head>
 <body>
  <!-- Lista de Movimientos Historia Clinica  ---->
  <div style="margin:15px !important">
  <h1>Seguimiento de Historia Cl&iacutenica</h1>
  <div  id="lista_archiveros">
    <table width="250" border="0">
    <tr>
      <td width="250"><table width="100%">
        <tr>
          <td width="66%"><fieldset>
            <legend><H5>Ultima Historia Clinica : </H5></legend>
            <table width="70%">
                <tr>
			    <td width="10%">&nbsp;</td>
                <td width="50px"></td>
                <td  width="1%">
				<BR>
				<h1><span id="recargado"></span></h1>
				</td>
				</tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
          </fieldset></td>
          </tr>
    </table>
	</tr>
  </table>
  <br>
  <table width="800" border="0">
    <tr>
      <td width="800"><table width="100%">
        <tr>
          <td width="66%"><fieldset>
            <legend>Buscar</legend>
            <table width="70%">
                <tr>
			    <td width="3%">&nbsp;</td>
                <td width="10%">Nro Historia Clinica</td>
                <td  width="1%"><input name="NroHistoriaClinica" type="text" id="NroHistoriaClinica" size="30" onkeyup="if(validateEnter(event) == true) { alert(this.value); }" ></td>
                <td  width="1%"><img src="../../MVC_Complemento/img/botonbuscar.jpg" width="69" height="22" id="Movimiento_Historia_Clinica" onMouseOver="style.cursor=cursor"></td>
                <td align="center" valign="top" width="1%" ><img src="../../MVC_Complemento/img/botonlimpiar.jpg" alt="" width="69" height="22" onClick="Limpiar();"></td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
          </fieldset></td>
          </tr>
    </table>
	</tr>
  </table>
			<table width="970">
			<tr>
			  <td width="970"><fieldset>
				<legend>Movimiento de Historia Clinica</legend>
				<div id="scroll">
				<div id="lineaResultado">
				</div>
				</div>
			  </fieldset></td>
			</tr>
		  </table>
  </div>
  </div>
  <!--Fin Lista de Archiveros  ---->  
  
  <!--Inicio de Contenedor  ---->  
  <div style="margin:15px !important">
  <div  id="contenedor">
  </div>
  </div>
  <!--Fin de Contenedor  ----> 
  
  
  
  <p>&nbsp;</p>
  <p>&nbsp;</p>
 </body>
 