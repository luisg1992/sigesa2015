<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
	<!DOCTYPE html>
	<html lang="en">
	<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>	 
	<!-- Script de Archivo Clinico  --->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
	<!-- Fin Script de Archivo Clinico  --->	
	</head>	
	<body>
	<form id="form_busqueda" name="form_busqueda" method="post" action="../../MVC_Vista/Archivo_Clinico/Reporte_Word/Reporte_HC_Dependencia.php"> 
	<div style="margin:15px !important">
    <h1>Reporte por Dependencia</h1>
		<table width="1040" border="0">
		<tr>
		  <td width="994"><table width="100%">
			<tr>
			  <td width="76%"><fieldset>
				<legend>Salida de Historia Clinica a Servicios</legend>
				<table width="800" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td colspan="8">
					<table width="100%">
					  <tr style="display:none">
					  <td><input  type="text"  name="IdEmpleado"  id="IdEmpleado" value="<?php echo $_REQUEST["IdEmpleado"]; ?>"></td>
					  </tr>
					  <tr>
					  <td width="3%">&nbsp;</td>
					  <td width="15%">Servicio / Consultorio</td>
					  <td width="12%">
					  	<select name="Servicio" id="Servicio" style="margin:10px 10px -9px 0px;">
                          <option value="0" >Todos los Servicios</option>
                          <?php 
                          if($ServiciosSeleccionarPorTipo != NULL)  { 
                          foreach($ServiciosSeleccionarPorTipo as $item2){?>
                              <option value="<?php echo $item2["IdServicio"]?>"<?php if($item2["IdServicio"]==$IdTipoServicio){?>selected<?php }?>><?php echo $item2["Servicio"]?></option>
                          <?php }}?>
						</select>
					  </td>
					  <td width="38%">Turno</td>
					  <td width="16%">
								<select name="Turno"   id="Turno" style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="%mañ%">Ma&ntilde;ana</option>
									<option value="%tar%">Tarde</option>
								</select>
					  </td>
					  </tr>
					  <tr>
						<td width="3%">&nbsp;</td>
						<td width="10%">Fecha</td>
						<td width="36%">
						<div style="margin-top:10px">						
						<input  style="margin:15px 10px -9px 0px;" type="text"  name="Fecha_Citas"  id="Fecha_Citas" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="15"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Citas",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador1"     // el id del botón que lanzará el calendario 
											}); 
										</script>
						</div>						
						</td>
						<td width="38%">Tipos de Citas</td>
						<td width="14.5%">
								<select name="Tipo_cita"  id="Tipo_cita" style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="citados">Citados</option>
									<option value="citados_dia">Consultas del Dia</option>
								</select>			
						</td>
						<td width="2%">&nbsp;</td>
						<td width="3%"><input type="submit" value="Generar Reporte"  class="cargar_dato"></td>
						<td width="2%">&nbsp;</td>
						<td > 
						</td>
						</tr>
					</table>
					</td>
					 <!-- Aqui estan las funciones propias de jcal para poder Cargar las Fechas de Rango-->
					  <script type="text/javascript">
					  RANGE_CAL_1 = new Calendar({
							  inputField: "Fecha_Citas",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaInicio_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
							  RANGE_CAL_2 = new Calendar({
							  inputField: "FechaFin",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaFinal_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
					</script>
				  </tr>
				  <tr>
				  <td>&nbsp;</td>
				  </tr>
				</table>
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				</tr>
				</table>
		</table>		
				
	</div>
	</form>	
	</body>
	</html>

