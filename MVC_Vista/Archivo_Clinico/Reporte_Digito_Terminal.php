<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
	<!DOCTYPE html>
	<html lang="en">
	<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	<!-- Script para Hora -->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.ptTimeSelect.css">
	<script src="../../MVC_Complemento/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/jquery.ptTimeSelect.js" type="text/javascript"></script>
	<!-- Fin Script para Hora -->
	</head>	
	<body>
	<div style="margin:15px !important">
	<?php
	  if($ListarDigitoTerminales != NULL)
	  {
	  $IdEmpleado=$_REQUEST["IdEmpleado"];
		  foreach($ListarDigitoTerminales as $digito)
		  { 
		  $digito_inicial=$digito["digito_inicial"];
		  $digito_final=$digito["digito_final"];
		  }
	  }	  
    ?>
    <h1>Digito Terminal   ( <?php  echo  $digito_inicial.' - '.$digito_final  ?> )</h1>
	<br>
	<form id="form_busqueda" name="form_busqueda" method="post" action="../../MVC_Vista/Archivo_Clinico/Reporte_Word/Reporte_HC_Salida.php">
	  <table width="900" border="0">
		<tr>
		  <td width="900"><table width="100%">
			<tr>
			  <td width="66%"><fieldset>
				<legend>Reporte de HC</legend>
				<table width="900" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td colspan="8">
					<table width="100%">
					  <tr style="display:none">
					  <td><input  type="text"  name="digito_inicial"  id="digito_inicial" value="<?php echo  $digito_inicial ?>"></td>
					  <td><input  type="text"  name="digito_final"  id="digito_final" value="<?php echo $digito_final ?>"></td>
					  <td><input  type="text"  name="IdEmpleado"  id="IdEmpleado" value="<?php echo $IdEmpleado ?>"></td>
					  </tr>
					  <tr>
						<td width="3%">&nbsp;</td>
						<td width="10%">Fecha Citas</td>
						<td width="18%"> 
						<input  type="text"  name="Fecha_Citas"  id="Fecha_Citas" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="15"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />		  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Citas",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador1"     // el id del botón que lanzará el calendario 
											}); 
										</script>	
						</td>
						<td width="10%">Tipos de Citas</td>
						<td width="12.5%">
						<div style="margin-top:10px">
								<select name="Tipo_cita"  id="Tipo_cita" style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="citados">Citados</option>
									<option value="citados_dia">Consultas del Dia</option>
								</select>
						</div>				
						</td>
						<td width="4%">Turno</td>
						<td width="10%">
						<div style="margin-top:10px">
								<select name="Turno"   id="Turno" style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="%mañ%">Ma&ntilde;ana</option>
									<option value="%tar%">Tarde</option>
								</select>
						</div>		
						</td>
						<td>
						&nbsp;		
						</td>
						</tr>
						<tr>
						<td width="3%">&nbsp;</td>
						<td width="10%">Estado</td>
						<td width="18%">
								<select name="Tipo_estado" id="Tipo_estado" style="padding: 5px; margin:10px 10px -9px 0px;">
									<option value="Pendientes">Pendientes</option>
									<option value="Totalidad">Totalidad</option>
								</select>							
						</td>
				   
						<td width="10%">Hora Inicial</td>
						<td width="12.5%">
							<div id="Hora_Inicial" style="margin-top:10px"> 
							<input name="Hora_Inicial" value="06:00 AM" size="8" style="padding: 5px; margin:10px 10px -9px 10px;">
							</div>			
						</td>
						<td width="4%">Hora Final</td>
						<td width="10%">
							<div id="Hora_Final" style="margin-top:10px"> 
							<input name="Hora_Final" value="" size="8" style="padding: 5px; margin:10px 10px -9px 10px;">
							</div>		
						</td>
						<td> 
						<input type="submit" value="Aceptar">
						</td>
						</tr>
					</table>
					</td>
					 <!-- Aqui estan las funciones propias de jcal para poder Cargar las Fechas de Rango-->
					  <script type="text/javascript">
					  RANGE_CAL_1 = new Calendar({
							  inputField: "Fecha_Citas",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaInicio_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
							  RANGE_CAL_2 = new Calendar({
							  inputField: "FechaFin",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaFinal_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
					</script>
					
					<script type="text/javascript">
						$(document).ready(function(){
							$('#Hora_Inicial input').ptTimeSelect();
						});
						
						$(document).ready(function(){
							$('#Hora_Final input').ptTimeSelect();
						});
					</script>
				  </tr>
				  <tr>
				  <td>&nbsp;</td>
				  </tr>
				</table>
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				</tr>
				</table>
	</form>
	</div>
	</body>
	</html>

