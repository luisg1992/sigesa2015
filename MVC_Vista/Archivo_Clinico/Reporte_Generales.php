<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
	<html> 
	<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	</head>	
	<body>
	<script type="text/javascript">
		$(function(){

		
		        /* Funcion que se ejecutara al Load  */
				$(document).ready(function() {
					
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Reporte_HC_sin_Retornar'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});
				});
		
		

				$("#Salida_Digito_Terminal").click(function()
				{
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Reporte_Salida_Digito_Terminal'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});

				});


				$("#HC_sin_Retornar").click(function()
				{
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Reporte_HC_sin_Retornar'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});

				});	
				
				$("#Salida_Digito_Terminal_Detalle").click(function()
				{
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Salida_Digito_Terminal_Detalle'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});

				});	
				
				
								
				$("#Salida_Fuente_Financiamiento").click(function()
				{
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Salida_Fuente_Financiamiento'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});

				});	
				
												
				$("#Salida_Consultorio").click(function()
				{
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Salida_Consultorio'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});

				});	
				
				
												
				$("#Salida_Consultorio_Detalle").click(function()
				{
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Salida_Consultorio_Detalle'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});

				});	
				

				
				
				$("#Reporte_Constancias_Medicas").click(function()
				{
					var IdEmpleado=$("#IdEmpleado").val();
					$("#Vista_de_Reporte").hide();
					var gridder = $('#contenedor');
					var UrlToPass = {
							IdEmpleado:IdEmpleado,
							acc : 'Reporte_Constancias_Medicas'
					};

					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'GET',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});

				});	

				
			
		});
	</script>
	<!--   Reporte de Historias Clinicas sin Retornar  -->
	<ul class="pro15 nover">
	<li><a href="#"  id="HC_sin_Retornar" class="nover" ><em class="nuevo nover"></em><b>HC sin Retornar</b></a></li>
	<li><a href="#"  id="Salida_Digito_Terminal" class="nover" ><em class="nuevo nover"></em><b>HC Dig. Terminal</b></a></li>
	<li><a href="#"  id="Salida_Digito_Terminal_Detalle" class="nover" ><em class="nuevo nover"></em><b>HC Dig. Terminal-Detalle</b></a></li>
	<li><a href="#"  id="Salida_Fuente_Financiamiento" class="nover" ><em class="nuevo nover"></em><b>HC Fuente Financiamiento</b></a></li>
	<li><a href="#"  id="Salida_Consultorio" class="nover" ><em class="nuevo nover"></em><b>HC por Consultorio</b></a></li>
	<li><a href="#"  id="Salida_Consultorio_Detalle" class="nover" ><em class="nuevo nover"></em><b>HC por Consultorio-Detalle</b></a></li>
	<li><a href="#"  id="Reporte_Constancias_Medicas" class="nover" ><em class="nuevo nover"></em><b>Reporte de Constancias Medicas</b></a></li>
	</ul>
	<br>
    <input type="hidden" name="IdEmpleado" id="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'];?>"></th>
	<br>
	<div  id="Vista_de_Reporte">
	Aqui saldra un mensaje 
	</div>
	
	
	  <!--Inicio de Contenedor  ---->  
  <div style="margin:15px !important">
  <div  id="contenedor">
  </div>
  </div>
  <!--Fin de Contenedor  ----> 
	</body>
	</html>

