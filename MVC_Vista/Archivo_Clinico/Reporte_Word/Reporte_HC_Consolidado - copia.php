<?php
		include("../../../MVC_Modelo/ArchivoClinicoM.php");
		include("../../../MVC_Modelo/EmpleadosM.php");
		include("../../../MVC_Complemento/librerias/Funciones.php");	
		include("../../../MVC_Complemento/PHPWord/PHPWord.php");

		
		
		$ListarDatosEmpleado=EmpleadosSeleccionarPorId_M($_REQUEST["IdEmpleado"]);
		if($ListarDatosEmpleado != NULL)	
		{ 
		foreach ($ListarDatosEmpleado as $campos)
			{
				$ApellidoPaterno=$campos["ApellidoPaterno"];
				$ApellidoMaterno=$campos["ApellidoMaterno"];
				$Nombres=$campos["Nombres"];
				$usuario=$ApellidoPaterno.' '.$ApellidoMaterno.' '.$Nombres;
			}
		}
	
		if($_REQUEST["Turno"]=='%mañ%')
		{
		$descripcion_turno="MANANA";
		}
		else
		{
		$descripcion_turno="TARDE";
		}	
		
	    if($_REQUEST['Tipo_cita']=='citados')
		{	
		$tipo='CITADOS';
		}
		
		if($_REQUEST['Tipo_cita']=='citados_dia')
		{	
		$tipo='CITADOS DEL DIA';
		}
		$hora=date("Y-m-d H:i:s");
		// New Word Document
		$PHPWord = new PHPWord(); 
		$section = $PHPWord->createSection();
		// Agregar Cabecera
		$header = $section->createHeader();
		$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'size'=>14));
		$PHPWord->addFontStyle('lStyle', array('bold'=>true, 'size'=>10));
        $PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
		$PHPWord->addParagraphStyle('rightStyle', array('align'=>'right', 'spaceAfter'=>100));
		$table = $header->addTable();
		$table->addRow();
		$table->addCell(4500)->addText('Hospital Nacional Daniel Alcides Carrion');
		/*$table->addCell(6000)->addImage('images/hndac.jpg', array('width'=>64, 'height'=>56, 'align'=>'right'));*/
		// Agregar Pie de Pagina
		$footer = $section->createFooter();
		$footer->addPreserveText('Pagina {PAGE} de {NUMPAGES}.', array('align'=>'center'));

		$ListarServicioyMedicos=Archivo_Clinico_Reporte_Consolidado_Lista_Servicio_y_Medico_M($_REQUEST["Turno"],gfecha($_REQUEST['Fecha_Citas']));
		if($ListarServicioyMedicos != NULL)	
		{ 
			foreach ($ListarServicioyMedicos as $fila)
			{

					$styleTable = array('borderSize'=>6, 'borderColor'=>'ffffff', 'cellMargin'=>10);
					$styleFirstRow = array('borderBottomSize'=>10, 'borderBottomColor'=>'000000', 'bgColor'=>'FFFFFF');
					$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);
					$section->addText('RELACION DE PACIENTES - ARCHIVEROS ( '.strtoupper($tipo).' )', 'rStyle', 'pStyle');
					$section->addText('  DIA :   '.$_REQUEST['Fecha_Citas'].'         TURNO : '.$descripcion_turno.'       FECHA : '.$hora, 'lStyle', 'rightStyle');
					$cabecera = $section->addTable('myOwnTableStyle');
					$cabecera->addRow(-300);
					$cabecera->addCell(12000, $styleCell)->addText('ESPECIALIDAD : '.utf8_decode($fila[1]), $fontStyle);
					$cabecera->addCell(11000, $styleCell)->addText('MEDICO : '.strtoupper(utf8_decode($fila[2])), $fontStyle);					
					$tabla = $section->addTable();
					$tabla->addRow(-300);
					$tabla->addCell(380)->addText('ID', $fontStyle);
					$tabla->addCell(960, $styleCell)->addText(' HC', $fontStyle);
					$tabla->addCell(8000, $styleCell)->addText(' NOMBRES Y APELLIDOS', $fontStyle);
					$tabla->addCell(1800, $styleCell)->addText('TIPO', $fontStyle);
					$tabla->addCell(2000, $styleCell)->addText('OBSERVACION', $fontStyle);		
					$i=0;
					$ListarPacientes_Archivero=Archivo_Clinico_Reporte_Consolidado_Lista_Pacientes_x_Archivero_M($fila[0],$_REQUEST["Turno"],gfecha($_REQUEST['Fecha_Citas']),$_REQUEST['Tipo_cita']);
					if($ListarPacientes_Archivero != NULL)	
					{ 
						foreach ($ListarPacientes_Archivero as $row)
						{
						 $i=$i+1;	
						$tabla->addRow(-300);
						$tabla->addCell(380, $styleCell)->addText(' '.$i, $fontStyle);
						$tabla->addCell(960, $styleCell)->addText(' '.utf8_decode($row[3]), $fontStyle);
						$tabla->addCell(8000, $styleCell)->addText(' '.utf8_decode($row[2]), $fontStyle);
						$tabla->addCell(1800, $styleCell)->addText(' '.utf8_decode($row[12]), $fontStyle);
						$tabla->addCell(2000, $styleCell)->addText('', $fontStyle);	
						}
					}
					$section->addText($value);
					$styleTable = array('borderSize'=>6, 'borderColor'=>'ffffff', 'cellMargin'=>10);
					$styleFirstRow = array('borderBottomSize'=>10, 'borderBottomColor'=>'000000', 'bgColor'=>'FFFFFF');
					$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);
					$section->addText('RELACION DE PACIENTES - CONSULTA EXTERNA ( '.strtoupper($tipo).' )', 'rStyle', 'pStyle');
					$section->addText('  DIA :   '.$_REQUEST['Fecha_Citas'].'         TURNO : '.$descripcion_turno.'       FECHA : '.$hora, 'lStyle', 'rightStyle');
					$cabecera = $section->addTable('myOwnTableStyle');
					$cabecera->addRow(-300);
					$cabecera->addCell(12000, $styleCell)->addText('ESPECIALIDAD : '.utf8_decode($fila[1]), $fontStyle);
					$cabecera->addCell(11000, $styleCell)->addText('MEDICO : '.strtoupper(utf8_decode($fila[2])), $fontStyle);
					$tabla = $section->addTable();
					$tabla->addRow(-300);
					$tabla->addCell(1000)->addText('N.CUPOS', $fontStyle);
					$tabla->addCell(960, $styleCell)->addText(' HC', $fontStyle);
					$tabla->addCell(8000, $styleCell)->addText(' NOMBRES Y APELLIDOS', $fontStyle);
					$tabla->addCell(1800, $styleCell)->addText('TIPO', $fontStyle);
					$tabla->addCell(3000, $styleCell)->addText('NRO. FUA', $fontStyle);		
					$i=0;
					$ListarPacientes_Servicio=Archivo_Clinico_Reporte_Consolidado_Lista_Pacientes_x_Servicio_M($fila[0],$_REQUEST["Turno"],gfecha($_REQUEST['Fecha_Citas']),$_REQUEST['Tipo_cita']);
					if($ListarPacientes_Servicio != NULL)	
					{ 
					foreach ($ListarPacientes_Servicio as $row)
						{
						$i=$i+1;	
						$tabla->addRow(-300);
						$tabla->addCell(1200, $styleCell)->addText(' '.utf8_decode($row[11].'-('.$row[14].')'), $fontStyle);
						$tabla->addCell(1800, $styleCell)->addText(' '.utf8_decode($row[3]), $fontStyle);
						$tabla->addCell(7500, $styleCell)->addText(' '.utf8_decode($row[2]), $fontStyle);
						$tabla->addCell(1800, $styleCell)->addText(' '.utf8_decode($row[12]), $fontStyle);
						$tabla->addCell(3000, $styleCell)->addText(''.utf8_decode($row[13]), $fontStyle);
						}
					}
					$section->addText($value);
					$section->addPageBreak();	
			}
		}
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		$objWriter->save('repositorio/consolidado'.utf8_decode($usuario).'-'.$i.'.docx');
		header("location:repositorio/consolidado".$usuario.'-'.$i.".docx");	
?>