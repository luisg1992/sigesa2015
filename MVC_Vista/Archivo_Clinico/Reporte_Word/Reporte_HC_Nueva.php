<?php
		header("Content-Type: text/html;charset=utf-8");		
		include("../../../MVC_Modelo/ArchivoClinicoM.php");
		include("../../../MVC_Modelo/EmpleadosM.php");
		include("../../../MVC_Complemento/librerias/Funciones.php");	
		include("../../../MVC_Complemento/PHPWord/PHPWord.php");
		
		$ListarDatosEmpleado=EmpleadosSeleccionarPorId_M($_REQUEST["IdEmpleado"]);
		if($ListarDatosEmpleado != NULL)	
		{ 
		foreach ($ListarDatosEmpleado as $campos)
			{
				$ApellidoPaterno=$campos["ApellidoPaterno"];
				$ApellidoMaterno=$campos["ApellidoMaterno"];
				$Nombres=$campos["Nombres"];
				$usuario=$ApellidoPaterno.' '.$ApellidoMaterno.' '.$Nombres;
			}
		}

		// New Word Document
		$PHPWord = new PHPWord();
		// New portrait section
		$section = $PHPWord->createSection(array('marginLeft'=>600, 'marginRight'=>600, 'marginTop'=>500, 'marginBottom'=>600));

		// Add header
		$header = $section->createHeader();
		$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'size'=>14));
		$PHPWord->addFontStyle('lStyle', array('bold'=>true, 'size'=>13));
		$PHPWord->addFontStyle('rStyle2', array('bold'=>true, 'size'=>11));
		$PHPWord->addFontStyle('lStyle2', array('bold'=>true, 'size'=>11));
        $PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
		$PHPWord->addParagraphStyle('gStyle', array('align'=>'center', 'spaceAfter'=>10));
		$PHPWord->addParagraphStyle('rightStyle', array('align'=>'right', 'spaceAfter'=>100));
		$section->addText('HISTORIA CLINICA N '.$_REQUEST["NroHistoriaClinica"], 'rStyle', 'pStyle');
		$table = $header->addTable();
		$table->addRow();
		$table->addCell(4500)->addText('Hospital Nacional Daniel Alcides Carrion');
		$table->addCell(1500)->addText(''.date("Y-m-d"), array('align'=>'left'));
		$table->addRow();
		$table->addCell(25000)->addText(utf8_decode('Sistema de Admision y Archivo                                                                                 '));
		$table->addCell(1500)->addText(''.date("H:i:s"), array('align'=>'left'));
		// Add footer
		$footer = $section->createFooter();
		$footer->addPreserveText('Usuario : '.$usuario, array('align'=>'center'));

		// Define table style arrays
		$styleTable = array('borderSize'=>6, 'borderColor'=>'ffffff', 'cellMargin'=>10);
		$styleFirstRow = array('borderBottomSize'=>2, 'borderBottomColor'=>'000000', 'bgColor'=>'FFFFFF');

		// Define cell style arrays
		$styleCell = array('valign'=>'center');
		$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);

		// Define font style for first row
		$fontStyle = array('bold'=>true, 'align'=>'center');

		// Add table style
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

		// Add table
		$PHPWord->addTableStyle('cabecera', $styleTable, $styleFirstRow);
		
		// Add table
		$PHPWord->addTableStyle('cabeza', $styleFirstRow);

		$Reporte_Historia_Clinica_Nueva=Archivo_Clinico_Reporte_Historia_Clinica_Nueva_M($_REQUEST["NroHistoriaClinica"]);
		if($Reporte_Historia_Clinica_Nueva != NULL)	
		{ 
			foreach ($Reporte_Historia_Clinica_Nueva as $row)
			{
			$section->addText(' Apellidos y Nombres  : '.utf8_decode($row[1]), 'rStyle2', 'pStyle2');
			$table = $section->addTable('myOwnTableStyle');
			// Add row
			$table->addRow(-300);
			// Add cells
			/*
			$table->addCell(165000, $styleCell)->addText(' Apellidos y Nombres -- : '.$row[1].'------------', $fontStyle);
			$table->addRow(-300);*/
			$table->addCell(25000, $styleCell)->addText(' F. Admision : '.$row[2], $fontStyle);
			$table->addRow(-300);
			$table->addCell(25000, $styleCell)->addText(' Fecha Nacimiento : '.$row[3].'          Sexo : '.$row[7], $fontStyle);
			$table->addCell(25000, $styleCell)->addText(' Edad : '.$row[8], $fontStyle);
			$table->addRow(-300);
			$table->addCell(25000, $styleCell)->addText(' Lugar de Nacimiento : '.utf8_decode($row[9].'  '.$row[10].' '.$row[11]), $fontStyle);
			$table->addCell(25000, $styleCell)->addText(' DNI : '.$row[12], $fontStyle);
			$table->addRow(-300);
			$table->addCell(25000, $styleCell)->addText(' Direccion : '.utf8_decode($row[13]), $fontStyle);
			$table->addCell(25000, $styleCell)->addText(' Estado : '.$row[14], $fontStyle);
			$table->addRow(-300);
			$table->addCell(25000, $styleCell)->addText(' Lugar de Residencia : '.utf8_decode($row[4].'  '.$row[5].' '.$row[6]), $fontStyle);
			$table->addCell(25000, $styleCell)->addText(' Telefono : '.$row[15], $fontStyle);
			}
		}
		$cabecera = $section->addTable('cabecera');
		$cabecera->addRow(100);
		$cabecera->addCell(10800, $styleCell)->addText('', $fontStyle);
		$table2 = $section->addTable('cabeza');
		$table2->addRow(900);
		$table2->addCell(25000, $styleCell)->addText(' Diagnostico : ', $fontStyle);
		$section->addText('Hospitalizaciones', 'lStyle', 'gStyle');
		$table2->addRow(900);
		$table2->addCell(25000, $styleCell)->addText('', $fontStyle);
		$table2->addRow(900);
		$table2->addCell(25000, $styleCell)->addText('', $fontStyle);
		$table2 = $section->addTable('cabeza');
		$table2->addRow(900);
		$table2->addCell(5000, $styleCell)->addText(' F. Ingreso                      F. Alta                 Sala Hosp                 Cama           Dias ', $fontStyle);
		$section->addText('Intervenciones Quirurgicas', 'lStyle', 'gStyle');
		$table2->addRow(900);
		$table2->addCell(25000, $styleCell)->addText('', $fontStyle);
		$table2 = $section->addTable('cabeza');
		$table2->addRow(900);
		$table2->addCell(25000, $styleCell)->addText(' F. Opera                                   CMP                        Tip. Anes                               DX                   Observaciones ', $fontStyle);
		$section->addText('Observacion', 'lStyle', array('align'=>'left'));
		$table2->addRow(900);
		$table2->addCell(25000, $styleCell)->addText('', $fontStyle);	
	    $hora= date("h.i.s");
        $fecha=date("d.m.y");
		$i=$fecha.'.'.$hora;
		// Save File
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		$objWriter->save('repositorio/r-'.utf8_decode($usuario).'-'.$i.'.docx');
		header("location:repositorio/r-".$usuario.'-'.$i.".docx");
							
			
?>