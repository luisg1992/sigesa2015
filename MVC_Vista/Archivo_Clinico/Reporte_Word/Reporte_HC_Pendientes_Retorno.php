<?php
		include("../../../MVC_Modelo/ArchivoClinicoM.php");
		include("../../../MVC_Modelo/EmpleadosM.php");
		include("../../../MVC_Complemento/librerias/Funciones.php");	
		include("../../../MVC_Complemento/PHPWord/PHPWord.php");
			
		$ListarDatosEmpleado=EmpleadosSeleccionarPorId_M($_REQUEST["IdEmpleado"]);
		if($ListarDatosEmpleado != NULL)	
		{ 
		foreach ($ListarDatosEmpleado as $campos)
			{
				$ApellidoPaterno=$campos["ApellidoPaterno"];
				$ApellidoMaterno=$campos["ApellidoMaterno"];
				$Nombres=$campos["Nombres"];
				$usuario=$ApellidoPaterno.' '.$ApellidoMaterno.' '.$Nombres;
			}
		}	
			
		$ListarHCPendientesRetorno=Archivo_Clinico_ReporteHC_Pendientes_Retorno_M(gfecha($_REQUEST['Fecha_Inicio']),gfecha($_REQUEST['Fecha_Final']));
		// New Word Document
		$PHPWord = new PHPWord();
		// New portrait section
		$section = $PHPWord->createSection(array('marginLeft'=>600, 'marginRight'=>600, 'marginTop'=>600, 'marginBottom'=>600));
		// Add header
		$header = $section->createHeader();
		$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'size'=>14));
		$PHPWord->addFontStyle('lStyle', array('bold'=>true, 'size'=>10));
        $PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
		$PHPWord->addParagraphStyle('rightStyle', array('align'=>'right', 'spaceAfter'=>100));
		$section->addText('HC SIN RETORNAR', 'rStyle', 'pStyle');
		$section->addText('  DESDE:   '.$_REQUEST['Fecha_Inicio'].'         -     HASTA : '.$_REQUEST['Fecha_Final'], 'lStyle', 'rightStyle');
		$table = $header->addTable();
		$table->addRow();
		$table->addCell(4500)->addText('Hospital Nacional Daniel Alcides Carrion');
		$table->addRow();
		$table->addCell(4500)->addText(''.date("Y-m-d H:i:s"));

		// Add footer
		$footer = $section->createFooter();
		$footer->addPreserveText('Pagina {PAGE} de {NUMPAGES}.', array('align'=>'center'));

		// Define table style arrays
		$styleTable = array('borderSize'=>6, 'borderColor'=>'006699', 'cellMargin'=>10);
		$styleFirstRow = array('borderBottomSize'=>2, 'borderBottomColor'=>'000000', 'bgColor'=>'FFFFFF');

		// Define cell style arrays
		$styleCell = array('valign'=>'center');
		$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);

		// Define font style for first row
		$fontStyle = array('bold'=>true, 'align'=>'center' , 'size'=>11);
        $estilo = array( 'align'=>'center' , 'size'=>10);
		// Add table style
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

		// Add table
		$PHPWord->addTableStyle('cabecera', $styleTable, $styleFirstRow);
		$cabecera = $section->addTable('cabecera');
		$cabecera->addRow(100);
		$cabecera->addCell(10800, $styleCell)->addText(' USUARIO : '.strtoupper(utf8_decode($usuario)).'                        ', $fontStyle);
		
		$table = $section->addTable('myOwnTableStyle');
		// Add row
		$table->addRow(-300);
		// Add cells
		$table->addCell(800)->addText('ID', $fontStyle);
		$table->addCell(1000, $styleCell)->addText('HC', $fontStyle);
		$table->addCell(1000, $styleCell)->addText('TIPO', $fontStyle);
		$table->addCell(4300, $styleCell)->addText('PACIENTE', $fontStyle);
		$table->addCell(3500, $styleCell)->addText('CONSULTORIO', $fontStyle);
		$table->addCell(1500, $styleCell)->addText('FECHA SALIDA', $fontStyle);	

		// Add more rows / cells
	    if($ListarHCPendientesRetorno != NULL)	
		{ 
		$i=0;
		foreach ($ListarHCPendientesRetorno as $row)
			{
				$i=$i+1;			
				$table->addRow(-300);
				$table->addCell(800, $styleCell)->addText(' '.$i, $fontStyle);
				$table->addCell(1000, $styleCell)->addText(' '.utf8_decode($row[0]), $fontStyle);
				$table->addCell(1000, $styleCell)->addText(' '. substr(utf8_decode($row[1]), 0, 4), $fontStyle);
				$table->addCell(4300, $styleCell)->addText(' '.utf8_decode($row[2]), $fontStyle);
				$table->addCell(3500, $styleCell)->addText(' '.utf8_decode($row[3]), $fontStyle);
				$table->addCell(1500, $styleCell)->addText(' '.utf8_decode($row[5]), $fontStyle);			
			}
		}
	    $hora= date("h.i.s");
        $fecha=date("d.m.y");
		$i=$fecha.'.'.$hora;
		// Save File

		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		$objWriter->save('repositorio/hc_sin_retornar-'.utf8_decode($usuario).'-'.$i.'.docx');
		header("location:repositorio/hc_sin_retornar-".$usuario.'-'.$i.".docx");	
		
?>