<?php
		include("../../../MVC_Modelo/ArchivoClinicoM.php");
		include("../../../MVC_Modelo/EmpleadosM.php");
		include("../../../MVC_Complemento/librerias/Funciones.php");	
		include("../../../MVC_Complemento/PHPWord/PHPWord.php");

		
		$ListarDatosEmpleado=EmpleadosSeleccionarPorId_M($_REQUEST["IdEmpleado"]);
		if($ListarDatosEmpleado != NULL)	
		{ 
		foreach ($ListarDatosEmpleado as $campos)
			{
				$ApellidoPaterno=$campos["ApellidoPaterno"];
				$ApellidoMaterno=$campos["ApellidoMaterno"];
				$Nombres=$campos["Nombres"];
				$usuario=$ApellidoPaterno.' '.$ApellidoMaterno.' '.$Nombres;
			}
		}
	
		
		if($_REQUEST["Turno"]=='%mañ%')
		{
		$descripcion_turno="MANANA";
		}
		else
		{
		$descripcion_turno="TARDE";
		}	
		
		if($_REQUEST['Tipo_estado']=='Pendientes')
		{	
		$ListarReporte=Archivo_Clinico_ReporteHC_Pendiente_M(gfecha($_REQUEST['Fecha_Citas']),$_REQUEST["Turno"],$_REQUEST["Tipo_cita"],$_REQUEST["digito_inicial"],$_REQUEST["digito_final"],retornar_hora($_REQUEST['Hora_Inicial']),retornar_hora($_REQUEST['Hora_Final']));
		}
		
		if($_REQUEST['Tipo_estado']=='Totalidad')
		{
		$ListarReporte=Archivo_Clinico_ReporteHC_Totalidad_M(gfecha($_REQUEST['Fecha_Citas']),$_REQUEST["Turno"],$_REQUEST["Tipo_cita"],$_REQUEST["digito_inicial"],$_REQUEST["digito_final"],retornar_hora($_REQUEST['Hora_Inicial']),retornar_hora($_REQUEST['Hora_Final']));
		}

	    if($_REQUEST['Tipo_cita']=='citados')
		{	
		$tipo='CITADOS';
		}
		
		if($_REQUEST['Tipo_cita']=='citados_dia')
		{	
		$tipo='CITADOS DEL DIA';
		}
		

		
		// New Word Document
		$PHPWord = new PHPWord();
		// New portrait section
		$section = $PHPWord->createSection(array('marginLeft'=>600, 'marginRight'=>600, 'marginTop'=>600, 'marginBottom'=>600));
		// Add header
		$header = $section->createHeader();
		$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'size'=>14));
		$PHPWord->addFontStyle('lStyle', array('bold'=>true, 'size'=>10));
        $PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
		$PHPWord->addParagraphStyle('rightStyle', array('align'=>'right', 'spaceAfter'=>100));
		$section->addText('RELACION DE PACIENTES', 'rStyle', 'pStyle');
		$section->addText('    DIA : '.gfecha($_REQUEST['Fecha_Citas']).'      HORA : '.$_REQUEST['Hora_Inicial'].' - '.$_REQUEST['Hora_Final'].' ', 'lStyle', 'rightStyle');
		$table = $header->addTable();
		$table->addRow();
		$table->addCell(4500)->addText('Hospital Nacional Daniel Alcides Carrion');
		$table->addRow();
		$table->addCell(4500)->addText(''.date("Y-m-d H:i:s"));

		// Add footer
		$footer = $section->createFooter();
		$footer->addPreserveText('Pagina {PAGE} de {NUMPAGES}.', array('align'=>'center'));

		// Define table style arrays
		$styleTable = array('borderSize'=>6, 'borderColor'=>'006699', 'cellMargin'=>10);
		$styleFirstRow = array('borderBottomSize'=>2, 'borderBottomColor'=>'000000', 'bgColor'=>'FFFFFF');

		// Define cell style arrays
		$styleCell = array('valign'=>'center');
		$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);

		// Define font style for first row
		$fontStyle = array('bold'=>true, 'align'=>'center' , 'size'=>11);
        $estilo = array( 'align'=>'center' , 'size'=>10);
		// Add table style
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

		// Add table
		$PHPWord->addTableStyle('cabecera', $styleTable, $styleFirstRow);
		$cabecera = $section->addTable('cabecera');
		$cabecera->addRow(100);
		$cabecera->addCell(10800, $styleCell)->addText(' USUARIO : '.strtoupper(utf8_decode($usuario)).'                        '.$tipo.'                 TURNO : '.strtoupper($descripcion_turno), $fontStyle);
		
		$table = $section->addTable('myOwnTableStyle');
		// Add row
		$table->addRow(-300);
		// Add cells
		$table->addCell(380)->addText('IDx', $fontStyle);
		$table->addCell(860, $styleCell)->addText(' HC', $fontStyle);
		$table->addCell(3700, $styleCell)->addText(' CONSULTORIO', $fontStyle);
		$table->addCell(4000, $styleCell)->addText(' NOMBRES Y APELLIDOS', $fontStyle);
		$table->addCell(1300, $styleCell)->addText('  FECHA REG.', $fontStyle);
		$table->addCell(690, $styleCell)->addText(' HORA', $fontStyle);

		// Add more rows / cells
	    if($ListarReporte != NULL)	
		{ 
		$i=0;
		foreach ($ListarReporte as $row2)
			{
			if($row2[14]!='Pro.' and $row2[13]=='1' and $row2[12]=='1')
			{
			}
			else
			{
			$i=$i+1;			
			$table->addRow(-300);
			$table->addCell(380)->addText(" ".$i);
			$table->addCell(860)->addText(" ".$row2[3],$estilo);
			$table->addCell(3700)->addText(" ".strtoupper(utf8_decode($row2[1])),$estilo);
			$table->addCell(4000)->addText(" ".utf8_decode($row2[2]),$estilo);
			$table->addCell(1300)->addText(" ".$row2[9].' '.$row2[8],$estilo);	
			$table->addCell(690)->addText('  '.$row2[11],$estilo);		
			}			
			}
		}
	    $hora= date("h.i.s");
        $fecha=date("d.m.y");
		$i=$fecha.'.'.$hora;
		// Save File

		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		$objWriter->save('repositorio/r-'.utf8_decode($usuario).'-'.$i.'.docx');
		header("location:repositorio/r-".$usuario.'-'.$i.".docx");	
		
?>