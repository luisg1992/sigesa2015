<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/estilos.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/js/classAjax.js"></script>
    <link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
       <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" />
      
     <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

	 <!-- Jquery y CSS  de Validacion  --->
	 <link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
     <script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	 <!-- Fin Script de Validacion  --->
	<!--------  Jquery y CSS Alert Dialog------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
	<!---------Fin Script de Jquery y CSS Alert Dialog -------->
	<!-- Script de Archivo Clinico  --->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
	<!-- Fin Script de Archivo Clinico  --->
	 
     <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">

				<script>
				var rowCount = 1; 
				var dato=[];

				$( function()
				{
				//Al hacer Click en el Boton Cargar_Dato llamara a la funcion Cargar_Datos
					$( '#cargar_dato' ).click(function()
					{
						var NroHistoriaClinica=$('.NroHistoriaClinica').val();
						var DigitoInicial=$('.DigitoInicial').val().slice(-2);
						var DigitoFinal=$('.DigitoFinal').val().slice(-2);
						if(DigitoInicial<= NroHistoriaClinica.slice(-2) && DigitoFinal >= NroHistoriaClinica.slice(-2))
						{
							Cargar_Datos(); 	
						}
						else
						{
							$(".NroHistoriaClinica").val('');
							$.alertx('Informacion','La HC '+NroHistoriaClinica+' No  corresponde a tu digito Terminal');
						}	
					});

					$("#Generar_Retorno_General").click(function(){
					var IdPaciente = [];
					var observaciones = [];
					var valor,valor2;
					
					$(".IdPaciente").each(
					function(index, value) {
					IdPaciente=IdPaciente+'/'+eval($(this).val());
					}
					);
					valor=IdPaciente.substr(1);



					var contador=0;
					$(".Observaciones").each(
					function(index, value) {
					observaciones=observaciones+'/'+$(this).val();
					contador=contador+1;
					}
					);
					valor2=observaciones.substr(1);

					Fecha=$("#Fecha").val();
					IdEmpleado=$("#IdEmpleado").val();
					var acc='Insertar_Retorno_General_Array';

					var parametros = {
							"IdPaciente"  : valor,
							"Observacion" : valor2,
							"IdEmpleado"  :IdEmpleado,
							"acc"		: acc
					};
					
					$.alertx('Informacion','Se realizara el retorno General de '+contador+' Historias Clinicas');
					$.ajax({
							data:  parametros,
							url:   '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
							type:  'post',
							success:  function () {},
							beforeSend: function () {
							location.reload();
							}
					});


					})
				});
				// Show the text box on click
				$('body').delegate('.editable', 'click', function(){
					var ThisElement = $(this);
					ThisElement.find('span').hide();
					ThisElement.find('.Observaciones').show().focus();
				});


				$(document).keypress(function(e) 
				{
				//Al hacer Enter llamara a la funcion Cargar_Datos	
				if(e.which == 13) 
					{
						var NroHistoriaClinica=$('.NroHistoriaClinica').val();
						var DigitoInicial=$('.DigitoInicial').val().slice(-2);
						var DigitoFinal=$('.DigitoFinal').val().slice(-2);
						if(DigitoInicial<= NroHistoriaClinica.slice(-2) && DigitoFinal >= NroHistoriaClinica.slice(-2))
						{
							Cargar_Datos(); 	
						}
						else
						{
							$(".NroHistoriaClinica").val('');
							$.alertx('Informacion','La HC '+NroHistoriaClinica+' No  corresponde a tu digito Terminal');
						}
					
					}
				});


				
				function Cargar_Datos(){
					
						//get the json data from data.php 
						$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
							{ 
							 NroHistoriaClinica: $('.NroHistoriaClinica').val(),
							 DigitoInicial: $('.DigitoInicial').val(),
							 DigitoFinal: $('.DigitoFinal').val(),
							 acc:'Cargar_Paciente'
							},
							 function( res ){
							//parse the result using $.parseJSON() jquery function
							parsedRes = $.parseJSON( res );	
							//use the key to print the values respectively
							var  IdPaciente=parsedRes.IdPaciente;
							var  ApellidoPaterno=parsedRes.ApellidoPaterno;
							var  ApellidoMaterno=parsedRes.ApellidoMaterno;
							var  PrimerNombre=parsedRes.PrimerNombre;
							var  SegundoNombre=parsedRes.SegundoNombre;
							var  NroDocumento=parsedRes.NroDocumento;
							var  NroHistoriaClinica=parsedRes.NroHistoriaClinica;
							$(".NroHistoriaClinica").val('');
							$(".NroHistoriaClinica").focus();
							rowCount ++;
							if ($.inArray(IdPaciente, dato) > -1)
							{
							$.alertx('Informacion','Historia Clinica ya fue Ingresada ');
							}
							else
							{
							dato.push(IdPaciente);
				var recRow = '<tr id="rowCount'+rowCount+'"><td  width="1%"  style="padding:10px"><input class="IdPaciente"  type="hidden" size="1" maxlength="120" value="'+IdPaciente+'"/></td><td  width="10%" style="padding:10px">'+NroHistoriaClinica+'</td><td width="45%" style="padding:10px"><center>'+ApellidoPaterno+'  '+ApellidoMaterno+'  '+PrimerNombre+'  '+SegundoNombre+'</center></td><td  width="15%" ><div class="grid_content editable"><input class="Observaciones" type ="text"  size="30" value="" /></div></td><td width="10%" text-align="center"><a href="javascript:void(0);" onclick="removeRow('+rowCount+','+IdPaciente+');">Eliminar</a></td></tr>'; jQuery('#addedRows').append(recRow);	
							}
						});
						
				}

				function removeRow(removeNum,IdPaciente)
				{
				pos = dato.indexOf(IdPaciente.toString());
				dato.splice(pos,1);
				jQuery('#rowCount'+removeNum).remove();
				}
				
	
				function generar_array() 
				{
				var IdPaciente = [];
				var Valor;
					$(".IdPaciente").each(
						function(index, value) {
							IdPaciente=IdPaciente+','+eval($(this).val());
						}
					);
					valor=IdPaciente.substr(1);
					
					var parametros = {
						
									"valor" : valor,
									"acc" : 'Generar_Retorno_General'
					};
					
					$.ajax({
									data:  parametros,
									url:   '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
									type:  'post',
									beforeSend: function () {
										location.reload();
									},
									success:  function (response) {
											$("#resultado").html(response);
									}
					});
				}
				
				$(window).load(function() {
					 $(".NroHistoriaClinica").focus();
				});
	
				</script> 


 </head>
 <body>
 	<?php
	  if($ListarDigitoTerminales != NULL)
	  {
	  $IdEmpleado=$_REQUEST["IdEmpleado"];
	  foreach($ListarDigitoTerminales as $digito){ 
	  $digito_inicial=$digito["digito_inicial"];
	  $digito_final=$digito["digito_final"];
	  }
	  }	  
    ?>
  <!-- Lista de Movimientos Historia Clinica  ---->
  <div style="margin:15px !important">
  <h1>Retorno General de HC ( <?php  echo  $digito_inicial.' - '.$digito_final  ?> )</h1>  
  <div  id="lista_archiveros">
  <table width="1000" border="0">
    <tr>
      <td width="994"><table width="100%">
        <tr>
          <td width="66%"><fieldset>
            <legend>Generar el Retorno</legend>
            <table width="70%">
                <tr>
			    <td width="1%">&nbsp;</td>
                <td width="3%">Nro Historia Cl&iacute;nica</td>
                <td  width="1%"><input type="text"   name="NroHistoriaClinica"  class="NroHistoriaClinica" autofocus size="30" ></td>
				<td  width="1%" style="display:none"><input type="text"   class="DigitoInicial" value="<?php  echo  $digito_inicial; ?>"></td>
				<td  width="1%" style="display:none"><input type="text"   class="DigitoFinal"   value="<?php  echo  $digito_final; ?>"></td>
                <td  width="1%"><input type="submit" value="Aceptar" id="cargar_dato"></td>
               <td align="center" valign="top" width="1%" ><input type="submit" id="Generar_Retorno_General" value="Generar Retorno General" ></td>
              <!--
                <td align="center" valign="top" width="1%" ><input type="submit" id="Generar_Retorno_General" value="Generar Retorno General" onclick="generar_array()"></td>-->
                </tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
              <input type="hidden"   id="IdEmpleado" value="<?php echo $IdEmpleado;  ?>">
          </fieldset></td>
          </tr>
    </table>
	</tr>
  </table>
  
   <div style="margin:15px !important">
			<table width="940">
			<tr>
			    <td width="950">
			    <fieldset>
				<legend>Lista de Historias Cl&iacute;nicas por Retornar</legend>
				<!--Inicio de Contenedor  ---->  
				<div id="lineaResultado">
				<table width="950" border="1" cellpadding="0" cellspacing="0"  class="data">
				<tr>
					<th width="10%" align="center">Nº Historia Cl&iacute;nica</th>
					<th width="40%" align="center">Nombres de Paciente</th>
					<th width="30%" align="center">Observacion de Retorno</th>
					<th width="10%" align="left">Acci&oacute;n</th>
				</tr>
				</table>
				<div id="scroll">
				<table width="940" border="1" cellpadding="0" cellspacing="0" id="addedRows" class="data"></table>
				</div>
				</td>
				</div>
				  <!--Fin de Contenedor  ----> 
			    </fieldset>
			    </td>
			</tr>
		  </table>
  </div>
  </div>
	<table rules="all" style="background:#fff;">
	<tr id="rowId">
	</table>
	<id style="DISPLAY:NONE">
		<tr><input type="text" id="IdPacientex"  value=""></td></tr>
		<tr><input type="text" id="ApellidoPaterno"  value=""></td></tr>
		<tr><input type="text" id="ApellidoMaterno"  value=""></td></tr>
		<tr><input type="text" id="PrimerNombre"  value=""></td></tr>
		<tr><input type="text" id="SegundoNombre"  value=""></td></tr>
		<tr><input type="text" id="NroDocumento"  value=""></td></tr>
		<tr><input type="text" id="NroHistoriaClinica" value=""></td></tr>
	</id>
	</tr>
 </body>
 