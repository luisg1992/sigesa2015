<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
	<!DOCTYPE html>
	<html lang="en">
	<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen_archivo.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
		
	<!--------  Jquery  Popup_query     ------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/Popup_query.js"></script>
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/Popup_query.css">
	<!--------  Fin Jquery  Popup_query ------------>
	
	<!-------- Script de Validacion      ------>
	<script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
	<!-------- Fin Script de Validacion  ------>
	
	
	<!-------- Script  Jquery Alert Dialog        ------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
	<!-------- Fin  Script  Jquery Alert Dialog   ------------>
	
	
	<script type="text/javascript">
	$(function(){
	
		$("#Retorno_HC").click(function Cargar(){		
		Cargar_Lista_Retorno(); 		
		});

		$("#Modificar_DigitoTerminal").click(function (){

							var Digito_Inicial=$(".Digito_Inicial").val(); 
							var Digito_Final=$(".Digito_Final").val();
							var Id_Empleado=$(".IdEmpleado").val();								
							var parametros =
							{
							"Digito_Inicial" : Digito_Inicial,
							"Digito_Final" : Digito_Final,
							"Id_Empleado" : Id_Empleado,
							"acc" : 'Modificar_DigitoTerminal'
							};			
							$.ajax({
											data:  parametros,
											url:   '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
											type:  'post',
											success:  function (response) {
													$("#resultado").html(response);
											}	
							}); 
		});
		
		
		// Show the text box on click
		$('body').delegate('.editable', 'click', function(){
			var ThisElement = $(this);
			ThisElement.find('span').hide();
			ThisElement.find('.gridder_input').show().focus();
		});
		
		// Pass and save the textbox values on blur function
		$('body').delegate('.gridder_input', 'blur', function(){
			
			var ThisElement = $(this);
			ThisElement.hide();
			ThisElement.prev('span').show().html($(this).val()).prop('title', $(this).val());
			var UrlToPass = 'acc=Agregar_Observacion_Retorno&Observacion='+ThisElement.val()+'&IdAtencion='+ThisElement.prop('name');	
			$.ajax({
				url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				type : 'POST',
				data : UrlToPass
			});
		});	
	});
	</script>
	
	
	<script type="text/javascript">
		  $(function() {
		  	var $selector = $('#Historia_Clinica');
			var gridder = $('#lineaResultado');
			$(document.body).off('keyup', $selector);
			// Bind to keyup events on the $selector.
			$(document.body).on('keyup', $selector, function(event) {
			  if(event.keyCode == 13) 
			  {			  
			  var UrlToPass = 
			  {
                Fecha_Inicio: $('#Fecha_Inicio').val(),
                Fecha_Final: $('#Fecha_Final').val(),
				Digito_Inicial: $('#Digito_Inicial').val(),
				Digito_Final: $('#Digito_Final').val(),
				Turno: $('#Turno').val(),
				Empleado: $('#IdEmpleado').val(),
				Tipo_estado: $('#Tipo_estado').val(),
				NroHistoriaClinica: $('#Historia_Clinica').val(),
				acc : 'Cargar_Retorno_HC'
			  };
			  $.ajax({
					url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
					type : 'POST',
					data : UrlToPass,
					success: function(responseText) {
						gridder.html(responseText);
					}
			  });
			  $("#Historia_Clinica").each(function(){	
				$($(this)).val('');
			  });
			  }
			});
		  });
		  
		function marcar(source)
		{
			checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
			for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
			{
				if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
				{
					checkboxes[i].checked=source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
				}
			}
		}
		
		
		
		
		function Cargar_Lista_Retorno() {

			var gridder = $('#lineaResultado');
			var UrlToPass = {
					Fecha_Inicio: $('#Fecha_Inicio').val(),
					Fecha_Final: $('#Fecha_Final').val(),
					Digito_Inicial: $('#Digito_Inicial').val(),
					Digito_Final: $('#Digito_Final').val(),
					Turno: $('#Turno').val(),
					Tipo_estado: $('#Tipo_estado').val(),
					acc : 'Listar_Retorno_HC'
			};

			$.ajax({
				url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				type : 'POST',
				data : UrlToPass,
				success: function(responseText) {
					gridder.html(responseText);
				}
			});
		
		}
		
		$(function(){
			
		$("#Cargar_Retorno_HC").click(function () {
		$("input:checkbox:checked:enabled").each(function()
		{
			var IdAtencion = $(this).val();
			var IdEmpleado = $('#IdEmpleado').val();
			var acc = 'Registrar_Retorno_Check';
			$.ajax({
				type: 'post',
				url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',			
				data: 'IdAtencion='+IdAtencion+'&Id_Empleado='+IdEmpleado+'&acc='+acc
			});		
		});
		
		Cargar_Lista_Retorno(); 	
		})
		});
		
		
	</script>
	</head>	
	<body>
	<div style="margin:15px !important">
	<?php
	  if($ListarDigitoTerminales != NULL)
	  {
	  $IdEmpleado=$_REQUEST["IdEmpleado"];
	  foreach($ListarDigitoTerminales as $digito){ 
	  $digito_inicial=$digito["digito_inicial"];
	  $digito_final=$digito["digito_final"];
	  }
	  }	  
    ?>
	
	
		
	<div class="overlay-container">
		<div class="window-container zoomin">
			<h3>Modicar Digitos Terminales</h3> 
			<table width="100%">
				<tr>
					  <td><span>Digito Inicial</span></td>
					  <td><input  type="text"  name="Digito_Inicial"  class="Digito_Inicial"   maxlength="2"  value="<?php echo  $digito_inicial ?>"></td>
				</tr>
				<tr>
					  <td><span>Digito Final</span></td>
					  <td><input  type="text"  name="Digito_Final"  class="Digito_Final"   maxlength="2"  value="<?php echo $digito_final ?>"></td>
				</tr>
				<tr style="display:none">
					  <td>IdEmpleado</td>
					  <td><input  type="text"  name="IdEmpleado"  class="IdEmpleado" value="<?php echo $IdEmpleado ?>"></td>
				</tr>
				<tr>
					  <td><span class="close" id="Modificar_DigitoTerminal">Modificar </span></td>
					  <td><span class="close">Cancelar</span></td>
				</tr>
			</table>
		<br>
		</div>
	</div>
    <h1>Retorno HC - D&iacute;gito Terminal ( <?php  echo  $digito_inicial.' - '.$digito_final  ?> )  
	<a rel="tooltip" title="Modificar mis Digitos Terminales" href="#"  
	class="button" data-type="zoomin"  class="Modificar_DigitoTerminal">
	<img src="../../MVC_Complemento/img/modiciar.jpg" width="16" height="16" />
	</a>
	</h1>
	  <table width="1040" border="0">
		<tr>
		  <td width="994">
		    <table width="100%">
			<tr>
			  <td width="66%"><fieldset>
				<legend>Reporte de HC</legend>
				<table width="900" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td colspan="8">
					<table width="100%">
					  <tr style="display:none">
					  <td><input  type="text"  name="Digito_Inicial"  id="Digito_Inicial" value="<?php echo  $digito_inicial ?>"></td>
					  <td><input  type="text"  name="Digito_Final"  id="Digito_Final" value="<?php echo $digito_final ?>"></td>
					  <td><input  type="text"  name="IdEmpleado"  id="IdEmpleado" value="<?php echo $IdEmpleado ?>"></td>
					  </tr>
					  <tr>
					    <td width="1%">&nbsp;</td>
						<td width="6%">Fecha Inicio</td>
						<td width="20%"> 
						<input  type="text"  name="Fecha_Inicio"  id="Fecha_Inicio" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="15"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Inicio",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador1"     // el id del botón que lanzará el calendario 
											}); 
										</script>	
						</td>
						<td width="8%">Fecha Final</td>
						<td width="19.5%">
						<input  type="text"  name="Fecha_Final"  id="Fecha_Final" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="15"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Final",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador2"     // el id del botón que lanzará el calendario 
											}); 
										</script>				
						</td>
						<td width="4%">Estado</td>
						<td width="5%">
						<div style="margin-top:10px">
								<select name="Tipo_estado" id="Tipo_estado"  style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="Totalidad">Totalidad</option>
									<option value="Pendientes">Pendientes</option>
								</select>
						</div>		
						</td>
						<td width="4%">Turno</td>
						<td width="15%">
								<select name="Turno"   id="Turno"  style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="%mañ%">Ma&ntilde;ana</option>
									<option value="%tar%">Tarde</option>
								</select>								
						</td>
						<td> 
						<input type="button"  id="Retorno_HC" value="Aceptar">
						</td>
						</tr>
					</table>
					</td>
					 <!-- Aqui estan las funciones propias de jcal para poder Cargar las Fechas de Rango-->
					  <script type="text/javascript">
					  RANGE_CAL_1 = new Calendar({
							  inputField: "Fecha_Inicio",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaInicio_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
							  RANGE_CAL_2 = new Calendar({
							  inputField: "Fecha_Final",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaFinal_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
					</script>
				  </tr>
				  <tr>
				  <td>&nbsp;</td>
				  </tr>
				</table>
				</td>
				</tr>
			</table>
			<div style="margin:15px !important;float:left">
			<table width="650">
			<tr>
			<td width="200">
			<h3><b>Historia Cl&iacute;nica :</b></h3>
			</td>
			<td width="190">
			<input type="text" id="Historia_Clinica" name="Historia_Clinica" size="18" />
			</td>
			<td>
			<input type="button" value="Aceptar" id="Historia_Clinica_Salida">
			</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td width="190">
			Seleccionar Todo
			</td>
			<!--<td style="width:60px"><input type="checkbox" onclick="marcar(this);" /></td>-->
			<td>&nbsp;</td>
			<td>
			<input type="button" value="Guardar Cambios" id="Cargar_Retorno_HC">
			</td>
			</tr>
		    </table>
			</div>
						<div style="margin:15px !important; clear:both;">
						<table width="970">
						<tr>
						  <td width="975">
						  <fieldset>
							<legend>Lista de Pacientes</legend>
							<table width="950" border="1" cellpadding="0" cellspacing="0" class="data">
							 <tr>
							   <th width="20" align="center">N&deg; Id</th>
							   <th width="180" align="center">Consultorio</th>
							   <th width="30" align="center">HC</th>
							   <th width="210" align="center">Apellidos y Nombres</th>
							   <!--<th width="20"><input type="checkbox" onclick="marcar(this);" /></th> -->
							   <th width="220" align="center">Observacion Retorno HC</th>     
							 </tr>
							</table>
							<!--Inicio de Contenedor  ----> 
							<div id="scroll">								
								<div id="lineaResultado">
								</div>
							</div>
							<!--Fin de Contenedor  ----> 
						  </fieldset>
						  </td>
						</tr>
					    </table>
					    </div>
	    </table>
	</div>
	</body>
	</html>

