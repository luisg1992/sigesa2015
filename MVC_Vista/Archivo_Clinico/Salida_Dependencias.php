<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
	<!DOCTYPE html>
	<html lang="en">
	<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>	 
	<!-- Script de Archivo Clinico  --->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
	<!-- Fin Script de Archivo Clinico  --->
	
	<script>	
			$( function(){
				//on click of the button
				$( '.cargar_dato' ).click(function(){
					//get the json data from data.php 
								$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
								{ 
								NroHistoriaClinica: $('.NroHistoriaClinica').val(),
								acc:'Cargar_Paciente'
								},
								 function( res ){
								//parse the result using $.parseJSON() jquery function
								parsedRes = $.parseJSON( res );	
								//use the key to print the values respectively
								$( '#IdPaciente' ).val( parsedRes.IdPaciente );
								$( '#ApellidoPaterno' ).val( parsedRes.ApellidoPaterno );
								$( '#ApellidoMaterno' ).val( parsedRes.ApellidoMaterno );
								$( '#PrimerNombre' ).val( parsedRes.PrimerNombre );
								$( '#SegundoNombre' ).val( parsedRes.SegundoNombre );
								$( '#NroDocumento' ).val(parsedRes.NroDocumento );
								$( '#NroHistoriaClinica' ).val(parsedRes.NroHistoriaClinica );
								obtenerDatos(); 
					});
					
					
				});
			});
			
			
			$(document).keypress(function(e) {
			if(e.which == 13) {
								$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
								{ 
								NroHistoriaClinica: $('.NroHistoriaClinica').val(),
								acc:'Cargar_Paciente'
								},
								 function( res ){
								//parse the result using $.parseJSON() jquery function
								parsedRes = $.parseJSON( res );	
								//use the key to print the values respectively
								$( '#IdPaciente' ).val( parsedRes.IdPaciente );
								$( '#ApellidoPaterno' ).val( parsedRes.ApellidoPaterno );
								$( '#ApellidoMaterno' ).val( parsedRes.ApellidoMaterno );
								$( '#PrimerNombre' ).val( parsedRes.PrimerNombre );
								$( '#SegundoNombre' ).val( parsedRes.SegundoNombre );
								$( '#NroDocumento' ).val(parsedRes.NroDocumento );
								$( '#NroHistoriaClinica' ).val(parsedRes.NroHistoriaClinica );
								obtenerDatos(); 
					});
				}
			});
		</script>
	
		<script type="text/javascript"> 
		var rowCount = 1; 
		function obtenerDatos(){
			var  IdPaciente=$("#IdPaciente").val();
			var  ApellidoPaterno=$("#ApellidoPaterno").val();
			var  ApellidoMaterno=$("#ApellidoMaterno").val();
			var  PrimerNombre=$("#PrimerNombre").val();
			var  SegundoNombre=$("#SegundoNombre").val();
			var  NroDocumento=$("#NroDocumento").val();
			var  NroHistoriaClinica=$("#NroHistoriaClinica").val();
			var  IdServicio=$('select[name=Servicios]').val();
			var  Servicio = $("#Servicios option:selected").text();
			var  FechaCita = $("#Fecha").val();
			var  Motivo = $("#Motivo_Movimiento option:selected").text();
			$(".NroHistoriaClinica").val('');
			$(".NroHistoriaClinica").focus();
			rowCount ++; 
			var recRow = '<tr id="rowCount'+rowCount+'"><td width="0%" style="padding:10px"><input class="FechaCita"  type="hidden"  maxlength="120" value="'+FechaCita+'"/><input class="IdPaciente"  type="hidden"  maxlength="120" value="'+IdPaciente+'"/><input class="IdServicio"  type="hidden"  maxlength="120" value="'+IdServicio+'"/></td><td width="10%" style="padding:10px">'+FechaCita+'</td><td width="10%" style="padding:10px">'+Motivo+'</td></td><td width="20%" style="padding:10px">'+Servicio+'</td><td width="10%" style="padding:10px">'+NroHistoriaClinica+'</td><td width="20%" style="padding:10px">'+ApellidoPaterno+'  '+ApellidoMaterno+'  '+PrimerNombre+'  '+SegundoNombre+'</td><td width="20%" style="padding:10px"><input type="text" size="25px"  class="comentario"></td><td width="10%"><a href="javascript:void(0);" onclick="removeRow('+rowCount+');">Eliminar</a></td></tr>'; jQuery('#addedRows').append(recRow);	
		}

		function removeRow(removeNum)
		{
		jQuery('#rowCount'+removeNum).remove();
		}
		
		
		function generar_array() {
		var IdPaciente = [];
		var Valor;
			$(".IdPaciente").each(
				function(index, value) {
					IdPaciente=IdPaciente+','+eval($(this).val());
				}
			);
			valor=IdPaciente.substr(1);
			
			
		var IdServicio = [];
		var Valor1;
			$(".IdServicio").each(
				function(index, value) {
					IdServicio=IdServicio+','+eval($(this).val());
				}
			);
			valor1=IdServicio.substr(1);


		Fecha=$("#Fecha").val();
		IdEmpleado=$("#IdEmpleado").val();
		
		
		var acc='Insertar_Dependencia';
		
		var comentario = [];
		$.each($('.comentario'), function() {
		comentario.push($(this).val());
		});
		
			var parametros = {
				
							"IdPaciente" : valor,
							"IdServicio" : valor1,
							"FechaCita" : Fecha,
							"Comentario" : comentario,
							"IdEmpleado" : IdEmpleado,
							"acc"		: acc
			};
			
			$.ajax({
							data:  parametros,
							url:   '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
							type:  'post',
							beforeSend: function () {
								location.reload();
							},
							success:  function (response) {
									$("#resultado").html(response);
							}
			});
		}
	</script> 
		
	
	
	<script type="text/javascript" charset="utf-8">
		  $(document).ready(function() {
		   $("#IdTipoServicio").change(function () {
			  $("#IdTipoServicio option:selected").each(function () {
				IdTipoServicio=$(this).val();
				$.post("../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php", { IdTipoServicio: IdTipoServicio  , acc :'Combo_Dependencia' }, function(data){
				$("#Servicios").html(data);
			  });     
			 });
		   });    
		});
	</script>

	
	</head>	
	<body>
	 
	<div style="margin:15px !important">
    <h1>Salida a Dependencias</h1>
		<table width="1040" border="0">
		<tr>
		  <td width="994"><table width="100%">
			<tr>
			  <td width="76%"><fieldset>
				<legend>Salida de Historia Clinica a Servicios</legend>
				<table width="800" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td colspan="8">
					<table width="100%">
					  <tr style="display:none">
					  <td><input  type="text"  name="IdEmpleado"  id="IdEmpleado" value="<?php echo $_REQUEST["IdEmpleado"]; ?>"></td>
					  </tr>
					  <tr>
					  <td width="3%">&nbsp;</td>
					  <td width="15%">Tipos de Servicio</td>
					  <td width="12%">
					  	<select name="IdTipoServicio" id="IdTipoServicio" style="margin:10px 10px -9px 0px;">
                          <option value="0" >Todos los T&oacute;picos</option>
                          <?php 
                          if($TiposServicio != NULL)  { 
                          foreach($TiposServicio as $item2){?>
                              <option value="<?php echo $item2["IdTipoServicio"]?>"<?php if($item2["IdTipoServicio"]==$IdTipoServicio){?>selected<?php }?>><?php echo $item2["Descripcion"]?></option>
                          <?php }}?>
						</select>
					  </td>
					  <td width="8%">Servicio / Consultorio</td>
					  <td width="16%">
					  	<select  name="Servicios" id="Servicios" style="margin:10px 10px -9px 0px;">
						<option value="0" >Todos los Servicios</option>
						</select>
					  </td>
					  </tr>
					  <tr>
					  <td width="3%">&nbsp;</td>
					  <td width="7%">Motivo</td>
					  <td width="16%">
					    <select name="Motivo_Movimiento" id="Motivo_Movimiento" style="margin:10px 10px -9px 0px;">
                          <option value="0" >Todos los Motivos</option>
                          <?php 
                          if($Motivos_Movimiento_HC != NULL)  { 
                          foreach($Motivos_Movimiento_HC as $item){?>
                              <option value="<?php echo $item["IdMotivo"]?>"<?php if($item["IdMotivo"]==$IdMotivo){?>selected<?php }?>><?php echo $item["Descripcion"]?></option>
                          <?php }}?>
						</select>
					  </td>
					  <td width="8%">Turno</td>
					  <td width="16%">
					  	<div style="margin-top:10px">
								<select name="Turno"   id="Turno" style="padding: 5px; margin:10px 10px -9px 0px;">
									<option value="%mañ%">Ma&ntilde;ana</option>
									<option value="%tar%">Tarde</option>
								</select>
						</div>	
					  </td>
					  </tr>
					  <tr>
						<td width="3%">&nbsp;</td>
						<td width="7%">Fecha</td>
						<td width="16%">
						<div style="margin-top:10px">						
						<input  style="margin:15px 10px -9px 0px;" type="text"  name="Fecha"  id="Fecha" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="15"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador1"     // el id del botón que lanzará el calendario 
											}); 
										</script>
						</div>						
						</td>
						<td width="8%">Historia Clinica</td>
						<td width="14.5%">
						<div style="margin-top:10px">
								<input  type="text"  name="NroHistoriaClinica"  class="NroHistoriaClinica">
						</div>				
						</td>
						<td width="2%">&nbsp;</td>
						<td width="3%"><input type="submit" value="Aceptar"  class="cargar_dato"></td>
						<td width="2%">&nbsp;</td>
						<td > 
						<input type="submit" value="Guardar Cambios" id="cambios" onclick="generar_array()">
						</td>
						</tr>
					</table>
					</td>
					 <!-- Aqui estan las funciones propias de jcal para poder Cargar las Fechas de Rango-->
					  <script type="text/javascript">
					  RANGE_CAL_1 = new Calendar({
							  inputField: "Fecha_Citas",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaInicio_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
							  RANGE_CAL_2 = new Calendar({
							  inputField: "FechaFin",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaFinal_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
					</script>
				  </tr>
				  <tr>
				  <td>&nbsp;</td>
				  </tr>
				</table>
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				</tr>
				</table>
		</table>		
				   <div style="margin:15px !important">
							<table width="940">
							<tr>
								<td width="950">
								<fieldset>
								<legend>Lista de Historias Clinicas</legend>
								<!--Inicio de Contenedor  ---->  
								<div id="lineaResultado">
								<table width="940" border="1" cellpadding="0" cellspacing="0"  class="data">
								<tr>
									<th width="10%" align="center">Fecha</th> 
									<th width="10%" align="center">Motivo</th> 
									<th width="20%" align="center">Consultorio</th> 
									<th width="10%">HC</th> 
									<th width="20%">Apellidos y Nombres</th>
									<th width="20%">Observaciones</th> 
									<th width="10%">Accion</th> 										
								</tr>
								</table>
								<div id="scroll">
								<table width="940" border="1" cellpadding="0" cellspacing="0" id="addedRows" class="data"></table>
								</div>
								</td>
								</div>
								  <!--Fin de Contenedor  ----> 
								</fieldset>
								</td>
							</tr>
						  </table>
					</div>
				
				
	</div>
	
	<table rules="all" style="background:#fff;">
	<tr id="rowId">
	</table>
		<id style="DISPLAY:NONE ">
			<tr><input type="text" id="IdPaciente"  value=""></td></tr>
			<tr><input type="text" id="ApellidoPaterno"  value=""></td></tr>
			<tr><input type="text" id="ApellidoMaterno"  value=""></td></tr>
			<tr><input type="text" id="PrimerNombre"  value=""></td></tr>
			<tr><input type="text" id="SegundoNombre"  value=""></td></tr>
			<tr><input type="text" id="NroDocumento"  value=""></td></tr>
			<tr><input type="text" id="NroHistoriaClinica" value=""></td></tr>
		</id>
	</tr> 	

		
	</body>
	</html>

