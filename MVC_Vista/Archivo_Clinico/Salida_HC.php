<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
	<!DOCTYPE html>
	<html lang="en">
	<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen_archivo.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	
	<!--------  Jquery  Popup_query     ------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/Popup_query.js"></script>
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/Popup_query.css">
	<!--------  Fin Jquery  Popup_query ------------>
	
	<!-------- Script de Validacion      ------>
	<script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
	<!-------- Fin Script de Validacion  ------>
	
	
	<!-------- Script  Jquery Alert Dialog        ------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
	<!-------- Fin  Script  Jquery Alert Dialog   ------------>
	
	
	
     <!-------- Script  Emergente de Historias Clinicas Pendientes       ------------>
	<link rel="stylesheet" href="../../MVC_Complemento/css/Popup_query_2.css" type="text/css" /> 
	<!-------- Fin  Script  Emergente de Historias Clinicas Pendientes    ------------>
	
	
	
	  <script>


	 $(document).ready(function(){
	 $("#search").keyup(function(){
	 _this = this;

	 $.each($("#mytable tbody tr"), function() {
	 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
	 $(this).hide();
	 else
	 $(this).show();
	 });
	 });
	});


	</script>
	<script type="text/javascript">
	$(function(){
		
		//aqui
		$("#Salida_HC").click(function (){
		Cargar_Lista_Salida(); 	
		});
	
	

		$('#Limpiar').click(function (){
			$('#Historia_Clinica').val('');
			$('#search').val('');
		});


		$("#Modificar_DigitoTerminal").click(function (){
							var Digito_Inicial=$(".Digito_Inicial").val(); 
							var Digito_Final=$(".Digito_Final").val();
							var Id_Empleado=$(".IdEmpleado").val();								
							var parametros =
							{
							"Digito_Inicial" : Digito_Inicial,
							"Digito_Final" : Digito_Final,
							"Id_Empleado" : Id_Empleado,
							"acc" : 'Modificar_DigitoTerminal'
							};			
							$.ajax({
											data:  parametros,
											url:   '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
											type:  'post',
											success:  function (response) {
													$("#resultado").html(response);
											}	
							}); 
		});

		// Show the text box on click
		$('body').delegate('.editable', 'click', function(){
			var ThisElement = $(this);
			ThisElement.find('span').hide();
			ThisElement.find('.gridder_input').show().focus();
		});


		//$('body').delegate('.IdAtencion', 'click', function(){
		$(document).on('change', '.IdAtencion', function() {
			var ThisElement = $(this);
			ThisElement.prop("disabled", false);
			//Si deshabilita el check
			if(this.checked == false){
			$(this).closest('tr').find('td').css("color", "red", "important");
			$.alertx('Informacion Archivo Clinico','Se Modifico Estado de Salida Satisfactoriamente');
			var UrlToPass = 'acc=Registrar_Retorno_de_Salida_Check&IdAtencion='+ThisElement.val();	
			$.ajax({
				url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				type : 'POST',
				data : UrlToPass
			});
			}
			//Si habilita el check Salida
			if(this.checked == true){
			//Cambiar el Color del Elemento
			$(this).closest('tr').find('td').css("color", "black", "important");
			var Id_Empleado=$(".IdEmpleado").val();		
			$.alertx('Informacion Archivo Clinico','Se Genero la Salida Satisfactoriamente');
			var parametros =
			{
				"IdAtencion" : ThisElement.val(),
				"Id_Empleado" : Id_Empleado,
				"acc" : 'Registrar_Salida_Check'
			};

			//var parametros = 'acc=Registrar_Salida_Check&IdAtencion='+ThisElement.val();	
			$.ajax({
				url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				type : 'POST',
				data : parametros
			});
			}
		});
		
		// Pass and save the textbox values on blur function
		$('body').delegate('.gridder_input', 'blur', function(){
			var ThisElement = $(this);
			ThisElement.hide();
			ThisElement.prev('span').show().html($(this).val()).prop('title', $(this).val());
			var UrlToPass = 'acc=Agregar_Observacion_Salida&Observacion='+ThisElement.val()+'&IdAtencion='+ThisElement.prop('name');	
			$.ajax({
				url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				type : 'POST',
				data : UrlToPass
			});
		});


		
	});
	</script>
	
	
	<script type="text/javascript">
		  $(function() {
		  	var $selector = $('#Historia_Clinica');
			$(document.body).off('keyup', $selector);
			// Bind to keyup events on the $selector.
			$(document.body).on('keyup', $selector, function(event) {
			  if(event.keyCode == 13) 
			  {
			  Validar_Cantidad();
			  }
			});
		  });

		  /*
		  function Generar_Salida(){
		  	var gridder = $('#lineaResultado');
		  	  var UrlToPass = 
			  {
                Fecha_Inicio: $('#Fecha_Inicio').val(),
                Fecha_Final: $('#Fecha_Final').val(),
				Digito_Inicial: $('#Digito_Inicial').val(),
				Digito_Final: $('#Digito_Final').val(),
				Turno: $('#Turno').val(),
				Empleado: $('#IdEmpleado').val(),
				Tipo_estado: $('#Tipo_estado').val(),
				NroHistoriaClinica: $('#Historia_Clinica').val(),
				acc : 'Cargar_Salida_HC'
			  };
			  $.ajax({
					url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
					type : 'POST',
					data : UrlToPass,
					success: function(responseText) {
						gridder.html(responseText);
					}
			  });
			  $("#Historia_Clinica").each(function(){	
				$($(this)).val('');
			  });
		  	}
			*/
			
			function Generar_Salida(){
				var gridder = $('#lineaResultado');
				  var UrlToPass = 
				  {
					Fecha_Inicio: $('#Fecha_Inicio').val(),
					Fecha_Final: $('#Fecha_Final').val(),
					Digito_Inicial: $('#Digito_Inicial').val(),
					Digito_Final: $('#Digito_Final').val(),
					Turno: $('#Turno').val(),
					Empleado: $('#IdEmpleado').val(),
					Tipo_estado: $('#Tipo_estado').val(),
					NroHistoriaClinica: $('#Historia_Clinica').val(),
					acc : 'Cargar_Salida_HC'
				  };

				$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				{ 
				NroHistoriaClinica: $('#Historia_Clinica').val(),
				Fecha_Inicio: $('#Fecha_Inicio').val(),
				Fecha_Final: $('#Fecha_Final').val(),
				acc:'Verificar_Salida_HistoriaClinica'
				},
				function( res ){
				parsedRes = $.parseJSON( res );	
				var  idFuenteFinanciamiento=parsedRes.idFuenteFinanciamiento;
				var  IdEstadoCita=parsedRes.IdEstadoCita;
				var  Clave=parsedRes.Clave;
				var Nombre=parsedRes.Nombre;
				if(String(Clave)!='Pro.' && IdEstadoCita=='1' && idFuenteFinanciamiento=='1')
				{
					$(this).checked = false; 
					$.alertx('Informacion Archivo Clinico','Historia para el Servicio de '+Nombre+'  no ah sido Pagada');
					return false;				
				}
				else
				{
					$.ajax({
						url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
						type : 'POST',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
				    });
					$("#Historia_Clinica").each(function(){	
						$($(this)).val('');
					});	
				}
				});
		  	}
			
	</script>
	
	<script type="text/javascript">
		
		function marcar(source)
		{
			checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
			for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
			{
				if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
				{
					$( ".IdAtencion" ).prop( "disabled", false );
				}
			}
		}
		
		function Cargar_Lista_Salida() {
			var gridder = $('#lineaResultado');
			var UrlToPass = {
					Fecha_Inicio: $('#Fecha_Inicio').val(),
					Fecha_Final: $('#Fecha_Final').val(),
					Digito_Inicial: $('#Digito_Inicial').val(),
					Digito_Final: $('#Digito_Final').val(),
					Turno: $('#Turno').val(),
					Tipo_estado: $('#Tipo_estado').val(),
					acc : 'Listar_Salida_HC'
			};

			$.ajax({
				url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				type : 'POST',
				data : UrlToPass,
				success: function(responseText) {
					gridder.html(responseText);
				}
			});		
		}
		



		$(function(){

		$("#Cargar_Salida_HC").click(function () {
		$("input:checkbox:checked:enabled").each(function()
		{
			var IdAtencion = $(this).val();
			var acc = 'Registrar_Salida_Check';
			$.ajax({
				type: 'post',
				url: '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',			
				data: 'IdAtencion='+IdAtencion+"&acc="+acc
			});		
		});
		Cargar_Lista_Salida(); 	
		})
		
		
		
		$(document).on('change', '.IdAtencion', function() {
			if(this.checked) 
			{
			$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
			{ 
			IdAtencion: $(this).val(),
			acc:'Verificar_Salida_Check'
			},
			function( res ){
			parsedRes = $.parseJSON( res );	
			var  idFuenteFinanciamiento=parsedRes.idFuenteFinanciamiento;
			var  IdEstadoCita=parsedRes.IdEstadoCita;
			var  Clave=parsedRes.Clave;
			var Nombre=parsedRes.Nombre;
			if(String(Clave)!='Pro.' && IdEstadoCita=='1' && idFuenteFinanciamiento=='1')
			{
				$(this).checked = false; 
				$.alertx('Informacion Archivo Clinico','Historia para el Servicio de '+Nombre+'  no ah sido Pagada');
				return false;				
			}
			});
			}
		});
		});
	</script>
	
	
	<!-- Script para Emergente de Cantidad de Historias Pendientes  -->	
	<script type="text/javascript">
	
				<!-- Funcion para Generar la recarga de el contador de Historias Clinicas Pendientes  -->
				function recargar()
				{				
					var acc='Mostrar_Cantidad_HC';
				    var Digito_Inicial=$('.Digito_Inicial').val();
					var Digito_Final=$('.Digito_Final').val();
					var Turno=$('#Turno').val();
					$("#recargado").fadeOut(function() {
						$.post("../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php", { acc: acc,Digito_Inicial:Digito_Inicial,Digito_Final:Digito_Final,Turno:Turno }, function(data){	
						$("#recargado").html(data).fadeIn();
						});			
					});
				}
				$actual=0;

				function llamar(){
				timer = setInterval("recargar()", 2000);	
				}
				
				
				<!-- Funcion para Generar el Emergente -->
				$(document).ready(function(){

                  //Limpiar los buscadores
		
				  $('#open').click(function(){
						$('#popup').fadeIn('slow');
						$('.popup-overlay').fadeIn('slow');
						$('.popup-overlay').height($(window).height());
						llamar();
						return false;
					});


				  	$('#Editor_Boton').click(function(){
						$('#popup').fadeIn('slow');
						$('.popup-overlay').fadeIn('slow');
						$('.popup-overlay').height($(window).height());
						return false;
					});
					
					$('#close_popup').click(function(){
						$('#popup').fadeOut('slow');
						$('.popup-overlay').fadeOut('slow');
						 $("#open").finish();
						return false;
					});
					
					
					$("#Historia_Clinica_Salida_Validacion").click(function(){
						Validar_Cantidad();
					}); 						
				});

				function Validar_Cantidad(){
							var  Fecha_Inicio= $('#Fecha_Inicio').val();
							var  Fecha_Final= $('#Fecha_Final').val();
							var  Digito_Inicial= $('#Digito_Inicial').val();
							var  Digito_Final= $('#Digito_Final').val();
							var  Turno= $('#Turno').val();
							var Empleado=$('#IdEmpleado').val();
							var	NroHistoriaClinica= $('#Historia_Clinica').val();
							var UrlToPass = {
									Fecha_Inicio: $('#Fecha_Inicio').val(),
									Fecha_Final: $('#Fecha_Final').val(),
									NroHistoriaClinica: $('#Historia_Clinica').val(),
									acc : 'Historia_Clinica_Salida_Validacion'
									};

							$.ajax({
							url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
							type : 'POST',
							data : UrlToPass,
							success: function(res) {
							if(res==0)
							{
							$.alertx('Informacion Archivo Clinico','No tiene Citas el Paciente');			
							return;
							}	
							if(res==1)
							{
							Generar_Salida();
							return;
							}
							if(res>=1)
							{
							$.alertx('Informacion Archivo Clinico','Genere una Salida Manual el Paciente tiene '+res+' Citas');				
							}	
							}
							});	
				}
	
	</script>
	<!-- Fin de Script para Emergente de Cantidad de Historias Pendientes  -->	
	</head>	
	<div style="margin:15px !important">
	<?php
	  if($ListarDigitoTerminales != NULL)
	  {
	  $IdEmpleado=$_REQUEST["IdEmpleado"];
	  foreach($ListarDigitoTerminales as $digito){ 
	  $digito_inicial=$digito["digito_inicial"];
	  $digito_final=$digito["digito_final"];
	  }
	  }	  
    ?>
	
	<div class="overlay-container">
		<div class="window-container zoomin">
			<h3>Modicar Digitos Terminales</h3> 
			<table width="100%">
				<tr>
					  <td><span>Digito Inicial</span></td>
					  <td><input  type="text"  name="Digito_Inicial"  class="Digito_Inicial"   maxlength="2"  value="<?php echo  $digito_inicial ?>"></td>
				</tr>
				<tr>
					  <td><span>Digito Final</span></td>
					  <td><input  type="text"  name="Digito_Final"  class="Digito_Final"   maxlength="2"  value="<?php echo $digito_final ?>"></td>
				</tr>
				<tr style="display:none">
					  <td>IdEmpleado</td>
					  <td><input  type="text"  name="IdEmpleado"  class="IdEmpleado" value="<?php echo $IdEmpleado ?>"></td>
				</tr>
				<tr>
					  <td><span class="close" id="Modificar_DigitoTerminal">Modificar </span></td>
					  <td><span class="close">Cancelar</span></td>
				</tr>
			</table>
		<br>
		</div>
	</div>
	
	
	
	<!--Emergente para mostrar la Cantidad de Pendientes -->
	<div id="popup" style="display: none;">
    <div class="content-popup">
        <div class="close_popup">
		<a href="#" id="close_popup"><img src="../../MVC_Complemento/img/close.png"/></a>
		</div>
        <div>
        	<center><h1 style="color:red; font-size:40px">Citas del Dia (PENDIENTES)</h2></center>
            <p><center><span  style="font-size:300px;align:center; font-weight:bold" id="recargado"></span></center></p>
        </div>
    </div>
	</div>
	<div class="popup-overlay"></div>
	<!--Fin de Emergente para mostrar la Cantidad de Pendientes -->
	
	
    <h1>Salida HC - D&iacute;gito Terminal ( <?php  echo  $digito_inicial.' - '.$digito_final  ?> )
	<a rel="tooltip" title="Modificar mis Digitos Terminales" href="#"  
	class="button" data-type="zoomin"  class="Modificar_DigitoTerminal">
	<img src="../../MVC_Complemento/img/modiciar.jpg" width="16" height="16" />
	</a>
	</h1>
	  <table width="1040" border="0">
		<tr>
		  <td width="994">
		    <table width="100%">
			<tr>
			  <td width="66%"><fieldset>
				<legend>Reporte de HC
				<a href="#" id="open">¿Citas del Dia (PENDIENTES)?</a>
				</legend>
				<table width="900" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td colspan="8">
					<table width="100%">
					  <tr style="display:none">
					  <td><input  type="text"  name="Digito_Inicial"  id="Digito_Inicial" value="<?php echo  $digito_inicial ?>"></td>
					  <td><input  type="text"  name="Digito_Final"  id="Digito_Final" value="<?php echo $digito_final ?>"></td>
					  <td><input  type="text"  name="IdEmpleado"  id="IdEmpleado" value="<?php echo $IdEmpleado ?>"></td>
					  </tr>
					  <tr>
					    <td width="1%">&nbsp;</td>
						<td width="6%">Fecha Inicio</td>
						<td width="20%"> 
						<input  type="text"  name="Fecha_Inicio"  id="Fecha_Inicio" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="15"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Inicio",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador1"     // el id del botón que lanzará el calendario 
											}); 
										</script>	
						</td>
						<td width="8%">Fecha Final</td>
						<td width="19.5%">
						<input  type="text"  name="Fecha_Final"  id="Fecha_Final" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="15"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Final",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador2"     // el id del botón que lanzará el calendario 
											}); 
										</script>				
						</td>
						<td width="4%">Estado</td>
						<td width="5%">
						<div style="margin-top:10px">
								<select name="Tipo_estado" id="Tipo_estado"  style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="Totalidad">Totalidad</option>
									<option value="Pendientes">Pendientes</option>
								</select>
						</div>		
						</td>
						<td width="4%">Turno</td>
						<td width="15%">
								<select name="Turno"   id="Turno"  style="padding: 5px; margin:10px 10px -9px 10px;">
									<option value="%mañ%">Ma&ntilde;ana</option>
									<option value="%tar%">Tarde</option>
								</select>								
						</td>
						<td> 
						<input type="button"  id="Salida_HC" value="Aceptar">

						</td>
						</tr>
					</table>
					</td>
					 <!-- Aqui estan las funciones propias de jcal para poder Cargar las Fechas de Rango-->
					  <script type="text/javascript">
					  RANGE_CAL_1 = new Calendar({
							  inputField: "Fecha_Inicio",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaInicio_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
							  RANGE_CAL_2 = new Calendar({
							  inputField: "Fecha_Final",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaFinal_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
					</script>
				  </tr>
				  <tr>
				  <td>&nbsp;</td>
				  </tr>
				</table>
				</td>
				</tr>
			</table>
			<div style="margin:15px !important;float:left">
			<table width="850">
			<tr>
			<td width="130">
			<h3><b>Historia Cl&iacute;nica :</b></h3>
			</td>
			<td width="120">
			<input type="text" id="Historia_Clinica" name="Historia_Clinica"   id="Historia_Clinica" size="11" />
			</td>
			<td width="30">
			<input type="button" value="Aceptar" id="Historia_Clinica_Salida_Validacion">
			</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td width="100">
			<div style="<?php  echo $permiso; ?>">
			<strong style="width:40px;color:Red">¿Desea Editar ?</strong>
			</div>
			</td>
			<td style="width:10px">
			<div style="<?php  echo $permiso; ?>">
			<input type="checkbox" onclick="marcar(this);" />
			</div>
			</td>
			<td>&nbsp;</td>
			<td width="120">
			<input type="text" size="11" class="form-control pull-right"  id="search" placeholder="Buscar...">
			</td>
			<!--
			<td>
			<input type="button" value="Guardar Cambios" id="Cargar_Salida_HC">
			</td>
			-->
			<td>
			<input type="button" id="Limpiar" value="Limpiar" >
			</td>
			</tr>
		    </table>
			</div>
						<div style="margin:15px !important; clear:both;">
						<table width="970">
						<tr>
						  <td width="975">
						  <fieldset>
							<legend>Lista de Pacientes</legend>
							<table width="950" border="1" cellpadding="0" cellspacing="0" class="data">
							 <tr>
							   <th width="20" align="center">N&deg; Id</th>
							   <th width="180" align="center">Consultorio</th>
							   <th width="30" align="center">HC</th>
							   <th width="210" align="center">Apellidos y Nombres</th>
							   <th width="220" align="center">Observacion Salida HC</th>     
							 </tr>
							</table>
							<!--Inicio de Contenedor  ----> 
							<div id="scroll">								
								<div id="lineaResultado">
								</div>
							</div>
							<!--Fin de Contenedor  ----> 
						  </fieldset>
						  </td>
						</tr>
					    </table>
					    </div>
	    </table>
	</div>
	