<?php
		include '../lib/conexion.php';
		require_once '../PHPWord.php';
		$fecha_inicio=$_POST['fecha_inicio'];
		$fecha_inicio=substr($fecha_inicio,6, 4).'-'.substr($fecha_inicio,3, 2).'-'.substr($fecha_inicio,0, 2);
		$turno=$_POST['turno'];
		$usuario=$_POST['usuario'];
		$cita=$_POST['cita'];
		$hora=date("Y-m-d H:i:s");
		if($cita=='citados')
		{
		$signo="!=";
		}
		else
		{
		$signo="=";
		}
		if($turno=='mañ')
		{
		$t="MANANA";
		}
		else
		{
		$t="TARDE";
		}
		$PHPWord = new PHPWord(); 
		$section = $PHPWord->createSection();
		// Agregar Cabecera
		$header = $section->createHeader();
		$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'size'=>14));
		$PHPWord->addFontStyle('lStyle', array('bold'=>true, 'size'=>10));
        $PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
		$PHPWord->addParagraphStyle('rightStyle', array('align'=>'right', 'spaceAfter'=>100));
		$table = $header->addTable();
		$table->addRow();
		$table->addCell(4500)->addText('Hospital Nacional Daniel Alcides Carrion');
		$table->addCell(6000)->addImage('images/hndac.jpg', array('width'=>64, 'height'=>56, 'align'=>'right'));
		// Agregar Pie de Pagina
		$footer = $section->createFooter();
		$footer->addPreserveText('Pagina {PAGE} de {NUMPAGES}.', array('align'=>'center'));

		
		    $consulta="select servicios.IdServicio,Servicios.Nombre,Empleados.Nombres+' '+Empleados.ApellidoPaterno+' '+Empleados.ApellidoMaterno as NombreMedico,
			ProgramacionMedica.Fecha from ProgramacionMedica 
			inner join Medicos on ProgramacionMedica.IdMedico=Medicos.IdMedico 
			inner join Servicios on ProgramacionMedica.IdServicio=Servicios.IdServicio 
			inner join Empleados on Empleados.IdEmpleado=Medicos.IdEmpleado 
			inner join Turnos on ProgramacionMedica.IdTurno=Turnos.IdTurno 
			where ProgramacionMedica.Fecha='".$fecha_inicio."' and Turnos.Descripcion like '%".$turno."%'
			and Servicios.IdServicio not in (310,311,312,313,127,128,129,130,131,132,141,142,163,165,175,201,87) 
			order by Servicios.Nombre desc";
			
		$stmt = $dbh->prepare($consulta);
		$stmt->execute();
		$stmt->rowCount();
		$resultado = $stmt->fetchAll();

		foreach($resultado as $fila) 
		{
		$styleTable = array('borderSize'=>6, 'borderColor'=>'ffffff', 'cellMargin'=>10);
		$styleFirstRow = array('borderBottomSize'=>10, 'borderBottomColor'=>'000000', 'bgColor'=>'FFFFFF');
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);
		$section->addText('RELACION DE PACIENTES - ARCHIVEROS ( '.strtoupper($cita).' )', 'rStyle', 'pStyle');
		$section->addText('  DIA :   '.$fecha_inicio.'         TURNO : '.$t.'       FECHA : '.$hora, 'lStyle', 'rightStyle');
		$cabecera = $section->addTable('myOwnTableStyle');
		$cabecera->addRow(-300);
		$cabecera->addCell(12000, $styleCell)->addText('ESPECIALIDAD : '.utf8_decode($fila[1]), $fontStyle);
		$cabecera->addCell(11000, $styleCell)->addText('MEDICO : '.strtoupper(utf8_decode($fila[2])), $fontStyle);
			


		  $consulta="SELECT   Citas.IdCita,Servicios.Nombre,' ' + CASE 
						 WHEN Pacientes.ApellidoPaterno is NULL THEN '' 
						 ELSE Pacientes.ApellidoPaterno END + ' ' + CASE 
						 WHEN Pacientes.ApellidoMaterno is NULL THEN '' 
						 ELSE Pacientes.ApellidoMaterno END + ' ' + CASE 
						 WHEN Pacientes.PrimerNombre is NULL THEN ' ' 
						 ELSE Pacientes.PrimerNombre  END  AS NombreCompleto,
			Pacientes.NroHistoriaClinica,HistoriasSolicitadas.IdAtencion,HistoriasSolicitadas.IdServicio,HistoriasSolicitadas.IdMotivo,HistoriasSolicitadas.IdMovimiento,Citas.HoraSolicitud,CONVERT(VARCHAR(10), Citas.FechaSolicitud, 103) as Fsolicitud,
			CONVERT(VARCHAR(10), Citas.Fecha, 103) AS Fecha ,(DATEDIFF ( MINUTE , ProgramacionMedica.HoraInicio, Citas.HoraInicio )/ProgramacionMedica.TiempoPromedioAtencion)+1 as norden,fuentesfinanciamiento.descripcion FROM Citas  
			INNER JOIN HistoriasSolicitadas ON Citas.IdAtencion=HistoriasSolicitadas.IdAtencion 
			INNER JOIN atenciones ON atenciones.idatencion=citas.idatencion
			INNER JOIN Pacientes on Citas.IdPaciente=Pacientes.IdPaciente  
			INNER JOIN Servicios ON Citas.IdServicio =  Servicios.IdServicio 
			INNER JOIN fuentesfinanciamiento ON fuentesfinanciamiento.idFuenteFinanciamiento=atenciones.idFuenteFinanciamiento
			INNER JOIN ProgramacionMedica ON Citas.IdProgramacion =  ProgramacionMedica.IdProgramacion
			INNER JOIN Turnos ON ProgramacionMedica.IdTurno =  Turnos.IdTurno
			where  Servicios.IdServicio='".$fila[0]."'   
				   and Citas.Fecha='".$fecha_inicio."'   
				   and Citas.FechaSolicitud".$signo."'".$fecha_inicio."'
				   and Turnos.Descripcion like '%".$turno."%' 
			order by right(Pacientes.NroHistoriaClinica,2),right(Pacientes.NroHistoriaClinica,4)";
			
			
			
		$stmt2 = $dbh->prepare($consulta);
		$stmt2->execute();
		$stmt2->rowCount();
		$resultado2 = $stmt2->fetchAll();
		$tabla = $section->addTable();
		$tabla->addRow(-300);
		$tabla->addCell(380)->addText('ID', $fontStyle);
		$tabla->addCell(960, $styleCell)->addText(' HC', $fontStyle);
		$tabla->addCell(8000, $styleCell)->addText(' NOMBRES Y APELLIDOS', $fontStyle);
		$tabla->addCell(1800, $styleCell)->addText('TIPO', $fontStyle);
		$tabla->addCell(2000, $styleCell)->addText('OBSERVACION', $fontStyle);		
		$i=0;
		foreach ($resultado2 as $row)
		{
		 $i=$i+1;	
		$tabla->addRow(-300);
		$tabla->addCell(380, $styleCell)->addText(' '.$i, $fontStyle);
		$tabla->addCell(960, $styleCell)->addText(' '.utf8_decode($row[3]), $fontStyle);
		$tabla->addCell(8000, $styleCell)->addText(' '.utf8_decode($row[2]), $fontStyle);
		$tabla->addCell(1800, $styleCell)->addText(' '.utf8_decode($row[12]), $fontStyle);
		$tabla->addCell(2000, $styleCell)->addText('', $fontStyle);	
		}
		$section->addText($value);
		
		
		
		$styleTable = array('borderSize'=>6, 'borderColor'=>'ffffff', 'cellMargin'=>10);
		$styleFirstRow = array('borderBottomSize'=>10, 'borderBottomColor'=>'000000', 'bgColor'=>'FFFFFF');
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);
		$section->addText('RELACION DE PACIENTES - CONSULTA EXTERNA ( '.strtoupper($cita).' )', 'rStyle', 'pStyle');
		$section->addText('  DIA :   '.$fecha_inicio.'         TURNO : '.$t.'       FECHA : '.$hora, 'lStyle', 'rightStyle');
		$cabecera = $section->addTable('myOwnTableStyle');
		$cabecera->addRow(-300);
		$cabecera->addCell(12000, $styleCell)->addText('ESPECIALIDAD : '.utf8_decode($fila[1]), $fontStyle);
		$cabecera->addCell(11000, $styleCell)->addText('MEDICO : '.strtoupper(utf8_decode($fila[2])), $fontStyle);

		$consulta="SELECT   Citas.IdCita,Servicios.Nombre,' ' + CASE 
						 WHEN Pacientes.ApellidoPaterno is NULL THEN '' 
						 ELSE Pacientes.ApellidoPaterno END + ' ' + CASE 
						 WHEN Pacientes.ApellidoMaterno is NULL THEN '' 
						 ELSE Pacientes.ApellidoMaterno END + ' ' + CASE 
						 WHEN Pacientes.PrimerNombre is NULL THEN ' ' 
						 ELSE Pacientes.PrimerNombre  END  AS NombreCompleto,
			Pacientes.NroHistoriaClinica,HistoriasSolicitadas.IdAtencion,HistoriasSolicitadas.IdServicio,HistoriasSolicitadas.IdMotivo,HistoriasSolicitadas.IdMovimiento,Citas.HoraSolicitud,CONVERT(VARCHAR(10), Citas.FechaSolicitud, 103) as Fsolicitud,
			CONVERT(VARCHAR(10), Citas.Fecha, 103) AS Fecha ,(DATEDIFF ( MINUTE , ProgramacionMedica.HoraInicio, Citas.HoraInicio )/ProgramacionMedica.TiempoPromedioAtencion)+1 as norden,fuentesfinanciamiento.descripcion,
			[SIGH_EXTERNA].[dbo].[SisFuaAtencion].FuaDisa+'-'+[SIGH_EXTERNA].[dbo].[SisFuaAtencion].FuaLote+'-'+[SIGH_EXTERNA].[dbo].[SisFuaAtencion].FuaNumero as Fuas
			FROM Citas  
			INNER JOIN HistoriasSolicitadas ON Citas.IdAtencion=HistoriasSolicitadas.IdAtencion 
			INNER JOIN atenciones ON atenciones.idatencion=citas.idatencion
			INNER JOIN Pacientes on Citas.IdPaciente=Pacientes.IdPaciente  
			INNER JOIN Servicios ON Citas.IdServicio =  Servicios.IdServicio 
			INNER JOIN fuentesfinanciamiento ON fuentesfinanciamiento.idFuenteFinanciamiento=atenciones.idFuenteFinanciamiento
			INNER JOIN ProgramacionMedica ON Citas.IdProgramacion =  ProgramacionMedica.IdProgramacion
			INNER JOIN Turnos ON ProgramacionMedica.IdTurno =  Turnos.IdTurno
			LEFT JOIN   [SIGH_EXTERNA].[dbo].[SisFuaAtencion] ON atenciones.IdCuentaAtencion=[SIGH_EXTERNA].[dbo].[SisFuaAtencion].IdCuentaAtencion
			where  Servicios.IdServicio='".$fila[0]."'   
				   and Citas.Fecha='".$fecha_inicio."'   
				   and Citas.FechaSolicitud".$signo."'".$fecha_inicio."'
				   and Turnos.Descripcion like '%".$turno."%' 
			order by (DATEDIFF ( MINUTE , ProgramacionMedica.HoraInicio, Citas.HoraInicio )/ProgramacionMedica.TiempoPromedioAtencion)+1 asc";
			
			
			
		$stmt2 = $dbh->prepare($consulta);
		$stmt2->execute();
		$stmt2->rowCount();
		$resultado2 = $stmt2->fetchAll();
		$tabla = $section->addTable();
		$tabla->addRow(-300);
		$tabla->addCell(1000)->addText('N.CUPOS', $fontStyle);
		$tabla->addCell(960, $styleCell)->addText(' HC', $fontStyle);
		$tabla->addCell(8000, $styleCell)->addText(' NOMBRES Y APELLIDOS', $fontStyle);
		$tabla->addCell(1800, $styleCell)->addText('TIPO', $fontStyle);
		$tabla->addCell(3000, $styleCell)->addText('NRO. FUA', $fontStyle);		
		$i=0;
		foreach ($resultado2 as $row)
		{
		 $i=$i+1;	
		$tabla->addRow(-300);
		$tabla->addCell(960, $styleCell)->addText(' '.utf8_decode($row[11]), $fontStyle);
		$tabla->addCell(1800, $styleCell)->addText(' '.utf8_decode($row[3]), $fontStyle);
		$tabla->addCell(8000, $styleCell)->addText(' '.utf8_decode($row[2]), $fontStyle);
		$tabla->addCell(1800, $styleCell)->addText(' '.utf8_decode($row[12]), $fontStyle);
		$tabla->addCell(3000, $styleCell)->addText(''.utf8_decode($row[13]), $fontStyle);
		}
		$section->addText($value);
		$section->addPageBreak();	
		}
		
		
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		$objWriter->save('repositorio/consolidado'.utf8_decode($usuario).'-'.$i.'.docx');
		header("location:repositorio/consolidado".$usuario.'-'.$i.".docx");	
					
			





			
?>