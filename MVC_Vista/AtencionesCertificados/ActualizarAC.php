<?php
if($ImprimirCertificado!=NULL){
				for ($i=0; $i < count($ImprimirCertificado); $i++) {
				$IdAtencionCertMed=$ImprimirCertificado[$i]["IdAtencionCertMed"];
				$AnnioCertificado=$ImprimirCertificado[$i]["AnnioCertificado"];
				$NroCertificado=$ImprimirCertificado[$i]["NroCertificado"];
				
				$FechaEmision=$ImprimirCertificado[$i]["FechaEmision"];
				
				$feA=substr($FechaEmision,0,4);
				$feM=substr($FechaEmision,5,2);
				$feD=substr($FechaEmision,8,2);								
				
				
				$IdDistritoDomicilio=$ImprimirCertificado[$i]["IdDistritoDomicilio"];

	
				$EdadPaciente=$ImprimirCertificado[$i]["EdadPaciente"];
				$IdPaciente=$ImprimirCertificado[$i]["IdPaciente"];
				$DireccionDomicilio=$ImprimirCertificado[$i]["DireccionDomicilio"];
				
				$NaturalezaIncapacidad=$ImprimirCertificado[$i]["NaturalezaIncapacidad"];
				
				if($NaturalezaIncapacidad=='1'){
					$optNIA='X';
					$optNIB='';
					$optNIC='';
				}else if($NaturalezaIncapacidad=='2'){
					$optNIB='X';
					$optNIC='';
					$optNIA='';
				}else if($NaturalezaIncapacidad=='3'){
					$optNIC='X';
					$optNIA='';
					$optNIB='';
				}
				
				$GradoIncapacidad=$ImprimirCertificado[$i]["GradoIncapacidad"];
				
				if($GradoIncapacidad=='1'){
					$optGIA='X';
					$optGIB='';
					$optGIC='';
				}else if($GradoIncapacidad=='2'){
					$optGIB='X';
					$optGIC='';
					$optGIA='';
				}else if($GradoIncapacidad=='3'){
					$optGIC='X';
					$optGIA='';
					$optGIB='';
					
				}

				
				$MenoscaboCombinado=$ImprimirCertificado[$i]["MenoscaboCombinado"];
				$TipoActividad=$ImprimirCertificado[$i]["TipoActividad"];
				$PosibilidadReubicaLaboral=$ImprimirCertificado[$i]["PosibilidadReubicaLaboral"];
				$Edad=$ImprimirCertificado[$i]["Edad"];
				$MenoscaboGlobal=$ImprimirCertificado[$i]["MenoscaboGlobal"];
				
				$FechaInicioIncapacidad=$ImprimirCertificado[$i]["FechaInicioIncapacidad"];
				$SwPrecisable=$ImprimirCertificado[$i]["SwPrecisable"];
				if($SwPrecisable=='1'){
				$SwPrec='X';
				$fiA='';
				$fiM='';
				$fiD='';
				$fechaInca='';
				}else{
				$SwPrec='';
				$fiA=substr($FechaInicioIncapacidad,0,4);
				$fiM=substr($FechaInicioIncapacidad,5,2);
				$fiD=substr($FechaInicioIncapacidad,8,2);	
				$fechaInca=$fiD.'/'.$fiM.'/'.$fiA;
				}
				$Observaciones=$ImprimirCertificado[$i]["Observaciones"];
				//$CodigoCMCI=$ImprimirCertificado[$i][""];
				$IdEmpleadoCrea=$ImprimirCertificado[$i]["IdEmpleadoCrea"];
				$FechaCrea=$ImprimirCertificado[$i]["FechaCrea"];
				$NroImpresion=$ImprimirCertificado[$i]["NroImpresion"];
				$ApellidoPaterno=$ImprimirCertificado[$i]["ApellidoPaterno"];
				$ApellidoMaterno=$ImprimirCertificado[$i]["ApellidoMaterno"];
				$PrimerNombre=$ImprimirCertificado[$i]["PrimerNombre"];
				$SegundoNombre=$ImprimirCertificado[$i]["SegundoNombre"];
				$FechaNacimiento=$ImprimirCertificado[$i]["FechaNacimiento"];
				$NroDocumento=$ImprimirCertificado[$i]["NroDocumento"];
				$DireccionDomicilio=$ImprimirCertificado[$i]["DireccionDomicilio"];
				
				$IdTipoSexo=$ImprimirCertificado[$i]["IdTipoSexo"];	
					if($IdTipoSexo=='1'){$sexo='MASCULINO';}else{$sexo='FEMENINO';}
				$dep=$ImprimirCertificado[$i]["dep"];
				$prov=$ImprimirCertificado[$i]["prov"];
				$dist=$ImprimirCertificado[$i]["dist"];	
				}
			}
            ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registro Certificado Medico </title>
<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
       
</head>


 <script>

	  $.extend( $( "#FechaIniIncapacidad" ).datebox.defaults,{
		formatter:function(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
		},
		parser:function(s){
			if (!s) return new Date();
			var ss = s.split('/');
			var d = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var y = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
	});	
// FIN (A2)						 
 </script>
<script>
$(document).ready(function(){  
						$('#Descripcion').combogrid({
               			 queryParams: {
                    	'edadpaciente':'<?php echo $EdadPaciente ?>',
                    	'IdTipoSexo': '<?php echo $IdTipoSexo ?>'
               
                			}
            			});
})
</script>

<body>


<div id="cc" class="easyui-layout" style="width:100%;height:100%;">
    <div data-options="region:'north',split:true" style="height:100px;">    

        <!-- Menu-->
        <div class="easyui-panel" style="padding:0px;">
     
            <a href="#" onclick="ActualizaDatos()" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" >Actualizar</a>
            
            <a href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Certificados&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true">Cancelar</a>
           <!-- <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true">Imprimir</a>-->
		  <a href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Certificados&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>" class="easyui-linkbutton" data-options="iconCls:'icon-back',plain:true">Salir</a>
      </div>
      <!-- Fin Menu-->
        
  </div>
  <div data-options="region:'south',title:'Actualiza Certificado Medico'" style="height:95%;" class="easyui-tabs">
        <div title="Datos del Paciente" style="padding:10px">
          <table width="946" border="0" cellpadding="0" cellspacing="0" style="font-size:12px">
            <tr>
              <td align="right">Nro Certificado&nbsp;</td>
              <td><input class="easyui-textbox" id="NroCertificado" name="NroCertificado" style="width:80%;height:25px;"  value="<?php echo $NroCertificado ?>"></td>
              <td align="right">Fecha Emision&nbsp;</td>
              <td><input class="easyui-textbox" id="FechaEmision" name="FechaEmision" style="width:80%;height:25px;" readonly  value="<?php echo vfecha($FechaEmision) ?>"></td>
              <td colspan="2" rowspan="2" align="right"><strong style="color:#F00">Nota: en caso falte otros datos del paciente comunicarse con el area correspondiente para la actualizacion.</strong></td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="144" align="right">Apellido Paterno&nbsp;</td>
              <td width="149"><input class="easyui-textbox" id="ApellidoPaterno" name="ApellidoPaterno" style="width:100%;height:25px;" readonly value="<?php echo mb_strtoupper($ApellidoPaterno) ?>"></td>
              <td width="104" align="right">Apellido Materno&nbsp;</td>
              <td width="168"><input class="easyui-textbox" id="ApellidoMaterno" name="ApellidoMaterno" style="width:100%;height:25px;" readonly value="<?php echo mb_strtoupper($ApellidoMaterno) ?>"></td>
              <td width="80" align="right">Nombres&nbsp;</td>
              <td width="301"><input class="easyui-textbox" id="Nombres" name="Nombres" style="width:100%;height:25px;" readonly value="<?php echo mb_strtoupper($PrimerNombre).' '.mb_strtoupper($SegundoNombre) ?>"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">Nº DNI&nbsp;</td>
              <td><input class="easyui-textbox" id="NroDocumento" name="NroDocumento" style="width:50%;height:25px;" readonly value="<?php echo $NroDocumento ?>" /></td>
              <td align="right">Sexo</td>
              <td><input class="easyui-textbox" id="IdTipoSexo" name="IdTipoSexo" style="width:50%;height:25px;" readonly value="<?php echo $sexo ?>"/></td>
              <td align="right">Edad&nbsp;</td>
              <td><input class="easyui-textbox" id="edadpaciente"  name="edadpaciente" style="width:20%;height:25px;" readonly value="<?php echo $EdadPaciente ?>"/>
              Fec.Nac&nbsp;                <input class="easyui-textbox" id="FechaNacimiento" style="width:40%;height:25px;" readonly value="<?php echo vfecha(substr($FechaNacimiento,0,10)) ?>"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">Distrito&nbsp;</td>
              <td><input class="easyui-textbox" id="NomDist" style="width:100%;height:25px;" readonly value="<?php echo mb_strtoupper($dist) ?>" /></td>
              <td align="right">Provincia&nbsp;</td>
              <td><input class="easyui-textbox" id="NomProv" style="width:100%;height:25px;" readonly value="<?php echo mb_strtoupper($prov) ?>"/></td>
              <td align="right">Departamento&nbsp;</td>
              <td><input class="easyui-textbox" id="NomDep" style="width:100%;height:25px;" readonly value="<?php echo mb_strtoupper($dep) ?>"/></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">Direccion&nbsp;</td>
              <td colspan="5"><input class="easyui-textbox"  id="DireccionDomicilio" name="DireccionDomicilio" style="width:100%;height:25px;" value="<?php echo mb_strtoupper($DireccionDomicilio) ?>"  /></td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
              <td align="right">
              <input  type="hidden" name="IdPaciente" id="IdPaciente"  value="<?php echo $IdPaciente ?>"/></td>
              <td colspan="5">&nbsp;
              <input  type="hidden" name="IdDistritoDomicilio" id="IdDistritoDomicilio" readonly value="<?php echo $IdDistritoDomicilio ?>"/>
              <input type="hidden" name="IdEmpleado"   id="IdEmpleado" value="<?php echo $_GET['IdEmpleado']; ?>"  />
              <input name="IdAtencionCertMed" type="hidden"  id="IdAtencionCertMed" value="<?php echo $IdAtencionCertMed ?>"  /></td>
            </tr>
          </table>
      </div>
        <div title="Diagnosticos" style="padding:10px">
        <table width="945" border="0" style="font-size:12px">
  <tr>
    <td>Busqueda:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    
    <td width="324">
        <select id="Descripcion" class="easyui-combogrid" name="Descripcion" style="width:250px;height:30px;"
                                            data-options="
                                            prompt:'Bus. Diagnostico.',
                                            panelWidth:500,
                                            //readonly:'true',
                                            url: '../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=BusquedaDiagnosticosx',                                                           
                                            type: 'POST',
                                            dataType: 'json',                                                            
                                            idField:'CodigoCIE10',
                                            textField:'Descripcion',
                                            mode:'remote',
                                            columns:[[
                                            {field:'CodigoCIE10',title:'Codigo CIE10',width:70},
                                            {field:'Descripcion',title:'Descripcion',width:350}                                          
                                            ]],
                                            onSelect:function(index,row){              
                                            
                                            $('#CodigoCIE10').val(row.CodigoCIE10);
                                            $('#Descripcion').val(row.Descripcion);                

                                            var tb = $('#Descripcion').combogrid('textbox');
                                            tb.bind('keydown',function(event){
                                            var keycode = (event.keyCode ? event.keyCode : event.which);
                                            if(keycode == '13'){                              
											// $('#CPTCantidad').select();					
                                             //$('#CPTCantidad').focus();
                                              
                                             
                                             //$('#RecetaCantidad:text:visible:first').focus();
                                             //RecetaRecalcularTotal();

                                            //exit;	
                                            }              
                                            });                 

                                            },
                                            fitColumns:true

                                            "></select>
      
      
      </td>
      <td width="167"><!--<input type="text" name="CodigoCIE10" id="CodigoCIE10"  class="easyui-searchbox" 
     style="width:100%;height:25px;"/>-->
        </td>
    <td width="440">
    
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" onclick="agregar()" onkeyup="agregar()" id="agregar" >Agregar</a>&nbsp;
    
    
    </td>
  </tr>
  
  <tr>
    <td colspan="3">
    
    
    
    
    
    </td>
    </tr>
     <table class="easyui-datagrid" style="width:50%;height:70%"
                            id="tblDiagnosticos"  >  
                            <thead>
                                <tr>
                                	
                                    <th data-options="field:'x1CodigoCIE10',width:100"></th>
                                    <th data-options="field:'x1Descripcion',width:450">Diagnóstico</th>
                                     <th data-options="field:'action',width:100,align:'right'">Accion</th>
                                </tr>
                            </thead>  
                        </table>
</table>

        
        
      </div>
        <div title="Caracteristicas" style="padding:10px">
          <table width="947" border="0" cellpadding="0" cellspacing="0" style="font-size:12px">
            <tr>
              <td colspan="3"><strong>Caracteristicas de la incapacidad</strong></td>
            </tr>
            <tr>
              <td colspan="3">Naturaleza de la Incapacidad</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="190">
              <label>
              <input type="radio" name="optNI" id="optNI" value="1" <?php if($optNIA=='X'){?> checked <?php }?> onclick="HabilitarGradoActividad()" />
              Temporal
              </label></td>
              <td width="190"><label>
              <input type="radio" name="optNI" id="optNI" value="2" <?php if($optNIB=='X'){?> checked <?php }?> onclick="HabilitarGradoActividad()"/>
              Permanente
              </label></td>
              <td width="567"><label>
              <input type="radio" name="optNI" id="optNI" value="3" <?php if($optNIC=='X'){?> checked <?php }?> onclick="HabilitarGradoActividad()"/>
              No Incapacidad
              </label></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Grado de la Incapacidad</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><label>
              <input type="radio" name="optGI" id="optGIA" value="1" <?php if($optGIA=='X'){?> checked <?php }?>/>
              Parcial
              </label></td>
              <td><label>
              <input type="radio" name="optGI" id="optGIB" value="2" <?php if($optGIB=='X'){?> checked <?php }?>/>
              Total
              </label></td>
              <td><label>
              <input type="radio" name="optGI" id="optGIC" value="3" <?php if($optGIC=='X'){?> checked <?php }?>/>
              Gran Incapacidad
              </label></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Menoscabo</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3"><table width="200" border="1" cellpadding="1" cellspacing="1">
                <tr>
                    <td><table width="795" border="0" cellpadding="0" cellspacing="0" style="font-size:12px">
                      <tr>
                        <td width="80">&nbsp;</td>
                        <td width="151">&nbsp;</td>
                        <td width="364">&nbsp;</td>
                        <td width="200">Porcentanje</td>
                      </tr>
                      <tr>
                        <td colspan="3" align="right">Menoscabo Combinado&nbsp;</td>
                        <td><input name="valor_combinado" type="text" class="easyui-numberspinner" id="valor_combinado" style="width:100%;height:25px;" value="<?php echo $MenoscaboCombinado ?>"    data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="center">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="2" rowspan="5" align="center">FACTORES COMPLEMENTARIOS</td>
                        <td align="right">Tipos de Actividad&nbsp;</td>
                        <td><input name="valor_actividad" type="text" class="easyui-numberspinner" id="valor_actividad" style="width:100%;height:25px;" value="<?php echo $TipoActividad ?>" data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="right">Posibilidad de reubicacion laboral&nbsp;</td>
                        <td><input name="valor_posibilidad" type="text" class="easyui-numberspinner" id="valor_posibilidad"  style="width:100%;height:25px;" value="<?php echo $PosibilidadReubicaLaboral ?>" data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="right">Edad&nbsp;</td>
                        <td><input name="valor_edad" type="text" class="easyui-numberspinner" id="valor_edad"  style="width:100%;height:25px;" value="<?php echo $Edad ?>" data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="right">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="3" align="right">Menoscabo Global&nbsp;</td>
                        <td><input name="valor_global" type="text" class="easyui-textbox" id="valor_global"  style="width:100%;height:25px;" value="<?php echo $MenoscaboGlobal ?>"/></td>
                      </tr>
                    </table></td>
                </tr>
                </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td> <label for="chkprecisable"><input name="chkprecisable" type="checkbox" id="chkprecisable" onclick="HabilitaFechaPrecisable()" 
              <?php if($SwPrecisable=='1'){ ?> value="1" checked="checked" <?php }?> />
               
              No es Precisable</label></td>
              <td><input class="easyui-datebox" id="FechaIniIncapacidad" name="FechaIniIncapacidad" style="width:80%;height:25px;" <?php if($SwPrecisable=='0'){ ?>
              value="<?php echo $fechaInca ?>" <?php }?>>&nbsp;</td>
              <td>(fecha inicio incapacidad)</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>
        <div title="Observaciones" style="padding:10px">
        Observaciones
        <input class="easyui-textbox"  labelPosition="top" multiline="true" value="<?php echo $Observaciones?>" style="width:40%;height:180px" id="observaciones" name="observaciones">
      </div>
       <!-- <div title="Miembros CMCI" style="padding:10px">
                 <table  class="easyui-datagrid" id="tblMiembros"  style="width:50%;height:400px" data-options="
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false">
                            <thead>
                                <tr>
                                    <th data-options="field:'x1Cargo',width:150">Cargo</th>
                                    <th data-options="field:'x1Nombre',width:300">Nombre</th>
                                    <th data-options="field:'x1Estado',width:80">Estado</th>
                                    <th data-options="field:'x1Vigencia',width:220">Vigencia</th>
                                </tr>
                            </thead>  
                        </table>
				</table>
        </div>-->
    </div>    
</div>

 <script type="text/javascript" src="../../MVC_Vista/AtencionesCertificados/FuncionesACupdate.js"></script>
 
 <script>
		function getDataDiagnostico(){
			var rows = [];
		<?php 
		
			if($ImprimirCertificado!=NULL){
				for ($i=0; $i < count($ImprimirCertificado); $i++) {									
					$x1CodigoCIE10 = mb_strtoupper($ImprimirCertificado[$i]["IdDiagnostico"]);
					$x1Descripcion = mb_strtoupper($ImprimirCertificado[$i]["CodigoCIE10"].' '.$ImprimirCertificado[$i]["Descripcion"]);						
		?>			
					rows.push({					
					x1CodigoCIE10: 	'<?php echo $x1CodigoCIE10; ?>',
					x1Descripcion: 	'<?php echo $x1Descripcion; ?>',
					action:'<a href="javascript:void(0)" onclick="CIE10Eliminar(this)">Eliminar</a>',
					
				});
			<?php  
		}
	}
?>	return rows;
		}		
		$(function(){			
			var dg =$('#tblDiagnosticos').datagrid({data:getDataDiagnostico()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			dg.datagrid('enableFilter');
		});	
	</script>
 
 
 
<?php /*?><script>
		function getData(){
			var rows = [];
		<?php 
		$anio_actual=date('Y');
		$mes_actual=date('n'); //dial mes sin ceros
			$resultados =SIGESA_CertificadoMedico_ListaMiembrosCMI_LCO($mes_actual,$anio_actual);
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {									
					if( $resultados[$i]["Cargo"]=='1'){$x1Cargo ='PRESIDENTE CMCI';}else if( $resultados[$i]["Cargo"]=='2'){$x1Cargo ='MIEMBRO CMCI';};
					$x1Nombre = mb_strtoupper($resultados[$i]["CMCI"]);
					$x1Vigencia = 'Desde '.vfecha($resultados[$i]["FecInicioCMI"]).' hasta '.vfecha($resultados[$i]["FecFinCMI"]);	
					if($resultados[$i]["Estado"]=='1'){$x1Estado='ACTIVO';	};							
		?>			
					rows.push({					
					x1Cargo: 	'<?php echo $x1Cargo; ?>',
					x1Nombre: 	'<?php echo $x1Nombre; ?>',
					x1Estado: 	'<?php echo $x1Estado; ?>',
					x1Vigencia: '<?php echo $x1Vigencia; ?>',	
				});
			<?php  
		}
	}
?>	return rows;
		}		
		$(function(){			
			var dg =$('#tblMiembros').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			dg.datagrid('enableFilter');
		});	
	</script><?php */?>
</body>
</html>