<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Listado de Certificados</title>
</head>
<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>
         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
            <style>
		.icon-filter{
			background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
		}
	</style> 
    <style>	
		 #fm{
                margin:0;
                padding:10px 30px;
            }
			#fmRegistro{
                margin:0;
                padding:10px 30px;
            }
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:60px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				margin-left:10px;
            }
			.fitem2 input {				
				 margin-right:25px;			 
				 
			}			
			#fmDevolver{
                margin:0;
                padding:10px 30px;
            }
	</style> 
  <script>
        function submitForm(){
            $('#frmconsulta').form('submit');
        }
    </script>      
<body>
	<div style="margin:0px 0;"></div>
	<form id="form1" name="form1" method="post" action="">
       <div id="tb" style="padding:5px;height:auto">
		<?php
		if($ListarPermisos != NULL)
	    {
		foreach($ListarPermisos as $Permiso)
		{ 
		$Agregar=$Permiso["Agregar"];
		$Modificar=$Permiso["Modificar"];
		$Eliminar=$Permiso["Eliminar"];
		$Consultar=$Permiso["Consultar"];
		}
		?>
		<?php						
		if($Consultar==0){?>
		<script>$(function(){ $('#Consultar').linkbutton({disabled:true});});</script>
		<?php } ?>
		<?php if($Modificar==0){?>
		<script>$(function(){ $('#Modificar').linkbutton({disabled:true});});</script>
		<?php } ?>
		<?php if($Eliminar==0){?>
		<script>$(function(){ $('#Eliminar').linkbutton({disabled:true});});</script>			
		<?php } ?>
		<?php if($Agregar==0){?>
		<script>$(function(){ $('#Agregar').linkbutton({disabled:true});});</script>
		<?php } ?>
		<?php
		}						
		?>
        <div style="margin-bottom:5px">
        	<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="nuevo()"  id="Agregar">Nuevo</a>
     		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editar()" id="Modificar">Editar</a>   
            <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="Abrir_PDF()" id="Consultar">Vista Previa</a>
           <?php /*?> <a href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Consulta&IdEmpleado=<?php echo $_GET['IdEmpleado'];?>" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="ayuda();">Ayuda</a><?php */?>
		    <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="salir();">Salir</a>
        </div>	
        <div>
        </div>
    </div>
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Listado de Certificados" style="width:888px;height:400px" data-options="
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
            	pageSize:30,
            	url:'../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Mostrar_AtencionesCertificados',method:'get'">
		<thead>
			<tr>
				<th field="anio"  width="100">Año Certificado</th>
				<th field="ndoc" width="100">N° Certificado</th>
				<th field="paciente" width="300">Paciente</th>
				<th field="dni" width="100" >N° DNI</th>
				<th field="femision" width="100" >Fecha Emision</th>
                
			
			</tr>
		</thead>
	</table>
   <script>
		function getData(){
		var rows = [];
		<?php 
	     $i = 0;	
		 $ListadoCertificados=SIGESA_CertificadoMedico_Listar_LCO();									
		if($ListadoCertificados!=NULL){
			foreach($ListadoCertificados as $item)
			{
				
				$input=$item["NroCertificado"];
				$NroCert= str_pad($input, 5, "0", STR_PAD_LEFT); 
				
				 ?>
					rows.push({
						anio: '<?php echo $item["AnnioCertificado"]; ?>',
						ndoc: '<?php echo $NroCert?>',
						paciente: '<?php echo mb_strtoupper(($item["ApellidoPaterno"])).' '.mb_strtoupper(($item["ApellidoMaterno"])).' '.mb_strtoupper(($item["PrimerNombre"])).' '.mb_strtoupper(($item["SegundoNombre"])); ?>',
						dni:  '<?php echo  $item["NroDocumento"]; ?>' ,
						femision: '<?php echo  vfecha(substr($item["FechaEmision"],0,10)); ?>',
						//tipo: 'hol',
						IdAtencionCertMed:'<?php echo  $item["IdAtencionCertMed"]; ?>',
						NroImpresion:'<?php echo  $item["NroImpresion"]; ?>',
						
						
					});
				//}
				<?php  $i += 1;	
			}
		}
		?>
		return rows;
		}
		$(function(){
			var dg = $('#dg').datagrid();
			dg.datagrid('enableFilter');

		});
	</script> 
    <script>
			function editar(){					
			var row = $('#dg').datagrid('getSelected');
				if (row){			
					if(row.NroImpresion=='0' || row.NroImpresion==''){
					location.href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Actualizar&NroDocumentoCertificado="+row.IdAtencionCertMed+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"; 
						//$.messager.alert('Info', row.c_estado);					
						//$.messager.alert('Info', row.c_numeoc+":"+row.c_nomprv+":"+row.d_fecoc);
					}else{
					$.messager.alert('SIGESA', ' No es posible actualizar ya fue impreso ','warning');		
					}
				}	
		}
		
		function imprimir(){
				var row = $('#dg').datagrid('getSelected');
				location.href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=ImprimeCertificadoMedico&NroDocumentoCertificado="+row.IdAtencionCertMed+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"; 
			}
			function nuevo(){
				
				location.href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Registro&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"; 
			}
	
	
	
			function imprimirpdf(){
				var row = $('#dg').datagrid('getSelected');
				location.href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=ImprimeCertificadoMedicopdf&NroDocumentoCertificado="+row.IdAtencionCertMed+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"; 
			}
			function nuevo(){
				
				location.href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Registro&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"; 
			}
	 function Abrir_PDF(){

			var row = $('#dg').datagrid('getSelected');
	
	valor=row.IdAtencionCertMed;
	NombreReporte='CertificadosMedicos';
	
	 var googlewin=dhtmlwindow.open("googlebox", "iframe", "../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=ImprimeCertificadoMedicopdf&NroDocumentoCertificado="+valor+"&NombreReporte="+NombreReporte+"&IdEmpleado=<?php echo $_GET['IdEmpleado'];?>",       "Certificado Medico   ", "width=999px,height=600px,resize=0,scrolling=1,center=1", "recal");		
		}
	</script>
 <link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
 <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
    </form>
     <script type="text/javascript" src="../../MVC_Vista/AtencionesCertificados/FuncionesAC.js"></script>
</body>
</html>