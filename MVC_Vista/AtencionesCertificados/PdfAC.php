﻿<?php
require('../../MVC_Complemento/fpdf/fpdf.php');
function CalcularE($fecha)
  {
    $anionaz = date("Y",strtotime($fecha)); 
    $anioactual = date("Y");
    $mesnaz = date("n",strtotime($fecha));
    $mesact = date("n");
    $dianaz = date("j",strtotime($fecha));
    $diaact = date("j");
    $edad = $anioactual-$anionaz;
    if($mesact == $mesnaz && $dianaz > $diaact)
      {$edad = $anioactual-$anionaz-1;}
    if($mesact < $mesnaz)
      {$edad = $anioactual-$anionaz-1;}
    
    return $edad;
}
//include('../../MVC_Complemento/librerias/Funciones.php');
//require('../../MVC_Complemento/fpdf/mysql_table.php');
//--include('../../MVC_Complemento/fpdf/php-barcode.php'); CalcularEdadTonno($data_mascara[$i]['FNACIMIENTO']);

class PDF extends FPDF{
	function Header(){	
		$this->SetY(10);
		$this->SetFont('Arial','',7);
		$this->Cell(24,6,' ',0,0,'C');
		//$this->Image('img/region_callao.jpg',40,8,18,20);
		//$this->SetFont('Arial','B',12);
		//$this->Cell(142,10,'GOBIERNO REGIONAL DEL CALLAO',0,0,'C');
		$this->Ln(7);
		$this->SetFont('Arial','b',12);
		$this->Cell(0,10,utf8_decode('CERTIFICADO MÉDICO - DS - N° 166 - 2005 - EF'),0,0,'C');
		
		$this->Image('../../MVC_Complemento/img/hndac.jpg',10,10,20);	
		//$this->SetFont('Arial','',5);
		$this->SetX(170.5);
		$this->Ln(7.5);

		$this->SetFont('Arial','',10);
		$this->Ln(1);
		//$this->SetX(67);
		//$this->Cell(70,10,utf8_decode('HOSPITAL NACIONAL "DANIEL A. CARRIÓN"'),0,0);
		$this->Ln(5);
		$this->SetX(173.5);
		$this->Ln(1);
		$this->SetFont('Arial','',9);
		//$this->Ln(7);
		$this->SetFont('Arial','b',12);
		//$this->Cell(0,6,utf8_decode('CERTIFICADO MÉDICO - DS - N° 166 - 2005 - EF'),0,0,'C');
		$this->SetFont('Arial','b',13);
		$this->Ln(1.5);
	}
	
var $ProcessingTable=false;
var $aCols=array();
var $TableX;
var $HeaderColor;
var $RowColors;
var $ColorIndex;

var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';


	
function WriteHTML($html)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0,5,$e,0,1,'C');
                else
                    $this->Write(5,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

function OpenTag($tag,$prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }



function TableHeader()
{
	$this->SetFont('Arial','B',12);
	$this->SetX($this->TableX);
	$fill=!empty($this->HeaderColor);
	if($fill)
		$this->SetFillColor($this->HeaderColor[0],$this->HeaderColor[1],$this->HeaderColor[2]);
	foreach($this->aCols as $col)
		$this->Cell($col['w'],6,$col['c'],1,0,'C',$fill);
	$this->Ln();
}

function Row($data)
{
	$this->SetX($this->TableX);
	$ci=$this->ColorIndex;
	$fill=!empty($this->RowColors[$ci]);
	if($fill)
		$this->SetFillColor($this->RowColors[$ci][0],$this->RowColors[$ci][1],$this->RowColors[$ci][2]);
	foreach($this->aCols as $col)
		$this->Cell($col['w'],5,$data[$col['f']],1,0,$col['a'],$fill);
	$this->Ln();
	$this->ColorIndex=1-$ci;
}

function CalcWidths($width,$align)
{
	//Compute the widths of the columns
	$TableWidth=0;
	foreach($this->aCols as $i=>$col)
	{
		$w=$col['w'];
		if($w==-1)
			$w=$width/count($this->aCols);
		elseif(substr($w,-1)=='%')
			$w=$w/100*$width;
		$this->aCols[$i]['w']=$w;
		$TableWidth+=$w;
	}
	//Compute the abscissa of the table
	if($align=='C')
		$this->TableX=max(($this->w-$TableWidth)/2,0);
	elseif($align=='R')
		$this->TableX=max($this->w-$this->rMargin-$TableWidth,0);
	else
		$this->TableX=$this->lMargin;
}

function AddCol($field=-1,$width=-1,$caption='',$align='L')
{
	//Add a column to the table
	if($field==-1)
		$field=count($this->aCols);
	$this->aCols[]=array('f'=>$field,'c'=>$caption,'w'=>$width,'a'=>$align);
}

function Table($query,$prop=array())
{
	//Issue query
	$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
	//Add all columns if none was specified
	if(count($this->aCols)==0)
	{
		$nb=mysql_num_fields($res);
		for($i=0;$i<$nb;$i++)
			$this->AddCol();
	}
	//Retrieve column names when not specified
	foreach($this->aCols as $i=>$col)
	{
		if($col['c']=='')
		{
			if(is_string($col['f']))
				$this->aCols[$i]['c']=ucfirst($col['f']);
			else
				$this->aCols[$i]['c']=ucfirst(mysql_field_name($res,$col['f']));
		}
	}
	//Handle properties
	if(!isset($prop['width']))
		$prop['width']=0;
	if($prop['width']==0)
		$prop['width']=$this->w-$this->lMargin-$this->rMargin;
	if(!isset($prop['align']))
		$prop['align']='C';
	if(!isset($prop['padding']))
		$prop['padding']=$this->cMargin;
	$cMargin=$this->cMargin;
	$this->cMargin=$prop['padding'];
	if(!isset($prop['HeaderColor']))
		$prop['HeaderColor']=array();
	$this->HeaderColor=$prop['HeaderColor'];
	if(!isset($prop['color1']))
		$prop['color1']=array();
	if(!isset($prop['color2']))
		$prop['color2']=array();
	$this->RowColors=array($prop['color1'],$prop['color2']);
	//Compute column widths
	$this->CalcWidths($prop['width'],$prop['align']);
	//Print header
	$this->TableHeader();
	//Print rows
	$this->SetFont('Arial','',11);
	$this->ColorIndex=0;
	$this->ProcessingTable=true;
	while($row=mysql_fetch_array($res))
		$this->Row($row);
	$this->ProcessingTable=false;
	$this->cMargin=$cMargin;
	$this->aCols=array();
}
function Code39($x, $y, $code, $ext = true, $cks = false, $w = 0.4, $h = 12, $wide = true) {

    //Display code
    $this->SetFont('Arial', '', 10);
    $this->Text($x, $y+$h+4, $code);

    if($ext)
    {
        //Extended encoding
        $code = $this->encode_code39_ext($code);

    }
    else
    {
        //Convert to upper case
        $code = strtoupper($code);
        //Check validity
        if(!preg_match('|^[0-9A-Z. $/+%-]*$|', $code))
            $this->Error('Invalid barcode value: '.$code);
    }

    //Compute checksum
    if ($cks)
        $code .= $this->checksum_code39($code);

    //Add start and stop characters
    $code = '*'.$code.'*';

    //Conversion tables
    $narrow_encoding = array (
        '0' => '101001101101', '1' => '110100101011', '2' => '101100101011', 
        '3' => '110110010101', '4' => '101001101011', '5' => '110100110101', 
        '6' => '101100110101', '7' => '101001011011', '8' => '110100101101', 
        '9' => '101100101101', 'A' => '110101001011', 'B' => '101101001011', 
        'C' => '110110100101', 'D' => '101011001011', 'E' => '110101100101', 
        'F' => '101101100101', 'G' => '101010011011', 'H' => '110101001101', 
        'I' => '101101001101', 'J' => '101011001101', 'K' => '110101010011', 
        'L' => '101101010011', 'M' => '110110101001', 'N' => '101011010011', 
        'O' => '110101101001', 'P' => '101101101001', 'Q' => '101010110011', 
        'R' => '110101011001', 'S' => '101101011001', 'T' => '101011011001', 
        'U' => '110010101011', 'V' => '100110101011', 'W' => '110011010101', 
        'X' => '100101101011', 'Y' => '110010110101', 'Z' => '100110110101', 
        '-' => '100101011011', '.' => '110010101101', ' ' => '100110101101', 
        '*' => '100101101101', '$' => '100100100101', '/' => '100100101001', 
        '+' => '100101001001', '%' => '101001001001' );

    $wide_encoding = array (
        '0' => '101000111011101', '1' => '111010001010111', '2' => '101110001010111', 
        '3' => '111011100010101', '4' => '101000111010111', '5' => '111010001110101', 
        '6' => '101110001110101', '7' => '101000101110111', '8' => '111010001011101', 
        '9' => '101110001011101', 'A' => '111010100010111', 'B' => '101110100010111', 
        'C' => '111011101000101', 'D' => '101011100010111', 'E' => '111010111000101', 
        'F' => '101110111000101', 'G' => '101010001110111', 'H' => '111010100011101', 
        'I' => '101110100011101', 'J' => '101011100011101', 'K' => '111010101000111', 
        'L' => '101110101000111', 'M' => '111011101010001', 'N' => '101011101000111', 
        'O' => '111010111010001', 'P' => '101110111010001', 'Q' => '101010111000111', 
        'R' => '111010101110001', 'S' => '101110101110001', 'T' => '101011101110001', 
        'U' => '111000101010111', 'V' => '100011101010111', 'W' => '111000111010101', 
        'X' => '100010111010111', 'Y' => '111000101110101', 'Z' => '100011101110101', 
        '-' => '100010101110111', '.' => '111000101011101', ' ' => '100011101011101', 
        '*' => '100010111011101', '$' => '100010001000101', '/' => '100010001010001', 
        '+' => '100010100010001', '%' => '101000100010001');

    $encoding = $wide ? $wide_encoding : $narrow_encoding;

    //Inter-character spacing
    $gap = ($w > 0.29) ? '00' : '0';

    //Convert to bars
    $encode = '';
    for ($i = 0; $i< strlen($code); $i++)
        $encode .= $encoding[$code{$i}].$gap;

    //Draw bars
    $this->draw_code39($encode, $x, $y, $w, $h);
}

function checksum_code39($code) {

    //Compute the modulo 43 checksum

    $chars = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
                            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 
                            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 
                            'W', 'X', 'Y', 'Z', '-', '.', ' ', '$', '/', '+', '%');
    $sum = 0;
    for ($i=0 ; $i<strlen($code); $i++) {
        $a = array_keys($chars, $code{$i});
        $sum += $a[0];
    }
    $r = $sum % 43;
    return $chars[$r];
}

function encode_code39_ext($code) {

    //Encode characters in extended mode

    $encode = array(
        chr(0) => '%U', chr(1) => '$A', chr(2) => '$B', chr(3) => '$C', 
        chr(4) => '$D', chr(5) => '$E', chr(6) => '$F', chr(7) => '$G', 
        chr(8) => '$H', chr(9) => '$I', chr(10) => '$J', chr(11) => '£K', 
        chr(12) => '$L', chr(13) => '$M', chr(14) => '$N', chr(15) => '$O', 
        chr(16) => '$P', chr(17) => '$Q', chr(18) => '$R', chr(19) => '$S', 
        chr(20) => '$T', chr(21) => '$U', chr(22) => '$V', chr(23) => '$W', 
        chr(24) => '$X', chr(25) => '$Y', chr(26) => '$Z', chr(27) => '%A', 
        chr(28) => '%B', chr(29) => '%C', chr(30) => '%D', chr(31) => '%E', 
        chr(32) => ' ', chr(33) => '/A', chr(34) => '/B', chr(35) => '/C', 
        chr(36) => '/D', chr(37) => '/E', chr(38) => '/F', chr(39) => '/G', 
        chr(40) => '/H', chr(41) => '/I', chr(42) => '/J', chr(43) => '/K', 
        chr(44) => '/L', chr(45) => '-', chr(46) => '.', chr(47) => '/O', 
        chr(48) => '0', chr(49) => '1', chr(50) => '2', chr(51) => '3', 
        chr(52) => '4', chr(53) => '5', chr(54) => '6', chr(55) => '7', 
        chr(56) => '8', chr(57) => '9', chr(58) => '/Z', chr(59) => '%F', 
        chr(60) => '%G', chr(61) => '%H', chr(62) => '%I', chr(63) => '%J', 
        chr(64) => '%V', chr(65) => 'A', chr(66) => 'B', chr(67) => 'C', 
        chr(68) => 'D', chr(69) => 'E', chr(70) => 'F', chr(71) => 'G', 
        chr(72) => 'H', chr(73) => 'I', chr(74) => 'J', chr(75) => 'K', 
        chr(76) => 'L', chr(77) => 'M', chr(78) => 'N', chr(79) => 'O', 
        chr(80) => 'P', chr(81) => 'Q', chr(82) => 'R', chr(83) => 'S', 
        chr(84) => 'T', chr(85) => 'U', chr(86) => 'V', chr(87) => 'W', 
        chr(88) => 'X', chr(89) => 'Y', chr(90) => 'Z', chr(91) => '%K', 
        chr(92) => '%L', chr(93) => '%M', chr(94) => '%N', chr(95) => '%O', 
        chr(96) => '%W', chr(97) => '+A', chr(98) => '+B', chr(99) => '+C', 
        chr(100) => '+D', chr(101) => '+E', chr(102) => '+F', chr(103) => '+G', 
        chr(104) => '+H', chr(105) => '+I', chr(106) => '+J', chr(107) => '+K', 
        chr(108) => '+L', chr(109) => '+M', chr(110) => '+N', chr(111) => '+O', 
        chr(112) => '+P', chr(113) => '+Q', chr(114) => '+R', chr(115) => '+S', 
        chr(116) => '+T', chr(117) => '+U', chr(118) => '+V', chr(119) => '+W', 
        chr(120) => '+X', chr(121) => '+Y', chr(122) => '+Z', chr(123) => '%P', 
        chr(124) => '%Q', chr(125) => '%R', chr(126) => '%S', chr(127) => '%T');

    $code_ext = '';
    for ($i = 0 ; $i<strlen($code); $i++) {
        if (ord($code{$i}) > 127)
            $this->Error('Invalid character: '.$code{$i});
        $code_ext .= $encode[$code{$i}];
    }
    return $code_ext;
}

function draw_code39($code, $x, $y, $w, $h){

    //Draw bars

    for($i=0; $i<strlen($code); $i++)
    {
        if($code{$i} == '1')
            $this->Rect($x+$i*$w, $y, $w, $h, 'F');
    }
}

	
function TextWithRotation($x, $y, $txt, $txt_angle, $font_angle=0)
    {
        $font_angle+=90+$txt_angle;
        $txt_angle*=M_PI/180;
        $font_angle*=M_PI/180;
    
        $txt_dx=cos($txt_angle);
        $txt_dy=sin($txt_angle);
        $font_dx=cos($font_angle);
        $font_dy=sin($font_angle);
    
        $s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',$txt_dx,$txt_dy,$font_dx,$font_dy,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
        if ($this->ColorFlag)
            $s='q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }
	
	function Footer(){	
		$this->Ln(2);	
		$this->SetLineWidth(0.1);
		 $this->Cell(17,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		 $this->Cell(15,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		 $this->Cell(15,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		
		$this->Ln(13);
		$this->Cell(17,5,'',0,0,'C');
		$this->Cell(44,5,'PRESIDENTE DEL CMCI',1,0,'C');
		 $this->Cell(15,5,'',0,0,'C');
		$this->Cell(44,5,'MIEMBRO DEL CMCI',1,0,'C');
		 $this->Cell(15,5,'',0,0,'C');
		$this->Cell(44,5,'MIEMBRO DEL CMCI',1,0,'C');	
		
		
			}
	
}


///*****////
			if($ImprimirCertificado!=NULL){
				$cantRegistros=count($ImprimirCertificado);
				for ($i=0; $i < count($ImprimirCertificado); $i++) {
				$IdAtencionCertMed=$ImprimirCertificado[$i]["IdAtencionCertMed"];
				$AnnioCertificado=$ImprimirCertificado[$i]["AnnioCertificado"];
				$NroCertificado=$ImprimirCertificado[$i]["NroCertificado"];
				
				$FechaEmision=$ImprimirCertificado[$i]["FechaEmision"];
				
				$feA=substr($FechaEmision,0,4);
				$feM=substr($FechaEmision,5,2);
				$feD=substr($FechaEmision,8,2);								
				
				
				$IdDistritoDomicilio=$ImprimirCertificado[$i]["IdDistritoDomicilio"];

	
				$EdadPaciente=CalcularE(substr($ImprimirCertificado[$i]["FechaNacimiento"],0,10));
				$IdPaciente=$ImprimirCertificado[$i]["IdPaciente"];
				$DireccionDomicilio=$ImprimirCertificado[$i]["DireccionDomicilio"];
				
				$NaturalezaIncapacidad=$ImprimirCertificado[$i]["NaturalezaIncapacidad"];
				
				if($NaturalezaIncapacidad=='1'){
					$optNIA='X';
					$optNIB='';
					$optNIC='';
				}else if($NaturalezaIncapacidad=='2'){
					$optNIB='X';
					$optNIC='';
					$optNIA='';
				}else if($NaturalezaIncapacidad=='3'){
					$optNIC='X';
					$optNIA='';
					$optNIB='';
				}
				
				$GradoIncapacidad=$ImprimirCertificado[$i]["GradoIncapacidad"];
				
				if($GradoIncapacidad=='1'){
					$optGIA='X';
					$optGIB='';
					$optGIC='';
				}else if($GradoIncapacidad=='2'){
					$optGIB='X';
					$optGIC='';
					$optGIA='';
				}else if($GradoIncapacidad=='3'){
					$optGIC='X';
					$optGIA='';
					$optGIB='';
					
				}else if($GradoIncapacidad=='0'){
					$optGIC='';
					$optGIA='';
					$optGIB='';
				}

				
				$MenoscaboCombinado=$ImprimirCertificado[$i]["MenoscaboCombinado"];
				$TipoActividad=$ImprimirCertificado[$i]["TipoActividad"];
				$PosibilidadReubicaLaboral=$ImprimirCertificado[$i]["PosibilidadReubicaLaboral"];
				$Edad=$ImprimirCertificado[$i]["Edad"];
				$MenoscaboGlobal=$ImprimirCertificado[$i]["MenoscaboGlobal"];
				
				$FechaInicioIncapacidad=$ImprimirCertificado[$i]["FechaInicioIncapacidad"];
				$SwPrecisable=$ImprimirCertificado[$i]["SwPrecisable"];
				if($SwPrecisable=='1'){
				$SwPrec='X';
				$fiA='';
				$fiM='';
				$fiD='';
				}else{
				$SwPrec='';

				
				$fiA=substr($FechaInicioIncapacidad,0,4);
				$fiM=substr($FechaInicioIncapacidad,5,2);
				$fiD=substr($FechaInicioIncapacidad,8,2);
								
				}
				
				
				
				$Observaciones=$ImprimirCertificado[$i]["Observaciones"];
				//$CodigoCMCI=$ImprimirCertificado[$i][""];
				$IdEmpleadoCrea=$ImprimirCertificado[$i]["IdEmpleadoCrea"];
				$FechaCrea=$ImprimirCertificado[$i]["FechaCrea"];
				
				$NroImpresion=$ImprimirCertificado[$i]["NroImpresion"];
				$ApellidoPaterno=$ImprimirCertificado[$i]["ApellidoPaterno"];
				$ApellidoMaterno=$ImprimirCertificado[$i]["ApellidoMaterno"];
				$PrimerNombre=$ImprimirCertificado[$i]["PrimerNombre"];
				$SegundoNombre=$ImprimirCertificado[$i]["SegundoNombre"];
				$FechaNacimiento=$ImprimirCertificado[$i]["FechaNacimiento"];
				$NroDocumento=$ImprimirCertificado[$i]["NroDocumento"];
				$DireccionDomicilio=$ImprimirCertificado[$i]["DireccionDomicilio"];
				
				$IdTipoSexo=$ImprimirCertificado[$i]["IdTipoSexo"];	
					if($IdTipoSexo=='1'){$sexo='MASCULINO';}else{$sexo='FEMENINO';}
				$dep=$ImprimirCertificado[$i]["dep"];
				$prov=$ImprimirCertificado[$i]["prov"];
				$dist=$ImprimirCertificado[$i]["dist"];	
				}
			}
			
			$resultados =SIGESA_CertificadoMedico_ListaMiembrosCMI_LCO(1,2017);
			if($resultados!=NULL){
				//for ($i=0; $i < count($resultados); $i++) {									
					$Presidente =$resultados[0]["CMCI"];
					$miembroA =$resultados[1]["CMCI"];
					$miembroB =$resultados[2]["CMCI"];
					
					
					
				//}
			}
///******///
	
    
// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
 /*
	$pdf->Image('img/hndac.jpg',40,8,18,20);
	$pdf->SetFont('Arial','B',10);
	$pdf->Ln(15);
	$pdf->Cell(190,10,utf8_decode('CERTIFICADO MÉDICO - DS - N° 166 - 2005 - EF'),0,0,'C');
	$pdf->Ln(7);
	$pdf->Cell(153,7,' ',0,0,'c');*/
	
	$pdf->SetFont('Arial','',9);
	$pdf->Cell(155,8,'',0,0,'L');
	$pdf->Cell(7,8,utf8_decode('Dia'),0,0,'C');
	$pdf->Cell(8,8,utf8_decode('Mes'),0,0,'C');
	$pdf->Cell(10,8,utf8_decode('Año'),0,0,'C');
	
	$pdf->SetFont('Arial','',9);
	$pdf->Ln(5);
	$pdf->SetLineWidth(0.5);
	
	$pdf->Cell(140,8,utf8_decode('    N° de Certficado Médico ').(str_pad($NroCertificado, 5, "0", STR_PAD_LEFT).' - '.$AnnioCertificado),'B',0,'L');
	$pdf->Cell(13,8,'Fecha','B',0,'L');
	$pdf->Cell(9,8,$feD,'B',0,'C');
	$pdf->Cell(9,8,$feM,'B',0,'C');
	$pdf->Cell(19,8,$feA,'B',0,'L');
	$pdf->Ln(6.8);
	$pdf->Cell(10,8,'I.- CENTRO ASISTENCIAL (Hospital/Institutos)',0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(190,5.5,utf8_decode('HOSPITAL NACIONAL "DANIEL ALCIDES CARRIÓN"'),'B',0,'C');
	$pdf->Ln(4.3);
	$pdf->Cell(10,8,'II.- DATOS PERSONALES DEL EVALUADO',0,0,'L');
	$pdf->Ln(7);
	$pdf->SetLineWidth(0.1);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(78,4,utf8_decode('Apellido Paterno'),'B',0,'L');
	 $pdf->Cell(3,4,'',0,0,'L');
	$pdf->Cell(45,4,utf8_decode('Apellido Materno'),'B',0,'L');
	 $pdf->Cell(3,4,'',0,0,'L');
	$pdf->Cell(49,4,utf8_decode('Nombres'),'B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(81,4, utf8_decode(mb_strtoupper($ApellidoPaterno)),0,0,'L'); //CAMPO APELLIDO PATERNO
	$pdf->Cell(48,4,utf8_decode(mb_strtoupper($ApellidoMaterno)),0,0,'L'); //CAMPO APELLIDO MATERNO
	$pdf->Cell(47,4,utf8_decode(mb_strtoupper($PrimerNombre)).' '.mb_strtoupper($SegundoNombre),0,0,'L'); // CAMPO NOMBRES
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,utf8_decode('N° de DNI'),'B',0,'L');
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(38,4,'Sexo','B',0,'L');
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(15,4,'Edad','B',0,'L');
	 $pdf->Cell(24,4,'',0,0,'L');
	$pdf->Cell(35,4,'Fecha de Nacimiento','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(54,4,$NroDocumento,0,0,'L'); //CAMPO DNI
	$pdf->Cell(53,4,$sexo,0,0,'L'); //CAMPO SEXO
	$pdf->Cell(39,4,$EdadPaciente,0,0,'L'); // CAMPO EDAD
	
	$fecha=vfecha(substr($FechaNacimiento,0,10));
	$FNd=substr($fecha,0,2);
	$FNm=substr($fecha,3,2);
	$FNa=substr($fecha,6,4);
	
	$pdf->Cell(47,4,$FNd.'  '.$FNm.'  '.$FNa,0,0,'L'); // CAMPO FECHA NACIMIENTO
	$pdf->Ln(6);
	$pdf->SetLineWidth(0.1);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(78,4,'Direccion Actual',0,0,'L');
	$pdf->Ln(4);
	 $pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(90,4,utf8_decode('Calle/ Jirón/ Avenida'),'B',0,'L');
	 $pdf->Cell(7,4,'',0,0,'L');
	$pdf->Cell(81,4,utf8_decode('Block/ Manzana/ Urbanización'),'B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(97,4,utf8_decode(mb_strtoupper($DireccionDomicilio)),0,0,'L'); //CAMPO DIRECCION
	$pdf->Cell(81,4,' ',0,0,'L'); //CAMPO BLOQUE MANZANA URBANIZACION
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(58,4,'Distrito','B',0,'L');
	 $pdf->Cell(2,4,'',0,0,'L');
	$pdf->Cell(50,4,'Provincia','B',0,'L');
	 $pdf->Cell(2,4,'',0,0,'L');
	$pdf->Cell(66,4,'Departamento','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(60,4,utf8_decode(mb_strtoupper($dist)) ,0,0,'L'); //CAMPO DISTRITO
	$pdf->Cell(52,4,utf8_decode(mb_strtoupper($prov)),0,0,'L'); //CAMPO PROVINCIA
	$pdf->Cell(61,4,utf8_decode(mb_strtoupper($dep)),0,0,'L'); // CAMPO DEPARTAMENTO
	$pdf->Ln(6);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(190,5,utf8_decode('III.- La Comisión Médica Calificadora de la Incapacidad - CMCI, de acuerdo a sus facultades certifica lo siguiente'),'T',0,'L');
//	$filas=count($ImprimirCertificado);
	if(count($ImprimirCertificado)=='5'){$filas=count($ImprimirCertificado);}else{$filas=5;}
	
	$pdf->SetLineWidth(0.1);
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,4,'a.- Diagnostico','B',0,'L');
	$pdf->Cell(12,4,'CIE - 10','B',0,'R');
	$pdf->Ln(5-$filas);
	if($ImprimirCertificado!=NULL){
				for ($i=0; $i < count($ImprimirCertificado); $i++) {	
					if($ImprimirCertificado[$i]["Descripcion"]!=''){
						$descripcion=$ImprimirCertificado[$i]["Descripcion"];
						$codigo=$ImprimirCertificado[$i]["CodigoCIE10"];
					}
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,20,(utf8_decode(mb_strtoupper($descripcion))),0,0,'L'); //CAMPO DIAGNOSTICO
	$pdf->Cell(12,20,mb_strtoupper($codigo),0,0,'L'); //CAMPO CIE
	$pdf->Ln(4);
				}
			}
	for ($j=$cantRegistros; $j < 8; $j++) {
		$pdf->Cell(166,20,' ',0,0,'L'); //CAMPO DIAGNOSTICO
		$pdf->Cell(12,20,'',0,0,'L'); //CAMPO CIE
		$pdf->Ln(4);
	}
	
	
	
	
	$pdf->Ln(15-$filas);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(178,6,'b.- Caracteristica de la incapacidad','T',0,'L');
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,'Naturaleza de la Incapacidad',0,0,'L');
		$pdf->Ln(5);	
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(28,5,'Temporal',1,0,'L');
	 $pdf->Cell(12,5,$optNIA,1,0,'C');
	  $pdf->Cell(26,5,'',0,0,'L');
	$pdf->Cell(28,5,'Permanente',1,0,'L');
	 $pdf->Cell(12,5,$optNIB,1,0,'C');
	  $pdf->Cell(21,5,'',0,0,'L');
	$pdf->Cell(30,5,'No Incapacidad',1,0,'L');
	$pdf->Cell(12,5,$optNIC,1,0,'C');
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,'Grado de la Incapacidad',0,0,'L');
		$pdf->Ln(5);	
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(28,5,'Parcial',1,0,'L');
	 $pdf->Cell(12,5,$optGIA,1,0,'C');
	  $pdf->Cell(26,5,'',0,0,'L');
	$pdf->Cell(28,5,'Total',1,0,'L');
	 $pdf->Cell(12,5,$optGIB,1,0,'C');
	  $pdf->Cell(21,5,'',0,0,'L');
	$pdf->Cell(30,5,'Gran Incapacidad',1,0,'L');
	$pdf->Cell(12,5,$optGIC,1,0,'C');
	$pdf->Ln(7);
	
	/***/
	
	
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,4,'c.- Menoscabo',0,0,'L');
	$pdf->Ln(3.5);
	$pdf->Cell(130,5,'',0,0,'L');
	$pdf->Cell(20,5,'Porcentaje',1,0,'L');
	$pdf->Ln(5);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->Cell(38,5,'','L'.'T',0,'L');
	$pdf->Cell(55,5,'Menoscabo Combinado','T',0,'C');
	$pdf->Cell(20,5.4,$MenoscaboCombinado,'B'.'R',0,'C'); // PORCENTAJE MENOSCABO COMBINADO
		$pdf->Ln(5.4);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->MultiCell(35,7.05,'Factores Complementarios',1,'C');
		$pdf->SetXY(82,204);
		$pdf->Cell(62,14.1,'','L'.'T'.'B',0,'L');
	$pdf->SetXY(82,204);
	$pdf->Cell(62,4,'Tipos de Actividad',0,0,'L');
	$pdf->SetXY(149,209);
	$pdf->Cell(62,4,$TipoActividad,0,0,'L'); // CAMPO PORCENTAJE TIPO ACTIVIDAD
	$pdf->SetXY(82,209);
	$pdf->Cell(62,4,utf8_decode('Posibilidad de reubicación laboral'),0,0,'L');
	$pdf->SetXY(149,205);
	$pdf->Cell(62,4,$PosibilidadReubicaLaboral,0,0,'L'); // CAMPO PORCENTAJE REUBICACION
	$pdf->SetXY(82,213.5);
	$pdf->Cell(62,4,'Edad',0,0,'L');
	$pdf->SetXY(149,213.5);
	$pdf->Cell(62,4,$Edad,0,0,'L'); // CAMPO PORCENTAJE EDAD	
		$pdf->SetXY(140,204);
		$pdf->Cell(20,14.1,'','R'.'B',0,'L');
	$pdf->Ln(14.1);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->Cell(38,5,'','B'.'L'.'T',0,'L');
	$pdf->Cell(55,5,'MENOSCABO GLOBAL','B'.'T',0,'C'); 
	$pdf->Cell(20,5,$MenoscaboGlobal,'B'.'T'.'R',0,'C'); // CAMPO MOENOSCABO GLOBAL PORCENTAJE TOTAL
	
	/****/
	
	
	
	
	

	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,4,'d-. Fecha de inicio de Incapacidad',0,0,'L');
	$pdf->Ln(4);
	$pdf->Cell(63,5,'',0,0,'L');
	$pdf->Cell(10,5,'Dia',0,0,'C');
	$pdf->Cell(10,5,'Mes',0,0,'C');
	$pdf->Cell(10,5,utf8_decode('Año'),0,0,'C');
	$pdf->Cell(35,5,'No es precisable',0,0,'C');
	
	$pdf->Ln(5);
	$pdf->Cell(63,5,'',0,0,'C'); 
	$pdf->Cell(10,5,$fiD,0,0,'C'); // CAMPO DIA INCAPACIDAD
	$pdf->Cell(10,5,$fiM,0,0,'C'); // CAMPO MES INCAPACIDAD
	$pdf->Cell(10,5,$fiA,0,0,'C'); //  CAMPO AÑO INCAPACIDAD
	$pdf->Cell(4,5,'',0,0,'C');
	$pdf->Cell(30,5,$SwPrec,1,0,'C'); // CAMPO IMPRECISABLE
	
	$pdf->Ln(8);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,9,'IV.- OBSERVACIONES','T',0,'L');
	$pdf->Ln(7);
	$pdf->Cell(7,4,'',0,0,'L');
	//$pdf->Cell(166,23,utf8_decode(mb_strtoupper($Observaciones)),0,0,'L');	
	//
	//$pdf->WriteHTML(utf8_decode($Observaciones));
	
	
	
	
	$pdf->MultiCell(186,4.05,utf8_decode($Observaciones),0,'L');
	$pdf->Ln(7-$filas);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,9,'V.- FIRMA Y SELLO','T',0,'L');
	$pdf->Ln(7);

  $fontSize = 10;
  $marge    = 10;   // POSICIONAMIENTO DE BARRA
  $x        = 8;  
  $y        = 21;  
  $height   = 10;   
  $width    = 0.4;    
  $angle    = 90;   
  
  $code     = '123456789012'; // CODIGO DE BARRAS!!
  $type     = 'ean13';
  $black    = '000000'; // COLOR DE BARRA

//$data = Barcode::fpdf($pdf, $black, $x, $y, $angle, $type, array('code'=>$code), $width, $height);
//$len = $pdf->GetStringWidth($data['hri']);$len = $pdf->GetStringWidth($data['hri']);
// Barcode::rotate(-$len / 2, ($data['height'] / 2) + $fontSize + $marge, $angle, $xt, $yt);
//$pdf->TextWithRotation($x + 8, $y + 13, $data['hri'], $angle);

$pdf->Output();

?>
