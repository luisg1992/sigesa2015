<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registro Certificado Medico </title>
<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
       
</head>


 <script>
function CambiaDepartamento(){
	
		var idDepartamento = $('#NomDep').combobox('getValue');
		
			$('#NomProv').combogrid({				
				panelWidth:250,
				value:'',
				url: '../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=MostrarProvinciasCombo&idDepartamento='+idDepartamento,
				idField:'IdProvincia', //ID QUE SE RECUPERA
				textField:'NomProvincia',
				mode:'remote',
				fitColumns:true,
				onSelect: function(rec){
        		var url = CambiaProvincia(); },
				columns:[[
					//{field:'IdProvincia',title:'Codigo',width:60},//VISTA
					{field:'NomProvincia',title:'Nombre',width:80}					
					
				]]
			});
	//limpiar combogrid distrito
	$('#NomProv').combogrid('setValue', '');	
	$('#NomDist').combogrid('setValue', '');	
	
	}

function CambiaProvincia(){	
	
	var idProvincias = $('#NomProv').combogrid('getValue');
		
			$('#NomDist').combogrid({
				panelWidth:250,
				value:'',
				url: '../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=MostrarDistritosCombo&idProvincias='+idProvincias,
				idField:'IdDistrito',
				textField:'NomDistrito',
				mode:'remote',
				fitColumns:true,
				/*onSelect: function(rec){
        		var url = cambiadistri(); },*/
				
				columns:[[
					//{field:'IdDistrito',title:'Id',width:60},
					{field:'NomDistrito',title:'Nombre',width:80}					
					
				]]
			});				
	
  } 
  
  $.extend( $( "#FechaIniIncapacidad" ).datebox.defaults,{
		formatter:function(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
		},
		parser:function(s){
			if (!s) return new Date();
			var ss = s.split('/');
			var d = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var y = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
	});	
// FIN (A2)	



/*function buscad(){*/

//}*/
					 
 </script>
<!--autocomplete producto-->
<script type="text/javascript">
</script>

<body>


<div id="cc" class="easyui-layout" style="width:100%;height:100%;">
    <div data-options="region:'north',split:true" style="height:100px;">    

        <!-- Menu-->
        <div class="easyui-panel" style="padding:5px;">
     
            <a href="#" onclick="GrabarDatos()" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" >Grabar</a>
            
            <a href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Certificados&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true">Cancelar</a>
           <!-- <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true">Imprimir</a>-->
		  <a href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Certificados&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>" class="easyui-linkbutton" data-options="iconCls:'icon-back',plain:true">Salir</a>
      </div>
      <!-- Fin Menu-->
        <div class="easyui-panel" style="padding:5px;">
            <input class="easyui-textbox" style="width:100px;height:27px" maxlength="8" data-options="prompt:'N°DNI'" id="DniPaciente" name="DniPaciente" 
           >
            <input class="easyui-numberbox" data-options="prompt:'N°Historia'" style="width:120px;height:27px" id="HcPaciente" name="HcPaciente">
          <strong style="color:#039"> <select id="nombre" class="easyui-combogrid" name="nombre" style="width:250px;height:30px;"
                                            data-options="
                                            prompt:'Bus. Paciente.',
                                            panelWidth:500,
                                            //readonly:'true',
                                            url: '../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=BuscarPacientes',                                                           
                                            type: 'POST',
                                            dataType: 'json',                                                            
                                            idField:'IdPaciente',
                                            textField:'Paciente',
                                            mode:'remote',
                                            columns:[[
                                            {field:'IdPaciente',title:'N°Historia',width:70},
                                            {field:'Paciente',title:'Paciente',width:350}                                          
                                            ]],
                                            onSelect:function(index,row){              
                                            
                                         
                                           $('#HcPaciente').textbox('textbox').focus();
                                           $('#HcPaciente').textbox('setText',row.IdPaciente);
                                           
                                             var tb = $('#nombre').combogrid('textbox');
                                            tb.bind('keydown',function(event){
                                            var keycode = (event.keyCode ? event.keyCode : event.which);
                                            if(keycode == '13'){                              
											
          
                                            }              
                                            });                 

                                            },
                                            fitColumns:true

                                            "></select> &nbsp;&nbsp; Presione la tecla enter para buscar</strong>
          
      </div>
  </div>
  <div data-options="region:'south',title:'Registro Certificado Medico'" style="height:90%;" class="easyui-tabs">
        <div title="Datos del Paciente" style="padding:10px">
          <table width="946" border="0" cellpadding="0" cellspacing="0" style="font-size:12px">
            <tr>
              <td align="right">Nro Certificado&nbsp;</td>
              <td><input class="easyui-textbox" id="NroCertificado" name="NroCertificado" style="width:80%;height:25px;" readonly prompt="Autogenerado"></td>
              <td align="right">Fecha Emision&nbsp;</td>
              <td><input class="easyui-textbox" id="FechaEmision" name="FechaEmision" style="width:80%;height:25px;" readonly  value="<?php echo date("d/m/Y") ?>"></td>
              <td colspan="2" rowspan="2" align="right"><strong style="color:#F00">Nota: en caso falte otros datos del paciente comunicarse con el area correspondiente para la actualizacion.</strong></td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="144" align="right">Apellido Paterno&nbsp;</td>
              <td width="149"><input class="easyui-textbox" id="ApellidoPaterno" name="ApellidoPaterno" style="width:100%;height:25px;" readonly></td>
              <td width="104" align="right">Apellido Materno&nbsp;</td>
              <td width="168"><input class="easyui-textbox" id="ApellidoMaterno" name="ApellidoMaterno" style="width:100%;height:25px;" readonly></td>
              <td width="80" align="right">Nombres&nbsp;</td>
              <td width="301"><input class="easyui-textbox" id="Nombres" name="Nombres" style="width:100%;height:25px;" readonly></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">Nº DNI&nbsp;</td>
              <td><input class="easyui-textbox" id="NroDocumento" name="NroDocumento" style="width:50%;height:25px;" /></td>
              <td align="right">Sexo</td>
              <td><input class="easyui-textbox" id="IdTipoSexo" name="IdTipoSexo" style="width:50%;height:25px;" /></td>
              <td align="right">Edad&nbsp;</td>
              <td><input class="easyui-textbox" id="edadpaciente"  name="edadpaciente" style="width:20%;height:25px;" />
              Fec.Nac&nbsp;                <input class="easyui-textbox" id="FechaNacimiento" style="width:40%;height:25px;" readonly></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">Departamento&nbsp;</td>
              <td>
               <!--//var url = Cambiapais();
                 $idDep="<script> document.write(IdDepartamento) </script>";
               --> 
             <!-- <input class="easyui-textbox" id="NomDep" style="width:100%;height:25px;" readonly />-->
              <?php 
			  $idDep="<script> document.write(IdDepartamento) </script>";
			  $sexo="<script> document.write(NomDistrito) </script>";
			  $edad="<script> document.write(edadpac) </script>";
			
			  $Departamento = DepartamentosSeleccionarTodos(); ?>
              <select name="NomDep" id="NomDep" class="easyui-combobox" style="width:150px;" data-options="
        valueField: 'id',
        textField: 'text',        
        onSelect: function(rec){
       var url = CambiaDepartamento();
        }" >
                
                <?php foreach($Departamento as $DepItem){?>
                <option value="<?php echo $DepItem["IdDepartamento"]?>" <?php if($DepItem["IdDepartamento"]==$idDep){?> selected <?php } ?> ><?php echo utf8_encode(mb_strtoupper($DepItem["Nombre"]))?></option>
                <?php }	?>
            	</select>
              
              </td>
              <td align="right">Provincia&nbsp;</td>
              <td>
             <!-- <input class="easyui-textbox" id="NomProv" style="width:100%;height:25px;" readonly />-->
             <?php $NomProvincia="<script> document.write(NomProvincia) </script>";
			 	   $IdProvincia="<script> document.write(IdProvincia) </script>"
					
					
					?>
             <select id="NomProv" class="easyui-combogrid" name="NomProv" style="width:150px;"
            data-options="
                panelWidth:450,              
                value:'<?php echo $NomProvincia; ?>',
                idField:'<?php echo $IdProvincia; ?>',
                textField:'<?php echo $NomProvincia; ?>',                
                columns:[[
					
					{field:'<?php echo $NomProvincia; ?>',title:'Nombre',width:80}					
					
				]]
            ">
            </select>
              </td>
              <td align="right">Distrito&nbsp;</td>
              <td>
              
            <!--  <input class="easyui-textbox" id="NomDist" style="width:100%;height:25px;" readonly />-->
               <?php $NomDistrito="<script> document.write(NomDistrito) </script>";
			 	   $IdDistrito="<script> document.write(IdDistrito) </script>";
					
					
					?>
             <select id="NomDist" class="easyui-combogrid" name="NomDist" style="width:150px;"
            data-options="
                panelWidth:450,              
                value:'<?php echo $NomDistrito; ?>',
                idField:'<?php echo $IdDistrito; ?>',
                textField:'<?php echo $NomDistrito; ?>',                
                columns:[[
					
					{field:'<?php echo $NomDistrito; ?>',title:'Nombre',width:80}					
					
				]]
            ">
            </select>
              
              
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">Direccion&nbsp;</td>
              <td colspan="5"><input name="DireccionDomicilio" class="easyui-textbox" id="DireccionDomicilio" style="width:75%;height:25px;"   /></td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td colspan="5">&nbsp;
              
              <input  type="hidden" name="IdDistritoDomicilio" id="IdDistritoDomicilio" />
              
              <input    type="hidden" id="IdEmpleado" name="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado']; ?>" />
              
              <input type="hidden" name="IdPaciente" id="IdPaciente" />
              
             
             </td>
            </tr>
          </table>
      </div>
        <div title="Diagnosticos" style="padding:10px">
        <table width="945" border="0" style="font-size:12px">
  <tr>
    <td>Busqueda:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    
    <td width="324">
    <!--<input  id="cccDescripcion" name="ccDescripcion"  style="width:450px;height:30px;"  
    />-->
    
    <select id="Descripcion" class="easyui-combogrid" name="Descripcion" style="width:250px;height:30px;"
                                            data-options="
                                            prompt:'Bus. Diagnostico.',
                                            panelWidth:500,
                                            //readonly:'true',
                                            url: '../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=BusquedaDiagnosticosx',                                                           
                                            type: 'POST',
                                            dataType: 'json',                                                            
                                            idField:'CodigoCIE10',
                                            textField:'Descripcion',
                                            mode:'remote',
                                            columns:[[
                                            {field:'CodigoCIE10',title:'Codigo CIE10',width:70},
                                            {field:'Descripcion',title:'Descripcion',width:350}                                          
                                            ]],
                                            onChange:function(index,row){              
                                            
                                            $('#CodigoCIE10').val(row.CodigoCIE10);
                                            $('#Descripcion').val(row.Descripcion);                
												
                                               // $('#agregar').linkbutton('textbox').focus();
                                              //  $('#agregar').linkbutton('select').focus();
                                                
                                             var tb = $('#Descripcion').combogrid('textbox');
                                            tb.bind('keydown',function(event){
                                            var keycode = (event.keyCode ? event.keyCode : event.which);
                                            if(keycode == '13'){                              
											
          
                                            }              
                                            });                 

                                            },
                                            fitColumns:true

                                            "></select>
  
       
      
      
      </td>
      <td width="167"><!--<input type="text" name="CodigoCIE10" id="CodigoCIE10"  class="easyui-searchbox" 
     style="width:100%;height:25px;"/>--></td>
    <td width="440">
    
    <a href="#" class="easyui-linkbutton" 
    data-options="iconCls:'icon-add'"  id="agregar" onclick="agregar()" >Agregar</a>&nbsp; 
    </td>
  </tr>
  
  <tr>
    <td colspan="3">
    
    
    
    
    
    </td>
    </tr>
     <table class="easyui-datagrid" style="width:50%;height:70%"
                            id="tblDiagnosticos"  >  
                            <thead>
                                <tr>
                                	
                                    <th data-options="field:'x1CodigoCIE10',width:100"></th>
                                    <th data-options="field:'x1Descripcion',width:450">Diagnóstico</th>
                                     <th data-options="field:'action',width:100,align:'right'">Accion</th>
                                </tr>
                            </thead>  
                        </table>
</table>

        
        
      </div>
        <div title="Caracteristicas" style="padding:10px">
          <table width="947" border="0" cellpadding="0" cellspacing="0" style="font-size:12px">
            <tr>
              <td colspan="3"><strong>Caracteristicas de la incapacidad</strong></td>
            </tr>
            <tr>
              <td colspan="3">Naturaleza de la Incapacidad</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="190">
              <label>
              <input type="radio" name="optNI" id="optNI" value="1"  onclick="HabilitarGradoActividad()"/>
              Temporal
              </label></td>
              <td width="190"><label>
              <input type="radio" name="optNI" id="optNI" value="2"  onclick="HabilitarGradoActividad()"/>
              Permanente
              </label></td>
              <td width="567"><label>
              <input type="radio" name="optNI" id="optNI" value="3"  onclick="HabilitarGradoActividad()"/>
              No Incapacidad
              </label></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Grado de la Incapacidad</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><label>
              <input type="radio" name="optGI" id="optGIA" value="1" />
              Parcial
              </label></td>
              <td><label>
              <input type="radio" name="optGI" id="optGIB" value="2" />
              Total
              </label></td>
              <td><label>
              <input type="radio" name="optGI" id="optGIC" value="3" />
              Gran Incapacidad
              </label></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Menoscabo</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3"><table width="200" border="1" cellpadding="1" cellspacing="1">
                <tr>
                    <td><table width="795" border="0" cellpadding="0" cellspacing="0" style="font-size:12px">
                      <tr>
                        <td width="80">&nbsp;</td>
                        <td width="151">&nbsp;</td>
                        <td width="364">&nbsp;</td>
                        <td width="200">Porcentanje</td>
                      </tr>
                      <tr>
                        <td colspan="3" align="right">Menoscabo Combinado&nbsp;</td>
                        <td><input name="valor_combinado" type="text" class="easyui-numberspinner" id="valor_combinado" style="width:100%;height:25px;" value="0"    data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="center">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="2" rowspan="5" align="center">FACTORES COMPLEMENTARIOS</td>
                        <td align="right">Tipos de Actividad&nbsp;</td>
                        <td><input name="valor_actividad" type="text" class="easyui-numberspinner" id="valor_actividad" style="width:100%;height:25px;" value="0" data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="right">Posibilidad de reubicacion laboral&nbsp;</td>
                        <td><input name="valor_posibilidad" type="text" class="easyui-numberspinner" id="valor_posibilidad"  style="width:100%;height:25px;" value="0" data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="right">Edad&nbsp;</td>
                        <td><input name="valor_edad" type="text" class="easyui-numberspinner" id="valor_edad"  style="width:100%;height:25px;" value="0" data-options="min:0,max:100,onChange: function(value){SumarMenoscabo()}"/></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="right">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="3" align="right">Menoscabo Global&nbsp;</td>
                        <td><input name="valor_global" type="text" class="easyui-textbox" id="valor_global"  style="width:100%;height:25px;" value="" readonly="readonly"/></td>
                      </tr>
                    </table></td>
                </tr>
                </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td> <label for="chkprecisable"><input name="chkprecisable" type="checkbox" id="chkprecisable" onclick="HabilitaFechaPrecisable()" value="1" checked="checked" />
               
              No es Precisable</label></td>
              <td><input class="easyui-datebox" id="FechaIniIncapacidad" name="FechaIniIncapacidad" style="width:80%;height:25px;" >&nbsp;</td>
              <td>(fecha inicio incapacidad)</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>
        <div title="Observaciones" style="padding:10px">
        Observaciones
        <input class="easyui-textbox"  labelPosition="top" multiline="true" value="" style="width:40%;height:180px" id="observaciones" name="observaciones">
      </div>
       <!-- <div title="Miembros CMCI" style="padding:10px">
                 <table  class="easyui-datagrid" id="tblMiembros"  style="width:50%;height:400px" data-options="
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false">
                            <thead>
                                <tr>
                                    <th data-options="field:'x1Cargo',width:150">Cargo</th>
                                    <th data-options="field:'x1Nombre',width:300">Nombre</th>
                                    <th data-options="field:'x1Estado',width:80">Estado</th>
                                    <th data-options="field:'x1Vigencia',width:220">Vigencia</th>
                                </tr>
                            </thead>  
                        </table>
				</table>
        </div>-->
    </div>    
</div>

 <script type="text/javascript" src="../../MVC_Vista/AtencionesCertificados/FuncionesAC.js"></script>
<script>
	<?php /*?>	function getData(){
			var rows = [];
		<?php 
		$anio_actual=date('Y');
		$mes_actual=date('n'); //dial mes sin ceros
			$resultados =SIGESA_CertificadoMedico_ListaMiembrosCMI_LCO($mes_actual,$anio_actual);
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {									
					if( $resultados[$i]["Cargo"]=='1'){$x1Cargo ='PRESIDENTE CMCI';}else if( $resultados[$i]["Cargo"]=='2'){$x1Cargo ='MIEMBRO CMCI';};
					$x1Nombre = mb_strtoupper($resultados[$i]["CMCI"]);
					$x1Vigencia = 'Desde '.vfecha($resultados[$i]["FecInicioCMI"]).' hasta '.vfecha($resultados[$i]["FecFinCMI"]);	
					if($resultados[$i]["Estado"]=='1'){$x1Estado='ACTIVO';	};							
		?>			
					rows.push({					
					x1Cargo: 	'<?php echo $x1Cargo; ?>',
					x1Nombre: 	'<?php echo $x1Nombre; ?>',
					x1Estado: 	'<?php echo $x1Estado; ?>',
					x1Vigencia: '<?php echo $x1Vigencia; ?>',	
				});
			<?php  
		}
	}
?>	return rows;
		}		
		$(function(){			
			var dg =$('#tblMiembros').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			dg.datagrid('enableFilter');
		});	<?php */?>
	</script>
</body>
</html>