<?php 
ini_set('memory_limit', '1024M');

require('../../MVC_Modelo/bscM.php');
require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Modelo/ReportesM.php');
require('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

	$objPHPExcel = new PHPExcel();

	#ESTILOS
	$estiloCentrado = array(
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ));

    $estiloEncabezados = array(
        'fill' => array(
        	'type' => PHPExcel_Style_Fill::FILL_SOLID,
        	'color' => array('rgb' => '4682B4'),
            
    	),
        'font' => array(
	        'bold' => true,
	        'color' => array('rgb' => 'ffffff'),
	        'size' => 11,
	        'name' => 'Calibri',
    		)
    );



	// Establecer propiedades
	$objPHPExcel->getProperties()
			->setCreator("HNDAC / OESI / DESARROLLO DE SISTEMAS")
			->setLastModifiedBy("HNDAC / OESI / DESARROLLO DE SISTEMAS")
			->setTitle("Atenciones")
			->setSubject("Atenciones")
			->setDescription("Descripcion de data.")
			->setKeywords("Excel Office 2007 openxml php")
			->setCategory("Pruebas de Excel");


    // CABECERA
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'IDATENCION')
			->setCellValue('B1', 'PACIENTE')
			->setCellValue('C1', 'DNI')
			->setCellValue('D1', 'FNACIMIENTO')
			->setCellValue('E1', 'SEXO')
			->setCellValue('F1', 'DISTRITO')
			->setCellValue('G1', 'AÑOS')
			->setCellValue('H1', 'MESES')
			->setCellValue('I1', 'FECHAINGRESO')
			->setCellValue('J1', 'CIE10')
			->setCellValue('K1', 'DIAGNOSTICO');

	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($estiloEncabezados);


	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaIng"]);
	$FechaInicio = $anio."/".$mes."/".$dia;

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaFin"]);
	$FechaFinal = $anio."/".$mes."/".$dia;

	$EdadIni 		= $_REQUEST["EdadIni"];
	$EdadFin 		= $_REQUEST["EdadFin"];
	$Diagnostico 	= $_REQUEST["Diagnostico"];
	$tipoReporte	= $_REQUEST["tipoReporte"];


	switch($tipoReporte){
			case 'A':
				$dataFechaEdad = ReporteFebrilesPorFechaEdad($FechaInicio, $FechaFinal, $EdadIni, $EdadFin);

			$contador = count($dataFechaEdad);
			$j = 2;
				for ($i=0; $i < $contador; $i++){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $dataFechaEdad[$i]['IDATENCION']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $dataFechaEdad[$i]['PACIENTE']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $dataFechaEdad[$i]['DNI']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, substr($dataFechaEdad[$i]['FNACIMIENTO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $dataFechaEdad[$i]['SEXO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $dataFechaEdad[$i]['DISTRITO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $dataFechaEdad[$i]['EDAD']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $dataFechaEdad[$i]['MESES']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, substr($dataFechaEdad[$i]['FECHAINGRESO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $dataFechaEdad[$i]['CIE10']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $dataFechaEdad[$i]['DIAGNOSTICO']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($estiloCentrado);
					$j = $j + 1;
				}
			break;

			case 'B':
			$dataFecha = ReporteFebrilesPorFecha($FechaInicio, $FechaFinal);

			$contador = count($dataFecha);
			$j = 2;
				for ($i=0; $i < $contador; $i++){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $dataFecha[$i]['IDATENCION']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $dataFecha[$i]['PACIENTE']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $dataFecha[$i]['DNI']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, substr($dataFecha[$i]['FNACIMIENTO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $dataFecha[$i]['SEXO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $dataFecha[$i]['DISTRITO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $dataFecha[$i]['EDAD']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $dataFecha[$i]['MESES']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, substr($dataFecha[$i]['FECHAINGRESO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $dataFecha[$i]['CIE10']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $dataFecha[$i]['DIAGNOSTICO']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($estiloCentrado);
					$j = $j + 1;
				}		
			break;

			case 'C':
			$dataFechaCodigo = ReporteFebrilesPorFechaCodigo($Diagnostico, $FechaInicio, $FechaFinal);

			$contador = count($dataFechaCodigo);
			$j = 2;
				for ($i=0; $i < $contador; $i++){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $dataFechaCodigo[$i]['IDATENCION']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $dataFechaCodigo[$i]['PACIENTE']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $dataFechaCodigo[$i]['DNI']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, substr($dataFechaCodigo[$i]['FNACIMIENTO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $dataFechaCodigo[$i]['SEXO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $dataFechaCodigo[$i]['DISTRITO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $dataFechaCodigo[$i]['EDAD']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $dataFechaCodigo[$i]['MESES']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, substr($dataFechaCodigo[$i]['FECHAINGRESO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $dataFechaCodigo[$i]['CIE10']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $dataFechaCodigo[$i]['DIAGNOSTICO']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($estiloCentrado);
					$j = $j + 1;
				}
			break;

			case 'D':
			$dataFechaEdadCodigo = ReporteFebrilesPorFechaCodigoEdad($Diagnostico, $FechaInicio, $FechaFinal, $EdadIni, $EdadFin);

			$contador = count($dataFechaEdadCodigo);
			$j = 2;
				for ($i=0; $i < $contador; $i++){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $dataFechaEdadCodigo[$i]['IDATENCION']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $dataFechaEdadCodigo[$i]['PACIENTE']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $dataFechaEdadCodigo[$i]['DNI']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, substr($dataFechaEdadCodigo[$i]['FNACIMIENTO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $dataFechaEdadCodigo[$i]['SEXO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $dataFechaEdadCodigo[$i]['DISTRITO']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $dataFechaEdadCodigo[$i]['EDAD']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $dataFechaEdadCodigo[$i]['MESES']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, substr($dataFechaEdadCodigo[$i]['FECHAINGRESO'],0,10));
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $dataFechaEdadCodigo[$i]['CIE10']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $dataFechaEdadCodigo[$i]['DIAGNOSTICO']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($estiloCentrado);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($estiloCentrado);
					$j = $j + 1;
				}
			break;
		}

		//echo "<pre>";
		//print_r($dataFechaEdad);exit();
		//echo "</pre>";
		
	// TITULO DEL DOCUMENTO
	$objPHPExcel->getActiveSheet()->setTitle('ATENCIONES POR DIAGNOSTICO');

	#AJUSTANDO CELDAS
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

    #FILTROS
    $objPHPExcel->getActiveSheet()->setAutoFilter("A1:K".$j); 

    // Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);

	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="atenciones.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit();
 ?>|