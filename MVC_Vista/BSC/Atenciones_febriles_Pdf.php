<?php 

ini_set('memory_limit', '1024M');


set_time_limit(0);

require('../../MVC_Complemento/fpdf/fpdf.php');
require('../../MVC_Modelo/ReportesM.php');

class PDF extends FPDF
{	
	function Header(){	

		//Imagen institucional
		$this->Cell(100);
		$this->Image('../../MVC_Complemento/img/hndac.jpg',12,5,10,10);
		$this->Cell(35);

		//Titulo principal
		$this->setfont('Arial','b',9);
		$this->Cell(1,2,'REPORTE DE VIGILANCIA DIARIA DE FEBRILES',0,0,'C');

		//Hora de impresion
		$this->setfont('Arial','',7);
		$this->Cell(140,2,date('h:m:s a'),0,0,'R');

	}
	function Footer()
	{
		$this->SetY(-11);
		$this->SetFont('Arial','',7);
		$this->Cell(190,5,"HNDAC / OESI / DESARROLLO DE SISTEMAS",0,0,'L'); 		 				
	}
}

$pdf = new PDF("L","mm","A4");
$pdf->AliasNbPages();
$pdf->AddPage();



	$FechaInicio_temp = substr($_REQUEST["fechaIng"], 0,10);
	$FechaFinal_temp = substr($_REQUEST["fechaFin"], 0,10);

	list($mes,$dia,$anio) = explode("/",$FechaInicio_temp);
	$FechaInicio = $anio."-".$mes."-".$dia;


	list($mes,$dia,$anio) = explode("/",$FechaFinal_temp);
	$FechaFinal = $anio."-".$mes."-".$dia;

	

	$EdadIni 		= $_REQUEST["EdadIni"];
	$EdadFin 		= $_REQUEST["EdadFin"];
	$Diagnostico 	= $_REQUEST["Diagnostico"];


	$h=0;
	
	$pdf->Ln(3);
	$pdf->Cell(0,13,'','B',0,'R');
	$pdf->Ln(15);
	$pdf->setfont('Arial','',6);
	$pdf->Cell(15,7,'IDATENCION','B',0,'C');
	$pdf->Cell(55,7,'PACIENTE','B',0,'C');
	$pdf->Cell(17,7,'DNI','B',0,'C');
	$pdf->Cell(17,7,'FNACIMIENTO','B',0,'C');
	$pdf->Cell(17,7,'SEXO','B',0,'C');
	$pdf->Cell(17,7,'DISTRITO','B',0,'C');
	$pdf->Cell(8,7, utf8_decode('AÑOS'),'B',0,'C');
	$pdf->Cell(8,7,'MESES','B',0,'C');
	$pdf->Cell(20,7,'FECHAINGRESO','B',0,'C');
	$pdf->Cell(10,7,'CIE10','B',0,'C');
	$pdf->Cell(82,7,'DIAGNOSTICO','B',0,'C');
	$pdf->Ln(1);

$tipoReporte = $_REQUEST["tipoReporte"];



	//Generar reporte de febriles
		switch($tipoReporte){
			case 'A':
				$dataFechaEdad = ReporteFebrilesPorFechaEdad($FechaInicio, $FechaFinal, $EdadIni, $EdadFin);
				for ($i=0; $i < count($dataFechaEdad); $i++){
					$pdf->Ln(8);
					$pdf->setfont('Arial','',6);
					$pdf->Cell(15,2,$dataFechaEdad[$i]['IDATENCION'],0,0,'C');
					$pdf->Cell(55,2,utf8_decode($dataFechaEdad[$i]['PACIENTE']),0,0,'C');
					$pdf->Cell(17,2,$dataFechaEdad[$i]['DNI'],0,0,'C');
					$pdf->Cell(17,2,substr($dataFechaEdad[$i]['FNACIMIENTO'],0,10),0,0,'C');
					$pdf->Cell(17,2,$dataFechaEdad[$i]['SEXO'],0,0,'C');
					$pdf->Cell(17,2,$dataFechaEdad[$i]['DISTRITO'],0,0,'C');
					$pdf->Cell(8,2,round($dataFechaEdad[$i]['EDAD']),0,0,'C');
					$pdf->Cell(8,2,round($dataFechaEdad[$i]['MESES']),0,0,'C');
					$pdf->Cell(20,2,substr($dataFechaEdad[$i]['FECHAINGRESO'],0,10),0,0,'C');
					$pdf->Cell(10,2,$dataFechaEdad[$i]['CIE10'],0,0,'C');
					$pdf->Cell(82,2,utf8_decode($dataFechaEdad[$i]['DIAGNOSTICO']),0,0,'C');
					$h=$h+1;
			
					if($h>=19)
					{
						$pdf->AddPage();
						$h=0;
						$pdf->Ln(3);
						$pdf->Cell(0,13,'','B',0,'R');
						$pdf->Ln(15);
						//$pdf->setfont('Arial','',7);
						$pdf->Cell(15,7,'IDATENCION','B',0,'C');
						$pdf->Cell(55,7,'PACIENTE','B',0,'C');
						$pdf->Cell(17,7,'DNI','B',0,'C');
						$pdf->Cell(17,7,'FNACIMIENTO','B',0,'C');
						$pdf->Cell(17,7,'SEXO','B',0,'C');
						$pdf->Cell(17,7,'DISTRITO','B',0,'C');
						$pdf->Cell(8,7, utf8_decode('AÑOS'),'B',0,'C');
						$pdf->Cell(8,7,'MESES','B',0,'C');
						$pdf->Cell(20,7,'FECHAINGRESO','B',0,'C');
						$pdf->Cell(10,7,'CIE10','B',0,'C');
						$pdf->Cell(82,7,'DIAGNOSTICO','B',0,'C');
						$pdf->Ln(1);
					}

					//echo "<pre>";
					//print_r($dataFechaEdad);exit();
					//echo "</pre>";
				}
			break;

			case 'B':
			$dataFecha = ReporteFebrilesPorFecha($FechaInicio, $FechaFinal);
				for ($i=0; $i < count($dataFecha); $i++){
					$pdf->Ln(8);
					$pdf->setfont('Arial','',6);
					$pdf->Cell(15,2,$dataFecha[$i]['IDATENCION'],0,0,'C');
					$pdf->Cell(55,2,utf8_decode($dataFecha[$i]['PACIENTE']),0,0,'C');
					$pdf->Cell(17,2,$dataFecha[$i]['DNI'],0,0,'C');
					$pdf->Cell(17,2,substr($dataFecha[$i]['FNACIMIENTO'],0,10),0,0,'C');
					$pdf->Cell(17,2,$dataFecha[$i]['SEXO'],0,0,'C');
					$pdf->Cell(17,2,$dataFecha[$i]['DISTRITO'],0,0,'C');
					$pdf->Cell(8,2,round($dataFecha[$i]['EDAD']),0,0,'C');
					$pdf->Cell(8,2,round($dataFecha[$i]['MESES']),0,0,'C');
					$pdf->Cell(20,2,substr($dataFecha[$i]['FECHAINGRESO'],0,10),0,0,'C');
					$pdf->Cell(10,2,$dataFecha[$i]['CIE10'],0,0,'C');
					$pdf->Cell(82,2,utf8_decode($dataFecha[$i]['DIAGNOSTICO']),0,0,'C');
					$h=$h+1;

					if($h>=19)
					{
						$pdf->AddPage();
						$h=0;
						$pdf->Ln(3);
						$pdf->Cell(0,13,'','B',0,'R');
						$pdf->Ln(15);
						$pdf->Cell(15,7,'IDATENCION','B',0,'C');
						$pdf->Cell(55,7,'PACIENTE','B',0,'C');
						$pdf->Cell(17,7,'DNI','B',0,'C');
						$pdf->Cell(17,7,'FNACIMIENTO','B',0,'C');
						$pdf->Cell(17,7,'SEXO','B',0,'C');
						$pdf->Cell(17,7,'DISTRITO','B',0,'C');
						$pdf->Cell(8,7, utf8_decode('AÑOS'),'B',0,'C');
						$pdf->Cell(8,7,'MESES','B',0,'C');
						$pdf->Cell(20,7,'FECHAINGRESO','B',0,'C');
						$pdf->Cell(10,7,'CIE10','B',0,'C');
						$pdf->Cell(82,7,'DIAGNOSTICO','B',0,'C');
						$pdf->Ln(1);
					}
				}		
			break;

			case 'C':
			$dataFechaCodigo = ReporteFebrilesPorFechaCodigo($Diagnostico, $FechaInicio, $FechaFinal);
				for ($i=0; $i < count($dataFechaCodigo); $i++){
					$pdf->Ln(8);
					$pdf->setfont('Arial','',6);
					$pdf->Cell(15,2,$dataFechaCodigo[$i]['IDATENCION'],0,0,'C');
					$pdf->Cell(55,2,utf8_decode($dataFechaCodigo[$i]['PACIENTE']),0,0,'C');
					$pdf->Cell(17,2,$dataFechaCodigo[$i]['DNI'],0,0,'C');
					$pdf->Cell(17,2,substr($dataFechaCodigo[$i]['FNACIMIENTO'],0,10),0,0,'C');
					$pdf->Cell(17,2,$dataFechaCodigo[$i]['SEXO'],0,0,'C');
					$pdf->Cell(17,2,$dataFechaCodigo[$i]['DISTRITO'],0,0,'C');
					$pdf->Cell(8,2,round($dataFechaCodigo[$i]['EDAD']),0,0,'C');
					$pdf->Cell(8,2,round($dataFechaCodigo[$i]['MESES']),0,0,'C');
					$pdf->Cell(20,2,substr($dataFechaCodigo[$i]['FECHAINGRESO'],0,10),0,0,'C');
					$pdf->Cell(10,2,$dataFechaCodigo[$i]['CIE10'],0,0,'C');
					$pdf->Cell(82,2,utf8_decode($dataFechaCodigo[$i]['DIAGNOSTICO']),0,0,'C');
					$h=$h+1;

					if($h>=19)
					{
						$pdf->AddPage();
						$h=0;
						$pdf->Ln(3);
						$pdf->Cell(0,13,'','B',0,'R');
						$pdf->Ln(15);
						$pdf->Cell(15,7,'IDATENCION','B',0,'C');
						$pdf->Cell(55,7,'PACIENTE','B',0,'C');
						$pdf->Cell(17,7,'DNI','B',0,'C');
						$pdf->Cell(17,7,'FNACIMIENTO','B',0,'C');
						$pdf->Cell(17,7,'SEXO','B',0,'C');
						$pdf->Cell(17,7,'DISTRITO','B',0,'C');
						$pdf->Cell(8,7, utf8_decode('AÑOS'),'B',0,'C');
						$pdf->Cell(8,7,'MESES','B',0,'C');
						$pdf->Cell(20,7,'FECHAINGRESO','B',0,'C');
						$pdf->Cell(10,7,'CIE10','B',0,'C');
						$pdf->Cell(82,7,'DIAGNOSTICO','B',0,'C');
						$pdf->Ln(1);
					}
				}
			break;

			case 'D':
			$dataFechaEdadCodigo = ReporteFebrilesPorFechaCodigoEdad($Diagnostico, $FechaInicio, $FechaFinal, $EdadIni, $EdadFin);
				for ($i=0; $i < count($dataFechaEdadCodigo); $i++){
					$pdf->Ln(8);
					$pdf->setfont('Arial','',6);
					$pdf->Cell(15,2,$dataFechaEdadCodigo[$i]['IDATENCION'],0,0,'C');
					$pdf->Cell(55,2,utf8_decode($dataFechaEdadCodigo[$i]['PACIENTE']),0,0,'C');
					$pdf->Cell(17,2,$dataFechaEdadCodigo[$i]['DNI'],0,0,'C');
					$pdf->Cell(17,2,substr($dataFechaEdadCodigo[$i]['FNACIMIENTO'],0,10),0,0,'C');
					$pdf->Cell(17,2,$dataFechaEdadCodigo[$i]['SEXO'],0,0,'C');
					$pdf->Cell(17,2,$dataFechaEdadCodigo[$i]['DISTRITO'],0,0,'C');
					$pdf->Cell(8,2,round($dataFechaEdadCodigo[$i]['EDAD']),0,0,'C');
					$pdf->Cell(8,2,round($dataFechaEdadCodigo[$i]['MESES']),0,0,'C');
					$pdf->Cell(20,2,substr($dataFechaEdadCodigo[$i]['FECHAINGRESO'],0,10),0,0,'C');
					$pdf->Cell(10,2,$dataFechaEdadCodigo[$i]['CIE10'],0,0,'C');
					$pdf->Cell(82,2,utf8_decode($dataFechaEdadCodigo[$i]['DIAGNOSTICO']),0,0,'C');
					$h=$h+1;

					if($h>=19)
					{
						$pdf->AddPage();
						$h=0;
						$pdf->Ln(3);
						$pdf->Cell(0,13,'','B',0,'R');
						$pdf->Ln(15);
						$pdf->Cell(15,7,'IDATENCION','B',0,'C');
						$pdf->Cell(55,7,'PACIENTE','B',0,'C');
						$pdf->Cell(17,7,'DNI','B',0,'C');
						$pdf->Cell(17,7,'FNACIMIENTO','B',0,'C');
						$pdf->Cell(17,7,'SEXO','B',0,'C');
						$pdf->Cell(17,7,'DISTRITO','B',0,'C');
						$pdf->Cell(8,7, utf8_decode('AÑOS'),'B',0,'C');
						$pdf->Cell(8,7,'MESES','B',0,'C');
						$pdf->Cell(20,7,'FECHAINGRESO','B',0,'C');
						$pdf->Cell(10,7,'CIE10','B',0,'C');
						$pdf->Cell(82,7,'DIAGNOSTICO','B',0,'C');
						$pdf->Ln(1);
					}
				}
			break;
		}
	
$pdf->Output();


?>