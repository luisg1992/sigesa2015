<?php 
ini_set('memory_limit', '1024M'); 
require('../../MVC_Modelo/bscM.php');
require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Complemento/fpdf/fpdf.php');
require('../../MVC_Complemento/librerias/Funciones.php');

#OBTENIENDO VALORES
$idcc = $_REQUEST["idcc"];
$nombrecc = $_REQUEST["nombrecc"];
$fechaini = $_REQUEST["fechaini"];
$fechafin = $_REQUEST["fechafin"];

class PDF extends FPDF
{	
	function Header(){	
		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
		$fechainicio = $anio."/".$mes."/".$dia;

		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
		$fechafin = $anio."/".$mes."/".$dia;

		$this->SetFont('Arial','',9);
		$this->Cell(60);
		$this->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(40);
		$this->setfont('arial','b',11);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		$this->SetFont('Arial','',9); 
	    $this->Ln(3);
		$this->Cell(95);
		$this->Cell(80,5,'REPORTE POR CENTRO DE COSTO',0,0,'C');
		$this->Ln(4);
		$this->setfont('arial','B',9);
		$this->Cell(45);
		$this->Cell(179,5,$_REQUEST["nombrecc"],0,0,'C');		
		
		$this->Image('../../MVC_Complemento/img/grcallo.jpg',265,5,18,20);
		$this->Ln(4);
		$this->setfont('arial','B',8);
		$this->Cell(45);
		$this->Cell(181,5,'RANGO: '.$fechainicio.' - '.$fechafin,0,0,'C');
		$this->Ln(10);
		
		$this->SetFont('Arial','b',9);
		$this->Cell(15,5,'COD',1,0,'C');  
		$this->Cell(173,5,'NOMBRES',1,0,'C');		
		$this->Cell(20,5,'CANTIDAD',1,0,'C');		
		$this->Cell(20,5,'SUB TOTAL',1,0,'C');		
		$this->Cell(30,5,'EXONERACIONES',1,0,'C');
		$this->Cell(20,5,'TOTAL',1,0,'C');
		$this->Ln(5);
	}
	function Footer()
	{
		$this->SetY(-10);
		$this->SetFont('Arial','',7);
		$this->Cell(200,5,"HNDAC / OESI / DESARROLLO DE SISTEMAS",0,0,'L'); 		 				
	}
}

$pdf = new PDF("L","mm","A4");
$pdf->AliasNbPages();
$pdf->AddPage();


$data_general = Centro_Costo_x_Id_fi_ff_M($idcc,$fechainicio,$fechafin);

		for ($i=0; $i < count($data_general); $i++) { 
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(15,5,$data_general[$i]["Codigo"],'LRB',0,'C');  
			$pdf->Cell(173,5,strtoupper(utf8_decode($data_general[$i]["Nombre"])),'LRB',0,'L');		
			$pdf->Cell(20,5,$data_general[$i]["Cantidad"],'LRB',0,'C');		
			$pdf->Cell(20,5,$data_general[$i]["SubTotal"],'LRB',0,'C');		
			$pdf->Cell(30,5,$data_general[$i]["Exoneraciones"],'LRB',0,'C');
			$pdf->Cell(20,5,$data_general[$i]["Total"],'LRB',0,'C');
			$pdf->Ln(5);

		}

	

$pdf->Output();
?>

