<?php 
    ini_set('memory_limit', '1024M');
 ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
        <title>Datos Emergencia</title>

        <!--CSS-->
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.datagrid.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
        
        <!--OPERACIONES JS-->
        <script>
        $(document).ready(function() {
            $("#tabla_cc").hide();
            $("#tabla_epi").hide();
            $("#tabla_trans").hide();
            $("#tabla_agreciones").hide();
            $("#tabla_topicos").hide();
        });

        function Data_genera_reporte()
        {
            $("#tabla_epi").hide(); 
            $("#tabla_trans").hide();
            $("#tabla_agreciones").hide();
            $("#tabla_topicos").hide();
            $("#tabla_cc").show();
            return false;    
        }

        function Data_epidemio_reporte()
        {
            $("#tabla_cc").hide();
            $("#tabla_trans").hide();
            $("#tabla_agreciones").hide();
            $("#tabla_topicos").hide();
            $("#tabla_epi").show(); 
            return false;
        }

        function Data_transito_reportes()
        {
            $("#tabla_cc").hide();
            $("#tabla_epi").hide();
            $("#tabla_agreciones").hide();
            $("#tabla_topicos").hide();
            $("#tabla_trans").show();
            return false;
        }

        function Data_agreciones_reporte()
        {
            $("#tabla_cc").hide();
            $("#tabla_epi").hide();
            $("#tabla_trans").hide();
            $("#tabla_topicos").hide();
            $("#tabla_agreciones").show();
            return false;
        }

        function Data_Topicos_reportes()
        {
            $("#tabla_cc").hide();
            $("#tabla_epi").hide();
            $("#tabla_trans").hide();
            $("#tabla_agreciones").hide();
            $("#tabla_topicos").show();
            return false;   
        }

        function ExportarExcelEmergencia()
        {
            var fecha_inicio_cc_rp  = $("#fecha_inicio_cc_rp").combobox('getValue');
            var fecha_fin_cc_rp = $("#fecha_fin_cc_rp").combobox('getValue');
            window.location.href = '../../MVC_Vista/BSC/emergencia_excel.php?fechaini='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp;
        }

        function ExportarExcelEpidemio()
        {
            var fecha_inicio_cc_rp  = $("#fecha_inicio_cc_rp").combobox('getValue');
            var fecha_fin_cc_rp = $("#fecha_fin_cc_rp").combobox('getValue');
            window.location.href = '../../MVC_Vista/BSC/epidemio_excel.php?fechaini='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp;
        }

        function ExportarExcelTransito()
        {
            var fecha_inicio_cc_rp  = $("#fecha_inicio_cc_rp").combobox('getValue');
            var fecha_fin_cc_rp = $("#fecha_fin_cc_rp").combobox('getValue');
            window.location.href = '../../MVC_Vista/BSC/acc_transp_excel.php?fechaini='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp;
        }
        
        function ExportarExcelAgreciones()
        {
            var fecha_inicio_cc_rp  = $("#fecha_inicio_cc_rp").combobox('getValue');
            var fecha_fin_cc_rp = $("#fecha_fin_cc_rp").combobox('getValue');
            window.location.href = '../../MVC_Vista/BSC/agresiones_excel.php?fechaini='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp;
        }

        function ExportarExcelTopicos()
        {
            var fecha_inicio_cc_rp  = $("#fecha_inicio_cc_rp").combobox('getValue');
            var fecha_fin_cc_rp = $("#fecha_fin_cc_rp").combobox('getValue');
            window.location.href = '../../MVC_Vista/BSC/topicos_excel.php?fechaini='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp;
        }
        
        </script>
    </head>
    <body>
        <div class="easyui-layout" style="width:100%;height:100%;">         
            <div data-options="region:'center'" style="width:100%;">                
                <div class="easyui-layout" data-options="fit:true">                    
                    <div data-options="region:'north',split:true" style="height:11%;padding:1%;" title="Datos Emergencia">
                        <!--OPERACIONES PARA REPORTES-->
                        <div id="operaciones_medico">
                            <td>&nbsp;&nbsp;&nbsp;De:</td>
                            <td>
                                <input class="easyui-datebox" data-options="sharedCalendar:'#cc'" id="fecha_inicio_cc_rp">
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;Hasta:</td>
                            <td>
                                <input class="easyui-datebox" data-options="sharedCalendar:'#cc'" id="fecha_fin_cc_rp">
                            </td> 
                            <td>
                                &nbsp;&nbsp;<a href="#"  class="easyui-linkbutton" id="" onclick="Data_genera_reporte()" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'" disabled>Data General&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </td>
                            <td>
                                &nbsp;&nbsp;<a href="#"  class="easyui-linkbutton" id="" onclick="Data_transito_reportes()" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'">Accidentes de Transito</a>
                            </td>
                            <td>
                                &nbsp;&nbsp;<a href="#"  class="easyui-linkbutton" id="" onclick="Data_agreciones_reporte()" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'">Agresiones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </td>
                            <td>
                                &nbsp;&nbsp;<a href="#"  class="easyui-linkbutton" id="" onclick="Data_Topicos_reportes()" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'">Por Topicos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </td>
                            <td>
                                &nbsp;&nbsp;<a href="#"  class="easyui-linkbutton" id="" onclick="Data_epidemio_reporte()" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'">Epidemiologia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </td>
                            <div id="cc" class="easyui-calendar"></div>
                        </div>
                    </div>
                    <div data-options="region:'center'" style="padding:2%;">
                        <div id="tabla_cc" style="">
                            Exportar a: <a href="#" class="easyui-linkbutton" onclick="ExportarExcelEmergencia()"><img src="../../MVC_Complemento/img/Excel_Document_Icon.PNG" height="17" width="17" alt="" style="margin-top:5px;">&nbsp;Excel Emergencia</a>                           
                        </div>
                        <div id="tabla_epi" style="">
                            Exportar a: <a href="#" class="easyui-linkbutton" onclick="ExportarExcelEpidemio()"><img src="../../MVC_Complemento/img/Excel_Document_Icon.PNG" height="17" width="17" alt="" style="margin-top:5px;">&nbsp;Excel Epidemio</a>                           
                        </div>
                        <div id="tabla_trans" style="">
                            Exportar a: <a href="#" class="easyui-linkbutton" onclick="ExportarExcelTransito()"><img src="../../MVC_Complemento/img/Excel_Document_Icon.PNG" height="17" width="17" alt="" style="margin-top:5px;">&nbsp;Excel A. Transito</a>                       
                        </div>
                        <div id="tabla_agreciones" style="">
                            Exportar a: <a href="#" class="easyui-linkbutton" onclick="ExportarExcelAgreciones()"><img src="../../MVC_Complemento/img/Excel_Document_Icon.PNG" height="17" width="17" alt="" style="margin-top:5px;">&nbsp;Excel Agreciones</a>                       
                        </div>
                        <div id="tabla_topicos" style="">
                            Exportar a: <a href="#" class="easyui-linkbutton" onclick="ExportarExcelTopicos()"><img src="../../MVC_Complemento/img/Excel_Document_Icon.PNG" height="17" width="17" alt="" style="margin-top:5px;">&nbsp;Excel Topicos</a>                       
                        </div>
                    </div>
                </div>
            </div>


            
        </div>
    </body>

</html>