<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>BSC</title>
        <!--CSS-->
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
        
        <!--OPERACIONES JS-->
        <script>
        function Reporte_General(){
            $("#id_medicos").combobox('clear');
            ClearCombobox();
            $.messager.progress({
                title:'Por favor espera',
                msg:'Cargando datos...'
            });
            setTimeout(function(){
                $.messager.progress('close');
            },3000)
            $("#opciones_hora_medico").show();
            $.ajax({
                url: '../../MVC_Controlador/BSC/bscC.php?acc=Pruebas_BSC',
                type: 'POST',
                dataType: 'json',
                data: {},
                success: function(data){
                    var Indicadores = [data.ENERO,data.FEBRERO,data.MARZO,data.ABRIL,data.MAYO,data.JUNIO,data.JULIO,data.AGOSTO,data.SETIEMBRE,data.OCTUBRE,data.NOVIEMBRE,data.DICIEMBRE];
                    var Meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembre','Octubre','Noviembre','Diciembre'];
                    GenerarGrafico(Meses,Indicadores,'HNDAC','INDICADORES DE RENDIMIENTO',0,6);
                }
            });
        }        

        function GenerarGrafico(categorias,valores,Subtitulo,Titulo,min,max){
            if (max == 0.5) {
                data = valores;
                var linea_tope = 0.1;
            }
            else {
                var data = [];
                $.each(valores, function(index, value){
                    var color;
                    if (value > 4) color = 'green';
                    else if (value > 2) color = 'yellow';
                    else if (value < 2) color = 'red';
                    data.push({y:value, color: color});
                });
                var linea_tope = 4;
            }
            $('#graficos').html(' ');
            $('#graficos').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: Titulo
                },
                subtitle: {
                    text: Subtitulo
                },
                xAxis: {
                    categories: categorias,
                    crosshair: true
                },
                yAxis: {
                    min: min,
                    max: max,
                    plotLines: [{
                        color: '#666',
                        width: 2,
                        value: linea_tope
                    }],
                    title: {
                        text: 'Indicador'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: ' ',
                    data: data  
                }],
                credits: {
                    text: 'OESI / AREA DE DESARROLLO DE SISTEMAS'
                }
            });
        }

        function MostrarReportesPorDepartamento(nombre){
            $.messager.progress({
                title:'Por favor espera',
                msg:'Cargando datos...'
            });
            
            setTimeout(function(){
                $.messager.progress('close');
            },3000)
            
            var id_dpto = $("#dpto_consultas").combobox('getValue');     
            $.ajax({
                url: '../../MVC_Controlador/BSC/bscC.php?acc=Reportes_x_Departamentos',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_dpto: id_dpto
                },
                success: function(data){                  
                    GenerarGrafico(data.ESPECILIDADES,data.INDICADORES,'DEPARTAMENTO: '+nombre,'INDICADORES DE RENDIMIENTO',0,6)
                }
            }); 
        }

        function MostrarReportesxEspecialidades(id_especilidad,nombre){
            $.messager.progress({
                title:'Por favor espera',
                msg:'Cargando datos...'
            });
            
            setTimeout(function(){
                $.messager.progress('close');
            },2000)
            var id_dpto = $("#dpto_consultas").combobox('getValue');
            var id_espe = id_especilidad;
            $('#graficos').html(' ');
            $.ajax({
                url: '../../MVC_Controlador/BSC/bscC.php?acc=Reportes_x_Especialidades',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_dpto: id_dpto,
                    especia: id_espe
                },
                success: function(data){
                    GenerarGrafico(data.MESES,data.INDICADORES,'ESPECIALIDADES: '+nombre,'INDICADORES DE RENDIMIENTO',0,6)
                }
            });            
        }

        function MostrarReportesxMedico(idmedico,nombre){
            $.messager.progress({
                title:'Por favor espera',
                msg:'Cargando datos...'
            });
            setTimeout(function(){
                $.messager.progress('close');
            },3000)
            var idmedico = idmedico;
            $.ajax({
                url: '../../MVC_Controlador/BSC/bscC.php?acc=Reportes_x_Id_Medico',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_medico: idmedico
                },
                success: function(data){
                    GenerarGrafico(data.MESES,data.INDICADORES,'MEDICO: '+nombre+'<br>ESPECIALIDAD: '+data.ESPECIALIDAD_MEDICO,'INDICADORES DE RENDIMIENTO',0,6);
                }
            });
        }

        function ReportesEmergencia(){
            $.messager.progress({
                title:'Por favor espera',
                msg:'Cargando datos...'
            });
            setTimeout(function(){
                $.messager.progress('close');
            },3200)
            $("#opciones_hora_medico").hide();
            //$("#dpto_consultas").combobox('disable');
            //$("#esp_consultas").combobox('disable');
            //$("#id_medicos").combobox('disable');
            $.ajax({
                url: '../../MVC_Controlador/BSC/bscC.php?acc=Atendidos_x_Emergencia',
                type: 'POST',
                dataType: 'json',
                data: {},
                success: function(data){
                    var Indicadores = [data.ENERO,data.FEBRERO,data.MARZO,data.ABRIL,data.MAYO,data.JUNIO,data.JULIO,data.AGOSTO,data.SETIEMBRE,data.OCTUBRE,data.NOVIEMBRE,data.DICIEMBRE];
                    var Meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembre','Octubre','Noviembre','Diciembre'];
                    GenerarGrafico(Meses,Indicadores,'HNDAC','RAZON DE EMERGENCIAS POR CONSULTA EXTERNA',0,0.5);
                }
            });
        }

        function Retornar(){
            if ($("#esp_consultas").combobox('getText').length < 1) {
                Reporte_General();
            }
            else {
                var nombre_temp = $("#dpto_consultas").combobox('getText');
                $("#esp_consultas").combobox('clear');
                MostrarReportesPorDepartamento(nombre_temp);
            }
        }

        function ClearCombobox(){
            $("#dpto_consultas").combobox('clear');
            $("#esp_consultas").combobox('clear');
        }

        $(function(){
            $('#mostrar_cuadro_general').tooltip({
                position: 'right',
                content: '<span style="color:#fff">SUMATORIA DE ATENCIONES <BR> SUMATORIA DE HORAS PROGRAMADAS</span>',
                onShow: function(){
                    $(this).tooltip('tip').css({
                        backgroundColor: 'rgba(0,0,0,0.7)',
                        borderColor:'rgba(0,0,0,0.7)'
                    });
                }
            });

            $('#razon_emergencia').tooltip({
                position: 'right',
                content: '<span style="color:#fff">SUMATORIA DE ATENCIONES DE EMERGENCIA <BR> SUMATORIA DE HORAS PROGRAMADAS</span>',
                onShow: function(){
                    $(this).tooltip('tip').css({
                        backgroundColor: 'rgba(0,0,0,0.7)',
                        borderColor:'rgba(0,0,0,0.7)'
                    });
                }
            });
        });
        </script>
    </head>
    <body>
        <div class="easyui-layout" style="width:100%;height:100%;">  

            <!--DIV ENCABEZADO MENU-->      
            <div data-options="region:'west'" title="Indicadores" style="width:13%;padding:6%;">
                
                <!--REPORTE GENERAL-->
                <a href="#"  class="easyui-linkbutton" id="mostrar_cuadro_general" onclick="Reporte_General()" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'">Rendimiento Hora - Medico</a>
                <br><br><br>
                <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'" onclick="ReportesEmergencia()" id="razon_emergencia">Razon de Emergencia CEX&nbsp;&nbsp;&nbsp;</a>
            </div>
            
            
            <!--DIV GRAFICOS-->
            <div data-options="region:'center'" style="width:87%;">                
                <div class="easyui-layout" data-options="fit:true">                    
                    <div data-options="region:'north',split:true" style="height:10%;padding:1%;" title="Opciones">
                        <!--OPCIONES PARA HORA MEDICO-->
                        <div id="opciones_hora_medico">
                            <!--REPORTE POR DEPARTAMENTO-->
                            <label>Departamentos: </label>&nbsp;&nbsp;
                            <input id="dpto_consultas" class="easyui-combobox" data-options="
                                valueField: 'id',
                                textField: 'text',
                                url: '../../MVC_Controlador/BSC/bscC.php?acc=Departamentos_Hospital',
                                onSelect: function(rec){
                                    MostrarReportesPorDepartamento(rec.text);
                                    $('#esp_consultas').combobox('clear');
                                    $('#id_medicos').combobox('clear');
                                    var url = '../../MVC_Controlador/BSC/bscC.php?acc=Especialidades_Hospital&id_dpto='+rec.id;
                                    $('#esp_consultas').combobox('reload', url);
                            }">&nbsp;&nbsp;
                            
                            <!--REPORTE POR ESPECIALIADAD-->
                            <label>Especialidades:</label>&nbsp;&nbsp;    
                            <input id="esp_consultas" class="easyui-combobox" data-options="
                                valueField:'id',
                                textField:'text',
                                onSelect:function(rec){
                                    MostrarReportesxEspecialidades(rec.id,rec.text);  
                            }">&nbsp;&nbsp;

                            <!--REPORTE POR MEDICO-->
                            <label>Medicos</label>&nbsp;&nbsp;
                            <input id="id_medicos" class="easyui-combobox" data-options="
                                valueField: 'id',
                                textField: 'text',
                                url: '../../MVC_Controlador/BSC/bscC.php?acc=Mostrar_Medicos',
                                onSelect: function(rec){
                                    ClearCombobox();
                                    MostrarReportesxMedico(rec.id,rec.text);
                            }">&nbsp;&nbsp;
                            <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="Retornar()">Retornar</a>
                        </div>                        
                    </div>
                    <div data-options="region:'center'" title="Graficos" style="height:90%;padding:7% 3% 3% 3%;">
                        <div id="graficos">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>