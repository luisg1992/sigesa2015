<?php 
ini_set('memory_limit', '1024M'); 

require('../../MVC_Modelo/bscM.php');
require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');
	
	$objPHPExcel = new PHPExcel();

	#ESTILOS
	$estiloCentrado = array(
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ));

    $estiloEncabezados = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4682B4'),
            
    	),
        'font' => array(
	            'bold' => true,
	            'color' => array('rgb' => 'ffffff'),
	            'size' => 11,
	            'name' => 'Calibri',
    		)
    );
    

	// Establecer propiedades
	$objPHPExcel->getProperties()
	->setCreator("Rodolfo Crisanto")
	->setLastModifiedBy("Rodolfo Crisanto")
	->setTitle("Emergencia")
	->setSubject("Emergencia")
	->setDescription("Descripcion de data.")
	->setKeywords("Excel Office 2007 openxml php")
	->setCategory("Pruebas de Excel");


	// CABECERA
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'N°')
	->setCellValue('B1', 'HISTORIA CLINICA')
	->setCellValue('C1', 'FECHA')
	->setCellValue('D1', 'PACIENTE')
	->setCellValue('E1', 'DESCRIPCION')
	->setCellValue('F1', 'CIEXT')
	->setCellValue('G1', 'EDAD')
	->setCellValue('H1', 'TIPO')
	->setCellValue('I1', 'SEXO');



	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($estiloEncabezados);

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
	$fechainicio = $anio."/".$mes."/".$dia;

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
	$fechafin = $anio."/".$mes."/".$dia;

	$data_emergencia = Data_General_Transtio_M($fechainicio,$fechafin);

	if (!$data_emergencia) {
		echo 'sin datos';exit();
	}

	$contador = count($data_emergencia);
	$j = 2;
	for ($i=0; $i < $contador; $i++) { 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $i);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $data_emergencia[$i]['HISTORIA']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, substr($data_emergencia[$i]['FECHA'], 0, 10));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $data_emergencia[$i]['PACIENTE']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $data_emergencia[$i]['DIAGNOSTICO1']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $data_emergencia[$i]['DX1']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $data_emergencia[$i]['EDAD']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $data_emergencia[$i]['TIPO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $data_emergencia[$i]['SEXO']);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($estiloCentrado);
		$j++;
	}



	// TITULO DEL DOCUMENTO
	$objPHPExcel->getActiveSheet()->setTitle('ACCIDENTES DE TRANSITO');

	#AJUSTANDO CELDAS
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

    #FILTROS
    $objPHPExcel->getActiveSheet()->setAutoFilter("A1:I".$j); 

	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);

	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="acc_transito.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit();
 ?>|