<?php 
ini_set('memory_limit', '1024M'); 

require('../../MVC_Modelo/bscM.php');
require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');
	
	$objPHPExcel = new PHPExcel();

	#ESTILOS
	$estiloCentrado = array(
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ));

    $estiloEncabezados = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4682B4'),
            
    	),
        'font' => array(
	            'bold' => true,
	            'color' => array('rgb' => 'ffffff'),
	            'size' => 11,
	            'name' => 'Calibri',
    		)
    );
    

	// Establecer propiedades
	$objPHPExcel->getProperties()
	->setCreator("Rodolfo Crisanto")
	->setLastModifiedBy("Rodolfo Crisanto")
	->setTitle("Emergencia")
	->setSubject("Emergencia")
	->setDescription("Descripcion de data.")
	->setKeywords("Excel Office 2007 openxml php")
	->setCategory("Pruebas de Excel");


	// CABECERA
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'HISTORIA')
	->setCellValue('B1', 'FECHA')
	->setCellValue('C1', 'PACIENTE')
	->setCellValue('D1', 'DESCRIPCION')
	->setCellValue('E1', 'CIEXT');



	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($estiloEncabezados);

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
	$fechainicio = $anio."/".$mes."/".$dia;

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
	$fechafin = $anio."/".$mes."/".$dia;

	$data_emergencia = Data_General_Agresiones_M($fechainicio,$fechafin);

	if (!$data_emergencia) {
		echo 'sin datos';exit();
	}

	$contador = count($data_emergencia);
	$j = 2;
	for ($i=0; $i < $contador; $i++) { 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $data_emergencia[$i]['HC']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, substr($data_emergencia[$i]['FECHA'], 0, 10));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $data_emergencia[$i]['PACIENTE']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $data_emergencia[$i]['DIAGNOSTICO1']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $data_emergencia[$i]['DX1']);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($estiloCentrado);
		$j++;
	}



	// TITULO DEL DOCUMENTO
	$objPHPExcel->getActiveSheet()->setTitle('AGRECIONES');

	#AJUSTANDO CELDAS
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

    #FILTROS
    $objPHPExcel->getActiveSheet()->setAutoFilter("A1:E".$j); 

	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);

	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="agresiones.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit();
 ?>