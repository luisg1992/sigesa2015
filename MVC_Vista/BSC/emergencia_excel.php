<?php 
ini_set('memory_limit', '10240M'); 

require('../../MVC_Modelo/bscM.php');
require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');
	
	$objPHPExcel = new PHPExcel();

	#ESTILOS
	$estiloCentrado = array(
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ));

    $estiloEncabezados = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4682B4'),
            
    	),
        'font' => array(
	            'bold' => true,
	            'color' => array('rgb' => 'ffffff'),
	            'size' => 11,
	            'name' => 'Calibri',
    		)
    );
    

	// Establecer propiedades
	$objPHPExcel->getProperties()
	->setCreator("Rodolfo Crisanto")
	->setLastModifiedBy("Rodolfo Crisanto")
	->setTitle("Emergencia")
	->setSubject("Emergencia")
	->setDescription("Descripcion de data.")
	->setKeywords("Excel Office 2007 openxml php")
	->setCategory("Pruebas de Excel");


	// CABECERA
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'IDATENCION')
	->setCellValue('B1', 'HISTORIA_CLINICA')
	->setCellValue('C1', 'EDAD')
	->setCellValue('D1', 'TIPO')
	->setCellValue('E1', 'EDADHIS')
	->setCellValue('F1', 'EDAD SUSALUD')
	->setCellValue('G1', 'SEXO')
	->setCellValue('H1', 'PACIENTE')
	->setCellValue('I1', 'DNI')
	->setCellValue('J1', 'DISTRITO')
	->setCellValue('K1', 'IDDISTRITO')
	->setCellValue('L1', 'FECHA')
	->setCellValue('M1', 'INGRESO')
	->setCellValue('N1', 'SALIDA')
	->setCellValue('O1', 'SEGURO')
	->setCellValue('P1', 'TOPICO')
	->setCellValue('Q1', 'SERVICIO_EGRESO')
	->setCellValue('R1', 'CAUSA_EXTERNA')
	->setCellValue('S1', 'PRIORIDAD DE ATENCION')
	->setCellValue('T1', 'DIA_DIG')
	->setCellValue('U1', 'HORA_DIG')
	->setCellValue('V1', 'DESCRIPCION MOTIVO INGRESO')
	->setCellValue('W1', 'DESCRIPCION DE PRIORIDAD')
	->setCellValue('X1', 'DESCRIPCION_CAUSA_EXTERNA')
	->setCellValue('Y1', 'DX1')
	->setCellValue('Z1', 'DIAGNOSTICO1');



	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray($estiloEncabezados);

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
	$fechainicio = $anio."/".$mes."/".$dia;

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
	$fechafin = $anio."/".$mes."/".$dia;

	$data_emergencia = Data_General_Emergencia_M($fechainicio,$fechafin);

	if (!$data_emergencia) {
		echo 'sin datos';exit();
	}

	$contador = count($data_emergencia);
	$j = 2;
	for ($i=0; $i < $contador; $i++) { 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $data_emergencia[$i]['IDATENCION']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $data_emergencia[$i]['HISTORIA_CLINICA']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $data_emergencia[$i]['EDAD']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $data_emergencia[$i]['TIPO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $data_emergencia[$i]['EDADHIS']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $data_emergencia[$i]['EDAD SUSALUD']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $data_emergencia[$i]['SEXO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $data_emergencia[$i]['PACIENTE']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $data_emergencia[$i]['DNI']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $data_emergencia[$i]['DISTRITO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $data_emergencia[$i]['IDDISTRITO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, substr($data_emergencia[$i]['FECHA'], 0,10));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$j, $data_emergencia[$i]['INGRESO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$j, $data_emergencia[$i]['SALIDA']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$j, $data_emergencia[$i]['SEGURO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$j, $data_emergencia[$i]['TOPICO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$j, $data_emergencia[$i]['SERVICIO_EGRESO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$j, $data_emergencia[$i]['CAUSA_EXTERNA']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$j, $data_emergencia[$i]['PRIORIDAD DE ATENCION']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$j, $data_emergencia[$i]['DIA_DIG']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$j, $data_emergencia[$i]['HORA_DIG']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$j, $data_emergencia[$i]['DESCRIPCION MOTIVO INGRESO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$j, $data_emergencia[$i]['DESCRIPCION DE PRIORIDAD']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$j, $data_emergencia[$i]['DESCRIPCION_CAUSA_EXTERNA']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$j, $data_emergencia[$i]['DX1']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$j, $data_emergencia[$i]['DIAGNOSTICO1']);

		$objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($estiloCentrado);

		$j++;
	}



	// TITULO DEL DOCUMENTO
	$objPHPExcel->getActiveSheet()->setTitle('ATENCIONES POR EMERGENCIA');

	#AJUSTANDO CELDAS
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);

    #FILTROS
    $objPHPExcel->getActiveSheet()->setAutoFilter("A1:Z".$j); 

	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);

	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="emergencia.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit();
 ?>