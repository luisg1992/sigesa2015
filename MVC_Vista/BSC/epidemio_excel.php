<?php 
ini_set('memory_limit', '1024M'); 

require('../../MVC_Modelo/bscM.php');
require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');
	
	$objPHPExcel = new PHPExcel();

	#ESTILOS
	$estiloCentrado = array(
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ));

    $estiloEncabezados = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4682B4'),
            
    	),
        'font' => array(
	            'bold' => true,
	            'color' => array('rgb' => 'ffffff'),
	            'size' => 11,
	            'name' => 'Calibri',
    		)
    );
    

	// Establecer propiedades
	$objPHPExcel->getProperties()
	->setCreator("HNDAC / OESI / DESARROLLO DE SISTEMAS - Rodolfo Crisanto")
	->setLastModifiedBy("HNDAC / OESI / DESARROLLO DE SISTEMAS - Rodolfo Crisanto")
	->setTitle("Emergencia")
	->setSubject("Emergencia")
	->setDescription("Descripcion de data.")
	->setKeywords("Excel Office 2007 openxml php")
	->setCategory("Pruebas de Excel");


	// CABECERA
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'FECHA INGRESO')
	->setCellValue('B1', 'HORA INGRESO')
	->setCellValue('C1', 'PATERNO')
	->setCellValue('D1', 'MATERNO')
	->setCellValue('E1', 'PRIMER NOMBRE')
	->setCellValue('F1', 'SEGUNDO NOMBRE')
	->setCellValue('G1', 'TIPO SEXO')
	->setCellValue('H1', 'EDAD')
	->setCellValue('I1', 'TIPO EDAD')
	->setCellValue('J1', 'TOPICO')
	->setCellValue('K1', 'DISTRITO DOMICILIO')
	->setCellValue('L1', 'FUENTE FINAN.')
	->setCellValue('M1', 'DXINGRESO')
	->setCellValue('N1', 'DESC_DXINGRESO')
	->setCellValue('O1', 'DXGRESO')
	->setCellValue('P1', 'DESC_DXEGRESO');



	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($estiloEncabezados);

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
	$fechainicio = $anio."/".$mes."/".$dia;

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
	$fechafin = $anio."/".$mes."/".$dia;

	$data_emergencia = Data_General_Epidemio_M($fechainicio,$fechafin);

	if (!$data_emergencia) {
		echo 'sin datos';exit();
	}

	$contador = count($data_emergencia);
	$j = 2;
	for ($i=0; $i < $contador; $i++) { 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, substr($data_emergencia[$i]['FECHA INGRESO'], 0,10));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $data_emergencia[$i]['HORA INGRESO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $data_emergencia[$i]['PATERNO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $data_emergencia[$i]['MATERNO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $data_emergencia[$i]['PRIMER NOMBRE']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $data_emergencia[$i]['SEGUNDO NOMBRE']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $data_emergencia[$i]['TIPO SEXO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $data_emergencia[$i]['EDAD']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $data_emergencia[$i]['TIPO EDAD']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $data_emergencia[$i]['TOPICO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $data_emergencia[$i]['DISTRITO DOMICILIO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, $data_emergencia[$i]['FUENTE FINAN.']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$j, $data_emergencia[$i]['DXINGRESO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$j, $data_emergencia[$i]['DESC_DXINGRESO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$j, $data_emergencia[$i]['DXGRESO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$j, $data_emergencia[$i]['DESC_DXEGRESO']);
		$j = $j + 1;
	}



	// TITULO DEL DOCUMENTO
	$objPHPExcel->getActiveSheet()->setTitle('ATENCIONES POR EMERGENCIA');

	#AJUSTANDO CELDAS
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

    #FILTROS
    $objPHPExcel->getActiveSheet()->setAutoFilter("A1:P".$j); 

	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);

	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="epidemio.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit();
 ?>|