<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title></title>
        <!--CSS-->
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
        
        <!--OPERACIONES JS-->
        <script>
        $(document).ready(function() {
            $("#operaciones_centro_costo").hide();
        });

        function GenerarGrafico(tipo,titulo,subtitulo,categorias,minimo_y,maximo_y,linea_tope_y,series,text_y_title){
            
            $('#graficos').html(' ');
            $('#graficos').highcharts({
                chart: {
                    type: tipo
                },
                title: {
                    text: titulo
                },
                subtitle: {
                    text: subtitulo
                },
                xAxis: {
                    categories: categorias,
                    crosshair: true
                },
                yAxis: {
                    min: minimo_y,
                    max: maximo_y,
                    plotLines: [{
                        color: '#666',
                        width: 2,
                        value: linea_tope_y
                    }],
                    title: {
                        text: text_y_title
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: series,
                credits: {
                    text: 'OESI / AREA DE DESARROLLO DE SISTEMAS'
                }
            });
        }

        function ObjJsonParametrosGraficos(tipo,titulo,subtitulo,categorias,minimo_y,maximo_y,linea_tope_y,series){
            var ObjParametrosGraficos = {
                type:       tipo,
                title:      titulo,
                subtitle:   subtitulo,
                categories: categorias,
                min:        minimo_y,
                max:        maximo_y,
                value:      linea_tope_y,
                data:       valores, 
            };
            return ObjParametrosGraficos;
        }

        

        function Ingreso_Centro_Costo(){
            //$("#operaciones_atenciones_atendidos").hide();
            $("#operaciones_centro_costo").show();
            LimpiarSelect();
            $.messager.progress({
                title:'Por favor espera',
                msg:'Cargando datos...'
            });
            
            setTimeout(function(){
                $.messager.progress('close');
            },2000)
            $.ajax({
                url: '../../MVC_Controlador/BSC/bscC.php?acc=Ingreso_x_Centro_Costo',
                type: 'POST',
                dataType: 'json',
                data: {},
                success: function(data){
                    var categorias = data.CATEGORIAS;
                    var series = data.SERIES;
                    var maximo_y = data.MAX;
                    var minimo_y = 0;
                    var tipo = 'bar';
                    var titulo = 'INGRESO POR CENTRO DE COSTO';
                    var subtitulo = 'ACUMULADO ANUAL';
                    var text_y_title = 'Nuevos Soles Peruanos';
                    GenerarGrafico(tipo,titulo,subtitulo,categorias,minimo_y,maximo_y,0,series,text_y_title)
                }
            });
            
        }

        function Ingreso_x_Centro_Costo_x_IdCC(idcc,anio,mes){
            $.ajax({
                url: '../../MVC_Controlador/BSC/bscC.php?acc=Ingreso_x_Centro_Costo_x_IdCC',
                type: 'POST',
                dataType: 'json',
                data: {
                    anio:anio,
                    idcc:idcc,
                    mes: mes 
                },
                success: function(data){
                    var categorias = data.CATEGORIAS;
                    var series = data.DATA;
                    var maximo_y = data.MAX;
                    var minimo_y = 0;
                    var tipo = 'column';
                    var titulo = 'INGRESO POR CENTRO DE COSTO';
                    var subtitulo = data.CENTRO_COSTO+'  '+data.MES;
                    var text_y_title = 'Nuevos Soles Peruanos';
                    GenerarGrafico(tipo,titulo,subtitulo,categorias,minimo_y,maximo_y,0,series,text_y_title);
                }
            });
            
        }

        function LimpiarSelect(){
            $("#combo_meses_aa").combobox('clear');
            //$("#anio_aa").combobox('clear');
            $("#centro_costos").combobox('clear');
            $("#combo_meses_cc").combobox('clear');
            //$("#anio_cc").combobox('clear');
        }

        function Retornar(){
            if ($('#combo_meses_cc').combobox('getText').length < 1) {
                Ingreso_Centro_Costo();
            }
            else {
                $('#combo_meses_cc').combobox('clear');
                var anio = $('#anio_cc').combobox('getValue');
                var mes = $('#combo_meses_cc').combobox('getValue');  
                var id = $("#centro_costos").combobox('getValue');                                 
                Ingreso_x_Centro_Costo_x_IdCC(id,anio,mes);
            }
        }
        </script>
    </head>
    <body>
       <div class="easyui-layout" style="width:100%;height:100%;">  

            <!--DIV ENCABEZADO MENU-->      
            <div data-options="region:'west'" title="Indicadores" style="width:13%;padding:6%;">
                <a href="#"  class="easyui-linkbutton" id="" onclick="Ingreso_Centro_Costo()" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'left'">Ingreso por Centro de Costo&nbsp;&nbsp;</a>
            </div>
            
            
            <!--DIV GRAFICOS-->
            <div data-options="region:'center'" style="width:87%;">                
                <div class="easyui-layout" data-options="fit:true">                    
                    <div data-options="region:'north',split:true" style="height:10%;padding:1%;" title="Opciones">
                        
                        <div id="operaciones_centro_costo">
                            <label>Centro de Costos: </label>&nbsp;&nbsp;
                            <input id="centro_costos" class="easyui-combobox" data-options="
                                valueField: 'id',
                                textField: 'text',
                                url: '../../MVC_Controlador/BSC/bscC.php?acc=Centros_Costos_Listado',
                                onSelect: function(rec){
                                    $('#combo_meses_cc').combobox('clear');
                                    var anio = $('#anio_cc').combobox('getValue');
                                    var mes = $('#combo_meses_cc').combobox('getValue');                                    
                                    Ingreso_x_Centro_Costo_x_IdCC(rec.id,anio,mes);
                                }">&nbsp;&nbsp;
                            <label>Periodo Mes/A&#241;o</label>
                            <select class="easyui-combobox" id="combo_meses_cc" data-options="
                            onSelect: function(rec){
                                var anio = $('#anio_cc').combobox('getValue');
                                var idcc = $('#centro_costos').combobox('getValue');
                                var mes = $('#combo_meses_cc').combobox('getValue');
                                Ingreso_x_Centro_Costo_x_IdCC(idcc,anio,mes);
                            }">
                                <option value="0"> </option>
                                <option value="1">ENERO</option>
                                <option value="2">FEBRERO</option>
                                <option value="3">MARZO</option>
                                <option value="4">ABRIL</option>
                                <option value="5">MAYO</option>
                                <option value="6">JUNIO</option>
                                <option value="7">JULIO</option>
                                <option value="8">AGOSTO</option>
                                <option value="9">SETIEMBRE</option>
                                <option value="10">OCTUBRE</option>
                                <option value="11">NOVIEMBRE</option>
                                <option value="12">DICIEMBRE</option>
                            </select>&nbsp;&nbsp;
                            <select class="easyui-combobox" id="anio_cc">
                                <?php 
                                    for ($i=date('Y'); $i>=1950; $i--) {
                                        echo "<option value='$i'>$i</option>";
                                    } 
                                 ?>
                            </select>&nbsp;&nbsp;&nbsp;
                            <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="Retornar()">Retornar</a>
                        </div>
                    </div>
                    <div data-options="region:'center'" title="Graficos" style="height:90%;padding:7% 3% 3% 3%;">
                        <div id="graficos" style="width:100%;">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>