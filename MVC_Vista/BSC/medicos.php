<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
        <title></title>
        <!--CSS-->
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
            .calendario
            {
                border-collapse: collapse;border-spacing: 0;width:100%;height:95%;margin:0%;padding:0px;
            }

            .calendario caption, th
            {                
                background-color:#eee;
                border-top:0px solid #D3D3D3;
                border-bottom:0px solid #D3D3D3;
                text-align:center;
                padding: 4px;
                font-weight: bolder;
                color:#555;
            }

            .calendario td {
                vertical-align:middle;
                border:1px dotted #D3D3D3;
                text-align:center;
                padding:10px;
                background-color: #fff;
                color:#000000;
            }


        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.datagrid.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
        
        <!--OPERACIONES JS-->
        <script>
        $(document).ready(function() {
            //$("#tabla_mostrar_pacientes").hide();
            //$("#operaciones_medico").hide();
            $("#campos_ocultos_medicos").hide();
            //$("#box_calendario").hide();
            $("#datos_programacion_detallada").hide();
            //$("#tabla_mostrar_medicos").hide();
        });

        function Buscar_general(){
                
                if ($("#tipo_busqueda_paciente").combobox('getValue') == 0) {
                    $.messager.alert('Warning','Para generar el reporte debe seleccionar un tipo de busqueda');
                    //return false;
                }

                if (($("#variable_busqueda").val().length < 1) && ($("#tipo_busqueda_paciente").combobox('getValue') == 1)) {
                    $.messager.alert('Warning','Debe ingresar un numero de historia clinica');
                }

                if (($("#variable_busqueda").val().length < 1) && ($("#tipo_busqueda_paciente").combobox('getValue') == 2)) {
                    $.messager.alert('Warning','Debe ingresar un numero de DNI');
                }

                //estrayendo datos
                var tipo_busqueda = $("#tipo_busqueda_paciente").combobox('getValue');
                var var_busqueda  = $("#variable_busqueda").val();
                
                $.ajax({
                    url: '../../MVC_Controlador/BSC/bscC.php?acc=Mostrar_Reporte_Paciente_x_NumHis_C',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        var_des: var_busqueda,
                        tipo_b: tipo_busqueda
                    },
                    success: function(data)
                    {   
                        if (data.respuesta == "R_1") {
                            $.messager.alert('Paciente sin informacion','No se encontro datos para este paciente');
                            $("#tabla_mostrar_pacientes").html(' ');
                            //return false;
                        }
                        else {
                            $("#tabla_mostrar_pacientes").show();
                            $("#tabla_mostrar_pacientes").html(data.tabla_paciente);                        
                            $("#tabla_pacientes").datagrid();
                            return false;
                        }
                    }
                }); 
            }

            

            function MostrarCalendario()
            {   
                var mes = $("#combo_meses_cc").combobox('getValue');
                var idmedico = $("#combo_medicos_rp").combobox('getValue');
                var anio = $("#anio_aa").combobox('getValue');

                $.ajax({
                    url: '../../MVC_Controlador/BSC/bscC.php?acc=Programacion_Medica_Por_Mes',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        mes:mes,
                        anio:anio,
                        idmedico:idmedico
                    },
                    success:function(data)
                    {   
                        if (data.calendario == "R_1") {
                            $.messager.alert('Warning','No se encontro datos');
                            $("#cabecera_detalles").html(' ');
                            $("#tabla_detalles_cuerpo").html(' ');
                            $("#pie_detalle").html(' ');
                            $("#box_calendario_c").html(' ');
                        }
                        else
                        {   
                            
                            $("#box_calendario_c").html(data.calendario);
                            return false;
                        }
                        
                    }
                });
                
            }

            function MostrarContenido(fecha_calendario,idmedico,celda)
            {   
                //celda:last-child.style.backgroundColor="#01A9DB";
                //celda.style.backgroundColor="#A9E2F3";
                var fecha = fecha_calendario;
                var idmedico = idmedico;
                $("#datos_programacion_detallada").show();
                $.ajax({
                    url: '../../MVC_Controlador/BSC/bscC.php?acc=Programacion_Medica_detallada',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        fecha:fecha,
                        idmedico:idmedico
                    },
                    success:function(data)
                    {
                        $("#cabecera_detalles").html(data.CABECERA);
                        $("#tabla_detalles_cuerpo").html(data.TABLA);
                        $("#pie_detalle").html(data.PIE);
                        return false;
                    }
                });                              
            }

            
            

        </script>
    </head>
    <body>
        <div class="easyui-layout" style="width:100%;height:100%;">  
            

             <!--DIV GRAFICOS-->
            <div data-options="region:'center'" style="width:100%;">                
                <div class="easyui-layout" data-options="fit:true">                    
                    <div data-options="region:'north',split:true" style="height:10%;padding:1%;" title="Opciones">
                        
                        

                        <!--OPERACIONES PARA MEDICO-->
                        <div id="operaciones_medico">
                            <td>Seleccione un Medico</td>
                            <td>
                                <input id="combo_medicos_rp" style="width:15%;" class="easyui-combobox" name="" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/BSC/bscC.php?acc=Mostrar_Medicos_Reportes_Generales',
                                    onSelect: function(rec){   
                                        $('#combo_meses_cc').combobox('clear');                                     
                                    }">
                            </td>   
                            <td>
                                Seleccione un Mes: &nbsp;
                                <select class="easyui-combobox" id="combo_meses_cc" data-options="
                                onSelect: function(rec){
                                    MostrarCalendario();
                                }">
                                    
                                    <option value="1">ENERO</option>
                                    <option value="2">FEBRERO</option>
                                    <option value="3">MARZO</option>
                                    <option value="4">ABRIL</option>
                                    <option value="5">MAYO</option>
                                    <option value="6">JUNIO</option>
                                    <option value="7">JULIO</option>
                                    <option value="8">AGOSTO</option>
                                    <option value="9">SETIEMBRE</option>
                                    <option value="10">OCTUBRE</option>
                                    <option value="11">NOVIEMBRE</option>
                                    <option value="12" selected="selected">DICIEMBRE</option>
                                </select>
                            </td>
                            <td>

								
								<select   class="easyui-combobox" style="width:300px" id="anio_aa">
								<?php 
								$resultados=ListarAnioM();	
								$anio_actual=date("Y");
								/*$anio_actual=date("Y");*/						
								if($resultados!=NULL){
								for ($i=0; $i < count($resultados); $i++) {
								
								?>
								<option value="<?php echo $resultados[$i]["Anio"] ?>" <?php if($anio_actual==$resultados[$i]["Anio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["Anio"]; ?></option>
								<?php 
								}}
								?>
								</select>

                            </td> 
                        </div>

                    </div>
                    <div data-options="region:'center'" style="padding:2%;">
                        <div id="graficos" style="width:100%;">
                            <div id="tabla_mostrar_pacientes">
                                
                            </div>
                            <div class="easyui-layout" data-options="fit:true" id="tabla_mostrar_medicos" style="height:700px;">
                                <div data-options="region:'west'" style="width:50%;padding:3%;"  id="box_calendario_c"></div>
                                <div data-options="region:'center'" style="width:50%;padding:1%;padding-left:8%;">
                                    <div id="cabecera_detalles"></div>
                                    <div id="pie_detalle"></div>
                                    <div id="tabla_detalles_cuerpo"></div>
                                    
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>


            
        </div>
    </body>

</html>