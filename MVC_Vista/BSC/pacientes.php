<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
        <title></title>
        <!--CSS-->
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
            .calendario
            {
                border-collapse: collapse;border-spacing: 0;width:100%;height:95%;margin:0%;padding:0px;
            }

            .calendario caption, th
            {                
                background-color:#eee;
                border-top:0px solid #D3D3D3;
                border-bottom:0px solid #D3D3D3;
                text-align:center;
                padding: 4px;
                font-weight: bolder;
                color:#555;
            }

            .calendario td {
                vertical-align:middle;
                border:1px dotted #D3D3D3;
                text-align:center;
                padding:10px;
                background-color: #fff;
                color:#000000;
            }


        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.datagrid.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
        
        <!--OPERACIONES JS-->
        <script>
        $(document).ready(function() {
            $("#tabla_mostrar_pacientes").hide();
            $("#operaciones_medico").hide();
            $("#campos_ocultos_medicos").hide();
            //$("#box_calendario").hide();
            $("#datos_programacion_detallada").hide();
            $("#tabla_mostrar_medicos").hide();
        });

        function Buscar_general(){
                
                if ($("#tipo_busqueda_paciente").combobox('getValue') == 0) {
                    $.messager.alert('Warning','Para generar el reporte debe seleccionar un tipo de busqueda');
                    //return false;
                }

                if (($("#variable_busqueda").val().length < 1) && ($("#tipo_busqueda_paciente").combobox('getValue') == 1)) {
                    $.messager.alert('Warning','Debe ingresar un numero de historia clinica');
                }

                if (($("#variable_busqueda").val().length < 1) && ($("#tipo_busqueda_paciente").combobox('getValue') == 2)) {
                    $.messager.alert('Warning','Debe ingresar un numero de DNI');
                }

                //estrayendo datos
                var tipo_busqueda = $("#tipo_busqueda_paciente").combobox('getValue');
                var var_busqueda  = $("#variable_busqueda").val();
                
                $.ajax({
                    url: '../../MVC_Controlador/BSC/bscC.php?acc=Mostrar_Reporte_Paciente_x_NumHis_C',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        var_des: var_busqueda,
                        tipo_b: tipo_busqueda
                    },
                    success: function(data)
                    {   
                        if (data.respuesta == "R_1") {
                            $.messager.alert('Paciente sin informacion','No se encontro datos para este paciente');
                            $("#tabla_mostrar_pacientes").html(' ');
                            //return false;
                        }
                        else {
                            $("#tabla_mostrar_pacientes").show();
                            $("#tabla_mostrar_pacientes").html(data.tabla_paciente);                        
                            $("#tabla_pacientes").datagrid();
                            return false;
                        }
                    }
                }); 
            }

            

            
            

        </script>
    </head>
    <body>
        <div class="easyui-layout" style="width:100%;height:100%;">  
            

             <!--DIV GRAFICOS-->
            <div data-options="region:'center'" style="width:100%;">                
                <div class="easyui-layout" data-options="fit:true">                    
                    <div data-options="region:'north',split:true" style="height:10%;padding:1%;" title="Opciones">
                        
                        <!--OPERACIONES PARA PACIENTES-->                            
                        <div id="operaciones_pacientes">
                            <td>Seleccione Tipo de busqueda:</td>
                            <td>
                                <select name="" id="tipo_busqueda_paciente" class="easyui-combobox" >
                                    <option value="0"> Seleccione </option>
                                    <option value="1">Historia Clinica</option>
                                    <option value="2">Numero de DNI</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" class="easyui-textbox" data-options="prompt:'Ingrese...'"  id="variable_busqueda">
                            </td>
                            <td>
                                <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="Buscar_general()">Buscar</a>
                            </td>
                        </div>

                        
                    </div>
                    <div data-options="region:'center'" style="padding:2%;">
                        <div id="graficos" style="width:100%;">
                            <div id="tabla_mostrar_pacientes">
                                
                            </div>
                                              
                        </div>
                    </div>
                </div>
            </div>


            
        </div>
    </body>

</html>