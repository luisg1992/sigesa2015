<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
        <title></title>
        <!--CSS-->
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.datagrid.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
        
        <!--OPERACIONES JS-->
        <script>
        $(document).ready(function() {
            $("#tabla_cc").hide();
        });

        function Buscar_general()
        {
            var combo_centro_costos_rp  = $("#combo_centro_costos_rp").combobox('getValue');
            var combo_centro_costos_rp_text  = $("#combo_centro_costos_rp").combobox('getText');
            var fecha_inicio_cc_rp  = $("#fecha_inicio_cc_rp").combobox('getValue');
            var fecha_fin_cc_rp = $("#fecha_fin_cc_rp").combobox('getValue');
                    
            $("#tabla_cc").show();
            $("#tabla_cc_desgregado").datagrid({
                url:'../../MVC_Controlador/BSC/bscC.php?acc=Centro_Costo_x_Id_fi_ff_C&idcc='+combo_centro_costos_rp+'&fechainicio='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp,
                columns:[[
                    {field:'CODIGO',title:'CODIGO',resizable:true,align:'center'},
                    {field:'NOMBRES',title:'NOMBRES',resizable:true},
                    {field:'CANTIDAD',title:'CANTIDAD',resizable:true,align:'center'},
                    {field:'SUB TOTAL',title:'SUB TOTAL',resizable:true,align:'center'},
                    {field:'EXONERACIONES',title:'EXONERACIONES',resizable:true,align:'center'},
                    {field:'TOTAL',title:'TOTAL',resizable:true,align:'center'}
                ]],
                title: combo_centro_costos_rp_text
            })                    
        }

        function ExportarPDF()
        {
            var combo_centro_costos_rp  = $("#combo_centro_costos_rp").combobox('getValue');
            var combo_centro_costos_rp_text  = $("#combo_centro_costos_rp").combobox('getText');
            var fecha_inicio_cc_rp  = $("#fecha_inicio_cc_rp").combobox('getValue');
            var fecha_fin_cc_rp = $("#fecha_fin_cc_rp").combobox('getValue');

            window.open('../../MVC_Vista/BSC/Centro_Costo_pdf.php?idcc='+combo_centro_costos_rp+'&nombrecc='+combo_centro_costos_rp_text+'&fechaini='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp, '_blank');
            //window.location.href = '../../MVC_Vista/BSC/Centro_Costo_pdf.php?idcc='+combo_centro_costos_rp+'&nombrecc='+combo_centro_costos_rp_text+'&fechaini='+fecha_inicio_cc_rp+'&fechafin='+fecha_fin_cc_rp;
        }
        </script>
    </head>
    <body>
        <div class="easyui-layout" style="width:100%;height:100%;">  
            

             <!--DIV GRAFICOS-->
            <div data-options="region:'center'" style="width:100%;">                
                <div class="easyui-layout" data-options="fit:true">                    
                    <div data-options="region:'north',split:true" style="height:10%;padding:1%;" title="Opciones">
                        
                        

                        <!--OPERACIONES PARA MEDICO-->
                        <div id="operaciones_medico">
                            <td>Seleccione un Centro de Costo</td>
                            <td>
                                <input id="combo_centro_costos_rp" style="width:15%;"class="easyui-combobox" name="" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/BSC/bscC.php?acc=Centros_Costos_Listado',
                                    onSelect: function(rec){
                                        
                                    }">
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;De:</td>
                            <td>
                                <input class="easyui-datebox" data-options="sharedCalendar:'#cc'" id="fecha_inicio_cc_rp">
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;Hasta:</td>
                            <td>
                                <input class="easyui-datebox" data-options="sharedCalendar:'#cc'" id="fecha_fin_cc_rp">
                            </td> 
                            <td>
                                &nbsp;&nbsp;&nbsp;<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="Buscar_general()">Buscar</a>
                            </td>
                            <div id="cc" class="easyui-calendar"></div>
                        </div>

                    </div>
                    <div data-options="region:'center'" style="padding:2%;">
                        <div id="tabla_cc" style="margin-left:15%;">
                            <table class="easyui-datagrid" style="width:77%;height:700px;" data-options="singleSelect:true,collapsible:true" toolbar="#tb" id="tabla_cc_desgregado">
                        
                            </table>
                            <div id="tb">
                                <a href="#" class="easyui-linkbutton" plain="true" onclick="ExportarPDF()"><img src="../../MVC_Complemento/img/pdf.png" height="15" width="15" alt="">&nbsp;PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            
        </div>
    </body>

</html>