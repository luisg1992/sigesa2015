<?php 
ini_set('memory_limit', '1024M'); 

require('../../MVC_Controlador/BSC/bscC.php');
require('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

	#ESTILOS
	$estiloCentrado = array(
        'alignment' => array(
        	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    		)
    );

    $estiloEncabezados = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4682B4'),
            
    	),
        'font' => array(
	            'bold' => true,
	            'color' => array('rgb' => 'ffffff'),
	            'size' => 11,
	            'name' => 'Calibri',
    		)
    );

    $estiloFinal = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'BDBDBD'),
            
    	),
        'font' => array(
	            'bold' => true,
	            'color' => array('rgb' => 'ffffff'),
	            'size' => 11,
	            'name' => 'Calibri',
    		)
    );
    

	// Establecer propiedades
	$objPHPExcel->getProperties()
	->setCreator("Rodolfo Crisanto")
	->setLastModifiedBy("Rodolfo Crisanto")
	->setTitle("Emergencia")
	->setSubject("Emergencia")
	->setDescription("Descripcion de data.")
	->setKeywords("Excel Office 2007 openxml php")
	->setCategory("Pruebas de Excel");

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'DIAS');
	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($estiloCentrado);

	list($mes,$dia_inicio,$anio) = explode("/",$_REQUEST["fechaini"]);
	$fechainicio = $anio."/".$mes."/".$dia_inicio;

	list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
	$fechafin = $anio."/".$mes."/".$dia;

	$ind = 2;
	for ($x=1; $x <= $dia; $x++) { 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$ind, $anio."/".$mes."/".$x);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$ind)->applyFromArray($estiloCentrado);
		$ind++;
	}

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$ind, 'TOTAL');

	$data = EmergenciaxTopicosC($fechainicio,$fechafin);

	if (!$data) {
		echo 'sin datos';
		exit();
	}

	$contador = count($data);

	#CABECERAS
	for ($i=0; $i < $contador; $i++) { 
		$letra = chr($i+66);
		
		for ($j=0; $j <= $dia; $j++) { 
			$indice 	= $j + 1;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letra.$indice, $data[$i][$j]);
			$objPHPExcel->getActiveSheet()->getStyle($letra.$indice)->applyFromArray($estiloCentrado);
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension($letra)->setAutoSize(true);
		$ultimo = $ind - 1 ;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letra.$ind, '=SUM('.$letra.'2:'.$letra.$ultimo.')');
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$letra.'1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->setAutoFilter("A1:".$letra.'1'); 
	$objPHPExcel->getActiveSheet()->getStyle('A:'.$letra)->applyFromArray($estiloCentrado);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$ind.':'.$letra.$ind)->applyFromArray($estiloCentrado);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$ind.':'.$letra.$ind)->applyFromArray($estiloFinal);

	$objPHPExcel->getActiveSheet()->setTitle('TOPICOS EMERGENCIA');
	$objPHPExcel->setActiveSheetIndex(0);

	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="topicos_emergencia.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit();


 ?>