<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Abrir Solicitud</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>            
      
        
   <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:60px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				/*margin-left:10px;*/
            }
			.fitem2 label{
                display:inline-block;
                width:120px;	
				margin-left:10px;			
            }
			.fitem2 input {	
				width:140px;		 
			}
			
			#dlg-VerRecepcion{
                margin:0;
                padding:10px 30px;
            }
			
			#dlg-VerRecepcion{
                margin:0;
                padding:10px 30px;
            }
			#dlg-VerDespacho{
                margin:0;
                padding:10px 30px;
            }
			
			.l-btn-icon {
				margin-top: -11px !important;
			}
	</style>       
    
    
<script type="text/javascript" >		   
	   
	   /*function Cerrar(){ //Una vez cerrado NO se modifica
		   var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){   		
			
		   		if(rowp.Resumen=='<font color="#FF0000">FALTA</font>'){	
					$.messager.alert('Mensaje','Falta Registrar el Resumen del Paciente','warning');
					return 0;
						
				}else if(rowp.Estado=='1' && rowp.HayReservas=='0'){	
					$.messager.confirm('Mensaje', 'La Solicitud Nro: '+rowp.Codigo+' NO tiene Hemocomponentes. ¿Seguro de Cerrarla?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=CerrarDetSolicitud&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
						}
					});		
					
				}else if(rowp.Estado=='1' && rowp.HayReservas=='1'){	
					$.messager.alert('Mensaje','NO puede cerrar la Solicitud Nro: '+rowp.Codigo+' porque tiene Hemocomponentes Reservados','warning');
					return 0;		
					
				}else if(rowp.Estado=='2'){	//Reservado
					$.messager.alert('Mensaje','Falta Despachar el Detalle de la Solicitud Nro: '+rowp.Codigo,'warning');
					return 0;	
					
				}else if(rowp.Estado=='3'){	//Despachado
					$.messager.alert('Mensaje','Falta Facturar el Detalle de la Solicitud Nro: '+rowp.Codigo,'warning');
					return 0;	
					
				}else{
					$.messager.confirm('Mensaje', '¿Seguro de Cerrar el Detalle de la Solicitud Nro: '+rowp.Codigo+'?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=CerrarDetSolicitud&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
						}
					});	
						
				}
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Paciente','warning');
				return 0;			
			}
	   }*/
	   
	   function Historial(){
		   var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){	
		   		if(rowp.TieneHistorialDona=='SI'){
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerHistorialDonaciones&IdPaciente="+rowp.IdPaciente+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>";	
				}else{
					$.messager.alert('Mensaje','El paciente NO tiene Historial','warning');
					return 0;	
				}
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Paciente','warning');
				return 0;			
			}				
	   }	  
	   
	   function regresar(){
		   location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=OtrosRegistrosDonaciones&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>";
	   }	
	   
	   function Buscar(){
		   //var NroSolicitud=$('#NroSolicitudB').numberbox('getText');
		   $('#form1').submit();
	   }
	   
	   function GuardarAbrirSolicitud(){
		    var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){	
		   		if(rowp.Estado=='5'){
					$.messager.confirm('Mensaje', '¿Seguro de Abrir el Detalle de la Solicitud Nro: '+rowp.Codigo+'?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarAbrirSolicitud&EstadoAnterior="+rowp.EstadoAnterior+"&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>";
						}
					});	
					
				}else{
					$.messager.alert('Mensaje','Seleccione una Solicitud Cerrada','warning');
					return 0;	
				}
			
			}else{
				$.messager.alert('Mensaje','Busque y Seleccione una Solicitud Cerrada','warning');
				return 0;			
			}
			
		
		   
	   }	
	
    </script>
</head>
<body> 
    
    
	    <div id="tb1" style="padding:5px;height:auto">
        	<!--<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="RegistrarSolicitud()">Registrar Solicitud y/o Resumen</a>     	           	               	 
			<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="IngresarFenotipo()">Ingresar Fenotipo</a> -->                                       
			
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="Historial();">Historial</a>            
           <!-- <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="ImprimirSolicitud();">Imprimir Solicitud</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="ImprimirBancoSangre();">impresión por Banco de Sangre</a>	-->
            
           <!-- <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="Cerrar();">Cerrar</a>            
             <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="Eliminar();">Eliminar</a>-->
             
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="GuardarAbrirSolicitud();">Abrir Solicitud</a> 
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a>   
			
            <div>
				<form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=AbrirSolicitud&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>">		
					&nbsp;&nbsp;<strong>Nro Solicitud:</strong> &nbsp;
					<span class="fitem">
					<input name="NroSolicitudB" id="NroSolicitudB" class="easyui-numberbox" value="<?php echo $NroSolicitud ?>" style="width:105px" maxlength="10" data-options="required:true" />
					</span>                          
					&nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>
				
				</form>  
            </div>
            			
		</div>
        
                
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Busqueda de Solicitudes Cerradas" style="width:100%;height:90%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:20">         
                
		<thead>
			<tr>
                <th field="FechaSolicitud" width="55">Fec.Solicitud</th>
                <th field="UsuarioReg" width="40">UsuarioReg</th>				
                <th field="Codigo" width="50">Código</th>	
                <th field="Cantidad"  width="30">Cantidad</th>	
                <th field="TipoRequerimiento" width="45">Contingencia</th>
                <!--<th field="Transcurrido" width="50">Transcurrido</th>-->
                <th field="Resumen" width="30">Resumen</th>
                <th field="NroHistoria" width="50">NroHistoria</th>
                <th field="NombresPaciente" width="100">Paciente</th>
                <th field="Servicio" width="80">Servicio</th>
                <th field="CodigoCama" width="29">Cama</th>
                <th field="GrupoSanguineo" width="29">GS</th>
                <th field="SNCSTodos" width="85">Hemocomponentes</th>
                
                <th field="FechaCierre" width="55">Fec.Cierre</th>
                <th field="UsuarioRegCierre" width="40">UsuarioCerró</th>
			</tr>
		</thead>
	</table> 
     
   <script> 
   
   $(function(){  	
	   $('#NroSolicitudB').numberbox('textbox').attr('maxlength', $('#NroSolicitudB').attr("maxlength"));	 
   });   
	  
		function getData(){
			var rows = [];			
			
	<?php 		
	 
	 //$BuscarSNCSTodos = ListarSolicitudes_M(0, 'T');	
	 $BuscarSNCSTodos = BuscarSolicitudesCerradas_M($NroSolicitud);
	 
     $i=1; $j=0;											
	if($BuscarSNCSTodos!=NULL){
		$TipoHemTodos=''; $SNCSTodos=''; $IdMovDetalleTodos='';
		$Resumen=''; 
		foreach($BuscarSNCSTodos as $item)
		{			
			$Estado=$item["Estado"];
			$EstadoDescripcion=DescripcionEstadoSolicitud($Estado);	
			
			$IdDiagnostico1=$item["IdDiagnostico1"]; //Los que tienen al menos diagnostico1 tienen RESUMEN
			if(trim($IdDiagnostico1)==''){
				$Resumen='<font color="#FF0000">FALTA</font>';				
			}else{
				$Resumen='<font color="#FF9900">SI</font>';				
			}	
			
			//Cabecera			
			$Paciente=SIGESA_BSD_Buscarpaciente_M($item["IdPaciente"],'IdPaciente');	
			$NombresPaciente=$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"];
			
			$Servicio=$item["Servicio"];
			$CodigoCama=$item["CodigoCama"];
			$GrupoSanguineo=$item["GrupoSanguineo"];
			
			//$FechaSolicitud=$item["FechaSolicitud"];
			$FechaSolicitud=vfecha(substr($item["FechaSolicitud"],0,10));
			$HoraSolicitud=substr($item["FechaSolicitud"],11,5);
			
			$FechaRecepcion=$item["FechaRecepcion"];	
			if($FechaRecepcion!=NULL){
				$FechaRecepcion=vfecha(substr($item["FechaRecepcion"],0,10));
				$HoraRecepcion=substr($item["FechaRecepcion"],11,8);
				$UsuRecepcion=$item["UsuRecepcion"];
			}else{
				$FechaRecepcion='';
				$HoraRecepcion='';
				$UsuRecepcion='';
			}
			
			$FechaDespacho=$item["FechaDespacho"];	
			if($FechaDespacho!=NULL){
				$FechaDespacho=vfecha(substr($item["FechaDespacho"],0,10));
				$HoraDespacho=substr($item["FechaDespacho"],11,8);
				$UsuDespacho=$item["UsuDespacho"];
			}else{
				$FechaDespacho='';
				$HoraDespacho='';
				$UsuDespacho='';
			}
			
			//Datos Cierre
			$FechaCierre=$item["FechaCierre"];	
			if($FechaCierre!=NULL){
				$FechaCierre=vfecha(substr($item["FechaCierre"],0,10));
				$HoraCierre=substr($item["FechaCierre"],11,8);
				$UsuarioRegCierre=$item["UsuarioRegCierre"];
			}else{
				$FechaCierre='';
				$HoraCierre='';
				$UsuarioRegCierre='';
			}
			
			$Codigo=$item["NroSolicitud"].'-'.$item["CodigoComponente"];	
						
			//Obtener Cantidad
			if($item["Volumen"]>0){
				$Cantidad=$item["Cantidad"].'-'.$item["Volumen"].'ml';
			}else{
				$Cantidad=$item["Cantidad"];	
			}			
			
			//Obtener Estado ANTES QUE LA SOLICITUD SEA CERRADA
			$VerReservas=VerSolicitudSangreDetalleReservaM($item["IdDetSolicitudSangre"]);
			if($VerReservas!=NULL){
				$HayReservas=1;	
				$EstadoAnterior=4;//Facturado
				//
				$TipoHemTodos=''; $SNCSTodos=''; $IdMovDetalleTodos='';
				for ($k=0; $k < count($VerReservas); $k++) {					
					//$SNCSTodos=$SNCSTodos.$VerReservas[$k]["SNCS"].'|';
					
					if(count($VerReservas)==($k+1)){
						$SNCSTodos=$SNCSTodos.$VerReservas[$k]["SNCS"];	
					}else{
						$SNCSTodos=$SNCSTodos.$VerReservas[$k]["SNCS"].',';	
					}
				}
			}else{
				$HayReservas=0;	
				$EstadoAnterior=1;	
			}			
			//FIN Obtener Estado ANTES QUE LA SOLICITUD SEA CERRADA
			
			$HistorialDonaciones=HistorialDonacionesM($item["IdPaciente"]); 
			if($HistorialDonaciones!=NULL){	
				$TieneHistorialDona='SI';
			}else{	
				$TieneHistorialDona='NO';	
			}	
			
			$listado=VerFacturacionesBancoDeSangreM($item['IdDetSolicitudSangre']);
			$CantidadFacturados=count($listado);//CANTIDAD REGISTRO
			if($listado!=''){				
				if($item["Estado"]=='4'){//if($CantidadFacturados>=$Cantidad){ //> en el caso ST
					$TieneFac='Todos';
				}else{	
					$TieneFac='PendAlgunos';	
				}
			}else{	
				$TieneFac='No';	
			}				
	
			?>   				
			
				rows.push({
					FechaSolicitud: '<?php echo $FechaSolicitud.' '.$HoraSolicitud;?>',					
					UsuarioReg: '<?php echo $item["UsuarioReg"]; ?>',											
					Codigo:'<?php echo  $Codigo; ?>',	
					Cantidad:'<?php echo  $Cantidad; ?>',
					TipoRequerimiento: '<?php echo $item["TipoRequerimiento"]; ?>',	
					Resumen:'<?php echo  $Resumen; ?>',	
					NombresPaciente:'<?php echo  $NombresPaciente; ?>',
					Servicio:'<?php echo  $Servicio; ?>',	
					CodigoCama:'<?php echo  $CodigoCama; ?>',
					IdGrupoSanguineo: '<?php echo $item["IdGrupoSanguineo"]; ?>',				
					GrupoSanguineo:'<?php echo  $GrupoSanguineo; ?>',
					NroHistoria:'<?php echo  $item["NroHistoria"]; ?>',
					IdPaciente:'<?php echo  $item["IdPaciente"]; ?>',					
					
					FechaRecepcion:'<?php echo $FechaRecepcion; ?>',
					HoraRecepcion:'<?php echo $HoraRecepcion; ?>',
					UsuRecepcion:'<?php echo $UsuRecepcion; ?>',					
					IdSolicitudSangre:'<?php echo  $item["IdSolicitudSangre"]; ?>',
					NroSolicitud:'<?php echo  $item["NroSolicitud"]; ?>',
					Fenotipo:'<?php echo  $item["Fenotipo"]; ?>',
					CodigoComponente:'<?php echo  $item["CodigoComponente"]; ?>',
					DeteccionCI:'<?php echo  $item["DeteccionCI"]; ?>',
					CoombsDirecto:'<?php echo  $item["CoombsDirecto"]; ?>',
					IdDetSolicitudSangre:'<?php echo  $item["IdDetSolicitudSangre"]; ?>',
					
					FechaDespacho:'<?php echo  $FechaDespacho; ?>',
					HoraDespacho:'<?php echo  $HoraDespacho; ?>',
					UsuDespacho:'<?php echo  $UsuDespacho; ?>',
					IdCuentaAtencion:'<?php echo  $item["IdCuentaAtencion"]; ?>',
					Estado:'<?php echo  $item["Estado"]; ?>',
					HayReservas:'<?php echo  $HayReservas; ?>',					
					
					SNCSTodos:'<?php echo $SNCSTodos; ?>',
					
					TieneHistorialDona:'<?php echo $TieneHistorialDona; ?>',
					TieneFac:'<?php echo $TieneFac; ?>',
					
					EstadoCab:'<?php echo  $item["EstadoCab"]; ?>',					
					
					FechaCierre: '<?php echo $FechaCierre.' '.$HoraCierre;?>',	
					UsuarioRegCierre:'<?php echo $UsuarioRegCierre; ?>',
					EstadoAnterior:'<?php echo $EstadoAnterior; ?>',
										
				});
			
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
				
		
		function ObtenerFechaActual(){
			f=new Date();
			var y = parseInt(f.getFullYear());
			var m = parseInt(f.getMonth()+1);
			var d = parseInt(f.getDate());
			if(m<10){
				m='0'+m;
			}
			if(d<10){
				d='0'+d;
			}						
			FechaActual=d+'/'+m+'/'+y;
				
			return FechaActual;
		}
			
		$(function(){
		var dgFrac = $('#dg').datagrid({
			remoteFilter: false,
			pagination: true,
			pageSize: 20,
			//pageList: [10,20,50,100]
		});
		
		dgFrac.datagrid('enableFilter');
		//FilterFechaActualSolicitud();												
		dgFrac.datagrid('loadData', getData());	
		
		function FilterFechaActualSolicitud(){
			FechaActual=ObtenerFechaActual();
								
			dgFrac.datagrid('addFilterRule', {
				field: 'FechaSolicitud',
				type:'datebox',
				op: 'contains',
				value: FechaActual
			});
		}//fin function FilterFechaActualSolicitud
		
		});
		
		

		</script> 
     

     
        
  

     
</body>
</html>