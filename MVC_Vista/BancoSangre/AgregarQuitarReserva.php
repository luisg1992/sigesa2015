<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Reserva Hemocomponentes</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <!--<script type="text/javascript" src="../../MVC_Vista/BancoSangre/FuncionesAE.js"></script> -->
        <script type="text/javascript">
			//////3. FILTRAR COMBOGRID Apellidos y Nombres PACIENTE   		
			$(function(){	 //Filtrar Paciente	
				$('#ApellidosNombresBusRec').combogrid({
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarPaciente',
					idField:'NroHistoriaClinica', //NombresPaciente
					textField:'NombresPaciente',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = buscarPacienteNombresApellidos(); },	//esta funcion llama cuando seleccionas el paciente					
					columns:[[							
						{field:'NombresPaciente',title:'Apellidos y Nombres',width:80}						
					]]
				});	//FIN 						
			});		
		
			//FUNCION QUE PERMITE LA BUSQUEDA POR APELLIDOS Y NOMBRES DEL Paciente	
			function buscarPacienteNombresApellidos(){ //esta funcion llama cuando seleccionas el paciente	   	
					var ApellidosNombresBusRec= $('#ApellidosNombresBusRec').combogrid('getValue');							
					$.ajax({
					url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BSD_BuscarPaciente",					
					type: "GET",
					dataType: "JSON",
					data: {						
						valor: ApellidosNombresBusRec,
						TipoBusqueda:'NroHistoriaClinica'	//ApellidosNombrespaciente						
					},							
						success: function (data) {
						
						document.getElementById('IdPaciente').value=data.IdPaciente;
						$('#NroHistoria').numberbox('setValue', data.NroHistoria);	
						$('#NroDocumento').numberbox('setValue', data.NroDocumento);	
												
						document.getElementById('IdGrupoSanguineoRecANT').value=data.IdGrupoSanguineo;
						$('#IdGrupoSanguineoRec').combobox('setValue', data.IdGrupoSanguineo);	
						cambiarGrupoSanguineoRec();						
						/*$('#IdServicioHospital').combobox('setValue', data.IdServicio);	
						$('#NroCama').numberbox('setValue', data.numerocama);	
						$('#Medico').textbox('setValue', data.Medico);	
						$('#CodigoCIE10Diagnostico').textbox('setValue', data.diagnostico);	*/							
						}			
					}).done(function (data) {
						console.log(data);
						//alert(data);						
						if(data=="") { //if(!data.success){				
							$.messager.alert('SIGESA','Los Apellidos y Nombres no pertenecen a ningun Paciente' ,'info'); //NO HAY DATOS		
						}
						//ObtenergsPaciente(data.IdPaciente);
					});	//fin done  	 
			}//FIN function			
			
			
			function cambiarGrupoSanguineoRec(){
				GrupoSanguineoRec=$('#IdGrupoSanguineoRec').combobox('getText');	
				document.getElementById('GrupoSanguineoRec').value=GrupoSanguineoRec;
			}
		</script>
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:10px;
            }			
            .fitem label{
                display:inline-block;
                width:80px;
							
            }
            .fitem input{
				display:inline-block;
                width:100px;	
				margin-left:10px;			
            }					
			
	</style> 
    
<script type="text/javascript" >			
		
		$.extend($("#FechaReserva").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaReserva").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});					
		
		function regresar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=StockSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}	
		
		function Buscar(){
			document.getElementById("form1").submit();
		}			
	
    </script>
</head>
<body>

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->
	<div style="margin:0px 0;"></div>
	 <form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=AgregarQuitarReserva&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">  
            	<!--<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'" onClick="Buscar()">Volver a Cargar</a> -->               	 
		        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="AgregarReserva()">Agregar Reserva</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="QuitarReserva()" >Quitar Reserva</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="VerReserva()" >Ver Reserva</a>                             
                <!--<!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="Imprimir();">Imprimir</a>	
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="Ayuda();">Ayuda</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a> 
			</div>
            
              <div>
               <strong>Busqueda por Hemocomponente:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
                   <select name="TipoHemB" id="TipoHemB" class="easyui-combobox" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" style="width:60px;">
                        <option value="T">Todos</option>
                        <option value="PG" <?php if($TipoHem=='PG'){?> selected <?php } ?> >PG </option>
                        <option value="PFC" <?php if($TipoHem=='PFC'){?> selected <?php } ?> >PFC </option>
                        <option value="PQ" <?php if($TipoHem=='PQ'){?> selected <?php } ?>>PQ </option>
                        <option value="CRIO" <?php if($TipoHem=='CRIO'){?> selected <?php } ?>>CRIO </option>
                    </select>-<input id="SNCSB"  name="SNCSB" type="text" class="easyui-textbox" maxlength="7" value="<?php echo $SNCS ?>" data-options="prompt:'SNCS',
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = document.form1.submit(); }" />        
                                         
                      <label for="BuscarSNCS" onClick="CambiarBuscar();" >Buscar por SNCS</label>
                      <input type="radio" name="EstadoB" id="BuscarSNCS" value="V" <?php if(trim($EstadoB)=="V"){?> checked <?php } ?> onClick="CambiarBuscar();" /> &nbsp;&nbsp;&nbsp;
                       <label for="BuscarReserva" >Todos en Reserva</label>
                      <input type="radio" name="EstadoB" id="BuscarReserva" value="2" <?php if(trim($EstadoB)=="2"){?> checked <?php } ?> /> &nbsp;&nbsp;&nbsp; 
                          
                    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>
                </div>
            			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Reserva de Hemocomponentes" style="width:1350px;height:450px" data-options="
				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	<th field="NroDonacion" width="80">N°Donacion</th>
				<th field="TipoHem"  width="100">Hemocomponente</th>				
                <th field="NuevoSNCS"  width="90">SNCS</th>	
				<th field="GrupoSanguineo"  width="60">GS</th>
                <th field="VolumenRestante"  width="70">Volumen</th>
				<th field="NroTabuladora" width="80">tubuladura</th>
                <th field="FechaExtraccion" width="100">F. Extracción</th>
                <th field="FechaVencimiento" width="100">F. Vencimiento</th>
                <th field="Tiempo" width="150">Tiempo</th>
                <th field="Paciente" width="220">Paciente</th>
                <th field="EstadoDescripcion" width="120">Estado</th>
                <th field="MotivoAgregarReserva2" width="120">Motivo Reserva</th>
                <!--<th field="VerDatosCustodia" width="100">Ver Reserva</th>-->
			</tr>
		</thead>
	</table>
     </form>
    

<!--FORMULARIO CUSTODIA-->
 <div id="dlg-CabHemocomponente" class="easyui-dialog" style="width:760px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmCabHemocomponente" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarAgregarReserva&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">
                    <input type="hidden" name="MovNumero" id="MovNumero" />
                    <input type="hidden" name="IdMovDetalle" id="IdMovDetalle" />
                    <label>Tipo Hemoc:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" style="width:100px" readonly>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>                          
                    <label>SNCS:</label>
                    <input id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" data-options="prompt:'SNCS'" readonly />                     
                    <label>Paciente:</label>                    
                    <input class="easyui-textbox" style="width:200px" id="ApellidosNombresBusRec" name="ApellidosNombresBusRec"  data-options="prompt:'Apellidos y Nombres',required:true">                                  
              </div>   
              
              <div class="fitem">   
              		<input type="hidden" name="IdPaciente" id="IdPaciente" />                 
                    <label>Nro Historia:</label>
                   <input class="easyui-numberbox" style="width:100px" id="NroHistoria" name="NroHistoria" data-options="prompt:'Nro Historia',required:true" readonly />  
                    <label>Nro Doc:</label>
                    <input class="easyui-numberbox" style="width:105px" id="NroDocumento" name="NroDocumento" data-options="prompt:'Nro Documento'" readonly />               
                    <label>Grupo Sanguineo:</label>                    
                   	<select class="easyui-combobox" name="IdGrupoSanguineoRec" id="IdGrupoSanguineoRec"  style="width: 105px" data-options="prompt:'Seleccione',required:true,
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarGrupoSanguineoRec(); }">
                          <option value="0"></option>
                          <?php
                                              $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
                                               if($listarSexo != NULL) { 
                                                 foreach($listarSexo as $item){?>
                          <option value="<?php echo $item["IdGrupoSanguineo"]?>" ><?php echo $item["Descripcion"]?></option>
                          <?php } } ?>
                        </select>  
                   <input type="hidden" name="GrupoSanguineoRec" id="GrupoSanguineoRec" />   
                   <input type="hidden" name="IdGrupoSanguineoRecANT" id="IdGrupoSanguineoRecANT" />                                                  
              </div>  
              
              <div class="fitem">
              		 <label>Responsable:</label>
                    <Select style="width:200px" class="easyui-combobox" id="UsuReserva" name="UsuReserva" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>                    
                    <label>Fec.Reserva:</label>                   
                    <input name="FechaReserva" id="FechaReserva" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />            		<input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraReserva" name="HoraReserva" data-options="showSeconds:true,required:true" style="width:85px" />
                          
              </div>  
               <div class="fitem">
               <label>Motivo Reserva:</label> 
               <input style="width:480px" class="easyui-textbox" name="MotivoAgregarReserva" id="MotivoAgregarReserva" />
               </div>        
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveReserva();" style="width:90px">Reserva</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponente').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO CUSTODIA-->

 <!--FORMULARIO REGISTRAR RESERVA-->
 <div id="dlg-CabHemocomponenteCrio" class="easyui-dialog" style="width:760px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmIngresarResultado" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarQuitarReserva&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">              
                	<input type="hidden" name="MovNumero2" id="MovNumero2" /> 
                    <input type="hidden" name="IdMovDetalle2" id="IdMovDetalle2" />
                    <label>Tipo Hemoc:</label>                    
                    <select name="TipoHem2" id="TipoHem2" class="easyui-combobox" readonly >
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                           
                    <label>SNCS:</label>                    
                    <input id="SNCS2"  name="SNCS2" type="text" class="easyui-textbox" maxlength="10" data-options="prompt:'SNCS'" readonly />
                   
                   <label>Paciente:</label>
                   <input class="easyui-textbox" style="width:200px" id="Paciente" name="Paciente"  disabled>
            	</div>   
              
              <div class="fitem">
              		 <label>Responsable:</label>
                     <Select style="width:200px" class="easyui-combobox" id="UsuQuitarReserva" name="UsuQuitarReserva" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select> 
                    <label>Fec.Quitar Reserva:</label>                   
                    <input name="FecQuitarReserva" id="FecQuitarReserva" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />            		
                    <input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraQuitarReserva" name="HoraQuitarReserva" data-options="showSeconds:true,required:true" style="width:85px" />    
                   
              </div> 
              
              <div class="fitem2">
              		<label>Estado:</label>                   
                    <label for="stock">Disponible</label>
                    <input type="radio" name="Estado2" id="stock" value="1" checked /> &nbsp;&nbsp;&nbsp; 
                    <label>Motivo:</label> 
                    <input style="width:360px" class="easyui-textbox" name="MotivoQuitarReserva" id="MotivoQuitarReserva" />  
                   
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-remove" onclick="saveQuitarReserva();" style="width:150px">Quitar Reserva</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponenteCrio').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO REGISTRAR RESERVA--> 
  
  
<!--FORMULARIO VER CUSTODIA-->
 <div id="dlg-VerReserva" class="easyui-dialog" style="width:760px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmVerCustodia" method="post" enctype="multipart/form-data" action="#">
            	<div class="fitem">
                    <label>Tipo Hemoc:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" disabled>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                           
                    <label>SNCS:</label> 
                    <input id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" data-options="prompt:'SNCS'" disabled />                     
                    <label>Paciente:</label>                    
                  <Select style="width:200px" class="easyui-combobox" id="Paciente" name="Paciente" disabled>
                    <option value=""></option>
                      <?php
                                          $ListarMotivoElimina=ListarMotivoElimina('CUSTO');
                                           if($ListarMotivoElimina != NULL) { 
                                             foreach($ListarMotivoElimina as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select>                                    
              </div>   
              
              <div class="fitem">
              		 <label>Resp.Reserva:</label>
                    <Select style="width:200px" class="easyui-combobox" id="UsuQuitarReserva2" name="UsuQuitarReserva2" readonly >
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>                    
                    <label>Fec.Reserva:</label>                   
                    <input name="FechaReserva2" id="FechaReserva2" class="easyui-datebox" style="width:105px" readonly />            		
                    <input class="easyui-timespinner" id="HoraReserva2" name="HoraReserva2" style="width:85px" data-options="showSeconds:true" readonly />                         
              </div> 
              
              <div class="fitem">
               <label>Motivo Reserva:</label> 
               <input style="width:480px" class="easyui-textbox" name="MotivoAgregarReserva2" id="MotivoAgregarReserva2" readonly />
               </div>
              
              <div class="fitem">
              		 <label>Resp.Quitar Reserva:</label>
                    <Select style="width:200px" class="easyui-combobox" id="UsuQuitarReserva2" name="UsuQuitarReserva2" readonly >
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select> 
                    <label>Fec.Quitar Reserva:</label>                   
                    <input name="FecQuitarReserva2" id="FecQuitarReserva2" class="easyui-datebox" style="width:105px" readonly />            		
                    <input class="easyui-timespinner" id="HoraQuitarReserva2" name="HoraQuitarReserva2" data-options="showSeconds:true" style="width:85px" readonly />    
                   
              </div> 
              
              <div class="fitem2">
              		<label>Estado:</label>
                    <input type="text" class="easyui-textbox" name="EstadoDescripcion2" id="EstadoDescripcion2" readonly />   
                    <label>Motivo:</label> 
                    <input style="width:312px" class="easyui-textbox" name="MotivoQuitarReserva2" id="MotivoQuitarReserva2" readonly />                 
                    
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">       
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-VerReserva').dialog('close')" style="width:90px">Cerrar</a>
	  </div>

<!--FIN FORMULARIO VER CUSTODIA-->
    
   <script> 
	$(function(){	
		$('#IdGrupoSanguineoRec').combobox({	
		editable: false,
		required: true								
		});	
	});
   
   function CambiarBuscar(){
	   if(document.getElementById('BuscarSNCS').checked==true){	 
			$.messager.alert({
				title: 'Mensaje',
				msg: 'Ingrese un Nro de SNCS a Buscar',
				icon:'warning',
				fn: function(){
					$('#SNCSB').next().find('input').focus();
				}
			});
			$('#SNCSB').next().find('input').focus();
			return 0;	
	   }	   
	   /*if(document.getElementById('BuscarReserva').checked==true){
		  $('#SNCSB').textbox('setValue', '');
		  return 0;			
	   }*/
   }
   
   function VerReserva(){
	   var rowp = $('#dg').datagrid('getSelected');
			if (rowp){	
			   if(rowp.Estado==2){//EN RESERVA
				  $('#dlg-VerReserva').dialog('open').dialog('setTitle','Ver Datos Hemocomponente en Reserva');						
				  $('#fmVerCustodia').form('load',rowp);
				   
			   }else if(rowp.MotivoQuitarReserva2!=''){//ESTUVO EN CUSTODIA
				  $('#dlg-VerReserva').dialog('open').dialog('setTitle','Ver Datos Hemocomponente Estuvo en Reserva');						
				  $('#fmVerCustodia').form('load',rowp);
				   
			   }else{					
					$.messager.alert('Mensaje','El Hemocomponente NO está en RESERVA','warning');
					return 0;
				}			
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente','warning');
				return 0;			
			}
	   
  }
   
   function AgregarReserva(){						
			
			var rowp = $('#dg').datagrid('getSelected');
			if (rowp){	
			   if(rowp.Estado==0){
				   $.messager.alert('Mensaje','La unidad se encuentra eliminada','warning');
				   return 0;
				   
			   /*}else if(rowp.Estado==3){
				   $.messager.alert('Mensaje','La unidad se encuentra reservada','warning');
				   return 0;*/
				   
			   }else if(rowp.Estado==2){
				   $.messager.alert('Mensaje','La unidad YA se encuentra en Reserva','warning');
				   return 0;
				   
			   }else if(rowp.Estado==5 || rowp.Estado==6){
				   $.messager.alert('Mensaje','La unidad YA ha sido donado','warning');
				   return 0;
				   
			   }else if(parseInt(rowp.VolumenFraccion)>0){
				  $.messager.confirm('Mensaje', 'La unidad se encuentra Fraccionada, ¿Seguro de poner en Reserva la Fracción Restante?', function(r){
						if (r){
							$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Reserva Hemocomponente');						
							$('#fmCabHemocomponente').form('load',rowp);
						}
					});
			   }else{					
					$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Reserva Hemocomponente');						
					$('#fmCabHemocomponente').form('load',rowp);
				}					
				url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarAgregarReserva&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>";
				
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente','warning');
				return 0;			
			}
		} 
		
	function saveReserva(){			
			
			var IdPaciente=document.getElementById('IdPaciente').value;
			 if(IdPaciente==""){				
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Busque un Paciente',
					icon:'warning',
					fn: function(){
						$('#ApellidosNombresBusRec').next().find('input').focus();
					}
				});
				$('#ApellidosNombresBusRec').next().find('input').focus();
				return 0;				
			}
			
			var IdGrupoSanguineoRec=$('#IdGrupoSanguineoRec').combobox('getValue');
			if(IdGrupoSanguineoRec.trim()=="" || IdGrupoSanguineoRec.trim()=="0"){
				$.messager.alert('Mensaje','Seleccione el Grupo Sanguineo del Paciente','info');
				$('#IdGrupoSanguineoRec').next().find('input').focus();
				return 0;			
			}	
			
			var UsuReserva=$('#UsuReserva').combobox('getText');
			 if(UsuReserva.trim()==""){
				$.messager.alert('Mensaje','Seleccione el Usuario que Reserva','info');
				$('#UsuReserva').next().find('input').focus();
				return 0;			
			}
			
			var FechaReserva=$('#FechaReserva').datebox('getText');
			 if(FechaReserva.trim()==""){
				$.messager.alert('Mensaje','Seleccione la fecha de Reserva','info');
				$('#FechaReserva').next().find('input').focus();
				return 0;			
			}
			
			var Motivo=$('#MotivoAgregarReserva').datebox('getText');
			 if(Motivo.trim()==""){
				$.messager.alert('Mensaje','Ingrese el Motivo de Reserva','info');
				$('#MotivoAgregarReserva').next().find('input').focus();
				return 0;			
			}
									
			//document.getElementById("fmCabHemocomponente").submit();
			var TipoHem=$('#TipoHem').combobox('getValue');			
			var SNCS=$('#SNCS').textbox('getValue');
			
			var IdGrupoSanguineoRecANT=document.getElementById('IdGrupoSanguineoRecANT').value;
			var mensaje2='';
			if(IdGrupoSanguineoRecANT=='0'){
				var mensaje2=' y Guardar el Grupo Sanguineo del Paciente ';
			}else if(IdGrupoSanguineoRecANT!=IdGrupoSanguineoRec){
				var mensaje2=' y Cambiar el Grupo Sanguineo del Paciente ';
			}
			
			
			$.messager.confirm('Mensaje', '¿Seguro de poner en Reserva el Hemocomponente '+ TipoHem +'-'+SNCS+mensaje2+'?', function(r){
				if (r){
					document.getElementById("fmCabHemocomponente").submit();
				}
			});	
			
		}	
	
	 function QuitarReserva(){				
			var rowp = $('#dg').datagrid('getSelected');			
			if (rowp){				
				if(rowp.Estado!='2'){
					$.messager.alert('Mensaje','La unidad debe estar en Reserva','warning');					
					return 0;
					
				}else if((rowp.MotivoAgregarReserva2).trim()=='POR SOLICITUD'){
					$.messager.alert('Mensaje','Debe quitar la Reserva en la Lista de Solicitudes','warning');					
					return 0;
					
				}else{
					$('#dlg-CabHemocomponenteCrio').dialog('open').dialog('setTitle','Quitar Reserva');						
					$('#fmIngresarResultado').form('load',rowp);						
				}
				url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarQuitarReserva&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>";
				
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente en Reserva','warning');
				return 0;			
			}
		}
		
		function saveQuitarReserva(){	
		
			var UsuMotivo=$('#UsuQuitarReserva').combobox('getValue');
			if(UsuMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione el Responsable que Elimina la Reserva','info');
				$('#UsuQuitarReserva').next().find('input').focus();
				return 0;			
			}	
			
			var FecQuitarReserva=$('#FecQuitarReserva').datebox('getText');
			 if(FecQuitarReserva.trim()==""){
				$.messager.alert('Mensaje','Seleccione una Fecha de Eliminar la Reserva','info');
				$('#FecQuitarReserva').next().find('input').focus();
				return 0;			
			}
			
			var MotivoQuitarReserva=$('#MotivoQuitarReserva').textbox('getText');
			if(MotivoQuitarReserva.trim()==""){
				$.messager.alert('Mensaje','Ingrese el Motivo de Eliminar la Reserva','info');
				$('#MotivoQuitarReserva').next().find('input').focus();
				return 0;			
			}	
			
			//document.getElementById("fmCabHemocomponente").submit();
			var TipoHem=$('#TipoHem2').combobox('getValue');			
			var SNCS=$('#SNCS2').textbox('getValue');
			
			mensaje='¿Seguro de Quitar la Reserva del hemocomponente: '+ TipoHem +'-'+SNCS+' y Enviar a stock?';			
			
			$.messager.confirm('Mensaje', mensaje, function(r){
				if (r){
					document.getElementById("fmIngresarResultado").submit();
				}
			});	
			
		} 	 
	
	$(function(){  	
	  $('#SNCSB').numberbox('textbox').attr('maxlength', $('#SNCSB').attr("maxlength"));
	   
	});	
	
		function getData(){
			var rows = [];			
			
	<?php 		
	 
	 $BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);
	 
     $i=1; $j=0;											
	if($BuscarSNCSTodos!=NULL){
		foreach($BuscarSNCSTodos as $item)
		{
			$MovTipo=$item["MovTipo"];			
			if($MovTipo=='F'){
				$j=$j+1;
				$NuevoSNCS=$item["SNCS"].'-F'.$j;
			}else{
				$NuevoSNCS=$item["SNCS"];
			}
			
			//Funcion en Funciones.php
			$EstadoDescripcion=DescripcionEstadoHemocomponente($item["Estado"]);			
			
			//Calcular Tiempo Restante de vencimiento
			$FechaVencimiento=$item["FechaVencimiento"];	
			
			//$fecharec=$item['FechaMovi'];			
			$datetime1 = new DateTime($FechaVencimiento);
			
			$fechaactual=date('Y-m-d H:i:s');
			$datetime2 = new DateTime($fechaactual);
			$interval = date_diff($datetime2, $datetime1);
			//$interval->format('%R%a días');
			if($interval->format('%R')=='-'){ //NEGATIVO
				$Tiempo='<font color="#FF0000">Vencido</font>';
			}else if($interval->format('%a')=='0'){ //SIN DIAS (SOLO HORAS Y MINUTOS)
				$Tiempo='Queda '.$interval->format('<font color="#FF0000">%R%h</font>Hor,<font color="#FF0000">%i</font>Min');
			}else{
				$Tiempo='Queda '.$interval->format('<font color="#FF0000">%R%a</font>Días');//%R
			}				
					
			$FechaReserva=$item["FechaReserva"];
			if($FechaReserva!=NULL){
				$FechaReserva=vfecha(substr($item["FechaReserva"],0,10));
				$HoraReserva=substr($item["FechaReserva"],11,8);
				$UsuReserva=$item["UsuReserva"];
			}else{
				$FechaReserva='';
				$HoraReserva='';
				$UsuReserva='';
			}	
			
			$FecQuitarReserva=$item["FecQuitarReserva"];
			if($FecQuitarReserva!=NULL){
				$FecQuitarReserva=vfecha(substr($item["FecQuitarReserva"],0,10));
				$HoraQuitarReserva=substr($item["FecQuitarReserva"],11,8);
				$UsuQuitarReserva=$item["UsuQuitarReserva"];
			}else{
				$FecQuitarReserva='';
				$HoraQuitarReserva='';
				$UsuQuitarReserva='';
			}			
			
			$IdPacienteReserva=$item["IdPacienteReserva"];
			$datosPaciente=SIGESA_BSD_Buscarpaciente_M($IdPacienteReserva,'IdPaciente');
			
			if($datosPaciente!=""){
				$Paciente=$datosPaciente[0]["ApellidoPaterno"].' '.$datosPaciente[0]["ApellidoMaterno"].' '.$datosPaciente[0]["PrimerNombre"];		
			}else{
				$Paciente='NO RESERVADO';
			}
			
						
			if($item["MotivoAgregarReserva"]!=''){				
				$MotivoAgregarReserva=$item["MotivoAgregarReserva"];						
			}else{
				if($item["Estado"]=='2'){//RESERVADO				
					$MotivoAgregarReserva='RESERVADO EN LA RECEPCIÓN';
				}else{
					$MotivoAgregarReserva='';
				}	
			}		
				
						
			?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TipoHem: '<?php echo $item["TipoHem"];?>',
					TipoHem2: '<?php echo $item["TipoHem"];?>',
					NroDonacion: '<?php echo $item["NroDonacion"]; ?>',											
					SNCS:'<?php echo  $item["SNCS"]; ?>',					
					GrupoSanguineo:'<?php echo  trim($item["GrupoSanguineo"]); ?>',
					
					VolumenTotRecol:'<?php echo  $item["VolumenTotRecol"]; ?>',
					VolumenFraccion:'<?php echo  $item["VolumenFraccion"]; ?>',		
					VolumenRestante:'<?php echo  $item["VolumenRestante"]; ?>',	
									
					NroTabuladora:'<?php echo  $item["NroTabuladora"]; ?>',
					FechaExtraccion:'<?php echo  vfecha(substr($item["FechaExtraccion"],0,10)); ?>',				
					FechaVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)); ?>',
					IdMovDetalle:'<?php echo  $item["IdMovDetalle"]; ?>',
					IdMovDetalle2:'<?php echo  $item["IdMovDetalle"]; ?>',
					MovNumero:'<?php echo  $item["MovNumero"]; ?>',
					MovNumero2:'<?php echo  $item["MovNumero"]; ?>',
					
					SNCS2:'<?php echo $item["SNCS"]; ?>',
					NuevoSNCS:'<?php echo $NuevoSNCS; ?>',
					MovTipo:'<?php echo $item["MovTipo"]; ?>',
					Estado:'<?php echo $item["Estado"]; ?>',
					EstadoDescripcion:'<?php echo $EstadoDescripcion; ?>',
					
					Tiempo:'<?php echo $Tiempo; ?>',
					MotivoEstado2:'<?php echo $item["MotivoEstado"]; ?>',
					UsuMotivo2:'<?php echo $item["UsuMotivo"]; ?>',				
					FechaVencimiento2:'<?php echo $item["FechaVencimiento"]; ?>',
										
					IdPacienteReserva:'<?php echo $item["IdPacienteReserva"]; ?>',
					Paciente:'<?php echo  $Paciente; ?>',
					
					FechaReserva2:'<?php echo  $FechaReserva; ?>',
					HoraReserva2:'<?php echo  $HoraReserva; ?>',
					UsuReserva2:'<?php echo  $UsuReserva; ?>',
					MotivoAgregarReserva2:'<?php echo  $MotivoAgregarReserva; ?>',
					FecQuitarReserva2:'<?php echo  $FecQuitarReserva; ?>',
					HoraQuitarReserva2:'<?php echo  $HoraQuitarReserva; ?>',
					UsuQuitarReserva2:'<?php echo  $UsuQuitarReserva; ?>',
					MotivoQuitarReserva2:'<?php echo  $item["MotivoQuitarReserva"]; ?>',
					EstadoDescripcion2:'<?php echo strip_tags($EstadoDescripcion); ?>',					
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});
		
		</script> 
     
   
	
  
</body>
</html>