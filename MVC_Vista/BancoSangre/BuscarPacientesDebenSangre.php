<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consulta de Postulantes</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >			
			
			function salir(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Consultas&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
			
			function Buscar(){
				document.getElementById("form1").submit();
		    }
			
			function imprimir(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=TablaPacientesHospitalizadosEmergencia&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
		    }
			
		function Historial(){
		   var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){	
		   		location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerHistorialDonaciones&IdPaciente="+rowp.IdPaciente+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&NroHistoriaClinicaBusRec=<?php echo $NroHistoriaClinicaBusRec ?>";	
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Paciente','warning');
				return 0;			
			}				
	   }
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>     
        
<body>

		<div style="margin:0px 0;"></div>    
        <div id="tb" style="padding:5px;height:auto">
       		<div style="margin-bottom:5px">
                            
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>  
                <!--<a href="#" class="easyui-linkbutton" iconCls="icon-excel" plain="true" onClick="imprimir()" >Imprimir</a>-->   
                <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onClick="Historial()" >Historial Donaciones</a>            
                <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="salir();">Salir</a>
        	</div> 
         <form name="form1" id="form1" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPacientesDebenSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" >
            <fieldset>
			  <legend style="color:#03C"><strong>Busqueda:</strong></legend>
				<table width="100%" style="font-size:12px;">
				  <tr align="center">
					<th bgcolor="#D6D6D6"><strong>Nro Historia Paciente</strong></th>
					<th bgcolor="#D6D6D6"><strong>Nombres Paciente</strong></th>
                    <th bgcolor="#D6D6D6"><strong>Buscar</strong></th>
				  </tr>
				  <tr align="center">
					<td><input name="NroHistoriaClinica" type="text" id="NroHistoriaClinica" class="easyui-textbox" style="width:150px;" data-options="prompt:'Nº Historia Clinica',validType:'justNumber'" value="<?php echo $NroHistoriaClinicaBusRec ?>" ></td><!-- searcher:BuscarPacientesHC,-->
					<td><input class="easyui-combogrid" style="width:200px" id="ApellidosNombresBusRec" name="ApellidosNombresBusRec"  data-options="prompt:'Apellidos y Nombres',validType:'justText'"></td> 
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onClick="Buscar();">Buscar</a></td>
				  </tr>                         
				</table> 
            </fieldset> 
            <!--<input name="NroHistoriaClinica" type="hidden" id="NroHistoriaClinica" readonly /> -->          
              
          </form>
            
            
        </div>   
        
        <script type="text/javascript">
   $.extend($('#NroHistoriaClinica').searchbox.defaults.rules, {
        justNumber: {
            validator: function(value, param){
                 return value.match(/[0-9]/);
            },
            message: 'Porfavor digite sólo numeros'  
        }
    });	
	$.extend($('#ApellidosNombresBusRec').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor digite sólo texto'  
        }
    });
	
	//////3. FILTRAR COMBOGRID Apellidos y Nombres PACIENTE   		
			$(function(){	 //Filtrar Paciente				
			
				$('#ApellidosNombresBusRec').combogrid({
					panelWidth:350,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarPaciente',
					idField:'NroHistoriaClinica', 
					textField:'NombresPaciente',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = BuscarReceptoresApellidosNombres(); },	//esta funcion llama cuando seleccionas el paciente					
					columns:[[							
						{field:'NombresPaciente',title:'Apellidos y Nombres',width:150},
						{field:'GrupoSanguineo',title:'G.Sanguineo',width:35}						
					]]
				});	//FIN 
				
				/*$('#NroHistoriaClinicaBusRec').combogrid({
					panelWidth:200,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarPaciente',
					idField:'NroHistoriaClinica', 
					textField:'NroHistoriaClinica',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = BuscarPacientesHC(); },	//esta funcion llama cuando seleccionas el paciente					
					columns:[[							
						{field:'NroHistoriaClinica',title:'Nro Historia Clinica',width:40},
						{field:'GrupoSanguineo',title:'G.Sanguineo',width:35}						
					]]
				});	//FIN */
				
			});				
	
	/*function BuscarPacientesHC(){		
		
		var g = $('#NroHistoriaClinicaBusRec').combogrid('grid');	// get datagrid object
		var r = g.datagrid('getSelected');	// get the selected row
		
		//$('#NroHistoriaClinica').textbox('setValue', r.NroHistoriaClinica);
		document.getElementById("NroHistoriaClinica").value=r.NroHistoriaClinica;
		$('#dg').datagrid('loadData', []); 
	
	}*/
	
	function BuscarReceptoresApellidosNombres(){		
		var g = $('#ApellidosNombresBusRec').combogrid('grid');	// get datagrid object
		var r = g.datagrid('getSelected');	// get the selected row
		$('#ApellidosNombres').textbox('setValue', r.NombresPaciente);
		$('#GrupoSanguineo').textbox('setValue', r.GrupoSanguineo);	
		
		var NroHistoriaClinica=$('#ApellidosNombresBusRec').combogrid('getValue');
		$('#NroHistoriaClinica').textbox('setValue', NroHistoriaClinica);
		//document.getElementById("NroHistoriaClinica").value=NroHistoriaClinica;
		$('#dg').datagrid('loadData', []);  
		
	}	
	
	
	</script>    

        
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Pacientes que DEBEN Sangre" style="width:80%;height:90%" data-options="				
                rownumbers:true,
                method:'get',
                fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10,Rowselected:false,singleSelect:true">
		<thead>
			  <tr>
			  <th field="FechaUltDespacho" width="90" sortable="true">F.Ult.Transfusión</th>
			  <th field="NroHistoriaClinica" width="45" sortable="true">HC</th>
              <th field="Paciente" width="130" sortable="true">Paciente</th>
			  <th field="GrupoSanguineoPaciente" width="45" align="center" sortable="true">GS & RH</th>                
			  <!--<th field="ServicioActual" width="100" sortable="true">Servicio</th>
			  <th field="cama" width="33" sortable="true">Cama</th>
              <th field="Medico" width="130" sortable="true">Medico</th>-->
			  <th field="CantidadPG" width="30" sortable="true">PG</th>
			  <th field="CantidadPFC" width="30" sortable="true">PFC</th>
			  <th field="CantidadPQ" width="30" sortable="true">PQ</th>
			  <th field="CantidadCRIO" width="30" sortable="true">CRIO</th>
			  <th field="TotEquiDon" width="70" sortable="true">Equivale Donantes</th>
              <th field="CantidadDonantesTraidos" width="70" sortable="true">Depositó Donantes</th>
              <th field="TraerDon" width="70" sortable="true">Traer Donantes</th>
         </thead>
	</table>
     
   <script type="text/javascript">
	 
	function getData(){
			var rows = [];			
			<?php			
			 $i = 1;			
						
			$listado=PacientesDebenSangre($NroHistoriaClinicaBusRec);
			if($listado!=NULL){
				
				$TotEquiDonPG=0;$TotEquiDonPFC=0;$TotEquiDonPQ=0;$TotEquiCRIO=0;
				$TotEquiDon=0;$TraerDon=0;
				
				foreach($listado as $item){				 
				 	
					$NroHistoriaClinica=$item["NroHistoriaClinica"];					 		 
					$Paciente=$item["Paciente"];	
					$GrupoSanguineoPaciente=$item["GrupoSanguineoPaciente"];
					//$ServicioActual=$item["ServicioActual"];
					//$cama=$item["cama"];
					//$Medico=$item["Medico"];						
					
					if($item["FechaUltDespacho"]!=NULL){
						$FechaUltDespacho=vfecha(substr($item["FechaUltDespacho"],0,10));
						$HoraUltDespacho=substr($item["FechaUltDespacho"],11,8);
					}else{
						$FechaUltDespacho="";
						$HoraUltDespacho="";
					}	
					
					//Cantidades
					$ObtenerCantidadPG=ObtenerCantidadTipoHem($item["IdPaciente"],'PG');
					$CantidadPG=$ObtenerCantidadPG[0]["Cantidad"];
					
					$ObtenerCantidadPFC=ObtenerCantidadTipoHem($item["IdPaciente"],'PFC');
					$CantidadPFC=$ObtenerCantidadPFC[0]["Cantidad"];
					
					$ObtenerCantidadPQ=ObtenerCantidadTipoHem($item["IdPaciente"],'PQ');
					$CantidadPQ=$ObtenerCantidadPQ[0]["Cantidad"];
					
					$ObtenerCantidadCRIO=ObtenerCantidadTipoHem($item["IdPaciente"],'CRIO');
					$CantidadCRIO=$ObtenerCantidadCRIO[0]["Cantidad"];	
					
					//Equivale Donantes
					$TotEquiDonPG=$CantidadPG;	$TotEquiDonPFC=$CantidadPFC;
					$TotEquiDonPQ=$CantidadPQ/3;$TotEquiCRIO=$CantidadCRIO/3;
					$TotEquiDonX=$TotEquiDonPG+$TotEquiDonPFC+$TotEquiDonPQ+$TotEquiCRIO;
					if(0<$TotEquiDonX && $TotEquiDonX<1){
						$TotEquiDon=1;	
					}else{
						$TotEquiDon=round($TotEquiDonX);
					}			
					
					//Depositó Donantes
					$ObtenerCantidadDonantesTraidos=ObtenerCantidadDonantesTraidos($item["IdPaciente"]);
					$CantidadDonantesTraidos=$ObtenerCantidadDonantesTraidos[0]["Cantidad"]; 
					
					//Traer Donantes
					$TraerDon=$TotEquiDon-$CantidadDonantesTraidos;
						
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							NroHistoriaClinica: '<?php echo $NroHistoriaClinica;?>',
							Paciente: '<?php echo $Paciente;?>',							
							GrupoSanguineoPaciente: '<?php echo $GrupoSanguineoPaciente;?>',	
							<?php /*?>ServicioActual:'<?php echo $ServicioActual;?>',
							cama: '<?php echo $cama;?>',
							Medico: '<?php echo $Medico;?>',<?php */?>
							FechaUltDespacho: '<?php echo $FechaUltDespacho.' '.$HoraUltDespacho;?>',
							CantidadPG: '<?php echo $CantidadPG;?>',
							CantidadPFC: '<?php echo $CantidadPFC;?>',
							CantidadPQ: '<?php echo $CantidadPQ;?>',
							CantidadCRIO: '<?php echo $CantidadCRIO;?>',
							CantidadDonantesTraidos: '<?php echo $CantidadDonantesTraidos;?>',
							TotEquiDon: '<?php echo $TotEquiDon;?>',
							TraerDon: '<?php echo $TraerDon;?>',
							IdPaciente: '<?php echo $item["IdPaciente"];?>',
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}
				 	
		
		$(function(){
			var dgPacientes = $('#dg').datagrid({
				remoteFilter: false,
				pagination: true,
				pageSize: 20,
				pageList: [10,20,50,100]
			});
		
			dgPacientes.datagrid('enableFilter');										
			dgPacientes.datagrid('loadData', getData());	
		
		});		

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>