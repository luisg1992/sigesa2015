<html>
<head>
	<meta charset="UTF-8">
	<title>Configuracion de Prametros</title>      
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css"> 
         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>     
        
		<style>
            .icon-filter{
                background:url('../images/filter.png') no-repeat center center;
            }
            
             #fm{
                    margin:0;
                    padding:10px 30px;
                }
                #fmCabVacacion{
                    margin:0;
                    padding:10px 30px;
                }
                .ftitle{
                    font-size:14px;
                    font-weight:bold;
                    padding:5px 0;
                    margin-bottom:10px;
                    border-bottom:1px solid #ccc;
                }
                .fitem{
                    margin-bottom:5px;
                }			
                .fitem label{
                    display:inline-block;
                    width:60px;	
                    margin-left:10px;			
                }
                .fitem input{
                    width:110px;				
                }
                
                .fitem2{
                    margin-bottom:5px;
                    margin-left:10px;
                }
                .fitem2 input {				
                     margin-right:25px;		 
                }
        </style> 
    
<script type="text/javascript" >  
	/////////MANTENIMIENTO CABVACION
		//var url;		
		function editarParametro(){
			var rowc = $('#dg').datagrid('getSelected');
				if (rowc){	
				$('#dlg-CabVacacion').dialog('open').dialog('setTitle','Editar Parametro');
				$('#fmCabVacacion').form('load',rowc);
				<?php /*?>url = "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdParametros&Codigo="+rowc.Codigo+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";<?php */?>
				}else{
					$.messager.alert('Mensaje de Información', 'Seleccione un Parametro','warning');
				}
		}
		
		function saveParametro(){
			$.messager.confirm('Mensaje de Confirmación', 'Seguro de Actualizar el Parametro', function(r){
				if (r){
					$('#fmCabVacacion').submit();					
				}
		   });					
		   //$('#fmCabVacacion').submit();			
		}	
		
		//////ELIMINAR
		function ActivarDesactivar(){					
			var row = $('#dg').datagrid('getSelected');
				if (row){
					nombreCompleto=row.Descripcion;				
					if(row.Estado=='1'){									
					    $.messager.confirm('Mensaje de Confirmacion', 'Seguro de Desactivar el Parametro seleccionado', function(r){
							if (r){
								location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ActivarDesactivarParametros&Codigo="+row.Codigo+"&Estado=0&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";					
							}
					   });
					
					}else{
					 	$.messager.confirm('Mensaje de Confirmacion', 'Seguro de Activar el Parametro seleccionado', function(r){
							if (r){
								location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ActivarDesactivarParametros&Codigo="+row.Codigo+"&Estado=1&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";					
							}
					   });
					}
						
				}else{
					$.messager.alert('Mensaje de Información', 'Seleccione un Parametro','warning');
				}	
		}		
	
</script>

</head>

<body>
   
		<div style="margin:0px 0;"></div>	      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">		
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editarParametro();" >Editar Parametro</a>	                
                <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-cancel'" onClick="ActivarDesactivar();">Activar/Desactivar Parametro</a>   
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>  
			</div>      			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Parámetros de Configuración" style="width:1100px;height:450px" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>            
            	<th field="Codigo"  width="80">Codigo</th>
				<th field="Descripcion"  width="350">Descripcion</th>                
				<th field="Unidad" width="80">Unidad</th>
                <th field="Valor1"  width="100">Valor</th>
                <th field="Modulo" width="100">Modulo</th>							
                <th field="detestado" width="80">Estado</th>
			
			</tr>
		</thead>
	</table>
        

 <!--FORMULARIO EDITAR CONFIGURACION-->
 <div id="dlg-CabVacacion" class="easyui-dialog" style="width:700px;height:250px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fmCabVacacion" name="fmCabVacacion" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdParametros&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
                   
              <div class="fitem">
                 	<label>Codigo:</label>
                    <input type="text" class="easyui-textbox" name="Codigo" id="Codigo" readonly/>
               	 	<label>Descripcion:</label>
                 	<input type="text" class="easyui-textbox" name="Descripcion" id="Descripcion" style="width:300px;" readonly />
                 	<!--<input type="text"  name="Dias" id="Dias"  />-->
               </div> 
               <div class="fitem">
               		<label>Modulo:</label>  
                    <input type="text" class="easyui-textbox" name="Modulo" id="Modulo" readonly/>
					<label>Unidad:</label>                   
               		<input type="text" class="easyui-textbox" name="Unidad" id="Unidad" readonly/>
                 	<label>Valor 1:</label>
                    <input type="text" class="easyui-textbox" name="Valor1" id="Valor1"/>
               </div>      
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveParametro();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-CabVacacion').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO EDITAR CONFIGURACION-->   
    
    
	<script>
	
		function getData(){
			var rows = [];			
			
	<?php 	
	 $data = SIGESA_ListarParametros_M(1);
    										
	if($data!=NULL){
		//foreach($listaos as $item)
		for ($i=0; $i < count($data); $i++)  {		  				
			
			if($data[$i]["Estado"]=='1'){
				$detestado='<font color="#0000FF">ACTIVO</font>';
			}else if($data[$i]["Estado"]=='0'){
				$detestado='<font color="#FF0000">INACTIVO</font>';
			}		
						
	 ?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					Codigo:'<?php echo  $data[$i]["Codigo"]; ?>',		
					Descripcion: '<?php echo utf8_encode($data[$i]["Descripcion"]);?>',
					Modulo: '<?php echo $data[$i]["Modulo"]; ?>',	
					Unidad:'<?php echo  $data[$i]["Unidad"]; ?>',										
					Valor1:'<?php echo  $data[$i]["Valor1"]; ?>',
					Valor2:'<?php echo  $data[$i]["Valor2"]; ?>',
					detestado:'<?php echo  $detestado; ?>',
					Estado:'<?php echo  $data[$i]["Estado"]; ?>',			
			
				});
			//}
			<?php 
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});
		/*$(function(){
			$('#dg').datagrid({data:getData()}).datagrid('clientPaging');
		});*/
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});
		
		</script>	
  
</body>
</html>