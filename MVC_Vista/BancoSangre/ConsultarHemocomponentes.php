<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Consultar Hemocomponentes</title>
</head>    
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		form{
                margin:0;
                padding:10px 30px;
            }		
			#fmCabHemocomponente{
                margin:0;
                padding:10px 20px;
            }
			#fmCabHemocomponenteCrio{
                margin:0;
                padding:10px 20px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:90px;
							
            }
            .fitem input{
				display:inline-block;
                width:100px;	
				margin-left:10px;			
            }
			
	</style> 
    
    <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>   
    
<script type="text/javascript" >			
		
		$.extend($("#FecMotivo").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FecMotivo").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});					
		
		function regresar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Consultas&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}
		
		function Buscar(){
			document.getElementById("form1").submit();
		}			
	
    </script>

<body>

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->
	<div style="margin:0px 0;"></div>
	 <form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">  
            	             	 
		       <!-- <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="FraccionarHemocomponente()">Consultar Hemocomponente</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="regresar();">Regresar</a> 
			</div>
            
              <div>
               <strong>Busqueda de Hemocomponentes:</strong> &nbsp;&nbsp;&nbsp;&nbsp; 
               		<select name="MovTipoB" id="MovTipoB" class="easyui-combobox" style="width:120px;" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }">                        
                        <option value="I" <?php if($MovTipo=='I'){?> selected <?php } ?> >Ingresos</option>
                        <option value="F" <?php if($MovTipo=='F'){?> selected <?php } ?> >Fracciones</option>
                    </select>
                                   
                   <select name="TipoHemB" id="TipoHemB" class="easyui-combobox" style="width:80px;" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }">  
                        <option value="">Seleccione</option>                                           
                        <option value="PG" <?php if($TipoHem=='PG'){?> selected <?php } ?> >PG </option>
                        <option value="PFC" <?php if($TipoHem=='PFC'){?> selected <?php } ?> >PFC </option>
                        <option value="PQ" <?php if($TipoHem=='PQ'){?> selected <?php } ?>>PQ </option>
                        <option value="CRIO" <?php if($TipoHem=='CRIO'){?> selected <?php } ?>>CRIO </option>
                    </select>
                 	Estado:
                    <select name="EstadoB" id="EstadoB" class="easyui-combobox" style="width:150px;" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }">
                        <option value="T">Todos</option>                        
                        <option value="1" <?php if($Estado=='1'){?> selected <?php } ?> >Disponible </option>
                        <option value="2" <?php if($Estado=='2'){?> selected <?php } ?> >Reservado </option>
                        <option value="3" <?php if($Estado=='3'){?> selected <?php } ?>>Fracciones Disponibles</option>
                        <option value="4" <?php if($Estado=='4'){?> selected <?php } ?>>Custodia </option>
                        <option value="5" <?php if($Estado=='5'){?> selected <?php } ?>>Tranferido otro Hospital </option>
                        <option value="6" <?php if($Estado=='6'){?> selected <?php } ?>>Transfundido Paciente </option>
                        <option value="0" <?php if($Estado=='0'){?> selected <?php } ?> >Eliminado </option>
                    </select>
                    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onClick="Buscar();">Buscar</a>
                </div>
            			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Ver Hemocomponentes" style="width:100%;height:auto" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:true,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	<th field="EstablecimientoIngreso" width="250">Hospital Ingreso</th>
            	<th field="NroDonacion" width="100">N°Donacion</th>
				<th field="TipoHem"  width="80">Hemocomp.</th>				
                <th field="SNCS"  width="100">SNCS</th>	
				<th field="GrupoSanguineo"  width="100">GS</th>
                <th field="VolumenTotRecol"  width="100">Volumen Total</th>
                <th field="VolumenFraccion"  width="100">Volumen Fraccionado</th>
                <th field="VolumenRestante"  width="100">Volumen Restante</th>
				<th field="NroTabuladora" width="80">tubuladura</th>
                <th field="FechaExtraccion" width="100">F. Extracción</th>
                <th field="FechaHoraVencimiento" width="140">F. Vencimiento</th>
                <th field="EstadoDescripcion" align="center" width="100">Estado</th>
                <th field="DescripcionMotivoEstado" align="center" width="130">Motivo</th>
			</tr>
		</thead>
	</table>
     </form>        
    
   <script> 
   
   $("#dg").datagrid({
		// Fires when data in datagrid is loaded successfully
		onLoadSuccess:function(){	
			// Get this datagrid's panel object
			$(this).datagrid('getPanel')
			// for all easyui-linkbutton <a>'s make them a linkbutton
			.find('a.easyui-linkbutton').linkbutton();
		}
    });
   
	
		function getData(){
			var rows = [];			
			
	<?php 
	
	 $BuscarSNCSDisponible = ConsultarHemocomponentes($MovTipo,$Estado,$TipoHem);
     $i = 1;										
	if($BuscarSNCSDisponible!=NULL){
		foreach($BuscarSNCSDisponible as $item)
		{
			//Funcion en Funciones.php
			$EstadoDescripcion=DescripcionEstadoHemocomponente($item["Estado"]);
						
			 ?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TipoHem: '<?php echo $item["TipoHem"];?>',				
					NroDonacion: '<?php echo $item["NroDonacion"]; ?>',											
					SNCS:'<?php echo  $item["SNCS"]; ?>',					
					GrupoSanguineo:'<?php echo  trim($item["GrupoSanguineo"]); ?>',
					
					VolumenTotRecol:'<?php echo  $item["VolumenTotRecol"]; ?>',
					VolumenFraccion:'<?php echo  $item["VolumenFraccion"]; ?>',		
					VolumenRestante:'<?php echo  $item["VolumenRestante"]; ?>',	
									
					NroTabuladora:'<?php echo  $item["NroTabuladora"]; ?>',
					FechaExtraccion:'<?php echo  vfecha(substr($item["FechaExtraccion"],0,10)); ?>',				
								
					IdMovDetalle:'<?php echo  $item["IdMovDetalle"]; ?>',
					MovNumero:'<?php echo  $item["MovNumero"]; ?>',
					EstadoDescripcion:'<?php echo  $EstadoDescripcion; ?>',					
				
					FechaHoraVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)).' '.substr($item["FechaVencimiento"],11,5); ?>',
					EstablecimientoIngreso:'<?php echo utf8_encode(trim($item["EstablecimientoIngreso"])); ?>',
					Estado:'<?php echo trim($item["Estado"]); ?>',
					DescripcionMotivoEstado:'<?php echo utf8_encode(trim($item["DescripcionMotivoEstado"])); ?>',
			
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
		
		/*$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});*/		
	
			
		$(function(){
			var dgHem = $('#dg').datagrid({
				remoteFilter: false,
				pagination: true,
				pageSize: 20,
				//pageList: [10,20,50,100]
			});
				
			dgHem.datagrid('enableFilter');
			//FilterFechaActualSolicitud();												
			dgHem.datagrid('loadData', getData());	
		
		});		
		
		$('#dg').datagrid({
			rowStyler:function(index,row){				
				if (row.Estado==0){				
					return 'background-color:pink;font-weight:bold;';
				}
			}
		});
		
		</script> 
     
   
	
  
</body>
</html>