<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consultar Movimientos Unidades Aptas</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >		
			
			$(function(){  	
			  $('#SNCS').textbox('textbox').attr('maxlength', $('#SNCS').attr("maxlength"));			   
			});				
			
			function salir(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Consultas&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}		
			
			/*function cambiarTipoHem(){
				$('#SNCS').next().find('input').focus();				
			}*/
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>     
        
<body>

		<div style="margin:0px 0;"></div>    
        <div id="tb" style="padding:5px;height:auto">
       		<div style="margin-bottom:5px">                
              <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>                
                <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="salir();">Salir</a>
        	</div> 
          <form name="form1" id="form1" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarMovimientosUnidadesAptas" method="post" >
            <fieldset>
			  <legend style="color:#03C"><strong>Busqueda:</strong></legend>
				<table width="100%" style="font-size:12px;">
				  <tr align="center">
					<th bgcolor="#D6D6D6"><strong>Tipo Hemocomponente</strong></th>
					<th bgcolor="#D6D6D6"><strong>Nro SNCS</strong></th>
					<th bgcolor="#D6D6D6"><strong>Buscar</strong></th>
				  </tr>
				  <tr align="center">
					<td><select style="width:120px;" name="TipoHem" id="TipoHem" class="easyui-combobox" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = Buscar(); }" >
                        <option value=""></option>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select></td><!-- searcher:BuscarPostulantesDNI,-->
					<td><input style="width:120px;" id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" value="" data-options="prompt:'SNCS',required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = Buscar(); }" /></td>
					<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onClick="Buscar();">Buscar</a></td> 
				  </tr>                         
				</table> 
            </fieldset>   
            </form> 
             
          <table width="80%" style="font-size:12px;">				   
              <tr>
                <td width="5%">&nbsp;</td>
                <td width="10%">Nro Donación</td>
                <td width="15%"><input name="NroDonacion" type="text" id="NroDonacion" class="easyui-textbox" style="width:100px;" readonly /></td>
                <td width="10%">Nro Tubuladura</td>
                <td width="15%"><input name="Tubuladura" type="text" id="Tubuladura" class="easyui-textbox" style="width:100px;" readonly /></td> 
                <td width="15%">Grupo Sanguineo</td>
                <td width="10%"><input name="GrupoSanguineo" type="text" id="GrupoSanguineo" class="easyui-textbox" style="width:100px;" readonly /></td>
              </tr>       
            </table>                 
          
     		        
        </div>       

        
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Consultar Movimientos Unidades Aptas" style="width:95%;height:80%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
                   <th field="SNCS" width="55">Hemocomp.</th>
                   <th field="VolumenRestante" width="40">Volumen</th>
                   
                   <th field="EstablecimientoIngreso" width="90">Hospital origen</th>           	  
                   
                   <th field="FechaRecibe" width="60">Fecha Ingreso</th>
                   <th field="FechaExtraccion" width="55">Fecha Extracción</th>                   
                   <th field="FechaVencimiento" width="60">Fecha Vencimto</th>
                   
                   <th field="Fenotipo" width="60">Fenotipo RH-Kell</th>                   
                    <th field="TipoDonacion" width="50">Tipo Donacion</th>
                   
                   <th field="FechaMovimiento" width="60">Fecha Movimiento</th>                   
                   <th field="EstadoDescripcion" width="50">Estado</th>
                   <th field="Observacion" width="150">Observación</th>                   
                                    	
			</tr>
		</thead>
	</table>
     
   <script type="text/javascript"> 
	
	function Buscar() { 
	
		var TipoHem=$('#TipoHem').combobox('getValue');
		var SNCS=$('#SNCS').textbox('getText');
		
		if(TipoHem==""){				
			$.messager.alert({
				title: 'Mensaje',
				msg: 'Seleccione un Tipo Hemocomponente',
				icon:'warning',
				fn: function(){
					$('#TipoHem').next().find('input').focus();
				}
			});
			$('#TipoHem').next().find('input').focus();
			return 0;				
		}	
		
		if(SNCS==""){				
			$.messager.alert({
				title: 'Mensaje',
				msg: 'Ingrese un Nro SNCS',
				icon:'warning',
				fn: function(){
					$('#SNCS').next().find('input').focus();
				}
			});
			$('#SNCS').next().find('input').focus();
			return 0;				
		} 	
		 
	 
		 // $.messager.confirm('SIGESA', 'Seguro de guardar la información: ', function(r){
			//if (r){
				 $.ajax({
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteMovimientosUnidadesAptas',
					type: 'POST',
					dataType: 'json',
					data: {
						//IdEmpleado :document.getElementById('IdEmpleado').value, 
						TipoHem  :   TipoHem,
						SNCS  :   SNCS
					},							
					success: function (data) {								
						/*$('#NroDonacion').textbox('setValue', data[0].NroDonacion);
						$('#Tubuladura').textbox('setValue', data[0].NroTabuladora);
						$('#GrupoSanguineo').textbox('setValue', data[0].GrupoSanguineo);*/						
					}
				}).done(function (data) {
                    console.log(data);
                    //alert(data);
						$('#dg').datagrid({
							data: data
						});
						
					    if(data=="") { //if(!data.success){						    
							//alert('NO HAY DATOS');			
							$.messager.alert('SIGESA','NO HAY DATOS' ,'info');	
							$('#NroDonacion').textbox('setValue', '');
							$('#Tubuladura').textbox('setValue', '');
							$('#GrupoSanguineo').textbox('setValue', '');		
						}else{
							$('#NroDonacion').textbox('setValue', data[0].NroDonacion);
							$('#Tubuladura').textbox('setValue', data[0].NroTabuladora);
							$('#GrupoSanguineo').textbox('setValue', data[0].GrupoSanguineo);	
						
						}//END else
				});			
				
                // stop the form from submitting the normal way and refreshing the page
                //event.preventDefault();
            
				//alert(msg);
				//$.messager.alert('SIGESA',msg ,'error');
			   
	   	   	
	}
	
    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>