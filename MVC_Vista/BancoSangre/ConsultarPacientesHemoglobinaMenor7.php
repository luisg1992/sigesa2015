<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consulta de Postulantes</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
		
		$.extend($("#FechaInicio").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaInicio").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});				
			
			function salir(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Consultas&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
			
			function Buscar(){
				document.getElementById("form1").submit();
		    }
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>     
        
<body>

		<div style="margin:0px 0;"></div>    
        <div id="tb" style="padding:5px;height:auto">
       		<div style="margin-bottom:5px">
                <!--<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="evaluacion()">Evaluación Predonación</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Actualizar Recepción</a> -->
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>              
                <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="salir();">Salir</a>
        	</div> 
          <form name="form1" id="form1" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarPacientesHemoglobinaMenor7&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" >
            <fieldset>
			  <legend style="color:#03C"><strong>Busqueda:</strong></legend>
				<table width="100%" style="font-size:12px;">
				  <tr align="center">
					<th bgcolor="#D6D6D6"><strong>Fecha Inicio</strong></th>
					<th bgcolor="#D6D6D6"><strong>Fecha Final</strong></th>
                    <th bgcolor="#D6D6D6"><strong>Buscar</strong></th>
				  </tr>
				  <tr align="center">
					<td><!--<input class="easyui-numberbox" style="width:110px" id="NroHistoriaClinicaBusRec" name="NroHistoriaClinicaBusRec" data-options="prompt:'Nº DNI'" maxlength="8">--><input name="FechaInicio" type="text" id="FechaInicio" class="easyui-datebox" style="width:120px;" value="<?php echo $FechaInicioB ?>" data-options="prompt:'Fecha Inicio',
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" /></td><!-- searcher:BuscarPacientesHC,-->
					<td><input name="FechaFinal" type="text" id="FechaFinal" class="easyui-datebox" style="width:120px;" value="<?php echo $FechaFinalB ?>" data-options="prompt:'Fecha Final',
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" /></td> 
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onClick="Buscar();">Buscar</a></td>
				  </tr>                         
				</table> 
            </fieldset>   
            </form>
        </div>       

        
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Listado de Pacientes con hemoglobina <= 7" style="width:80%;height:90%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,remoteSort:false,
				pageSize:10">
		<thead>
			  <tr>
			  <th field="idOrden" width="55"  align="right" sortable="true">Orden</th>
			  <th field="ValorNumero" width="55" align="right" sortable="true">Hemogolobina</th>
              <th field="Hematocrito" width="55" align="right" sortable="true">Hematocrito</th>
			  <th field="Fecha" width="100" align="center" sortable="true">Fecha Resultado</th>                      
			  <th field="Paciente" width="200" sortable="true">Paciente</th>
			  <th field="NroHistoriaClinica" width="90" sortable="true">NroHistoriaClinica</th>
			  <th field="NroDocumento" width="90" sortable="true">NroDocumento</td>
         </thead>
	</table>
     
   <script type="text/javascript">
	 
	function getData(){
			var rows = [];			
			<?php			
			 $i = 1;	
			$FechaInicio= gfecha($FechaInicioB);	
			$FechaFinal= gfecha($FechaFinalB);				
			$listado=SIGESA_BSD_ConsultarPacientesHemoglobinaMenorSiete_M($FechaInicio,$FechaFinal);
			if($listado!=NULL){
				foreach($listado as $item){								 
				 	
				 $ValorNumero=round($item["ValorNumero"],2);	
				 $Hematocrito=round($item["Hematocrito"],2);			 
				 $Fecha=$item["Fecha"];	
					if($Fecha!=NULL){
						$Fecha=vfecha(substr($item["Fecha"],0,10)).' '.substr($item["Fecha"],11,8);					
					}				
						
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							idOrden: '<?php echo $item['idOrden'];?>',
							ValorNumero: '<?php echo $ValorNumero;?>',							
							Fecha: '<?php echo $Fecha;?>',	
							Paciente:'<?php echo $item['ApellidoPaterno'].' '.$item['ApellidoMaterno'].' '.$item['PrimerNombre'].' '.$item['SegundoNombre'].' '.$item['TercerNombre'];?>',
							NroHistoriaClinica: '<?php echo $item['NroHistoriaClinica'];?>',
							NroDocumento: '<?php echo $item['NroDocumento'];?>',
							Hematocrito: '<?php echo $Hematocrito;?>',
						
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}
				 	
		
		$(function(){
			var dgPacientes = $('#dg').datagrid({
				remoteFilter: false,
				pagination: true,
				pageSize: 20,
				pageList: [10,20,50,100]
			});
		
			dgPacientes.datagrid('enableFilter');										
			dgPacientes.datagrid('loadData', getData());	
		
		});		

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>