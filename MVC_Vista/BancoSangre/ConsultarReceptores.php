<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consulta de Postulantes</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
		
			//////3. FILTRAR COMBOGRID Apellidos y Nombres PACIENTE   		
			$(function(){	 //Filtrar Paciente	
				$('#ApellidosNombresBusRec').combogrid({
					panelWidth:350,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarReceptor',
					idField:'NroHistoriaClinica', //NombresPaciente
					textField:'NombresPaciente',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = BuscarReceptoresApellidosNombres(); },	//esta funcion llama cuando seleccionas el paciente					
					columns:[[							
						{field:'NombresPaciente',title:'Apellidos y Nombres',width:150},
						{field:'GrupoSanguineo',title:'G.Sanguineo',width:35}						
					]]
				});	//FIN 
				
				$('#NroHistoriaClinicaBusRec').combogrid({
					panelWidth:200,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarReceptor',
					idField:'NombresPaciente', //NombresPaciente
					textField:'NroHistoriaClinica',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = BuscarPacientesHC(); },	//esta funcion llama cuando seleccionas el paciente					
					columns:[[							
						{field:'NroHistoriaClinica',title:'Nro Historia Clinica',width:40},
						{field:'GrupoSanguineo',title:'G.Sanguineo',width:35}						
					]]
				});	//FIN 
				
			});				
								
			
			$.extend( $( "#FechaInicio" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#FechaInicio" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaInicio" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 
			
			function salir(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Consultas&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>     
        
<body>

		<div style="margin:0px 0;"></div>    
        <div id="tb" style="padding:5px;height:auto">
       		<div style="margin-bottom:5px">
                <!--<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="evaluacion()">Evaluación Predonación</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Actualizar Recepción</a> -->
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>              
                <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="salir();">Salir</a>
        	</div> 
          <form name="form1" id="form1" action="../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Emergencia_ListarPacientes" method="post" >
            <fieldset>
			  <legend style="color:#03C"><strong>Busqueda:</strong></legend>
				<table width="100%" style="font-size:12px;">
				  <tr align="center">
					<th bgcolor="#D6D6D6"><strong>Nro Historia Paciente</strong></th>
					<th bgcolor="#D6D6D6"><strong>Apellidos y Nombres Paciente</strong></th>
				  </tr>
				  <tr align="center">
					<td><!--<input class="easyui-numberbox" style="width:110px" id="NroHistoriaClinicaBusRec" name="NroHistoriaClinicaBusRec" data-options="prompt:'Nº DNI'" maxlength="8">--><input name="NroHistoriaClinicaBusRec" type="text" id="NroHistoriaClinicaBusRec" class="easyui-combogrid" style="width:120px;" data-options="prompt:'Nº Historia Clinica',validType:'justNumber'" ></td><!-- searcher:BuscarPacientesHC,-->
					<td><input class="easyui-combogrid" style="width:200px" id="ApellidosNombresBusRec" name="ApellidosNombresBusRec"  data-options="prompt:'Apellidos y Nombres',validType:'justText'"></td> 
				  </tr>                         
				</table> 
            </fieldset>   
            </form> 
             
            <table width="94%" style="font-size:12px;">				   
              <tr>
                <td width="5%">&nbsp;</td>
                <td width="9%">Nro Historia</td>
                <td width="10%"><input name="NroHistoriaClinica" type="text" id="NroHistoriaClinica" class="easyui-textbox" style="width:100px;" readonly /></td>
                <td width="12%">Apellidos y Nombres</td>
                <td width="27%"><input name="ApellidosNombres" type="text" id="ApellidosNombres" class="easyui-textbox" style="width:300px;" readonly /></td> 
                <td width="12%">Grupo Sanguineo</td>
                <td width="25%"><input name="GrupoSanguineo" type="text" id="GrupoSanguineo" class="easyui-textbox" style="width:100px;" readonly /></td>
              </tr>       
            </table>                 
          
     		        
        </div>       

        
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Consulta de Receptores (Pacientes)" style="width:100%;height:80%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	   <th field="NroHistoria" width="55">Historia</th>
                   <th field="HospitalProcedencia" width="80">Hospital</th> 
            	   <th field="NroMovimiento" width="65">Nro Postulación</th>
            	   <th field="FechaMovi" width="100">Fecha y Hora Recepción</th>  
                   <th field="Postulante" width="130">Postulante</th> 
                   <th field="TipoDonacion" width="55">Tipo Donacion</th>                  
                   <th field="NroDonacion" width="55">Nro Donacion</th>                
                   
                   <th field="DiferidoPredonacion" width="60">Dif.Pre-donación</th>
                   <th field="DiferidoExtraccion" width="60">Dif.Extracción</th>                  
                   <!--<th field="Paciente" width="130">Paciente</th>
                   <th field="GrupoSanguineoPaciente" width="50">G.S.Paciente</th>-->              
                                     
                   <th field="Tamizaje" width="80">Tamizaje</th>
                                     	
			</tr>
		</thead>
	</table>
     
   <script type="text/javascript">
   $.extend($('#NroHistoriaClinicaBusRec').searchbox.defaults.rules, {
        justNumber: {
            validator: function(value, param){
                 return value.match(/[0-9]/);
            },
            message: 'Porfavor digite sólo numeros'  
        }
    });	
	$.extend($('#ApellidosNombresBusRec').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor digite sólo texto'  
        }
    });
	
	function BuscarPacientesHC(){
		
		//var NroHistoriaClinicaBusRec=$('#NroHistoriaClinicaBusRec').combogrid('getText'); 
		//$('#NroHistoriaClinica').textbox('setValue', NroHistoriaClinicaBusRec);
		var g = $('#NroHistoriaClinicaBusRec').combogrid('grid');	// get datagrid object
		var r = g.datagrid('getSelected');	// get the selected row
		$('#NroHistoriaClinica').textbox('setValue', r.NroHistoriaClinica);
		$('#GrupoSanguineo').textbox('setValue', r.GrupoSanguineo);
		
		var ApellidosNombres=$('#NroHistoriaClinicaBusRec').combogrid('getValue');
		$('#ApellidosNombres').textbox('setValue', ApellidosNombres);
		
		$('#ApellidosNombresBusRec').combogrid('setValue', '');//LIMPIAR BUSQUEDA APELLIDOS Y NOMBRES	
		BuscarPostulantes()
	}
	
	function BuscarReceptoresApellidosNombres(){
		//var ApellidosNombres=$('#ApellidosNombresBusRec').combogrid('getText');
		//$('#ApellidosNombres').textbox('setValue', ApellidosNombres);
		var g = $('#ApellidosNombresBusRec').combogrid('grid');	// get datagrid object
		var r = g.datagrid('getSelected');	// get the selected row
		$('#ApellidosNombres').textbox('setValue', r.NombresPaciente);
		$('#GrupoSanguineo').textbox('setValue', r.GrupoSanguineo);	
		
		var NroHistoriaClinica=$('#ApellidosNombresBusRec').combogrid('getValue');
		$('#NroHistoriaClinica').textbox('setValue', NroHistoriaClinica);
		
		$('#NroHistoriaClinicaBusRec').combogrid('setValue', '');//LIMPIAR BUSQUEDA NRO DOCUMENTO
		BuscarPostulantes()
	}
	
	function BuscarPostulantes() {  
	
	 var ok = true;
	 var msg = "Falta completar los siguientes datos:</br>";	  
	     //alert('holaa');
	  /*if ($('#NroHistoriaClinicaBusRec').numberbox('getText')!="" && $('#NroHistoriaClinicaBusRec').numberbox('getText').length < 8){
                msg += "- DNI debe Tener 8 Digitos </br>";
                ok = false;
      }*/ 
	  
	  var NroHistoriaClinicaBusRec=$('#NroHistoriaClinicaBusRec').combogrid('getText'); 	  
	  if(NroHistoriaClinicaBusRec!=''){
		  //var NroHistoriaClinica=NroHistoriaClinicaBusRec; 
		 var g = $('#NroHistoriaClinicaBusRec').combogrid('grid');	// get datagrid object
		 var r = g.datagrid('getSelected');	// get the selected row
		 NroHistoriaClinica=r.NroHistoriaClinica; 		
	  }else{
		var NroHistoriaClinica= $('#ApellidosNombresBusRec').combogrid('getValue');	   
	  }
		 
	  if (ok != false) {
		 // $.messager.confirm('SIGESA', 'Seguro de guardar la información: ', function(r){
			//if (r){
				 $.ajax({
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteReceptores',
					type: 'POST',
					dataType: 'json',
					data: {
						//IdEmpleado :document.getElementById('IdEmpleado').value, 
						NroHistoria  :   NroHistoriaClinica
					},							
					success: function (data) {		
						//document.getElementById('IdPaciente').value=data.IdPaciente;	
						//$('#ApellidosNombres').textbox('setValue', data.Postulante);
					}
				}).done(function (data) {
                    console.log(data);
                    //alert(data);
						$('#dg').datagrid({
							data: data
						});
						
					    if(data=="") { //if(!data.success){						    
							//alert('NO HAY DATOS');			
							$.messager.alert('SIGESA','NO HAY DATOS' ,'info');			
						}else{
							
						
						}//END else
				});			
				
                // stop the form from submitting the normal way and refreshing the page
                //event.preventDefault();
            }else{
				//alert(msg);
				$.messager.alert('SIGESA',msg ,'error');
			}   
	   	   	
	}
		
function validaEntero(){	
 	/*//alert('validaEntero');
	var s = $.fn.searchbox.defaults.formatter(/[0-9]/);
	if(s==value){
		return true;
	}else{
		$("#IdCuenta" ).searchbox('setValue', '');
		return false;
	}*/
}
	 
	<?php /*?>function getData(){
			var rows = [];			
			<?php			
			 $i = 1;					
			 $Listar=ReportePostulantesM("");				
			 if($Listar!=NULL){   
				foreach($Listar as $item)
				{					 
				 $EstadoApto=$item["EstadoApto"]; //EstadoApto SIGESA_BSD_ExamenMedico				 
				 if($EstadoApto==''){
					 $DiferidoPredonacion='F';
				 }else if($EstadoApto=='2'){
					 $DiferidoPredonacion='NO';
				 }else{
					$DiferidoPredonacion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $IdMotivoRechazoDurante=$item["IdMotivoRechazoDurante"];
				 $IdMotivoRechazoDespues=$item["IdMotivoRechazoDespues"];
				 $IdMotivoRechazoExtraccion=$IdMotivoRechazoDurante.' '.$IdMotivoRechazoDespues;
				 if(trim($IdMotivoRechazoExtraccion)==''){
					 $DiferidoExtraccion='F';
				 }else if($IdMotivoRechazoDurante=='0' && $IdMotivoRechazoDespues=='0'){
					 $DiferidoExtraccion='NO';
				 }else{
					$DiferidoExtraccion='<font color="#FF0000">SI</font>'; 
				 }
				 
				 $EstadoAptoTamizaje=$item["EstadoAptoTamizaje"];
				 if($EstadoAptoTamizaje==''){
					 $Tamizaje='F';
				 }else if($EstadoAptoTamizaje=='0'){ //NO apto tamizaje
					$Tamizaje='<font color="#FF0000">Conversar con Médico</font>';  
				 }else{ //1
					$Tamizaje='Apto'; 
				 }
						
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							NroMovimiento: '<?php echo 'PDAC'.$item['NroMovimiento'];?>',
							FechaMovi: '<?php echo vfecha(substr($item['FechaMovi'],0,10)).' '.substr($item['FechaMovi'],11,8);?>',							
							NroDocumento: '<?php echo $item['NroDocumento'];?>',
							Postulante: '<?php echo $item['ApellidosPostulante'].' '.$item['NombresPostulante'];?>',
							NroDonacion:  '<?php echo $item['NroDonacion'];?>',
							TipoDonacion:  '<?php echo $item['TipoDonacion'];?>',
							DiferidoPredonacion:  '<?php echo $DiferidoPredonacion;?>',
							DiferidoExtraccion:  '<?php echo $DiferidoExtraccion;?>',
													
							Paciente:'<?php echo $item['ApellidoPaternoPaciente'].' '.$item['ApellidoMaternoPaciente'].' '.$item['NombresPaciente'];?>',
							GrupoSanguineoPaciente:'<?php echo $item['GrupoSanguineoPaciente'];?>',
							HospitalProcedencia:'<?php echo $item['HospitalProcedencia'];?>',						
							Tamizaje:'<?php echo $Tamizaje;?>'
						
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}<?php */?>
		
		
		/*$('#dg').datagrid({
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){
			var dg =$('#dg').datagrid({data:getData()}).datagrid({			
			//var dg =$('#dg').datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});	*/		

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>