<html>
<head>
	<meta charset="UTF-8">
	<title>Consultas para todos</title>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css"> 
         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >			
			function ConsultarPostulantes(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarPostulantes&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";			
			}
			
			function ConsultarReceptores(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarReceptores&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";			
			}
			
			function ConsultarPacientesHemoglobinaMenor7(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarPacientesHemoglobinaMenor7&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";			
			}
			
			function ConsultarPacientesDebenSangre(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarPacientesDebenSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";			
			}
			
			function ConsultarMovimientosUnidadesAptas(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarMovimientosUnidadesAptas&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
			}
			
			function ConsultarHemocomponentes(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarHemocomponentes&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
			}
			
			function BuscarPacientesDebenSangre(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPacientesDebenSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
			}
			
		</script>            
        
</head>    
        
<body>

    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteExcel1&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:90%;;height:auto;"title="Banco de Sangre: CONSULTAS DEL SISTEMA BANCO DE SANGRE" iconCls="icon-search" align="left"> 	
              
       <table width="621">
            <tr>
              <td width="9">&nbsp;</td>
              <td width="368">&nbsp;</td>
              <td width="228">&nbsp;</td>
            </tr>                       
            <tr>
              <td>&nbsp;</td>
              <td>1. Listado de Postulantes</td>
              <td>
				<div class="buttons" align="center">
					<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarPostulantes();">Consultar</button> 			  
				</div>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>2. Listado de Receptores (Pacientes)</td>
              <td>
				<div class="buttons" align="center">
					<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarReceptores();">Consultar</button> 			  
				</div>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>3. Listado de Pacientes con hemoglobina <= 7</td>
              <td>
				<div class="buttons" align="center">
					<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarPacientesHemoglobinaMenor7();">Consultar</button> 			  
				</div>
			  </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>4. Listado de Pacientes en Hospitalización y Emergencia que DEBEN Sangre</td>
              <td>
              	<div class="buttons" align="center">
					<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarPacientesDebenSangre();">Consultar</button>
                </div>
              
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>5. Movimientos de unidades aptas</td>
              <td>
              	<div class="buttons" align="center">
					<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarMovimientosUnidadesAptas();">Consultar</button>
                </div>
              
              </td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td>6. Listado de Hemocomponentes por Tipo</td>
              <td>
              	<div class="buttons" align="center">
					<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarHemocomponentes();">Consultar</button>
                </div>
              
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>7. Buscar Pacientes deben Sangre</td>
              <td>
              	<div class="buttons" align="center">
					<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="BuscarPacientesDebenSangre();">Buscar</button>
                </div></td>
            </tr>
        </table>              
     	<br> 	
     </div>
</form>
      
 
  
      
</body>
</html>