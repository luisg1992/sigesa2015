<head>

<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/demo.css">
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/formulario.css">
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/calendario.css">
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.autocomplete.css" />
    
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/funciones2.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/calendar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/calendar-es.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/calendar-setup.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/FechaMasck.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery_enter.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src="../../MVC_Complemento/js/jquery.autocomplete.js"></script> 


 <script type="text/javascript" src="../../MVC_Complemento/js/jsalert/jquery.alerts.js"></script>
 <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/js/jsalert/jquery.alerts.css"/>
<script type="text/javascript">


$().ready(function() {
 
		  var IdPuntoCargaSer=document.formElem.IdPuntoCargaSer.value;
		  var idFuenteFinanciamiento=document.formElem.xidFuenteFinanciamiento.value;
		  var GeneraPago=document.formElem.xGeneraPago.value;
		  
  	  $("#BuscarCPT").autocomplete("../../MVC_Controlador/Facturacion/FacturacionC.php?acc=BuscarCPTCodigo&IdPuntoCarga="+IdPuntoCargaSer+"&idFuenteFinanciamiento="+GeneraPago, {
		width: 500, 
		matchContains: true,
			 
		//selectFirst: false
	 
	  }); 
	
	  
		
	$("#BuscarCPT").result(function(event, data, formatted) {
		$("#BuscarCPT").val(data[1]);
		$("#sql").val(data[0]);
		$("#codigo").val(data[1]);
		$("#descripcion").val(data[2]);
		$("#precio").val(data[3]);
		$("#IdProducto").val(data[4]);
 		//document.getElementById("precio").focus();
		//document.formElem.xvalor.focus();
		
		
 
	});
	
	/****Buscar Por Nombres **/
	 $("#descripcion").autocomplete("../../MVC_Controlador/Facturacion/FacturacionC.php?acc=BuscarCPTNombre&IdPuntoCarga="+IdPuntoCargaSer+"&idFuenteFinanciamiento="+GeneraPago, {
		width: 500, 
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",		 
		//selectFirst: false
	 
	  }); 
	
	  
		
	$("#descripcion").result(function(event, data, formatted) {
		$("#BuscarCPT").val(data[1]);
		$("#sql").val(data[0]);
		$("#codigo").val(data[1]);
		$("#descripcion").val(data[2]);
		$("#precio").val(data[3]);
		$("#IdProducto").val(data[4]);
 
	});
 
});
 
</script>

<!-- fin rusbel-->
 
<!-- inicion luis-->
 <script type="text/javascript">
//inicio de grilla

var INPUT_NAME_IdProducto = 'IdProducto';
var INPUT_NAME_PREFIX = 'codigo'; // this is being set via script a
var INPUT_NAME_DES = 'descripcion'; // this is being set via script b
var INPUT_NAME_PRE = 'precio'; // this is being set via script c
var INPUT_NAME_DSC = 'dscto'; // this is being set via script d
var INPUT_NAME_CAN = 'cantidad'; // this is being set via script e
var INPUT_NAME_IMP = 'imp'; // this is being set via script f



//var RADIO_NAME = 'totallyrad'; // this is being set via script
var TABLE_NAME = 'tblSample'; // this should be named in the HTML
var ROW_BASE = 0; // first number (for display)
var hasLoaded = false;
window.onload=fillInRows;
function fillInRows()
{
	hasLoaded = true;	
}
// CONFIG:
// myRowObject is an object for storing information about the table rows
function myRowObject(one,two,tres,cuatro,cinco,seis,siete,ocho)
{
	this.one = one; // text object
	this.two = two; // input text object
	this.tres=tres;
	this.cuatro=cuatro;
	this.cinco=cinco;
	this.seis=seis;
	this.siete=siete;
	this.ocho=ocho;

}

function insertRowToTable()
{
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAME);
		var rowToInsertAt = tbl.tBodies[0].rows.length;
		for (var i=0; i<tbl.tBodies[0].rows.length; i++) {
			if (tbl.tBodies[0].rows[i].myRow) {
				rowToInsertAt = i;
				break;
			}
		}
		addRowToTable(rowToInsertAt);
        reorderRows(tbl, rowToInsertAt);
	}
}


function addRowToTable(num)
{
	//alert('hola');
	//recupero el input del usuario
	var IdProducto=document.getElementById("IdProducto").value
	var codigo=document.getElementById("codigo").value
	var des=document.getElementById("descripcion").value
	var pre=document.getElementById("precio").value
	var dscto=document.getElementById("dscto").value
	var can=document.getElementById("cantidad").value
	var	valor=parseFloat(pre-dscto);	//si hubiera descuento
	var valor2=parseFloat(valor)*parseFloat(can);	//calcula importe (precio-descuento)* cantidad
	var imp=valor2;
	
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAME);
		var nextRow = tbl.tBodies[0].rows.length;
		var iteration = nextRow + ROW_BASE;
		if (num == null) { 
			num = nextRow;
		} else {
			iteration = num + ROW_BASE;
		}
		
		// add the row
		var row = tbl.tBodies[0].insertRow(num);
		
		// CONFIG: requires classes named classy0 and classy1
		row.className = 'classy' + (iteration % 2);
	
		// CONFIG: This whole section can be configured
		
		// cell 0 - text
		var cell0 = row.insertCell(0);
		var textNode = document.createTextNode(iteration);
		cell0.appendChild(textNode);
		
		// cell 1 - input text
		var cell1 = row.insertCell(1);
		var txtInpa = document.createElement('input');
		txtInpa.setAttribute('type', 'text');
		txtInpa.setAttribute('name', INPUT_NAME_PREFIX + iteration);
		txtInpa.setAttribute('id', INPUT_NAME_PREFIX + iteration);
		txtInpa.setAttribute('size', '7');
		txtInpa.setAttribute('value', codigo); // iteration included for debug purposes
		txtInpa.setAttribute('readonly', 'readonly');
		//txtInpa.setAttribute('class', 'texto'); 
		cell1.appendChild(txtInpa);
		
		
		var cell2 = row.insertCell(2);
		var txtInpb = document.createElement('input');
		txtInpb.setAttribute('type', 'text');
		txtInpb.setAttribute('name', INPUT_NAME_DES + iteration);
		txtInpb.setAttribute('id', INPUT_NAME_DES + iteration);
		txtInpb.setAttribute('size', '50');
		txtInpb.setAttribute('value', des); // iteration included for debug purposes txtInpf.setAttribute('value', dsc); // iteration included for debug purposes //readonly='readonly' class="texto"
		txtInpb.setAttribute('readonly', 'readonly'); // iteration included for debug purposes
		//txtInpb.setAttribute('class', 'texto'); 
		cell2.appendChild(txtInpb);
		
		
		
		var cell3 = row.insertCell(3);
		var txtInpe = document.createElement('input');
		txtInpe.setAttribute('type', 'text');
		txtInpe.setAttribute('name', INPUT_NAME_PRE + iteration);
		txtInpe.setAttribute('id', INPUT_NAME_PRE + iteration);
		 txtInpe.setAttribute('size', '5');
		txtInpe.setAttribute('value', pre); // iteration included for debug purposes
		//txtInpe.setAttribute('class', 'texto');
		 txtInpe.setAttribute('readonly', 'readonly'); 
		cell3.appendChild(txtInpe);
		
		// en caso descuento
		
		var cell4 = row.insertCell(4);
		var txtInpf = document.createElement('input');
		txtInpf.setAttribute('type', 'hidden');
		txtInpf.setAttribute('name', INPUT_NAME_DSC + iteration);
		txtInpf.setAttribute('id', INPUT_NAME_DSC + iteration);
		txtInpf.setAttribute('size', '5');
		txtInpf.setAttribute('value', dscto); // iteration included for debug purposes
		//txtInpf.setAttribute('class', 'texto'); 
		 txtInpf.setAttribute('readonly', 'readonly'); 
		cell4.appendChild(txtInpf);
		
		
		var cell5 = row.insertCell(5);
		var txtInpg = document.createElement('input');
		txtInpg.setAttribute('type', 'text');
		txtInpg.setAttribute('name', INPUT_NAME_CAN + iteration);
		txtInpg.setAttribute('id', INPUT_NAME_CAN + iteration);
		txtInpg.setAttribute('size', '5');
		txtInpg.setAttribute('value', can); // iteration included for debug purposes 
		//txtInpg.setAttribute('class', 'texto'); 
		// en caso queramos actualizar la cantidad //txtInpg.setAttribute('onkeyup','actualizar_importe(this.name)');
		//txtInpg.setAttribute('readonly', 'readonly'); 
		cell5.appendChild(txtInpg);
		
		
		var cell6 = row.insertCell(6);
		var txtInph = document.createElement('input');
		txtInph.setAttribute('type', 'text');
		txtInph.setAttribute('name', INPUT_NAME_IMP + iteration);
		txtInph.setAttribute('id', INPUT_NAME_IMP + iteration);
		txtInph.setAttribute('size', '5');
		txtInph.setAttribute('value', imp); // iteration included for debug purposes
		//txtInph.setAttribute('class', 'texto'); 
		 txtInph.setAttribute('readonly', 'readonly');
		cell6.appendChild(txtInph);
		
		
		// cell 2 - input button
		var cell7 = row.insertCell(7);
		var btnEl = document.createElement('input');
		btnEl.setAttribute('type', 'button');
		btnEl.setAttribute('value', 'X');
		btnEl.onclick = function () {deleteCurrentRow(this)};
		cell7.appendChild(btnEl);
		
		
		/*ifproducto*/
		var cell8 = row.insertCell(8);
		var txtIdPr = document.createElement('input');
		txtIdPr.setAttribute('type', 'hidden');
		txtIdPr.setAttribute('name', INPUT_NAME_IdProducto + iteration);
		txtIdPr.setAttribute('id', INPUT_NAME_IdProducto + iteration);
		txtIdPr.setAttribute('size', '5');
		txtIdPr.setAttribute('value', IdProducto); // iteration included for debug purposes
		//txtInph.setAttribute('class', 'texto'); 
		 txtIdPr.setAttribute('readonly', 'readonly');
		cell8.appendChild(txtIdPr);  
		
		
		row.myRow = new myRowObject(textNode,txtInpa,txtInpb,txtInpe,txtInpf,txtInpg,txtInph,txtIdPr);
	}
}


// If there isn't an element with an onclick event in your row, then this function can't be used.
function deleteCurrentRow(obj)
{
	if (hasLoaded) {
		var delRow = obj.parentNode.parentNode;
		var tbl = delRow.parentNode.parentNode;
		var rIndex = delRow.sectionRowIndex;
		var rowArray = new Array(delRow);
		deleteRows(rowArray);
		reorderRows(tbl, rIndex);
	  calculartotales();
	}
}

function reorderRows(tbl, startingIndex)
{
	if (hasLoaded) {
		if (tbl.tBodies[0].rows[startingIndex]) {
			var count = startingIndex + ROW_BASE;
			for (var i=startingIndex; i<tbl.tBodies[0].rows.length; i++) {
			
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.one.data = count; // text
				
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.two.name = INPUT_NAME_PREFIX + count; // input text
				tbl.tBodies[0].rows[i].myRow.tres.name = INPUT_NAME_DES + count;
				tbl.tBodies[0].rows[i].myRow.cuatro.name = INPUT_NAME_PRE  + count;
				
				tbl.tBodies[0].rows[i].myRow.cinco.name = INPUT_NAME_DSC  + count;
				tbl.tBodies[0].rows[i].myRow.seis.name = INPUT_NAME_CAN  + count;
				tbl.tBodies[0].rows[i].myRow.siete.name = INPUT_NAME_IMP  + count;

				var tempVal = tbl.tBodies[0].rows[i].myRow.two.value.split(' '); 
//aplica stylo de tabla
				tbl.tBodies[0].rows[i].className = 'classy' + (count % 2);
				
				count++;
				
			}
		}
	}
}

function deleteRows(rowObjArray)
{
	if (hasLoaded) {
		for (var i=0; i<rowObjArray.length; i++) {
			var rIndex = rowObjArray[i].sectionRowIndex;
			rowObjArray[i].parentNode.deleteRow(rIndex);
		}
	}
}


function sumarcolumnatabla(){
sumar=0;
calculo=0;
var hc=document.getElementById('xc_rh').value; //con igv o sin igv controla si es con igv o no
var iniciost=document.getElementById("st").value; //importe
var dscto3=document.getElementById("dsctof").value; //descuentofinal
var xsub=document.getElementById("sub").value; //subtotal
var ig2=document.getElementById("igv").value;
var inicio=document.getElementById("bi").value; //total
var dscto2=document.getElementById("dscto").value; //captura el descuento ingresado
var tot=parseFloat(document.getElementById("precio").value)*parseFloat(document.getElementById("cantidad").value);	
//descuentos
var v1=dscto2/100;
var des=parseFloat(tot)*(1+v1); ///total*1.dscto
var de=parseFloat(des)-parseFloat(tot); //monto de descuento
var subdes=de+parseFloat(dscto3);

//subtotales
var subt=tot+parseFloat(iniciost);
//var subt2=subt-subdes;
var subt2=subt-subdes;
//igv
if(hc==0){ //sin importacion o sin check
var igv=parseFloat(subt2)*1.18;
}else{ // importacion o con check
var igv=parseFloat(subt2)*1;	
}
var ig=parseFloat(igv)-parseFloat(subt2);
var subigv=ig;//+parseFloat(ig2);

//total
var total=subt2+subigv;

	document.getElementById("st").value=((Math.floor(subt*100))/100).toFixed(2); 
	document.getElementById("dsctof").value=((Math.floor(subdes*100))/100).toFixed(2); 
	document.getElementById("sub").value=((Math.floor(subt2*100))/100).toFixed(2); 
	document.getElementById("igv").value=((Math.floor(subigv*100))/100).toFixed(2);
	document.getElementById("bi").value=((Math.floor(total*100))/100).toFixed(2); 
	

}


function calculartotales(){
var hc=document.getElementById('xc_rh').value;
sumar=0;
igv=0;
total=0;
descu=0;
subt=0;
des1=0;
v2=0;
de=0;
for(var i=1; i<=50; i++)
{
if(!document.getElementById("imp"+i)){
}else{
	
	//descuentos
	
	
sumar+=parseFloat(document.getElementById("imp"+i).value);
descu+=parseFloat(document.getElementById("dscto"+i).value); //30 el descuento no se acumula
var v2=descu/100;
var des1=parseFloat(sumar)*(1+v2);
var de=parseFloat(des1)-parseFloat(sumar);
//numero.toFixed(2);
var subt=sumar-de;
if(hc==0){
 ig1=subt*1.18;
}else{
	 ig1=subt*1;
}
var igv=ig1-subt;
var total=subt+igv;
}
}
limpiatotales();
document.getElementById("st").value=((Math.floor(sumar*100))/100).toFixed(2); 
document.getElementById("dsctof").value=((Math.floor(de*100))/100).toFixed(2);
document.getElementById("sub").value=((Math.floor(subt*100))/100).toFixed(2);  
document.getElementById("igv").value=((Math.floor(igv*100))/100).toFixed(2);
document.getElementById("bi").value=((Math.floor(total*100))/100).toFixed(2); 
}	

function limpiatotales(){
	
document.getElementById("st").value='';
document.getElementById("dsctof").value='';
document.getElementById("sub").value='';
document.getElementById("igv").value='';
document.getElementById("bi").value='';
	
}

function validadcodigo(){
var	codigo=document.getElementById("codigo").value;
var theTable = document.getElementById('tblSample');
var	cantFilas = theTable.rows.length;
 document.getElementById("TotalFilas").value=cantFilas;
 if(document.formElem.codigo.value.length!=0  ){
				if(cantFilas==1){
						//alert('addciona fila 1');
						addRowToTable();	
				}
				
				
				for(i=1; i<=cantFilas; i++){
					
					var xcodigo=document.getElementById("codigo"+i).value;
					
					if(codigo == xcodigo){
						  sw=1;
						break;
						}else{
						  sw=2;
						//addRowToTable();
						break;
						}
					//alert('sw'+sw+'codigo'+xcodigo);
				}
					
					if(sw==2){
						addRowToTable();
						}else{
						//	alert('sw'+sw+'codigo'+xcodigo);
							}

   }

}

function accionagregar(){
var codigo=document.formElem.codigo.value;
if(codigo!=''){	
	validadcodigo();
	sumarcolumnatabla();
	document.formElem.BuscarCPT.value="";
	document.formElem.descripcion.value="";
	document.formElem.codigo.value="";
	document.formElem.precio.value="";
	document.formElem.BuscarCPT.focus();
	}else{
	alert('Buscar CPT por Codigo o Descripcion.');
	document.formElem.BuscarCPT.focus();	
		}
}
	
	
 /*	function ValidarNocoverturados(){
		
		document.getElementById("idFuenteFinanciamiento").value='';
	    document.getElementById("GeneraPago").value='';
		 
	 }*/
	 
	 
function CargarInformacion(){
	var IdPaciente=document.getElementById("IdPaciente").value;
	if (IdPaciente==''){
	document.getElementById("NroHistoriaClinica").focus();
	}else{
	document.getElementById("NroHistoriaClinica").focus();
	}
	 
	} 
	
	
function Cerrar(){		
		  location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListaEspera&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
		
} 

function Guardar(){
	/*codigo1
	IdProducto1
	IdCuentaAtencion*/
	
	var theTable = document.getElementById('tblSample');
	var	cantFilas = theTable.rows.length;
	 document.getElementById("TotalFilas").value=parseInt(cantFilas)-1;
	calculartotales();
	if (document.formElem.IdCuentaAtencion.value.length==0){
		alert("Ingresar Numero de Cuenta.");
		document.formElem.IdCuentaAtencion.focus();
		return 0;
	}
	if (document.formElem.IdProducto1.value.length==0){
		alert("Registra Almenos un Servico");
		//document.formElem.PAC_APATERNO.focus();
		return 0;
	}
	if (document.formElem.codigo1.value.length==0){
		alert("Registra Almenos un Servico");
		//document.formElem.PAC_AMATERNO.focus();
		return 0;
	}
	
	confirmar=confirm("¿Seguro que desea Guardar las Ordenes de Facturación?");
       if (confirmar){
          document.formElem.submit();
		} 
	
	} 
	


function CambiarPuntoCarga(){
	
   var posicion=document.getElementById('IdServicioIngreso').options.selectedIndex; //posicion
   var texto=document.getElementById('IdServicioIngreso').options[posicion].text
   
    var elem = texto.split('-');
    var puntoCargar = elem[0];
	var textosinespacio=puntoCargar.trim() ;
	document.getElementById('IdPuntoCargaSer').value=textosinespacio;
   
	}
function CargarPaquete(){

var idpaquete=document.formElem.paquetes.options[document.formElem.paquetes.selectedIndex].value;

document.formElem.idpaquete.value=idpaquete;


var IdCuentaAtencion=document.getElementById("IdCuentaAtencion").value;
var NroHistoriaClinica=document.getElementById("NroHistoriaClinica").value;
var NroDocumento=document.getElementById("NroDocumento").value;

if(IdCuentaAtencion!=''){
	valorbuscar=IdCuentaAtencion;
}else if(NroHistoriaClinica!=''){
	valorbuscar=NroHistoriaClinica;
	
	}else if(NroDocumento!=''){
	valorbuscar=NroDocumento;
	}


var IdEmpleado=document.getElementById("IdEmpleado").value;
var AbirCuenta='SI';

calculartotales();

 location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FacturacionCargarCatalogos&idpaquete="+idpaquete+"&IdCuentaAtencion="+IdCuentaAtencion+"&IdEmpleado="+IdEmpleado+"&valorbuscar="+valorbuscar+"&pivotbuscar=2&FechaDespacho=<?php echo $_GET['FechaDespacho'] ?>&HoraDespacho=<?php echo $_GET['HoraDespacho'] ?>&SNCSB=<?php echo $SNCSB ?>&TipoHemB=<?php echo $TipoHemB ?>";




	} 
	

	</script>   


 </head>
 
 <body > <!--onLoad="calculartotales()"-->

 <form id="formElem" name="formElem"  method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarConsumodeServicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">
 
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>Datos de Cabecera</strong></legend>
    <table width="955" height="197" border="0">
	<tr>
		<td><div class="buttons" align="center">  
		    
		  <a "#" onClick="Cerrar();" class="negative">Cancelar</a>      
		  <button type="button" class="positive" name="update" onClick="Guardar();">Guardar</button>
		  
		</div></td>
    </tr>   
    <tr>
      <td width="535" height="193" bgcolor="#F7F7F7"><fieldset>
        <legend>Buscar</legend>
        <table width="507" border="1">
          <tr>
          
        
          
          
            <td width="116">Nº Hist.Clini: </td>
            <td width="79"><input name="NroHistoriaClinica" type="text" id="NroHistoriaClinica"  value="<?php echo $NroHistoriaClinica;?>" size="15" readonly /></td>
            <td width="290"><font size="+4"  color="#000099"><strong><?php echo $Paciente;?></strong></font></td>
          </tr>
          <tr>
            <td><a href="javascript:void(0)" >Nº Cuenta:</a></td>
            <td><input name="IdCuentaAtencion" type="text" id="IdCuentaAtencion" value="<?php echo $IdCuentaAtencion;?>" size="15" readonly /></td>       

            
            
           
            
            
            </tr>
          <tr>
            <td>Nº DNI:</td>
            <td><input name="NroDocumento" type="text" id="NroDocumento" value="<?php echo $NroDocumento;?>" size="15" readonly /></td>
            <td ><font   color="#000000" size="+2"><strong>
               
               <?php if($valorbuscar==NULL){?>
              <input name="valorbuscar" type="hidden" id="valorbuscar" value="<?php echo $NroHistoriaClinica;?>">
              <input name="pivotbuscar" type="hidden" id="pivotbuscar" value="2">               
             
              <?php }else{?>

               <input name="valorbuscar" type="hidden" id="valorbuscar" value="<?php echo $valorbuscar;?>">
              <input name="pivotbuscar" type="hidden" id="pivotbuscar" value="<?php echo $pivotbuscar;?>">
              
              <?php }?>                           
              
            </strong></font></td>
            </tr>
            
            <tr>
            <td>Fecha Despacho:</td>
            <td>
<input name="FechaDespacho" type="text" id="FechaDespacho" value="<?php echo $FechaDespacho; ?>" size="15" readonly />
<input name="FechaRegistro" type="hidden" id="FechaRegistro" value="<?php echo vfechahora($FechaServidor); ?>" size="15" /></td>
            <td></td>
            </tr>
            
          <tr>
            <td colspan="3" align="right" valign="top"><select name="IdServicioIngreso" id="IdServicioIngreso"  style="width:300px; right:auto;border:1px solid #04467E; font-size:12px" onChange="CambiarPuntoCarga()" >
              <option value=""> Procedencia :</option>
              <?php 
				if($ListarSigesDevuelveServicios != NULL)	{ 
				foreach($ListarSigesDevuelveServicios as $item41){?>
              <option value="<?php echo $item41["IdServicio"]?>"<?php if($item41["IdServicio"]==$IdServicioIngreso){?>selected<?php }?>><?php echo $item41["IdPuntoCarga"]?> - <?php echo $item41["Nombre"]?> - <?php echo $item41["DservicioHosp"]?></option>
              <?php }	}?>
              </select>
              
              <?php /*?><input type="hidden" name="xIdPuntoCarga" id="xIdPuntoCarga" value="<?php echo $idpaquete ?>" /><?php */?> <!--PROPUESTO-->              
              <input type="hidden" name="xIdPuntoCarga" id="xIdPuntoCarga" value="1" /> <!-- ACTUAL (1 CONSUMO EN EL SERVICIO, 11 BANCO DE SANGRE)--> 
                     
              <input type="hidden" name="xidFuenteFinanciamiento" id="xidFuenteFinanciamiento" value="<?php echo $idFuenteFinanciamiento; ?>">
              
              <input type="hidden" name="xGeneraPago" id="xGeneraPago" value="<?php echo $GeneraPago; ?>">
              </td>
            </tr>
          </table>
        <input type="hidden" name="sql" id="sql" />
        <span style="color:#930">
          <input name="TotalFilas" type="hidden" id="TotalFilas" size="10" />
          <input name="IdEmpleado" type="hidden" id="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'];?>" size="10" />
          <input name="dato03" type="hidden" id="dato03" size="10" />
          <input name="dato04" type="hidden" id="dato04" size="10" />
          <input name="IdPuntoCargaSer" type="hidden" id="IdPuntoCargaSer" value="<?php echo $IdPuntoCarga; ?>" />
          
          </span>
        
        <input name="Paciente" type="hidden" id="Paciente" value="<?php echo $Paciente;?>" size="35" readonly />
        <input name="FinanciamientoPaciente" type="hidden" id="FinanciamientoPaciente" value="<?php echo $FinanciamientoPaciente;?>" size="35" readonly />
        <input name="IdPaciente" type="hidden" id="IdPaciente" value="<?php echo $IdPaciente;?>" />
        <span style="color:#930">          
        </fieldset>
        <table width="317" border="0" align="center">
 		  <tr>
			<td></td>   
            <td><strong>Hemocomponente: <?php echo $TipoHemB ?>-<?php echo $SNCSB ?></strong><input name="IdReserva" type="hidden" id="IdReserva" value="<?php echo $_REQUEST['IdReserva'] ?>" /></td>           
		  </tr>
          	
		  <tr>
			<td colspan="2">					
			  <?php 
			  if($GeneraPago!=NULL){
			  $ListaCatalago= FactCatalogoPaqueteSeleccionarTodos($GeneraPago,'1');
			  if($ListaCatalago !=NULL){
			  ?>
              	  <input name="idpaquete" type="hidden"  id="idpaquete" value="<?php echo $idpaquete ?>" size="5">
				 
				  <select name="paquetes" id="paquetes" onChange="CargarPaquete()">
				  <option value="0">[Seleccionar]</option>
				  <?php 
				  
				  foreach($ListaCatalago as $ItemCatalogo){?>
				  <option value="<?php echo $ItemCatalogo["idFactPaquete"]?>" <?php if($ItemCatalogo["idFactPaquete"]==$idpaquete){?>selected<?php }?> > <?php echo $ItemCatalogo["Codigo"]?> - <?php echo (mb_strtoupper($ItemCatalogo["Descripcion"]))?></option>
				  <?php }?>
				</select>
						  <?php }}?>
				
			   </td>
              
		  </tr>
		 </table>
        </td>
      
    </tr>
  
    </table>
</fieldset>
<fieldset class="fieldset legend">
    
    <legend style="color:#03C"><strong>Detalle <?php echo $DescripcionPaquete; ?></strong></legend> 
    
    <table width="955" border="0">
      <tr>
        <td colspan="2">Buscar</td>
        <td>Codigo</td>
        <td>Precio</td>
        <td>Cantidad</td>
        <td></td>
      </tr>
      <tr>
        <td width="48"><input name="BuscarCPT" type="text"  id="BuscarCPT"   size="10"  onchange="accionagregar()" onFocus="calculartotales()"    /></td>
        <td width="348"><input name="descripcion" type="text" id="descripcion" size="45" onChange="accionagregar()"   /></td>
        <td width="90"><input name="codigo" type="text"  id="codigo" size="10" readonly  /></td>
        <td width="75"><input name="precio" type="text" id="precio"  size="10" readonly/></td>
        <td width="70"><input name="cantidad" type="text" id="cantidad"   onkeyup="actualizar_importe(event)" value="1" size="5" readonly /></td>
        <td> <input type="button" name="add" id="add" value="Agregar" onClick="accionagregar()"/> 
          <input name="dscto" type="hidden" id="dscto"    value="0" size="5" readonly />
          <input name="xc_rh" type="hidden" id="xc_rh" value="1" />
          <input type="hidden" name="xvalor" id="xvalor"  onfocus="accionagregar();" size="1" />
          <input name="imp" type="hidden" id="imp" value="0" size="5" />
        <input name="impf" type="hidden" id="impf" value="0" size="5"/>
        <span style="color:#930">
        <input name="IdProducto" type="hidden" id="IdProducto" size="10"  value="0"/>
        </span></td>
      </tr>
    </table>
    <table width="848" border="1" cellpadding="1" cellspacing="1" id="tblSample">
      <tr>
        <td width="23">Nro</td>
        <td width="73">Codigo</td>
        <td width="320">Descripcion</td>
        <td width="111">Precio</td>
        <td width="8">&nbsp;</td>
        <td width="93">Cantidad</td>
        <td width="78">Importe</td>
        <td width="103">Quitar</td>
      </tr>
       <?php 
		error_reporting(0);							 
							
		if($listaPaquetes != NULL)
		{		
			$i = 1;			
			
			$sumar=0;$igv=0;$total=0;$descu=0;$subt=0;$des1=0;$v2=0;$de=0;//INICIALIZAR
			foreach($listaPaquetes as $itemD)
			{
				//$total+=$itemD["n_totimp"];
				//TOTALES				
					$sumar+=number_format($itemD['Importe'],2);
					
					//Descuento
					$descu+=0; //30 el descuento no se acumula
					$v2=$descu/100;
					$des1=$sumar*(1+$v2);
					$de=($des1)-$sumar;				
					$subt=$sumar-$de;
					//igv
					$ig1=$subt*1;				
					$igv=$ig1-$subt;
					$total=$subt+$igv;
				
					//Obteniendo variables del input			
					$st=$sumar; //				
					$dsctof=$de;//
					$sub=$subt; // 
					$igv=$igv;//
					$bi=$total; //
	
		?>
      <tr>
        <td>
        <input name="<?php echo 'IdProducto'.$i ?>" type="hidden" id="<?php echo 'IdProducto'.$i ?>" value="<?php echo $itemD['IdProducto'] ?>" size="4" /><?php echo $i ?></td>
        <td><input name="<?php echo 'codigo'.$i ?>" type="text" id="<?php echo 'codigo'.$i ?>" value="<?php echo $itemD['Codigo'] ?>"size="7" readonly /></td>
        <td><input name="<?php echo 'descripcion'.$i ?>" type="text" id="<?php echo 'descripcion'.$i ?>" value="<?php echo $itemD['Procedimiento'] ?>" size="50" readonly /></td>
        <td><input name="<?php echo 'precio'.$i ?>" type="text" id="<?php echo 'precio'.$i ?>" value="<?php echo number_format($itemD['Precio'],2) ?>" size="5" readonly /></td>
        <td><input name="<?php echo 'dscto'.$i ?>" type="hidden" id="<?php echo 'dscto'.$i ?>" value="0.00" size="5" /></td>
        <td><input name="<?php echo 'cantidad'.$i ?>" type="text" id="<?php echo 'cantidad'.$i ?>" value="<?php echo $itemD['Cantidad'] ?>" size="5" readonly /></td>
        <td><input name="<?php echo 'imp'.$i ?>" type="text" id="<?php echo 'imp'.$i ?>" value="<?php echo number_format($itemD['Importe'],2) ?>" size="5" readonly /></td>
        <td>&nbsp;</td>
      </tr>
      <?php $i++; }}?>
    </table>
     
    
     
</fieldset>
<fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>Totales</strong></legend><table width="955" border="0">
  <tr>
    <td width="260" align="center">&nbsp;</td>
    <td align="center"><strong></strong><strong></strong>
      <input type="hidden" name="xsum" id="xsum" />
      <input name="dsctof" type="hidden" class="texto" id="dsctof" value="<?php echo $dsctof ?>" size="10" readonly/>
      <input name="igv" type="hidden" id="igv" value="<?php echo $igv ?>" size="10" readonly />      
      <input name="sub" type="hidden" class="texto" id="sub" value="<?php echo $sub ?>" size="10" readonly/>
      <strong>Sub Total</strong>
      <input name="st" type="text" id="st" value="<?php echo $st ?>" size="10" readonly onFocus="actualizar_importe()" /></td>
    <td width="326" align="center"><strong>Total
          <input name="bi" type="text" id="bi" value="<?php echo $bi ?>" size="10" readonly  onblur="actualizar_importe()" />
    </strong>
     </td>
  </tr>
  </table>
</fieldset>
<br>

 <fieldset class="fieldset legend">
    <legend style="color:#03C"> </legend>
   <table width="955" border="0" align="center">
 
  <tr>
    <td height="36" align="center" valign="middle" >
    </td>
  </tr>
  </table>  
        
</fieldset> 

</form>

<div id="DetalleOrden">
	<iframe id="Panet_Datos" name="Panet_Datos" width="0" height="0" frameborder="0">
	  <ilayer width="0" height="0" id="Panet_Datos" name="Panet_Datos">
	  </ilayer>
	</iframe>
</div>
          
 </body>         