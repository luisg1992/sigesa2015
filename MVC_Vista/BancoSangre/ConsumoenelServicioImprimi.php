<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/imprimir.css"/>
<title></title>
 
<style type="text/css">
.botonExcel{cursor:pointer;}

@media all {
   div.saltopagina{
      display: none;
   }
}
   
@media print{
   div.saltopagina{
      display:block;
      page-break-before:always;
   }
} 
</style>
<style type="text/css" media="print">
.nover {display:none}
</style>
</head>
<body  onLoad="window.print();">
<ul class="pro15 nover">
<li><a href="#nogo" onClick="window.print();" class="nover"><em class="home nover"></em><b>Imprimir</b></a></li>
 
<?php /*?><li><a href="../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ConsumoenelServicioAgregar&IdEmpleado=<?php echo $IdEmpleado; ?>"   class="nover" >
<em class="find nover"></em><b><< Nuevo </b></a>
</li>
<li><a href="../../MVC_Controlador/Facturacion/FacturacionC.php?acc=FacturacionBuscarCuentas&IdCuentaAtencion=<?php echo $IdCuentaAtencion; ?>&IdEmpleado=<?php echo $IdEmpleado; ?>"   class="nover" >
<em class="find nover"></em><b><< Mismo Paciente </b></a>

</li><?php */?>
 
<li><a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListaEspera&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"   class="nover" >
<em class="find nover"></em><b><< Regresar </b></a>
</li>

</ul>
 <?php
if($ListarFacturacionServicioPagos != NULL)	{ 
		       $i=0;
	foreach($ListarFacturacionServicioPagos as $item){
?>
<table width="425" border="0" cellpadding="0" cellspacing="0">
 <tr>
   <td colspan="5">&nbsp;</td>
 </tr>
 <tr>
   <td colspan="5" align="center"><font face="Calibri" size="+1">Nº Ord.&nbsp;&nbsp; <?php echo $idOrden; ?></font></td>
 </tr>
 <tr>
   <td colspan="5"><font face="Calibri" >H.C: <?php echo $NroHistoriaClinica;  ?>&nbsp;&nbsp; <?php echo $ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre; ?> -
     <?php $TIPOEdad= Atencion_Edad_IdCuentaAtencion_M($IdCuentaAtencion); echo $TIPOEdad[0][0].' '.$TIPOEdad[0][1]; ?>
   </font></td>
 </tr>
 <tr>
   <td colspan="5"><font face="Calibri" >Procedencia: <?php echo $dservicio; ?></font></td>
 </tr>
 <tr>
   <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tr>
       <td width="13%"><font face="Calibri" size="2" >N° Cuen.</font></td>
       <td width="12%"><font face="Calibri" ><?php echo $IdCuentaAtencion; ?></font></td>
      
       <td width="40%">&nbsp;&nbsp;<font face="Calibri" size="2">
         <?php if($idFuenteFinanciamiento==1 or $idFuenteFinanciamiento==5){?>
         <strong>N° OrPg.</strong>
<?php }?>
       </font>
       
       
       <font face="Calibri" > <?php echo $idOrdenPago; ?> 
        <?php 
         $dfinanciamientoX=isset($item["dfinanciamiento"]) ? $item["dfinanciamiento"] : '';
                
         if($dfinanciamientoX==''){ echo ' Par.';
         }else{ echo $item["dfinanciamiento"];} ?>
           
       </font></td>
       
       <td width="35%"><font face="Calibri" >F.R. <?php echo $FechaCreacion; ?></font></td>
     </tr>
   </table></td>
 </tr>
 <tr>
   <td>&nbsp;</td>
   <td  >&nbsp;</td>
   <td align="center">&nbsp;</td>
   <td align="center">&nbsp;</td>
   <td align="center">&nbsp;</td>
 </tr>
 <tr>
   <td width="4">&nbsp;</td>
   <td width="57"  ><font face="Calibri" size="2" >Codigo</font></td>
   <td width="242" align="center"><font face="Calibri" size="2"  >Descripcion</font></td>
   <td width="46" align="center"><font face="Calibri" size="2"  >Cant.</font></td>
   <td width="81" align="right"><font face="Calibri" size="2"  >Importe</font></td>
 </tr>
 <tr>
   <td>&nbsp;</td>
   <td rowspan="3"   valign="top"><font face="Calibri" ><?php echo $item["Codigo"]; ?></font></td>
   <td rowspan="3" valign="top"  ><font face="Calibri" size="2" ><?php echo $item["Nombre"]; ?></font></td>
   <td rowspan="3" align="center" valign="top"><font face="Calibri" ><?php echo $item["Cantidad"]; ?></font></td>
   <td rowspan="3" align="right" valign="top"><font face="Calibri" ><?php echo number_format($item["Total"], 2, '.', ' '); ?></font></td>
 </tr>
 <tr>
   <td>&nbsp;</td>
 </tr>
 <tr>
   <td>&nbsp;</td>
 </tr>
 
 <tr>
   <td>&nbsp;</td>
   <td><font face="Calibri" >Termina:</font></td>
   <td><font face="Calibri" ><?php echo substr($NombrePC,0,10); ?></font>  <font face="Calibri" >Usuario: <?php echo $Usuario;?></font></td>
   <td align="right"><font face="Calibri" >Monto</font></td>
   <td align="right"><font face="Calibri" >S/. <?php echo number_format($item["Total"], 2, '.', ' '); ?></font></td>
 </tr>
 <tr>
   <td>&nbsp;</td>
   <td colspan="3" align="center"> <font face="Calibri" size="+2" > PAGO PENDIENTE -   <?php if($dfinanciamientoX==''){ echo 'Particular';}else{ echo $item["dfinanciamiento"];} ?></font></td>
   <td align="right"><font face="Calibri" >S/. 0.00
     <?php // if($idFuenteFinanciamiento==1 || $idFuenteFinanciamiento==5){?>
     <?php //echo number_format($item["Total"], 2, '.', ' '); ?>
     <?php //}else{?>
     
     <?php //}?>
   </font></td>
 </tr>
 <tr>
   <td colspan="5" align="center"><font face="Calibri" size="+1">H.C:<?php echo $NroHistoriaClinica;  ?> &nbsp;&nbsp; <?php echo $ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre; ?></font> - <font face="Calibri" >
     <?php $TIPOEdad= Atencion_Edad_IdCuentaAtencion_M($IdCuentaAtencion); echo $TIPOEdad[0][0].' '.$TIPOEdad[0][1]; ?>
   </font></td>
 </tr>
</table>
<?php  
//if($i>1)  {
?>
<div class="saltopagina"></div>
<?php // }  ?>


<?php    }
	   } ?>