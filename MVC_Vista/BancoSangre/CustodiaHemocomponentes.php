<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Custodia Hemocomponentes</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:10px;
            }			
            .fitem label{
                display:inline-block;
                width:90px;
							
            }
            .fitem input{
				display:inline-block;
                width:100px;	
				margin-left:10px;			
            }					
			
	</style> 
    
<script type="text/javascript" >			
		
		$.extend($("#FecMotivo").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FecMotivo").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});					
		
		function regresar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=StockSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}
		
		function cargar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=CustodiaHemocomponentes&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
		}
		
		function Buscar(){
			document.getElementById("form1").submit();
		}			
	
    </script>
</head>
<body>

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->
	<div style="margin:0px 0;"></div>
	 <form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=CustodiaHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">  
            	<!--<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'" onClick="Buscar()">Volver a Cargar</a> -->               	 
		        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="CustodiaHemocomponente()">Custodia Hemocomponente</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="IngresarResultado()" >Resultado Investigación</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="VerCustodia()" >Ver Custodia</a>                             
                <!--<!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="Imprimir();">Imprimir</a>	
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="Ayuda();">Ayuda</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a> 
			</div>
            
              <div>
               <strong>Busqueda por Hemocomponente:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
                   <select name="TipoHemB" id="TipoHemB" class="easyui-combobox" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }">
                        <option value="T">Todos</option>
                        <option value="PG" <?php if($TipoHem=='PG'){?> selected <?php } ?> >PG </option>
                        <option value="PFC" <?php if($TipoHem=='PFC'){?> selected <?php } ?> >PFC </option>
                        <option value="PQ" <?php if($TipoHem=='PQ'){?> selected <?php } ?>>PQ </option>
                        <option value="CRIO" <?php if($TipoHem=='CRIO'){?> selected <?php } ?>>CRIO </option>
                    </select>-<input id="SNCSB"  name="SNCSB" type="text" class="easyui-textbox" maxlength="7" value="<?php echo $SNCS ?>" data-options="prompt:'SNCS',
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = document.form1.submit(); }" />        
                                         
                      <label for="BuscarSNCS" onClick="CambiarBuscar();" >Buscar por SNCS</label>
                      <input type="radio" name="EstadoB" id="BuscarSNCS" value="V" <?php if(trim($EstadoB)=="V"){?> checked <?php } ?> onClick="CambiarBuscar();" /> &nbsp;&nbsp;&nbsp;
                       <label for="Custodia" >Todos en Custodia</label>
                      <input type="radio" name="EstadoB" id="Custodia" value="4" <?php if(trim($EstadoB)=="4"){?> checked <?php } ?> /> &nbsp;&nbsp;&nbsp; 
                          
                      <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>
                </div>
            			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Custodia de Hemocomponentes" style="width:1300px;height:450px" data-options="
				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	<th field="NroDonacion" width="80">N°Donacion</th>
				<th field="TipoHem"  width="100">Hemocomponente</th>				
                <th field="NuevoSNCS"  width="90">SNCS</th>	
				<th field="GrupoSanguineo"  width="80">GS</th>
                <th field="VolumenRestante"  width="100">Volumen</th>
				<th field="NroTabuladora" width="80">tubuladura</th>
                <th field="FechaExtraccion" width="100">F. Extracción</th>
                <th field="FechaVencimiento" width="100">F. Vencimiento</th>
                <th field="Tiempo" width="200">Tiempo</th>
                <th field="EstadoDescripcion" width="120">Estado</th>
                <!--<th field="VerDatosCustodia" width="100">Ver Custodia</th>-->
			</tr>
		</thead>
	</table>
     </form>
    

<!--FORMULARIO CUSTODIA-->
 <div id="dlg-CabHemocomponente" class="easyui-dialog" style="width:760px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmCabHemocomponente" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarCustodiaHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">
                    <input type="hidden" name="MovNumero" id="MovNumero" />
                    <label>Tipo Hemoc:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" readonly>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                           
                    <label>SNCS:</label>
                    <input id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" data-options="prompt:'SNCS'" readonly />                     
                    <label>Motivo:</label>                    
                    <Select style="width:200px" class="easyui-combobox" id="MotivoEstado" name="MotivoEstado" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarMotivoElimina=ListarMotivoElimina('CUSTO');
                                           if($ListarMotivoElimina != NULL) { 
                                             foreach($ListarMotivoElimina as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select>                                    
              </div>   
              
              <div class="fitem">
              		 <label>Responsable:</label>
                    <Select style="width:200px" class="easyui-combobox" id="UsuMotivo" name="UsuMotivo" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>                    
                    <label>Fec.Custodia:</label>                   
                    <input name="FecMotivo" id="FecMotivo" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />            		<input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraMotivo" name="HoraMotivo" data-options="showSeconds:true,required:true" style="width:85px" />
                          
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveCustodiaHemocomponente();" style="width:90px">Custodia</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponente').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO CUSTODIA-->

 <!--FORMULARIO RESULTADO-->
 <div id="dlg-CabHemocomponenteCrio" class="easyui-dialog" style="width:760px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmIngresarResultado" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarResultadoCustodia&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">              
                	<input type="hidden" name="MovNumero2" id="MovNumero2" />  
                    <label>Tipo Hemoc:</label>                    
                    <select name="TipoHem2" id="TipoHem2" class="easyui-combobox" readonly >
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                           
                    <label>SNCS:</label>                    
                    <input id="SNCS2"  name="SNCS2" type="text" class="easyui-textbox" maxlength="10" data-options="prompt:'SNCS'" readonly />
                   
                   <label>Motivo:</label>                    
                   <Select style="width:200px" class="easyui-combobox" id="MotivoEstado2" name="MotivoEstado2" data-options="prompt:'Seleccione',required:true" readonly>
                      <option value=""></option>
                      <?php
                                          $ListarMotivoElimina=ListarMotivoElimina('CUSTO');
                                           if($ListarMotivoElimina != NULL) { 
                                             foreach($ListarMotivoElimina as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select>                    
              </div>   
              
              <div class="fitem">
              		 <label>Responsable:</label>
                    <Select style="width:200px" class="easyui-combobox" id="UsuResultado" name="UsuResultado" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select> 
                    <label>Fec.Resultado:</label>                   
                    <input name="FecResultado" id="FecResultado" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />            		<input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraResultado" name="HoraResultado" data-options="showSeconds:true,required:true" style="width:85px" />    
                   
              </div> 
              
              <div class="fitem2">
              		<label>Estado:</label>                      
                      <label for="Eliminar">Eliminar</label>
                      <input type="radio" name="Estado2" id="Eliminar" value="0" checked /> &nbsp;&nbsp;&nbsp;
                      <label for="stock">A stock</label>
                      <input type="radio" name="Estado2" id="stock" value="1" /> &nbsp;&nbsp;&nbsp; 
                      
                    <label>Resultado:</label>                    
                    <input style="width:200px;height:50px" class="easyui-textbox" multiline="true" name="ResultadoCustodia" id="ResultadoCustodia" />
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveResultado();" style="width:90px">Registrar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponenteCrio').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO RESULTADO--> 
  
  
<!--FORMULARIO VER CUSTODIA-->
 <div id="dlg-VerCustodia" class="easyui-dialog" style="width:760px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmVerCustodia" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarCustodiaHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">
                    <label>Tipo Hemoc:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" disabled>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                           
                    <label>SNCS:</label> 
                    <input id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" data-options="prompt:'SNCS'" disabled />                     
                    <label>Motivo:</label>                    
                  <Select style="width:200px" class="easyui-combobox" id="MotivoEstado3" name="MotivoEstado3" disabled>
                      <option value=""></option>
                      <?php
                                          $ListarMotivoElimina=ListarMotivoElimina('CUSTO');
                                           if($ListarMotivoElimina != NULL) { 
                                             foreach($ListarMotivoElimina as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select>                                    
              </div>   
              
              <div class="fitem">
              		 <label>Resp.Custodia:</label>
                    <Select style="width:200px" class="easyui-combobox" id="UsuMotivo3" name="UsuMotivo3" readonly >
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>                    
                    <label>Fec.Custodia:</label>                   
                    <input name="FecMotivo3" id="FecMotivo3" class="easyui-datebox" style="width:105px" readonly />            		
                    <input class="easyui-timespinner" id="HoraMotivo3" name="HoraMotivo3" style="width:85px" data-options="showSeconds:true" readonly />                         
              </div> 
              
              <div class="fitem">
              		 <label>Resp.Resultado:</label>
                    <Select style="width:200px" class="easyui-combobox" id="UsuResultado3" name="UsuResultado3" readonly >
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select> 
                    <label>Fec.Resultado:</label>                   
                    <input name="FecResultado3" id="FecResultado3" class="easyui-datebox" style="width:105px" readonly />            		
                    <input class="easyui-timespinner" id="HoraResultado3" name="HoraResultado3" data-options="showSeconds:true" style="width:85px" readonly />    
                   
              </div> 
              
              <div class="fitem2">
              		<label>Estado:</label>
                    <input type="text" class="easyui-textbox" name="EstadoDescripcion3" id="EstadoDescripcion3" readonly />  
                      
                    <label>Resultado:</label>                    
                    <input style="width:200px;height:50px" class="easyui-textbox" multiline="true" name="ResultadoCustodia3" id="ResultadoCustodia3" readonly />
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">       
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-VerCustodia').dialog('close')" style="width:90px">Cerrar</a>
	  </div>

<!--FIN FORMULARIO VER CUSTODIA-->
    
   <script> 
   
   function CambiarBuscar(){
	   if(document.getElementById('BuscarSNCS').checked==true){	 
			$.messager.alert({
				title: 'Mensaje',
				msg: 'Ingrese un Nro de SNCS a Buscar',
				icon:'warning',
				fn: function(){
					$('#SNCSB').next().find('input').focus();
				}
			});
			$('#SNCSB').next().find('input').focus();
			return 0;	
	   }	   
	   /*if(document.getElementById('BuscarReserva').checked==true){
		  $('#SNCSB').textbox('setValue', '');
		  return 0;			
	   }*/
   }
   
   function VerCustodia(){
	   var rowp = $('#dg').datagrid('getSelected');
			if (rowp){	
			   if(rowp.Estado==4){//EN CUSTODIA
				  $('#dlg-VerCustodia').dialog('open').dialog('setTitle','Ver Datos Hemocomponente en Custodia');						
				  $('#fmVerCustodia').form('load',rowp);
				   
			   }else if(rowp.ResultadoCustodia3!=''){//ESTUVO EN CUSTODIA
				  $('#dlg-VerCustodia').dialog('open').dialog('setTitle','Ver Resultado de Custodia');						
				  $('#fmVerCustodia').form('load',rowp);
				   
			   }else{					
					$.messager.alert('Mensaje','El Hemocomponente NO está, ni estuvo en CUSTODIA','warning');
					return 0;
				}			
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente','warning');
				return 0;			
			}
	   
  }
   
   function CustodiaHemocomponente(){						
			
			var rowp = $('#dg').datagrid('getSelected');
			if (rowp){	
			   if(rowp.Estado==0){
				   $.messager.alert('Mensaje','La unidad se encuentra eliminada','warning');
				   return 0;
				   
			   /*}else if(rowp.Estado==3){
				   $.messager.alert('Mensaje','La unidad se encuentra reservada','warning');
				   return 0;*/
				   
			   }else if(rowp.Estado==4){
				   $.messager.alert('Mensaje','La unidad YA se encuentra en custodia','warning');
				   return 0;
				   
			   }else if(rowp.Estado==5 || rowp.Estado==6){
				   $.messager.alert('Mensaje','La unidad YA ha sido donado','warning');
				   return 0;
				   
			   }else if(parseInt(rowp.VolumenFraccion)>0){
				  $.messager.confirm('Mensaje', 'La unidad se encuentra Fraccionada, ¿Seguro de poner en Custodia la Fracción Restante?', function(r){
						if (r){
							$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Custodia Hemocomponente');						
							$('#fmCabHemocomponente').form('load',rowp);
						}
					});
			   }else if(rowp.ResultadoCustodia3!=''){//ESTUVO EN CUSTODIA
				  $.messager.confirm('Mensaje', 'La unidad YA tiene Datos de Custodia, ¿Seguro que desea Modificar los Datos de Custodia?', function(r){
						if (r){
							$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Modificar Custodia Hemocomponente');						
							$('#fmCabHemocomponente').form('load',rowp);
						}
					});
			   }else{					
					$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Custodia Hemocomponente');						
					$('#fmCabHemocomponente').form('load',rowp);
				}					
				url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarCustodiaHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>";
				
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente','warning');
				return 0;			
			}
		} 
		
	function saveCustodiaHemocomponente(){			
			
			var MotivoEstado=$('#MotivoEstado').combobox('getValue');
			 if(MotivoEstado==""){
				$.messager.alert('Mensaje','Ingrese un Motivo de Custodia','info');
				$('#MotivoEstado').next().find('input').focus();
				return 0;			
			}
			
			var UsuMotivo=$('#UsuMotivo').combobox('getValue');
			if(UsuMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione el Responsable que Registra la Custodia','info');
				$('#UsuMotivo').next().find('input').focus();
				return 0;			
			}	
			
			var FecMotivo=$('#FecMotivo').datebox('getText');
			 if(FecMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione una Fecha de Custodia','info');
				$('#FecMotivo').next().find('input').focus();
				return 0;			
			}						
			//document.getElementById("fmCabHemocomponente").submit();
			var TipoHem=$('#TipoHem').combobox('getValue');			
			var SNCS=$('#SNCS').textbox('getValue');
			$.messager.confirm('Mensaje', '¿Seguro de poner en Custodia el Hemocomponente '+ TipoHem +'-'+SNCS+'?', function(r){
				if (r){
					document.getElementById("fmCabHemocomponente").submit();
				}
			});	
			
		}	
	
	 function IngresarResultado(){				
			var rowp = $('#dg').datagrid('getSelected');			
			if (rowp){				
				if(rowp.Estado!='4'){
					$.messager.alert('Mensaje','La unidad debe estar en Custodia','warning');					
					return 0;
					
				}else{
					$('#dlg-CabHemocomponenteCrio').dialog('open').dialog('setTitle','Ingresar Resultado Custodia');						
					$('#fmIngresarResultado').form('load',rowp);						
				}
				url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarResultadoCustodia&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>";
				
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente en custodia para Ingresar Resultado','warning');
				return 0;			
			}
		}
		
		function saveResultado(){	
		
			var UsuMotivo=$('#UsuResultado').combobox('getValue');
			if(UsuMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione el Responsable que Ingresa el Resultado de la Custodia','info');
				$('#UsuResultado').next().find('input').focus();
				return 0;			
			}	
			
			var FecMotivo=$('#FecResultado').datebox('getText');
			 if(FecMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione una Fecha del Resultado de la Custodia','info');
				$('#FecResultado').next().find('input').focus();
				return 0;			
			}
			
			var ResultadoCustodia=$('#ResultadoCustodia').textbox('getText');
			if(ResultadoCustodia.trim()==""){
				$.messager.alert('Mensaje','Ingrese el Resultado de la Custodia','info');
				$('#ResultadoCustodia').next().find('input').focus();
				return 0;			
			}	
			
			//document.getElementById("fmCabHemocomponente").submit();
			var TipoHem=$('#TipoHem2').combobox('getValue');			
			var SNCS=$('#SNCS2').textbox('getValue');
			
			var mensaje='';
			if(document.getElementById('Eliminar').checked==true){
				estado='Eliminar';
				mensaje='¿Seguro de ingresar el Resultado de la Custodia '+ TipoHem +'-'+SNCS+' y Eliminar el hemocomponente?';
			}else{
				mensaje='¿Seguro de ingresar el Resultado de la Custodia '+ TipoHem +'-'+SNCS+' y Enviar a stock el hemocomponente?';
			}
			
			
			$.messager.confirm('Mensaje', mensaje, function(r){
				if (r){
					document.getElementById("fmIngresarResultado").submit();
				}
			});	
			
		} 	 
	
	$(function(){  	
	  $('#SNCSB').numberbox('textbox').attr('maxlength', $('#SNCSB').attr("maxlength"));
	   
	});	
	
		function getData(){
			var rows = [];			
			
	<?php 		
	 
	 $BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);
	 
     $i=1; $j=0;											
	if($BuscarSNCSTodos!=NULL){
		foreach($BuscarSNCSTodos as $item)
		{
			$MovTipo=$item["MovTipo"];			
			if($MovTipo=='F'){
				$j=$j+1;
				$NuevoSNCS=$item["SNCS"].'-F'.$j;
			}else{
				$NuevoSNCS=$item["SNCS"];
			}
						
			//Funcion en Funciones.php
			$EstadoDescripcion=DescripcionEstadoHemocomponente($item["Estado"]);			
			
			//Calcular Tiempo Restante de vencimiento
			$FechaVencimiento=$item["FechaVencimiento"];	
			
			//$fecharec=$item['FechaMovi'];			
			$datetime1 = new DateTime($FechaVencimiento);
			
			$fechaactual=date('Y-m-d H:i:s');
			$datetime2 = new DateTime($fechaactual);
			$interval = date_diff($datetime2, $datetime1);
			//$interval->format('%R%a días');
			if($interval->format('%R')=='-'){ //NEGATIVO
				$Tiempo='<font color="#FF0000">Vencido</font>';
			}else if($interval->format('%a')=='0'){ //SIN DIAS (SOLO HORAS Y MINUTOS)
				$Tiempo='Queda '.$interval->format('<font color="#FF0000">%R%h</font>Horas, <font color="#FF0000">%i</font>Minutos');
			}else{
				$Tiempo='Queda '.$interval->format('<font color="#FF0000">%R%a</font>Días');//%R
			}
			
						
			
			$FecMotivo=$item["FecMotivo"];
			if($FecMotivo!=NULL){
				$FecMotivo3=vfecha(substr($item["FecMotivo"],0,10));
				$HoraMotivo3=substr($item["FecMotivo"],11,8);
			}else{
				$FecMotivo3='';
				$HoraMotivo3='';
			}			
			$FecResultado=$item["FecResultado"];
			if($FecResultado!=NULL){
				$FecResultado3=vfecha(substr($item["FecResultado"],0,10));
				$HoraResultado3=substr($item["FecResultado"],11,8);
			}else{
				$FecResultado3='';
				$HoraResultado3='';
			}		
						
			?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TipoHem: '<?php echo $item["TipoHem"];?>',
					TipoHem2: '<?php echo $item["TipoHem"];?>',
					NroDonacion: '<?php echo $item["NroDonacion"]; ?>',											
					SNCS:'<?php echo  $item["SNCS"]; ?>',					
					GrupoSanguineo:'<?php echo  trim($item["GrupoSanguineo"]); ?>',
					
					VolumenTotRecol:'<?php echo  $item["VolumenTotRecol"]; ?>',
					VolumenFraccion:'<?php echo  $item["VolumenFraccion"]; ?>',		
					VolumenRestante:'<?php echo  $item["VolumenRestante"]; ?>',	
									
					NroTabuladora:'<?php echo  $item["NroTabuladora"]; ?>',
					FechaExtraccion:'<?php echo  vfecha(substr($item["FechaExtraccion"],0,10)); ?>',				
					FechaVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)); ?>',
					IdMovDetalle:'<?php echo  $item["IdMovDetalle"]; ?>',
					MovNumero:'<?php echo  $item["MovNumero"]; ?>',
					MovNumero2:'<?php echo  $item["MovNumero"]; ?>',
					
					SNCS2:'<?php echo $item["SNCS"]; ?>',
					NuevoSNCS:'<?php echo $NuevoSNCS; ?>',
					MovTipo:'<?php echo $item["MovTipo"]; ?>',
					Estado:'<?php echo $item["Estado"]; ?>',
					EstadoDescripcion:'<?php echo $EstadoDescripcion; ?>',
					
					Tiempo:'<?php echo  $Tiempo; ?>',
					MotivoEstado2:'<?php echo  $item["MotivoEstado"]; ?>',
					UsuMotivo2:'<?php echo  $item["UsuMotivo"]; ?>',
					
					MotivoEstado3:'<?php echo  $item["MotivoEstado"]; ?>',
					UsuMotivo3:'<?php echo  $item["UsuMotivo"]; ?>',
					FecMotivo3:'<?php echo  $FecMotivo3; ?>',
					HoraMotivo3:'<?php echo  $HoraMotivo3; ?>',
					ResultadoCustodia3:'<?php echo  $item["ResultadoCustodia"]; ?>',
					UsuResultado3:'<?php echo  $item["UsuResultado"]; ?>',
					FecResultado3:'<?php echo  $FecResultado3; ?>',
					HoraResultado3:'<?php echo  $HoraResultado3; ?>',
					EstadoDescripcion3:'<?php echo strip_tags($EstadoDescripcion); ?>',
					
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});
		
		</script> 
     
   
	
  
</body>
</html>