<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Eliminar Hemocomponentes</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		   form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:10px;
            }			
            .fitem label{
                display:inline-block;
                width:90px;
							
            }
            .fitem input{
				display:inline-block;
                width:100px;	
				margin-left:10px;			
            }
			
	</style> 
    
<script type="text/javascript" >			
		
		$.extend($("#FecMotivo").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FecMotivo").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});					
		
		function regresar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=StockSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}
		
		function Buscar(){
			document.getElementById("form1").submit();
		}			
	
    </script>
</head>
<body>

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->
	<div style="margin:0px 0;"></div>
	 <form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=EliminarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">  
            	             	 
		        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-remove'" onClick="EliminarHemocomponente()">Eliminar Hemocomponente</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="PrepararCrioprecipitado()" >Convertir PFC a CRIO</a>                                
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="Imprimir();">Imprimir</a>	
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="Ayuda();">Ayuda</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a> 
			</div>
            
              <div>
               <strong>Busqueda por Hemocomponente:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
                   <select name="TipoHemB" id="TipoHemB" class="easyui-combobox" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }">
                        <option value=""></option>
                        <option value="PG" <?php if($TipoHem=='PG'){?> selected <?php } ?> >PG </option>
                        <option value="PFC" <?php if($TipoHem=='PFC'){?> selected <?php } ?> >PFC </option>
                        <option value="PQ" <?php if($TipoHem=='PQ'){?> selected <?php } ?>>PQ </option>
                        <option value="CRIO" <?php if($TipoHem=='CRIO'){?> selected <?php } ?>>CRIO </option>
                    </select>-<input id="SNCSB"  name="SNCSB" type="text" class="easyui-textbox" maxlength="7" value="<?php echo $SNCS ?>" data-options="prompt:'SNCS',
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = document.form1.submit(); }" />
                       <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>
                </div>
            			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Mantenimiento de Hemocomponentes" style="width:1100px;height:450px" data-options="
				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	<th field="NroDonacion" width="80">N°Donacion</th>
				<th field="TipoHem"  width="100">Hemocomponente</th>				
                <th field="NuevoSNCS"  width="90">SNCS</th>	
				<th field="GrupoSanguineo"  width="80">GS</th>
                 <th field="VolumenTotRecol"  width="100">Volumen Total</th>
                <th field="VolumenFraccion"  width="100">Volumen Fraccionado</th>
                <th field="VolumenRestante"  width="100">Volumen Restante</th>
				<th field="NroTabuladora" width="80">tubuladura</th>
                <th field="FechaExtraccion" width="100">F. Extracción</th>
                <th field="FechaVencimiento" width="100">F. Vencimiento</th>
                <th field="EstadoDescripcion" width="120">Estado</th>
			</tr>
		</thead>
	</table>
     </form>
    

<!--FORMULARIO ELIMINAR-->
 <div id="dlg-CabHemocomponente" class="easyui-dialog" style="width:680px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmCabHemocomponente" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarEliminarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">
                    <label>Tipo Hemoc-SNCS:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" readonly>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>-<input id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" data-options="prompt:'SNCS'" readonly />                     
                    <label>Motivo:</label>                    
                    <Select style="width:190px" class="easyui-combobox" id="MotivoEstado" name="MotivoEstado" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarMotivoElimina=ListarMotivoElimina('HEM');
                                           if($ListarMotivoElimina != NULL) { 
                                             foreach($ListarMotivoElimina as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select>                                    
              </div>   
              
              <div class="fitem">
              		 <label>Responsable:</label>
                    <Select style="width:162px" class="easyui-combobox" id="UsuMotivo" name="UsuMotivo" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>                    
                    <label>Fec.Elimina:</label>                   
                    <input name="FecMotivo" id="FecMotivo" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />            		<input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraMotivo" name="HoraMotivo" data-options="showSeconds:true,required:true" style="width:85px" />
                    
                    <input type="hidden" name="MovNumero" id="MovNumero" />      
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveEliminarHemocomponente();" style="width:90px">Eliminar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponente').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO ELIMINAR--> 


  
     <!--FORMULARIO NUEVO-->
 <div id="dlg-CabHemocomponenteCrio" class="easyui-dialog" style="width:600px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmCabHemocomponenteCrio" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarCrioHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">
                    <label>Tipo Hemoc:</label>                    
                    <select name="TipoHem2" id="TipoHem2" class="easyui-combobox" readonly >
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          
                    <label>SNCS:</label>                    
                    <input id="SNCS2"  name="SNCS2" type="text" class="easyui-textbox" maxlength="10" data-options="prompt:'SNCS'" readonly />                   
              </div>   
              
              <div class="fitem">
              		 <label>Responsable:</label>
                    <Select style="width:162px" class="easyui-combobox" id="UsuConvirtio" name="UsuConvirtio" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>     
                   <label>Fecha Convirtio:</label>                    
                   <input name="FechaConvirtio" id="FechaConvirtio" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />                  
                   <input type="hidden" name="MovNumero2" id="MovNumero2" />
              </div> 
              
              <div class="fitem">         
                    <label>Volumen:</label>                    
                    <input style="width:162px" id="VolumenRestante2"  name="VolumenRestante2" type="text" class="easyui-numberbox" data-options="prompt:'Volumen Restante',precision:2,required:true" />                   
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveConvertirHemocomponente();" style="width:90px">Convertir</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponenteCrio').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO NUEVO--> 
    
    
   <script> 
   
   function EliminarHemocomponente(){						
			
			var rowp = $('#dg').datagrid('getSelected');
			if (rowp){	
				if(rowp.Estado==0){
				   $.messager.alert('Mensaje','La unidad YA se encuentra eliminada','warning');
				   return 0;
				   
			   }else if(rowp.Estado==2){
				   $.messager.alert('Mensaje','La unidad se encuentra Reservada','warning');
				   return 0;
				   
			   }else if(rowp.Estado==5 || rowp.Estado==6){
				   $.messager.alert('Mensaje','La unidad YA ha sido donado','warning');
				   return 0;
				   
			   }else if(parseInt(rowp.VolumenFraccion)>0){
					$.messager.confirm('Mensaje', 'La unidad se encuentra Fraccionada, ¿Seguro de eliminar la Fracción Restante?', function(r){
						if (r){
							$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Eliminar Hemocomponente');						
							$('#fmCabHemocomponente').form('load',rowp);
						}
					});
				
				}else{					
					$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Eliminar Hemocomponente');						
					$('#fmCabHemocomponente').form('load',rowp);
				}					
				url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarEliminarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>";
				
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente a Eliminar','warning');
				return 0;			
			}
		} 
		
	function saveEliminarHemocomponente(){				
			
			var MotivoEstado=$('#MotivoEstado').combobox('getValue');
			 if(MotivoEstado==""){
				$.messager.alert('Mensaje','Ingrese un Motivo de Eliminación','info');
				$('#MotivoEstado').next().find('input').focus();
				return 0;			
			}
			
			var UsuMotivo=$('#UsuMotivo').combobox('getValue');
			if(UsuMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione el Responsable que Elimina','info');
				$('#UsuMotivo').next().find('input').focus();
				return 0;			
			}	
			
			var FecMotivo=$('#FecMotivo').datebox('getText');
			 if(FecMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione una Fecha de Eliminación','info');
				$('#FecMotivo').next().find('input').focus();
				return 0;			
			}		
			//document.getElementById("fmCabHemocomponente").submit();
			var TipoHem=$('#TipoHem').combobox('getValue');			
			var SNCS=$('#SNCS').textbox('getValue');
			$.messager.confirm('Mensaje', '¿Seguro de Eliminar el Hemocomponente '+ TipoHem +'-'+SNCS+'?', function(r){
				if (r){
					document.getElementById("fmCabHemocomponente").submit();
				}
			});	
			
		}
		
	function PrepararCrioprecipitado(){						
			
			var FechaActual='<?php echo $FechaServidor ?>';
			var rowp = $('#dg').datagrid('getSelected');
			
			if (rowp){			
				if(rowp.TipoHem=='PFC'){
					if(rowp.MovTipo=='F'){
						$.messager.alert('Mensaje','La unidad es una Fracción Pediátrica','warning');					
					    return 0;
						
					}else if(rowp.Estado!='1'){
						$.messager.alert('Mensaje','La unidad debe estar disponible','warning');					
					    return 0;
						
					}else if(rowp.FechaVencimiento2<FechaActual){					
						$.messager.confirm('Mensaje', 'Unidad venció el '+rowp.FechaVencimiento+', ¿Consultó con el jefe de servicio?', function(r){
							if (r){
								$('#dlg-CabHemocomponenteCrio').dialog('open').dialog('setTitle','Convertir Hemocomponente');						
								$('#fmCabHemocomponenteCrio').form('load',rowp);
							}
						});	
					
					}else if(rowp.GrupoSanguineo!='O+'){					
						$.messager.confirm('Mensaje', 'Unidad no es O+, ¿Consultó con el jefe de servicio?', function(r){
							if (r){
								$('#dlg-CabHemocomponenteCrio').dialog('open').dialog('setTitle','Convertir Hemocomponente');						
								$('#fmCabHemocomponenteCrio').form('load',rowp);
							}
						});	
					
					}else{
						$('#dlg-CabHemocomponenteCrio').dialog('open').dialog('setTitle','Convertir Hemocomponente');						
						$('#fmCabHemocomponenteCrio').form('load',rowp);						
					}					
										
				}else{
					$.messager.alert('Mensaje','Seleccione un Plasma Fresco Congelado (PFC)','warning');
					$('#TipoHemB').next().find('input').focus();
					return 0;
					
				}
				
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente a Convertir','warning');
				return 0;			
			}
		}
		
		function saveConvertirHemocomponente(){	
		
			var UsuMotivo=$('#UsuConvirtio').combobox('getValue');
			if(UsuMotivo.trim()==""){
				$.messager.alert('Mensaje','Seleccione un Responsable','info');
				$('#UsuConvirtio').next().find('input').focus();
				return 0;			
			}				
			
			var FechaConvirtio=$('#FechaConvirtio').datebox('getValue');
			 if(FechaConvirtio==""){
				$.messager.alert('Mensaje','Ingrese la Fecha que Convirtió','info');
				$('#FechaConvirtio').next().find('input').focus();
				return 0;			
			}	
			
			var VolumenRestante2=$('#VolumenRestante2').numberbox('getValue');
			 if(VolumenRestante2==""){
				$.messager.alert('Mensaje','Ingrese el Volumen de CRIO','info');
				$('#VolumenRestante2').next().find('input').focus();
				return 0;			
			}
			
			//document.getElementById("fmCabHemocomponente").submit();
			var TipoHem=$('#TipoHem2').combobox('getValue');			
			var SNCS=$('#SNCS2').textbox('getValue');
			$.messager.confirm('Mensaje', '¿Seguro de Eliminar el Hemocomponente '+ TipoHem +'-'+ SNCS +' y Crear el CRIO-'+ SNCS +'?', function(r){
				if (r){
					document.getElementById("fmCabHemocomponenteCrio").submit();
				}
			});	
			
		} 	 
	
	$(function(){  	
	  $('#SNCSB').numberbox('textbox').attr('maxlength', $('#SNCSB').attr("maxlength"));
	   
	});	
	
		function getData(){
			var rows = [];			
			
	<?php 
	 $EstadoB='V';//Por Defecto Todos
	 $BuscarSNCSTodos = BuscarSNCSTodos_M($TipoHem,$SNCS,$EstadoB);
     $i=1; $j=0;										
	if($BuscarSNCSTodos!=NULL){
		foreach($BuscarSNCSTodos as $item)
		{
			$MovTipo=$item["MovTipo"];	
			if($MovTipo=='F'){
				$j=$j+1;
				$NuevoSNCS=$item["SNCS"].'-F'.$j;
			}else{
				$NuevoSNCS=$item["SNCS"];
			}
			
			//Funcion en Funciones.php
			$EstadoDescripcion=DescripcionEstadoHemocomponente($item["Estado"]);		
						
			 ?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TipoHem: '<?php echo $item["TipoHem"];?>',
					TipoHem2: '<?php echo $item["TipoHem"];?>',
					NroDonacion: '<?php echo $item["NroDonacion"]; ?>',											
					SNCS:'<?php echo $item["SNCS"]; ?>',					
					GrupoSanguineo:'<?php echo  trim($item["GrupoSanguineo"]); ?>',
					
					VolumenTotRecol:'<?php echo  $item["VolumenTotRecol"]; ?>',
					VolumenFraccion:'<?php echo  $item["VolumenFraccion"]; ?>',		
					VolumenRestante:'<?php echo  $item["VolumenRestante"]; ?>',	
									
					NroTabuladora:'<?php echo  $item["NroTabuladora"]; ?>',
					FechaExtraccion:'<?php echo  vfecha(substr($item["FechaExtraccion"],0,10)); ?>',				
					FechaVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)); ?>',
					IdMovDetalle:'<?php echo  $item["IdMovDetalle"]; ?>',
					MovNumero:'<?php echo  $item["MovNumero"]; ?>',
					MovNumero2:'<?php echo  $item["MovNumero"]; ?>',
					
					SNCS2:'<?php echo $item["SNCS"]; ?>',
					NuevoSNCS:'<?php echo $NuevoSNCS; ?>',
					MovTipo:'<?php echo $item["MovTipo"]; ?>',
					Estado:'<?php echo $item["Estado"]; ?>',
					EstadoDescripcion:'<?php echo $EstadoDescripcion; ?>',
					FechaVencimiento2:'<?php echo  $item["FechaVencimiento"]; ?>',
			
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});
		
		</script> 
     
   
	
  
</body>
</html>