<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Enviar Hemocomponentes</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 #fm{
                margin:0;
                padding:10px 20px;
            }
			#fmCabHemocomponente{
                margin:0;
                padding:10px 20px;
            }
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:90px;
							
            }
            .fitem input{
				display:inline-block;
                width:100px;	
				margin-left:10px;			
            }			
			
	</style> 
    
<script type="text/javascript" >

	//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
	$.extend($.fn.validatebox.defaults.rules,{
		exists:{
			validator:function(value,param){
				var cc = $(param[0]);
				var v = cc.combobox('getValue');
				var rows = cc.combobox('getData');
				for(var i=0; i<rows.length; i++){
					if (rows[i].id == v){return true}
				}
				return false;
			},
			message:'El valor ingresado no existe.'
		}
	});	

	$(function(){
		
		$('#IdEstablecimientoEgresoB').combogrid({
			panelWidth:650,
			value:'<?php echo $IdEstablecimientoEgreso ?>',
			url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarEstablecimientos',
			idField:'IdEstablecimiento',
			textField:'Nombre',
			editable: true,
			//required: true,
			mode:'remote',
			fitColumns:true,
			onSelect: function(rec){
			var url = document.form1.submit(); },						
			columns:[[		
				{field:'Codigo',title:'Codigo',width:60},				
				{field:'Nombre',title:'Nombre',width:550}						
			]]
		});		
				
		$('#Establecimientos').combogrid({
			panelWidth:650,
			value:'<?php echo $IdEstablecimientoEgreso ?>',
			url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarEstablecimientos',
			idField:'IdEstablecimiento',
			textField:'Nombre',
			editable: true,
			required: true,
			mode:'remote',
			fitColumns:true,
			onSelect: function(rec){
			var url = cambiarEstablecimientos(); },						
			columns:[[		
				{field:'Codigo',title:'Codigo',width:60},				
				{field:'Nombre',title:'Nombre',width:550}						
			]]
		});	
		
		document.getElementById('IdEstablecimientoEgreso').value='<?php echo $IdEstablecimientoEgreso ?>';
		
		$('#TipoHem').combobox({	
			editable: false,
			required: true								
		});	
		
		$('#UsuarioSalida').combobox({	
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#UsuarioSalida"]',
			filter: function (q, row) {
			return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
			}								
		});						
		$('#UsuarioSalida').combobox('validate');								
									
	});
	
	function cambiarEstablecimientos(){
		var Establecimientos= $('#Establecimientos').combobox('getValue');
		document.getElementById('IdEstablecimientoEgreso').value=Establecimientos;
	}
		
		$.extend($("#FechaSalida").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaSalida").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FechaNacimiento" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});
		
		/////////MANTENIMIENTO CABVACION
		var url;		
		function nuevaCabHemocomponente(){
			//$('#Empleado').combobox('clear');		
			//$('#FecIngreso').textbox('clear');
			$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Nuevo Hemocomponente a Enviar');
			url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarEnviarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>";
		}	
		
		function saveCabHemocomponente(){
			var FechaSalida=$('#FechaSalida').datebox('getText');
			if(FechaSalida=="" || $('#FechaSalida').datebox('isValid')==false){	
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione Fecha de Salida',
					icon:'warning',
					fn: function(){
						$('#FechaSalida').next().find('input').focus();
					}
				});
				$('#FechaSalida').next().find('input').focus();
				return 0;				
			}
			
			//var IdEstablecimientoEgreso=$('#IdEstablecimientoEgreso').combobox('getValue');
			var IdEstablecimientoEgreso=document.getElementById('IdEstablecimientoEgreso').value;		
			if(IdEstablecimientoEgreso.trim()=="" || IdEstablecimientoEgreso.trim()=="0"){			
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione un Hospital Destino',
					icon:'warning',
					fn: function(){
						$('#Establecimientos').next().find('input').focus();
					}
				});
				$('#Establecimientos').next().find('input').focus();
				return 0;				
			}
			
			var g = $('#Establecimientos').combogrid('grid');	// get datagrid object
			var r = g.datagrid('getSelected');	// get the selected row
				
			if(r!=null){
				var Codigo=r.Codigo; //alert(Codigo);	
				if(parseInt(Codigo)=="6218"){		
					$.messager.alert({
						title: 'Mensaje',
						msg: 'No puede ENVIAR al Hospital Seleccionado',
						icon:'warning',
						fn: function(){
							$('#Establecimientos').next().find('input').focus();
						}
					});
					$('#Establecimientos').next().find('input').focus();
					return 0;				
				}
			}else{
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Busque un Hospital Destino Valido',
					icon:'warning',
					fn: function(){
						$('#Establecimientos').next().find('input').focus();
					}
				});
				$('#Establecimientos').next().find('input').focus();
				$('#Establecimientos').combogrid('validate');	
				return 0;					
			
			}
			
			var UsuarioSalida=$('#UsuarioSalida').combobox('getValue');
			if(UsuarioSalida=="" || $('#UsuarioSalida').combobox('isValid')==false){	
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione un Usuario Salida',
					icon:'warning',
					fn: function(){
						$('#UsuarioSalida').next().find('input').focus();
					}
				});
				$('#UsuarioSalida').next().find('input').focus();
				return 0;				
			}		
			
			var TipoHem=$('#TipoHem').combobox('getValue');			
			var SNCS=$('#SNCS').textbox('getValue');
			 if(SNCS==""){
				$.messager.alert('Mensaje','Ingrese un SNCS','info');
				$('#SNCS').next().find('input').focus();
				return 0;			
			}			
			//document.getElementById("fmCabHemocomponente").submit();
			$.messager.confirm('Mensaje', '¿Seguro de Enviar el Hemocomponente '+ TipoHem +'-'+ SNCS +'?', function(r){
				if (r){
					document.getElementById("fmCabHemocomponente").submit();
				}
			});	
			
		}	
					
		
		function regresar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=StockSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}
		
		function Buscar(){
			document.getElementById("form1").submit();
		}	
		
		function Imprimir(){
			//var IdEstablecimientoEgreso=$('#IdEstablecimientoEgreso').combobox('getValue');
			var IdEstablecimientoEgreso='<?php echo $IdEstablecimientoEgreso ?>';		
			if(IdEstablecimientoEgreso.trim()=="" || IdEstablecimientoEgreso.trim()=="0"){			
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione un Hospital Destino',
					icon:'warning',
					fn: function(){
						$('#IdEstablecimientoEgresoB').next().find('input').focus();
					}
				});
				$('#IdEstablecimientoEgresoB').next().find('input').focus();
				return 0;				
			}
			
			var g = $('#IdEstablecimientoEgresoB').combogrid('grid');	// get datagrid object
			var r = g.datagrid('getSelected');	// get the selected row
				
			if(r!=null){
				var Codigo=r.Codigo; //alert(Codigo);	
				if(parseInt(Codigo)=="6218"){		
					$.messager.alert({
						title: 'Mensaje',
						msg: 'No puede ENVIAR al Hospital Seleccionado',
						icon:'warning',
						fn: function(){
							$('#IdEstablecimientoEgresoB').next().find('input').focus();
						}
					});
					$('#IdEstablecimientoEgresoB').next().find('input').focus();
					return 0;				
				}
			}else{
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Busque un Hospital Destino Valido',
					icon:'warning',
					fn: function(){
						$('#IdEstablecimientoEgresoB').next().find('input').focus();
					}
				});
				$('#IdEstablecimientoEgresoB').next().find('input').focus();
				$('#IdEstablecimientoEgresoB').combogrid('validate');	
				return 0;
			}
			
			var FechaSalidaB=$('#FechaSalidaB').datebox('getText');
			if(FechaSalidaB=="" || $('#FechaSalidaB').datebox('isValid')==false){	
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione Fecha de Salida',
					icon:'warning',
					fn: function(){
						$('#FechaSalidaB').next().find('input').focus();
					}
				});
				$('#FechaSalidaB').next().find('input').focus();
				return 0;				
			}
			
			<?php /*?>location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirEnvio&IdEstablecimientoEgreso="+IdEstablecimientoEgreso+"&FechaSalida="+FechaSalidaB+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";<?php */?>
			
			$.messager.confirm('Mensaje', '¿Seguro que Desea Imprimir?', function(r){
				if (r){
					//document.getElementById("form1").submit();					
					window.open('../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirEnvio&IdEstablecimientoEgreso='+IdEstablecimientoEgreso+'&FechaSalida='+FechaSalidaB+'&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>','_blank'); 
				}
			});			
			
		}		
	
    </script>
</head>
<body>

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->
	<div style="margin:0px 0;"></div>
	 <form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=EnviarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">            	 
		        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="nuevaCabHemocomponente()">Enviar Hemocomponente</a>
				<!--<a href="#" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="nuevoDetHemocomponente()" >Nuevo Detalle</a>							
                <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="declarardetalle();">Declarar & Eliminar Detalle </a> -->               
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="Imprimir();">Imprimir Envío</a>	
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="ayuda();">Ayuda</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a>
			</div>
            
              <div>
               <strong>Busqueda por Hospital Destino:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
                    <?php  $emple = ListarEstablecimientos_M('0',''); ?>
                    <select name="IdEstablecimientoEgresoB" id="IdEstablecimientoEgresoB" class="easyui-combogrid" style="width:300px" data-options="
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" > <!-- onchange="submit()"--> 
                        <option value="0" selected>Todos</option>
                        <?php foreach($emple as $empleitem){?>
                        <option value="<?php echo $empleitem["IdEstablecimiento"]?>" <?php if($empleitem["IdEstablecimiento"]==$IdEstablecimientoEgreso){?> selected <?php } ?>><?php echo (mb_strtoupper($empleitem["Nombre"]));?></option> <!-- -->
                        <?php }	?>
                    </select> &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Fecha Salida:</strong> &nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="FechaSalidaB" id="FechaSalidaB" class="easyui-datebox" value="<?php echo $FechaSalidaB; ?>" validType="validDate" style="width:105px" data-options="required:true,
                    valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" />
                     <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>   
                </div>
            			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Enviar Hemocomponentes Disponibles" style="width:1100px;height:450px" data-options="
				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	<th field="NroDonacion" width="80">N°Donacion</th>
				<th field="TipoHem"  width="100">Hemocomponente</th>				
                <th field="SNCS"  width="80">SNCS</th>	
				<th field="GrupoSanguineo"  width="70">GS</th>
                <th field="VolumenRestante"  width="100">Volumen</th>
				<th field="NroTabuladora" width="80">tubuladura</th>
                <th field="FechaExtraccion" width="100">F. Extracción</th>
                <th field="FechaVencimiento" width="100">F. Vencimiento</th>				
                <th field="EstablecimientoEgreso" width="250">Hosp. Destino</th>
			</tr>
		</thead>
	</table>
     </form>
    
   
 <!--FORMULARIO NUEVO-->
 <div id="dlg-CabHemocomponente" class="easyui-dialog" style="width:600px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmCabHemocomponente" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarEnviarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">
                	<label>Fecha Salida:</label>                   
                    <input name="FechaSalida" id="FechaSalida" class="easyui-datebox" value="<?php echo $FechaSalidaB; ?>" validType="validDate" style="width:150px" data-options="required:true" />        
                    <label>Hospital Destino:</label>                      
                    <select style="width:165px" class="easyui-combobox" id="Establecimientos" name="Establecimientos" ></select>
                	<input type="hidden" name="IdEstablecimientoEgreso" id="IdEstablecimientoEgreso" value="0" />              
                                       
              </div>   
              
              <div class="fitem">
              		 <label>Responsable:</label>
                    <Select style="width:150px" class="easyui-combobox" id="UsuarioSalida" name="UsuarioSalida" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>     
                    <label>Tipo Hemoc-SNCS:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" >
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>-<input id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" value="" data-options="prompt:'SNCS'" />        
              </div>           
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveCabHemocomponente();" style="width:90px">Enviar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponente').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO NUEVO-->   
    
    
    
   <script>
   	$(function(){  	
	  $('#SNCS').numberbox('textbox').attr('maxlength', $('#SNCS').attr("maxlength"));
	   
	});		
	
		function getData(){
			var rows = [];			
			
	<?php 
	
	 $BuscarSNCSEnviado = BuscarSNCSEnviado_M($IdEstablecimientoEgreso,$FechaSalida);
     $i = 1;										
	if($BuscarSNCSEnviado!=NULL){
		foreach($BuscarSNCSEnviado as $item)
		{
						
			 ?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TipoHem: '<?php echo $TipoHem=utf8_encode($item["TipoHem"]);?>',
					NroDonacion: '<?php echo $NroDonacion=$item["NroDonacion"]; ?>',											
					SNCS:'<?php echo  $item["SNCS"]; ?>',
					GrupoSanguineo:'<?php echo  $item["GrupoSanguineo"]; ?>',
					VolumenRestante:'<?php echo  $item["VolumenRestante"]; ?>',					
					NroTabuladora:'<?php echo  $item["NroTabuladora"]; ?>',
					FechaExtraccion:'<?php echo  vfecha(substr($item["FechaExtraccion"],0,10)); ?>',				
					FechaVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)); ?>',
					IdEstablecimientoEgreso:'<?php echo  $item["IdEstablecimientoEgreso"]; ?>',
					EstablecimientoEgreso:'<?php echo mb_strtoupper($item["EstablecimientoEgreso"]); ?>',
			
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});
		
		</script> 
     
   
	
  
</body>
</html>