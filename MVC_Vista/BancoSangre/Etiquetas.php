<html>
<head>
	<meta charset="UTF-8">
	<title>Impresion de Etiquetas</title>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css"> 
         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >			
			function ConsultarEtiquetaAlicuota(){
				var NroDonacion=$('#NroDonacion').numberbox('getValue');
				if(NroDonacion==''){
					$.messager.alert('Mensaje de Información', 'Escriba un Nro Donacion','warning');	
				}else{
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirEtiquetaAlicuota&NroDonacion="+NroDonacion+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
				}							
			}
			
			function ConsultarEtiquetaReceptor(){
				var NroHistoriaClinica=$('#NroHistoriaClinica').numberbox('getValue');
				if(NroHistoriaClinica==''){
					$.messager.alert('Mensaje de Información', 'Escriba un NroHistoria Clinica del Paciente','warning');	
				}else{
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirEtiquetaReceptor&NroHistoriaClinica="+NroHistoriaClinica+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
				}						
			}
			
			function ConsultarEtiquetaOrdenesLaboratorio(){
				var NroOrdenBar=$("#idNumOrden").textbox('getValue');
				if(NroOrdenBar!=''){			
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=OrdenesLaboratorio_ImprimirOrden&IdOrden="+NroOrdenBar+"&retorno=1&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
				}else{
					$.messager.alert('Mensaje de Información', 'Escriba Orden','warning');	
					}				 
				//alert('no existe numero'+NroOrdenBar);				
										
			}
		</script>            
        
</head>    
        
<body>

    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteExcel1&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:90%;;height:auto;"title="Banco de Sangre: IMPRESIÓN DE ETIQUETAS" iconCls="icon-search" align="left"> 	
              
       <table width="938">
            <tr>
              <td width="9">&nbsp;</td>
              <td width="301">&nbsp;</td>
              <td width="152">&nbsp;</td>
              <td width="456">&nbsp;</td>
            </tr> 
                                  
            <tr>
              <td>&nbsp;</td>
              <td>1. Etiqueta para Alicuota</td>
              <td><input class="easyui-numberbox" name="NroDonacion" id="NroDonacion" data-options="prompt:'Nro Donacion'" />Ejm:1800280</td>
              <td><div class="buttons">
              	<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarEtiquetaAlicuota();">Consultar</button> 			  
              </div>
              </td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td>2. Etiqueta para Receptor de Sangre (Paciente)</td>
              <td><input class="easyui-numberbox" name="NroHistoriaClinica" id="NroHistoriaClinica" data-options="prompt:'Nro Historia Clinica'" />Ejm:1719929</td>
              <td>
              <div class="buttons">
              	<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarEtiquetaReceptor();">Consultar</button> 			  
              </div>
              </td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td>3. Etiqueta de Solicitud de Análisis del Paciente</td>
              <td><input class="easyui-numberbox" name="idNumOrden" id="idNumOrden" data-options="prompt:'Nro Orden'" />Ejm:3777269</td>
              <td>
              <div class="buttons">
              	<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarEtiquetaOrdenesLaboratorio();">Consultar</button> 			  
              </div>
              </td>
            </tr>
            
        </table>              
     	<br> 	
     </div>
</form>
      
 
  
      
</body>
</html>