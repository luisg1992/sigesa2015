<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Listado de Recepcion de Postulantes</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >			
			
			function evaluacion(){				 
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegEvaluacionPredonacionPostulante&IdMovimiento="+rowp.IdMovimiento+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar una Recepción del Postulante','warning');						
				}
			}
			
			function editar(){					
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=UpdPostulante&IdMovimiento="+rowp.IdMovimiento+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar una Recepción a EDITAR','warning');						
				}	
		    }
			
			$.extend( $( "#FechaInicio" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#FechaInicio" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaInicio" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>     
        
<body>

		<div style="margin:0px 0;"></div>    
        <div id="tb" style="padding:5px;height:auto">
       		<div style="margin-bottom:5px">
                <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="evaluacion()">Evaluación Predonación</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Actualizar Recepción</a> 
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>
                <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="ayuda();">Ayuda</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="salir();">Salir</a>-->
        	</div>           
        </div>
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Lista de Recepción en Espera para Evaluación Predonación" style="width:100%;height:80%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	   <th field="FechaMovi" width="100">Fecha y Hora Recepción</th>
                   <th field="NroMovimiento" width="70">Nro Postulación</th>
                   <th field="NroDocumento" width="70">DNI Postulante</th>
                   <th field="Postulante" width="150">Postulantes en Espera Evaluación</th>                   
                   <th field="TipoDonante" width="60">Tipo Donante</th>
                   <th field="TipoDonacion" width="60">Tipo Donacion</th>
                   <th field="Paciente" width="150">Paciente</th>
                   <th field="GrupoSanguineoPaciente" width="60">G.S.Paciente</th>
                   <!--<th field="DescripcionEstadoApto" width="70">Estado</th>-->
                   <th field="Accion" width="120">Acción</th>
                   <!--formatter="formatPrioridad"-->                   	
			</tr>
		</thead>
	</table>
     
   <script type="text/javascript">	
	
	/*$.extend($('#Dni').searchbox.defaults.rules, {
        justNumber: {
            validator: function(value, param){
                 return value.match(/[0-9]/);
            },
            message: 'Porfavor digite sólo numeros'  
        }
    });*/
	
	
	/*$.extend($('#ApellidoPaterno').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor no digite numeros'  
        }
    });*/
		
function validaEntero(){	
 	/*//alert('validaEntero');
	var s = $.fn.searchbox.defaults.formatter(/[0-9]/);
	if(s==value){
		return true;
	}else{
		$("#IdCuenta" ).searchbox('setValue', '');
		return false;
	}*/
}
	 
	function getData(){
			var rows = [];			
			<?php			
			 $i = 1;					
			 $Listar=Listar_MovimientosRecepcionM("");				
			 if($Listar!=NULL){   
				foreach($Listar as $item)
				{			
					$EstadoApto=$item['EstadoApto'];//0 No Apto, 1 Apto parcial, 2 Apto	
					if($EstadoApto=='0'){
						$DescripcionEstadoApto='No Apto';
						$Accion='Atendido';
					}else if($EstadoApto=='1'){
						$DescripcionEstadoApto='Apto parcial';
						$fecharec=$item['FechaMovi'];
						$fechaactual=date('Y-m-d H:i:s');
						$datetime1 = new DateTime($fecharec);
						$datetime2 = new DateTime($fechaactual);
						$interval = date_diff($datetime1, $datetime2);
						//$interval->format('%R%a días');
						if($interval->format('%a')=='0'){
							$Accion='En espera...'.$interval->format('<font color="#FF0000">%R%h</font>H, <font color="#FF0000">%i</font>M');
						}else{
							$Accion='En espera...'.$interval->format('<font color="#FF0000">%R%a</font>D, <font color="#FF0000">%h</font>H, <font color="#FF0000">%i</font>M');//%R
						}
					}else if($EstadoApto=='2'){
						$DescripcionEstadoApto='Apto';
						$Accion='Atendido';
					}
					
					//Si encuentra estos caracteres no muestra el listado
					$novalidados = array("'", ",");	
					
					$xPostulante=$item['ApellidosPostulante'].' '.$item['NombresPostulante'];					
					$PostulanteV = str_replace($novalidados, "", $xPostulante);
					
					$xPaciente=$item['Paciente'];					
					$PacienteV = str_replace($novalidados, "", $xPaciente);
						
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							FechaMovi: '<?php echo vfecha(substr($item['FechaMovi'],0,10)).' '.substr($item['FechaMovi'],11,8);?>',							
							NroDocumento: '<?php echo $item['NroDocumento'];?>',
							Postulante: '<?php echo $PostulanteV; ?>',
							TipoDonante:  '<?php echo $item['TipoDonante'];?>',
							TipoDonacion:  '<?php echo $item['TipoDonacion'];?>',
							CondicionDonante:  '<?php echo $item['CondicionDonante'];?>',							
							Paciente:'<?php echo $PacienteV; ?>',
							GrupoSanguineoPaciente:'<?php echo $item['GrupoSanguineoPaciente'];?>',
							DescripcionEstadoApto:'<?php echo $DescripcionEstadoApto;?>',
							Accion:'<?php echo $Accion;?>',
							IdMovimiento:'<?php echo $item['IdMovimiento'];?>',
							NroMovimiento:'<?php echo 'PDAC'.$item['NroMovimiento'];?>'	
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}
		
		
		$('#dg').datagrid({
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){
			var dg =$('#dg').datagrid({data:getData()}).datagrid({			
			//var dg =$('#dg').datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});			

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>