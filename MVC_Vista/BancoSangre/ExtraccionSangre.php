<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Listado de Postulantes Aptos Para Extraccion de Sangre</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css"> 
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
			function Limpiar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RecepcionPostulante&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
			
			function RegExtraccionSangre(){					 
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					var f = new Date();
					fechaactual=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
					/*if(rowp.EstadoApto=='0'){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado es No Apto Definitivo','warning');	
					}*/
					if(rowp.EstadoApto=='0'){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado es NO Apto Definitivo','warning');
							
					}else if(rowp.EstadoApto=='1' && rowp.FechaApto!=fechaactual){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado Actualmente NO está Apto','warning');	
						
					}else if(rowp.NroDonacion=='NO CREADO'){
						$.messager.alert('Mensaje de Información', 'Falta Generar Nro Donación','warning');	
						
					}else if(rowp.EstadoAptoMov=='2'){
						$.messager.alert('Mensaje de Información', 'La extracción Ya fue Registrada','warning');	
						
					}else{						
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegExtraccionSangre&IdExamenMedico="+rowp.IdExamenMedico+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
					}
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar un Postulante','warning');						
				}
			}
			
			
			$.extend( $( "#FechaInicio" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#FechaInicio" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaInicio" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>         
        
	<style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 #fm{
                margin:0;
                padding:10px 30px;
            }
			#fmGenerarNroDonacion{
                margin:0;
                padding:10px 30px;
            }
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:80px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				margin-left:10px;
            }
			.fitem2 input {				
				 margin-right:25px;					 
			}
	</style> 
    
    <script type="text/javascript">
		/////////MANTENIMIENTO CABVACION
		//var url;		
		function Imprimir(){
			var rowp = $('#dg').datagrid('getSelected');
				if (rowp){	
				var f = new Date();
					fechaactual=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
					/*if(rowp.EstadoApto=='0'){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado es No Apto Definitivo','warning');	
					}*/
					if(rowp.EstadoApto=='0'){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado es NO Apto Definitivo','warning');
							
					}else if(rowp.EstadoApto=='1' && rowp.FechaApto!=fechaactual){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado Actualmente NO está Apto','warning');
							
					}else if(rowp.NroDonacion=='NO CREADO'){ //Generar
						$('#dlg-CabVacacion').dialog('open').dialog('setTitle','Generar e Imprimir Codigo Barra de Nro Donación');
						$('#fmGenerarNroDonacion').form('load',rowp);
						
					}else{ //Imprimir
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirNroDonacionMovimiento&NroDonacionCreado="+rowp.NroDonacion+"&IdExamenMedico="+rowp.IdExamenMedico+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
						
					}				
				}else{
					$.messager.alert('Mensaje de Información', 'Seleccione un Postulante en Espera de Extraccion','warning');
				}
		}
		
		function editar(){					
			var rowp = $('#dg').datagrid('getSelected');
			if (rowp){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=UpdEvaluacionPredonacionPostulante&IdExamenMedico="+rowp.IdExamenMedico+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
			}else{
				$.messager.alert('Mensaje de Información', 'Debe seleccionar una Evaluación Predonación a EDITAR','warning');						
			}	
		}
		
		function saveNroDonacion(){	
			var NroDonacionCreado=$('#NroDonacionCreado').numberbox('getValue');			
			if(NroDonacionCreado.length!=7){				
				$.messager.alert({
					title: 'Mensaje de Información',
					msg: 'El número de Donación debe tener 7 números',
					icon:'warning',
					fn: function(){
						$('#NroDonacionCreado').next().find('input').focus();
					}
				});
				$('#NroDonacionCreado').next().find('input').focus();
				
			}else{		 		
				$.messager.confirm('Mensaje', '¿Seguro de Generar Número Donación?', function(r){
					if (r){
						$('#fmGenerarNroDonacion').submit();	
					}
				});
			}
			/*$('#dlg-CabVacacion').dialog('close');
			location.reload();		///para regresar			
			$('#fmGenerarNroDonacion').form('submit',{url:url});*/			
		}
	</script> 
      
<body>
  
		<div style="margin:0px 0;"></div>
        <div id="tb" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">
				<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="RegExtraccionSangre()">Extracción Sangre</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Editar Eval.Predonación</a> 
                <!--<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onClick="Limpiar()">Eliminar</a>-->
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" onClick="Imprimir()">Generar e Imprimir Codigo Barra de Nro Donación</a>				
				<!-- <a href="#" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="ayuda();">Ayuda</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="salir();">Salir</a>-->
			</div>        
       </div>
       
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Lista de Espera para Extracción" style="width:100%;height:80%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	   <th field="FechaEvaluacion" width="110">Fecha y Hora Evaluación</th>
            	   <th field="FechaApto" width="110">Fecha y Hora de Apto</th>
                   <th field="NroMovimiento" width="75">Nro Postulación</th>
                   <!--<th field="NroDocumento" width="70">DNI Postulante</th>-->
                   <th field="Postulante" width="150">Postulantes en Espera Extraccion</th>   
                   <th field="GrupoSanguineoPostulante" width="60">G.S.Postulante</th>                
                   <th field="TipoDonante" width="70">Tipo Donante</th>
                   <th field="TipoDonacion" width="70">Tipo Extraccion</th>
                   <th field="NroDonacion" width="70">Nro Donacion</th>
                   <!--<th field="Paciente" width="150">Paciente</th>-->
                   <th field="DescripcionEstadoApto" width="70">Estado</th>
                   <th field="Accion" width="150">Acción</th>
                   <!--formatter="formatPrioridad"-->                   	
			</tr>
		</thead>
	</table>
    
     <!--FORMULARIO GENERAR NRO DONACION-->
 <div id="dlg-CabVacacion" class="easyui-dialog" style="width:700px;height:250px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fmGenerarNroDonacion" name="fmGenerarNroDonacion" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=UpdNroDonacionMovimiento&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
                   
              <div class="fitem">
                 	<label>Nro Postulación:</label>                    
                    <!--<input type="hidden"  name="CodigoPostulante" id="CodigoPostulante"  />-->
                    <input type="text" class="easyui-textbox" name="NroMovimiento" id="NroMovimiento" readonly/>
               	 	<label>Nombre Completo:</label>
                 	<input type="text" class="easyui-textbox" name="Postulante" id="Postulante" style="width:300px;" readonly />
                 	<input type="hidden"  name="IdMovimiento" id="IdMovimiento"  />
                    <input type="hidden"  name="IdExamenMedico" id="IdExamenMedico"  />
               </div> 
               <div class="fitem">
               		<label>Nro Donación:</label>  
                    <input type="text" class="easyui-numberbox" name="NroDonacionCreado" id="NroDonacionCreado" maxlength="7" />
               </div>      
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveNroDonacion();" style="width:190px">Guardar e Imprimir</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-CabVacacion').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO GENERAR NRO DONACION-->    
   
   <script type="text/javascript">	
   		 
	function getData(){
			var rows = [];			
			<?php			
			 $i = 1;					
			 $Listar=Listar_ExamenMedicoM("");				
			 if($Listar!=NULL){   
				foreach($Listar as $item)
				{
					$FechaEvaluacion=vfecha(substr($item['FechaEvaluacion'],0,10)).' '.substr($item['FechaEvaluacion'],11,8);
					
					$FechaFinRechazo=$item['FechaFinRechazo'];					
					$FechaEspera=$item['FechaEspera'];
					
					//Inicio Obtener FechaApto
					if($item['EstadoApto']!='2'){//0 No Apto, 1 No Apto Temporal	
						if($FechaFinRechazo!=NULL && $FechaEspera!=NULL){
							$difdias=dias_transcurridos($FechaFinRechazo,$FechaEspera); //dias_transcurridos('2012-07-01','2012-07-18');
							if($difdias<=0){
								$FechaApto=vfecha(substr($item["FechaFinRechazo"],0,10));
							}else{
								$FechaApto=vfecha(substr($item["FechaEspera"],0,10));
							}
						}else if($FechaEspera!=NULL){
							$FechaApto=vfecha(substr($item["FechaEspera"],0,10));
						}else if($FechaFinRechazo!=NULL){
							$FechaApto=vfecha(substr($item["FechaFinRechazo"],0,10));
						}	
					}else{//2 Apto
						$FechaApto=$FechaEvaluacion;
					}
					//Fin Obtener FechaApto
								
					$EstadoApto=$item['EstadoApto'];//0 No Apto, 1 No Apto Temporal, 2 Apto	
					$gFechaApto=gfecha(substr($FechaApto,0,10)).' '.substr($item['FechaEvaluacion'],11,8);
					if($EstadoApto=='0'){
						$DescripcionEstadoApto='No Apto Definitivo';
						$Accion='Atendido';
						
					}else if($EstadoApto=='1'){
						$DescripcionEstadoApto='No Apto Temporal';						
						$fechaactual=date('Y-m-d');
						$datetime1 = new DateTime($gFechaApto);
						$datetime2 = new DateTime($fechaactual);
						$interval = date_diff($datetime1, $datetime2);
						//$interval->format('%R%a días');
						if($interval->format('%R')=="+"){
							$Accion='En espera...'.$interval->format('<font color="#FF0000">%R%a</font>D, <font color="#FF0000">%h</font>H, <font color="#FF0000">%i</font>M');//%R
						}else{
							$Accion='Espere...'.$interval->format('<font color="#FF0000">%R%a</font>D, <font color="#FF0000">%h</font>H, <font color="#FF0000">%i</font>M');//%R
						}
						
					}else if($EstadoApto=='2'){
						$DescripcionEstadoApto='Apto';
						//$Accion='Atendido';
						$fechaactual=date('Y-m-d H:i:s');
						$datetime1 = new DateTime($gFechaApto);
						$datetime2 = new DateTime($fechaactual);
						$interval = date_diff($datetime1, $datetime2);
						//$interval->format('%R%a días');
						if($interval->format('%a')=='0'){
							$Accion='En espera...'.$interval->format('<font color="#FF0000">%R%h</font>H, <font color="#FF0000">%i</font>M');
						}else{
							$Accion='En espera...'.$interval->format('<font color="#FF0000">%R%a</font>D, <font color="#FF0000">%h</font>H, <font color="#FF0000">%i</font>M');//%R
						}
					}					
					
					if($item['EstadoAptoMov']=='2'){
						$Accion='<font color="#0000FF">EXTRACCION REGISTRADA</font>';				
					}else{
						$Accion=$Accion;
					}					
					
					$NroDonacion=$item['NroDonacion'];
					if($NroDonacion!=""){
						$NroDonacion=$item['NroDonacion'];
					}else{
						$NroDonacion="NO CREADO";
					}
					
					$ObtenerMaxNroDonacion=ObtenerMaxNroDonacionExtraccion_M();
					if($ObtenerMaxNroDonacion!=NULL && $ObtenerMaxNroDonacion[0]["maxNroDonacion"]!=NULL){
						$NroDonacionCreado=($ObtenerMaxNroDonacion[0]["maxNroDonacion"])+1;
					}else{
						$NroDonacionCreado=substr(date('Y'),2,2).'00001'; //1700001;
					}	
					
					//Si encuentra estos caracteres no muestra el listado
					$novalidados = array("'", ",");	
					
					$xPostulante=$item['ApellidosPostulante'].' '.$item['NombresPostulante'];					
					$PostulanteV = str_replace($novalidados, "", $xPostulante);
					
					$xPaciente=$item['Paciente'];					
					$PacienteV = str_replace($novalidados, "", $xPaciente);
									
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							FechaEvaluacion: '<?php echo $FechaEvaluacion;?>',
							FechaApto: '<?php echo $FechaApto;?>',							
							NroDocumento: '<?php echo $item['NroDocumento'];?>',
							Postulante: '<?php echo $PostulanteV;?>',
							GrupoSanguineoPostulante: '<?php echo $item['GrupoSanguineoPostulante'];?>',
							TipoDonante:  '<?php echo $item['TipoDonante'];?>' ,
							CondicionDonante:  '<?php echo $item['CondicionDonante'];?>',
							TipoDonacion:  '<?php echo $item['TipoDonacion'];?>',
							NroDonacion	:  '<?php echo $NroDonacion;?>',					
							Paciente:'<?php echo $PacienteV;?>',
							DescripcionEstadoApto:'<?php echo $DescripcionEstadoApto;?>',
							Accion:'<?php echo $Accion;?>',
							IdExamenMedico:'<?php echo $item['IdExamenMedico'];?>',
							EstadoApto:'<?php echo $item['EstadoApto'];?>',
							IdMovimiento:'<?php echo $item['IdMovimiento'];?>',
							NroDonacionCreado:'<?php echo $NroDonacionCreado;?>',
							EstadoAptoMov:'<?php echo $item['EstadoAptoMov'];?>',
							NroMovimiento:'<?php echo 'PDAC'.$item['NroMovimiento'];?>'		
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}
		
		
		$('#dg').datagrid({
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){
			var dg =$('#dg').datagrid({data:getData()}).datagrid({			
			//var dg =$('#dg').datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});	
		
		$(function(){	 
		  $('#NroDonacionCreado').numberbox('textbox').attr('maxlength', $('#NroDonacionCreado').attr("maxlength"));	  	 
		});			

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>