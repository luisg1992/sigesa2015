<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Listado de Fraccionamiento de Sangre</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">  
        <style>
            html, body { height: 100%;}
        </style>   

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
			function Limpiar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RecepcionPostulante&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
			
			function RegExtraccionSangre(){					 
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					var f = new Date();
					fechaactual=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
					/*if(rowp.EstadoApto=='0'){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado es No Apto Definitivo','warning');	
					}*/
					if(rowp.EstadoApto=='1' && rowp.FechaApto!=fechaactual){
						$.messager.alert('Mensaje de Información', 'El Postulante seleccionado Actualmente No está Apto','warning');	
					}else{
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegExtraccionSangre&IdExamenMedico="+rowp.IdExamenMedico+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
					}
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar un Postulante','warning');						
				}
			}
			
			function editar(){					
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					if(rowp.EstadoAptoExtraccion=='1'){
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=UpdExtraccionSangre&IdExtraccion="+rowp.IdExtraccion+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
					}else{
						$.messager.alert('Mensaje de Información', 'No se puede Editar porque el Nro Donación está Anulado. Debe eliminar, para volver a Registrar la Extracción.','warning');	
					}					
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar una Extraccion a EDITAR','warning');						
				}	
			}
			
			function EliminarExtraccion(){					
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					if(rowp.EstadoAptoExtraccion=='0'){
						$.messager.confirm('Mensaje de Confirmación', '¿Seguro de eliminar la extracción anulada seleccionada?', function(r){
							if (r){
								location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=EliminarExtraccionSangre&IdExtraccion="+rowp.IdExtraccion+"&IdExamenMedico="+rowp.IdExamenMedico+"&IdMovimiento="+rowp.IdMovimiento+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";					
							}
					   });
						
					}else{
						$.messager.alert('Mensaje de Información', 'Solo se puede eliminar extracción anulada','warning');	
					}										
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar una Extraccion a Eliminar','warning');						
				}	
			}
			
			
			$.extend( $( "#FechaCentrif" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#FechaCentrif" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaCentrif" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 
		</script>  
        
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:60px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				/*margin-left:10px;*/
            }
			.fitem2 label{
                display:inline-block;
                width:120px;	
				margin-left:10px;			
            }
			.fitem2 input {	
				width:140px;		 
			}
	</style>       
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>         
        
    
        
<body>

 <!--FORMULARIO REGISTRAR TABULADURA-->
 <div id="dlg-Tabuladura" class="easyui-dialog" style="width:700px;height:250px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fmTabuladura" name="fmTabuladura" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarTabuladura&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
                   
              <div class="fitem2">                
                    <label>Nro Donacion:</label>
                    <input type="text" class="easyui-textbox" name="NroDonacion1" id="NroDonacion1" readonly />
                    <input type="hidden" name="IdExtraccion1" id="IdExtraccion1">
                    <label>Responsable:</label>
                    <select style="width:200px" class="easyui-combobox" id="IdResponsable" name="IdResponsable" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
					  	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
						$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                      $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>
              </div>              
              <div class="fitem2">
               		<label>Tubuladura:</label>  
                    <input type="text" class="easyui-textbox" name="NroTabuladora" id="NroTabuladora" maxlength="8" data-options="prompt:'Tubuladura',required:true"/>
					<label>Rep.Tubuladura:</label>                   
               		<input type="text" class="easyui-textbox" name="NroTabuladora2" id="NroTabuladora2" maxlength="8" data-options="prompt:'Repetir Tubuladura',required:true"/>
                    <!--<label style="width:350px;">Formato: 12L12345 (2Números, 1Letra y 5Números)</label>--> 
              </div> 
              <div class="fitem2">
               		<label>Vol. Total(ML):</label>  
                    <input type="text" class="easyui-numberbox" name="VolumenTotRecol" id="VolumenTotRecol" data-options="prompt:'Volumen Total',min:0,precision:2,required:true" />
              </div>      
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveTabuladura();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-Tabuladura').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO REGISTRAR TABULADURA-->  

<!--FORMULARIO REGISTRAR 1ERA CENTRIFUGACION-->
 <div id="dlg-1eraCentrif" class="easyui-dialog" style="width:700px;height:250px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fm1eraCentrif" name="fm1eraCentrif" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarPrimeraCentrif&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
                   
              <div class="fitem2">
              		<input type="hidden" name="IdExtraccion2" id="IdExtraccion2">
                    <input type="hidden" name="NroDonacion2" id="NroDonacion2">
                    <input type="hidden" name="NroTabuladoraReg" id="NroTabuladoraReg">
              		<label>Fecha 1° Centrif:</label>
                    <input class="easyui-datebox" name="FechaPrimeraCentrif" id="FechaPrimeraCentrif" data-options="required:true" validType="validDate"/>
                 	<label>Hora 1° Centrif:</label>
                    <input class="easyui-timespinner" value="" id="HoraPrimeraCentrif" name="HoraPrimeraCentrif" data-options="required:true,showSeconds:true" />              
              </div>                     
              <div class="fitem2">
               		<label>Volumen PG:</label>  
                    <input type="text" class="easyui-numberbox" name="VolumenPaqueteGlobu" id="VolumenPaqueteGlobu" data-options="prompt:'Volumen Paquete Globular',required:true,min:0,precision:2" />
					<label>Motivo Elimina:</label> 
                    <select style="width:200px" class="easyui-combobox" id="MotivoElimPaqueteGlobu" name="MotivoElimPaqueteGlobu" data-options="prompt:'Seleccione',required:true,
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarMotivoElimPaqueteGlobu(); }">
                      <option value="0">Ninguno</option>
                      <?php
                                      $listar=ListarMotivoElimina('PG'); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select> 
              </div>  
              <div class="fitem2">
              		<label>Volumen PFC(ML):</label> 
               		<input type="text" class="easyui-numberbox" name="VolumenPlasma" id="VolumenPlasma" data-options="prompt:'Volumen Plasma',required:true,min:0,precision:2" />
                    <label>Motivo Elimina:</label>
                    <select style="width:200px" class="easyui-combobox" id="MotivoElimPlasma" name="MotivoElimPlasma" data-options="prompt:'Seleccione',required:true,
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarMotivoElimPlasma(); }">
                      <option value="0">Ninguno</option>
                       <?php
                                      $listar=ListarMotivoElimina('PFC'); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select>                  
               		
              </div> 
              <div class="fitem2">
              		<label>Confirme Tubuladura:</label> 
               		<input type="text" class="easyui-textbox" name="ConfirmaTabuladura" id="ConfirmaTabuladura" maxlength="8" data-options="prompt:'Tubuladura',required:true" />
              		<label>Datos Sistema Fraccionamiento:</label>             
               		<input name="chkllenarDatos" id="chkllenarDatos" type="checkbox" onClick="ListarDatosSistemaFraccionamiento()" value="1">
              </div>     
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="save1eraCentrif();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-1eraCentrif').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO REGISTRAR 1ERA CENTRIFUGACION--> 

<!--FORMULARIO REGISTRAR 2DA CENTRIFUGACION-->
 <div id="dlg-2daCentrif" class="easyui-dialog" style="width:700px;height:250px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fm2daCentrif" name="fm2daCentrif" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarSegundaCentrif&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
                   
              <div class="fitem2">
              		<input type="hidden" name="IdExtraccion3" id="IdExtraccion3">
              		<label>Fecha 2° Centrif:</label>
                    <input class="easyui-datebox" name="FechaSegundaCentrif" id="FechaSegundaCentrif" data-options="required:true" validType="validDate"/>
                 	<label>Hora 2° Centrif:</label>
                    <input class="easyui-timespinner" value="" id="HoraSegundaCentrif" name="HoraSegundaCentrif" data-options="required:true,showSeconds:true" />
              </div> 
               <div class="fitem2">
               		<label>Volumen PQ(ML):</label>  
                    <input type="text" class="easyui-numberbox" name="VolumenPlaquetas" id="VolumenPlaquetas" data-options="prompt:'Volumen Plaquetas',required:true,min:0,precision:2"/>
                    <label>Motivo Elimina:</label>
                    <select style="width:140px" class="easyui-combobox" id="MotivoElimPlaquetas" name="MotivoElimPlaquetas" data-options="prompt:'Seleccione',required:true,
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarMotivoElimPlaquetas(); }">
                      <option value="0">Ninguno</option>
                       <?php
                                      $listar=ListarMotivoElimina('PQ'); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select> 					
              </div>      
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="save2daCentrif();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-2daCentrif').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO REGISTRAR 2DA CENTRIFUGACION-->  
   
		<div style="margin:0px 0;"></div>
        <div id="tb" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">
				<!--<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="RegExtraccionSangre()">Fraccionamiento Sangre</a>-->
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Editar Extracción</a> 
                <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onClick="EliminarExtraccion()">Eliminar Extracción</a> 		 
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>          			
				<!-- <a href="#" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="ayuda();">Ayuda</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="salir();">Salir</a>-->
			</div> 
       </div>
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Lista de Espera para Fraccionamiento y Centrifugación" style="width:100%;height:80%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:true,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	   <th field="NroDonacion" width="40">Nro Donacion</th>
            	   <th field="FechaExtraccion" width="60">Fecha y H. Extracción</th>                   
                   <th field="Postulante" width="120">Postulantes en Espera de Fraccionamiento</th>  
                   <th field="SexoPostulante" width="50">Sexo Postulante</th>
                   <th field="GrupoSanguineoPostulante" width="50">G.S.Postulante</th>                  
                   <th field="TipoDonante" width="50">Tipo Donante</th>
                   <th field="TipoDonacion" width="50">Tipo Extraccion</th>                
                  
                   <th data-options="field:'Tabuladura',formatter:quickTabuladura" align="center" width="30">Tubuladura</th>
                   <th data-options="field:'NroTabuladoraReg',formatter:quick1eraCentrif" align="center" width="40">1ra Centrif.</th>
                   <th data-options="field:'2daCentrif',formatter:quick2daCentrif" align="center" width="40">2da Centrif.</th>
                   <!--formatter="formatPrioridad"-->                   	
			</tr>
		</thead>
	</table>
     
   <script type="text/javascript">	
   $("#dg").datagrid({
		// Fires when data in datagrid is loaded successfully
		onLoadSuccess:function(){	
			// Get this datagrid's panel object
			$(this).datagrid('getPanel')
			// for all easyui-linkbutton <a>'s make them a linkbutton
			.find('a.easyui-linkbutton').linkbutton();
		}
    });
   
    //Tabuladura
	function quickTabuladura(val,row){
      //var url = "print.php?id="+ row.NroDonacion;	 	  
	  //return '<a href="'+url +'" class="easyui-linkbutton" iconCls="icon-add"></a>'; 	  
	  //return "<a onclick='RegTabuladura("+val+","+row.NroDonacion+",'"+row.TipoDonacion+"');' class='easyui-linkbutton' iconCls='icon-add'></a>"; 
	  if(row.EstadoAptoExtraccion=='1'){ 
		if(row.IdTipoDonacion=="2"){ //AFERESIS
			return 'Terminado';   
		}else{ //Sangre Total
			if(row.EstadoExtraccion.trim()=="1"){  
		 		return '<a  onclick="verDetalle('+val+','+row.NroDonacion+','+row.IdExtraccion+','+row.VolumenTotRecol+');" class="easyui-linkbutton" iconCls="icon-add"></a>';   
			}else if(row.EstadoExtraccion=="2"){
				return 'OK';   
			}
		}//end else	
	  }else{//
		  return 'Anulado'; 
	  }
	  
    }	
	function verDetalle(val,NroDonacion,IdExtraccion,VolumenTotRecol){ 				 	
		$('#dlg-Tabuladura').dialog('open').dialog('setTitle','Registrar Tubuladura '+NroDonacion);	
		$('#FechaCentrif').datebox('setValue', '');
		//$('#IdResponsable').combobox('setValue', '');
		$('#NroTabuladora').textbox('setValue', '');
		$('#NroTabuladora2').textbox('setValue', '');	
		$('#VolumenTotRecol').numberbox('setValue', VolumenTotRecol);		
		$('#NroDonacion1').textbox('setValue', NroDonacion);	
		document.getElementById('IdExtraccion1').value=IdExtraccion;							
	}
	
	function saveTabuladura(){			
		var NroTabuladora=$('#NroTabuladora').textbox('getValue');	
		var NroTabuladora2=$('#NroTabuladora2').textbox('getValue');
				
		//numeros = /^[0-9]+$/;
		//letras = /^[a-zA-Z]+$/;	
					
		 $('#fmTabuladura').form('submit', {			
			onSubmit: function(){			
				// return false to prevent submit;				
				if($(this).form('validate')==false){					
					return $(this).form('validate');					
				}/*else if(!NroTabuladora.match(/^[0-9]{2}[A-Z]{1}[0-9]{5}$/)) {
					$.messager.alert('Mensaje de Información', 'El Formato de la Tabuladura es incorrecto','warning');
					return 0;					
				}*/else if(NroTabuladora!=NroTabuladora2){					
					$.messager.alert('Mensaje de Información', 'El Nro de Tabuladoras no coinciden','warning');
					return 0;
				}			
			},
			success:function(data){
				//$('#fmTabuladura').submit();//MANDA 2 VECES
				//$('#dlg-Tabuladura').dialog('close');
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Fraccionamiento&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
			}
		});//
		//$('#fmTabuladura').submit();
	}
	
	//1eraCentrif
	function quick1eraCentrif(val,row,index){ 
	  if(row.EstadoAptoExtraccion=='1'){
		if(row.IdTipoDonacion=="2"){ //AFERESIS
			return 'Terminado';   
		}else{ //Sangre Total
			if(row.EstadoExtraccion.trim()=="2" && row.EstadoFraccionamiento=="1"){ //EstadoFraccionamiento=1 (Falta 1eraCentrif) 				
				/*return '<a  onclick="Reg1eraCentrif('+val+','+index+','+row.NroDonacion+','+row.IdExtraccion+','+row.IdTipoSexo+');" class="easyui-linkbutton" iconCls="icon-add"></a>';*/ 
				return '<a onclick=\"Reg1eraCentrif('+index+','+row.NroDonacion+','+row.IdExtraccion+','+row.IdTipoSexo+');\" class="easyui-linkbutton" iconCls="icon-add"></a>';
			}else if(row.EstadoExtraccion=="1"){
				return 'Falta Tab.';   
			}else{
				return 'OK';    
			} 
		}//end else	
	  }else{//
		  return 'Anulado'; 
	  } 	    
    }	
	function Reg1eraCentrif(index,NroDonacion,IdExtraccion,IdTipoSexo){//NroTabuladoraReg no llamaba por ser texto(solucion llamarlo por index)					 	
		$('#dlg-1eraCentrif').dialog('open').dialog('setTitle','Registrar Datos Primera Centrifugación '+NroDonacion);
		//var rowc = $('#dg').datagrid('getSelected');
		//$('#fm1eraCentrif').form('load',rowc);  
		$('#HoraPrimeraCentrif').timespinner('setValue', '');
		$('#VolumenPaqueteGlobu').numberbox('setValue', '');
		$('#ConfirmaTabuladura').textbox('setValue', '');
		document.getElementById('chkllenarDatos').checked=false;		
		document.getElementById('NroDonacion2').value=NroDonacion;
		document.getElementById('IdExtraccion2').value=IdExtraccion;		
		
		var val = $('#dg').datagrid('getRows')[index].NroTabuladoraReg; //Obtener fila por index
		document.getElementById('NroTabuladoraReg').value=val;	
				
		if(IdTipoSexo=='2'){
			$('#VolumenPlasma').numberbox('setValue', '');
			$('#MotivoElimPlasma').combobox('setValue', '6');
		}else{
			$('#VolumenPlasma').numberbox('setValue', '');
			$('#MotivoElimPlasma').combobox('setValue', '0');
		}											
	}
	
	function save1eraCentrif(){	
		var NroTabuladoraReg=document.getElementById('NroTabuladoraReg').value;	
		var ConfirmaTabuladura=$('#ConfirmaTabuladura').textbox('getValue');
		
		var VolumenPaqueteGlobu=$('#VolumenPaqueteGlobu').numberbox('getValue');
		var MotivoElimPaqueteGlobu=$('#MotivoElimPaqueteGlobu').combobox('getValue');
		
		if(parseInt(VolumenPaqueteGlobu)==0 && MotivoElimPaqueteGlobu==0){
			//$.messager.alert('Mensaje de Información', 'El Volumen de PG es 0. Seleccione Motivo Elimina PG','warning');		
			$.messager.alert({
				title: 'Mensaje',
				msg: 'El Volumen de PG es 0. Seleccione Motivo Elimina PG',
				icon:'warning',
				fn: function(){
					$('#MotivoElimPaqueteGlobu').next().find('input').focus();
				}
			});
			return 0;	
		}
		
		var VolumenPlasma=$('#VolumenPlasma').numberbox('getValue');
		var MotivoElimPlasma=$('#MotivoElimPlasma').combobox('getValue');
		if(parseInt(VolumenPlasma)==0 && MotivoElimPlasma==0){
			//$.messager.alert('Mensaje de Información', 'El Volumen de PFC es 0. Seleccione Motivo Elimina PFC','warning');		
			$.messager.alert({
				title: 'Mensaje',
				msg: 'El Volumen de PFC es 0. Seleccione Motivo Elimina PFC',
				icon:'warning',
				fn: function(){
					$('#MotivoElimPlasma').next().find('input').focus();
				}
			});
			return 0;	
		}		
		
		$('#fm1eraCentrif').form('submit', {			
			onSubmit: function(){			
				// return false to prevent submit;				
				if($(this).form('validate')==false){					
					return $(this).form('validate');					
				}/*else if(!ConfirmaTabuladura.match(/^[0-9]{2}[A-Z]{1}[0-9]{5}$/)) {
					//$.messager.alert('Mensaje de Información', 'La Tabuladura NO coincide con el Registrado y El Formato es incorrecto','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'La Tabuladura NO coincide con el Registrado y el Formato es incorrecto',
						icon:'warning',
						fn: function(){
							$('#ConfirmaTabuladura').next().find('input').focus();
						}
					});
					return 0;					
				}*/else if(NroTabuladoraReg!=ConfirmaTabuladura){					
					$.messager.confirm('Mensaje de Confirmación', 'La Tabuladora NO coincide con el Registrado, ¿Seguro de Cambiarlo?', function(r){
						if (r){
							$('#fm1eraCentrif').submit();					
						}
				   });
				   return 0;
				}		
			},
			success:function(data){
				$('#fm1eraCentrif').submit();
			}
		});//						
			
	}
	
	//2daCentrif
	function quick2daCentrif(val,row){ 
	  if(row.EstadoAptoExtraccion=='1'){ 
		if(row.IdTipoDonacion=="2"){ //AFERESIS
			//return 'Terminado'; 
			return '<a  onclick="Reg2daCentrif('+val+','+row.NroDonacion+','+row.IdExtraccion+');" class="easyui-linkbutton" iconCls="icon-add"></a>';  
		}else{ //Sangre Total
			if(row.EstadoExtraccion.trim()=="2" && row.EstadoFraccionamiento=="3"){ //EstadoFraccionamiento=3 (Falta 2daCentrif) 
				return '<a  onclick="Reg2daCentrif('+val+','+row.NroDonacion+','+row.IdExtraccion+');" class="easyui-linkbutton" iconCls="icon-add"></a>';
			}else if(row.EstadoExtraccion=="1"){
				return 'Falta Tab.';   
			}else if(row.EstadoFraccionamiento=="1"){
				return 'Falta 1ra Centrif.';   
			}else{
				return 'OK';    
			} 
		}//end else
	  }else{//
		  return 'Anulado'; 
	  }	      
    }
	function Reg2daCentrif(val,NroDonacion,IdExtraccion){					 	
		$('#dlg-2daCentrif').dialog('open').dialog('setTitle','Registrar Datos Segunda Centrifugación '+NroDonacion);	
		$('#HoraSegundaCentrif').timespinner('setValue', '');
		$('#VolumenPlaquetas').numberbox('setValue', '');	
		//$('#NroDonacion3').textbox('setValue', NroDonacion);
		document.getElementById('IdExtraccion3').value=IdExtraccion;									
	}		
	
	function save2daCentrif(){
		var VolumenPlaquetas=$('#VolumenPlaquetas').numberbox('getValue');
		var MotivoElimPlaquetas=$('#MotivoElimPlaquetas').combobox('getValue');
		if(parseInt(VolumenPlaquetas)==0 && MotivoElimPlaquetas==0){
			//$.messager.alert('Mensaje de Información', 'El Volumen de PFC es 0. Seleccione Motivo Elimina PFC','warning');		
			$.messager.alert({
				title: 'Mensaje',
				msg: 'El Volumen de PQ es 0. Seleccione Motivo Elimina PQ',
				icon:'warning',
				fn: function(){
					$('#MotivoElimPlaquetas').next().find('input').focus();
				}
			});
			return 0;	
		}
		
		$('#fm2daCentrif').form('submit', {			
			onSubmit: function(){			
				// return false to prevent submit;				
				if($(this).form('validate')==false){					
					return $(this).form('validate');					
				}			
			},
			success:function(data){
				$('#fm2daCentrif').submit();
			}
		});//				
	}	
	
	
	$(function(){	  
	  $('#NroTabuladora').textbox('textbox').attr('maxlength', $('#NroTabuladora').attr("maxlength"));
	  $('#NroTabuladora2').textbox('textbox').attr('maxlength', $('#NroTabuladora2').attr("maxlength"));	 
	  $('#ConfirmaTabuladura').textbox('textbox').attr('maxlength', $('#ConfirmaTabuladura').attr("maxlength"));
	});	
	
function validaEntero(){	
 	/*//alert('validaEntero');
	var s = $.fn.searchbox.defaults.formatter(/[0-9]/);
	if(s==value){
		return true;
	}else{
		$("#IdCuenta" ).searchbox('setValue', '');
		return false;
	}*/
}

function ListarDatosSistemaFraccionamiento(){
	var NroDonacion2=document.getElementById('NroDonacion2').value;
	if(document.getElementById('chkllenarDatos').checked==true){
		//alert(NroDonacion);
			  $.ajax({
				url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarDatosSistemaFraccionamiento",					
				type: "GET",
				dataType: "JSON",
				data: {						
					NroDonacion: NroDonacion2						
				},							
					success: function (data) {		
						//$.messager.alert('SIGESA','correcto' ,'info'); 					
							$('#FechaPrimeraCentrif').datebox('setValue', data.FechaPrimeraCentrif); 														
							$('#HoraPrimeraCentrif').timespinner('setValue', data.HoraPrimeraCentrif); 
							$('#VolumenPaqueteGlobu').numberbox('setValue', data.GR); 	
							$('#VolumenPlasma').numberbox('setValue', data.PLASMA); 
					}				
				}).done(function (data) {
                    console.log(data);
                    //alert(data);						
					if(data=="") { //if(!data.success){								
						$.messager.alert('SIGESA','El Nro Donacion No existe en el Sistema Fraccionamiento' ,'info'); //NO HAY DATOS									
					}
				});	//fin done		
	}else{
		$('#FechaPrimeraCentrif').datebox('setValue', ''); 														
		$('#HoraPrimeraCentrif').timespinner('setValue', ''); 
		$('#VolumenPaqueteGlobu').numberbox('setValue', ''); 	
		$('#VolumenPlasma').numberbox('setValue', ''); 
		
	}	
}

function cambiarMotivoElimPaqueteGlobu(){
	var MotivoElimPaqueteGlobu=$('#MotivoElimPaqueteGlobu').combobox('getValue');	
	if(MotivoElimPaqueteGlobu!=0){
		$('#VolumenPaqueteGlobu').numberbox('setValue', 0); 
	}else{
		$('#VolumenPaqueteGlobu').numberbox('setValue', ''); 
	}	
}

function cambiarMotivoElimPlasma(){
	var MotivoElimPlasma=$('#MotivoElimPlasma').combobox('getValue');	
	if(MotivoElimPlasma!=0){
		$('#VolumenPlasma').numberbox('setValue', 0); 
	}else{
		$('#VolumenPlasma').numberbox('setValue', ''); 
	}	
}

function cambiarMotivoElimPlaquetas(){
	var MotivoElimPlaquetas=$('#MotivoElimPlaquetas').combobox('getValue');	
	if(MotivoElimPlaquetas!=0){
		$('#VolumenPlaquetas').numberbox('setValue', 0); 
	}else{
		$('#VolumenPlaquetas').numberbox('setValue', ''); 
	}	
}



	 
	function getData(){
			var rows = [];			
			<?php			
			 $i = 1;					
			 $Listar=Listar_ExtraccionSangreM("");				
			 if($Listar!=NULL){   
				foreach($Listar as $item)
				{
					$FechaExtraccion=vfecha(substr($item['FechaExtraccion'],0,10)).' '.substr($item['HoraInicio'],0,8);					
					$NroDonacion=$item['NroDonacion'];		
					
					$DatosFraccionamiento=SIGESA_ListarExtraccionFraccionamiento_M($item['IdExtraccion']);
					if($DatosFraccionamiento!=NULL){
						$EstadoFraccionamiento=$DatosFraccionamiento[0]['Estado'];
						$NroTabuladoraReg=$DatosFraccionamiento[0]['NroTabuladora'];
									
					}else{
						$EstadoFraccionamiento="0";
						$NroTabuladoraReg="0";
					}
					
					//Si encuentra estos caracteres no muestra el listado					
					$novalidados = array("'", ",");	
									
					$xPostulante=$item['ApellidosPostulante'].' '.$item['NombresPostulante'];					
					$PostulanteV = str_replace($novalidados, "", $xPostulante);
					
					$xPaciente=$item['Paciente'];					
					$PacienteV = str_replace($novalidados, "", $xPaciente);					
					
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							FechaExtraccion: '<?php echo $FechaExtraccion;?>',
							CodigoPostulante: '<?php echo $item['CodigoPostulante'];?>',
							NroDocumento: '<?php echo $item['NroDocumento'];?>',
							Postulante: '<?php echo $PostulanteV;?>',
							GrupoSanguineoPostulante: '<?php echo $item['GrupoSanguineoPostulante'];?>',
							SexoPostulante: '<?php echo $item['Sexo'];?>',
							IdTipoSexo: '<?php echo $item['IdTipoSexo'];?>',
							TipoDonante:  '<?php echo $item['TipoDonante'];?>' ,
							CondicionDonante:  '<?php echo $item['CondicionDonante'];?>',
							TipoDonacion:  '<?php echo $item['TipoDonacion'];?>',
							NroDonacion	:  '<?php echo $NroDonacion;?>',					
							Paciente:'<?php echo $PacienteV;?>',			
							
							IdExamenMedico:'<?php echo $item['IdExamenMedico'];?>',
							EstadoApto:'<?php echo $item['EstadoApto'];?>',
							IdMovimiento:'<?php echo $item['IdMovimiento'];?>',
							IdTipoDonacion:'<?php echo $item['IdTipoDonacion'];?>',
							IdExtraccion:'<?php echo $item['IdExtraccion'];?>',
							VolumenTotRecol:'<?php echo $item['VolColeccionReal'];?>',
							EstadoExtraccion:'<?php echo $item['EstadoExtraccion'];?>',
							EstadoFraccionamiento:'<?php echo $EstadoFraccionamiento;?>',
							NroTabuladoraReg:'<?php echo $NroTabuladoraReg;?>',
							EstadoAptoExtraccion:'<?php echo $item['EstadoAptoExtraccion'];?>'
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}
		
		
		$('#dg').datagrid({
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){
			var dg =$('#dg').datagrid({data:getData()}).datagrid({			
			//var dg =$('#dg').datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});	
		
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
			
			$(function () {	
				$('#IdResponsable').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdResponsable"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					} 								
				});						
				$('#IdResponsable').combobox('validate');	
							
				$('#MotivoElimPaqueteGlobu').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimPaqueteGlobu"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					} 								
				});						
				$('#MotivoElimPaqueteGlobu').combobox('validate');			
				
				$('#MotivoElimPlasma').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimPlasma"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					} 								
				});						
				$('#MotivoElimPlasma').combobox('validate');
				
				$('#MotivoElimPlaquetas').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimPlaquetas"]'	,
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0;  
					}							
				});						
				$('#MotivoElimPlaquetas').combobox('validate');						
				
			});					

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>