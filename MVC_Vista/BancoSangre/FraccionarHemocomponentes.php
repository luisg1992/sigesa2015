<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Fraccionar Hemocomponentes</title>
</head>    
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		form{
                margin:0;
                padding:10px 30px;
            }		
			#fmCabHemocomponente{
                margin:0;
                padding:10px 20px;
            }
			#fmCabHemocomponenteCrio{
                margin:0;
                padding:10px 20px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:90px;
							
            }
            .fitem input{
				display:inline-block;
                width:100px;	
				margin-left:10px;			
            }
			
	</style> 
    
    <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>   
    
<script type="text/javascript" >			
		
		$.extend($("#FecMotivo").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FecMotivo").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});					
		
		function regresar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=StockSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}
		
		function Buscar(){
			document.getElementById("form1").submit();
		}			
	
    </script>

<body>

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->
	<div style="margin:0px 0;"></div>
	 <form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FraccionarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">  
            	             	 
		        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="FraccionarHemocomponente()">Fraccionar Hemocomponente</a>
				<!--<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="PrepararCrioprecipitado()" >Convertir PFC a CRIO</a>  -->                              
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="Imprimir();">Imprimir</a>	
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="Ayuda();">Ayuda</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="regresar();">Regresar</a> 
			</div>
            
              <div>
               <strong>Busqueda de Hemocomponentes Disponibles:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
                   <select name="TipoHemB" id="TipoHemB" class="easyui-combobox" style="width:80px;" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }">
                        <option value=""></option>
                        <option value="PG" <?php if($TipoHem=='PG'){?> selected <?php } ?> >PG </option>
                        <option value="PFC" <?php if($TipoHem=='PFC'){?> selected <?php } ?> >PFC </option>
                        <option value="PQ" <?php if($TipoHem=='PQ'){?> selected <?php } ?>>PQ </option>
                        <option value="CRIO" <?php if($TipoHem=='CRIO'){?> selected <?php } ?>>CRIO </option>
                    </select>-<input id="SNCSB"  name="SNCSB" type="text" class="easyui-textbox" maxlength="7" value="<?php echo $SNCS ?>" data-options="prompt:'SNCS',
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = document.form1.submit(); }" />
                    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onClick="Buscar();">Buscar</a>
                </div>
            			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Fraccionar Hemocomponentes Disponibles" style="width:1200px;height:450px" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:true,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	<th field="NroDonacion" width="80">N°Donacion</th>
				<th field="TipoHem"  width="100">Hemocomponente</th>				
                <th field="SNCS"  width="80">SNCS</th>	
				<th field="GrupoSanguineo"  width="100">GS</th>
                <th field="VolumenTotRecol"  width="100">Volumen Total</th>
                <th field="VolumenFraccion"  width="100">Volumen Fraccionado</th>
                <th field="VolumenRestante"  width="100">Volumen Restante</th>
				<th field="NroTabuladora" width="80">tubuladura</th>
                <th field="FechaExtraccion" width="100">F. Extracción</th>
                <th field="FechaHoraVencimiento" width="140">F. Vencimiento</th>
                <th data-options="field:'IdMovDetalle',formatter:VerFracciones" align="center" width="100">Ver Fracciones</th>
			</tr>
		</thead>
	</table>
     </form>
    

<!--FORMULARIO Fraccionar-->
 <div id="dlg-CabHemocomponente" class="easyui-dialog" style="width:650px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmCabHemocomponente" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarFraccionarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            	<div class="fitem">
                	<input type="hidden" name="IdMovDetalle" id="IdMovDetalle" /> 
                    <label>Tipo Hemoc-SNCS:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" readonly>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>-<input id="SNCS"  name="SNCS" type="text" class="easyui-textbox" maxlength="7" data-options="prompt:'SNCS'" readonly />  
                    
                    <label>Fecha Fracciona:</label>                   
                    <input name="FechaPreparaFraccion" id="FechaPreparaFraccion" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" /> 
                    <input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraPreparaFraccion" name="HoraPreparaFraccion" data-options="showSeconds:true,required:true" style="width:85px" />                               
                                       
              </div>   
              
              <div class="fitem">
              		 <label>Método:</label>
                    <Select style="width:160px" class="easyui-combobox" id="MetodoPreparaFraccion" name="MetodoPreparaFraccion" data-options="prompt:'Seleccione',required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = cambiarMetodo(); }">
                      <option value=""></option>
                      <option value="Manual">Manual</option>
                      <option value="Automatico">Automático(Conector Esteril)</option>
                  
                    </select>     
                   <label>Fecha Vencimiento:</label>                    
                   <input name="FechaVencimiento" id="FechaVencimiento" class="easyui-datebox" value="" style="width:105px" data-options="required:true" readonly /> 
                   <input type="hidden" name="FechaVencimiento2" id="FechaVencimiento2" /> 
                   <input class="easyui-timespinner" value="" id="HoraVencimiento" name="HoraVencimiento" data-options="showSeconds:true" style="width:85px" />                           
              </div> 
              
              <div class="fitem"> 
              	  <label>Volumen Restante(mL):</label>                    
                   <input name="VolumenRestante" id="VolumenRestante" class="easyui-numberbox" value="" style="width:105px" data-options="required:true" readonly />               		    
                   <label>Volumen de Fracción(mL):</label>                    
                   <input name="VolumenFraccion" id="VolumenFraccion" class="easyui-numberbox" value="" style="width:105px" data-options="precision:2,required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = cambiarVolumenFraccion(); }" />                         
              </div>   
              
                      
                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveFraccionarHemocomponente();" style="width:90px">Fraccionar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-CabHemocomponente').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO Fraccionar-->  
    
    
     <div id="dlg-VerDetalle" class="easyui-dialog" style="width:700px;height:360px;"
			closed="true"   buttons="#dlg-buttonsVerdet">
      
       <table  class="easyui-datagrid" id="dg_DetalleVerdet" title="Ver Detalle" style="width:670px;height:280px" data-options="
				iconCls: 'icon-edit',
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10"   url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=verdetalleFraccion&IdMovDetalle="+rowc.IdMovDetalle+"" >
		<thead>
			<tr>
            	<th field="TipoHem"  width="100">Hemocomponente</th>
                <th field="SNCS"  width="80">SNCS</th>	
				<th field="GrupoSanguineo"  width="60">GS</th>
                <th field="VolumenTotRecol"  width="120">Volumen Preparado</th>
				<th field="MetodoPreparaFraccion" width="80">Método</th>
                <th field="FechaPreparaFraccion" width="140">F. Preparación</th>                         
			</tr>
		</thead>
	</table>    

      <div id="dlg-buttonsVerdet"> 
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-VerDetalle').dialog('close')" style="width:90px">Cerrar</a>
	  </div>
	</div>
    
   <!-- fin ver detalle-->
    
   <script> 
   
   $("#dg").datagrid({
		// Fires when data in datagrid is loaded successfully
		onLoadSuccess:function(){	
			// Get this datagrid's panel object
			$(this).datagrid('getPanel')
			// for all easyui-linkbutton <a>'s make them a linkbutton
			.find('a.easyui-linkbutton').linkbutton();
		}
    });
   
   
    //VerFracciones
	function VerFracciones(val,row){
	  if(parseInt(row.VolumenFraccion)>0){
      	return '<a  onclick="VerDetalle('+val+','+row.NroDonacion+','+row.IdMovDetalle+');" class="easyui-linkbutton" iconCls="icon-search"></a>'; 
	  }else{
		 return 'NO Fraccionado'; 
	  }
	  
    }	
	function VerDetalle(val,NroDonacion,IdMovDetalle){  	
			
			//var rowc = $('#dg').datagrid('getSelected');
			//if (rowc){										
					$('#dlg-VerDetalle').dialog('open').dialog('setTitle','Detalle de Fracciones');						
					//('#dg_DetalleVerdet').datagrid({   title:NroDonacion});
					//$('#dg_DetalleVerdet').datagrid('load',rowc);      							
			// }else{
				//$.messager.alert('Mensaje de Información','Seleccione un Periodo de Vacacion','warning');										
				//return 0;	
			 //}			
				
			$('#dg_DetalleVerdet').datagrid('load', {
				IdMovDetalle: IdMovDetalle
			});					
	}
	
	function cambiarMetodo(){
		var MetodoPreparaFraccion=$('#MetodoPreparaFraccion').combobox('getValue');
		var FechaPreparaFraccion=$('#FechaPreparaFraccion').datebox('getText');
		var HoraPreparaFraccion=$('#HoraPreparaFraccion').datebox('getText');
		
			if(MetodoPreparaFraccion.trim()=="Manual"){
				farray= FechaPreparaFraccion.split('/');					
				var y = parseInt(farray[2]);
				var m = parseInt(farray[1]);
				var d = parseInt(farray[0]);
				FechaVencimiento=(d+1)+'/'+m+'/'+y;												
				$('#FechaVencimiento').datebox('setValue',FechaVencimiento);				
				$('#HoraVencimiento').timespinner('setValue',HoraPreparaFraccion);						
			}else{
				FechaVencimiento2=document.getElementById('FechaVencimiento2').value;
				$('#FechaVencimiento').datebox('setValue',FechaVencimiento2);
				$('#HoraVencimiento').timespinner('setValue','');	
			}	
	}
	
	function cambiarVolumenFraccion(){
		var VolumenRestante=$('#VolumenRestante').numberbox('getText');
		var VolumenFraccion=$('#VolumenFraccion').numberbox('getText');
		if(parseFloat(VolumenFraccion)>=parseFloat(VolumenRestante)){
			$.messager.alert('Mensaje','El volumen de la Fracción debe ser menor a '+ VolumenRestante,'info');
			$('#VolumenFraccion').next().find('input').focus();
			return 0;									
		}
		
	}
   
   function FraccionarHemocomponente(){	
   					
			var FechaActual='<?php echo $FechaServidor ?>';
			var rowp = $('#dg').datagrid('getSelected');
			
			if (rowp){				
				if(rowp.FechaVencimiento3<FechaActual){					
					$.messager.confirm('Mensaje', 'Unidad venció el '+rowp.FechaHoraVencimiento+', ¿Consultó con el jefe de servicio?', function(r){
						if (r){
							$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Fraccionar Hemocomponente');						
							$('#fmCabHemocomponente').form('load',rowp);
							$('#VolumenFraccion').numberbox('clear');
						}
					});						
				}else{
					$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Fraccionar Hemocomponente');						
					$('#fmCabHemocomponente').form('load',rowp);
					$('#VolumenFraccion').numberbox('clear');						
				}				
				
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente a Fraccionar','warning');
				return 0;			
			}
		} 
		
	function saveFraccionarHemocomponente(){
			
			var FechaPreparaFraccion=$('#FechaPreparaFraccion').datebox('getText');
			 if(FechaPreparaFraccion.trim()==""){
				$.messager.alert('Mensaje','Seleccione una Fecha de Preparación de la Fracción Pediátrica','info');
				$('#FechaPreparaFraccion').next().find('input').focus();
				return 0;			
			}
			
			var MetodoPreparaFraccion=$('#MetodoPreparaFraccion').combobox('getValue');
			if(MetodoPreparaFraccion.trim()==""){
				$.messager.alert('Mensaje','Seleccione el Metodo de Preparación de la Fracción Pediátrica','info');
				$('#MetodoPreparaFraccion').next().find('input').focus();
				return 0;			
			}			
			
			var VolumenFraccion=$('#VolumenFraccion').numberbox('getText');
			 if(VolumenFraccion.trim()=='' || parseFloat(VolumenFraccion)<=0){
				$.messager.alert('Mensaje','Ingrese el Volumen de la Fracción Pediátrica','info');
				$('#VolumenFraccion').next().find('input').focus();
				return 0;			
			}		
			//document.getElementById("fmCabHemocomponente").submit();
			var TipoHem=$('#TipoHem').combobox('getValue');			
			var SNCS=$('#SNCS').textbox('getValue');
			$.messager.confirm('Mensaje', '¿Seguro de Fraccionar el Hemocomponente '+ TipoHem +'-'+SNCS+'?', function(r){
				if (r){
					document.getElementById("fmCabHemocomponente").submit();
				}
			});	
			
		}		
	
	
	$(function(){  	
	  $('#SNCSB').numberbox('textbox').attr('maxlength', $('#SNCSB').attr("maxlength"));
	   
	});	
	
		function getData(){
			var rows = [];			
			
	<?php 
	
	 $BuscarSNCSDisponible = BuscarSNCSDisponible_M($TipoHem,$SNCS);
     $i = 1;										
	if($BuscarSNCSDisponible!=NULL){
		foreach($BuscarSNCSDisponible as $item)
		{
						
			 ?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TipoHem: '<?php echo $item["TipoHem"];?>',
					TipoHem2: '<?php echo $item["TipoHem"];?>',
					NroDonacion: '<?php echo $item["NroDonacion"]; ?>',											
					SNCS:'<?php echo  $item["SNCS"]; ?>',
					SNCS2:'<?php echo  $item["SNCS"]; ?>',
					GrupoSanguineo:'<?php echo  trim($item["GrupoSanguineo"]); ?>',
					
					VolumenTotRecol:'<?php echo  $item["VolumenTotRecol"]; ?>',
					VolumenFraccion:'<?php echo  $item["VolumenFraccion"]; ?>',		
					VolumenRestante:'<?php echo  $item["VolumenRestante"]; ?>',	
									
					NroTabuladora:'<?php echo  $item["NroTabuladora"]; ?>',
					FechaExtraccion:'<?php echo  vfecha(substr($item["FechaExtraccion"],0,10)); ?>',				
					FechaVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)); ?>',
					FechaVencimiento2:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)); ?>',
					IdMovDetalle:'<?php echo  $item["IdMovDetalle"]; ?>',
					MovNumero:'<?php echo  $item["MovNumero"]; ?>',
					MovNumero2:'<?php echo  $item["MovNumero"]; ?>',
					
					FechaVencimiento3:'<?php echo  $item["FechaVencimiento"]; ?>',
					FechaHoraVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)).' '.substr($item["FechaVencimiento"],11,8);; ?>',
			
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});
		
		</script> 
     
   
	
  
</body>
</html>