//JS DE BUSQUEDAS POSTULANTE Y PACIENTES
//1. BUSQUEDA POR Nro Documento DEL POSTULANTE
//2. FILTRAR COMBOGRID Apellidos y Nombres POSTULANTE con su funcion de seleccion
//3. FILTRAR COMBOGRID Apellidos y Nombres PACIENTE con su funcion de seleccion
//4. BUSQUEDA RECEPTOR POR Nro Documento,APELLIDOS y NOMBRES, Y Nro Historia

	function LlenarDatosPostulanteExiste(data){ //esta funcion se invoca en otras funciones
		$('#MensajeExiste').html('Postulante ya Existe');
		$('#CodigoPostulante').textbox('setValue', data.CodigoPostulante); 
		document.getElementById('IdPostulante').value=data.IdPostulante;
		
		$('#IdDocIdentidad').combobox('setValue', data.IdDocIdentidad);		
		$('#NroDocumento').numberbox('setValue', data.NroDocumento);						
		$('#ApellidoPaterno').textbox('setValue', data.ApellidoPaterno);
		$('#ApellidoMaterno').textbox('setValue', data.ApellidoMaterno);	
		$('#PrimerNombre').textbox('setValue', data.PrimerNombre);
		$('#SegundoNombre').textbox('setValue', data.SegundoNombre);
		$('#TercerNombre').textbox('setValue', data.TercerNombre);						
		
		$('#DireccionDomicilio').textbox('setValue', data.DireccionDomicilio);
		$('#IdTipoSexo').combobox('setValue', data.IdTipoSexo);	
		$('#FechaNacimiento').datebox('setValue', data.FechaNacimiento);
		$('#Edad').numberbox('setValue', data.Edad);
		
		$('#IdEstadoCivil').combobox('setValue', data.IdEstadoCivil);
		$('#IdTipoOcupacion').combobox('setValue', data.IdTipoOcupacion);
		$('#Telefono').textbox('setValue', data.Telefono);	
		$('#IdGrupoSanguineo').combobox('setValue', data.IdGrupoSanguineo);	
		$('#Email').textbox('setValue', data.Email);
		/*$('#IdTipoDonacion').combobox('setValue', data.IdTipoDonacion);
		$('#IdTipoDonante').combobox('setValue', data.IdTipoDonante);
		$('#IdTipoRelacion').combobox('setValue', data.IdTipoRelacion);
		$('#IdCondicionDonante').combobox('setValue', data.IdCondicionDonante);
		$('#Observaciones').textbox('setValue', data.Observaciones);
		$('#FechaMovi').datebox('setValue', data.FechaMovi);
		$('#HoraMovi').textbox('setValue', data.HoraMovi);*/
		
		//Datos Domicilio-Procedencia-Nacimiento
		$('#IdPaisDomicilio').combobox('setValue', data.IdPaisDomicilio);
		$('#IdPaisProcedencia').combobox('setValue', data.IdPaisProcedencia);
		$('#IdPaisNacimiento').combobox('setValue', data.IdPaisNacimiento);
							
		$('#NomDepD').combobox('setValue', data.IdDepD);
		CambiaDepartamentoD();					
		$('#NomProvD').combogrid('setValue', data.IdProvD);
		CambiaProvinciaD();
		$('#IdDistritoDomicilio').combogrid('setValue', data.IdDistritoDomicilio);
		CambiaDistritoD();
		$('#IdCentroPobladoDomicilio').combobox('setValue', data.IdCentroPobladoDomicilio);
							
		$('#NomDepP').combobox('setValue', data.IdDepP);
		CambiaDepartamentoP();					
		$('#NomProvP').combogrid('setValue', data.IdProvP);
		CambiaProvinciaP();
		$('#IdDistritoProcedencia').combogrid('setValue', data.IdDistritoProcedencia);
		CambiaDistritoP();
		$('#IdCentroPobladoProcedencia').combobox('setValue', data.IdCentroPobladoProcedencia);
		
		$('#NomDepN').combobox('setValue', data.IdDepN);
		CambiaDepartamentoN();					
		$('#NomProvN').combogrid('setValue', data.IdProvN);
		CambiaProvinciaN();
		$('#IdDistritoNacimiento').combogrid('setValue', data.IdDistritoNacimiento);
		CambiaDistritoN();
		$('#IdCentroPobladoNacimiento').combobox('setValue', data.IdCentroPobladoNacimiento);			
		$('#NroDocumentoBus').numberbox('setValue','');	
	}
	
	//1. BUSQUEDA POR Nro Documento DEL POSTULANTE (Tabla Postulante y si no hay datos de la RENIEC)		
	$(function(){	
							
	    $('#NroDocumentoBus').numberbox('textbox').keypress(function(e){	
		    var NroDocumentoBus= $('#NroDocumentoBus').numberbox('getText');
			var IdDocIdentidadBus= $('#IdDocIdentidadBus').combobox('getValue');
			
			if (e.which == 13) {	//&& (document.getElementById('chkburen').checked==true)
				
				if(IdDocIdentidadBus=='1' && (NroDocumentoBus.trim()).length!='8'){
					//$.messager.alert('Mensaje','El DNI debe tener 8 Digitos','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'El DNI debe tener 8 Digitos',
						icon:'warning',
						fn: function(){
							$('#NroDocumentoBus').next().find('input').focus();
						}
					});
					$('#NroDocumentoBus').next().find('input').focus();
					return 0;
				}	
							
				$.ajax({
				url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPostulantesPorDNI",					
				type: "GET",
				dataType: "JSON",
				data: {						
					valor: NroDocumentoBus						
				},							
					success: function (data) {				
					
						if(data.CodigoPostulante!=""){ //&& data.IdMensaje=="" Postulante ya Existe
							LlenarDatosPostulanteExiste(data);	
													
						}else{ //Postulante NO Existe (Datos RENIEC)
							$('#MensajeExiste').html('');
							$('#CodigoPostulante').textbox('setValue', data.CodigoPostulante);//'' 
							document.getElementById('IdPostulante').value=data.IdPostulante;//'' 
							
							$('#IdDocIdentidad').combobox('setValue', '1');
							$('#NroDocumento').numberbox('setValue', data.NroDocumento); 
							$('#ApellidoPaterno').textbox('setValue', data.ApellidoPaterno);
							$('#ApellidoMaterno').textbox('setValue', data.ApellidoMaterno);	
							$('#PrimerNombre').textbox('setValue', data.PrimerNombre);
							$('#SegundoNombre').textbox('setValue', data.SegundoNombre);
							$('#TercerNombre').textbox('setValue', data.TercerNombre);			
													
							$('#DireccionDomicilio').textbox('setValue', data.DireccionDomicilio);
							$('#IdTipoSexo').combobox('setValue', data.IdTipoSexo);	
							$('#FechaNacimiento').datebox('setValue', data.FechaNacimiento);							
							/////////////							
							$('#IdEstadoCivil').combobox('setValue', data.IdEstadoCivil);
							$('#IdTipoOcupacion').combobox('setValue', data.IdTipoOcupacion);							
							$('#Telefono').textbox('setValue', data.Telefono);	
							$('#IdGrupoSanguineo').combobox('setValue', data.IdGrupoSanguineo);	
							$('#Email').textbox('setValue', data.Email);												
							/*$('#IdTipoDonacion').combobox('setValue', 0);
							$('#IdTipoDonante').combobox('setValue', 0);
							$('#IdTipoRelacion').combobox('setValue', 0);
							$('#IdCondicionDonante').combobox('setValue', 0);*/
							
							//Datos Domicilio-Procedencia-Nacimiento
							$('#IdPaisDomicilio').combobox('setValue', data.IdPaisDomicilio);//166
							$('#IdPaisDomicilio').combobox('setText', data.NomPaisDomicilio);						
							$('#NomDepD').combobox('setValue', data.IdDepD);
							$('#NomDepD').combobox('setText', data.NomDepD);
							CambiaDepartamentoD();
							$('#NomProvD').combogrid('setValue', data.IdProvD);
							$('#NomProvD').combogrid('setText', data.NomProvD);
							CambiaProvinciaD();							
							$('#IdDistritoDomicilio').combogrid('setValue', data.IdDistritoDomicilio);
							$('#IdDistritoDomicilio').combogrid('setText', data.NomDistritoDomicilio);
							CambiaDistritoD();
							$('#IdCentroPobladoDomicilio').combobox('setValue', data.IdCentroPobladoDomicilio);
							$('#IdCentroPobladoDomicilio').combobox('setText', data.NomCentroPobladoDomicilio);							
							/////////////
							$('#NomDepP').combobox('setValue', '0');												
							$('#NomProvP').combogrid('setValue', '');							
							$('#IdDistritoProcedencia').combogrid('setValue', '');							
							$('#IdCentroPobladoProcedencia').combobox('setValue', '');							
							$('#NomDepN').combobox('setValue', '0');											
							$('#NomProvN').combogrid('setValue', '');							
							$('#IdDistritoNacimiento').combogrid('setValue', '');							
							$('#IdCentroPobladoNacimiento').combobox('setValue', '');						
						}													
					}				
				}).done(function (data) {
                    console.log(data);
                    //alert(data);						
					if(data=="") { //if(!data.success){	
						document.getElementById('IdPostulante').value="";			
						$.messager.alert('SIGESA','El DNI No existe en la Reniec' ,'info'); //NO HAY DATOS
						$('#MensajeExiste').html('');			
					}else if(data.IdMensaje=="PostulanteGris"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="PostulanteReactivo"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="Postulantedonoantes"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="donoantesAnnoVaron"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="donoantesAnnoMUJER"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="PostulanteNuncaPodraDonar"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="Recepcionpendiente"){							
						$.messager.alert('Mensaje','El Postulante tiene una Recepcion Pendiente','info');
					}//END else
				});	//fin done
				e.preventDefault();
				return false;
		    }
	    }); //FIN NroDocumentoBus	  
	  
    });//fin function


 
	//////2. FILTRAR COMBOGRID Apellidos y Nombres POSTULANTE 
	$(function(){	
		
		$('#ApellidosNombresBus').combogrid({ //Filtrar Postulante
			panelWidth:250,
			value:'',
			url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarPostulante',
			idField:'NroDocumento',
			textField:'NombresPostulante',
			mode:'remote',
			fitColumns:true,
			onSelect: function(rec){
			var url = buscarpostulanteNombresApellidos(); }, //esta funcion llama cuando seleccionas el Postulante						
			columns:[[							
				{field:'NombresPostulante',title:'Apellidos y Nombres',width:80}						
			]]
		});	//FIN 
									
	});		
		
	  //FUNCION QUE PERMITE LA BUSQUEDA POR APELLIDOS Y NOMBRES DEL POSTULANTE	
	    function buscarpostulanteNombresApellidos(){  //esta funcion llama cuando seleccionas el Postulante		  
			var ApellidosNombresBus= $('#ApellidosNombresBus').combogrid('getValue');						
				$.ajax({
				url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPostulantesPorDNI",					
				type: "GET",
				dataType: "JSON",
				data: {						
					valor: ApellidosNombresBus
					//,TipoBusqueda:'ApellidosNombres'						
				},							
					success: function (data) {
						//if(data.IdMensaje==""){
							LlenarDatosPostulanteExiste(data);
						//}
					}			
				}).done(function (data) {
                    console.log(data);
                    //alert(data);						
					/*if(data=="") { //if(!data.success){				
						$.messager.alert('SIGESA','Los Apellidos y Nombres no pertenecen a ningun Postulante' ,'info'); //NO HAY DATOS		
					}else if(data=="Postulantedonoantes"){							
						$.messager.alert('Mensaje','El Postulante donó antes de 2 meses','info');
					}else if(data.IdMensaje=="Recepcionpendiente"){							
						$.messager.alert('Mensaje','El Postulante tiene una Recepcion Pendiente','info');
					}//END else*/
					if(data=="") { //if(!data.success){	
						document.getElementById('IdPostulante').value="";			
						$.messager.alert('SIGESA','El DNI No existe en la Reniec' ,'info'); //NO HAY DATOS
						$('#MensajeExiste').html('');			
					}else if(data.IdMensaje=="PostulanteGris"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="PostulanteReactivo"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="Postulantedonoantes"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="donoantesAnnoVaron"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="donoantesAnnoMUJER"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="PostulanteNuncaPodraDonar"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						$('#MensajeExiste').html('');
					}else if(data.IdMensaje=="Recepcionpendiente"){							
						$.messager.alert('Mensaje','El Postulante tiene una Recepcion Pendiente','info');
					}//END else
				});	//fin done
				//e.preventDefault();
				//return false;	  	 
 		}//FIN function			
			
			
	//////3. FILTRAR COMBOGRID Apellidos y Nombres PACIENTE   		
	$(function(){	 //Filtrar Paciente	
		$('#ApellidosNombresBusRec').combogrid({
			panelWidth:250,
			value:'',
			url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarPaciente',
			idField:'NroHistoriaClinica', //NombresPaciente
			textField:'NombresPaciente',
			mode:'remote',
			fitColumns:true,
			onSelect: function(rec){
			var url = buscarPacienteNombresApellidos(); },	//esta funcion llama cuando seleccionas el paciente					
			columns:[[							
				{field:'NombresPaciente',title:'Apellidos y Nombres',width:80}						
			]]
		});	//FIN 						
	});		
		
	//FUNCION QUE PERMITE LA BUSQUEDA POR APELLIDOS Y NOMBRES DEL Paciente	
	function buscarPacienteNombresApellidos(){ //esta funcion llama cuando seleccionas el paciente	   	
		    var ApellidosNombresBusRec= $('#ApellidosNombresBusRec').combogrid('getValue');							
			$.ajax({
			url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BSD_BuscarPaciente",					
			type: "GET",
			dataType: "JSON",
			data: {						
				valor: ApellidosNombresBusRec,
				TipoBusqueda:'NroHistoriaClinica'	//ApellidosNombrespaciente						
			},							
				success: function (data) {
				//$('#IdPaciente').textbox('setText', data.IdPaciente); 
				document.getElementById('IdPaciente').value=data.IdPaciente;					
				$('#ApellidoPaternoRec').textbox('setValue', data.ApellidoPaterno);
				$('#ApellidoMaternoRec').textbox('setValue', data.ApellidoMaterno);	
				$('#PrimerNombreRec').textbox('setValue', data.PrimerNombre);
				$('#SegundoNombreRec').textbox('setValue', data.SegundoNombre);
				$('#TercerNombreRec').textbox('setValue', data.TercerNombre);	
				$('#NroHistoria').numberbox('setValue', data.NroHistoria);	
				$('#NroDocumentoRec').numberbox('setValue', data.NroDocumento);
				
				$('#IdTipoSexoRec').combobox('setValue', data.IdTipoSexo);	
				$('#FechaNacimientoRec').datebox('setValue', data.FechaNacimiento);	
				$('#IdGrupoSanguineoRec').combobox('setValue', data.IdGrupoSanguineo);	
				
				/*$('#IdServicioHospital').combobox('setValue', data.IdServicio);	
				$('#NroCama').numberbox('setValue', data.numerocama);	
				$('#Medico').textbox('setValue', data.Medico);	
				$('#CodigoCIE10Diagnostico').textbox('setValue', data.diagnostico);	*/							
				}			
			}).done(function (data) {
				console.log(data);
				//alert(data);						
				if(data=="") { //if(!data.success){				
					$.messager.alert('SIGESA','Los Apellidos y Nombres no pertenecen a ningun Paciente' ,'info'); //NO HAY DATOS		
				}
				ObtenergsPaciente(data.IdPaciente);
			});	//fin done  	 
 	}//FIN function
 
 

///////////4. BUSQUEDA RECEPTOR POR Nro Documento,APELLIDOS y NOMBRES, Y Nro Historia
$(function(){
	
	 //INICIA BUSQUEDA RECEPTOR POR Nro Documento	
	$('#NroDocumentoBusRec').numberbox('textbox').keypress(function(e){	
		var NroDocumentoBusRec= $('#NroDocumentoBusRec').numberbox('getText');				
		if (e.which == 13) {					
				$.ajax({
				url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BSD_BuscarPaciente",					
				type: "GET",
				dataType: "JSON",
				data: {						
					valor: NroDocumentoBusRec,
					TipoBusqueda:'NroDocumento'					
				},							
					success: function (data) {	
					//$('#IdPaciente').textbox('setText', data.IdPaciente); 
					document.getElementById('IdPaciente').value=data.IdPaciente;					
					$('#ApellidoPaternoRec').textbox('setValue', data.ApellidoPaterno);
					$('#ApellidoMaternoRec').textbox('setValue', data.ApellidoMaterno);	
					$('#PrimerNombreRec').textbox('setValue', data.PrimerNombre);
					$('#SegundoNombreRec').textbox('setValue', data.SegundoNombre);
					$('#TercerNombreRec').textbox('setValue', data.TercerNombre);	
					$('#NroHistoria').numberbox('setValue', data.NroHistoria);	
					$('#NroDocumentoRec').numberbox('setValue', data.NroDocumento);
					
					$('#IdTipoSexoRec').combobox('setValue', data.IdTipoSexo);	
					$('#FechaNacimientoRec').datebox('setValue', data.FechaNacimiento);
					$('#IdGrupoSanguineoRec').combobox('setValue', data.IdGrupoSanguineo);						  
					
					/*$('#IdServicioHospital').combobox('setValue', data.IdServicio);	
					$('#NroCama').numberbox('setValue', data.numerocama);	
					$('#Medico').textbox('setValue', data.Medico);	
					$('#CodigoCIE10Diagnostico').textbox('setValue', data.diagnostico);	*/				
					}				
				}).done(function (data) {
                    console.log(data);
					if(data=="") { //if(!data.success){	
						document.getElementById('IdPaciente').value="";			
						$.messager.alert('SIGESA','El DNI No existe en la tabla Pacientes' ,'info'); //NO HAY DATOS									
					}
					ObtenergsPaciente(data.IdPaciente);
				});	
				
				/*e.preventDefault();
				return false;*/
		}
	}); 
	  
	  //INICIA BUSQUEDA RECEPTOR POR Nro Historia
	$('#NroHistoriaClinicaBusRec').numberbox('textbox').keypress(function(e){		
		var NroHistoriaClinicaBus= $('#NroHistoriaClinicaBusRec').numberbox('getText');			
		if (e.which == 13) {					
				$.ajax({
				url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BSD_BuscarPaciente",					
				type: "GET",
				dataType: "JSON",
				data: {						
					valor: NroHistoriaClinicaBus,
					TipoBusqueda:'NroHistoriaClinica'					
				},							
					success: function (data) {	
					//$('#IdPaciente').textbox('setValue', data.IdPaciente); 
					document.getElementById('IdPaciente').value=data.IdPaciente;					
					$('#ApellidoPaternoRec').textbox('setValue', data.ApellidoPaterno);
					$('#ApellidoMaternoRec').textbox('setValue', data.ApellidoMaterno);	
					$('#PrimerNombreRec').textbox('setValue', data.PrimerNombre);
					$('#SegundoNombreRec').textbox('setValue', data.SegundoNombre);
					$('#TercerNombreRec').textbox('setValue', data.TercerNombre);	
					$('#NroHistoria').numberbox('setValue', data.NroHistoria);	
					$('#NroDocumentoRec').numberbox('setValue', data.NroDocumento);
					
					$('#IdTipoSexoRec').combobox('setValue', data.IdTipoSexo);	
					$('#FechaNacimientoRec').datebox('setValue', data.FechaNacimiento);						
					$('#IdGrupoSanguineoRec').combobox('setValue', data.IdGrupoSanguineo);						
					
					/*$('#IdServicioHospital').combobox('setValue', data.IdServicio);	
					$('#NroCama').numberbox('setValue', data.numerocama);	
					$('#Medico').textbox('setValue', data.Medico);	
					$('#CodigoCIE10Diagnostico').textbox('setValue', data.diagnostico);	*/			
					}				
				}).done(function (data) {
                    console.log(data);
					if(data=="") { //if(!data.success){	
						document.getElementById('IdPaciente').value="";			
						$.messager.alert('SIGESA','La Historia Clinica No existe en la tabla Pacientes' ,'info'); //NO HAY DATOS									
					}
					ObtenergsPaciente(data.IdPaciente);
				});
				/*e.preventDefault();
				return false;*/
		}
	});//
	 //INICIA BUSQUEDA DATOS UBIGEO
	$('#UbigeoN').numberbox('textbox').keypress(function(e){		
		var UbigeoN= $('#UbigeoN').textbox('getText');	
		//var NroHistoriaClinicaBus= $('#NroHistoriaClinicaBusRec').numberbox('getText');			
		if (e.which == 13) {					
				$.ajax({
				url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ObtenerDatosReniec",					
				type: "GET",
				dataType: "JSON",
				data: {						
					IdReniec: UbigeoN				
				},							
					success: function (data) {
						//Datos Nacimiento
						$('#IdPaisNacimiento').combobox('setValue', '166');//166 PERU
						CambiaPaisN();				
						$('#NomDepN').combobox('setValue', data.IdDepartamento);							
						CambiaDepartamentoN();
						$('#NomProvN').combogrid('setValue', data.IdProvincia);							
						CambiaProvinciaN();							
						$('#IdDistritoNacimiento').combogrid('setValue', data.IdDistrito);							
						CambiaDistritoN();
						//$('#IdCentroPobladoDomicilio').combobox('setValue', data.IdCentroPobladoDomicilio);											
					}				
				}).done(function (data) {
                    console.log(data);
					if(data=="") { 								
						$.messager.alert('SIGESA','El Ubigeo No existe' ,'info'); //NO HAY DATOS									
					}					
				});
				/*e.preventDefault();
				return false;*/
		}
	}); //	
		

});	
	

//BUSQUEDA POR Nro Documento DEL POSTULANTE INGRESO MANUAL	
	function validarDNIpostulante(){				
	    
		    var NroDocumentoBus= $('#NroDocumento').numberbox('getText');
			var IdDocIdentidadBus= $('#IdDocIdentidad').combobox('getValue');	
				
				if(IdDocIdentidadBus=='1' && (NroDocumentoBus.trim()).length!='8'){
					//$.messager.alert('Mensaje','El DNI debe tener 8 Digitos','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'El DNI debe tener 8 Digitos',
						icon:'warning',
						fn: function(){
							$('#NroDocumento').next().find('input').focus();
						}
					});
					$('#NroDocumento').next().find('input').focus();
					document.getElementById('errorPostulante').value='El DNI debe tener 8 Digitos';  
					return 0;
				}else{
					document.getElementById('errorPostulante').value='';
				}	
							
				$.ajax({
				url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPostulantesPorDNI",					
				type: "GET",
				dataType: "JSON",
				data: {						
					valor: NroDocumentoBus						
				},							
					success: function (data) {					
						/*if(data.CodigoPostulante!=""){ //&& data.IdMensaje=="" Postulante ya Existe
							//LlenarDatosPostulanteExiste(data);	
													
						}else{ //Postulante NO Existe (Datos RENIEC)
												
						}*/													
					}				
				}).done(function (data) {
                    console.log(data);
                    //alert(data);						
					if(data=="") { //if(!data.success){										
						//$.messager.alert('SIGESA','El DNI No existe en la Reniec' ,'info'); //NO HAY DATOS						
						document.getElementById('errorPostulante').value='';
								
					}else if(data.IdMensaje=="PostulanteGris"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						document.getElementById('errorPostulante').value=data.Mensaje;
						
					}else if(data.IdMensaje=="PostulanteReactivo"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						document.getElementById('errorPostulante').value=data.Mensaje;
						
					}else if(data.IdMensaje=="Postulantedonoantes"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						document.getElementById('errorPostulante').value=data.Mensaje;
						
					}else if(data.IdMensaje=="donoantesAnnoVaron"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						document.getElementById('errorPostulante').value=data.Mensaje;
						
					}else if(data.IdMensaje=="donoantesAnnoMUJER"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						document.getElementById('errorPostulante').value=data.Mensaje;
						
					}else if(data.IdMensaje=="PostulanteNuncaPodraDonar"){							
						$.messager.alert('Mensaje',data.Mensaje,'info');
						document.getElementById('errorPostulante').value=data.Mensaje;
						
					}else if(data.IdMensaje=="Recepcionpendiente"){							
						$.messager.alert('Mensaje','El Postulante tiene una Recepcion Pendiente','info');
						document.getElementById('errorPostulante').value='El Postulante tiene una Recepcion Pendiente';
						
					}else{//END else
						document.getElementById('errorPostulante').value='';
					}
				});	//fin done
				
				return false;  
	  
    }//fin function 
	






