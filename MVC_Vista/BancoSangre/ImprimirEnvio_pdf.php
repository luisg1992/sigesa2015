<?php 
//ini_set('error_reporting',0);//para xamp
ini_set('memory_limit', '-1'); 
date_default_timezone_set('America/Bogota');
//require('../../MVC_Modelo/SistemaM.php');
//include('../../MVC_Modelo/BancoSangreM.php');
require('../../MVC_Complemento/fpdf/fpdfLV.php');
//require('../../MVC_Complemento/librerias/Funciones.php');

#OBTENIENDO VALORES


class PDF extends FPDF
{	
	//INICIO funciones para auto imprimir
	protected $javascript;
	protected $n_js;

	function IncludeJS($script, $isUTF8=false) {
		if(!$isUTF8)
			$script=utf8_encode($script);
		$this->javascript=$script;
	}

	function _putjavascript() {
		$this->_newobj();
		$this->n_js=$this->n;
		$this->_put('<<');
		$this->_put('/Names [(EmbeddedJS) '.($this->n+1).' 0 R]');
		$this->_put('>>');
		$this->_put('endobj');
		$this->_newobj();
		$this->_put('<<');
		$this->_put('/S /JavaScript');
		$this->_put('/JS '.$this->_textstring($this->javascript));
		$this->_put('>>');
		$this->_put('endobj');
	}

	function _putresources() {
		parent::_putresources();
		if (!empty($this->javascript)) {
			$this->_putjavascript();
		}
	}

	function _putcatalog() {
		parent::_putcatalog();
		if (!empty($this->javascript)) {
			$this->_put('/Names <</JavaScript '.($this->n_js).' 0 R>>');
		}
	}
	
	//INICIO funciones para auto imprimir
	function AutoPrint($printer='')
	{
		// Open the print dialog
		if($printer)
		{
			$printer = str_replace('\\', '\\\\', $printer);
			$script = "var pp = getPrintParams();";
			$script .= "pp.interactive = pp.constants.interactionLevel.full;";
			$script .= "pp.printerName = '$printer'";
			$script .= "print(pp);";
		}
		else
			$script = 'print(true);';
		$this->IncludeJS($script);
	}	
	//FIN funciones para auto imprimir
	
//INICIO FUNCIONES TABLA	
var $widths;
var $aligns;

function SetWidths($w)
{
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a)
{
	//Set the array of column alignments
	$this->aligns=$a;
}

function NbLines($w,$txt)
{
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax)
		{
			if($sep==-1)
			{
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}

function CheckPageBreak($h)
{
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function Row($data,$Align)
{
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++)
	{
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] :$Align; //$Align='C','L'
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		
		$this->Rect($x,$y,$w,$h);

		$this->MultiCell($w,5,$data[$i],1,$a,'true');
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}
//FIN FUNCIONES TABLA
	
		
	function Header(){	
		
		#OBTENIENDO DATOS
		$IdEstablecimientoEgreso=isset($_REQUEST['IdEstablecimientoEgreso']) ? $_REQUEST['IdEstablecimientoEgreso'] : '0';
  		$FechaSalidaB=isset($_REQUEST['FechaSalida']) ? $_REQUEST['FechaSalida'] :  date('d/m/Y');
		$FechaSalida=gfecha($FechaSalidaB);
		$respuesta=BuscarSNCSEnviado_M($IdEstablecimientoEgreso,$FechaSalida);
		
		//ENCABEZADO
		#LOGOS	
		$this->Cell(60);
		$this->Image('../../MVC_Complemento/img/oficio-logo.jpg',20,5,20,20);
		$this->Cell(42);			
		$this->Image('../../MVC_Complemento/img/oficio-encabezado.jpg',70,5,115,11);
		$this->Ln(10);		
		#PARTE 1
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(50);// Move to 8 cm to the right
		//$this->MultiCell(91,5,'EG05-FR11: CONSTANCIA DE TRANSFERENCIA DE SANGRE Y/O HEMOCOMPONENTES SANGUINEOS','C');	
		$this->MultiCell(100, 5,'EG05-FR11: CONSTANCIA DE TRANSFERENCIA DE SANGRE Y/O HEMOCOMPONENTES SANGUINEOS' , 0 , 'C' , false);	
		#PARTE 2
		$this->Ln(3);
		$this->SetFont('Helvetica','BU');	
		$this->Cell(10);$this->Cell(80,10,utf8_decode('OFICIO N°    DG-DPCAP-SHBS/HNDAC-2018'),0,0,'L');		
		#PARTE 3
		$this->Ln(10);
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(10);$this->Cell(8,10,'A: ',0,0,'L');
		$this->SetFont('Helvetica','',9);	
		$this->Cell(10);$this->Cell(80,10,utf8_decode($respuesta[0]["EstablecimientoEgreso"]),0,0,'L');			
		$this->Ln(5);
		$this->SetFont('Helvetica','B',9);
		$this->Cell(10);$this->Cell(8,10,'De: ',0,0,'L');	
		$this->SetFont('Helvetica','',9);
		$this->Cell(10);$this->Cell(80,10,'DIRECTORA DEL HOSPITAL NACIONAL DANIEL  ALCIDES  CARRION-CALLAO',0,0,'L');				
		$this->Ln(5);
		$this->SetFont('Helvetica','B',9);
		$this->Cell(10);$this->Cell(8,10,'Atencion: ',0,0,'L');	
		$this->SetFont('Helvetica','',9);
		$this->Cell(10);$this->Cell(80,10,'RESPONSABLE   DEL CENTRO DE HEMOTERAPIA',0,0,'L');				
		$this->Ln(5);
		
		$arrayfecha=explode('/',$FechaSalidaB);//d/m/Y
		$dia=str_pad($arrayfecha[0], 2, "0", STR_PAD_LEFT); 
		$FechaLetras=$dia.' de '.nombremes($arrayfecha[1]).' del '.$arrayfecha[2];
		$this->SetFont('Helvetica','B',9);
		$this->Cell(10);$this->Cell(8,10,'Fecha: ',0,0,'L');
		$this->SetFont('Helvetica','',9);
		$this->Cell(10);$this->Cell(80,10,'Bellavista, '.$FechaLetras,0,0,'L');	
		$this->Ln(5);
		$this->SetFont('Helvetica','B',9);
		$this->Cell(10);$this->Cell(8,10,'Ref.: ',0,0,'L');  
		                                    
		#CONTENIDO
		$this->Ln(10);
		$this->SetFont('Helvetica','',10);		
		$this->MultiCell(185,5,utf8_decode('Por medio del presente hacemos constar que como parte del apoyo interinstitucional del Programa Nacional de Hemoterapia y Bancos de Sangre del Ministerio de Salud, se transfieren a su institución las unidades que se señalan a continuación, las mismas que cuentan con las pruebas serológicas NO REACTIVAS que se indican:'),'C'); 			
		$this->Ln(5);		
	}

	function Footer()
	{
		/*$ip = $_SERVER['REMOTE_ADDR'];$hostname = gethostbyaddr($ip);
		$usuario=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);			
		$this->SetFont('Helvetica','',6);
		
		$this->Ln(20);
		$this->Cell(60,5,'TERMINAL: ' .$hostname ,0,0,'L');	$this->Ln(3);	
		$this->Cell(60,5,'USUARIO: ' .utf8_decode(strtoupper($usuario[0]['Usuario'])),0,0,'L');	$this->Ln(3);	
		$this->Cell(60,5,utf8_decode('FECHA Y HORA DE IMPRESIÓN: ') .date('d/m/Y H:i:s'),0,0,'L');$this->Ln(3);			
		$this->Cell(60,5,"HNDAC / OESY / DESARROLLO DE SISTEMAS",0,0,'L'); $this->Ln(5);*/	
		
		//PARTE 1	
		$this->Ln(10);
		$this->Ln(2);	
		$this->Cell(120); $this->Cell(60,0,"",'LRB',0,0,'C');	
		$this->Ln(2);		
		$this->Cell(120); $this->Cell(60,0,'Nombre, firma y sello de Director(a)',0,0,'C'); 
		$this->Ln(4);		
		$this->Cell(120); $this->Cell(60,0,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C'); 
		//PARTE 2	
		$this->Ln(5);	
		$this->SetWidths(array(110, 80));
		$this->SetFont('Arial','B',9);
		$this->SetFillColor(255);
		$this->SetTextColor(0);
		
		for($i=0;$i<1;$i++)
			{
				$this->Row(array('RECIBE:','DNI:'),'L');
				$this->Row(array('CARGO:','HORA DE SALIDA:'),'L');					
			}
			 				
	}

}

	$pdf = new PDF("P","mm","A3");//P or Portrait Y L or Landscape
	$pdf->SetTitle('Envio de HEMOCOMPONENTES');
	$pdf->SetAuthor('Mahali Huamán');
	$pdf->AliasNbPages();
	$pdf->AddPage();	
	//$pdf->SetFont('Arial','',12);
	
	$pdf->SetWidths(array(23, 17, 30, 20, 20, 20, 20, 20, 20));
	$pdf->SetFont('Arial','B',9);
	//$pdf->SetFillColor(150,255,255);
	$pdf->SetFillColor(150,255,255);
    $pdf->SetTextColor(0);
		
		for($i=0;$i<1;$i++)
			{
				//$pdf->Row(array('NRO LOTE',	'GRUPO SANGUINEO', 'HEMOCOMPONENTE', 'SELLO N. CALIDAD', 'VOLUMEN HEMOC.', 'TUBULADURA', 'FENOTIPO SANG.','FECHA EXTRAC.','FECHA VENCIM.'),'C');	
				$pdf->Row(array(utf8_decode('N° Lote'),	'GrupoS.', 'Hemocomp.','SNCS', 'Volumen', 'Tubuladura', 'Fenotipo','Fec.extrac.','Fec.venc.'),'C');			
			}

	#OBTENIENDO DATOS
	$IdEstablecimientoEgreso=isset($_REQUEST['IdEstablecimientoEgreso']) ? $_REQUEST['IdEstablecimientoEgreso'] : '0';
	$FechaSalidaB=isset($_REQUEST['FechaSalida']) ? $_REQUEST['FechaSalida'] :  date('d/m/Y');
	$FechaSalida=gfecha($FechaSalidaB);
	$BuscarSNCSEnviado=BuscarSNCSEnviado_M($IdEstablecimientoEgreso,$FechaSalida);
	
	for ($i=0; $i<count($BuscarSNCSEnviado); $i++)
		{
			//Obtener Fenotipo
			$TipoHem=$BuscarSNCSEnviado[$i]['TipoHem'];
			$SNCS=$BuscarSNCSEnviado[$i]['SNCS'];
			if($TipoHem=='CRIO'){
				$TipoHem='PFC';
			}else{
				$TipoHem=$TipoHem;
			}
			$TipoHemSNCS=$TipoHem.'-'.$SNCS;//PG-0315152
			$ObtenerDatosVerificacionAptos=ObtenerDatosVerificacionAptosXSNCS_M($TipoHemSNCS);
			$Fenotipo=$ObtenerDatosVerificacionAptos[0]['Fenotipo']; //C+E+c+e+K-
			//$fila = $historial->fetch_array();
			$pdf->SetFont('Arial','',8);
			
			if($i%2 == 1)
			{
				//$pdf->SetFillColor(153,255,153);
				$pdf->SetFillColor(255);
    			$pdf->SetTextColor(0);
				$pdf->Row(array($BuscarSNCSEnviado[$i]['NroDonacion'], $BuscarSNCSEnviado[$i]['GrupoSanguineo'], $BuscarSNCSEnviado[$i]['TipoHem'], $BuscarSNCSEnviado[$i]['SNCS'], $BuscarSNCSEnviado[$i]['VolumenRestante'], $BuscarSNCSEnviado[$i]['NroTabuladora'], $Fenotipo, vfecha(substr($BuscarSNCSEnviado[$i]['FechaExtraccion'],0,10)), vfecha(substr($BuscarSNCSEnviado[$i]['FechaVencimiento'],0,10)) ),'C');
			}
			else
			{
				//$pdf->SetFillColor(102,204,51);
				$pdf->SetFillColor(255);
    			$pdf->SetTextColor(0);
				$pdf->Row(array($BuscarSNCSEnviado[$i]['NroDonacion'], $BuscarSNCSEnviado[$i]['GrupoSanguineo'], $BuscarSNCSEnviado[$i]['TipoHem'], $BuscarSNCSEnviado[$i]['SNCS'], $BuscarSNCSEnviado[$i]['VolumenRestante'], $BuscarSNCSEnviado[$i]['NroTabuladora'], $Fenotipo, vfecha(substr($BuscarSNCSEnviado[$i]['FechaExtraccion'],0,10)), vfecha(substr($BuscarSNCSEnviado[$i]['FechaVencimiento'],0,10)) ),'C');
			}
		}
//FIN TABLA

		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','B',9);		
		$pdf->Cell(10);$pdf->Cell(80,10,utf8_decode('Pruebas serológicas realizadas a las unidades:'),0,0,'L');				
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',9);	
		$pdf->Cell(10);$pdf->Cell(60,10,'Anticuerpos Anticore Total (Anti-HBc):',0,0,'L');
		$pdf->SetFont('Helvetica','B',9);
		$pdf->Cell(10);$pdf->Cell(40,10,'NO REACTIVO',0,0,'L');				
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',9);
		$pdf->Cell(10);$pdf->Cell(60,10,utf8_decode('Antígeno Australiano (AgHBs):'),0,0,'L');
		$pdf->SetFont('Helvetica','B',9);
		$pdf->Cell(10);$pdf->Cell(40,10,'NO REACTIVO',0,0,'L');					
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',9);
		$pdf->Cell(10);$pdf->Cell(60,10,utf8_decode('Anticuerpos Anti HCV:'),0,0,'L');
		$pdf->SetFont('Helvetica','B',9);
		$pdf->Cell(10);$pdf->Cell(40,10,'NO REACTIVO',0,0,'L');					
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',9);
		$pdf->Cell(10);$pdf->Cell(60,10,utf8_decode('Anticuerpos Anti HIV I-II:'),0,0,'L');
		$pdf->SetFont('Helvetica','B',9);
		$pdf->Cell(10);$pdf->Cell(40,10,'NO REACTIVO',0,0,'L');					
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',9);
		$pdf->Cell(10);$pdf->Cell(60,10,utf8_decode('Prueba de Chagas:'),0,0,'L');
		$pdf->SetFont('Helvetica','B',9);
		$pdf->Cell(10);$pdf->Cell(40,10,'NO REACTIVO',0,0,'L');					
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',9);
		$pdf->Cell(10);$pdf->Cell(60,10,utf8_decode('Anticuerpos Anti HTLV  I-II:'),0,0,'L');
		$pdf->SetFont('Helvetica','B',9);
		$pdf->Cell(10);$pdf->Cell(40,10,'NO REACTIVO',0,0,'L');					
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',9);
		$pdf->Cell(10);$pdf->Cell(60,10,utf8_decode('Serología de Sífilis:'),0,0,'L');
		$pdf->SetFont('Helvetica','B',9);
		$pdf->Cell(10);$pdf->Cell(40,10,'NO REACTIVO',0,0,'L');					
		$pdf->Ln(10);
		$pdf->SetFont('Helvetica','',9);
		$pdf->Cell(10);$pdf->Cell(80,10,utf8_decode('Atentamente,'),0,0,'L');

				
	$pdf->AutoPrint();//para auto imprimir
	$pdf->Output();
?>

