<?php 
//ini_set('error_reporting',0);//para xamp
ini_set('memory_limit', '1024M'); 
date_default_timezone_set('America/Bogota');

require_once('../../MVC_Complemento/fpdf/fpdfLV.php');

require_once('../../MVC_Complemento/setasign/Fpdi/autoload.php');
use \setasign\Fpdi\Fpdi;

require_once('../../MVC_Complemento/setasign/FpdiProtection/autoload.php');
use \setasign\FpdiProtection\FpdiProtection;

	class PDF_Rotate extends FPDI {
	
		/*var $angle = 0;
	
		function Rotate($angle, $x = -1, $y = -1) {
			if ($x == -1)
				$x = $this->x;
			if ($y == -1)
				$y = $this->y;
			if ($this->angle != 0)
				$this->_out('Q');
			$this->angle = $angle;
			if ($angle != 0) {
				$angle*=M_PI / 180;
				$c = cos($angle);
				$s = sin($angle);
				$cx = $x * $this->k;
				$cy = ($this->h - $y) * $this->k;
				$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
			}
		}
	
		function _endpage() {
			if ($this->angle != 0) {
				$this->angle = 0;
				$this->_out('Q');
			}
			parent::_endpage();
		}*/
	
	}
	
	

	$fullPathToFile = "../../MVC_Complemento/setasign/SOLICITUD-TRANSFUSIONAL-2.pdf";
	//$fullPathToFile2 = "../../MVC_Complemento/setasign/SOLICITUD-TRANSFUSIONAL-2.pdf";

	class PDF extends PDF_Rotate {

		var $_tplIdx;
	
		function Header() {
			global $fullPathToFile;	//global $fullPathToFile2;	
			
			//DATOS
			$IdSolicitudSangre=$_REQUEST["IdSolicitudSangre"];
			$DatosSolicitudCabecera=ImprimirSolicitud($IdSolicitudSangre,0);
			
			$Paciente=SIGESA_BSD_Buscarpaciente_M($DatosSolicitudCabecera[0]["IdPaciente"],'IdPaciente');	
			//$NombresPaciente=$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"];
			$IdTipoSexo=$Paciente[0]["IdTipoSexo"];
			$NroHistoriaClinica=$Paciente[0]["NroHistoriaClinica"];			
						
			$FechaNacimientoX = time() - strtotime($Paciente[0]['FechaNacimiento']);
			$edad = floor((($FechaNacimientoX / 3600) / 24) / 360);	
			
			$GrupoSanguineo=$DatosSolicitudCabecera[0]["GrupoSanguineo"];
			$NroSolicitud=$DatosSolicitudCabecera[0]["NroSolicitud"];
			$EspecifidadAnticuerpo=$DatosSolicitudCabecera[0]["EspecifidadAnticuerpo"];
			$ObsFenotipo=$DatosSolicitudCabecera[0]["ObsFenotipo"];
			
			$IdPaciente=$DatosSolicitudCabecera[0]["IdPaciente"];
			$BuscarFenotipoPaciente1=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'cmayor');
			$FenotipoPacientecmayor=$BuscarFenotipoPaciente1[0]["ValorFenotipo"];//C+
			
			$BuscarFenotipoPaciente2=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'emayor');
			$FenotipoPacienteemayor=$BuscarFenotipoPaciente2[0]["ValorFenotipo"];//E+
			
			$BuscarFenotipoPaciente3=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'cmenor');
			$FenotipoPacientecmenor=$BuscarFenotipoPaciente3[0]["ValorFenotipo"];//c-
			
			$BuscarFenotipoPaciente4=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'emenor');
			$FenotipoPacienteemenor=$BuscarFenotipoPaciente4[0]["ValorFenotipo"];//e-
			
			$BuscarFenotipoPaciente5=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'Cw');
			$FenotipoPacienteCw=$BuscarFenotipoPaciente5[0]["ValorFenotipo"];//Cw-
			
			$BuscarFenotipoPaciente6=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'kmayor');
			$FenotipoPacientekmayor=$BuscarFenotipoPaciente6[0]["ValorFenotipo"];//K-
			
			$BuscarFenotipoPaciente7=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'kmenor');
			$FenotipoPacientekmenor=$BuscarFenotipoPaciente7[0]["ValorFenotipo"];//k-	
			
			$BuscarFenotipoPaciente8=SIGESA_BSD_BuscarFenotipoM('R', 0, $IdPaciente, 'control');
			$FenotipoPacientecontrol=$BuscarFenotipoPaciente8[0]["ValorFenotipo"];//k-		
				
			//FIN DATOS				
			
			//LINEA 1
			$this->SetFont('Helvetica','',12);
			$this->SetTextColor(0, 0, 0);	
			
			$this->Ln(72);//espacios abajo
			$this->Cell(235);//espacios a la derecha
			
			$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
			$DatosDonante=ImprimirSolicitudDatosDonante($IdSolicitudSangre,$IdDetSolicitudSangre);	
			$this->Cell(50,57,$NroSolicitud.'-'.$DatosDonante[0]["TipoHem"],0,0,'C');			
			
			//LINEA 2
			$this->SetFont('Helvetica','',10);
			$this->SetTextColor(0, 0, 0);		

			$this->Ln(12);//espacios abajo
			$this->Cell(74);//espacios a la derecha	
			
			if($Paciente[0]["NroDocumento"]!=''){
				$NroDocumentoPaciente=' DNI:'.$Paciente[0]["NroDocumento"];	
				//$NroDocumentoPaciente='<font color="#FF0000">FALTA</font>';
			}else{
				$NroDocumentoPaciente='';
			}			
				
			$this->Cell(50,57,$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"].' '.$Paciente[0]["SegundoNombre"].' HCL:'.$NroHistoriaClinica.' '.$NroDocumentoPaciente,0,0,'L');//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])	
			
			$this->Ln(35);//espacios abajo
			$this->Cell(12);					
			
			$this->SetFont('Helvetica','',16);	
			$this->Cell(20,57,$GrupoSanguineo,0,0,'L');
			
			$this->SetFont('Helvetica','',10);		
			$this->Cell(10,57,$FenotipoPacientecmayor,0,0,'L');
			$this->Cell(10,57,$FenotipoPacientecmenor,0,0,'L');
			$this->Cell(10,57,$FenotipoPacienteemayor,0,0,'L');
			$this->Cell(10,57,$FenotipoPacienteemenor,0,0,'L');
			$this->Cell(10,57,$FenotipoPacientekmayor,0,0,'L');			
			$this->Cell(15,57,$FenotipoPacientecontrol,0,0,'L');		
			
			#SETEANDO PUNTERO
			$this->SetXY(-60,-150);
			$this->MultiCell(45,4,utf8_decode($EspecifidadAnticuerpo),0);
			
			#SETEANDO PUNTERO
			$this->SetXY(-0.1,-150);			
			$this->MultiCell(45,4,utf8_decode($ObsFenotipo),0);
			
			
			//Registro de unidades preparadas:
			
			$IdDetSolicitudSangre=$_REQUEST["IdDetSolicitudSangre"];
			$DatosDonante=ImprimirSolicitudDatosDonante($IdSolicitudSangre,$IdDetSolicitudSangre);
			//$this->Ln(33);//espacios abajo
			
			#SETEANDO PUNTERO
			$this->SetXY(-200,-113);	//(WIDTH,HEIGHT)	
			$this->SetFont('Helvetica','',9);	
			
				if($DatosDonante!=NULL){									
					$i=0;									
					foreach ($DatosDonante as $item){
						$i=$i+1;				
						//$FechaRecepcion=isset($item['FechaRecepcion']) ? (vfecha(substr($item['FechaRecepcion'],0,10))) : '';	
						//$FechaDespacho=isset($item['FechaDespacho']) ? (vfecha(substr($item['FechaDespacho'],0,10))) : '';	
						//$DatosPostulante=$item['ApellidosPostulante'].' '.$item['NombresPostulante'];	
						
						$IdPostulante=$item['IdPostulante'];
						$BuscarFenotipoPostulante1=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'cmayor');
						$FenotipoPostulantecmayor=$BuscarFenotipoPostulante1[0]["ValorFenotipo"];//C+
						
						$BuscarFenotipoPostulante1=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'cmayor');
						$FenotipoPostulantecmayor=$BuscarFenotipoPostulante1[0]["ValorFenotipo"];//C+
						
						$BuscarFenotipoPostulante2=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'emayor');
						$FenotipoPostulanteemayor=$BuscarFenotipoPostulante2[0]["ValorFenotipo"];//E+
						
						$BuscarFenotipoPostulante3=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'cmenor');
						$FenotipoPostulantecmenor=$BuscarFenotipoPostulante3[0]["ValorFenotipo"];//c-
						
						$BuscarFenotipoPostulante4=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'emenor');
						$FenotipoPostulanteemenor=$BuscarFenotipoPostulante4[0]["ValorFenotipo"];//e-
						
						$BuscarFenotipoPostulante5=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'Cw');
						$FenotipoPostulanteCw=$BuscarFenotipoPostulante5[0]["ValorFenotipo"];//Cw-
						
						$BuscarFenotipoPostulante6=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'kmayor');
						$FenotipoPostulantekmayor=$BuscarFenotipoPostulante6[0]["ValorFenotipo"];//K-
						
						$BuscarFenotipoPostulante7=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'kmenor');
						$FenotipoPostulantekmenor=$BuscarFenotipoPostulante7[0]["ValorFenotipo"];//k-
						
						$BuscarFenotipoPostulante8=SIGESA_BSD_BuscarFenotipoM('D', $IdPostulante, 0, 'control');
						$FenotipoPostulantecontrol=$BuscarFenotipoPostulante8[0]["ValorFenotipo"];//control					
										
						$this->Cell(13);		
						$this->Cell(28,20,$item['NroDonacion'],0,0,'L');
						$this->Cell(17,20,$item['GrupoSanguineo'],0,0,'L');						
						$this->Cell(10,20,$FenotipoPostulantecmayor,0,0,'L');
						$this->Cell(10,20,$FenotipoPostulantecmenor,0,0,'L');
						$this->Cell(10,20,$FenotipoPostulanteemayor,0,0,'L');
						$this->Cell(10,20,$FenotipoPostulanteemenor,0,0,'L');
						$this->Cell(9,20,$FenotipoPostulantekmayor,0,0,'L');						
						$this->Cell(14,20,$FenotipoPostulantecontrol,0,0,'L');
						
						if($item['TipoHem']=='PG'){							
							$this->Cell(10,20,round($item['VolumenRestante'],2),0,0,'L');
							$this->Cell(37);
							
						}else if($item['TipoHem']=='PFC'){
							$this->Cell(10);
							$this->Cell(10,20,round($item['VolumenRestante'],2),0,0,'L');
							$this->Cell(27);
							
						}else if($item['TipoHem']=='PQ'){
							$this->Cell(22);
							$this->Cell(10,20,round($item['VolumenRestante'],2),0,0,'L');
							$this->Cell(15);
							
						}else if($item['TipoHem']=='CRIO'){
							$this->Cell(32);
							$this->Cell(15,20,round($item['VolumenRestante'],2),0,0,'L');							
						}
						
						$this->Cell(27,20,$item['NroTabuladora'],0,0,'L');
						$this->Cell(32,20,$item['TipoHem'].'-'.$item['SNCS'],0,0,'L');
						$this->Cell(10,20,"X",0,0,'L');							
						
						$this->Ln(4.7);	
					}//fin foreach
				}// fin if
				
				$this->SetFont('Helvetica','',9);
				
				$FechaReservaX=$DatosDonante[0]["FechaReserva"];
				$FechaReserva=empty($FechaReservaX) ? '' : vfecha(substr($FechaReservaX,0,10));
				$HoraReserva=substr($FechaReservaX,11,5);				
				
				$cmpReserva=$DatosDonante[0]["cmpReserva"];
				$UsuReserva=$DatosDonante[0]["UsuReserva"];
				$ListarUsuario2=ListarUsuarioxIdempleado_M($UsuReserva);
				$NombreUsuReserva=$ListarUsuario2[0]["ApellidoPaterno"].' '.$ListarUsuario2[0]["ApellidoMaterno"].' '.$ListarUsuario2[0]["Nombres"];

				$FechaDespachoX=$DatosDonante[0]["FechaDespacho"];
				$FechaDespacho=empty($FechaDespachoX) ? '' : vfecha(substr($FechaDespachoX,0,10));
				$HoraDespacho=substr($FechaDespachoX,11,5);
			
				$UsuDespacho=$DatosDonante[0]["UsuDespacho"];
				$ListarUsuario1=ListarUsuarioxIdempleado_M($UsuDespacho);
				$NombreUsuDespacho=$ListarUsuario1[0]["ApellidoPaterno"].' '.$ListarUsuario1[0]["ApellidoMaterno"].' '.$ListarUsuario1[0]["Nombres"];
				
				
				
				#SETEANDO PUNTERO
				$this->SetXY(-178,-36);	//(WIDTH,HEIGHT)		
				$this->Cell(40,20,$NombreUsuReserva,0,0,'C');
				
				$this->Cell(48);
				$this->Cell(40,20,$NombreUsuReserva,0,0,'C');
				
				$this->Ln(5);
				$this->Cell(25);
				$this->Cell(20,20,$cmpReserva,0,0,'L');
				
				$this->Ln(4);
				$this->Cell(25);
				$this->Cell(20,20,$FechaReserva,0,0,'L');
				$this->Cell(68);
				$this->Cell(20,20,$FechaDespacho,0,0,'L');
				
				$this->Ln(4);
				$this->Cell(25);
				$this->Cell(20,20,$HoraReserva,0,0,'L');
				$this->Cell(68);
				$this->Cell(20,20,$HoraDespacho,0,0,'L');
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			if (is_null($this->_tplIdx)) {	
				// THIS IS WHERE YOU GET THE NUMBER OF PAGES
				$this->numPages = $this->setSourceFile($fullPathToFile);
				$this->_tplIdx = $this->importPage(1);
			}
			//$this->useTemplate($this->_tplIdx, 0, 0, 200);
			$this->useTemplate($this->_tplIdx, 0, 0, 300,200,true);
		}
	
		function RotatedText($x, $y, $txt, $angle) {
			//Text rotated around its origin
			$this->Rotate($angle, $x, $y);
			$this->Text($x, $y, $txt);
			$this->Rotate(0);
		}

	}


	$pdf = new PDF();
	$pdf->AddPage();
	//$pdf->SetFont('Arial', '', 12);
	$pdf->Output();	


?>

