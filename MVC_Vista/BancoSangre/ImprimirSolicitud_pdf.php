<?php 
//ini_set('error_reporting',0);//para xamp
ini_set('memory_limit', '1024M'); 
date_default_timezone_set('America/Bogota');

require_once('../../MVC_Complemento/fpdf/fpdfLV.php');

require_once('../../MVC_Complemento/setasign/Fpdi/autoload.php');
use \setasign\Fpdi\Fpdi;

require_once('../../MVC_Complemento/setasign/FpdiProtection/autoload.php');
use \setasign\FpdiProtection\FpdiProtection;

	class PDF_Rotate extends FPDI {
	
		/*var $angle = 0;
	
		function Rotate($angle, $x = -1, $y = -1) {
			if ($x == -1)
				$x = $this->x;
			if ($y == -1)
				$y = $this->y;
			if ($this->angle != 0)
				$this->_out('Q');
			$this->angle = $angle;
			if ($angle != 0) {
				$angle*=M_PI / 180;
				$c = cos($angle);
				$s = sin($angle);
				$cx = $x * $this->k;
				$cy = ($this->h - $y) * $this->k;
				$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
			}
		}
	
		function _endpage() {
			if ($this->angle != 0) {
				$this->angle = 0;
				$this->_out('Q');
			}
			parent::_endpage();
		}*/
	
	}
	
	

	$fullPathToFile = "../../MVC_Complemento/setasign/SOLICITUD-TRANSFUSIONAL-1.pdf";
	//$fullPathToFile2 = "../../MVC_Complemento/setasign/SOLICITUD-TRANSFUSIONAL-2.pdf";

	class PDF extends PDF_Rotate {

		var $_tplIdx;
	
		function Header() {
			global $fullPathToFile;	//global $fullPathToFile2;	
			
			//DATOS			
			$IdSolicitudSangre=$_REQUEST["IdSolicitudSangre"];
			$DatosSolicitudCabecera=ImprimirSolicitud($IdSolicitudSangre,0);
			
				$Paciente=SIGESA_BSD_Buscarpaciente_M($DatosSolicitudCabecera[0]["IdPaciente"],'IdPaciente');	
				//$NombresPaciente=$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"];
				$IdTipoSexo=$Paciente[0]["IdTipoSexo"];
				$NroHistoriaClinica=$Paciente[0]["NroHistoriaClinica"];			
							
				$FechaNacimientoX = time() - strtotime($Paciente[0]['FechaNacimiento']);
				$edad = floor((($FechaNacimientoX / 3600) / 24) / 360);	
			
			$NroSolicitud=$DatosSolicitudCabecera[0]["NroSolicitud"];	
			$IdTipoServicio=$DatosSolicitudCabecera[0]["IdTipoServicio"];			
			$Servicio=$DatosSolicitudCabecera[0]["Servicio"];	
			$CodigoCama=$DatosSolicitudCabecera[0]["CodigoCama"];
			$GrupoSanguineo=$DatosSolicitudCabecera[0]["GrupoSanguineo"];
			$PesoReceptor=$DatosSolicitudCabecera[0]["PesoReceptor"];
			
			$Diagnostico1=$DatosSolicitudCabecera[0]["Diagnostico1"];
			$Diagnostico2=$DatosSolicitudCabecera[0]["Diagnostico2"];
			
			$swTransPre=$DatosSolicitudCabecera[0]["swTransPre"];
			$swReacTrans=$DatosSolicitudCabecera[0]["swReacTrans"];
			$swReacAler=$DatosSolicitudCabecera[0]["swReacAler"];
			
			$EmbarazoPrevio=$DatosSolicitudCabecera[0]["EmbarazoPrevio"];
			$Aborto=$DatosSolicitudCabecera[0]["Aborto"];
			$IncompatibilidadMF=$DatosSolicitudCabecera[0]["IncompatibilidadMF"];
			
			$Hemoglobina=$DatosSolicitudCabecera[0]["Hemoglobina"]; 
			$Hematocrito=$DatosSolicitudCabecera[0]["Hematocrito"];
			
			$swAnemia1=$DatosSolicitudCabecera[0]["swAnemia1"];
			$swAnemia2=$DatosSolicitudCabecera[0]["swAnemia2"];
			
			$Tromboplastina=$DatosSolicitudCabecera[0]["Tromboplastina"];
			$Protrombina=$DatosSolicitudCabecera[0]["Protrombina"];
			$INR=$DatosSolicitudCabecera[0]["INR"];
			
			$swPlasma1=$DatosSolicitudCabecera[0]["swPlasma1"];
			$Plaquetas=$DatosSolicitudCabecera[0]["Plaquetas"];
			$swPQ1=$DatosSolicitudCabecera[0]["swPQ1"];
			
			$swConsentimiendoInformado=$DatosSolicitudCabecera[0]["swConsentimiendoInformado"];
			
			//FIN DATOS
			
			$this->SetFont('Helvetica','',14);
			$this->SetTextColor(0, 0, 0);		
			
			//LINEA 1
			$this->Cell(155);//espacios a la derecha	
			$this->Cell(20,1,$NroSolicitud,0,0,'L');
			
			$this->SetFont('Helvetica','',11);
			$this->SetTextColor(0, 0, 0);
			$this->Ln(0);//espacios abajo
					
			//LINEA 2
			$this->Cell(20);//espacios a la derecha	
				
			$this->Cell(50,57,utf8_decode($Paciente[0]["ApellidoPaterno"]),0,0,'L');//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])	
			$this->Cell(52,57,utf8_decode($Paciente[0]["ApellidoMaterno"]),0,0,'L');
			$this->Cell(50,57,utf8_decode($Paciente[0]["PrimerNombre"].' '.$Paciente[0]["SegundoNombre"]),0,0,'L');
					
			$this->Ln(6);//espacios abajo
			$this->Cell(8);//espacios a la derecha
			if($IdTipoSexo==1){
				$this->Cell(10,58,'X',0,0,'L');
				$this->Cell(10);
			}else{
				$this->Cell(10);
				$this->Cell(10,58,'X',0,0,'L');
			}
			$this->Cell(10);
			$this->Cell(20,57,$edad,0,0,'L');
			
			$this->Cell(20);
			$this->Cell(30,57,$NroHistoriaClinica,0,0,'L');
			
			if($IdTipoServicio==1){ //Consultorios Externos
				$this->Cell(11);
				$this->Cell(10,58,'X',0,0,'L');				
				
			}else if($IdTipoServicio==2 || $IdTipoServicio==4){ //Consultorios y observacion de Emergencia
				$this->Cell(36);
				$this->Cell(10,58,'X',0,0,'L');
				
			}else if($IdTipoServicio==3){ //Hospitalización 
				$this->Cell(62);
				$this->Cell(10,58,'X',0,0,'L');
								
			}else{ //vacio
				$this->Cell(10);
			}
			
			$this->Ln(5);//espacios abajo
			$this->Cell(10);
			
			$this->SetFont('Helvetica','',8);
			$this->SetTextColor(0, 0, 0);
			$this->Cell(52,59,$Servicio,0,0,'L');
			
			$this->Cell(13,59,'',0,0,'L');			
			$this->Cell(30,59,$CodigoCama,0,0,'L');
			
			$this->Cell(3,59,'',0,0,'L');			
			$this->Cell(15,59,$PesoReceptor,0,0,'L');
			
			$this->Cell(50,59,'',0,0,'L');
			$this->Cell(30,59,$GrupoSanguineo,0,0,'L');			
			
			$this->Ln(5);//espacios abajo
			$this->Cell(10);			
			$this->SetFont('Helvetica','',11);
			
			//Transfusiones previas Y Embarazo Previo
			if($swTransPre=='SI'){//swTransPre, swReacTrans, swReacAler, EmbarazoPrevio, Aborto, IncompatibilidadMF,						
				$this->Cell(36);
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(22);
				
			}else if($swTransPre=='NO'){
				$this->Cell(47);
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(11);
				
			}else if($swTransPre=='Desconocido'){
				$this->Cell(58);
				$this->Cell(36,77,'X',0,0,'L');
			}
			
			if($EmbarazoPrevio=='SI'){						
				$this->Cell(33);
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(23);
				
			}else if($EmbarazoPrevio=='NO'){
				$this->Cell(45); 
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(11);
				
			}else if($EmbarazoPrevio=='Desconocido'){
				$this->Cell(56);
				$this->Cell(36,77,'X',0,0,'L');				
			}	
			//FIN Transfusiones previas Y Embarazo Previo
			
			$this->Ln(3);//espacios abajo
			$this->Cell(10);			
			
			//Reacciones alérgicas Y Incompatibilidad Materno fetal
			if($swReacAler=='SI'){//swTransPre, swReacTrans, swReacAler, EmbarazoPrevio, Aborto, IncompatibilidadMF,						
				$this->Cell(36);
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(22);
				
			}else if($swReacAler=='NO'){
				$this->Cell(47);
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(11);
				
			}else if($swReacAler=='Desconocido'){
				$this->Cell(58);
				$this->Cell(36,77,'X',0,0,'L');
			}
			
			if($IncompatibilidadMF=='SI'){						
				$this->Cell(33);
				$this->Cell(36,77,'X',0,0,'L');
				
			}else if($IncompatibilidadMF=='NO'){
				$this->Cell(45); 
				$this->Cell(36,77,'X',0,0,'L');
				
			}else if($IncompatibilidadMF=='Desconocido'){
				$this->Cell(56);
				$this->Cell(36,77,'X',0,0,'L');
			}		
			//FIN Reacciones alérgicas Y Incompatibilidad Materno fetal
			
			$this->Ln(4);//espacios abajo
			$this->Cell(10);			
			
			//Reacciones transfusionales previas Y Aborto previo
			if($swReacTrans=='SI'){//swTransPre, swReacTrans, swReacAler, EmbarazoPrevio, Aborto, IncompatibilidadMF,						
				$this->Cell(36);
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(22);
				
			}else if($swReacTrans=='NO'){
				$this->Cell(47);
				$this->Cell(36,77,'X',0,0,'L');
				$this->Cell(11);
				
			}else if($swReacTrans=='Desconocido'){
				$this->Cell(58);
				$this->Cell(36,77,'X',0,0,'L');
			}
			
			if($Aborto=='SI'){						
				$this->Cell(33);
				$this->Cell(36,77,'X',0,0,'L');
				
			}else if($Aborto=='NO'){
				$this->Cell(45); 
				$this->Cell(36,77,'X',0,0,'L');
				
			}else if($Aborto=='Desconocido'){
				$this->Cell(56);
				$this->Cell(36,77,'X',0,0,'L');
			}		
			//FIN Reacciones transfusionales previas Y Aborto previo
			
			
			//Diagnosticos
			$this->SetFont('Helvetica','',8);
			$this->Ln(20);//espacios abajo
			$this->Cell(20,62,'',0,0,'L');
			$this->Cell(20,62,utf8_decode($Diagnostico1),0,0,'L');
			$this->Ln(6);
			$this->Cell(20,62,'',0,0,'L');
			$this->Cell(20,62,utf8_decode($Diagnostico2),0,0,'L');
			
			
			//Cantidades
			$this->Ln(7);//espacios abajo
			$this->Cell(1);
			$this->Cell(15,63,round($Hemoglobina,2),0,0,'L');
			$this->Cell(14);
			$this->Cell(15,63,round($Hematocrito,2),0,0,'L');
			$this->Cell(29);
			$this->Cell(15,63,round($Tromboplastina,2),0,0,'L');//TTP 
			$this->Cell(8);
			$this->Cell(15,63,round($Protrombina,2),0,0,'L');//TP
			$this->Cell(3);
			$this->Cell(10,63,round($INR,2),0,0,'L');
			$this->Cell(33);
			$this->Cell(15,63,round($Plaquetas,2),0,0,'L');
			
			//ANEMIAS
			$this->Ln(2);//espacios abajo	
			$this->SetFont('Helvetica','',11);		
			if($swAnemia1=='Aguda'){//swTransPre, swReacTrans, swReacAler, EmbarazoPrevio, Aborto, IncompatibilidadMF, 
				$this->Cell(30,72,'X',0,0,'C');
				$this->Cell(32);				
				
			}else if($swAnemia1=='Crónica'){
				$this->Cell(32);
				$this->Cell(30,72,'X',0,0,'L');				
			}
			
			if($swAnemia2=='Leve'){//swTransPre, swReacTrans, swReacAler, EmbarazoPrevio, Aborto, IncompatibilidadMF, 
				$this->Cell(54,72,'X',0,0,'C');
				$this->Cell(30);
				
			}else if($swAnemia2=='Moderada'){
				$this->Cell(38);
				$this->Cell(30,72,'X',0,0,'L');	
							
			}else if($swAnemia2=='Severa'){
				$this->Cell(59);
				$this->Cell(30,72,'X',0,0,'L');				
			}
			
			
			//DETALLES
			//det.CodigoComponente,det.Cantidad,det.Volumen,det.TipoRequerimiento,det.FechaProg
			$DatosSolicitudDetalle=ImprimirSolicitud($IdSolicitudSangre,0);
			
			$CantidadST='';$VolumenST=''; 
			$CantidadPG='';$VolumenPG=''; 
			$CantidadPFC='';$VolumenPFC='';
			$CantidadPQAF='';$VolumenPQAF='';
			$CantidadPQ='';$VolumenPQ='';
			$CantidadCRIO='';$VolumenCRIO='';			
			
			$TipoRequerimientoST='';$FechaProgST=''; 
			$TipoRequerimientoPG='';$FechaProgPG=''; 
			$TipoRequerimientoPFC='';$FechaProgPFC='';
			$TipoRequerimientoPQAF='';$FechaProgPQAF='';
			$TipoRequerimientoPQ='';$FechaProgPQ='';
			$TipoRequerimientoCRIO='';$FechaProgCRIO='';
			
			foreach($DatosSolicitudDetalle as $item){
				$CodigoComponente=$item["CodigoComponente"];//ST,PG-1,PFC,PQAF,PQ,CRIO
				$Cantidad=$item["Cantidad"];
				$Volumen=$item["Volumen"];
				$TipoRequerimiento=$item["TipoRequerimiento"];
				$FechaProg=$item["FechaProg"];				
								
				$ArregloCodigo=explode('-', $CodigoComponente);
				if(trim($ArregloCodigo[0])=="ST"){ //1
					$ComponenteST=$CodigoComponente;//ST,PG-1,PFC,PQAF,PQ,CRIO
					$CantidadST=$Cantidad;
					$VolumenST=$Volumen;
					$TipoRequerimientoST=$TipoRequerimiento;
					$FechaProgST=$FechaProg;
				}
				if(trim($ArregloCodigo[0])=="PG" ){//|| $ArregloCodigo[0]=="GRlav"
					$ComponentePG=$CodigoComponente;//ST,PG-1,PFC,PQAF,PQ,CRIO
										
					if(count($ArregloCodigo)>=2){
						$CantidadPG=$ArregloCodigo[1];
					}else{
						$CantidadPG=$Cantidad;
					}	
					
					$VolumenPG=$Volumen;
					$TipoRequerimientoPG=$TipoRequerimiento;
					$FechaProgPG=$FechaProg;
				}
				if(trim($ArregloCodigo[0])=="PFC"){
					$ComponentePFC=$CodigoComponente;//ST,PG-1,PFC,PQAF,PQ,CRIO
					$CantidadPFC=$Cantidad;
					$VolumenPFC=$Volumen;
					$TipoRequerimientoPFC=$TipoRequerimiento;
					$FechaProgPFC=$FechaProg;
				}
				if(trim($ArregloCodigo[0])=="PQAF"){
					$ComponentePQAF=$CodigoComponente;//ST,PG-1,PFC,PQAF,PQ,CRIO
					$CantidadPQAF=$Cantidad;
					$VolumenPQAF=$Volumen;
					$TipoRequerimientoPQAF=$TipoRequerimiento;
					$FechaProgPQAF=$FechaProg;
				}
				if(trim($ArregloCodigo[0])=="PQ"){
					$ComponentePQ=$CodigoComponente;//ST,PG-1,PFC,PQAF,PQ,CRIO
					$CantidadPQ=$Cantidad;
					$VolumenPQ=$Volumen;
					$TipoRequerimientoPQ=$TipoRequerimiento;
					$FechaProgPQ=$FechaProg;
				}
				if(trim($ArregloCodigo[0])=="CRIO"){
					$ComponenteCRIO=$CodigoComponente;//ST,PG-1,PFC,PQAF,PQ,CRIO
					$CantidadCRIO=$Cantidad;
					$VolumenCRIO=$Volumen;
					$TipoRequerimientoCRIO=$TipoRequerimiento;
					$FechaProgCRIO=$FechaProg;
				}				
				
			}
			    
				//ST
				$this->Ln(28);//espacios abajo	
				$this->SetFont('Helvetica','',11);		
				$this->Cell(53);
				$this->Cell(20,72,$CantidadST,0,0,'C');	
				$this->Cell(17,72,$VolumenST,0,0,'C');
				
				$this->Cell(10);	
				if($TipoRequerimientoST=="Muy urgente"){
					$this->Cell(13,72,"X",0,0,'C');
					
				}else if($TipoRequerimientoST=="Urgente"){
					$this->Cell(24);
					$this->Cell(13,72,"Y",0,0,'C');
					
				}else if($TipoRequerimientoST=="Programado"){
					$this->Cell(45);
					$this->Cell(30,72,vfecha(substr($FechaProgST,0,10)).' '.substr($FechaProgST,11,5),0,0,'C');
				}					
				
				//PG
				$this->Ln(8);//espacios abajo							
				$this->Cell(53);
				$this->Cell(20,72,$CantidadPG,0,0,'C');	
				$this->Cell(17,72,$VolumenPG,0,0,'C');
				
				$this->Cell(10);	
				if($TipoRequerimientoPG=="Muy urgente"){
					$this->Cell(13,72,"X",0,0,'C');
					
				}else if($TipoRequerimientoPG=="Urgente"){
					$this->Cell(24);
					$this->Cell(13,72,"Y",0,0,'C');
					
				}else if($TipoRequerimientoPG=="Programado"){
					$this->Cell(45);
					$this->Cell(30,72,vfecha(substr($FechaProgPG,0,10)).' '.substr($FechaProgPG,11,5),0,0,'C');
				}
				
				//PFC				
				$this->Ln(8);//espacios abajo						
				$this->Cell(53);
				$this->Cell(20,72,$CantidadPFC,0,0,'C');	
				$this->Cell(17,72,$VolumenPFC,0,0,'C');
				
				$this->Cell(10);	
				if($TipoRequerimientoPFC=="Muy urgente"){
					$this->Cell(13,72,"X",0,0,'C');
					
				}else if($TipoRequerimientoPFC=="Urgente"){
					$this->Cell(24);
					$this->Cell(13,72,"Y",0,0,'C');
					
				}else if($TipoRequerimientoPFC=="Programado"){
					$this->Cell(45);
					$this->Cell(30,72,vfecha(substr($FechaProgPFC,0,10)).' '.substr($FechaProgPFC,11,5),0,0,'C');
				}
				
				//PQAF				
				$this->Ln(8);//espacios abajo						
				$this->Cell(53);
				$this->Cell(20,72,$CantidadPQAF,0,0,'C');	
				$this->Cell(17,72,$VolumenPQAF,0,0,'C');
				
				$this->Cell(10);	
				if($TipoRequerimientoPQAF=="Muy urgente"){
					$this->Cell(13,72,"X",0,0,'C');
					
				}else if($TipoRequerimientoPQAF=="Urgente"){
					$this->Cell(24);
					$this->Cell(13,72,"Y",0,0,'C');
					
				}else if($TipoRequerimientoPQAF=="Programado"){
					$this->Cell(45);
					$this->Cell(30,72,vfecha(substr($FechaProgPQAF,0,10)).' '.substr($FechaProgPQAF,11,5),0,0,'C');
				}
				
				//PQ
				$this->Ln(8);//espacios abajo							
				$this->Cell(53);
				$this->Cell(20,72,$CantidadPQ,0,0,'C');	
				$this->Cell(17,72,$VolumenPQ,0,0,'C');
				
				$this->Cell(10);	
				if($TipoRequerimientoPQ=="Muy urgente"){
					$this->Cell(13,72,"X",0,0,'C');
					
				}else if($TipoRequerimientoPQ=="Urgente"){
					$this->Cell(24);
					$this->Cell(13,72,"Y",0,0,'C');
					
				}else if($TipoRequerimientoPQ=="Programado"){
					$this->Cell(45);
					$this->Cell(30,72,vfecha(substr($FechaProgPQ,0,10)).' '.substr($FechaProgPQ,11,5),0,0,'C');
				}
				
				//CRIO
				$this->Ln(7);//espacios abajo					
				$this->Cell(53);
				$this->Cell(20,72,$CantidadCRIO,0,0,'C');	
				$this->Cell(17,72,$VolumenCRIO,0,0,'C');	
				
				$this->Cell(10);	
				if($TipoRequerimientoCRIO=="Muy urgente"){
					$this->Cell(13,72,"X",0,0,'C');
					
				}else if($TipoRequerimientoCRIO=="Urgente"){
					$this->Cell(24);
					$this->Cell(13,72,"Y",0,0,'C');
					
				}else if($TipoRequerimientoCRIO=="Programado"){
					$this->Cell(45);
					$this->Cell(30,72,vfecha(substr($FechaProgCRIO,0,10)).' '.substr($FechaProgCRIO,11,5),0,0,'C');
				}		
			
			//CONSENTIMIENTO
			$this->Ln(29);//espacios abajo	
			$this->SetFont('Helvetica','',11);		
			$this->Cell(124);
			
			if($swConsentimiendoInformado=='SI'){
				$this->Cell(15,72,"X",0,0,'L');
			}else{
				$this->Cell(29);		
				$this->Cell(15,72,"X",0,0,'L');	
			}	
			
			//MEDICOS
			$MedicoSolicitante=$DatosSolicitudCabecera[0]["MedicoSolicitante"];
			$ListarUsuario1=ListarUsuarioxIdempleado_M($MedicoSolicitante);
			$NombreSolicitante=$ListarUsuario1[0]["ApellidoPaterno"].' '.$ListarUsuario1[0]["ApellidoMaterno"].' '.$ListarUsuario1[0]["Nombres"];				
			$cmpSolicitante=$DatosSolicitudCabecera[0]["cmpSolicitante"];
			
			$FechaSolicitudX=$DatosSolicitudCabecera[0]["FechaSolicitud"];
			$FechaSolicitud=empty($FechaSolicitudX) ? '' : vfecha(substr($FechaSolicitudX,0,10));
			$HoraSolicitud=substr($FechaSolicitudX,11,5);
			
			$UsuTomoMuestraPaciente=$DatosSolicitudCabecera[0]["UsuTomoMuestraPaciente"];	
			$ListarUsuario2=ListarUsuarioxIdempleado_M($UsuTomoMuestraPaciente);
			$NombreTM=$ListarUsuario2[0]["ApellidoPaterno"].' '.$ListarUsuario2[0]["ApellidoMaterno"].' '.$ListarUsuario2[0]["Nombres"];
					
			$UsuRecepcion=$DatosSolicitudCabecera[0]["UsuRecepcion"]; 
			$ListarUsuario3=ListarUsuarioxIdempleado_M($UsuRecepcion);
			$NombreRe=$ListarUsuario3[0]["ApellidoPaterno"].' '.$ListarUsuario3[0]["ApellidoMaterno"].' '.$ListarUsuario3[0]["Nombres"];
						
			$FechaRecepcionX=$DatosSolicitudCabecera[0]["FechaRecepcion"];
			$FechaRecepcion=empty($FechaRecepcionX) ? '' : vfecha(substr($FechaRecepcionX,0,10));
			$HoraRecepcion=substr($FechaRecepcionX,11,5);
			
			$this->Ln(42);//espacios abajo	
			$this->SetFont('Helvetica','',9);	
				
			$this->Cell(15);			
			$this->Cell(15,80,utf8_decode($NombreSolicitante),0,0,'C');
			
			$this->Cell(50);			
			$this->Cell(15,80,utf8_decode($NombreTM),0,0,'C');
			
			$this->Cell(50);			
			$this->Cell(15,80,utf8_decode($NombreRe),0,0,'C');
			
			//
			$this->Ln(5);//espacios abajo					
			$this->Cell(25);			
			$this->Cell(15,80,$cmpSolicitante,0,0,'L');
			//
			$this->Ln(4);//espacios abajo					
			$this->Cell(22);			
			$this->Cell(15,80,$FechaSolicitud,0,0,'L');
			
			$this->Cell(44);			
			$this->Cell(15,80,'',0,0,'C');
			
			$this->Cell(58);			
			$this->Cell(15,80,$FechaRecepcion,0,0,'L');
			
			//
			$this->Ln(4);//espacios abajo					
			$this->Cell(22);			
			$this->Cell(15,80,$HoraSolicitud,0,0,'L');
			
			$this->Cell(44);			
			$this->Cell(15,80,'',0,0,'C');
			
			$this->Cell(58);			
			$this->Cell(15,80,$HoraRecepcion,0,0,'L');
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									
			
			if (is_null($this->_tplIdx)) {	
				// THIS IS WHERE YOU GET THE NUMBER OF PAGES
				$this->numPages = $this->setSourceFile($fullPathToFile);
				$this->_tplIdx = $this->importPage(1);
			}
			//$this->useTemplate($this->_tplIdx, 0, 0, 200);
			$this->useTemplate($this->_tplIdx, 0, 0, 200,300,true);
		}
	
		function RotatedText($x, $y, $txt, $angle) {
			//Text rotated around its origin
			$this->Rotate($angle, $x, $y);
			$this->Text($x, $y, $txt);
			$this->Rotate(0);
		}

	}


	$pdf = new PDF();
	$pdf->AddPage();
	//$pdf->SetFont('Arial', '', 12);
	$pdf->Output();	


?>

