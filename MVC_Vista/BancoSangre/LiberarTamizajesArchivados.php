<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	
	/*$FechaReg_Inicio=isset($_REQUEST['FechaReg_Inicio']) ? $_REQUEST['FechaReg_Inicio'] : '';	
	$FechaReg_Final=isset($_REQUEST['FechaReg_Final']) ? $_REQUEST['FechaReg_Final'] : date('d/m/Y');
	
	$RealizaNAT=isset($_REQUEST['RealizaNAT']) ? $_REQUEST['RealizaNAT'] : '';	
	
	$FechaActual = vfecha(substr($FechaServidor,0,10));
	$HoraActual = substr($FechaServidor,11,8);*/
	
	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_REQUEST['IdEmpleado']);
	$Usuario=$ListarUsuarioxIdempleado[0]['Usuario'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HOSPITAL NACIONAL DANIEL ALCIDES CARRION - SERVICIO DE HEMOTERAPIA Y BANCO DE SANGRE</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Fecha --> 
    <link rel="stylesheet" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css" />     
    
<!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/tables/site.js"></script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async></script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->
    
    <script type="text/javascript">	
		   
		   function buscar(){
			   var NroDonacion=document.getElementById('NroDonacion').value;		  
			   if(NroDonacion==""){
				   alertify.error('Debe ingresar Nro Donacion');
			   }else{
				   $('#form1').submit();	
			   }
			}
			
			function cancelar(){
				window.location = "../BancoSangre/BancoSangreC.php?acc=LiberarTamizajesArchivados&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>";
			}
           
		   function Desarchivar(){
			   var NroDonacion=document.getElementById('NroDonacion').value;	   
			   
			   var Mensaje="¿Seguro de Desarchivar el Tamizaje de Nro Donacion: "+NroDonacion+" ?"; 
				
			   var DesEstadoArchivado=document.getElementById('DesEstadoArchivado').value;
			   var TieneVerificacion=document.getElementById('TieneVerificacion').value;
			   	
				if(NroDonacion==""){
					 alertify.error('Debe ingresar Nro Donacion');
					 document.getElementById('NroDonacion').focus();
					 return 0;
					 
				}else if(DesEstadoArchivado.trim()==""){
					 alertify.error('No existe Tamizaje con el Número de Donación seleccionado');
					 return 0;
					 
				}else if(DesEstadoArchivado.trim()=="NO"){
					 alertify.error('El Tamizaje de Nro Donacion:'+ NroDonacion+ ' NO está archivado');
					 return 0;
					 
				}else if(TieneVerificacion.trim()=="SI"){
					 alertify.error('No se puede Desarchivar porque el Nro Donacion:'+ NroDonacion+ ' tiene registro en Verificación de Donantes Aptos');
					 return 0;
				}				
							     		
					alertify.confirm(Mensaje, function (e) {
						if (e) {
							//alertify.success("Verificación Quirúrgica eliminada correctamente");
							//Redireccionamos si das a aceptar
					 		window.location.href = "../BancoSangre/BancoSangreC.php?acc=DesarchivarTamizaje&NroDonacion="+NroDonacion+"&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>";    //página web a la que te redirecciona si confirmas la eliminación
						} else {
							//alertify.alert("Successful AJAX after Cancel");
							alertify.error('Cancelado')
						}
					});			
		   }
		   
   </script> 
</head>

<body>


    <div id="wrapper">
        <div id="page-wrapper">            
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary"> <!--panel-default-->
                        <div class="panel-heading"> BUSQUEDA DE TAMIZAJES ARCHIVADOS DEFINITIVAMENTE </div>
                        <div class="panel-body">
                        <form action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=LiberarTamizajesArchivados&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>" name="form1" id="form1" method="post">                        
                        	 <div class="" align="right">
                                <!--<a class="btn btn-success" onClick="validarguardar();" href="#">Registrar</a>-->
                                <input class="btn btn-success" type="button" onclick="buscar()" value="Buscar"/>
                                &nbsp;<input class="btn btn-danger" type="button" onClick="cancelar();" value="Cancelar" />
                                &nbsp;<input class="btn btn-warning" type="button" onclick="Desarchivar()" value="Desarchivar"/>&nbsp;
                                <!--&nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                              </div>
                                                 
                            <!--fila 1-->
                            <div class="form-group row">
                                <label class="control-label col-xs-1"></label>
                            </div>                            
                            <div class="form-group row">                                               
                                <label class="control-label col-xs-1">Nro Donacion</label>
                                <div class="col-xs-2">
                                    <input type="text" id="NroDonacion" name="NroDonacion" class="form-control input-sm" placeholder="Nro Donacion" value="<?php echo $NroDonacion ?>" onChange="buscar()"/>   
                                </div>                              
                                ejm: 1705940 
                                
                                
                        </div><!--fin fila 1-->
                        
                                          
                        </form>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            
                               <?php  							
									/*$FechaReg_Inicio=$_REQUEST["FechaReg_Inicio"];
									if($FechaReg_Inicio!=NULL){
										$FechaReg_Inicio=gfecha($_REQUEST["FechaReg_Inicio"]);
									}
									$FechaReg_Final=$_REQUEST["FechaReg_Final"];
									if($FechaReg_Final!=NULL){
										$FechaReg_Final=gfecha($_REQUEST["FechaReg_Final"]);
									}	*/									
									 
								?>
                            
                                <table width="100%" class="table table-bordered" id="dataTables-example"> <!--table-hover-->
                                	<thead>
                                        <tr class="text-center">
                                            <th rowspan="2">&nbsp;</th>
                                            <th rowspan="2">#Donación</th>
                                            <th rowspan="2">GS</th>
                                            <th rowspan="2">Fecha Extraccion</th>
                                            <th rowspan="2">Fecha Etiquetado y Liberación</th>
                                            <th rowspan="2">Tubuladura</th>
                                            <th colspan="3">Sello Nacional de Calidad de Sangre (SNCS)</th>
                                            <th rowspan="2" align="center">Observación</th>
                                            <th rowspan="2" align="center">Archivado</th>                                          
                                         </tr>                                    
                                        <tr class="text-center">
                                            <th>PG</th>
                                            <th>PFC</th>
                                            <th>PQ</th>                                             
                                         </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
									  		$i=0;								
											$numactivos=0; $numentrada=0;$numpausa=0;$numsalida=0; $todosId="";
											$ReporteSNCS=ReporteArchivoDefinitivo_M($NroDonacion);//PRUEBA (FALTA CREAR COSULTA)																					
											//$ReporteSNCS=SIGESA_ListVer_FiltroPacientesAtendidos_M($FechaReg_Inicio,$FechaReg_Final,$Servicio,$TipoSala,$Sala);											
											
											if($ReporteSNCS!=NULL){
									  		foreach($ReporteSNCS as $itemSNCS){
												$i++; 
									  			$EstadoAptoTamizaje=$itemSNCS["EstadoAptoTamizaje"];
												if($EstadoAptoTamizaje=='1'){
													$DesEstadoAptoTamizaje='<font color="#00FF00">NO REACTIVO</font>';
													$colorestado="#FFF";
													$Observacion='';
												}else if($EstadoAptoTamizaje=='0'){
													$EstadoAptoTamizaje='<font color="#FF0000">REACTIVO</font>';
													$colorestado="#FFAAAA";
													
													$PRquimioVIH=($itemSNCS["quimioVIH"]!='0') ? 'VIH '.round($itemSNCS["PRquimioVIH"],2) : '';					
													$PRquimioSifilis=($itemSNCS["quimioSifilis"]!='0') ? 'Sifilis '.round($itemSNCS["PRquimioSifilis"],2) : '';													
													$PRquimioHTLV=($itemSNCS["quimioHTLV"]!='0') ? 'HTLV '.round($itemSNCS["PRquimioHTLV"],2) : '';														
													$PRquimioANTICHAGAS=($itemSNCS["quimioANTICHAGAS"]!='0') ? 'Antichagas '.round($itemSNCS["PRquimioANTICHAGAS"],2) : '';														
													$PRquimioHBS=($itemSNCS["quimioHBS"]!='0') ? 'HBS '.round($itemSNCS["PRquimioHBS"],2) : '';														
													$PRquimioVHB=($itemSNCS["quimioVHB"]!='0') ? 'VHB '.round($itemSNCS["PRquimioVHB"],2) : '';														
													$PRquimioVHC=($itemSNCS["quimioVHC"]!='0') ? 'VHC '.round($itemSNCS["PRquimioVHC"],2) : '';														
													$Observacion=$PRquimioVIH.' '.$PRquimioSifilis.' '.$PRquimioHTLV.' '.$PRquimioANTICHAGAS.' '.$PRquimioHBS.' '.$PRquimioVHB.' '.$PRquimioVHC;
													
												}else if($EstadoAptoTamizaje==''){
													$EstadoAptoTamizaje='<font color="#0000FF">FALTA</font>';
													$colorestado="#d3f1f8";
													$Observacion='F - '.mb_strtolower($itemSNCS["DesMotivoElimMuestra"]);
												}
												
												//SNCS	
												$IdTipoDonacion=$itemSNCS["IdTipoDonacion"];								
												$SNCS=$itemSNCS["SNCS"];
												
												if(trim($IdTipoDonacion)=='1'){ //SÓLO SANGRE TOTAL
													if(trim($itemSNCS["DesMotivoElimPaqueteGlobu"])!=''){ //PG
														$SNCSPG='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPaqueteGlobu"]).'</font>';
													}else if($EstadoAptoTamizaje=='1'){														
														if($SNCS!=NULL){
															$SNCSPG='PG-'.$SNCS;
														}else{
															$SNCSPG='PG-#SNCS';
														}
													}else{
														$SNCSPG='';
													}
													
													if(trim($itemSNCS["DesMotivoElimPlasma"])!=''){ //PFC													
														$SNCSPFC='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPlasma"]).'</font>';														
													}else if($EstadoAptoTamizaje=='1'){														
														if($SNCS!=NULL){
															$SNCSPFC='PFC-'.$SNCS;
														}else{
															$SNCSPFC='PFC-#SNCS';
														}
													}else{
														$SNCSPFC='';
													}
																									
												}else{ //SÓLO AFERESIS
													$SNCSPG='';
													$SNCSPFC='';
												}											
												
												//TODO (SANGRE TOTAL Y AFERESIS) 
												if(trim($itemSNCS["DesMotivoElimPlaquetas"])!=''){ //PQ
													$SNCSPQ='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPlaquetas"]).'</font>';
												}else if($EstadoAptoTamizaje=='1'){													
													if($SNCS!=NULL){
														$SNCSPQ='PQ-'.$SNCS;
													}else{
														$SNCSPQ='PQ-#SNCS';
													}
												}else{
													$SNCSPQ='';
												}
												
												$EstadoArchivado=$itemSNCS["EstadoArchivado"];
												 if($EstadoArchivado==''){
													 $DesEstadoArchivado='';
												 }else if($EstadoArchivado=='0'){
													 $DesEstadoArchivado='<font color="#0000FF">NO</font>';
												 }else{
													 $DesEstadoArchivado='<font color="#FF0000">SI</font>'; 
												 }	
												 
												 //Validar si tiene registro en la tabla SIGESA_BSD_VerificacionAptos
												 $ObtenerDatosVerificacionAptos=ObtenerDatosVerificacionAptos_M($itemSNCS["IdFraccionamiento"]);
												 if($ObtenerDatosVerificacionAptos==''){
													 $TieneVerificacion='NO';
												 }else{
													 $TieneVerificacion='SI'; 
												 }																						
												
									  ?>                                                                     						
 
                                      <tr  bgcolor="<?php echo $colorestado; ?>" class="text-center">
                                            <td align="center"><?php echo $i;?></td>
                                            <td align="center"><?php echo 'DAC'.$itemSNCS["NroDonacion"];?></td>
                                            <td align="center"><?php echo $itemSNCS["GrupoSanguineoPostulante"];?></td>
                                            <td align="center"><?php echo vfecha(substr($itemSNCS["FechaExtraccion"],0,10));?></td>
                                            <td align="center"><?php echo vfecha(substr($itemSNCS["FechaTamizaje"],0,10));?></td>
                                            <td align="center"><?php echo $itemSNCS["NroTabuladora"];?></td>
                                            <td align="center"><?php echo $SNCSPG;?></td>
                                            <td align="center"><?php echo $SNCSPFC;?></td>
                                            <td align="center"><?php echo $SNCSPQ;?></td>
                                            <td align="center"><?php echo $Observacion;?></td>
                                            <td align="center"><?php echo $DesEstadoArchivado;?> <input name="DesEstadoArchivado" id="DesEstadoArchivado" type="hidden" value="<?php echo strip_tags($DesEstadoArchivado);?>"> <input name="TieneVerificacion" id="TieneVerificacion" type="hidden" value="<?php echo $TieneVerificacion;?>">  </td>                                       
                                                                                    
                                        </tr> 
                                        <?php 
											}
										 }else{ 
										?> 
                                   <input name="DesEstadoArchivado" id="DesEstadoArchivado" type="hidden" value="">  
                                   <input name="TieneVerificacion" id="TieneVerificacion" type="hidden" value="">   
                                        <?php 
											}									
										?>                                       
                                    </tbody>
                                    
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <!--<div class="well">
                                <h4>COMO REALIZAR BÚSQUEDAS DIRECTAS </h4>
                                <p>Búsqueda de pacientes por Nro. Historia clínica, DNI, Apellidos y Nombres </p><p>
<strong>Eje: Nro. Historia. 162578,  Eje: DNI. 43623262, Eje: Apellidos y Nombres. Ariza Bravo</strong>
</p>
                                
                            </div>-->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - alertify - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
    
    <!-- Fecha -->     
    <script src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>       
	

</body>

</html>
