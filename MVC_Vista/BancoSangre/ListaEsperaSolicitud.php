<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lista de Espera</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>            
      
        
   <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:60px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				/*margin-left:10px;*/
            }
			.fitem2 label{
                display:inline-block;
                width:120px;	
				margin-left:10px;			
            }
			.fitem2 input {	
				width:140px;		 
			}
			
			#dlg-VerRecepcion{
                margin:0;
                padding:10px 30px;
            }
			
			#dlg-VerRecepcion{
                margin:0;
                padding:10px 30px;
            }
			#dlg-VerDespacho{
                margin:0;
                padding:10px 30px;
            }
			
			.l-btn-icon {
				margin-top: -11px !important;
			}
	</style>       
    
    
<script type="text/javascript" >			
		
		$.extend($("#FechaRecepcion").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaRecepcion").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});					
		
		function RegistrarSolicitud(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegistrarSolicitud&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}	
		
		function Buscar(){
			document.getElementById("form1").submit();
		}		
		
		function IngresarFenotipo(){
			var rowp = $('#dg').datagrid('getSelected');
			if (rowp){	
				if(rowp.FechaRecepcion==''){
				   $.messager.alert('Mensaje','Falta Recepcionar la Solicitud','warning');
				   return 0; 
				   
				}else if(rowp.HayReservas=='1'){//Hay Reservas
				   $.messager.alert('Mensaje','No puede Modificar Fenotipo porque la Solicitud TIENE Unidades Reservadas','warning');
				   return 0; 
				   
				}else if( (rowp.IdOrdenFen).trim()=='' ){//Hay Reservas
				   $.messager.alert('Mensaje','No puede Registrar Fenotipo. Falta ingresar Nro Orden de Facturación Fenotipo','warning');
				   return 0; 
				   
				}else{
				var IdGrupoSanguineo=rowp.IdGrupoSanguineo;
				var IdPaciente=rowp.IdPaciente;	
				var IdSolicitudSangre=rowp.IdSolicitudSangre;		
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegFenotipoReceptor&IdGrupoSanguineo="+IdGrupoSanguineo+"&IdPaciente="+IdPaciente+"&IdSolicitudSangre="+IdSolicitudSangre+"&IdOrdenFen="+rowp.IdOrdenFen+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
				}	
			
			}else{
				$.messager.alert('Mensaje','Seleccione una Solicitud','warning');
				return 0;			
			}		   
	   }
	   
	   function Historial(){
		   var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){	
		   		if(rowp.TieneHistorialDona=='SI'){
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerHistorialDonaciones&IdPaciente="+rowp.IdPaciente+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
				}else{
					$.messager.alert('Mensaje','El paciente NO tiene Historial','warning');
					return 0;	
				}
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Paciente','warning');
				return 0;			
			}				
	   }
	   
	   function ImprimirSolicitud(){
		   var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){	
		   		if(rowp.Resumen=='<font color="#FF9900">SI</font>'){
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirSolicitud&IdSolicitudSangre="+rowp.IdSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
				}else{
					$.messager.alert('Mensaje','Falta Registrar el Resumen del Paciente','warning');
					return 0;	
				}
			
			}else{
				$.messager.alert('Mensaje','Seleccione una Solicitud','warning');
				return 0;			
			}			   
	   }
	   
	   function ImprimirBancoSangre(){//POR DETALLE
		   var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){	
		   		if(rowp.Estado=='4'){
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirSolicitudBancoSangre&IdSolicitudSangre="+rowp.IdSolicitudSangre+"&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
				}else{
					$.messager.alert('Mensaje','Falta Facturar Algún Hemocomponente del Detalle de la Solicitud Seleccionado','warning');
					return 0;	
				}
			
			}else{
				$.messager.alert('Mensaje','Seleccione una Solicitud','warning');
				return 0;			
			}		   
		   
		}
		
		function Eliminar(){
		   var rowp = $('#dg').datagrid('getSelected');
		   
		   if (rowp){   
		   
		   		 /*var CodigoComponente=rowp.CodigoComponente;			
				 var arrayCodigoComponente=CodigoComponente.split("-");		
				 var TipoHem=arrayCodigoComponente[0].trim();*/		
			
		   		if(rowp.Estado=='1' && rowp.HayReservas=='1'){ 
					$.messager.alert('Mensaje','NO puede Eliminar la Solicitud Nro: '+rowp.Codigo+' porque tiene Hemocomponentes Reservados','warning');
					return 0;		
					
				}else if(rowp.Estado=='2'){	//detalle solicitud Reservado
					$.messager.alert('Mensaje','NO puede Eliminar la Solicitud Nro: '+rowp.Codigo+' porque tiene Hemocomponentes Reservados','warning');
					return 0;	
					
				}else if(rowp.Estado=='3'){	//detalle solicitud Despachado
					$.messager.alert('Mensaje','NO puede Eliminar la Solicitud Nro: '+rowp.Codigo+' porque tiene Hemocomponentes Despachados','warning');
					return 0;	
					
				}else if(rowp.Estado=='4'){	//detalle solicitud Facturado
					$.messager.alert('Mensaje','NO puede Eliminar la Solicitud Nro: '+rowp.Codigo+' porque tiene Hemocomponentes Facturados','warning');
					return 0;	
					
				}else if(rowp.Estado=='5'){	//detalle solicitud Cerrado
					$.messager.alert('Mensaje','NO puede Eliminar la Solicitud Nro: '+rowp.Codigo+' porque está cerrado','warning');
					return 0;	
					
				}else if(rowp.Resumen=='<font color="#FF9900">SI</font>'){	
					//$.messager.alert('Mensaje','No se puede Eliminar porque tiene Resumen','warning');
					//return 0;
					$.messager.confirm('Mensaje', 'Tiene Resumen. ¿Seguro de Eliminar el Detalle de la Solicitud Nro: '+rowp.Codigo+'?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=EliminarDetSolicitud&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
						}
					});	
						
				}else{
					$.messager.confirm('Mensaje', '¿Seguro de Eliminar el Detalle de la Solicitud Nro: '+rowp.Codigo+'?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=EliminarDetSolicitud&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
						}
					});	
						
				}
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Paciente','warning');
				return 0;			
			}
			
			
		}
	   
	   
	   function Cerrar(){ //Una vez cerrado NO se modifica
		   var rowp = $('#dg').datagrid('getSelected');
		   if (rowp){   		
			
		   		if(rowp.Resumen=='<font color="#FF0000">FALTA</font>'){	
					$.messager.alert('Mensaje','Falta Registrar el Resumen del Paciente','warning');
					return 0;
						
				}else if(rowp.Estado=='1' && rowp.HayReservas=='0'){	
					$.messager.confirm('Mensaje', 'La Solicitud Nro: '+rowp.Codigo+' NO tiene Hemocomponentes. ¿Seguro de Cerrarla?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=CerrarDetSolicitud&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
						}
					});		
					
				}else if(rowp.Estado=='1' && rowp.HayReservas=='1'){	
					$.messager.alert('Mensaje','NO puede cerrar la Solicitud Nro: '+rowp.Codigo+' porque tiene Hemocomponentes Reservados','warning');
					return 0;		
					
				}else if(rowp.Estado=='2'){	//Reservado
					$.messager.alert('Mensaje','Falta Despachar el Detalle de la Solicitud Nro: '+rowp.Codigo,'warning');
					return 0;	
					
				}else if(rowp.Estado=='3'){	//Despachado
					$.messager.alert('Mensaje','Falta Facturar el Detalle de la Solicitud Nro: '+rowp.Codigo,'warning');
					return 0;	
					
				}else{
					$.messager.confirm('Mensaje', '¿Seguro de Cerrar el Detalle de la Solicitud Nro: '+rowp.Codigo+'?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=CerrarDetSolicitud&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
						}
					});	
						
				}
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Paciente','warning');
				return 0;			
			}
	   }		
	
    </script>
</head>
<body>
	<!--INICIO REGISTRAR RECEPCION -->
	<div id="dlg-Recepcion" class="easyui-dialog" style="width:500px;height:180px;"
			closed="true" buttons="#dlg-buttons"><!--buttons="#dlg-buttons"-->
		<!--<div class="ftitle">Datos del Equipo</div>-->
		<form id="fmRecepcion" name="fmRecepcion" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarRecepcionSolicitud&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
			   
		  <div class="fitem2">
				<input type="hidden" name="NroSolicitud" id="NroSolicitud">
				<input type="hidden" name="IdSolicitudSangre" id="IdSolicitudSangre">
				<label>Responsable:</label>
				<select style="width:245px" class="easyui-combobox" id="UsuRecepcion" name="UsuRecepcion" data-options="prompt:'Seleccione',required:true">
				  <option value=""></option>                      
				  <?php
					//$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
					//$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
								  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
								   if($listar != NULL) { 
									 foreach($listar as $item){?>
				   <option value="<?php echo $item["IdEmpleado"]?>" <?php if(trim($item["IdEmpleado"])==trim($_REQUEST['IdEmpleado'])){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
				   <?php } } ?>
				</select>
		  </div>              
		  <div class="fitem2">
				<label>Fecha Recepción:</label>  
				<input name="FechaRecepcion" id="FechaRecepcion" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:130px" data-options="required:true" />                   
				 
				<input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraRecepcion" name="HoraRecepcion" data-options="showSeconds:true,required:true" style="width:110px" />
				
		  </div>                 
				   
		</form>
    </div>
        
    <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveRecepcion();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-Recepcion').dialog('close')" style="width:90px">Cancelar</a>
	</div><!--FIN -->
    
	<!--INICIO VER RECEPCION -->
	<div id="dlg-VerRecepcion" class="easyui-dialog" style="width:500px;height:180px;"
			closed="true">           	          
                   
              <div class="fitem2">                	
                    <label>Responsable:</label>
                    <select style="width:245px" class="easyui-combobox" id="UsuRecepcion2" name="UsuRecepcion2" data-options="prompt:'Seleccione',required:true" disabled>
                      <option value=""></option>
                      <?php
					  	//$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
						//$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                      $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdEmpleado"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>
              </div>              
              <div class="fitem2">
               		<label>Fecha Recepción:</label>  
                    <input name="FechaRecepcion2" id="FechaRecepcion2" class="easyui-datebox" value="" validType="validDate" style="width:130px" data-options="required:true" disabled />                   
					 
                    <input class="easyui-timespinner" value="" id="HoraRecepcion2" name="HoraRecepcion2" data-options="showSeconds:true,required:true" style="width:110px" disabled />
                    
              </div>                 
                       
           
    </div><!--FIN-->    
    
    
    
    <!--INICIO REGISTRAR DESPACHO -->
	<div id="dlg-Despacho" class="easyui-dialog" style="width:500px;height:180px;"
			closed="true" buttons="#dlg-buttons"><!--buttons="#dlg-buttons"-->
		<!--<div class="ftitle">Datos del Equipo</div>-->
		<form id="fmDespacho" name="fmDespacho" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarDespachoSolicitud&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
			   
		  <div class="fitem2">
				<input type="hidden" name="NroSolicitud2" id="NroSolicitud2">
				<input type="hidden" name="IdDetSolicitudSangre" id="IdDetSolicitudSangre">
                <input type="hidden" name="IdSolicitudSangre2" id="IdSolicitudSangre2">
                <input type="hidden" name="TipoHem2" id="TipoHem2">
                <input type="hidden" name="SNCS2" id="SNCS2">
                <input type="hidden" name="IdMovDetalle2" id="IdMovDetalle2">
                
				<label>Responsable:</label>
				<select style="width:200px" class="easyui-combobox" id="UsuDespacho" name="UsuDespacho" data-options="prompt:'Seleccione',required:true">
				  <option value=""></option>                      
				  <?php
					//$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
					//$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
								  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
								   if($listar != NULL) { 
									 foreach($listar as $item){?>
				   <option value="<?php echo $item["IdEmpleado"]?>" <?php if(trim($item["IdEmpleado"])==trim($_REQUEST['IdEmpleado'])){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
				   <?php } } ?>
				</select>
		  </div>              
		  <div class="fitem2">
				<label>Fecha Despacho:</label>  
				<input name="FechaDespacho" id="FechaDespacho" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:110px" data-options="required:true" />                   
				 
				<input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraDespacho" name="HoraDespacho" data-options="showSeconds:true,required:true" style="width:85px" />
				
		  </div>                 
				   
		</form>
    </div>
        
    <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveDespacho();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-Despacho').dialog('close')" style="width:90px">Cancelar</a>
	</div><!--FIN -->   
	  
	<!--INICIO VER DESPACHO -->
	<div id="dlg-VerDespacho" class="easyui-dialog" style="width:500px;height:180px;"
			closed="true">           	          
                   
              <div class="fitem2">                	
                    <label>Responsable:</label>
                    <select style="width:200px" class="easyui-combobox" id="UsuDespacho2" name="UsuDespacho2" data-options="prompt:'Seleccione',required:true" disabled >
                      <option value=""></option>
                      <?php
					  	//$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
						//$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                      $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdEmpleado"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>
              </div>              
              <div class="fitem2">
               		<label>Fecha Despacho:</label>  
                    <input name="FechaDespacho2" id="FechaDespacho2" class="easyui-datebox" value="" validType="validDate" style="width:110px" data-options="required:true" disabled />                   
					 
                    <input class="easyui-timespinner" value="" id="HoraDespacho2" name="HoraDespacho2" data-options="showSeconds:true,required:true" style="width:85px" disabled />
                    
              </div>                 
                       
           
    </div><!--FIN-->
    
    
    <!--INICIO REGISTRAR ORDEN FENOTIPO -->
	<div id="dlg-OrdenFenotipo" class="easyui-dialog" style="width:550px;height:140px;"
			closed="true" buttons="#dlg-buttons"><!--buttons="#dlg-buttons"-->
		<!--<div class="ftitle">Datos del Equipo</div>-->
		<form id="fmOrdenFenotipo" name="fmOrdenFenotipo" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarOrdenFenotipo&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
		  <br>    
		  <div class="fitem2">
				<input type="hidden" name="NroSolicitud3" id="NroSolicitud3">			
                <input type="hidden" name="IdSolicitudSangre3" id="IdSolicitudSangre3">
                <input type="hidden" name="IdPaciente3" id="IdPaciente3">
                
				<label>Nro Orden:</label>
				<input type="text" class="easyui-numberbox" name="IdOrdenFen" id="IdOrdenFen" maxlength="9"  />
		  </div>          
		                
				   
		</form>
    </div>
        
    <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveOrdenFenotipo();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-OrdenFenotipo').dialog('close')" style="width:90px">Cancelar</a>
	</div><!--FIN -->  
    
    
     <!--INICIO VINCULAR ORDEN -->
	<div id="dlg-OrdenFacturacion" class="easyui-dialog" style="width:500px;height:140px;"
			closed="true" buttons="#dlg-buttons"><!--buttons="#dlg-buttons"-->
		<!--<div class="ftitle">Datos del Equipo</div>-->
		<form id="fmOrdenFacturacion" name="fmOrdenFacturacion" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarOrdenFacturacion&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
		  <br>    
		  <div class="fitem2">
				<input type="hidden" name="IdReserva" id="IdReserva" />
                <input type="hidden" name="IdPacienteVal" id="IdPacienteVal" />
				<label>Nro Orden:</label>
				<input type="text" class="easyui-numberbox" name="idOrdenFac" id="idOrdenFac" maxlength="9"  />
		  </div>          
		                
				   
		</form>
    </div>
        
    <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveOrdenFacturacion();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-OrdenFacturacion').dialog('close')" style="width:90px">Cancelar</a>
	</div><!--FIN -->       
    
    
    
	    <div id="tb1" style="padding:5px;height:auto">
        	<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="RegistrarSolicitud()">Registrar Solicitud y/o Resumen</a>     	           	               	 
			<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="IngresarFenotipo()">Ingresar Fenotipo</a>                                        
			
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="Historial();">Historial</a>
            
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="ImprimirSolicitud();">Imprimir Solicitud</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="ImprimirBancoSangre();">impresión por Banco de Sangre</a>	
            
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="Cerrar();">Cerrar</a>
            
             <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="Eliminar();">Eliminar</a>
			<!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a>-->   
			
            <div>
				<?php /*?><form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarSolicitudes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
				
					<strong> Hemocomponente:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
					<select name="TipoHemB" id="TipoHemB" class="easyui-combobox" data-options="required:true,
							valueField: 'id',
							textField: 'text',        
							onSelect: function(rec){
							var url = document.form1.submit(); }" style="width:60px;">
							<option value="T">Todos</option>
							<option value="PG">PG </option>
							<option value="PFC">PFC </option>
							<option value="PQ">PQ </option>
							<option value="CRIO">CRIO </option>
					</select>
					&nbsp;&nbsp;<strong>Fecha:</strong> &nbsp;
					<span class="fitem">
					<input name="FechaSolicitudB" id="FechaSolicitudB" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />
					</span>                          
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>
				
				</form> <?php */?> 
            </div>
            			
		</div>
        
                
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Lista de Espera de las Solicitudes" style="width:100%;height:90%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:20">         
                
		<thead>
			<tr>
                <th field="FechaSolicitud" width="84">Fec.Solicitud</th>
                <th field="UsuarioReg" width="45">UsuarioReg</th>				
                <th field="Codigo" width="63">Código</th>	
                <th field="Cantidad"  width="40">Cantidad</th>	
                <th field="TipoRequerimiento" width="50">Contingencia</th>
                <th field="Transcurrido" width="65">Transcurrido</th>
                <th field="Resumen" width="42">Resumen</th>
                <th field="NroHistoria" width="50">NroHistoria</th>
                <th field="NombresPaciente" width="125">Paciente</th>
                <th field="Servicio" width="88">Servicio</th>
                <th field="CodigoCama" width="28">Cama</th>
                <th field="GrupoSanguineo" width="32">GS</th>
                <th data-options="field:'FechaRecepcion',formatter:RegFechaRecepcion" align="center" width="42">Recepción</th>
                <th field="UltFenotipo" width="57">Ult.Fenotipo</th>  
                <th data-options="field:'IdOrdenFen',formatter:RegIdOrdenFen" align="center" width="47">OrdenFen.</th> 
                <th field="Fenotipo" width="55">Fenotipo</th>                  
                <th data-options="field:'UnidadCompatible',formatter:BuscarUnidadCompatible" align="center" width="55">Reservar</th>              
                <th data-options="field:'FechaDespacho',formatter:RegFechaDespacho" align="center" width="55">Despacho</th>
                <th data-options="field:'Facturacion',formatter:RegFacturacion" align="center" width="55">Facturacion</th>
			</tr>
		</thead>
	</table> 
     
   <script> 
   
   $(function(){  	
	   $('#IdOrdenFen').numberbox('textbox').attr('maxlength', $('#IdOrdenFen').attr("maxlength"));
	   $('#idOrdenFac').numberbox('textbox').attr('maxlength', $('#idOrdenFac').attr("maxlength"));
   });
   
   function RegIdOrdenFen(val,row,index){       
	  if(val.trim()=='' && row.Fenotipo.trim()==''){
		   return "<a onclick='RegInOrdenFenotipo("+row.IdSolicitudSangre+","+row.NroSolicitud+","+row.IdPaciente+","+index+");' class='easyui-linkbutton' iconCls='icon-add' plain='true'></a>"; 
	  }else{	  
		  return val;
	  }    
   }
   
   function RegInOrdenFenotipo(IdSolicitudSangre,NroSolicitud,IdPaciente,index){       
	    var NombresPaciente = $('#dg').datagrid('getRows')[index].NombresPaciente;			 	
		$('#dlg-OrdenFenotipo').dialog('open').dialog('setTitle','Orden para la Prueba Fenotipo del Paciente: '+NombresPaciente);			
		document.getElementById('IdSolicitudSangre3').value=IdSolicitudSangre;
		document.getElementById('NroSolicitud3').value=NroSolicitud;	
		document.getElementById('IdPaciente3').value=IdPaciente;		
		$('#IdOrdenFen').numberbox('setValue', ''); 		
   }
   
   function saveOrdenFenotipo(){       
	   		var IdOrdenFen=$('#IdOrdenFen').numberbox('getValue');
			 if(IdOrdenFen.trim()=="" || IdOrdenFen.trim()=="0"){
				$.messager.alert('Mensaje','Ingrese un Nro de Orden','info');
				$('#IdOrdenFen').next().find('input').focus();
				return 0;			
			}		
			
			var NroSolicitud3=document.getElementById('NroSolicitud3').value;
			$.messager.confirm('Mensaje', '¿Seguro de Guardar el Nro de Orden '+ IdOrdenFen +' a la Solicitud N°'+ NroSolicitud3 +'?', function(r){
				if (r){
					document.getElementById("fmOrdenFenotipo").submit();
				}
			});	
   }
   
   //VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
	$.extend($.fn.validatebox.defaults.rules,{
		exists:{
			validator:function(value,param){
				var cc = $(param[0]);
				var v = cc.combobox('getValue');
				var rows = cc.combobox('getData');
				for(var i=0; i<rows.length; i++){
					if (rows[i].id == v){return true}
				}
				return false;
			},
			message:'El valor ingresado no existe.'
		}
	});	
	
	$(function(){	
		/*$('#IdGrupoSanguineoRec').combobox({	
		editable: false,
		required: true								
		});	*/
		$('#UsuRecepcion').combobox({	
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#UsuRecepcion"]',
			filter: function (q, row) {
			return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
			}								
		});						
		$('#UsuRecepcion').combobox('validate');
		
		$('#UsuDespacho').combobox({	
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#UsuDespacho"]',
			filter: function (q, row) {
			return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
			}								
		});						
		$('#UsuDespacho').combobox('validate');
		
			
	});
   
   $("#dg").datagrid({
		// Fires when data in datagrid is loaded successfully
		onLoadSuccess:function(){	
			// Get this datagrid's panel object
			$(this).datagrid('getPanel')
			// for all easyui-linkbutton <a>'s make them a linkbutton
			.find('a.easyui-linkbutton').linkbutton();
		}
    });
   
   //Fecha Recepcion
	function RegFechaRecepcion(val,row,index){
      //var url = "print.php?id="+ row.NroDonacion;	 	  
	  //return '<a href="'+url +'" class="easyui-linkbutton" iconCls="icon-add"></a>';	  
	  if(val.trim()==''){
		   return "<a onclick='RegFR("+row.IdSolicitudSangre+","+row.NroSolicitud+","+row.IdPaciente+","+index+");' class='easyui-linkbutton' iconCls='icon-add' plain='true'></a>"; 
	  }else{	  
		  return '<a onclick=\'VerFR("'+row.FechaRecepcion+'","'+row.HoraRecepcion+'","'+row.UsuRecepcion+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true">Ver</a>';
	  } 
	  
    }
	
	function RegFR(IdSolicitudSangre,NroSolicitud,IdPaciente,index){ 	
		var NombresPaciente = $('#dg').datagrid('getRows')[index].NombresPaciente;			 	
		$('#dlg-Recepcion').dialog('open').dialog('setTitle','Recepcionar Solicitud del Paciente: '+NombresPaciente);			
		document.getElementById('IdSolicitudSangre').value=IdSolicitudSangre;
		document.getElementById('NroSolicitud').value=NroSolicitud;	
		//$('#FechaRecepcion').datebox('setValue', ''); 
		//$('#HoraRecepcion').timespinner('setValue', ''); 
		//$('#UsuRecepcion').combobox('setValue', ''); 							
	}
	
	function VerFR(FechaRecepcion,HoraRecepcion,UsuRecepcion,index){ 	
		var NombresPaciente = $('#dg').datagrid('getRows')[index].NombresPaciente;			 	
		$('#dlg-VerRecepcion').dialog('open').dialog('setTitle','Ver Recepción Solicitud del Paciente: '+NombresPaciente);		
		$('#FechaRecepcion2').datebox('setValue', FechaRecepcion); 
		$('#HoraRecepcion2').timespinner('setValue', HoraRecepcion); 
		$('#UsuRecepcion2').combobox('setValue', UsuRecepcion); 						
	}
	
	//Unidad Compatible
	function BuscarUnidadCompatible(val,row,index){		  
	  if(row.FechaRecepcion==''){
		return '';
	  }else if(row.Estado!='1'){//=='2' TODA LA CANTIDAD RESERVADA
		  return '<a onclick=\'VerUnidadesReservadas("'+row.Fenotipo+'","'+row.IdGrupoSanguineo+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true">Ver</a>';
		  
	  }else if(row.HayReservas=='1'){//Hay Reservas
		  return '<a onclick=\'RegUnidadCompatible("'+row.Fenotipo+'","'+row.IdGrupoSanguineo+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-add" plain="true"></a><a onclick=\'VerUnidadesReservadas("'+row.Fenotipo+'","'+row.IdGrupoSanguineo+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true">Ver</a>';
		  
	  }else{  //NO HAY RESERVAS	  
      	  return '<a onclick=\'RegUnidadCompatible("'+row.Fenotipo+'","'+row.IdGrupoSanguineo+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-add" plain="true">Buscar</a>'; 	
	  }
	    
    }
	
	function VerUnidadesReservadas(Fenotipo,IdGrupoSanguineo,index){ 				
			
			var CodigoComponente=$('#dg').datagrid('getRows')[index].CodigoComponente;			
			var arrayCodigoComponente=CodigoComponente.split("-");		
			var TipoHem=arrayCodigoComponente[0].trim();		
			
			var IdDetSolicitudSangre = $('#dg').datagrid('getRows')[index].IdDetSolicitudSangre;
			
			var url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerUnidadesReservadas&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>&IdDetSolicitudSangre="+IdDetSolicitudSangre;
							
			$('#dlg-UnidadesReservadas').dialog('open').dialog('setTitle',TipoHem+' Reservadas:');				
			$('#dg_UnidadesReser').datagrid('load',url); 		
	 }
	 
	 function QuitarReserva(){
		var rowp = $('#dg_UnidadesReser').datagrid('getSelected');
		if (rowp){	
			if((rowp.IdOrden).trim()!='0'){
			   $.messager.alert('Mensaje','El hemocomponente está Facturado','warning');
			   return 0; 
			   
			}if((rowp.Estado).trim()=='3'){
			   /*$.messager.alert('Mensaje','El hemocomponente está Despachado','warning');
			   return 0; */
			   
			   var TSNCS=rowp.TipoHem+"-"+rowp.SNCS;
			   $.messager.confirm('Mensaje', 'El hemocomponente está Despachado. ¿Seguro de Quitar el despacho y la Reserva del Hemocomponente: '+TSNCS+'?', function(r){
					if (r){						
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=QuitarReserva&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdMovDetalle="+rowp.IdMovDetalle+"&IdReserva="+rowp.IdReserva+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
					}
				});
			   
			}else{
				var TSNCS=rowp.TipoHem+"-"+rowp.SNCS;
				$.messager.confirm('Mensaje', '¿Seguro de Quitar la Reserva del Hemocomponente: '+TSNCS+'?', function(r){
					if (r){					
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=QuitarReserva&IdDetSolicitudSangre="+rowp.IdDetSolicitudSangre+"&IdMovDetalle="+rowp.IdMovDetalle+"&IdReserva="+rowp.IdReserva+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
					}
				});	
			  	
			}	  
		
		}else{
			$.messager.alert('Mensaje','Seleccione una Reserva','warning');
			return 0;			
		}	
		 
	 }
	 
	function RegUnidadCompatible(Fenotipo,IdGrupoSanguineo,index){ 	
		if(IdGrupoSanguineo==0){
			 $.messager.alert('Mensaje','Falta Grupo Sanguineo. Registrarlo en Ingresar Fenotipo','warning');
			 
		}else if(IdGrupoSanguineo>=5 && Fenotipo==''){
			 $.messager.alert('Mensaje','Para Rh(-) es Obligatorio ingresar el Fenotipo','warning');
			 
		}else{
			var GrupoSanguineo = $('#dg').datagrid('getRows')[index].GrupoSanguineo;
			var Fenotipo = $('#dg').datagrid('getRows')[index].Fenotipo;	
			
			var CodigoComponente=$('#dg').datagrid('getRows')[index].CodigoComponente;			
			var arrayCodigoComponente=CodigoComponente.split("-");		
			var TipoHem=arrayCodigoComponente[0].trim();
			
			var DeteccionCI = $('#dg').datagrid('getRows')[index].DeteccionCI;
			var CoombsDirecto = $('#dg').datagrid('getRows')[index].CoombsDirecto;
			var IdPaciente = $('#dg').datagrid('getRows')[index].IdPaciente;
			
			var IdDetSolicitudSangre = $('#dg').datagrid('getRows')[index].IdDetSolicitudSangre;
			
			var VolumenPediatrico = $('#dg').datagrid('getRows')[index].VolumenPediatrico;
			
			var url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerHemocomponentesDisponibles&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>&TipoHem="+TipoHem+"&IdGrupoSanguineo="+IdGrupoSanguineo+"&DeteccionCI="+DeteccionCI+"&CoombsDirecto="+CoombsDirecto+"&IdPaciente="+IdPaciente+"&IdDetSolicitudSangre="+IdDetSolicitudSangre+"&VolumenPediatrico="+VolumenPediatrico;
							
			$('#dlg-UnidadesCompatibles').dialog('open').dialog('setTitle',TipoHem+' Compatibles para Tipo Sangre:'+GrupoSanguineo);//+' y Fenotipo:'+Fenotipo				
			$('#dg_UnidadesDispo').datagrid('load',url); 
		}
	 }
		
	//Fecha Despacho
	function RegFechaDespacho(val,row,index){ 	 	
	  /*if(row.Estado=='1'){ //FALTA RESERVAR ALGUN HEMOCOMPONENTE
		 return '';		 
	  }*/ 
	    if(row.HayReservas!='1'){ //NO HayReservas
		 return '';
		 
	    }else if(row.Estado=='2' || row.Estado=='1'){ //Reservado  //if(row.FechaDespacho==''){
		 return '<a onclick=\'RegistrarFechaDespacho('+row.IdSolicitudSangre+','+row.IdDetSolicitudSangre+','+row.NroSolicitud+','+index+');\' class="easyui-linkbutton" iconCls="icon-add" plain="true"></a>';
		 
	  }else{ 
	   //return FechaDespacho;	   
		 return '<a onclick=\'VerFD("'+row.FechaDespacho+'","'+row.HoraDespacho+'","'+row.UsuDespacho+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true">Ver</a><a onclick=\'Imprimir("'+row.IdDetSolicitudSangre+'","'+row.IdPaciente+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-print" plain="true"></a>'; 
       	
	  }
    }
	
	function Imprimir(IdDetSolicitudSangre,IdPaciente,index){			 	
		 location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ImprimirEtiquetaTransfusiones&IdDetSolicitudSangre="+IdDetSolicitudSangre+"&IdPaciente="+IdPaciente+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
	}
	
	function RegistrarFechaDespacho(IdSolicitudSangre,IdDetSolicitudSangre,NroSolicitud,index){
		
		var NombresPaciente = $('#dg').datagrid('getRows')[index].NombresPaciente;	
		
		var CodigoComponente=$('#dg').datagrid('getRows')[index].CodigoComponente;			
		var arrayCodigoComponente=CodigoComponente.split("-");		
		var TipoHem=arrayCodigoComponente[0].trim();			
		
		document.getElementById('IdSolicitudSangre2').value=IdSolicitudSangre;				
		document.getElementById('IdDetSolicitudSangre').value=IdDetSolicitudSangre;
		document.getElementById('NroSolicitud2').value=NroSolicitud;
		
		var TipoHemTodos=$('#dg').datagrid('getRows')[index].TipoHemTodos;	
		document.getElementById('TipoHem2').value=TipoHemTodos;
		var SNCSTodos=$('#dg').datagrid('getRows')[index].SNCSTodos;	
		document.getElementById('SNCS2').value=SNCSTodos;	
		
		var IdMovDetalleTodos=$('#dg').datagrid('getRows')[index].IdMovDetalleTodos;
		document.getElementById('IdMovDetalle2').value=IdMovDetalleTodos;			
		
		//
		var Estado = $('#dg').datagrid('getRows')[index].Estado;		
		if(Estado=='1'){
				$.messager.confirm('Mensaje', 'Falta Reservar Hemocomponentes. ¿Seguro de Registrar el Despacho?', function(r){
					if (r){					
						$('#dlg-Despacho').dialog('open').dialog('setTitle','Despacho de '+TipoHem+' Reservados');
					}
				});	
		}else{
			$('#dlg-Despacho').dialog('open').dialog('setTitle','Despacho de '+TipoHem+' Reservados');
		}
		
	}
	
	function VerFD(FechaDespacho,HoraDespacho,UsuDespacho,index){
		var NombresPaciente = $('#dg').datagrid('getRows')[index].NombresPaciente;			 	
		$('#dlg-VerDespacho').dialog('open').dialog('setTitle','Ver Despacho al Paciente: '+NombresPaciente);		
		$('#FechaDespacho2').datebox('setValue', FechaDespacho); 
		$('#HoraDespacho2').timespinner('setValue', HoraDespacho); 
		$('#UsuDespacho2').combobox('setValue', UsuDespacho); 
	}
	
	//Facturacion
	function RegFacturacion(val,row,index){
      
	 var CodigoComponente=row.CodigoComponente;			
	 var arrayCodigoComponente=CodigoComponente.split("-");		
	 var TipoHem=arrayCodigoComponente[0].trim();
		
	 var FechaDespacho=row.FechaDespacho; 	  
	
		if(FechaDespacho.trim()!=''){  
		  if(row.Estado=='4'){ //Todos los detalles Facturados 
			  return '<a onclick=\'VerFacturacion("'+row.FechaDespacho+'","'+row.HoraDespacho+'",'+row.IdPaciente+','+row.IdCuentaAtencion+','+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true"></a>';			  
			  	
		  }else if(row.TieneFac=='No'){
			 return '<a onclick=\'RegistrarFacturacion("'+row.FechaDespacho+'","'+row.HoraDespacho+'",'+row.IdPaciente+','+row.IdCuentaAtencion+','+row.NroSolicitud+',"'+TipoHem+'","'+row.HayReservas+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-add" plain="true"></a>';
			  
		  }else if(row.TieneFac=='PendAlgunos'){
			  return '<a onclick=\'RegistrarFacturacion("'+row.FechaDespacho+'","'+row.HoraDespacho+'",'+row.IdPaciente+','+row.IdCuentaAtencion+','+row.NroSolicitud+',"'+TipoHem+'","'+row.HayReservas+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-add" plain="true"></a><a onclick=\'VerFacturacion("'+row.FechaDespacho+'","'+row.HoraDespacho+'",'+row.IdPaciente+','+row.IdCuentaAtencion+','+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true"></a>';	
			  
		  }
		}
		
	}
	
	function RegistrarFacturacion(FechaDespacho,HoraDespacho,IdPaciente,IdCuentaAtencion,NroSolicitud,TipoHem,HayReservas,index){
		if(HayReservas=='0'){//Hay Reservas
			return $.messager.alert('Mensaje','Falta Buscar la Unidad Compatible','warning');	
				 
		}else{			
			var IdDetSolicitudSangre = $('#dg').datagrid('getRows')[index].IdDetSolicitudSangre;			
			var url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerUnidadesDespachadas&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>&IdDetSolicitudSangre="+IdDetSolicitudSangre;							
			$('#dlg-UnidadesDespachadas').dialog('open').dialog('setTitle',TipoHem+' Despachadas:');				
			$('#dg_UnidadesDespachadas').datagrid('load',url); 	
		}			
	}
	
	function Facturar(){
		var rowp = $('#dg_UnidadesDespachadas').datagrid('getSelected');
			if (rowp){	
				if((rowp.IdOrden).trim()!='0'){
				   $.messager.alert('Mensaje','El hemocomponente YA está Facturado','warning');
				   return 0; 
				   
				}else{
				  location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FacturacionCargarCatalogos&FechaDespacho="+rowp.FechaDespacho+"&HoraDespacho="+rowp.HoraDespacho+"&IdPaciente="+rowp.IdPaciente+"&IdCuentaAtencion="+rowp.IdCuentaAtencion+"&TipoHem="+rowp.TipoHem+"&SNCS="+rowp.SNCS+"&IdReserva="+rowp.IdReserva+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
				}	  
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente Despachado','warning');
				return 0;			
			}				
		
	}
	
	function YaFacturado(){
		var rowp = $('#dg_UnidadesDespachadas').datagrid('getSelected');
			if (rowp){	
				if((rowp.IdOrden).trim()!='0'){
				   $.messager.alert('Mensaje','El hemocomponente YA está Facturado','warning');
				   return 0; 
				   
				}else{				
					document.getElementById('IdReserva').value=rowp.IdReserva; 
					document.getElementById('IdPacienteVal').value=rowp.IdPaciente; 
				 	$('#dlg-OrdenFacturacion').dialog('open').dialog('setTitle','Nro Orden Facturación de '+rowp.TipoHem+"-"+rowp.SNCS);	
				}	  
			
			}else{
				$.messager.alert('Mensaje','Seleccione un Hemocomponente Despachado','warning');
				return 0;			
			}
		
	}
	
	function saveOrdenFacturacion(){
		    var idOrdenFac=$('#idOrdenFac').numberbox('getValue');
			 if(idOrdenFac.trim()=="" || idOrdenFac.trim()=="0"){
				$.messager.alert('Mensaje','Ingrese un Nro de Orden','info');
				$('#idOrdenFac').next().find('input').focus();
				return 0;			
			}		
			
			$.messager.confirm('Mensaje', '¿Seguro de Facturar la Solicitud con el Nro de Orden '+ idOrdenFac +'?', function(r){
				if (r){
					document.getElementById("fmOrdenFacturacion").submit();
				}
			});			 
	 }	
	 
	
	function VerFacturacion(FechaDespacho,HoraDespacho,IdPaciente,IdCuentaAtencion,index){
			var IdDetSolicitudSangre = $('#dg').datagrid('getRows')[index].IdDetSolicitudSangre;			 
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerFacturacion&FechaDespacho="+FechaDespacho+"&HoraDespacho="+HoraDespacho+"&IdPaciente="+IdPaciente+"&IdCuentaAtencion="+IdCuentaAtencion+"&IdDetSolicitudSangre="+IdDetSolicitudSangre+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";						
	}
   	
	function saveRecepcion(){		
			
			var UsuRecepcion=$('#UsuRecepcion').combobox('getText');
			 if(UsuRecepcion.trim()=="" || $('#UsuRecepcion').combobox('isValid')==false){
				$.messager.alert('Mensaje','Seleccione el Usuario que Recepciona','info');
				$('#UsuRecepcion').next().find('input').focus();
				return 0;			
			}
			
			var FechaRecepcion=$('#FechaRecepcion').datebox('getText');
			 if(FechaRecepcion.trim()=="" || $('#FechaRecepcion').datebox('isValid')==false){
				$.messager.alert('Mensaje','Seleccione la Fecha de Recepción','info');
				$('#FechaRecepcion').next().find('input').focus();
				return 0;			
			}
			
			var HoraRecepcion=$('#HoraRecepcion').timespinner('getText');
			 if(HoraRecepcion.trim()==""){
				$.messager.alert('Mensaje','Ingrese la Hora de Recepción','info');
				$('#HoraRecepcion').next().find('input').focus();
				return 0;			
			}
			
			var NroSolicitud=document.getElementById('NroSolicitud').value;
			
			$.messager.confirm('Mensaje', '¿Seguro de Recepcionar la Solicitud Nro: '+ NroSolicitud +'?', function(r){
				if (r){
					document.getElementById("fmRecepcion").submit();
				}
			});	
			
		}
		
	 
	 function saveDespacho(){
		 var UsuDespacho=$('#UsuDespacho').combobox('getText');
			 if(UsuDespacho.trim()=="" || $('#UsuDespacho').combobox('isValid')==false){
				$.messager.alert('Mensaje','Seleccione el Usuario que Despacho','info');
				$('#UsuDespacho').next().find('input').focus();
				return 0;			
			}
			
			var FechaDespacho=$('#FechaDespacho').datebox('getText');
			 if(FechaDespacho.trim()=="" || $('#FechaDespacho').datebox('isValid')==false){
				$.messager.alert('Mensaje','Seleccione la Fecha de Despacho','info');
				$('#FechaDespacho').next().find('input').focus();
				return 0;			
			}
			
			var HoraDespacho=$('#HoraDespacho').timespinner('getText');
			 if(HoraDespacho.trim()==""){
				$.messager.alert('Mensaje','Ingrese la Hora de Despacho','info');
				$('#HoraDespacho').next().find('input').focus();
				return 0;			
			}
			
			var NroSolicitud=document.getElementById('NroSolicitud2').value;
			
			$.messager.confirm('Mensaje', '¿Seguro de Despachar la Solicitud Nro '+ NroSolicitud +'?', function(r){
				if (r){
					document.getElementById("fmDespacho").submit();
				}
			});	
		 
	 }	
	  
		function getData(){
			var rows = [];			
			
	<?php 		
	 
	 $BuscarSNCSTodos = ListarSolicitudes_M(0, 'T');
	 
     $i=1; $j=0;											
	if($BuscarSNCSTodos!=NULL){
		$TipoHemTodos=''; $SNCSTodos=''; $IdMovDetalleTodos='';
		$Resumen=''; 
		foreach($BuscarSNCSTodos as $item)
		{			
			$Estado=$item["Estado"];
			$EstadoDescripcion=DescripcionEstadoSolicitud($Estado);	
			
			$IdDiagnostico1=$item["IdDiagnostico1"]; //Los que tienen al menos diagnostico1 tienen RESUMEN
			if(trim($IdDiagnostico1)==''){
				$Resumen='<font color="#FF0000">FALTA</font>';				
			}else{
				$Resumen='<font color="#FF9900">SI</font>';				
			}	
			
			//Cabecera			
			$Paciente=SIGESA_BSD_Buscarpaciente_M($item["IdPaciente"],'IdPaciente');	
			$NombresPaciente=$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"];
			
			$Servicio=$item["Servicio"];
			$CodigoCama=$item["CodigoCama"];
			$GrupoSanguineo=$item["GrupoSanguineo"];
			
			//$FechaSolicitud=$item["FechaSolicitud"];
			$FechaSolicitud=vfecha(substr($item["FechaSolicitud"],0,10));
			$HoraSolicitud=substr($item["FechaSolicitud"],11,5);
			
			$FechaRecepcion=$item["FechaRecepcion"];	
			if($FechaRecepcion!=NULL){
				$FechaRecepcion=vfecha(substr($item["FechaRecepcion"],0,10));
				$HoraRecepcion=substr($item["FechaRecepcion"],11,8);
				$UsuRecepcion=$item["UsuRecepcion"];
			}else{
				$FechaRecepcion='';
				$HoraRecepcion='';
				$UsuRecepcion='';
			}
			
			$FechaDespacho=$item["FechaDespacho"];	
			if($FechaDespacho!=NULL){
				$FechaDespacho=vfecha(substr($item["FechaDespacho"],0,10));
				$HoraDespacho=substr($item["FechaDespacho"],11,8);
				$UsuDespacho=$item["UsuDespacho"];
			}else{
				$FechaDespacho='';
				$HoraDespacho='';
				$UsuDespacho='';
			}
			
			$Codigo=$item["NroSolicitud"].'-'.$item["CodigoComponente"];	
						
			//Obtener Cantidad
			if($item["Volumen"]>0){
				$Cantidad=$item["Cantidad"].'-'.$item["Volumen"].'ml';
			}else{
				$Cantidad=$item["Cantidad"];	
			}
			
			//Tiempo Transcurrido			
			$datetime1 = new DateTime($item['FechaSolicitud']);
			$datetime2 = new DateTime(date('Y-m-d H:i:s'));
			$interval = date_diff($datetime1, $datetime2);
			//$interval->format('%R%a días');
			if($interval->format('%a')=='0'){
				$Transcurrido=$interval->format('<font color="#FF0000">%R%h</font>H, <font color="#FF0000">%i</font>M');
			}else{
				$Transcurrido=$interval->format('<font color="#FF0000">%R%a</font>D, <font color="#FF0000">%h</font>H, <font color="#FF0000">%i</font>M');//%R
			}
			
			$VerReservas=VerSolicitudSangreDetalleReservaM($item["IdDetSolicitudSangre"]);
			if($VerReservas!=NULL){
				$HayReservas=1;	
				//
				$TipoHemTodos=''; $SNCSTodos=''; $IdMovDetalleTodos='';
				for ($k=0; $k < count($VerReservas); $k++) {	
					$TipoHemTodos=$TipoHemTodos.$VerReservas[$k]["TipoHem"].'|';
					$SNCSTodos=$SNCSTodos.$VerReservas[$k]["SNCS"].'|';
					$IdMovDetalleTodos=$IdMovDetalleTodos.$VerReservas[$k]["IdMovDetalle"].'|';
				}
			}else{
				$HayReservas=0;	
			}
			
			$HistorialDonaciones=HistorialDonacionesM($item["IdPaciente"]); 
			if($HistorialDonaciones!=NULL){	
				$TieneHistorialDona='SI';
			}else{	
				$TieneHistorialDona='NO';	
			}	
			
			$listado=VerFacturacionesBancoDeSangreM($item['IdDetSolicitudSangre']);
			$CantidadFacturados=count($listado);//CANTIDAD REGISTRO
			if($listado!=''){				
				if($item["Estado"]=='4'){//if($CantidadFacturados>=$Cantidad){ //> en el caso ST
					$TieneFac='Todos';
				}else{	
					$TieneFac='PendAlgunos';	
				}
			}else{	
				$TieneFac='No';	
			}	
			
			//INICIO Ver Unidades Disponibles sin filtro
			$CodigoComArreglo=explode("-", $item["CodigoComponente"]);
			$CodigoCom=$CodigoComArreglo[0];			
			if($CodigoCom=="GRlav"){
				$CodigoCom='PG';
			}
			if($CodigoCom=="PQAF"){
				$CodigoCom='PQ';
			}
			
			$VolumenPediatricoSol=$item["Volumen"];
			if($CodigoComArreglo[0]=="PQAF"){ // PLAQUETAS POR AFERESIS
				$filtro="  or (d.Estado=3 and TipoHem=''PQ'') ";		
			}else{
				$filtro="";
			}
			$ListarHem=ListarHemDisponiblesM($CodigoCom,$VolumenPediatricoSol,$filtro);
					
			if($ListarHem!=''){ //HAY HEMO
				$HemDispo=1;
			}else{
				$HemDispo=0;
			}
			
			//FIN Ver Unidades Disponibles sin filtro
			
			//Obtener Ultimo Fenotipo Paciente de la Tabla SIGESA_BSD_SolicitudSangreCabecera
			$ObtenerUltFenotipoPacS=ObtenerUltFenotipoPacienteSolicitud($item["IdPaciente"], $item["IdSolicitudSangre"]);
			$UltFenotipo=$ObtenerUltFenotipoPacS[0]["Fenotipo"];
			
			?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					FechaSolicitud: '<?php echo $FechaSolicitud.' '.$HoraSolicitud;?>',					
					UsuarioReg: '<?php echo $item["UsuarioReg"]; ?>',											
					Codigo:'<?php echo  $Codigo; ?>',	
					Cantidad:'<?php echo  $Cantidad; ?>',
					TipoRequerimiento: '<?php echo $item["TipoRequerimiento"]; ?>',	
					Resumen:'<?php echo  $Resumen; ?>',	
					NombresPaciente:'<?php echo  $NombresPaciente; ?>',
					Servicio:'<?php echo  $Servicio; ?>',	
					CodigoCama:'<?php echo  $CodigoCama; ?>',
					IdGrupoSanguineo: '<?php echo $item["IdGrupoSanguineo"]; ?>',				
					GrupoSanguineo:'<?php echo  $GrupoSanguineo; ?>',
					NroHistoria:'<?php echo  $item["NroHistoria"]; ?>',
					IdPaciente:'<?php echo  $item["IdPaciente"]; ?>',
					Transcurrido:'<?php echo  $Transcurrido; ?>',
					
					FechaRecepcion:'<?php echo $FechaRecepcion; ?>',
					HoraRecepcion:'<?php echo $HoraRecepcion; ?>',
					UsuRecepcion:'<?php echo $UsuRecepcion; ?>',					
					IdSolicitudSangre:'<?php echo  $item["IdSolicitudSangre"]; ?>',
					NroSolicitud:'<?php echo  $item["NroSolicitud"]; ?>',
					Fenotipo:'<?php echo  $item["Fenotipo"]; ?>',
					CodigoComponente:'<?php echo  $item["CodigoComponente"]; ?>',
					DeteccionCI:'<?php echo  $item["DeteccionCI"]; ?>',
					CoombsDirecto:'<?php echo  $item["CoombsDirecto"]; ?>',
					IdDetSolicitudSangre:'<?php echo  $item["IdDetSolicitudSangre"]; ?>',
					
					FechaDespacho:'<?php echo  $FechaDespacho; ?>',
					HoraDespacho:'<?php echo  $HoraDespacho; ?>',
					UsuDespacho:'<?php echo  $UsuDespacho; ?>',
					IdCuentaAtencion:'<?php echo  $item["IdCuentaAtencion"]; ?>',
					Estado:'<?php echo  $item["Estado"]; ?>',
					HayReservas:'<?php echo  $HayReservas; ?>',
					
					TipoHemTodos:'<?php echo $TipoHemTodos; ?>',
					SNCSTodos:'<?php echo $SNCSTodos; ?>',
					IdMovDetalleTodos:'<?php echo $IdMovDetalleTodos; ?>',
					TieneHistorialDona:'<?php echo $TieneHistorialDona; ?>',
					TieneFac:'<?php echo $TieneFac; ?>',
					HemDispo:'<?php echo $HemDispo; ?>',
					EstadoCab:'<?php echo  $item["EstadoCab"]; ?>',
					VolumenPediatrico:'<?php echo  $VolumenPediatricoSol; ?>',
					UltFenotipo:'<?php echo  $UltFenotipo; ?>',
					IdOrdenFen:'<?php echo  $item["IdOrdenFen"]; ?>',
										
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
				
		
		function ObtenerFechaActual(){
			f=new Date();
			var y = parseInt(f.getFullYear());
			var m = parseInt(f.getMonth()+1);
			var d = parseInt(f.getDate());
			if(m<10){
				m='0'+m;
			}
			if(d<10){
				d='0'+d;
			}						
			FechaActual=d+'/'+m+'/'+y;
				
			return FechaActual;
		}
			
		$(function(){
		var dgFrac = $('#dg').datagrid({
			remoteFilter: false,
			pagination: true,
			pageSize: 20,
			//pageList: [10,20,50,100]
		});
		
		dgFrac.datagrid('enableFilter');
		//FilterFechaActualSolicitud();												
		dgFrac.datagrid('loadData', getData());	
		
		function FilterFechaActualSolicitud(){
			FechaActual=ObtenerFechaActual();
								
			dgFrac.datagrid('addFilterRule', {
				field: 'FechaSolicitud',
				type:'datebox',
				op: 'contains',
				value: FechaActual
			});
		}//fin function FilterFechaActualSolicitud
		
		});
		
		
		$('#dg').datagrid({
			rowStyler:function(index,row){
				if (row.Estado==2){
					//return 'background-color:pink;color:blue;font-weight:bold;';
					return 'background-color:#42E4F7;font-weight:bold;';
				}
				if (row.HemDispo==0 && row.Estado==1){				
					return 'background-color:pink;';
				}
			}
		});

		</script> 
     
    <div id="dlg-UnidadesCompatibles" class="easyui-dialog" style="width:845px;height:auto;"
			closed="true"   buttons="#dlg-buttonsx">
      
       <table  class="easyui-datagrid" toolbar="#tb_UnidadesDispo" id="dg_UnidadesDispo" title="Unidades Disponibles" style="width:830px;height:auto" data-options="
                rownumbers:true,
                method:'get',
				singleSelect:true,				
				pagination:true,remoteSort:false,
				pageSize:10" > <!-- autoRowHeight:true,-->
		<thead>
			<tr>
				<th field="TipoHem"  width="60" sortable="true">TipoHem</th>
                <th field="SNCS"  width="70" sortable="true">SNCS</th> 
                <th field="Volumen"  width="70" sortable="true">Volumen</th>            
				<th field="NroDonacion" width="90" sortable="true">NroDonacion</th>	
                <th field="GrupoSanguineo" width="60" sortable="true">GrupoS.</th>
                <th field="Fenotipo" width="125" sortable="true">Fenotipo</th>			
				<th field="FechaExtraccion" width="100" sortable="true">Fecha Extraccion</th> 
				<th field="FechaVencimiento" width="125" sortable="true">Fecha Vencimiento</th>
                <th field="Puntuacion" width="90" sortable="true">Puntuación</th>  
                <!--<th field="Vencido" width="90">Vencido</th> -->
			</tr>
		</thead>
	</table>    

      <div id="dlg-buttonsx">   	
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="SeleccionarUnidad();" style="width:95px">Reservar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-UnidadesCompatibles').dialog('close')" style="width:90px">Cancelar</a>
	  </div>
	</div>  
     
        
    <script>
	$(function(){
		$('#dg_UnidadesDispo').datagrid('sort', {//FIRST SORT -- ASCENDING			
			sortName: 'Puntuacion',
			sortOrder: 'asc'
		});	
		/*$('#dg_UnidadesDispo').datagrid('sort', {	// SECOND SORT -- DESCENDING
			sortName: 'FechaVencimiento',
			sortOrder: 'desc'
		});*/
	});		
	function SeleccionarUnidad(){	
	
		var rows = $('#dg_UnidadesDispo').datagrid('getRows');
		var numFilas=rows.length;
		
		if(numFilas==0){
			$.messager.alert('Mensaje','No hay unidades Disponibles','warning');
			return 0;
		}			
		
		var rowd = $('#dg_UnidadesDispo').datagrid('getSelected');
		
		if (rowd){	
			var TipoHem=rowd.TipoHem;
			var SNCS=rowd.SNCS;	
			
			var IdMovDetalle=rowd.IdMovDetalle;//ID que no se repite en SIGESA_BSD_MovSangreDetalle
			
			var IdDetSolicitudSangre=rowd.IdDetSolicitudSangre;
			var IdPaciente=rowd.IdPaciente;
			
		   if(rowd.Vencido==1){
			   $.messager.alert('Mensaje','El Hemocomponente seleccionado se encuentra vencido','warning');
				return 0;			
		   }else if(rowd.Puntuacion>0){				   
			   $.messager.confirm('Mensaje', '¿Seguro de escoger hemocomponente con RIESGO de provocar anticuerpos en el receptor?', function(r){
				   if (r){								 
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdSepararHemocomponente&IdMovDetalle="+IdMovDetalle+"&IdDetSolicitudSangre="+IdDetSolicitudSangre+"&IdPaciente="+IdPaciente+"&Puntuacion="+rowd.Puntuacion+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
					}
				});	
			   
		   }else if( SNCS==undefined ){				   
			   $.messager.alert('Mensaje','No puede seleccionar un Hemocomponente vacío','warning');
				return 0;
			   
		   }else{	
		   		Hemocomponente=TipoHem+'-'+SNCS;	  
				$.messager.confirm('Mensaje', '¿Seguro de escoger el hemocomponente '+Hemocomponente+' ?', function(r){
					if (r){					
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdSepararHemocomponente&IdMovDetalle="+IdMovDetalle+"&IdDetSolicitudSangre="+IdDetSolicitudSangre+"&IdPaciente="+IdPaciente+"&Puntuacion="+rowd.Puntuacion+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
					}
				});							
			}			
			
		}else{
			$.messager.alert('Mensaje','Seleccione un Hemocomponente','warning');
			return 0;			
		}			
				
	}
	</script> 
     
  <!--fin pagar cuenta detalle-->

   <!--Inicio Ver Unidades Reservadas-->	
   <div id="dlg-UnidadesReservadas" class="easyui-dialog" style="width:800px;height:auto;"
			closed="true"   buttons="#dlg-buttonsy">
      
       <table  class="easyui-datagrid" toolbar="#tb_UnidadesReser" id="dg_UnidadesReser" title="" style="width:770px;height:auto" data-options="
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:true" > <!--,pagination:true, pageSize:10-->
		<thead>
			<tr>
				<th field="TipoHem"  width="90">TipoHem</th>
                <th field="SNCS"  width="90">SNCS</th>  
                <th field="VolumenReserva"  width="70" sortable="true">Volumen</th>   
                <th field="GrupoSanguineo"  width="90">Grupo S.</th>           
				<th field="FechaReg" width="150">Fecha Reserva</th>	
                <th field="UsuarioReg" width="110">Usuario Reserva</th> 
                <th field="IdOrden"  width="90">Orden</th>   
			</tr>
		</thead>
	</table>    

      <div id="dlg-buttonsy"> 
      	 <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-remove" onclick="QuitarReserva();" style="width:140px">Quitar Reserva</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-UnidadesReservadas').dialog('close')" style="width:90px">Cerrar</a>
	  </div>
	</div>
    <!--Fin Ver Unidades Reservadas-->  
    
    <!--Inicio Ver Unidades Despachadas-->	
   <div id="dlg-UnidadesDespachadas" class="easyui-dialog" style="width:665px;height:auto;"
			closed="true"   buttons="#dlg-buttonsz">
      
       <table  class="easyui-datagrid" toolbar="#tb_UnidadesReser" id="dg_UnidadesDespachadas" title="" style="width:648px;height:auto" data-options="
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:true" > <!--,pagination:true, pageSize:10-->
		<thead>
			<tr>
				<th field="TipoHem"  width="90">TipoHem</th>
                <th field="SNCS"  width="90">SNCS</th>  
                <th field="GrupoSanguineo"  width="90">Grupo S.</th>                        
				<th field="FechaHoraDespacho" width="150">Fecha Despacho</th>	
                <th field="UsuarioReg" width="110">Usuario Registro</th> 
                <th field="IdOrden"  width="85">Orden</th>    
			</tr>
		</thead>
	</table>    

      <div id="dlg-buttonsz">
      	 <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-add" onClick="Facturar();" style="width:90px">Facturar</a> 
         
         <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onClick="YaFacturado();" style="width:110px">Ya Facturado</a>
         
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-UnidadesDespachadas').dialog('close')" style="width:90px">Cerrar</a>
	  </div>
	</div>
    <!--Fin Ver Unidades Despachadas--> 
     
</body>
</html>