﻿<?php 
$RolesPermisos=RolesItemSeleccionarPermisosPorIdEmpleadoYIdListItem_M($_GET['IdEmpleado'],$_GET['IdListItem']);
$Agregar=$RolesPermisos[0]["Agregar"]; //1 Modificar Fraccionamiento , 0 no
$Modificar=$RolesPermisos[0]["Modificar"]; //1 Modificar Tamizaje , 0 no
$Eliminar=$RolesPermisos[0]["Eliminar"];
$Consultar=$RolesPermisos[0]["Consultar"];
?>

<html>
<head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >     
		   
		   function iniciar(){			   
			   var Agregar='<?php echo $Agregar ?>';		   
			   if(Agregar>='1'){			   
			   	$("#Fraccionamiento").show();	
			   }else{
				$("#Fraccionamiento").hide();	  
			   }
			   
			   var Modificar='<?php echo $Modificar ?>';
			   if(Modificar>='1'){			   
			   	$("#Tamizaje").show();	
			   }else{
				$("#Tamizaje").hide();	  
			   }			  	   				
		   }
		   
		   
			function cambiarMotivoElimPaqueteGlobu(){
				var MotivoElimPaqueteGlobu=$('#MotivoElimPaqueteGlobu').combobox('getValue');	
				if(MotivoElimPaqueteGlobu!=0){
					$('#VolumenPaqueteGlobu').numberbox('setValue', 0); 
				}else{
					$('#VolumenPaqueteGlobu').numberbox('setValue', ''); 
				}	
			}
			
			function cambiarMotivoElimPlasma(){
				var MotivoElimPlasma=$('#MotivoElimPlasma').combobox('getValue');	
				if(MotivoElimPlasma!=0){
					$('#VolumenPlasma').numberbox('setValue', 0); 
				}else{
					$('#VolumenPlasma').numberbox('setValue', ''); 
				}	
			}
			
			function cambiarMotivoElimPlaquetas(){
				var MotivoElimPlaquetas=$('#MotivoElimPlaquetas').combobox('getValue');	
				if(MotivoElimPlaquetas!=0){
					$('#VolumenPlaquetas').numberbox('setValue', 0); 
				}else{
					$('#VolumenPlaquetas').numberbox('setValue', ''); 
				}	
			}
			
			function ObtenerFechaActual(){
				f=new Date();
				var y = parseInt(f.getFullYear());
				var m = parseInt(f.getMonth()+1);
				var d = parseInt(f.getDate());
				if(m<10){
					m='0'+m;
				}
				if(d<10){
					d='0'+d;
				}						
				FechaActual=d+'/'+m+'/'+y;
					
				return FechaActual;
			}
		   
	   </script>		 
        
         <style type="text/css">
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:60px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				/*margin-left:10px;*/
            }
			.fitem2 label{
                display:inline-block;
                width:120px;	
				margin-left:10px;			
            }
			.fitem2 input {	
				width:140px;		 
			}
            </style>        
         
</head>

<body onLoad="iniciar();">

 <!--FORMULARIO EDITAR FRACCIONAMIENTO-->
 <div id="dlg-Fraccionamiento" class="easyui-dialog" style="width:700px;height:250px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fmFraccionamiento" name="fmFraccionamiento" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdFraccionamiento&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>&IdListItem=<?php echo $_GET['IdListItem']; ?>">            
                   
              <div class="fitem2">                
                    <label>Nro Donacion:</label>
                    <input type="text" class="easyui-textbox" name="NroDonacion" id="NroDonacion" disabled />
                    <input type="hidden" name="IdFraccionamiento" id="IdFraccionamiento">
                    <label>Responsable:</label>
                    <select style="width:200px" class="easyui-combobox" id="IdResponsable" name="IdResponsable" data-options="prompt:'Seleccione',required:true" disabled >
                      <option value=""></option>
                      <?php
					  	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
						$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                      $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>
              </div>              
              <div class="fitem2">
              		<label>Vol. Total(ML):</label>  
                    <input type="text" class="easyui-numberbox" name="VolumenTotRecol" id="VolumenTotRecol" data-options="prompt:'Volumen Total',min:0,precision:2,required:true" disabled />
               		<label>Tubuladura:</label>  
                    <input type="text" class="easyui-textbox" name="NroTabuladora" id="NroTabuladora" maxlength="8" data-options="prompt:'Tubuladura',required:true"/>					
                    <!--<label style="width:350px;">Formato: 12L12345 (2Números, 1Letra y 5Números)</label>--> 
              </div> 
              <div class="fitem2">
               		<label>Volumen PG:</label>  
                    <input type="text" class="easyui-numberbox" name="VolumenPaqueteGlobu" id="VolumenPaqueteGlobu" data-options="prompt:'Volumen Paquete Globular',required:true,min:0,precision:2" />
					<label>Motivo Elimina:</label> 
                    <select style="width:200px" class="easyui-combobox" id="MotivoElimPaqueteGlobu" name="MotivoElimPaqueteGlobu" data-options="prompt:'Seleccione',required:true,
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarMotivoElimPaqueteGlobu(); }">
                      <option value="0">Ninguno</option>
                      <?php
                                      $listar=ListarMotivoElimina('PG'); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select> 
              </div>  
              <div class="fitem2">
              		<label>Volumen PFC(ML):</label> 
               		<input type="text" class="easyui-numberbox" name="VolumenPlasma" id="VolumenPlasma" data-options="prompt:'Volumen Plasma',required:true,min:0,precision:2" />
                    <label>Motivo Elimina:</label>
                    <select style="width:200px" class="easyui-combobox" id="MotivoElimPlasma" name="MotivoElimPlasma" data-options="prompt:'Seleccione',required:true,
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarMotivoElimPlasma(); }">
                      <option value="0">Ninguno</option>
                       <?php
                                      $listar=ListarMotivoElimina('PFC'); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select>              		
              </div> 
              
               <div class="fitem2">
               		<label>Volumen PQ(ML):</label>  
                    <input type="text" class="easyui-numberbox" name="VolumenPlaquetas" id="VolumenPlaquetas" data-options="prompt:'Volumen Plaquetas',required:true,min:0,precision:2"/>
                    <label>Motivo Elimina:</label>
                    <select style="width:200px" class="easyui-combobox" id="MotivoElimPlaquetas" name="MotivoElimPlaquetas" data-options="prompt:'Seleccione',required:true,
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarMotivoElimPlaquetas(); }">
                      <option value="0">Ninguno</option>
                       <?php
                                      $listar=ListarMotivoElimina('PQ'); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                      <?php } } ?>
                    </select> 					
              </div>      
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveFraccionamiento();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-Fraccionamiento').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO EDITAR FRACCIONAMIENTO-->  

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->	
    <div class="easyui-tabs" style="width:90%;height:auto">		
		<!--Inicio Fraccionamiento-->
		<div title="Fraccionamiento" style="padding:0px"> 
			<div id="tbtasas" style="padding:5px;height:auto">
				<div style="margin-bottom:5px">
					<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="nuevoFraccionamiento()">Nuevo</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editarFraccionamiento()" >Editar</a>
					<!--<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="eliminarFraccionamiento()">Eliminar</a>--> 
					<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>  
				</div>    
			</div>
			
            <div id="Fraccionamiento">
			<table  class="easyui-datagrid" toolbar="#tbtasas" id="dg_Fraccionamiento" title="Listado de Fraccionamiento y Centrifugación" style="height:auto" data-options="						
						rownumbers:true,
						method:'get',
						singleSelect:true,
						autoRowHeight:false,
						pagination:true,
						pageSize:10" >
				<thead>
					<tr>
					    <th field="NroDonacion"  width="90">Nro Donacion</th>
                        <th field="Postulante" width="170">Postulante</th>
                        <th field="ResponsableFraccionamiento" width="90">Responsable</th>
                        <th field="NroTabuladora"  width="80">Tubuladura</th>
                        <th field="VolumenTotRecol" width="80">Vol.Total(ML)</th>                     
                        
						<th field="FechaPrimeraCentrif" width="140">Fecha y H. 1° Centrif</th>
                        <th field="VolumenPaqueteGlobu" width="100">Volumen PG</th>  
                        <th field="DesMotivoElimPaqueteGlobu" width="100">Elimina PG</th>  
                        <th field="VolumenPlasma" width="100">Volumen PFC</th>  
                        <th field="DesMotivoElimPlasma" width="100">Elimina PFC</th>  
                        
						<th field="FechaSegundaCentrif" width="140">Fecha y H. 2° Centrif</th>
                        <th field="VolumenPlaquetas" width="100">Volumen PQ</th>  
                        <th field="DesMotivoElimPlaquetas" width="100">Elimina PQ</th>                
                      			
					</tr>
				</thead>
			</table>
			</div>	
<!--FORMULARIO NUEVO Fraccionamiento-->
	
        
       

<!--FIN FORMULARIO NUEVO Tasa-->

			<script type="text/javascript">					
								
				function nuevoFraccionamiento(){					
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Fraccionamiento&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";	
				}
				
				function editarFraccionamiento(){
					var rowp = $('#dg_Fraccionamiento').datagrid('getSelected');
					if (rowp){
						$('#dlg-Fraccionamiento').dialog('open').dialog('setTitle','Editar Fraccionamiento Donación '+rowp.NroDonacion);
						$('#fmFraccionamiento').form('load',rowp);
						//$('#dlg-Fraccionamiento').dialog('open').dialog('setTitle','Editar Fraccionamiento '+rowp);
					}else{
						$.messager.alert('Mensaje de Información', 'Debe seleccionar Fraccionamiento a EDITAR','warning');						
					}					
				}	
				
				function saveFraccionamiento(){											
					//numeros = /^[0-9]+$/;
					//letras = /^[a-zA-Z]+$/;									
					 $('#fmFraccionamiento').form('submit', {			
						onSubmit: function(){			
							// return false to prevent submit;				
							if($(this).form('validate')==false){					
								return $(this).form('validate');					
							}		
						},
						success:function(data){
							//$('#fmFraccionamiento').submit();//MANDA 2 VECES
							//$('#dlg-Tabuladura').dialog('close');
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ModificarFT&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem']; ?>";
						}
					});//
					//$('#fmFraccionamiento').submit();
				}		
				
				function getData(){
						var rows = [];			
						<?php			
						 $i = 1;					
						 $Listar=ListarFraccionamientoM("");				
						 if($Listar!=NULL){   
							foreach($Listar as $item)
							{			
								$FechaPrimeraCentrif=$item['FechaPrimeraCentrif'];
								if($FechaPrimeraCentrif==""){
									$FechaPrimeraCentrif="";
								}else{
									$FechaPrimeraCentrif=vfecha(substr($item['FechaPrimeraCentrif'],0,10)).' '.substr($item['FechaPrimeraCentrif'],11,8);
								}
								
								$FechaSegundaCentrif=$item['FechaSegundaCentrif'];	
								if($FechaSegundaCentrif==""){
									$FechaSegundaCentrif="";
								}else{
									$FechaSegundaCentrif=vfecha(substr($item['FechaSegundaCentrif'],0,10)).' '.substr($item['FechaSegundaCentrif'],11,8);
								}			
								 ?>
									rows.push({											
										Postulante: "<?php echo $item['ApellidosPostulante']." ".$item['NombresPostulante'];?>",
										GrupoSanguineoPostulante: '<?php echo $item['GrupoSanguineoPostulante'];?>',
										ResponsableFraccionamiento: '<?php echo $item['ResponsableFraccionamiento'];?>',										
										NroDonacion	:  '<?php echo $item['NroDonacion'];?>',
										NroTabuladora	:  '<?php echo $item['NroTabuladora'];?>',
										VolumenTotRecol :  '<?php echo $item['VolumenTotRecol'];?>',
										FechaPrimeraCentrif :  '<?php echo $FechaPrimeraCentrif ?>',
										FechaSegundaCentrif :  '<?php echo $FechaSegundaCentrif ?>',
										
										VolumenPaqueteGlobu:  '<?php echo $item['VolumenPaqueteGlobu'];?>',
										MotivoElimPaqueteGlobu:  '<?php echo $item['MotivoElimPaqueteGlobu'];?>',
										DesMotivoElimPaqueteGlobu:  '<?php echo $item['DesMotivoElimPaqueteGlobu'];?>',
										
										VolumenPlasma:  '<?php echo $item['VolumenPlasma'];?>',
										MotivoElimPlasma:  '<?php echo $item['MotivoElimPlasma'];?>',
										DesMotivoElimPlasma:  '<?php echo $item['DesMotivoElimPlasma'];?>',
										
										VolumenPlaquetas:  '<?php echo $item['VolumenPlaquetas'];?>',
										MotivoElimPlaquetas:  '<?php echo $item['MotivoElimPlaquetas'];?>',
										DesMotivoElimPlaquetas:  '<?php echo $item['DesMotivoElimPlaquetas'];?>',
										
										IdFraccionamiento:  '<?php echo $item['IdFraccionamiento'];?>',
										IdResponsable:  '<?php echo $item['IdResponsable'];?>'
											
									});			
								<?php  $i += 1;	
							}
						 }
					 ?>
					 return rows;		
				}					
					
					/*$('#dg_Fraccionamiento').datagrid({
					  pagination:true,
					  pageSize:10,
					  remoteFilter:false
					});		
					
					$(function(){
						var dg =$('#dg_Fraccionamiento').datagrid({data:getData()}).datagrid({			
						//var dg =$('#dg').datagrid({
							filterBtnIconCls:'icon-filter'
						});
						
						dg.datagrid('enableFilter');
					});	*/	
					
					$(function(){
						var dgFrac = $('#dg_Fraccionamiento').datagrid({
							remoteFilter: false,
							pagination: true,
							pageSize: 20,
							pageList: [10,20,50,100]
						});
						
						dgFrac.datagrid('enableFilter');
						FilterFechaActualFrac();
						//showFiltersFrac();										
						dgFrac.datagrid('loadData', getData());	
						
						function FilterFechaActualFrac(){
							FechaActual=ObtenerFechaActual();
												
							dgFrac.datagrid('addFilterRule', {
								field: 'FechaPrimeraCentrif',
								type:'datebox',
								op: 'contains',
								value: FechaActual
							});
						}//fin function FilterFechaActualFrac
						
						/*function showFiltersFrac(){
							FechaActual=ObtenerFechaActual();
							
							dgFrac.datagrid('enableFilter', [{	
								
								field:'FechaPrimeraCentrif',
								type:'combobox',								
								options:{
									panelHeight:'auto',
									data:[{value:'',text:'Todos'},{value:FechaActual,text:FechaActual}],
									onChange:function(value){
										if (value == ''){
											dgFrac.datagrid('removeFilterRule', 'FechaPrimeraCentrif');
										} else {
											dgFrac.datagrid('addFilterRule', {
												field: 'FechaPrimeraCentrif',
												op: 'contains',
												value: value
											});
										}
										dgFrac.datagrid('doFilter');
									}
								}						
								
							}]);					
						}*///fin function showFiltersFrac	
								
          			});	
					
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
			
			$(function () {				
				$('#MotivoElimPaqueteGlobu').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimPaqueteGlobu"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					} 								
				});						
				$('#MotivoElimPaqueteGlobu').combobox('validate');			
				
				$('#MotivoElimPlasma').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimPlasma"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					} 								
				});						
				$('#MotivoElimPlasma').combobox('validate');
				
				$('#MotivoElimPlaquetas').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimPlaquetas"]'	,
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0;  
					}							
				});						
				$('#MotivoElimPlaquetas').combobox('validate');						
				
			});			
			
		</script>  
				
			
		</div>
		<!--Fin Fraccionamiento-->
		
		<!--Inicio Tamizaje-->
		<div title="Tamizaje" style="padding:0px" > 
			<div id="tb4" style="padding:5px;height:auto">
				<div style="margin-bottom:5px">
					<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="nuevoTamizaje()">Nuevo</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editarTamizaje()" >Editar</a>
					<!--<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="eliminarTamizaje()">Eliminar</a>-->  
					<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>  
				</div>    
			</div>
			<div id="Tamizaje" >
			<table  class="easyui-datagrid" toolbar="#tb4" id="dg_Tamizaje" title="Listado de Tamizaje" style="height:auto" data-options="					
					rownumbers:true,
					method:'get',
					singleSelect:true,
					autoRowHeight:false,
					pagination:true,
					pageSize:10" >
				<thead>
					<tr>
						<th field="NroDonacion"  width="90">Nro Donacion</th>
                        <th field="Postulante" width="170">Postulante</th>
                        <th field="ResponsableTamizaje" width="90">Responsable</th>
                        <th field="FechaTamizaje"  width="140">FechaTamizaje</th>
                        
                        <th field="DquimioVIH" width="80">VIH</th>                         
						<th field="DquimioSifilis" width="80">Sifilis</th>
                        <th field="DquimioHTLV" width="80">HTLV</th>  
                        <th field="DquimioANTICHAGAS" width="80">ANTICHAGAS</th>  
                        <th field="DquimioHBS" width="80">HBS</th>  
                        <th field="DquimioVHB" width="80">VHB</th>                         
						<th field="DquimioVHC" width="80">VHC</th>

						<th field="DnatVIH" width="80">nat VIH</th>  
                        <th field="DnatHBV" width="80">nat HBV</th>  
                        <th field="DnatHCV" width="80">nat HCV</th> 
                        
                        <th field="ReactivoTamizaje" width="80">Reactivo</th>  						
					</tr>
				</thead>
			</table>
			</div>	
<!--FORMULARIO EDITAR TAMIZAJE-->
 <div id="dlg-Tamizaje" class="easyui-dialog" style="width:1000px;height:530px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fmTamizaje" name="fmTamizaje" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdTamizaje&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>&IdListItem=<?php echo $_GET['IdListItem']; ?>">            
                   
              <div class="fitem2">                
                    <label>Nro Donacion:</label>
                    <input type="text" class="easyui-textbox" name="NroDonacion" id="NroDonacion" disabled />
                    <input type="hidden" name="IdTamizaje" id="IdTamizaje">
                    <input type="hidden" name="IdExtraccion" id="IdExtraccion">
                    <input type="hidden" name="IdMovimiento" id="IdMovimiento">
                    <label>Responsable:</label>
                    <select style="width:200px" class="easyui-combobox" id="IdResponsable" name="IdResponsable" data-options="prompt:'Seleccione',required:true" disabled >
                      <option value=""></option>
                      <?php
					  	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
						$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                      $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>
              </div>              
              <table>
            <tr>
              <td colspan="4"><b>EXAMEN DE QUIMIOLUMINICENCIA</b></td>
            </tr>
            <tr align="center">
                <td>
                    <fieldset>
                        <legend>1. VIH</legend>
                        <label for="Reactivo1">Reactivo</label>
                        <input type="radio" name="quimioVIH" id="Reactivo1" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo1">No Reactivo</label>
                        <input type="radio" name="quimioVIH" id="NoReactivo1" value="0" onClick="MostrarMedicionReactivo();" checked />
                  		<br><input class="easyui-numberbox" name="PRquimioVIH" id="PRquimioVIH" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/>
                  </fieldset>  
                </td>
                <td>
                    <fieldset>
                        <legend>2. HTLV I/II</legend>                       
                        <label for="Reactivo2">Reactivo</label>
                        <input type="radio" name="quimioHTLV" id="Reactivo2" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo2">No Reactivo</label>
                        <input type="radio" name="quimioHTLV" id="NoReactivo2" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioHTLV" id="PRquimioHTLV" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/>
                    </fieldset>   
                </td>
                <td>
            		<fieldset>
           			 	<legend>3. HBS Ag (Hepatitis B Ag)</legend>                        
						<label for="Reactivo3">Reactivo</label>
                        <input type="radio" name="quimioHBS" id="Reactivo3" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo3">No Reactivo</label>
                        <input type="radio" name="quimioHBS" id="NoReactivo3" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioHBS" id="PRquimioHBS" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/>           			   
            		</fieldset>  
                </td>
               <td>
                    <fieldset>
                    	<legend>4. CORE VHB(Hepatitis B core)</legend>                        
                        <label for="Reactivo4">Reactivo</label>
                        <input type="radio" name="quimioVHB" id="Reactivo4" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo4">No Reactivo</label>
                        <input type="radio" name="quimioVHB" id="NoReactivo4" value="0" onClick="MostrarMedicionReactivo();" checked />
                    	 <br><input class="easyui-numberbox" name="PRquimioVHB" id="PRquimioVHB" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
              </td>
          </tr>
          
          <tr align="center">
                <td>
                    <fieldset>
                    	<legend>5. VHC (Hepatitis C)</legend>                       
                        <label for="Reactivo5">Reactivo</label>
                        <input type="radio" name="quimioVHC" id="Reactivo5" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo5">No Reactivo</label>
                        <input type="radio" name="quimioVHC" id="NoReactivo5" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioVHC" id="PRquimioVHC" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
                </td>
               <td>
                    <fieldset>
                    	<legend>6. SIFILIS</legend>                    	
                        <label for="Reactivo6">Reactivo</label>
                        <input type="radio" name="quimioSifilis" id="Reactivo6" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo6">No Reactivo</label>
                        <input type="radio" name="quimioSifilis" id="NoReactivo6" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioSifilis" id="PRquimioSifilis" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
              </td>
                <td>
                    <fieldset>
                    	<legend>7. CHAGAS</legend>                       
                        <label for="Reactivo7">Reactivo</label>
                        <input type="radio" name="quimioANTICHAGAS" id="Reactivo7" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo7">No Reactivo</label>
                        <input type="radio" name="quimioANTICHAGAS" id="NoReactivo7" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioANTICHAGAS" id="PRquimioANTICHAGAS" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
                </td>
                <td> </td>
            </tr>
      </table>
      <br>
      <table width="892" border="1" cellpadding="0" cellspacing="0">
            <tr align="center">
              <td colspan="4">
              <b>Realizará NAT</b>:&nbsp;&nbsp;
              <Select style="width:150px" class="easyui-combobox" id="RealizaNAT" name="RealizaNAT"  data-options="prompt:'Seleccione',required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarNAT(); }">
                <option value=""></option>
                <option value="1" selected>1=SI</option>
                <option value="0">0=NO</option>
              </select>
              </td>
            </tr>
            <tr align="center">
              <td><b>MOTIVO</b></td>
              <td colspan="3"><b>EXAMEN DE BOLOGÍA MOLECULAR (NAT)</b></td>              
            </tr>
            <tr>
              <td align="center">
              <Select style="width:240px" class="easyui-combobox" id="IdMotivoNoNAT" name="IdMotivoNoNAT" data-options="prompt:'Seleccione',required:true,
              			valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarMotivoNoNAT(); }" readonly>
                <option value="0">No requiere</option>
                <?php
                                  $listar=SIGESA_BSD_MotivoNoNAT_M(); 
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
                <option value="<?php echo $item["IdMotivoNoNAT"]?>" ><?php echo mb_strtoupper($item["IdMotivoNoNAT"].'= '.$item["Descripcion"])?></option>
                <?php } } ?>
              </select> <br>
               <input class="easyui-textbox" name="MotivoNoNAT" id="MotivoNoNAT" data-options="prompt:'Motivo',required:true"/>               
                </td>
                <td>
                    <fieldset>
                        <legend>VIH</legend>
                        <label for="Reactivo11">Reactivo</label>
                        <input type="radio" name="natVIH" id="Reactivo11" value="1" />
                        <label for="NoReactivo11">No Reactivo</label>
                        <input type="radio" name="natVIH" id="NoReactivo11" value="0" />
                  
                    </fieldset>  
                </td>
                <td>
                    <fieldset><legend>HBV (Hepatitis B)</legend>
                        <label for="Reactivo22">Reactivo</label>
                        <input type="radio" name="natHBV" id="Reactivo22" value="1" />
                        <label for="NoReactivo22">No Reactivo</label>
                        <input type="radio" name="natHBV" id="NoReactivo22" value="0" />
                    </fieldset>  
                </td>
                <td>
            		<fieldset><legend>HCV (Hepatitis C)</legend>
            			<label for="Reactivo33">Reactivo</label>
                        <input type="radio" name="natHCV" id="Reactivo33" value="1" />
                        <label for="NoReactivo33">No Reactivo</label>
                        <input type="radio" name="natHCV" id="NoReactivo33" value="0" />
           			</fieldset>  
                </td>
          </tr>
           
      </table>   
      <br>
      
      <table width="892" height="83" border="0" cellpadding="0" cellspacing="0">
        <tr align="center">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr align="center">
          <td width="418" height="31"><b>Nº DE SELLO NACIONAL DE CALIDAD</b></td>
          <td>
          	<input type="checkbox" name="NoSNCS" id="NoSNCS" onClick="cambiarNoSNCS()" />
          	<label for="NoSNCS" >NO crear Nº SNCS y NO ingresar ningún resultado de tamizaje</label>
          </td>
        </tr>
        <tr align="center">
          <td align="center">
          	<input class="easyui-numberbox" name="SNCS" id="SNCS" data-options="prompt:'Nº SNCS',
                                            formatter:function(v){
                                                var s = new String(v||'');                                                                                              
                                                return s;
                                            },
                                            parser:function(s){
                                                return s;
                                            }
                                            " maxlength="7" /></td>
          <td>
                Motivo falta resultado (F): 
                <select style="width:200px" class="easyui-combobox" id="MotivoElimMuestraX" name="MotivoElimMuestraX" data-options="prompt:'Seleccione',
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarMotivoElimMuestra(); }" readonly >
                  <option value="">Ninguno</option>
                  <?php
                                          $listar=ListarMotivoElimina('TAM'); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                  <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                  <?php } } ?>
                </select>
                <input name="MotivoElimMuestra" id="MotivoElimMuestra" type="hidden" />
          </td>
        </tr>
        <tr align="center">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr align="center">
          <td colspan="2">Observación: <input style="width:300px;height:50px" class="easyui-textbox" multiline="true" name="Observacion" id="Observacion" /> </td>
        </tr>
        
      </table> 
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveTamizaje();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-Tamizaje').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO EDITAR TAMIZAJE--> 	


			<script type="text/javascript">	
					
				 function nuevoTamizaje(){
					location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Tamizaje&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem']; ?>";	
				}
				
				$(function(){	
				
				$.extend($.fn.textbox.methods, {
					show: function(jq){
						return jq.each(function(){
							$(this).next().show();
						})
					},
					hide: function(jq){
						return jq.each(function(){
							$(this).next().hide();
						})
					}
				})				
									
			});
			
			function cambiarNAT(){
				var RealizaNAT= $('#RealizaNAT').combobox('getValue'); 
				if(RealizaNAT=='0'){ //NO					
					//document.getElementById('Reactivo11').checked=false;
					document.getElementById('Reactivo11').style.visibility = 'hidden';
					//document.getElementById('NoReactivo11').checked=false;
					document.getElementById('NoReactivo11').style.visibility = 'hidden';
					//document.getElementById('Reactivo22').value=false;
					document.getElementById('Reactivo22').style.visibility = 'hidden';
					//document.getElementById('NoReactivo22').checked=false;
					document.getElementById('NoReactivo22').style.visibility = 'hidden';
					//document.getElementById('Reactivo33').value=false;
					document.getElementById('Reactivo33').style.visibility = 'hidden';
					//document.getElementById('NoReactivo33').checked=false;
					document.getElementById('NoReactivo33').style.visibility = 'hidden';
					$("#IdMotivoNoNAT").combobox('readonly',false);	
					$('#IdMotivoNoNAT').next().find('input').focus();	
				}else{ //SI		
					document.getElementById('Reactivo11').style.visibility = 'visible';					
					document.getElementById('NoReactivo11').style.visibility = 'visible';					
					document.getElementById('Reactivo22').style.visibility = 'visible';					
					document.getElementById('NoReactivo22').style.visibility = 'visible';					
					document.getElementById('Reactivo33').style.visibility = 'visible';					
					document.getElementById('NoReactivo33').style.visibility = 'visible';								
					document.getElementById('NoReactivo11').checked=true;					
					document.getElementById('NoReactivo22').checked=true;					
					document.getElementById('NoReactivo33').checked=true;
					$("#IdMotivoNoNAT").combobox('readonly',true);	
					$("#IdMotivoNoNAT").combobox('setValue','0');
					$('#MotivoNoNAT').textbox('hide');
				}
			}
			
			function cambiarMotivoNoNAT(){
				var IdMotivoNoNAT= $('#IdMotivoNoNAT').combobox('getValue');	
				if(IdMotivoNoNAT=='4'){ //4 = OTRO, ESPECIFICAR:
					$('#MotivoNoNAT').textbox('show'); 	
					$('#MotivoNoNAT').next().find('input').focus();	
				}else{
					$('#MotivoNoNAT').textbox('hide'); 	
				}
			}
			
			$(function(){	
				
				$.extend($.fn.textbox.methods, {
					show: function(jq){
						return jq.each(function(){
							$(this).next().show();
						})
					},
					hide: function(jq){
						return jq.each(function(){
							$(this).next().hide();
						})
					}
				})				
				$('#MotivoNoNAT').textbox('hide');			
									
			});			
			
			function MostrarMedicionReactivo(){
				if(document.getElementById('Reactivo1').checked==true){
					$('#PRquimioVIH').numberbox('show');					
				}else{
					$("#PRquimioVIH").numberbox('setValue','');
					$('#PRquimioVIH').numberbox('hide');									
				}
				
				if(document.getElementById('Reactivo2').checked==true){
					$('#PRquimioHTLV').numberbox('show');
				}else{
					$("#PRquimioHTLV").numberbox('setValue','');
					$('#PRquimioHTLV').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo3').checked==true){
					$('#PRquimioHBS').numberbox('show');
				}else{
					$("#PRquimioHBS").numberbox('setValue','');
					$('#PRquimioHBS').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo4').checked==true){
					$('#PRquimioVHB').numberbox('show');
				}else{
					$("#PRquimioVHB").numberbox('setValue','');
					$('#PRquimioVHB').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo5').checked==true){
					$('#PRquimioVHC').numberbox('show');
				}else{
					$("#PRquimioVHC").numberbox('setValue','');
					$('#PRquimioVHC').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo6').checked==true){
					$('#PRquimioSifilis').numberbox('show');
				}else{
					$("#PRquimioSifilis").numberbox('setValue','');
					$('#PRquimioSifilis').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo7').checked==true){
					$('#PRquimioANTICHAGAS').numberbox('show');
				}else{
					$("#PRquimioANTICHAGAS").numberbox('setValue','');
					$('#PRquimioANTICHAGAS').numberbox('hide');
				}
				
				//TODOS				
				if( (document.getElementById('Reactivo1').checked==true) || (document.getElementById('Reactivo2').checked==true) || (document.getElementById('Reactivo3').checked==true) || (document.getElementById('Reactivo4').checked==true) || (document.getElementById('Reactivo5').checked==true) || (document.getElementById('Reactivo6').checked==true) || (document.getElementById('Reactivo7').checked==true)){			
					$("#RealizaNAT").combobox('setValue','0');
					$("#IdMotivoNoNAT").combobox('setValue','5');
					
					$("#SNCS").numberbox('setValue','');
					$("#SNCS").numberbox('readonly',true);
					document.getElementById('NoSNCS').disabled=true;
					
				}else{						
					$("#RealizaNAT").combobox('setValue','1');	
					
					$("#SNCS").numberbox('readonly',false);	
					document.getElementById('NoSNCS').disabled=false;			
				}						
				cambiarNAT();				
			}
			
			
			function cambiarMotivoElimMuestra(){
				var MotivoElimMuestra=$('#MotivoElimMuestraX').combobox('getValue');	
				document.getElementById('MotivoElimMuestra').value=MotivoElimMuestra	
			}
			
			$(function(){		  
		  		$('#SNCS').numberbox('textbox').attr('maxlength', $('#SNCS').attr("maxlength"));		  	   
			});	
			
			function cambiarNoSNCS(){				
				if(document.getElementById('NoSNCS').checked==true){ //CHECKED	
					//QUIMIOLUMINICENCIA
					document.getElementById('Reactivo1').style.visibility = 'hidden';					
					document.getElementById('NoReactivo1').style.visibility = 'hidden';					
					document.getElementById('Reactivo2').style.visibility = 'hidden';					
					document.getElementById('NoReactivo2').style.visibility = 'hidden';					
					document.getElementById('Reactivo3').style.visibility = 'hidden';					
					document.getElementById('NoReactivo3').style.visibility = 'hidden';						
					document.getElementById('Reactivo4').style.visibility = 'hidden';					
					document.getElementById('NoReactivo4').style.visibility = 'hidden';					
					document.getElementById('Reactivo5').style.visibility = 'hidden';					
					document.getElementById('NoReactivo5').style.visibility = 'hidden';					
					document.getElementById('Reactivo6').style.visibility = 'hidden';					
					document.getElementById('NoReactivo6').style.visibility = 'hidden';						
					document.getElementById('Reactivo7').style.visibility = 'hidden';					
					document.getElementById('NoReactivo7').style.visibility = 'hidden';	
									
					//NAT
					document.getElementById('Reactivo11').style.visibility = 'hidden';					
					document.getElementById('NoReactivo11').style.visibility = 'hidden';					
					document.getElementById('Reactivo22').style.visibility = 'hidden';					
					document.getElementById('NoReactivo22').style.visibility = 'hidden';					
					document.getElementById('Reactivo33').style.visibility = 'hidden';					
					document.getElementById('NoReactivo33').style.visibility = 'hidden';					
					$("#IdMotivoNoNAT").combobox('readonly',false);	
					$("#IdMotivoNoNAT").combobox('setValue','6');						
					$("#RealizaNAT").combobox('setValue','0');						
					
					$("#MotivoElimMuestraX").combobox('readonly',false);	
					$('#MotivoElimMuestraX').next().find('input').focus();	
					$("#SNCS").numberbox('setValue','');
					$("#SNCS").numberbox('readonly',true);			
					
				}else{ //NO CHECKED	
					//QUIMIOLUMINICENCIA
					document.getElementById('Reactivo1').style.visibility = 'visible';					
					document.getElementById('NoReactivo1').style.visibility = 'visible';					
					document.getElementById('Reactivo2').style.visibility = 'visible';					
					document.getElementById('NoReactivo2').style.visibility = 'visible';					
					document.getElementById('Reactivo3').style.visibility = 'visible';					
					document.getElementById('NoReactivo3').style.visibility = 'visible';						
					document.getElementById('Reactivo4').style.visibility = 'visible';					
					document.getElementById('NoReactivo4').style.visibility = 'visible';					
					document.getElementById('Reactivo5').style.visibility = 'visible';					
					document.getElementById('NoReactivo5').style.visibility = 'visible';					
					document.getElementById('Reactivo6').style.visibility = 'visible';					
					document.getElementById('NoReactivo6').style.visibility = 'visible';						
					document.getElementById('Reactivo7').style.visibility = 'visible';					
					document.getElementById('NoReactivo7').style.visibility = 'visible';
					document.getElementById('NoReactivo1').checked=true;					
					document.getElementById('NoReactivo2').checked=true;					
					document.getElementById('NoReactivo3').checked=true;	
					document.getElementById('NoReactivo4').checked=true;
					document.getElementById('NoReactivo5').checked=true;
					document.getElementById('NoReactivo6').checked=true;
					document.getElementById('NoReactivo7').checked=true;										
					//NAT	
					document.getElementById('Reactivo11').style.visibility = 'visible';					
					document.getElementById('NoReactivo11').style.visibility = 'visible';					
					document.getElementById('Reactivo22').style.visibility = 'visible';					
					document.getElementById('NoReactivo22').style.visibility = 'visible';					
					document.getElementById('Reactivo33').style.visibility = 'visible';					
					document.getElementById('NoReactivo33').style.visibility = 'visible';								
					document.getElementById('NoReactivo11').checked=true;					
					document.getElementById('NoReactivo22').checked=true;					
					document.getElementById('NoReactivo33').checked=true;
					$("#IdMotivoNoNAT").combobox('readonly',true);	
					$("#IdMotivoNoNAT").combobox('setValue','0');
					$('#MotivoNoNAT').textbox('hide');					
					$("#RealizaNAT").combobox('setValue','1');					
					
					$("#MotivoElimMuestraX").combobox('readonly',true);	
					$("#MotivoElimMuestraX").combobox('setValue','');
					document.getElementById('MotivoElimMuestra').value='';
					
					if( (document.getElementById('Reactivo1').checked==false) && (document.getElementById('Reactivo2').checked==false) && (document.getElementById('Reactivo3').checked==false) && (document.getElementById('Reactivo4').checked==false) && (document.getElementById('Reactivo5').checked==false) && (document.getElementById('Reactivo6').checked==false) && (document.getElementById('Reactivo7').checked==false) ){
						$("#SNCS").numberbox('readonly',false);
						$('#SNCS').next().find('input').focus();	
					}
						
				}
			}
			
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
						
			$(function () {				
				
				$('#RealizaNAT').combobox({	
					editable: false,
					required: true								
				});	
				$('#RealizaNAT').combobox('validate');					
				
				$('#IdMotivoNoNAT').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdMotivoNoNAT"]'								
				});						
				$('#IdMotivoNoNAT').combobox('validate');
				
				$('#MotivoElimMuestraX').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimMuestraX"]'								
				});						
				$('#MotivoElimMuestraX').combobox('validate');				
				
			});
				
				function editarTamizaje(){
					var rowp = $('#dg_Tamizaje').datagrid('getSelected');
					if (rowp){
						$('#dlg-Tamizaje').dialog('open').dialog('setTitle','Editar Tamizaje Donación '+rowp.NroDonacion);
						$('#fmTamizaje').form('load',rowp);
						MostrarMedicionReactivo();
						if(rowp.ReactivoTamizaje=='F'){
							document.getElementById('NoSNCS').checked=true
						}else{
							document.getElementById('NoSNCS').checked=false
						}
						
						cambiarNoSNCS();
																	
					}else{
						$.messager.alert('Mensaje de Información', 'Debe seleccionar Tamizaje a EDITAR','warning');						
					}					
				}	
				
				function saveTamizaje(){					
					//QUIMIOLUMINICENCIA
					var PRquimioVIH=$('#PRquimioVIH').numberbox('getValue');	
					if(document.getElementById('Reactivo1').checked==true && PRquimioVIH.trim()==""){
						//$('#PRquimioVIH').numberbox('show');
						//$.messager.alert('Mensaje','Falta Ingresar Valor medición VIH','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Valor medición VIH',
							icon:'warning',
							fn: function(){
								$('#PRquimioVIH').next().find('input').focus();
							}
						});
						$('#PRquimioVIH').next().find('input').focus();
						return 0;
					}
					
					var PRquimioHTLV=$('#PRquimioHTLV').numberbox('getValue');
					if(document.getElementById('Reactivo2').checked==true && PRquimioHTLV.trim()==""){
						//$('#PRquimioHTLV').numberbox('show');
						//$.messager.alert('Mensaje','Falta Ingresar Valor medición HTLV','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Valor medición HTLV',
							icon:'warning',
							fn: function(){
								$('#PRquimioHTLV').next().find('input').focus();
							}
						});
						$('#PRquimioHTLV').next().find('input').focus();
						return 0;
					}
					
					var PRquimioHBS=$('#PRquimioHBS').numberbox('getValue');
					if(document.getElementById('Reactivo3').checked==true && PRquimioHBS.trim()==""){
						//$('#PRquimioHBS').numberbox('show');
						//$.messager.alert('Mensaje','Falta Ingresar Valor medición HBS','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Valor medición HBS',
							icon:'warning',
							fn: function(){
								$('#PRquimioHBS').next().find('input').focus();
							}
						});
						$('#PRquimioHBS').next().find('input').focus();
						return 0;
					}
					
					var PRquimioVHB=$('#PRquimioVHB').numberbox('getValue');
					if(document.getElementById('Reactivo4').checked==true && PRquimioVHB.trim()==""){
						//$('#PRquimioVHB').numberbox('show');
						//$.messager.alert('Mensaje','Falta Ingresar Valor medición VHB','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Valor medición VHB',
							icon:'warning',
							fn: function(){
								$('#PRquimioVHB').next().find('input').focus();
							}
						});
						$('#PRquimioVHB').next().find('input').focus();
						return 0;
					}
					
					var PRquimioVHC=$('#PRquimioVHC').numberbox('getValue');
					if(document.getElementById('Reactivo5').checked==true && PRquimioVHC.trim()==""){
						//$('#PRquimioVHC').numberbox('show');
						//$.messager.alert('Mensaje','Falta Ingresar Valor medición VHC','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Valor medición VHC',
							icon:'warning',
							fn: function(){
								$('#PRquimioVHC').next().find('input').focus();
							}
						});
						$('#PRquimioVHC').next().find('input').focus();
						return 0;
					}
					
					var PRquimioSifilis=$('#PRquimioSifilis').numberbox('getValue');
					if(document.getElementById('Reactivo6').checked==true && PRquimioSifilis.trim()==""){
						//$('#PRquimioSifilis').numberbox('show');
						//$.messager.alert('Mensaje','Falta Ingresar Valor medición Sifilis','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Valor medición Sifilis',
							icon:'warning',
							fn: function(){
								$('#PRquimioSifilis').next().find('input').focus();
							}
						});
						$('#PRquimioSifilis').next().find('input').focus();
						return 0;
					}
					
					var PRquimioANTICHAGAS=$('#PRquimioANTICHAGAS').numberbox('getValue');
					if(document.getElementById('Reactivo7').checked==true && PRquimioANTICHAGAS.trim()==""){
						//$('#PRquimioANTICHAGAS').numberbox('show');
						//$.messager.alert('Mensaje','Falta Ingresar Valor medición CHAGAS','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Valor medición CHAGAS',
							icon:'warning',
							fn: function(){
								$('#PRquimioANTICHAGAS').next().find('input').focus();
							}
						});
						$('#PRquimioANTICHAGAS').next().find('input').focus();
						return 0;
					}
					
					
					//NAT
					var RealizaNAT=$('#RealizaNAT').combobox('getValue');		
					if(RealizaNAT.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Seleccionar si Realizará NAT','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar si Realizará NAT',
							icon:'warning',
							fn: function(){
								$('#RealizaNAT').next().find('input').focus();
							}
						});
						$('#RealizaNAT').next().find('input').focus();
						return 0;			
					}
					
					var IdMotivoNoNAT=$('#IdMotivoNoNAT').combobox('getValue');	
					if(RealizaNAT.trim()=="0" && IdMotivoNoNAT.trim()=="0" || ($('#IdMotivoNoNAT').combobox('isValid')==false)){ //NO
						//$.messager.alert('Mensaje','Falta Seleccionar Motivo que no realizará NAT','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar Motivo que no realizará NAT',
							icon:'warning',
							fn: function(){
								$('#IdMotivoNoNAT').next().find('input').focus();
							}
						});
						$('#IdMotivoNoNAT').next().find('input').focus();
						return 0;			
					}	
					
					var MotivoNoNAT=$('#MotivoNoNAT').textbox('getValue');	
					if(IdMotivoNoNAT.trim()=="4" && MotivoNoNAT.trim()==""){ //NO y 4 = OTRO, ESPECIFICAR:
						//$.messager.alert('Mensaje','Falta Ingresar Motivo que no realizará NAT','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Motivo que no realizará NAT',
							icon:'warning',
							fn: function(){
								$('#MotivoNoNAT').next().find('input').focus();
							}
						});
						$('#MotivoNoNAT').next().find('input').focus();
						return 0;			
					}
					
					var SNCS=$('#SNCS').numberbox('getValue');
					if( (document.getElementById('NoSNCS').checked==false && SNCS.trim()=="") && (document.getElementById('Reactivo1').checked==false) && (document.getElementById('Reactivo2').checked==false) && (document.getElementById('Reactivo3').checked==false) && (document.getElementById('Reactivo4').checked==false) && (document.getElementById('Reactivo5').checked==false) && (document.getElementById('Reactivo6').checked==false) && (document.getElementById('Reactivo7').checked==false)){	//SOLO APTOS	
						//$.messager.alert('Mensaje','Falta Ingresar Nº de Sello Nacional de Calidad','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Nº de Sello Nacional de Calidad',
							icon:'warning',
							fn: function(){
								$('#SNCS').next().find('input').focus();
							}
						});
						$('#SNCS').next().find('input').focus();
						return 0;	
					}
					
					var MotivoElimMuestra=document.getElementById('MotivoElimMuestra').value;
					var MotivoElimMuestraX=$('#MotivoElimMuestraX').combobox('getValue');					
					if( (document.getElementById('NoSNCS').checked==true && (MotivoElimMuestraX.trim()=="" || MotivoElimMuestra.trim()=="")) || $('#IdMotivoNoNAT').combobox('isValid')==false ){ //CHECKED		
						//$.messager.alert('Mensaje','Falta Ingresar Motivo falta resultado (F)','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar Motivo falta resultado (F)',
							icon:'warning',
							fn: function(){
								$('#MotivoElimMuestraX').next().find('input').focus();
							}
						});
						$('#MotivoElimMuestraX').next().find('input').focus();
						return 0;	
					}
																	
					//document.form1.submit();
					$.messager.confirm('Mensaje', '¿Seguro de Actualizar los Datos de Tamizaje de Sangre del Donante?', function(r){
						if (r){
							$('#fmTamizaje').submit();	
						}
					});	
					
				}		
				
				function getDataTamizaje(){
						var rows = [];			
						<?php			
						 $i = 1;					
						 $Listar=ListarTamizajeM("");				
						 if($Listar!=NULL){   
							foreach($Listar as $item)
							{			
								$FechaTamizaje=$item['FechaTamizaje'];
								if($FechaTamizaje==""){
									$FechaTamizaje="";
								}else{
									$FechaTamizaje=vfecha(substr($item['FechaTamizaje'],0,10));
								}	
								
								 $quimioVIH=$item["quimioVIH"]; 
								 if($quimioVIH=='1'){
									$DquimioVIH='REACTIVO'; 
								 }else if($quimioVIH=='0'){
									$DquimioVIH='no reactivo';  
								 }else{
									$DquimioVIH='';  
								 }
								 
								 $quimioSifilis=$item["quimioSifilis"]; 
								 if($quimioSifilis=='1'){
									$DquimioSifilis='REACTIVO'; 
								 }else if($quimioSifilis=='0'){
									$DquimioSifilis='no reactivo';  
								 }else{
									$DquimioSifilis='';  
								 }
								 
								 $quimioHTLV=$item["quimioHTLV"]; 
								 if($quimioHTLV=='1'){
									$DquimioHTLV='REACTIVO'; 
								 }else if($quimioHTLV=='0'){
									$DquimioHTLV='no reactivo';  
								 }else{
									$DquimioHTLV='';  
								 }
								 
								 $quimioANTICHAGAS=$item["quimioANTICHAGAS"];
								 if($quimioANTICHAGAS=='1'){
									$DquimioANTICHAGAS='REACTIVO'; 
								 }else if($quimioANTICHAGAS=='0'){
									$DquimioANTICHAGAS='no reactivo';  
								 }else{
									$DquimioANTICHAGAS='';  
								 }
								  
								 $quimioHBS=$item["quimioHBS"]; 
								 if($quimioHBS=='1'){
									$DquimioHBS='REACTIVO'; 
								 }else if($quimioHBS=='0'){
									$DquimioHBS='no reactivo';  
								 }else{
									$DquimioHBS='';  
								 }
								 
								 $quimioVHB=$item["quimioVHB"];
								 if($quimioVHB=='1'){
									$DquimioVHB='REACTIVO'; 
								 }else if($quimioVHB=='0'){
									$DquimioVHB='no reactivo';  
								 }else{
									$DquimioVHB='';  
								 }
								  
								 $quimioVHC=$item["quimioVHC"]; 
								 if($quimioVHC=='1'){
									$DquimioVHC='REACTIVO'; 
								 }else if($quimioVHC=='0'){
									$DquimioVHC='no reactivo';  
								 }else{
									$DquimioVHC='';  
								 }
								 
								 $natVIH=$item["natVIH"];
								 if($natVIH=='1'){
									$DnatVIH='REACTIVO'; 
								 }else if($natVIH=='0'){
									$DnatVIH='no reactivo';  
								 }else{
									$DnatVIH='';  
								 }
								  
								 $natHBV=$item["natHBV"]; 
								 if($natHBV=='1'){
									$DnatHBV='REACTIVO'; 
								 }else if($natHBV=='0'){
									$DnatHBV='no reactivo';  
								 }else{
									$DnatHBV='';  
								 }
								 
								 $natHCV=$item["natHCV"];
								 if($natHCV=='1'){
									$DnatHCV='REACTIVO'; 
								 }else if($natHCV=='0'){
									$DnatHCV='no reactivo';  
								 }else{
									$DnatHCV='';  
								 }		
								 
								 $EstadoAptoTamizaje=$item["EstadoAptoTamizaje"];
								 if($EstadoAptoTamizaje==NULL || $EstadoAptoTamizaje==''){
									$ReactivoTamizaje='F';
								 }else if($EstadoAptoTamizaje=='0'){//NO APTO
									$ReactivoTamizaje='SI'; //SI REACTIVO
								 }else if($EstadoAptoTamizaje=='1'){//APTO
									$ReactivoTamizaje='NO'; //NO REACTIVO
								 }						
											
								 ?>
									rows.push({											
										Postulante: "<?php echo $item['ApellidosPostulante']." ".$item['NombresPostulante'];?>",
										GrupoSanguineoPostulante: '<?php echo $item['GrupoSanguineoPostulante'];?>',
										ResponsableTamizaje: '<?php echo $item['ResponsableTamizaje'];?>',										
										NroDonacion	:  '<?php echo $item['NroDonacion'];?>',
										FechaTamizaje	:  '<?php echo $FechaTamizaje;?>',
										
										quimioVIH	:  '<?php echo $item['quimioVIH'];?>',
										quimioSifilis :  '<?php echo $item['quimioSifilis'];?>',
										quimioHTLV :  '<?php echo $item['quimioHTLV'] ?>',
										quimioANTICHAGAS :  '<?php echo $item['quimioANTICHAGAS'] ?>',										
										quimioHBS	:  '<?php echo $item['quimioHBS'];?>',
										quimioVHB :  '<?php echo $item['quimioVHB'];?>',
										quimioVHC :  '<?php echo $item['quimioVHC'] ?>',
										
										PRquimioVIH	:  '<?php echo $item['PRquimioVIH'];?>',
										PRquimioSifilis :  '<?php echo $item['PRquimioSifilis'];?>',
										PRquimioHTLV :  '<?php echo $item['PRquimioHTLV'] ?>',
										PRquimioANTICHAGAS :  '<?php echo $item['PRquimioANTICHAGAS'] ?>',										
										PRquimioHBS	:  '<?php echo $item['PRquimioHBS'];?>',
										PRquimioVHB :  '<?php echo $item['PRquimioVHB'];?>',
										PRquimioVHC :  '<?php echo $item['PRquimioVHC'] ?>',	
																				
										natVIH :  '<?php echo $item['natVIH'] ?>',
										natHBV :  '<?php echo $item['natHBV'] ?>',
										natHCV :  '<?php echo $item['natHCV'] ?>',
										
										DquimioVIH	:  '<?php echo $DquimioVIH;?>',
										DquimioSifilis :  '<?php echo $DquimioSifilis;?>',
										DquimioHTLV :  '<?php echo $DquimioHTLV ?>',
										DquimioANTICHAGAS :  '<?php echo $DquimioANTICHAGAS ?>',										
										DquimioHBS	:  '<?php echo $DquimioHBS;?>',
										DquimioVHB :  '<?php echo $DquimioVHB;?>',
										DquimioVHC :  '<?php echo $DquimioVHC ?>',
										
										DnatVIH :  '<?php echo $DnatVIH ?>',
										DnatHBV :  '<?php echo $DnatHBV ?>',
										DnatHCV :  '<?php echo $DnatHCV ?>',
										ReactivoTamizaje:  '<?php echo $ReactivoTamizaje ?>',		
										
										IdResponsable:  '<?php echo  $item['IdResponsable'] ?>',
										IdTamizaje:  '<?php echo $item['IdTamizaje'] ?>',
										IdExtraccion:  '<?php echo $item['IdExtraccion'] ?>',	
										IdMovimiento:  '<?php echo $item['IdMovimiento'] ?>',
										SNCS	:  '<?php echo $item['SNCS'] ?>',
										MotivoElimMuestra	:  '<?php echo $item['MotivoElimMuestra'] ?>',
										MotivoElimMuestraX	:  '<?php echo $item['MotivoElimMuestra'] ?>',
										Observacion	:  '<?php echo $item['Observacion'] ?>'										
									});			
								<?php  $i += 1;	
							}
						 }
					 ?>
					 return rows;		
				}
				
				$('#dg_Tamizaje').datagrid({
					rowStyler:function(index,row){
						if (row.DquimioVIH=='REACTIVO'){ //1
							return 'background-color:#ed9a93;color:black;';
						}
						if (row.DquimioSifilis=='REACTIVO'){ //2
							return 'background-color:#ed9a93;color:black;';
						}
						if (row.DquimioHTLV=='REACTIVO'){ //3
							return 'background-color:#ed9a93;color:black;';
						}
						if (row.DquimioANTICHAGAS=='REACTIVO'){ //4
							return 'background-color:#ed9a93;color:black;';
						}
						if (row.DquimioHBS=='REACTIVO'){ //5
							return 'background-color:#ed9a93;color:black;';
						}
						if (row.DquimioVHB=='REACTIVO'){ //6
							return 'background-color:#ed9a93;color:black;';
						}
						if (row.DquimioVHC=='REACTIVO'){ //6
							return 'background-color:#ed9a93;color:black;';
						} 
					}
				});
					
					
					/*$('#dg_Tamizaje').datagrid({
					  pagination:true,
					  pageSize:10,
					  remoteFilter:false
					});		
					
					$(function(){
						var dg =$('#dg_Tamizaje').datagrid({data:getDataTamizaje()}).datagrid({			
						//var dg =$('#dg').datagrid({
							filterBtnIconCls:'icon-filter'
						});
						
						dg.datagrid('enableFilter');
					});	*/
					
			$.extend( $( "#FechaTamizaje" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#FechaTamizaje" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaTamizaje" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 
						
			$(function(){
				var dg = $('#dg_Tamizaje').datagrid({
					remoteFilter: false,
					pagination: true,
					pageSize: 20,
					pageList: [10,20,50,100]
				});
				
				//dg.datagrid('enableFilter');	
				FilterFechaActualTamizaje();
				showFiltersTamizaje();							
				dg.datagrid('loadData', getDataTamizaje());	
				
				function FilterFechaActualTamizaje(){
					
					FechaActual=ObtenerFechaActual();					
					dg.datagrid('addFilterRule', {
						field: 'FechaTamizaje',
						type:'datebox',
						op: 'contains',
						value: FechaActual
					});
				}//fin function FilterFechaActualTamizaje	
				
				function showFiltersTamizaje(){
					dg.datagrid('enableFilter', [{	
						/*field:'costvalue',
						type:'numberbox',
						options:{precision:1},
						op:['equal','notequal','less','greater']
					},{					
						field:'FechaTamizaje',
						type:'datebox'//,
						//options:{precision:1},
						//op:['equal']
					},{*/
						field:'ReactivoTamizaje',
						type:'combobox',
						options:{
							panelHeight:'auto',
							data:[{value:'',text:'Todos'},{value:'SI',text:'SI'},{value:'NO',text:'NO'},{value:'F',text:'F'}],
							onChange:function(value){
								if (value == ''){
									dg.datagrid('removeFilterRule', 'ReactivoTamizaje');
								} else {
									dg.datagrid('addFilterRule', {
										field: 'ReactivoTamizaje',
										op: 'equal',
										value: value
									});
								}
								dg.datagrid('doFilter');
							}
						}						
						
					}]);					
				}//fin function showFiltersTamizaje	
          });			  		
				

			</script>
			 
		</div>
		<!--Fin Tamizaje-->		
		
	</div>   
</body>
</html>