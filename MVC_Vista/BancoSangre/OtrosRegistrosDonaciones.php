<html>
<head>
	<meta charset="UTF-8">
	<title>Consultas para todos</title>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css"> 
         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >			
			function RegistroBUT(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegistroBUT&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>";			
			}
			function AbrirSolicitud(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=AbrirSolicitud&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>";		
			}
		</script>            
        
</head>    
        
<body>

    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteExcel1&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:90%;;height:auto;"title="Banco de Sangre: OTROS REGISTROS DONACIONES DE SANGRE" iconCls="icon-search" align="left"> 	
              
       <table width="621">
            <tr>
              <td width="2%">&nbsp;</td>
              <td width="30%">&nbsp;</td>
              <td width="5%">&nbsp;</td>
              <td width="5%">&nbsp;</td>
            </tr>                        
            <tr>
              <td>&nbsp;</td>
              <td>1. Registro de Bolsa Unitaria Transfundida</td>
              <td>
              <div class="buttons" align="center">
              	<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-add'" onClick="RegistroBUT();">Registrar</button> 			  
              </div>
              </td>
              <td>&nbsp;</td>
            </tr>
            
            <?php 
			$IdListItem=isset($_GET['IdListItem']) ? $_GET['IdListItem'] : '';
			$Listar_Permisos_Usuario=Listar_Permisos_Usuario_M($_GET['IdEmpleado'],$IdListItem);
			$Modificar=$Listar_Permisos_Usuario[0]["Modificar"];
			if($Modificar>=1){
			?>
             <tr>
              <td>&nbsp;</td>
              <td  align="left">2. Abrir Solicitudes de transfusión cerradas erroneamente</td>
              <td>
              <div class="buttons" align="center">
              	<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-edit'" onClick="AbrirSolicitud();">Abrir Solicitud</button> 			  
              </div>
              </td>
              <td>&nbsp;</td>
            </tr>
            <?php } ?>
            
            <!--<tr>
              <td>&nbsp;</td>
              <td>2. Listado de Receptores (Pacientes)</td>
              <td>
              <div class="buttons" align="center">
              	<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onClick="ConsultarReceptores();">Consultar</button> 			  
              </div>
              </td>
            </tr>-->
        </table>              
     	<br> 	
     </div>
</form>
      
 
  
      
</body>
</html>