<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consulta de Donantes Reactivos</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
		
			//////2. FILTRAR COMBOGRID Apellidos y Nombres POSTULANTE 
			$(function(){	
				
				$('#ApellidosNombresBus').combogrid({ //Filtrar Postulante
					panelWidth:400,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarDonante',
					idField:'NroDocumento',
					textField:'NombresPostulante',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = BuscarPostulantesApellidosNombres(); }, //esta funcion llama cuando seleccionas el Postulante						
					columns:[[							
						{field:'NombresPostulante',title:'Apellidos y Nombres',width:150},
						{field:'GrupoSanguineoPostulante',title:'G.Sanguineo',width:35},
						{field:'DescripcionEApto',title:'Tamizaje',width:50}					
					]]
				});	//FIN 
				
				$('#NroDocumentoBus').combogrid({ //Filtrar Postulante
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FiltrarDonante',
					idField:'NombresPostulante',
					textField:'NroDocumento',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = BuscarPostulantesDNI(); }, //esta funcion llama cuando seleccionas el Postulante						
					columns:[[							
						{field:'NroDocumento',title:'Nro Documento',width:40},
						{field:'GrupoSanguineoPostulante',title:'G.Sanguineo',width:35},
						{field:'DescripcionEApto',title:'Tamizaje',width:50}							
					]]
				});	//FIN 
											
			});				
			
			$.extend( $( "#FechaPrueba" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#FechaPrueba" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaInicio" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 
			
			function salir(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Consultas&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
	

			function RegPruebaConfirmatoria(){					
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){						
					if(rowp.ResultadoFinal.trim()==""){  
					 	$('#dlg-Tabuladura').dialog('open').dialog('setTitle','Registrar Prueba Confirmatoria - Donacion: '+rowp.NroDonacion);	
						$('#NroDonacion').textbox('setValue', rowp.NroDonacion);	
						document.getElementById("IdTamizaje").value=rowp.IdTamizaje;
						document.getElementById("PruebaReactivo").value=rowp.PruebaReactivo;	
						document.getElementById("NroDocumentoPostulante").value=rowp.NroDocumento;
					}else{
						$.messager.alert('Mensaje de Información', 'Prueba Confirmatoria YA REGISTRADA','warning'); 
					}	
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar un Donante Reactivo','warning');						
				}
			}

	function GuardarPruebaConfirmatoria(){	
		var DocPruebaConfirma=$('#DocPruebaConfirma').textbox('getValue');	
		var res = DocPruebaConfirma.substr(-4, 4); //.pdf		
		
		$('#fmTabuladura').form('submit', {			
			onSubmit: function(){			
				// return false to prevent submit;				
				if($(this).form('validate')==false){					
					return $(this).form('validate');					
				}else if(res!='.pdf') {
					$.messager.alert('Mensaje de Información', 'El Archivo NO es un PDF','warning');
					return 0;				
				}else{								
					//alert(res);return 0;
					$.messager.confirm('Mensaje de Confirmación', '¿Seguro de Guardar la Prueba Confirmatoria?', function(r){
						if (r){
							$('#fmTabuladura').submit();					
						}
				   });
				   return 0;
				}		
			},
			success:function(data){
				$('#fmTabuladura').submit();
			}
		});		 
	}

		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>  

        <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:60px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				/*margin-left:10px;*/
            }
			.fitem2 label{
                display:inline-block;
                width:120px;	
				margin-left:10px;			
            }
			.fitem2 input {	
				width:140px;		 
			}
	</style>    
        
<body>

	 <!--FORMULARIO REGISTRAR TABULADURA-->
 	 <div id="dlg-Tabuladura" class="easyui-dialog" style="width:700px;height:250px;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
           	<form id="fmTabuladura" name="fmTabuladura" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarPruebaConfirmatoria&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
                   
              <div class="fitem2">
              		<input type="hidden" name="IdTamizaje" id="IdTamizaje" />  
              		<input type="hidden" name="PruebaReactivo" id="PruebaReactivo" /> 
              		<input type="hidden" name="NroDocumentoPostulante" id="NroDocumentoPostulante" />               		               
                    <label>Nro Donacion:</label>
                    <input type="text" class="easyui-textbox" name="NroDonacion" id="NroDonacion" readonly />
                    <label>Responsable:</label>
                    <select style="width:200px" class="easyui-combobox" id="IdResponsablePrueba" name="IdResponsablePrueba" data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
					  	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
						$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                      $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>
              </div>              
              <div class="fitem2">
               		<label>Documento PDF:</label>  
                    	<!--  <input type="file" name="DocPruebaConfirma" id="DocPruebaConfirma" />	 -->			
					  <input id="DocPruebaConfirma" name="DocPruebaConfirma"  class="easyui-filebox" data-options="prompt:'Escoge un archivo...',required:true" /> 

					<label>Fecha Prueba :</label>  
                    <input type="text" class="easyui-datebox" name="FechaPrueba" id="FechaPrueba" value="<?php echo date('d/m/Y'); ?>" data-options="prompt:'Fecha Prueba',required:true"/>
              </div> 

              <div class="fitem2">
               		<label>Resultado Final:</label>  
					<select style="width:140px" class="easyui-combobox" id="ResultadoFinal" name="ResultadoFinal" data-options="prompt:'Seleccione',required:true">
					  <option value="0">NO REACTIVO</option>	
                      <option value="1">REACTIVO</option>                      
                    </select>
                    <label>Valor medición:</label> 
              		<input class="easyui-numberbox" name="ValorMedicion" id="ValorMedicion" data-options="prompt:'Valor medición',min:0,precision:2,required:true" value="0" />   
              </div>

              <div class="fitem2">
              		<label>Comentario:</label>  
                    <input style="width:150px;height:70px" class="easyui-textbox" multiline="true" name="Comentario" id="Comentario" data-options="prompt:'Comentario',required:true"/>
              </div>      
               <!--<input type="submit" value="registar" >--> <!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="GuardarPruebaConfirmatoria();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-Tabuladura').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO REGISTRAR TABULADURA-->  

		<div style="margin:0px 0;"></div>    
        <div id="tb" style="padding:5px;height:auto">
       		<div style="margin-bottom:5px">
                <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="RegPruebaConfirmatoria()">Prueba Confirmatoria</a>
                <!--<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Actualizar Recepción</a> -->
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>                
                <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="salir();">Salir</a> -->
        	</div>
        </div> 

          <form name="form1" id="form1" action="../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Emergencia_ListarPacientes" method="post" >
            <fieldset>
			  <legend style="color:#03C"><strong>Busqueda de Donantes:</strong></legend>
				<table width="100%" style="font-size:12px;">
				  <tr align="center">
					<th bgcolor="#D6D6D6"><strong>Nro Documento</strong></th>
					<th bgcolor="#D6D6D6"><strong>Apellidos y Nombres</strong></th>
				  </tr>
				  <tr align="center">
					<td><!--<input class="easyui-numberbox" style="width:110px" id="NroDocumentoBus" name="NroDocumentoBus" data-options="prompt:'Nº DNI'" maxlength="8">--><input name="NroDocumentoBus" type="text" id="NroDocumentoBus" class="easyui-combogrid" style="width:120px;" data-options="prompt:'Nº DNI',validType:'justNumber'" ></td><!-- searcher:BuscarPostulantesDNI,-->
					<td><input class="easyui-combogrid" style="width:200px" id="ApellidosNombresBus" name="ApellidosNombresBus"  data-options="prompt:'Apellidos y Nombres',validType:'justText'"></td> 
				  </tr>                         
				</table> 
            </fieldset>   
            </form> 
             
            <table width="94%" style="font-size:12px;">				   
              <tr>
                <td width="2%">&nbsp;</td>
                <td width="10%">Nro Documento</td>
                <td width="10%"><input name="NroDocumento" type="text" id="NroDocumento" class="easyui-textbox" style="width:100px;" readonly /></td>
                <td width="10%">Apellidos y Nombres</td>
                <td width="25%"><input name="ApellidosNombres" type="text" id="ApellidosNombres" class="easyui-textbox" style="width:300px;" readonly /></td> 
                <td width="10%">Grupo Sanguineo</td>
                <td width="10%"><input name="GrupoSanguineo" type="text" id="GrupoSanguineo" class="easyui-textbox" style="width:100px;" readonly /></td>
                <td width="10%">Estado Tamizaje</td>
                <td width="15%"><input name="DescripcionEApto" type="text" id="DescripcionEApto" class="easyui-textbox" style="width:100px;" readonly /></td>
              </tr>       
            </table>                 
          
     		        
              

        
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Consulta de Donantes Reactivos" style="width:auto;height:auto" data-options="		
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:true,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	   <th field="NroDocumento" width="30">Documento</th>
            	   <th field="FechaTamizaje" width="40">Fecha Tamizaje</th>
            	   <th field="FechaExtraccion" width="40">Fecha Extracción</th>                   
                   <th field="SNCS" width="20">SNCS</th>
                   <th field="GrupoSanguineoPostulante" width="20">G.S.Postulante</th>
                   
                   <th field="PruebaReactivo" width="30">Prueba Reactivas</th>
                   <th field="ObservacionTamizaje" width="30">ObservacionTam.</th>
                  
                   <th data-options="field:'NombreDoc',formatter:verpdf" align="center" width="30">Doc.Prueba-Confirma</th>

                   <th field="ResultadoFinal" width="15">Res.Final</th>
                   <th field="Comentario" width="15">Comentario</th>                   
                   <th field="FechaPrueba" width="15">Fecha Reg.</th>                   
                   <th field="ResponsablePrueba" width="15">Usuario Reg.</th>
                   <th field="EstadoDefinitivo" width="15">Estado</th>            
                   <!--<th field="Accion" width="120">Acción</th>-->
                   <!--formatter="formatPrioridad"-->                   	
			</tr>
		</thead>
	</table>
     
   <script type="text/javascript">
   	$("#dg").datagrid({
		// Fires when data in datagrid is loaded successfully
		onLoadSuccess:function(){	
			// Get this datagrid's panel object
			$(this).datagrid('getPanel')
			// for all easyui-linkbutton <a>'s make them a linkbutton
			.find('a.easyui-linkbutton').linkbutton();
		}
    });
   	function verpdf(val,row){
   		if(row.ResultadoFinal.trim()!=""){  
		 		return '<a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerPdf&IdPruebaConfirmatoria='+row.IdPruebaConfirmatoria+'" class="easyui-linkbutton" iconCls="icon-pdf" target="_blank">'+val+'</a>';  // onclick="VerPDFseleccionado('+row.IdPruebaConfirmatoria+');"
		}else{
			return '';   
		}				
    }	

    /*function VerPDFseleccionado(IdPruebaConfirmatoria){		
		location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerPdf&IdPruebaConfirmatoria="+IdPruebaConfirmatoria;	
	}*/

   $.extend($('#NroDocumentoBus').searchbox.defaults.rules, {
        justNumber: {
            validator: function(value, param){
                 return value.match(/[0-9]/);
            },
            message: 'Porfavor digite sólo numeros'  
        }
    });	
	$.extend($('#ApellidosNombresBus').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor digite sólo texto'  
        }
    });
	
	function BuscarPostulantesDNI(){
		
		//var NroDocumentoBus=$('#NroDocumentoBus').combogrid('getText'); 
		//$('#NroDocumento').textbox('setValue', NroDocumentoBus);
		var g = $('#NroDocumentoBus').combogrid('grid');	// get datagrid object
		var r = g.datagrid('getSelected');	// get the selected row
		$('#NroDocumento').textbox('setValue', r.NroDocumento);
		$('#GrupoSanguineo').textbox('setValue', r.GrupoSanguineoPostulante);
		$('#DescripcionEApto').textbox('setValue', r.DescripcionEApto2);	
		
		var ApellidosNombres=$('#NroDocumentoBus').combogrid('getValue');
		$('#ApellidosNombres').textbox('setValue', ApellidosNombres);
		
		$('#ApellidosNombresBus').combogrid('setValue', '');//LIMPIAR BUSQUEDA APELLIDOS Y NOMBRES	
		BuscarPostulantes()
	}
	
	function BuscarPostulantesApellidosNombres(){
		//var ApellidosNombres=$('#ApellidosNombresBus').combogrid('getText');
		//$('#ApellidosNombres').textbox('setValue', ApellidosNombres);
		var g = $('#ApellidosNombresBus').combogrid('grid');	// get datagrid object
		var r = g.datagrid('getSelected');	// get the selected row
		$('#ApellidosNombres').textbox('setValue', r.NombresPostulante);
		$('#GrupoSanguineo').textbox('setValue', r.GrupoSanguineoPostulante);
		$('#DescripcionEApto').textbox('setValue', r.DescripcionEApto2);	
		
		var NroDocumento=$('#ApellidosNombresBus').combogrid('getValue');
		$('#NroDocumento').textbox('setValue', NroDocumento);		
		
		$('#NroDocumentoBus').combogrid('setValue', '');//LIMPIAR BUSQUEDA NRO DOCUMENTO
		BuscarPostulantes()
	}
	
	function BuscarPostulantes() {  
	
	 var ok = true;
	 var msg = "Falta completar los siguientes datos:</br>";	  
	     //alert('holaa');
	  /*if ($('#NroDocumentoBus').numberbox('getText')!="" && $('#NroDocumentoBus').numberbox('getText').length < 8){
                msg += "- DNI debe Tener 8 Digitos </br>";
                ok = false;
      }*/ 
	  
	  var NroDocumentoBus=$('#NroDocumentoBus').combogrid('getText'); 	  
	  if(NroDocumentoBus!=''){
		  //var NroDocumento=NroDocumentoBus; 
		 var g = $('#NroDocumentoBus').combogrid('grid');	// get datagrid object
		 var r = g.datagrid('getSelected');	// get the selected row
		 NroDocumento=r.NroDocumento; 		
	  }else{
		var NroDocumento= $('#ApellidosNombresBus').combogrid('getValue');	   
	  }
		 
	  if (ok != false) {
		 // $.messager.confirm('SIGESA', 'Seguro de guardar la información: ', function(r){
			//if (r){
				 $.ajax({
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteDonantesReactivos',
					type: 'POST',
					dataType: 'json',
					data: {
						//IdEmpleado :document.getElementById('IdEmpleado').value, 
						NroDocumento  :   NroDocumento
					},							
					success: function (data) {		
						//document.getElementById('IdPaciente').value=data.IdPaciente;	
						//$('#ApellidosNombres').textbox('setValue', data.Postulante);
					}
				}).done(function (data) {
                    console.log(data);
                    //alert(data);
						$('#dg').datagrid({
							data: data
						});
						
					    if(data=="") { //if(!data.success){						    
							//alert('NO HAY DATOS');			
							$.messager.alert('SIGESA','NO ES DONANTE REACTIVO' ,'info');			
						}else{
							
						
						}//END else
				});			
				
                // stop the form from submitting the normal way and refreshing the page
                //event.preventDefault();
            }else{
				//alert(msg);
				$.messager.alert('SIGESA',msg ,'error');
			}   
	   	   	
	}		

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>