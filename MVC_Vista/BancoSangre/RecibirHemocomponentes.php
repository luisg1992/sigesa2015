<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Recibir Hemocomponentes</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
    <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 #fm{
                margin:0;
                padding:10px 20px;
            }
			#fmCabHemocomponente{
                margin:0;
                padding:10px 20px;
            }
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:90px;
							
            }
            .fitem input {
				display:inline-block;
                width:120px;	
				/*margin-left:10px;*/			
            }						
			
	</style> 
    
<script type="text/javascript" >
	//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
	$.extend($.fn.validatebox.defaults.rules,{
		exists:{
			validator:function(value,param){
				var cc = $(param[0]);
				var v = cc.combobox('getValue');
				var rows = cc.combobox('getData');
				for(var i=0; i<rows.length; i++){
					if (rows[i].id == v){return true}
				}
				return false;
			},
			message:'El valor ingresado no existe.'
		}
	});	

	$(function(){
		
		$('#IdEstablecimientoIngresoB').combogrid({
			panelWidth:650,
			value:'<?php echo $IdEstablecimientoIngreso ?>',
			url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarEstablecimientos',
			idField:'IdEstablecimiento',
			textField:'Nombre',
			editable: true,
			//required: true,
			mode:'remote',
			fitColumns:true,
			onSelect: function(rec){
			var url = document.form1.submit(); },						
			columns:[[		
				{field:'Codigo',title:'Codigo',width:60},				
				{field:'Nombre',title:'Nombre',width:550}						
			]]
		});		
				
		$('#Establecimientos').combogrid({
			panelWidth:650,
			value:'<?php echo $IdEstablecimientoIngreso ?>',
			url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarEstablecimientos',
			idField:'IdEstablecimiento',
			textField:'Nombre',
			editable: true,
			required: true,
			mode:'remote',
			fitColumns:true,
			onSelect: function(rec){
			var url = cambiarEstablecimientos(); },						
			columns:[[		
				{field:'Codigo',title:'Codigo',width:60},				
				{field:'Nombre',title:'Nombre',width:550}						
			]]
		});	
		
		document.getElementById('IdEstablecimientoIngreso').value='<?php echo $IdEstablecimientoIngreso ?>';
		
		$('#TipoHem').combobox({	
			editable: false,
			required: true								
		});	
		
		$('#UsuarioRecibe').combobox({	
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#UsuarioRecibe"]',
			filter: function (q, row) {
			return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
			}								
		});						
		$('#UsuarioRecibe').combobox('validate');
		
		$('#IdGrupoSanguineo').combobox({	
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#IdGrupoSanguineo"]',
			filter: function (q, row) {
			return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0;  
			}							
		});						
		$('#IdGrupoSanguineo').combobox('validate');								
									
	});
	
	function cambiarEstablecimientos(){
		var Establecimientos= $('#Establecimientos').combobox('getValue');
		document.getElementById('IdEstablecimientoIngreso').value=Establecimientos;
	}
	
		$.extend($("#FechaRecibe").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaRecibe").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FechaNacimiento" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});
		
/*function CambiaEmpleado(){
	var Empleado = $('#Empleado').combobox('getValue');	
	var porciones=Empleado.split('|');
	var id=porciones[0];
	var FecIngreso=porciones[1];
	document.getElementById('IdEmple').value=id;
	$( "#FecIngreso" ).textbox('setValue', FecIngreso);

}

function cambiarDias(){	
	//alert('dsjky');
	var FecInicio = $('#FecInicio').datebox('getValue');
	var FecFinal = $('#FecFinal').datebox('getValue');	
	        fecha1=FecInicio.split('/');
			f1=new Date(fecha1[2], fecha1[1]-1 , fecha1[0]);
			fecha2= FecFinal.split('/');
			f2 = new Date(fecha2[2], fecha2[1]-1 , fecha2[0]);			
			if(f2<f1){
				$.messager.alert('Mensaje','La fecha final debe ser mayor a la fecha de inicio','info');
				$("#FecFinal").datebox('setValue','');
				$("#Dias").datebox('setValue','');
				$('#FecFinal').next().find('input').focus();
				return 0;			
			}else if(FecInicio==''||FecFinal==''){
				$("#Dias").datebox('setValue','');
				return 0;			
			}else{
			 var aFecha1 = FecInicio.split('/'); 
			 var aFecha2 = FecFinal.split('/'); 
			 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
			 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
			 var dif = fFecha2 - fFecha1;
			 var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	         $( "#Dias" ).textbox('setValue', dias+1);
			}
	
}*/
		
		/////////MANTENIMIENTO CABVACION
		var url;		
		function nuevaCabHemocomponente(){
			//$('#Empleado').combobox('clear');		
			//$('#FecIngreso').textbox('clear');
			$('#dlg-CabHemocomponente').dialog('open').dialog('setTitle','Nuevo Hemocomponente a Recibir');
			url="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarRecibirHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>";
		}
		
		function saveCabHemocomponente(){
			//var IdEstablecimientoIngreso=$('#IdEstablecimientoIngreso').combobox('getValue');
			var IdEstablecimientoIngreso=document.getElementById('IdEstablecimientoIngreso').value;
			if(IdEstablecimientoIngreso.trim()=="" || IdEstablecimientoIngreso.trim()=="0"){			
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione un Hospital Origen',
					icon:'warning',
					fn: function(){
						$('#Establecimientos').next().find('input').focus();
					}
				});
				$('#Establecimientos').next().find('input').focus();
				return 0;				
			}
			
			var g = $('#Establecimientos').combogrid('grid');	// get datagrid object
			var r = g.datagrid('getSelected');	// get the selected row
				
			if(r!=null){
				var Codigo=r.Codigo; //alert(Codigo);	
				if(parseInt(Codigo)=="6218"){		
					$.messager.alert({
						title: 'Mensaje',
						msg: 'No puede RECIBIR del Hospital Seleccionado',
						icon:'warning',
						fn: function(){
							$('#Establecimientos').next().find('input').focus();
						}
					});
					$('#Establecimientos').next().find('input').focus();
					return 0;				
				}
			}else{
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Busque un Hospital Destino Valido',
					icon:'warning',
					fn: function(){
						$('#Establecimientos').next().find('input').focus();
					}
				});
				$('#Establecimientos').next().find('input').focus();
				$('#Establecimientos').combogrid('validate');	
				return 0;					
			
			}		
			
			var UsuarioRecibe=$('#UsuarioRecibe').combobox('getValue');
			 if(UsuarioRecibe==""){
				$.messager.alert('Mensaje','Seleccione un Usuario Recibe','info');
				$('#UsuarioRecibe').next().find('input').focus();
				return 0;			
			}
			
			if($('#UsuarioRecibe').combobox('isValid')==false){ 
				//$.messager.alert('Mensaje','El Responsable Ingresado no Existe','info');
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione Responsable Válido',
					icon:'warning',
					fn: function(){
						$('#UsuarioRecibe').next().find('input').focus();
					}
				});
				$('#UsuarioRecibe').next().find('input').focus();
				return 0;			
			}
			
			var TipoHem=$('#TipoHem').combobox('getValue');
			if(TipoHem.trim()==""){
				$.messager.alert('Mensaje','Seleccione un Tipo de Hemocomponente','info');
				$('#TipoHem').next().find('input').focus();
				return 0;			
			}			
			
			var SNCS=$('#SNCS').numberbox('getValue');
			 if(SNCS==""){
				$.messager.alert('Mensaje','Ingrese un SNCS','info');
				$('#SNCS').next().find('input').focus();
				return 0;			
			}
			
			var NroDonacion=$('#NroDonacion').textbox('getValue');
			 if(NroDonacion==""){
				$.messager.alert('Mensaje','Ingrese un Nro Donacion','info');
				$('#NroDonacion').next().find('input').focus();
				return 0;			
			}
			
			var IdGrupoSanguineo=$('#IdGrupoSanguineo').combobox('getValue');
			 if(IdGrupoSanguineo==""){
				$.messager.alert('Mensaje','Seleccione Grupo Sanguineo','info');
				$('#IdGrupoSanguineo').next().find('input').focus();
				return 0;			
			}
			
			if($('#IdGrupoSanguineo').combobox('isValid')==false){ 
				//$.messager.alert('Mensaje','El Responsable Ingresado no Existe','info');
				$.messager.alert({
					title: 'Mensaje',
					msg: 'Seleccione Grupo Sanguineo Válido',
					icon:'warning',
					fn: function(){
						$('#IdGrupoSanguineo').next().find('input').focus();
					}
				});
				$('#IdGrupoSanguineo').next().find('input').focus();
				return 0;			
			}
			
			var VolumenTotRecol=$('#VolumenTotRecol').numberbox('getValue');
			 if(VolumenTotRecol==""){
				$.messager.alert('Mensaje','Ingrese Volumen','info');
				$('#VolumenTotRecol').next().find('input').focus();
				return 0;			
			}
			
			var NroTabuladora=$('#NroTabuladora').textbox('getValue');
			 if(NroTabuladora==""){
				$.messager.alert('Mensaje','Imgrese Nro Tubuladora','info');
				$('#NroTabuladora').next().find('input').focus();
				return 0;			
			}
			
			var FechaExtraccion=$('#FechaExtraccion').datebox('getValue');
			 if(FechaExtraccion==""){
				$.messager.alert('Mensaje','Ingrese una Fecha Extraccion','info');
				$('#FechaExtraccion').next().find('input').focus();
				return 0;			
			}	
						
			var FechaVencimiento=$('#FechaVencimiento').datebox('getValue');
			 if(FechaVencimiento==""){
				$.messager.alert('Mensaje','Ingrese una Fecha Vencimiento','info');
				$('#FechaVencimiento').next().find('input').focus();
				return 0;			
			}		
			//document.getElementById("fmCabHemocomponente").submit();
			$.messager.confirm('Mensaje', '¿Seguro de Recibir el Hemocomponente '+ TipoHem +'-'+ SNCS +'?', function(r){
				if (r){
					document.getElementById("fmCabHemocomponente").submit();
				}
			});	
			
		}	
					
		
		function regresar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=StockSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}	
		
		function Buscar(){
			document.getElementById("form1").submit();
		}		
	
    </script>
</head>
<body>

    <!--<p>This sample shows how to implement client side pagination in DataGrid.</p>-->
	<div style="margin:0px 0;"></div>
	 <form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RecibirHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
      
	    <div id="tb1" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">            	 
		        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="nuevaCabHemocomponente()">Nuevo Hemocomponente</a>
				<!--<a href="#" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="nuevoDetHemocomponente()" >Nuevo Detalle</a>							
                <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="declarardetalle();">Declarar & Eliminar Detalle </a> -->               
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="verDetHemocomponente();">Imprimir</a>	
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="ayuda();">Ayuda</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a>
			</div>
            
              <div>
               <strong>Busqueda por Hospital Origen:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
                    <?php  $emple = ListarEstablecimientos_M('0',''); ?>
                    <select name="IdEstablecimientoIngresoB" id="IdEstablecimientoIngresoB" class="easyui-combogrid" style="width:300px" data-options="
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" > <!-- onchange="submit()"--> 
                        <option value="0">Todos</option>
                        <?php foreach($emple as $empleitem){?>
                        <option value="<?php echo $empleitem["IdEstablecimiento"]?>" <?php if($empleitem["IdEstablecimiento"]==$IdEstablecimientoIngreso){?> selected <?php } ?>><?php echo (mb_strtoupper($empleitem["Nombre"]));?></option> <!-- -->
                        <?php }	?>
                    </select> &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Fecha Recepcción:</strong> &nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="FechaRecibeB" id="FechaRecibeB" class="easyui-datebox" value="<?php echo $FechaRecibeB; ?>" validType="validDate" style="width:105px" data-options="required:true,
                    valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" />
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>   
                </div>
            			
		</div>
            
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Recibir Hemocomponentes" style="width:1100px;height:450px" data-options="
				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	<th field="NroDonacion" width="80">N°Donacion</th>
				<th field="TipoHem"  width="100">Hemocomponente</th>				
                <th field="SNCS"  width="80">SNCS</th>	
				<th field="GrupoSanguineo"  width="70">GS</th>
                <th field="VolumenRestante"  width="100">Volumen</th>
				<th field="NroTabuladora" width="80">tubuladura</th>
                <th field="FechaExtraccion" width="100">F. Extracción</th>
                <th field="FechaVencimiento" width="100">F. Vencimiento</th>				
                <th field="EstablecimientoIngreso" width="250">Hosp. Origen</th>
			</tr>
		</thead>
	</table>
     </form>
    
   
 <!--FORMULARIO NUEVO-->
 <div id="dlg-CabHemocomponente" class="easyui-dialog" style="width:600px;height:auto;"
			closed="true" buttons="#dlg-buttons">
            <!--<div class="ftitle">Datos del Equipo</div>-->
   <form id="fmCabHemocomponente" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarRecibirHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
            <div class="fitem">
            		<label>Fecha Recibe:</label>                   
                    <input name="FechaRecibe" id="FechaRecibe" class="easyui-datebox" value="<?php echo $FechaRecibeB; ?>" validType="validDate" data-options="required:true" />  
                    
                    <label>Hospital Origen:</label>
                    <select style="width:165px" class="easyui-combobox" id="Establecimientos" name="Establecimientos" ></select>
                	<input type="hidden" name="IdEstablecimientoIngreso" id="IdEstablecimientoIngreso" value="0" />                    
                               
             </div>   
              
             <div class="fitem">
              		 <label>Responsable:</label>
                    <Select name="UsuarioRecibe" id="UsuarioRecibe" class="easyui-combobox"  style="width:122px"  data-options="prompt:'Seleccione',required:true">
                      <option value=""></option>
                      <?php
                                          $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
                                          $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                          $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                      <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                      <?php } } ?>
                    </select>     
                    <label>Tipo Hemoc-SNCS:</label>                    
                    <select name="TipoHem" id="TipoHem" class="easyui-combobox" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = cambiarFechaExtraccion(); }" >
                        <option value=""></option>
                        <option value="PG">PG </option>
                        <option value="PFC">PFC </option>
                        <option value="PQ">PQ </option>
                        <option value="CRIO">CRIO </option>
                    </select>-<input id="SNCS"  name="SNCS" type="text" class="easyui-numberbox" maxlength="7" value="" data-options="prompt:'SNCS'" />        
              </div>  
              
              <div class="fitem">
              		<label>Nro Donación:</label>
                    <input id="NroDonacion"  name="NroDonacion" type="text" class="easyui-textbox" maxlength="10" data-options="prompt:'Nro Donacion'" />                                              
                    <label>Grupo Sanguineo:</label>                    
                    <select class="easyui-combobox" name="IdGrupoSanguineo" id="IdGrupoSanguineo"  style="width: 122px" >
                    <option value=""></option>
                    <?php
                          $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
                           if($listarSexo != NULL) { 
                             foreach($listarSexo as $item){?>
                    <option value="<?php echo $item["IdGrupoSanguineo"]?>" ><?php echo $item["IdGrupoSanguineo"].'='.$item["Descripcion"]?></option>
                    <?php } } ?>
                  </select>       
              </div> 
              
              <div class="fitem">
              		<label>Volumen(mL):</label>
                    <input id="VolumenTotRecol"  name="VolumenTotRecol" type="text" class="easyui-numberbox" data-options="prompt:'Volumen',required:true,min:0,precision:2"/>                                              
                    <label>Tubuladora:</label>                    
                    <input type="text" class="easyui-textbox" name="NroTabuladora" id="NroTabuladora" maxlength="8" data-options="prompt:'Tubuladura',required:true"/>  
              </div>
              
              <div class="fitem">
              		<label>F.Extracción:</label>
                    <input name="FechaExtraccion" id="FechaExtraccion" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = cambiarFechaExtraccion(); }" />                                             
                    <label>F.Vencimiento:</label>                    
                    <input name="FechaVencimiento" id="FechaVencimiento" class="easyui-datebox" value="" validType="validDate" data-options="required:true" />      
              </div>                   
               <!--<input type="submit" value="registar" >--><!--para probar guardar aqui si muestra errores-->         
            </form>
        </div>
        
       <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveCabHemocomponente();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-CabHemocomponente').dialog('close')" style="width:90px">Cancelar</a>
	  </div>

<!--FIN FORMULARIO NUEVO-->   
    
    
    
   <script>
   
   function cambiarFechaExtraccion(){	   
	 var TIDPG='<?php echo $TIDPG ?>';// 42
	 var TIDPFC='<?php echo $TIDPFC ?>';// 365
	 var TIDPQ='<?php echo $TIDPQ ?>';// 5	
	 
	 var TipoHem=$('#TipoHem').combobox('getValue');
	 var tiempo=0;
	 
	 if(TipoHem=='PG'){
		var tiempo=TIDPG;
	 }else if(TipoHem=='PFC'){
		var tiempo=TIDPFC; 
	 }else if(TipoHem=='PQ'){
		var tiempo=TIDPQ; 
	 }else if(TipoHem=='CRIO'){
		var tiempo=TIDPFC;
	 }
	 	
	if(tiempo==0){ 
		$('#FechaVencimiento').datebox('setValue','');		
		$.messager.alert({
			title: 'Mensaje',
			msg: 'Seleccione Tipo de Hemocomponente',
			icon:'info',
			fn: function(){
				$('#TipoHem').next().find('input').focus();
			}
		});	
		$('#TipoHem').next().find('input').focus();
		return 0;			
	}
	
	var FechaExtraccion=$('#FechaExtraccion').datebox('getValue');//06/07/2018	 					
	if(FechaExtraccion==""){ 
		$('#FechaVencimiento').datebox('setValue','');		
		$.messager.alert({
			title: 'Mensaje',
			msg: 'Ingrese Fecha Extracción',
			icon:'info',
			fn: function(){
				$('#FechaExtraccion').next().find('input').focus();
			}
		});	
		$('#FechaExtraccion').next().find('input').focus();
		return 0;			
	}						
		
	farray= FechaExtraccion.split('/');					
	var y = parseInt(farray[2]);
	var m = parseInt(farray[1]);
	var d = parseInt(farray[0]);
	FechaVencimiento=(d+parseInt(tiempo))+'/'+m+'/'+y;												
	$('#FechaVencimiento').datebox('setValue',FechaVencimiento);
		   
   }
	
	$(function(){	  
	  $('#NroTabuladora').textbox('textbox').attr('maxlength', $('#NroTabuladora').attr("maxlength"));	
	  $('#NroDonacion').numberbox('textbox').attr('maxlength', $('#NroDonacion').attr("maxlength"));
	  $('#SNCS').numberbox('textbox').attr('maxlength', $('#SNCS').attr("maxlength"));
	  	 
	});	
	
		function getData(){
			var rows = [];			
			
	<?php 
	
	 $BuscarSNCSRecibido = BuscarSNCSRecibido_M($IdEstablecimientoIngreso,$FechaRecibe);
     $i = 1;										
	if($BuscarSNCSRecibido!=NULL){
		foreach($BuscarSNCSRecibido as $item)
		{
						
			 ?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TipoHem: '<?php echo $TipoHem=utf8_encode($item["TipoHem"]);?>',
					NroDonacion: '<?php echo $NroDonacion=$item["NroDonacion"]; ?>',											
					SNCS:'<?php echo  $item["SNCS"]; ?>',
					GrupoSanguineo:'<?php echo  $item["GrupoSanguineo"]; ?>',
					VolumenRestante:'<?php echo  $item["VolumenRestante"]; ?>',					
					NroTabuladora:'<?php echo  $item["NroTabuladora"]; ?>',
					FechaExtraccion:'<?php echo  vfecha(substr($item["FechaExtraccion"],0,10)); ?>',				
					FechaVencimiento:'<?php echo  vfecha(substr($item["FechaVencimiento"],0,10)); ?>',
					IdEstablecimientoIngreso:'<?php echo  $item["IdEstablecimientoIngreso"]; ?>',
					EstablecimientoIngreso:'<?php echo mb_strtoupper($item["EstablecimientoIngreso"]); ?>',
			
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});
		
		</script> 
     
   
	
  
</body>
</html>