<?php 
	$IdExamenMedico=$_REQUEST['IdExamenMedico']; 
	$data=Listar_ExamenMedicoM($IdExamenMedico); 	   
		 	
	$IdDocIdentidad=$data[0]["IdDocIdentidad"];
	$NroDocumento=$data[0]["NroDocumento"];
	$NombresPostulante=$data[0]["ApellidoPaterno"].' '.$data[0]["ApellidoMaterno"].' '.$data[0]["PrimerNombre"].' '.$data[0]["SegundoNombre"];
	$NroMovimiento='PDAC'.$data[0]["NroMovimiento"];
	$IdTipoDonacion=$data[0]["IdTipoDonacion"];
	$IdGrupoSanguineo=$data[0]["IdGrupoSanguineo"];		
	$TipoDonacion='<font color="#0000FF">'.$data[0]["TipoDonacion"].'</font>';	
	$IdLugarColecta=$data[0]["IdLugarColecta"];	
	$IdMovimiento=$data[0]["IdMovimiento"];
	$NroDonacion=$data[0]["NroDonacion"];	

	//llamar Datos de la BD Acces Extraccion
	$ObtenerDatosSistExtraccion=ObtenerDatosSistExtraccion_M($NroDonacion);
	if($ObtenerDatosSistExtraccion!=""){
		$IdResponsableIni=$ObtenerDatosSistExtraccion[0]["IdResponsableIni"];
		$IdResponsableFin=$ObtenerDatosSistExtraccion[0]["IdResponsableFin"];
		$NroLote=$ObtenerDatosSistExtraccion[0]["NroLote"];
		if($NroLote==""){
			$NroLote=0;
		}else{
			$NroLote=$ObtenerDatosSistExtraccion[0]["NroLote"];
		}
		$SerieHemobascula=$ObtenerDatosSistExtraccion[0]["SerieHemobascula"];
		$HoraInicio=$ObtenerDatosSistExtraccion[0]["HoraInicio"].':00'; //10:55:00	
		$DuracionColeccion='00:'.$ObtenerDatosSistExtraccion[0]["DuracionColeccion"]; //00:09:53
		$HoraFin=CalcularHoraSalida($HoraInicio,$DuracionColeccion);//11:04:53
			
		$VolColeccionPre=$ObtenerDatosSistExtraccion[0]["VolColeccionPre"];
		$VolColeccionReal=$ObtenerDatosSistExtraccion[0]["VolColeccionReal"];	
		$FlujoAlto=$ObtenerDatosSistExtraccion[0]["FlujoAlto"];
		$FlujoBajo=$ObtenerDatosSistExtraccion[0]["FlujoBajo"];
		$ConInterrupcion=$ObtenerDatosSistExtraccion[0]["ConInterrupcion"];
	}else{
		$IdResponsableIni="";
		$IdResponsableFin="";
		$NroLote="";		
		$SerieHemobascula="";
		$HoraInicio=""; //10:55:00	
		$DuracionColeccion=""; //00:09:53
		$HoraFin="";//11:04:53
			
		$VolColeccionPre="";
		$VolColeccionReal="";	
		$FlujoAlto="";
		$FlujoBajo="";
		$ConInterrupcion="";
		
	}
?>

<html>
<head>
        <meta charset="UTF-8">    
        <title>Registrar Datos Extracción Sangre</title> 
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">        
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript"> 
		
			function cancelar(){ 
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Extraccion&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
		
		$.extend($("#FechaExtraccion").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaExtraccion").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FechaNacimiento" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});		
		
		/*function siguienteEnter(txt1,txt2){
			$("'#"+txt1+"'").textbox('textbox').bind('keydown', function(e){
				if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.
					//t.textbox('setValue', $(this).val());
					$("'#"+txt2+"'").next().find('input').focus();return false;	
				}
			});		
		}*/		
			
		$(function(){	
			
			//Datos Generales
			$("#IdResponsableIni").combobox('textbox').bind('keydown',function(e){
				// your code here
				if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
					$("#IdResponsableFin").next().find('input').focus();return false;	
				}
			});	
			
			$("#IdResponsableFin").combobox('textbox').bind('keydown',function(e){
				// your code here
				if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
					$("#NroLote").next().find('input').focus();return false;	
				}
			});	
			
			$("#NroLote").textbox('textbox').bind('keydown',function(e){
				// your code here
				if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
					$("#FechaExtraccion").next().find('input').focus();return false;	
				}
			});	
			
			var IdTipoDonacion=document.getElementById('IdTipoDonacion').value;				
			if(IdTipoDonacion=='1'){ //Sangre Total	
				$("#SerieHemobascula").textbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#VolColeccionPre").next().find('input').focus();return false;	
					}
				});	
				
				$("#VolColeccionPre").textbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#VolColeccionReal").next().find('input').focus();return false;	
					}
				});	
				
				/*$("#VolColeccionReal").textbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#Anular").next().find('input').focus();return false;	
					}
				});*/					
							
			}else if(IdTipoDonacion=='2'){ //AFERESIS				
				$("#SerieEquipo").textbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#VolProcesado").next().find('input').focus();return false;	
					}
				});	
				
				$("#VolProcesado").textbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#VolUsado").next().find('input').focus();return false;	
					}
				});	
				
				$("#VolUsado").textbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#VolPlaquetas").next().find('input').focus();return false;	
					}
				});	
				
				$("#VolPlaquetas").textbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#NroCiclos").next().find('input').focus();return false;	
					}
				});	
				
				$("#NroCiclos").numberbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#RendimientoEstimado").next().find('input').focus();return false;	
					}
				});	
				
				$("#RendimientoEstimado").numberbox('textbox').bind('keydown', function(e){
					if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
						$("#RendimientoObjetivo").next().find('input').focus();return false;	
					}
				});	
								
			}
			
			//Datos Detallados de la Extracción
			/*$("#Anular").combobox('textbox').bind('keydown',function(e){
				// your code here
				if (e.keyCode == 13){	// when press ENTER key, accept the inputed value.					
					$("#Observaciones").next().find('input').focus();return false;	//multiline no acepta
				}
			});	*/
			
			
		  /*$('#SerieHemobascula').textbox('textbox').attr('tabindex', $('#SerieHemobascula').attr("tabindex"));*/	
		  //$('input,select,textarea').textbox('textbox').attr('tabindex',$('input,select,textarea').attr("tabindex"));//todos		
			/*$("#form1").keypress(function(e){	
				if (e.which == 13) {									
					var tabindex = $("#SerieHemobascula").attr('tabindex');	//	SerieHemobascula deberia ser el input donde tecleo			
					var siguiente=parseInt(tabindex)+1; //increment tabindex
					//alert(siguiente);
					$('[tabindex="'+siguiente+'"]').next().find('input').focus();					
				}					
			 });*/			  
			
   	    });//fin function
		
		    //VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
						
			$(function () {				
				
				$('#IdBolsaColectora').combobox({	
					editable: false,
					required: true								
				});				
				$('#IdMezcladorBasculante').combobox({	
					editable: false,
					required: true							
				});	
				$('#IdEquipoAferesis').combobox({	
					editable: false,
					required: true							
				});
				$('#IdSetAferesis').combobox({	
					editable: false,
					required: true							
				});							
				
				$('#IdResponsableIni').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdResponsableIni"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					}								
				});						
				$('#IdResponsableIni').combobox('validate');
				
				$('#IdResponsableFin').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdResponsableFin"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					}								
				});						
				$('#IdResponsableFin').combobox('validate');	
				
				$('#MotivoRechazoDurante').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoRechazoDurante"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					}								
				});						
				$('#MotivoRechazoDurante').combobox('validate');	
				
				$('#MotivoRechazoDespues').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoRechazoDespues"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					}								
				});						
				$('#MotivoRechazoDespues').combobox('validate');			
				
			});
		
		</script>
        
        <script type="text/javascript">			
			
			function cambiarDiferimientoRechazo(){
				
				var MotivoRechazoDurante= $('#MotivoRechazoDurante').combobox('getValue');
				var MotivoRechazoDespues= $('#MotivoRechazoDespues').combobox('getValue');
				
				if(MotivoRechazoDurante.trim()!="0" || MotivoRechazoDespues.trim()!="0"){	
					//var arreglo=MotivoRechazoDurante.split("|");
					//var IdMotivoRechazoDurante=arreglo[0];				
					document.getElementById('IdMotivoRechazoDurante').value=MotivoRechazoDurante;	
					document.getElementById('IdMotivoRechazoDespues').value=MotivoRechazoDespues;						
					//$("#Anular").combobox('setValue',"0"); //0=Si 1=No	
					document.getElementById('radioSI').checked=true;	
							
				}else{					
					document.getElementById('IdMotivoRechazoDurante').value='';				
					document.getElementById('IdMotivoRechazoDespues').value='';
					//$("#Anular").combobox('setValue',"1"); //0=Si 1=No
					document.getElementById('radioNO').checked=true;
				}//END else				
			} 			
			
			function cambiarAnular(){
				
				//var Anular= $('#Anular').combobox('getValue');				
				//if(Anular.trim()=="1"){	//0=Si 1=No	
				if(document.getElementById('radioNO').checked==true){	//0=Si 1=No								
					document.getElementById('IdMotivoRechazoDurante').value='';
					$("#MotivoRechazoDurante").combobox('setValue',"0"); 
					document.getElementById('IdMotivoRechazoDespues').value='';	
					$("#MotivoRechazoDespues").combobox('setValue',"0"); 								
				}
			}
			
			function guardar(){				
				var IdResponsableIni=$('#IdResponsableIni').combobox('getValue');		
				if(IdResponsableIni.trim()=="" || $('#IdResponsableIni').combobox('isValid')==false){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable Inicial','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar el Responsable Inicial',
						icon:'info',
						fn: function(){
							$('#IdResponsableIni').next().find('input').focus();
						}
					});
					$('#IdResponsableIni').next().find('input').focus();
					return 0;			
				}				
				var IdResponsableFin=$('#IdResponsableFin').combobox('getValue');		
				if(IdResponsableFin.trim()=="" || $('#IdResponsableFin').combobox('isValid')==false){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable Final','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar el Responsable Final',
						icon:'info',
						fn: function(){
							$('#IdResponsableFin').next().find('input').focus();
						}
					});
					$('#IdResponsableFin').next().find('input').focus();
					return 0;			
				}	
				/*var NroLote=$('#NroLote').textbox('getValue');		
				if(NroLote.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Ingresar el Nro Lote','info');					
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar el Nro Lote',
						icon:'info',
						fn: function(){
							$('#NroLote').next().find('input').focus();
						}
					});
					$('#NroLote').next().find('input').focus();
					return 0;			
				}*/
				var FechaExtraccion=$('#FechaExtraccion').datebox('getValue');		
				if(FechaExtraccion.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Ingresar la Fecha Extracción','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar la Fecha Extracción',
						icon:'info',
						fn: function(){
							$('#FechaExtraccion').next().find('input').focus();
						}
					});
					$('#FechaExtraccion').next().find('input').focus();
					return 0;			
				}
				
				var HoraInicio=$('#HoraInicio').timespinner('getValue');		
				if(HoraInicio.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Ingresar la Hora Inicio','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar la Hora Inicio',
						icon:'info',
						fn: function(){
							$('#HoraInicio').next().find('input').focus();
						}
					});
					$('#HoraInicio').next().find('input').focus();
					return 0;			
				}
				var HoraFin=$('#HoraFin').timespinner('getValue');		
				if(HoraFin.trim()==""){ 
					$.messager.alert('Mensaje','Falta Ingresar la Duración Coleccion','info');
					//$('#DuracionColeccion').next().find('input').focus();
					return 0;			
				}
				
				var IdTipoDonacion=document.getElementById('IdTipoDonacion').value;
				
				if(IdTipoDonacion=='1'){ //Sangre Total
					var DuracionColeccion=$('#DuracionColeccion').timespinner('getValue');		
					if(DuracionColeccion.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Duración Coleccion','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Duración Coleccion',
							icon:'info',
							fn: function(){
								$('#DuracionColeccion').next().find('input').focus();
							}
						});
						$('#DuracionColeccion').next().find('input').focus();
						return 0;			
					}
					var IdBolsaColectora=$('#IdBolsaColectora').combobox('getValue');		
					if(IdBolsaColectora.trim()=="0"){ 
						//$.messager.alert('Mensaje','Falta Seleccionar el Tipo de Bolsa Colectora','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar el Tipo de Bolsa Colectora',
							icon:'info',
							fn: function(){
								$('#IdBolsaColectora').next().find('input').focus();
							}
						});
						$('#IdBolsaColectora').next().find('input').focus();
						return 0;			
					}
					var IdMezcladorBasculante=$('#IdMezcladorBasculante').combobox('getValue');		
					if(IdMezcladorBasculante.trim()=="0"){ 
						//$.messager.alert('Mensaje','Falta Seleccionar el Mezclador Basculante Sangre','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar el Mezclador Basculante Sangre',
							icon:'info',
							fn: function(){
								$('#IdMezcladorBasculante').next().find('input').focus();
							}
						});
						$('#IdMezcladorBasculante').next().find('input').focus();
						return 0;			
					}
					var SerieHemobascula=$('#SerieHemobascula').textbox('getValue');		
					if(SerieHemobascula.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Serie Hemobascula','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Serie Hemobascula',
							icon:'info',
							fn: function(){
								$('#SerieHemobascula').next().find('input').focus();
							}
						});
						$('#SerieHemobascula').next().find('input').focus();
						return 0;			
					}					
					document.getElementById('FlujoAlto').disabled=false;
					document.getElementById('FlujoBajo').disabled=false;
					document.getElementById('ConInterrupcion').disabled=false;
					
				}else if(IdTipoDonacion=='2'){ //AFERESIS
					var DuracionColeccion2=$('#DuracionColeccion2').timespinner('getValue');		
					if(DuracionColeccion2.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Duración Coleccion','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Duración Coleccion',
							icon:'info',
							fn: function(){
								$('#DuracionColeccion2').next().find('input').focus();
							}
						});
						$('#DuracionColeccion2').next().find('input').focus();
						return 0;			
					}
					var IdEquipoAferesis=$('#IdEquipoAferesis').combobox('getValue');		
					if(IdEquipoAferesis.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Seleccionar el Equipo de Aferesis','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar el Equipo de Aferesis',
							icon:'info',
							fn: function(){
								$('#IdEquipoAferesis').next().find('input').focus();
							}
						});
						$('#IdEquipoAferesis').next().find('input').focus();
						return 0;			
					}
					var IdSetAferesis=$('#IdSetAferesis').combobox('getValue');
					if(IdSetAferesis.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Seleccionar el Tipo Set de Aferesis','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar el Tipo Set de Aferesis',
							icon:'info',
							fn: function(){
								$('#IdSetAferesis').next().find('input').focus();
							}
						});
						$('#IdSetAferesis').next().find('input').focus();
						return 0;			
					}
					var SerieEquipo=$('#SerieEquipo').textbox('getValue');		
					if(SerieEquipo.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Serie del Equipo','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Serie del Equipo',
							icon:'info',
							fn: function(){
								$('#SerieEquipo').next().find('input').focus();
							}
						});
						$('#SerieEquipo').next().find('input').focus();
						return 0;			
					}					
					
					var VolProcesado=$('#VolProcesado').textbox('getValue');
					if(VolProcesado.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Volumen Procesado','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar el Volumen Procesado',
							icon:'info',
							fn: function(){
								$('#VolProcesado').next().find('input').focus();
							}
						});
						$('#VolProcesado').next().find('input').focus();
						return 0;			
					}					
					var VolUsado=$('#VolUsado').textbox('getValue');		
					if(VolUsado.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Volumen AC usado','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar el Volumen AC usado',
							icon:'info',
							fn: function(){
								$('#VolUsado').next().find('input').focus();
							}
						});
						$('#VolUsado').next().find('input').focus();
						return 0;			
					}
					var VolPlaquetas=$('#VolPlaquetas').textbox('getValue');		
					if(VolPlaquetas.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Volumen de Plaquetas','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar el Volumen de Plaquetas',
							icon:'info',
							fn: function(){
								$('#VolPlaquetas').next().find('input').focus();
							}
						});
						$('#VolPlaquetas').next().find('input').focus();
						return 0;			
					}
					var NroCiclos=$('#NroCiclos').numberbox('getValue');		
					if(NroCiclos.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Nro de Ciclos','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar Nro Ciclos',
							icon:'info',
							fn: function(){
								$('#NroCiclos').next().find('input').focus();
							}
						});
						$('#NroCiclos').next().find('input').focus();
						return 0;			
					}
					var RendimientoEstimado=$('#RendimientoEstimado').numberbox('getValue');		
					if(RendimientoEstimado.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Rendimiento Estimado','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar el Rendimiento Estimado',
							icon:'info',
							fn: function(){
								$('#RendimientoEstimado').next().find('input').focus();
							}
						});
						$('#RendimientoEstimado').next().find('input').focus();
						return 0;			
					}
					var RendimientoObjetivo=$('#RendimientoObjetivo').numberbox('getValue');		
					if(RendimientoObjetivo.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Rendimiento Objetivo','info');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar el Rendimiento Objetivo',
							icon:'info',
							fn: function(){
								$('#RendimientoObjetivo').next().find('input').focus();
							}
						});
						$('#RendimientoObjetivo').next().find('input').focus();
						return 0;			
					}
				}	
				
				//var Anular=$('#Anular').combobox('getValue');		
				/*if(Anular.trim()==""){ 
					$.messager.alert('Mensaje','Seleccione si Anular Nro Donacion','info');
					$('#Anular').next().find('input').focus();
					return 0;			
				}*/
				
				var MotivoRechazoDurante=$('#MotivoRechazoDurante').combobox('getValue');
				var MotivoRechazoDespues=$('#MotivoRechazoDespues').combobox('getValue');				
				if( (document.getElementById('radioSI').checked==true) && (MotivoRechazoDurante.trim()=="0" && MotivoRechazoDespues.trim()=="0")){ //Anular.trim()=="0"
					$.messager.alert('Mensaje','Seleccione un Motivo Rechazo','info');
					$('#MotivoRechazoDurante').next().find('input').focus();
					return 0;			
				}
				if($('#MotivoRechazoDurante').combobox('isValid')==false){ //Anular.trim()=="0"
					$.messager.alert('Mensaje','El Incidente Durante Ingresado NO EXISTE','info');
					$('#MotivoRechazoDurante').next().find('input').focus();
					return 0;			
				}
				if($('#MotivoRechazoDespues').combobox('isValid')==false){ //Anular.trim()=="0"
					$.messager.alert('Mensaje','El Incidente Despues Ingresado NO EXISTE','info');
					$('#MotivoRechazoDespues').next().find('input').focus();
					return 0;			
				}				
				
				var NroDonacion=$('#NroDonacion').textbox('getValue');		
				if(NroDonacion.trim()==""){ 
					$.messager.alert('Mensaje','Falta Ingresar el Nro de Donacion','info');
					$('#NroDonacion').next().find('input').focus();
					return 0;			
				}			
												
				//document.form1.submit();
				$.messager.confirm('Mensaje', '¿Seguro de Guardar los Datos de Extracción de Sangre del Postulante?', function(r){
					if (r){
						$('#form1').submit();	
					}
				});	
			}  
			
			function llenarDatosSistExtraccion(){
				
				if(document.getElementById('Autom').checked==true){
					//llenarDatosSistExtraccion
					$("#IdResponsableIni").combobox('setValue', '<?php echo $IdResponsableIni ?>');
					$("#IdResponsableFin").combobox('setValue', '<?php echo $IdResponsableFin ?>');
					$("#NroLote").textbox('setValue', '<?php echo $NroLote ?>');
					$("#SerieHemobascula").textbox('setValue', '<?php echo $SerieHemobascula ?>');
					$("#HoraInicio").timespinner('setValue', '<?php echo $HoraInicio ?>');
					$("#DuracionColeccion" ).timespinner('setValue', '<?php echo $DuracionColeccion ?>');					
					$("#HoraFin").timespinner('setValue', '<?php echo $HoraFin ?>');						
					$("#VolColeccionPre").textbox('setValue', '<?php echo $VolColeccionPre ?>');
					$("#VolColeccionReal").textbox('setValue', '<?php echo $VolColeccionReal ?>');
					
					var FlujoAlto='<?php echo $FlujoAlto ?>';
					if(FlujoAlto=='1'){
						document.getElementById('FlujoAlto').checked=true;
					}else{
						document.getElementById('FlujoAlto').checked=false;
					}
					
					var FlujoBajo='<?php echo $FlujoBajo ?>';
					if(FlujoBajo=='1'){
						document.getElementById('FlujoBajo').checked=true;
					}else{
						document.getElementById('FlujoBajo').checked=false;
					}
					
					var ConInterrupcion='<?php echo $ConInterrupcion ?>';
					if(ConInterrupcion=='1'){
						document.getElementById('ConInterrupcion').checked=true;
					}else{
						document.getElementById('ConInterrupcion').checked=false;
					}
									
					//SOLO LECTURA
					//$("#IdResponsableIni").combobox('readonly',true);	
					//$("#IdResponsableFin").combobox('readonly',true);	
					$("#NroLote").textbox('readonly',true);	
					$("#SerieHemobascula").textbox('readonly',true);	
					//$("#HoraInicio").timespinner('readonly',true);	
					//$("#DuracionColeccion" ).timespinner('readonly',true);						
					//$("#HoraFin").timespinner('readonly',true);							
					$("#VolColeccionPre").textbox('readonly',true);	
					$("#VolColeccionReal").textbox('readonly',true);					
					document.getElementById('FlujoAlto').disabled=true;
					document.getElementById('FlujoBajo').disabled=true;
					document.getElementById('ConInterrupcion').disabled=true;		
					
				}else{
					//LimpiarDatosSistExtraccion				
					$("#IdResponsableIni").combobox('setValue', '');
					$("#IdResponsableFin").combobox('setValue', '');
					$("#NroLote").textbox('setValue', '');
					$("#SerieHemobascula").textbox('setValue', '');
					var f=new Date();
					var HoraActual=f.getHours()+":"+f.getMinutes()+":"+f.getSeconds(); 
					$("#HoraInicio").timespinner('setValue', HoraActual);
					$("#DuracionColeccion" ).timespinner('setValue', '');					
					$("#HoraFin").timespinner('setValue', '');						
					$("#VolColeccionPre").textbox('setValue', 450);
					$("#VolColeccionReal").textbox('setValue', '');					
					document.getElementById('FlujoAlto').checked=false;
					document.getElementById('FlujoBajo').checked=false;
					document.getElementById('ConInterrupcion').checked=false;
					
					//SOLO LECTURA
					//$("#IdResponsableIni").combobox('readonly',false);	
					//$("#IdResponsableFin").combobox('readonly',false);	
					$("#NroLote").textbox('readonly',false);	
					$("#SerieHemobascula").textbox('readonly',false);	
					//$("#HoraInicio").timespinner('readonly',false);	
					//$("#DuracionColeccion" ).timespinner('readonly',false);						
					//$("#HoraFin").timespinner('readonly',false);							
					$("#VolColeccionPre").textbox('readonly',false);	
					$("#VolColeccionReal").textbox('readonly',false);					
					document.getElementById('FlujoAlto').disabled=false;
					document.getElementById('FlujoBajo').disabled=false;
					document.getElementById('ConInterrupcion').disabled=false;	
				}
			}
			
			function CalcularHoraSalida(hora_ingreso,jornal) { //10:55:55 , 00:09:53   
				var hora_ingreso = hora_ingreso.split(":");//10:55:55  
				var jornal = jornal.split(":");//00:09:53
				var horas=parseInt(hora_ingreso[0])+parseInt(jornal[0]);//10
				var minutos=parseInt(hora_ingreso[1])+parseInt(jornal[1]);//64
				var segundos=parseInt(hora_ingreso[2])+parseInt(jornal[2]);//108				
				var horas=horas+(parseInt(minutos/60));		
				//var minutos=minutos%60; //(minutos si no hay segundos); 
				var minutos=(parseInt(segundos/60))+(parseInt(minutos%60));							
				var segundos=segundos%60; //residuo 108/60=48				
				if(minutos<10)minutos="0"+minutos ;//05
				if(segundos<10)segundos="0"+segundos ;//48
				var hora_salida = horas+":"+minutos+":"+segundos;//11:05:48
				return hora_salida; 
			}
			
			function cambiarDuracionAferesis(){
				var HoraInicio=$('#HoraInicio').timespinner('getValue');	
				var DuracionColeccion2=$('#DuracionColeccion2').timespinner('getValue');
				HoraFin=CalcularHoraSalida(HoraInicio,DuracionColeccion2);	
				$("#HoraFin").timespinner('setValue', HoraFin);						
			}   
			
			function cambiarDuracionSTManual(){
				if(document.getElementById('Manual').checked==true){
					var HoraInicio=$('#HoraInicio').timespinner('getValue');	
					var DuracionColeccion=$('#DuracionColeccion').timespinner('getValue');
					HoraFin=CalcularHoraSalida(HoraInicio,DuracionColeccion);	
					$("#HoraFin").timespinner('setValue', HoraFin);
				}
			}   
       
		</script>
</head>

<body>   
    
    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarExtraccionSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:90%;;height:auto;"title="Banco de Sangre: Registrar Extracción de Sangre" iconCls="icon-save" align="center"> 	
    <div class="easyui-panel" style="padding:0px;">     
        <!--<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Refrescar</a>-->              		
        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="guardar()">Guardar</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="cancelar();">Cancelar(ESC)</a>                      
	</div>
    
    <table>
        <tr align="center">
            <td>
				<?php echo "<font color='#0000FF'>$NombresPostulante ($NroMovimiento)</font>" ; ?><br>
				<?php echo "Nro Documento "."<font color='#0000FF'>$NroDocumento</font>" ; ?>
          </td>
        </tr>
    </table>
    
    <table width="956" border="0">
            <tr>
                <td width="950"> 					
				  <fieldset>
				    <legend>Datos Generales</legend>
						<table width="704" border="0" cellspacing="4">
						   
								<tr>
								  <td>Tipo Donación</td>
								  <td width="192">
                                        <select class="easyui-combobox" name="" id="" style="width: 150px" disabled>
                                            <option value="0">Seleccione</option>
                                            <?php
                                              $listar=SIGESA_BSD_ListarTipoDonacion_M();
                                               if($listar != NULL) { 
                                                 foreach($listar as $item){?>
                                            <option value="<?php echo $item["IdTipoDonacion"]?>" <?php if($item["IdTipoDonacion"]==$IdTipoDonacion){?> selected <?php } ?>><?php echo $item["IdTipoDonacion"].'='.$item["Descripcion"]?></option>
                                            <?php } } ?>
                                        </select>
                                        <input name="IdTipoDonacion" id="IdTipoDonacion" value="<?php echo $IdTipoDonacion ?>" type="hidden" /> 
                                        <input name="IdMovimiento" id="IdMovimiento" value="<?php echo $IdMovimiento ?>" type="hidden" />               
                                   </td>
								  <td width="129">Grupo Sanguineo Postulante</td>
								  <td width="229"><select class="easyui-combobox" name="" id=""  style="width: 150px" disabled>
								    <option value="0">Seleccione</option>
								    <?php
							  $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
							   if($listarSexo != NULL) { 
								 foreach($listarSexo as $item){?>
								    <option value="<?php echo $item["IdGrupoSanguineo"]?>" <?php if($item["IdGrupoSanguineo"]==$IdGrupoSanguineo){?> selected <?php } ?> ><?php echo $item["Descripcion"]?></option>
								    <?php } } ?>
								    </select>
								    <input name="IdExamenMedico" id="IdExamenMedico" value="<?php echo $IdExamenMedico ?>" type="hidden" /></td>
						  </tr>
								<tr>
								  <td width="126">Responsable Inicial</td>
								  <td><Select style="width:150px" class="easyui-combobox" id="IdResponsableIni" name="IdResponsableIni" data-options="prompt:'Seleccione',required:true">
								    <option value=""></option>
								    <?php
                                  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
								    <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
								    <?php } } ?>
							      </select></td>
								  <td>Responsable Final</td>
								  <td><Select style="width:150px" class="easyui-combobox" id="IdResponsableFin" name="IdResponsableFin" data-options="prompt:'Seleccione',required:true">
								    <option value=""></option>
								    <?php
                                  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
								    <option value="<?php echo $item["DNI"]?>" ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
								    <?php } } ?>
							      </select></td>
				          </tr>
								<tr>
								  <td>Nro Lote de la Bolsa</td>
								  <td><input name="NroLote" class="easyui-textbox" id="NroLote" style="width:150px" data-options="prompt:'Nro Lote'"></td>
								  <td>Fecha Extraccion/ Donación</td>
								  <td><input name="FechaExtraccion" class="easyui-datebox" id="FechaExtraccion" style="width:150px" value="<?php echo date('d/m/Y');?>" data-options="required:true" validType="validDate" /></td>
						  </tr>
                                 
								
								<tr>
								  <td>Hora Inicio (H:M:S)</td>
								  <td><input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" style="width:150px;" id="HoraInicio" name="HoraInicio" data-options="showSeconds:true,required:true"></td>
								  <td>Hora Fin (H:M:S)</td>
								  <td><input class="easyui-timespinner" value="" style="width:150px;" id="HoraFin" name="HoraFin" data-options="showSeconds:true" readonly></td>
						  </tr>
								<tr>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
						  </tr>
						</table>
					</fieldset>
				
                </td>
            </tr>
			
			<tr>
				<td>
					 <!--INICIO TABLA EXTRACCION-->
					<table width="948" height="204">
						<tr>
							<td width="58%">				
							  <fieldset>
								<legend>Datos Especificos de <?php echo $TipoDonacion ?></legend>
                                
                                <?php if($IdTipoDonacion=='1'){ //1=Sangre Total ?>
                                <table width="96%">
										<tr>
										  <td>Duración Colección (H:M:S)</td>
										  <td width="60%"><input  class="easyui-timespinner" style="width:200px" id="DuracionColeccion" name="DuracionColeccion"  data-options="prompt:'Duración Colección',showSeconds:true,required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = cambiarDuracionSTManual(); }"></td>
										  <td align="center">Autom./Manual</td>
								  </tr>
										<tr>
										  <td>Tipo Bolsa Colectora</td>
										  <td>
                                          <Select style="width:200px" class="easyui-combobox" id="IdBolsaColectora" name="IdBolsaColectora">
										    <option value="0">Seleccione</option>
										    <?php
										  	$listar=SIGESA_ListarEquipoExtraccion_M('BCO'); 
										   	if($listar != NULL) { 
											foreach($listar as $item){?>
											<option value="<?php echo $item["IdEquipoExtraccion"]?>" <?php if($item["IdEquipoExtraccion"]=='1'){?> selected <?php } ?>><?php echo mb_strtoupper($item["Descripcion"])?></option>
											<?php } } ?>
									      </select>                                          
                                          </td>
										  <td align="center"><label for="Autom">Autom.</label>
                                          <input type="radio" name="TipoReg" id="Autom" value="1" onClick="llenarDatosSistExtraccion();" /></td>
								      </tr>
										<tr>
											<td width="40%">Mezclador Basculante Sangre</td>
											<td><!--<button class="btn">.</button>-->
											  <Select style="width:200px" class="easyui-combobox" id="IdMezcladorBasculante" name="IdMezcladorBasculante">
											    <option value="0">Seleccione</option>
											    <?php
										  	$listar=SIGESA_ListarEquipoExtraccion_M('MBA'); 
										   	if($listar != NULL) { 
											foreach($listar as $item){?>
											    <option value="<?php echo $item["IdEquipoExtraccion"]?>" selected><?php echo mb_strtoupper($item["Descripcion"])?></option>
											    <?php } } ?>
									        </select></td>
											<td align="center"><label for="Manual">Manual</label>
                                            <input type="radio" name="TipoReg" id="Manual" value="2" onClick="llenarDatosSistExtraccion();" checked /></td>
										</tr>
										<tr>
										  <td>Serie Hemobascula</td>
										  <td><input  class="easyui-textbox" style="width:200px" id="SerieHemobascula" name="SerieHemobascula"  data-options="prompt:'Serie Hemobascula',required:true" tabindex="1">
                       <!-- <input id="write_input" class="easyui-textbox" data-options="prompt:'write here...',                               
                        onkeyup: function(e){sendChatMessage($(e.data.target).textbox('getValue'),'1');}"  style="width:238px;height:24px;">-->
                                          <!--<input type="text" maxlength="5" onkeyup="if (event.keyCode == 13) txt.focus()" />
<input type="text" name="txt" />-->
                                          </td>
										  <td align="center">&nbsp;</td>
						          </tr>
										<tr>
										  <td>Volumen Col. Preestablecido(ML)</td>
										  <td colspan="2"><input name="VolColeccionPre" class="easyui-textbox" id="VolColeccionPre" style="width:200px" data-options="prompt:'Vol. Colección preestablecido',required:true"  tabindex="2" value="450"></td>
								  </tr>
										<tr>
										  <td>Vol. Colección Real(ML)</td>
										  <td colspan="2"><input style="width:200px" class="easyui-textbox" id="VolColeccionReal" name="VolColeccionReal" data-options="prompt:'Vol. Colección Real',required:true" tabindex="3"></td>
										</tr>
										<tr>
										  <td>Alarma Ocurrida</td>
										  <td colspan="2">
                                          Flujo Alto<input name="FlujoAlto" id="FlujoAlto" type="checkbox" value="1" />
                                          Flujo Bajo<input name="FlujoBajo" id="FlujoBajo" type="checkbox" value="1" />
                                          Con Interrupcion<input name="ConInterrupcion" id="ConInterrupcion" type="checkbox" value="1" />
                                          <input type="hidden" id="AlarmaOcurrida" name="AlarmaOcurrida" />
                                          </td>
									    </tr>
									</table>
                                    
                                  <?php }else if($IdTipoDonacion=='2'){ //2=AFERESIS ?>
                                    <table width="94%">
										<tr>
										  <td>Duración Colección (H:M:S)</td>
										  <td><input  class="easyui-timespinner" style="width:200px" id="DuracionColeccion2" name="DuracionColeccion2"   data-options="prompt:'Duración Colección',showSeconds:true,required:true,                  
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = cambiarDuracionAferesis(); }"></td>
									  </tr>
										<tr>
										  <td>Equipo de Aferesis</td>
										  <td>
                                          <Select style="width:200px" class="easyui-combobox" id="IdEquipoAferesis" name="IdEquipoAferesis" data-options="prompt:'Seleccione',required:true">
										    <option value=""></option>
										    <?php
										  	$listar=SIGESA_ListarEquipoExtraccion_M('EAF'); 
										   	if($listar != NULL) { 
											foreach($listar as $item){?>
											<option value="<?php echo $item["IdEquipoExtraccion"]?>"><?php echo mb_strtoupper($item["Descripcion"])?></option>
											<?php } } ?>
									      </select>                                          
                                          </td>
									  </tr>
										<tr>
											<td width="40%">Tipo Set de Aferesis</td>
											<td><!--<button class="btn">.</button>-->
											  <Select style="width:200px" class="easyui-combobox" id="IdSetAferesis" name="IdSetAferesis" data-options="prompt:'Seleccione',required:true">
											    <option value=""></option>
											    <?php
										  	$listar=SIGESA_ListarEquipoExtraccion_M('SAF'); 
										   	if($listar != NULL) { 
											foreach($listar as $item){?>
											    <option value="<?php echo $item["IdEquipoExtraccion"]?>"><?php echo mb_strtoupper($item["Descripcion"])?></option>
											    <?php } } ?>
									        </select></td>
										</tr>
										<tr>
										  <td>Serie Equipo</td>
										  <td><input  class="easyui-textbox" style="width:200px" id="SerieEquipo" name="SerieEquipo"  data-options="prompt:'Serie Equipo',required:true"></td>
								       </tr>										
									   <tr>
										  <td>Vol. Procesado(ML)</td>
										  <td><input name="VolProcesado" class="easyui-textbox" id="VolProcesado" style="width:200px" data-options="prompt:'Vol. Procesado',required:true"></td>
								       </tr>
										<tr>
										  <td>Vol. AC usado(ML)</td>
										  <td><input style="width:200px" class="easyui-textbox" id="VolUsado" name="VolUsado" data-options="prompt:'Vol. AC usado',required:true"></td>
										</tr>
										<tr>
										  <td>Vol. Plaquetas(ML)</td>
										  <td><input style="width:200px" class="easyui-textbox" id="VolPlaquetas" name="VolPlaquetas" data-options="prompt:'Vol. Plaquetas',required:true"></td>
									  </tr>
										<tr>
										  <td>Nro Ciclos</td>
										  <td><input style="width:200px" class="easyui-numberbox" id="NroCiclos" name="NroCiclos" data-options="prompt:'Nro Ciclos',min:0,precision:2,required:true"></td>
									  </tr>
										<tr>
										  <td>Rendimientos (yield)</td>
										  <td><input style="width:100px" class="easyui-numberbox" id="RendimientoEstimado" name="RendimientoEstimado" data-options="prompt:'Estimado',min:0,precision:2,required:true">
									      <input style="width:95px" class="easyui-numberbox" id="RendimientoObjetivo" name="RendimientoObjetivo" data-options="prompt:'Objetivo',min:0,precision:2,required:true"></td>
									  </tr>
									</table>
                                    <?php } ?>
							  </fieldset>
							</td>
					
							<td width="42%">			
							  <fieldset>  
								<legend>Datos Detallados de	la	Extracción </legend>
								<table width="100%">				
										<!--<tr>
										  <td>Fecha Donación</td>
										  <td><input name="FechaDonacion" class="easyui-datebox" id="FechaDonacion" style="width:145px"></td>
								  		</tr>-->
										<tr>
											  <td width="39%">Nro Donación</td>
											  <td width="61%">
                                              	<input name="NroDonacion" class="easyui-textbox" id="NroDonacion" style="width:145px" value="<?php echo $NroDonacion ?>"  data-options="prompt:'Nro Donación'" readonly />
                                                <input name="NroDonacionAnt" id="NroDonacionAnt" value="<?php echo $NroDonacion ?>" type="hidden" />
                                              
                                              </td>
									  </tr>
											<tr>
											  <td>Incidente Durante</td>
											  <td>
                                                 <Select tabindex="4" style="width:145px" class="easyui-combobox" id="MotivoRechazoDurante" name="MotivoRechazoDurante" data-options="
                                                    valueField: 'id',
                                                    textField: 'text',        
                                                    onSelect: function(rec){
                                                    var url = cambiarDiferimientoRechazo(); }">
                                                  <option value="0">Ninguno</option>
                                                  <?php
                                                              $listar=SIGESA_ListarMotivoRechazo_M('Extraccion',''); //1 Evaluacion, 2 Extraccion, 3 Tamizaje
                                                               if($listar != NULL) { 
                                                                 foreach($listar as $item){?>
                                                  <option value="<?php echo $item["IdMotivoRechazo"]?>"><?php echo $item["Descripcion"]?></option>
                                                  <?php } } ?>
                                                </select>
                                                <input name="IdMotivoRechazoDurante" type="hidden" id="IdMotivoRechazoDurante" />
                                              </td>
											</tr>
                                            
											<tr>
											  <td>Anular Nro Donacion</td>
											  <td>
                                                  <!--<Select name="Anular" id="Anular" class="easyui-combobox" style="width:145px" data-options="prompt:'Seleccione',required:true,
                                                    valueField: 'id',
                                                    textField: 'text',        
                                                    onSelect: function(rec){
                                                    var url = cambiarAnular(); }" tabindex="5">
                                                  	<option value=""></option>
                                                    <option value="1" selected>1=NO</option>
                                                    <option value="0">0=SI</option>											    
                                                  </select>-->
                                                  <label for="radioNO">1=NO</label>
                                          		  <input type="radio" name="Anular" id="radioNO" value="1" onClick="cambiarAnular();" checked /> &nbsp;&nbsp;&nbsp;
                                                  <label for="radioSI">0=SI</label>
                                            	  <input type="radio" name="Anular" id="radioSI" value="0" onClick="cambiarAnular();" />
                                              </td>
											</tr>
                                            
											<tr>
											  <td>Incidente Despues</td>
											  <td><Select style="width:145px" class="easyui-combobox" id="MotivoRechazoDespues" name="MotivoRechazoDespues" data-options="
                                                    valueField: 'id',
                                                    textField: 'text',        
                                                    onSelect: function(rec){
                                                    var url = cambiarDiferimientoRechazo(); }">
											    <option value="0">Ninguno</option>
											    <?php
                                                              $listar=SIGESA_ListarMotivoRechazo_M('Extraccion',''); //1 Evaluacion, 2 Extraccion, 3 Tamizaje
                                                               if($listar != NULL) { 
                                                                 foreach($listar as $item){?>
											    <option value="<?php echo $item["IdMotivoRechazo"]?>"><?php echo $item["Descripcion"]?></option>
											    <?php } } ?>
											    </select>
                                                <input name="IdMotivoRechazoDespues" type="hidden" id="IdMotivoRechazoDespues" />
                                                </td>
								  </tr>
                                                                                        
											<tr>
											  <td>Observaciones</td>
											  <td><input style="width:150px;height:70px" class="easyui-textbox" multiline="true" name="Observaciones" id="Observaciones" /></td>
											</tr>
								  </table>
								</fieldset>
							</td>
						</tr>                 
                         
					</table>
					<!--FIN TABLA EXTRACCION-->
			</td>		
		</tr>
    </table>   
     
</div>
</form>
</body></html>