<?php			
			
		$IdPaciente=$_REQUEST["IdPaciente"];
		$Paciente=SIGESA_BSD_Buscarpaciente_M($IdPaciente,'IdPaciente');	
		$NombresPaciente=$Paciente[0]["ApellidoPaterno"].' '.$Paciente[0]["ApellidoMaterno"].' '.$Paciente[0]["PrimerNombre"];
		
		$IdSolicitudSangre=$_REQUEST["IdSolicitudSangre"];			
				
		//LLAMAR DATOS DEL EQUIPO ( DE LA TABLA SIGESA_BANCODESANGRE_Recepcion )				
		$ID_Orden=$_REQUEST["IdOrdenFen"];	
				
		//Datos Examenes
		
		//3837	TEST DE COOMBS DIRECTO: NEG,
		$ObtenerCOOMBSDIRECTO=BuscarDatosPacientesEquiposBSD_M($ID_Orden,3837);
		$Valor_COOMBSDIRECTO=$ObtenerCOOMBSDIRECTO[0]['Valor_Examen'];
				
		//3839	TEST DE COOMBS INDIRECTO: NEG o POS, (2) 
		//TEST DE COOMBS INDIRECTO: basta un positivo es positivo. Ejemplo: ++=+, +-=+, -+=+, --=-
		$ObtenerCOOMBSINDIRECTO=BuscarDatosPacientesEquiposBSD_M($ID_Orden,3839);
		$Valor_COOMBSINDIRECTO1=$ObtenerCOOMBSINDIRECTO[0]['Valor_Examen'];
		$Valor_COOMBSINDIRECTO2=$ObtenerCOOMBSINDIRECTO[1]['Valor_Examen'];
		
		if($Valor_COOMBSINDIRECTO1=="POS" || $Valor_COOMBSINDIRECTO2=="POS"){
			$Valor_COOMBSINDIRECTO="POS";
		}else{
			$Valor_COOMBSINDIRECTO="NEG";
		}
		
		//3844	Tipificación completa fenotipo Rh (Factor RH):	C+c-E-e+K-
		$ObtenerFenotipo=BuscarDatosPacientesEquiposBSD_M($ID_Orden,3844);
		$Valor_Fenotipo=$ObtenerFenotipo[0]['Valor_Examen'];
		
		//DIVIDIR Fenotipo		
		$pos1= strpos($Valor_Fenotipo ,"C", 0 );
		if( trim($pos1)!="" ){
			$FenC=$Valor_Fenotipo[$pos1+1];
		}else{
			$FenC="";
		}
		
		$pos2= strpos($Valor_Fenotipo ,"c", 0 );
		if( trim($pos2)!="" ){
			$Fencc=$Valor_Fenotipo[$pos2+1];
		}else{
			$Fencc="";
		}		
		
		$pos3= strpos($Valor_Fenotipo ,"E", 0 );
		if( trim($pos3)!="" ){
			$FenE=$Valor_Fenotipo[$pos3+1];
		}else{
			$FenE="";
		}
		
		$pos4= strpos($Valor_Fenotipo ,"e", 0 );
		if( trim($pos4)!="" ){
			$Fenee=$Valor_Fenotipo[$pos4+1];
		}else{
			$Fenee="";
		}
		
		$pos5= strpos($Valor_Fenotipo ,"K", 0 );
		if( trim($pos5)!="" ){
			$FenK=$Valor_Fenotipo[$pos5+1];
		}else{
			$FenK="";
		}
		
		$pos6= strpos($Valor_Fenotipo ,"k", 0 );
		if( trim($pos6)!="" ){
			$Fenkk=$Valor_Fenotipo[$pos6+1];
		}else{
			$Fenkk="";
		}
		
		$pos7= strpos($Valor_Fenotipo ,"Cw", 0 );
		if( trim($pos7)!="" ){
			$FenCw=$Valor_Fenotipo[$pos7+1];
		}else{
			$FenCw="";
		}		
		
		$pos8= strpos($Valor_Fenotipo ,"control", 0 );
		if( trim($pos8)!="" ){
			$Fencontrol=$Valor_Fenotipo[$pos8+1];
		}else{
			$Fencontrol="";
		}
		
		//3842	Grupo Sanguineo y Factor RH:	0POS
		$ObtenerGrupoSanguineo=BuscarDatosPacientesEquiposBSD_M($ID_Orden,3842);
		$Valor_GrupoSanguineo=$ObtenerGrupoSanguineo[0]['Valor_Examen'];
		$DatosGS=ObtenerDatosGrupoSanguineo_M($Valor_GrupoSanguineo);		
		
		if($DatosGS!=NULL){
			$IdGrupoSanguineo=$DatosGS[0]['IdGrupoSanguineo'];
		}else{
			$IdGrupoSanguineo=$_REQUEST["IdGrupoSanguineo"];
		}			
		$IdGrupoSanguineoAnterior=$_REQUEST["IdGrupoSanguineo"];
		$Observacion=$ObtenerGrupoSanguineo[0]['Observacion'];		
?>

<html>
<head>
        <meta charset="UTF-8">      
        <title>Registrar Fenotipo Paciente</title> 
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">        
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript"> 
		
		$.extend($("#FecRegFenotipo").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FecRegFenotipo").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FechaNacimiento" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});
		
			function cancelar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListaEspera&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
		
			function guardar(){	
			
				var IdGrupoSanguineo=$('#IdGrupoSanguineo').combobox('getValue');		
				if(IdGrupoSanguineo.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable del Tamizaje','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar el Grupo Sanguineo',
						icon:'warning',
						fn: function(){
							$('#IdGrupoSanguineo').next().find('input').focus();
						}
					});
					$('#IdGrupoSanguineo').next().find('input').focus();
					return 0;			
				}	
				
				if($('#IdGrupoSanguineo').combobox('isValid')==false){ 
					//$.messager.alert('Mensaje','El Responsable Ingresado no Existe','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'El Grupo Sanguineo Ingresado no Existe',
						icon:'warning',
						fn: function(){
							$('#IdGrupoSanguineo').next().find('input').focus();
						}
					});
					$('#IdGrupoSanguineo').next().find('input').focus();
					return 0;			
				}			
					
				var FecRegFenotipo=$('#FecRegFenotipo').datebox('getText');		
				if(FecRegFenotipo.trim()==""){ 
					$.messager.alert('Mensaje','Falta Ingresar la Fecha de Ingreso Fenotipo','warning');					
					$('#FecRegFenotipo').next().find('input').focus();
					return 0;			
				}
				var array_FecRegFenotipo = FecRegFenotipo.split("/");	
					//si el array no tiene tres partes, la fecha es incorrecta
					if (array_FecRegFenotipo.length!=3){	
					   $.messager.alert({
							title: 'Mensaje',
							msg: 'Fecha de Ingreso Fenotipo Incorrecta',
							icon:'warning',
							fn: function(){
								$('#FecRegFenotipo').next().find('input').focus();
							}
						});
						$('#FecRegFenotipo').next().find('input').focus();
						return 0;
					}
									
				var HoraRegFenotipo=$('#HoraRegFenotipo').timespinner('getValue');		
				if(HoraRegFenotipo.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable Final','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar la Hora de Ingreso Fenotipo',
						icon:'warning',
						fn: function(){
							$('#HoraRegFenotipo').next().find('input').focus();
						}
					});
					$('#HoraRegFenotipo').next().find('input').focus();
					return 0;			
				}
				
				var UsuFenotipo=$('#UsuFenotipo').combobox('getValue');		
				if(UsuFenotipo.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable del Tamizaje','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar el Responsable de la Verificación',
						icon:'warning',
						fn: function(){
							$('#UsuFenotipo').next().find('input').focus();
						}
					});
					$('#UsuFenotipo').next().find('input').focus();
					return 0;			
				}				
					
				if($('#UsuFenotipo').combobox('isValid')==false){ 
					//$.messager.alert('Mensaje','El Responsable Ingresado no Existe','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'El Responsable Ingresado no Existe',
						icon:'warning',
						fn: function(){
							$('#UsuFenotipo').next().find('input').focus();
						}
					});
					$('#UsuFenotipo').next().find('input').focus();
					return 0;			
				}	
				
				var IdGrupoSanguineo=$('#IdGrupoSanguineo').combobox('getValue');
				var IdGrupoSanguineoAnterior=document.getElementById('IdGrupoSanguineoAnterior').value;	
				
				var mensaje1=""; var mensaje2=""; var mensaje3=""; var mensaje4="";
				
				if(IdGrupoSanguineoAnterior=='0' || IdGrupoSanguineoAnterior.trim()==''){
					mensaje1="y Agregar el Grupo Sanguineo del Postulante";
								
				}else if(IdGrupoSanguineo!=IdGrupoSanguineoAnterior){
					mensaje1="y Modificar el Grupo Sanguineo del Postulante";			
				}
				
				/*if(document.getElementById('Positivo1').checked==true){
					mensaje2="Eliminar en Fraccionamiento PFC y PQ por ANTICUERPOS IRREGULARES ,";			
				}	
				
				if(document.getElementById('Positivo2').checked==true){
					mensaje3="Eliminar en Fraccionamiento todos los hemocomponentes por COOMBS DIRECTO";			
				}*/	
				
				//En todos los NEGATIVOS, será obligatorio las 4 primeras columnas (CcEe)
				if(IdGrupoSanguineo.trim()>=5){
					if(document.getElementById('Positivo5').checked==false && document.getElementById('Negativo5').checked==false){ //Positivo5 AL Positivo8
						$.messager.alert('Mensaje','(-)Falta Seleccionar el Fenotipo C','warning');					
						//$('#FecRegFenotipo').next().find('input').focus();
						return 0;						
					}					
					if(document.getElementById('Positivo6').checked==false && document.getElementById('Negativo6').checked==false){ 
						$.messager.alert('Mensaje','(-)Falta Seleccionar el Fenotipo E','warning');					
						//$('#FecRegFenotipo').next().find('input').focus();
						return 0;						
					}
					if(document.getElementById('Positivo7').checked==false && document.getElementById('Negativo7').checked==false){ 
						$.messager.alert('Mensaje','(-)Falta Seleccionar el Fenotipo c','warning');					
						//$('#FecRegFenotipo').next().find('input').focus();
						return 0;						
					}
					if(document.getElementById('Positivo8').checked==false && document.getElementById('Negativo8').checked==false){ 
						$.messager.alert('Mensaje','(-)Falta Seleccionar el Fenotipo e','warning');					
						//$('#FecRegFenotipo').next().find('input').focus();
						return 0;						
					}	
							
				}
																	
				//document.form1.submit();
				$.messager.confirm('Mensaje', '¿Seguro de Guardar el Fenotipo del Receptor de Sangre '+ mensaje1 + mensaje2+ mensaje3 + '?', function(r){
					if (r){
						$('#form1').submit();	
					}
				});	
			}  
			
			
			/*$(function(){	
				
				$.extend($.fn.textbox.methods, {
					show: function(jq){
						return jq.each(function(){
							$(this).next().show();
						})
					},
					hide: function(jq){
						return jq.each(function(){
							$(this).next().hide();
						})
					}
				})				
				$('#MotivoNoNAT').textbox('hide');
				$('#PRquimioVIH').numberbox('hide');
				$('#PRquimioHTLV').numberbox('hide');
				$('#PRquimioHBS').numberbox('hide');
				$('#PRquimioVHB').numberbox('hide');
				$('#PRquimioVHC').numberbox('hide');
				$('#PRquimioSifilis').numberbox('hide');
				$('#PRquimioANTICHAGAS').numberbox('hide');
									
			});*/			
			
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
						
			$(function () {			
				
				$('#UsuFenotipo').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#UsuFenotipo"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0;  
					}							
				});						
				$('#UsuFenotipo').combobox('validate');	
				
				$('#IdGrupoSanguineo').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdGrupoSanguineo"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0;  
					}							
				});						
				$('#IdGrupoSanguineo').combobox('validate');						
				
			});			
			
			function CompletarFenotipo(){
				var a=''; var b=''; var c=''; var d=''; var e=''; var f=''; var g=''; var h='';
				if(document.getElementById('Positivo5').checked==true){
					a=document.getElementById('Positivo5').value;
				}else if(document.getElementById('Negativo5').checked==true){
					a=document.getElementById('Negativo5').value;
				}
								
				if(document.getElementById('Positivo6').checked==true){
					b=document.getElementById('Positivo6').value;
				}else if(document.getElementById('Negativo6').checked==true){
					b=document.getElementById('Negativo6').value;
				}
				
				if(document.getElementById('Positivo7').checked==true){
					c=document.getElementById('Positivo7').value;
				}else if(document.getElementById('Negativo7').checked==true){
					c=document.getElementById('Negativo7').value;
				}
				
				if(document.getElementById('Positivo8').checked==true){
					d=document.getElementById('Positivo8').value;
				}else if(document.getElementById('Negativo8').checked==true){
					d=document.getElementById('Negativo8').value;
				}
				
				if(document.getElementById('Positivo9').checked==true){
					e=document.getElementById('Positivo9').value;
				}else if(document.getElementById('Negativo9').checked==true){
					e=document.getElementById('Negativo9').value;
				}
				
				if(document.getElementById('Positivo10').checked==true){
					f=document.getElementById('Positivo10').value;
				}else if(document.getElementById('Negativo10').checked==true){
					f=document.getElementById('Negativo10').value;
				}
				
				if(document.getElementById('Positivo11').checked==true){
					g=document.getElementById('Positivo11').value;
				}else if(document.getElementById('Negativo11').checked==true){
					g=document.getElementById('Negativo11').value;
				}
				
				if(document.getElementById('Positivo12').checked==true){
					h=document.getElementById('Positivo12').value;
				}else if(document.getElementById('Negativo12').checked==true){
					h=document.getElementById('Negativo12').value;
				}			
				
				document.getElementById('Fenotipo').value=a+b+c+d+e+f+g+h;
			}
			
		</script>
</head>
<body>        
    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdSolicitudSangreFenotipo&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:90%;;height:auto;"title="Banco de Sangre: Registrar Fenotipo de los Receptores" iconCls="icon-save" align="center"> 	
    <div class="easyui-panel" style="padding:0px;">     
        <!--<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Refrescar</a>-->             		
     	<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="guardar()">Guardar</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="cancelar();">Cancelar(ESC)</a>                      
	</div>
        
        <table>
            <tr align="center">
              <td>Paciente</td>
                 <td>
                   Grupo / Factor
                </td>
                 <td>
                    Fecha / Hora
                </td>
                 <td>Responsable</td>
                
            </tr>
        <tr align="center">
          <td><?php echo $NombresPaciente; ?>           	  
              <input type="hidden" name="IdPaciente" id="IdPaciente" value="<?php echo $IdPaciente; ?>" />        
              <input type="hidden" name="IdSolicitudSangre" id="IdSolicitudSangre" value="<?php echo $IdSolicitudSangre; ?>" />               
              
            </td>
            <td>
			<select class="easyui-combobox" name="IdGrupoSanguineo" id="IdGrupoSanguineo"  style="width: 117px" data-options="required:true" readonly> 
             <option value="">Seleccione</option>          
            <?php
                  $listar=SIGESA_BSD_ListarGrupoSanguineo_M();
                   if($listar != NULL) { 
                     foreach($listar as $item){?>
            <option value="<?php echo $item["IdGrupoSanguineo"]?>" <?php if($item["IdGrupoSanguineo"]==$IdGrupoSanguineo){?> selected <?php } ?> ><?php echo $item["IdGrupoSanguineo"].'='.$item["Descripcion"]?></option>
            <?php } } ?>
          </select>
             <input type="hidden" name="IdGrupoSanguineoAnterior" id="IdGrupoSanguineoAnterior" value="<?php echo $IdGrupoSanguineoAnterior; ?>" style="width:110px" />                              
			
            <td>
               <input name="FecRegFenotipo" id="FecRegFenotipo" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />
               <input name="HoraRegFenotipo" id="HoraRegFenotipo" class="easyui-timespinner" value="<?php echo date('H:i:s'); ?>" data-options="showSeconds:true,required:true" style="width:100px"  />
            </td>
            <td><Select style="width:150px" class="easyui-combobox" id="UsuFenotipo" name="UsuFenotipo" data-options="prompt:'Seleccione',required:true">
              <option value=""></option>
              <?php
			  					  //$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
								  //$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                   if($listar != NULL){ 
                                     foreach($listar as $item){?>
              <option value="<?php echo $item["IdEmpleado"]?>" <?php if(trim($item["IdEmpleado"])==trim($_GET['IdEmpleado'])){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
              <?php } } ?>
            </select></td>
            
        </tr>
        </table>              
     	<br>
        <table>
            <tr>
              <td colspan="4"><b>Ingreso Opcional</b></td>
            </tr>
            <tr align="center">
                <td>
                    <fieldset>
                        <legend> Detección Ac. Irregulares (CI)</legend>
                        <label for="Positivo1">Positivo</label>
                        <input type="radio" name="DeteccionCI" id="Positivo1" value="+" />
                        <label for="Negativo1">Negativo</label>
                        <input type="radio" name="DeteccionCI" id="Negativo1" value="-" />        
                    </fieldset>  
                </td>
                <td>
                    <fieldset>
                        <legend>Coombs Directo</legend>                        
                        <label for="Positivo2">Positivo</label>
                        <input type="radio" name="CoombsDirecto" id="Positivo2" value="+" />
                        <label for="Negativo2">Negativo</label>
                        <input type="radio" name="CoombsDirecto" id="Negativo2" value="-" />  
                  </fieldset>   
                </td>
                <td>
            		<fieldset>
           			 	<legend>DVI</legend>
           			 	<label for="Positivo3">Positivo</label>
           			 	<input type="radio" name="DVI" id="Positivo3" value="+" />
           			 	<label for="Negativo3">Negativo</label>
           			 	<input type="radio" name="DVI" id="Negativo3" value="-" />
                        <br>
            		</fieldset>  
                </td>
               <td>
                   
              </td>
          </tr>
          
          <tr align="center">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
          </tr>
          <tr align="center">
              <td>
              	<fieldset>                    	
                    <legend>1. C </legend>
                  <label for="Positivo5">C+</label>
                    <input name="FenC" type="radio" id="Positivo5" onClick="CompletarFenotipo();" value="C+"  />
                    <label for="Negativo5">C-</label>
                  <input type="radio" name="FenC" id="Negativo5" value="C-" onClick="CompletarFenotipo();" />                 
              	</fieldset>
              </td>
              <td>              
             	 <fieldset>                    	
                    <legend>2. E </legend>
                    <label for="Positivo6">E+</label>
                    <input type="radio" name="FenE" id="Positivo6" value="E+" onClick="CompletarFenotipo();" />
                    <label for="Negativo6">E-</label>
                    <input type="radio" name="FenE" id="Negativo6" value="E-" onClick="CompletarFenotipo();" />                 
              	</fieldset>              
              </td>
              
             <td>
              	<fieldset>                    	
                    <legend>3. c </legend>
                    <label for="Positivo7">c+</label>
                    <input type="radio" name="Fencc" id="Positivo7" value="c+" onClick="CompletarFenotipo();" />
                    <label for="Negativo7">c-</label>
                    <input type="radio" name="Fencc" id="Negativo7" value="c-" onClick="CompletarFenotipo();"  />                 
              	</fieldset>
              </td>
              <td>              
             	 <fieldset>                    	
                    <legend>4. e </legend>
                    <label for="Positivo8">e+</label>
                    <input type="radio" name="Fenee" id="Positivo8" value="e+" onClick="CompletarFenotipo();" />
                    <label for="Negativo8">e-</label>
                    <input type="radio" name="Fenee" id="Negativo8" value="e-" onClick="CompletarFenotipo();"  />                 
              	</fieldset>              
              </td>
          </tr>
          <tr align="center">
                <td>
              	<fieldset>                    	
                    <legend>5. Cw </legend>
                    <label for="Positivo9">Cw+</label>
                    <input type="radio" name="FenCw" id="Positivo9" value="Cw+" onClick="CompletarFenotipo();" />
                    <label for="Negativo9">Cw-</label>
                    <input type="radio" name="FenCw" id="Negativo9" value="Cw-" onClick="CompletarFenotipo();"  />                 
              	</fieldset>
              </td>
              <td>              
             	 <fieldset>
             	   <legend>6. K </legend>
                    <label for="Positivo10">K+</label>
                    <input type="radio" name="FenK" id="Positivo10" value="K+" onClick="CompletarFenotipo();" />
                    <label for="Negativo10">K-</label>
                    <input type="radio" name="FenK" id="Negativo10" value="K-" onClick="CompletarFenotipo();"  />                 
              	</fieldset>              
              </td>
              
             <td>
              	<fieldset>                    	
                    <legend>7. k </legend>
                    <label for="Positivo11">k+</label>
                    <input type="radio" name="Fencontrol" id="Positivo11" value="k+" onClick="CompletarFenotipo();" />
                    <label for="Negativo11">k-</label>
                    <input type="radio" name="Fenkk" id="Negativo11" value="k-" onClick="CompletarFenotipo();"  />                 
              	</fieldset>
              </td>
              <td>              
           	    <fieldset>                    	
                    <legend>8. control </legend>
                    <label for="Positivo12">control+</label>
                    <input type="radio" name="Fencontrol" id="Positivo12" value="control+" onClick="CompletarFenotipo();" />
                    <label for="Negativo12">control-</label>
                    <input type="radio" name="Fencontrol" id="Negativo12" value="control-" onClick="CompletarFenotipo();"  />                 
              	</fieldset>            
              </td>
            </tr>
          <tr align="center">
            <td>&nbsp;</td>           
            <td colspan="2">
            	<fieldset>                    	
                    <legend>Fenotipo</legend>
                    <input type="text" name="Fenotipo" id="Fenotipo" value="" />                
              	</fieldset>                 
            </td>
            <td>&nbsp;</td>
          </tr>
      </table>     
     
      
      <br>
      
      <table height="83" border="0" cellpadding="0" cellspacing="0">
        
        <tr align="center">
          <td colspan="2">Especifidad Anticuerpo: <input style="width:300px;height:50px" class="easyui-textbox" multiline="true" name="EspecifidadAnticuerpo" id="EspecifidadAnticuerpo" /> </td>
          <td colspan="2">Observaciones: <input style="width:300px;height:50px" class="easyui-textbox" multiline="true" name="ObsFenotipo" id="ObsFenotipo" value="<?php echo $Observacion; ?>" /> </td>
        </tr>       
        
      </table>
          
      <!--<a href="#" class="easyui-linkbutton">GUARDAR</a><a href="#" class="easyui-linkbutton">EDITAR</a>
        <a href="#" class="easyui-linkbutton">IMPRIMIR</a><a href="#" class="easyui-linkbutton">SALIR</a>
       <a href="#" class="easyui-linkbutton">Sincronizar Analizador Automatico Plab</a>-->
     </div>
</form>
</body></html>                   
