<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Registrar Datos Recepción Postulante</title> 
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">        
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>        
        <!--<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-detailview.js"></script>-->
        
        <script type="text/javascript" src="../../MVC_Vista/BancoSangre/FuncionesAE.js"></script> 
        
        <script type="text/javascript">			
		
			function guardar(){
					
					//Datos Personales del Postulante					
					var NroDocumento=$('#NroDocumento').numberbox('getValue');									
					
					if( ((NroDocumento.trim())=="" || (NroDocumento.length)<0) && (document.getElementById('chkmanual').checked==false) ){
						//$.messager.alert('Mensaje','Ingrese Nro Documento y ENTER para buscar Postulante','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese Nro Documento y ENTER para buscar Postulante',
							icon:'warning',
							fn: function(){
								$('#NroDocumentoBus').next().find('input').focus();
							}
						});
						$('#NroDocumentoBus').next().find('input').focus();
						return 0;			
					}					
				    	
					if((NroDocumento.trim())=="" || (NroDocumento.length)<0){
						//$.messager.alert('Mensaje','Ingrese Nro Documento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese Nro Documento',
							icon:'warning',
							fn: function(){
								$('#NroDocumento').next().find('input').focus();
							}
						});
						$('#NroDocumento').next().find('input').focus();
						return 0;			
					}
					
					var errorPostulante=document.getElementById('errorPostulante').value;					
					if( errorPostulante.trim()!="" ){
						//$.messager.alert('Mensaje','Ingrese Nro Documento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: errorPostulante,
							icon:'warning',
							fn: function(){
								$('#NroDocumento').next().find('input').focus();
							}
						});
						$('#NroDocumento').next().find('input').focus();
						return 0;			
					}
					
					var ApellidoPaterno=$('#ApellidoPaterno').textbox('getValue');	
					if((ApellidoPaterno.trim())=="" || (ApellidoPaterno.length)<0){
						$.messager.alert('Mensaje','Ingrese Apellido Paterno','warning');
						$('#ApellidoPaterno').next().find('input').focus();
						return 0;			
					}
					
					var ApellidoMaterno=$('#ApellidoMaterno').textbox('getValue');	
					if((ApellidoMaterno.trim())=="" || (ApellidoMaterno.length)<0){
						$.messager.alert('Mensaje','Ingrese Apellido Materno','warning');
						$('#ApellidoMaterno').next().find('input').focus();
						return 0;			
					}
					
					var PrimerNombre=$('#PrimerNombre').textbox('getValue');	
					if((PrimerNombre.trim())=="" || (PrimerNombre.length)<0){
						$.messager.alert('Mensaje','Ingrese Primer Nombre','warning');
						$('#PrimerNombre').next().find('input').focus();
						return 0;			
					}
					
					FechaNacimiento=$('#FechaNacimiento').datebox('getValue');	
					EdadPaciente=$('#EdadPaciente').numberbox('getValue');
					
					if((FechaNacimiento=="" && document.getElementById('chkfecnac').checked==true)||(EdadPaciente=="" && document.getElementById('chkfecnac').checked==true)){
						$.messager.alert('Mensaje','Ingrese Edad Actual','warning');
						$('#EdadPaciente').next().find('input').focus();
						return 0;			
					}
					
					if(FechaNacimiento=="" && document.getElementById('chkfecnac').checked==false){
						$.messager.alert('Mensaje','Ingrese Fecha Nacimiento','warning');
						$('#FechaNacimiento').next().find('input').focus();
						return 0;
					}
					
					if(EdadPaciente=="" && document.getElementById('chkfecnac').checked==false){
						$.messager.alert('Mensaje','Ingrese Fecha Nacimiento Correcta','warning');
						$('#FechaNacimiento').next().find('input').focus();
						return 0;
					}						
					
					IdTipoSexo=$('#IdTipoSexo').combobox('getValue');				
					if(IdTipoSexo=="0" || IdTipoSexo.trim()==""){
						$.messager.alert('Mensaje','Seleccione Sexo','warning');
						$('#IdTipoSexo').next().find('input').focus();
						return 0;			
					}
					
					IdEstadoCivil=$('#IdEstadoCivil').combobox('getValue');				
					if(IdEstadoCivil=="0" || IdEstadoCivil.trim()==""){
						$.messager.alert('Mensaje','Seleccione Estado Civil','warning');
						$('#IdEstadoCivil').next().find('input').focus();
						return 0;			
					}
					
					IdTipoOcupacion=$('#IdTipoOcupacion').combobox('getValue');				
					if(IdTipoOcupacion=="0" || IdTipoOcupacion.trim()==""){
						$.messager.alert('Mensaje','Seleccione Ocupación','warning');
						$('#IdTipoOcupacion').next().find('input').focus();
						return 0;			
					}
					
					Telefono=$('#Telefono').numberbox('getValue');				
					if(Telefono.trim()==""){
						$.messager.alert('Mensaje','Ingrese Teléfono','warning');
						$('#Telefono').next().find('input').focus();
						return 0;			
					}
					
					IdGrupoSanguineo=$('#IdGrupoSanguineo').combobox('getValue');				
					if(IdGrupoSanguineo.trim()==""){
						$.messager.alert('Mensaje','Seleccione Grupo Sanguineo','warning');
						$('#IdGrupoSanguineo').next().find('input').focus();
						return 0;			
					}
					
					IdPaisDomicilio=$('#IdPaisDomicilio').combobox('getValue');				
					if(IdPaisDomicilio.trim()==""){
						$.messager.alert('Mensaje','Seleccione Pais Domicilio','warning');
						$('#IdPaisDomicilio').next().find('input').focus();
						return 0;			
					}
					
					if(IdPaisDomicilio=='166'){					
						IdDistritoDomicilio=$('#IdDistritoDomicilio').combobox('getValue');				
						if(IdDistritoDomicilio.trim()=="" || IdDistritoDomicilio.trim()=="0"){
							$.messager.alert('Mensaje','Seleccione Distrito Domicilio','warning');
							$('#IdDistritoDomicilio').next().find('input').focus();
							return 0;			
						}
					}
					
					IdPaisProcedencia=$('#IdPaisProcedencia').combobox('getValue');	
					if(IdPaisProcedencia=='166'){				
						IdDistritoProcedencia=$('#IdDistritoProcedencia').combobox('getValue');				
						if(IdDistritoProcedencia.trim()=="" || IdDistritoProcedencia.trim()=="0"){
							$.messager.alert('Mensaje','Seleccione Distrito Procedencia','warning');
							$('#IdDistritoProcedencia').next().find('input').focus();
							return 0;			
						}
					}
					
					IdPaisNacimiento=$('#IdPaisNacimiento').combobox('getValue');	
					if(IdPaisNacimiento=='166'){	
						IdDistritoNacimiento=$('#IdDistritoNacimiento').combobox('getValue');				
						if(IdDistritoNacimiento.trim()=="" || IdDistritoNacimiento.trim()=="0"){
							$.messager.alert('Mensaje','Seleccione Distrito Nacimiento','warning');
							$('#IdDistritoNacimiento').next().find('input').focus();
							return 0;			
						}
					}
					
					
					//Datos Detallados del Postulante
					IdTipoDonacion=$('#IdTipoDonacion').combobox('getValue');				
					if(IdTipoDonacion=="0" || IdTipoDonacion.trim()==""){
						//$.messager.alert('Mensaje','Seleccione Tipo Donacion','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione Tipo Donacion',
							icon:'warning',
							fn: function(){
								$('#IdTipoDonacion').next().find('input').focus();
							}
						});
						$('#IdTipoDonacion').next().find('input').focus();
						return 0;			
					}
					
					IdTipoDonante=$('#IdTipoDonante').combobox('getValue');				
					if(IdTipoDonante=="0" || IdTipoDonante.trim()==""){
						//$.messager.alert('Mensaje','Seleccione Tipo Donante','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione Tipo Donante',
							icon:'warning',
							fn: function(){
								$('#IdTipoDonante').next().find('input').focus();
							}
						});
						$('#IdTipoDonante').next().find('input').focus();
						return 0;			
					}
					
					IdTipoRelacion=$('#IdTipoRelacion').combobox('getValue');	
					if(IdTipoRelacion.trim()==""){ 
						//$.messager.alert('Mensaje','Seleccione Tipo Relacion','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione Tipo Relación',
							icon:'warning',
							fn: function(){
								$('#IdTipoRelacion').next().find('input').focus();
							}
						});
						$('#IdTipoRelacion').next().find('input').focus();
						return 0;			
					}			
					if(IdTipoDonante=="1" && (IdTipoRelacion=="0" || IdTipoRelacion.trim()=="") ){ //si es por reposicion tiene que tener un tipo de relacion
						//$.messager.alert('Mensaje','Seleccione Tipo Relacion','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione Tipo Relación',
							icon:'warning',
							fn: function(){
								$('#IdTipoRelacion').next().find('input').focus();
							}
						});
						$('#IdTipoRelacion').next().find('input').focus();
						return 0;			
					}					
					
					IdCondicionDonante=$('#IdCondicionDonante').combobox('getValue');				
					if(IdCondicionDonante=="0" || IdCondicionDonante.trim()==""){
						//$.messager.alert('Mensaje','Seleccione Condicion Donante','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione Condicion Donante',
							icon:'warning',
							fn: function(){
								$('#IdCondicionDonante').next().find('input').focus();
							}
						});
						$('#IdCondicionDonante').next().find('input').focus();
						return 0;			
					}
					
					FechaMovi=$('#FechaMovi').datebox('getValue');
					var array_FechaMovi = FechaMovi.split("/");	
					if(FechaMovi.trim()==""){
						//$.messager.alert('Mensaje','Seleccione Fecha Recepción','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione Fecha Recepción',
							icon:'warning',
							fn: function(){
								$('#FechaMovi').next().find('input').focus();
							}
						});
						$('#FechaMovi').next().find('input').focus();
						return 0;			
					}
					//si el array no tiene tres partes, la fecha es incorrecta
					if (array_FechaMovi.length!=3){					   
					   //$.messager.alert('Mensaje','Fecha Recepcion Incorrecta','warning');
					   $.messager.alert({
							title: 'Mensaje',
							msg: 'Fecha Recepción Incorrecta',
							icon:'warning',
							fn: function(){
								$('#FechaMovi').next().find('input').focus();
							}
						});
						$('#FechaMovi').next().find('input').focus();
						return 0;
					}
					
					HoraMovi=$('#HoraMovi').textbox('getValue');				
					if(HoraMovi.trim()==""){
						//$.messager.alert('Mensaje','Seleccione Hora Recepcion','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione Hora Recepcion',
							icon:'warning',
							fn: function(){
								$('#HoraMovi').next().find('input').focus();
							}
						});
						$('#HoraMovi').next().find('input').focus();
						return 0;			
					}
					
					//Registro del Receptor (Paciente) 
					
					NroHistoria=$('#NroHistoria').textbox('getValue');				
					if(IdTipoDonante=="1" && NroHistoria==""){ //si es por reposicion tiene que tener un nrohistoria Receptor 
						//$.messager.alert('Mensaje','Falta Buscar al Receptor (Paciente)','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Buscar al Receptor (Paciente)',
							icon:'warning',
							fn: function(){
								$('#NroHistoria').next().find('input').focus();
							}
						});
						$('#NroHistoria').next().find('input').focus();
						return 0;			
					}
					
					IdHospital=$('#IdHospital').combobox('getValue');				
					if(IdTipoDonante=="1" && (IdHospital.trim()=="" || IdHospital.trim()=="0") ){ //si es por reposicion tiene que tener Hospital Origen
						//$.messager.alert('Mensaje','Falta Seleccionar el Hospital Origen','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar el Hospital Origen',
							icon:'warning',
							fn: function(){
								$('#IdHospital').next().find('input').focus();
							}
						});
						$('#IdHospital').next().find('input').focus();
						return 0;			
					}
					
					IdGrupoSanguineoRec=$('#IdGrupoSanguineoRec').combobox('getValue');		
					if( IdTipoDonante=="1" && (IdGrupoSanguineoRec.trim()=="" || IdGrupoSanguineoRec.trim()=="0") ){ //si es por reposicion tiene que tener un GrupoSanguineo Receptor 
						//$.messager.alert('Mensaje','Falta Seleccionar Grupo Sanguineo del Receptor','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar Grupo Sanguineo del Receptor',
							icon:'warning',
							fn: function(){
								$('#IdGrupoSanguineoRec').next().find('input').focus();
							}
						});
						$('#IdGrupoSanguineoRec').next().find('input').focus();
						return 0;			
					}									
			
					//document.form1.submit();
					$.messager.confirm('Mensaje', '¿Seguro de Guardar los Datos del Postulante?', function(r){
						if (r){
							$('#form1').submit();	
						}
					});
			}
			
			function cambiartipodonante(){
				IdTipoDonante=$('#IdTipoDonante').combobox('getValue');		
				if(IdTipoDonante=='1' || IdTipoDonante=='3' || IdTipoDonante=='5'){
					$("#DivReceptor").show();
					$("#MensajeDesabilitarReceptor").hide();				
					
				}else{
					$("#DivReceptor").hide();					
					$("#MensajeDesabilitarReceptor").show();					
					//$("#DivReceptor").form('clear');
					$('#IdTipoRelacion').combobox('setValue', '0');					
				}
			}
			
			function cambiartipodonacion(){
				IdTipoDonacion=$('#IdTipoDonacion').combobox('getValue');		
				if(IdTipoDonacion=='2'){	
					$('#IdTipoDonante').combobox('setValue', '1');					
					$("#IdTipoDonante").combobox('readonly',true);	
					cambiartipodonante();			
				}else{
					$("#IdTipoDonante").combobox('readonly',false);					
				}
			}		
		
			//UBIGEO DOMICILIO	
			function CambiaDepartamentoD(){	
				var idDepartamento = $('#NomDepD').combobox('getValue');				
				$('#NomProvD').combogrid({				
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarProvinciasCombo&idDepartamento='+idDepartamento,
					idField:'IdProvincia', //ID QUE SE RECUPERA
					textField:'NomProvincia',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = CambiaProvinciaD(); },
					columns:[[
						{field:'NomProvincia',title:'Nombre',width:80}					
					]]
				});
				//limpiar combogrid distrito					
				$('#IdDistritoDomicilio').combogrid('setValue', '');	
				$('#IdCentroPobladoDomicilio').combobox('setValue', '');			
			}
		
		   function CambiaProvinciaD(){				
			   var idProvincias = $('#NomProvD').combogrid('getValue');				
				$('#IdDistritoDomicilio').combogrid({
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarDistritosCombo&idProvincias='+idProvincias,
					idField:'IdDistrito',
					textField:'NomDistrito',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = CambiaDistritoD(); },						
					columns:[[							
						{field:'NomDistrito',title:'Nombre',width:80}						
					]]
				});	
				$('#IdCentroPobladoDomicilio').combobox('setValue', '');			
		  } 
		  
		  function CambiaDistritoD(){
			   var idDistritos = $('#IdDistritoDomicilio').combogrid('getValue');				
				$('#IdCentroPobladoDomicilio').combogrid({
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarCentroPobladoCombo&idDistritos='+idDistritos,
					idField:'IdCentroPoblado',
					textField:'NomCentroPoblado',
					mode:'remote',
					fitColumns:true,						
					columns:[[							
						{field:'NomCentroPoblado',title:'Nombre',width:80}						
					]]
				});					  
		  }
		  
		  function CambiaPaisD(){
			 var IdPaisDomicilio = $('#IdPaisDomicilio').combobox('getValue'); 
			 if(IdPaisDomicilio!='166'){
				 $('#NomDepD').combobox('clear');					
				 $('#NomDepD' ).combobox({ disabled: true });
				 $('#NomProvD').combogrid('clear');					
				 $('#NomProvD' ).combogrid({ disabled: true });	
				 $('#IdDistritoDomicilio').combogrid('clear');					
				 $('#IdDistritoDomicilio' ).combogrid({ disabled: true });
				 $('#IdCentroPobladoDomicilio').combobox('clear');					
				 $('#IdCentroPobladoDomicilio' ).combobox({ disabled: true });		
			 }else{
				 //$('#IdPaisDomicilio').combobox('setText','166');						
				 $('#NomDepD' ).combobox({ disabled: false });				 			
				 $('#NomProvD' ).combogrid({ disabled: false });					
				 $('#IdDistritoDomicilio' ).combogrid({ disabled: false });	
				 $('#IdCentroPobladoDomicilio' ).combobox({ disabled: false });	 
			}			  
		  } 
		  
		  //UBIGEO PROCEDENCIA	
			function CambiaDepartamentoP(){	
				var idDepartamento = $('#NomDepP').combobox('getValue');				
				$('#NomProvP').combogrid({				
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarProvinciasCombo&idDepartamento='+idDepartamento,
					idField:'IdProvincia', //ID QUE SE RECUPERA
					textField:'NomProvincia',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = CambiaProvinciaP(); },
					columns:[[
						{field:'NomProvincia',title:'Nombre',width:80}					
					]]
				});
				//limpiar combogrid distrito					
				$('#IdDistritoProcedencia').combogrid('setValue', '');	
				$('#IdCentroPobladoProcedencia').combobox('setValue', '');				
			}
		
		   function CambiaProvinciaP(){				
			   var idProvincias = $('#NomProvP').combogrid('getValue');				
				$('#IdDistritoProcedencia').combogrid({
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarDistritosCombo&idProvincias='+idProvincias,
					idField:'IdDistrito',
					textField:'NomDistrito',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = CambiaDistritoP(); },							
					columns:[[							
						{field:'NomDistrito',title:'Nombre',width:80}						
					]]
				});	
				$('#IdCentroPobladoProcedencia').combobox('setValue', '');				
		  } 
		  
		  function CambiaDistritoP(){
			   var idDistritos = $('#IdDistritoProcedencia').combogrid('getValue');				
				$('#IdCentroPobladoProcedencia').combogrid({
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarCentroPobladoCombo&idDistritos='+idDistritos,
					idField:'IdCentroPoblado',
					textField:'NomCentroPoblado',
					mode:'remote',
					fitColumns:true,						
					columns:[[							
						{field:'NomCentroPoblado',title:'Nombre',width:80}						
					]]
				});					  
		  }
		  
		  function CambiaPaisP(){
			 var IdPaisProcedencia = $('#IdPaisProcedencia').combobox('getValue'); 
			 if(IdPaisProcedencia!='166'){
				 $('#NomDepP').combobox('clear');					
				 $('#NomDepP' ).combobox({ disabled: true });
				 $('#NomProvP').combogrid('clear');					
				 $('#NomProvP' ).combogrid({ disabled: true });	
				 $('#IdDistritoProcedencia').combogrid('clear');					
				 $('#IdDistritoProcedencia' ).combogrid({ disabled: true });
				 $('#IdCentroPobladoProcedencia').combobox('clear');					
				 $('#IdCentroPobladoProcedencia' ).combobox({ disabled: true });		
			 }else{
				 //$('#IdPaisProcedencia').combobox('setText','166');						
				 $('#NomDepP' ).combobox({ disabled: false });				 			
				 $('#NomProvP' ).combogrid({ disabled: false });					
				 $('#IdDistritoProcedencia' ).combogrid({ disabled: false });	
				 $('#IdCentroPobladoProcedencia' ).combobox({ disabled: false });	 
			}			  
		  }		  
		  
		  //UBIGEO NACIMIENTO	
			function CambiaDepartamentoN(){	
				var idDepartamento = $('#NomDepN').combobox('getValue');				
				$('#NomProvN').combogrid({				
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarProvinciasCombo&idDepartamento='+idDepartamento,
					idField:'IdProvincia', //ID QUE SE RECUPERA
					textField:'NomProvincia',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = CambiaProvinciaN(); },
					columns:[[
						{field:'NomProvincia',title:'Nombre',width:80}					
					]]
				});
				//limpiar combogrid distrito					
				$('#IdDistritoNacimiento').combogrid('setValue', '');
				$('#IdCentroPobladoNacimiento').combobox('setValue', '');				
			}
		
		   function CambiaProvinciaN(){			
			   var idProvincias = $('#NomProvN').combogrid('getValue');				
				$('#IdDistritoNacimiento').combogrid({
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarDistritosCombo&idProvincias='+idProvincias,
					idField:'IdDistrito',
					textField:'NomDistrito',
					mode:'remote',
					fitColumns:true,						
					onSelect: function(rec){
					var url = CambiaDistritoN(); },							
					columns:[[							
						{field:'NomDistrito',title:'Nombre',width:80}						
					]]
				});	
				$('#IdCentroPobladoNacimiento').combobox('setValue', '');			
		  } 
		  
		  function CambiaDistritoN(){
			   var idDistritos = $('#IdDistritoNacimiento').combogrid('getValue');				
				$('#IdCentroPobladoNacimiento').combogrid({
					panelWidth:250,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=MostrarCentroPobladoCombo&idDistritos='+idDistritos,
					idField:'IdCentroPoblado',
					textField:'NomCentroPoblado',
					mode:'remote',
					fitColumns:true,						
					columns:[[							
						{field:'NomCentroPoblado',title:'Nombre',width:80}						
					]]
				});				  
		  }
		  
		  function CambiaPaisN(){
			 var IdPaisNacimiento = $('#IdPaisNacimiento').combobox('getValue'); 
			 if(IdPaisNacimiento!='166'){
				 $('#NomDepN').combobox('clear');					
				 $('#NomDepN' ).combobox({ disabled: true });
				 $('#NomProvN').combogrid('clear');					
				 $('#NomProvN' ).combogrid({ disabled: true });	
				 $('#IdDistritoNacimiento').combogrid('clear');					
				 $('#IdDistritoNacimiento' ).combogrid({ disabled: true });
				 $('#IdCentroPobladoNacimiento').combobox('clear');					
				 $('#IdCentroPobladoNacimiento' ).combobox({ disabled: true });		
			 }else{
				 //$('#IdPaisProcedencia').combobox('setText','166');						
				 $('#NomDepN' ).combobox({ disabled: false });				 			
				 $('#NomProvN' ).combogrid({ disabled: false });					
				 $('#IdDistritoNacimiento' ).combogrid({ disabled: false });	
				 $('#IdCentroPobladoNacimiento' ).combobox({ disabled: false });	 
			}			  
		  }			
			
		  function copiachkigualdomP(){
			if(document.getElementById('chkigualdomP').checked==true){
				IdPaisDomicilio=$('#IdPaisDomicilio').combobox('getValue');
				$('#IdPaisProcedencia').combobox('setValue',IdPaisDomicilio);//value
				NomPaisDomicilio=$('#IdPaisDomicilio').combobox('getText');	
				$('#IdPaisProcedencia').combobox('setText',NomPaisDomicilio);//text
				CambiaPaisP();	
				
				IdDepD=$('#NomDepD').combobox('getValue');
				$('#NomDepP').combobox('setValue',IdDepD);//value
				NomDepD=$('#NomDepD').combobox('getText');	
				$('#NomDepP').combobox('setText',NomDepD);//text
				CambiaDepartamentoP();	
								
				IdProvD=$('#NomProvD').combogrid('getValue');
				$('#NomProvP').combogrid('setValue',IdProvD);//value
				NomProvD=$('#NomProvD').combogrid('getText');
				$('#NomProvP').combogrid('setText',NomProvD);//text					
				CambiaProvinciaP();
							
				IdDistritoDomicilio=$('#IdDistritoDomicilio').combogrid('getValue');
				$('#IdDistritoProcedencia').combogrid('setValue',IdDistritoDomicilio);//value
				NomDistritoDomicilio=$('#IdDistritoDomicilio').combogrid('getText');
				$('#IdDistritoProcedencia').combogrid('setText',NomDistritoDomicilio);//text
				CambiaDistritoP();
				
				IdCentroPobladoDomicilio=$('#IdCentroPobladoDomicilio').combobox('getValue');
				$('#IdCentroPobladoProcedencia').combobox('setValue',IdCentroPobladoDomicilio);//value
				NombreCentroPobladoDomicilio=$('#IdCentroPobladoDomicilio').combobox('getText');	
				$('#IdCentroPobladoProcedencia').combobox('setText',NombreCentroPobladoDomicilio);//text
								
			}else{				
				$('#NomDepP').combobox('setValue','0');//value				
				$('#NomDepP').combobox('setText','');//text				
				$('#NomProvP').combogrid('setValue','');//value				
				$('#NomProvP').combogrid('setText','');//text					
				$('#IdDistritoProcedencia').combogrid('setValue','');//value			
				$('#IdDistritoProcedencia').combogrid('setText','');//text	
				$('#IdCentroPobladoProcedencia').combobox('setValue','');//value				
				$('#IdCentroPobladoProcedencia').combobox('setText','');//text	
			}		
		}
		
		function copiachkigualdomN(){ 
			if(document.getElementById('chkigualdomN').checked==true){
				IdPaisDomicilio=$('#IdPaisDomicilio').combobox('getValue');
				$('#IdPaisNacimiento').combobox('setValue',IdPaisDomicilio);//value
				NomPaisDomicilio=$('#IdPaisDomicilio').combobox('getText');	
				$('#IdPaisNacimiento').combobox('setText',NomPaisDomicilio);//text
				CambiaPaisN();
				
				IdDepD=$('#NomDepD').combobox('getValue');
				$('#NomDepN').combobox('setValue',IdDepD);//value
				NomDepD=$('#NomDepD').combobox('getText');	
				$('#NomDepN').combobox('setText',NomDepD);//text
				CambiaDepartamentoN();	
								
				IdProvD=$('#NomProvD').combogrid('getValue');
				$('#NomProvN').combogrid('setValue',IdProvD);//value
				NomProvD=$('#NomProvD').combogrid('getText');
				$('#NomProvN').combogrid('setText',NomProvD);//text	
				CambiaProvinciaN();				
							
				IdDistritoDomicilio=$('#IdDistritoDomicilio').combogrid('getValue');
				$('#IdDistritoNacimiento').combogrid('setValue',IdDistritoDomicilio);//value
				NomDistritoDomicilio=$('#IdDistritoDomicilio').combogrid('getText');
				$('#IdDistritoNacimiento').combogrid('setText',NomDistritoDomicilio);//text
				CambiaDistritoN();
				
				IdCentroPobladoDomicilio=$('#IdCentroPobladoDomicilio').combobox('getValue');
				$('#IdCentroPobladoNacimiento').combobox('setValue',IdCentroPobladoDomicilio);//value
				NombreCentroPobladoDomicilio=$('#IdCentroPobladoDomicilio').combobox('getText');	
				$('#IdCentroPobladoNacimiento').combobox('setText',NombreCentroPobladoDomicilio);//text
								
			}else{				
				$('#NomDepN').combobox('setValue','0');//value				
				$('#NomDepN').combobox('setText','');//text				
				$('#NomProvN').combogrid('setValue','');//value				
				$('#NomProvN').combogrid('setText','');//text					
				$('#IdDistritoNacimiento').combogrid('setValue','');//value			
				$('#IdDistritoNacimiento').combogrid('setText','');//text	
				$('#IdCentroPobladoNacimiento').combobox('setValue','');//value				
				$('#IdCentroPobladoNacimiento').combobox('setText','');//text	
			}		
		}
			
			$.extend( $( "#FechaNacimiento" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#FechaNacimiento" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaNacimiento" ).datebox('setValue', '');
							$("#EdadPaciente").numberbox('setValue','');
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 

			
			function cancelar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RecepcionPostulante&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}									
			
			function calcularfecnac(){						
				if(document.getElementById("chkfecnac").checked==true){							
					$("#FechaNacimiento").datebox('readonly',true);				
					$("#EdadPaciente").numberbox('readonly',false);
					calcularfechanac();	
											
				}else{																		
					$("#FechaNacimiento").datebox('readonly',false);				
					$("#EdadPaciente").numberbox('readonly',true);
					calcularedad();										
				}			
			}

			function calcularfechanac(){
			  if(document.getElementById("chkfecnac").checked==true){				
				EdadPaciente=$("#EdadPaciente").numberbox('getText');				
				if(EdadPaciente==""){ // || IdTipoEdad=="0"
					$('#FechaNacimiento').datebox('setValue','');
					//$.messager.alert('Mensaje','Ingrese Edad Actual','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Edad Actual',
						icon:'info',
						fn: function(){
							$('#EdadPaciente').next().find('input').focus();
						}
					});	
					$('#EdadPaciente').next().find('input').focus();
					
				}else{							
					//fecha actual									
					f=new Date();
					var y = parseInt(f.getFullYear());
					var m = parseInt(f.getMonth()+1);
					var d = parseInt(f.getDate());	
					FechaNacimiento=d+'/'+m+'/'+(y-EdadPaciente);												
					$('#FechaNacimiento').datebox('setValue',FechaNacimiento);					
				}
			  }		
			}			
			
			function calcularedad(){					
				fecha=$("#FechaNacimiento").datebox('getValue');//getText			
				if(fecha!=""){	
					//validar fecha actual					
					fnacarray= fecha.split('/');
					fnac = new Date(fnacarray[2], fnacarray[1]-1 , fnacarray[0]);					
					f=new Date();		
					factual = new Date(f.getFullYear() , (f.getMonth()) , f.getDate());							
					if(fnac>factual){ //validar fecha actual								
						$.messager.alert('Mensaje','La Fecha de Nacimiento debe ser menor a la Fecha actual','info');
						$('#FechaNacimiento').next().find('input').focus();
						$('#FechaNacimiento').datebox('setValue','');
						$('#EdadPaciente').numberbox('setValue','');								
						return 0;			
					}
													
					//calculo la fecha que recibo
					//La descompongo en un array
					var array_fecha = fecha.split("/");												
					//si el array no tiene tres partes, la fecha es incorrecta
					if (array_fecha.length!=3){
					   //$.messager.alert('Mensaje','Fecha Incorrecta','warning');					   
					}else{									
						//compruebo que los ano, mes, dia son correctos								
						var ano = parseInt(array_fecha[2]);								
						var mes = parseInt(array_fecha[1]);								
						var dia = parseInt(array_fecha[0]);							
						//calculo la fecha de hoy
						hoy=new Date();
						var ahora_ano = hoy.getYear();
						var ahora_mes = hoy.getMonth()+1;						
						var ahora_dia = hoy.getDate();
																		
						//CALCULO DE AÑOS							
							//resto los años de las dos fechas								
							var edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año	
																						
							//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
							if (hoy.getMonth() + 1 - mes < 0){ //+ 1 porque los meses empiezan en 0
							   edadanos=edad;
							}else if (hoy.getMonth() + 1 - mes > 0){
							   edadanos=edad+1;	
							}else{													
								//entonces es que eran iguales. miro los dias								
								//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
								if (hoy.getUTCDate() - dia >= 0){
								   edadanos=edad+1;
								}else{
								   edadanos=edad;
								}									 
							}								   							   	  
						   //alert(edadanos); //edad en anos
						   EdadPaciente=edadanos;
						   //IdTipoEdad=1;
						   var EdadMinimaDonante=document.getElementById('EdadMinimaDonante').value;
						   var EdadMaximaDonante=document.getElementById('EdadMaximaDonante').value;							
							if(edadanos<parseInt(EdadMinimaDonante)){								
								$('#EdadPaciente').numberbox('setValue','');
								$.messager.alert('Mensaje','El Postulante debe haber cumplido los '+EdadMinimaDonante+' años','info');
								return 0;															
							}
							if(edadanos>parseInt(EdadMaximaDonante)){								
								$('#EdadPaciente').numberbox('setValue','');
								$.messager.alert('Mensaje','El Postulante debe tener menos de '+EdadMaximaDonante+' años','info');
								return 0;															
							}											    
					}							
					$('#EdadPaciente').numberbox('setValue',EdadPaciente);						
				}else{
					$('#EdadPaciente').numberbox('setValue','');
						//$.messager.alert('Mensaje','Ingrese Fecha de Nacimiento','warning');
						//return 0;	
				}	
			}
			
			function LlenarManual(){
				if(document.getElementById('chkmanual').checked==true){													
					$("#NroDocumento").numberbox('setValue','');				
					$("#NroDocumento").numberbox('readonly',false);	
															
					$("#ApellidoPaterno").textbox('setValue','');
					$("#ApellidoPaterno").textbox('readonly',false);												
					$("#ApellidoMaterno").textbox('setValue','');				
					$("#ApellidoMaterno").textbox('readonly',false);		
									
					$("#PrimerNombre").textbox('setValue','');				
					$("#PrimerNombre").textbox('readonly',false);	
					$("#SegundoNombre").textbox('setValue','');				
					$("#SegundoNombre").textbox('readonly',false);	
					$("#TercerNombre").textbox('setValue','');				
					$("#TercerNombre").textbox('readonly',false);	
					
					$("#FechaNacimiento").datebox('setValue','');				
					$("#FechaNacimiento").datebox('readonly',false);						
					$("#EdadPaciente").numberbox('setValue','');					
					//$("#EdadPaciente").numberbox('readonly',false);		
					$("#IdTipoSexo").combobox('setValue','');
					$("#IdTipoSexo").combobox('readonly',false);
					
					$("#IdEstadoCivil").combobox('setValue',0);			
					$("#IdTipoOcupacion").combobox('setValue',0);						
					$("#Telefono").numberbox('setValue','');						
					$("#IdGrupoSanguineo").combobox('setValue',0);						
					$("#Email").textbox('setValue','');									
					
					$("#NroDocumentoBus").numberbox('setValue','');					
					$("#NroDocumentoBus").numberbox('readonly',true);	
					$("#NroDocumento").next().find("input").focus();
					
					$('#CodigoPostulante').textbox('setValue',''); 
					document.getElementById('IdPostulante').value='';
					$('#MensajeExiste').html('');
					
				}else{
					$("#NroDocumento").numberbox('setValue','');				
					$("#NroDocumento").numberbox('readonly',true);	
															
					$("#ApellidoPaterno").textbox('setValue','');
					$("#ApellidoPaterno").textbox('readonly',true);												
					$("#ApellidoMaterno").textbox('setValue','');				
					$("#ApellidoMaterno").textbox('readonly',true);		
									
					$("#PrimerNombre").textbox('setValue','');				
					$("#PrimerNombre").textbox('readonly',true);	
					$("#SegundoNombre").textbox('setValue','');				
					$("#SegundoNombre").textbox('readonly',true);	
					$("#TercerNombre").textbox('setValue','');				
					$("#TercerNombre").textbox('readonly',true);	
					
					$("#FechaNacimiento").datebox('setValue','');				
					$("#FechaNacimiento").datebox('readonly',true);						
					$("#EdadPaciente").numberbox('setValue','');					
					//$("#EdadPaciente").numberbox('readonly',true);	
					$("#IdTipoSexo").combobox('setValue','');
					$("#IdTipoSexo").combobox('readonly',true);						
						
					$("#IdEstadoCivil").combobox('setValue',0);			
					$("#IdTipoOcupacion").combobox('setValue',0);						
					$("#Telefono").numberbox('setValue','');						
					$("#IdGrupoSanguineo").combobox('setValue',0);						
					$("#Email").textbox('setValue','');							
						
					$("#NroDocumentoBus").numberbox('setValue','');			
					$("#NroDocumentoBus").numberbox('readonly',false);	
					$("#NroDocumentoBus").next().find("input").focus();				
				}				
			}
			
			/*$(function(){
				
				$.extend($.fn.textbox.methods, {
					show: function(jq){
						return jq.each(function(){
							$(this).next().show();
						})
					},
					hide: function(jq){
						return jq.each(function(){
							$(this).next().hide();
						})
					}
				})
									
			});*/
			
			function ObtenergsPaciente(IdPaciente){				
					 //IdPaciente=document.getElementById('IdPaciente').value;
					 //if(document.getElementById('chkgs').checked==true && IdPaciente.trim()!=""){
					    $('#tt').datagrid({
							title:'Grupo Sanguineo Paciente',
							width:300,
							//height:100,	
							url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ObtenerGrupoSanguineoPaciente&IdPaciente="+IdPaciente,
							idField:'idOrden',
							textField:'ValorTexto',
							mode:'remote',
							fitColumns:true,										
							columns:[[							
								{field:'idOrden',title:'Orden',width:50},
								{field:'ValorTexto',title:'Valor',width:80},
								{field:'Fecha',title:'Fecha',width:50}
								//{field:'RealizaPrueba',title:'RealizaPrueba',width:80}
														
							]]													
						});
					 /*}else{	
					 	 //$('#tt').datagrid('loadData', {"total":0,"rows":[]});
						 $.messager.alert('Mensaje','Falta Buscar al Receptor (Paciente)','warning');	              			 				 
					}*/
			}
			
			$(function(){
				
				$('#NroDocumentoBus').next().find('input').focus();				
				$('#tabstt').tabs({
					border:false,
					onSelect:function(title){
						//alert(title+' is selected');
						if(title.trim()=='2. Registro del Receptor (Paciente)'){
							$('#NroHistoriaClinicaBusRec').next().find('input').focus();
						}
					}
				});
				
			});	
			
			function cambiarGrupoSanguineoRec(){
				GrupoSanguineoRec=$('#IdGrupoSanguineoRec').combobox('getText');	
				document.getElementById('GrupoSanguineoRec').value=GrupoSanguineoRec;
			}			
			
		</script>
</head>
<body>

<form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarRecepcion&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
   
<div id="p" class="easyui-panel" style="width:90%;height:auto;"
   title="Banco de Sangre: Registrar Datos Recepción Postulante" iconCls="icon-save" align="left"> <!--collapsible="true"  --> 
   
	<div class="easyui-panel" style="padding:0px;">    	
        <!--<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Refrescar</a>-->         								        
        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="guardar()">Guardar</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="cancelar();">Cancelar(ESC)</a>   
        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'" onClick="cancelar()">Volver a Cargar</a>          
	</div>
 
 <div id="tabstt" class="easyui-tabs" >
   <div title="1. Registro del Postulante" style="padding:10px">
        <table width="956" border="0">
            <tr>
                <td width="950">                 
                    <!--<table>
                        <tr>
                            <td>
                                <label>Buscar en:</label>         	
                            </td>
                            <td>  
                                <input type="radio" name="chkbu" id="chkpost" onClick="busquedapost();" checked />Ninguno  
                                <input type="radio" onClick="busquedapost();" name="chkbu" id="chkburen" />Reniec                                  
                                <input type="radio" onClick="busquedapost();" name="chkbu" id="chkbupos"/>Postulantes                  
                            </td>    
                        </tr>
                    </table>  <br>--> 
					
					<fieldset>
					  <legend>Datos de Busqueda</legend>
						<table border="0" cellspacing="4">
						   
								<tr>
								  <td width="84">Tipo  Dcto</td>
								  <td width="126"><select class="easyui-combobox" name="IdDocIdentidadBus" id="IdDocIdentidadBus"  style="width: 105px" data-options="required:true,onChange: function(rec){ var url = validarDocumentoBus(); }" >
								    <option value="0">0=SIN DOCUMENTO</option>
								    <?php
                                  $listarDoc=SIGESA_Emergencia_ListarTipoDocumento_M();
                                   if($listarDoc != NULL) { 
                                     foreach($listarDoc as $item){?>
								    <option value="<?php echo $item["IdDocIdentidad"]?>" <?php if($item["IdDocIdentidad"]=='1'){?> selected <?php } ?> ><?php echo $item["IdDocIdentidad"]."=".$item["Descripcion"]?></option>
								    <?php } } ?>
							      </select></td>
									<td width="104">Nro Documento</td>
									<td width="126">
                                        <input class="easyui-numberbox" style="width:110px" id="NroDocumentoBus" name="NroDocumentoBus" data-options="prompt:'Nº Documento',
                                            formatter:function(v){
                                                var s = new String(v||'');                                                                                              
                                                return s;
                                            },
                                            parser:function(s){
                                                return s;
                                            }
                                            " maxlength="8" />
                                    </td>
									<td width="153">Apellidos y Nombres</td>									
									<td width="200"><input class="easyui-combogrid" style="width:200px" id="ApellidosNombresBus" name="ApellidosNombresBus"  data-options="prompt:'Apellidos y Nombres'"></td> 
                                                                                                
									<td width="109">                                    
                                    	<?php 
											$EDMIN=SIGESA_ListarParametros_M('EDMIN');
											$EdadMinimaDonante=$EDMIN[0]["Valor1"];																						
											$EDMAX=SIGESA_ListarParametros_M('EDMAX');
											$EdadMaximaDonante=$EDMAX[0]["Valor1"];																							
										?>
                                     	<input type="hidden" id="EdadMinimaDonante" name="EdadMinimaDonante" value="<?php echo $EdadMinimaDonante; ?>" />
                                        <input type="hidden" id="EdadMaximaDonante" name="EdadMaximaDonante" value="<?php echo $EdadMaximaDonante; ?>" />            							                                        
                                    </td>									
								</tr>						   
						</table>
					</fieldset>				
                </td>
            </tr>
			
			<tr>
				<td>
					<!--INICIO TABLA POSTULANTES-->
					<table width="948" height="204">
						<tr>
							<td width="60%">				
								<fieldset>
								<legend>Datos Personales del Postulante</legend>
									<table width="100%">
										<tr>
										  <td>Nro Postulante</td>
										  <td>
                                          	<input class="easyui-textbox" style="width:105px" id="CodigoPostulante" name="CodigoPostulante" data-options="prompt:'Autogenerado'" readonly />
                                            <input type="hidden" id="IdPostulante" name="IdPostulante" />
                                          </td>
										  <td><div id="MensajeExiste" style="color:#F00;"></div></td>
                                          <td>Manual<input name="chkmanual" id="chkmanual" type="checkbox" value="1" onClick="LlenarManual()" /></td>
									  </tr>
										<tr>
											<td width="24%">Tipo  Dcto</td>
											<td width="26%"><!--<button class="btn">.</button>-->
											  <select class="easyui-combobox" name="IdDocIdentidad" id="IdDocIdentidad"  style="width: 105px" data-options="required:true,onChange: function(rec){ var url = validarDocumento(); }">
											    <option value="0">0=SIN DOCUMENTO</option>
											    <?php
                                  $listarDoc=SIGESA_Emergencia_ListarTipoDocumento_M();
                                   if($listarDoc != NULL) { 
                                     foreach($listarDoc as $item){?>
											    <option value="<?php echo $item["IdDocIdentidad"]?>" <?php if($item["IdDocIdentidad"]=='1'){?> selected <?php } ?> ><?php echo $item["IdDocIdentidad"]."=".$item["Descripcion"]?></option>
											    <?php } } ?>
									        </select></td>
											<td width="25%">Nro Dcto</td>
											<td width="25%">
                                            <input name="NroDocumento" class="easyui-numberbox" id="NroDocumento" style="width:117px" data-options="prompt:'Nº Documento',required:true,onChange: function(rec){ var url = validarDNIpostulante(); },
                                            formatter:function(v){
                                                var s = new String(v||'');                                                                                              
                                                return s;
                                            },
                                            parser:function(s){
                                                return s;
                                            }
                                            " maxlength="8" readonly />
                                            <input name="errorPostulante" id="errorPostulante" type="hidden" />
                                            </td>  
										</tr>
										<tr>
										  <td>Ap.Paterno </td>
										  <td><input name="ApellidoPaterno"  class="easyui-textbox" id="ApellidoPaterno" style="width:105px" readonly data-options="prompt:'Ap.Paterno',validType:'justText',required:true" /></td>
										 <td>Ap.Materno</td>
										 <td><input name="ApellidoMaterno" class="easyui-textbox" id="ApellidoMaterno" style="width:117px" readonly data-options="prompt:'Ap.Materno',validType:'justText',required:true" /></td>
									    </tr>
										<tr>
										  <td>1.Nombre</td>
										  <td><input name="PrimerNombre" class="easyui-textbox" id="PrimerNombre" style="width:105px" readonly data-options="prompt:'1er Nombre',validType:'justText',required:true" /></td>
											<td>2.Nombre</td>
											<td><input name="SegundoNombre" class="easyui-textbox" id="SegundoNombre" style="width:117px" readonly data-options="prompt:'2do Nombre',validType:'justText'" /></td>
										</tr>
										<tr>
										  <td>3.Nombre </td>
										  <td><input name="TercerNombre" class="easyui-textbox" id="TercerNombre" style="width:105px" readonly data-options="prompt:'3er Nombre',validType:'justText'" /></td>
										  <td>Fecha Nacim.</td>
										  <td><input name="FechaNacimiento" class="easyui-datebox" id="FechaNacimiento" style="width:105px" readonly data-options="required:true,onChange: function(rec){ var url = calcularedad(); }" validType="validDate" />
									      <input type="checkbox" onClick="calcularfecnac();" name="chkfecnac" id="chkfecnac" value="1" /></td>
										</tr>
										<tr>
										  <td>Edad Actual</td>
										  <td><input style="width:105px" class="easyui-numberbox" id="EdadPaciente" name="EdadPaciente" data-options="required:true,onChange: function(rec){ var url = calcularfechanac(); }" readonly /></td>
										  <td>Sexo </td>
										  <td>
											 <select class="easyui-combobox" name="IdTipoSexo" id="IdTipoSexo"  style="width: 117px" data-options="prompt:'Seleccione',required:true" >
												<option value="0"></option>
												 <?php
												 $listarSexo=SIGESA_Emergencia_ListarTiposSexo_M();
												 if($listarSexo != NULL) { 
													foreach($listarSexo as $item){?>
												<option value="<?php echo $item["IdTipoSexo"]?>" ><?php echo $item["IdTipoSexo"]."=".$item["Descripcion"]?></option>
												<?php } } ?>
											</select>
										  </td>
										</tr>
										<tr>
											<td>Estado Civil</td>
											<td><select class="easyui-combobox" name="IdEstadoCivil" id="IdEstadoCivil"  style="width:105px" data-options="prompt:'Seleccione',required:true" >
											  <option value="0"></option>
											  <?php
										  $listarEC=SIGESA_Emergencia_ListarEstadosCiviles_M();
										   if($listarEC != NULL) { 
											 foreach($listarEC as $item){?>
											  <option value="<?php echo $item["IdEstadoCivil"]?>" ><?php echo $item["Descripcion"]?></option>
											  <?php } } ?>
											</select></td>
											<td>Ocupación</td>
											<td><select class="easyui-combobox" name="IdTipoOcupacion" id="IdTipoOcupacion" style="width: 117px" data-options="prompt:'Seleccione',required:true" >
											  <option value="0"></option>
											  <?php
										  $listarOcu=SIGESA_Emergencia_ListarTiposOcupacion_M();
										   if($listarOcu != NULL) { 
											 foreach($listarOcu as $item){?>
											  <option value="<?php echo $item["IdTipoOcupacion"]?>" ><?php echo trim($item["Descripcion"])?></option>
											  <?php } } ?>
											</select></td>
										</tr>
										<tr>
										  <td>Teléfono</td>
										  <td><input style="width:105px" class="easyui-numberbox" name="Telefono" id="Telefono" data-options="prompt:'Telefono',required:true" /></td>
										  <td>Grupo Sanguineo</td>
										  <td><select class="easyui-combobox" name="IdGrupoSanguineo" id="IdGrupoSanguineo"  style="width: 117px" >
											<option value="0">No sabe</option>
											<?php
												  $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
												   if($listarSexo != NULL) { 
													 foreach($listarSexo as $item){?>
											<option value="<?php echo $item["IdGrupoSanguineo"]?>" ><?php echo $item["IdGrupoSanguineo"].'='.$item["Descripcion"]?></option>
											<?php } } ?>
										  </select></td>
										</tr>
										<tr>
										  <td>Email</td>
										  <td colspan="3"><input style="width:95%" class="easyui-textbox" name="Email" id="Email" data-options="prompt:'Email'" /></td>
										</tr>
									</table>
								</fieldset>
							</td>
					
							<td width="40%">			
								<fieldset>  
								<legend>Datos Detallados del Postulante</legend>  
									<table width="100%">				
											<tr>
											  <td>Tipo Donación</td>
											  <td><select class="easyui-combobox" name="IdTipoDonacion" id="IdTipoDonacion"  style="width: 150px" data-options="prompt:'Seleccione',required:true,
												valueField: 'id',
												textField: 'text',        
												onSelect: function(rec){
												var url = cambiartipodonacion(); }">
											    <option value="0"></option>
											    <?php
															  $listar=SIGESA_BSD_ListarTipoDonacion_M();
															   if($listar != NULL) { 
																 foreach($listar as $item){?>
											    <option value="<?php echo $item["IdTipoDonacion"]?>" <?php if($item["IdTipoDonacion"]=='1'){?> selected <?php } ?> ><?php echo $item["IdTipoDonacion"].'='.$item["Descripcion"]?></option>
											    <?php } } ?>
										      </select></td>
									  </tr>
											<tr>
											  <td> Tipo Donante</td>
											  <td><select class="easyui-combobox" name="IdTipoDonante" id="IdTipoDonante"  style="width: 150px" data-options="prompt:'Seleccione',required:true,
												valueField: 'id',
												textField: 'text',        
												onSelect: function(rec){
												var url = cambiartipodonante(); }">
										        <option value="0"></option>
											      <?php
															  $listar=SIGESA_BSD_ListarTipoDonante_M();
															   if($listar != NULL) { 
																 foreach($listar as $item){?>
											      <option value="<?php echo $item["IdTipoDonante"]?>" <?php if($item["IdTipoDonante"]=='1'){?> selected <?php } ?> ><?php echo $item["IdTipoDonante"].'='.$item["Descripcion"]?></option>
											      <?php } } ?>
									          </select></td>
											</tr>
											<tr>
											  <td>Tipo Relacion</td>
											  <td><select class="easyui-combobox" name="IdTipoRelacion" id="IdTipoRelacion"  style="width: 150px" data-options="prompt:'Seleccione',required:true">
											    <option value=""></option>
											    <?php
																  $listar=ListarTipoRelacionM();
																   if($listar != NULL) { 
																	 foreach($listar as $item){?>
											    <option value="<?php echo $item["IdTipoRelacion"]?>" ><?php echo $item["IdTipoRelacion"].'='.$item["Descripcion"]?></option>
											    <?php } } ?>
										      </select></td>
											</tr>
											<tr>
											  <td>Condicion Donante</td>
											  <td><select class="easyui-combobox" name="IdCondicionDonante" id="IdCondicionDonante"  style="width: 150px" data-options="prompt:'Seleccione',required:true">
												<option value="0"></option>
												<?php
															  $listarSexo=SIGESA_BSD_ListarCondicionDonante_M();
															   if($listarSexo != NULL) { 
																 foreach($listarSexo as $item){?>
												<option value="<?php echo $item["IdCondicionDonante"]?>" ><?php echo $item["Descripcion"]?></option>
												<?php } } ?>
											  </select></td>
											</tr>
                                            
											<tr>
											  <td>Fecha Recepcion</td>
											  <td><input class="easyui-datebox" value="<?php echo date('d/m/Y');?>" style="width:150px;" id="FechaMovi" name="FechaMovi" data-options="required:true" /></td>
									  		</tr>
                                                                                        
											<tr>
											  <td>Hora Recepcion</td>
											  <td><input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" style="width:150px;" id="HoraMovi" name="HoraMovi" data-options="showSeconds:true,required:true" >
												<input type="hidden" value="<?php echo $_REQUEST['IdEmpleado'];?>" style="width:150px;" id="UsuarioReg" name="UsuarioReg"/></td>
											</tr>
                                            
											<tr>
											  <td>Observaciones</td>
											  <td><input style="width:150px;height:70px" class="easyui-textbox" multiline="true" name="Observaciones" id="Observaciones" /></td>
											</tr>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
					<!--FIN TABLA POSTULANTES-->			  
			</td>		
		</tr>
    </table>
	
	
   <div class="easyui-tabs"  >
   <div title="1.1)Datos de Domicilio(F7)" style="padding:10px;">
       <fieldset>
             <table width="950" border="0" cellspacing="4">
                <thead>
                    <tr>
                        <td>Departamento</td>
                        <td>                       
                         <?php 
						  $Departamento = DepartamentosSeleccionarTodos(); ?>
                             <select name="NomDepD" id="NomDepD" class="easyui-combobox" data-options="prompt:'Seleccione',required:true,
                                valueField: 'id',
                                textField: 'text',        
                                onSelect: function(rec){
                               var url = CambiaDepartamentoD();
                                }" style="width:130px;">
                                <option value="0"></option>                                      
								<?php foreach($Departamento as $DepItem){?>
                                <option value="<?php echo $DepItem["IdDepartamento"]?>" ><?php echo trim(mb_strtoupper($DepItem["Nombre"]))?></option>
                                <?php }	?>
                           </select>                        
                        </td>
                        <td>Provincia</td>
                        <td>                     	
							 <select id="NomProvD" class="easyui-combogrid" name="NomProvD" style="width:130px;"
								data-options="prompt:'Seleccione',required:true">
							</select>
                        </td>
                        <td>Distrito</td>
                        <td>                    
                            <select id="IdDistritoDomicilio" class="easyui-combogrid" name="IdDistritoDomicilio" style="width:150px;"
                            	data-options="prompt:'Seleccione',required:true">
                            </select>
                       </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Centro Poblado</td>
                        <td>                        	
                        	<select class="easyui-combobox" id="IdCentroPobladoDomicilio" name="IdCentroPobladoDomicilio" style="width:130px;">
                            	<option value="0"></option>
                            </select>
                        </td>
                        <td>Pais</td>
                        <td>                        	
                            <?php $Paises = ListarPaises_M(); ?>
                            <select name="IdPaisDomicilio" id="IdPaisDomicilio" class="easyui-combobox" style="width:130px;" data-options="prompt:'Seleccione',required:true,
                                valueField: 'id',
                                textField: 'text',        
                                onSelect: function(rec){
                                var url = CambiaPaisD();
                                }">
                                <option value="0"></option>                                      
								<?php foreach($Paises as $PaisesItem){?>
                                <option value="<?php echo $PaisesItem["IdPais"]?>" <?php if($PaisesItem["IdPais"]=='166'){?> selected <?php } ?>><?php echo trim(mb_strtoupper($PaisesItem["Nombre"]))?></option>
                                <?php }	?>
                           </select>
                        </td>
                        <td>Direccion</td>
                        <td><input class="easyui-textbox" name="DireccionDomicilio" id="DireccionDomicilio" style="width:250px;" data-options="required:true"></td>
                    
                    </tr>
                </tbody>
            </table>
        </fieldset>
   </div>
       <div title="1.2)Datos de Procedencia(F8))" style="padding:10px;">
           <fieldset>
                      <table width="950" border="0" cellspacing="4">
                         
                <thead>
                    <tr>
                        <td>Departamento</td>
                        <td><?php 
						  $Departamento = DepartamentosSeleccionarTodos(); ?>
                          <select name="NomDepP" id="NomDepP" class="easyui-combobox" data-options="prompt:'Seleccione',required:true,
                                valueField: 'id',
                                textField: 'text',        
                                onSelect: function(rec){
                               var url = CambiaDepartamentoP();
                                }" style="width:130px;">
                            <option value="0"></option>
                            <?php foreach($Departamento as $DepItem){?>
                            <option value="<?php echo $DepItem["IdDepartamento"]?>" ><?php echo trim(mb_strtoupper($DepItem["Nombre"]))?></option>
                            <?php }	?>
                        </select></td>
                        <td>Provincia</td>
                        <td>
                          <select id="NomProvP" class="easyui-combogrid" name="NomProvP" style="width:130px;"
								data-options="prompt:'Seleccione',required:true">
                        </select></td>
                        <td>Distrito</td>
                        <td>
                          <select id="IdDistritoProcedencia" class="easyui-combogrid" name="IdDistritoProcedencia" style="width:150px;"
                            	data-options="prompt:'Seleccione',required:true">
                        </select></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Centro Poblado</td>
                        <td>
                            <select class="easyui-combobox" id="IdCentroPobladoProcedencia" name="IdCentroPobladoProcedencia" style="width:130px;">
                              <option value="0"></option>
                            </select>
                        </td>
                        <td>Pais</td>
                        <td><?php $Paises = ListarPaises_M(); ?>
                          <select name="IdPaisProcedencia" id="IdPaisProcedencia" class="easyui-combobox" style="width:130px;" data-options="prompt:'Seleccione',required:true,
                                valueField: 'id',
                                textField: 'text',        
                                onSelect: function(rec){
                                var url = CambiaPaisP();
                                }">
                            <option value="0"></option>
                            <?php foreach($Paises as $PaisesItem){?>
                            <option value="<?php echo $PaisesItem["IdPais"]?>" <?php if($PaisesItem["IdPais"]=='166'){?> selected <?php } ?>><?php echo trim(mb_strtoupper($PaisesItem["Nombre"]))?></option>
                            <?php }	?>
                        </select></td>
                        <td><input type="checkbox" name="chkigualdomP" id="chkigualdomP" onClick="copiachkigualdomP()"></td>
                        <td>Igual que el Domicilio</td>
                    
                    </tr>
                </tbody>
            </table>
          </fieldset>
       </div>
       <div title="1.3)Datos de Nacimiento(F9)" style="padding:10px;">
            <fieldset>
                <table width="950" border="0" cellspacing="4">
                         
                <thead>
                      <tr>
                        <td>Buscar por Ubigeo</td>
                        <td><input class="easyui-numberbox" style="width:130px;" id="UbigeoN" name="UbigeoN" data-options="prompt:'Ubigeo Nacimiento'" maxlength="6"></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>Departamento</td>
                        <td><?php 
						  $Departamento = DepartamentosSeleccionarTodos(); ?>
                          <select name="NomDepN" id="NomDepN" class="easyui-combobox" data-options="prompt:'Seleccione',required:true,
                                valueField: 'id',
                                textField: 'text',        
                                onSelect: function(rec){
                               var url = CambiaDepartamentoN();
                                }" style="width:130px;">
                            <option value="0"></option>
                            <?php foreach($Departamento as $DepItem){?>
                            <option value="<?php echo $DepItem["IdDepartamento"]?>" ><?php echo trim(mb_strtoupper($DepItem["Nombre"]))?></option>
                            <?php }	?>
                        </select></td>
                        <td>Provincia</td>
                        <td>
                          <select id="NomProvN" class="easyui-combogrid" name="NomProvN" style="width:130px;"
								data-options="prompt:'Seleccione',required:true">
                        </select></td>
                        <td>Distrito</td>
                        <td>
                          <select id="IdDistritoNacimiento" class="easyui-combogrid" name="IdDistritoNacimiento" style="width:150px;"
                            	data-options="prompt:'Seleccione',required:true">
                        </select></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Centro Poblado</td>
                        <td>
                       	  <select class="easyui-combobox" id="IdCentroPobladoNacimiento" name="IdCentroPobladoNacimiento" style="width:130px;">
                              <option value="0"></option>
                          </select>
                        </td>
                        <td>Pais</td>
                        <td><?php $Paises = ListarPaises_M(); ?>
                          <select name="IdPaisNacimiento" id="IdPaisNacimiento" class="easyui-combobox" style="width:130px;" data-options="prompt:'Seleccione',required:true,
                                valueField: 'id',
                                textField: 'text',        
                                onSelect: function(rec){
                                var url = CambiaPaisN();
                                }">
                            <option value="0"></option>
                            <?php foreach($Paises as $PaisesItem){?>
                            <option value="<?php echo $PaisesItem["IdPais"]?>" <?php if($PaisesItem["IdPais"]=='166'){?> selected <?php } ?>><?php echo trim(mb_strtoupper($PaisesItem["Nombre"]))?></option>
                            <?php }	?>
                        </select></td>
                        <td><input type="checkbox" name="chkigualdomN" id="chkigualdomN" onClick="copiachkigualdomN()"></td>
                        <td>Igual que el Domicilio</td>
                    
                    </tr>
                </tbody>
				</table>
            </fieldset>
       </div>  <!--fin--> 
    </div>
    	
    </div>  <!--fin 1. Registro del Postulante-->  
    
    <div title="2. Registro del Receptor (Paciente)" style="padding:10px" >
   		<div id="DivReceptor">
		<table width="988" border="0">
            <tr>
                <td width="852">   
					<fieldset>
					<legend>Datos de Busqueda</legend>
						<table width="674" border="0" cellspacing="4">				   
							<tr>
								<td width="120">Nro Documento</td>
								<td width="139"><input class="easyui-numberbox" style="width:110px" id="NroDocumentoBusRec" name="NroDocumentoBusRec" data-options="prompt:'Nº Documento',
                                            formatter:function(v){
                                                var s = new String(v||'');                                                                                              
                                                return s;
                                            },
                                            parser:function(s){
                                                return s;
                                            }
                                            " maxlength="8"></td>
								<td width="127">Apellidos y Nombres</td>
								
								<td width="260"><input class="easyui-textbox" style="width:200px" id="ApellidosNombresBusRec" name="ApellidosNombresBusRec"  data-options="prompt:'Apellidos y Nombres'"></td>
							</tr>
					   
							<tr>
								<td>Nro Historia:</td>
								<td><input class="easyui-numberbox" style="width:110px" id="NroHistoriaClinicaBusRec" name="NroHistoriaClinicaBusRec"  data-options="prompt:'Nº Historia'" maxlength="8"></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>					
						</table>
					</fieldset>
                </td>
            </tr>
			
			<tr>
				<td>
					<fieldset>
					<legend>Datos del Paciente</legend>
					<table width="952">
					  <tr>
					    <td width="607"><table width="600">
					      <tr>
					        <td>Nro Historia</td>
					        <td><input class="easyui-numberbox" style="width:105px" id="NroHistoria" name="NroHistoria" data-options="prompt:'Nro Historia',required:true" maxlength="8" readonly /> <input name="IdPaciente" id="IdPaciente" type="hidden" /></td>
					        <td>Nro Dcto</td>
					        <td><input class="easyui-numberbox" style="width:105px" id="NroDocumentoRec" name="NroDocumentoRec" data-options="prompt:'Nro Documento'" maxlength="8" readonly /></td>
				          </tr>
					      <tr>
					        <td width="120">Ap.Paterno </td>
					        <td width="160"><!--<button class="btn">.</button>-->
				            <input  class="easyui-textbox" style="width:105px" id="ApellidoPaternoRec" name="ApellidoPaternoRec" data-options="prompt:'Ap.Paterno',required:true,validType:'justText'" readonly /></td>
					        <td width="130">Ap.Materno</td>
					        <td width="170"><input name="ApellidoMaternoRec" class="easyui-textbox" id="ApellidoMaternoRec" style="width:105px" data-options="prompt:'Ap.Materno',required:true,validType:'justText'" readonly /></td>
				          </tr>
					      <tr>
					        <td>1.Nombre</td>
					        <td><input style="width:105px" class="easyui-textbox" id="PrimerNombreRec" name="PrimerNombreRec" data-options="prompt:'1er Nombre',required:true,validType:'justText'" readonly /></td>
					        <td>2.Nombre</td>
					        <td><input style="width:105px" class="easyui-textbox" id="SegundoNombreRec" name="SegundoNombreRec" data-options="prompt:'2do Nombre',validType:'justText'" readonly /></td>
				          </tr>
					      <tr>
					        <td>3.Nombre </td>
					        <td><input style="width:105px" class="easyui-textbox" id="TercerNombreRec" name="TercerNombreRec" data-options="prompt:'3er Nombre',validType:'justText'" readonly /></td>
					        <td>Fecha Nacim.</td>
					        <td><input style="width:105px" class="easyui-datebox" name="FechaNacimientoRec" id="FechaNacimientoRec" validType="validDate" readonly /></td>
				          </tr>
					      <tr>
					        <td>Sexo </td>
					        <td><select class="easyui-combobox" name="IdTipoSexoRec" id="IdTipoSexoRec"  style="width: 105px" data-options="prompt:'Seleccione',required:true" readonly >
					          <option value="0"></option>
					          <?php
                                         		 $listarSexo=SIGESA_Emergencia_ListarTiposSexo_M();
                                           		 if($listarSexo != NULL) { 
                                             		foreach($listarSexo as $item){?>
					          <option value="<?php echo $item["IdTipoSexo"]?>" ><?php echo $item["IdTipoSexo"]."=".$item["Descripcion"]?></option>
					          <?php } } ?>
				            </select></td>
					        <td>Grupo Sanguineo</td>
					        <td><select class="easyui-combobox" name="IdGrupoSanguineoRec" id="IdGrupoSanguineoRec"  style="width: 105px" data-options="prompt:'Seleccione',required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarGrupoSanguineoRec(); }">
					          <option value="0"></option>
					          <?php
                                                  $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
                                                   if($listarSexo != NULL) { 
                                                     foreach($listarSexo as $item){?>
					          <option value="<?php echo $item["IdGrupoSanguineo"]?>" ><?php echo $item["Descripcion"]?></option>
					          <?php } } ?>
				            </select>
                            <input name="GrupoSanguineoRec" id="GrupoSanguineoRec" type="hidden" />
				            <input name="chkgs" id="chkgs" type="checkbox" value="1" onClick="ObtenergsPaciente(document.getElementById('IdPaciente').value)" />
                            </td>
				          </tr>
					      </table>
					    </td>
					    <td width="333">
                        
                        <table width="333"> <!--style="position:relative;top: -35px;"-->
					      <tr>
					        <td>Hospital Origen</td>
					        <td><select class="easyui-combobox" name="IdHospital" id="IdHospital"  style="width: 150px" data-options="prompt:'Seleccione',required:true">
					            <option value="0"></option>
					            <?php
                                              $listarSexo=SIGESA_BSD_ListarHospital_M();
                                               if($listarSexo != NULL) { 
                                                 foreach($listarSexo as $item){?>
					            <option value="<?php echo $item["IdHospital"]?>" ><?php echo $item["IdHospital"].'='.$item["Descripcion"]?></option>
					            <?php } } ?>
			                </select></td>
				          </tr>
					      <tr>
					        <td width="130">Servicio</td>
					        <td width="191"><select class="easyui-combobox" name="IdServicioHospital" id="IdServicioHospital"  style="width:150px" >
					          <option value="0">No se sabe</option>
					          <?php
                                          $listarEC=ListarServiciosHospitalM();
                                           if($listarEC != NULL) { 
                                             foreach($listarEC as $item){?>
					          <option value="<?php echo $item["IdServicio"]?>" ><?php echo trim(ucfirst(mb_strtolower($item["Nombre"]))).' | '.trim(mb_strtoupper($item["DescripcionTipoServicio"]));?></option>
					          <?php } } ?>
				            </select></td>
				          </tr>
					      <!--<tr>
					        <td>Nro Cama</td>
					        <td><input style="width:150px" class="easyui-numberbox" id="NroCama" name="NroCama" data-options="prompt:'Nro Cama'"></td>
				          </tr>
					      <tr>
					        <td>Medico</td>
					        <td><input style="width:150px" class="easyui-textbox" id="Medico" name="Medico" data-options="prompt:'Medico',validType:'justText'"></td>
				          </tr>
					      <tr>
					        <td>Diagnóstico</td>
					        <td><input style="width:150px" class="easyui-textbox" id="CodigoCIE10Diagnostico" name="CodigoCIE10Diagnostico" data-options="prompt:'Diagnostico'"></td>
				          </tr>
					      <tr>
					        <td>Alta</td>
					        <td><input type="checkbox" name="EstadoAlta" id="EstadoAlta" value="1"  /></td>
				          </tr>-->
				        </table>
                        <table id="tt" style="margin:0px !important"></table>
                        
                        </td>
				      </tr>
					  </table>   
					</fieldset>		
		
				</td>
			</tr>    
		</table> 
        
        <!--<div class="easyui-tabs"  >
               <div title="1.1)Postulantes y Donantes" style="padding:10px;">       
                     <table  width="950" border="1" cellspacing="0" cellpadding="0">
                       <thead>
                         <tr>
                           <th width="110">Fecha</th>
                           <th width="168">Nombre</th>
                           <th width="93">Entrevista</th>
                           <th width="152">Tamizaje</th>
                           <th width="132">Lote</th>
                           <th width="281">CodBar</th>
                         </tr>
                       </thead>
                       <tbody>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                         </tr>
                       </tbody>
                     </table>       
               </div>
               <div title="1.2)Hemocomponentes Transfundidos" style="padding:10px;">           
                     <table  width="950" border="1" cellspacing="0" cellpadding="0">                      
                        <thead>
                            <tr>
                                <th width="110">Fecha</th>
                                <th width="165">Hemocomponente</th>
                                <th width="97">Lote</th>
                                <th width="153">Grupo Sanguineo</th>
                                <th width="131">Solicitud</th>
                                <th width="280">Nro Bolsa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>                    
                            </tr>
                        </tbody>
                    </table>          
               </div>   
			</div>--> <!--fin easyui-tabs-->
         
    	</div> <!--fin DivReceptor --> 
        <div id="MensajeDesabilitarReceptor"> 
        	Los Postulantes de Tipo REPOSICIÓN, AUTÓLOGO y DIRIGIDO tienen RECEPTOR
        </div> 
    </div> <!--fin 2. Registro del Receptor (Paciente) --> 
   		 <!--<table></table>-->
   </div>     
</div>
       
</form>
     
	<script type="text/javascript">	
	
	$.extend($('#ApellidoPaterno').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor no digite numeros'  
        }
    });
	
	$.extend($('#ApellidoMaterno').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor no digite numeros'  
        }
    });
	
	$.extend($('#PrimerNombre').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor no digite numeros'  
        }
    });
	
	$.extend($('#SegundoNombre').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor no digite numeros'  
        }
    });
	
	$.extend($('#TercerNombre').searchbox.defaults.rules, {
        justText: {
            validator: function(value, param){
                 return !value.match(/[0-9]/);
            },
            message: 'Porfavor no digite numeros'  
        }
    });
		
	
		$(function(){
		  
		  $('#NroDocumento').numberbox('textbox').attr('maxlength', $('#NroDocumento').attr("maxlength"));
		  $('#NroDocumentoBus').numberbox('textbox').attr('maxlength', $('#NroDocumentoBus').attr("maxlength"));
		  $('#NroDocumentoBusRec').numberbox('textbox').attr('maxlength', $('#NroDocumentoBusRec').attr("maxlength"));
		  	   
		});		
		
		function validarDocumentoBus(){
			IdDocIdentidadBus=$('#IdDocIdentidadBus').combobox('getValue');
			if(IdDocIdentidadBus=='1'){
				$('#NroDocumentoBus').numberbox('setValue', '');
				$('#NroDocumentoBus').numberbox('textbox').attr('maxlength', $('#NroDocumentoBus').attr("maxlength"));
			}else{
				//$('#NroDocumentoBus').numberbox('setValue', '');	
				$('#NroDocumentoBus').numberbox('textbox').attr('maxlength', '');
			}
		}
		
		function validarDocumento(){
			IdDocIdentidad=$('#IdDocIdentidad').combobox('getValue');
			if(IdDocIdentidad=='1'){
				$('#NroDocumento').numberbox('setValue', '');
				$('#NroDocumento').numberbox('textbox').attr('maxlength', $('#NroDocumento').attr("maxlength"));
			}else{
				//$('#NroDocumento').numberbox('setValue', '');	
				$('#NroDocumento').numberbox('textbox').attr('maxlength', '');
				document.getElementById('errorPostulante').value='';
			}
		}
		
	</script>   
       
       
    <link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
    
   
</body>
</html>