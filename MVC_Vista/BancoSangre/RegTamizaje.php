<?php
		$NroDonacion=$_REQUEST["NroDonacion"];				
		$data=Listar_TamizajePendientesM($NroDonacion);			    
							
		$IdDocIdentidad=$data[0]["IdDocIdentidad"];		
		$NombresPostulante=$data[0]["ApellidoPaterno"].' '.$data[0]["ApellidoMaterno"].' '.$data[0]["PrimerNombre"].' '.$data[0]["SegundoNombre"];					
		$TipoDonacion=$data[0]["TipoDonacion"];
		$IdGrupoSanguineo=$data[0]["IdGrupoSanguineo"];
		$GrupoSanguineoPostulante=$data[0]["GrupoSanguineoPostulante"];
		$FechaDonacion=vfecha(substr($data[0]['FechaDonacion'],0,10));
		$HoraDonacion=substr($data[0]['FechaDonacion'],11,8);
		$IdExtraccion=($data[0]['IdExtraccion']!="") ? $data[0]['IdExtraccion'] : 0;
		$IdMovimiento=($data[0]['IdMovimiento']!="") ? $data[0]['IdMovimiento'] : 0;	
		
		$ObtenerMaximoSNCS=ObtenerMaximoSNCS_M();
		$XValor1=$ObtenerMaximoSNCS[0]['Valor1'];
		$LengValor1=$ObtenerMaximoSNCS[0]['LengValor1'];	
			
		$Valor1=str_pad( $XValor1+1, $LengValor1, "0", STR_PAD_LEFT);	
		
?>

<html>
<head>
        <meta charset="UTF-8">      
        <title>Registrar Datos Tamizaje</title> 
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">        
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript"> 
		
		$.extend($("#FechaTamizaje").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaTamizaje").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FechaNacimiento" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});
		
			function cancelar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Tamizaje&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
		
			function guardar(){				
					
				var FechaTamizaje=$('#FechaTamizaje').datebox('getText');		
				if(FechaTamizaje.trim()==""){ 
					$.messager.alert('Mensaje','Falta Ingresar la Fecha Tamizaje','warning');					
					$('#FechaTamizaje').next().find('input').focus();
					return 0;			
				}
				var array_FechaTamizaje = FechaTamizaje.split("/");	
					//si el array no tiene tres partes, la fecha es incorrecta
					if (array_FechaTamizaje.length!=3){	
					   $.messager.alert({
							title: 'Mensaje',
							msg: 'Fecha Tamizaje Incorrecta',
							icon:'warning',
							fn: function(){
								$('#FechaTamizaje').next().find('input').focus();
							}
						});
						$('#FechaTamizaje').next().find('input').focus();
						return 0;
					}
									
				var HoraTamizaje=$('#HoraTamizaje').timespinner('getValue');		
				if(HoraTamizaje.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable Final','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar la Hora Tamizaje',
						icon:'warning',
						fn: function(){
							$('#HoraTamizaje').next().find('input').focus();
						}
					});
					$('#HoraTamizaje').next().find('input').focus();
					return 0;			
				}
				
				var IdResponsable=$('#IdResponsable').combobox('getValue');		
				if(IdResponsable.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable del Tamizaje','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar el Responsable del Tamizaje',
						icon:'warning',
						fn: function(){
							$('#IdResponsable').next().find('input').focus();
						}
					});
					$('#IdResponsable').next().find('input').focus();
					return 0;			
				}				
					
				if($('#IdResponsable').combobox('isValid')==false){ 
					//$.messager.alert('Mensaje','El Responsable Ingresado no Existe','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'El Responsable Ingresado no Existe',
						icon:'warning',
						fn: function(){
							$('#IdResponsable').next().find('input').focus();
						}
					});
					$('#IdResponsable').next().find('input').focus();
					return 0;			
				}
				
				//QUIMIOLUMINICENCIA
				var PRquimioVIH=$('#PRquimioVIH').numberbox('getValue');	
				if(document.getElementById('Reactivo1').checked==true && PRquimioVIH.trim()==""){
					//$('#PRquimioVIH').numberbox('show');
					//$.messager.alert('Mensaje','Falta Ingresar Valor medición VIH','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Valor medición VIH',
						icon:'warning',
						fn: function(){
							$('#PRquimioVIH').next().find('input').focus();
						}
					});
					$('#PRquimioVIH').next().find('input').focus();
					return 0;
				}
				
				var PRquimioHTLV=$('#PRquimioHTLV').numberbox('getValue');
				if(document.getElementById('Reactivo2').checked==true && PRquimioHTLV.trim()==""){
					//$('#PRquimioHTLV').numberbox('show');
					//$.messager.alert('Mensaje','Falta Ingresar Valor medición HTLV','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Valor medición HTLV',
						icon:'warning',
						fn: function(){
							$('#PRquimioHTLV').next().find('input').focus();
						}
					});
					$('#PRquimioHTLV').next().find('input').focus();
					return 0;
				}
				
				var PRquimioHBS=$('#PRquimioHBS').numberbox('getValue');
				if(document.getElementById('Reactivo3').checked==true && PRquimioHBS.trim()==""){
					//$('#PRquimioHBS').numberbox('show');
					//$.messager.alert('Mensaje','Falta Ingresar Valor medición HBS','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Valor medición HBS',
						icon:'warning',
						fn: function(){
							$('#PRquimioHBS').next().find('input').focus();
						}
					});
					$('#PRquimioHBS').next().find('input').focus();
					return 0;
				}
				
				var PRquimioVHB=$('#PRquimioVHB').numberbox('getValue');
				if(document.getElementById('Reactivo4').checked==true && PRquimioVHB.trim()==""){
					//$('#PRquimioVHB').numberbox('show');
					//$.messager.alert('Mensaje','Falta Ingresar Valor medición VHB','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Valor medición VHB',
						icon:'warning',
						fn: function(){
							$('#PRquimioVHB').next().find('input').focus();
						}
					});
					$('#PRquimioVHB').next().find('input').focus();
					return 0;
				}
				
				var PRquimioVHC=$('#PRquimioVHC').numberbox('getValue');
				if(document.getElementById('Reactivo5').checked==true && PRquimioVHC.trim()==""){
					//$('#PRquimioVHC').numberbox('show');
					//$.messager.alert('Mensaje','Falta Ingresar Valor medición VHC','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Valor medición VHC',
						icon:'warning',
						fn: function(){
							$('#PRquimioVHC').next().find('input').focus();
						}
					});
					$('#PRquimioVHC').next().find('input').focus();
					return 0;
				}
				
				var PRquimioSifilis=$('#PRquimioSifilis').numberbox('getValue');
				if(document.getElementById('Reactivo6').checked==true && PRquimioSifilis.trim()==""){
					//$('#PRquimioSifilis').numberbox('show');
					//$.messager.alert('Mensaje','Falta Ingresar Valor medición Sifilis','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Valor medición Sifilis',
						icon:'warning',
						fn: function(){
							$('#PRquimioSifilis').next().find('input').focus();
						}
					});
					$('#PRquimioSifilis').next().find('input').focus();
					return 0;
				}
				
				var PRquimioANTICHAGAS=$('#PRquimioANTICHAGAS').numberbox('getValue');
				if(document.getElementById('Reactivo7').checked==true && PRquimioANTICHAGAS.trim()==""){
					//$('#PRquimioANTICHAGAS').numberbox('show');
					//$.messager.alert('Mensaje','Falta Ingresar Valor medición CHAGAS','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Valor medición CHAGAS',
						icon:'warning',
						fn: function(){
							$('#PRquimioANTICHAGAS').next().find('input').focus();
						}
					});
					$('#PRquimioANTICHAGAS').next().find('input').focus();
					return 0;
				}
				
				
				//NAT
				var RealizaNAT=$('#RealizaNAT').combobox('getValue');		
				if(RealizaNAT.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar si Realizará NAT','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar si Realizará NAT',
						icon:'warning',
						fn: function(){
							$('#RealizaNAT').next().find('input').focus();
						}
					});
					$('#RealizaNAT').next().find('input').focus();
					return 0;			
				}
				
				var IdMotivoNoNAT=$('#IdMotivoNoNAT').combobox('getValue');	
				if(RealizaNAT.trim()=="0" && IdMotivoNoNAT.trim()=="0" || ($('#IdMotivoNoNAT').combobox('isValid')==false)){ //NO
					//$.messager.alert('Mensaje','Falta Seleccionar Motivo que no realizará NAT','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar Motivo que no realizará NAT',
						icon:'warning',
						fn: function(){
							$('#IdMotivoNoNAT').next().find('input').focus();
						}
					});
					$('#IdMotivoNoNAT').next().find('input').focus();
					return 0;			
				}	
				
				var MotivoNoNAT=$('#MotivoNoNAT').textbox('getValue');	
				if(IdMotivoNoNAT.trim()=="4" && MotivoNoNAT.trim()==""){ //NO y 4 = OTRO, ESPECIFICAR:
					//$.messager.alert('Mensaje','Falta Ingresar Motivo que no realizará NAT','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Motivo que no realizará NAT',
						icon:'warning',
						fn: function(){
							$('#MotivoNoNAT').next().find('input').focus();
						}
					});
					$('#MotivoNoNAT').next().find('input').focus();
					return 0;			
				}
				
				var SNCS=$('#SNCS').numberbox('getValue');				
				if( (document.getElementById('NoSNCS').checked==false && SNCS.trim()=="") && (document.getElementById('Reactivo1').checked==false) && (document.getElementById('Reactivo2').checked==false) && (document.getElementById('Reactivo3').checked==false) && (document.getElementById('Reactivo4').checked==false) && (document.getElementById('Reactivo5').checked==false) && (document.getElementById('Reactivo6').checked==false) && (document.getElementById('Reactivo7').checked==false)){	//SOLO APTOS
					//$.messager.alert('Mensaje','Falta Ingresar Nº de Sello Nacional de Calidad','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Nº de Sello Nacional de Calidad',
						icon:'warning',
						fn: function(){
							$('#SNCS').next().find('input').focus();
						}
					});
					$('#SNCS').next().find('input').focus();
					return 0;	
				}
				
				var MotivoElimMuestra=document.getElementById('MotivoElimMuestra').value;
				var MotivoElimMuestraX=$('#MotivoElimMuestraX').combobox('getValue');
				if( (document.getElementById('NoSNCS').checked==true && (MotivoElimMuestraX.trim()=="" || MotivoElimMuestra.trim()=="")) || $('#IdMotivoNoNAT').combobox('isValid')==false ){ //CHECKED	
					//$.messager.alert('Mensaje','Falta Ingresar Motivo falta resultado (F)','info');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar Motivo falta resultado (F)',
						icon:'warning',
						fn: function(){
							$('#MotivoElimMuestraX').next().find('input').focus();
						}
					});
					$('#MotivoElimMuestraX').next().find('input').focus();
					return 0;	
				}
																
				//document.form1.submit();
				$.messager.confirm('Mensaje', '¿Seguro de Guardar los Datos de Tamizaje de Sangre del Donante?', function(r){
					if (r){
						$('#form1').submit();	
					}
				});	
			}  
			
			
			$(function(){	
				
				$.extend($.fn.textbox.methods, {
					show: function(jq){
						return jq.each(function(){
							$(this).next().show();
						})
					},
					hide: function(jq){
						return jq.each(function(){
							$(this).next().hide();
						})
					}
				})				
				$('#MotivoNoNAT').textbox('hide');
				$('#PRquimioVIH').numberbox('hide');
				$('#PRquimioHTLV').numberbox('hide');
				$('#PRquimioHBS').numberbox('hide');
				$('#PRquimioVHB').numberbox('hide');
				$('#PRquimioVHC').numberbox('hide');
				$('#PRquimioSifilis').numberbox('hide');
				$('#PRquimioANTICHAGAS').numberbox('hide');
									
			});
			
			function cambiarNAT(){
				var RealizaNAT= $('#RealizaNAT').combobox('getValue'); 
				if(RealizaNAT=='0'){ //NO					
					//document.getElementById('Reactivo11').checked=false;
					document.getElementById('Reactivo11').style.visibility = 'hidden';
					//document.getElementById('NoReactivo11').checked=false;
					document.getElementById('NoReactivo11').style.visibility = 'hidden';
					//document.getElementById('Reactivo22').value=false;
					document.getElementById('Reactivo22').style.visibility = 'hidden';
					//document.getElementById('NoReactivo22').checked=false;
					document.getElementById('NoReactivo22').style.visibility = 'hidden';
					//document.getElementById('Reactivo33').value=false;
					document.getElementById('Reactivo33').style.visibility = 'hidden';
					//document.getElementById('NoReactivo33').checked=false;
					document.getElementById('NoReactivo33').style.visibility = 'hidden';
					$("#IdMotivoNoNAT").combobox('readonly',false);	
					$('#IdMotivoNoNAT').next().find('input').focus();	
				}else{ //SI		
					document.getElementById('Reactivo11').style.visibility = 'visible';					
					document.getElementById('NoReactivo11').style.visibility = 'visible';					
					document.getElementById('Reactivo22').style.visibility = 'visible';					
					document.getElementById('NoReactivo22').style.visibility = 'visible';					
					document.getElementById('Reactivo33').style.visibility = 'visible';					
					document.getElementById('NoReactivo33').style.visibility = 'visible';								
					document.getElementById('NoReactivo11').checked=true;					
					document.getElementById('NoReactivo22').checked=true;					
					document.getElementById('NoReactivo33').checked=true;
					$("#IdMotivoNoNAT").combobox('readonly',true);	
					$("#IdMotivoNoNAT").combobox('setValue','0');
					$('#MotivoNoNAT').textbox('hide');
				}
			}
			
			function cambiarMotivoNoNAT(){
				var IdMotivoNoNAT= $('#IdMotivoNoNAT').combobox('getValue');	
				if(IdMotivoNoNAT=='4'){ //4 = OTRO, ESPECIFICAR:
					$('#MotivoNoNAT').textbox('show'); 	
					$('#MotivoNoNAT').next().find('input').focus();	
				}else{
					$('#MotivoNoNAT').textbox('hide'); 	
				}
			}
			
			function MostrarMedicionReactivo(){
				if(document.getElementById('Reactivo1').checked==true){
					$('#PRquimioVIH').numberbox('show');					
				}else{
					$("#PRquimioVIH").numberbox('setValue','');
					$('#PRquimioVIH').numberbox('hide');									
				}
				
				if(document.getElementById('Reactivo2').checked==true){
					$('#PRquimioHTLV').numberbox('show');
				}else{
					$("#PRquimioHTLV").numberbox('setValue','');
					$('#PRquimioHTLV').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo3').checked==true){
					$('#PRquimioHBS').numberbox('show');
				}else{
					$("#PRquimioHBS").numberbox('setValue','');
					$('#PRquimioHBS').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo4').checked==true){
					$('#PRquimioVHB').numberbox('show');
				}else{
					$("#PRquimioVHB").numberbox('setValue','');
					$('#PRquimioVHB').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo5').checked==true){
					$('#PRquimioVHC').numberbox('show');
				}else{
					$("#PRquimioVHC").numberbox('setValue','');
					$('#PRquimioVHC').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo6').checked==true){
					$('#PRquimioSifilis').numberbox('show');
				}else{
					$("#PRquimioSifilis").numberbox('setValue','');
					$('#PRquimioSifilis').numberbox('hide');
				}
				
				if(document.getElementById('Reactivo7').checked==true){
					$('#PRquimioANTICHAGAS').numberbox('show');
				}else{
					$("#PRquimioANTICHAGAS").numberbox('setValue','');
					$('#PRquimioANTICHAGAS').numberbox('hide');
				}
				
				//TODOS				
				if( (document.getElementById('Reactivo1').checked==true) || (document.getElementById('Reactivo2').checked==true) || (document.getElementById('Reactivo3').checked==true) || (document.getElementById('Reactivo4').checked==true) || (document.getElementById('Reactivo5').checked==true) || (document.getElementById('Reactivo6').checked==true) || (document.getElementById('Reactivo7').checked==true)){			
					$("#RealizaNAT").combobox('setValue','0');
					$("#IdMotivoNoNAT").combobox('setValue','5');
					
					$("#SNCS").numberbox('setValue','');
					$("#SNCS").numberbox('readonly',true);
					document.getElementById('NoSNCS').disabled=true;
					
				}else{						
					$("#RealizaNAT").combobox('setValue','1');		
					
					$("#SNCS").numberbox('readonly',false);	
					document.getElementById('NoSNCS').disabled=false;		
				}						
				cambiarNAT();				
			}
			
			function cambiarMotivoElimMuestra(){
				var MotivoElimMuestra=$('#MotivoElimMuestraX').combobox('getValue');	
				document.getElementById('MotivoElimMuestra').value=MotivoElimMuestra	
			}
			
			$(function(){		  
		  		$('#SNCS').numberbox('textbox').attr('maxlength', $('#SNCS').attr("maxlength"));		  	   
			});	
			
			function cambiarNoSNCS(){				
				if(document.getElementById('NoSNCS').checked==true){ //CHECKED	
					//QUIMIOLUMINICENCIA
					document.getElementById('Reactivo1').style.visibility = 'hidden';					
					document.getElementById('NoReactivo1').style.visibility = 'hidden';					
					document.getElementById('Reactivo2').style.visibility = 'hidden';					
					document.getElementById('NoReactivo2').style.visibility = 'hidden';					
					document.getElementById('Reactivo3').style.visibility = 'hidden';					
					document.getElementById('NoReactivo3').style.visibility = 'hidden';						
					document.getElementById('Reactivo4').style.visibility = 'hidden';					
					document.getElementById('NoReactivo4').style.visibility = 'hidden';					
					document.getElementById('Reactivo5').style.visibility = 'hidden';					
					document.getElementById('NoReactivo5').style.visibility = 'hidden';					
					document.getElementById('Reactivo6').style.visibility = 'hidden';					
					document.getElementById('NoReactivo6').style.visibility = 'hidden';						
					document.getElementById('Reactivo7').style.visibility = 'hidden';					
					document.getElementById('NoReactivo7').style.visibility = 'hidden';	
									
					//NAT
					document.getElementById('Reactivo11').style.visibility = 'hidden';					
					document.getElementById('NoReactivo11').style.visibility = 'hidden';					
					document.getElementById('Reactivo22').style.visibility = 'hidden';					
					document.getElementById('NoReactivo22').style.visibility = 'hidden';					
					document.getElementById('Reactivo33').style.visibility = 'hidden';					
					document.getElementById('NoReactivo33').style.visibility = 'hidden';					
					$("#IdMotivoNoNAT").combobox('readonly',false);	
					$("#IdMotivoNoNAT").combobox('setValue','6');						
					$("#RealizaNAT").combobox('setValue','0');						
					
					$("#MotivoElimMuestraX").combobox('readonly',false);	
					$('#MotivoElimMuestraX').next().find('input').focus();		
					$("#SNCS").numberbox('setValue','');
					$("#SNCS").numberbox('readonly',true);		
					
				}else{ //NO CHECKED	
					//QUIMIOLUMINICENCIA
					document.getElementById('Reactivo1').style.visibility = 'visible';					
					document.getElementById('NoReactivo1').style.visibility = 'visible';					
					document.getElementById('Reactivo2').style.visibility = 'visible';					
					document.getElementById('NoReactivo2').style.visibility = 'visible';					
					document.getElementById('Reactivo3').style.visibility = 'visible';					
					document.getElementById('NoReactivo3').style.visibility = 'visible';						
					document.getElementById('Reactivo4').style.visibility = 'visible';					
					document.getElementById('NoReactivo4').style.visibility = 'visible';					
					document.getElementById('Reactivo5').style.visibility = 'visible';					
					document.getElementById('NoReactivo5').style.visibility = 'visible';					
					document.getElementById('Reactivo6').style.visibility = 'visible';					
					document.getElementById('NoReactivo6').style.visibility = 'visible';						
					document.getElementById('Reactivo7').style.visibility = 'visible';					
					document.getElementById('NoReactivo7').style.visibility = 'visible';					
					//NAT	
					document.getElementById('Reactivo11').style.visibility = 'visible';					
					document.getElementById('NoReactivo11').style.visibility = 'visible';					
					document.getElementById('Reactivo22').style.visibility = 'visible';					
					document.getElementById('NoReactivo22').style.visibility = 'visible';					
					document.getElementById('Reactivo33').style.visibility = 'visible';					
					document.getElementById('NoReactivo33').style.visibility = 'visible';								
					document.getElementById('NoReactivo11').checked=true;					
					document.getElementById('NoReactivo22').checked=true;					
					document.getElementById('NoReactivo33').checked=true;
					$("#IdMotivoNoNAT").combobox('readonly',true);	
					$("#IdMotivoNoNAT").combobox('setValue','0');
					$('#MotivoNoNAT').textbox('hide');					
					$("#RealizaNAT").combobox('setValue','1');					
					
					$("#SNCS").numberbox('readonly',false);
					$("#MotivoElimMuestraX").combobox('readonly',true);	
					$("#MotivoElimMuestraX").combobox('setValue','');
					document.getElementById('MotivoElimMuestra').value='';
					
					if( (document.getElementById('Reactivo1').checked==false) && (document.getElementById('Reactivo2').checked==false) && (document.getElementById('Reactivo3').checked==false) && (document.getElementById('Reactivo4').checked==false) && (document.getElementById('Reactivo5').checked==false) && (document.getElementById('Reactivo6').checked==false) && (document.getElementById('Reactivo7').checked==false) ){
						$("#SNCS").numberbox('readonly',false);
						$('#SNCS').next().find('input').focus();	
					}				
					
				}
			}			
			
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
						
			$(function () {
				/*$('#IdResponsable').combobox({
					panelHeight: 'auto',
					selectOnNavigation: false,
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdResponsable"]',
					onLoadSuccess: function () { },
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) == 0;
					}
				});*/
				
				$('#RealizaNAT').combobox({	
					editable: false,
					required: true								
				});	
				$('#RealizaNAT').combobox('validate');				
				
				$('#IdResponsable').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdResponsable"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0;  
					}							
				});						
				$('#IdResponsable').combobox('validate');
				
				$('#IdMotivoNoNAT').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdMotivoNoNAT"]'								
				});						
				$('#IdMotivoNoNAT').combobox('validate');
				
				$('#MotivoElimMuestraX').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MotivoElimMuestraX"]'								
				});						
				$('#MotivoElimMuestraX').combobox('validate');				
				
			});
			
			function InicioNAT(){							
					$("#RealizaNAT").combobox('setValue','0');						
					document.getElementById('Reactivo11').style.visibility = 'hidden';					
					document.getElementById('NoReactivo11').style.visibility = 'hidden';					
					document.getElementById('Reactivo22').style.visibility = 'hidden';					
					document.getElementById('NoReactivo22').style.visibility = 'hidden';					
					document.getElementById('Reactivo33').style.visibility = 'hidden';				
					document.getElementById('NoReactivo33').style.visibility = 'hidden';							
					$("#IdMotivoNoNAT").combobox('setValue','1');								
			}
			
		</script>
</head>
<body onLoad="InicioNAT()">        
    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarTamizaje&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:90%;;height:auto;"title="Banco de Sangre: Registrar Tamizaje de Sangre" iconCls="icon-save" align="center"> 	
    <div class="easyui-panel" style="padding:0px;">     
        <!--<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Refrescar</a>-->             		
     	<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="guardar()">Guardar</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="cancelar();">Cancelar(ESC)</a>                      
	</div>
        
        <table>
            <tr align="center">
              <td>Nombres Donante</td>
                <td>
                    Nro. Donación
                </td>
                 <td>
                   Tipo Donación
               </td>
                 <td>
                   Grupo / Factor
                </td>
                 <td>
                    Fecha / Hora
                </td>
                 <td>Responsable</td>
                
            </tr>
        <tr align="center">
          <td><?php echo $NombresPostulante; ?></td>
            <td><?php echo $NroDonacion; ?>
            	<input type="hidden" name="NroDonacion" id="NroDonacion" value="<?php echo $NroDonacion; ?>" style="width:110px" />            	
                <input type="hidden" name="IdExtraccion" id="IdExtraccion" value="<?php echo $IdExtraccion; ?>" style="width:110px" />
                <input type="hidden" name="IdMovimiento" id="IdMovimiento" value="<?php echo $IdMovimiento; ?>" style="width:110px" />
            </td>
            <td><?php echo $TipoDonacion; ?></td>
            <td><?php echo $GrupoSanguineoPostulante; ?></td>
            <td>
               <input name="FechaTamizaje" id="FechaTamizaje" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />
               <input name="HoraTamizaje" id="HoraTamizaje" class="easyui-timespinner" value="<?php echo date('H:i:s'); ?>" data-options="showSeconds:true,required:true" style="width:100px"  />
            </td>
            <td><Select style="width:150px" class="easyui-combobox" id="IdResponsable" name="IdResponsable" data-options="prompt:'Seleccione',required:true">
              <option value=""></option>
              <?php
			  					  $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
								  $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
                                  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
              <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
              <?php } } ?>
            </select></td>
            
        </tr>
        </table>              
     	<br>
        <table>
            <tr>
              <td colspan="4"><b>EXAMEN DE QUIMIOLUMINICENCIA</b></td>
            </tr>
            <tr align="center">
                <td>
                    <fieldset>
                        <legend>1. VIH</legend>
                        <label for="Reactivo1">Reactivo</label>
                        <input type="radio" name="quimioVIH" id="Reactivo1" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo1">No Reactivo</label>
                        <input type="radio" name="quimioVIH" id="NoReactivo1" value="0" onClick="MostrarMedicionReactivo();" checked />
                  		<br><input class="easyui-numberbox" name="PRquimioVIH" id="PRquimioVIH" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/>
                  </fieldset>  
                </td>
                <td>
                    <fieldset>
                        <legend>2. HTLV I/II</legend>                       
                        <label for="Reactivo2">Reactivo</label>
                        <input type="radio" name="quimioHTLV" id="Reactivo2" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo2">No Reactivo</label>
                        <input type="radio" name="quimioHTLV" id="NoReactivo2" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioHTLV" id="PRquimioHTLV" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/>
                    </fieldset>   
                </td>
                <td>
            		<fieldset>
           			 	<legend>3. HBS Ag (Hepatitis B Ag)</legend>                        
						<label for="Reactivo3">Reactivo</label>
                        <input type="radio" name="quimioHBS" id="Reactivo3" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo3">No Reactivo</label>
                        <input type="radio" name="quimioHBS" id="NoReactivo3" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioHBS" id="PRquimioHBS" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/>           			   
            		</fieldset>  
                </td>
               <td>
                    <fieldset>
                    	<legend>4. CORE VHB(Hepatitis B core)</legend>                        
                        <label for="Reactivo4">Reactivo</label>
                        <input type="radio" name="quimioVHB" id="Reactivo4" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo4">No Reactivo</label>
                        <input type="radio" name="quimioVHB" id="NoReactivo4" value="0" onClick="MostrarMedicionReactivo();" checked />
                    	 <br><input class="easyui-numberbox" name="PRquimioVHB" id="PRquimioVHB" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
              </td>
          </tr>
          
          <tr align="center">
                <td>
                    <fieldset>
                    	<legend>5. VHC (Hepatitis C)</legend>                       
                        <label for="Reactivo5">Reactivo</label>
                        <input type="radio" name="quimioVHC" id="Reactivo5" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo5">No Reactivo</label>
                        <input type="radio" name="quimioVHC" id="NoReactivo5" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioVHC" id="PRquimioVHC" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
                </td>
               <td>
                    <fieldset>
                    	<legend>6. SIFILIS</legend>                    	
                        <label for="Reactivo6">Reactivo</label>
                        <input type="radio" name="quimioSifilis" id="Reactivo6" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo6">No Reactivo</label>
                        <input type="radio" name="quimioSifilis" id="NoReactivo6" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioSifilis" id="PRquimioSifilis" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
              </td>
                <td>
                    <fieldset>
                    	<legend>7. CHAGAS</legend>                       
                        <label for="Reactivo7">Reactivo</label>
                        <input type="radio" name="quimioANTICHAGAS" id="Reactivo7" value="1" onClick="MostrarMedicionReactivo();" />
                        <label for="NoReactivo7">No Reactivo</label>
                        <input type="radio" name="quimioANTICHAGAS" id="NoReactivo7" value="0" onClick="MostrarMedicionReactivo();" checked />
                        <br><input class="easyui-numberbox" name="PRquimioANTICHAGAS" id="PRquimioANTICHAGAS" data-options="prompt:'Valor medición',min:0,precision:2,required:true"/> 
                    </fieldset>  
                </td>
                <td> </td>
            </tr>
      </table>      
      <br>
      <table width="892" border="1" cellpadding="0" cellspacing="0">
            <tr align="center">
              <td colspan="4">
              <b>Realizará NAT</b>:&nbsp;&nbsp;
              <Select style="width:150px" class="easyui-combobox" id="RealizaNAT" name="RealizaNAT"  data-options="prompt:'Seleccione',required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarNAT(); }">
                <option value=""></option>
                <option value="1">1=SI</option>
                <option value="0">0=NO</option>
              </select>
              </td>
            </tr>
            <tr align="center">
              <td><b>MOTIVO</b></td>
              <td colspan="3"><b>EXAMEN DE BOLOGÍA MOLECULAR (NAT)</b></td>              
            </tr>
            <tr>
              <td align="center">
              <Select style="width:240px" class="easyui-combobox" id="IdMotivoNoNAT" name="IdMotivoNoNAT" data-options="prompt:'Seleccione',required:true,
              			valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarMotivoNoNAT(); }" readonly>
                <option value="0">No requiere</option>
                <?php
                                  $listar=SIGESA_BSD_MotivoNoNAT_M(); 
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
                <option value="<?php echo $item["IdMotivoNoNAT"]?>" ><?php echo mb_strtoupper($item["IdMotivoNoNAT"].'= '.$item["Descripcion"])?></option>
                <?php } } ?>
              </select> <br>
               <input class="easyui-textbox" name="MotivoNoNAT" id="MotivoNoNAT" data-options="prompt:'Motivo',required:true"/>               
                </td>
                <td>
                    <fieldset>
                        <legend>VIH</legend>
                        <label for="Reactivo11">Reactivo</label>
                        <input type="radio" name="natVIH" id="Reactivo11" value="1" />
                        <label for="NoReactivo11">No Reactivo</label>
                        <input type="radio" name="natVIH" id="NoReactivo11" value="0" checked />
                  
                    </fieldset>  
                </td>
                <td>
                    <fieldset><legend>HBV (Hepatitis B)</legend>
                        <label for="Reactivo22">Reactivo</label>
                        <input type="radio" name="natHBV" id="Reactivo22" value="1" />
                        <label for="NoReactivo22">No Reactivo</label>
                        <input type="radio" name="natHBV" id="NoReactivo22" value="0" checked />
                    </fieldset>  
                </td>
                <td>
            		<fieldset><legend>HCV (Hepatitis C)</legend>
            			<label for="Reactivo33">Reactivo</label>
                        <input type="radio" name="natHCV" id="Reactivo33" value="1" />
                        <label for="NoReactivo33">No Reactivo</label>
                        <input type="radio" name="natHCV" id="NoReactivo33" value="0" checked />
           			</fieldset>  
                </td>
          </tr>
           
      </table>
      <br>
      
      <table width="892" height="83" border="0" cellpadding="0" cellspacing="0">
        <tr align="center">
          <td colspan="2">&nbsp;</td>
        </tr>        
        <tr align="center">
          <td width="418" height="31"><b>Nº DE SELLO NACIONAL DE CALIDAD</b></td>
          <td>
          	<input type="checkbox" name="NoSNCS" id="NoSNCS" onClick="cambiarNoSNCS()" />
          	<label for="NoSNCS" >NO crear Nº SNCS y NO ingresar ningún resultado de tamizaje</label>
          </td>
        </tr>
        <tr align="center">
          <td align="center">
          	<input class="easyui-numberbox" name="SNCS" id="SNCS" data-options="prompt:'Nº SNCS',
                                            formatter:function(v){
                                                var s = new String(v||'');                                                                                              
                                                return s;
                                            },
                                            parser:function(s){
                                                return s;
                                            }
                                            " maxlength="7" value="<?php echo $Valor1; ?>" /></td>
          <td>
                Motivo falta resultado (F): 
                <select style="width:200px" class="easyui-combobox" id="MotivoElimMuestraX" name="MotivoElimMuestraX" data-options="prompt:'Seleccione',
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarMotivoElimMuestra(); }" readonly >
                  <option value="">Ninguno</option>
                  <?php
                                          $listar=ListarMotivoElimina('TAM'); 
                                           if($listar != NULL) { 
                                             foreach($listar as $item){?>
                  <option value="<?php echo $item["IdMotivoElimina"]?>" ><?php echo mb_strtoupper($item["Descripcion"])?></option>
                  <?php } } ?>
                </select>
                <input name="MotivoElimMuestra" id="MotivoElimMuestra" type="hidden" />
          </td>
        </tr>
        <tr align="center">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr align="center">
          <td colspan="2">Observación: <input style="width:300px;height:50px" class="easyui-textbox" multiline="true" name="Observacion" id="Observacion" /> </td>
        </tr>       
        
      </table>
      <p>&nbsp;</p>      
      <!--<a href="#" class="easyui-linkbutton">GUARDAR</a><a href="#" class="easyui-linkbutton">EDITAR</a>
        <a href="#" class="easyui-linkbutton">IMPRIMIR</a><a href="#" class="easyui-linkbutton">SALIR</a>
       <a href="#" class="easyui-linkbutton">Sincronizar Analizador Automatico Plab</a>-->
     </div>
</form>
</body></html>                   
