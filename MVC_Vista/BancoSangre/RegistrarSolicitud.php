<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Solicitud de Sangre</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
		
		//Registrar Solicitud de Sangre del Paciente
			$(function(){			
				$('#IdCuentaAtencionBusRec').combogrid({	
				editable: false							
				});	
			});
			
			function cambiarGrupoSanguineoRec(){
				GrupoSanguineoRec=$('#IdGrupoSanguineoRec').combobox('getText');	
				document.getElementById('GrupoSanguineoRec').value=GrupoSanguineoRec;
			}
			
			$(function(){						
					
				$('#NroHistoriaClinicaBusRec').numberbox('textbox').keypress(function(e){	
					var NroHistoriaClinica= $('#NroHistoriaClinicaBusRec').numberbox('getText');
					
					if (e.which == 13) {			
						//$.messager.alert('Mensaje','El DNI debe tener 8 Digitos','warning');					
						$.ajax({
						url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPacienteAtenciones",					
						type: "GET",
						dataType: "JSON",
						data: {						
							q: NroHistoriaClinica						
						},							
							success: function (data) {																																
																					
							}				
						}).done(function (data) {
							console.log(data);
							//alert(data);						
							if(data=="") { //if(!data.success){	
								LimpiarDatosPaciente(); ObtenergsPaciente(0);
								LimpiarDatosAtenciones();	
								$.messager.alert('SIGESA','El Paciente NO EXISTE o NO TIENE CUENTA ACTIVA' ,'info'); //NO HAY DATOS
								//$('#MensajeExiste').html('');			
							}else if(data.NroSolicitud!=""){
								LimpiarDatosPaciente(); ObtenergsPaciente(0);
								LimpiarDatosAtenciones();
								$.messager.alert('SIGESA','El Paciente TIENE Pendiente la Solicitud N° '+data.NroSolicitud,'info'); //NO HAY DATOS
							}else{
								$('#IdCuentaAtencionBusRec').next().find('input').focus();
								LlenarDatosPacientesHC(data);	
								VerDatosAtenciones(data.IdPaciente);
							}
						});	//fin done
						e.preventDefault();
						return false;
					}
				}); //FIN NroHistoriaClinicaBusRec								
				///////////////////////////////			   
								
			});	//fin function
			
	function LimpiarDatosPaciente(){
		$('#NroHistoriaClinica').textbox('setValue', '');	
		$('#ApellidosNombres').textbox('setValue', '');	
		//Limpiar Datos frm
		$('#GrupoSanguineo').textbox('setValue', '');		
		document.getElementById('IdPaciente').value='';			
		$('#NroDocumento').textbox('setValue', '');
		$('#IdTipoSexoRec').combobox('setValue', '');	
		$('#IdGrupoSanguineoRec').combobox('setValue', '');
		$('#FechaNacimiento').textbox('setValue', '');
		$('#Edad').textbox('setValue', '');
		document.getElementById('GrupoSanguineoRecANT').value='';	
		document.getElementById('GrupoSanguineoRec').value='';		
	}
	
	function LlenarDatosPacientesHC(r){	
		LimpiarDatosPaciente();		
		$('#NroHistoriaClinica').textbox('setValue', r.NroHistoriaClinica);	
		$('#ApellidosNombres').textbox('setValue', r.NombresPaciente);	
		//Obtener Datos frm
		$('#GrupoSanguineo').textbox('setValue', r.GrupoSanguineo);		
		document.getElementById('IdPaciente').value=r.IdPaciente;			
		$('#NroDocumento').textbox('setValue', r.NroDocumento);
		$('#IdTipoSexoRec').combobox('setValue', r.IdTipoSexo);	
		$('#IdGrupoSanguineoRec').combobox('setValue', r.IdGrupoSanguineo);
		$('#FechaNacimiento').textbox('setValue', r.FechaNacimiento);
		$('#Edad').textbox('setValue', r.Edad);
		document.getElementById('GrupoSanguineoRecANT').value=r.GrupoSanguineo;	
		document.getElementById('GrupoSanguineoRec').value=r.GrupoSanguineo;
		
		//GS
		ObtenergsPaciente(r.IdPaciente);
		
		//Limpiar Atenciones
		LimpiarDatosAtenciones();		
						
	}	
	
	function LimpiarDatosAtenciones(){
		document.getElementById('IdCuentaAtencion').value='';
		document.getElementById('IdAtencion').value='';
		$('#IdTipoServicio').combobox('setValue', '');
		$('#IdServicioHospital').combobox('setValue', '');	
		$('#CodigoCama').textbox('setValue', '');
		$('#IdCuentaAtencionBusRec').combogrid('setValue', '');
		//document.getElementById('IdDiagnostico').value='';
		//$('#CodigoCIE2004').combogrid('setValue', '');
		//$('#diagnostico').combogrid('setValue','');	
			
	}
	
	function VerDatosAtencionesEstanciaHospitalaria(){	
	var IdAtencion=$('#IdCuentaAtencionBusRec').combogrid('getValue');
		$.ajax({
			url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarDatosPacientesAtencionesEstancia',
			type: 'POST',
			dataType: 'json',
			data: {				
				IdAtencion:IdAtencion,
			},							
			success: function (data) {		
				//alert(data.numerocama);
				document.getElementById('IdCuentaAtencion').value=data.IdCuentaAtencion;
				document.getElementById('IdAtencion').value=data.IdAtencion;
				$('#IdTipoServicio').combobox('setValue', data.IdTipoServicio);
				$('#IdServicioHospital').combobox('setValue', data.IdServicioIngreso);	
				$('#CodigoCama').textbox('setValue', data.numerocama);
				/*var diagnostico=data.diagnostico;
				var IdDiagnostico=''; CodigoCIE2004=''; Descripciondiagnostico='';
				if(diagnostico!=null){
					var partesdiagnostico=diagnostico.split("|");
					IdDiagnostico=partesdiagnostico[0];
					CodigoCIE2004=partesdiagnostico[1];
					Descripciondiagnostico=partesdiagnostico[2];
				}				
				document.getElementById('IdDiagnostico').value=IdDiagnostico;
				$('#CodigoCIE2004').combogrid('setValue', CodigoCIE2004);
				$('#diagnostico').combogrid('setValue', Descripciondiagnostico);*/
								
			}
		}).done(function (data) {
				console.log(data);
				if(data=="") { //if(!data.success){						    
					//alert('NO HAY DATOS');			
					$.messager.alert('SIGESA','EL PACIENTE NO TIENE DATOS DE ATENCIÓN' ,'info');			
				}else{					
				
				}//END else
		});	
	
	}		
									
				
			function VerDatosAtenciones(IdPaciente){
				//var IdPaciente=document.getElementById('IdPaciente').value;
				$('#IdCuentaAtencionBusRec').combogrid({
					panelWidth:480,
					//value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarDatosPacientesAtenciones&IdPaciente='+IdPaciente,
					method: 'get',
					idField:'IdAtencion', //NombresPaciente
					textField:'NombreServicio',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					//var url = BuscarReceptoresApellidosNombres(); },	//esta funcion llama cuando seleccionas el paciente			
					var url = VerDatosAtencionesEstanciaHospitalaria(); },		
					columns:[[							
						{field:'IdCuentaAtencion',title:'NroCuenta',width:75},
						{field:'NombreServicio',title:'Servicio Ingreso',width:250},
						{field:'FechaIngreso',title:'Fecha Ing.',width:80},
						{field:'FuneteFinnac',title:'FuenFinan',width:75}					
					]]
				});	
			}				
			
			$.extend($("#FechaSolicitud").datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($("#FechaSolicitud").datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#FechaInicio" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    }); 			
			
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});						
			
			function guardar(){
				//Datos Personales del Receptor					
				var NroHistoriaClinica=$('#NroHistoriaClinica').numberbox('getValue');					
				if( ((NroHistoriaClinica.trim())=="" || (NroHistoriaClinica.length)<0) ){
					//$.messager.alert('Mensaje','Ingrese Nro Documento y ENTER para buscar Postulante','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Nro Historia Clinica y ENTER para buscar Paciente',
						icon:'warning',
						fn: function(){
							$('#NroHistoriaClinicaBusRec').next().find('input').focus();
						}
					});
					$('#NroHistoriaClinicaBusRec').next().find('input').focus();
					return 0;			
				}
				
				var IdGrupoSanguineoRec=$('#IdGrupoSanguineoRec').combobox('getValue');		
				var GrupoSanguineoRecANT=document.getElementById('GrupoSanguineoRecANT').value;			
				if( (IdGrupoSanguineoRec.trim()=="0" || IdGrupoSanguineoRec.trim()=="") &&  GrupoSanguineoRecANT.trim()!=""){
					//$.messager.alert('Mensaje','Seleccione Grupo Sanguineo del Paciente','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione Grupo Sanguineo del Paciente',
						icon:'warning',
						fn: function(){
							$('#IdGrupoSanguineoRec').next().find('input').focus();
						}
					});
					$('#IdGrupoSanguineoRec').next().find('input').focus();
					return 0;			
				}
				
				//Datos Atenciones
				var IdCuentaAtencion=document.getElementById('IdCuentaAtencion').value;				
				if(IdCuentaAtencion.trim()==""){
					//$.messager.alert('Mensaje','Seleccione una Cuenta del Paciente','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione una Cuenta del Paciente',
						icon:'warning',
						fn: function(){
							$('#IdCuentaAtencionBusRec').next().find('input').focus();
						}
					});
					//$('#IdCuentaAtencionBusRec').next().find('input').focus();
					$('#IdCuentaAtencionBusRec').combogrid({
					  inputEvents: $.extend({},$.fn.combogrid.defaults.inputEvents,{
						focus: function(e){ $(e.data.target).combogrid('showPanel');}
					  })
					})
					return 0;			
				}
				
				var CodigoCama=$('#CodigoCama').textbox('getValue');					
				if(CodigoCama.trim()==""){
					//$.messager.alert('Mensaje','Ingrese Código de Cama','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Código de Cama',
						icon:'warning',
						fn: function(){
							$('#CodigoCama').next().find('input').focus();
						}
					});
					$('#CodigoCama').next().find('input').focus();
					return 0;			
				}				
				
				var FechaSolicitud=$('#FechaSolicitud').datebox('getText');				
				if(FechaSolicitud.trim()==""){
					//$.messager.alert('Mensaje','Ingrese Fecha Solicitud','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Fecha Solicitud',
						icon:'warning',
						fn: function(){
							$('#FechaSolicitud').next().find('input').focus();
						}
					});
					$('#FechaSolicitud').next().find('input').focus();
					return 0;			
				}
				
				//INICIO DETALLE
				var CantidadST=$('#CantidadST').numberbox('getText');
				var CantidadPG=$('#CantidadPG').numberbox('getText');
				var CantidadGRlav=$('#CantidadGRlav').numberbox('getText');
				var CantidadPFC=$('#CantidadPFC').numberbox('getText');
				var CantidadPQ=$('#CantidadPQ').numberbox('getText');
				var CantidadCRIO=$('#CantidadCRIO').numberbox('getText');
				//alert(CantidadST);
				if( (CantidadST.trim()=='' || CantidadST==0) && (CantidadPG.trim()=='' || CantidadPG==0) && (CantidadGRlav.trim()=='' || CantidadGRlav==0) && (CantidadPFC.trim()=='' || CantidadPFC==0) && (CantidadPQ.trim()=='' || CantidadPQ==0) && (CantidadCRIO.trim()=='' || CantidadCRIO==0)){
						//$.messager.alert('Mensaje','Ingrese por lo menos un componente a la Solicitud','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese por lo menos un componente a la Solicitud',
							icon:'warning',
							fn: function(){
								$('#CantidadST').next().find('input').focus();
							}
						});
						$('#CantidadST').next().find('input').focus();
						return 0;
					}			
				
				
				var CantidadST=$('#CantidadST').numberbox('getText');	
				if ($('input[name="TipoReqST"]').is(':checked')) {											
				}else{
					if(CantidadST>0){
						//$.messager.alert('Mensaje','Seleccione si obtuvo Consentimiendo Informado del Receptor','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione el tipo de Requerimiento de SANGRE TOTAL',
							icon:'warning',
							fn: function(){
								$('#STclick').next().find('input').focus();
							}
						});
						$('#STclick').next().find('input').focus();
						return 0;
					}					
				}//end else
				
				var CantidadPG=$('#CantidadPG').numberbox('getText');
				if ($('input[name="TipoReqPG"]').is(':checked')) {											
				}else{
					if(CantidadPG>0){
						//$.messager.alert('Mensaje','Seleccione si obtuvo Consentimiendo Informado del Receptor','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione el tipo de Requerimiento de PAQUETE GLOBULAR',
							icon:'warning',
							fn: function(){
								$('#PGclick').next().find('input').focus();
							}
						});
						$('#PGclick').next().find('input').focus();
						return 0;
					}					
				}//end else
					
				var CantidadGRlav=$('#CantidadGRlav').numberbox('getText');	
				if ($('input[name="TipoReqGRlav"]').is(':checked')) {											
				}else{
					if(CantidadGRlav>0){
						//$.messager.alert('Mensaje','Seleccione si obtuvo Consentimiendo Informado del Receptor','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione el tipo de Requerimiento de GR LAVADOS',
							icon:'warning',
							fn: function(){
								$('#GRlavclick').next().find('input').focus();
							}
						});
						$('#GRlavclick').next().find('input').focus();
						return 0;
					}					
				}//end else
				
				var CantidadPFC=$('#CantidadPFC').numberbox('getText');	
				if ($('input[name="TipoReqPFC"]').is(':checked')) {											
				}else{
					if(CantidadPFC>0){
						//$.messager.alert('Mensaje','Seleccione si obtuvo Consentimiendo Informado del Receptor','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione el tipo de Requerimiento de PFC',
							icon:'warning',
							fn: function(){
								$('#PFCclick').next().find('input').focus();
							}
						});
						$('#PFCclick').next().find('input').focus();
						return 0;
					}					
				}//end else
				
				var CantidadPQ=$('#CantidadPQ').numberbox('getText');
				if ($('input[name="TipoReqPQ"]').is(':checked')) {											
				}else{
					if(CantidadPQ>0){
						//$.messager.alert('Mensaje','Seleccione si obtuvo Consentimiendo Informado del Receptor','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione el tipo de Requerimiento de PLAQUETAS ',
							icon:'warning',
							fn: function(){
								$('#PQclick').next().find('input').focus();
							}
						});
						$('#PQclick').next().find('input').focus();
						return 0;
					}					
				}//end else
					
				var CantidadCRIO=$('#CantidadCRIO').numberbox('getText');
				if ($('input[name="TipoReqCRIO"]').is(':checked')) {											
				}else{
					if(CantidadCRIO>0){
						//$.messager.alert('Mensaje','Seleccione si obtuvo Consentimiendo Informado del Receptor','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Seleccione el tipo de Requerimiento de CRIOPRECIPITADOS',
							icon:'warning',
							fn: function(){
								$('#CRIOclick').next().find('input').focus();
							}
						});
						$('#CRIOclick').next().find('input').focus();
						return 0;
					}					
				}//end else
				
				//FIN DETALLE			
				
				
				/*var FechaReservaOperST=$('#FechaReservaOperST').datebox('getText');
				var HoraReservaOperST=$('#HoraReservaOperST').datebox('getText');
				
				if(document.getElementById('ReservaOper1').checked==true){
					if(FechaReservaOperST.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#FechaReservaOperST').next().find('input').focus();
							}
						});
						$('#FechaReservaOperST').next().find('input').focus();
						return 0;			
					}
					
					if(HoraReservaOperST.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#HoraReservaOperST').next().find('input').focus();
							}
						});
						$('#HoraReservaOperST').next().find('input').focus();
						return 0;			
					}
				}*/
				
				var FechaProgST=$('#FechaProgST').datebox('getText');
				var HoraProgST=$('#HoraProgST').datebox('getText');
				if(document.getElementById('Programado1').checked==true){
					if(FechaProgST.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#FechaProgST').next().find('input').focus();
							}
						});
						$('#FechaProgST').next().find('input').focus();
						return 0;			
					}
					
					if(HoraProgST.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#HoraProgST').next().find('input').focus();
							}
						});
						$('#HoraProgST').next().find('input').focus();
						return 0;			
					}
				}
				
				/*var FechaReservaOperPG=$('#FechaReservaOperPG').datebox('getText');
				var HoraReservaOperPG=$('#HoraReservaOperPG').timespinner('getText');
				if(document.getElementById('ReservaOper2').checked==true){
					if(FechaReservaOperPG.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#FechaReservaOperPG').next().find('input').focus();
							}
						});
						$('#FechaReservaOperPG').next().find('input').focus();
						return 0;			
					}
					
					if(HoraReservaOperPG.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#HoraReservaOperPG').next().find('input').focus();
							}
						});
						$('#HoraReservaOperPG').next().find('input').focus();
						return 0;			
					}
				}*/
				
				var FechaProgPG=$('#FechaProgPG').datebox('getText');
				var HoraProgPG=$('#HoraProgPG').timespinner('getText');
				if(document.getElementById('Programado2').checked==true){
					if(FechaProgPG.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#FechaProgPG').next().find('input').focus();
							}
						});
						$('#FechaProgPG').next().find('input').focus();
						return 0;			
					}
					
					if(HoraProgPG.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#HoraProgPG').next().find('input').focus();
							}
						});
						$('#HoraProgPG').next().find('input').focus();
						return 0;			
					}
				}
				
				/*var FechaReservaOperGRlav=$('#FechaReservaOperGRlav').datebox('getText');
				var HoraReservaOperGRlav=$('#HoraReservaOperGRlav').timespinner('getText');
				if(document.getElementById('ReservaOper3').checked==true){
					if(FechaReservaOperGRlav.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#FechaReservaOperGRlav').next().find('input').focus();
							}
						});
						$('#FechaReservaOperGRlav').next().find('input').focus();
						return 0;			
					}
					
					if(HoraReservaOperGRlav.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#HoraReservaOperGRlav').next().find('input').focus();
							}
						});
						$('#HoraReservaOperGRlav').next().find('input').focus();
						return 0;			
					}
				}*/
				
				var FechaProgGRlav=$('#FechaProgGRlav').datebox('getText');
				var HoraProgGRlav=$('#HoraProgGRlav').timespinner('getText');
				if(document.getElementById('Programado3').checked==true){
					if(FechaProgGRlav.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#FechaProgGRlav').next().find('input').focus();
							}
						});
						$('#FechaProgGRlav').next().find('input').focus();
						return 0;			
					}
					
					if(HoraProgGRlav.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#HoraProgGRlav').next().find('input').focus();
							}
						});
						$('#HoraProgGRlav').next().find('input').focus();
						return 0;			
					}
				}
				
				
				/*var FechaReservaOperPFC=$('#FechaReservaOperPFC').datebox('getText');
				var HoraReservaOperPFC=$('#HoraReservaOperPFC').timespinner('getText');
				if(document.getElementById('ReservaOper4').checked==true){
					if(FechaReservaOperPFC.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#FechaReservaOperPFC').next().find('input').focus();
							}
						});
						$('#FechaReservaOperPFC').next().find('input').focus();
						return 0;			
					}
					
					if(HoraReservaOperPFC.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#HoraReservaOperPFC').next().find('input').focus();
							}
						});
						$('#HoraReservaOperPFC').next().find('input').focus();
						return 0;			
					}
				}*/
				
				var FechaProgPFC=$('#FechaProgPFC').datebox('getText');
				var HoraProgPFC=$('#HoraProgPFC').timespinner('getText');
				if(document.getElementById('Programado4').checked==true){
					if(FechaProgPFC.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#FechaProgPFC').next().find('input').focus();
							}
						});
						$('#FechaProgPFC').next().find('input').focus();
						return 0;			
					}
					
					if(HoraProgPFC.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#HoraProgPFC').next().find('input').focus();
							}
						});
						$('#HoraProgPFC').next().find('input').focus();
						return 0;			
					}
				}
				
				
				/*var FechaReservaOperPQ=$('#FechaReservaOperPQ').datebox('getText');
				var HoraReservaOperPQ=$('#HoraReservaOperPQ').timespinner('getText');
				if(document.getElementById('ReservaOper5').checked==true){
					if(FechaReservaOperPQ.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#FechaReservaOperPQ').next().find('input').focus();
							}
						});
						$('#FechaReservaOperPQ').next().find('input').focus();
						return 0;			
					}
					
					if(HoraReservaOperPQ.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#HoraReservaOperPQ').next().find('input').focus();
							}
						});
						$('#HoraReservaOperPQ').next().find('input').focus();
						return 0;			
					}
				}*/
				
				var FechaProgPQ=$('#FechaProgPQ').datebox('getText');
				var HoraProgPQ=$('#HoraProgPQ').timespinner('getText');
				if(document.getElementById('Programado5').checked==true){
					if(FechaProgPQ.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#FechaProgPQ').next().find('input').focus();
							}
						});
						$('#FechaProgPQ').next().find('input').focus();
						return 0;			
					}
					
					if(HoraProgPQ.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#HoraProgPQ').next().find('input').focus();
							}
						});
						$('#HoraProgPQ').next().find('input').focus();
						return 0;			
					}
				}
				
				/*var FechaReservaOperCRIO=$('#FechaReservaOperCRIO').datebox('getText');
				var HoraReservaOperCRIO=$('#HoraReservaOperCRIO').timespinner('getText');
				if(document.getElementById('ReservaOper6').checked==true){
					if(FechaReservaOperCRIO.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#FechaReservaOperCRIO').next().find('input').focus();
							}
						});
						$('#FechaReservaOperCRIO').next().find('input').focus();
						return 0;			
					}
					
					if(HoraReservaOperCRIO.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Reserva Operatoria','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Reserva Operatoria',
							icon:'warning',
							fn: function(){
								$('#HoraReservaOperCRIO').next().find('input').focus();
							}
						});
						$('#HoraReservaOperCRIO').next().find('input').focus();
						return 0;			
					}
				}*/
				
				var FechaProgCRIO=$('#FechaProgCRIO').datebox('getText');
				var HoraProgCRIO=$('#HoraProgCRIO').timespinner('getText');
				if(document.getElementById('Programado6').checked==true){
					if(FechaProgCRIO.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Fecha de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Fecha de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#FechaProgCRIO').next().find('input').focus();
							}
						});
						$('#FechaProgCRIO').next().find('input').focus();
						return 0;			
					}
					
					if(HoraProgCRIO.trim()==""){
						//$.messager.alert('Mensaje','Ingrese la Hora de Programación del Procedimiento','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Ingrese la Hora de Programación del Procedimiento',
							icon:'warning',
							fn: function(){
								$('#HoraProgCRIO').next().find('input').focus();
							}
						});
						$('#HoraProgCRIO').next().find('input').focus();
						return 0;			
					}
				}								
				
				//document.fmSolicitud.submit();				
				
				var GrupoSanguineoRecANT=document.getElementById('GrupoSanguineoRecANT').value;
				var GrupoSanguineoRec=document.getElementById('GrupoSanguineoRec').value;
				var mensaje2='';
				if(GrupoSanguineoRecANT.trim()=='0' || GrupoSanguineoRecANT.trim()==''){
					var mensaje2=' y Guardar el Grupo Sanguineo del Paciente ';
				}else if(GrupoSanguineoRecANT!=GrupoSanguineoRec){
					var mensaje2=' y Cambiar el Grupo Sanguineo del Paciente ';
				}				
				
				$.messager.confirm('Mensaje', '¿Seguro de Guardar la Solicitud del Paciente '+mensaje2+'?', function(r){
					if (r){
						document.getElementById("fmSolicitud").submit();
					}
				});			
			}	
		
		    $(function(){	  
			    $('#CodigoCama').textbox('textbox').attr('maxlength', $('#CodigoCama').attr("maxlength"));
				 
			});	
			
			/*function CambiarHematies(){
			   if(document.getElementById('OtrosAnemia1').checked==true){
				  $("#OtroswAnemia1").textbox('readonly',false);		 
				  $('#OtroswAnemia1').textbox({required: true});	
				  $('#OtroswAnemia1').next().find('input').focus();				  
			   }else{
				 $('#OtroswAnemia1').textbox('setValue', '');
				 $("#OtroswAnemia1").textbox('readonly',true);	
				 $('#OtroswAnemia1').textbox({required: false});
			   }   
		   }
		   
		   function CambiarPlasma(){
			   if(document.getElementById('OtrosPlasma1').checked==true){
				  $("#OtroswPlasma1").textbox('readonly',false);		 
				  $('#OtroswPlasma1').textbox({required: true});	
				  $('#OtroswPlasma1').next().find('input').focus();					  
			   }else{
				 $('#OtroswPlasma1').textbox('setValue', '');
				 $("#OtroswPlasma1").textbox('readonly',true);	
				 $('#OtroswPlasma1').textbox({required: false});
			   }   
		   }
		   
		   function CambiarPQ(){
			   if(document.getElementById('OtrosPQ1').checked==true){
				  $("#OtroswPQ1").textbox('readonly',false);		 
				  $('#OtroswPQ1').textbox({required: true});	
				  $('#OtroswPQ1').next().find('input').focus();					  
			   }else{
				 $('#OtroswPQ1').textbox('setValue', '');
				 $("#OtroswPQ1").textbox('readonly',true);	
				 $('#OtroswPQ1').textbox({required: false});
			   }   
		   } */
		   
		   //////////////////////////////////////////////////////
		   function CambiarCantidadST(){
			   var CantidadST=$('#CantidadST').numberbox('getText');
			   if(CantidadST.trim()==''){			
				   var checked = document.fmSolicitud.querySelectorAll('input[name=TipoReqST]:checked');	
				   if (checked.length > 0) {
					   for (var i = 0, length = checked.length; i < length; i++) {					
							checked[i].checked=false;		
						}	
				   }
					$("#FechaReservaOperST").datebox('setValue', '');
					$("#FechaReservaOperST").textbox('readonly',true);	
					$("#FechaReservaOperST").datebox({required: false});		 
					$("#HoraReservaOperST").timespinner('setValue', '');
					$("#HoraReservaOperST").textbox('readonly',true);	
					$("#HoraReservaOperST").textbox({required: false});
					
					$("#FechaProgST").datebox('setValue', '');
					$("#FechaProgST").textbox('readonly',true);		 
					$("#FechaProgST").datebox({required: false});		 
					$("#HoraProgST").timespinner('setValue', '');	  
					$("#HoraProgST").textbox('readonly',true);		 
					$("#HoraProgST").textbox({required: false});
			   }	  
		   } 
		   
		   function CambiarCantidadPG(){
			   var CantidadPG=$('#CantidadPG').numberbox('getText');
			   if(CantidadPG.trim()==''){			
				   var checked = document.fmSolicitud.querySelectorAll('input[name=TipoReqPG]:checked');	
				   if (checked.length > 0) {
					   for (var i = 0, length = checked.length; i < length; i++) {					
							checked[i].checked=false;		
						}	
				   }
					$("#FechaReservaOperPG").datebox('setValue', '');
					$("#FechaReservaOperPG").textbox('readonly',true);	
					$("#FechaReservaOperPG").datebox({required: false});		 
					$("#HoraReservaOperPG").timespinner('setValue', '');
					$("#HoraReservaOperPG").textbox('readonly',true);	
					$("#HoraReservaOperPG").textbox({required: false});
					
					$("#FechaProgPG").datebox('setValue', '');
					$("#FechaProgPG").textbox('readonly',true);		 
					$("#FechaProgPG").datebox({required: false});		 
					$("#HoraProgPG").timespinner('setValue', '');	  
					$("#HoraProgPG").textbox('readonly',true);		 
					$("#HoraProgPG").textbox({required: false});
			   }	  
		   } 
		   function CambiarCantidadGRlav(){
			   var CantidadGRlav=$('#CantidadGRlav').numberbox('getText');
			   if(CantidadGRlav.trim()==''){			
				   var checked = document.fmSolicitud.querySelectorAll('input[name=TipoReqGRlav]:checked');	
				   if (checked.length > 0) {
					   for (var i = 0, length = checked.length; i < length; i++) {					
							checked[i].checked=false;		
						}	
				   }
					$("#FechaReservaOperGRlav").datebox('setValue', '');
					$("#FechaReservaOperGRlav").textbox('readonly',true);	
					$("#FechaReservaOperGRlav").datebox({required: false});		 
					$("#HoraReservaOperGRlav").timespinner('setValue', '');
					$("#HoraReservaOperGRlav").textbox('readonly',true);	
					$("#HoraReservaOperGRlav").textbox({required: false});
					
					$("#FechaProgGRlav").datebox('setValue', '');
					$("#FechaProgGRlav").textbox('readonly',true);		 
					$("#FechaProgGRlav").datebox({required: false});		 
					$("#HoraProgGRlav").timespinner('setValue', '');	  
					$("#HoraProgGRlav").textbox('readonly',true);		 
					$("#HoraProgGRlav").textbox({required: false});
			   }	  
		   } 
		   
		   function CambiarCantidadPFC(){
			   var CantidadPFC=$('#CantidadPFC').numberbox('getText');
			   if(CantidadPFC.trim()==''){			
				   var checked = document.fmSolicitud.querySelectorAll('input[name=TipoReqPFC]:checked');	
				   if (checked.length > 0) {
					   for (var i = 0, length = checked.length; i < length; i++) {					
							checked[i].checked=false;		
						}	
				   }
					$("#FechaReservaOperPFC").datebox('setValue', '');
					$("#FechaReservaOperPFC").textbox('readonly',true);	
					$("#FechaReservaOperPFC").datebox({required: false});		 
					$("#HoraReservaOperPFC").timespinner('setValue', '');
					$("#HoraReservaOperPFC").textbox('readonly',true);	
					$("#HoraReservaOperPFC").textbox({required: false});
					
					$("#FechaProgPFC").datebox('setValue', '');
					$("#FechaProgPFC").textbox('readonly',true);		 
					$("#FechaProgPFC").datebox({required: false});		 
					$("#HoraProgPFC").timespinner('setValue', '');	  
					$("#HoraProgPFC").textbox('readonly',true);		 
					$("#HoraProgPFC").textbox({required: false});
			   }	  
		   } 
		   
		   function CambiarCantidadPQ(){
			   var CantidadPQ=$('#CantidadPQ').numberbox('getText');
			   if(CantidadPQ.trim()==''){			
				   var checked = document.fmSolicitud.querySelectorAll('input[name=TipoReqPQ]:checked');	
				   if (checked.length > 0) {
					   for (var i = 0, length = checked.length; i < length; i++) {					
							checked[i].checked=false;		
						}	
				   }
					$("#FechaReservaOperPQ").datebox('setValue', '');
					$("#FechaReservaOperPQ").textbox('readonly',true);	
					$("#FechaReservaOperPQ").datebox({required: false});		 
					$("#HoraReservaOperPQ").timespinner('setValue', '');
					$("#HoraReservaOperPQ").textbox('readonly',true);	
					$("#HoraReservaOperPQ").textbox({required: false});
					
					$("#FechaProgPQ").datebox('setValue', '');
					$("#FechaProgPQ").textbox('readonly',true);		 
					$("#FechaProgPQ").datebox({required: false});		 
					$("#HoraProgPQ").timespinner('setValue', '');	  
					$("#HoraProgPQ").textbox('readonly',true);		 
					$("#HoraProgPQ").textbox({required: false});
			   }	  
		   } 
		   
		   function CambiarCantidadCRIO(){
			   var CantidadCRIO=$('#CantidadCRIO').numberbox('getText');
			   if(CantidadCRIO.trim()==''){			
				   var checked = document.fmSolicitud.querySelectorAll('input[name=TipoReqCRIO]:checked');	
				   if (checked.length > 0) {
					   for (var i = 0, length = checked.length; i < length; i++) {					
							checked[i].checked=false;		
						}	
				   }
					$("#FechaReservaOperCRIO").datebox('setValue', '');
					$("#FechaReservaOperCRIO").textbox('readonly',true);	
					$("#FechaReservaOperCRIO").datebox({required: false});		 
					$("#HoraReservaOperCRIO").timespinner('setValue', '');
					$("#HoraReservaOperCRIO").textbox('readonly',true);	
					$("#HoraReservaOperCRIO").textbox({required: false});
					
					$("#FechaProgCRIO").datebox('setValue', '');
					$("#FechaProgCRIO").textbox('readonly',true);		 
					$("#FechaProgCRIO").datebox({required: false});		 
					$("#HoraProgCRIO").timespinner('setValue', '');	  
					$("#HoraProgCRIO").textbox('readonly',true);		 
					$("#HoraProgCRIO").textbox({required: false});
			   }	  
		   }  
		  
		   //////////////////////////////////////////////////////
		   
		   function CambiarRequerimientoST(){
			   var CantidadST=$('#CantidadST').numberbox('getText');
			   if(CantidadST.trim()=='' || CantidadST<=0){		  
				   $("#CantidadST").numberbox('setValue', 1);	
			   }			  		   
			   /*if(document.getElementById('ReservaOper1').checked==true){		  
				  $("#FechaReservaOperST").textbox('readonly',false);		 
				  $("#FechaReservaOperST").textbox({required: true});	
				  $("#FechaReservaOperST").next().find('input').focus();		  
				  $("#HoraReservaOperST").textbox('readonly',false);	 
				  $("#HoraReservaOperST").textbox({required: true});
			   }else{
				 $("#FechaReservaOperST").datebox('setValue', '');
				 $("#FechaReservaOperST").textbox('readonly',true);	
				 $("#FechaReservaOperST").datebox({required: false});		 
				 $("#HoraReservaOperST").timespinner('setValue', '');
				 $("#HoraReservaOperST").textbox('readonly',true);	
				 $("#HoraReservaOperST").textbox({required: false});		 
			   }*/ 
			   
			   if(document.getElementById('Programado1').checked==true){
				  $("#FechaProgST").textbox('readonly',false);		 
				  $("#FechaProgST").textbox({required: true});
				  $("#FechaProgST").next().find('input').focus();		  		  
				  $("#HoraProgST").textbox('readonly',false);	 
				  $("#HoraProgST").textbox({required: true});		  		
			   }else{		 
				 $("#FechaProgST").datebox('setValue', '');
				 $("#FechaProgST").textbox('readonly',true);		 
				 $("#FechaProgST").datebox({required: false});		 
				 $("#HoraProgST").timespinner('setValue', '');	  
				 $("#HoraProgST").textbox('readonly',true);		 
				 $("#HoraProgST").textbox({required: false});				 
			   } 	   
		   }
		   
			function CambiarRequerimientoPG(){
				var CantidadPG=$('#CantidadPG').numberbox('getText');
			    if(CantidadPG.trim()=='' || CantidadPG<=0){		  
				   $("#CantidadPG").numberbox('setValue', 1);	
			    }
				/*if(document.getElementById('ReservaOper2').checked==true){
				  $("#FechaReservaOperPG").textbox('readonly',false);		 
				  $("#FechaReservaOperPG").textbox({required: true});	
				  $("#FechaReservaOperPG").next().find('input').focus();		  
				  $("#HoraReservaOperPG").textbox('readonly',false);	 
				  $("#HoraReservaOperPG").textbox({required: true});		  
			   }else{
				 $("#FechaReservaOperPG").datebox('setValue', '');
				 $("#FechaReservaOperPG").textbox('readonly',true);	
				 $("#FechaReservaOperPG").datebox({required: false});		 
				 $("#HoraReservaOperPG").timespinner('setValue', '');
				 $("#HoraReservaOperPG").textbox('readonly',true);	
				 $("#HoraReservaOperPG").textbox({required: false});		 
			   }*/ 
			   
			   if(document.getElementById('Programado2').checked==true){
				  $("#FechaProgPG").textbox('readonly',false);		 
				  $("#FechaProgPG").textbox({required: true});	
				  $("#FechaProgPG").next().find('input').focus();		  
				  $("#HoraProgPG").textbox('readonly',false);	 
				  $("#HoraProgPG").textbox({required: true});		
			   }else{		 
				 $("#FechaProgPG").datebox('setValue', '');
				 $("#FechaProgPG").textbox('readonly',true);		 
				 $("#FechaProgPG").datebox({required: false});		 
				 $("#HoraProgPG").timespinner('setValue', '');	  
				 $("#HoraProgPG").textbox('readonly',true);		 
				 $("#HoraProgPG").textbox({required: false});				 
			   }	
			}
			
			function CambiarRequerimientoGRlav(){
				 var CantidadGRlav=$('#CantidadGRlav').numberbox('getText');
			     if(CantidadGRlav.trim()=='' || CantidadGRlav<=0){		  
				   $("#CantidadGRlav").numberbox('setValue', 1);	
			     }
				 /*if(document.getElementById('ReservaOper3').checked==true){
				  $("#FechaReservaOperGRlav").textbox('readonly',false);		 
				  $("#FechaReservaOperGRlav").textbox({required: true});	
				  $("#FechaReservaOperGRlav").next().find('input').focus();		  
				  $("#HoraReservaOperGRlav").textbox('readonly',false);	 
				  $("#HoraReservaOperGRlav").textbox({required: true});	  
			   }else{
				 $("#FechaReservaOperGRlav").datebox('setValue', '');
				 $("#FechaReservaOperGRlav").textbox('readonly',true);	
				 $("#FechaReservaOperGRlav").datebox({required: false});		 
				 $("#HoraReservaOperGRlav").timespinner('setValue', '');
				 $("#HoraReservaOperGRlav").textbox('readonly',true);	
				 $("#HoraReservaOperGRlav").textbox({required: false}); 	 
			   } */
			   
			   if(document.getElementById('Programado3').checked==true){
				  $("#FechaProgGRlav").textbox('readonly',false);		 
				  $("#FechaProgGRlav").textbox({required: true});	
				  $("#FechaProgGRlav").next().find('input').focus();		  
				  $("#HoraProgGRlav").textbox('readonly',false);	 
				  $("#HoraProgGRlav").textbox({required: true});	
			   }else{		 
				 $("#FechaProgGRlav").datebox('setValue', '');
				 $("#FechaProgGRlav").textbox('readonly',true);		 
				 $("#FechaProgGRlav").datebox({required: false});		 
				 $("#HoraProgGRlav").timespinner('setValue', '');	  
				 $("#HoraProgGRlav").textbox('readonly',true);		 
				 $("#HoraProgGRlav").textbox({required: false});			 
			   }
			}
			
			
			function CambiarRequerimientoPFC(){
				 var CantidadPFC=$('#CantidadPFC').numberbox('getText');
			     if(CantidadPFC.trim()=='' || CantidadPFC<=0){		  
				   $("#CantidadPFC").numberbox('setValue', 1);	
			     }
				/*if(document.getElementById('ReservaOper4').checked==true){
				  $("#FechaReservaOperPFC").textbox('readonly',false);		 
				  $("#FechaReservaOperPFC").textbox({required: true});	
				  $("#FechaReservaOperPFC").next().find('input').focus();		  
				  $("#HoraReservaOperPFC").textbox('readonly',false);	 
				  $("#HoraReservaOperPFC").textbox({required: true});	  
			   }else{
				 $("#FechaReservaOperPFC").datebox('setValue', '');
				 $("#FechaReservaOperPFC").textbox('readonly',true);	
				 $("#FechaReservaOperPFC").datebox({required: false});		 
				 $("#HoraReservaOperPFC").timespinner('setValue', '');
				 $("#HoraReservaOperPFC").textbox('readonly',true);	
				 $("#HoraReservaOperPFC").textbox({required: false}); 		 
			   } */
				
			   if(document.getElementById('Programado4').checked==true){
				  $("#FechaProgPFC").textbox('readonly',false);		 
				  $("#FechaProgPFC").textbox({required: true});	
				  $("#FechaProgPFC").next().find('input').focus();		  
				  $("#HoraProgPFC").textbox('readonly',false);	 
				  $("#HoraProgPFC").textbox({required: true});		
			   }else{		 
				 $("#FechaProgPFC").datebox('setValue', '');
				 $("#FechaProgPFC").textbox('readonly',true);		 
				 $("#FechaProgPFC").datebox({required: false});		 
				 $("#HoraProgPFC").timespinner('setValue', '');	  
				 $("#HoraProgPFC").textbox('readonly',true);		 
				 $("#HoraProgPFC").textbox({required: false});			 
			   }
			
			}
			
			
			function CambiarRequerimientoPQ(){
				var CantidadPQ=$('#CantidadPQ').numberbox('getText');
			     if(CantidadPQ.trim()=='' || CantidadPQ<=0){		  
				   $("#CantidadPQ").numberbox('setValue', 1);	
			     }
				/*if(document.getElementById('ReservaOper5').checked==true){
				  $("#FechaReservaOperPQ").textbox('readonly',false);		 
				  $("#FechaReservaOperPQ").textbox({required: true});	
				  $("#FechaReservaOperPQ").next().find('input').focus();		  
				  $("#HoraReservaOperPQ").textbox('readonly',false);	 
				  $("#HoraReservaOperPQ").textbox({required: true});		  
			   }else{
				 $("#FechaReservaOperPQ").datebox('setValue', '');
				 $("#FechaReservaOperPQ").textbox('readonly',true);	
				 $("#FechaReservaOperPQ").datebox({required: false});		 
				 $("#HoraReservaOperPQ").timespinner('setValue', '');
				 $("#HoraReservaOperPQ").textbox('readonly',true);	
				 $("#HoraReservaOperPQ").textbox({required: false}); 		 
			   }*/ 
			   
			   if(document.getElementById('Programado5').checked==true){
				  $("#FechaProgPQ").textbox('readonly',false);		 
				  $("#FechaProgPQ").textbox({required: true});	
				  $("#FechaProgPQ").next().find('input').focus();		  
				  $("#HoraProgPQ").textbox('readonly',false);	 
				  $("#HoraProgPQ").textbox({required: true});		
			   }else{		 
				 $("#FechaProgPQ").datebox('setValue', '');
				 $("#FechaProgPQ").textbox('readonly',true);		 
				 $("#FechaProgPQ").datebox({required: false});		 
				 $("#HoraProgPQ").timespinner('setValue', '');	  
				 $("#HoraProgPQ").textbox('readonly',true);		 
				 $("#HoraProgPQ").textbox({required: false});			 
			   }
			}
			
			
			function CambiarRequerimientoCRIO(){
				var CantidadCRIO=$('#CantidadCRIO').numberbox('getText');
			    if(CantidadCRIO.trim()=='' || CantidadCRIO<=0){		  
				   $("#CantidadCRIO").numberbox('setValue', 1);	
			    }
				/*if(document.getElementById('ReservaOper6').checked==true){
				  $("#FechaReservaOperCRIO").textbox('readonly',false);		 
				  $("#FechaReservaOperCRIO").textbox({required: true});	
				  $("#FechaReservaOperCRIO").next().find('input').focus();		  
				  $("#HoraReservaOperCRIO").textbox('readonly',false);	 
				  $("#HoraReservaOperCRIO").textbox({required: true});	 
			   }else{
				 $("#FechaReservaOperCRIO").datebox('setValue', '');
				 $("#FechaReservaOperCRIO").textbox('readonly',true);	
				 $("#FechaReservaOperCRIO").datebox({required: false});		 
				 $("#HoraReservaOperCRIO").timespinner('setValue', '');
				 $("#HoraReservaOperCRIO").textbox('readonly',true);	
				 $("#HoraReservaOperCRIO").textbox({required: false}); 		 
			   }*/ 
			   
			   if(document.getElementById('Programado6').checked==true){
				  $("#FechaProgCRIO").textbox('readonly',false);		 
				  $("#FechaProgCRIO").textbox({required: true});	
				  $("#FechaProgCRIO").next().find('input').focus();		  
				  $("#HoraProgCRIO").textbox('readonly',false);	 
				  $("#HoraProgCRIO").textbox({required: true});			
			   }else{		 
				 $("#FechaProgCRIO").datebox('setValue', '');
				 $("#FechaProgCRIO").textbox('readonly',true);		 
				 $("#FechaProgCRIO").datebox({required: false});		 
				 $("#HoraProgCRIO").timespinner('setValue', '');	  
				 $("#HoraProgCRIO").textbox('readonly',true);		 
				 $("#HoraProgCRIO").textbox({required: false});			 
			   }
			}	
		//FIN Registrar Solicitud de Sangre del Paciente
		
		
		//Registrar Resumen de Solicitud Transfusional	

			//Diagnosticos
			$(function(){	
				$('#CodigoCIE2004').combogrid({
					panelWidth:400,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Buscar_Diagnosticos&TipoBusqueda=1',
					idField:'IdDiagnostico', //NombresPaciente
					textField:'CodigoCIE2004',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){//esta funcion llama cuando seleccionas						 
						var IdDiagnostico= $('#CodigoCIE2004').combogrid('getValue');		
						document.getElementById('IdDiagnostico').value=IdDiagnostico;
						var g = $('#CodigoCIE2004').combogrid('grid');	// get datagrid object
						var r = g.datagrid('getSelected');	// get the selected row
						$('#diagnostico').textbox('setValue', r.Descripcion);
					},					
					columns:[[							
						{field:'CodigoCIE2004',title:'Codigo',width:70},
						{field:'Descripcion',title:'Descripcion',width:350}						
					]]
				});
				
				$('#diagnostico').combogrid({
					panelWidth:400,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Buscar_Diagnosticos&TipoBusqueda=2',
					idField:'IdDiagnostico', //NombresPaciente
					textField:'Descripcion',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){//esta funcion llama cuando seleccionas 						 
						var IdDiagnostico= $('#diagnostico').combogrid('getValue');		
						document.getElementById('IdDiagnostico').value=IdDiagnostico;
						var g = $('#diagnostico').combogrid('grid');	// get datagrid object
						var r = g.datagrid('getSelected');	// get the selected row
						$('#CodigoCIE2004').textbox('setValue', r.CodigoCIE2004);
					},				
					columns:[[							
						{field:'CodigoCIE2004',title:'Codigo',width:70},
						{field:'Descripcion',title:'Descripcion',width:350}						
					]]
				});
				
				$('#CodigoCIE2').combogrid({
					panelWidth:400,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Buscar_Diagnosticos&TipoBusqueda=1',
					idField:'IdDiagnostico', //NombresPaciente
					textField:'CodigoCIE2004',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){//esta funcion llama cuando seleccionas 						 
						var IdDiagnostico= $('#CodigoCIE2').combogrid('getValue');		
						document.getElementById('IdDiagnostico2').value=IdDiagnostico;
						var g = $('#CodigoCIE2').combogrid('grid');	// get datagrid object
						var r = g.datagrid('getSelected');	// get the selected row
						$('#diagnostico2').textbox('setValue', r.Descripcion);
					},					
					columns:[[							
						{field:'CodigoCIE2004',title:'Codigo',width:70},
						{field:'Descripcion',title:'Descripcion',width:350}						
					]]
				});
				
				$('#diagnostico2').combogrid({
					panelWidth:400,
					value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Buscar_Diagnosticos&TipoBusqueda=2',
					idField:'IdDiagnostico', //NombresPaciente
					textField:'Descripcion',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){//esta funcion llama cuando seleccionas 						 
						var IdDiagnostico= $('#diagnostico2').combogrid('getValue');		
						document.getElementById('IdDiagnostico2').value=IdDiagnostico;
						var g = $('#diagnostico2').combogrid('grid');	// get datagrid object
						var r = g.datagrid('getSelected');	// get the selected row
						$('#CodigoCIE2').textbox('setValue', r.CodigoCIE2004);
					},				
					columns:[[							
						{field:'CodigoCIE2004',title:'Codigo',width:70},
						{field:'Descripcion',title:'Descripcion',width:350}						
					]]
				});			
				///////////////////////////////
				
				
				$('#NroHistoriaBusReg').numberbox('textbox').keypress(function(e){	
					var NroHistoriaClinica= $('#NroHistoriaBusReg').numberbox('getText');
										
					if (e.which == 13) {			
						//$.messager.alert('Mensaje','El DNI debe tener 8 Digitos','warning');					
						$.ajax({
						url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPacienteAtenciones",					
						type: "GET",
						dataType: "JSON",
						data: {						
							q: NroHistoriaClinica						
						},							
							success: function (data) {	
										
									//$('#IdCuentaAtencionBusRec').next().find('input').focus();
									//LlenarDatosPacientesHC(data);	
									$('#NroHistoriaClinica2').textbox('setValue', data.NroHistoria);	
									$('#ApellidosNombres2').textbox('setValue', data.NombresPaciente);	
									$('#IdCuentaAtencionReg').textbox('setValue', data.IdCuentaAtencion);
									$('#NroSolicitud').textbox('setValue', data.NroSolicitud);
									document.getElementById('IdSolicitudSangre').value=data.IdSolicitudSangre;
									
									if(data.IdTipoSexo==2){//mujer					
										document.getElementById('SITP2').disabled=false;
										document.getElementById('SITP3').disabled=false;
										document.getElementById('SITP4').disabled=false;
									}else{//hombre			
										document.getElementById('NOTP2').checked=true;	
										document.getElementById('NOTP3').checked=true;	
										document.getElementById('NOTP4').checked=true;	
										
										document.getElementById('SITP2').disabled=true;	
										document.getElementById('SITP3').disabled=true;
										document.getElementById('SITP4').disabled=true;	
									}
																			//								
									$('#IdGrupoSanguineoRec2').combobox('setValue', data.IdGrupoSanguineo);
									//document.getElementById('GrupoSanguineoRecANT2').value=data.GrupoSanguineo;	
									//document.getElementById('GrupoSanguineoRec2').value=data.GrupoSanguineo;
									VerDatosAtencionesSolicitud(data.IdAtencion);																								
																					
							}				
						}).done(function (data) {
							console.log(data);
							//alert(data);						
							if(data=="") { //if(!data.success){	
								//document.getElementById('IdPostulante').value="";			
								$.messager.alert('SIGESA','El Paciente NO EXISTE o NO TIENE CUENTA ACTIVA' ,'info'); //NO HAY DATOS
								//$('#MensajeExiste').html('');			
							}else if(data.NroSolicitud==""){
								$.messager.alert('SIGESA','El Paciente NO TIENE Solicitud Pendiente para Registrar Resumen Transfusional','info'); //NO HAY DATOS
							}
						});	//fin done
						e.preventDefault();
						return false;
					}
				}); //FIN NroHistoriaBusReg
				///////////////////////////////	
				
			});
			
			function VerDatosAtencionesSolicitud(IdAtencion){	
				$.ajax({
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarDatosPacientesAtencionesEstancia',
					type: 'POST',
					dataType: 'json',
					data: {				
						IdAtencion:IdAtencion,
					},							
					success: function (data) {		
						//alert(data.numerocama);
						//document.getElementById('IdCuentaAtencion').value=data.IdCuentaAtencion;
						//$('#IdTipoServicio').combobox('setValue', data.IdTipoServicio);
						//$('#IdServicioHospital').combobox('setValue', data.IdServicioIngreso);	
						//$('#CodigoCama').textbox('setValue', data.numerocama);
						var diagnostico=data.diagnostico;
						var IdDiagnostico=''; CodigoCIE2004=''; Descripciondiagnostico='';
						if(diagnostico!=null){
							var partesdiagnostico=diagnostico.split("|");
							IdDiagnostico=partesdiagnostico[0];
							CodigoCIE2004=partesdiagnostico[1];
							Descripciondiagnostico=partesdiagnostico[2];
						}				
						document.getElementById('IdDiagnostico').value=IdDiagnostico;
						$('#CodigoCIE2004').combogrid('setValue', CodigoCIE2004);
						$('#diagnostico').combogrid('setValue', Descripciondiagnostico);
										
					}
				})
				
			}			
			
			///////////////////////////////
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
			
			$(function(){
				$('#MedicoSolicitante').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MedicoSolicitante"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					}								
				});										
				$('#MedicoSolicitante').combobox('validate');	
				
				$('#UsuTomoMuestraPaciente').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					//required: true,    
					validType: 'exists["#UsuTomoMuestraPaciente"]',
					filter: function (q, row) {
					return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
					}								
				});						
				$('#UsuTomoMuestraPaciente').combobox('validate');	
				
			});			
			
			function GuardarResumen(){
				var NroHistoriaClinica2=document.getElementById('NroHistoriaClinica2').value;	
								
				if( NroHistoriaClinica2.trim()=="" ){
					//$.messager.alert('Mensaje','Ingrese Código de Cama','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Busque un Paciente que tenga Solicitud Pendiente para Registrar Resumen Transfusional',
						icon:'warning',
						fn: function(){
							$('#NroHistoriaBusReg').next().find('input').focus();
						}
					});
					$('#NroHistoriaBusReg').next().find('input').focus();
					return 0;			
				}				
				
				var diagnostico=$('#diagnostico').textbox('getValue');		
				var IdDiagnostico=document.getElementById('IdDiagnostico').value;
				
				//var diagnostico2=$('#diagnostico2').textbox('getValue');		
				//var IdDiagnostico2=document.getElementById('IdDiagnostico2').value;	
								
				if(diagnostico.trim()=="" || IdDiagnostico.trim()==""){
					//$.messager.alert('Mensaje','Ingrese Código de Cama','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Por lo menos el Diagnóstico 1',
						icon:'warning',
						fn: function(){
							$('#diagnostico').next().find('input').focus();
						}
					});
					$('#diagnostico').next().find('input').focus();
					return 0;			
				}			
				
				var Hemoglobina=$('#Hemoglobina').numberbox('getText');				
				/*if(Hemoglobina.trim()==""){
					//$.messager.alert('Mensaje','Ingrese Código de Cama','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Hemoglobina del Paciente',
						icon:'warning',
						fn: function(){
							$('#Hemoglobina').next().find('input').focus();
						}
					});
					$('#Hemoglobina').next().find('input').focus();
					return 0;			
				}*/
				
				var Hematocrito=$('#Hematocrito').numberbox('getText');				
				/*if(Hematocrito.trim()==""){
					//$.messager.alert('Mensaje','Ingrese Código de Cama','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Porcentaje de Hematocrito del Paciente',
						icon:'warning',
						fn: function(){
							$('#Hematocrito').next().find('input').focus();
						}
					});
					$('#Hematocrito').next().find('input').focus();
					return 0;			
				}*/
				
				var Plaquetas=$('#Plaquetas').numberbox('getText');				
				/*if(Plaquetas.trim()==""){
					//$.messager.alert('Mensaje','Ingrese Código de Cama','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese mm3 de Plaquetas del Paciente',
						icon:'warning',
						fn: function(){
							$('#Plaquetas').next().find('input').focus();
						}
					});
					$('#Plaquetas').next().find('input').focus();
					return 0;			
				}*/
				
				if ($('input[name="swTransPre"]').is(':checked')) {							
				}else{
					//$.messager.alert('Mensaje','Seleccione si es una Transfusión Previa','warning');	
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione si es una Transfusión Previa',
						icon:'warning',
						fn: function(){
							$('#TPclick').next().find('input').focus();
						}
					});
					$('#TPclick').next().find('input').focus();
					return 0;
				}
				
				if ($('input[name="swReacTrans"]').is(':checked')) {							
				}else{
					//$.messager.alert('Mensaje','Seleccione si tiene Reacciones Transfunsionales Previas','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione si tiene Reacciones Transfunsionales Previas',
						icon:'warning',
						fn: function(){
							$('#RPclick').next().find('input').focus();
						}
					});
					$('#RPclick').next().find('input').focus();
					return 0;	
				}
				
				if ($('input[name="swReacAler"]').is(':checked')) {							
				}else{
					//$.messager.alert('Mensaje','Seleccione si tiene Reacciones Transfunsionales Previas','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione si tiene Reacciones Alergicas',
						icon:'warning',
						fn: function(){
							$('#RAclick').next().find('input').focus();
						}
					});
					$('#RAclick').next().find('input').focus();
					return 0;	
				}				
				////////////////////////////////
			
				if ($('input[name="swConsentimiendoInformado"]').is(':checked')) {							
				}else{
					//$.messager.alert('Mensaje','Seleccione si obtuvo Consentimiendo Informado del Receptor','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione si obtuvo Consentimiendo Informado del Receptor',
						icon:'warning',
						fn: function(){
							$('#consentimientoclick').next().find('input').focus();
						}
					});
					$('#consentimientoclick').next().find('input').focus();
					return 0;	
				}
				
				var MotivoNOConsentimiendo=$('#MotivoNOConsentimiendo').textbox('getText');				
				if(MotivoNOConsentimiendo.trim()=="" && document.getElementById('NO').checked==true){
					//$.messager.alert('Mensaje','Ingrese Motivo de NO consentimiento Informado','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Ingrese Motivo de NO consentimiento Informado',
						icon:'warning',
						fn: function(){
							$('#MotivoNOConsentimiendo').next().find('input').focus();
						}
					});
					$('#MotivoNOConsentimiendo').next().find('input').focus();
					return 0;			
				}
				
				var MedicoSolicitante=$('#MedicoSolicitante').combobox('getValue');				
				if(MedicoSolicitante.trim()=="" || $('#MedicoSolicitante').combobox('isValid')==false){
					//$.messager.alert('Mensaje','Seleccione Medico Solicitante','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione Medico Solicitante',
						icon:'warning',
						fn: function(){
							$('#MedicoSolicitante').next().find('input').focus();
						}
					});
					$('#MedicoSolicitante').next().find('input').focus();
					return 0;			
				}
				
				var UsuTomoMuestraPaciente=$('#UsuTomoMuestraPaciente').combobox('getValue');	
				/*if(UsuTomoMuestraPaciente.trim()=="" || $('#UsuTomoMuestraPaciente').combobox('isValid')==false){
					//$.messager.alert('Mensaje','Seleccione Personal que Tomó Muestra del Paciente','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Seleccione Personal que Tomó Muestra del Paciente',
						icon:'warning',
						fn: function(){
							$('#UsuTomoMuestraPaciente').next().find('input').focus();
						}
					});
					$('#UsuTomoMuestraPaciente').next().find('input').focus();
					return 0;			
				}*/
				
				$.messager.confirm('Mensaje', '¿Seguro de Guardar el Resumen de la Solicitud Transfusional?', function(r){
					if (r){
						document.getElementById("fmResumen").submit();
					}
				});
			}
			
		function CambiarMotivo(){
		   if(document.getElementById('SI').checked==true){
			 $('#MotivoNOConsentimiendo').textbox('setValue', '');
			 $("#MotivoNOConsentimiendo").textbox('readonly',true);	
			 $('#MotivoNOConsentimiendo').textbox({required: false});		  
		   }else{
			  $("#MotivoNOConsentimiendo").textbox('readonly',false);		 
			  $('#MotivoNOConsentimiendo').textbox({required: true});	
			  $('#MotivoNOConsentimiendo').next().find('input').focus();	 
		   }   
	    }	
		//Fin Resumen de Solicitud Transfusional		
		
		
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>     
        
<body>

<div style="margin:0px 0;"></div>    
            
<div class="easyui-tabs" style="width:90%;height:auto">		
		<!--Inicio Fraccionamiento-->
		<div title="Registrar Solicitud de Sangre del Paciente" style="padding:0px" iconCls="icon-save"> 		
			
            <div id="Fraccionamiento">
            
           	  <form id="fmSolicitud" name="fmSolicitud" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarSolicitudSangre&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">                  
				  <!--<div id="p" class="easyui-panel" style="width:90%;height:auto;" title="Banco de Sangre: Registrar Solicitud de Sangre del Paciente" iconCls="icon-save" align="left">--> <!--collapsible="true"  -->  
	
			    <div id="tb" style="padding:5px;height:auto">
						<div style="margin-bottom:5px">				
							<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>  				        
							<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="guardar()">Guardar Solicitud</a>
							<a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="Regresar();">Regresar</a>
						</div> 
					  
						<fieldset style="width:90%">					
							<legend style="color:#03C"><strong>Busqueda:</strong></legend>
							<table width="94%" style="font-size:12px;">
							  <tr align="center">
								<th width="41%" bgcolor="#D6D6D6"><strong>Nro Historia Paciente</strong></th>
								<th width="59%" bgcolor="#D6D6D6"><strong>Cuentas del Paciente</strong></th>
							  </tr>
							  <tr align="center">
								<td><input class="easyui-numberbox" style="width:110px" id="NroHistoriaClinicaBusRec" name="NroHistoriaClinicaBusRec" data-options="prompt:'Nº Historia'" maxlength="12"> Digite y ENTER
								<td>
									<input class="easyui-combogrid" style="width:200px" id="IdCuentaAtencionBusRec" name="IdCuentaAtencionBusRec"  data-options="prompt:'Cuentas del Paciente'">
									<input name="IdCuentaAtencion" id="IdCuentaAtencion" type="hidden" />
                                    <input name="IdAtencion" id="IdAtencion" type="hidden" />
								</td> 
							  </tr>                         
							</table> 
						</fieldset> 								
			    </div>  
	 
			    <table width="90%">
				  <tr>
					  <td width="38">&nbsp;</td>
				    <td width="159">Nro Historia</td>
				    <td width="219"><input name="NroHistoriaClinica" type="text" id="NroHistoriaClinica" class="easyui-textbox" style="width:100px;" readonly />
				    <input name="IdPaciente" id="IdPaciente" type="hidden" /></td>
				    <td width="169">Apellidos y Nombres</td>
				    <td colspan="2"><input name="ApellidosNombres" type="text" id="ApellidosNombres" class="easyui-textbox" style="width:250px;" readonly /></td>
				  </tr>
				  <tr>
					  <td>&nbsp;</td>
				    <td>Nro Documento</td>
				    <td><input name="NroDocumento" type="text" id="NroDocumento" class="easyui-textbox" style="width:100px;" readonly /></td>
				    <td>Sexo</td>
				    <td><select class="easyui-combobox" name="IdTipoSexoRec" id="IdTipoSexoRec"  style="width: 150px" data-options="prompt:'Seleccione'" readonly>
					  <option value="0"></option>
					  <?php
											 $listarSexo=SIGESA_Emergencia_ListarTiposSexo_M();
											 if($listarSexo != NULL) { 
												foreach($listarSexo as $item){?>
					  <option value="<?php echo $item["IdTipoSexo"]?>" ><?php echo $item["IdTipoSexo"]."=".$item["Descripcion"]?></option>
					  <?php } } ?>
				    </select></td>
                    
				    <td rowspan="2">
                    	<table id="tt" style="margin:0px !important"></table>
                    </td>
				  </tr>
				  <tr>
					  <td>&nbsp;</td>
				    <td>Fecha Nacimiento</td>
				    <td><input name="FechaNacimiento" type="text" id="FechaNacimiento" class="easyui-textbox" style="width:100px;" readonly />
				    <input name="Edad" type="text" id="Edad" class="easyui-textbox" style="width:60px;" data-options="prompt:'Edad'" readonly /></td>
				    <td>Grupo Sanguineo<br> (si cuenta con resultado)</td>
				    <td><select class="easyui-combobox" name="IdGrupoSanguineoRec" id="IdGrupoSanguineoRec"  style="width: 150px" data-options="prompt:'Seleccione',
					valueField: 'id',
					textField: 'text',        
					onSelect: function(rec){
					var url = cambiarGrupoSanguineoRec(); }">
					  <option value="0"></option>
					  <?php
											  $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
											   if($listarSexo != NULL) { 
												 foreach($listarSexo as $item){?>
					  <option value="<?php echo $item["IdGrupoSanguineo"]?>" ><?php echo $item["Descripcion"]?></option>
					  <?php } } ?>
					  </select>
					  <input name="GrupoSanguineoRec" id="GrupoSanguineoRec" type="hidden" />
					  <input name="GrupoSanguineoRecANT" id="GrupoSanguineoRecANT" type="hidden" />              
				    
                    </td>
			      </tr>
				  <tr>
				    <td colspan="6"><hr></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>Contingencia</td>
				    <td><select class="easyui-combobox" name="IdTipoServicio" id="IdTipoServicio"  style="width: 160px" data-options="prompt:'Seleccione'" >
					  <option value="0"></option>
					  <option value="1" >Consultorios Externos</option>
					  <option value="2" >Consultorios de Emergencia</option>
					  <option value="4" >Observación Emergencia</option>
					  <option value="3" >Hospitalización</option>
				    </select></td>
				    <td>Servicio</td>
				    <td colspan="2"><select class="easyui-combobox" name="IdServicioHospital" id="IdServicioHospital"  style="width:250px" >
					  <option value="0">No se sabe</option>
					  <?php
									  $listarEC=ListarServiciosHospitalM();
									   if($listarEC != NULL) { 
										 foreach($listarEC as $item){?>
					  <option value="<?php echo $item["IdServicio"]?>" ><?php echo trim(ucfirst(mb_strtoupper($item["Nombre"])));?></option>
					  <?php } } ?>
				    </select></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>Codigo Cama</td>
				    <td><input name="CodigoCama" type="text" id="CodigoCama" class="easyui-textbox" style="width:160px;" data-options="required:true" maxlength="5" /></td>
				    <td colspan="3"> </td>
			      </tr>	  
				  
				  <tr>             
				    <td colspan="6"><hr></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>Fecha y Hora</td>
				    <td><input name="FechaSolicitud" id="FechaSolicitud" class="easyui-datebox" style="width:100px;" value="<?php echo date('d/m/Y');?>" data-options="required:true" validType="validDate"/>
				    <input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" style="width:85px;" id="HoraSolicitud" name="HoraSolicitud" data-options="showSeconds:true,required:true"></td>
				    <td>&nbsp;</td>
				    <td colspan="2">&nbsp;</td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td colspan="5"><table width="98%" border="1" cellpadding="0" cellspacing="0" class="">
					  <thead>
						<tr align="center">
						  <th colspan="4">&nbsp;</th>
						  <th colspan="3"><strong>REQUERIMIENTO</strong></th>
					    </tr>
						<tr align="center">
						  <th><strong>CODIGO</strong></th>
						  <th><strong>COMPONENTE SOLICITADO</strong></th>
						  <th><strong>CANTIDAD</strong></th>
						  <th><p>VOLUMEN en ml<br>(Pediatría)</p></th>
						  <th>Muy urgente</th>
						  <th>Urgente</th>
						  <th>Programado para el día</th>
						</tr>
					  </thead>
					  <tbody>
						<tr align="center">
						  <td>
						  <input name="CodigoST" type="text" id="CodigoST" value="ST" style="width:65px;" readonly/></td>
						  <td>SANGRE TOTAL  RECONSTITUIDA</td>
						  <td><input name="CantidadST" type="text" id="CantidadST" class="easyui-numberbox" data-options="max:1,prompt:'Cantidad ST',
                                valueField: 'id',
                                textField: 'text',        
                                onChange: function(rec){
                                var url = CambiarCantidadST(); }" style="width:85px;" />
						  </td>
						  <td><input name="VolumenST" type="text" id="VolumenST" class="easyui-numberbox" data-options="min:0,prompt:'Volumen ST'" style="width:85px;" readonly /></td>
						  <td>                 
							<label for="Inmediato" onClick="CambiarRequerimientoST()" >Sin Prueba de <br>Compatibilidad</label>
							<input type="radio" name="TipoReqST" id="Inmediato" value="Muy urgente" onClick="CambiarRequerimientoST()" />                  
						  </td>
						  <td id="STclick">
							<label for="Urgente" onClick="CambiarRequerimientoST()" >< 1 hora</label>
							<input type="radio" name="TipoReqST" id="Urgente" value="Urgente" onClick="CambiarRequerimientoST()" />
						  </td>
						  <td>
                          	<label for="Programado1">
                           	  <input name="FechaProgST" type="text" id="FechaProgST" class="easyui-datebox" style="width:100px;" value="" validType="validDate" readonly />
			   				  <input class="easyui-timespinner" value="" style="width:70px;" id="HoraProgST" name="HoraProgST" readonly />
                            </label>    
			   			  <input type="radio" name="TipoReqST" id="Programado1" value="Programado" onClick="CambiarRequerimientoST()" /></td>
						</tr>
						<tr align="center">
						  <td><input name="CodigoPG" type="text" id="CodigoPG" value="PG" style="width:65px;" readonly/></td>
						  <td>PAQUETE GLOBULAR </td>
						  <td><input name="CantidadPG" type="text" id="CantidadPG" class="easyui-numberbox" data-options="prompt:'Cantidad PG',
                                valueField: 'id',
                                textField: 'text',        
                                onChange: function(rec){
                                var url = CambiarCantidadPG(); }" style="width:85px;"/>
						  </td>
						  <td><input name="VolumenPG" type="text" id="VolumenPG" class="easyui-numberbox" data-options="min:0,prompt:'Volumen PG'" style="width:85px;" /></td>
						  <td>
							<label for="Inmediato2" onClick="CambiarRequerimientoPG()" >Sin Prueba de <br>Compatibilidad</label>
							<input type="radio" name="TipoReqPG" id="Inmediato2" value="Muy urgente" onClick="CambiarRequerimientoPG()" /></td>
						  <td id="PGclick">
							<label for="Urgente2" onClick="CambiarRequerimientoPG()" >< 1 hora</label>
							<input type="radio" name="TipoReqPG" id="Urgente2" value="Urgente" onClick="CambiarRequerimientoPG()" /></td>
						  <td><label for="Programado2">
						    <input name="FechaProgPG" type="text" id="FechaProgPG" class="easyui-datebox" style="width:100px;" value="" validType="validDate" readonly />
						    <input class="easyui-timespinner" value="" style="width:70px;" id="HoraProgPG" name="HoraProgPG" readonly />
						    </label>
                          <input type="radio" name="TipoReqPG" id="Programado2" value="Programado" onClick="CambiarRequerimientoPG()" /></td>
						</tr>
						<tr align="center">
						  <td><input name="CodigoPFC" type="text" id="CodigoPFC" value="PFC" style="width:65px;" readonly/></td>
						  <td>PLASMA FRESCO CONGELADO</td>
						  <td><input name="CantidadPFC" type="text" id="CantidadPFC" class="easyui-numberbox" data-options="prompt:'Cantidad PFC',
                                valueField: 'id',
                                textField: 'text',        
                                onChange: function(rec){
                                var url = CambiarCantidadPFC(); }" style="width:85px;"/>
						  </td>
						  <td><input name="VolumenPFC" type="text" id="VolumenPFC" class="easyui-numberbox" data-options="min:0,prompt:'Volumen PFC'" style="width:85px;" /></td>
						  <td>
							<label for="Inmediato4" onClick="CambiarRequerimientoPFC()" >Sin Prueba de <br>Compatibilidad</label>
							<input type="radio" name="TipoReqPFC" id="Inmediato4" value="Muy urgente" onClick="CambiarRequerimientoPFC()" /></td>
						  <td id="PFCclick">
							<label for="Urgente4" onClick="CambiarRequerimientoPFC()" >< 1 hora</label>
							<input type="radio" name="TipoReqPFC" id="Urgente4" value="Urgente" onClick="CambiarRequerimientoPFC()" /></td>
						  <td><label for="Programado4">
						    <input name="FechaProgPFC" type="text" id="FechaProgPFC" class="easyui-datebox" style="width:100px;" value="" validType="validDate" readonly />
						    <input class="easyui-timespinner" value="" style="width:70px;" id="HoraProgPFC" name="HoraProgPFC" readonly />
						    </label>
                          <input type="radio" name="TipoReqPFC" id="Programado4" value="Programado" onClick="CambiarRequerimientoPFC()" /></td>
					    </tr>
						<tr align="center">
						  <td><input name="CodigoGRlav" type="text" id="CodigoGRlav" value="PQAF" style="width:65px;" readonly/></td>
						  <td>PLAQUETAS POR AFERESIS</td>
						  <td><input name="CantidadGRlav" type="text" id="CantidadGRlav" class="easyui-numberbox" data-options="prompt:'Cantidad PQ',
                                valueField: 'id',
                                textField: 'text',        
                                onChange: function(rec){
                                var url = CambiarCantidadGRlav(); }" style="width:85px;"/>
						  </td>
						  <td><input name="VolumenGRlav" type="text" id="VolumenGRlav" class="easyui-numberbox" data-options="min:0,prompt:'Volumen PQ'" style="width:85px;" /></td>
						  <td>
							<label for="Inmediato3" onClick="CambiarRequerimientoGRlav()" >Sin Prueba de <br>Compatibilidad</label>
							<input type="radio" name="TipoReqGRlav" id="Inmediato3" value="Muy urgente" onClick="CambiarRequerimientoGRlav()" /></td>
						  <td id="GRlavclick">
							<label for="Urgente3" onClick="CambiarRequerimientoGRlav()" >< 1 hora</label>
							<input type="radio" name="TipoReqGRlav" id="Urgente3" value="Urgente" onClick="CambiarRequerimientoGRlav()" /></td>
						  <td><label for="Programado3">
						    <input name="FechaProgGRlav" type="text" id="FechaProgGRlav" class="easyui-datebox" style="width:100px;" value="" validType="validDate" readonly />
						    <input class="easyui-timespinner" value="" style="width:70px;" id="HoraProgGRlav" name="HoraProgGRlav" readonly />
						    </label>
                          <input type="radio" name="TipoReqGRlav" id="Programado3" value="Programado" onClick="CambiarRequerimientoGRlav()" /></td>
						</tr>
						<tr align="center">
						  <td><input name="CodigoPQ" type="text" id="CodigoPQ" value="PQ" style="width:65px;" readonly/></td>
						  <td>PLAQUETAS </td>
						  <td><input name="CantidadPQ" type="text" id="CantidadPQ" class="easyui-numberbox" data-options="prompt:'Cantidad PQ',
                                valueField: 'id',
                                textField: 'text',        
                                onChange: function(rec){
                                var url = CambiarCantidadPQ(); }" style="width:85px;"/>
						  </td>
						  <td><input name="VolumenPQ" type="text" id="VolumenPQ" class="easyui-numberbox" data-options="min:0,prompt:'Volumen PQ'" style="width:85px;" /></td>
						  <td>
							<label for="Inmediato5" onClick="CambiarRequerimientoPQ()" >Sin Prueba de <br>Compatibilidad</label>
							<input type="radio" name="TipoReqPQ" id="Inmediato5" value="Muy urgente" onClick="CambiarRequerimientoPQ()" /></td>
						  <td id="PQclick">
							<label for="Urgente5" onClick="CambiarRequerimientoPQ()" >< 1 hora</label>
							<input type="radio" name="TipoReqPQ" id="Urgente5" value="Urgente" onClick="CambiarRequerimientoPQ()" /></td>
						  <td><label for="Programado5">
						    <input name="FechaProgPQ" type="text" id="FechaProgPQ" class="easyui-datebox" style="width:100px;" value="" validType="validDate" readonly />
						    <input class="easyui-timespinner" value="" style="width:70px;" id="HoraProgPQ" name="HoraProgPQ" readonly />
						    </label>
                          <input type="radio" name="TipoReqPQ" id="Programado5" value="Programado" onClick="CambiarRequerimientoPQ()" /></td>
						</tr>
						<tr align="center">
						  <td><input name="CodigoCRIO" type="text" id="CodigoCRIO" value="CRIO" style="width:65px;" readonly/></td>
						  <td>CRIOPRECIPITADOS </td>
						  <td><input name="CantidadCRIO" type="text" id="CantidadCRIO" class="easyui-numberbox" data-options="prompt:'Cantidad CRIO',
                                valueField: 'id',
                                textField: 'text',        
                                onChange: function(rec){
                                var url = CambiarCantidadCRIO(); }" style="width:85px;"/>
						  </td>
						  <td><input name="VolumenCRIO" type="text" id="VolumenCRIO" class="easyui-numberbox" data-options="min:0,prompt:'Volumen CRIO'" style="width:85px;" /></td>
						  <td>
							<label for="Inmediato6" onClick="CambiarRequerimientoCRIO()" >Sin Prueba de <br>Compatibilidad</label>
							<input type="radio" name="TipoReqCRIO" id="Inmediato6" value="Muy urgente" onClick="CambiarRequerimientoCRIO()" /></td>
						  <td id="CRIOclick">
							<label for="Urgente6" onClick="CambiarRequerimientoCRIO()" >< 1 hora</label>
							<input type="radio" name="TipoReqCRIO" id="Urgente6" value="Urgente" onClick="CambiarRequerimientoCRIO()" /></td>
						  <td><label for="Programado6">
						    <input name="FechaProgCRIO" type="text" id="FechaProgCRIO" class="easyui-datebox" style="width:100px;" value="" validType="validDate" readonly />
						    <input class="easyui-timespinner" value="" style="width:70px;" id="HoraProgCRIO" name="HoraProgCRIO" readonly />
						    </label>
                          <input type="radio" name="TipoReqCRIO" id="Programado6" value="Programado" onClick="CambiarRequerimientoCRIO()" /></td>
						</tr>
					  </tbody>
				    </table></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>				  
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td width="327">&nbsp;</td>
				  </tr>
				  
				  </table>		 
					
				<!--</div>-->        
		      </form>
			
			</div>		
				
			
		</div>
		<!--Fin Fraccionamiento-->
		
		<!--Inicio Tamizaje-->		
        <div title="Registrar Resumen de Solicitud Transfusional" style="padding:0px" iconCls="icon-save">
			<!--<div id="tb4" style="padding:5px;height:auto">
				<div style="margin-bottom:5px">
					<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="nuevoTamizaje()">Nuevo</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editarTamizaje()" >Editar</a>					
					<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>  
				</div>    
			</div>-->
			<div id="Tamizaje" >
           	  <form id="fmResumen" name="fmResumen" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarResumenSolicitudSangre&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">                  
				  <!--<div id="p" class="easyui-panel" style="width:90%;height:auto;" title="Banco de Sangre: Registrar Solicitud de Sangre del Paciente" iconCls="icon-save" align="left">--> <!--collapsible="true"  -->  
	
				  <div id="tb" style="padding:5px;height:auto">
						<div style="margin-bottom:5px">				
							<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>  				        
							<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="GuardarResumen()">Guardar Resumen</a>
							<a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="Regresar();">Regresar</a>
						</div> 
					  
						<fieldset style="width:90%;">					
							<legend style="color:#03C"><strong>Busqueda:</strong></legend>
							<table width="90%" style="font-size:12px;">
							  <tr align="center">
								<th width="41%" bgcolor="#D6D6D6"><strong>Nro Historia Paciente</strong></th>
								<th width="59%" bgcolor="#D6D6D6"><strong>Cuenta del Paciente</strong></th>
							  </tr>
							  <tr align="center">
								<td><input class="easyui-numberbox" style="width:110px" id="NroHistoriaBusReg" name="NroHistoriaBusReg" data-options="prompt:'Nº Historia'" maxlength="12"> Digite y ENTER
								<td>
									<input class="easyui-textbox" style="width:200px" id="IdCuentaAtencionReg" name="IdCuentaAtencionReg"  data-options="prompt:'Cuenta del Paciente'" readonly />									
								</td> 
							  </tr>                         
							</table> 
						</fieldset> 								
				  </div>  
	 
			    <table width="90%">
				  <tr>
					  <td width="35">&nbsp;</td>
				    <td width="146">Nro Historia</td>
				    <td width="210"><input name="NroHistoriaClinica2" type="text" id="NroHistoriaClinica2" class="easyui-textbox" style="width:130px;" readonly />
				    <!--<input name="IdPaciente2" id="IdPaciente2" type="hidden" />--></td>
				    <td width="140">Apellidos y Nombres</td>
				    <td colspan="3"><input name="ApellidosNombres2" type="text" id="ApellidosNombres2" class="easyui-textbox" style="width:250px;" readonly /></td>
				  </tr>
				  <tr>
					  <td>&nbsp;</td>
				    <td>Nro Solicitud </td>
				    <td>
                        <input name="NroSolicitud" id="NroSolicitud" class="easyui-textbox"  style="width:130px;" readonly />
                        <input name="IdSolicitudSangre" id="IdSolicitudSangre" type="hidden" />
                    </td>
				    <td>Grupo Sanguineo </td>
				    <td><select class="easyui-combobox" name="IdGrupoSanguineoRec2" id="IdGrupoSanguineoRec2"  style="width: 130px" data-options="prompt:'Seleccione',required:true,
					valueField: 'id',
					textField: 'text',        
					onSelect: function(rec){
					var url = cambiarGrupoSanguineoRec(); }" readonly>
				      <option value="0"></option>
				      <?php
											  $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
											   if($listarSexo != NULL) { 
												 foreach($listarSexo as $item){?>
				      <option value="<?php echo $item["IdGrupoSanguineo"]?>" ><?php echo $item["Descripcion"]?></option>
				      <?php } } ?>
				      </select>
                    <!--<input name="GrupoSanguineoRec2" id="GrupoSanguineoRec2" type="hidden" />
                    <input name="GrupoSanguineoRecANT2" id="GrupoSanguineoRecANT2" type="hidden" />-->                    </td>
                    
				    <td colspan="2">Peso &nbsp;&nbsp;
			          <input name="PesoReceptor" id="PesoReceptor" class="easyui-numberbox" data-options="required:true,prompt:'Peso Paciente',min:0,precision:2" style="width:130px"  /></td>
			      </tr>
				  <tr>
				    <td colspan="7"><hr></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>Código Diagnóstico 1</td>
				    <td><input name="CodigoCIE2004" type="text" id="CodigoCIE2004" class="easyui-combogrid" data-options="required:true,prompt:'Busqueda por Código'" style="width:130px;" /></td>
				    <td> Nombre Diagnóstico 1</td>
				    <td colspan="3">
					  <input name="diagnostico" type="text" id="diagnostico" class="easyui-combogrid" data-options="required:true,prompt:'Busqueda por Descripcion'" style="width:250px;" />
					  <input name="IdDiagnostico" id="IdDiagnostico" type="hidden" />              
				    </td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>Código Diagnóstico 2</td>
				    <td><input name="CodigoCIE2" type="text" id="CodigoCIE2" class="easyui-combogrid" data-options="required:true,prompt:'Busqueda por Código'" style="width:130px;" /></td>
				    <td>Nombre Diagnóstico 2</td>
				    <td colspan="3"><input name="diagnostico2" type="text" id="diagnostico2" class="easyui-combogrid" data-options="required:true,prompt:'Busqueda por Descripcion'" style="width:250px;" />
                    <input name="IdDiagnostico2" id="IdDiagnostico2" type="hidden" /></td>
			      </tr>
				  <tr>
				    <td colspan="7"><hr></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td colspan="6"><table width="80%" border="0" cellpadding="0" cellspacing="0" class="">
				      <tr>
				        <td colspan="2"><strong>HEMATÍES</strong></td>
				        <td colspan="2"><strong>PLASMA</strong></td>
				        <td width="27%"><strong>PLAQUETAS</strong></td>
			          </tr>
				      <tr>
				        <td width="16%">Hemoglobina (gr/dl)</td>
				        <td width="21%"><input name="Hemoglobina" class="easyui-numberbox" id="Hemoglobina" style="width:130px" data-options="prompt:'Hemoglobina',min:0,max:100,precision:1"></td>
				        <td width="16%">T. Tromboplastina:</td>
				        <td width="20%"><input name="Tromboplastina" class="easyui-numberbox" id="Tromboplastina" style="width:100px" data-options="prompt:'T.Tromboplastina',min:0,precision:1"></td>
				        <td>&nbsp;</td>
			          </tr>
				      <tr>
				        <td>Hematocrito (%)</td>
				        <td><input name="Hematocrito" class="easyui-numberbox" id="Hematocrito" style="width:130px" data-options="prompt:'Hematocrito',min:0,precision:1"></td>
				        <td>T. Protrombina:</td>
				        <td><input name="Protrombina" class="easyui-numberbox" id="Protrombina" style="width:100px" data-options="prompt:'T.Protrombina',min:0,precision:1"></td>
				        <td>Plaquetas (mm3):
                        <input name="Plaquetas" class="easyui-numberbox" id="Plaquetas" style="width:100px" data-options="prompt:'Plaquetas',min:0,precision:1"></td>
			          </tr>
				      <tr>
				        <td>&nbsp;</td>
				        <td>&nbsp;</td>
				        <td>INR:</td>
				        <td><input name="INR" class="easyui-numberbox" id="INR" style="width:100px" data-options="prompt:'INR',min:0,precision:1"></td>
				        <td>&nbsp;</td>
			          </tr>
				      <tr>
				        <td>
                        <input type="radio" name="swAnemia1" id="Aguda" value="Aguda" />                         
                        <label for="Aguda">Anemia aguda </label></td>
				        <td><input type="radio" name="swAnemia2" id="riesgo" value="Leve"  />
                        <label for="riesgo" >Anemia Leve</label></td>
				        <td>&nbsp;</td>
				        <td>&nbsp;</td>
				        <td>&nbsp;</td>
			          </tr>
				      <tr>
				        <td>
                          <input type="radio" name="swAnemia1" id="Cronica" value="Crónica" />
                          <label for="Cronica" >Anemia crónica</label>
                        </td>
                          
				        <td><input type="radio" name="swAnemia2" id="AnemiaModerada" value="Moderada"  />
<label for="AnemiaModerada" >Anemia Moderada</label></td>
				        <td colspan="2">
                           <!--<input type="radio" name="swPlasma1" id="Coagulopatíaporhemorragia" value="Coagulopatía por hemorragia activa" onClick="CambiarPlasma()"/>
                           <label for="Coagulopatíaporhemorragia" onClick="CambiarPlasma()">Coagulopatía por hemorragia activa</label>-->
                        </td>
				        <td><!--<input type="radio" name="swPQ1" id="ProfilaxisTrombopenia" value="Profilaxis en Trombopenia" onClick="CambiarPQ()"/>
                        <label for="ProfilaxisTrombopenia" onClick="CambiarPQ()">Profilaxis en Trombopenia</label>--></td>
			          </tr>
                      
				      <tr>
				        <td>&nbsp;</td>
				        <td><input type="radio" name="swAnemia2" id="AnemiaSevera" value="Severa" />
                        <label for="AnemiaSevera" >Anemia Severa</label></td>
                        
				        <td colspan="2"><!--<input type="radio" name="swPlasma1" id="Coagulopatíasinhemorragia2" value="Coagulopatía sin hemorragia" onClick="CambiarPlasma()"/>
                        <label for="Coagulopatíasinhemorragia2" onClick="CambiarPlasma()">Coagulopatía sin hemorragia</label>--></td>
                        
				        <td><!--<input type="radio" name="swPQ1" id="TerapeuticaTrombopenia" value="Terapeutica en Trombopenia" onClick="CambiarPQ()"/>
                        <label for="TerapeuticaTrombopenia" onClick="CambiarPQ()">Terapeutica en Trombopenia</label>--></td>
			          </tr>
				      <tr>
				        <td colspan="2">&nbsp;</td>
                         
				        <td colspan="2">
                          <!--<input type="radio" name="swPlasma1" id="OtrosPlasma1" value="Otros" onClick="CambiarPlasma()"/>
                       	  <label for="OtrosPlasma1" onClick="CambiarPlasma()">Otros:</label>
                          <input name="OtroswPlasma1" id="OtroswPlasma1" class="easyui-textbox" style="width:180px" data-options="prompt:'Especifique'" readonly />-->
                        </td>
                        
				        <td>
                          <!--<input type="radio" name="swPQ1" id="OtrosPQ1" value="Otros" onClick="CambiarPQ()"/>
                       	  <label for="OtrosPQ1" onClick="CambiarPQ()">Otros:</label>
                          <input name="OtroswPQ1" id="OtroswPQ1" class="easyui-textbox" style="width:150px" data-options="prompt:'Especifique'" readonly />-->
                        </td>
			          </tr>
				      
			        </table></td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td width="150">&nbsp;</td>
				    <td width="122">&nbsp;</td>
				    <td width="297">&nbsp;</td>
				  </tr>
				  <tr>
				    <td colspan="7"><hr></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td id="TPclick">Transfusiones Previas</td>
				    <td>
					    <label for="SITP" >SI</label>
					    <input type="radio" name="swTransPre" id="SITP" value="SI"  />
					    <label for="NOTP" >NO</label>
					    <input type="radio" name="swTransPre" id="NOTP" value="NO"  />
					    <label for="DesconocidoTP" >Desconocido</label>
					    <input type="radio" name="swTransPre" id="DesconocidoTP" value="Desconocido" checked  />
				    </td>
				    <td colspan="4" id="RPclick">Reacción Transfunsional Previas &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					    <label for="SIRP" >SI</label>
					    <input type="radio" name="swReacTrans" id="SIRP" value="SI"  />
					    <label for="NORP" >NO</label>
					    <input type="radio" name="swReacTrans" id="NORP" value="NO"  />
					    <label for="DesconocidoRP" >Desconocido</label>
					    <input type="radio" name="swReacTrans" id="DesconocidoRP" value="Desconocido" checked  />
                    </td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td id="RAclick">Reacciones Alergicas</td>
				    <td>
                      <label for="SIRA" >SI</label>
                      <input type="radio" name="swReacAler" id="SIRA" value="SI"  />
                      <label for="NORA" >NO</label>
                      <input type="radio" name="swReacAler" id="NORA" value="NO"  />
                      <label for="DesconocidoRA" >Desconocido</label>
                      <input type="radio" name="swReacAler" id="DesconocidoRA" value="Desconocido" checked />
                    </td>
				    <td colspan="4">&nbsp;</td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td colspan="3">&nbsp;</td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>Embarazo Previo</td>
				    <td>
					  <label for="SITP2" >SI</label>
					  <input type="radio" name="EmbarazoPrevio" id="SITP2" value="SI" disabled />
					  <label for="NOTP2" >NO</label>
					  <input type="radio" name="EmbarazoPrevio" id="NOTP2" value="NO" checked />  
                      <label for="DesconocidoTP2" >Desconocido</label>
					  <input type="radio" name="EmbarazoPrevio" id="DesconocidoTP2" value="Desconocido" checked />
                    </td>
				    <td colspan="4">Aborto &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
					  <label for="SITP3" >SI</label>
					  <input type="radio" name="Aborto" id="SITP3" value="SI" disabled />
					  <label for="NOTP3" >NO</label>
					  <input type="radio" name="Aborto" id="NOTP3" value="NO" checked />
                      <label for="DesconocidoTP3" >Desconocido</label>
					  <input type="radio" name="Aborto" id="DesconocidoTP3" value="Desconocido" checked /></td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td colspan="6">Incompatibilidad Materno-Perinatal &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				      <label for="SITP4" >SI</label>
                      <input type="radio" name="IncompatibilidadMF" id="SITP4" value="SI" disabled />
                      <label for="NOTP4" >NO</label>
                    <input type="radio" name="IncompatibilidadMF" id="NOTP4" value="NO" checked /> 
                    <label for="DesconocidoTP4" >Desconocido</label>
                    <input type="radio" name="IncompatibilidadMF" id="DesconocidoTP4" value="Desconocido" checked /></td>
			      </tr>
				  <tr>             
				    <td colspan="7"><hr></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td colspan="2" id="consentimientoclick">Se obtuvo consentimiento Informado del Receptor:</td>
				    <td>
					    <label for="SI" onClick="CambiarMotivo()">SI</label>
					    <input type="radio" name="swConsentimiendoInformado" id="SI" value="SI" onClick="CambiarMotivo()" checked />
				    </td>
				    <td colspan="3">
					    <label for="NO" onClick="CambiarMotivo()">NO</label>
					    <input type="radio" name="swConsentimiendoInformado" id="NO" value="NO" onClick="CambiarMotivo()" />
					    <input name="MotivoNOConsentimiendo" type="text" id="MotivoNOConsentimiendo" class="easyui-textbox" data-options="prompt:'Motivo de NO consentimiento Informado'" style="width:400px;" readonly />
				    </td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td colspan="2">Médico Solicitante</td>
				    <td colspan="4"><Select style="width:250px" class="easyui-combobox" id="MedicoSolicitante" name="MedicoSolicitante" data-options="prompt:'Seleccione',required:true">
					  <option value=""></option>
					  <?php
											  //$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
											  //$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
											  $listar=ListarMedicos_M(); 
											   if($listar != NULL) { 
												 foreach($listar as $item){?>
					  <option value="<?php echo $item["IdEmpleado"]?>" <?php if(trim($item["IdEmpleado"])==trim($_GET['IdEmpleado'])){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
					  <?php } } ?>
				    </select></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td colspan="2">Personal quien toma la muestra</td>
				    <td colspan="4">
					  <Select style="width:250px" class="easyui-combobox" id="UsuTomoMuestraPaciente" name="UsuTomoMuestraPaciente" data-options="prompt:'Seleccione'">
					    <option value=""></option>
					    <?php
											  //$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
											  //$DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
											  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
											   if($listar != NULL) { 
												 foreach($listar as $item){?>
					    <option value="<?php echo $item["IdEmpleado"]?>" <?php if(trim($item["IdEmpleado"])==trim($_GET['IdEmpleado'])){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
					    <?php } } ?>
					  </select>  
				    </td>
				  </tr>
				  </table>		 
					
				<!--</div>-->        
		      </form>
			
			
			</div>	
			 
		</div>
		<!--Fin Tamizaje-->		
		
	</div>
    
   
     
   <script type="text/javascript"> 
   
	//Funciones AMBOS Tabs
   
	function Regresar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListaEspera&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
	}	 
	
	function ObtenergsPaciente(IdPaciente){				 
			 
		$('#tt').datagrid({
			title:'Grupo Sanguineo Paciente',
			width:300,
			//height:100,	
			url: "../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ObtenerGrupoSanguineoPaciente&IdPaciente="+IdPaciente,
			idField:'idOrden',
			textField:'ValorTexto',
			mode:'remote',
			fitColumns:true,										
			columns:[[							
				{field:'idOrden',title:'Orden',width:50},
				{field:'ValorTexto',title:'Valor',width:80},
				{field:'Fecha',title:'Fecha',width:50}
				//{field:'RealizaPrueba',title:'RealizaPrueba',width:80}
										
			]]													
		});
			
	}			

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>