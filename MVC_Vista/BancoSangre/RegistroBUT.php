<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lista de Transfusiones</title>
	<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>            
      
        
   <style>
		.icon-filter{
			background:url('../images/filter.png') no-repeat center center;
		}
		
		 	form{
                margin:0;
                padding:10px 30px;
            }
			
            .ftitle{
                font-size:14px;
                font-weight:bold;
                padding:5px 0;
                margin-bottom:10px;
                border-bottom:1px solid #ccc;
            }
            .fitem{
                margin-bottom:5px;
            }			
            .fitem label{
                display:inline-block;
                width:60px;	
				margin-left:10px;			
            }
            .fitem input{
                width:110px;				
            }
			
			.fitem2{
                margin-bottom:5px;
				/*margin-left:10px;*/
            }
			.fitem2 label{
                display:inline-block;
                width:120px;	
				margin-left:10px;			
            }
			.fitem2 input {	
				width:140px;		 
			}
			
			#dlg-VerRecepcion{
                margin:0;
                padding:10px 30px;
            }
			
			#dlg-VerRecepcion{
                margin:0;
                padding:10px 30px;
            }
			#dlg-VerDespacho{
                margin:0;
                padding:10px 30px;
            }
			
			.l-btn-icon {
				margin-top: -11px !important;
			}
	</style>       
    
    
<script type="text/javascript" >			
		
		$.extend($("#FechaRecBUT").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaRecBUT").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});					
		
		function regresar(){
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=OtrosRegistrosDonaciones&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
		}	
		
		function Buscar(){
			document.getElementById("form1").submit();
		}				
	
    </script>
</head>
<body>  
    
    <!--INICIO REGISTRAR DESPACHO -->
	<div id="dlg-RegresoBUT" class="easyui-dialog" style="width:500px;height:180px;"
			closed="true" buttons="#dlg-buttons"><!--buttons="#dlg-buttons"-->
		<!--<div class="ftitle">Datos del Equipo</div>-->
		<form id="fmRegresarBUT" name="fmRegresarBUT" method="post" enctype="multipart/form-data" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarRegresoBUT&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>">            
			   
		  <div class="fitem2">
                <input type="hidden" name="IdReserva" id="IdReserva">
                <input type="hidden" name="TSNCS" id="TSNCS">
				<label>Área Usuaria:</label>
				<select class="easyui-combobox" name="UsuSerUsuBUT" id="UsuSerUsuBUT"  style="width:200px" >
                      <option value="0">Seleccione</option>
                      <?php
                                  $listarEC=ListarServiciosHospitalM();
                                   if($listarEC != NULL) { 
                                     foreach($listarEC as $item){?>
                      <option value="<?php echo $item["IdServicio"]?>" ><?php echo trim(ucfirst(mb_strtolower($item["Nombre"]))).' | '.trim(mb_strtoupper($item["DescripcionTipoServicio"]));?></option>
                      <?php } } ?>
                </select>
		  </div>              
		  <div class="fitem2">
				<label>Fecha Recepción BUT:</label>  
				<input name="FechaRecBUT" id="FechaRecBUT" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:110px" data-options="required:true" />                <input class="easyui-timespinner" value="<?php echo date('H:i:s');?>" id="HoraRecBUT" name="HoraRecBUT" data-options="showSeconds:true,required:true" style="width:85px" />
				
		  </div>                 
				   
		</form>
    </div>
        
    <div id="dlg-buttons">		
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onClick="saveRecepcionBUT();" style="width:90px">Guardar</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg-RegresoBUT').dialog('close')" style="width:90px">Cancelar</a>
	</div><!--FIN -->   
	  
	
    
	    <div id="tb1" style="padding:5px;height:auto">        	
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="regresar();">Regresar</a>  
			
            <div>
				<?php /*?><form id="form1" name="form1" method="post" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarSolicitudes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado']; ?>">
				
					<strong> Hemocomponente:</strong> &nbsp;&nbsp;&nbsp;&nbsp;                
					<select name="TipoHemB" id="TipoHemB" class="easyui-combobox" data-options="required:true,
							valueField: 'id',
							textField: 'text',        
							onSelect: function(rec){
							var url = document.form1.submit(); }" style="width:60px;">
							<option value="T">Todos</option>
							<option value="PG">PG </option>
							<option value="PFC">PFC </option>
							<option value="PQ">PQ </option>
							<option value="CRIO">CRIO </option>
					</select>
					&nbsp;&nbsp;<strong>Fecha:</strong> &nbsp;
					<span class="fitem">
					<input name="FechaSolicitudB" id="FechaSolicitudB" class="easyui-datebox" value="<?php echo date('d/m/Y'); ?>" validType="validDate" style="width:105px" data-options="required:true" />
					</span>                          
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onclick="Buscar();">Buscar</a>
				
				</form> <?php */?> 
            </div>
            			
		</div>
        
                
       <table  class="easyui-datagrid" toolbar="#tb1" id="dg" title="Lista de Transfusiones" style="width:100%;height:90%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:20">           			
              
		<thead>
			<tr>
            	<th field="FechaDespacho" width="65">Fecha y Hora Despacho</th>
                <th field="UsuDespacho" width="55">UsuDespacho</th>
                <th field="TSNCS" width="55">Tipo y SNCS</th>               
                <th field="NroDonacion" width="55">NroDonacion</th>				
                <th field="NroTabuladora" width="55">NroTabuladora</th>	
                <th field="FechaRecBUT"  width="65">Fec y Hora Recepciona B.U.T</th>	
                <th field="DescripcionUsuSerUsuBUT" width="80">Area Usuaria</th>
                <th data-options="field:'RegresoBUT',formatter:RegresarBUT" align="center" width="40">Regresar BUT</th>
             </tr>
		</thead>
	</table> 
     
   <script> 
   
   //VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
	$.extend($.fn.validatebox.defaults.rules,{
		exists:{
			validator:function(value,param){
				var cc = $(param[0]);
				var v = cc.combobox('getValue');
				var rows = cc.combobox('getData');
				for(var i=0; i<rows.length; i++){
					if (rows[i].id == v){return true}
				}
				return false;
			},
			message:'El valor ingresado no existe.'
		}
	});	
	
	$(function(){	
		/*$('#IdGrupoSanguineoRec').combobox({	
		editable: false,
		required: true								
		});	*/
		$('#UsuSerUsuBUT').combobox({	
			valueField: 'id',
			textField: 'text',
			editable: true,
			required: true,    
			validType: 'exists["#UsuSerUsuBUT"]',
			filter: function (q, row) {
			return row.text.toUpperCase().indexOf(q.toUpperCase()) >= 0; 
			}								
		});						
		$('#UsuSerUsuBUT').combobox('validate');		
			
	});
   
   $("#dg").datagrid({
		// Fires when data in datagrid is loaded successfully
		onLoadSuccess:function(){	
			// Get this datagrid's panel object
			$(this).datagrid('getPanel')
			// for all easyui-linkbutton <a>'s make them a linkbutton
			.find('a.easyui-linkbutton').linkbutton();
		}
    });
   
   //Fecha Recepcion
	function RegresarBUT(val,row,index){
      //var url = "print.php?id="+ row.NroDonacion;	 	  
	  //return '<a href="'+url +'" class="easyui-linkbutton" iconCls="icon-add"></a>';	  
	  if(val.trim()=='NO'){		   
		    return '<a onclick=\'RegRegresarBUT("'+row.IdReserva+'","'+row.TSNCS+'",'+index+');\' class="easyui-linkbutton" iconCls="icon-add" plain="true"></a>';
	  }else{	  
		 return val;
	  } 	  
    }	
	
	
	function RegRegresarBUT(IdReserva,TSNCS,index){						 	
		$('#dlg-RegresoBUT').dialog('open').dialog('setTitle','Recepcion BUT '+TSNCS);		
		document.getElementById('IdReserva').value=IdReserva;				
		document.getElementById('TSNCS').value=TSNCS;		
		
	}		
	 
	 function saveRecepcionBUT(){
		 var UsuSerUsuBUT=$('#UsuSerUsuBUT').combobox('getValue');
			 if(UsuSerUsuBUT.trim()=="" || UsuSerUsuBUT.trim()=="0" || $('#UsuSerUsuBUT').combobox('isValid')==false){
				$.messager.alert('Mensaje','Seleccione el Área Usuaria','info');
				$('#UsuSerUsuBUT').next().find('input').focus();
				return 0;			
			}
			
			var FechaRecBUT=$('#FechaRecBUT').datebox('getText');
			 if(FechaRecBUT.trim()=="" || $('#FechaRecBUT').datebox('isValid')==false){
				$.messager.alert('Mensaje','Seleccione la Fecha de Recepción BUT','info');
				$('#FechaRecBUT').next().find('input').focus();
				return 0;			
			}
			
			var HoraRecBUT=$('#HoraRecBUT').timespinner('getText');
			 if(HoraRecBUT.trim()==""){
				$.messager.alert('Mensaje','Ingrese la Hora de Recepción BUT','info');
				$('#HoraRecBUT').next().find('input').focus();
				return 0;			
			}
			
			var TSNCS=document.getElementById('TSNCS').value;
			
			$.messager.confirm('Mensaje', '¿Seguro de Registrar regreso de la Bolsa '+ TSNCS +'?', function(r){
				if (r){
					document.getElementById("fmRegresarBUT").submit();
				}
			});	
		 
	 }	
	  
		function getData(){
			var rows = [];			
			
	<?php 		
	 
	 $ListarSolicitudesPend = ListarSolicitudesPendientesBUT();
	 
     $i=1; $j=0;											
	if($ListarSolicitudesPend!=NULL){		
		foreach($ListarSolicitudesPend as $item)
		{						
			$FechaRecBUT=isset($item['FechaRecBUT']) ? (vfecha(substr($item['FechaRecBUT'],0,10)).' '.substr($item['FechaRecBUT'],11,8)) : '';
			if($item['RegresoBUT']==0){	
				$RegresoBUT='NO';
			}else{	
				$RegresoBUT='SI';	
			}
			
			$FechaDespacho=isset($item['FechaDespacho']) ? (vfecha(substr($item['FechaDespacho'],0,10)).' '.substr($item['FechaDespacho'],11,8)) : '';
			?>
   				
				
			//for(var i=1; i<=800; i++){
				//var amount = Math.floor(Math.random()*1000);
				//var price = Math.floor(Math.random()*1000);
				rows.push({
					TSNCS: '<?php echo $item["TipoHem"].'-'.$item["SNCS"];?>',
					NroDonacion: '<?php echo $item["NroDonacion"];?>',
					NroTabuladora: '<?php echo $item["NroTabuladora"]; ?>',											
					FechaRecBUT:'<?php echo  $FechaRecBUT; ?>',	
					UsuSerUsuBUT: '<?php echo $item["UsuSerUsuBUT"]; ?>',
					DescripcionUsuSerUsuBUT: '<?php echo $item["DescripcionUsuSerUsuBUT"]; ?>',	
					RegresoBUT:'<?php echo  $RegresoBUT; ?>',
					IdReserva: '<?php echo $item["IdReserva"]; ?>',		
					FechaDespacho: '<?php echo $FechaDespacho; ?>',
					UsuDespacho: '<?php echo $item["UsuDespacho"]; ?>',				
										
				});
			//}
			<?php  $i += 1;	
		}
	}
?>
		return rows;
		}
				
		
		function ObtenerFechaActual(){
			f=new Date();
			var y = parseInt(f.getFullYear());
			var m = parseInt(f.getMonth()+1);
			var d = parseInt(f.getDate());
			if(m<10){
				m='0'+m;
			}
			if(d<10){
				d='0'+d;
			}						
			FechaActual=d+'/'+m+'/'+y;
				
			return FechaActual;
		}
			
		$(function(){
		var dgFrac = $('#dg').datagrid({
			remoteFilter: false,
			pagination: true,
			pageSize: 20,
			pageList: [10,20,50,100]
		});
		
		dgFrac.datagrid('enableFilter');
		FilterFechaActualSolicitud();												
		dgFrac.datagrid('loadData', getData());	
		
		function FilterFechaActualSolicitud(){
			FechaActual=ObtenerFechaActual();
								
			dgFrac.datagrid('addFilterRule', {
				field: 'FechaDespacho',
				type:'datebox',
				op: 'contains',
				value: FechaActual
			});
		}//fin function FilterFechaActualSolicitud
		
		});
		</script>       
     
</body>
</html>