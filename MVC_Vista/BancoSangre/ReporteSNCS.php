<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	
	$FechaReg_Inicio=isset($_REQUEST['FechaReg_Inicio']) ? $_REQUEST['FechaReg_Inicio'] : '';	
	$FechaReg_Final=isset($_REQUEST['FechaReg_Final']) ? $_REQUEST['FechaReg_Final'] : date('d/m/Y');
	
	$RealizaNAT=isset($_REQUEST['RealizaNAT']) ? $_REQUEST['RealizaNAT'] : '';	
	$NroDonacionBus=isset($_REQUEST["NroDonacionBus"]) ? $_REQUEST["NroDonacionBus"] : '';	
	
	$FechaActual = vfecha(substr($FechaServidor,0,10));
	$HoraActual = substr($FechaServidor,11,8);
	
	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_REQUEST['IdEmpleado']);
	$Usuario=$ListarUsuarioxIdempleado[0]['Usuario'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HOSPITAL NACIONAL DANIEL ALCIDES CARRION - SERVICIO DE HEMOTERAPIA Y BANCO DE SANGRE</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Fecha --> 
    <link rel="stylesheet" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css" />     
    
<!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/tables/site.js"></script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async></script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<script type="text/javascript" class="init">
    $(document).ready(function () {
		
		

		// Append a caption to the table before the DataTables initialisation
    	var FechaReg_Inicio=document.getElementById('FechaReg_Inicio').value;
		var FechaReg_Final=document.getElementById('FechaReg_Final').value;
 
 		//$('#dataTables-example').append("<td class='consultas'>No se encontraron registros!!</td>");
        $('#dataTables-example').DataTable({
            dom: 'Bfrtip',
			 lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar Todo' ]
        ],
       
            buttons: [ 'pageLength',
				//'colvis',
                <!--'copyHtml5','csvHtml5','excelHtml5',-->
				<!--'copy', 'csv', 'excel','print',-->	
				{ //1
					extend: 'copyHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //2
					extend: 'csvHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //3
					extend: 'excelHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //4
					extend: 'print',
					exportOptions: {columns: ':visible'}
				},
								
                {//valores por defecto orientation:'portrait' and pageSize:'A4',
					
                    extend: 'pdfHtml5', header: true,	 sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",		
					//message: '\t\t\t\t\t\t\t\t\t\t\t\t\t\t Fecha de Tamizaje: '+FechaReg_Inicio+' \t'+ FechaReg_Final +' \n \t\t\t\t\t\t\t\t\t\t\t\t\t\t',//Turno: \t Mañana (8am - 2pm)
	customize: function ( doc ) {
	var cols = [];
   cols[0] = {text: '-------------------------\nJefe de Servicio', alignment: 'center', fontSize: '7', margin:[40,15,0,1] }; //left	('derecha,arriba,izquierda,abajo')
   cols[1] = {text: '-----------------------------------\nResponsable de Etiquetado y Liberación', alignment: 'center', fontSize: '7', margin:[40,15,40,1]}; //right	('derecha,arriba,izquierda,abajo')
   //cols[2] = {text: 'Bfrtip', alignment: 'Top', margin:[0,0] };
   var objFooter = {};
   objFooter['columns'] = cols;
   doc['footer']=objFooter;  
   
       doc.content.splice(1, 0, {
          text: [{
            text: 'Fecha y Hora de Impresion: '+'<?php echo $FechaActual.' '.$HoraActual ?>' + '\n',
            bold: true,
            fontSize: 8, alignment: 'right'
          }, {
            text: ' Usuario: '+'<?php echo $Usuario ?>' + '\n',
            bold: true,
            fontSize: 8, alignment: 'right'
          }, {
            text: 'Fecha de Tamizaje: '+FechaReg_Inicio+' \t'+ FechaReg_Final + '\n',
            bold: true,
            fontSize: 12, alignment: 'center'
          }/*, {
            text: 'Turno:',
            bold: true,
            fontSize: 12, alignment: 'center'
          }*/],
          margin: [0, 0, 0, 12]//,alignment: 'center'
        });
      
    }   ,
					  					
					                 
                    orientation: 'landscape',
                    pageSize: 'A4',
					exportOptions: {columns: ':visible'}
                } //,				
				
				
            ]
        });
    });
</script> 
    
    <script type="text/javascript">	
		   
		   function buscar(){
			   var FechaReg_Inicio=document.getElementById('FechaReg_Inicio').value;
			   var FechaReg_Final=document.getElementById('FechaReg_Final').value;
			   if((FechaReg_Inicio=="") || (FechaReg_Final=="")){
				   alertify.error('Debe ingresar ambas fechas');
			   }else{
				   $('#form1').submit();	
			   }
			}
			
			function cancelar(){
				window.location = "../BancoSangre/BancoSangreC.php?acc=ReporteSNCS&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>";
			}
           
		   function Archivar(){
			   var FechaReg_Inicio=document.getElementById('FechaReg_Inicio').value;
			   var FechaReg_Final=document.getElementById('FechaReg_Final').value;
			   var RealizaNAT=document.getElementById('RealizaNAT').value;
			   
			   var TodosNroDonacion=document.getElementById('TodosNroDonacionConSNCS').value;
			   var maxitem=document.getElementById('maxitem').value;
			   
			   var NroDonacionBus=document.getElementById('NroDonacionBus').value;
			   
			   if(maxitem==0){				 
				 alertify.error('Busque Datos a Archivar');
				 return 0;
				 
			   }else if(NroDonacionBus!=''){
				 var TodosNroDonacion=document.getElementById('NroDonacionBus').value;  
				 var Mensaje="¿Seguro de Archivar los Tamizajes del "+FechaReg_Inicio+" al "+FechaReg_Final+" y NroDonacion="+NroDonacionBus+" ?"; 
				   
			   }else if(RealizaNAT==''){
				 var Mensaje="¿Seguro de Archivar los Tamizajes del "+FechaReg_Inicio+" al "+FechaReg_Final+"?";  
			   }else if(RealizaNAT=='1'){
				 var Mensaje="¿Seguro de Archivar los Tamizajes del "+FechaReg_Inicio+" al "+FechaReg_Final+" que tienen NAT?";   
			   }else{
				 var Mensaje="¿Seguro de Archivar los Tamizajes del "+FechaReg_Inicio+" al "+FechaReg_Final+" que NO tienen NAT?";   
			   }			     		
					alertify.confirm(Mensaje, function (e) {
						if (e) {
							//alertify.success("Verificación Quirúrgica eliminada correctamente");
							//Redireccionamos si das a aceptar
					 		window.location.href = "../BancoSangre/BancoSangreC.php?acc=ArchivarTamizaje&FechaReg_Inicio="+FechaReg_Inicio+"&FechaReg_Final="+FechaReg_Final+"&RealizaNAT="+RealizaNAT+"&TodosNroDonacion="+TodosNroDonacion+"&NroDonacionBus="+NroDonacionBus+"&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>";    //página web a la que te redirecciona si confirmas la eliminación
						} else {
							//alertify.alert("Successful AJAX after Cancel");
							alertify.error('Cancelado')
						}
					});			
		   }
		   
   </script> 
   
   <script>
    $(function () {

//Array para dar formato en español
        $.datepicker.regional['es'] =
                {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        $.datepicker.setDefaults($.datepicker.regional['es']);

//miDate: fecha de comienzo D=días | M=mes | Y=año
//maxDate: fecha tope D=días | M=mes | Y=año
        //var f = new Date();
        //diaactual=f.getDate();
        // $( "#Ipd_frecibido").datepicker({ minDate: "-"+diaactual+"D", maxDate: "+1M +10D" });    
        //$( "#NT_FGUI" ).datepicker({ minDate: "-1M", maxDate: "+1M +10D" }); 
        $("#FechaReg_Inicio").datepicker();
        $("#FechaReg_Final").datepicker();

    });	
	
	
</script>

</head>

<body>


    <div id="wrapper">
        <div id="page-wrapper">            
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary"> <!--panel-default-->
                        <div class="panel-heading"> REPORTE DE ETIQUETADO "SNCS" y ARCHIVO DEFINITIVO </div>
                        <div class="panel-body">
                        <form action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteSNCS&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>" name="form1" id="form1" method="post">                        
                        	 <div class="" align="right">
                                <!--<a class="btn btn-success" onClick="validarguardar();" href="#">Registrar</a>-->
                                <input class="btn btn-success" type="button" onclick="buscar()" value="Buscar"/>
                                &nbsp;<a class="btn btn-danger" onClick="cancelar();">Cancelar</a>
                                &nbsp;<input class="btn btn-warning" type="button" onclick="Archivar()" value="Archivar Definitivo"/>&nbsp;
                                <!--&nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                              </div>
                                                 
                            <!--fila 1-->
                            <div class="form-group row">
                                <label class="control-label col-xs-1"></label>
                            </div>                            
                            <div class="form-group row">                                               
                                <label class="control-label col-xs-2">Fecha Inicio Ing. Resultados</label>
                                <div class="col-xs-1">
                                    <input type="text" id="FechaReg_Inicio" name="FechaReg_Inicio" class="form-control datepicker input-sm" placeholder="Fecha Registro Inicial" value="<?php echo $FechaReg_Inicio ?>" onChange="buscar()"/>    
                                </div>
                                <label class="control-label col-xs-2">Fecha Fin Ing. Resultados</label>
                                <div class="col-xs-1">
                                    <input type="text" id="FechaReg_Final" name="FechaReg_Final" class="form-control datepicker input-sm" placeholder="Fecha Registro Final" value="<?php echo $FechaReg_Final ?>" onChange="buscar()"/>    
                                </div>
                                <label class="control-label col-xs-1">Realizó NAT</label>
                                <div class="col-xs-1">
                                	<select id="RealizaNAT" name="RealizaNAT" class="form-control input-sm" onChange="buscar()">
                                    <option value="" <?php if($RealizaNAT==''){?> selected <?php } ?> >TODOS</option>
                                    <option value="1" <?php if($RealizaNAT=='1'){?> selected <?php } ?> >SI</option>
                                    <option value="0" <?php if($RealizaNAT=='0'){?> selected <?php } ?> >NO</option>
                                    
                                    </select>
                                        
                                </div>
                                
                                <label class="control-label col-xs-1">Nro Donación</label>
                                <div class="col-xs-1">                                	
                                    <input type="text" id="NroDonacionBus" name="NroDonacionBus" class="form-control input-sm" placeholder="Nro Donacion" value="<?php echo $NroDonacionBus ?>" onChange="buscar()"/>   
                                
                                        
                                </div>
                                
                        </div><!--fin fila 1-->
                        
                                          
                        </form>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            
                               <?php  							
									$FechaReg_Inicio=$_REQUEST["FechaReg_Inicio"];
									if($FechaReg_Inicio!=NULL){
										$FechaReg_Inicio=gfecha($_REQUEST["FechaReg_Inicio"]);
									}
									$FechaReg_Final=$_REQUEST["FechaReg_Final"];
									if($FechaReg_Final!=NULL){
										$FechaReg_Final=gfecha($_REQUEST["FechaReg_Final"]);
									}										
									 
								?>
                            
                                <table width="100%" class="table table-bordered" id="dataTables-example"> <!--table-hover-->
                                	<thead>
                                        <tr class="text-center">
                                            <th rowspan="2">&nbsp;</th>
                                            <th rowspan="2">#Donación</th>
                                            <th rowspan="2">GS</th>
                                            <th rowspan="2">Fecha Extraccion</th>
                                            <th rowspan="2">Fecha Etiquetado y Liberación</th>
                                            <th rowspan="2">Tubuladura</th>
                                            <th colspan="3">Sello Nacional de Calidad de Sangre (SNCS)</th>
                                            <th rowspan="2" align="center">Observación</th>                                          
                                         </tr>                                    
                                        <tr class="text-center">
                                            <th>PG</th>
                                            <th>PFC</th>
                                            <th>PQ</th>                                             
                                         </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
									  		$i=0;								
											$numactivos=0; $numentrada=0;$numpausa=0;$numsalida=0; $todosId="";
											$ReporteSNCS=ReporteSNCS_M($FechaReg_Inicio,$FechaReg_Final,$RealizaNAT,$NroDonacionBus);//PRUEBA (FALTA CREAR COSULTA)																					
																					
											$NroDonacionConSNCS='';$TodosNroDonacionConSNCS='';
																						
											if($ReporteSNCS!=NULL){
									  		foreach($ReporteSNCS as $itemSNCS){
												$i++; 
									  			$EstadoAptoTamizaje=$itemSNCS["EstadoAptoTamizaje"];
												if($EstadoAptoTamizaje=='1'){
													$DesEstadoAptoTamizaje='<font color="#00FF00">NO REACTIVO</font>';
													$colorestado="#FFF";
													$Observacion='';
												}else if($EstadoAptoTamizaje=='0'){
													$EstadoAptoTamizaje='<font color="#FF0000">REACTIVO</font>';
													$colorestado="#FFAAAA";
													
													$PRquimioVIH=($itemSNCS["quimioVIH"]!='0') ? 'VIH '.round($itemSNCS["PRquimioVIH"],2) : '';					
													$PRquimioSifilis=($itemSNCS["quimioSifilis"]!='0') ? 'Sifilis '.round($itemSNCS["PRquimioSifilis"],2) : '';													
													$PRquimioHTLV=($itemSNCS["quimioHTLV"]!='0') ? 'HTLV '.round($itemSNCS["PRquimioHTLV"],2) : '';														
													$PRquimioANTICHAGAS=($itemSNCS["quimioANTICHAGAS"]!='0') ? 'Antichagas '.round($itemSNCS["PRquimioANTICHAGAS"],2) : '';														
													$PRquimioHBS=($itemSNCS["quimioHBS"]!='0') ? 'HBS '.round($itemSNCS["PRquimioHBS"],2) : '';														
													$PRquimioVHB=($itemSNCS["quimioVHB"]!='0') ? 'VHB '.round($itemSNCS["PRquimioVHB"],2) : '';														
													$PRquimioVHC=($itemSNCS["quimioVHC"]!='0') ? 'VHC '.round($itemSNCS["PRquimioVHC"],2) : '';														
													$Observacion=$PRquimioVIH.' '.$PRquimioSifilis.' '.$PRquimioHTLV.' '.$PRquimioANTICHAGAS.' '.$PRquimioHBS.' '.$PRquimioVHB.' '.$PRquimioVHC;
													
												}else if($EstadoAptoTamizaje==''){
													$EstadoAptoTamizaje='<font color="#0000FF">FALTA</font>';
													$colorestado="#d3f1f8";
													$Observacion='F - '.mb_strtolower($itemSNCS["DesMotivoElimMuestra"]);
												}
																																		
												
												//SNCS	
												$IdTipoDonacion=$itemSNCS["IdTipoDonacion"];								
												$SNCS=$itemSNCS["SNCS"];
												
												if(trim($IdTipoDonacion)=='1'){ //SÓLO SANGRE TOTAL
													if(trim($itemSNCS["DesMotivoElimPaqueteGlobu"])!=''){ //PG
														$SNCSPG='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPaqueteGlobu"]).'</font>';
													}else if($EstadoAptoTamizaje=='1'){														
														if($SNCS!=NULL){
															$SNCSPG='PG-'.$SNCS;
															$NroDonacionConSNCS=$itemSNCS["NroDonacion"];
														}else{
															$SNCSPG='PG-#SNCS';
														}
													}else{
														$SNCSPG='';
													}
													
													if(trim($itemSNCS["DesMotivoElimPlasma"])!=''){ //PFC													
														$SNCSPFC='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPlasma"]).'</font>';														
													}else if($EstadoAptoTamizaje=='1'){														
														if($SNCS!=NULL){
															$SNCSPFC='PFC-'.$SNCS;
															$NroDonacionConSNCS=$itemSNCS["NroDonacion"];
														}else{
															$SNCSPFC='PFC-#SNCS';
														}
													}else{
														$SNCSPFC='';
													}
																									
												}else{ //SÓLO AFERESIS
													$SNCSPG='';
													$SNCSPFC='';
												}											
												
												//TODO (SANGRE TOTAL Y AFERESIS) 
												if(trim($itemSNCS["DesMotivoElimPlaquetas"])!=''){ //PQ
													$SNCSPQ='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPlaquetas"]).'</font>';
												}else if($EstadoAptoTamizaje=='1'){													
													if($SNCS!=NULL){
														$SNCSPQ='PQ-'.$SNCS;
														$NroDonacionConSNCS=$itemSNCS["NroDonacion"];
													}else{
														$SNCSPQ='PQ-#SNCS';
													}
												}else{
													$SNCSPQ='';
												}
												
																																			
												if($NroDonacionConSNCS!=''){													
													$TodosNroDonacionConSNCS=$TodosNroDonacionConSNCS.$NroDonacionConSNCS.',';
												}else{													
													$TodosNroDonacionConSNCS='';
												}									
												
												
									  ?>                                                                     						
 
                                      <tr  bgcolor="<?php echo $colorestado; ?>" class="text-center">
                                            <td align="center"><?php echo $i;?></td>
                                            <td align="center"><?php echo 'DAC'.$itemSNCS["NroDonacion"];?></td>
                                            <td align="center"><?php echo $itemSNCS["GrupoSanguineoPostulante"];?></td>
                                            <td align="center"><?php echo vfecha(substr($itemSNCS["FechaExtraccion"],0,10));?></td>
                                            <td align="center"><?php echo vfecha(substr($itemSNCS["FechaTamizaje"],0,10));?></td>
                                            <td align="center"><?php echo $itemSNCS["NroTabuladora"];?></td>
                                            <td align="center"><?php echo $SNCSPG;?></td>
                                            <td align="center"><?php echo $SNCSPFC;?></td>
                                            <td align="center"><?php echo $SNCSPQ;?></td>
                                            <td align="center">
												<?php echo $Observacion;?>                                            	 
                                            </td>                                       
                                                                                    
                                        </tr> 
                                        <?php 
											}
										 } 
										?>                                                                                  
                                    </tbody>
                                    		 <input type="hidden" id="TodosNroDonacionConSNCS" name="TodosNroDonacionConSNCS" value="<?php echo $TodosNroDonacionConSNCS;?>" />
                                             <input type="hidden" id="maxitem" name="maxitem" value="<?php echo $i;?>" />
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">
                                <h4>COMO REALIZAR BÚSQUEDAS DIRECTAS </h4>
                                <p>Búsqueda de pacientes por Nro. Historia clínica, DNI, Apellidos y Nombres </p><p>
<strong>Eje: Nro. Historia. 162578,  Eje: DNI. 43623262, Eje: Apellidos y Nombres. Ariza Bravo</strong>
</p>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - alertify - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
    
    <!-- Fecha -->     
    <script src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>       
	

</body>

</html>
