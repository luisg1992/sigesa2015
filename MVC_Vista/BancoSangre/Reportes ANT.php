<html>
<head>
	<meta charset="UTF-8">
	<title>Reportes General Administrador</title>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css"> 
         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
			function Limpiar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RecepcionPostulante&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
			
			function RegTamizaje(){				
				/*location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegEvaluacionPredonacionPostulante&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";*/ 
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					if(rowp.EstadoApto=='1'){
						$.messager.confirm('Mensaje', 'Aún NO se registra la Extracción. ¿Seguro de Registrar el Tamizaje de Sangre del Donante?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegTamizaje&NroDonacion="+rowp.NroDonacion+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";	
						}
					});	
					
					}else{
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegTamizaje&NroDonacion="+rowp.NroDonacion+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
					}
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar un Postulante','warning');						
				}
			}
			
			$.extend( $("#fecha_inicio").datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($("#fecha_inicio").datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#fecha_inicio" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    });
			
			$.extend( $("#fecha_fin").datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($("#fecha_fin").datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#fecha_fin" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    });
			
			function exportarexcel1(){
				document.form1.submit();			
			} 
		</script>            
        
</head>    
        
<body>

    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteExcel1&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:90%;;height:auto;"title="Banco de Sangre: REPORTES DEL SISTEMA BANCO DE SANGRE" iconCls="icon-print" align="left"> 	
              
       <table width="621">
            <tr>
              <td>&nbsp;</td>
              <td align="right">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td  width="20">&nbsp;</td>
              <td  width="353" align="right"><strong>Fecha Inicio Recepción: 
                <input type="text" id="fecha_inicio" name="fecha_inicio" class="easyui-datebox" data-options="prompt:'Fecha Inicio'" validType="validDate" value="<?php echo '01/'.$Mes.'/'.$Anio; ?>" style="width:150px;" />
              </strong></td>
              <td  width="232">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="right"><strong>Fecha Final Recepción:
                <input type="text" id="fecha_fin" name="fecha_fin" class="easyui-datebox" data-options="prompt:'Fecha Fin'" validType="validDate" value="<?php echo getUltimoDiaMes($Anio,$Mes).'/'.$Mes.'/'.$Anio; ?>" style="width:150px;" />
              </strong></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr align="center">
              <td>&nbsp;</td>
              <td><strong>Tipo Reporte</strong></td>
              <td><strong>Exportar Excel</strong></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>1. Datos del Donante/ Postulante (Reporte General)</td>
              <td>
              <div class="buttons" align="center">
              <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" onClick="exportarexcel1();">Exportar Excel</button> 
              </div>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><!--2. Datos del Receptores (Pacientes)--></td>
              <td>&nbsp;</td>
            </tr>
        </table>              
     	<br> 	
     </div>
</form>
      
 
  
      
</body>
</html>