<html>
<head>
	<meta charset="UTF-8">
	<title>Reportes General Administrador</title>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css"> 
         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
			function Limpiar(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RecepcionPostulante&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}			
			
			$.extend( $("#fecha_inicio").datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($("#fecha_inicio").datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#fecha_inicio" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    });
			
			$.extend( $("#fecha_fin").datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($("#fecha_fin").datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							//$("#fecha_fin" ).datebox('setValue', '');							
							return false;
						}
					},  
					message: 'Porfavor Seleccione una fecha valida.'  
				}
		    });
			
			function exportarexcel1(){
				document.form1.submit();			
			} 
			
			function exportarexcel2(){	
				var fecha_inicio2=$('#fecha_inicio2').datebox('getText');	
				var fecha_fin2=$('#fecha_fin2').datebox('getText');			
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteExcel2&fecha_inicio2="+fecha_inicio2+"&fecha_fin2="+fecha_fin2+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";			
			} 
		</script>            
        
</head>    
        
<body>

    <form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteExcel1&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">  
        
    <div id="p" class="easyui-panel" style="width:95%;;height:auto;"title="Banco de Sangre: REPORTES DEL SISTEMA BANCO DE SANGRE" iconCls="icon-print" align="left"> 	
              
       <table width="870">
            <tr>
              <td width="1">&nbsp;</td>
              <td width="236">&nbsp;</td>
              <td colspan="4">&nbsp;</td>
            </tr>
            <tr align="center">
              <td>&nbsp;</td>
              <td><strong>Tipo Reporte</strong></td>
              <td width="150"><strong>Busqueda por:</strong></td>
              <td width="150"><strong>Fec.Inicio</strong></td>
              <td width="150"><strong>Fec.Fin</strong></td>
              <td width="143"><strong>Exportar Excel</strong></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>1. Datos del Donante/ Postulante<br></td>
              <td align="center">Fecha Recepción</td>
              <td><strong>
                <input type="text" id="fecha_inicio" name="fecha_inicio" class="easyui-datebox" data-options="prompt:'Fecha Inicio'" validType="validDate" value="<?php echo date('d/m/Y'); ?>" style="width:150px;" />
              </strong>
              
              </td>
              <td><strong>
                <input type="text" id="fecha_fin" name="fecha_fin" class="easyui-datebox" data-options="prompt:'Fecha Fin'" validType="validDate" value="<?php echo date('d/m/Y'); ?>" style="width:150px;" />
              </strong></td>
              <td><div class="buttons" align="center">
              <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" onClick="exportarexcel1();">Exportar Excel</button> 
              </div></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>2. Datos de Transfusiones</td>
              <td align="center">Fecha Solicitud</td>
              <td><strong>
                <input type="text" id="fecha_inicio2" name="fecha_inicio2" class="easyui-datebox" data-options="prompt:'Fecha Inicio'" validType="validDate" value="<?php echo date('d/m/Y'); ?>" style="width:150px;" />
              </strong></td>
              <td><strong>
                <input type="text" id="fecha_fin2" name="fecha_fin2" class="easyui-datebox" data-options="prompt:'Fecha Fin'" validType="validDate" value="<?php echo date('d/m/Y'); ?>" style="width:150px;" />
              </strong></td>
              <td><div class="buttons" align="center">
              <button type="button"  name="save2" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" onClick="exportarexcel2();">Exportar Excel</button> 
              </div></td>
            </tr>
        </table>              
     	<br> 	
     </div>
</form>
      
 
  
      
</body>
</html>