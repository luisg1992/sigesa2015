<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	
	/*$TieneSNCS=isset($_REQUEST['TieneSNCS']) ? $_REQUEST['TieneSNCS'] : '9';

    //$lcFiltroComun
    $FechaReg_Inicio=isset($_REQUEST['FechaReg_Inicio']) ? $_REQUEST['FechaReg_Inicio'] : '';   
    $FechaReg_Final=isset($_REQUEST['FechaReg_Final']) ? $_REQUEST['FechaReg_Final'] : date('d/m/Y');    
    $RealizaNAT=isset($_REQUEST['RealizaNAT']) ? $_REQUEST['RealizaNAT'] : '9';  
    $TipoHemocomponente=isset($_REQUEST['TipoHemocomponente']) ? $_REQUEST['TipoHemocomponente'] : ''; 

	$FechaActual = vfecha(substr($FechaServidor,0,10));
	$HoraActual = substr($FechaServidor,11,8);	
	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_REQUEST['IdEmpleado']);
	$Usuario=$ListarUsuarioxIdempleado[0]['Usuario'];*/
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HOSPITAL NACIONAL DANIEL ALCIDES CARRION - SERVICIO DE HEMOTERAPIA Y BANCO DE SANGRE</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Fecha --> 
    <link rel="stylesheet" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css" />     
    
<!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/tables/site.js"></script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async></script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<style type="text/css">
	.rotar {	
    /*transform: rotate(-20deg);*/
	}
</style>

<script type="text/javascript">
	function VerDetalle(TipoHem,IdGrupoSanguineo,Estado,Vencido){	
			
			 $('#my_modalReg').modal('show');
			 $('#tablaDetalle').load("../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerDetalleStock&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>",{TipoHem:TipoHem,IdGrupoSanguineo:IdGrupoSanguineo,Estado:Estado,Vencido:Vencido});
				
	}	
</script>

</head>

<body>

	<!--reg verificacion -->
<div class="modal fade" id="my_modalReg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg">       

    <!-- Modal content-->
    <div class="modal-content">
	
	 <form action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=LiberarCuarentena&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>" name="FrmLiberar" id="FrmLiberar" method="post">                     
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h5 class="modal-title"><strong>DETALLE DE HEMOCOMPONENTES</strong></h5>
		</div>
		<!--<div class="alert alert-warning">
			<input name="mensaje" id="mensaje" type="text" style="background-color: #fcf8e3;
			border: 0px solid #fcf8e3;width:100%;" readonly />       
		</div>-->
		<div class="modal-body"> 
        	<div id="tablaDetalle"></div>		 
			<!--<table class="table">
				<tr align="center">          	
					<td><strong>Grupo Sanguineo</strong></td>
					<td><strong>SNCS</strong></td>
				</tr>
				<tr align="center">          	
					<td><div id="gs"></div></td>
					<td><div id="SNCS"></div></td>
				</tr>          
			</table> -->	
            								
		</div>     
        <!--<div class="alert alert-warning">
			<b>¿Seguro de quitar de cuarentena y enviar al Stock de Sangre APTAS?</b>   
		</div>-->                
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<!--<button type="button" class="btn btn-primary" onClick="GuardarLiberarCuarentena();">Aceptar</button> -->
		</div>
	  
	 </form>  
    </div>

  </div>
   
</div>
<!--fin modal my_modalReg -->

    <div id="wrapper">
        <div id="page-wrapper"> 
        
        	<!--<div class="row">				
				<?php  //include('../../MVC_Vista/ListaVerificacion/CabeceraLV.php'); ?>
			</div> -->      
          
            <!-- /.row -->
            <div class="row">
                <div class=""><!--col-lg-12-->
                    <div class="panel panel-primary"> <!--panel-default-->
                        <div class="panel-heading"> Stock Actual de Banco de Sangre </div>
                        <div class="panel-body">                                      
                        
                              <div class="dataTable_wrapper">      
                              
                              <?php
							  $TotalPGOPos=0;  $TotalPGAPos=0;  $TotalPGBPos=0;  $TotalPGABPos=0;
							  $TotalPGONeg=0;  $TotalPGANeg=0;  $TotalPGBNeg=0;  $TotalPGABNeg=0;
							  
							  $TotalPFCOPos=0;  $TotalPFCAPos=0;  $TotalPFCBPos=0;  $TotalPFCABPos=0;
							  $TotalPFCONeg=0;  $TotalPFCANeg=0;  $TotalPFCBNeg=0;  $TotalPFCABNeg=0;
							  
							  $TotalPQOPos=0;  $TotalPQAPos=0;  $TotalPQBPos=0;  $TotalPQABPos=0;
							  $TotalPQONeg=0;  $TotalPQANeg=0;  $TotalPQBNeg=0;  $TotalPQABNeg=0;
							  
							  $TotalCRIOOPos=0;  $TotalCRIOAPos=0;  $TotalCRIOBPos=0;  $TotalCRIOABPos=0;
							  $TotalCRIOONeg=0;  $TotalCRIOANeg=0;  $TotalCRIOBNeg=0;  $TotalCRIOABNeg=0;	
							  
							  $cantidadDispoCRIOTotal=0; $cantidadReserCRIOTotal=0; $cantidadCRIOTotal=0; $cantidadVencidosCRIOTotal=0;	
							  $cantidadFraccionCRIOTotal=0;					  
							  
							  	//POSITIVOS 
							  	$ObtenerStockOPos=SIGESA_BSD_ReporteStock_M(1);
								if($ObtenerStockOPos!=NULL){
									$cantidadDispoPGOPos=$ObtenerStockOPos[0]["cantidadDispoPG"];	
									$cantidadDispoPFCOPos=$ObtenerStockOPos[0]["cantidadDispoPFC"];
									$cantidadDispoPQOPos=$ObtenerStockOPos[0]["cantidadDispoPQ"];
									$cantidadReserPGOPos=$ObtenerStockOPos[0]["cantidadReserPG"];	
									$cantidadReserPFCOPos=$ObtenerStockOPos[0]["cantidadReserPFC"];	
									$cantidadReserPQOPos=$ObtenerStockOPos[0]["cantidadReserPQ"];
									$cantidadVencidosPGOPos=$ObtenerStockOPos[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCOPos=$ObtenerStockOPos[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQOPos=$ObtenerStockOPos[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOOPos=$ObtenerStockOPos[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOOPos=$ObtenerStockOPos[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOOPos=$ObtenerStockOPos[0]["cantidadVencidosCRIO"];	
									
									$cantidadFraccionPGOPos=$ObtenerStockOPos[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCOPos=$ObtenerStockOPos[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQOPos=$ObtenerStockOPos[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOOPos=$ObtenerStockOPos[0]["cantidadFraccionCRIO"];	
									
									$cantidadCustodiaPGOPos=$ObtenerStockOPos[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCOPos=$ObtenerStockOPos[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQOPos=$ObtenerStockOPos[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOOPos=$ObtenerStockOPos[0]["cantidadCustodiaCRIO"];								
								}else{
									$cantidadDispoPGOPos=0;	
									$cantidadDispoPFCOPos=0;
									$cantidadDispoPQOPos=0;
									$cantidadReserPGOPos=0;	
									$cantidadReserPFCOPos=0;	
									$cantidadReserPQOPos=0;
									$cantidadVencidosPGOPos=0;	
									$cantidadVencidosPFCOPos=0;	
									$cantidadVencidosPQOPos=0;
									
									$cantidadDispoCRIOOPos=0;
									$cantidadReserCRIOOPos=0;
									$cantidadVencidosCRIOOPos=0;
									
									$cantidadFraccionPGOPos=0;	
									$cantidadFraccionPFCOPos=0;	
									$cantidadFraccionPQOPos=0;	
									$cantidadFraccionCRIOOPos=0;	
									$cantidadCustodiaPGOPos=0;	
									$cantidadCustodiaPFCOPos=0;	
									$cantidadCustodiaPQOPos=0;	
									$cantidadCustodiaCRIOOPos=0;		
								}
								
								$ObtenerStockAPos=SIGESA_BSD_ReporteStock_M(2);
								if($ObtenerStockAPos!=NULL){
									$cantidadDispoPGAPos=$ObtenerStockAPos[0]["cantidadDispoPG"];	
									$cantidadDispoPFCAPos=$ObtenerStockAPos[0]["cantidadDispoPFC"];
									$cantidadDispoPQAPos=$ObtenerStockAPos[0]["cantidadDispoPQ"];
									$cantidadReserPGAPos=$ObtenerStockAPos[0]["cantidadReserPG"];	
									$cantidadReserPFCAPos=$ObtenerStockAPos[0]["cantidadReserPFC"];	
									$cantidadReserPQAPos=$ObtenerStockAPos[0]["cantidadReserPQ"];
									$cantidadVencidosPGAPos=$ObtenerStockAPos[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCAPos=$ObtenerStockAPos[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQAPos=$ObtenerStockAPos[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOAPos=$ObtenerStockAPos[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOAPos=$ObtenerStockAPos[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOAPos=$ObtenerStockAPos[0]["cantidadVencidosCRIO"];
									
									$cantidadFraccionPGAPos=$ObtenerStockAPos[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCAPos=$ObtenerStockAPos[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQAPos=$ObtenerStockAPos[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOAPos=$ObtenerStockAPos[0]["cantidadFraccionCRIO"];
									$cantidadCustodiaPGAPos=$ObtenerStockAPos[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCAPos=$ObtenerStockAPos[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQAPos=$ObtenerStockAPos[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOAPos=$ObtenerStockAPos[0]["cantidadCustodiaCRIO"];		
								}else{
									$cantidadDispoPGAPos=0;	
									$cantidadDispoPFCAPos=0;
									$cantidadDispoPQAPos=0;
									$cantidadReserPGAPos=0;	
									$cantidadReserPFCAPos=0;	
									$cantidadReserPQAPos=0;
									$cantidadVencidosPGAPos=0;	
									$cantidadVencidosPFCAPos=0;	
									$cantidadVencidosPQAPos=0;	
									
									$cantidadDispoCRIOAPos=0;
									$cantidadReserCRIOAPos=0;
									$cantidadVencidosCRIOAPos=0;
									
									$cantidadFraccionPGAPos=0;	
									$cantidadFraccionPFCAPos=0;	
									$cantidadFraccionPQAPos=0;	
									$cantidadFraccionCRIOAPos=0;
									$cantidadCustodiaPGAPos=0;	
									$cantidadCustodiaPFCAPos=0;	
									$cantidadCustodiaPQAPos=0;	
									$cantidadCustodiaCRIOAPos=0;
								}
								
								$ObtenerStockBPos=SIGESA_BSD_ReporteStock_M(3);
								if($ObtenerStockBPos!=NULL){
									$cantidadDispoPGBPos=$ObtenerStockBPos[0]["cantidadDispoPG"];	
									$cantidadDispoPFCBPos=$ObtenerStockBPos[0]["cantidadDispoPFC"];
									$cantidadDispoPQBPos=$ObtenerStockBPos[0]["cantidadDispoPQ"];
									$cantidadReserPGBPos=$ObtenerStockBPos[0]["cantidadReserPG"];	
									$cantidadReserPFCBPos=$ObtenerStockBPos[0]["cantidadReserPFC"];	
									$cantidadReserPQBPos=$ObtenerStockBPos[0]["cantidadReserPQ"];
									$cantidadVencidosPGBPos=$ObtenerStockBPos[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCBPos=$ObtenerStockBPos[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQBPos=$ObtenerStockBPos[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOBPos=$ObtenerStockBPos[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOBPos=$ObtenerStockBPos[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOBPos=$ObtenerStockBPos[0]["cantidadVencidosCRIO"];
									
									$cantidadFraccionPGBPos=$ObtenerStockBPos[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCBPos=$ObtenerStockBPos[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQBPos=$ObtenerStockBPos[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOBPos=$ObtenerStockBPos[0]["cantidadFraccionCRIO"];	
									$cantidadCustodiaPGBPos=$ObtenerStockBPos[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCBPos=$ObtenerStockBPos[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQBPos=$ObtenerStockBPos[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOBPos=$ObtenerStockBPos[0]["cantidadCustodiaCRIO"];	
								}else{
									$cantidadDispoPGBPos=0;	
									$cantidadDispoPFCBPos=0;
									$cantidadDispoPQBPos=0;
									$cantidadReserPGBPos=0;	
									$cantidadReserPFCBPos=0;	
									$cantidadReserPQBPos=0;
									$cantidadVencidosPGBPos=0;	
									$cantidadVencidosPFCBPos=0;	
									$cantidadVencidosPQBPos=0;	
									
									$cantidadDispoCRIOBPos=0;
									$cantidadReserCRIOBPos=0;
									$cantidadVencidosCRIOBPos=0;
									
									$cantidadFraccionPGBPos=0;	
									$cantidadFraccionPFCBPos=0;	
									$cantidadFraccionPQBPos=0;	
									$cantidadFraccionCRIOBPos=0;
									$cantidadCustodiaPGBPos=0;	
									$cantidadCustodiaPFCBPos=0;	
									$cantidadCustodiaPQBPos=0;	
									$cantidadCustodiaCRIOBPos=0;
								}
								
								$ObtenerStockABPos=SIGESA_BSD_ReporteStock_M(4);
								if($ObtenerStockABPos!=NULL){	
									$cantidadDispoPGABPos=$ObtenerStockABPos[0]["cantidadDispoPG"];	
									$cantidadDispoPFCABPos=$ObtenerStockABPos[0]["cantidadDispoPFC"];
									$cantidadDispoPQABPos=$ObtenerStockABPos[0]["cantidadDispoPQ"];
									$cantidadReserPGABPos=$ObtenerStockABPos[0]["cantidadReserPG"];	
									$cantidadReserPFCABPos=$ObtenerStockABPos[0]["cantidadReserPFC"];	
									$cantidadReserPQABPos=$ObtenerStockABPos[0]["cantidadReserPQ"];
									$cantidadVencidosPGABPos=$ObtenerStockABPos[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCABPos=$ObtenerStockABPos[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQABPos=$ObtenerStockABPos[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOABPos=$ObtenerStockABPos[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOABPos=$ObtenerStockABPos[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOABPos=$ObtenerStockABPos[0]["cantidadVencidosCRIO"];
									
									$cantidadFraccionPGABPos=$ObtenerStockABPos[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCABPos=$ObtenerStockABPos[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQABPos=$ObtenerStockABPos[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOABPos=$ObtenerStockABPos[0]["cantidadFraccionCRIO"];
									$cantidadCustodiaPGABPos=$ObtenerStockABPos[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCABPos=$ObtenerStockABPos[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQABPos=$ObtenerStockABPos[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOABPos=$ObtenerStockABPos[0]["cantidadCustodiaCRIO"];		
								}else{
									$cantidadDispoPGABPos=0;	
									$cantidadDispoPFCABPos=0;
									$cantidadDispoPQABPos=0;
									$cantidadReserPGABPos=0;	
									$cantidadReserPFCABPos=0;	
									$cantidadReserPQABPos=0;
									$cantidadVencidosPGABPos=0;	
									$cantidadVencidosPFCABPos=0;	
									$cantidadVencidosPQABPos=0;	
									
									$cantidadDispoCRIOABPos=0;
									$cantidadReserCRIOABPos=0;
									$cantidadVencidosCRIOABPos=0;
									
									$cantidadFraccionPGABPos=0;	
									$cantidadFraccionPFCABPos=0;	
									$cantidadFraccionPQABPos=0;	
									$cantidadFraccionCRIOABPos=0;
									$cantidadCustodiaPGABPos=0;	
									$cantidadCustodiaPFCABPos=0;	
									$cantidadCustodiaPQABPos=0;	
									$cantidadCustodiaCRIOABPos=0;
								}	
								
								//NEGATIVOS							
							  	$ObtenerStockONeg=SIGESA_BSD_ReporteStock_M(5);
								if($ObtenerStockONeg!=NULL){
									$cantidadDispoPGONeg=$ObtenerStockONeg[0]["cantidadDispoPG"];	
									$cantidadDispoPFCONeg=$ObtenerStockONeg[0]["cantidadDispoPFC"];
									$cantidadDispoPQONeg=$ObtenerStockONeg[0]["cantidadDispoPQ"];
									$cantidadReserPGONeg=$ObtenerStockONeg[0]["cantidadReserPG"];	
									$cantidadReserPFCONeg=$ObtenerStockONeg[0]["cantidadReserPFC"];	
									$cantidadReserPQONeg=$ObtenerStockONeg[0]["cantidadReserPQ"];
									$cantidadVencidosPGONeg=$ObtenerStockONeg[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCONeg=$ObtenerStockONeg[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQONeg=$ObtenerStockONeg[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOONeg=$ObtenerStockONeg[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOONeg=$ObtenerStockONeg[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOONeg=$ObtenerStockONeg[0]["cantidadVencidosCRIO"];
									
									$cantidadFraccionPGONeg=$ObtenerStockONeg[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCONeg=$ObtenerStockONeg[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQONeg=$ObtenerStockONeg[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOONeg=$ObtenerStockONeg[0]["cantidadFraccionCRIO"];
									$cantidadCustodiaPGONeg=$ObtenerStockONeg[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCONeg=$ObtenerStockONeg[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQONeg=$ObtenerStockONeg[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOONeg=$ObtenerStockONeg[0]["cantidadCustodiaCRIO"];	
								}else{
									$cantidadDispoPGONeg=0;	
									$cantidadDispoPFCONeg=0;
									$cantidadDispoPQONeg=0;
									$cantidadReserPGONeg=0;	
									$cantidadReserPFCONeg=0;	
									$cantidadReserPQONeg=0;
									$cantidadVencidosPGONeg=0;	
									$cantidadVencidosPFCONeg=0;	
									$cantidadVencidosPQONeg=0;	
									
									$cantidadDispoCRIOONeg=0;
									$cantidadReserCRIOONeg=0;
									$cantidadVencidosCRIOONeg=0;
									
									$cantidadFraccionPGONeg=0;	
									$cantidadFraccionPFCONeg=0;	
									$cantidadFraccionPQONeg=0;	
									$cantidadFraccionCRIOONeg=0;
									$cantidadCustodiaPGONeg=0;	
									$cantidadCustodiaPFCONeg=0;	
									$cantidadCustodiaPQONeg=0;	
									$cantidadCustodiaCRIOONeg=0;
								}
								
								$ObtenerStockANeg=SIGESA_BSD_ReporteStock_M(6);
								if($ObtenerStockANeg!=NULL){
									$cantidadDispoPGANeg=$ObtenerStockANeg[0]["cantidadDispoPG"];	
									$cantidadDispoPFCANeg=$ObtenerStockANeg[0]["cantidadDispoPFC"];
									$cantidadDispoPQANeg=$ObtenerStockANeg[0]["cantidadDispoPQ"];
									$cantidadReserPGANeg=$ObtenerStockANeg[0]["cantidadReserPG"];	
									$cantidadReserPFCANeg=$ObtenerStockANeg[0]["cantidadReserPFC"];	
									$cantidadReserPQANeg=$ObtenerStockANeg[0]["cantidadReserPQ"];
									$cantidadVencidosPGANeg=$ObtenerStockANeg[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCANeg=$ObtenerStockANeg[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQANeg=$ObtenerStockANeg[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOANeg=$ObtenerStockANeg[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOANeg=$ObtenerStockANeg[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOANeg=$ObtenerStockANeg[0]["cantidadVencidosCRIO"];
									
									$cantidadFraccionPGANeg=$ObtenerStockANeg[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCANeg=$ObtenerStockANeg[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQANeg=$ObtenerStockANeg[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOANeg=$ObtenerStockANeg[0]["cantidadFraccionCRIO"];	
									$cantidadCustodiaPGANeg=$ObtenerStockANeg[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCANeg=$ObtenerStockANeg[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQANeg=$ObtenerStockANeg[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOANeg=$ObtenerStockANeg[0]["cantidadCustodiaCRIO"];	
								}else{
									$cantidadDispoPGANeg=0;	
									$cantidadDispoPFCANeg=0;
									$cantidadDispoPQANeg=0;
									$cantidadReserPGANeg=0;	
									$cantidadReserPFCANeg=0;	
									$cantidadReserPQANeg=0;
									$cantidadVencidosPGANeg=0;	
									$cantidadVencidosPFCANeg=0;	
									$cantidadVencidosPQANeg=0;	
									
									$cantidadDispoCRIOANeg=0;
									$cantidadReserCRIOANeg=0;
									$cantidadVencidosCRIOANeg=0;
									
									$cantidadFraccionPGANeg=0;	
									$cantidadFraccionPFCANeg=0;	
									$cantidadFraccionPQANeg=0;	
									$cantidadFraccionCRIOANeg=0;
									$cantidadCustodiaPGANeg=0;	
									$cantidadCustodiaPFCANeg=0;	
									$cantidadCustodiaPQANeg=0;	
									$cantidadCustodiaCRIOANeg=0;
								}
								
								$ObtenerStockBNeg=SIGESA_BSD_ReporteStock_M(7);
								if($ObtenerStockBNeg!=NULL){
									$cantidadDispoPGBNeg=$ObtenerStockBNeg[0]["cantidadDispoPG"];	
									$cantidadDispoPFCBNeg=$ObtenerStockBNeg[0]["cantidadDispoPFC"];
									$cantidadDispoPQBNeg=$ObtenerStockBNeg[0]["cantidadDispoPQ"];
									$cantidadReserPGBNeg=$ObtenerStockBNeg[0]["cantidadReserPG"];	
									$cantidadReserPFCBNeg=$ObtenerStockBNeg[0]["cantidadReserPFC"];	
									$cantidadReserPQBNeg=$ObtenerStockBNeg[0]["cantidadReserPQ"];
									$cantidadVencidosPGBNeg=$ObtenerStockBNeg[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCBNeg=$ObtenerStockBNeg[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQBNeg=$ObtenerStockBNeg[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOBNeg=$ObtenerStockBNeg[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOBNeg=$ObtenerStockBNeg[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOBNeg=$ObtenerStockBNeg[0]["cantidadVencidosCRIO"];
									
									$cantidadFraccionPGBNeg=$ObtenerStockBNeg[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCBNeg=$ObtenerStockBNeg[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQBNeg=$ObtenerStockBNeg[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOBNeg=$ObtenerStockBNeg[0]["cantidadFraccionCRIO"];
									$cantidadCustodiaPGBNeg=$ObtenerStockBNeg[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCBNeg=$ObtenerStockBNeg[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQBNeg=$ObtenerStockBNeg[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOBNeg=$ObtenerStockBNeg[0]["cantidadCustodiaCRIO"];	
								}else{
									$cantidadDispoPGBNeg=0;	
									$cantidadDispoPFCBNeg=0;
									$cantidadDispoPQBNeg=0;
									$cantidadReserPGBNeg=0;	
									$cantidadReserPFCBNeg=0;	
									$cantidadReserPQBNeg=0;
									$cantidadVencidosPGBNeg=0;	
									$cantidadVencidosPFCBNeg=0;	
									$cantidadVencidosPQBNeg=0;
									
									$cantidadDispoCRIOBNeg=0;
									$cantidadReserCRIOBNeg=0;
									$cantidadVencidosCRIOBNeg=0;	
									
									$cantidadFraccionPGBNeg=0;	
									$cantidadFraccionPFCBNeg=0;	
									$cantidadFraccionPQBNeg=0;	
									$cantidadFraccionCRIOBNeg=0;									
									$cantidadCustodiaPGBNeg=0;	
									$cantidadCustodiaPFCBNeg=0;	
									$cantidadCustodiaPQBNeg=0;	
									$cantidadCustodiaCRIOBNeg=0;
								}
								
								$ObtenerStockABNeg=SIGESA_BSD_ReporteStock_M(8);
								if($ObtenerStockABNeg!=NULL){
									$cantidadDispoPGABNeg=$ObtenerStockABNeg[0]["cantidadDispoPG"];	
									$cantidadDispoPFCABNeg=$ObtenerStockABNeg[0]["cantidadDispoPFC"];
									$cantidadDispoPQABNeg=$ObtenerStockABNeg[0]["cantidadDispoPQ"];
									$cantidadReserPGABNeg=$ObtenerStockABNeg[0]["cantidadReserPG"];	
									$cantidadReserPFCABNeg=$ObtenerStockABNeg[0]["cantidadReserPFC"];	
									$cantidadReserPQABNeg=$ObtenerStockABNeg[0]["cantidadReserPQ"];
									$cantidadVencidosPGABNeg=$ObtenerStockABNeg[0]["cantidadVencidosPG"];	
									$cantidadVencidosPFCABNeg=$ObtenerStockABNeg[0]["cantidadVencidosPFC"];	
									$cantidadVencidosPQABNeg=$ObtenerStockABNeg[0]["cantidadVencidosPQ"];
									
									$cantidadDispoCRIOABNeg=$ObtenerStockABNeg[0]["cantidadDispoCRIO"];
									$cantidadReserCRIOABNeg=$ObtenerStockABNeg[0]["cantidadReserCRIO"];
									$cantidadVencidosCRIOABNeg=$ObtenerStockABNeg[0]["cantidadVencidosCRIO"];
									
									$cantidadFraccionPGABNeg=$ObtenerStockABNeg[0]["cantidadFraccionPG"];	
									$cantidadFraccionPFCABNeg=$ObtenerStockABNeg[0]["cantidadFraccionPFC"];	
									$cantidadFraccionPQABNeg=$ObtenerStockABNeg[0]["cantidadFraccionPQ"];	
									$cantidadFraccionCRIOABNeg=$ObtenerStockABNeg[0]["cantidadFraccionCRIO"];
									$cantidadCustodiaPGABNeg=$ObtenerStockABNeg[0]["cantidadCustodiaPG"];	
									$cantidadCustodiaPFCABNeg=$ObtenerStockABNeg[0]["cantidadCustodiaPFC"];	
									$cantidadCustodiaPQABNeg=$ObtenerStockABNeg[0]["cantidadCustodiaPQ"];	
									$cantidadCustodiaCRIOABNeg=$ObtenerStockABNeg[0]["cantidadCustodiaCRIO"];	
								}else{
									$cantidadDispoPGABNeg=0;	
									$cantidadDispoPFCABNeg=0;
									$cantidadDispoPQABNeg=0;
									$cantidadReserPGABNeg=0;	
									$cantidadReserPFCABNeg=0;	
									$cantidadReserPQABNeg=0;
									$cantidadVencidosPGABNeg=0;	
									$cantidadVencidosPFCABNeg=0;	
									$cantidadVencidosPQABNeg=0;	
									
									$cantidadDispoCRIOABNeg=0;
									$cantidadReserCRIOABNeg=0;
									$cantidadVencidosCRIOABNeg=0;
									
									$cantidadFraccionPGABNeg=0;	
									$cantidadFraccionPFCABNeg=0;	
									$cantidadFraccionPQABNeg=0;	
									$cantidadFraccionCRIOABNeg=0;
									$cantidadCustodiaPGABNeg=0;	
									$cantidadCustodiaPFCABNeg=0;	
									$cantidadCustodiaPQABNeg=0;	
									$cantidadCustodiaCRIOABNeg=0;
								}
								
								
								//CUARENTENA POSITIVOS 
							  	$ObtenerStockCuarentenaOPos=SIGESA_BSD_ReporteStockCuarentena_M(1);
								if($ObtenerStockCuarentenaOPos!=NULL){
									$cantidadCuarentenaPGOPos=$ObtenerStockCuarentenaOPos[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCOPos=$ObtenerStockCuarentenaOPos[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQOPos=$ObtenerStockCuarentenaOPos[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGOPos=0;	
									$cantidadCuarentenaPFCOPos=0;
									$cantidadCuarentenaPQOPos=0;
								}
								
								$ObtenerStockCuarentenaAPos=SIGESA_BSD_ReporteStockCuarentena_M(2);
								if($ObtenerStockCuarentenaAPos!=NULL){
									$cantidadCuarentenaPGAPos=$ObtenerStockCuarentenaAPos[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCAPos=$ObtenerStockCuarentenaAPos[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQAPos=$ObtenerStockCuarentenaAPos[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGAPos=0;	
									$cantidadCuarentenaPFCAPos=0;
									$cantidadCuarentenaPQAPos=0;
								}
								
								$ObtenerStockCuarentenaBPos=SIGESA_BSD_ReporteStockCuarentena_M(3);
								if($ObtenerStockCuarentenaBPos!=NULL){
									$cantidadCuarentenaPGBPos=$ObtenerStockCuarentenaBPos[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCBPos=$ObtenerStockCuarentenaBPos[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQBPos=$ObtenerStockCuarentenaBPos[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGBPos=0;	
									$cantidadCuarentenaPFCBPos=0;
									$cantidadCuarentenaPQBPos=0;
								}
								
								$ObtenerStockCuarentenaABPos=SIGESA_BSD_ReporteStockCuarentena_M(4);
								if($ObtenerStockCuarentenaABPos!=NULL){
									$cantidadCuarentenaPGABPos=$ObtenerStockCuarentenaABPos[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCABPos=$ObtenerStockCuarentenaABPos[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQABPos=$ObtenerStockCuarentenaABPos[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGABPos=0;	
									$cantidadCuarentenaPFCABPos=0;
									$cantidadCuarentenaPQABPos=0;
								}
								
								//CUARENTENA NEGATIVOS 
							  	$ObtenerStockCuarentenaONeg=SIGESA_BSD_ReporteStockCuarentena_M(5);
								if($ObtenerStockCuarentenaONeg!=NULL){
									$cantidadCuarentenaPGONeg=$ObtenerStockCuarentenaONeg[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCONeg=$ObtenerStockCuarentenaONeg[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQONeg=$ObtenerStockCuarentenaONeg[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGONeg=0;	
									$cantidadCuarentenaPFCONeg=0;
									$cantidadCuarentenaPQONeg=0;
								}
								
								$ObtenerStockCuarentenaANeg=SIGESA_BSD_ReporteStockCuarentena_M(6);
								if($ObtenerStockCuarentenaANeg!=NULL){
									$cantidadCuarentenaPGANeg=$ObtenerStockCuarentenaANeg[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCANeg=$ObtenerStockCuarentenaANeg[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQANeg=$ObtenerStockCuarentenaANeg[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGANeg=0;	
									$cantidadCuarentenaPFCANeg=0;
									$cantidadCuarentenaPQANeg=0;
								}
								
								$ObtenerStockCuarentenaBNeg=SIGESA_BSD_ReporteStockCuarentena_M(7);
								if($ObtenerStockCuarentenaBNeg!=NULL){
									$cantidadCuarentenaPGBNeg=$ObtenerStockCuarentenaBNeg[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCBNeg=$ObtenerStockCuarentenaBNeg[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQBNeg=$ObtenerStockCuarentenaBNeg[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGBNeg=0;	
									$cantidadCuarentenaPFCBNeg=0;
									$cantidadCuarentenaPQBNeg=0;
								}
								
								$ObtenerStockCuarentenaABNeg=SIGESA_BSD_ReporteStockCuarentena_M(8);
								if($ObtenerStockCuarentenaABNeg!=NULL){
									$cantidadCuarentenaPGABNeg=$ObtenerStockCuarentenaABNeg[0]["cantidadCuarentenaPG"];	
									$cantidadCuarentenaPFCABNeg=$ObtenerStockCuarentenaABNeg[0]["cantidadCuarentenaPFC"];
									$cantidadCuarentenaPQABNeg=$ObtenerStockCuarentenaABNeg[0]["cantidadCuarentenaPQ"];
								}else{
									$cantidadCuarentenaPGABNeg=0;	
									$cantidadCuarentenaPFCABNeg=0;
									$cantidadCuarentenaPQABNeg=0;
								}	
								
							  $TotalPGOPos=$TotalPGOPos+$cantidadDispoPGOPos+$cantidadReserPGOPos+$cantidadCuarentenaPGOPos;
							  $TotalPGAPos=$TotalPGAPos+$cantidadDispoPGAPos+$cantidadReserPGAPos+$cantidadCuarentenaPGAPos; 
							  $TotalPGBPos=$TotalPGBPos+$cantidadDispoPGBPos+$cantidadReserPGBPos+$cantidadCuarentenaPGBPos;  
							  $TotalPGABPos=$TotalPGABPos+$cantidadDispoPGABPos+$cantidadReserPGABPos+$cantidadCuarentenaPGABPos;
							  
							  $TotalPGONeg=$TotalPGONeg+$cantidadDispoPGONeg+$cantidadReserPGONeg+$cantidadCuarentenaPGONeg;  
							  $TotalPGANeg=$TotalPGANeg+$cantidadDispoPGANeg+$cantidadReserPGANeg+$cantidadCuarentenaPGANeg;  
							  $TotalPGBNeg=$TotalPGBNeg+$cantidadDispoPGBNeg+$cantidadReserPGBNeg+$cantidadCuarentenaPGBNeg; 
							  $TotalPGABNeg=$TotalPGABNeg+$cantidadDispoPGABNeg+$cantidadReserPGABNeg+$cantidadCuarentenaPGABNeg;
							  
							  $TotalPFCOPos=$TotalPFCOPos+$cantidadDispoPFCOPos+$cantidadReserPFCOPos+$cantidadCuarentenaPFCOPos;  
							  $TotalPFCAPos=$TotalPFCAPos+$cantidadDispoPFCAPos+$cantidadReserPFCAPos+$cantidadCuarentenaPFCAPos;  
							  $TotalPFCBPos=$TotalPFCBPos+$cantidadDispoPFCBPos+$cantidadReserPFCBPos+$cantidadCuarentenaPFCBPos;  
							  $TotalPFCABPos=$TotalPFCABPos+$cantidadDispoPFCABPos+$cantidadReserPFCABPos+$cantidadCuarentenaPFCABPos;
							  
							  $TotalPFCONeg=$TotalPFCONeg+$cantidadDispoPFCONeg+$cantidadReserPFCONeg+$cantidadCuarentenaPFCONeg;  
							  $TotalPFCANeg=$TotalPFCANeg+$cantidadDispoPFCANeg+$cantidadReserPFCANeg+$cantidadCuarentenaPFCANeg; 
							  $TotalPFCBNeg=$TotalPFCBNeg+$cantidadDispoPFCBNeg+$cantidadReserPFCBNeg+$cantidadCuarentenaPFCBNeg;  
							  $TotalPFCABNeg=$TotalPFCABNeg+$cantidadDispoPFCABNeg+$cantidadReserPFCABNeg+$cantidadCuarentenaPFCABNeg;
							  
							  $TotalPQOPos=$TotalPQOPos+$cantidadDispoPQOPos+$cantidadReserPQOPos+$cantidadCuarentenaPQOPos;  
							  $TotalPQAPos=$TotalPQAPos+$cantidadDispoPQAPos+$cantidadReserPQAPos+$cantidadCuarentenaPQAPos;  
							  $TotalPQBPos=$TotalPQBPos+$cantidadDispoPQBPos+$cantidadReserPQBPos+$cantidadCuarentenaPQBPos;  
							  $TotalPQABPos=$TotalPQABPos+$cantidadDispoPQABPos+$cantidadReserPQABPos+$cantidadCuarentenaPQABPos;
							  
							  $TotalPQONeg=$TotalPQONeg+$cantidadDispoPQONeg+$cantidadReserPQONeg+$cantidadCuarentenaPQONeg;  
							  $TotalPQANeg=$TotalPQANeg+$cantidadDispoPQANeg+$cantidadReserPQANeg+$cantidadCuarentenaPQANeg;
							  $TotalPQBNeg=$TotalPQBNeg+$cantidadDispoPQBNeg+$cantidadReserPQBNeg+$cantidadCuarentenaPQBNeg;
							  $TotalPQABNeg=$TotalPQABNeg+$cantidadDispoPQABNeg+$cantidadReserPQABNeg+$cantidadCuarentenaPQABNeg;	
							  
							  $TotalCRIOOPos=$TotalCRIOOPos+$cantidadDispoCRIOOPos+$cantidadReserCRIOOPos+$cantidadCuarentenaCRIOOPos;  
							  $TotalCRIOAPos=$TotalCRIOAPos+$cantidadDispoCRIOAPos+$cantidadReserCRIOAPos+$cantidadCuarentenaCRIOAPos;  
							  $TotalCRIOBPos=$TotalCRIOBPos+$cantidadDispoCRIOBPos+$cantidadReserCRIOBPos+$cantidadCuarentenaCRIOBPos;  
							  $TotalCRIOABPos=$TotalCRIOABPos+$cantidadDispoCRIOABPos+$cantidadReserCRIOABPos+$cantidadCuarentenaCRIOABPos;
							  
							  $TotalCRIOONeg=$TotalCRIOONeg+$cantidadDispoCRIOONeg+$cantidadReserCRIOONeg+$cantidadCuarentenaCRIOONeg;  
							  $TotalCRIOANeg=$TotalCRIOANeg+$cantidadDispoCRIOANeg+$cantidadReserCRIOANeg+$cantidadCuarentenaCRIOANeg;
							  $TotalCRIOBNeg=$TotalCRIOBNeg+$cantidadDispoCRIOBNeg+$cantidadReserCRIOBNeg+$cantidadCuarentenaCRIOBNeg;
							  $TotalCRIOABNeg=$TotalCRIOABNeg+$cantidadDispoCRIOABNeg+$cantidadReserCRIOABNeg+$cantidadCuarentenaCRIOABNeg;		
							  
							  $cantidadDispoCRIOTotal=$cantidadDispoCRIOTotal+$cantidadDispoCRIOOPos+$cantidadDispoCRIOAPos+$cantidadDispoCRIOBPos+$cantidadDispoCRIOABPos+
							  												  $cantidadDispoCRIOONeg+$cantidadDispoCRIOANeg+$cantidadDispoCRIOBNeg+$cantidadDispoCRIOABNeg;	
																			  
							  $cantidadReserCRIOTotal=$cantidadReserCRIOTotal+$cantidadReserCRIOOPos+$cantidadReserCRIOAPos+$cantidadReserCRIOBPos+$cantidadReserCRIOABPos+
							  												  $cantidadReserCRIOONeg+$cantidadReserCRIOANeg+$cantidadReserCRIOBNeg+$cantidadReserCRIOABNeg;
																																					
							  $cantidadCRIOTotal=$cantidadCRIOTotal+$cantidadDispoCRIOTotal+$cantidadReserCRIOTotal;
							  
							  $cantidadVencidosCRIOTotal=$cantidadVencidosCRIOTotal+$cantidadVencidosCRIOOPos+$cantidadVencidosCRIOAPos+$cantidadVencidosCRIOBPos+$cantidadVencidosCRIOABPos+
							  												  $cantidadVencidosCRIOONeg+$cantidadVencidosCRIOANeg+$cantidadVencidosCRIOBNeg+$cantidadVencidosCRIOABNeg;
																			  
							  $cantidadFraccionCRIOTotal=$cantidadFraccionCRIOTotal+$cantidadFraccionCRIOOPos+$cantidadFraccionCRIOAPos+$cantidadFraccionCRIOBPos+$cantidadFraccionCRIOABPos+
							  												  $cantidadFraccionCRIOONeg+$cantidadFraccionCRIOANeg+$cantidadFraccionCRIOBNeg+$cantidadFraccionCRIOABNeg;
																			  
							  $cantidadCustodiaCrioTodos=$cantidadCustodiaCrioTodos+$cantidadCustodiaCRIOOPos+$cantidadCustodiaCRIOAPos+$cantidadCustodiaCRIOBPos+$cantidadCustodiaCRIOABPos+
							  												  $cantidadCustodiaCRIOONeg+$cantidadCustodiaCRIOANeg+$cantidadCustodiaCRIOBNeg+$cantidadCustodiaCRIOABNeg;											  
																			  
							  ?>                
                            
                                <table width="100%" class="table table-bordered" id="dataTables-example"> <!--table-hover--> <!--class="table table-bordered table-hover dataTable no-footer"--> 
                                	<thead>
                                        <tr class="text-center">
                                            <th rowspan="2">&nbsp;</th>
                                            <th rowspan="2">&nbsp;</th>
                                            <th colspan="8">PAQUETE GLOBULAR</th>
                                            <th colspan="8">PLASMA FRESCO CONGELADO</th>
                                            <th rowspan="2" class="rotar" width="20">CRIOPRE-CIPITADO</th>
                                            <th colspan="8">PLAQUETAS</th>
                                      </tr>                                    
                                        <tr class="text-center">
                                          <td>O+</td>
                                          <td>O-</td>
                                          <td>A+</td>
                                          <td>A-</td>
                                          <td>B+</td>
                                          <td>B-</td>
                                          <td>AB+</td>
                                          <td>AB-</td>
                                          <td>O+</td>
                                          <td>O-</td>
                                          <td>A+</td>
                                          <td>A-</td>
                                          <td>B+</td>
                                          <td>B-</td>
                                          <td>AB+</td>
                                          <td>AB-</td>
                                          <td>O+</td>
                                          <td>O-</td>
                                          <td>A+</td>
                                          <td>A-</td>
                                          <td>B+</td>
                                          <td>B-</td>
                                          <td>AB+</td>
                                          <td>AB-</td>
                                      </tr>
                                    </thead>
                                    <tbody>                                                                                                      						
 
                                      <tr  class="text-center">
                                        <td rowspan="3" ><p>&nbsp;</p>
                                        <p><strong>Aptas</strong></p></td>
                                        <td>Disponibles</td>
										<?php $IdGrupoSanguineoOPos='1';$IdGrupoSanguineoAPos='2';$IdGrupoSanguineoBPos='3';$IdGrupoSanguineoABPos='4';$IdGrupoSanguineoONeg='5';$IdGrupoSanguineoANeg='6';$IdGrupoSanguineoBNeg='7';$IdGrupoSanguineoABNeg='8';?>
										<?php $TipoHem='PG';$Estado='1';$Vencido='1'; ?>
                                        <td align="center"> <?php if($cantidadDispoPGOPos==0){ echo $cantidadDispoPGOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPGONeg==0){ echo $cantidadDispoPGONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPGAPos==0){ echo $cantidadDispoPGAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPGANeg==0){ echo $cantidadDispoPGANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPGBPos==0){ echo $cantidadDispoPGBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPGBNeg==0){ echo $cantidadDispoPGBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPGABPos==0){ echo $cantidadDispoPGABPos;}else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPGABNeg==0){ echo $cantidadDispoPGABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPGABNeg; ?></button><?php } ?></td>
                                        
										<?php $TipoHem='PFC';$Estado='1';$Vencido='1'; ?>
                                        <td align="center"> <?php if($cantidadDispoPFCOPos==0){ echo $cantidadDispoPFCOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPFCONeg==0){ echo $cantidadDispoPFCONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPFCAPos==0){ echo $cantidadDispoPFCAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPFCANeg==0){ echo $cantidadDispoPFCANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPFCBPos==0){ echo $cantidadDispoPFCBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPFCBNeg==0){ echo $cantidadDispoPFCBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPFCABPos==0){ echo $cantidadDispoPFCABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPFCABNeg==0){ echo $cantidadDispoPFCABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPFCABNeg; ?></button><?php } ?></td>
                                        
										<?php $TipoHem='CRIO';$Estado='1';$Vencido='1'; //cantidadDispoCRIOTotal ?>
										<td align="center"> <?php if($cantidadDispoCRIOTotal==0){ echo $cantidadDispoCRIOTotal; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','0','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoCRIOTotal; ?></button><?php } ?></td>
                                       
										<?php $TipoHem='PQ';$Estado='1';$Vencido='1'; ?>
                                        <td align="center"> <?php if($cantidadDispoPQOPos==0){ echo $cantidadDispoPQOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPQONeg==0){ echo $cantidadDispoPQONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPQAPos==0){ echo $cantidadDispoPQAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPQANeg==0){ echo $cantidadDispoPQANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPQBPos==0){ echo $cantidadDispoPQBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPQBNeg==0){ echo $cantidadDispoPQBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPQABPos==0){ echo $cantidadDispoPQABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadDispoPQABNeg==0){ echo $cantidadDispoPQABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadDispoPQABNeg; ?></button><?php } ?></td>
                                      </tr>
									  
                                      <tr  class="text-center">
                                      	<?php $TipoHem='PG';$Estado='2';$Vencido='2'; //cantidadReserPGOPos ?>
                                        <td>Reservadas</td>
                                        <td align="center"> <?php if($cantidadReserPGOPos==0){ echo $cantidadReserPGOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPGONeg==0){ echo $cantidadReserPGONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPGAPos==0){ echo $cantidadReserPGAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPGANeg==0){ echo $cantidadReserPGANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPGBPos==0){ echo $cantidadReserPGBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPGBNeg==0){ echo $cantidadReserPGBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPGABPos==0){ echo $cantidadReserPGABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPGABNeg==0){ echo $cantidadReserPGABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPGABNeg; ?></button><?php } ?></td>
                                      
										<?php $TipoHem='PFC';$Estado='2';$Vencido='2'; //cantidadReserPFCOPos ?>
                                        <td align="center"> <?php if($cantidadReserPFCOPos==0){ echo $cantidadReserPFCOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPFCONeg==0){ echo $cantidadReserPFCONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPFCAPos==0){ echo $cantidadReserPFCAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPFCANeg==0){ echo $cantidadReserPFCANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPFCBPos==0){ echo $cantidadReserPFCBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPFCBNeg==0){ echo $cantidadReserPFCBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPFCABPos==0){ echo $cantidadReserPFCABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPFCABNeg==0){ echo $cantidadReserPFCABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPFCABNeg; ?></button><?php } ?></td>
                                      
										<?php $TipoHem='CRIO';$Estado='2';$Vencido='2'; //cantidadReserCRIOTotal ?>
                                        <td align="center"> <?php if($cantidadReserCRIOTotal==0){ echo $cantidadReserCRIOTotal; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','0','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserCRIOTotal; ?></button><?php } ?></td>
                                        
										<?php $TipoHem='PQ';$Estado='2';$Vencido='2'; //cantidadReserPQOPos ?>
                                        <td align="center"> <?php if($cantidadReserPQOPos==0){ echo $cantidadReserPQOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPQONeg==0){ echo $cantidadReserPQONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPQAPos==0){ echo $cantidadReserPQAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPQANeg==0){ echo $cantidadReserPQANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPQBPos==0){ echo $cantidadReserPQBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPQBNeg==0){ echo $cantidadReserPQBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPQABPos==0){ echo $cantidadReserPQABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadReserPQABNeg==0){ echo $cantidadReserPQABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadReserPQABNeg; ?></button><?php } ?></td>
                                      
                                      </tr>
                                      <tr  class="text-center">
                                        <td>Fraccion Pediatrica</td>
										<?php $TipoHem='PG';$Estado='3';$Vencido='2'; //cantidadFraccionPGOPos ?>
                                        <td align="center"> <?php if($cantidadFraccionPGOPos==0){ echo $cantidadFraccionPGOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPGONeg==0){ echo $cantidadFraccionPGONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPGAPos==0){ echo $cantidadFraccionPGAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPGANeg==0){ echo $cantidadFraccionPGANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPGBPos==0){ echo $cantidadFraccionPGBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPGBNeg==0){ echo $cantidadFraccionPGBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPGABPos==0){ echo $cantidadFraccionPGABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPGABNeg==0){ echo $cantidadFraccionPGABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPGABNeg; ?></button><?php } ?></td>
                                      
										<?php $TipoHem='PFC';$Estado='3';$Vencido='2'; //cantidadFraccionPFCOPos ?>
                                       <td align="center"> <?php if($cantidadFraccionPFCOPos==0){ echo $cantidadFraccionPFCOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPFCONeg==0){ echo $cantidadFraccionPFCONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPFCAPos==0){ echo $cantidadFraccionPFCAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPFCANeg==0){ echo $cantidadFraccionPFCANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPFCBPos==0){ echo $cantidadFraccionPFCBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPFCBNeg==0){ echo $cantidadFraccionPFCBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPFCABPos==0){ echo $cantidadFraccionPFCABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPFCABNeg==0){ echo $cantidadFraccionPFCABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPFCABNeg; ?></button><?php } ?></td>
                                      
										<?php $TipoHem='CRIO';$Estado='3';$Vencido='2'; //cantidadFraccionCRIOTotal ?>
                                        <td align="center"> <?php if($cantidadFraccionCRIOTotal==0){ echo $cantidadFraccionCRIOTotal; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','0','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionCRIOTotal; ?></button><?php } ?></td>
                                        
										<?php $TipoHem='PQ';$Estado='3';$Vencido='2'; //cantidadFraccionPQOPos ?>
                                        <td align="center"> <?php if($cantidadFraccionPQOPos==0){ echo $cantidadFraccionPQOPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQOPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPQONeg==0){ echo $cantidadFraccionPQONeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQONeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPQAPos==0){ echo $cantidadFraccionPQAPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQAPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPQANeg==0){ echo $cantidadFraccionPQANeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQANeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPQBPos==0){ echo $cantidadFraccionPQBPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQBPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPQBNeg==0){ echo $cantidadFraccionPQBNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQBNeg; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPQABPos==0){ echo $cantidadFraccionPQABPos; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQABPos; ?></button><?php } ?></td>
                                        <td align="center"> <?php if($cantidadFraccionPQABNeg==0){ echo $cantidadFraccionPQABNeg; }else{ ?><button type="button" class="btn btn-info btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadFraccionPQABNeg; ?></button><?php } ?></td>
                                      </tr>
									  
                                      <tr  class="text-center">
                                        <td><strong>Pre Almacenamiento</strong></td>
                                        <td>Cuarentena</td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGOPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGONeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGAPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGANeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGBPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGBNeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGABPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPGABNeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCOPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCONeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCAPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCANeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCBPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCBNeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCABPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPFCABNeg; ?></td>
                                        <td align="center">0</td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQOPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQONeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQAPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQANeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQBPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQBNeg; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQABPos; ?></td>
                                        <td align="center"><?php echo $cantidadCuarentenaPQABNeg; ?></td>
                                      </tr>
                                      <tr  class="text-center">
										<?php $TipoHem='PG';$Estado='4';$Vencido='2'; //cantidadCustodiaPGOPos ?>
                                        <td colspan="2">Custodia</td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGOPos==0){ echo $cantidadCustodiaPGOPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGOPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGONeg==0){ echo $cantidadCustodiaPGONeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGONeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGAPos==0){ echo $cantidadCustodiaPGAPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGAPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGANeg==0){ echo $cantidadCustodiaPGANeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGANeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGBPos==0){ echo $cantidadCustodiaPGBPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGBPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGBNeg==0){ echo $cantidadCustodiaPGBNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGBNeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGABPos==0){ echo $cantidadCustodiaPGABPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGABPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPGABNeg==0){ echo $cantidadCustodiaPGABNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPGABNeg; ?></button><?php } ?></td>
                                        
										<?php $TipoHem='PFC';$Estado='4';$Vencido='2'; //cantidadCustodiaPFCOPos ?>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCOPos==0){ echo $cantidadCustodiaPFCOPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCOPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCONeg==0){ echo $cantidadCustodiaPFCONeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCONeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCAPos==0){ echo $cantidadCustodiaPFCAPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCAPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCANeg==0){ echo $cantidadCustodiaPFCANeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCANeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCBPos==0){ echo $cantidadCustodiaPFCBPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCBPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCBNeg==0){ echo $cantidadCustodiaPFCBNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCBNeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCABPos==0){ echo $cantidadCustodiaPFCABPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCABPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPFCABNeg==0){ echo $cantidadCustodiaPFCABNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPFCABNeg; ?></button><?php } ?></td>
                                        
										<?php $TipoHem='CRIO';$Estado='4';$Vencido='2'; //cantidadCustodiaCrioTodos ?>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaCrioTodos==0){ echo $cantidadCustodiaCrioTodos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','0','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaCrioTodos; ?></button><?php } ?></td>
                                        
										<?php $TipoHem='PQ';$Estado='4';$Vencido='2'; //cantidadCustodiaPQOPos ?>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQOPos==0){ echo $cantidadCustodiaPQOPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQOPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQONeg==0){ echo $cantidadCustodiaPQONeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQONeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQAPos==0){ echo $cantidadCustodiaPQAPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQAPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQANeg==0){ echo $cantidadCustodiaPQANeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQANeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQBPos==0){ echo $cantidadCustodiaPQBPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQBPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQBNeg==0){ echo $cantidadCustodiaPQBNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQBNeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQABPos==0){ echo $cantidadCustodiaPQABPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQABPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadCustodiaPQABNeg==0){ echo $cantidadCustodiaPQABNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadCustodiaPQABNeg; ?></button><?php } ?></td>
                                      </tr>
									  
                                      <tr  class="text-center">
                                        <td colspan="2">Vencidos</td>
										<?php $TipoHem='PG';$Estado='1';$Vencido='0'; //cantidadVencidosPGOPos ?>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGOPos==0){ echo $cantidadVencidosPGOPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGOPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGONeg==0){ echo $cantidadVencidosPGONeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGONeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGAPos==0){ echo $cantidadVencidosPGAPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGAPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGANeg==0){ echo $cantidadVencidosPGANeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGANeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGBPos==0){ echo $cantidadVencidosPGBPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGBPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGBNeg==0){ echo $cantidadVencidosPGBNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGBNeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGABPos==0){ echo $cantidadVencidosPGABPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGABPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPGABNeg==0){ echo $cantidadVencidosPGABNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPGABNeg; ?></button><?php } ?></td>
                                      
										<?php $TipoHem='PFC';$Estado='1';$Vencido='0'; //cantidadVencidosPFCOPos ?>
                                         <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCOPos==0){ echo $cantidadVencidosPFCOPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCOPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCONeg==0){ echo $cantidadVencidosPFCONeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCONeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCAPos==0){ echo $cantidadVencidosPFCAPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCAPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCANeg==0){ echo $cantidadVencidosPFCANeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCANeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCBPos==0){ echo $cantidadVencidosPFCBPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCBPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCBNeg==0){ echo $cantidadVencidosPFCBNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCBNeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCABPos==0){ echo $cantidadVencidosPFCABPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCABPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPFCABNeg==0){ echo $cantidadVencidosPFCABNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPFCABNeg; ?></button><?php } ?></td>
                                      
										<?php $TipoHem='CRIO';$Estado='1';$Vencido='0'; //cantidadVencidosCRIOTotal ?>
										<td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosCRIOTotal==0){ echo $cantidadVencidosCRIOTotal; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','0','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosCRIOTotal; ?></button><?php } ?></td>
                                        										
										<?php $TipoHem='PQ';$Estado='1';$Vencido='0'; //cantidadVencidosPQOPos ?>
                                         <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQOPos==0){ echo $cantidadVencidosPQOPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoOPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQOPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQONeg==0){ echo $cantidadVencidosPQONeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoONeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQONeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQAPos==0){ echo $cantidadVencidosPQAPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoAPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQAPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQANeg==0){ echo $cantidadVencidosPQANeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoANeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQANeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQBPos==0){ echo $cantidadVencidosPQBPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQBPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQBNeg==0){ echo $cantidadVencidosPQBNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoBNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQBNeg; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQABPos==0){ echo $cantidadVencidosPQABPos; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABPos ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQABPos; ?></button><?php } ?></td>
                                        <td align="center" bgcolor="#FF6666"> <?php if($cantidadVencidosPQABNeg==0){ echo $cantidadVencidosPQABNeg; }else{ ?><button type="button" class="btn btn-danger btn-circle" onClick="VerDetalle('<?php echo $TipoHem ?>','<?php echo $IdGrupoSanguineoABNeg ?>','<?php echo $Estado ?>','<?php echo $Vencido ?>')"><?php echo $cantidadVencidosPQABNeg; ?></button><?php } ?></td>
                                      </tr>
									  
                                      <tr  class="text-center">
                                        <td colspan="2"><strong>TOTAL</strong></td>
                                        <td align="center"><?php echo $TotalPGOPos; ?></td>
                                        <td align="center"><?php echo $TotalPGONeg; ?></td>
                                        <td align="center"><?php echo $TotalPGAPos; ?></td>
                                        <td align="center"><?php echo $TotalPGANeg; ?></td>
                                        <td align="center"><?php echo $TotalPGBPos; ?></td>
                                        <td align="center"><?php echo $TotalPGBNeg; ?></td>
                                        <td align="center"><?php echo $TotalPGABPos; ?></td>
                                        <td align="center"><?php echo $TotalPGABNeg; ?></td>
                                        <td align="center"><?php echo $TotalPFCOPos; ?></td>
                                        <td align="center"><?php echo $TotalPFCONeg; ?></td>
                                        <td align="center"><?php echo $TotalPFCAPos; ?></td>
                                        <td align="center"><?php echo $TotalPFCANeg; ?></td>
                                        <td align="center"><?php echo $TotalPFCBPos; ?></td>
                                        <td align="center"><?php echo $TotalPFCBNeg; ?></td>
                                        <td align="center"><?php echo $TotalPFCABPos; ?></td>
                                        <td align="center"><?php echo $TotalPFCABNeg; ?></td>
                                            <td align="center"><?php echo $cantidadCRIOTotal; ?></td>
                                            <td align="center"><?php echo $TotalPQOPos; ?></td>
                                            <td align="center"><?php echo $TotalPQONeg; ?></td>
                                            <td align="center"><?php echo $TotalPQAPos; ?></td>
                                            <td align="center"><?php echo $TotalPQANeg; ?></td>
                                            <td align="center"><?php echo $TotalPQBPos; ?></td>
                                            <td align="center"><?php echo $TotalPQBNeg; ?></td>
                                            <td align="center"><?php echo $TotalPQABPos; ?></td>
                                            <td align="center"><?php echo $TotalPQABNeg; ?></td>
                                      </tr>                                                                      
                                </table>                             
                               
                          </div>
                          <!-- /.table-responsive -->                          
						
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class=""><!--col-lg-12-->
                    <div class="panel panel-primary"> <!--panel-default-->
                        <div class="panel-heading">Movimientos Hemocomponentes</div>
                        <div class="panel-body">
                              <a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=EnviarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>"><p><strong>1. Enviar Hemocomponentes</strong></p></a>                        
                              <a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RecibirHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>"><p><strong>2. Recibir Hemocomponentes</strong></p></a>                              
                              <a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=EliminarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>"><p><strong>3. Eliminar Hemocomponentes y Preparar Crioprecipitado</strong></p></a> 
                              <a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=FraccionarHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>"><p><strong>4. Fracción Pediátrica</strong></p></a>   
                              <a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=CustodiaHemocomponentes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>"><p><strong>5. Custodia Hemocomponentes</strong></p>	</a>   			                   
                              <a href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=AgregarQuitarReserva&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>"><p><strong>6. Agregar/Quitar Reserva</strong></p></a>                        
                              			
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - alertify - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
    
    <!-- Fecha -->     
    <script src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>       
	

</body>

</html>
