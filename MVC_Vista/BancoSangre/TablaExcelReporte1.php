<?php 
include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

	$objPHPExcel = new PHPExcel();	
				
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:T1');
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Datos del Donante/Postulante');
			
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('U1:AA1');
	$objPHPExcel->getActiveSheet()->setCellValue('U1', 'Datos del Paciente/Receptor');	
	
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AC1:AJ1');	
	$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'Fraccionamiento');	
	
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL1:AZ1');
	$objPHPExcel->getActiveSheet()->setCellValue('AL1', 'Tamizaje del Donante');

	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BA1:BM1');
	$objPHPExcel->getActiveSheet()->setCellValue('BA1', 'Prueba Confirmatoria');	
	
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BO1:BS1');
	$objPHPExcel->getActiveSheet()->setCellValue('BO1', 'Verificacion Aptos');
	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', "Nro Postulación");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', "A");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', "M");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', "D");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', "Nro Donación");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', "DNI");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G2', "A. Paterno");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H2', "A. Materno");			
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I2', "Nombres");		
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J2', "Grupo");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K2', "Sexo");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L2', "Edad");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M2', "Tipo Donacion");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N2', "Tipo Donante");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O2', "Condición Donante");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P2', "Distrito");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q2', "Dirección Donante");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R2', "Teléfono Donante");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S2', "Diferimiento (Eval.Predonacion)");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T2', "Diferimiento (extracción)");
	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U2', "HCL Paciente");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V2', "A.Paterno Pcte");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W2', "A.Materno Pcte");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X2', "Nombres Pcte");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y2', "Grupo");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z2', "Hospital Procede");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA2', "Resp.Eval.Predon.");
	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC2', "PG(vol)");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD2', "PFC(vol)");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE2', "PQ(vol)");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF2', "N°Tubuladura");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AG2', "Motivo elimina PG");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH2', "Motivo elimina PFC");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AI2', "Motivo elimina PQ");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ2', "Responsable");
	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL2', "1 HIV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AM2', "2 HTLV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN2', "3 HBsAg");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AO2', "4 CORE HB");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP2', "5 HVC");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AQ2', "6 SIFILIS");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR2', "7 CHAGAS");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AS2', "NAT HIV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT2', "NAT HBV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AU2', "NAT HCV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV2', "REACTIVO");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AW2', "Responsable");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX2', "SNCS");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AY2', "Motivo Falta Resultado");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ2', "Observación");

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BA2', "1 HIV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB2', "2 HTLV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BC2', "3 HBsAg");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD2', "4 CORE HB");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BE2', "5 HVC");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF2', "6 SIFILIS");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BG2', "7 CHAGAS");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH2', "NAT HIV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BI2', "NAT HBV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ2', "NAT HCV");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BK2', "REACTIVO");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL2', "Responsable");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BM2', "Fecha Prueba");
	
	//VERIFICACION DE GRUPO SANGUINEO	corrigiò? Grupo Sanguineo	Fenotipo Rh-kell	Coombs indirecto (Rastreo de anticuerpos)	Coombs Directo

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BO2', "Verifica GrupoSang.");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP2', "Corrigió G.S.?"); 
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BQ2', "Fenotipo Rh-kell");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BR2', "Coombs Indirecto");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BS2', "Coombs Directo");  
				
			$resultados = ReporteGeneralM($fecha_inicio,$fecha_fin); //Prueba	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {					 
				 
				 if($resultados[$i]['FechaNacimiento']!=NULL){
				 	$FechaNacimientox = time() - strtotime($resultados[$i]['FechaNacimiento']);
				 	$edad = floor((($FechaNacimientox / 3600) / 24) / 360);	
				 }else{
					$edad = ""; 
				 }
				 
				 $IdTipoSexo=$resultados[$i]["IdTipoSexo"];
				 if($IdTipoSexo=='1'){
					 $Sexo='M';
				 }else if($IdTipoSexo=='2'){
					 $Sexo='F';
				 }else{
					$Sexo=''; 
				 }
				 
				 $MotivoRechazo=$resultados[$i]["MotivoRechazo"];
				 $Medicacion=$resultados[$i]["Medicacion"];
				 $MotivoDiferimientoEP=$MotivoRechazo.' '.$Medicacion;
				 
				 $MotivoRechazoDurante=$resultados[$i]["MotivoRechazoDurante"];
				 $MotivoRechazoDespues=$resultados[$i]["MotivoRechazoDespues"];
				 $MotivoRechazoExtraccion=$MotivoRechazoDurante.' '.$MotivoRechazoDespues;
				 
				 $NroDonacion=$resultados[$i]["NroDonacion"];
				 if($NroDonacion!=NULL){
					 $NroDonacion='DAC'.$resultados[$i]["NroDonacion"];
				 }else{
					 $NroDonacion='';
				 }
				 
				 $quimioVIH=$resultados[$i]["quimioVIH"]; 
				 if($quimioVIH=='1'){
					$DquimioVIH='REACTIVO'; 
				 }else if($quimioVIH=='0'){
					$DquimioVIH='no reactivo';  
				 }else{
					$DquimioVIH='';  
				 }
				 
				 $quimioSifilis=$resultados[$i]["quimioSifilis"]; 
				 if($quimioSifilis=='1'){
					$DquimioSifilis='REACTIVO'; 
				 }else if($quimioSifilis=='0'){
					$DquimioSifilis='no reactivo';  
				 }else{
					$DquimioSifilis='';  
				 }
				 
				 $quimioHTLV=$resultados[$i]["quimioHTLV"]; 
				 if($quimioHTLV=='1'){
					$DquimioHTLV='REACTIVO'; 
				 }else if($quimioHTLV=='0'){
					$DquimioHTLV='no reactivo';  
				 }else{
					$DquimioHTLV='';  
				 }
				 
				 $quimioANTICHAGAS=$resultados[$i]["quimioANTICHAGAS"];
				 if($quimioANTICHAGAS=='1'){
					$DquimioANTICHAGAS='REACTIVO'; 
				 }else if($quimioANTICHAGAS=='0'){
					$DquimioANTICHAGAS='no reactivo';  
				 }else{
					$DquimioANTICHAGAS='';  
				 }
				  
				 $quimioHBS=$resultados[$i]["quimioHBS"]; 
				 if($quimioHBS=='1'){
					$DquimioHBS='REACTIVO'; 
				 }else if($quimioHBS=='0'){
					$DquimioHBS='no reactivo';  
				 }else{
					$DquimioHBS='';  
				 }
				 
				 $quimioVHB=$resultados[$i]["quimioVHB"];
				 if($quimioVHB=='1'){
					$DquimioVHB='REACTIVO'; 
				 }else if($quimioVHB=='0'){
					$DquimioVHB='no reactivo';  
				 }else{
					$DquimioVHB='';  
				 }
				  
				 $quimioVHC=$resultados[$i]["quimioVHC"]; 
				 if($quimioVHC=='1'){
					$DquimioVHC='REACTIVO'; 
				 }else if($quimioVHC=='0'){
					$DquimioVHC='no reactivo';  
				 }else{
					$DquimioVHC='';  
				 }
				 
				 $natVIH=$resultados[$i]["natVIH"];
				 if($natVIH=='1'){
					$DnatVIH='REACTIVO'; 
				 }else if($natVIH=='0'){
					$DnatVIH='no reactivo';  
				 }else{
					$DnatVIH='';  
				 }
				  
				 $natHBV=$resultados[$i]["natHBV"]; 
				 if($natHBV=='1'){
					$DnatHBV='REACTIVO'; 
				 }else if($natHBV=='0'){
					$DnatHBV='no reactivo';  
				 }else{
					$DnatHBV='';  
				 }
				 
				 $natHCV=$resultados[$i]["natHCV"];
				 if($natHCV=='1'){
					$DnatHCV='REACTIVO'; 
				 }else if($natHCV=='0'){
					$DnatHCV='no reactivo';  
				 }else{
					$DnatHCV='';  
				 }		
				 
				 $EstadoAptoTamizaje=$resultados[$i]["EstadoAptoTamizaje"];
				 if(trim($EstadoAptoTamizaje)=='' && $resultados[$i]["IdTamizaje"]!=''){
					$ReactivoTamizaje='F';
				 }else if($EstadoAptoTamizaje=='0'){//NO APTO
					$ReactivoTamizaje='SI'; //SI REACTIVO
				 }else if($EstadoAptoTamizaje=='1'){//APTO
					$ReactivoTamizaje='NO'; //NO REACTIVO
				 }else{//AUN NO SE REGISTRA
					$ReactivoTamizaje=''; 
				 }		

				 //PRUEBA CONFIRMATORIA
				 ////Estado Definitivo Donante
				 $NroDocumento=$resultados[$i]["NroDocumento"];
				 $BuscarDonante=SIGESA_BSD_ListarDonantesEstadoDefinitivoM($NroDocumento);
				 $EstadoDefinitivo=$BuscarDonante[0]["EstadoDefinitivo"]; //APTO, NO APTO
				 $cantidadPruebaConfirmaReactivo=$BuscarDonante[0]["cantidadPruebaConfirmaReactivo"];//Cantidad Examenes Reactivos Confirmatorios del Donante				 

				 if($cantidadPruebaConfirmaReactivo>0){//NO APTO
					$ReactivoDefinitivo='SI'; //SI REACTIVO
				 }else if($EstadoDefinitivo=='APTO'){//APTO
					$ReactivoDefinitivo='NO'; //NO REACTIVO
				 }else{//NO SE REGISTRA
					$ReactivoDefinitivo=''; 
				 }

				 //Primer Responsable y la primera fecha de la PruebaConfirmatoria
				 $UsuarioPrueba="";
				 $FechaPrueba="";	

				 //Buscar PruebaConfirmatoria quimioVIH
				 $PruebaReactivo1='quimioVIH';
				 $Reactivo1=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo1);
				 if($Reactivo1!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo1[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo1[0]["FechaPrueba"];
				 	$quimioVIH1= $Reactivo1[0]["ResultadoFinal"];
				 	if($quimioVIH1=='1'){
						$DquimioVIH1='REACTIVO';					
					}else if($quimioVIH1=='0'){
						$DquimioVIH1='no reactivo';  
					}else{
						$DquimioVIH1='';  
					}
				 }else{//end if Reactivo1				 	
				 	$DquimioVIH1='';
				 }

				 //Buscar PruebaConfirmatoria quimioSifilis
				 $PruebaReactivo2='quimioSifilis';
				 $Reactivo2=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo2);
				 if($Reactivo2!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo2[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo2[0]["FechaPrueba"];
				 	$quimioSifilis1= $Reactivo2[0]["ResultadoFinal"];
				 	if($quimioSifilis1=='1'){
						$DquimioSifilis1='REACTIVO';					
					}else if($quimioSifilis1=='0'){
						$DquimioSifilis1='no reactivo';  
					}else{
						$DquimioSifilis1='';  
					}
				 }else{//end if Reactivo2
				 	$DquimioSifilis1='';
				 }

				 //Buscar PruebaConfirmatoria quimioHTLV
				 $PruebaReactivo3='quimioHTLV';
				 $Reactivo3=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo3);
				 if($Reactivo3!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo3[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo3[0]["FechaPrueba"];
				 	$quimioHTLV1= $Reactivo3[0]["ResultadoFinal"];
				 	if($quimioHTLV1=='1'){
						$DquimioHTLV1='REACTIVO';					
					}else if($quimioHTLV1=='0'){
						$DquimioHTLV1='no reactivo';  
					}else{
						$DquimioHTLV1='';  
					}
				 }else{//end if Reactivo3
				 	$DquimioHTLV1='';
				 }

				 //Buscar PruebaConfirmatoria quimioANTICHAGAS
				 $PruebaReactivo4='quimioANTICHAGAS';
				 $Reactivo4=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo4);
				 if($Reactivo4!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo4[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo4[0]["FechaPrueba"];
				 	$quimioANTICHAGAS1= $Reactivo4[0]["ResultadoFinal"];
				 	if($quimioANTICHAGAS1=='1'){
						$DquimioANTICHAGAS1='REACTIVO';					
					}else if($quimioANTICHAGAS1=='0'){
						$DquimioANTICHAGAS1='no reactivo';  
					}else{
						$DquimioANTICHAGAS1='';  
					}
				 }else{//end if Reactivo4
				 	$DquimioANTICHAGAS1='';
				 }

				 //Buscar PruebaConfirmatoria quimioHBS
				 $PruebaReactivo5='quimioHBS';
				 $Reactivo5=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo5);
				 if($Reactivo5!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo5[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo5[0]["FechaPrueba"];
				 	$quimioHBS1= $Reactivo5[0]["ResultadoFinal"];
				 	if($quimioHBS1=='1'){
						$DquimioHBS1='REACTIVO';					
					}else if($quimioHBS1=='0'){
						$DquimioHBS1='no reactivo';  
					}else{
						$DquimioHBS1='';  
					}
				 }else{//end if Reactivo5
				 	$DquimioHBS1='';
				 }

				 //Buscar PruebaConfirmatoria quimioVHB
				 $PruebaReactivo6='quimioVHB';
				 $Reactivo6=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo6);
				 if($Reactivo6!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo6[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo6[0]["FechaPrueba"];
				 	$quimioVHB1= $Reactivo6[0]["ResultadoFinal"];
				 	if($quimioVHB1=='1'){
						$DquimioVHB1='REACTIVO';					
					}else if($quimioVHB1=='0'){
						$DquimioVHB1='no reactivo';  
					}else{
						$DquimioVHB1='';  
					}
				 }else{//end if Reactivo6
				 	$DquimioVHB1='';
				 }

				 //Buscar PruebaConfirmatoria quimioVHC
				 $PruebaReactivo7='quimioVHC';
				 $Reactivo7=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo7);
				 if($Reactivo7!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo7[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo7[0]["FechaPrueba"];
				 	$quimioVHC1= $Reactivo7[0]["ResultadoFinal"];
				 	if($quimioVHC1=='1'){
						$DquimioVHC1='REACTIVO';					
					}else if($quimioVHC1=='0'){
						$DquimioVHC1='no reactivo';  
					}else{
						$DquimioVHC1='';  
					}
				 }else{//end if Reactivo7
				 	$DquimioVHC1='';
				 }

				 //Buscar PruebaConfirmatoria natVIH
				 $PruebaReactivo8='natVIH';
				 $Reactivo8=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo8);
				 if($Reactivo8!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo8[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo8[0]["FechaPrueba"];
				 	$natVIH1= $Reactivo8[0]["ResultadoFinal"];
				 	if($natVIH1=='1'){
						$DnatVIH1='REACTIVO';					
					}else if($natVIH1=='0'){
						$DnatVIH1='no reactivo';  
					}else{
						$DnatVIH1='';  
					}
				 }else{//end if Reactivo8
				 	$DnatVIH1='';
				 }

				 //Buscar PruebaConfirmatoria natHBV
				 $PruebaReactivo9='natHBV';
				 $Reactivo9=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo9);
				 if($Reactivo9!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo9[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo9[0]["FechaPrueba"];
				 	$natHBV1= $Reactivo9[0]["ResultadoFinal"];
				 	if($natHBV1=='1'){
						$DnatHBV1='REACTIVO';					
					}else if($natHBV1=='0'){
						$DnatHBV1='no reactivo';  
					}else{
						$DnatHBV1='';  
					}
				 }else{//end if Reactivo9
				 	$DnatHBV1='';
				 }

				 //Buscar PruebaConfirmatoria natHCV
				 $PruebaReactivo10='natHCV';
				 $Reactivo10=SIGESA_BuscarPruebaConfirmatoriaM($NroDocumento,$PruebaReactivo10);
				 if($Reactivo10!=NULL){				 	
				 	$UsuarioPrueba= $Reactivo10[0]["ResponsablePrueba"];
				 	$FechaPrueba= $Reactivo10[0]["FechaPrueba"];
				 	$natHCV1= $Reactivo10[0]["ResultadoFinal"];
				 	if($natHCV1=='1'){
						$DnatHCV1='REACTIVO';					
					}else if($natHCV1=='0'){
						$DnatHCV1='no reactivo';  
					}else{
						$DnatHCV1='';  
					}
				 }else{//end if Reactivo10
				 	$DnatHCV1='';
				 }				 
				 	 
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+3), 'PDAC'.$resultados[$i]["NroMovimiento"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+3), $resultados[$i]["yearRecepcion"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+3), $resultados[$i]["monthRecepcion"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+3), $resultados[$i]["dayRecepcion"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+3), $NroDonacion);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+3), $resultados[$i]["NroDocumento"]);				 													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+3), $resultados[$i]["ApellidoPaterno"]);				 						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+3), $resultados[$i]["ApellidoMaterno"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+3), $resultados[$i]["NombresPostulante"]);				 				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+3), $resultados[$i]["GrupoSanguineoPostulante"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+3), $Sexo);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+3), $edad);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+3), $resultados[$i]["TipoDonacion"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.($i+3), $resultados[$i]["TipoDonante"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.($i+3), $resultados[$i]["CondicionDonante"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.($i+3), $resultados[$i]["DistritoDomicilio"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.($i+3), $resultados[$i]["DireccionDomicilio"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.($i+3), $resultados[$i]["Telefono"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.($i+3), $MotivoDiferimientoEP);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.($i+3), $MotivoRechazoExtraccion);	
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.($i+3), $resultados[$i]["NroHistoria"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.($i+3), $resultados[$i]["ApellidoPaternoPaciente"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.($i+3), $resultados[$i]["ApellidoMaternoPaciente"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.($i+3), $resultados[$i]["NombresPaciente"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.($i+3), $resultados[$i]["GrupoSanguineoPaciente"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.($i+3), $resultados[$i]["HospitalProcedencia"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA'.($i+3), strtolower($resultados[$i]["Entrevistador"]));	
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC'.($i+3), $resultados[$i]["VolumenPaqueteGlobu"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.($i+3), $resultados[$i]["VolumenPlasma"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE'.($i+3), $resultados[$i]["VolumenPlaquetas"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.($i+3), $resultados[$i]["NroTabuladora"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AG'.($i+3), $resultados[$i]["DesMotivoElimPaqueteGlobu"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.($i+3), $resultados[$i]["DesMotivoElimPlasma"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AI'.($i+3), $resultados[$i]["DesMotivoElimPlaquetas"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.($i+3), strtolower($resultados[$i]["ResponsableFraccionamiento"]));
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.($i+3), ($DquimioVIH));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AM'.($i+3), ($DquimioHTLV));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.($i+3), ($DquimioHBS));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AO'.($i+3), ($DquimioVHB));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.($i+3), ($DquimioVHC));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AQ'.($i+3), ($DquimioSifilis));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.($i+3), ($DquimioANTICHAGAS));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AS'.($i+3), ($DnatVIH));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.($i+3), ($DnatHBV));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AU'.($i+3), ($DnatHCV));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.($i+3), ($ReactivoTamizaje));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AW'.($i+3), ($resultados[$i]["ResponsableTamizaje"]));			 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.($i+3), ($resultados[$i]["SNCS"]));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AY'.($i+3), ($resultados[$i]["DesMotivoElimMuestra"]));	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.($i+3), ($resultados[$i]["Observacion"]));

				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BA'.($i+3), ($DquimioVIH1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.($i+3), ($DquimioHTLV1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BC'.($i+3), ($DquimioHBS1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.($i+3), ($DquimioVHB1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BE'.($i+3), ($DquimioVHC1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.($i+3), ($DquimioSifilis1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BG'.($i+3), ($DquimioANTICHAGAS1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.($i+3), ($DnatVIH1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BI'.($i+3), ($DnatHBV1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.($i+3), ($DnatHCV1));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BK'.($i+3), ($ReactivoDefinitivo));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.($i+3), ($UsuarioPrueba));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BM'.($i+3), ($FechaPrueba));
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BO'.($i+3), ($resultados[$i]["GrupoSanguineoVerificado"]));				 
				 if($resultados[$i]["IdGrupoSanguineoVerificado"]!=""){
					if($resultados[$i]["IdGrupoSanguineoVerificado"]!=$resultados[$i]["IdGrupoSanguineoAnterior"]){
						$corrigio='SI';					
					}else{
						$corrigio='NO';  
					}				
				 }else{
					$corrigio='';  
				 }
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.($i+3), ($corrigio));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BQ'.($i+3), ($resultados[$i]["Fenotipo"]));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BR'.($i+3), ($resultados[$i]["DeteccionCI"]));
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BS'.($i+3), ($resultados[$i]["CoombsDirecto"]));
				 
				 //SI ES REACTIVO QUE SALGA EN LETRA ROJA
				 $styleArray = array(
					 'font'  => array(
						'bold'  => true,
						'color' => array('rgb' => 'FF0000'),
					 ),
					 'alignment' => array(
            		 	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			)				 
				 );				
				
				 if($quimioVIH=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AL'.($i+3))->applyFromArray($styleArray);
				 }
				 if($quimioHTLV=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AM'.($i+3))->applyFromArray($styleArray);
				 }
				 if($quimioHBS=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AN'.($i+3))->applyFromArray($styleArray);
				 }
				 if($quimioVHB=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AO'.($i+3))->applyFromArray($styleArray);
				 }
				 if($quimioVHC=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AP'.($i+3))->applyFromArray($styleArray);
				 }
				 if($quimioSifilis=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AQ'.($i+3))->applyFromArray($styleArray);
				 }
				 if($quimioANTICHAGAS=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AR'.($i+3))->applyFromArray($styleArray);
				 }
				 if($natVIH=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AS'.($i+3))->applyFromArray($styleArray);
				 }
				 if($natHBV=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AT'.($i+3))->applyFromArray($styleArray);
				 }
				 if($natHCV=='1'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AU'.($i+3))->applyFromArray($styleArray);
				 }
				 
				 if($ReactivoTamizaje=='SI'){   
					 $objPHPExcel->getActiveSheet()->getStyle('AV'.($i+3))->applyFromArray($styleArray);
				 }

				 if($ReactivoDefinitivo=='SI'){   
					 $objPHPExcel->getActiveSheet()->getStyle('BK'.($i+3))->applyFromArray($styleArray);
				 }
		 }
	   }	   
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('REPORTE'); 		
		 $objActSheet->getColumnDimension('B')->setWidth(7);	
		 $objActSheet->getColumnDimension('C')->setWidth(5);		
		 $objActSheet->getColumnDimension('D')->setWidth(5);
		 $objActSheet->getColumnDimension('I')->setWidth(20);		
		
		 $objActSheet->getColumnDimension('J')->setWidth(7);
		 $objActSheet->getColumnDimension('K')->setWidth(7);  
		 $objActSheet->getColumnDimension('L')->setWidth(7);
		  
		 $objActSheet->getColumnDimension('Q')->setWidth(50);//DireccionDomicilio
		 $objActSheet->getColumnDimension('S')->setWidth(50);//MotivoDiferimientoEP
		 $objActSheet->getColumnDimension('T')->setWidth(20);//MotivoRechazoExtraccion
		 
		 $objActSheet->getColumnDimension('X')->setWidth(20);//NombresPaciente
		 $objActSheet->getColumnDimension('Y')->setWidth(7);//GrupoSanguineoPaciente
		 
		 $objActSheet->getColumnDimension('AY')->setWidth(20);//Motivo Falta Resultado	 
		 $objActSheet->getColumnDimension('AZ')->setWidth(50);//Observacion
		 
	
	
//Formato General
//El ancho de columna
//$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(13); 
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);	
//$objPHPExcel->getActiveSheet()->getHighestRow()->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//$objPHPExcel->getDefaultStyle()->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$borderArray=array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		)
	)
);
	
$objPHPExcel->getActiveSheet()->getStyle("A1:AA".($i+2))->applyFromArray($borderArray);
$objPHPExcel->getActiveSheet()->getStyle("AC1:AJ".($i+2))->applyFromArray($borderArray);
$objPHPExcel->getActiveSheet()->getStyle("AL1:AZ".($i+2))->applyFromArray($borderArray);
$objPHPExcel->getActiveSheet()->getStyle("BA1:BM".($i+2))->applyFromArray($borderArray);

$objPHPExcel->getActiveSheet()->getStyle("BO1:BS".($i+2))->applyFromArray($borderArray);
	 	 
//Formato primera Fila 
    $objStyleA1 = $objActSheet ->getStyle('A1:BS2');
    //$objStyleA1 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 	
	//Configuración de tipos de letra 
    $objFontA1 = $objStyleA1->getFont(); 
    $objFontA1->setName('Arial'); 
    $objFontA1->setSize(11); 
    $objFontA1->setBold(true); 
    //$objFontA1->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);  
    //$objFontA1 ->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE); //setARGB('FFFF00') ;		
	//Establecer la alineación 
    $objAlignA1 = $objStyleA1->getAlignment(); 
	$objAlignA1->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignA1->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	//Ajustar Texto
	$objAlignA1->setWrapText(true);   
	
	//background Primera y Segunda Fila
	$objActSheet->getStyle('A1:T2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'b0ecf6')
            )
        )
    );	
	$objActSheet->getStyle('U1:AA2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'adfb5f')
            )
        )
    );	
	$objActSheet->getStyle('AC1:AJ2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '5ffbef')
            )
        )
    );
	$objActSheet->getStyle('AL1:AZ2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f8fc83')
            )
        )
    );	
	$objActSheet->getStyle('BA1:BM2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffc600')
            )
        )
    );	
	$objActSheet->getStyle('BO1:BS2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'b0ecf6')
            )
        )
    );

 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="ReporteGeneralDonantes.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
?>
			
  