<?php 
include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

	$objPHPExcel = new PHPExcel();	
				
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SOLICITUD');
			
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I1:N1');
	$objPHPExcel->getActiveSheet()->setCellValue('I1', 'PACIENTE O RECEPTOR');	
	
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O1:T1');
	$objPHPExcel->getActiveSheet()->setCellValue('O1', 'UNIDADES COMPATIBLES TRANSFUSIÓN');	
	
	
	/*$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AC1:AJ1');	
	$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'Unidades Compatibles Transfusión');*/		
		
	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', "Codigo");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', "Fecha");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', "Hora");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', "Fecha y Hora Reserva");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', "Personal Despacha");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', "Fecha y Hora Despacha");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G2', "Personal Recibe");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H2', "Cuenta");
			
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I2', "HCL paciente");		
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J2', "Apellidos y Nombres del paciente");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K2', "Fecha nacimiento");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L2', "DNI");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M2', "Sexo");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N2', "Servicio");	
	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O2', "SNC, LOTE");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P2', "Puntuación");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q2', "B.U.T (regreso?)");	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R2', "Reacción Transfusional");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S2', "Tipo de Reacción transfusional");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T2', "Observaciones");	
				
			$resultados = ReporteGeneralTransfusionesM($fecha_inicio,$fecha_fin); //Prueba	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {	
				
				 $FechaSolicitud=isset($resultados[$i]['FechaSolicitud']) ? (vfecha(substr($resultados[$i]['FechaSolicitud'],0,10))) : '';		
				 $HoraSolicitud=isset($resultados[$i]['FechaSolicitud']) ? (substr($resultados[$i]['FechaSolicitud'],11,5)) : '';
				 
				 $FechaSolicitud=isset($resultados[$i]['FechaSolicitud']) ? (vfecha(substr($resultados[$i]['FechaSolicitud'],0,10))) : '';		
				 $HoraSolicitud=isset($resultados[$i]['FechaSolicitud']) ? (substr($resultados[$i]['FechaSolicitud'],11,5)) : '';	
				 
				 $IdSolicitudSangre=$resultados[$i]['IdSolicitudSangre'];
				 $IdDetSolicitudSangre=$resultados[$i]['IdDetSolicitudSangre'];
				 
				 //$DatosDonante=ImprimirSolicitudDatosDonante($IdSolicitudSangre,$IdDetSolicitudSangre);
				 
				 $FechaReservaX=$resultados[$i]["FechaReserva"];
				 $FechaReserva=empty($FechaReservaX) ? '' : vfecha(substr($FechaReservaX,0,10));
			 	 $HoraReserva=substr($FechaReservaX,11,5);				
				
			   	$cmpReserva=$resultados[$i]["cmpReserva"];
			  	$UsuReserva=$resultados[$i]["UsuReserva"];
				$ListarUsuario2=ListarUsuarioxIdempleado_M($UsuReserva);
				$NombreUsuReserva=$ListarUsuario2[0]["ApellidoPaterno"].' '.$ListarUsuario2[0]["ApellidoMaterno"].' '.$ListarUsuario2[0]["Nombres"];

				$FechaDespachoX=$resultados[$i]["FechaDespacho"];
				$FechaDespacho=empty($FechaDespachoX) ? '' : vfecha(substr($FechaDespachoX,0,10));
				$HoraDespacho=substr($FechaDespachoX,11,5);
			
				$UsuDespacho=$resultados[$i]["UsuDespacho"];
				$ListarUsuario1=ListarUsuarioxIdempleado_M($UsuDespacho);
				$NombreUsuDespacho=$ListarUsuario1[0]["ApellidoPaterno"].' '.$ListarUsuario1[0]["ApellidoMaterno"].' '.$ListarUsuario1[0]["Nombres"];				
				
				$SNCS=$resultados[$i]["SNCS"]; 
				
				if($SNCS!=NULL){
					$SNCSNroDonacionF=$SNCS.' (DAC'.$resultados[$i]["NroDonacion"].')'; 
				}else{
					$SNCSNroDonacionF=''; 
				}
				
				$RegresoBUT=$resultados[$i]["RegresoBUT"];	
				
				if($RegresoBUT=="1"){
					$RegresoBUT='SI';
				}else{
					$RegresoBUT='NO';
				}			
			
				$Estado=$resultados[$i]["Estado"];
				$EstadoDescripcion=strip_tags(DescripcionEstadoSolicitud($Estado));	 
				
				
				$datosPaciente=SIGESA_BSD_Buscarpaciente_M($resultados[$i]["IdPaciente"],'IdPaciente');
			
				if($datosPaciente!=""){
					$Paciente=$datosPaciente[0]["ApellidoPaterno"].' '.$datosPaciente[0]["ApellidoMaterno"].' '.$datosPaciente[0]["PrimerNombre"];		
					$FechaNacimientoX=$datosPaciente[0]["FechaNacimiento"];		
					$FechaNacimiento=empty($FechaNacimientoX) ? '' : vfecha(substr($FechaNacimientoX,0,10));
					$NroDocumento=$datosPaciente[0]["NroDocumento"];
					if($datosPaciente[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($datosPaciente[0]["IdTipoSexo"]==2){$Sexo='Femenino';}   		
				}else{
					$Paciente='';
					$FechaNacimiento='';	
					$NroDocumento='';	
					$Sexo='';		
				}
				
				$Servicio=$resultados[$i]["Servicio"];
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+3), $resultados[$i]["NroSolicitud"].'-'.$resultados[$i]["CodigoComponente"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+3), $FechaSolicitud );						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+3), $HoraSolicitud);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+3), $FechaReserva.' '.$HoraReserva);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+3), $NombreUsuDespacho);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+3), $FechaDespacho.' '.$HoraDespacho);				 													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+3), "");
				 				 						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+3), $resultados[$i]["IdCuentaAtencion"]);	
				 			 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+3), $resultados[$i]["NroHistoria"]);			 				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+3), $Paciente);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+3), $FechaNacimiento);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+3), $NroDocumento);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+3), $Sexo);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.($i+3), $Servicio);
				 
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.($i+3), $SNCSNroDonacionF);//085858(DAC1804040)	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.($i+3), $resultados[$i]["Puntuacion"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.($i+3), $RegresoBUT);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.($i+3), "");	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.($i+3), "");	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.($i+3), "");			 
				 
		 }
	   }	   
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('REPORTE'); 		
		 $objActSheet->getColumnDimension('B')->setWidth(10);	
		 $objActSheet->getColumnDimension('C')->setWidth(7);		
		 $objActSheet->getColumnDimension('D')->setWidth(18);
		 
		 $objActSheet->getColumnDimension('E')->setWidth(25);		
		 
		 $objActSheet->getColumnDimension('F')->setWidth(18);
		 $objActSheet->getColumnDimension('G')->setWidth(25);
		  
		 $objActSheet->getColumnDimension('I')->setWidth(15);		
		
		 $objActSheet->getColumnDimension('J')->setWidth(40);
		 $objActSheet->getColumnDimension('K')->setWidth(15);  
		 $objActSheet->getColumnDimension('L')->setWidth(15);
		 $objActSheet->getColumnDimension('M')->setWidth(15);
		 $objActSheet->getColumnDimension('N')->setWidth(30);		 
		 
	
	
//Formato General
//El ancho de columna
//$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(15); 
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);	
//$objPHPExcel->getActiveSheet()->getHighestRow()->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//$objPHPExcel->getDefaultStyle()->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$borderArray=array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '000000')
		)
	)
);
	
$objPHPExcel->getActiveSheet()->getStyle("A1:T".($i+2))->applyFromArray($borderArray);
//$objPHPExcel->getActiveSheet()->getStyle("AC1:AJ".($i+2))->applyFromArray($borderArray);
//$objPHPExcel->getActiveSheet()->getStyle("AL1:AZ".($i+2))->applyFromArray($borderArray);
//$objPHPExcel->getActiveSheet()->getStyle("BA1:BM".($i+2))->applyFromArray($borderArray);
	 	 
//Formato primera Fila 
    $objStyleA1 = $objActSheet ->getStyle('A1:T2');
    //$objStyleA1 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 	
	//Configuración de tipos de letra 
    $objFontA1 = $objStyleA1->getFont(); 
    $objFontA1->setName('Arial'); 
    $objFontA1->setSize(11); 
    $objFontA1->setBold(true); 
    //$objFontA1->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);  
    //$objFontA1 ->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE); //setARGB('FFFF00') ;		
	//Establecer la alineación 
    $objAlignA1 = $objStyleA1->getAlignment(); 
	$objAlignA1->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignA1->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	//Ajustar Texto
	$objAlignA1->setWrapText(true);   
	
	//background Primera Fila
	$objActSheet->getStyle('A1:H2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'b0ecf6')
            )
        )
    );	
	
	//background Segunda Fila
	$objActSheet->getStyle('I1:N2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'adfb5f')
            )
        )
    );	
	$objActSheet->getStyle('O1:T2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '5ffbef')
            )
        )
    );	
	/*$objActSheet->getStyle('AC1:AJ2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '5ffbef')
            )
        )
    );
	$objActSheet->getStyle('AL1:AZ2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f8fc83')
            )
        )
    );	
	$objActSheet->getStyle('BA1:BM2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffc600')
            )
        )
    );	*/

 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="ReporteGeneralTransfusiones.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
?>
			
  