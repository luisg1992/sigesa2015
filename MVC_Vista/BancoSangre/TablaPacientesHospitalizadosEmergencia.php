<?php

include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');
$objPHPExcel = new PHPExcel();

// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
$resultadosEspe=ListarServiciosHospitalizacionEmergenciaM();	
	   							 
 if($resultadosEspe!=NULL){	
	 $hoja=0;
	 $nservicio=0;	
	 for ($ies=0; $ies < count($resultadosEspe); $ies++) {
		 //echo $resultadosEspe[$ies]["Nombre"];
	
	/*if($hoja>0){
		$nueva_hoja = $objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex($hoja); // marcar como activa la nueva hoja
	}*/


$resultados = ConsultarPacientesHospitalizadosRecibieronSangre($resultadosEspe[$ies]["IdServicio"]);	
if($resultados[0]["ServicioActual"]!=NULL){
//PRIMER SERVICIO	
	if($resultados!=NULL){			
		for ($i=0; $i < count($resultados); $i++) {			
					//Cantidades
					$ObtenerCantidadPG=ObtenerCantidadTipoHem($resultados[$i]["IdPaciente"],'PG');
					$CantidadPG=$ObtenerCantidadPG[0]["Cantidad"];
					
					$ObtenerCantidadPFC=ObtenerCantidadTipoHem($resultados[$i]["IdPaciente"],'PFC');
					$CantidadPFC=$ObtenerCantidadPFC[0]["Cantidad"];
					
					$ObtenerCantidadPQ=ObtenerCantidadTipoHem($resultados[$i]["IdPaciente"],'PQ');
					$CantidadPQ=$ObtenerCantidadPQ[0]["Cantidad"];
					
					$ObtenerCantidadCRIO=ObtenerCantidadTipoHem($resultados[$i]["IdPaciente"],'CRIO');
					$CantidadCRIO=$ObtenerCantidadCRIO[0]["Cantidad"];	
					
					//Equivale Donantes
					$TotEquiDonPG=$CantidadPG;	$TotEquiDonPFC=$CantidadPFC;
					$TotEquiDonPQ=$CantidadPQ/3;$TotEquiCRIO=$CantidadCRIO/3;
					$TotEquiDonX=$TotEquiDonPG+$TotEquiDonPFC+$TotEquiDonPQ+$TotEquiCRIO;
					if(0<$TotEquiDonX && $TotEquiDonX<1){
						$TotEquiDon=1;	
					}else{
						$TotEquiDon=round($TotEquiDonX);
					}			
					
					//Depositó Donantes
					$ObtenerCantidadDonantesTraidos=ObtenerCantidadDonantesTraidos($resultados[$i]["IdPaciente"]);
					$CantidadDonantesTraidos=$ObtenerCantidadDonantesTraidos[0]["Cantidad"]; 
					
					//Traer Donantes
					$TraerDon=$TotEquiDon-$CantidadDonantesTraidos;	
					
			//PRIMERA FILA		
			$objPHPExcel->setActiveSheetIndex($hoja)->mergeCells('A1:'.'C1');
			$objPHPExcel->getActiveSheet()->setCellValue('A1', "HOSPITAL NACIONAL DANIEL A. CARRION");		
			$objPHPExcel->setActiveSheetIndex($hoja)->mergeCells('D1:'.'I1');
			$objPHPExcel->getActiveSheet()->setCellValue('D1', "DEBEN TRAER DONANTES (PACIENTES EN HOSPITALIZACIÓN Y EMERGENCIA)");	
			$objPHPExcel->setActiveSheetIndex($hoja)->mergeCells('J1:'.'N1');
			$objPHPExcel->getActiveSheet()->setCellValue('J1', "SERVICIO DE HEMOTERAPIA Y BANCO DE SANGRE");	
			
			//SEGUNDA FILA
			$objPHPExcel->setActiveSheetIndex($hoja)->mergeCells('H2:K2');
			$objPHPExcel->getActiveSheet()->setCellValue('H2', "Fecha&Hora Reporte");	
			
			$objPHPExcel->setActiveSheetIndex($hoja)->mergeCells('L2:N2');			
			$FechaHoraServidor=vfecha(substr($FechaServidor,0,10)).' '.substr($FechaServidor,11,5);		
			$objPHPExcel->getActiveSheet()->setCellValue('L2', $FechaHoraServidor);			
			
			//TERCERA
			$objPHPExcel->setActiveSheetIndex($hoja)->mergeCells('A'.($nservicio+4).':'.'N'.($nservicio+4));
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($nservicio+4),  $resultados[0]["ServicioActual"]);
			
			//CUARTA
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('A'.($nservicio+5), "Nro");
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('B'.($nservicio+5), "Cuenta");
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('C'.($nservicio+5), "PACIENTE");	 
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('D'.($nservicio+5), "H.C.");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('E'.($nservicio+5), "F.Ing.");	
			//$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F'.($nservicio+5), "F.EgrAdm");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F'.($nservicio+5), "Plan");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('G'.($nservicio+5), "Cama");	
						
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('H'.($nservicio+5), "PG");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('I'.($nservicio+5), "PFC");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('J'.($nservicio+5), "PQ");
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('K'.($nservicio+5), "CRIO");
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('L'.($nservicio+5), "Equi. Don.");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('M'.($nservicio+5), "Depositó");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('N'.($nservicio+5), "Traer");	

			 $Plan=substr($resultados[$i]["Plan"], 0 ,5);

			 //QUINTA
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('A'.($nservicio+$i+6), ($i+1));	
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('B'.($nservicio+$i+6), $resultados[$i]["IdCuentaAtencion"]);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('C'.($nservicio+$i+6), mb_strtoupper($resultados[$i]["Paciente"]));	
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('D'.($nservicio+$i+6), $resultados[$i]["NroHistoriaClinica"]);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('E'.($nservicio+$i+6), $resultados[$i]["FechaIngreso"]);

			 //$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F'.($nservicio+$i+6), $resultados[$i]["FechaEgresoAdministrativo"]);	

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F'.($nservicio+$i+6), $Plan);							
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('G'.($nservicio+$i+6), $resultados[$i]["cama"]);  						
			 
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('H'.($nservicio+$i+6), $CantidadPG);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('I'.($nservicio+$i+6), $CantidadPFC);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('J'.($nservicio+$i+6), $CantidadPQ);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('K'.($nservicio+$i+6), $CantidadCRIO);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('L'.($nservicio+$i+6), $TotEquiDon);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('M'.($nservicio+$i+6), $CantidadDonantesTraidos);
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('N'.($nservicio+$i+6), $TraerDon);
		}		 
	}	

	//Establecer la anchura 				  
	//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
	 $objPHPExcel->setActiveSheetIndex($hoja); 
	 $objActSheet = $objPHPExcel->getActiveSheet(); 

	 //El nombre de la hoja actual de las actividades
	 /*$NombreServicio=trim(substr($resultadosEspe[$ies]["Nombre"],0,30)); 
	 $objActSheet->setTitle($NombreServicio); */
	 $objActSheet->setTitle('Servicios'); //Maximum 31 characters allowed in sheet title 
	 
	 //Formato primera Fila 
	$objStyleA1 = $objActSheet ->getStyle('A1:Z1');
	//Configuración de tipos de letra 
	$objFontA1 = $objStyleA1->getFont(); 
	$objFontA1->setName('Arial'); 
	$objFontA1->setSize(9); 
	$objFontA1->setBold(true);
	//Establecer la alineación 
	$objAlignA1 = $objStyleA1->getAlignment(); 
	$objAlignA1->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	$objAlignA1->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objAlignA1->setWrapText(true); //Ajustar Texto 
	$objActSheet->getRowDimension('1')->setRowHeight(30);
	//$objActSheet->getRowDimension('1')->setRowHeight(-1);//AUTOMATICO
	
	 //Formato segunda Fila 
	$objStyleA2 = $objActSheet ->getStyle('A'.($nservicio+2).':'.'Z'.($nservicio+2));
	//Configuración de tipos de letra 
	$objFontA2 = $objStyleA2->getFont(); 
	$objFontA2->setName('Arial'); 
	$objFontA2->setSize(8); 
	//$objFontA2->setBold(true);
	//Establecer la alineación 
	$objAlignA2 = $objStyleA2->getAlignment(); 
	$objAlignA2->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	$objAlignA2->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objAlignA2->setWrapText(true); //Ajustar Texto 

	 //Formato TERCERA Fila 
	$objStyleA4 = $objActSheet ->getStyle('A'.($nservicio+4).':'.'Z'.($nservicio+4));
	//Configuración de tipos de letra 
	$objFontA4 = $objStyleA4->getFont(); 
	$objFontA4->setName('Arial'); 
	$objFontA4->setSize(10); 
	$objFontA4->setBold(true);
	//Establecer la alineación 
	$objAlignA4 = $objStyleA4->getAlignment(); 
	$objAlignA4->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	$objAlignA4->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objAlignA4->setWrapText(true); //Ajustar Texto 

	//Formato CUARTA Fila 
	$objStyleA5 = $objActSheet ->getStyle('A'.($nservicio+5).':'.'Z'.($nservicio+5));
	//$objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
	$objFontA5 = $objStyleA5->getFont(); 
	$objFontA5->setName('Arial'); 
	$objFontA5->setSize(9); 
	$objFontA5->setBold(true);
	//Establecer la alineación 
	$objAlignA5 = $objStyleA5->getAlignment(); 
	$objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	$objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objAlignA5->setWrapText(true); //Ajustar Texto 

	//Formato QUINTA Fila (DATOS)
	$objStyleC = $objActSheet ->getStyle('A'.($nservicio+6).':'.'Z'.($nservicio+$i+5));
	//Configuración de tipos de letra 
	$objFontC = $objStyleC->getFont();
	$objFontC->setName('Arial');
	$objFontC->setSize(8);
	//Establecer la alineación 
	$objAlignC = $objStyleC->getAlignment(); 
	$objAlignC->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	$objAlignC->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objAlignC->setWrapText(true); //Ajustar Texto 
	
	$objActSheet->getColumnDimension('A')->setWidth(4);
	$objActSheet->getColumnDimension('B')->setWidth(7);
	$objActSheet->getColumnDimension('C')->setWidth(16);
	$objActSheet->getColumnDimension('D')->setWidth(7);
	$objActSheet->getColumnDimension('E')->setWidth(9);
	$objActSheet->getColumnDimension('F')->setWidth(6);	
	$objActSheet->getColumnDimension('G')->setWidth(6);
	
	$objActSheet->getColumnDimension('H')->setWidth(4);
	$objActSheet->getColumnDimension('I')->setWidth(4);
	$objActSheet->getColumnDimension('J')->setWidth(4);
	$objActSheet->getColumnDimension('K')->setWidth(5);
	
	$objActSheet->getColumnDimension('L')->setWidth(5);
	$objActSheet->getColumnDimension('M')->setWidth(5);
	$objActSheet->getColumnDimension('N')->setWidth(5);
	
		
//FIN PRIMER SERVICIO	-------------------------------------------------------------------------------------------
		
	//$hoja++;
	$nservicio=$nservicio+$i+3;

} 
	    
}}	
	 

//Formato General	
//$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true); 

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
 
header('Content-Type: application/vnd.ms-excel');
$DiaActual=date('dmY');
$NombreExcel='PacientesHospitalizadosEmergencia'.$DiaActual;
header('Content-Disposition: attachment;filename='.$NombreExcel.'.xlsx');
//header('Content-Disposition: attachment;filename="IngresosEgresosHospitalizacion.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>



						
  