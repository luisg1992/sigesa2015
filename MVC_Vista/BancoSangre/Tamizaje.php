<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Listado de Espera para Tamizaje</title>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >			
			
			function RegTamizaje(){					
				var rowp = $('#dg').datagrid('getSelected');
				if (rowp){
					if(rowp.EstadoDonacion=='1'){
						$.messager.confirm('Mensaje', 'Aún NO se registra la Extracción. ¿Seguro de Registrar el Tamizaje de Sangre del Donante?', function(r){
						if (r){
							location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegTamizaje&NroDonacion="+rowp.NroDonacion+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";	
						}
					});	
					
					}else{
						location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegTamizaje&NroDonacion="+rowp.NroDonacion+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";
					}
				}else{
					$.messager.alert('Mensaje de Información', 'Debe seleccionar un Postulante','warning');						
				}
			}
			
			
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>         
        
</head>    
        
<body>
   
		<div style="margin:0px 0;"></div>
        <div id="tb" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">
				<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="RegTamizaje()">Registrar Tamizaje</a>
				<!--<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Editar</a> -->
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>        
			</div>        
        </div>
       
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Lista de Postulantes en Espera para Tamizaje" style="width:100%;height:80%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:true,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
            	   <th field="FechaDonacion" width="110">Fecha Impresión Etiqueta</th>
                   <th field="NroDonacion" width="60">N° Donacion</th>
                   <th field="NroDocumento" width="60">DNI Donante</th>
                   <th field="Postulante" width="140">Donantes en Espera Tamizaje</th> 
                   <th field="GrupoSanguineoPostulante" width="60">G.S.Donante</th>                  
                   <th field="TipoDonante" width="60">Tipo Donante</th>
                   <th field="TipoDonacion" width="60">Tipo Donacion</th>
                   <th field="Paciente" width="150">Paciente</th>
                   <th field="GrupoSanguineoPaciente" width="60">G.S.Paciente</th>                   
                   <th field="Accion" width="100">Acción</th>
                   <!--formatter="formatPrioridad"-->                   	
			</tr>
		</thead>
	</table>  
   
   <script type="text/javascript">		
	 
	function getData(){
			var rows = [];			
			<?php			
			 $i = 1;					
			 $Listar=Listar_TamizajePendientesM("");				
			 if($Listar!=NULL){   
				foreach($Listar as $item)
				{			
					$EstadoDonacion=$item['EstadoDonacion'];//2 (APTO),1 (FALTA EXTRACCION pero tiene NroDonacion), 0 (NO APTO),null no tiene NroDonacion
					if($EstadoDonacion=='0'){						
						$Accion='Extracción No Apto';
					}else if($EstadoDonacion=='1'){						
						$Accion='<font color="#FF0000">Extracción Pendiente</font>';								
					}else if($EstadoDonacion=='2'){
						$Accion='<font color="#0000FF">Extracción Realizada</font>';
					}else{
						$Accion='';
					}
					
					if($item['FechaDonacion']!=NULL){
						$FechaDonacion=vfecha(substr($item['FechaDonacion'],0,10)).' '.substr($item['FechaDonacion'],11,8);
					}else{
						$FechaDonacion="";
					}
					
					//Si encuentra estos caracteres no muestra el listado					
					$novalidados = array("'", ",");	
									
					$xPostulante=$item['ApellidosPostulante']." ".$item['NombresPostulante'];					
					$PostulanteV = str_replace($novalidados, "", $xPostulante);
					
					$xPaciente=$item['Paciente'];					
					$PacienteV = str_replace($novalidados, "", $xPaciente);	
						
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							FechaMovi: '<?php echo vfecha(substr($item['FechaMovi'],0,10)).' '.substr($item['FechaMovi'],11,8);?>',
							CodigoPostulante: '<?php echo $item['CodigoPostulante'];?>',
							NroDocumento: '<?php echo $item['NroDocumento'];?>',
							Postulante: "<?php echo $PostulanteV;?>",
							TipoDonante:  '<?php echo $item['TipoDonante'];?>',
							TipoDonacion:  '<?php echo $item['TipoDonacion'];?>',
							CondicionDonante:  '<?php echo $item['CondicionDonante'];?>',							
							Paciente:'<?php echo $PacienteV;?>',
							GrupoSanguineoPaciente:'<?php echo $item['GrupoSanguineoPaciente'];?>',
							Accion:'<?php echo $Accion;?>',
							IdMovimiento:'<?php echo $item['IdMovimiento'];?>',
							NroDonacion	:'<?php echo $item['NroDonacion'];?>',
							FechaDonacion: '<?php echo $FechaDonacion;?>',
							EstadoDonacion: '<?php echo $EstadoDonacion;?>',
							GrupoSanguineoPostulante:'<?php echo $item['GrupoSanguineoPostulante'];?>'
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}
		
		
		$('#dg').datagrid({
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){
			var dg =$('#dg').datagrid({data:getData()}).datagrid({			
			//var dg =$('#dg').datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});			

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>