<?php 
			$IdExamenMedico=$_REQUEST['IdExamenMedico']; 
			$data=Listar_ExamenMedicoM($IdExamenMedico); 	   
				
			$IdDocIdentidad=$data[0]["IdDocIdentidad"];
			$NroDocumento=$data[0]["NroDocumento"];
			$NombresPostulante=$data[0]["ApellidoPaterno"].' '.$data[0]["ApellidoMaterno"].' '.$data[0]["PrimerNombre"].' '.$data[0]["SegundoNombre"];
			$NroMovimiento='PDAC'.$data[0]["NroMovimiento"];
			$IdTipoDonacion=$data[0]["IdTipoDonacion"];
			$IdGrupoSanguineo=$data[0]["IdGrupoSanguineo"];		
			$TipoDonacion='<font color="#0000FF">'.$data[0]["TipoDonacion"].'</font>';	
			$IdLugarColecta=$data[0]["IdLugarColecta"];	
			$IdMovimiento=$data[0]["IdMovimiento"];
			$IdPostulante=$data[0]["IdPostulante"];
			$NroDonacion=$data[0]["NroDonacion"];	
			$GrupoSanguineoPaciente=$data[0]["GrupoSanguineoPaciente"];	
			$IdGrupoSanguineoPaciente=$data[0]["IdGrupoSanguineoPaciente"];
			
			$Turno=$data[0]["Turno"];
			$IdResponsableEv=$data[0]["IdResponsableEv"];
			$IdEntrevistador=$data[0]["IdEntrevistador"];
			$FechaEvaluacion=vfecha(substr($data[0]['FechaEvaluacion'],0,10));
			$HoraEvaluacion=substr($data[0]['FechaEvaluacion'],11,8);	
			
			$Peso=$data[0]['Peso'];
			$Talla=$data[0]['Talla']; 
			$PresionDiastolica=$data[0]['PresionDiastolica']; 		
			$PresionSistolica=$data[0]['PresionSistolica']; 
			$Pulso=$data[0]['Pulso']; 
			$Temperatura=$data[0]['Temperatura']; 
			$MetodologiaHb=$data[0]['MetodologiaHb']; 
			$Hematocrito=$data[0]['Hematocrito']; 
			$Hemoglobina=$data[0]['Hemoglobina']; 
			$MetodologiaHcto=$data[0]['MetodologiaHcto']; 
			
			$Observacion=$data[0]['Observacion'];
			$EstadoApto=$data[0]['EstadoApto']; 
			$IdMotivoRechazo=$data[0]['IdMotivoRechazo']; 
			$FechaInicioRechazo=$data[0]['FechaInicioRechazo']; 
			if($FechaInicioRechazo!=""){
				$FechaInicioRechazo=vfecha(substr($data[0]['FechaInicioRechazo'],0,10));
			}else{
				$FechaInicioRechazo="";
			}
			$FechaFinRechazo=$data[0]['FechaFinRechazo']; 
			if($FechaFinRechazo!=""){
				$FechaFinRechazo=vfecha(substr($data[0]['FechaFinRechazo'],0,10));
			}else{
				$FechaFinRechazo="";
			}
						
			$IdMedicacion=$data[0]['IdMedicacion'];
			$FechaMedicacion=$data[0]['FechaMedicacion'];
			if($FechaMedicacion!=""){
				$FechaMedicacion=vfecha(substr($data[0]['FechaMedicacion'],0,10));
			}else{
				$FechaMedicacion="";
			}						
			$FechaEspera=$data[0]['FechaEspera']; 	
			if($FechaEspera!=""){
				$FechaEspera=vfecha(substr($data[0]['FechaEspera'],0,10));
			}else{
				$FechaEspera="";
			}						
						
			$xMotivoRechazo=$data[0]['MotivoRechazo'];
			//solución ya que cuando busca no reconoce [
			$porciones = explode("[", $xMotivoRechazo);
			$xMotivoRechazo=$porciones[0]; // porción1

			if(trim($xMotivoRechazo)!=""){		
				$ListarMotivoRechazo=SIGESA_ListarMotivoRechazo_M('Evaluacion',$xMotivoRechazo);
				$MotivoRechazo= $ListarMotivoRechazo[0]["IdMotivoRechazo"].'|'.$ListarMotivoRechazo[0]["Diferimiento"].'|'.$ListarMotivoRechazo[0]["Tiempo"].'|'.$ListarMotivoRechazo[0]["TipoTiempo"];
			}else{
				$MotivoRechazo="";
			}
			
			//$ListarMedicacion=SIGESA_ListarMedicacion_M($IdMedicacion);			//$Medicacion=$ListarMedicacion[0]["IdMedicacion"].'|'.$ListarMedicacion[0]["Diferimiento"].'|'.$ListarMedicacion[0]["Tiempo"].'|'.$ListarMedicacion[0]["TipoTiempo"];
?>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Actualizar Evaluación Predonación Postulante</title>        
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">        
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>        
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript">
        	$(function(){
				
				$('#MotivoRechazo').combogrid({
					panelWidth:700,
					//value:'',
					url: '../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListarMotivoRechazo',
					idField:'Datos',
					textField:'Descripcion',
					mode:'remote',
					fitColumns:true,
					onSelect: function(rec){
					var url = cambiarDiferimientoRechazo(); },						
					columns:[[							
						{field:'Descripcion',title:'Nombre',width:500}						
					]]
				});					
				
				$.extend($.fn.textbox.methods, {
					show: function(jq){
						return jq.each(function(){
							$(this).next().show();
						})
					},
					hide: function(jq){
						return jq.each(function(){
							$(this).next().hide();
						})
					}
				})
				
				//$('#Donante').hide();
				//document.getElementById('Donante').style.display = 'none';
				//document.getElementById('Donante').style.visibility = 'hidden';
				//$('#NroDonacion').textbox('hide'); 
				/*document.getElementById('MotivoRechazo').style.display = 'none';
				document.getElementById('MotivoRechazo').style.visibility = 'hidden';
				$('#MotivoRechazo').textbox('hide');*/ 
									
			});
			
			/*function cambiarEstadoApto(){
				var EstadoApto= $('#EstadoApto').combobox('getValue');	
				if(EstadoApto=='0' || EstadoApto=='1'){ //RECHAZADO
					document.getElementById('MotivoRechazo').style.display = 'block';
					document.getElementById('MotivoRechazo').style.visibility = 'visible';
					$('#MotivoRechazo').textbox('show'); 
					document.getElementById('Donante').style.display = 'none';
					document.getElementById('Donante').style.visibility = 'hidden';
					$('#NroDonacion').textbox('hide'); 					
				
				}else if(EstadoApto=='2'){ //ACEPTADO
					document.getElementById('Donante').style.display = 'block';
					document.getElementById('Donante').style.visibility = 'visible';
					$('#NroDonacion').textbox('show'); 
					document.getElementById('MotivoRechazo').style.display = 'none';
					document.getElementById('MotivoRechazo').style.visibility = 'hidden';
					$('#MotivoRechazo').textbox('hide');
					
				}else if(EstadoApto==''){ //Seleccione
					document.getElementById('Donante').style.display = 'none';
					document.getElementById('Donante').style.visibility = 'hidden';
					$('#NroDonacion').textbox('hide'); 
					document.getElementById('MotivoRechazo').style.display = 'none';
					document.getElementById('MotivoRechazo').style.visibility = 'hidden';
					$('#MotivoRechazo').textbox('hide');
				}
			}*/
			
			
			function cambiarTemperatura(){				
				var Temperatura= $('#Temperatura').numberbox('getValue');				
				if(parseFloat(Temperatura)>37 && parseFloat(Temperatura)<=46){
					$('#fiebre').html("FIEBRE"); 	
				}else if(parseFloat(Temperatura)<35 || parseFloat(Temperatura)>46){
					$('#fiebre').html("NO COHERENTE"); 	
				}else{
					$('#fiebre').html(""); 	
				}							
			}			
			
			function sumaFecha(d, fecha, tipo){ //tipo=d,h,m,y
				 var Fecha = new Date();
				 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
				 var sep = sFecha.indexOf('/') != -1 ? '/' : '-'; 
				 var aFecha = sFecha.split(sep);
				 
				 if(aFecha[0].length==2 && aFecha[1].length==2 && aFecha[2].length==4){
					 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
					 fecha= new Date(fecha);
					 if(tipo=='d'){ //DIAS
						fecha.setDate(fecha.getDate()+parseInt(d));
					 }else if(tipo=='m'){ //MES
						fecha.setMonth(fecha.getMonth()+parseInt(d));
					 }else if(tipo=='h'){ //HORA
						fecha.setDate(fecha.getDate()+parseInt(d/24));
					 }else if(tipo=='y'){ //AÑO
						fecha.setYear(fecha.getFullYear()+parseInt(d));
					 }				 
					 var anno=fecha.getFullYear();
					 var mes= fecha.getMonth()+1;
					 var dia= fecha.getDate();
					 mes = (mes < 10) ? ("0" + mes) : mes;
					 dia = (dia < 10) ? ("0" + dia) : dia;
					 var fechaFinal = dia+sep+mes+sep+anno;
					 
				 }else{
					var fechaFinal='';	 
				 }				 
				 return (fechaFinal);				 
			}
			 
			
			function cambiarDiferimientoRechazo(){
				
				var Medicacion= $('#Medicacion').combobox('getValue');
				var MotivoRechazo= $('#MotivoRechazo').combobox('getValue');
				
				if(MotivoRechazo.trim()!=""){	
					var arreglo=MotivoRechazo.split("|");					
					document.getElementById('IdMotivoRechazo').value=arreglo[0];
					
					//Cambiar Estado Apto 
						//si uno de los motivos es por tiempo INDEFINIDO, entonces siempre el motivo va ser 0=NO-No Apto Definitivo						
						if(Medicacion.trim()!=""){
							var arregloMedicacion=Medicacion.split("|"); //EstadoApto=arregloMedicacion[1]						
							if(arreglo[1]<arregloMedicacion[1]){ //INDEFINIDO
								$("#EstadoApto").combobox('setValue',arreglo[1]);						
							}
						}else{
							$("#EstadoApto").combobox('setValue',arreglo[1]);	
						}
					//FIN Cambiar Estado Apto 					
						
					var Tiempo=arreglo[2];
					var TipoTiempo=arreglo[3];
					var FechaInicioRechazo= $('#FechaInicioRechazo').datebox('getValue');//fecha inicio medicacion
					
					var array_FechaInicioRechazo = FechaInicioRechazo.split("/");				
					if(FechaInicioRechazo.trim()=="" || array_FechaInicioRechazo.length!=3){
						$("#FechaFinRechazo").datebox('setValue',"");							
					}else{
						var FechaFinRechazo=sumaFecha(Tiempo,FechaInicioRechazo,TipoTiempo);	
						$("#FechaFinRechazo").datebox('setValue',FechaFinRechazo);							
					}					
				}else{					
					document.getElementById('IdMotivoRechazo').value='0';				
					$("#FechaInicioRechazo").datebox('setValue','');	
					$("#FechaFinRechazo").datebox('setValue','');
					if(Medicacion.trim()!=""){
						var arreglo=Medicacion.split("|");
						$("#EstadoApto").combobox('setValue',arreglo[1]);						
					}else{//END if	
						$("#EstadoApto").combobox('setValue','');		
					}
					$.messager.alert('Mensaje','Falta Seleccionar un Motivo Principal Rechazo','info');
				}//END else				
			} 
			
			
			function cambiarMedicacion(){
				
				var MotivoRechazo= $('#MotivoRechazo').combobox('getValue');								
				var Medicacion=$('#Medicacion').combobox('getValue');
				
				if(Medicacion.trim()!=""){	
					var arreglo=Medicacion.split("|");					
					document.getElementById('IdMedicacion').value=arreglo[0];
					
					//Cambiar Estado Apto 
						//si uno de los motivos es por tiempo INDEFINIDO, entonces siempre el motivo va ser 0=NO-No Apto Definitivo						
						if(MotivoRechazo.trim()!=""){
							var arregloMotivoRechazo=MotivoRechazo.split("|"); //EstadoApto=arregloMotivoRechazo[1]						
							if(arreglo[1]<arregloMotivoRechazo[1].trim()){ //Estadoapto es menor que el Estadoapto anterior 
								$("#EstadoApto").combobox('setValue',arreglo[1]);						
							}
						}else{
							$("#EstadoApto").combobox('setValue',arreglo[1]);	
						}
					//FIN Cambiar Estado Apto 					
						
					var Tiempo=arreglo[2];
					var TipoTiempo=arreglo[3];
					var FechaMedicacion= $('#FechaMedicacion').datebox('getValue');//fecha inicio medicacion
					var array_FechaMedicacion = FechaMedicacion.split("/");				
					if(FechaMedicacion.trim()=="" || array_FechaMedicacion.length!=3){
						$("#FechaEspera").datebox('setValue',"");
					}else{
						var FechaEspera=sumaFecha(Tiempo,FechaMedicacion,TipoTiempo);	
						$("#FechaEspera").datebox('setValue',FechaEspera);	
					}
					
				}else{					
					document.getElementById('IdMedicacion').value='0';					
					$("#FechaMedicacion").datebox('setValue','');	
					$("#FechaEspera").datebox('setValue','');
					if(MotivoRechazo.trim()!=""){
						var arreglo=MotivoRechazo.split("|");
						$("#EstadoApto").combobox('setValue',arreglo[1]);			
					}else{//END if	
						$("#EstadoApto").combobox('setValue','');		
					}
					$.messager.alert('Mensaje','Falta Seleccionar un Motivo por Medicamento','info');						
				}//END else							
			}
			
			
			function cambiarEstadoApto(){
				var EstadoApto= $('#EstadoApto').combobox('getValue');	
				if(EstadoApto=='0' || EstadoApto=='1'){
					//$("#Peso").numberbox('required',false);
					 $('#Peso').numberbox({required: false});
					 $('#Talla').numberbox({required: false});		
				}else{
					$('#Peso').numberbox({required: true});
					$('#Talla').numberbox({required: true});
				}
				$("#MotivoRechazo").combobox('setValue','');
				$("#FechaInicioRechazo").datebox('setValue','');
				$("#FechaFinRechazo").datebox('setValue','');				
				
				$("#Medicacion").combobox('setValue','');
				$("#FechaMedicacion").datebox('setValue','');
				$("#FechaEspera").datebox('setValue','');
				
				document.getElementById('IdMotivoRechazo').value=0;
				document.getElementById('IdMedicacion').value=0;				
			}
			
			
			//$(function(){			
				$.extend( $( "#FechaEvaluacion" ).datebox.defaults,{
					formatter:function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
					},
					parser:function(s){
						if (!s) return new Date();
						var ss = s.split('/');
						var d = parseInt(ss[0],10);
						var m = parseInt(ss[1],10);
						var y = parseInt(ss[2],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d);
						} else {
							return new Date();
						}
					}
				});
				
				$.extend($( "#FechaEvaluacion" ).datebox.defaults.rules, { 
					validDate: {  
						validator: function(value, element){  
							var date = $.fn.datebox.defaults.parser(value);
							var s = $.fn.datebox.defaults.formatter(date);	
							
							if(s==value){
								return true;
							}else{								
								//$("#FechaNacimiento" ).datebox('setValue', '');
								//$("#EdadPaciente").textbox('setValue','');
								return false;
							}
						},  
						message: 'Porfavor Seleccione una fecha valida.'  
					}
				});
				
				$.extend( $( "#FechaInicioRechazo" ).datebox.defaults,{
					formatter:function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
					},
					parser:function(s){
						if (!s) return new Date();
						var ss = s.split('/');
						var d = parseInt(ss[0],10);
						var m = parseInt(ss[1],10);
						var y = parseInt(ss[2],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d);
						} else {
							return new Date();
						}
					}
				});
				$.extend($( "#FechaInicioRechazo" ).datebox.defaults.rules, { 
					validDate: {  
						validator: function(value, element){  
							var date = $.fn.datebox.defaults.parser(value);
							var s = $.fn.datebox.defaults.formatter(date);	
							
							if(s==value){
								return true;
							}else{								
								//$("#FechaNacimiento" ).datebox('setValue', '');
								//$("#EdadPaciente").textbox('setValue','');
								return false;
							}
						},  
						message: 'Porfavor Seleccione una fecha valida.'  
					}
				});
				
				$.extend( $( "#FechaMedicacion" ).datebox.defaults,{
					formatter:function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
					},
					parser:function(s){
						if (!s) return new Date();
						var ss = s.split('/');
						var d = parseInt(ss[0],10);
						var m = parseInt(ss[1],10);
						var y = parseInt(ss[2],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d);
						} else {
							return new Date();
						}
					}
				});
				$.extend($( "#FechaMedicacion" ).datebox.defaults.rules, { 
					validDate: {  
						validator: function(value, element){  
							var date = $.fn.datebox.defaults.parser(value);
							var s = $.fn.datebox.defaults.formatter(date);	
							
							if(s==value){
								return true;
							}else{								
								//$("#FechaNacimiento" ).datebox('setValue', '');
								//$("#EdadPaciente").textbox('setValue','');
								return false;
							}
						},  
						message: 'Porfavor Seleccione una fecha valida.'  
					}
				});
				
			//VALIDAR QUE SELECCIONEN UNA OPCION DEL COMBO
			$.extend($.fn.validatebox.defaults.rules,{
				exists:{
					validator:function(value,param){
						var cc = $(param[0]);
						var v = cc.combobox('getValue');
						var rows = cc.combobox('getData');
						for(var i=0; i<rows.length; i++){
							if (rows[i].id == v){return true}
						}
						return false;
					},
					message:'El valor ingresado no existe.'
				}
			});
						
			$(function () {				
				
				$('#EstadoApto').combobox({	
					editable: false,
					required: true								
				});	
				$('#EstadoApto').combobox('validate');					
				
				$('#MetodologiaHb').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MetodologiaHb"]'								
				});						
				$('#MetodologiaHb').combobox('validate');
				
				$('#MetodologiaHcto').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#MetodologiaHcto"]'								
				});						
				$('#MetodologiaHcto').combobox('validate');	
				
				$('#IdGrupoSanguineo').combobox({	
					valueField: 'id',
					textField: 'text',
					editable: true,
					required: true,    
					validType: 'exists["#IdGrupoSanguineo"]'								
				});						
				$('#IdGrupoSanguineo').combobox('validate');				
				
			});			
				
			function cancelar(){ 
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=Extraccion&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
			
			function guardar(){	
				//Datos Generales
				var IdGrupoSanguineo=$('#IdGrupoSanguineo').combobox('getValue');		
				if(IdGrupoSanguineo.trim()=="" || $('#IdGrupoSanguineo').combobox('isValid')==false){ 
					//$.messager.alert('Mensaje','Falta Seleccionar Grupo Sanguineo del Postulante','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar Grupo Sanguineo del Postulante',
						icon:'warning',
						fn: function(){
							$('#IdGrupoSanguineo').next().find('input').focus();
						}
					});
					$('#IdGrupoSanguineo').next().find('input').focus();
					return 0;			
				}
				
				var EstadoApto=$('#EstadoApto').combobox('getValue');				
				if(EstadoApto=='2'){							
					if(IdGrupoSanguineo.trim()=="" || IdGrupoSanguineo.trim()=="0"){ 
						//$.messager.alert('Mensaje','Falta Seleccionar Grupo Sanguineo del Postulante','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Seleccionar Grupo Sanguineo del Postulante',
							icon:'warning',
							fn: function(){
								$('#IdGrupoSanguineo').next().find('input').focus();
							}
						});
						$('#IdGrupoSanguineo').next().find('input').focus();
						return 0;			
					}
				}
				
				var IdLugarColecta=$('#IdLugarColecta').combobox('getValue');		
				if(IdLugarColecta.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar Lugar Colecta','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar Lugar Colecta',
						icon:'warning',
						fn: function(){
							$('#IdLugarColecta').next().find('input').focus();
						}
					});
					$('#IdLugarColecta').next().find('input').focus();
					return 0;			
				}
				
				var Turno=$('#Turno').combobox('getValue');		
				if(Turno.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar Turno','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar Turno',
						icon:'warning',
						fn: function(){
							$('#Turno').next().find('input').focus();
						}
					});
					$('#Turno').next().find('input').focus();
					return 0;			
				}
				
				var FechaEvaluacion=$('#FechaEvaluacion').datebox('getValue');		
				if(FechaEvaluacion.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Ingresar Fecha de Evaluación','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar Fecha de Evaluación',
						icon:'warning',
						fn: function(){
							$('#FechaEvaluacion').next().find('input').focus();
						}
					});
					$('#FechaEvaluacion').next().find('input').focus();
					return 0;			
				}
				
				var IdResponsableEv=$('#IdResponsableEv').combobox('getValue');
				if(IdResponsableEv.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Responsable Evaluación','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar el Responsable Evaluación',
						icon:'warning',
						fn: function(){
							$('#IdResponsableEv').next().find('input').focus();
						}
					});
					$('#IdResponsableEv').next().find('input').focus();
					return 0;			
				}
				
				var IdEntrevistador=$('#IdEntrevistador').combobox('getValue');
				if(IdEntrevistador.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar el Entrevistador','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar el Entrevistador',
						icon:'warning',
						fn: function(){
							$('#IdEntrevistador').next().find('input').focus();
						}
					});
					$('#IdEntrevistador').next().find('input').focus();
					return 0;			
				}
				
				//SOLO SI ESTA APTO ES OBLIGATORIO LOS Datos de Triaje
				var EstadoApto=$('#EstadoApto').combobox('getValue');		
				if(EstadoApto.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Seleccionar si ¿Está Apto?','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar si ¿Está Apto?',
						icon:'warning',
						fn: function(){
							$('#EstadoApto').next().find('input').focus();
						}
					});
					$('#EstadoApto').next().find('input').focus();
					return 0;			
				}
				
				//Datos de Triaje
				if(EstadoApto=='2'){
							
					var Peso=$('#Peso').numberbox('getValue');		
					if(Peso.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Peso','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar el Peso',
							icon:'warning',
							fn: function(){
								$('#Peso').next().find('input').focus();
							}
						});
						$('#Peso').next().find('input').focus();
						return 0;			
					}				
					var Talla=$('#Talla').numberbox('getValue');		
					if(Talla.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Talla','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Talla',
							icon:'warning',
							fn: function(){
								$('#Talla').next().find('input').focus();
							}
						});
						$('#Talla').next().find('input').focus();
						return 0;			
					}					
					/*var PresionSistolica=$('#PresionSistolica').numberbox('getValue');		
					if(PresionSistolica.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Presión Sistolica','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Presión Sistolica',
							icon:'warning',
							fn: function(){
								$('#PresionSistolica').next().find('input').focus();
							}
						});
						$('#PresionSistolica').next().find('input').focus();
						return 0;			
					}				
					var PresionDiastolica=$('#PresionDiastolica').numberbox('getValue');		
					if(PresionDiastolica.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Presión Diastolica','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Presión Diastolica',
							icon:'warning',
							fn: function(){
								$('#PresionDiastolica').next().find('input').focus();
							}
						});
						$('#PresionDiastolica').next().find('input').focus();
						return 0;			
					}*/
					
					/*var Pulso=$('#Pulso').numberbox('getValue');		
					if(Pulso.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar el Pulso','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar el Pulso',
							icon:'warning',
							fn: function(){
								$('#Pulso').next().find('input').focus();
							}
						});
						$('#Pulso').next().find('input').focus();
						return 0;			
					}*/				
					/*var Temperatura=$('#Temperatura').numberbox('getValue');		
					if(Temperatura.trim()==""){ 
						//$.messager.alert('Mensaje','Falta Ingresar la Temperatura','warning');
						$.messager.alert({
							title: 'Mensaje',
							msg: 'Falta Ingresar la Temperatura',
							icon:'warning',
							fn: function(){
								$('#Temperatura').next().find('input').focus();
							}
						});
						$('#Temperatura').next().find('input').focus();
						return 0;			
					}*/						
					//Hemoglobina Y Hematocrito 
					var Hemoglobina=$('#Hemoglobina').numberbox('getValue');
					var Hematocrito=$('#Hematocrito').numberbox('getValue');		
					if(Hemoglobina.trim()=="" && Hematocrito.trim()==""){ 
						$.messager.alert('Mensaje','Debe Ingresar Hemoglobina o Hematocrito','warning');					
						return 0;			
					}								
					var MetodologiaHb=$('#MetodologiaHb').combobox('getValue');		
					if( ((MetodologiaHb.trim()=="" || MetodologiaHb.trim()=="0") && Hemoglobina.trim()!="") || $('#MetodologiaHb').combobox('isValid')==false ){ 
						$.messager.alert('Mensaje','Falta Seleccionar la Metodologia Hb','warning');
						$('#MetodologiaHb').next().find('input').focus();
						return 0;			
					}									
					var MetodologiaHcto=$('#MetodologiaHcto').combobox('getValue');		
					if( ((MetodologiaHcto.trim()=="" || MetodologiaHcto.trim()=="0") && Hematocrito.trim()!="") || $('#MetodologiaHcto').combobox('isValid')==false ){ 
						$.messager.alert('Mensaje','Falta Seleccionar la Metodologia Hcto','warning');
						$('#MetodologiaHcto').next().find('input').focus();
						return 0;			
					}				
				}//FIN if EstadoApto=='2'
								
				//Datos si hay Diferimiento	
				
				var MotivoRechazo=$('#MotivoRechazo').combobox('getValue');
				var Medicacion=$('#Medicacion').combobox('getValue');		
				if(EstadoApto.trim()!="2" && (MotivoRechazo.trim()=="" && Medicacion.trim()=="")){ 
					//$.messager.alert('Mensaje','Falta Seleccionar un MOTIVO PRINCIPAL o MOTIVO POR MEDICAMENTO','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Seleccionar un MOTIVO PRINCIPAL o MOTIVO POR MEDICAMENTO',
						icon:'warning',
						fn: function(){
							$('#MotivoRechazo').next().find('input').focus();
						}
					});
					$('#MotivoRechazo').next().find('input').focus();
					return 0;			
				}
								
				var FechaInicioRechazo=$('#FechaInicioRechazo').datebox('getValue');
				if(MotivoRechazo.trim()!="" && FechaInicioRechazo.trim()==""){ //EstadoApto.trim()=="1"
					//$.messager.alert('Mensaje','Falta Ingresar la Fecha Inicio Diferimiento Rechazo','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar la Fecha Inicio Diferimiento Rechazo',
						icon:'warning',
						fn: function(){
							$('#FechaInicioRechazo').next().find('input').focus();
						}
					});
					$('#FechaInicioRechazo').next().find('input').focus();
					return 0;			
				}
				
				var FechaFinRechazo=$('#FechaFinRechazo').datebox('getValue');
				if(MotivoRechazo.trim()!="" && FechaFinRechazo.trim()==""){ 
					$.messager.alert({
						title: 'Mensaje',
						msg: 'La Fecha Inicio Diferimiento Rechazo NO ES VÁLIDA',
						icon:'warning',
						fn: function(){
							$('#FechaInicioRechazo').next().find('input').focus();
						}
					});
					$('#FechaInicioRechazo').next().find('input').focus();
					return 0;			
				}
				
				var FechaMedicacion=$('#FechaMedicacion').datebox('getValue');
				if(Medicacion.trim()!="" && FechaMedicacion.trim()==""){ 
					//$.messager.alert('Mensaje','Falta Ingresar la Fecha Inicio Diferimiento Medicamento','warning');
					$.messager.alert({
						title: 'Mensaje',
						msg: 'Falta Ingresar la Fecha Inicio Diferimiento Medicamento',
						icon:'warning',
						fn: function(){
							$('#FechaMedicacion').next().find('input').focus();
						}
					});
					$('#FechaMedicacion').next().find('input').focus();
					return 0;			
				}
				
				var FechaEspera=$('#FechaEspera').datebox('getValue');
				if(Medicacion.trim()!="" && FechaEspera.trim()==""){					
					$.messager.alert({
						title: 'Mensaje',
						msg: 'La Fecha Inicio Diferimiento Medicamento NO ES VÁLIDA',
						icon:'warning',
						fn: function(){
							$('#FechaMedicacion').next().find('input').focus();
						}
					});
					$('#FechaMedicacion').next().find('input').focus();
					return 0;			
				}
				
				//VALIDACIONES
				var Peso= $('#Peso').numberbox('getValue');				
				if(parseFloat(Peso)<50 && MotivoRechazo.trim()==""){
					$.messager.alert('Mensaje','Peso No Apto. Seleccione Motivo Principal Rechazo: Peso < 50kg, 1 mes','info');
					$('#MotivoRechazo').next().find('input').focus();
					return 0;
				}
											
				if(parseFloat(Hemoglobina)<12.5 && MotivoRechazo.trim()==""){
					$.messager.alert('Mensaje','Hemoglobina No Apto. Seleccione Motivo Principal Rechazo: Hemoglobina < 12 5  ó  Hematocrito < 38%, 3 meses','info');
					$('#MotivoRechazo').next().find('input').focus();
					return 0;
				}				
				if(parseFloat(Hematocrito)<38 && MotivoRechazo.trim()==""){
					$.messager.alert('Mensaje','Hematocrito No Apto. Seleccione Motivo Principal Rechazo: Hemoglobina < 12 5  ó  Hematocrito < 38%, 3 meses','info');
					$('#MotivoRechazo').next().find('input').focus();
					return 0;
				}
				
				if(parseFloat(PresionSistolica)>180 && MotivoRechazo.trim()==""){
					$.messager.alert('Mensaje','Presion Sistolica No Apto. Seleccione Motivo Principal Rechazo: Presión Arterial Alta PS >180/ PD >100','info');
					$('#MotivoRechazo').next().find('input').focus();
					return 0;
				}
				if(parseFloat(PresionDiastolica)>100 && MotivoRechazo.trim()==""){
					$.messager.alert('Mensaje','Presion Diastolica No Apto. Seleccione Motivo Principal Rechazo: Presión Arterial Alta PS >180/ PD >100','info');
					$('#MotivoRechazo').next().find('input').focus();
					return 0;
				}
				
				if(parseFloat(Temperatura)>37.5 && MotivoRechazo.trim()==""){
					$.messager.alert('Mensaje','Temperatura No Apto. Seleccione Motivo Principal Rechazo: T° oral >37 5°C  ó  T° axilar >37°C','info');
					$('#MotivoRechazo').next().find('input').focus();
					return 0;
				}
				
				IdGrupoSanguineoPaciente=document.getElementById('IdGrupoSanguineoPaciente').value;
				IdTipoDonacion=document.getElementById('IdTipoDonacion').value;
				if(IdTipoDonacion=="2" && IdGrupoSanguineoPaciente!=IdGrupoSanguineo && MotivoRechazo.trim()==""){ //por aferesis los tipos de sangre deben ser iguales
					$.messager.alert('Mensaje','Grupo Sanguineo No Apto. Seleccione Motivo Principal Rechazo: Grupo Sanguineo diferente (aferesis PQ)','info');
					$('#MotivoRechazo').next().find('input').focus();
					return 0;
				}
				
				var NroDonacionAct=document.getElementById('NroDonacionAct').value;
				//var DescripcionEstadoApto= $('#EstadoApto').combobox('getText');
				if(NroDonacionAct.trim()!="" && EstadoApto.trim()!="2"){
					var mensajeAd="y anular el Número de Donación "+NroDonacionAct;
				}else{
					var mensajeAd="";
				}
												
				//document.form1.submit();
				$.messager.confirm('Mensaje', '¿Seguro de Actualizar la Evaluación Predonación del Postulante '+mensajeAd+ '?', function(r){
					if (r){
						$('#form1').submit();
						//alert('Estoy Modificando');	
					}
				});	
			}
        
        </script>
        
        <style>
            html, body { height: 100%;}
			<!--#MotivoRechazo{visibility:hidden;display:none;}-->
			<!--#Donante{visibility:hidden;display:none;}-->
        </style> 
        
</head>

<body>

<form id="form1" name="form1"  method="POST" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=GuardarUpdExamenMedico&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>">      
    <div id="p" class="easyui-panel" style="width:90%;height:auto;"title="Banco de Sangre: Actualizar Evaluación Predonación Postulante" iconCls="icon-save" align="center"> 
    
    <div class="easyui-panel" style="padding:0px;">        
        <!--<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Refrescar</a>-->             
        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="guardar()">Actualizar</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="cancelar();">Cancelar(ESC)</a>                    
	</div>
    
    <table>
        <tr align="center">
            <td>
				<?php echo "<font color='#0000FF'>$NombresPostulante ($NroMovimiento)</font>" ; ?><br>
				<?php echo "Nro Documento "."<font color='#0000FF'>$NroDocumento</font>" ; ?>
            </td>
        </tr>
        <?php if($IdTipoDonacion=='2'){ ?>
        <tr align="center">
            <td><?php echo "Tipo Sangre Paciente "."<font color='#0000FF'>$GrupoSanguineoPaciente</font>" ; ?></td>
        </tr>
        <?php } ?>
    </table>
    
    <fieldset style="width:80%;">
	<legend>Datos Generales</legend>
    <table width="750">       	
            <tr>
                <td width="176">Tipo Donación</td>
                <td width="180">
                <input name="IdExamenMedico" id="IdExamenMedico" type="hidden" value="<?php echo $IdExamenMedico ?>">
                <select class="easyui-combobox" name="TipoDonacion" id="TipoDonacion"  style="width: 145px" disabled>
                  <option value="0">Seleccione</option>
                  <?php
                                      $listar=SIGESA_BSD_ListarTipoDonacion_M();
                                       if($listar != NULL) { 
                                         foreach($listar as $item){?>
                  <option value="<?php echo $item["IdTipoDonacion"]?>" <?php if($item["IdTipoDonacion"]==$IdTipoDonacion){?> selected <?php } ?>><?php echo $item["IdTipoDonacion"].'='.$item["Descripcion"]?></option>
                  <?php } } ?>
                </select></td>
                <td width="138">Grupo Sanguineo Postulante</td>
                	<td width="236">
                     <input type="hidden" name="IdGrupoSanguineoPaciente" id="IdGrupoSanguineoPaciente" value="<?php echo $IdGrupoSanguineoPaciente ?>" />
                     <input type="hidden" name="IdTipoDonacion" id="IdTipoDonacion" value="<?php echo $IdTipoDonacion ?>" />
                     <select class="easyui-combobox" name="IdGrupoSanguineo" id="IdGrupoSanguineo"  style="width: 145px" data-options="prompt:'Seleccione',required:true" >
                       <option value="0">No sabe</option>
                         <?php
                                  $listarSexo=SIGESA_BSD_ListarGrupoSanguineo_M();
                                   if($listarSexo != NULL) { 
                                     foreach($listarSexo as $item){?>
                         <option value="<?php echo $item["IdGrupoSanguineo"]?>" <?php if($item["IdGrupoSanguineo"]==$IdGrupoSanguineo){?> selected <?php } ?> ><?php echo $item["Descripcion"]?></option>
                         <?php } } ?>
                     </select>
                     <input name="IdMovimiento" id="IdMovimiento" value="<?php echo $IdMovimiento ?>" type="hidden" />
                     <input name="IdPostulante" id="IdPostulante" value="<?php echo $IdPostulante ?>" type="hidden" />
                 
                 	</td>
                 </tr>
                  <tr>
                    <td>Lugar Colecta  </td>
                    <td><select class="easyui-combobox" name="IdLugarColecta" id="IdLugarColecta"  style="width: 145px" data-options="prompt:'Seleccione',required:true" disabled>
                      <option value=""></option>
                      <?php
                                              $listarSexo=SIGESA_BSD_ListarHospital_M();
                                               if($listarSexo != NULL) { 
                                                 foreach($listarSexo as $item){?>
                      <option value="<?php echo $item["IdHospital"]?>" <?php if($item["IdHospital"]==$IdLugarColecta){?> selected <?php } ?> ><?php echo $item["Descripcion"]?></option>
                      <?php } } ?>
                    </select></td>
                    <td>Turno</td>
                    <td>
                    	<select class="easyui-combobox" name="Turno" id="Turno"  style="width: 145px" data-options="prompt:'Seleccione',required:true" disabled>
                      	  <option value=""></option>
                          <option value="M" <?php if('M'==$Turno){?> selected <?php } ?>>Mañana</option>
                          <option value="T" <?php if('T'==$Turno){?> selected <?php } ?>>Tarde</option>
                        </select>
                    </td>
                  </tr>
            
              <tr>
                    <td> Fecha Evaluación </td>
                    <td><input class="easyui-datebox" value="<?php echo $FechaEvaluacion;?>" style="width:145px;" id="FechaEvaluacion" name="FechaEvaluacion" validType="validDate" data-options="required:true" disabled/></td>
                    <td>Hora Evaluación</td>
                    <td><input class="easyui-timespinner" value="<?php echo $HoraEvaluacion;?>" style="width:145px;" id="HoraEvaluacion" name="HoraEvaluacion"
             data-options="showSeconds:true" disabled ></td>
        </tr>
              <tr>
                <td>Responsable Hb,venas,GS&amp;RH</td>
                <td colspan="3"><Select style="width:250px" class="easyui-combobox" id="IdResponsableEv" name="IdResponsableEv" data-options="prompt:'Seleccione Responsable',required:true" disabled>
                  <option value=""></option>
                  <?php
                                  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
                  <option value="<?php echo $item["IdEmpleado"]?>" <?php if($item["IdEmpleado"]==$IdResponsableEv){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                  <?php } } ?>
                </select></td>
              </tr>
              <tr>
                <td>Respons. Entrevista</td>
                <td colspan="3"><Select style="width:250px" class="easyui-combobox" id="IdEntrevistador" name="IdEntrevistador" data-options="prompt:'Seleccione Entrevistador',required:true" disabled>
                  <option value=""></option>
                  <?php
                                  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
                  <option value="<?php echo $item["IdEmpleado"]?>" <?php if($item["IdEmpleado"]==$IdEntrevistador){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
                  <?php } } ?>
                </select></td>
              </tr>              
       </table>
     </fieldset>
         
                    
      <table width="86%">
        <tr>
          <td width="50%"><fieldset>
            <legend>Datos de Triaje</legend>
            <table width="102%">
              <tr>
                <td>Peso(Kg.) </td>
                <td><input name="Peso" class="easyui-numberbox" id="Peso" style="width:145px" data-options="                
                        prompt:'Peso en Kilogramos',min:0,max:500,precision:1,required:true" value="<?php echo $Peso;?>" /></td>
                <td><font color="#FF0000">mínimo 50 Kg.</font></td>
              </tr>
              <tr>
                <td width="30%">Talla (mts.)</td>
                <td width="30%"><!--<button class="btn">.</button>-->
                <input name="Talla" class="easyui-numberbox" id="Talla" style="width:145px" data-options="prompt:'Talla en metros',min:0,max:3.00,precision:2,required:true" value="<?php echo $Talla;?>" /></td>
                <td width="40%">&nbsp;</td>
              </tr>
              <tr>
                <td>Presión Sistólica</td>
                <td><input name="PresionSistolica" class="easyui-numberbox" id="PresionSistolica" style="width:145px" data-options="prompt:'Sistólica',min:40,max:200" value="<?php echo $PresionSistolica;?>" /></td>
                <td><font color="#FF0000">90 a 140 (140-180 consultar)</font></td>
              </tr>
              <tr>
                <td>Presión Diastólica</td>
                <td><input name="PresionDiastolica" class="easyui-numberbox" id="PresionDiastolica" style="width:145px" data-options="prompt:'Diastolica',min:20,max:200" value="<?php echo $PresionDiastolica;?>" /></td>
                <td><font color="#FF0000">60 a 90 (90-100 consultar)</font></td>
              </tr>
              <tr>
                <td>Pulso</td>
                <td><input name="Pulso" class="easyui-numberbox" id="Pulso" style="width:145px" data-options="prompt:'Pulso',min:50,max:250" value="<?php echo $Pulso;?>" /></td>
                <td><font color="#FF0000">50 a 100</font></td>
              </tr>
              <tr>
                <td>Temperatura Oral (ºC)</td>
                <td><input name="Temperatura" class="easyui-numberbox" id="Temperatura" style="width:145px" data-options="                
                        prompt:'Temperatura',min:35,max:46,precision:1,
                        valueField: 'id',
                        textField: 'text',        
                        onChange: function(rec){
                        var url = cambiarTemperatura(); }" value="<?php echo $Temperatura;?>" /></td>
                <td><font color="#FF0000">36 a 37.5 ºC (oral)</font><div id="fiebre" style="color:#F00;"></div></td>
              </tr>
              <tr>
                <td>Hemoglobina (gr/dl)</td>
                <td><input name="Hemoglobina" class="easyui-numberbox" id="Hemoglobina" style="width:145px" data-options="prompt:'Hemoglobina',min:0,max:100,precision:1" value="<?php echo $Hemoglobina;?>" /></td>
                <td><font color="#FF0000">mínimo 12.5 (gr/dl)</font></td>
              </tr>
              <tr>
                <td>Metodología Hb</td>
                <td><select class="easyui-combobox" name="MetodologiaHb" id="MetodologiaHb"  style="width:145px" >
                  <option value="0">Ninguno</option>
                  <?php
						  $listar=SIGESA_ListarMetodologia_M('cw_hb');//$tipo=cw_hb,cw_hcto
						   if($listar != NULL) { 
							 foreach($listar as $item){?>
                  <option value="<?php echo $item["IdMetodologia"]?>" <?php if($item["IdMetodologia"]==$MetodologiaHb){?> selected <?php } ?> ><?php echo $item["Descripcion"]?></option>
                  <?php } } ?>
                </select></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Hematocrito</td>
                <td><input name="Hematocrito" class="easyui-numberbox" id="Hematocrito" style="width:145px" data-options="prompt:'Hematocrito',min:0,precision:1" value="<?php echo $Hematocrito;?>" /></td>
                <td><font color="#FF0000">mínimo 38%</font></td>
              </tr>
              <tr>
                <td>Metodología Hcto</td>
                <td><select class="easyui-combobox" name="MetodologiaHcto" id="MetodologiaHcto"  style="width:145px" >
                  <option value="0">Ninguno</option>
                  <?php
						  $listar=SIGESA_ListarMetodologia_M('cw_hcto');//$tipo=cw_hb,cw_hcto
						   if($listar != NULL) { 
							 foreach($listar as $item){?>
                  <option value="<?php echo $item["IdMetodologia"]?>" <?php if($item["IdMetodologia"]==$MetodologiaHcto){?> selected <?php } ?> ><?php echo $item["Descripcion"]?></option>
                  <?php } } ?>
                </select></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3"><font color="#FF0000">La información de color rojo define los valores NORMALES para mayores de 18 años</font></td>
              </tr>
            </table>
          </fieldset>
          </td>
          
          <td width="50%">          
          <fieldset style="position:relative;top: -8px;">
          <legend>Datos si hay Diferimiento</legend>
            <table width="97%">
              <tr>
                <td width="27%">¿Apto?</td>
                <td colspan="2"><Select style="width:305px" class="easyui-combobox" id="EstadoApto" name="EstadoApto"  data-options="required:true,
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarEstadoApto(); }">
                  <option value="">Seleccione</option>
                  <option value="2" <?php if('2'==$EstadoApto){?> selected <?php } ?> >2=SI-Apto</option>
                  <option value="1" <?php if('1'==$EstadoApto){?> selected <?php } ?> >1=NO-No Apto Temporal</option>
                  <option value="0" <?php if('0'==$EstadoApto){?> selected <?php } ?> >0=NO-No Apto Definitivo</option>
                </select>
                <input name="NroDonacionAct" id="NroDonacionAct" type="hidden" value="<?php echo $NroDonacion; ?>" />
                </td>
              </tr>
            <!--<tr>
                <td id="Donante"><strong>Nro Donante</strong></td>
                <td colspan="3"><input style="width:145px" class="easyui-textbox" id="NroDonacion" name="NroDonacion" /></td>
            </tr>-->
              <tr>
                <td>Motivo Principal Rechazo</td><!--id="MotivoRechazo"-->
                <td colspan="2">
                	<?php /*?><Select style="width:305px" class="easyui-combobox" id="MotivoRechazo" name="MotivoRechazo" data-options="
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = cambiarDiferimientoRechazo(); }">
                      <option value="">Seleccione</option>
                      <?php
                                  $listar=SIGESA_ListarMotivoRechazo_M('Evaluacion'); //1 Evaluacion, 2 Extraccion, 3 Tamizaje
                                   if($listar != NULL) { 
                                     foreach($listar as $item){?>
                      <option value="<?php echo $item["IdMotivoRechazo"].'|'.$item["Diferimiento"].'|'.$item["Tiempo"].'|'.$item["TipoTiempo"]?>" ><?php echo $item["Descripcion"]?></option>
                      <?php } } ?>
                    </select><?php */?>                    
                	<!--<select style="width:305px" class="easyui-combobox" id="MotivoRechazo" name="MotivoRechazo" ></select>-->
                    <input style="width:305px" class="easyui-combobox" id="MotivoRechazo" name="MotivoRechazo" value="<?php echo $MotivoRechazo;?>" />
                	<input type="hidden" name="IdMotivoRechazo" id="IdMotivoRechazo" value="<?php echo $IdMotivoRechazo;?>" /></td>
              </tr>
              <tr>
                <td>Fecha  Diferimiento Rechazo</td>
                <td width="28%">Desde<input class="easyui-datebox" style="width:105px;" id="FechaInicioRechazo" name="FechaInicioRechazo" data-options="
                    valueField: 'id',
                    textField: 'text',        
                    onChange: function(rec){
                    var url = cambiarDiferimientoRechazo(); },
                    prompt:'Fecha Inicio'" validType="validDate" value="<?php echo $FechaInicioRechazo;?>" /></td>
                <td width="45%">Hasta<input class="easyui-datebox" style="width:105px;" name="FechaFinRechazo" id="FechaFinRechazo" data-options="prompt:'FechaEspera'" value="<?php echo $FechaFinRechazo;?>" readonly /></td>
              </tr>
              <tr>
                <td>Motivo por Medicamento</td>
                <td colspan="2"><Select style="width:305px" class="easyui-combobox" id="Medicacion" name="Medicacion" data-options="
                    valueField: 'id',
                    textField: 'text',        
                    onSelect: function(rec){
                    var url = cambiarMedicacion(); }">
                  <option value="">Sin Medicacion</option>
                  <?php
							  $listar=SIGESA_ListarMedicacion_M(''); 
							   if($listar != NULL) { 
								 foreach($listar as $item){?>
                  <option value="<?php echo $item["IdMedicacion"].'|'.$item["Diferimiento"].'|'.$item["Tiempo"].'|'.$item["TipoTiempo"]?>" <?php if($item["IdMedicacion"]==$IdMedicacion){?> selected <?php } ?> ><?php echo $item["Descripcion"]?></option>
                  <?php } } ?>
                </select>
                <input type="hidden" name="IdMedicacion" id="IdMedicacion" value="<?php echo $IdMedicacion;?>" /></td>
              </tr>
              <tr>
                <td>Fecha  Diferimiento Medicamento</td>
                <td>Desde<input class="easyui-datebox" style="width:105px;" id="FechaMedicacion" name="FechaMedicacion" data-options="
                    valueField: 'id',
                    textField: 'text',        
                    onChange: function(rec){
                    var url = cambiarMedicacion(); },
                    prompt:'Fecha Inicio'" validType="validDate" value="<?php echo $FechaMedicacion;?>" /></td>
                <td>Hasta<input class="easyui-datebox" style="width:105px;" name="FechaEspera" id="FechaEspera" data-options="prompt:'FechaEspera'" value="<?php echo $FechaEspera;?>" readonly /></td>
              </tr>
              <tr>
                <td>Observaciones Generales</td>
                <td colspan="2">
                	<!--<input name="Observacion" class="easyui-textbox" id="Observacion" style="width:250px" />-->
           	   		<input style="width:305px;height:70px" class="easyui-textbox" multiline="true" name="Observacion" id="Observacion" value="<?php echo $Observacion;?>" />
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </fieldset></td>
        </tr>      
        
      </table> 
</div>
</form>

</body></html>