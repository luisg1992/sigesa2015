<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
	<?php
			//Funcion en Funciones.php
			$EstadoDescripcion=DescripcionEstadoHemocomponente($Estado);						
			
			$ObtenerGrupoSanguineo=ObtenerGrupoSanguineo_M($IdGrupoSanguineo);
			if($ObtenerGrupoSanguineo!=NULL){
				$DescripcionGrupoSanguineo=$ObtenerGrupoSanguineo[0]["Descripcion"];
			}else{
				$DescripcionGrupoSanguineo="Todos";
			}
	?> 
    <p><strong>Hemocomponente: </strong><?php echo $TipoHem ?> &nbsp; 
       <strong>Estado: </strong><?php echo $EstadoDescripcion ?> &nbsp; 
       <strong>Grupo Sanguineo: </strong><?php echo $DescripcionGrupoSanguineo ?></p>   
	<?php 
		$SIGESA_BSD_BuscarStock=SIGESA_BSD_BuscarStock_M($TipoHem,$IdGrupoSanguineo,$Estado,$Vencido); 
		if($SIGESA_BSD_BuscarStock!=NULL){	
	?>     
<table class="table">
    	<thead>
            <tr align="center">
              <th>Nro</th>
              <th>GS</th>
              <th>NroDonacion</th>
              <th>NroTabuladora</th>
              <th>TipoHem</th>
              <th>SNCS</th>
              <th>FechaExtraccion</th>
              <th>FechaVencimiento</th>
              <th>Volumen(mL)</th>
              <th>Estado</th>
            </tr>
        </thead>
        <tbody>
			<?php                  
                $i=0;
                foreach ($SIGESA_BSD_BuscarStock as $item){
                $i=$i+1;	
				
				if($item["Estado"]=='0'){				
					$EstadoDescripcion='<font color="#FF0000">Elim.</font>';
					
				}else if($item["Estado"]=='1'){
					$EstadoDescripcion='<font color="#0000FF">Dispon.</font>';
					
				}else if($item["Estado"]=='2'){
					$EstadoDescripcion='<font color="#FF9900">Reserv.</font>';	
								
				}else if($item["Estado"]=='3'){ //NO ES UN INGRESO, ES TIPO F
					$EstadoDescripcion='<font color="#0000FF">Fracción</font>';
					
				}else if($item["Estado"]=='4'){
					$EstadoDescripcion='<font color="#FF9900">Custodia</font>';
					
				}else if($item["Estado"]=='5' || $item["Estado"]=='6'){
					$EstadoDescripcion='<font color="#8080FF">Donado</font>';				
				}
            ?>        
            <tr align="center">
              <td><?php echo $i; ?></td>
              <td><?php echo $item['GrupoSanguineo']; ?></td>	
              <td><?php echo $item['NroDonacion']; ?></td>
              <td><?php echo $item['NroTabuladora']; ?></td>
              <td><?php echo $item['TipoHem']; ?></td>
              <td><?php echo $item['SNCS']; ?></td>
              <td><?php echo vfecha(substr($item["FechaExtraccion"],0,10)); ?></td>          	
              <td><?php echo vfecha(substr($item["FechaVencimiento"],0,10)).' '.substr($item["FechaVencimiento"],11,5); ?></td>
              <td><?php echo $item['VolumenRestante']; ?></td>
              <td><?php echo $EstadoDescripcion; ?></td>
            </tr> 
            
            <?php 
                }
            ?> 
        </tbody>        
    </table>
    <?php 
		}else{
			echo "SIN HEMOCOMPONENTES";
		}
	?> 

</body>
</html>