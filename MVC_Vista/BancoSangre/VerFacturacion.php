<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ver Facturacion</title>
</head>
		<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/demo/demo.css">
        <style>
            html, body { height: 100%;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
        
        <script type="text/javascript" >
		
		$.extend($("#FechaInicio").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});
		$.extend($("#FechaInicio").datebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){  
					var date = $.fn.datebox.defaults.parser(value);
					var s = $.fn.datebox.defaults.formatter(date);	
					
					if(s==value){
						return true;
					}else{								
						//$("#FecMotivo" ).datebox('setValue', '');
						//$("#EdadPaciente").textbox('setValue','');
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});				
			
			function salir(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ListaEspera&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";				
			}
			
			function Buscar(){
				document.getElementById("form1").submit();
		    }
		</script>        
        
        <style type="text/css">
			.datagrid-row-over td{ /*color cuando pasas el mouse en la fila(hover)*/
				/*background:#D0E5F5;*/
				background:#A3ABFA;
			}
			.datagrid-row-selected td{ /*color cuando das click en la fila*/
				/*background:#FBEC88;*/
				background:#5F5FFA;
			}
	    </style>
        
		<style>
            .icon-filter{
                background:url('../../MVC_Complemento/easyui/filtro/filter.png') no-repeat center center;
            }
        </style>     
        
<body>

		<div style="margin:0px 0;"></div>    
        <div id="tb" style="padding:5px;height:auto">
       		<div style="margin-bottom:5px">
                <!--<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="evaluacion()">Evaluación Predonación</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onClick="editar()" >Actualizar Recepción</a> -->
                <a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Volver a Cargar</a>              
                <a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onClick="salir();">Salir</a>
        	</div> 
          <?php /*?><form name="form1" id="form1" action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ConsultarPacientesHemoglobinaMenor7&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" >
            <fieldset>
			  <legend style="color:#03C"><strong>Busqueda:</strong></legend>
				<table width="100%" style="font-size:12px;">
				  <tr align="center">
					<th bgcolor="#D6D6D6"><strong>Fecha Inicio</strong></th>
					<th bgcolor="#D6D6D6"><strong>Fecha Final</strong></th>
                    <th bgcolor="#D6D6D6"><strong>Buscar</strong></th>
				  </tr>
				  <tr align="center">
					<td><!--<input class="easyui-numberbox" style="width:110px" id="NroHistoriaClinicaBusRec" name="NroHistoriaClinicaBusRec" data-options="prompt:'Nº DNI'" maxlength="8">--><input name="FechaInicio" type="text" id="FechaInicio" class="easyui-datebox" style="width:120px;" value="<?php echo $FechaInicioB ?>" data-options="prompt:'Fecha Inicio',
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" /></td><!-- searcher:BuscarPacientesHC,-->
					<td><input name="FechaFinal" type="text" id="FechaFinal" class="easyui-datebox" style="width:120px;" value="<?php echo $FechaFinalB ?>" data-options="prompt:'Fecha Final',
                        valueField: 'id',
                        textField: 'text',        
                        onSelect: function(rec){
                        var url = document.form1.submit(); }" /></td> 
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'" onClick="Buscar();">Buscar</a></td>
				  </tr>                         
				</table> 
            </fieldset>   
            </form><?php */?>
        </div>       

        
       <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Listado de Facturaciones en Banco de Sangre" style="width:80%;height:90%" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,fitColumns:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10"> <?php /*?>de la Cuenta<?php echo ' '.$IdCuentaAtencion ?> el <?php echo $XFechaDespacho ?><?php */?> 
		<thead>
			  <tr>
                <th field="IdCuentaAtencion" width="90">CuentaAtencion</th>
			    <th field="IdOrden" width="90">IdOrden</th>
			    <th field="dfinanciamiento" width="90">Financiamiento</th>
			    <th field="FechaCreacion" width="90">Fecha Creacion</th>
			    <th field="FechaDespacho" width="90">Fecha Despacho</th>
			    <th field="NombreServicio" width="90">Nombre Servicio</th>
                <th field="EstadoFacturacion" width="90">Estado Facturacion</th>
                <th field="TotalOrden" width="90" align="right">Total Orden</th>
         </thead>
	</table>
     
   <script type="text/javascript">
	 
	function getData(){
			var rows = [];			
			<?php			
			 $i = 1;	
			//$FechaInicio= gfecha($FechaInicioB);	
			//$FechaFinal= gfecha($FechaFinalB);				
			//$listado=VerFacturacionesBancoDeSangreM($IdCuentaAtencion,$FechaDespacho);
			if($listado!=NULL){
				foreach($listado as $item){				 	
				 				 
				 $FechaCreacion=$item["FechaCreacion"];	
					if($FechaCreacion!=NULL){
						$FechaCreacion=vfecha(substr($item["FechaCreacion"],0,10)).' '.substr($item["FechaCreacion"],11,8);					
					}
					
				  $FechaDespacho=$item["FechaDespacho"];	
					if($FechaDespacho!=NULL){
						$FechaDespacho=vfecha(substr($item["FechaDespacho"],0,10)).' '.substr($item["FechaDespacho"],11,8);					
					}	
					
					$TotalOrden=number_format($item['TotalOrden'], 2, '.', ' ');//round($item['TotalOrden'],2);			
						
					 ?>
						rows.push({
							Nro: '<?php echo $i; ?>',
							IdOrden: '<?php echo $item['IdOrden'];?>',
							IdCuentaAtencion: '<?php echo  $item['IdCuentaAtencion'];?>',							
							dfinanciamiento: '<?php echo  $item['dfinanciamiento'];?>',	
							FechaCreacion:'<?php echo $FechaCreacion;?>',
							FechaDespacho:'<?php echo $FechaDespacho;?>',
							NombreServicio: '<?php echo $item['NombreServicio'];?>',
							EstadoFacturacion: '<?php echo $item['EstadoFacturacion'];?>',
							TotalOrden: '<?php echo 'S/ '.$TotalOrden;?>',
						
							
						});			
					<?php  $i += 1;	
				}
			 }
		 ?>
		 return rows;		
	}
		
		
		$('#dg').datagrid({
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});		
		
		$(function(){
			var dg =$('#dg').datagrid({data:getData()}).datagrid({			
			//var dg =$('#dg').datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});			

    </script>  
   
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>

      
</body>
</html>