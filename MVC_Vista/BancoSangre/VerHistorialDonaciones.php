<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HOSPITAL NACIONAL DANIEL ALCIDES CARRION - SERVICIO DE HEMOTERAPIA Y BANCO DE SANGRE</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Fecha --> 
    <link rel="stylesheet" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css" />     
    
<!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/tables/site.js"></script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async></script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<script type="text/javascript" class="init">
    $(document).ready(function () {		
		
        $('#dataTables-example').DataTable({
            dom: 'Bfrtip',
			 lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar Todo' ]
        ],
       
            buttons: [ 'pageLength',
				//'colvis',
                <!--'copyHtml5','csvHtml5','excelHtml5',-->
				<!--'copy', 'csv', 'excel','print',-->	
				{ //1
					extend: 'copyHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //2
					extend: 'csvHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //3
					extend: 'excelHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //4
					extend: 'print',
					exportOptions: {columns: ':visible'}
				},
								
                {//valores por defecto orientation:'portrait' and pageSize:'A4',
					
                    extend: 'pdfHtml5', header: true,	 sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",						
					  					
					                 
                    orientation: 'landscape',
                    pageSize: 'A4',
					exportOptions: {columns: ':visible'}
                } //,				
				
				
            ]
        });//FIN dataTables-example
		
		$('#dataTables-example2').DataTable({
			 "searching": false,
             dom: 'Bfrtip',
			 lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar Todo' ]
        ],
       
            buttons: [ 'pageLength',
				//'colvis',
                <!--'copyHtml5','csvHtml5','excelHtml5',-->
				<!--'copy', 'csv', 'excel','print',-->	
				{ //1
					extend: 'copyHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //2
					extend: 'csvHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //3
					extend: 'excelHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //4
					extend: 'print',
					exportOptions: {columns: ':visible'}
				},
								
                {//valores por defecto orientation:'portrait' and pageSize:'A4',
					
                    extend: 'pdfHtml5', header: true,	 sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",						
					  					
					                 
                    orientation: 'landscape',
                    pageSize: 'A4',
					exportOptions: {columns: ':visible'}
                } //,				
				
				
            ]
        });//FIN dataTables-example2
		
    });
</script> 
    
    <script type="text/javascript">		
			function cancelar(){
				window.location = "../BancoSangre/BancoSangreC.php?acc=ListaEspera&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>";
			}           
		  
		   function AbrirSolicitud(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=AbrirSolicitud&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>";		
			}
			
			function BuscarPacientesDebenSangre(){
				location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=BuscarPacientesDebenSangre&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>&IdListItem=<?php echo $_GET['IdListItem'] ?>&NroHistoriaClinica=<?php echo $_GET['NroHistoriaClinicaBusRec'] ?>";			
			}
			
   </script> 
   
</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper">            
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary"> <!--panel-default-->
                        <div class="panel-heading">TRANSFUSIONES DESPACHADOS</div>
                        <div class="panel-body">
                        
     <?php			
			
	$Buscarpaciente=SIGESA_BSD_BuscarReceptorFiltro_M($IdPaciente,'IdPaciente');
	?>      
	  
                        <form action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=ReporteSNCS&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>" name="form1" id="form1" method="post">   
                        		<div class="" align="right">
                                                            
                                 <?php 
								 	//ABRIR SOLICITUD
									$IdListItem=isset($_GET['IdListItem']) ? $_GET['IdListItem'] : '';
									$Listar_Permisos_Usuario=Listar_Permisos_Usuario_M($_GET['IdEmpleado'],$IdListItem);
									$Modificar=$Listar_Permisos_Usuario[0]["Modificar"];
									
									//CONSULTAS: BuscarPacientesDebenSangre
									$NroHistoriaClinicaBusRec=isset($_GET['NroHistoriaClinicaBusRec']) ? $_GET['NroHistoriaClinicaBusRec'] : '';
									
									if($Modificar>=1){
									?>									
                                     <input class="btn btn-default" type="button" onclick="AbrirSolicitud()" value="Regresar"/>
									<?php }else if ( trim($NroHistoriaClinicaBusRec)!='' ){ ?>
                                         <input class="btn btn-danger" type="button" onclick="BuscarPacientesDebenSangre()" value="Regresar"/>	
                                    <?php }else{ ?>
                                         <input class="btn btn-danger" type="button" onclick="cancelar()" value="Regresar"/>	
                                    <?php } ?>
                                
                              </div>                             
                            
                            <!--fila 2-->                         
                            <div class="form-group row"> 
                            	<label class="control-label col-xs-1">Paciente:</label>
                                 <div class="col-xs-3"><?php echo $Buscarpaciente[0]['ApellidoPaterno'].' '.$Buscarpaciente[0]['ApellidoMaterno'].' '.$Buscarpaciente[0]['PrimerNombre'].' '.$Buscarpaciente[0]['SegundoNombre'].' '.$Buscarpaciente[0]['TercerNombre']; ?></div>
                                                                               
                                <label class="control-label col-xs-1">Fecha Nacim.: </label>
                                <div class="col-xs-1"><?php echo vfecha(substr($Buscarpaciente[0]["FechaNacimiento"],0,10)); ?></div>
                                
                                <label class="control-label col-xs-1">Grupo Sang.:</label>
                                <div class="col-xs-1"><?php echo $Buscarpaciente[0]["GrupoSanguineoRec"] ?></div>                                
                                
                   		  </div><!--fin fila 2-->
                        
                                          
                        </form>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                          <div class="dataTable_wrapper">                            
                       	    <p><strong>DATOS SOLICITUD</strong></p>
                              <table width="100%" class="table table-bordered" id="dataTables-example"> <!--table-hover-->
                                	<thead>
                                        <tr class="text-center">
                                            <th>&nbsp;</th>
                                            <td><strong>Codigo</strong></td>
                                            <td><strong>#SNCS</strong></td>
                                            <td><strong>Usuario Despacho</strong></td>
                                            <td><strong>Fecha Recepción</strong></td>
                                            <td><strong>Calidad</strong></td>
                                            <td><strong>Apellidos y Nombres</strong></td>
                                            <td><strong>Servicio</strong></td>
                                            <td><strong>Nota</strong></td>
                                            <td><strong>Regresó BUT</strong></td>
                                            <td><strong>Orden</strong></td>
                                            <td><strong>Estado</strong></td>
                                      </tr>                                    
                                    </thead>
                                    <tbody>
                                      <?php 									  		
										$HistorialDonaciones=HistorialDonacionesM($IdPaciente); 
										if($HistorialDonaciones!=NULL){									
											$i=0;
											foreach ($HistorialDonaciones as $item){
											$i=$i+1;				
											$FechaRecepcion=isset($item['FechaRecepcion']) ? (vfecha(substr($item['FechaRecepcion'],0,10))) : '';	
											$FechaDespacho=isset($item['FechaDespacho']) ? (vfecha(substr($item['FechaDespacho'],0,10))) : '';	
											$DatosPostulante=$item['ApellidosPostulante'].' '.$item['NombresPostulante'];
											
											if($item['RegresoBUT']==0){	
												$RegresoBUT='NO';
											}else{	
												$RegresoBUT='SI';	
											}	
											
											$EstadoDescripcion='';
											if($item["Estado"]=='3'){ 
												$EstadoDescripcion='<font color="#FF0000">Falta Facturar</font>';
												
											}else if($item["Estado"]=='4'){
												$EstadoDescripcion='<font color="#0000FF">Facturado</font>';
												
											}else if($item["Estado"]=='5'){
												$EstadoDescripcion='<font color="#0000FF">Cerrado</font>';
												
											}												
									  ?>                                                                     						
 
                                      <tr bgcolor="<?php echo $colorestado; ?>" class="text-center">
                                            <td align="center"><?php echo $i;?></td>
                                            <td align="center"><?php echo $item['Codigo']; ?></td>
                                            <td align="center"><?php echo $item["TSNCS"]; ?></td>
                                            <td align="center"><?php echo $item['UsuDespacho']; ?></td>
                                            <td align="center"><?php echo $FechaRecepcion; ?></td>
                                            <td align="center"><?php echo $item['TipoRequerimiento']; ?></td>
                                            <td align="center"><?php echo $DatosPostulante; ?></td>
                                            <td align="center"><?php echo mb_strtoupper($item['Servicio']); ?></td>
                                            <td align="center"><?php echo $item['Observacion']; ?></td>
                                            <td align="center"><?php echo $RegresoBUT; ?></td>
                                            <td align="center"><?php echo $item["IdOrden"]; ?></td>
                                            <td align="center"><?php echo $EstadoDescripcion; ?></td>
                                      </tr> 
                                        <?php 
											}
										 } 
										?>                                       
                                    </tbody>
                                    
                              </table>
                              <p><strong>DATOS PACIENTE Y DONANTE</strong></p>  
                            <table width="100%" class="table table-bordered" id="dataTables-example2"> <!--table-hover-->
                               	  <thead>
                                      <tr class="text-center">
                                        <th>&nbsp;</th>
                                        <td>&nbsp;</td>
                                        <td colspan="7"><strong>PACIENTE</strong></td>
                                        <td colspan="7"><strong>DONANTE</strong></td>
                                      </tr>
                                      <tr class="text-center">
                                          <th>&nbsp;</th>
                                          <td><strong>#SNCS</strong></td>
                                          <td><strong>UsuReg Fenotipo</strong></td>
                                          <td><strong>FechaReg Fenotipo</strong></td>
                                          <td><strong>Grupo S.</strong></td>
                                          <td><strong> Ac. Irregulares</strong></td>
                                          <td><strong>Coombs Directo</strong></td>
                                          <td><strong>DVI</strong></td>
                                          <td><strong>Fenotipo</strong></td>
                                          <td><strong>UsuReg Verificación</strong></td>
                                          <td><strong>FechaReg Verificación</strong></td>
                                          <td><strong>Grupo S.</strong></td>
                                          <td><strong> Ac. Irregulares</strong></td>
                                          <td><strong>Coombs Directo</strong></td>
                                          <td><strong>DVI</strong></td>
                                          <td><strong>Fenotipo</strong></td>
                                    </tr>                                    
                                  </thead>
                                  <tbody>
                                    <?php 									  		
										$HistorialDonaciones=HistorialDonacionesM($IdPaciente); 
										if($HistorialDonaciones!=NULL){									
											$i=0;
											foreach ($HistorialDonaciones as $item){
											$i=$i+1;				
											$FecRegFenotipo=isset($item['FecRegFenotipo']) ? (vfecha(substr($item['FecRegFenotipo'],0,10))) : '';												
											$FechaVerifica=isset($item['FechaVerifica']) ? (vfecha(substr($item['FechaVerifica'],0,10))) : '';												
																								
									  ?>                                                                     						
 
                                      <tr bgcolor="<?php echo $colorestado; ?>" class="text-center">
                                          <td align="center"><?php echo $i;?></td>
                                          <td align="center"><?php echo $item["TSNCS"]; ?></td>
                                          <td align="center"><?php echo $item['UsuFenotipo']; ?></td>
                                          <td align="center"><?php echo $FecRegFenotipo; ?></td>
                                          <td align="center"><?php echo $item['GrupoSanguineoSolicitud']; ?></td>
                                          <td align="center"><?php echo $item['DeteccionCI']; ?></td>
                                          <td align="center"><?php echo $item['CoombsDirecto']; ?></td>
                                          <td align="center"><?php echo $item['DVI']; ?></td>
                                          <td align="center"><?php echo $item['Fenotipo']; ?></td>
                                          <td align="center"><?php echo $item['UsuVerifica']; ?></td>
                                          <td align="center"><?php echo $FechaVerifica; ?></td>
                                          <td align="center"><?php echo $item['GrupoSanguineoVerifica']; ?></td>
                                          <td align="center"><?php echo $item['DeteccionCIVerifica']; ?></td>
                                          <td align="center"><?php echo $item['CoombsDirectoVerifica']; ?></td>
                                          <td align="center"><?php echo $item['DVIVerifica']; ?></td>
                                          <td align="center"><?php echo $item['FenotipoVerifica']; ?></td>
                                      </tr> 
                                      <?php 
											}
										 } 
										?>                                       
                                  </tbody>
                                    
                              </table>
                            </div>
                            <!-- /.table-responsive -->                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - alertify - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
    
    <!-- Fecha -->     
    <script src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>  	

</body>

</html>