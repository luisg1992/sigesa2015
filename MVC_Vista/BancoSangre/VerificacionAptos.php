<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	
	$TieneSNCS=isset($_REQUEST['TieneSNCS']) ? $_REQUEST['TieneSNCS'] : '9';
    
	//resto 42 days
	$fecha_actual = date("d-m-Y");
	$FechaReg_InicioX=date("d/m/Y",strtotime($fecha_actual."- 42 days"));	
    $FechaReg_Inicio=isset($_REQUEST['FechaReg_Inicio']) ? $_REQUEST['FechaReg_Inicio'] : $FechaReg_InicioX;  
	 
    $FechaReg_Final=isset($_REQUEST['FechaReg_Final']) ? $_REQUEST['FechaReg_Final'] : date('d/m/Y');    
    $RealizaNAT=isset($_REQUEST['RealizaNAT']) ? $_REQUEST['RealizaNAT'] : '9';  
    $TipoHemocomponente=isset($_REQUEST['TipoHemocomponente']) ? $_REQUEST['TipoHemocomponente'] : ''; 

	$FechaActual = vfecha(substr($FechaServidor,0,10));
	$HoraActual = substr($FechaServidor,11,8);	
	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_REQUEST['IdEmpleado']);
	$Usuario=$ListarUsuarioxIdempleado[0]['Usuario'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HOSPITAL NACIONAL DANIEL ALCIDES CARRION - SERVICIO DE HEMOTERAPIA Y BANCO DE SANGRE</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Fecha --> 
    <link rel="stylesheet" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css" />     
    
<!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/tables/site.js"></script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async></script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<script type="text/javascript" class="init">
    $(document).ready(function () {
		
		

		// Append a caption to the table before the DataTables initialisation
    	//var FechaReg_Inicio=document.getElementById('FechaReg_Inicio').value;
		//var FechaReg_Final=document.getElementById('FechaReg_Final').value;
 
 		//$('#dataTables-example').append("<td class='consultas'>No se encontraron registros!!</td>");
        $('#dataTables-example').DataTable({
			"columnDefs": [
            {
                "targets": [ 9,10,11,12 ],
                //"visible": false,
                "orderData": false
            }/*,
            {
                "targets": [ 3 ]
            }*/
        ],
            dom: 'Bfrtip',
			 lengthMenu: [
            [ 20, 25, 50, -1 ],
            [ '20 Filas', '25 Filas', '50 Filas', 'Mostrar Todo' ]
        ],
       
            buttons: [ 'pageLength',
				//'colvis',
                <!--'copyHtml5','csvHtml5','excelHtml5',-->
				<!--'copy', 'csv', 'excel','print',-->	
				{ //1
					extend: 'copyHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //2
					extend: 'csvHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //3
					extend: 'excelHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //4
					extend: 'print',
					exportOptions: {columns: ':visible'}
				}				
				
				
            ]
        });
    });
</script> 
    
    <script type="text/javascript">	
		   
		   function buscar(){
			   var FechaReg_Inicio=document.getElementById('FechaReg_Inicio').value;
			   var FechaReg_Final=document.getElementById('FechaReg_Final').value;

               var TieneSNCS=document.getElementById('TieneSNCS').value;

               if(TieneSNCS==0){
                  document.getElementById('TipoHemocomponente').value='';
               }


			   if((FechaReg_Inicio=="") || (FechaReg_Final=="")){
				   alertify.error('Debe ingresar ambas fechas');
			   }else{
				   $('#form1').submit();	
			   }
			}
			
			function cancelar(){
				window.location = "../BancoSangre/BancoSangreC.php?acc=VerificacionAptos&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>";
			}		   
		   
   </script> 
   
   <script>
    $(function () {

//Array para dar formato en español
        $.datepicker.regional['es'] =
                {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        $.datepicker.setDefaults($.datepicker.regional['es']);

//miDate: fecha de comienzo D=días | M=mes | Y=año
//maxDate: fecha tope D=días | M=mes | Y=año
        //var f = new Date();
        //diaactual=f.getDate();
        // $( "#Ipd_frecibido").datepicker({ minDate: "-"+diaactual+"D", maxDate: "+1M +10D" });    
        //$( "#NT_FGUI" ).datepicker({ minDate: "-1M", maxDate: "+1M +10D" }); 
        $("#FechaReg_Inicio").datepicker();
        $("#FechaReg_Final").datepicker();
		$("#FechaRecibe").datepicker();

    });	
	
	function RegVerificacion(NroDonacion,IdGrupoSanguineo,IdPostulante,SNCS_PG,SNCS_PFC,SNCS_PQ,IdFraccionamiento,NoModificaDeteccionCI,NoModificaCoombsDirecto){		
			location.href="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegVerificacionAptos&NroDonacion="+NroDonacion+"&IdGrupoSanguineo="+IdGrupoSanguineo+"&IdPostulante="+IdPostulante+"&SNCS_PG="+SNCS_PG+"&SNCS_PFC="+SNCS_PFC+"&SNCS_PQ="+SNCS_PQ+"&IdFraccionamiento="+IdFraccionamiento+"&NoModificaDeteccionCI="+NoModificaDeteccionCI+"&NoModificaCoombsDirecto="+NoModificaCoombsDirecto+"&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>";	
			
			// $('#my_modalReg').modal('show');
			 //$('#tablaRec').load("../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=RegVerificacionAptos&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>",{NroDonacion:NroDonacion,IdGrupoSanguineo:IdGrupoSanguineo,IdPostulante:IdPostulante});
				
	}	
		
		function obtenerselmanualPG() {
			//var checked = document.form1.querySelectorAll('input[type=checkbox]:checked');
			var checked = document.form1.querySelectorAll('input[class=PG]:checked');
			var Emp_cant = 0;
			if (checked.length == 0) {
				//SINO HA SELECCIONADO
				document.getElementById('cantselmanualPG').value = 0;
				return false;
			} 
			for (var i = 0, length = checked.length; i < length; i++) {
				if (checked[i].value != '') {
					//					
					var checkedPFC = document.form1.querySelectorAll('input[class=PFC]:checked');			
					if (checkedPFC.length > 0) {
						//SINO HA SELECCIONADO
						alert("Ha seleccionado Liberar PFC");
						checked[i].checked=false;
						return false;
					}
					var checkedPQ = document.form1.querySelectorAll('input[class=PQ]:checked');			
					if (checkedPQ.length > 0) {
						//SINO HA SELECCIONADO
						alert("Ha seleccionado Liberar PQ");
						checked[i].checked=false;
						return false;
					} 
					//
					Emp_cant++;
					document.getElementById('cantselmanualPG').value = Emp_cant;
				} 
			}	
		}
		
		function obtenerselmanualPFC() {			
			var checked = document.form1.querySelectorAll('input[class=PFC]:checked');
			var Emp_cant = 0;
			if (checked.length == 0) {
				//SINO HA SELECCIONADO
				document.getElementById('cantselmanualPFC').value = 0;
				return false;
			} 
			for (var i = 0, length = checked.length; i < length; i++) {
				if (checked[i].value != '') {					
					//
					var checkedPG = document.form1.querySelectorAll('input[class=PG]:checked');			
					if (checkedPG.length > 0) {
						//SINO HA SELECCIONADO
						alert("Ha seleccionado Liberar PG");
						checked[i].checked=false;
						return false;
					}
					var checkedPQ = document.form1.querySelectorAll('input[class=PQ]:checked');			
					if (checkedPQ.length > 0) {
						//SINO HA SELECCIONADO
						alert("Ha seleccionado Liberar PQ");
						checked[i].checked=false;
						return false;
					} 
					//					
					Emp_cant++;
					document.getElementById('cantselmanualPFC').value = Emp_cant;
				} 
			}	
		}
		
		function obtenerselmanualPQ() {
			var checked = document.form1.querySelectorAll('input[class=PQ]:checked');
			var Emp_cant = 0;
			if (checked.length == 0) {
				//SINO HA SELECCIONADO
				document.getElementById('cantselmanualPQ').value = 0;
				return false;
			} 
			for (var i = 0, length = checked.length; i < length; i++) {
				if (checked[i].value != '') {
					//
					var checkedPG = document.form1.querySelectorAll('input[class=PG]:checked');			
					if (checkedPG.length > 0) {
						//SINO HA SELECCIONADO
						alert("Ha seleccionado Liberar PG");
						checked[i].checked=false;
						return false;
					}
					var checkedPFC = document.form1.querySelectorAll('input[class=PFC]:checked');			
					if (checkedPFC.length > 0) {
						//SINO HA SELECCIONADO
						alert("Ha seleccionado Liberar PFC");
						checked[i].checked=false;
						return false;
					} 
					//	
					Emp_cant++;
					document.getElementById('cantselmanualPQ').value = Emp_cant;
				} 
			}	
		}
		
		function LiberarCuarentena() {
			           
			var checkedPG = document.form1.querySelectorAll('input[class=PG]:checked');			
			var checkedPFC = document.form1.querySelectorAll('input[class=PFC]:checked');
			var checkedPQ = document.form1.querySelectorAll('input[class=PQ]:checked');
			
			if (checkedPG.length == 0 && checkedPFC.length == 0 && checkedPQ.length == 0) {
				//SINO HA SELECCIONADO
				alert("Seleccione Hemocomponente a Liberar");
				return false;
			}
			var checked; cantidad=0;
			if(checkedPG.length>0){
				checked=checkedPG;
				tipohem='PG';
				var cantselmanual= document.getElementById('cantselmanualPG').value;
				
			}else if(checkedPFC.length>0){
				checked=checkedPFC;
				tipohem='PFC';
				var cantselmanual= document.getElementById('cantselmanualPFC').value;
				
			}else if(checkedPQ.length>0){
				checked=checkedPQ;
				tipohem='PQ';
				var cantselmanual= document.getElementById('cantselmanualPQ').value;
			}
						
			var area_interes = "";
			for (var i = 0, length = checked.length; i < length; i++) {
				area_interes += checked[i].value + ",";   
				document.getElementById('datosseleccionado').value=area_interes;          
			}
			datosseleccionado=document.form1.datosseleccionado.value;
			//PG-0314585|A+,PG-0314586|O+, 
			//PFC-0314586|O+,PFC-0314587|O+,
			//alert(datosseleccionado);
			//return 0;
			
			var j1=''; 
			var he=''; var gs=''; 		
			var NgsOPos=0; var NgsAPos=0; var NgsBPos=0; var NgsABPos=0; 
			var NgsONeg=0; var NgsANeg=0; var NgsBNeg=0; var NgsABNeg=0;
			
			var TotgsOPos=''; var TotgsAPos=''; var TotgsBPos=''; var TotgsABPos=''; 
			var TotgsONeg=''; var TotgsANeg=''; var TotgsBNeg=''; var TotgsABNeg='';
			
			var gsOPos=''; var gsAPos=''; var gsBPos=''; var gsABPos='';
			var gsONeg=''; var gsANeg=''; var gsBNeg=''; var gsABNeg='';
			
			var heOPosT='';  
			var heAPosT='';  
			var heBPosT=''; 
			var heABPosT=''; 
			
			var heONegT='';  
			var heANegT='';  
			var heBNegT=''; 
			var heABNegT=''; 
			
			var NgsTodos=''; 
			
			var partesj = datosseleccionado.split(",");	
			for(var j=0; j<cantselmanual; j++){
				j1 = partesj[j];
				//alert(j1);
				var partesk= j1.split("|");					
				he = partesk[0];
				gs = partesk[1];
				
				//Separar por tipo de Sangre
				//POSITIVO
				if(gs=='O+'){
					NgsOPos=NgsOPos+1;
					heOPosT = heOPosT+he+'<br>';					
					TotgsOPos=NgsOPos+'O+ ';
					gsOPos=gsOPos+'O+<br>';
					//document.form1.datosOPos.value = NgsOPos+'O+,'+heOPosT; 							
				}
				if(gs=='A+'){
					NgsAPos=NgsAPos+1;
					heAPosT = heAPosT+he+'<br>';
					TotgsAPos=NgsAPos+'A+ ';
					gsAPos=gsAPos+'A+<br>';
					//document.form1.datosAPos.value = NgsAPos+'A+,'+heAPosT; 
				}
				if(gs=='B+'){
					NgsBPos=NgsBPos+1;
					heBPosT = heBPosT+he+'<br>';
					TotgsBPos=NgsBPos+'B+ ';
					gsBPos=gsBPos+'B+<br>';
					//document.form1.datosBPos.value = NgsBPos+'B+,'+heBPosT; 
				}
				if(gs=='AB+'){
					NgsABPos=NgsABPos+1;
					heABPosT = heABPosT+he+'<br>';
					TotgsABPos=NgsABPos+'AB+ ';
					gsABPos=gsABPos+'AB+<br>';					
				}
				//NEGATIVO
				if(gs=='O-'){
					NgsONeg=NgsONeg+1;
					heONegT = heONegT+he+'<br>';
					TotgsONeg=NgsONeg+'O- ';
					gsONeg=gsONeg+'O-<br>';
					//document.form1.datosONeg.value = NgsONeg+'O-,'+heONegT; 							
				}
				if(gs=='A-'){
					NgsANeg=NgsANeg+1;
					heANegT = heANegT+he+'<br>';
					TotgsANeg=NgsANeg+'A- ';
					gsANeg=gsANeg+'A-<br>';
					//document.form1.datosANeg.value = NgsANeg+ 'A-,'+heANegT; 
				}
				if(gs=='B-'){
					NgsBNeg=NgsBNeg+1;
					heBNegT = heBNegT+he+'<br>';
					TotgsBNeg=NgsBNeg+'B- ';
					gsBNeg=gsBNeg+'B-<br>';
					//document.form1.datosBNeg.value = NgsBNeg+'B-,'+heBNegT;
				}
				if(gs=='AB-'){
					NgsABNeg=NgsABNeg+1;
					heABNegT = heABNegT+he+'<br>';
					TotgsABNeg=NgsABNeg+'AB- ';
					gsABNeg=gsABNeg+'AB-<br>';					
				}							
			
			}//end for	
			
			 var hemenvio=cantselmanual+' '+tipohem;
			 var TotgsTodos=TotgsOPos+TotgsAPos+TotgsBPos+TotgsABPos+ TotgsONeg+TotgsANeg+TotgsBNeg+TotgsABNeg;
			 var heTodos=heOPosT+heAPosT+heBPosT+heABPosT+ heONegT+heANegT+heBNegT+heABNegT;
			 //alert(cantselmanual+' '+tipohem);
			 var gsTodos=gsOPos+gsAPos+gsBPos+gsABPos+ gsONeg+gsANeg+gsBNeg+gsABNeg;
			 var mensaje = "Está enviando "+hemenvio+" ("+TotgsTodos+") como se detalla: ";
             $('#my_modalReg').modal('show');
             $('#mensaje').val(mensaje);
			 
			 $('#gs').html(gsTodos);
			 $('#SNCS').html(heTodos);
			 document.getElementById('ComponentesEnvio').value=heTodos;			 		
            
        }//fin function				
		
		
		 $(function () {
  		
			// Si se hace click sobre el input de tipo checkbox con id checkTodoPG
			$('#checkTodoPG').click(function() {
				// Si esta seleccionado (si la propiedad checked es igual a true)
				if ($(this).prop('checked')) {										
					
					var checkedPFC = document.form1.querySelectorAll('input[class=PFC]:checked');
					var checkedPQ = document.form1.querySelectorAll('input[class=PQ]:checked');					
					
					if (checkedPFC.length > 0) {
						alert("Ha seleccionado Liberar PFC");						
						return false;	
						
					}else if (checkedPQ.length > 0) {
						alert("Ha seleccionado Liberar PQ");						
						return false;
											
					}else{
						// Selecciona cada input que tenga la clase .checar	
						$('.PG').prop('checked', true);
						obtenerselmanualPG();				
					}					
					
				} else {
					// Deselecciona cada input que tenga la clase .checar
					$('.PG').prop('checked', false);
				}
			});
			
			// Si se hace click sobre el input de tipo checkbox con id checkTodoPFC
			$('#checkTodoPFC').click(function() {
				// Si esta seleccionado (si la propiedad checked es igual a true)
				if ($(this).prop('checked')) {
					
					var checkedPG = document.form1.querySelectorAll('input[class=PG]:checked');
					var checkedPQ = document.form1.querySelectorAll('input[class=PQ]:checked');					
					
					if (checkedPG.length > 0) {
						alert("Ha seleccionado Liberar PG");						
						return false;	
						
					}else if (checkedPQ.length > 0) {
						alert("Ha seleccionado Liberar PQ");						
						return false;
											
					}else{
						// Selecciona cada input que tenga la clase .checar	
						$('.PFC').prop('checked', true);
						obtenerselmanualPFC();				
					}
						
				} else {
					// Deselecciona cada input que tenga la clase .checar
					$('.PFC').prop('checked', false);
				}
			});
			
			// Si se hace click sobre el input de tipo checkbox con id checkTodoPFC
			$('#checkTodoPQ').click(function() {
				// Si esta seleccionado (si la propiedad checked es igual a true)
				if ($(this).prop('checked')) {
					
					var checkedPG = document.form1.querySelectorAll('input[class=PG]:checked');
					var checkedPFC = document.form1.querySelectorAll('input[class=PFC]:checked');									
					
					if (checkedPG.length > 0) {
						alert("Ha seleccionado Liberar PG");						
						return false;	
						
					}else if (checkedPFC.length > 0) {
						alert("Ha seleccionado Liberar PFC");						
						return false;
											
					}else{
						// Selecciona cada input que tenga la clase .checar	
						$('.PQ').prop('checked', true);
						obtenerselmanualPQ();				
					}
					
				} else {
					// Deselecciona cada input que tenga la clase .checar
					$('.PQ').prop('checked', false);
				}
			});
		
		 });	
		
		/*function marcarTodoPFC(){
			$('.PFC').prop('checked', true);	
			obtenerselmanualPFC();	
		}*/		
		
		
function GuardarLiberarCuarentena(){
	  
	   var UsuarioRecibe=document.getElementById('UsuarioRecibe').value;
	   if(UsuarioRecibe==''){			
			var mensje = "Falta Seleccionar Usuario Recibe ...!!!";				
			alert(mensje);
			document.getElementById("UsuarioRecibe").focus();
			return 0;
		}
		
		var FechaRecibe=document.getElementById('FechaRecibe').value;
	    if(FechaRecibe==''){			
			var mensje = "Falta Ingresar la Fecha que Recibe...!!!";				
			alert(mensje);
			document.getElementById("FechaRecibe").focus();
			return 0;
		} 
		
		if(confirm("¿Seguro de quitar de cuarentena y enviar al Stock de Sangre APTAS?")){		  	
		   document.getElementById("FrmLiberar").submit();		
	    }	
 }
 
 
function buscarInicio(){   
	$('#form1').submit();	
  
}
	
</script>

</head>

<body>

<!--reg verificacion -->
<div class="modal fade" id="my_modalReg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg">       

    <!-- Modal content-->
    <div class="modal-content">
	
	 <form action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=LiberarCuarentena&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>" name="FrmLiberar" id="FrmLiberar" method="post">                     
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h5 class="modal-title">Liberar Hemocomponentes de Cuarentena</h5>
		</div>
		<div class="alert alert-warning">
			<input name="mensaje" id="mensaje" type="text" style="background-color: #fcf8e3;
			border: 0px solid #fcf8e3;width:100%;" readonly />       
		</div>
		<div class="modal-body"> 		 
			<table class="table">
				<tr align="center">          	
					<td><strong>Grupo Sanguineo</strong></td>
					<td><strong>SNCS</strong></td>
				</tr>
				<tr align="center">          	
					<td><div id="gs"></div></td>
					<td><div id="SNCS"></div></td>
				</tr>          
			</table> 			   
														 
			<!--fila 1-->
			<div class="form-group row">
				<label class="control-label col-xs-1"></label>
                <input type="hidden" id="ComponentesEnvio" name="ComponentesEnvio" />    
			</div> 
			<!--fila 2-->                           
			<div class="form-group row">                                               
				<label class="control-label col-xs-2">Usuario Recibe: </label>
				<div class="col-xs-3">					
					<Select class="form-control input-sm" id="UsuarioRecibe" name="UsuarioRecibe">
					  <option value=""></option>
					  <?php
										  $ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
										  $DNIEmpleado=$ListarUsuarioxIdempleado[0]["DNI"];
										  $listar=SIGESA_ListarEmpleadosLugarDeTrabajoBDS_M(); 
										   if($listar != NULL) { 
											 foreach($listar as $item){?>
					  <option value="<?php echo $item["DNI"]?>" <?php if(trim($item["DNI"])==trim($DNIEmpleado)){?> selected <?php } ?> ><?php echo mb_strtoupper($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"])?></option>
					  <?php } } ?>
					</select>
			
				</div>
				<label class="control-label col-xs-2">Fecha Recibe</label>
				<div class="col-xs-2">
					<input type="text" id="FechaRecibe" name="FechaRecibe" class="form-control datepicker input-sm" placeholder="Fecha Recibe" value="<?php echo  date('d/m/Y') ?>" />    
				</div>								
			</div><!--fin fila 2-->
            								
		</div>     
        <!--<div class="alert alert-warning">
			<b>¿Seguro de quitar de cuarentena y enviar al Stock de Sangre APTAS?</b>   
		</div>-->                
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="button" class="btn btn-primary" onClick="GuardarLiberarCuarentena();">Aceptar</button> 
		</div>
	  
	 </form>  
    </div>

  </div>
   
</div>
<!--fin modal my_modalReg -->

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<!--<div class="row">				
				<?php  //include('../../MVC_Vista/ListaVerificacion/CabeceraLV.php'); ?>
			</div> -->           
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary"> <!--panel-default-->
                        <div class="panel-heading"> Verificación de Grupo Sanguineo / Detección Anticuerpos Irregulares /Fenotipo </div>
                        <div class="panel-body">
                        <form action="../../MVC_Controlador/BancoSangre/BancoSangreC.php?acc=VerificacionAptos&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>" name="form1" id="form1" method="post">                        
                        	 <div class="" align="right">
                                <!--<a class="btn btn-success" onClick="validarguardar();" href="#">Registrar</a>-->
                                <input class="btn btn-success" type="button" onclick="buscar()" value="Buscar"/>
                                &nbsp;<a class="btn btn-danger" onClick="cancelar();">Cancelar</a>
                                &nbsp;<input class="btn btn-warning" type="button" onclick="LiberarCuarentena()" value="Liberar de cuarentena"/>&nbsp;                               
                                <!--&nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                              </div>
                                                 
                            <!--fila 1-->
                            <div class="form-group row">
                                <label class="control-label col-xs-1"></label>
                            </div> 
                            <!--fila 2-->                           
                            <div class="form-group row">                                               
                                <label class="control-label col-xs-2">Fecha Inicio Ing. Resultados</label>
                                <div class="col-xs-2">
                                    <input type="text" id="FechaReg_Inicio" name="FechaReg_Inicio" class="form-control datepicker input-sm" placeholder="Fecha Registro Inicial" value="<?php echo $FechaReg_Inicio ?>" onChange="buscar()"/>    
                                </div>
                                <label class="control-label col-xs-2">Fecha Fin Ing. Resultados</label>
                                <div class="col-xs-2">
                                    <input type="text" id="FechaReg_Final" name="FechaReg_Final" class="form-control datepicker input-sm" placeholder="Fecha Registro Final" value="<?php echo $FechaReg_Final ?>" onChange="buscar()"/>    
                                </div>
                                <label class="control-label col-xs-1">Realizó NAT</label>
                                <div class="col-xs-2">
                                	<select id="RealizaNAT" name="RealizaNAT" class="form-control input-sm" onChange="buscar()">
                                    <option value="9" <?php if($RealizaNAT=='9'){?> selected <?php } ?> >TODOS</option>
                                    <option value="1" <?php if($RealizaNAT=='1'){?> selected <?php } ?> >SI</option>
                                    <option value="0" <?php if($RealizaNAT=='0'){?> selected <?php } ?> >NO</option>                                    
                                    </select>                                        
                                </div>
                                
                            </div><!--fin fila 2-->

                            <!--fila 3-->                           
                            <div class="form-group row">                    
                            	<!--<label class="control-label col-xs-2">Tiene SNCS</label> 
                                <div class="col-xs-2">
                                	<select id="TieneSNCS" name="TieneSNCS" class="form-control input-sm" onChange="buscar()">                                  
                                    <option value="9" <?php if($TieneSNCS=='9'){?> selected <?php } ?> >TODOS</option>
                                    <option value="1" <?php if($TieneSNCS=='1'){?> selected <?php } ?> >CON SNCS</option>
                                    <option value="0" <?php if($TieneSNCS=='0'){?> selected <?php } ?> >SIN SNCS</option>                                    
                                    </select>                                        
                                </div> -->
                                <input type="hidden" id="TieneSNCS" name="TieneSNCS" value="9" /> <!--TODOS TIENEN SNCS-->
                         		<label class="control-label col-xs-2">Tipo Hemocomponente</label>
                                <div class="col-xs-2">
                                    <select id="TipoHemocomponente" name="TipoHemocomponente" class="form-control input-sm" onChange="buscar()">
                                    <option value="" <?php if($TipoHemocomponente==''){?> selected <?php } ?> >TODOS</option>
                                    <option value="MotivoElimPaqueteGlobu" <?php if($TipoHemocomponente=='MotivoElimPaqueteGlobu'){?> selected <?php } ?> >PG</option>
                                    <option value="MotivoElimPlasma" <?php if($TipoHemocomponente=='MotivoElimPlasma'){?> selected <?php } ?> >PFC</option>
                                    <option value="MotivoElimPlaquetas" <?php if($TipoHemocomponente=='MotivoElimPlaquetas'){?> selected <?php } ?> >PQ</option>                                    
                                    </select>      
                                </div>                                
                                
                            </div><!--fin fila 3-->
                        
                                          
                        
                            <div class="dataTable_wrapper">
                            
                               <?php  							
									$FechaReg_Inicio=$_REQUEST["FechaReg_Inicio"];
									if($FechaReg_Inicio!=NULL){
										$FechaReg_Inicio=gfecha($_REQUEST["FechaReg_Inicio"]);
									}
									$FechaReg_Final=$_REQUEST["FechaReg_Final"];
									if($FechaReg_Final!=NULL){
										$FechaReg_Final=gfecha($_REQUEST["FechaReg_Final"]);
									}	

                                    if($FechaReg_Inicio=='' || $FechaReg_Final==''){
                                        $lcFiltroComun=" and CONVERT(date, t7.FechaTamizaje) is null";
                                       // $lcSql = $lcSql . "  dbo.Pacientes.NroHistoriaClinica= " . $_REQUEST['NroHistoriaClinica'] . " and";
                                       
                                    }else{
                                        $lcFiltroComun=" AND CONVERT(date, t7.FechaTamizaje) BETWEEN ''".$FechaReg_Inicio."'' AND ''".$FechaReg_Final."''";
                                    }

                                    //lcFiltro
                                    if($TipoHemocomponente==''){
                                        $lcFiltro='';    
                                       
                                    }else{
                                        $lcFiltro=' AND '.$TipoHemocomponente.' =0 ';    //AND MotivoElimPlasma=0
                                    }   
                                    
   									
									 
								?>
                            
                                <table width="100%" class="table table-bordered table-hover dataTable no-footer" id="dataTables-example"> <!--table-hover--> <!--class="table table-bordered"--> 
                                	<thead>
                                        <tr class="text-center">
                                            <th rowspan="2">&nbsp;</th>
                                            <th rowspan="2">#Donación</th>
                                            <th rowspan="2">GS</th>
                                            <th rowspan="2">Fecha Extraccion</th>
                                            <th colspan="2">OBLIGATORIO</th>
                                            <th rowspan="2">Coombs Directo</th>
                                            <th rowspan="2">DVI</th>
                                            <th rowspan="2">FENOTIPO</th>
                                            <th colspan="3">SNCS</th>
                                            <th rowspan="2" align="center">VALIDAR</th>                                          
                                         </tr>                                    
                                        <tr class="text-center">
                                          <th>Verifica GS</th>
                                          <th>Detección Ac.Irreg.(CI)</th>
                                          <th>LIBERAR PG <input id="checkTodoPG" type="checkbox" /></th>
                                          <th>LIBERAR PFC <input id="checkTodoPFC" type="checkbox" /></th>
                                          <th>LIBERAR PQ <input id="checkTodoPQ" type="checkbox" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
									  		$j=0;								
											$numactivos=0; $numentrada=0;$numpausa=0;$numsalida=0; $todosId="";
											$NoModificaDeteccionCI=0;$NoModificaCoombsDirecto=0;
											
											//$ReporteSNCS=ReporteSNCS_M($FechaReg_Inicio,$FechaReg_Final,$RealizaNAT);												
                                            $ReporteSNCS=ReporteSNCSDinamico_M($RealizaNAT,$TieneSNCS,$lcFiltroComun,$lcFiltro);											
											
											if($ReporteSNCS!=NULL){
									  		foreach($ReporteSNCS as $itemSNCS){
												$j++; 
									  			$EstadoAptoTamizaje=$itemSNCS["EstadoAptoTamizaje"];
												if($EstadoAptoTamizaje=='1'){
													$DesEstadoAptoTamizaje='<font color="#00FF00">NO REACTIVO</font>';
													$colorestado="#FFF";
													$Observacion='';
												}else if($EstadoAptoTamizaje=='0'){
													$EstadoAptoTamizaje='<font color="#FF0000">REACTIVO</font>';
													$colorestado="#FFAAAA";
													
													$PRquimioVIH=($itemSNCS["quimioVIH"]!='0') ? 'VIH '.round($itemSNCS["PRquimioVIH"],2) : '';					
													$PRquimioSifilis=($itemSNCS["quimioSifilis"]!='0') ? 'Sifilis '.round($itemSNCS["PRquimioSifilis"],2) : '';													
													$PRquimioHTLV=($itemSNCS["quimioHTLV"]!='0') ? 'HTLV '.round($itemSNCS["PRquimioHTLV"],2) : '';														
													$PRquimioANTICHAGAS=($itemSNCS["quimioANTICHAGAS"]!='0') ? 'Antichagas '.round($itemSNCS["PRquimioANTICHAGAS"],2) : '';														
													$PRquimioHBS=($itemSNCS["quimioHBS"]!='0') ? 'HBS '.round($itemSNCS["PRquimioHBS"],2) : '';														
													$PRquimioVHB=($itemSNCS["quimioVHB"]!='0') ? 'VHB '.round($itemSNCS["PRquimioVHB"],2) : '';														
													$PRquimioVHC=($itemSNCS["quimioVHC"]!='0') ? 'VHC '.round($itemSNCS["PRquimioVHC"],2) : '';														
													$Observacion=$PRquimioVIH.' '.$PRquimioSifilis.' '.$PRquimioHTLV.' '.$PRquimioANTICHAGAS.' '.$PRquimioHBS.' '.$PRquimioVHB.' '.$PRquimioVHC;
													
												}else if($EstadoAptoTamizaje==''){
													$EstadoAptoTamizaje='<font color="#0000FF">FALTA</font>';
													$colorestado="#d3f1f8";
													$Observacion='F - '.mb_strtolower($itemSNCS["DesMotivoElimMuestra"]);
												}
												
												//SNCS	
												$IdTipoDonacion=$itemSNCS["IdTipoDonacion"];								
												$SNCS=$itemSNCS["SNCS"];
												
												$PGvalido=''; $PFCvalido=''; $PQvalido='';
												
												if(trim($IdTipoDonacion)=='1'){ //SÓLO SANGRE TOTAL
													if(trim($itemSNCS["DesMotivoElimPaqueteGlobu"])!=''){ //PG
														$SNCSPG='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPaqueteGlobu"]).'</font>';
													}else if($EstadoAptoTamizaje=='1'){														
														if($SNCS!=NULL){
															$SNCSPG='PG-'.$SNCS;
															$PGvalido='PG-'.$SNCS;
														}else{
															$SNCSPG='PG-#SNCS';
														}
													}else{
														$SNCSPG='';
													}
													
													if(trim($itemSNCS["DesMotivoElimPlasma"])!=''){ //PFC													
														$SNCSPFC='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPlasma"]).'</font>';														
													}else if($EstadoAptoTamizaje=='1'){														
														if($SNCS!=NULL){
															$SNCSPFC='PFC-'.$SNCS;
															$PFCvalido='PFC-'.$SNCS;
														}else{
															$SNCSPFC='PFC-#SNCS';
														}
													}else{
														$SNCSPFC='';
													}
																									
												}else{ //SÓLO AFERESIS
													$SNCSPG='';
													$SNCSPFC='';
												}											
												
												//TODO (SANGRE TOTAL Y AFERESIS) 
												if(trim($itemSNCS["DesMotivoElimPlaquetas"])!=''){ //PQ
													$SNCSPQ='<font color="#FF0000">'.mb_strtolower($itemSNCS["DesMotivoElimPlaquetas"]).'</font>';
												}else if($EstadoAptoTamizaje=='1'){													
													if($SNCS!=NULL){
														$SNCSPQ='PQ-'.$SNCS;
														$PQvalido='PQ-'.$SNCS;
													}else{
														$SNCSPQ='PQ-#SNCS';
													}
												}else{
													$SNCSPQ='';
												}
												
												//
												$ObtenerDVAptos=ObtenerDatosVerificacionAptos_M($itemSNCS["IdFraccionamiento"]);																							
												if($ObtenerDVAptos!=NULL){
													if($ObtenerDVAptos[0]["IdGrupoSanguineo"]==$ObtenerDVAptos[0]["IdGrupoSanguineoAnterior"]){
														$VerificaGS='VALIDADO';
													}else{
														$VerificaGS='CORREGIDO & VALIDADO';
													}
													
													//NoModificaDeteccionCI
													if($ObtenerDVAptos[0]["DeteccionCI"]=='+'){
														$DeteccionCI='<font color="#FF0000">POSITIVO</font>';	
														$NoModificaDeteccionCI=1;													
													}else if($ObtenerDVAptos[0]["DeteccionCI"]=='-'){
														$DeteccionCI='NEGATIVO';
														$NoModificaDeteccionCI=0;
													}else{														
														$NoModificaDeteccionCI=0;
													}
													
													//NoModificaCoombsDirecto
													if($ObtenerDVAptos[0]["CoombsDirecto"]=='+'){
														$CoombsDirecto='<font color="#FF0000">POSITIVO</font>';
														$NoModificaCoombsDirecto=1;
													}else if($ObtenerDVAptos[0]["CoombsDirecto"]=='-'){
														$CoombsDirecto='NEGATIVO';
														$NoModificaCoombsDirecto=0;
													}else{														
														$NoModificaCoombsDirecto=0;
													}
													
													if($ObtenerDVAptos[0]["VDI"]=='+'){
														$VDI='POSITIVO';
													}else if($ObtenerDVAptos[0]["VDI"]=='-'){
														$VDI='NEGATIVO';
													}
													$Fenotipo=$ObtenerDVAptos[0]["Fenotipo"];
													
													$Envio_PG=$ObtenerDVAptos[0]["Envio_PG"];
													$Envio_PFC=$ObtenerDVAptos[0]["Envio_PFC"];
													$Envio_PQ=$ObtenerDVAptos[0]["Envio_PQ"];													
													$EstadoEnvio=$ObtenerDVAptos[0]["EstadoEnvio"];
													
													
													/*if($ObtenerDVAptos[0]["IdGrupoSanguineo"]==0){ //PRE-REGISTRO
														$VerificaGS='';
														$DeteccionCI='';
														$CoombsDirecto='';
														$VDI='';
														$Fenotipo='';
													
														$PGvalido='';
														$PFCvalido='';
														$PQvalido='';
													}*/
													
												}else{
													$VerificaGS='';
													$DeteccionCI='';
													$CoombsDirecto='';
													$VDI='';
													$Fenotipo='';
													
													$Envio_PG='';
													$Envio_PFC='';
													$Envio_PQ='';
													$EstadoEnvio='';
													
													$NoModificaDeteccionCI=0;
													$NoModificaCoombsDirecto=0;													
												}
									  ?>                                                                     						
 
                                      <tr  bgcolor="<?php echo $colorestado; ?>" class="text-center">
                                            <td align="center"><?php echo $j;?></td>
                                            <td align="center"><?php echo 'DAC'.$itemSNCS["NroDonacion"];?></td>
                                            <td align="center"><?php echo $itemSNCS["GrupoSanguineoPostulante"];?></td>
                                            <td align="center"><?php echo vfecha(substr($itemSNCS["FechaExtraccion"],0,10));?></td>
                                            <td align="center"><?php echo $VerificaGS;?></td>
                                            <td align="center"><?php echo $DeteccionCI;?></td>
                                            <td align="center"><?php echo $CoombsDirecto;?></td>
                                            <td align="center"><?php echo $VDI;?></td>
                                            <td><?php echo $Fenotipo;?></td>
                                            <td align="center"><label for="<?php echo 'chkPG' . $j ?>"><?php echo $SNCSPG;?></label>
                                              <?php if($PGvalido!="" && $Envio_PG=="0"){ ?>
                                              <?php /*?><input type="checkbox" name="<?php echo 'chkreg' . $j ?>" id="<?php echo 'chkreg' . $j ?>" value='<?php echo $PGvalido . '|' . $PFCvalido . '|' . $PQvalido . '|' . $itemSNCS["GrupoSanguineoPostulante"]; ?>' onClick="obtenerselmanual()" /><?php */?>
                                              <input type="checkbox" name="<?php echo 'chkPG' . $j ?>" id="<?php echo 'chkPG' . $j ?>" value='<?php echo $PGvalido. '|' . $itemSNCS["GrupoSanguineoPostulante"]; ?>' onClick="obtenerselmanualPG()" class="PG"  />                                              
                                            <?php }else if($Envio_PG=="1"){ echo "Enviado";} ?>
                                            </td>
                                            
                                            <td align="center"><label for="<?php echo 'chkPFC' . $j ?>"><?php echo $SNCSPFC;?></label>
                                              <?php if($PFCvalido!="" && $Envio_PFC=="0"){ ?>
                                              <input type="checkbox" name="<?php echo 'chkPFC' . $j ?>" id="<?php echo 'chkPFC' . $j ?>" value='<?php echo $PFCvalido. '|' . $itemSNCS["GrupoSanguineoPostulante"]; ?>' onClick="obtenerselmanualPFC()" class="PFC" />
                                           	  <?php }else if($Envio_PFC=="1"){ echo "Enviado";} ?>
                                            </td>
                                            
                                            <td align="center"><label for="<?php echo 'chkPQ' . $j ?>"><?php echo $SNCSPQ;?></label>
                                              <?php if($PQvalido!="" && $Envio_PQ=="0"){ ?>
                                              <input type="checkbox" name="<?php echo 'chkPQ' . $j ?>" id="<?php echo 'chkPQ' . $j ?>" value='<?php echo $PQvalido. '|' . $itemSNCS["GrupoSanguineoPostulante"]; ?>' onClick="obtenerselmanualPQ()" class="PQ" />
                                              <?php }else if($Envio_PQ=="1"){ echo "Enviado";} ?>
                                            </td>
                                            <td align="center">
                                            
                                            	<?php if($EstadoEnvio=="1"){ echo "Liberado";  ?> 
                                                
                                           		<?php }else if($Envio_PG=="1" || $Envio_PFC=="1" || $Envio_PQ=="1"){echo "Con Hemocomp.liberados"; ?>       
                                                                                         
												<?php }else{ //if($EstadoEnvio=="0")?>
                                                <a class="btn btn-default" data-toggle="confirmation" onClick="RegVerificacion('<?php echo $itemSNCS["NroDonacion"] ?>','<?php echo $itemSNCS["IdGrupoSanguineo"] ?>','<?php echo $itemSNCS["IdPostulante"] ?>','<?php echo $PGvalido ?>','<?php echo $PFCvalido ?>','<?php echo $PQvalido ?>','<?php echo $itemSNCS["IdFraccionamiento"] ?>','<?php echo $NoModificaDeteccionCI ?>','<?php echo $NoModificaCoombsDirecto ?>')"><img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font></a>  
                                                <?php } ?>                               
                                                
                                            </td>                                       
                                                                                    
                                        </tr> 
                                        <?php 
											}
										 } 
										?>                                                                                                                      
                                                                           
                                    </tbody>
                                    
                                </table>
                                <?php /*?><input type="text" name="maxitem" id="maxitem" value="<?php echo $j ?>"   /><?php */?>
                                <input type="hidden" name="cantselmanualPG" id="cantselmanualPG" value="0"   /> 
                                <input type="hidden" name="cantselmanualPFC" id="cantselmanualPFC" value="0"   /> 
                                <input type="hidden" name="cantselmanualPQ" id="cantselmanualPQ" value="0"   /> 
                                <input type="hidden" name="datosseleccionado" id="datosseleccionado" value=""   /> 
                            </div>
                            <!-- /.table-responsive -->
                           
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - alertify - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
    
    <!-- Fecha -->     
    <script src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>       
	

</body>

</html>
