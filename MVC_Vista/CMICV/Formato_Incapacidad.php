<?php
require('../../MVC_Complemento/fpdf/fpdf.php');
//--include('../../MVC_Complemento/fpdf/php-barcode.php');

class PDF extends FPDF{
	function Header(){	
		$this->SetY(10);
		$this->SetFont('Arial','',7);
		$this->Cell(24,6,' ',0,0,'C');
		//$this->Image('img/region_callao.jpg',40,8,18,20);
		$this->SetFont('Arial','B',12);
		$this->Cell(142,10,'GOBIERNO REGIONAL DEL CALLAO',0,0,'C');
		//$this->Image('img/hndac.jpg',151,8,18,20);	
		$this->SetFont('Arial','',5);
		$this->SetX(170.5);
		$this->Ln(7.5);

		$this->SetFont('Arial','',10);
		$this->Ln(1);
		$this->SetX(67);
		$this->Cell(70,10,utf8_decode('HOSPITAL NACIONAL "DANIEL A. CARRIÓN"'),0,0);
		$this->Ln(5);
		$this->SetX(173.5);
		$this->Ln(1);
		$this->SetFont('Arial','',9);
		$this->Ln(7);
		$this->SetFont('Arial','b',12);
		$this->Cell(0,6,utf8_decode('CERTIFICADO MÉDICO - DS - N° 166 - 2005 - EF'),0,0,'C');
		$this->SetFont('Arial','b',13);
		$this->Ln(1.5);
	}
	
function TextWithRotation($x, $y, $txt, $txt_angle, $font_angle=0)
    {
        $font_angle+=90+$txt_angle;
        $txt_angle*=M_PI/180;
        $font_angle*=M_PI/180;
    
        $txt_dx=cos($txt_angle);
        $txt_dy=sin($txt_angle);
        $font_dx=cos($font_angle);
        $font_dy=sin($font_angle);
    
        $s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',$txt_dx,$txt_dy,$font_dx,$font_dy,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
        if ($this->ColorFlag)
            $s='q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }
	
	function Footer(){	
		$this->Ln(2);	
		$this->SetLineWidth(0.1);
		 $this->Cell(17,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		 $this->Cell(15,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		 $this->Cell(15,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		
		$this->Ln(13);
		$this->Cell(17,5,'',0,0,'C');
		$this->Cell(44,5,'PRESIDENTE DEL CMCI',1,0,'C');
		 $this->Cell(15,5,'',0,0,'C');
		$this->Cell(44,5,'MIEMBRO DEL CMCI',1,0,'C');
		 $this->Cell(15,5,'',0,0,'C');
		$this->Cell(44,5,'MIEMBRO DEL CMCI',1,0,'C');	
		
		
			}
	
}
	
    
// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
 /*
	$pdf->Image('img/hndac.jpg',40,8,18,20);
	$pdf->SetFont('Arial','B',10);
	$pdf->Ln(15);
	$pdf->Cell(190,10,utf8_decode('CERTIFICADO MÉDICO - DS - N° 166 - 2005 - EF'),0,0,'C');
	$pdf->Ln(7);
	$pdf->Cell(153,7,' ',0,0,'c');
	$pdf->SetFont('Arial','',9);
	$pdf->Cell(9,7,utf8_decode('Dia'),0,0,'C');
	$pdf->Cell(9,7,utf8_decode('Mes'),0,0,'C');
	$pdf->Cell(9,7,utf8_decode('Año'),0,0,'C');
	*/
	$pdf->SetFont('Arial','',9);
	$pdf->Ln(5);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(140,8,utf8_decode('    N° de Certficado Médico 0305-2015'),'B',0,'L');
	$pdf->Cell(13,8,'Fecha','B',0,'L');
	$pdf->Cell(9,8,'10','B',0,'C');
	$pdf->Cell(9,8,'6','B',0,'C');
	$pdf->Cell(19,8,'2015','B',0,'L');
	$pdf->Ln(6.8);
	$pdf->Cell(10,8,'I.- CENTRO ASISTENCIAL (Hospital/Institutos)',0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(190,5.5,utf8_decode('HOSPITAL NACIONAL "DANIEL ALCIDES CARRIÓN"'),'B',0,'C');
	$pdf->Ln(4.3);
	$pdf->Cell(10,8,'II.- DATOS PERSONALES DEL EVALUADO',0,0,'L');
	$pdf->Ln(7);
	$pdf->SetLineWidth(0.1);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(78,4,'Apellido Paterno','B',0,'L');
	 $pdf->Cell(3,4,'',0,0,'L');
	$pdf->Cell(45,4,'Apellido Materno','B',0,'L');
	 $pdf->Cell(3,4,'',0,0,'L');
	$pdf->Cell(49,4,'Nombres','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(81,4,'ESCALANTE',0,0,'L'); //CAMPO APELLIDO PATERNO
	$pdf->Cell(48,4,'HIDALGO',0,0,'L'); //CAMPO APELLIDO MATERNO
	$pdf->Cell(47,4,'ADEMIR',0,0,'L'); // CAMPO NOMBRES
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,utf8_decode('N° de DNI'),'B',0,'L');
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(38,4,'Sexo','B',0,'L');
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(15,4,'Edad','B',0,'L');
	 $pdf->Cell(24,4,'',0,0,'L');
	$pdf->Cell(35,4,'Fecha de Nacimiento','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(54,4,'05242040',0,0,'L'); //CAMPO DNI
	$pdf->Cell(53,4,'Masculino',0,0,'L'); //CAMPO SEXO
	$pdf->Cell(39,4,'63',0,0,'L'); // CAMPO EDAD
	$pdf->Cell(47,4,'6 12 1951',0,0,'L'); // CAMPO FECHA NACIMIENTO
	$pdf->Ln(6);
	$pdf->SetLineWidth(0.1);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(78,4,'Direccion Actual',0,0,'L');
	$pdf->Ln(4);
	 $pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(90,4,utf8_decode('Calle/ Jirón/ Avenida'),'B',0,'L');
	 $pdf->Cell(7,4,'',0,0,'L');
	$pdf->Cell(81,4,utf8_decode('Block/ Manzana/ Urbanización'),'B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(97,4,'TUMBES 285 URB ALTA MAR',0,0,'L'); //CAMPO DIRECCION
	$pdf->Cell(81,4,' ',0,0,'L'); //CAMPO BLOQUE MANZANA URBANIZACION
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(58,4,'Distrito','B',0,'L');
	 $pdf->Cell(2,4,'',0,0,'L');
	$pdf->Cell(50,4,'Provincia','B',0,'L');
	 $pdf->Cell(2,4,'',0,0,'L');
	$pdf->Cell(66,4,'Departamento','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(60,4,'LA PERLA',0,0,'L'); //CAMPO DISTRITO
	$pdf->Cell(52,4,'CALLAO',0,0,'L'); //CAMPO PROVINCIA
	$pdf->Cell(61,4,'CALLAO(PROVICINCIA CONSTITUCIONAL)',0,0,'L'); // CAMPO DEPARTAMENTO
	$pdf->Ln(6);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(190,5,utf8_decode('III.- La Comisión Médica Calificadora de la Incapacidad - CMCI, de acuerdo a sus facultades certifica lo siguiente'),'T',0,'L');
	$pdf->SetLineWidth(0.1);
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,4,'a.- Diagnostico','B',0,'L');
	$pdf->Cell(12,4,'CIE - 10','B',0,'R');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,20,'DEMENCIA EN LA ENFERMEDAD DE ALZHEIMER (G30.-+)',0,0,'L'); //CAMPO DIAGNOSTICO
	$pdf->Cell(12,20,'F00*',0,0,'L'); //CAMPO CIE
	$pdf->Ln(20);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(178,6,'b.- Caracteristica de la incapacidad','T',0,'L');
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,'Naturaleza de la Incapacidad',0,0,'L');
		$pdf->Ln(5);	
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(28,5,'Temporal',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(26,5,'',0,0,'L');
	$pdf->Cell(28,5,'Permanente',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(21,5,'',0,0,'L');
	$pdf->Cell(30,5,'No Incapacidad',1,0,'L');
	$pdf->Cell(12,5,'',1,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,'Grado de la Incapacidad',0,0,'L');
		$pdf->Ln(5);	
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(28,5,'Parcial',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(26,5,'',0,0,'L');
	$pdf->Cell(28,5,'Total',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(21,5,'',0,0,'L');
	$pdf->Cell(30,5,'Gran Incapacidad',1,0,'L');
	$pdf->Cell(12,5,'',1,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,4,'c.- Menoscabo',0,0,'L');
	$pdf->Ln(3.5);
	$pdf->Cell(130,5,'',0,0,'L');
	$pdf->Cell(20,5,'Porcentaje',1,0,'L');
	$pdf->Ln(5);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->Cell(38,5,'','L'.'T',0,'L');
	$pdf->Cell(55,5,'Menoscabo Combinado','T',0,'C');
	$pdf->Cell(20,5.4,'60','B'.'R',0,'C'); // PORCENTAJE MENOSCABO COMBINADO
		$pdf->Ln(5.4);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->MultiCell(35,7.05,'Factores Complementarios',1,'C');
		$pdf->SetXY(82,187);
		$pdf->Cell(62,14.1,'','L'.'T'.'B',0,'L');
	$pdf->SetXY(82,187);
	$pdf->Cell(62,4,'Tipos de Actividad',0,0,'L');
	$pdf->SetXY(149,187);
	$pdf->Cell(62,4,'0',0,0,'L'); // CAMPO PORCENTAJE TIPO ACTIVIDAD
	$pdf->SetXY(82,191);
	$pdf->Cell(62,4,utf8_decode('Posibilidad de reubicaión laboral'),0,0,'L');
	$pdf->SetXY(149,191);
	$pdf->Cell(62,4,'0',0,0,'L'); // CAMPO PORCENTAJE REUBICACION
	$pdf->SetXY(82,195.5);
	$pdf->Cell(62,4,'Edad',0,0,'L');
	$pdf->SetXY(149,195.5);
	$pdf->Cell(62,4,'0',0,0,'L'); // CAMPO PORCENTAJE EDAD	
		$pdf->SetXY(140,187);
		$pdf->Cell(20,14.1,'','R'.'B',0,'L');
	$pdf->Ln(14.1);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->Cell(38,5,'','B'.'L'.'T',0,'L');
	$pdf->Cell(55,5,'MENOSCABO GLOBAL','B'.'T',0,'C'); 
	$pdf->Cell(20,5,'60','B'.'T'.'R',0,'C'); // CAMPO MOENOSCABO GLOBAL PORCENTAJE TOTAL
	

	$pdf->Ln(8);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,4,'d-. Fecha de inicio de Incapacidad',0,0,'L');
	$pdf->Ln(4);
	$pdf->Cell(63,5,'',0,0,'L');
	$pdf->Cell(10,5,'Dia',0,0,'C');
	$pdf->Cell(10,5,'Mes',0,0,'C');
	$pdf->Cell(10,5,utf8_decode('Año'),0,0,'C');
	$pdf->Cell(35,5,'No es precisable',0,0,'C');
	
	$pdf->Ln(5);
	$pdf->Cell(63,5,'',0,0,'C'); 
	$pdf->Cell(10,5,'28',0,0,'C'); // CAMPO DIA INCAPACIDAD
	$pdf->Cell(10,5,'3',0,0,'C'); // CAMPO MES INCAPACIDAD
	$pdf->Cell(10,5,'2015',0,0,'C'); //  CAMPO AÑO INCAPACIDAD
	$pdf->Cell(4,5,'',0,0,'C');
	$pdf->Cell(30,5,'',1,0,'C'); // CAMPO IMPRECISABLE
	
	$pdf->Ln(7);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,9,'IV.- OBSERVACIONES','T',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(7,4,'',0,0,'L');
	$pdf->Cell(166,23,'CAPITULO XI SNC: ASIGNA 60% MGP. EVALUADO POR NEUROLOGIA Y NEUROPSICOLOGIA',0,0,'L');
	$pdf->Ln(23);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,9,'V.- FIRMA Y SELLO','T',0,'L');
	$pdf->Ln(7);

  $fontSize = 10;
  $marge    = 10;   // POSICIONAMIENTO DE BARRA
  $x        = 8;  
  $y        = 21;  
  $height   = 10;   
  $width    = 0.4;    
  $angle    = 90;   
  
  $code     = '123456789012'; // CODIGO DE BARRAS!!
  $type     = 'ean13';
  $black    = '000000'; // COLOR DE BARRA

//$data = Barcode::fpdf($pdf, $black, $x, $y, $angle, $type, array('code'=>$code), $width, $height);
//$len = $pdf->GetStringWidth($data['hri']);$len = $pdf->GetStringWidth($data['hri']);
// Barcode::rotate(-$len / 2, ($data['height'] / 2) + $fontSize + $marge, $angle, $xt, $yt);
$pdf->TextWithRotation($x + 8, $y + 13, $data['hri'], $angle);

$pdf->Output();

?>
