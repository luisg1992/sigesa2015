<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/demo.css">
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.autocomplete.css" />
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/funciones2.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery_enter.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src="../../MVC_Complemento/js/jquery.autocomplete.js"></script> 
<script type="text/javascript" src="../../MVC_Complemento/js/jsalert/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/js/jsalert/jquery.alerts.css"/>
 
<style type="text/css">
<!--
#tblSample td, th { padding: 0.2em; }
.classy0 { background-color: #234567; color: #89abcd; }
.classy1 { background-color: #89abcd; color: #234567; }

#ocultarTexto {
  display: none;
}




 dt,  table, tbody, tfoot, thead, tr, th, td {
	margin:0;
	padding:0;
	border:0;
	font-weight:inherit;
	font-style:inherit;
	 
	font-family:inherit;
	vertical-align:baseline;
}


 
.cuerpo1 {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
color: #000;
 
}

form
{width: auto; background-color: #ffffff; }

textarea.Formulario {
padding: 5px;
border: 1px solid #D4D4D4;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
width:100%;
}

input.Formulario {
border: 1px solid #D4D4D4;
padding: 5px;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
}

.Combos {
     font: small-caption cursive ; 

     background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    width: 150px;
    float:left;
	
text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;

}

 
.texto {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;
  
    /*float:left;*/

}
.textodesable {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#ECECEC;
  
    /*float:left;*/

}


.buttons a, .buttons button{
    
    display:block;
    float:left;
    margin:0 17px 0 0;
    background-color:#f5f5f5;
    border:1px solid #dedede;
    border-top:1px solid #eee;
    border-left:1px solid #eee;

    font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
    font-size: 15px;
    line-height:130%;
    text-decoration:none;
    font-weight:bold;
    color:#565656;
    cursor:pointer;
    padding:5px 10px 6px 7px; /* Links */
}
.buttons button{
    width:auto;
    overflow:visible;
    padding:4px 10px 3px 7px; /* IE6 */
}
.buttons button[type]{
    padding:5px 10px 5px 7px; /* Firefox */
    line-height:17px; /* Safari */
}
*:first-child+html button[type]{
    padding:4px 10px 3px 7px; /* IE7 */
}
.buttons button img, .buttons a img{
    margin:0 3px -3px 0 !important;
    padding:0;
    border:none;
    width:16px;
    height:16px;
}

/* STANDARD */

button:hover, .buttons a:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

/* POSITIVE */

button.positive, .buttons a.positive{
    color:#529214;
}
.buttons a.positive:hover, button.positive:hover{
    background-color:#E6EFC2;
    border:1px solid #C6D880;
    color:#529214;
}
.buttons a.positive:active{
    background-color:#529214;
    border:1px solid #529214;
    color:#fff;
}

/* NEGATIVE */

.buttons a.negative, button.negative{
    color:#d12f19;
}
.buttons a.negative:hover, button.negative:hover{
    background:#fbe3e4;
    border:1px solid #fbc2c4;
    color:#d12f19;
}
.buttons a.negative:active{
    background-color:#d12f19;
    border:1px solid #d12f19;
    color:#fff;
}

/* REGULAR */

button.regular, .buttons a.regular{
    color:#336699;
}
.buttons a.regular:hover, button.regular:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a.regular:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

 /*-----*/
 fieldset { border:1px solid green }

.legend {
  border: 1px solid #BDD7FF;
width: 95%;
background: #F7F7F7;
padding: 3px;

/*  text-align:right;*/
  }
  
  
   
-->
</style> 
<script type="text/javascript">


$().ready(function() {
         /*
		  var IdPuntoCargaSer=document.formElem.IdPuntoCargaSer.value;
		  var idFuenteFinanciamiento=document.formElem.xidFuenteFinanciamiento.value;
		  var GeneraPago=document.formElem.xGeneraPago.value;
		  */
  	  $("#BuscarCIE10").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=1", {
		width: 500, 
		matchContains: true,
			 
		//selectFirst: false
	 
	  }); 
	
	  
		
	$("#BuscarCIE10").result(function(event, data, formatted) {
		$("#BuscarCIE10").val(data[2]);
	 	$("#CodigoCIE2004").val(data[2]);
		$("#descripcion").val(data[3]);
		$("#IdDiagnostico").val(data[1]);
		agregarCausaExterna();
 		//document.getElementById("precio").focus();
		//document.formElem.xvalor.focus();
		
		
 
	});
	
	/****Buscar Por Nombres **/
	 $("#descripcion").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=2", {
		width: 500, 
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		 
		//selectFirst: false
	 
	  }); 
	
	  
		
	$("#descripcion").result(function(event, data, formatted) {
		$("#BuscarCIE10").val(data[2]);
	 	$("#CodigoCIE2004").val(data[2]);
		$("#descripcion").val(data[3]);
		$("#IdDiagnostico").val(data[1]);
		agregarCausaExterna();
		
  		      //  $IdDiagnostico= $item["IdDiagnostico"] ;
			//	$CodigoCIE2004CIE2004= $item["CodigoCIE2004CIE2004"] ;
		//		$Descripcion= $item["Descripcion"] ;
				
	//			echo "$fintrado|$IdDiagnostico|$CodigoCIE2004CIE2004|$Descripcion\n";
//				       0               1          2               3
 
	});
 
});
 
</script>

 <?php 
if($ListaDiagnostico_idatencion != NULL){
$i=0;
foreach($ListaDiagnostico_idatencion as $item4){
	$i++;
  }
  
 $CanFilasCE=$i;
}

?>                

 <script language="javascript" type="text/javascript">  
 
 
 var posicionCampoCE=<?php if($CanFilasCE==0){ echo 1;}else{echo $CanFilasCE+1;};?>;
function agregarCausaExterna(){
	
	var CodigoCIE2004=document.getElementById('CodigoCIE2004').value;
	var descripcion=document.getElementById('descripcion').value;
	var IdDiagnostico=document.getElementById('IdDiagnostico').value;
	if(IdDiagnostico!=""){	 
	///alert('-->'+CodigoCIE2004+'->'+descripcion+'->'+IdDiagnostico);
	nuevaFila = document.getElementById("tablaCausaExterna").insertRow(-1);
	nuevaFila.id=posicionCampoCE;
 /*
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='153'> <input type='text' name='EME_C_EXTERNA"+posicionCampoCE+"' id='EME_C_EXTERNA"+posicionCampoCE+"' value="+CodigoCIE2004+" readonly size='10'></td>";  
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='153'> <input type='text' name='descripcionC_EXTERNA"+posicionCampoCE+"' id='descripcionC_EXTERNA"+posicionCampoCE+"' value="+descripcion+" onChange='agregarCausaExterna()' readonly size='45'></td> ";
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='153'><input type='hiddens' name='IdDiagnostico"+posicionCampoCE+"' id='IdDiagnostico"+posicionCampoCE+"' value='"+IdDiagnostico+"' /><input type='hiddens' name='contadorCE' id='contadorCE' value='"+posicionCampoCE+"' /> <img src='../images/buscar.png'  onClick=BuscarDiagnostico('../MVC_Controlador/emergenciaC.php?acc=diag1&TipoBus=CXEM&contadorCE="+posicionCampoCE+"')> <img src='../images/user_logout.png' value='Eliminar'  onclick='eliminarUsuario(this)'>  </td>";
	*/
	
	 nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input name='BuscarCIE10"+posicionCampoCE+"' type='text' disabled id='BuscarCIE10"+posicionCampoCE+"'  value="+CodigoCIE2004+" size='20'></td>";
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='648' align='left'>"+descripcion+" <input type='hidden' name='IdDiagnostico"+posicionCampoCE+"' id='IdDiagnostico"+posicionCampoCE+"' value="+IdDiagnostico+" ><input type='hidden' name='contadorCE' id='contadorCE' value='"+posicionCampoCE+"'><input type='hidden' name='IdAtencionDiagnostico"+posicionCampoCE+"' id='IdAtencionDiagnostico"+posicionCampoCE+"'></td>";
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='58' align='left'><img src='../../MVC_Complemento/img/user_logout.png' width='16' height='16' onclick='eliminarUsuario(this)'></td>";
	
	
	 posicionCampoCE++;
	 
	document.getElementById('BuscarCIE10').value="";
	document.getElementById('CodigoCIE2004').value="";
	document.getElementById('descripcion').value="";
	document.getElementById('IdDiagnostico').value="";
    document.getElementById('BuscarCIE10').focus();
	}else{
		alert("Ingrese un Diagnostico Valido.");
		document.getElementById('BuscarCIE10').focus();
		
		}
    }	
	
/*FUNONES PARA DIAGNOSTICOS ENFERMERIA */
  

    function eliminarUsuario(obj){
    var oTr = obj;
    while(oTr.nodeName.toLowerCase()!='tr'){
    oTr=oTr.parentNode;
    }
    var root = oTr.parentNode;
    root.removeChild(oTr);
    }

   
   
   
function Guardar(){
	
	  /*
	
     	if (document.getElementById('NumOrdden').value.length==0){
			         alert("Ingrese N° Orden ");
		             document.getElementById('IdOrden').focus();
		             return 0;
				} 
		if (document.getElementById('OrdenaPrueba').value.length==0){
			         alert("Ingrese Nombre del Medico ");
		             document.getElementById('OrdenaPrueba').focus();
		             return 0;
				} 
      
	   if (document.getElementById('idempleadoregistro').value.selectedIndex==0){
			         alert("Ingrese Nombre del Medico ");
		             document.getElementById('idempleadoregistro').focus();
		             return 0;
				} 
		*/
		
		if (document.getElementById('IdTipoGravedad').selectedIndex==1){
		       alert("Seleccionar Prioridad.");
    		   document.getElementById('IdTipoGravedad').focus();
				return 0;
	    }
				if (document.getElementById('IdCausaExternaMorbilidad').selectedIndex==0){
		       alert("Seleccionar Causa Morvilidad.");
    		   document.getElementById('IdCausaExternaMorbilidad').focus();
				return 0;
	    }				
			
			
						
				
	    document.formElem.submit();
		parent.BuscarPacientesEmergencia();
		 parent.googlebox.hide();
 	}	


function Cerrar(){
	parent.BuscarPacientesEmergencia();
	 parent.googlebox.hide();
	}
    </script> 
    <script language="javascript"> 
  function NumCheck(e, field) {
    key = e.keyCode ? e.keyCode : e.which
    if (key == 8) return true
    if (key > 47 && key < 58) {
      if (field.value == "") return true
      regexp = /.[0-9]{5}$/
      return !(regexp.test(field.value))
    }
    if (key == 46) {
      if (field.value == "") return false
      regexp = /^[0-9]+$/
      return regexp.test(field.value)
    }
    return false
  }
</script> 
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <table width="717" border="1" widtd="200">
    <tr>
      <td width="373"><fieldset class="fieldset legend">
        <legend>DATOS PERSONALES DEL EVALUADO</legend>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="112" height="19"><strong>Nro Historia</strong></td>
            <td width="193" ><?php echo $NroHistoriaClinica; ?>
              <input name="NroHistoriaClinica" type="hidden" id="NroHistoriaClinica" value="<?php echo $NroHistoriaClinica; ?>" /></td>
          </tr>
          <tr>
            <td height="19" ><strong>Apellidos </strong></td>
            <td><?php echo $ApellidoPaterno;?> <?php echo $ApellidoMaterno;?></td>
          </tr>
          <tr>
            <td height="19" ><strong>Nombres</strong></td>
            <td><?php echo $PrimerNombre;?></td>
          </tr>
          <tr>
            <td height="19"><strong>Fecha Nacimiento</strong></td>
            <td><?php echo vfechahora(substr($FechaNacimiento,0,10));?>:</td>
          </tr>
          <tr>
            <td height="19"><strong>Edad</strong></td>
            <td><?php
			
			$fecha_nacimiento = vfechahora(substr($FechaNacimiento,0,10));;
             $fecha_control = vfechahora(substr($FechaServidor,0,10));;

   $tiempo = tiempo_transcurrido($fecha_nacimiento, $fecha_control);
   $texto = "$tiempo[0] Años con $tiempo[1] Me. y $tiempo[2] Dí.";
   print "".$texto."";

			
			  ?></td>
          </tr>
          <tr>
            <td height="19"><strong>Sexo</strong></td>
            <td><?php echo strtoupper($Sexo);?></td>
          </tr>
          <tr>
            <td height="15" ><strong>Fte.Financiam/IAFA</strong></td>
            <td><?php echo $FuenteFinanciamiento;?><span style="background:#E0ECF8">
              <input name="idAtencion_Diagnostico" type="hidden" id="idAtencion_Diagnostico" value="<?php echo $IdAtencion;?>" size="15" readonly="readonly" />
              <input name="IdEmpleado" type="hidden" id="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'];?>" size="10" />
              <input name="IdPaciente" type="hidden" id="IdPaciente" value="<?php echo $IdPaciente;?>" />
            </span></td>
          </tr>
          <tr>
            <td height="15" ><strong>Nº Atencion</strong></td>
            <td><?php echo $IdAtencion;?>
              <input name="IdAtencion" type="hidden" id="IdAtencion" value="<?php echo $IdAtencion;?>" /></td>
          </tr>
          <tr>
            <td height="15" ><strong>Nº Cuenta </strong></td>
            <td><?php echo $IdCuentaAtencion; ?>
              <input name="IdCuentaAtencion" type="hidden" id="IdCuentaAtencion" value="<?php echo $IdCuentaAtencion; ?>" /></td>
          </tr>
        </table>
      </fieldset></td>
      <td width="328" ><fieldset class="fieldset legend">
        <legend>Historial de Consultas</legend>
        <table width="305" border="1" widtd="200">
          <tr class="ac_over">
            <td width="50"  >Fecha</td>
            <td width="128">Consultorio</td>
            <td width="106">Estado</td>
          </tr>
         
          <tr>
            <td  >01/01/2015</td>
            <td>Medicina Fisica -reahbilitacion</td>
            <td>Atendido</td>
          </tr>
          
        </table>
      </fieldset></td>
    </tr>
    <tr>
      <td colspan="2"><fieldset class="fieldset legend">
        <legend>DIAGNOSTICO</legend>
        <table width="700" id="tablaCausaExterna">
      <tr>
        <td><strong>Código</strong></td>
        <td colspan="2"><strong>Descripción</strong></td>
      </tr>
      <tr>
        <td width="148"><input name="BuscarCIE10" type="text"  id="BuscarCIE10"   size="20"  onChange="accionagregar()" class="texto"    /></td>
        <td colspan="2"><input name="descripcion" type="text" id="descripcion" size="70" onChange="accionagregar()"  class="texto" />
          <input name="CodigoCIE2004" type="hidden"  id="CodigoCIE2004" size="10" readonly="readonly"  />
          <input type="hidden" name="IdDiagnostico" id="IdDiagnostico">
        <input type="button" name="add" id="add" value="Agregar" onClick="agregarCausaExterna()" class="buttons"/></td>
      </tr>
        <?php 
                    if($ListaDiagnostico_idatencion != NULL)
                    { 
					$i=1;
                    foreach($ListaDiagnostico_idatencion as $item4)            {   
                    ?>
                    
 
      <tr>
        <td><input name="BuscarCIE10<?php echo $i;?>" type="text" disabled id="BuscarCIE10<?php echo $i;?>" value="<?php echo $item4["CodigoCIE2004"];?>" size="20"></td>
        <td width="468" align="left"> 
        <?php echo $item4["Descripcion"];?>
        <input type="hidden" name="IdDiagnostico<?php echo $i;?>" id="IdDiagnostico<?php echo $i;?>" value="<?php echo $item4["IdDiagnostico"];?>">
        <input type="hidden" name="contadorCE" id="contadorCE" value="<?php echo $i;?>">
        <input type="hidden" name="IdAtencionDiagnostico<?php echo $i;?>" id="IdAtencionDiagnostico<?php echo $i;?>" value="<?php echo $item4["idAtencionDiagnostico"]?>"></td>
        <td width="52" align="left"><img src="../../MVC_Complemento/img/user_logout.png" width="16" height="16" onclick='eliminarUsuario(this)'></td>
      </tr>
      
                <?php  $i++;} }?>
    </table>
      </fieldset></td>
    </tr>
    <tr>
      <td colspan="2"><fieldset class="fieldset legend">
        <legend>Caracteristica de la Incapacidad</legend>
        <table width="700" widtd="200">
          <tr>
            <td width="149"  ><strong>Naturaleza de la Incapacidad</strong></td>
            <td width="523"  ><select name="select" id="select">
              <option value="No Incapacidad">No Incapacidad</option>
              <option value="Temporal">Temporal</option>
              <option value="Permanente">Permanente</option>
            </select></td>
          </tr>
          <tr>
            <td  ><strong>Grado de la Incapacidad</strong></td>
            <td  ><label for="select2"></label>
              <select name="select2" id="select2">
                <option value="Gran Incapacidad">Gran Incapacidad</option>
                <option value="Parcial">Parcial</option>
                <option value="Total">Total</option>
              </select></td>
          </tr>
        </table>
      </fieldset></td>
    </tr>
    <tr>
      <td colspan="2"><fieldset class="fieldset legend">
        <legend>MENOSCABO</legend>
        <table width="700" widtd="200">
          <tr>
            <td colspan="2"  ><strong>Menoscabo Combinado</strong></td>
            <td width="319"><label for="textfield5"></label>
              <input name="textfield5" type="text" id="textfield5" size="10" class="texto"   /></td>
          </tr>
          <tr>
            <td width="167" rowspan="3" align="center" valign="middle"    >  
              <strong><BR>
              Factores Complementarios </strong></td>
            <td width="173"  ><strong>Tipos de Actividad</strong></td>
            <td><label for="textfield4"></label>
              <input name="textfield4" type="text" id="textfield4" size="10" class="texto"  /></td>
          </tr>
          <tr>
            <td  ><strong>Posibilidad de reubicaión laboral</strong></td>
            <td><label for="textfield3"></label>
              <input name="textfield3" type="text" id="textfield3" size="10" class="texto"   /></td>
          </tr>
          <tr>
            <td  ><strong>Edad</strong></td>
            <td><label for="textfield2"></label>
              <input name="textfield2" type="text" id="textfield2" size="10" class="texto"  /></td>
          </tr>
          <tr>
            <td colspan="2"  ><strong>MENOSCABO GLOBAL</strong></td>
            <td><input name="textfield6" type="text" id="textfield6" size="10" class="texto"  /></td>
          </tr>
          <tr>
            <td colspan="2"  ><strong>Fecha de Inicio de Incapacidad
              <input type="text" name="textfield" id="textfield" />
            </strong></td>
            <td><strong>
              <label for="checkbox">No es precisable
                <input name="checkbox" type="checkbox" id="checkbox" />
              </label>
            </strong></td>
          </tr>
          <tr>
            <td colspan="2"  >&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </fieldset></td>
    </tr>
    <tr>
      <td colspan="2"><fieldset class="fieldset legend">
        <legend>OBSERVACION</legend>
        <table width="700" widtd="200">
          <tr>
            <td  ><label for="textarea"></label>
              <textarea name="textarea" id="textarea" cols="60" rows="5"></textarea></td>
            </tr>
        </table>
      </fieldset></td>
    </tr>
    <tr>
      <td colspan="2"><span style="text-align: center"><img src="../../MVC_Complemento/img/botoncancelar.jpg" width="85" height="22" onclick="Cerrar();"/><img src="../../MVC_Complemento/img/botonaceptar.jpg" width="85" height="22" onclick="Guardar();" /></span></td>
    </tr>
  </table>
</form>
</body>
</html>
