<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
  <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
  <!-- Esta es para la UI -->
  <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery-ui.css">
  <!-- Esta es la plantilla que edite para tener un estilo propio -->
  <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jtemplate.css">


  <!--Cargar la Libreria JQuery -->
  <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
  <!--    -->
  <!--Cargar la Libreria JQuery -->
  <script type="text/javascript" src="../../MVC_Complemento/js/jquery-ui_jl.js"></script>
  <!--    -->

	  <script>
	  $(function() {
	    $( "#tabs" ).tabs();
	  });
	  </script>
	  <script>
	  $(document).ready(function(){
	    $('#generar_devolucion').click(function()
	    	{

	    		var gridder = $('#refrescar');
	    		var UrlToPass = 
			      {
			      NroDocumento:$('#NroDocumento').val(),
			      NroSerie:$('#NroSerie').val(),
			      acc : 'Mostrar_CajaComprobantesPago'
			      };
			      $.ajax({
			      type: "POST",
			      url : '../../MVC_Controlador/Caja/CajaC.php',
			      data : UrlToPass,
			      success: function(a) 
			      {
			      gridder.html(a);
			      }
			      }); 



	   		});


	    $('#modificar_comprobante_devolucion').click(function()
	    	{     
	
	    		    $( "#dialog-confirm" ).dialog({
				      resizable: false,
				      height:140,
				      modal: true,
				      buttons: {
				        "Aceptar": function()
				        {
						  var UrlToPass = 
					      {
					      IdComprobantePagos:$('#IdComprobantePagos').val(),
					      IdEmpleado:$('#IdEmpleado').val(),
					      acc : 'CajaComprobante_Devolver'
					      };
					      $.ajax({
					      type: "POST",
					      url : '../../MVC_Controlador/Caja/CajaC.php',
					      data : UrlToPass,
					      beforeSend: function() 
					      {
					      	var FechaCobranza=$('#FechaCobranza').val();
					      	var Fecha = FechaCobranza.substring(0, 10);
				   		    var f_hoy=$('#Fecha_hoy').val();
				   		    if (Fecha >= f_hoy) 
						    {
	        					  $( "#dialog" ).dialog();
	        					  return false;
						    }
              			  },
					      success: function(a) 
					      {
					      	var IdComprobantePagos=$('#IdComprobantePagos').val();
							var googlewin=dhtmlwindow.open("googlebox", "iframe", "../../MVC_Controlador/Caja/CajaC.php?acc=Imprimir_Devolucion&IdComprobantePagos="+IdComprobantePagos,"Devolucion de Boleta de Cobranza", "width=790px,height=420px,resize=0,scrolling=1,center=1", "recal")
					      }
					      }); 	
				          $( this ).dialog( "close" );
				        },
				        Cancelar: function() 
				        {
				          $( this ).dialog( "close" );
				        }
				      }
				    });

	    	});	   

		});

	  </script>
</head>
<body>
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Registro de Comprobante</a></li>
    <li><a href="#tabs-2">Gestion de Caja</a></li>
  </ul>
  <div id="tabs-1">
  <p>
     <?php
	   include('../../MVC_Vista/Caja/RegistroComprobante.php');
	  ?>
  </p>
      <?php
	   include('../../MVC_Vista/Caja/dialog_comprobante.php');
	  ?>
 </div>
   <div id="tabs-2">
  	<p>
	   <?php
	   include('../../MVC_Vista/Caja/GestionCaja.php');
	   ?>
    </p>
  </div>
</div>
 
 
</body>
</html>