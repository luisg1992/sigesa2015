<?php   $IdEmpleado=$_REQUEST["IdEmpleado"]; ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/imprimir.css"/>
<title></title>
 
<style type="text/css">
.botonExcel{cursor:pointer;}

@media all {
   div.saltopagina{
      display: none;
   }
}
   
@media print{
   div.saltopagina{
      display:block;
      page-break-before:always;
   }
} 
</style>
<style type="text/css" media="print">
.nover {display:none}
</style>
</head>
<body  onLoad="window.print();">
<ul class="pro15 nover">
<li><a href="#nogo" onClick="window.print();" class="nover"><em class="home nover"></em><b>Imprimir</b></a></li>
 
<li><a href="../../MVC_Controlador/Caja/CajaC.php?acc=Devoluciones&IdEmpleado=<?php echo $IdEmpleado; ?>"   class="nover" ><em class="find nover"></em><b><< Retornar </b></a></li>
 


</ul>
<?php 
 				if($Mostrar_CajaComprobantePagoPorIdComprobante != NULL) { 
                foreach($Mostrar_CajaComprobantePagoPorIdComprobante as $itemdev){
					 
					$idDevolucion=$itemdev["idDevolucion"];	
					$UsuarioDevol=$itemdev["UsuarioDevol"];	
					$fechaDevolucion=$itemdev["fechaDevolucion"];	
					$motivo=$itemdev["motivo"];	
					$IdordenPago=$itemdev["IdordenPago"];	
					$NroHistoriaClinica=$itemdev["NroHistoriaClinica"];	
					$RazonSocial=$itemdev["RazonSocial"];	
					$NroSerie=$itemdev["NroSerie"];	
					$NroDocumento=$itemdev["NroDocumento"];	
					$Totaldev=$itemdev["Total"];	
					$FechaCobranza=$itemdev["FechaCobranza"];	
					$IdCuentaAtencion=$itemdev["IdCuentaAtencion"];	
					$Usuario=$itemdev["Usuario"];	
					$ApellidoPaterno=$itemdev["ApellidoPaterno"];	
					$ApellidoMaterno=$itemdev["ApellidoMaterno"];	
					$Nombres=$itemdev["Nombres"];
					$montoTotal=$itemdev["montoTotal"];
					
 
				}
				}
				?>

<table width="425" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center"><FONT FACE="Calibri" size="+1"><strong>N  Nota Credito 001-000
        <?php  echo $idDevolucion;  ?>
    </strong>    </FONT></td>
  </tr>
  <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
         
  <tr>
    <td colspan="5"><FONT FACE="Calibri" >H.C: <?php echo $NroHistoriaClinica;  ?>&nbsp;&nbsp; <?php echo $RazonSocial; ?></FONT></td>
  </tr>
 
 
  <tr>
    <td colspan="5"><FONT FACE="Calibri" >N° Documento: 
        <strong>
        <?php  echo $NroSerie.' - '.$NroDocumento;  ?>
        </strong>F.Emision:<strong><?php echo vfechahora($FechaCobranza); ?></strong></FONT></td>
  </tr>
  <tr>
    <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="14%"><FONT FACE="Calibri"  >N° Cuen.</FONT></td>
        <td width="20%"><?php  echo ' '.$IdCuentaAtencion;?></td>
        <td width="15%"><FONT FACE="Calibri" > N° OrPg. </FONT></td>
        <td width="16%"><?php  echo ' '.$IdordenPago;?></td>
        <td width="35%"><FONT FACE="Calibri"  >Usuario Emis.</FONT><strong><?php echo  $Usuario; ?></strong></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td  >&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td width="4">&nbsp;</td>
    <td width="57"  ><FONT FACE="Calibri"  ><strong>Codigo</strong></FONT></td>
    <td width="242" align="center"><FONT FACE="Calibri"   ><strong>Descripcion</strong></FONT></td>
    <td width="46" align="center"><FONT FACE="Calibri"   ><strong>Cant</strong>.</FONT></td>
    <td width="81" align="right"><FONT FACE="Calibri"   ><strong>Importe</strong></FONT></td>
  </tr>
  <?php
      if($Lista_OrdenesServicio != NULL) { 
	  $totalPorPagar=0;
          foreach($Lista_OrdenesServicio as $item) 
                              {
                              ?> 
                              
								
  <tr>
    <td>&nbsp;</td>
    <td rowspan="2"   valign="top"><FONT FACE="Calibri" ><?php echo $item["Codigo"]; ?></font></td>
    <td rowspan="2" valign="top"  ><FONT FACE="Calibri"  ><?php echo $item["Nombre"]; ?><br>
    </FONT></td>
    <td rowspan="2" align="center" valign="top"><FONT FACE="Calibri" ><?php echo $item["Cantidad"].$item["Total"]; ?></FONT></td>
    <td rowspan="2" align="right" valign="top"><FONT FACE="Calibri" ><?php echo number_format($item["totalPorPagar"], 2, '.', ' '); ?></FONT></td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
                                $totalPorPagar= $totalPorPagar=$item["totalPorPagar"];}
								
								}
                                ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><font face="Calibri"  ><strong>Motivo Devolución:</strong><br>
    <?php echo $motivo; ?></font></td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font face="Calibri" >Termina:</font></td>
    <td><font face="Calibri" ><?php echo  Terminal(); ?></font></td>
    <td align="right"><font face="Calibri" >Monto</font></td>
    <td align="right"><font face="Calibri" ><strong>S/. <?php echo number_format($totalPorPagar, 2, '.', ' '); ?></strong></font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><font face="Calibri" >Usuario Dev:<strong> <?php echo  $UsuarioDevol;?></strong> F.Dev:
      <strong><?php echo vfechahora($fechaDevolucion); ?></strong></font></td>
    <td align="right"><font face="Calibri" >T.Dev</font></td>
    <td align="right"><font face="Calibri" ><strong>S/.<?php echo number_format($montoTotal, 2, '.', ' '); ?></strong></font></td>
  </tr>
  <tr>
    <td colspan="5" align="center"><FONT FACE="Calibri" size="+1">H.C:<?php echo $NroHistoriaClinica;  ?>  <?php echo $RazonSocial; ?></FONT> </td>
  </tr>
</table>
 