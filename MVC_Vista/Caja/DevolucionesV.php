<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/demo.css">
 
 
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.autocomplete.css" />
    
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/funciones2.js"></script>
 
 
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery_enter.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src="../../MVC_Complemento/js/jquery.autocomplete.js"></script> 


 <script type="text/javascript" src="../../MVC_Complemento/js/jsalert/jquery.alerts.js"></script>
 <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/js/jsalert/jquery.alerts.css"/>
 
<style type="text/css">
<!--
#tblSample td, th { padding: 0.2em; }
.classy0 { background-color: #234567; color: #89abcd; }
.classy1 { background-color: #89abcd; color: #234567; }

#ocultarTexto {
  display: none;
}




 dt,  table, tbody, tfoot, thead, tr, th, td {
	margin:0;
	padding:0;
	border:0;
	font-weight:inherit;
	font-style:inherit;
	 
	font-family:inherit;
	vertical-align:baseline;
}


 
.cuerpo1 {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
color: #000;
 
}

form
{width: auto; background-color: #ffffff; }

textarea.Formulario {
padding: 5px;
border: 1px solid #D4D4D4;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
width:100%;
}

input.Formulario {
border: 1px solid #D4D4D4;
padding: 5px;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
}

.Combos {
     font: small-caption cursive ; 

     background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    width: 150px;
    float:left;
	
text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;

}

 
.texto {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;
  
    /*float:left;*/

}
.textodesable {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#ECECEC;
  
    /*float:left;*/

}


.buttons a, .buttons button{
    
    display:block;
    float:left;
    margin:0 17px 0 0;
    background-color:#f5f5f5;
    border:1px solid #dedede;
    border-top:1px solid #eee;
    border-left:1px solid #eee;

    font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
    font-size: 15px;
    line-height:130%;
    text-decoration:none;
    font-weight:bold;
    color:#565656;
    cursor:pointer;
    padding:5px 10px 6px 7px; /* Links */
}
.buttons button{
    width:auto;
    overflow:visible;
    padding:4px 10px 3px 7px; /* IE6 */
}
.buttons button[type]{
    padding:5px 10px 5px 7px; /* Firefox */
    line-height:17px; /* Safari */
}
*:first-child+html button[type]{
    padding:4px 10px 3px 7px; /* IE7 */
}
.buttons button img, .buttons a img{
    margin:0 3px -3px 0 !important;
    padding:0;
    border:none;
    width:16px;
    height:16px;
}

/* STANDARD */

button:hover, .buttons a:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

/* POSITIVE */

button.positive, .buttons a.positive{
    color:#529214;
}
.buttons a.positive:hover, button.positive:hover{
    background-color:#E6EFC2;
    border:1px solid #C6D880;
    color:#529214;
}
.buttons a.positive:active{
    background-color:#529214;
    border:1px solid #529214;
    color:#fff;
}

/* NEGATIVE */

.buttons a.negative, button.negative{
    color:#d12f19;
}
.buttons a.negative:hover, button.negative:hover{
    background:#fbe3e4;
    border:1px solid #fbc2c4;
    color:#d12f19;
}
.buttons a.negative:active{
    background-color:#d12f19;
    border:1px solid #d12f19;
    color:#fff;
}

/* REGULAR */

button.regular, .buttons a.regular{
    color:#336699;
}
.buttons a.regular:hover, button.regular:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a.regular:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

 /*-----*/
 fieldset { border:1px solid green }

.legend {
  border: 1px solid #BDD7FF;
width: 95%;
background: #F7F7F7;
padding: 3px;

/*  text-align:right;*/
  }
  
  
   
-->
</style>             

 <script language="javascript" type="text/javascript">  
 
  
   
function Guardar(){
	
	  /*
	
     	if (document.getElementById('NumOrdden').value.length==0){
			         alert("Ingrese N° Orden ");
		             document.getElementById('IdOrden').focus();
		             return 0;
				}   */
		if (document.getElementById('IdComprobantePago').value.length==0){
			         alert("Busca Documento para Devolver .. ");
		             document.getElementById('NroSerie').focus();
		             return 0;
				} 
    if (document.getElementById('IdEstadoComprobante').value.length==0){
			         alert("Busca Documento para Devolver .. ");
		             document.getElementById('NroSerie').focus();
		             return 0;
				} 
				 
		 
				 
				var FechaCobranza = Date.parse(document.getElementById("FechaCobranza").value);
                var FechaActual = Date.parse(document.getElementById("FechaActual").value);
				 
				 
				if(parseInt(FechaCobranza) == parseInt(FechaActual)) {
			         alert("SOLO SE PUEDE DEVOLVER BOLETAS DE FECHAS PASADAS, TIENES QUE EXTORNAR LA BOLAETA ..");
		             document.getElementById('NroSerie').focus();
		             return 0;
				}
	    
		var montoDevuelto=document.getElementById('montoDevuelto').value;
		var MontoRecaudado=document.getElementById('MontoRecaudado').value;
		
		 
		if (parseInt(MontoRecaudado)<parseInt(montoDevuelto)){
			         alert("MONTO A DEVOLVER SUPERA AL MONTO RECAUDADO...");
		             document.getElementById('NroSerie').focus();
		             return 0;
				} 
		if (document.getElementById('IdEstadoComprobante').value==6){
			         alert("DOCUMENTO YA DEVUELTO..");
		             document.getElementById('NroSerie').focus();
		             return 0;
				} 
		if (document.getElementById('IdEstadoComprobante').value==9){
			         alert("DOCUMENTO EXTORNANDO NO SE PUEDE DEVUELTO..");
		             document.getElementById('NroSerie').focus();
		             return 0;
				} 	
				
					
		if (document.getElementById('Observaciones').value.length==0){
			         alert("Ingrese Motivo de Devolucion");
		             document.getElementById('Observaciones').focus();
		             return 0;
				} 
				
		 
		/*IdComprobantePago
IdEstadoComprobante
MontoRecaudado
IdEmpleado
*/
/*
		if (document.getElementById('IdTipoGravedad').selectedIndex==1){
		       alert("Seleccionar Prioridad.");
    		   document.getElementById('IdTipoGravedad').focus();
				return 0;
	    }
				if (document.getElementById('IdCausaExternaMorbilidad').selectedIndex==0){
		       alert("Seleccionar Causa Morvilidad.");
    		   document.getElementById('IdCausaExternaMorbilidad').focus();
				return 0;
	    }				
			
			*/
						
				
	    document.formElem.submit();
	 
 	}	

 
 function Buscar(){
	 
			var NroSerie=document.getElementById("NroSerie").value;
			var NroDocumento=document.getElementById("NroDocumento").value;
			var IdEmpleado=document.getElementById("IdEmpleado").value;
		 
	      if(NroSerie!="" &&NroDocumento !="" ){
		 
		  
			 
			  location.href="../../MVC_Controlador/Caja/CajaC.php?acc=Buscar_Devoluciones&NroSerie="+NroSerie+"&IdEmpleado="+IdEmpleado+"&NroDocumento="+NroDocumento;
		  }else{
			  alert("Ingrese Numero de Documento");
			   document.getElementById("NroSerie").focus();
			  }
		}
    </script> 
     <script language="JavaScript">
function TabularEnter(e,t)
{
var k=null;
(e.keyCode) ? k=e.keyCode : k=e.which;
if(k==13) (!t) ? Buscar() : t.focus();
}
function B()
{
document.forms[0].submit();
return true;
}
</script>
  </head>

 
 
 <body   >
 
 
 <form id="formElem" name="formElem"  method="post" action="../../MVC_Controlador/Caja/CajaC.php?acc=Guardar_Devoluciones">
  <label></label>
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>Buscar Documento</strong></legend>
    <table width="800" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td height="16" colspan="4"><strong>  <input type="radio" name="tipo_gestion" class="tipo_gestion" value="radio"  checked="checked"/>
              DEVOLUCION
                  
            </strong></td>
            </tr>
          <tr>
            <td width="97" rowspan="2" >&nbsp;</td>
            <td width="97" height="16" ><strong>N° Serie</strong></td>
            <td width="353"><strong>N° Documento</strong></td>
            <td width="353">&nbsp;</td>
          </tr>
          <tr>
            <td height="16" ><input name="NroSerie" type="text" id="NroSerie" size="7" onKeyDown="TabularEnter(event,this.form.NroDocumento);" maxlength="3"   /></td>
            <td><input type="text" name="NroDocumento" id="NroDocumento" size="15" onKeyDown="TabularEnter(event,null);" /></td>
            <td><span style="text-align: center"><img src="../../MVC_Complemento/img/botonbuscar.jpg" width="69" height="22" onClick="Buscar();" /></span></td>
          </tr>
         
        </table></td>
        <td width="376"  ><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="112" height="19"><strong>Cajero:</strong></td>
            <td width="193" ><?php echo $ApellidoPaterno.' '.$ApellidoMaterno.' '.$Nombres; ?>
               <label for="textfield5"></label>
               <input name="IdEmpleado" type="hidden" id="IdEmpleado" value="<?php echo $IdEmpleado; ?>"></td>
          </tr>
          <tr>
            <td height="19" ><strong>Usuario:</strong></td>
            <td> 
		<?php echo $Usuario  .' <strong> DNI:</strong> '.$DNI; 	?></td>
          </tr>
          <tr>
            <td height="19" ><strong>Fecha:</strong></td>
            <td><strong><?php echo vfecha($FechaActual);?></strong> </td>
          </tr>
          <tr>
            <td height="19" ><strong>Monto Actual:</strong></td>
            <td>S/. <font color="#FF0000" size="+3"><strong><?php echo $ModntoRecaudado;?>
             
              <input name="MontoRecaudado" type="hidden" id="MontoRecaudado" value="<?php echo $ModntoRecaudadoc;?>">
              <input name="IdEstadoComprobante" type="hidden" id="IdEstadoComprobante" value="<?php echo $IdEstadoComprobante; ?>">
              <label for="IdComprobantePago"></label>
              <input name="IdComprobantePago" type="hidden" id="IdComprobantePago" value="<?php echo $IdComprobantePago; ?>">
              <input type="hidden" name="FechaCobranza" id="FechaCobranza" value="<?php echo substr($FechaCobranza,0,10); ?>">
              <input type="hidden" name="FechaActual" id="FechaActual" value="<?php echo substr($FechaActual,0,10); ?>">
            </strong></font></td>
          </tr>
        </table>
          
        </td>
      </tr>
    </table>
  
  </fieldset>
  <BR>
  
        
<fieldset class="fieldset legend">
    
    <legend style="color:#03C"><strong>Detalle Documento </strong></legend> 
<table width="800" height="37" border="0">
          <tr>
            <td colspan="2"><table width="807" border="0">
              <tr>
                <td width="274"><strong style="font-size:11px; color:#333">Razon Social</strong></td>
                <td width="96"><strong style="font-size:11px; color:#333">RUC</strong></td>
                <td width="121" align="center"><strong style="font-size:11px; color:#333">Fecha Emesion</strong></td>
                <td width="82"><strong style="font-size:11px; color:#333">Nª Serie</strong></td>
                <td width="126"><strong style="font-size:11px; color:#333">Nª de Documento</strong></td>
                <td width="82"><strong>Estado</strong></td>
              </tr>
              <tr>
                <td><strong><?php echo $RazonSocial; ?> </strong></td>
                <td><strong><?php echo $RUC; ?> </strong></td>
                <td align="right"><strong><?php echo vfechahora($FechaCobranza); ?> </strong></td>
                <td><strong><?php echo $NroSerie; ?> </strong></td>
                <td><strong><?php echo $NroDocumento; ?></strong></td>
               
                <td <?php 
				if($IdEstadoComprobante==4) {
					echo "bgcolor='#0000CC'";
					}else if($IdEstadoComprobante==6) {
						echo "bgcolor='#00FF00'";
					 }else if($IdEstadoComprobante==9){
						 echo "bgcolor='#FF0000'";
						 }
					 
 
				
				 ?>><?php 
				if($IdEstadoComprobante==4) {
					echo " <font color='#FFFFFF'><strong>Pagado</strong></font>";
					}else if($IdEstadoComprobante==6) {
						echo "Devuelto";
						echo "<a href='../../MVC_Controlador/Caja/CajaC.php?acc=Imprimir_Devolucion&IdComprobantePago=$IdComprobantePago&IdEmpleado=$IdEmpleado'><img src='../../MVC_Complemento/img/imprimir.png'   /></a>";
					 }else if($IdEstadoComprobante==9){
						 echo "Anulado";
						 }
					 
 
				
				 ?></td>
                
 
 


              </tr>
            </table></td>
            </tr>
          <tr>
            <td width="173" valign="top"><strong style="font-size:11px; color:#333"><label for="">Observaciones</label></strong></td>
            <td width="623"><label for="Observaciones"></label>
              <textarea name="Observaciones" cols="65" rows="2"  id="Observaciones"></textarea></td>
          </tr>
          <tr>
            <td colspan="2">
              <table width="807" border="0">
                <tr bgcolor="#333333" style="color:#FFF;  font-size:12px">
                  <td width="67"><strong style="font-size:11px; color:#fff">C&oacute;digo</strong></td>
                  <td width="364"><strong style="font-size:11px; color:#fff">Descripci&oacute;n</strong></td>
                  <td width="141"><strong style="font-size:11px; color:#fff">Cantidad</strong></td>
                  <td width="86"><strong style="font-size:11px; color:#fff">P.U.(S/.)</strong></td>
                  <td width="115"><strong style="font-size:11px; color:#fff">SubTotal</strong></td>
                </tr>
                
                
							   <?php
                               if($Lista_OrdenesServicio != NULL) { 
                              foreach($Lista_OrdenesServicio as $item) 
                              {
                              ?> 
                              <tr border="1" bgcolor="#FFF">
                              <td><?php echo $item["Codigo"]; ?> </td>
                              <td><?php echo $item["Nombre"]; ?></td>
                              <td><?php echo $item["Cantidad"]; ?></td>
                              <td>s/.<?php echo substr($item["PrecioUnitario"], 0, -2); ?></td>
                              <td>s/.<?php echo substr($item["totalPorPagar"], 0, -2); ?></td>
                              </tr>
								<?php
                                }
                                ?>
                              <tr border="1" bgcolor="#FFF">
                              <td width="67" bgcolor="#F0F0F0"></td>
                              <td width="364" bgcolor="#F0F0F0"></td>
                              <td width="141" bgcolor="#F0F0F0"></td>
                              <td width="86" bgcolor="#333333"><strong style="font-size:11px; color:#fff">Total</strong></td>
                              <td width="115">s/.<?php echo substr($Total,0,-2); ?>
                                <label for="textfield7"></label>
                                <input type="hidden" name="montoDevuelto" id="montoDevuelto" value="<?php echo substr($Total,0,-2); ?>"></td>
                              </tr>  
							  <?php
                              }else{
                              ?>  <tr border="1" bgcolor="#FFF">
                              <td colspan="5" align="center" bgcolor="#F0F0F0" class="classy1"><strong style="font-size:11px; color:#fff">Buscar Documento</strong></td>
                              </tr>  
                        <?php
                                }
                                ?>
            </table>
            </td>
            </tr>
        </table>
    
    <table width="848" border="1" cellpadding="1" cellspacing="1" id="tblSample">
   
    </table>
     
    
     
</fieldset>
<BR>

<fieldset class="fieldset legend">
    <legend style="color:#03C"> </legend>
    <table width="848" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
    <th scope="row"><span style="text-align: center"> </span> <span style="text-align: center"> <img src="../../MVC_Complemento/img/botonaceptar.jpg" width="85" height="22" onClick="Guardar();" /></span></th>
  </tr>
</table>
</fieldset>

 </form>

 <div id="DetalleOrden">
          <iframe id="Panet_Datos" name="Panet_Datos" width="0" height="0" frameborder="0">
  <ilayer width="0" height="0" id="Panet_Datos" name="Panet_Datos">
  </ilayer>
</iframe>
		  </div>
          
 </body>         