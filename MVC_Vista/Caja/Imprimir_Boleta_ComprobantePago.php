<html>
<head>
<meta charset="utf-8">
<title>
Boleta de Impresion
</title>
</head>
<STYLE>
BODY table{
	font-size:12px;
	font-family:Tahoma, Geneva, sans-serif;	
}
</style>

<body onLoad="window.print(); window.close();">
				<?php 
 				if($Mostrar_CajaComprobantePagoPorIdComprobante != NULL) 
				{ 
                foreach($Mostrar_CajaComprobantePagoPorIdComprobante as $item)
				{
          $Id_devolucion=$item["idDevolucion"];
					$IdOrdenPago=$item["IdordenPago"];
          $NroHistoriaClinica=$item["NroHistoriaClinica"];
          $RazonSocial=$item["RazonSocial"];
          $NroSerie=$item["NroSerie"];
          $NroDocumento=$item["NroDocumento"];
          $Total=$item["Total"];
          $FechaCobranza=$item["FechaCobranza"];
          $IdCuentaAtencion=$item["IdCuentaAtencion"];
          $Usuario=$item["Usuario"];
				}
				}
				?>
<br>
<table width="742" border="0">
  <tr>
    <td width="286">&nbsp;</td>
    <td width="45">&nbsp;</td>
    <td width="138">&nbsp;</td>
    <td width="83">&nbsp;</td>
    <td width="168">&nbsp;</td>
  </tr>
   <tr>
    <td width="286">&nbsp;</td>
    <td width="45">&nbsp;</td>
    <td width="138">&nbsp;</td>
    <td width="83">&nbsp;</td>
    <td width="168">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><strong><h3>Nª de Boleta  01-00000<?php  echo $Id_devolucion;  ?></h3></strong></td>
    <td colspan="3">F. E.<?php print date("m/d/Y G.i:s<br>", time());?></td>
  </tr>
  <tr height="1">
    <td height="1">&nbsp;</td>
    <td height="1">&nbsp;</td>
    <td height="1">&nbsp; </td>
    <td height="1">&nbsp;</td>
    <td height="1">&nbsp;</td>
  </tr>
  <tr>
    <td>Fecha de Emision BD</td>
    <td>&nbsp;</td>
    <td><?php  echo substr($FechaCobranza,0,10);  ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Nª de Boleta Devuelta</td>
    <td>&nbsp;</td>
    <td><?php  echo $NroSerie.' - '.$NroDocumento;  ?></td>
    <td>&nbsp;</td>
    <td></td>
  </tr>
  <tr>
    <td>Usuario que Genero BD</td>
    <td>&nbsp;</td>
    <td><?php  echo $Usuario;  ?></td>
    <td>&nbsp;</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">Paciente.  <?php  echo ' '.$RazonSocial;?></td>
    <td>HC</td>
    <td><?php  echo ' '.$NroHistoriaClinica;?></td>
  </tr>
  <tr>
    <td colspan="5"><table width="475" border="0">
      <tr>
        <td width="137" height="25">Nª cta.</td>
        <td width="126"><?php  echo ' '.$IdCuentaAtencion;?></td>
        <td width="123">Nª orden.</td>
        <td width="71"><?php  echo ' '.$IdOrdenPago;?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><table width="333" border="0">
      <tr>
        <td width="57">&nbsp;</td>
        <td width="266" style="text-align:right">Monto a Devolver</td>
        </tr>
    </table></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>S/.<?php  echo substr($Total,0,-2);  ?></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>