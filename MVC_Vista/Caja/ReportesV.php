    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Antimicrobianos</title>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

		
		.letras_pequeñas{
			font-size:9px;
			font-weight:bold;
			color:red;
		}
		
		.letras_pequeñas_2{
			font-size:8px;
			font-weight:bold;
			color:red;
		}

		label[for=message]
		{
		font-weight:bold;
		}
		</style>
		
		
		<script>
						
			$(document).ready(function() {

				$('.Contenedor_Reporte_I').hide();
				$('.Contenedor_Reporte_II').hide();
				$('.Contenedor_Reporte_III').hide();	
				$('.Contenedor_Reporte_IV').hide();					
				$('.Contenedor_Reporte_V').hide();	
				$('.Contenedor_Reporte_VI').hide();	
				$('.Contenedor_Reporte_VII').hide();	
				$('.Contenedor_Reporte_VIII').hide();	
				$('.Contenedor_Reporte_IX').hide();	
				$('.Contenedor_Reporte_X').hide();	

				/* Da Formato correcto al easyui  */
				
				$('.easyui-datebox').datebox({
					formatter : function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
					},
					parser : function(s){

						if (!s) return new Date();
						var ss = s.split('-');
						var y = parseInt(ss[2],10);
						var m = parseInt(ss[1],10);
						var d = parseInt(ss[0],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d)
						} else {
							return new Date();
						}
					}

				});				
						

				$("#Fuente_Financiamiento").change(function () {
				   $("#Fuente_Financiamiento option:selected").each(function () {
						elegido=$(this).val();
						$.post("../../MVC_Controlador/Caja/CajaC.php?acc=Selected_Fuente_Financiamiento", { elegido: elegido }, function(data){
						$("#Tipos_Financiamiento").html(data);
						});            
					});
				})



				/*   1.- Funcion de Deudas     */
				function Lista_Deudas(Fecha_Inicial,Fecha_Final,IdFuenteFinanciamiento,IdTipoServicio){
									var dg = $('#Listado_Deudas').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Listado_Deudas&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final+'&IdFuenteFinanciamiento='+IdFuenteFinanciamiento+'&IdTipoServicio='+IdTipoServicio,
									columns:[[
											{field:'ServicioNombre',title:'Nombre de Servicio',width:250,sortable:true},
											{field:'NombrePaciente',title:'Nombre de Paciente',width:300,sortable:true},
											{field:'IdCuentaAtencion',title:'Nro <br>Cuenta',width:90,sortable:true},
											{field:'NroHistoriaClinica',title:'Historia <br>Clinica',width:90,sortable:true},
											{field:'FechaIngreso',title:'Fecha <br>Ingreso',width:90,sortable:true},
											{field:'FechaEgreso',title:'Fecha <br>Egreso',width:90,sortable:true},
											{field:'Descripcion',title:'Financiamiento',width:190,sortable:true},
											{field:'TotalPagar',title:'Total a <br>Pagar',width:110,sortable:true}
										]],
									rowStyler: function(index,row)
									{
										
												if (parseFloat(row.IdEstadoComprobante)==6 ){
													return 'color:#04B404;';
												}
												if (parseFloat(row.IdEstadoComprobante)==9){
													return 'color:#FF0000;';
												}		
									}
									});
									dg.datagrid('enableFilter');

				}		
						
						

				/* 2.- Funcion de Liquidaciones */	
				function Lista_Liquidaciones(Fecha_Inicial,Fecha_Final){
									var dg = $('#Lista_Deudas_Hospitalizaciones').datagrid({
									filterBtnIconCls:'icon-filter',
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Liquidaciones&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final,
									columns:[[
											{field:'RazonSocial',title:'Apellidos y Nombres',width:260,sortable:true},
											{field:'NroHistoriaClinica',title:'Nro Historia<br> Clinica',width:90,sortable:true},
											{field:'IdCuentaAtencion',title:'Nro <br> Cuenta',width:80,sortable:true},
											{field:'Boleta_Venta',title:'Nro Boleta<br>de Venta',width:130,sortable:true},
											{field:'ServicioNombre',title:'Nombre de <br>Servicio',width:200,sortable:true},
											{field:'Total',title:'Monto',width:90,sortable:true,styler:cellStyler},
											{field:'FechaCobranza',title:'Fecha <br>Cobranza',width:170,sortable:true},
											{field:'InicialesCajero',title:'Usuario',width:50,sortable:true},
											{field:'NombreCajero',title:'Nombre de <br>Usuario',width:250,sortable:true},
										]],
									rowStyler: function(index,row)
									{
										
												if (parseFloat(row.IdEstadoComprobante)==6 ){
													return 'color:#04B404;';
												}
												if (parseFloat(row.IdEstadoComprobante)==9){
													return 'color:#FF0000;';
												}
												
									}
									});
				}		
					

				/*3.- Funcion de Extornos */
				
				function Lista_Extornos(Fecha_Inicial,Fecha_Final){
									var dg = $('#Listado_Extornos').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Listado_Extornos&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final,
									columns:[[
											{field:'NombreCajero',title:'Nombre de Cajero',width:250,sortable:true},
											{field:'RazonSocial',title:'Nombres y Apellidos del Paciente',width:250,sortable:true},
											{field:'NroHistoriaClinica',title:'Historia <br> Clinica',width:80},
											{field:'NroSerie',title:'Nro de <br> Serie',width:55,sortable:true},
										    {field:'NroDocumento',title:'Nro de <br> Documento',width:75,sortable:true},
											{field:'Total',title:'Monto',width:80,sortable:true,styler:cellStyler},
											{field:'FechaCobranza',title:'Fecha de<br>Cobranza',width:150,sortable:true},
											{field:'UsuarioExtorno',title:'Usuario Realizo<br>Extorno',width:200,sortable:true}
											
										]]
									});
									dg.datagrid('enableFilter');

				}
					
                



               	/*3.- Funcion de Consolidado por_Servicio */
				

				function Lista_Consolidado_por_Servicio(Fecha_Inicial,Fecha_Final){
									var dg = $('#Listado_Consolidado_por_Servicio').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Consolidado_Servicios&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final,
									columns:[[
											{field:'Nombre',title:'Nombre de Servicio',width:300,sortable:true},
											{field:'SumaTotal',title:'Monto',width:130,sortable:true}
										]]
									});
									dg.datagrid('enableFilter');

				}
				
				function Lista_Devoluciones(Fecha_Inicial,Fecha_Final){
					
									var dg = $('#Lista_Devoluciones').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Devoluciones&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final,
									columns:[[
											{field:'NombreCajero',title:'Nombre de Cajero',width:190,sortable:true},
											{field:'RazonSocial',title:'Nombres y Apellidos del Paciente',width:250,sortable:true},
											{field:'NroHistoriaClinica',title:'Historia <br> Clinica',width:80},
											{field:'NroSerie',title:'Nro de <br> Serie',width:55,sortable:true},
										    {field:'NroDocumento',title:'Nro de <br> Documento',width:75,sortable:true},
											{field:'Total',title:'Monto',width:80,sortable:true,styler:cellStyler},
											{field:'FechaPagado',title:'Fecha de<br>Devolucion',width:100,sortable:true},
											{field:'NombreAutoriza',title:'Usuario Realizo<br>Extorno',width:200,sortable:true}
		
										]]
									});
									dg.datagrid('enableFilter');

				}
				
				
				function Lista_Devoluciones_II(Fecha_Inicial,Fecha_Final,NroSerie){
					
									var dg = $('#Lista_Devoluciones_II').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Devoluciones_II&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final+'&NroSerie='+NroSerie,
									columns:[[
									
											{field:'NroSerie',title:'Nro de <br> Serie',width:50,sortable:true},
											{field:'NroDocumento',title:' Nro de <br> Documento',width:90,sortable:true},
											{field:'RazonSocial',title:'Razon Social',width:250},
											{field:'Exoneraciones',title:'Exoneraciones',width:110,sortable:true},
										    {field:'Total',title:'Total',width:100,sortable:true},
											{field:'FechaCobranza',title:'Fecha de Cobranza',width:120,sortable:true},
											{field:'NombreCajero',title:'Nombre de Cajero',width:200,sortable:true}
										]]
									});
									dg.datagrid('enableFilter');

				}
				
				
				function Lista_Pendientes_Pago(Fecha_Inicial,Fecha_Final,Cajeros){
					  
		  
									var dg = $('#Lista_Pendientes_Pago').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									rowtexts:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Pendientes_Pago&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final+'&Cajeros='+Cajeros,
									columns:[[
											{field:'IdOrden',title:'Nro Orden',width:80,sortable:true},
											{field:'ApellidoPaterno',title:'Apellido <br> Paterno',width:120,sortable:true},
											{field:'ApellidoMaterno',title:' Apellido <br> Materno',width:120,sortable:true},
											{field:'PrimerNombre',title:'Primer <br>  Nombre',width:110},
											{field:'Total',title:'Total',width:90,sortable:true,styler:cellStyler},
											{field:'IdCuentaAtencion',title:'Cuenta <br> Atencion',width:80,sortable:true},
										    {field:'NroHistoriaClinica',title:'Nro Historia <br>Clinica',width:85,sortable:true},
											{field:'FechaCreacion',title:'Fecha de <br> Creacion',width:130,sortable:true},
											{field:'Empleado',title:'Empleado',width:220,sortable:true}
										]]
									});
				}

		

				function Lista_Recaudado_Por_Cajero(Fecha_Inicial,Fecha_Final){	  
									var dg = $('#Lista_Recaudado_Por_Cajero').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									rowtexts:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Recaudado_Por_Cajero&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final,
									columns:[[
											{field:'ApellidoPaterno',title:'Apellido<br>Paterno',width:150,sortable:true},
											{field:'ApellidoMaterno',title:'Apellido<br>Materno',width:150,sortable:true},
											{field:'Nombres',title:'Nombres',width:150,sortable:true},
											{field:'m_total',title:'Monto <br>Total',width:120,sortable:true},
											/*{field:'m_pagado',title:'Monto <br>Pagado',width:125,sortable:true},*/
											{field:'m_anulado',title:'Monto <br>Anulado',width:120,sortable:true},
											{field:'m_devuelto',title:'Monto <br>Devuelto',width:120,sortable:true},
											{field:'m_recaudado',title:'Monto <br>Recaudado',width:120,sortable:true,styler:cellStyler},
											{field:'b_totales',title:'Nro<br>Boletas<br>Totales',width:70,sortable:true},
											{field:'b_anuladas',title:'Nro<br>Boletas<br>Anuladas',width:70,sortable:true}
										]]
									});
				}



		        function cellStyler(value,row,index){
		                return 'background-color:#FAF8DE;color:red;';
		        }

				
				
				function Lista_Consolidado_por_Aseguradoras(Fecha_Inicial,Fecha_Final,IdFuenteFinanciamiento,IdTipoServicio){
									var dg = $('#Lista_Consolidado_por_Aseguradoras').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Consolidado_por_Aseguradoras&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final+'&IdFuenteFinanciamiento='+IdFuenteFinanciamiento+'&IdTipoServicio='+IdTipoServicio,
									columns:[[
											{field:'TipoServicio',title:'Tipo de<br>Servicio',width:190,sortable:true},
											{field:'ServicioNombre',title:'Nombre de Servicio',width:200,sortable:true},
											{field:'NombrePaciente',title:'Nombre de Paciente',width:300,sortable:true},
											{field:'IdCuentaAtencion',title:'Nro <br>Cuenta',width:90,sortable:true},
											{field:'NroHistoriaClinica',title:'Historia <br>Clinica',width:90,sortable:true},
											{field:'FechaIngreso',title:'Fecha <br>Ingreso',width:90,sortable:true},
											{field:'TotalPagar',title:'Total a <br>Pagar',width:90,sortable:true,styler:cellStyler}
										]]
									});
									dg.datagrid('enableFilter');

				}
				
				
				function Lista_Consolidado_por_Serie(Fecha_Inicial,Fecha_Final){
									var dg = $('#Listado_Consolidado_por_Serie').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Caja/CajaC.php?acc=Lista_Consolidado_por_Serie&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final,
									columns:[[
											{field:'NroSerie',title:'Nro de <br> Serie',width:90,sortable:true},
											{field:'NrodocumentoMin',title:'Nro <br>Documento Min',width:130,sortable:true},
											{field:'NrodocumentoMax',title:'Nro <br>Documento Max',width:130,sortable:true},
											{field:'Total',title:'Total en <br>Boletas',width:110,sortable:true},
											{field:'Anulado',title:'Total <br>Extornado',width:110,sortable:true},
											{field:'Pagado',title:'Total <br>Recaudado',width:110,sortable:true,styler:cellStyler}
										]]
									});
				}				
			
												
				function Highchart_Consolidado_por_Servicio(Fecha_Inicial_Reporte,Fecha_Final_Reporte)
				
				{

					$.ajax({
						url: '../../MVC_Controlador/Caja/CajaC.php?acc=Highchart_Consolidado_por_Servicio',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte
						},

						success:function(res)
						{
						var	Fecha_Inicial=Fecha_Inicial_Reporte;
						var	Fecha_Final=Fecha_Final_Reporte;
						objeto=jQuery.parseJSON(res);				
						$('#Contenedor_Reporte_III').highcharts({		
								chart: {
									plotBackgroundColor: null,
									plotBorderWidth: null,
									plotShadow: false,
									type: 'pie'
								},
								title: {
									text: '<strong>Reporte de Consolidado por Servicio<br>'+
										  'Desde :'+Fecha_Inicial+'<br>'+
									      'Hasta :'+Fecha_Final+'</strong>'
								},
								tooltip: {
									pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
								},
								plotOptions: {
									pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										dataLabels: {
											enabled: true,
											format: '<b>{point.name}</b>: {point.percentage:.1f} %',
											style: {
												color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
											}
										}
									}
								},
								series: [{
									name: 'Brands',
									colorByPoint: true,
									data: objeto
								}]

									
							});
					
						}
					});
					
				}			
				



									
				function Highchart_Recaudado_por_Cajero(Fecha_Inicial_Reporte,Fecha_Final_Reporte)
				
				{

					$.ajax({
						url: '../../MVC_Controlador/Caja/CajaC.php?acc=Highchart_Recaudado_por_Cajero',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte
						},

						success:function(res)
						{
						var	Fecha_Inicial=Fecha_Inicial_Reporte;
						var	Fecha_Final=Fecha_Final_Reporte;
						objeto=jQuery.parseJSON(res);				
						$('#Contenedor_Reporte_V').highcharts({		
								chart: {
									plotBackgroundColor: null,
									plotBorderWidth: null,
									plotShadow: false,
									type: 'pie'
								},
								title: {
									text: '<strong>Reporte de Recaudacion por Cajero<br>'+
										  'Desde :'+Fecha_Inicial+'<br>'+
									      'Hasta :'+Fecha_Final+'</strong>'
								},
								tooltip: {
									pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
								},
								plotOptions: {
									pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										dataLabels: {
											enabled: true,
											format: '<b>{point.name}</b>: {point.percentage:.1f} %',
											style: {
												color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
											}
										}
									}
								},
								series: [{
									name: 'Brands',
									colorByPoint: true,
									data: objeto
								}]

									
							});
					
						}
					});
					
				}	



				function Highchart_Recaudado_por_Serie(Fecha_Inicial_Reporte,Fecha_Final_Reporte)
				
				{

					$.ajax({
						url: '../../MVC_Controlador/Caja/CajaC.php?acc=Highchart_Consolidado_por_Serie',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte
						},

						success:function(res)
						{
						var	Fecha_Inicial=Fecha_Inicial_Reporte;
						var	Fecha_Final=Fecha_Final_Reporte;
						objeto=jQuery.parseJSON(res);				
						$('#Contenedor_Reporte_VI').highcharts({		
								chart: {
									plotBackgroundColor: null,
									plotBorderWidth: null,
									plotShadow: false,
									type: 'pie'
								},
								title: {
									text: '<strong>Reporte de Recaudacion por Serie<br>'+
										  'Desde :'+Fecha_Inicial+'<br>'+
									      'Hasta :'+Fecha_Final+'</strong>'
								},
								tooltip: {
									pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
								},
								plotOptions: {
									pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										dataLabels: {
											enabled: true,
											format: '<b>{point.name}</b>: {point.percentage:.1f} %',
											style: {
												color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
											}
										}
									}
								},
								series: [{
									name: 'Brands',
									colorByPoint: true,
									data: objeto
								}]

									
							});
					
						}
					});
					
				}	

				
				function Exportar_Reporte_Liquidaciones(Fecha_Inicial_Reporte,Fecha_Final_Reporte,IdFuenteFinanciamiento,IdTipoServicio)
				{
					$.ajax({
						url: '../../MVC_Controlador/Caja/CajaC.php?acc=Exportar_Reporte_Liquidaciones',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte,
							IdFuenteFinanciamiento:IdFuenteFinanciamiento,
							IdTipoServicio:IdTipoServicio
						},

						success:function(impresion)
						{
						 window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
						 e.preventDefault();
						 alert(impresion);
						}
					});

				}	
				
				
				function Exportar_Reporte_Consolidado_por_Servicio(Fecha_Inicial,Fecha_Final)
				{
					$.ajax({
						url: '../../MVC_Controlador/Caja/CajaC.php?acc=Exportar_Reporte_Consolidado_Servicios',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial,
							Fecha_Final:Fecha_Final
						},

						success:function(impresion)
						{
						 window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
						 e.preventDefault();
						 alert(impresion);
						}
					});

				}	
			
				function Exportar_Reporte_Extornos(Fecha_Inicial,Fecha_Final)
				{
					$.ajax({
						url: '../../MVC_Controlador/Caja/CajaC.php?acc=Exportar_Reporte_Extornos',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial,
							Fecha_Final:Fecha_Final
						},

						success:function(impresion)
						{
						 window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
						 e.preventDefault();
						 alert(impresion);
						}
					});

				}
				
				function Exportar_Reporte_por_Serie(Fecha_Inicial,Fecha_Final)
				{
					$.ajax({
						url: '../../MVC_Controlador/Caja/CajaC.php?acc=Exportar_Reporte_Serie',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial,
							Fecha_Final:Fecha_Final
						},

						success:function(impresion)
						{
						 window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
						 e.preventDefault();
						 alert(impresion);
						}
					});

				}
				

				//Evento de Click de Vistas Previas

				$("#Generar_Reporte_I").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Deudas');
					$('.Contenedor_Reporte_I').show();
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();
					$('.Contenedor_Reporte_X').hide();

					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento_Deuda option:selected" ).val();
					var IdTipoServicio = $( "#Tipo_Servicio_Deuda option:selected" ).val();
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Deudas(Fecha_Inicial_Reporte,Fecha_Final_Reporte,IdFuenteFinanciamiento,IdTipoServicio);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
				});
				


				
				
				$("#Generar_Reporte_II").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Extornos');
					$('.Contenedor_Reporte_II').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_III').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_V').hide();
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();	
					$('.Contenedor_Reporte_X').hide();					

					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_II').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_II').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{								
								Lista_Extornos(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				
				
				
				


				$("#Generar_Reporte_III").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Consolidado por Servicio');
					$('.Contenedor_Reporte_III').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_II').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_V').hide();
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();
					$('.Contenedor_Reporte_X').hide();

					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_III').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_III').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Consolidado_por_Servicio(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								Highchart_Consolidado_por_Servicio(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				

				


				
				$("#Generar_Reporte_IV").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de estado de cuenta de las empresas aseguradoras y entidades publicas con convenio');
					$('.Contenedor_Reporte_IV').show();	
					$('.Contenedor_Reporte_I').hide();	
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();
					$('.Contenedor_Reporte_V').hide();
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();
					$('.Contenedor_Reporte_X').hide();

					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_IV').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_IV').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento option:selected" ).val();
					var IdTipoServicio=$( "#Tipo_Servicio option:selected" ).val();
					
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
							    Lista_Consolidado_por_Aseguradoras(Fecha_Inicial_Reporte,Fecha_Final_Reporte,IdFuenteFinanciamiento,IdTipoServicio);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				


				

				$("#Generar_Reporte_V").click(function(event) {
					$('#Titulo_Reporte').text('Reporte Recaudacion por Cajero');
					$('.Contenedor_Reporte_V').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();
					$('.Contenedor_Reporte_X').hide();	

					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_V').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_V').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Recaudado_Por_Cajero(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								Highchart_Recaudado_por_Cajero(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				




				
				$("#Generar_Reporte_VI").click(function(event) {
					$('#Titulo_Reporte').text('Reporte por Serie');
					$('.Contenedor_Reporte_VI').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();						
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_V').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();
					$('.Contenedor_Reporte_X').hide();

					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VI').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VI').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Consolidado_por_Serie(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								Highchart_Recaudado_por_Serie(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				


				
				$("#Generar_Reporte_VII").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Liquidaciones');
					$('.Contenedor_Reporte_VII').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_V').hide();
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();
					$('.Contenedor_Reporte_X').hide();
			
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VII').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VII').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Liquidaciones(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				


				$("#Generar_Reporte_VIII").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Devoluciones por Dia');
					$('.Contenedor_Reporte_VIII').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_IX').hide();
					$('.Contenedor_Reporte_X').hide();
			
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VIII').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VIII').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Devoluciones(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
					


				$("#Generar_Reporte_IX").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Devoluciones');
					$('.Contenedor_Reporte_IX').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_X').hide();
				
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_IX').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_IX').datebox('getValue');
					var NroSerie=$( "#NroSerie option:selected" ).val();
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Devoluciones_II(Fecha_Inicial_Reporte,Fecha_Final_Reporte,NroSerie);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});




				$("#Generar_Reporte_X").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Pendientes de Pago');
					$('.Contenedor_Reporte_X').show();
					$('.Contenedor_Reporte_I').hide();
					$('.Contenedor_Reporte_II').hide();
					$('.Contenedor_Reporte_III').hide();	
					$('.Contenedor_Reporte_IV').hide();	
					$('.Contenedor_Reporte_VI').hide();
					$('.Contenedor_Reporte_VII').hide();
					$('.Contenedor_Reporte_VIII').hide();
					$('.Contenedor_Reporte_IX').hide();
						
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_X').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_X').datebox('getValue');
					var Cajeros=$( "#Cajeros option:selected" ).val();
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Pendientes_Pago(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Cajeros);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});













			    //Generar Excel 	


				$("#Generar_Excel_I").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento_Deuda option:selected" ).val();
					var IdTipoServicio = $( "#Tipo_Servicio_Deuda option:selected" ).val();
					Exportar_Reporte_Liquidaciones(Fecha_Inicial_Reporte,Fecha_Final_Reporte,IdFuenteFinanciamiento,IdTipoServicio);
				});
				
				$("#Generar_Excel_Previa_I").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento_Deuda option:selected" ).val();
					var IdTipoServicio = $( "#Tipo_Servicio_Deuda option:selected" ).val();
					Exportar_Reporte_Liquidaciones(Fecha_Inicial_Reporte,Fecha_Final_Reporte,IdFuenteFinanciamiento,IdTipoServicio);
				});





				$("#Generar_Excel_II").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_II').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_II').datebox('getValue');
					Exportar_Reporte_Extornos(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
				});
				



				$("#Generar_Excel_III").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_III').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_III').datebox('getValue');
					Exportar_Reporte_Consolidado_por_Servicio(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
				});
				



			     $("#Generar_Excel_V").click(function(e) {
				    var htmltable= document.getElementById('Contenido_Excel_Recaudacion');
					var html = htmltable.outerHTML;
					  //add more symbols if needed...
		 				     while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;'); 
	    					 while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;'); 
	    					 while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;'); 
	    					 while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;'); 
	    					 while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;'); 
	     					 while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;'); 
	     					 while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;'); 
	    					 while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;'); 
	    					 while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;'); 
	    					 while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;'); 
	   					     while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;'); 
	     			         while (html.indexOf('ñ') != -1) html = html.replace('ñ', '&ntilde;'); 
	                         while (html.indexOf('Ñ') != -1) html = html.replace('Ñ', '&Ntilde;'); 
					window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
				});


				$("#Generar_Excel_VI").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VI').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VI').datebox('getValue');
					Exportar_Reporte_por_Serie(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
				});
				


			     $("#Generar_Excel_VII").click(function(e) {
				    var htmltable= document.getElementById('Contenido_Excel_Liquidaciones');
					var html = htmltable.outerHTML;
					  //add more symbols if needed...
		 				     while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;'); 
	    					 while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;'); 
	    					 while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;'); 
	    					 while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;'); 
	    					 while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;'); 
	     					 while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;'); 
	     					 while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;'); 
	    					 while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;'); 
	    					 while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;'); 
	    					 while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;'); 
	   					     while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;'); 
	     			         while (html.indexOf('ñ') != -1) html = html.replace('ñ', '&ntilde;'); 
	                         while (html.indexOf('Ñ') != -1) html = html.replace('Ñ', '&Ntilde;'); 
					window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
				});





				
			     $("#Generar_Excel_X").click(function(e) {
				    var htmltable= document.getElementById('Contenido_Excel');
					var html = htmltable.outerHTML;
					  //add more symbols if needed...
		 				     while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;'); 
	    					 while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;'); 
	    					 while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;'); 
	    					 while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;'); 
	    					 while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;'); 
	     					 while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;'); 
	     					 while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;'); 
	    					 while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;'); 
	    					 while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;'); 
	    					 while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;'); 
	   					     while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;'); 
	     			         while (html.indexOf('ñ') != -1) html = html.replace('ñ', '&ntilde;'); 
	                         while (html.indexOf('Ñ') != -1) html = html.replace('Ñ', '&Ntilde;'); 
					window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
				});





			     //Generar PDF



				$("#Generar_PDF_I").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento_Deuda option:selected" ).val();
					var IdTipoServicio = $( "#Tipo_Servicio_Deuda option:selected" ).val();
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Deudas&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&IdTipoServicio="+IdTipoServicio+"&IdFuenteFinanciamiento="+IdFuenteFinanciamiento;
				});

				$("#P_Generar_Pdf_I").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento_Deuda option:selected" ).val();
					var IdTipoServicio = $( "#Tipo_Servicio_Deuda option:selected" ).val();
					if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Deudas&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&IdTipoServicio="+IdTipoServicio+"&IdFuenteFinanciamiento="+IdFuenteFinanciamiento;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}

		
				
				});






				$("#Generar_PDF_II").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_II').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_II').datebox('getValue');
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Extornos&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
				});
				
				
				$("#P_Generar_Pdf_II").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_II').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_II').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
						location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Extornos&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
					
				});
				




				$("#Generar_Pdf_III").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_III').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_III').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Vista/Reportes/Caja/Consolidado.php?FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
					
				});
				

				$("#P_Generar_Pdf_III").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_III').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_III').datebox('getValue');
					if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Vista/Reportes/Caja/Consolidado.php?FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
				});
				



				$("#Generar_Pdf_IV").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_IV').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_IV').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento option:selected" ).val();
					var IdTipoServicio=$( "#Tipo_Servicio option:selected" ).val();
					if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Aseguradoras&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&IdFuenteFinanciamiento="+IdFuenteFinanciamiento+"&IdTipoServicio="+IdTipoServicio;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
					
				});
				


				$("#P_Generar_Pdf_IV").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_IV').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_IV').datebox('getValue');
					var IdFuenteFinanciamiento=$( "#Fuente_Financiamiento option:selected" ).val();
					var IdTipoServicio=$( "#Tipo_Servicio option:selected" ).val();
					if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Aseguradoras&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&IdFuenteFinanciamiento="+IdFuenteFinanciamiento+"&IdTipoServicio="+IdTipoServicio;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}

				});
				





				// V. PDF Monto Recaudado por Cajero

				$("#Generar_Pdf_V").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_V').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Inicial_Reporte_V').datebox('getValue');
					if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Monto_Recaudado_por_Cajero&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
				});


				$("#P_Generar_Pdf_V").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_V').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_V').datebox('getValue');
					if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Monto_Recaudado_por_Cajero&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
				});




				// VI. PDF Reporte por Serie

				$("#Generar_Pdf_VI").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VI').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VI').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Serie&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}
					
				});	
				
				$("#P_Generar_Pdf_VI").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VI').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VI').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Serie&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}

				});	






				// VII. PDF Reporte por Liquidaciones

				$("#Generar_Pdf_VII").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VII').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VII').datebox('getValue');
					if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
					{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
					}
					else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Liquidaciones_Hospitalizados&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
					}
				});
				

				$("#P_Generar_Pdf_VII").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VII').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VII').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Liquidaciones_Hospitalizados&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}


				
				});




				// VII. PDF Reporte por Devoluciones

				$("#Generar_Pdf_VIII").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VIII').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VIII').datebox('getValue');
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Devoluciones&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
				});

				$("#P_Generar_Pdf_VIII").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_VIII').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_VIII').datebox('getValue');
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Devoluciones&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte;
				});

				

				


				// IX. PDF Reporte por Devoluciones
				$("#Generar_Pdf_IX").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_IX').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_IX').datebox('getValue');
					var NroSerie=$( "#NroSerie option:selected" ).val();
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Devoluciones_II&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&NroSerie="+NroSerie;
				});



				$("#P_Generar_Pdf_IX").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_IX').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_IX').datebox('getValue');
					var NroSerie=$( "#NroSerie option:selected" ).val();
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Devoluciones_II&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&NroSerie="+NroSerie;
				});
				

				



				// X. PDF Reporte Pendiente de Pago

				$("#Generar_Pdf_X").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_X').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_X').datebox('getValue');
					var Cajeros=$( "#Cajeros option:selected" ).val();
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Pendientes_Pago&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&Cajeros="+Cajeros;
				});


				$("#P_Generar_Pdf_X").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_X').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_X').datebox('getValue');
					var Cajeros=$( "#Cajeros option:selected" ).val();
					location="../../MVC_Controlador/Caja/CajaC.php?acc=PDF_Pendientes_Pago&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&Cajeros="+Cajeros;
				});

				
				
			});
		</script>
		
		
	


    </head>
	
    <body>
	
		<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
		<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
		<div class="easyui-layout" style="width:100%;height:800px;">
		
		
				<div data-options="region:'center',iconCls:'icon-ok'"  style="width:80%;">
				
				
		
					
					<div class="easyui-tabs" data-options="tabWidth:100,tabHeight:60" style="width:100%;height:100%;">
					
					
						<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/lista.png' width='35' height='35'/>
						<br>Lista</span>" style="padding:10px;height:850px">
						
								<span id="Titulo_Reporte" style="padding-top:19;padding-left:4px;padding-bottom:60px;font-weight:bold;font-size:18px;Color:#585858"></span>
								
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_I">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_I">Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_PDF_I" >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<table id="Listado_Deudas"></table>
								</div>
						
						
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_II">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_II" disabled>Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_PDF_II" >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<table id="Listado_Extornos"></table>
								</div>
						
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_III">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_III" disabled>Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_III"  >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<table id="Listado_Consolidado_por_Servicio"></table>
								</div>
								

								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_IV">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_IV" disabled>Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_IV" >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<table id="Lista_Consolidado_por_Aseguradoras"></table>
								</div>
								
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_V">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_V" >Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_V"   >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									
									<div  id="Contenido_Excel_Recaudacion">
									<table id="Lista_Recaudado_Por_Cajero"   style="width:100%   !important ;height: 450px   !important;  "></table>
									</div>
								</div>

								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_VI">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_VI" disabled>Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_VI" >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<table id="Listado_Consolidado_por_Serie"></table>
								</div>
								
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_VII">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_VII">Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_VII" >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<div  id="Contenido_Excel_Liquidaciones">
									<table id="Lista_Deudas_Hospitalizaciones" style="height:600px ! important;overflow: auto ! important;"></table>
									</div>
								</div>
								
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_VIII">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_VIII" disabled>Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_VIII" >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<table id="Lista_Devoluciones"></table>
								</div>
								
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_IX">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_IX" disabled>Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_IX" >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									<table id="Lista_Devoluciones_II"></table>
								</div>
															
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_X">
								    <div style="padding:5px 0;">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Excel_X" >Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Pdf_X"   >Generar PDF</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" disabled>Generar Word</a>
									</div>
									
									<div  id="Contenido_Excel">
									<table id="Lista_Pendientes_Pago"   style="width:100%   !important ;height: 450px   !important;  "></table>
									</div>
								</div>
						</div>
						
						
						<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/grafico.png' width='35' height='35'/>
						<br>Grafico</span>" 
						style="padding:10px">
							<div id="Contenedor_Reporte_I" class="Contenedor_Reporte_I" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_II" class="Contenedor_Reporte_II" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_III" class="Contenedor_Reporte_III" style="width: 900px; height: 650px;">
							</div>
							<div id="Contenedor_Reporte_IV" class="Contenedor_Reporte_IV" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_V" class="Contenedor_Reporte_V" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_VI" class="Contenedor_Reporte_VI" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_VII" class="Contenedor_Reporte_VII" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_VIII" class="Contenedor_Reporte_VIII" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_IX" class="Contenedor_Reporte_IX" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
							<div id="Contenedor_Reporte_X" class="Contenedor_Reporte_X" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
						</div>
					</div>
					<style scoped="scoped">
						.tt-inner{
							display:inline-block;
							line-height:12px;
							padding-top:5px;
						}
						.tt-inner img{
							border:0;
						}
					</style>
					
					
				</div>
				<div data-options="region:'west',split:true"  style="width:20%;">	
				<div class="easyui-tabs" style="width:100%;">	
					<div title="Reportes de Economia">			
					<div class="easyui-accordion">




						<!-------------- I. Reporte de Deudas   ------------------->	
						<div title="Reporte de Deudas" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de Deudas</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fuente <br> Financiamiento :</span>
									<br>
									<select  name="Fuente_Financiamiento_Deuda" style="width:250px" id="Fuente_Financiamiento_Deuda" >
									<option value="0">Todos</option>
									<?php 
									$resultados=Mostrar_Lista_Fuentes_Financiamiento_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["idfuentefinanciamiento"] ?>" <?php if($_REQUEST["idfuentefinanciamiento"]==$resultados[$i]["idfuentefinanciamiento"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
									<?php 
									}}
									?>                   
								</select>
								</div>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Tipo de <br> Servicio :</span>
									<br>
									<select  name="Tipo_Servicio_Deuda" style="width:250px" id="Tipo_Servicio_Deuda" >
									<!--<option value="0">Todos</option>-->
									<?php 
									$resultados=Mostrar_Lista_Tipo_Servicio_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["IdTipoServicio"] ?>" <?php if($_REQUEST["idTipoServicio"]==$resultados[$i]["idTipoServicio"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
									<?php 
									}}
									?>                   
									</select>
								</div>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_I" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_I" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_I">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Excel_Previa_I">Generar Excel</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_I">Generar PDF</a>
								</div>		
						</div>






						<!-------------- II. Reporte de Extornos   ------------------->	

						<div title="Reporte de Extornos" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de Externos</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_II" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_II" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_II">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_II">Generar PDF</a>
								</div>									
						</div>




						<!-------------- III. Reporte de Consolidado por Servicio   ------------------->	

						<div title="Reporte de Consolidado por Servicio" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de Consolidado por Servicio</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_III" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_III" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_III">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_III">Generar PDF</a>
								</div>

						</div>
						

						




						<!-------------- IV. Reporte de Estado de Cuenta Empresas   ------------------->	

						<div title="Reporte de Estado de Cuenta Empresas" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de estado de cuenta de las empresas aseguradoras y entidades publicas con convenio</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_IV" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_IV" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fuente <br> Financiamiento :</span>
									<br>
									<select  name="Fuente_Financiamiento" style="width:250px" id="Fuente_Financiamiento" >
									<?php 
									$resultados=Mostrar_Lista_Fuentes_Financiamiento_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["idfuentefinanciamiento"] ?>" <?php if($_REQUEST["idfuentefinanciamiento"]==$resultados[$i]["idfuentefinanciamiento"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
									<?php 
									}}
									?>                   
								</select>
								</div>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Tipo de <br> Servicio :</span>
									<br>
									<select  name="Tipo_Servicio" style="width:250px" id="Tipo_Servicio" >
									<option value="">Todos</option>
									<?php 
									$resultados=Mostrar_Lista_Tipo_Servicio_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["IdTipoServicio"] ?>" <?php if($_REQUEST["idTipoServicio"]==$resultados[$i]["idTipoServicio"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
									<?php 
									}}
									?>                   
									</select>
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_IV">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_IV">Generar PDF</a>
								</div>								
						</div>		



						<!--------------V. Reporte Recaudacion por Cajero  ------------------->	

						<div title="Reporte Recaudacion por Cajero " data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte Recaudacion por Cajero</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_V" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_V" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_V">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_V">Generar PDF</a>
								</div>										
						</div>









						<!-------------- Reporte por Serie   ------------------->	

						<div title="Reporte Recaudacion por Serie " data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte Recaudacion por Serie</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_VI" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_VI" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_VI">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_VI">Generar PDF</a>
								</div>									
						</div>





						<!-------------- II. Reporte de Liquidaciones   ------------------->	

						<div title="Reporte de Liquidaciones" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de Liquidaciones</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_VII" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_VII" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_VII">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_VII"> Generar PDF</a>
								</div>	
						</div>



						<!-------------- Reporte por Devoluciones del Dia   ------------------->	

						<div title="Reporte por Devoluciones del Dia"   data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte por Devoluciones del Dia</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_VIII" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_VIII" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_VIII">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_VIII">Generar PDF</a>
								</div>											
						</div>	





						<!-------------- Reporte por Devoluciones   ------------------->	

						<div title="Reporte por Devoluciones " data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte por Devoluciones </strong></p>
								<hr>
								<br>
								<span style="font-weight:bold; color:#414141">Serie :</span>
								<br>
								<select  name="NroSerie" style="width:100px" id="NroSerie" >
								<option value="">Todos</option> 
									<?php 
									$resultados=Mostrar_Lista_Serie_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo substr($resultados[$i]["Codigo"], 1) ?>" <?php if($_REQUEST["Codigo"]==substr($resultados[$i]["Codigo"], 1)){?> selected <?php } ?> ><?php echo mb_strtoupper(substr($resultados[$i]["Codigo"], 1)) ?></option>
									<?php 
									}}
									?>                   
								</select>
								<br>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_IX" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_IX" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_IX">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_IX">Generar PDF</a>
								</div>									
						</div>





						<!-------------- Reporte de Pendiente de Pago   ------------------->	

						<div title="Reporte de Pendiente de Pago " data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de Pendiente de Pago </strong></p>
								<hr>
								<br>
								<span style="font-weight:bold; color:#414141">Cajero :</span>
								<br>
								<select  name="Cajeros" style="width:300px" id="Cajeros" >
								<option value="%">Todos</option> 
									<?php 
									$resultados=Mostrar_Lista_Cajeros_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["IdEmpleado"]; ?>" <?php if($_REQUEST["IdEmpleado"]==$resultados[$i]["IdEmpleado"]){?> selected <?php } ?> ><?php echo $resultados[$i]["ApellidoPaterno"].' '.$resultados[$i]["ApellidoMaterno"].' '.$resultados[$i]["Nombres"]; ?></option>
									<?php 
									}}
									?>                   
								</select>
								<br>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_X" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_X" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_X">Vista Previa</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="P_Generar_Pdf_X">Generar PDF</a>
								</div>									
						
						</div>
						




					</div>	
					</div>
				</div>
			</div>
		</div>
    </body>
    </html>



