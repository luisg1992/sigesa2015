<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/imprimir.css"/>
<title></title>
 
<style type="text/css">
.botonExcel{cursor:pointer;}

@media all {
   div.saltopagina{
      display: none;
   }
}
   
@media print{
   div.saltopagina{
      display:block;
      page-break-before:always;
   }
} 
</style>
<style type="text/css" media="print">
.nover {display:none}
</style>
</head>
<body  onLoad="window.print();">
<ul class="pro15 nover">
<li><a href="#nogo" onClick="window.print();" class="nover"><em class="home nover"></em><b>Imprimir</b></a></li>
 
<li><a href="../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ConsumoenelServicioAgregar&IdEmpleado=<?php echo $IdEmpleado; ?>"   class="nover" >
<em class="find nover"></em><b><< Nuevo </b></a>
</li>
<li><a href="../../MVC_Controlador/Facturacion/FacturacionC.php?acc=FacturacionBuscarCuentas&IdCuentaAtencion=<?php echo $IdCuentaAtencion; ?>&IdEmpleado=<?php echo $IdEmpleado; ?>"   class="nover" >
<em class="find nover"></em><b><< Mismo Paciente </b></a>

</li>
 


</ul>
 <?php
if($ListarFacturacionServicioPagos != NULL)	{ 
		       $i=0;
	foreach($ListarFacturacionServicioPagos as $item){
?> 

<table width="425" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <?php /*?><td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td><?php */?>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center"><FONT FACE="Calibri" size="+1">Nº Ord.&nbsp;&nbsp; <?php echo $idOrden; ?></FONT></td>
  </tr>
         
  <tr>
    <td colspan="5"><FONT FACE="Calibri" >H.C: <?php echo $NroHistoriaClinica;  ?>&nbsp;&nbsp; <?php echo $ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre; ?> - 
      <?php $TIPOEdad= Atencion_Edad_IdCuentaAtencion_C($IdCuentaAtencion); echo $TIPOEdad[0][0].' '.$TIPOEdad[0][1]; ?>
    </FONT></td>
  </tr>
  <tr>
    <td colspan="5"><FONT FACE="Calibri" >Procedencia: <?php echo $dservicio; ?>  </FONT></td>
  </tr>
  <tr>
    <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="14%"><FONT FACE="Calibri" size="2" >N° Cuen.</FONT></td>
        <td width="20%"><FONT FACE="Calibri" ><?php echo $IdCuentaAtencion; ?></FONT></td>
        <td width="15%"><FONT FACE="Calibri" size="2"><?php if($idFuenteFinanciamiento==1){?>N° OrPg.<?php }?></FONT></td>
        <td width="16%"><FONT FACE="Calibri" >  <?php echo $idOrdenPago; ?> <?php echo $item["dfinanciamiento"]; ?> </FONT></td>
        <td width="35%"><FONT FACE="Calibri" >F.R. <?php echo $FechaCreacion; ?></FONT></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td  >&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td width="4">&nbsp;</td>
    <td width="57"  ><FONT FACE="Calibri" size="2" >Codigo</FONT></td>
    <td width="242" align="center"><FONT FACE="Calibri" size="2"  >Descripcion</FONT></td>
    <td width="46" align="center"><FONT FACE="Calibri" size="2"  >Cant.</FONT></td>
    <td width="81" align="right"><FONT FACE="Calibri" size="2"  >Importe</FONT></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td rowspan="4"   valign="top"><FONT FACE="Calibri" ><?php echo $item["Codigo"]; ?></font></td>
    <td rowspan="4" valign="top"  ><FONT FACE="Calibri" size="2" ><?php echo $item["Nombre"]; ?></FONT></td>
    <td rowspan="4" align="center" valign="top"><FONT FACE="Calibri" ><?php echo $item["Cantidad"]; ?></FONT></td>
    <td rowspan="4" align="right" valign="top"><FONT FACE="Calibri" ><?php echo number_format($item["Total"], 2, '.', ' '); ?></FONT></td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font face="Calibri" >Termina:</font></td>
    <td><font face="Calibri" ><?php echo substr($NombrePC,0,10); ?></font></td>
    <td align="right"><font face="Calibri" >Monto</font></td>
    <td align="right"><font face="Calibri" >S/. <?php echo number_format($item["Total"], 2, '.', ' '); ?></font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font face="Calibri" >Usuario:</font></td>
    <td><font face="Calibri" > <?php echo  $Usuario;?></font></td>
    <td align="right">T.Pago</td>
    <td align="right"><font face="Calibri" >S/. <?php if($idFuenteFinanciamiento==1){?><?php echo number_format($item["Total"], 2, '.', ' '); ?><?php }else{?>0.00<?php }?></font></td>
  </tr>
  <tr>
    <td colspan="5" align="center"><FONT FACE="Calibri" size="+1">H.C:<?php echo $NroHistoriaClinica;  ?> &nbsp;&nbsp; <?php echo $ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre; ?></FONT> - <FONT FACE="Calibri" >
      <?php $TIPOEdad= Atencion_Edad_IdCuentaAtencion_C($IdCuentaAtencion); echo $TIPOEdad[0][0].' '.$TIPOEdad[0][1]; ?>
    </FONT></td>
  </tr>
</table>
<?php  
//if($i>1)  {
?>
<div class="saltopagina"></div>
<?php // }  ?>


<?php    }
	   } ?>