<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ordenes para analisis de laboratorio</title>

	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<style>
		html,body { 
        	height: 100%;
        	font-family: Helvetica; 

        }
	</style>

	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>

    <script>
    $(document).ready(function() {
    	//CARGANDO LAS CONFIGURACIONES POR DEFECTO
		$('#busqueda_numero_cuenta').textbox('clear').textbox('textbox').focus();
		$('#tabla_servicios').datagrid({
			rowStyler:function(index,row){
				if (row.NUMERO % 2 == 0){
					return 'background-color:#F2F2F2;';
				}
			}
		});

		//BUSCAR POR NUMERO DE CUENTA
		$("#busqueda_numero_cuenta").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numero_cuenta = $("#busqueda_numero_cuenta").textbox('getText');
				$.ajax({
					url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
					type: 'POST',
					dataType: 'json',
					data: {
						tipobusuqeda:1,
						dato:numero_cuenta
					},
					success: function(data){
						$('#tabla_resultados_xhc').datagrid('loadData', {"CUENTA":0,"rows":[]});
						$("#num_cuenta_d_paciente_mostrar").textbox('setText',numero_cuenta);
						$("#nombre_d_paciente_mostrar").textbox('setText',data.PACIENTE);
						$("#historia_d_paciente_mostrar").textbox('setText',data.HC);
						$("#fingreso_d_paciente_mostrar").textbox('setText',data.FECHAINGRESO);
						$("#fapertura_d_paciente_mostrar").textbox('setText',data.FAPERTURA);
						$("#estadocuenta_d_paciente_mostrar").textbox('setText',data.CUENTA);
						$("#faltamedica_d_paciente_mostrar").textbox('setText',data.FECHAALTAMEDICA);
						$("#fegreadmin_d_paciente_mostrar").textbox('setText',data.FEGRESOADMIN);
						$("#servicio_d_paciente_mostrar").textbox('setText',data.SERVICIO);
						$("#domicilio_d_paciente_mostrar").textbox('setText',data.DOMICILIO);
						$("#diagnostico_d_paciente_mostrar").textbox('setText',data.DIAGNOSTICO);

						$('#tabla_servicios').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ServiciosEstadoCuenta&dato='+numero_cuenta});
					}
				});
				
			}

		});

		$("#buqueda_numero_historia").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numero_cuenta = $("#buqueda_numero_historia").textbox('getText');
				$.ajax({
					url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
					type: 'POST',
					dataType: 'json',
					data: {
						tipobusuqeda:2,
						dato:numero_cuenta
					},
					success: function(resultado){
						$('#tabla_resultados_xhc').datagrid('loadData', {"CUENTA":0,"rows":[]});
						$("#tabla_resultados_xhc").datagrid({data:resultado});
					}
				});
				
			}

		});

		function ImprimirEstadoCuentaConsolidado()
		{
			var num_cuenta_d_paciente_mostrar 	= $("#num_cuenta_d_paciente_mostrar").textbox('getText');
			var nombre_d_paciente_mostrar 		= $("#nombre_d_paciente_mostrar").textbox('getText');
			var historia_d_paciente_mostrar 	= $("#historia_d_paciente_mostrar").textbox('getText');
			var fingreso_d_paciente_mostrar 	= $("#fingreso_d_paciente_mostrar").textbox('getText');
			var fapertura_d_paciente_mostrar 	= $("#fapertura_d_paciente_mostrar").textbox('getText');
			var estadocuenta_d_paciente_mostrar = $("#estadocuenta_d_paciente_mostrar").textbox('getText');
			var faltamedica_d_paciente_mostrar 	= $("#faltamedica_d_paciente_mostrar").textbox('getText');
			var idUsuarioparaSigesaLI 			= $("#idUsuarioparaSigesaLI").val();
			var newWin = window.open();
			$.ajax({
				url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ImprimirEstadoCuenta',
				type: 'POST',
				dataType: 'json',
				data: {
					num_cuenta_d_paciente_mostrar:num_cuenta_d_paciente_mostrar,
					nombre_d_paciente_mostrar:nombre_d_paciente_mostrar,
					historia_d_paciente_mostrar:historia_d_paciente_mostrar,
					fingreso_d_paciente_mostrar:fingreso_d_paciente_mostrar,
					fapertura_d_paciente_mostrar:fapertura_d_paciente_mostrar,
					estadocuenta_d_paciente_mostrar:estadocuenta_d_paciente_mostrar,
					faltamedica_d_paciente_mostrar:faltamedica_d_paciente_mostrar,
					idUsuarioparaSigesaLI  : idUsuarioparaSigesaLI
				},
				success:function(impresion)
				{
					newWin.document.write(impresion);
				    newWin.document.close();
				    newWin.focus();
				    newWin.print();
				    newWin.close();
				}
			});
			
		}


		//BOTON IMPRIMIR
		$("#imprimirResultadoConsolidado_btn").click(function(event) {
			
			ImprimirEstadoCuentaConsolidado();
		});

		//FIN
    });

	function cellStyler(value,row,index){
        return 'background-color:#ffee00;color:red;';
    }
    </script>
</head>
<body>
	<div class="easyui-panel"  style="width:100%;height:100%;" title="Estado de Cuenta del Paciente">
		<div class="easyui-layout" data-options="fit:true">
			<!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:true" style="height:25%;">
            	<div class="easyui-layout" data-options="fit:true">
            		<input type="hidden" id="idUsuarioparaSigesaLI" value="<?php echo $_REQUEST['IdEmpleado']?>">
					<!-- CONTENEDOR: BUSQUEDAS -->
            		<div data-options="region:'west'" title="" style="width:30%;height:100%;padding:1%;" >
            			<table>
            				<tr>
            					<td><u><strong>BUSQUEDAS:</strong></u></td>
            					<td></td>
            				</tr>
            				<tr>
            					<td>N° CUENTA</td>
            					<td><input type="text" class="easyui-textbox" id="busqueda_numero_cuenta" data-options="prompt:'N°CUENTA'"></td>
            				</tr>
            				<tr>
            					<td>N° H.C.</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N° H.C.'" id="buqueda_numero_historia"></td>
            				</tr>
            				<!--<tr>
            					<td>N° DNI</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N° DNI'"></td>
            				</tr>-->
            				
            				<!--<tr>
            					<td>N° ORD. PAGO SERV.</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N°ORD. PAGO SERV.'"></td>
            				</tr>
            				<tr>
            					<td>N° DCTO. EXO. FARM.</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N° DCTO. EXO. FARM.'"></td>
            				</tr>-->
            			</table>
            		</div>
					<!-- CUENTAS A SELECCIONAR -->
            		<div data-options="region:'center'" title="" style="width:70%;height:100%;padding:1%;" >
            			<u><strong>LISTA DE CUENTAS:</strong></u><br>
            			<table class="easyui-datagrid" style="width:95%;height:auto;" data-options="singleSelect:true,collapsible: true,
							onClickRow:function(row){	
								var row = $('#tabla_resultados_xhc').datagrid('getSelected');			    			
								$.ajax({
								url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
								type: 'POST',
								dataType: 'json',
								data: {
									tipobusuqeda:1,
									dato:row.CUENTA
								},
								success: function(data){
									$('#num_cuenta_d_paciente_mostrar').textbox('setText',row.CUENTA);
									$('#nombre_d_paciente_mostrar').textbox('setText',data.PACIENTE);
									$('#historia_d_paciente_mostrar').textbox('setText',data.HC);
									$('#fingreso_d_paciente_mostrar').textbox('setText',data.FECHAINGRESO);
									$('#fapertura_d_paciente_mostrar').textbox('setText',data.FAPERTURA);
									$('#estadocuenta_d_paciente_mostrar').textbox('setText',data.CUENTA);
									$('#faltamedica_d_paciente_mostrar').textbox('setText',data.FECHAALTAMEDICA);
									$('#fegreadmin_d_paciente_mostrar').textbox('setText',data.FEGRESOADMIN);
									$('#servicio_d_paciente_mostrar').textbox('setText',data.SERVICIO);
									$('#domicilio_d_paciente_mostrar').textbox('setText',data.DOMICILIO);
									$('#diagnostico_d_paciente_mostrar').textbox('setText',data.DIAGNOSTICO);
									$('#tabla_servicios').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ServiciosEstadoCuenta&dato='+row.CUENTA});
								}
							});
				 			}
            			" id="tabla_resultados_xhc">
				        <thead>
				            <tr>			                
				                <th data-options="field:'CUENTA',rezisable:true,align:'center', styler:cellStyler">CUENTA</th>
								<th data-options="field:'PACIENTE',rezisable:true,align:'center'">PACIENTE</th>	
								<th data-options="field:'FAPERTURA',rezisable:true,align:'center'">F. APERTURA</th>	
								<th data-options="field:'SERVICIO',rezisable:true,align:'left'">SERVICIO</th>	
								<th data-options="field:'FINANCIAMIENTO',rezisable:true,align:'center'">FUENTE FINAN.</th>								
				            </tr>
				        </thead>
				    </table>
            		</div>
            	</div>
            </div>


            <!-- DATOS PACIENTES-->
            <div data-options="region:'center',split:true" style="height:20%;padding:1%;">
            	<u><strong>DATOS PACIENTE:</strong></u>
            	<table style="width:100%;">
            		<tr>
            			<td>NOMBRE</td>
            			<td>
            				<input type="text" class="easyui-textbox" id="num_cuenta_d_paciente_mostrar" data-options="prompt:'N°CUENTA'" style="width:70px;">&nbsp;
            				<input type="text" class="easyui-textbox" id="nombre_d_paciente_mostrar" data-options="prompt:'PACIENTE'" style="width:290px;">&nbsp;
            				<input type="text" class="easyui-textbox" id="historia_d_paciente_mostrar" data-options="prompt:'N° H.C.'" style="width:70px;">
            			</td>
            			<td>F. INGRESO</td>
            			<td><input type="text" class="easyui-textbox" id="fingreso_d_paciente_mostrar" data-options="prompt:'FECHA INGRESO'"></td>
            			<td>F. APER. CTA.</td>
            			<td><input type="text" class="easyui-textbox" id="fapertura_d_paciente_mostrar" data-options="prompt:'FECHA APERTURA'"></td>
            		</tr>
            		<tr>
            			<td>CUENTA</td>
            			<td><input type="text" class="easyui-textbox" id="estadocuenta_d_paciente_mostrar" data-options="prompt:''" style="width:440px;color:red;"></td>
            			<td>F. ALTA MED.</td>
            			<td><input type="text" class="easyui-textbox" id="faltamedica_d_paciente_mostrar" data-options="prompt:'FECHA ALTA'"></td>
            			<td>F. EGRE. ADM.</td>
            			<td><input type="text" class="easyui-textbox" id="fegreadmin_d_paciente_mostrar" data-options="prompt:'FECHA EGRESO'"></td>
            		</tr>
            		<tr>
            			<td>SERV. EG.</td>
            			<td><input type="text" class="easyui-textbox" id="servicio_d_paciente_mostrar" data-options="prompt:'SERVICIO'" style="width:440px;"></td>
            			<td>DOMICILIO</td>
            			<td colspan="3"><input type="text" class="easyui-textbox" id="domicilio_d_paciente_mostrar" data-options="prompt:'DOMICILIO'" style="width:460px;"></td>
            			
            		</tr>
            		<tr>
            			<td>DX. EGR.</td>
            			<td><input type="text" class="easyui-textbox" id="diagnostico_d_paciente_mostrar" data-options="prompt:'DX. EGRE'" style="width:440px;"></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            		</tr>
            	</table>
            </div>

            <!-- DATOS CUERPO-->
            <div data-options="region:'south',split:true" style="height:55%;padding:1%;">
                <div class="easyui-tabs" style="width:100%;height:80%;">
			        <div title="Servicios" style="padding:10px">
			            <table class="easyui-datagrid" style="width:95%;height:300px;" data-options="collapsible: true,
							onClickRow:function(row){	
								var row = $('#tabla_servicios').datagrid('getSelected');			    			
								
								}
            			" id="tabla_servicios">
				        <thead>
				            <tr>
				            	<th data-options="field:'NUMERO',rezisable:true,hidden:true">F. Atencion</th>			                
				                <th data-options="field:'FATENCION',rezisable:true,align:'left'">F. Atencion</th>
								<th data-options="field:'PUNTOCARGA',rezisable:true,align:'left'">Puntos de Carga</th>	
								<th data-options="field:'CODIGO',rezisable:true,align:'center'">Codigo</th>	
								<th data-options="field:'DESCRIPCION',rezisable:true,align:'left'">Descripcion</th>	
								<th data-options="field:'CANTIDAD',rezisable:true,align:'center'">Cantidad</th>								
								<th data-options="field:'PRUNIT',rezisable:true,align:'center'">Pr. Unitario</th>
								<th data-options="field:'TOTALPAGAR',rezisable:true,align:'center'">Total</th>
				            </tr>
				        </thead>
				    </table>
			        </div>
			        <div title="Farmacia" style="padding:10px">
			            En desarrollo
			        </div>
			        <div title="Consolidado" style="padding:10px">
			            En desarrollo
			        </div>
			    </div><br>
			    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:96px" id="imprimirResultadoConsolidado_btn">Imprimir Cuenta</a>
			    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:96px" id="imprimirResultado_btn">Imprimir(F4)</a>
			    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:96px" id="imprimirResultado_btn">Imprimir(F4)</a>
            </div>
		</div>
	</div>
</body>
</html>