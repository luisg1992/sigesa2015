<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>REGISTRO DE TRIAJE</title>
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
    <style>
        html,body { 
            padding: 0px;
            margin: 0px;
            height: 100%;
            font-family: Helvetica; 
        }    
    </style>

        <!-- JS -->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-cellediting.js"></script>

        <!-- JS: OPERACIONES -->
        <script>
        $(document).ready(function() {

            //FUNCIONES
            function MostrarDataGeneral(nroCuenta, nroDni, histClinica, apPaterno, apMaterno, fecTriaje)   
            {
                var nroCuenta = $("#nroCuenta").textbox('getValue');
                var nroDni = $("#nroDni").textbox('getValue');
                var histClinica = $("#histClinica").textbox('getValue');
                var apPaterno = $("#apPaterno").textbox('getValue');
                var apMaterno = $("#apMaterno").textbox('getValue');
                var fecTriaje = $("#fecTriaje").datetimebox('getValue');

                $.ajax({
                    url:'../../MVC_Controlador/ConsultaExterna/ConsultaExternaC.php?acc=mostrarPacientes',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        nroCuenta:nroCuenta,
                        nroDni:nroDni,
                        histClinica:histClinica,
                        apPaterno:apPaterno,
                        apMaterno:apMaterno,
                        fecTriaje:fecTriaje
                    },
                    success:function(data)
                    {
                        $("#tabla_triaje").datagrid({
                            data:data
                        });
                        return false;
                    }
                });
            }

            //EVENTOS
            $("#consultar").click(function(event) {
                MostrarDataGeneral();
            });


        });
    </script>
</head>
<body>
     <div class="easyui-panel"  style="width:100%;height:120%;">

        <!-- CONTENEDORES -->
        <div class="easyui-layout" data-options="fit:true">
            
            <!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:false" style="height:18%;padding:2%;" title="TRIAJE">
                <div style="float:left;">
                    <table>
                        <td>
                            <td>NRO DE CUENTA:</td>
                            <td style="width:170px;">
                                <input class="easyui-textbox" data-options="valueField:'id',textField:'text'" style="width:150px;" id="nroCuenta">
                            </td>                                                                             
                        </td>
                        <td>
                            <td>DNI:</td>
                            <td style="width:170px;">
                                <input class="easyui-textbox" data-options="valueField:'id',textField:'text'" style="width:150px;" id="nroDni">
                            </td>                                                                             
                        </td>
                        <td>
                            <td>NRO DE HISTORIA:</td>
                            <td style="width:170px;">
                                <input class="easyui-textbox" data-options="valueField:'id',textField:'text'" style="width:150px;" id="histClinica">
                            </td>                                                                             
                        </td>
                        <td>
                            <td>APELLIDO PATERNO:</td>
                            <td style="width:170px;">
                                <input class="easyui-textbox" data-options="valueField:'id',textField:'text'" style="width:150px;" id="apPaterno">
                            </td>                                                                             
                        </td>
                        <td>
                            <td>APELLIDO MATERNO:</td>
                            <td style="width:170px;">
                                <input class="easyui-textbox" data-options="valueField:'id',textField:'text'" style="width:150px;" id="apMaterno">
                            </td>                                                                             
                        </td>
                        <td>
                            <td>FECHA DE PROGRAMACIÓN</td>
                            <td style="width:350px;"><input id="fecTriaje" class="easyui-datebox" style="width:150px;" value="<?php echo date('dd/mm')?>"></td>
                        </td>
                    </table>
                </div>

                <div style="float:left; padding:2%;">
                    <a class="easyui-linkbutton" id="consultar"><img src="../../MVC_Complemento/easyui/themes/icons/search.png" height="15" width="15" alt="">CONSULTAR</a>
                    <a class="easyui-linkbutton" id="limpiar"><img src="../../MVC_Complemento/easyui/themes/icons/clear.png" height="15" width="15" alt="">LIMPIAR</a>
                </div>
            </div>
            VISUALIZACIONES 
            <div data-options="region:'center',split:false" class="easyui-panel" style="height:82%;" title="RESULTADOS">
                <div style="float:left;width:45%;padding:1%;">
                 
                   <table id="tabla_triaje" title="LISTA DE PACIENTES" class="easyui-datagrid" style="width:1200px;" data-options="singleSelect:true,collapsible:true"> 
                        <thead>
                            <tr>
                              <th data-options="field:'NroCuenta',rezisable:true,width:100" >Nro Cuenta</th>       
                              <th data-options="field:'NroHistoria',rezisable:true,width:100">Nro Historia</th>
                              <th data-options="field:'ApellidoPaterno',rezisable:true,width:200">Ap. Paterno</th>
                              <th data-options="field:'ApellidoMaterno',rezisable:true,width:200" >Ap. Materno</th>
                              <th data-options="field:'PrimerNombre',rezisable:true,width:200" >1er Nombre</th>
                              <th data-options="field:'TriajeFecha',rezisable:true,width:100" >Fecha triaje</th>
                              <th data-options="field:'Consultorios',rezisable:true,width:200" >Consultorio</th>
                              <th data-options="field:'FechaCitas',rezisable:true,width:100" >Fecha cita</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
            </div>
</div>
</body>
</html>