<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Cupos adicionales</title>
	<!--CSS-->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">	    
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">              
	<!--Scripts-->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>         
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
    <style type="text/css">
	    html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }
        .mayus>input{
			text-transform: capitalize;
        }		

    	#tabCerrar,#tabHini {
    		float: left;
    		margin: 20px 30px 10px 30px;
    		border: 1px solid #D4D4D4;
    		border-collapse: collapse;
    		/*border-style: dotted;*/
    	}
    	#tabCerrar td,#tabHini td, #tabHfin td{
    		padding: 10px;

    	}
    	#tabHini th, #tabHfin th{
    		background-color: #F2F2F2;
			background: -webkit-linear-gradient(top,#ffffff 0,#F2F2F2 100%);
    		border-color: #D4D4D4;
    		border-style: solid;
    		border-width: 1px;
    		padding: 5px;
    		/*background: -moz-linear-gradient(white, #b3ccff); 
    		background: linear-gradient(white, #b3ccff);*/
    	}
    	#tabHfin {
    		float: left;
    		margin: 20px 0px 40px 0px;
    		border: 1px solid #D4D4D4;
    		border-collapse: collapse;
    		/*border-style: dotted;*/
    	}
		label{	
			font-weight:bold;
			font-size: 12px;
			font-weight: bold;
			color: #777;
		}
    </style>
    <!--
	<script src='http://icambron.github.com/timestack/files/moment.min.js'></script>
	<script src='http://icambron.github.com/timestack/files/timestack.min.js'></script>
	<link rel='stylesheet' type='text/css' href='http://icambron.github.com/timestack/files/timestack.css'>
    -->

    <script type="text/javascript">

    	$(document).ready((function(){

			$('#txtCantidadCuposI').on('input', function () { 
			 this.value = this.value.replace(/[^0-9]/g,'');
			});

    		//TIPO DE BUSQUEDA
    			var rutaControlador = "../../MVC_Controlador/General/GeneralC.php";

    			$('#vista2').hide();
    			$('#grid2').hide();

    			var cond = 1;


    			$('#r1').click(function(){
    				$('#vista1').show();
    				$('#vista2').hide();
    				$('#grid1').show();
    				$('#grid2').hide();

    				cond = 1;
    			});

    			$('#r2').click(function(){
    				$('#vista1').hide();
    				$('#vista2').show();
    				$('#grid1').hide();
    				$('#grid2').show();

    				cond = 2
    			});

    		//LLENAR COMBOBOX MEDICOS
    			$('#ComboMedicos').combobox({
			        url: rutaControlador+'?acc=ListarMedicos',
			        valueField:'IDMEDICO',
			        textField:'MEDICO',
			        onSelect: function(){
			        				var v = $('#ComboMedicos').combobox('getValue');
						            $('#txtIdMedico').textbox('setText',v);
						        }
			    });

    		//LLENAR COMBOBOX SERVICIOS
    		    $('#ComboServicios').combobox({
			        url: rutaControlador+'?acc=ListarServicio',
			        valueField:'IDSERVICIO',
			        textField:'SERVICIO',
			        onSelect: function(){
			        				var v = $('#ComboServicios').combobox('getValue');
						            $('#txtIdServicio').textbox('setText',v);
						        }
			    });

			//FORMATO FECHA
				function darFormato(date){
					var y = date.getFullYear();
		            var m = date.getMonth()+1;
		            var d = date.getDate();
		            return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
				}

				function convertirFecha(s){
					if (!s) return new Date();
			            var ss = (s.split('-'));
			            var y = parseInt(ss[0],10);
			            var m = parseInt(ss[1],10);
			            var d = parseInt(ss[2],10);
		            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
		                return new Date(y,m-1,d);
		            } else {
		                return new Date();
		            }
				}

				$('#dateProg').datebox({
					formatter: darFormato,
					parser: convertirFecha
				});

			//BUSQUEDA
				$("#Buscar").click(function(event) {
					var id=$('#txtIdServicio').textbox('getText')
					var fecha=$('#dateProg').datebox('getValue');

					if (id!="" && fecha!="") {
						$("#txtIdProgramacion").textbox('clear');
						$("#txtIdProgramacion2").textbox('clear');
					 	$("#txtHI").textbox('clear');
					 	$("#txtHF").textbox('clear');
					 	$('#txtNHI').textbox('clear');
					 	$('#txtNHF').textbox('clear');	
					 	$('#txtNHI').textbox('disable');
					 	$('#txtNHF').textbox('disable');	
				        BuscarProgramacion(id,fecha);
						BuscarFlujoProgramacion(id,fecha);
			    	}else{
			    		alert("Debe llenar todos los campos");
			    	}
			        //alert(id);
			    });

			    $("#Buscar2").click(function(event) {
					var id=$('#txtIdMedico').textbox('getText')
					var fecha=$('#dateProgMed').datebox('getValue');

					if (id!="" && fecha!="") {
						$("#txtIdProgramacion").textbox('clear');
						$("#txtIdProgramacion2").textbox('clear');
					 	$("#txtHI").textbox('clear');
					 	$("#txtHF").textbox('clear');
					 	$('#txtNHI').textbox('clear');
					 	$('#txtNHF').textbox('clear');	
					 	$('#txtNHI').textbox('disable');
					 	$('#txtNHF').textbox('disable');	
				        BuscarProgramacionxMedico(id,fecha);
						
			    	}else{
			    		alert("Debe llenar todos los campos");
			    	}
			        //alert(id);
			    });

			//SELECCIONAR REGISTRO
				$('#busqueda').datagrid({        
					    onDblClickRow:function(){                             
					        SeleccionarRegistro();                                     
					    }          
				});

				$('#busqueda2').datagrid({        
					    onDblClickRow:function(){                             
					        SeleccionarRegistro2();                                     
					    }          
				});


			//FUNCIONES
			
				function BuscarFlujoProgramacion(id,fecha){
			 		$.ajax({
			            url: rutaControlador+'?acc=GenerarFlujoProgramacion',                    
			            type: 'POST',
			            dataType: 'json',
			            data: {
			                id: id,
			                fecha: fecha 
			            },
			            success:function(dato){
							var objeto = JSON.parse(dato);
							$('#timeline').timestack({
								span: 'hour',
								data: objeto
							});	                           
			                return 1;
			            }
			        });

			 	}
			
			
			
			 	function BuscarProgramacion(id,fecha){
			 		$.ajax({
			            url: rutaControlador+'?acc=BuscarProgramacion',                    
			            type: 'POST',
			            dataType: 'json',
			            data: {
			                id: id,
			                fecha: fecha 
			            },
			            success:function(data){
			            if (data.dato == 'bad') {
			                $.messager.alert('Error','No se encontró ningun registro','Error');
			                $("#busqueda").datagrid('loadData',[]);
			                return 0;

			            }
			            else{         
			                $("#busqueda").datagrid({data:data});return false;                                  
			                }
			                return 1;
			            }
			        });

			 	}

			 	function BuscarProgramacionxMedico(id,fecha){
			 		$.ajax({
			            url: rutaControlador+'?acc=BuscarProgramacionxMedico',                    
			            type: 'POST',
			            dataType: 'json',
			            data: {
			                id: id,
			                fecha: fecha 
			            },
			            success:function(data){
			            if (data.dato == 'bad') {
			                $.messager.alert('Error','No se encontró ningun registro','Error');
			                $("#busqueda2").datagrid('loadData',[]);
			                return 0;

			            }
			            else{         
			                $("#busqueda2").datagrid({data:data});return false;                                  
			                }
			                return 1;
			            }
			        });

			 	}

			 	function SeleccionarRegistro(){
			 		var row = $('#busqueda').datagrid('getSelected'); 
			 		if (row) {
			 			$("#txtIdProgramacion").textbox('setValue',row.IDPROG);
			 			$("#txtIdProgramacion2").textbox('setValue',row.IDPROG);
						$("#txtIdProgramacion3").textbox('setValue',row.IDPROG);
			 			$("#txtHI").textbox('setValue',row.HI);
			 			$("#txtHF").textbox('setValue',row.HF);
						/*
						$('#txtNHI').textbox('enable');
						*/
			 			$('#txtNHF').textbox('enable');
			 			$('#txtNHI').textbox('setValue', row.HI);
			 			$('#txtNHF').textbox('setValue', row.HF);
						$('#txtTPROMEDIOI').textbox('setValue', row.TPROMEDIO);
						$('#txtTPROMEDIOF').textbox('setValue', row.TPROMEDIO);
						$('#txtCantidadCuposI').val('');
			 		}
			 		
			 	}

			 	function SeleccionarRegistro2(){
			 		var row = $('#busqueda2').datagrid('getSelected'); 
			 		if (row) {
			 			$("#txtIdProgramacion").textbox('setValue',row.IDPROG);
			 			$("#txtIdProgramacion2").textbox('setValue',row.IDPROG);
						$("#txtIdProgramacion3").textbox('setValue',row.IDPROG);
			 			$("#txtHI").textbox('setValue',row.HI);
			 			$("#txtHF").textbox('setValue',row.HF);
			 			$('#txtNHF').textbox('enable');
			 			$('#txtNHI').textbox('setValue', row.HI);
			 			$('#txtNHF').textbox('setValue', row.HF);
						$('#txtTPROMEDIOI').textbox('setValue', row.TPROMEDIO);
						$('#txtTPROMEDIOF').textbox('setValue', row.TPROMEDIO);
						$('#txtCantidadCuposI').val('');
			 		}
			 	}

			 	function validarHora(lahora){
			 		 
				        var arrHora = lahora.split(":");
				        if (arrHora.length!=2) {
				            return false; 
				        }
				        if (parseInt(arrHora[0])<0 || parseInt(arrHora[0])>23) {
				            return false; 
				        }

				        if (arrHora[1].length<2 || parseInt(arrHora[1])<0 || parseInt(arrHora[1])>59) {
				            return false; 
				        }

				        if (isNaN(arrHora[0]) || isNaN(arrHora[1])) {
				        	return false;
				        }

				        return true;
				        

				    
			 	}

			 	//EDTITAR HORA
			 	$("#editarHI").click(function(event) {
					var id = $('#txtIdProgramacion').textbox('getText');
					var nhi = $('#txtNHI').textbox('getText');
					var nhf = $('#txtNHF').textbox('getText');	
					var tiempoPromedio = $('#txtTPROMEDIOI').textbox('getText');	
					var cantidad_cupos = $("#txtCantidadCuposI").val();		
			        var v=validarHora(nhi);
			        if(v){
			        	if (nhi.length==4) {
			        		nhi = "0"+nhi;
			        	}
				        EditarHoraInicial(id,nhi);
				        EditarHoraFinal(id,nhf);
				        EditarHorarioCitas(id,nhi,tiempoPromedio,cantidad_cupos);
				        if(cond == 1){
					        var idss=$('#txtIdServicio').textbox('getText')
							var fechass=$('#dateProg').datebox('getValue');
							BuscarProgramacion(idss,fechass);
							BuscarFlujoProgramacion(idss,fechass);
						}else{
							var idss=$('#txtIdMedico').textbox('getText')
							var fechass=$('#dateProgMed').datebox('getValue');
							BuscarProgramacionxMedico(idss,fechass);
						}
						$.messager.alert('Sigesa','Hora de inicio actualizada');
				        $("#txtIdProgramacion").textbox('clear');
				        $("#txtIdProgramacion2").textbox('clear');
			 			$("#txtHI").textbox('clear');
			 			$("#txtHF").textbox('clear');
			 			$('#txtNHI').textbox('clear');
			 			$('#txtNHF').textbox('clear');
						$('#txtCantidadCuposI').val('');
			 			$('#txtNHI').textbox('disable');
			 			$('#txtNHF').textbox('disable');
			        }else{
			        	alert("Hora no valida");
			        }


			      
			    });

			    $("#editarHF").click(function(event) {
					var id = $('#txtIdProgramacion2').textbox('getText');
					var nhf = $('#txtNHF').textbox('getText');	
					var v=validarHora(nhf);
			        if(v){
			        	if (nhf.length==4) {
			        		nhf = "0"+nhf;
			        	}
				        EditarHoraFinal(id,nhf);

				        if (cond == 1) {
					        var idss=$('#txtIdServicio').textbox('getText')
							var fechass=$('#dateProg').datebox('getValue');
							BuscarProgramacion(idss,fechass);
						}else{
							var idss=$('#txtIdMedico').textbox('getText')
							var fechass=$('#dateProgMed').datebox('getValue');
							BuscarProgramacionxMedico(idss,fechass);
						}
						$.messager.alert('Sigesa','Hora final actualizada');
				        $("#txtIdProgramacion2").textbox('clear');
				        $("#txtIdProgramacion").textbox('clear');
			 			$("#txtHI").textbox('clear');
			 			$("#txtHF").textbox('clear');
			 			$('#txtNHI').textbox('clear');
			 			$('#txtNHF').textbox('clear');
						$('#txtCantidadCuposI').val('');
			 			$('#txtNHI').textbox('disable');
			 			$('#txtNHF').textbox('disable');
						$('#txtNHF').textbox('disable');



			        }else{
			        	$.messager.alert('Sigesa','Hora no valida');
			        }

			       	
			    });


			    $("#CerrarProg").click(function(event) {
					var IdProgramacion = $('#txtIdProgramacion3').textbox('getText');
					$.post("../../MVC_Controlador/General/GeneralC.php?acc=Retornar_HoraFin", 
					{ 
					IdProgramacion: IdProgramacion				
					}, 
					function(data){
					$.messager.alert('Sigesa','Se cierra la Programacion a las :'+data);
					$('#busqueda').datagrid('reload');  
					$('#busqueda2').datagrid('reload');  
					}); 
			    });
				
				
				
			 	function  EditarHoraInicial(id,nhi){
			 		$.ajax({
			            url: rutaControlador+'?acc=EditarHoraInicial',                    
			            type: 'POST',
			            dataType: 'json',
			            data: {
			                id: id,
			                nhi: nhi
			            }
			        });
			 	}

			 	function EditarHoraFinal(id,nhf){
			 		$.ajax({
			            url: rutaControlador+'?acc=EditarHoraFinal',                    
			            type: 'POST',
			            dataType: 'json',
			            data: {
			                id: id,
			                nhf: nhf 
			            }
			        });
			 	}


			 	function  EditarHorarioCitas(id,nhi,tiempoPromedio,cantidad_cupos){
			 		$.ajax({
			            url: rutaControlador+'?acc=EditarHorarioCitas',                    
			            type: 'POST',
			            dataType: 'json',
			            data: {
			                id: id,
			                nhi: nhi,
			                tiempoPromedio:tiempoPromedio,
			                cantidad_cupos:cantidad_cupos
			            }
			        });
			 	}


			    $("#txtCantidadCuposI").change(function () {
					var cantidad_cupos = $("#txtCantidadCuposI").val();
					var HoraInicio = $('#txtHI').textbox('getText');
					var HoraFinal = $('#txtHF').textbox('getText');
					var hora_I = HoraInicio.substring(0, 2);
					var minutos_I = HoraInicio.substr(3, 2);
					var hora_F = HoraFinal.substring(0, 2);
					var minutos_F = HoraFinal.substr(3, 2);
					var tiempo = $('#txtTPROMEDIOI').textbox('getText');
					/***** Modificar la Hora de Inicio   ****/				
					$.post("../../MVC_Controlador/General/GeneralC.php?acc=ColocarHoraInicio", 
					{ 
					hora: hora_I,
					minutos: minutos_I, 
					tiempo: tiempo, 
					cantidad_cupos: cantidad_cupos 					
					}, 
					function(data){
					$("#txtNHI").textbox('setValue',data.trim());
					});

					/***** Modificar la Hora de Inicio   ****/	
					$.post("../../MVC_Controlador/General/GeneralC.php?acc=ColocarHoraInicio", 
					{ 
					hora: hora_F,
					minutos: minutos_F, 
					tiempo: tiempo, 
					cantidad_cupos: cantidad_cupos 					
					}, 
					function(data){
					$("#txtNHF").textbox('setValue',data.trim());
					});             
				})	

    	}));

    </script>
	

</head>
<body>
	<!--PRINCIPAL -->
	<div class="easyui-layout" style="width:1650px;height:900px;">
		<!--NORTE-->
	    <div data-options="region:'north', title:'Buscar Programación'" style="height:15%;padding:1%;">
	    	<table>
	    		<tr>
		        	<td><input id="r1" type="radio" name="tipoBusqueda" value="con" checked="true">Buscar por consultorio</td>
		        	<td width="40px"></td>
		        	<td><input id="r2" type="radio" name="tipoBusqueda" value="med" >Buscar por médico</td>
	        	<tr>
	        </table>
	        <table id="vista1" style="padding:10px">
	        	<tr>
	        		
		        		<td><label for="ComboServicios">Servicio: <label/></td>
		        		<td><input id="ComboServicios" style="width: 350px" class="easyui-combobox"></td>
		        		<td width="40px"></td>
		        		<td>Id Servicio: </td>
		        		<td><input id="txtIdServicio" class="easyui-textbox" style="width:100px" disabled></td>
		        		<td width="40px"></td>
		        		<td>Fecha: </td>
		        		<td><input class="easyui-datebox" id="dateProg" required="required" style="width:100%;height:30px"></input></td>
		        		<td width="40px"></td>
		        		<td><a href="javascript:void(0)" id="Buscar" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px">Buscar</a></td>
	        		
	        	</tr>
	        </table>
	        <table id="vista2" style="padding:10px">
	        	<tr>
	        		<td><label for="ComboMedicos">Médico: <label/></td>
		        	<td><input id="ComboMedicos" style="width: 350px" class="easyui-combobox"></td>
		        	<td width="40px"></td>
		        	<td>Id Médico: </td>
		        	<td><input id="txtIdMedico" class="easyui-textbox" style="width:100px" disabled></td>
		        	<td width="40px"></td>
		        	<td>Fecha: </td>
		        	<td><input class="easyui-datebox" id="dateProgMed" required="required" style="width:100%;height:30px"></input></td>
		        	<td width="40px"></td>
		        	<td><a href="javascript:void(0)" id="Buscar2" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px">Buscar</a></td>
	        	</tr>
	        </table>
	    </div>

	    <!-- CENTRO -->
	    <div data-options="region:'center', title:'Cambio de Programación',iconCls:'icon-ok'" style="height:40%;">
					
					<div class="easyui-layout" style="width:100%;height:350px;">
					<div data-options="region:'west',split:true" title="1.- Hora de Inicio" style="width:30%">
					 <div class="easyui-tabs" style="width:100%;height:100%">
						<div title="1.1.- Adicionales">
								<table id="tabHini">
								<tr>
								<td><label>Id Programación: </label></td>
								<td width="20px"></td>
								<td><input id="txtIdProgramacion" class="easyui-textbox" style="width:100%" disabled></td>
								</tr>
								<tr>
								<td><label>Hora de inicio actual: </label></td>
								<td width="20px"></td>
								<td><input id="txtHI" class="easyui-textbox" style="width:50%" disabled></td>
								</tr>
								<tr>
								<td><label>Nueva hora de inicio: </label></td>
								<td width="20px"></td>
								<td><input id="txtNHI" class="easyui-textbox" style="width:50%;" disabled></td>	
								</tr>
								<tr>
								<td><label>Tiempo Promedio: </label></td>
								<td width="20px"></td>
								<td><input id="txtTPROMEDIOI" class="easyui-textbox" style="width:50%;" disabled></td>
								</tr>
								<tr>
								<td><label>Cantidad Cupos: </label></td>
								<td width="20px"></td>
								<td><input id="txtCantidadCuposI"  style="width:50%;" maxlength="2"></td>
								</tr>
								<tr>
								<td colspan="3" align="center"><div><a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="editarHI" href="javascript:void(0)" style="width:100px">Guardar</a></div></td>
								</tr>
								</table>
						</div>
						<div title="1.2.- Cerrar Programacion" style="padding:10px">
							<br>
							<table id="tabCerrar">
								<tr>
								<td><label>Id Programación: </label></td>
								<td width="20px"></td>
								<td><input id="txtIdProgramacion3" class="easyui-textbox" style="width:100%" disabled></td>
								</tr>
								<tr>
								<td colspan="3" align="center"><div><a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="CerrarProg" href="javascript:void(0)" style="width:100px">Guardar</a></div></td>
								</tr>
							</table>
						</div>
						
						<div title="1.3.- Cambiar Hora Final" style="padding:20px">
								<table id="tabHfin">
									<tr>
										<td><label>Id Programación: </label></td>
										<td width="20px"></td>
										<td><input id="txtIdProgramacion2" class="easyui-textbox" style="width:100%" disabled></td>
									</tr>
									<tr>
										<td><label>Hora final actual: </label></td>
										<td width="20px"></td>
										<td><input id="txtHF" class="easyui-textbox" style="width:100%" disabled></td>
									</tr>
									<tr>
										<td><label>Nueva hora final: </label></td>
										<td width="20px"></td>
										<td><input id="txtNHF" class="easyui-textbox" style="width:100%;" disabled></td>
									</tr>
									<tr>
										<td><label>Tiempo Promedio: </label></td>
										<td width="20px"></td>
										<td><input id="txtTPROMEDIOF" class="easyui-textbox" style="width:100%;" disabled></td>
									</tr>
									<tr>
										<td colspan="3" align="center"><div><a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="editarHF" href="javascript:void(0)" style="width:100px">Guardar</a></div></td>
									</tr>
								</table>
						</div>	
						
					 </div>
					</div>
					<div data-options="region:'center',title:'2.- Resultados'"   style="width:70%">
						<div id="grid1">
							<table class="easyui-datagrid" title="Resultados de Búsqueda por consultorio" style="width:100%;height:auto;" data-options="singleSelect:true,collapsible:false,pagination:true,rownumbers:true,fitColumns:true" id="busqueda">
								<thead>
									<tr>
										<!--<th data-options="field:'IDPROG',width:'25%'"><span style="font-weight:bold">Id Programación</th> --> 
										<th data-options="field:'MEDICO',width:'55%'"><span style="font-weight:bold">Medico</th>	
										<th data-options="field:'FECHAPROG',width:'15%'"><span style="font-weight:bold">Fecha</th>
										<th data-options="field:'HI',width:'15%'"><span style="font-weight:bold">Hora inicio</th>
										<th data-options="field:'HF',width:'15%'"><span style="font-weight:bold">Hora fin</th>
									</tr>
								</thead>
							</table>
						</div>

						<div id="grid2">
							<table class="easyui-datagrid" title="Resultados de Búsqueda por médico" style="width:100%;height:auto;" data-options="singleSelect:true,collapsible:false,pagination:true,rownumbers:true,fitColumns:true" id="busqueda2">
								<thead>
									<tr>
										<!--<th data-options="field:'IDPROG',width:'20%'"><span style="font-weight:bold">Id Programación </th>  -->     
										<th data-options="field:'CONSULTORIO',width:'20%'"><span style="font-weight:bold">Consultorio</th>                     
										<th data-options="field:'FECHAPROG',width:'20%'"><span style="font-weight:bold">Fecha </th>
										<th data-options="field:'HI',width:'20%'"><span style="font-weight:bold">Hora inicio </th>
										<th data-options="field:'HF',width:'20%'"><span style="font-weight:bold">Hora fin </th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					</div>
    	</div>

    	<!-- SUR -->
    	<div data-options="region:'south', title:'Flujos de Programación',iconCls:'icon-ok'" style="height:40%;padding:1%;">
			<div id='timeline' ></div>	
	    </div>

	</div>

</body>
</html>