<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Basic DataGrid - jQuery EasyUI Demo</title>
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/jquery-easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/jquery-easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/jquery-easyui/themes/color.css">

    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/jquery-easyui/demo/demo.css">
    <script type="text/javascript" src="../../MVC_Complemento/jquery-easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/jquery-easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/jquery-easyui/datagrid-detailview.js"></script>
</head>
<body>
	<div class="easyui-panel" title="DataGrid with searching box" icon="icon-save" style="width:680px;height:420px;" collapsible="true" minimizable="true" maximizable=true closable="true">
    <div class="easyui-layout" fit="true">
        <div region="north" border="false" style="border-bottom:1px solid #ddd;height:32px;padding:2px 5px;background:#fafafa;">
            <div style="float:left;">
                <a href="#" class="easyui-linkbutton" plain="true" icon="icon-add">Newly added</a>
            </div>
			<div class="datagrid-btn-separator"></div>
			 <div style="float:left;">
                <a href="#" class="easyui-linkbutton" plain="true" icon="icon-save">The editor</a>
            </div>
			<div class="datagrid-btn-separator"></div>
			 <div style="float:left;">
				<a href="#" class="easyui-linkbutton" plain="true" icon="icon-remove">Delete</a>
            </div>
            <div style="float:right;">
               <input class="easyui-searchbox" data-options="prompt:'Please Input Value',searcher:''" style="width:130px;vertical-align:middle;"></input>
			   <a href="#" class="easyui-linkbutton" plain="true">Advanced search</a>
            </div>
        </div>
        <div region="center" border="false">
            <table id="tt"></table>
        </div>
    </div>
</div>
<script>
$('#tt').datagrid({
    fit:true,
    border:false,
    url:'../datagrid/datagrid_data1.json',
    idField:'code',
    columns:[[
       {field:'itemid',width:80,title:'Item ID'}, 
	   {field:'productid',width:100,title:'Product'},
	   {field:'listprice',width:80,align:'right',title:'List Price'},
	   {field:'unitcost',width:80,align:'right',title:'Unit Cost'},
	   {field:'attr1',width:240,title:'Attribute'},
	   {field:'status',width:60,align:'center',title:'Status'}
    ]],
    pagination:true,
    rownumbers:true
});
</script>

</body>
</html>