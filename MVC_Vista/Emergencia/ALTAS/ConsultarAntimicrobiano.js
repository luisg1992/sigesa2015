	// Script que se ejecutaran al cargar la Pagina  
	$(document).ready(function(){				
			//Deshabilitar el boton de Guardar para la Vista Consultar
		    $('#Guardar_Cambios_Consultar').linkbutton({disabled:true});
		});


		// Script de Estilo para columna de resultado de Examenes   
		var products = [
		    {IdEstadoAntimicrobiano:'1',Descripcion:'Pendiente'},
		    {IdEstadoAntimicrobiano:'2',Descripcion:'Aprobado'},
		    {IdEstadoAntimicrobiano:'3',Descripcion:'Desaprobado'}
		];   
   
   
	 //Script Estilo para Resultados  	
	  function Resultado_Estilo(value,row,index){
			if (row.RESULTAD == 'NO'){
			return 'color:red;font-weight:bold';
			}
			else{
			return 'color:green;font-weight:bold';
			}
	  }	
	
	
	// Script de Estilo para columna de resultado de Examenes   
	function Estilo_Datagrid(value,row,index){
	return 'background-color:#F6E3CE;color:#8A0808;font-weight:bold !important';
	}		
	
	// Script de Estilo para columna   
	function Estilo_Columna(value,row,index){
	return 'background-color:#F6E3CE; color:red !important';
	}
	
	// Script de Estilo para columna   
	function Estilo_Basico(value,row,index){
	return 'background-color:white   !important;';
	}	
	
  //Funcion para cerrar el contenedor de Consultar
	function Consultar(IdCabeceraAntimicrobiano,IdCuentaAtencion,NroHistoriaClinica){		
	//var row = $('#Antimicrobianos').datagrid('getSelected');
	
	//parent.googlebox.hide();  
	//$('#Contenedor_Consultar').dialog('open');
	$('#Contenedor_Consultar').dialog('open').dialog('setTitle','Examenes de Laboratorio de la Cuenta: '+IdCuentaAtencion);	
	//Cargar_Datos_Personales_Consultar(IdCabeceraAntimicrobiano);//
	Cargar_Servicios_Visualizar(IdCuentaAtencion);//
	//AntimicrobianoPrevios_Visualizar(IdCabeceraAntimicrobiano);//
	//AntimicrobianoSolicitados_Visualizar(IdCabeceraAntimicrobiano);//
	//Cargar_Diagnosticos(IdCabeceraAntimicrobiano);
	//Cargar_Funciones_Vitales(IdCabeceraAntimicrobiano);
	}
	
	//Script para Cargar Datos Personales de Paciente  - Consultar 		
		/*function Cargar_Datos_Personales_Consultar(IdCabeceraAntimicrobiano){	
				$.post('../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php', {acc: 'MostrarDatosCabeceraAntimicrobiano',IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano}, function(htmlexterno){
				var Datos=JSON.parse(htmlexterno.replace("[", "").replace("]", ""));
				//Datos Personales de Paciente
				$('#IdCabecera').val(Datos['IdCabeceraAntimicrobiano']);
				$('.PrimerNombre').val(Datos['PrimerNombre']);
				$('.SegundoNombre').val(Datos['SegundoNombre']);
				$('.ApellidoPaterno').val(Datos['ApellidoPaterno']);
				$('.ApellidoMaterno').val(Datos['ApellidoMaterno']);
				$('.FechaNacimiento').val(Datos['FechaNacimiento']);
				$('.NroHistoriaClinica').val(Datos['NroHistoriaClinica']);
				$('.Edad').val(Datos['Edad']);
				if(Datos['IdTipoSexo']==1)
				{
				$("#Sexo_Masculino_Consultar").prop("checked", true);												
				}
				else
				{
				$("#Sexo_Femenino_Consultar").prop("checked", true);													
				}
				
				if(Datos['Infeccion_IntraHospitalaria']==0)
				{
				$("#Infeccion_NO_Consultar").prop("checked", true);			
				$("#Contenedor_Infeccion_Consultar").hide();
				}
				else
				{
				$("#Infeccion_SI_Consultar").prop("checked", true);		
				$("#Contenedor_Infeccion_Consultar").show();
				tinymce.get('Descripcion_IntraHospitalaria_Consultar').setContent(Datos['Descripcion_Intrahospitalaria']);				
				}
				//Datos de Atencion
				$('.FuenteFinanciamiento').val(Datos['Descripcion']);
				$('.IdCuentaAtencion').val(Datos['IdCuentaAtencion']);
				$('.FechaIngreso').val(Datos['FechaIngreso']);
				$('.HoraIngreso').val(Datos['HoraIngreso']);
				$('.Servicio').val(Datos['Nombre']);
				//Datos Adicionales
				$('.Cama').val(Datos['CamaPaciente']);
				//Datos Detalle
				var	Just = tinymce.get('Justificacion_Consultar').setContent(ValidarVacio(Datos['Justificacion']));
				var	Eva = tinymce.get('Evaluacion_Consultar').setContent(ValidarVacio(Datos['Evaluacion']));
				});
			}	*/
			
					
		//Script para cargar los Examenes de Antimicrobianos  							
		function Cargar_Servicios_Visualizar(IdCuentaAtencion)
		{								
		var dg =$('#Examenes_Antimicrobianos_Visualizar').datagrid({
		//iconCls:'icon-edit',
		singleSelect:true,
		rownumbers:true,
		pagination:false,
		//pageSize:10,
		url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=ExamenesAntimicrobianos&IdCuentaAtencion='+IdCuentaAtencion,
		columns:[[
		//{field:'IdCuentaAtencion',title:'N&#176; <br> Cuenta',width:80,styler:Estilo_Basico},
		{field:'ORDEN',title:'N&#176; <br> Orden',width:80,styler:Estilo_Basico},
		{field:'NUMERO',title:'N&#176;',width:40},
		{field:'CODIGO',title:'Codigo',width:60},
		{field:'NOMBRE',title:'Nombre',width:280},
		{field:'CANTIDAD',title:'Cant.',width:40},
		{field:'RESULTAD',title:'Resultado',width:70,sortable:true,align:'center',styler:Resultado_Estilo},
		{field:'FECHADESPACHO',title:'Fecha <br> Despacho',width:122}		
		]],
		onLoadSuccess: CombinarCeldas_Visualizar,
		onClickRow:function(row){
		var row = $('#Examenes_Antimicrobianos_Visualizar').datagrid('getSelected');            		
		if(row.RESULTAD == 'SI')
		{
		$('#Detalle_Orden').window('setTitle',row.CODIGO+' - '+row.NOMBRE);
		IngresarResultadosKK(row.ORDEN,row.IDPRODCPT);
		var idorden = row.ORDEN;
		var idproductocpt = row.IDPRODCPT;
		$.ajax({
		url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=datosCabecerasDetalle',
		type: 'POST',
		dataType: 'json',
		data: {
		idorden:idorden,
		idproductocpt:idproductocpt
		}
		});
		$('#Detalle_Orden').window('open');
		}
		else
		{
		$.messager.alert('SIGESA','No cuenta con Resultado');
		}
		},
		rowStyler: function(index,row){
		   if (row.TIPO ==1){
			 return 'font-weight:bold';
		  }
		}
		});
		}
		
		//Script para combinar celdas de la tabla de Examenes  
		function CombinarCeldas_Visualizar(data){
				var rows=$('#Examenes_Antimicrobianos_Visualizar').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var Posicion=row.POSICION;
				var Cantidad=row.ITEMS;
				if(row.NUMERO==1)
				{
				$(this).datagrid('mergeCells',{
				index: Posicion,
				field: 'ORDEN',
				rowspan: Cantidad
				});		
				}				
			}									
		}		
		
	    // Script para combinar celdas de la tabla de Examenes  
		function CombinarCeldas(data){
				var rows=$('#Examenes_Antimicrobianos').datagrid('getRows');
				for(i=0;i<rows.length;i++)
				{
				var row = rows[i];
				var Posicion=row.POSICION;
				var Cantidad=row.ITEMS;
				if(row.NUMERO==1)
				{
				$(this).datagrid('mergeCells',{
				index: Posicion,
				field: 'ORDEN',
				rowspan: Cantidad
				});		
				}				
			}									
		}
		
//Funcion para cargar los Antimicrobianos Previos del Paciente
/*function AntimicrobianoPrevios_Visualizar(IdCabeceraAntimicrobiano)
{
var dg =$('#PreviosAntimicrobianos_Visualizar').datagrid({
iconCls:'icon-edit',
singleSelect:true,
rownumbers:true,
pagination:true,
pageSize:10,
url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarPreviosAntimicrobiano&IdCabeceraAntimicrobiano='+IdCabeceraAntimicrobiano,
columns:[[
		{field:'Codigo',title:'Codigo',width:80},
		{field:'Nombre',title:'Nombre',width:450},
		{field:'Dias',title:'Dias',width:60}]]
});
}


//Funcion para cargar Antimicrobianos Solicitados para el Paciente
function AntimicrobianoSolicitados_Visualizar(IdCabeceraAntimicrobiano)
{
var dg =$('#DetalleAntimicrobianos_Visualizar').datagrid({
iconCls:'icon-edit',
singleSelect:true,
rownumbers:true,
pagination:true,
pageSize:10,
url:'../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php?acc=MostrarDetalleAntimicrobianos&IdCabeceraAntimicrobiano='+IdCabeceraAntimicrobiano,
rowStyler: function(index,row){
   if (row.IdEstadoAntimicrobiano ==1){
     return 'color:#ff8c1a;';
   }
   if (row.IdEstadoAntimicrobiano ==2){
     return 'color:#009900;';
   }
   if (row.IdEstadoAntimicrobiano ==3){
	return 'color:#ff0000;';
   }
},
columns:[[
		{field:'Codigo',title:'Codigo',width:50},
		{field:'Nombre',title:'Nombre',width:330},
		{field:'Dosis',title:'Dosis',width:60},
		{field:'Frecuencia',title:'Frecuencia',width:70},
		{field:'Dias',align:'center',title:'Dias',width:40,sortable:true,align:'center'},
		{field:'Total',align:'center',title:'Total',width:80},
		{field:'Descripcion',align:'center',title:'Estado',width:105},
		{field:'FechaCalificacion',align:'center',title:'Fecha',width:105}]]
});
}	*/	
		
		
		    //Script para Cargar Diagnosticos  de Paciente 
			/*function Cargar_Diagnosticos(IdCabeceraAntimicrobiano){
				$.post('../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php', {acc: 'MostrarDiagnosticosCabeceraAntimicrobiano',IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano}, function(htmlexterno){
				var Datos=JSON.parse(htmlexterno.replace("[", "").replace("]", ""));
				//Diagnosticos
				$('.dx_Antecedentes').val(Datos['DescripcionA']);
				$('.codigo_dx_Antecedentes').val(Datos['CodigoA']);
				$('.dx_infeccioso').val(Datos['DescripcionB']);
				$('.codigo_dx_infeccioso').val(Datos['CodigoB']);
				$('.dx_otro').val(Datos['DescripcionC']);
				$('.codigo_dx_otro').val(Datos['CodigoC']);
				});		
			}			
			
			
		   //Script para Cargar Funciones Vitales de Paciente 	
			function Cargar_Funciones_Vitales(IdCabeceraAntimicrobiano){
				$.post('../../MVC_Controlador/Antimicrobianos/AntimicrobianosC.php', {acc: 'MostrarFuncionesVitalesAntimicrobiano',IdCabeceraAntimicrobiano:IdCabeceraAntimicrobiano}, function(htmlexterno){
				var Datos=JSON.parse(htmlexterno.replace("[", "").replace("]", ""));
				//Funciones Vitales
				$('.FuncionV_FR').val(Datos['Valor_FR']);
				$('.FuncionV_FC').val(Datos['Valor_FC']);
				$('.FuncionV_PAS').val(Datos['Valor_PAS']);
				$('.FuncionV_PAD').val(Datos['Valor_PAD']);
				$('.FuncionV_Glasgon').val(Datos['Valor_Glasgon']);
				$('.FuncionV_Temperatura').val(Datos['Valor_Temperatura']);
				$('.FuncionV_Peso').val(Datos['Valor_Peso']);
				});
			}
			
			
	//Funcion para cerrar el contenedor de Consultar
	function Cerrar_Contenedor_Consultar()
	{
	$('#Contenedor_Consultar').dialog('close');
	}*/
	
	
	// Script para cargar los Resultados del Examen 	
		function IngresarResultadosKK(orden,iprod)
		{
			$.ajax({
                url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ResultadosDetallados',
                type: 'POST',
                dataType: 'json',
                data: {
                    idorden: orden,
                    idproductocpt:iprod
                },
                success:function(data)
                {   
                	$("#tabla_resultados_kk").html(' ');
                	var valor = 1;
                    var content = "<table><tr><th style='width:1%;'>Grupo</th><th style='width:15%;'>Item</th><th style='width:5%;'>Valor Numero</th><th style='width:35%;'>Valor Texto</th><th style='width:11%;'>Valor Combo</th><th style='width:11%;'>Valor Check</th><th style='width:11%;'>Valor Referencial</th><th style='width:11%;'>Metodo</th><th style='display:none;''>idproducto</th><th style='display:none;'>ordenxresultado</th> </tr><tbody>";
                    
                    $.each(data, function(index, val) {
                        
                        if(data[index].GRUPO == null){data[index].GRUPO = " ";}
                        if(data[index].ITEM == null){data[index].ITEM = " ";}
                        if(data[index].VALORNUMERO == null){data[index].VALORNUMERO = " ";}
                        if(data[index].VALORTEXTO == null){data[index].VALORTEXTO = " ";}
                        if(data[index].VALORCOMBO == null){data[index].VALORCOMBO = " ";}
                        if(data[index].VALORCHECK == null){data[index].VALORCHECK = " ";}
                        if(data[index].VALORREFERENCIAL == null){data[index].VALORREFERENCIAL = " ";}
                        if(data[index].METODO == null){data[index].METODO = " ";}
                        
                        content += '<tr>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].GRUPO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].ITEM+'" disabled/></td>';
                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLONUMERO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }

                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLOTEXTO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCOMBO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCHECK+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORREFERENCIAL+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].METODO+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].IDPRODCPT+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].ORDENXRESULTADO+'" disabled/></td>';
                        content += '</tr>';
                    }); 
                    content += '</tbody></table>';
                    $("#tabla_resultados_kk").append(content); 
                }
            });
		}			
	