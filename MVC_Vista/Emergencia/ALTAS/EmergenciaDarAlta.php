<head>
<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">

<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
	
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/demo.css">
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/tabs.css"> 
 
 <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.autocomplete.css" /> 
    
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/funciones2.js"></script> 
 
 
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery_enter.js"></script>
<!--  <script type="text/javascript" src="../../MVC_Complemento/js/jquery-autocomplete.js"></script>   -->
<script type='text/javascript' src="../../MVC_Complemento/js/jquery.autocomplete.js"></script> 


 <script type="text/javascript" src="../../MVC_Complemento/js/jsalert/jquery.alerts.js"></script>
 <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/js/jsalert/jquery.alerts.css"/>
	
  <!-- <script type="text/javascript" src="autocompletar.js"></script> -->	
	<script type="text/javascript" src="aw.js"></script>  
<style type="text/css">

#tblSample td, th { padding: 0.2em; }
.classy0 { background-color: #234567; color: #89abcd; }
.classy1 { background-color: #89abcd; color: #234567; }

#ocultarTexto {
  display: none;
}

 dt,  table, tbody, tfoot, thead, tr, th, td {
	margin:0;
	padding:0;
	/*border:0;*/
	font-weight:inherit;
	font-style:inherit;
	 
	font-family:inherit;
	vertical-align:baseline;
}


 
.cuerpo1 {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
color: #000;
 
}

form
{width: auto; background-color: #ffffff; }

textarea.Formulario {
padding: 5px;
border: 1px solid #D4D4D4;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
width:100%;
}

input.Formulario {
border: 1px solid #D4D4D4;
padding: 5px;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
}

.Combos {
     font: small-caption cursive ; 

     background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    width: 150px;
    /*float:left;*/
	
text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;

}

 
.texto {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;
  
    /*float:left;*/

}
.textodesable {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#ECECEC;
  
    /*float:left;*/

}


.buttons a, .buttons button{
    
    display:block;
    float:left;
    margin:0 17px 0 0;
    background-color:#f5f5f5;
    border:1px solid #dedede;
    border-top:1px solid #eee;
    border-left:1px solid #eee;

    font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
    font-size: 15px;
    line-height:130%;
    text-decoration:none;
    font-weight:bold;
    color:#565656;
    cursor:pointer;
    padding:5px 10px 6px 7px; /* Links */
}
.buttons button{
    width:auto;
    overflow:visible;
    padding:4px 10px 3px 7px; /* IE6 */
}
.buttons button[type]{
    padding:5px 10px 5px 7px; /* Firefox */
    line-height:17px; /* Safari */
}
*:first-child+html button[type]{
    padding:4px 10px 3px 7px; /* IE7 */
}
.buttons button img, .buttons a img{
    margin:0 3px -3px 0 !important;
    padding:0;
    border:none;
    width:16px;
    height:16px;
}

/* STANDARD */

button:hover, .buttons a:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

/* POSITIVE */

button.positive, .buttons a.positive{
    color:#529214;
}
.buttons a.positive:hover, button.positive:hover{
    background-color:#E6EFC2;
    border:1px solid #C6D880;
    color:#529214;
}
.buttons a.positive:active{
    background-color:#529214;
    border:1px solid #529214;
    color:#fff;
}

/* NEGATIVE */

.buttons a.negative, button.negative{
    color:#d12f19;
}
.buttons a.negative:hover, button.negative:hover{
    background:#fbe3e4;
    border:1px solid #fbc2c4;
    color:#d12f19;
}
.buttons a.negative:active{
    background-color:#d12f19;
    border:1px solid #d12f19;
    color:#fff;
}

/* REGULAR */

button.regular, .buttons a.regular{
    color:#336699;
}
.buttons a.regular:hover, button.regular:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a.regular:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

 /*-----*/
 fieldset { border:1px solid green }

.legend {
  border: 1px solid #BDD7FF;
width: 95%;
background: #F7F7F7;
padding: 3px;

/*  text-align:right;*/
  } 
   

</style>	
	
<script>
function openTabs(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
	
/*function listadoEstablecimiento(div,nombres){
	var recipiente = document.getElementById(div);
	var g_ajaxPagina = new AW.HTTP.Request;  
	g_ajaxPagina.setURL("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Listado_Establecimientos");
	g_ajaxPagina.setRequestMethod("POST");
	//g_ajaxPagina.setParameter("action", "BuscaPersona");
	g_ajaxPagina.setParameter("div", div);
	g_ajaxPagina.setParameter("nombres", nombres);
	g_ajaxPagina.response = function(xform){
		recipiente.innerHTML = xform;
	};
	g_ajaxPagina.request();
}

function buscarPersona(e,div){
  if(!e) e = window.event; 
    var keyc = e.keyCode || e.which;     
    
    if(keyc == 38 || keyc == 40 || keyc == 13) {
        autocompletar_teclado(div, 'tablaPersona', keyc);
        
    }else{
		if(div=='divregistrosPersona'){
			//si presiona retroceso o suprimir
			if(keyc == 8 || keyc == 46) {
				document.getElementById('txtIdPersona').value="";
			}
			listadoEstablecimiento(div,document.getElementById('txtPersona').value);
		}
  		eval(div+'.style.display="";');
		window.setTimeout(div+'.style.display="";', 300);
  }
}*/
function LlenarDatosEstablecimiento(IdEstablecimiento,Nombre,Codigo){		
	/*document.getElementById('txtIdPersona').value = IdEstablecimiento;
	document.getElementById('txtPersona').value = Nombre;
	divregistrosPersona.style.display="none";*/
	$(".ac_over").click(function (e) {
    	e.stopPropagation(); 
    	return false;
  	});
	tableEstablecimiento.style.display="none";
	$("#EstablecimientoDestino").val(Nombre);
	$("#IdEstablecimientoDestino").val(IdEstablecimiento);
	$("#CodigoEstablecimientoDestino").val(Codigo);
	
}


</script>

<style>
	#tableEstablecimiento {background:#FFF !important;}	
	#tableEstablecimiento tr:hover td{background:rgb(10, 36, 106) !important;color:#FFF; /*background:#93F6EE !important;*/}	
	.ac_results{
		width: auto !important;	
		list-style: none;
		padding: 0;
		margin: 0;		
	}
	.ac_results li{
		padding: 0px 0px;	
	}
	    
	#tablaEpisodioClinico tr:hover td{background:rgb(10, 36, 106) !important;color:#FFF;}
	
/*.ac_over {
    background-color: #11CFBF;    
}*/
</style>

<script type="text/javascript">
	
	function listadoEpisodioHistorico(div,nombres){
	var recipiente = document.getElementById(div);
	var g_ajaxPagina = new AW.HTTP.Request;  
	g_ajaxPagina.setURL("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Listado_EpisodioClinico&IdPaciente='<?php echo $IdPaciente ?>'");
	g_ajaxPagina.setRequestMethod("POST");
	g_ajaxPagina.setParameter("div", div);
	g_ajaxPagina.response = function(xform){
		recipiente.innerHTML = xform;
	};
	g_ajaxPagina.request();
}

 $().ready(function() {
		//$('#EpisodioHistorico').on('keypress', function (e) {
		$("#EpisodioHistorico").keydown(function (e) {							
			if (e.keyCode == 27) { //si presiona escape
				document.getElementById('EpisodioHistorico').value="";
				document.getElementById('lblEpisodioElegido').innerHTML="";		  
				borrartabla();
			}					 
		});
  });
function buscarEpisodioHistorico(e,div){ 
  if(!e) e = window.event; 
   var keyc = e.keyCode || e.which;

    if(keyc == 13) { //13 enter(LLAMA EL TECLADO SI ES QUE ES FUNCION keypress)
        //autocompletar_teclado(div, 'tablaEpisodioClinico', keyc);       		
    }else{
		if(div=='divEpisodioHistorico'){			
			listadoEpisodioHistorico(div,document.getElementById('EpisodioHistorico').value);
			document.getElementById("chkEpisodioNew").checked=false;
		}else{
			//alert('holaa2');
		}
  		eval(div+'.style.display="";');
		window.setTimeout(div+'.style.display="";', 300);
    }
}
	
	function LlenarDatosEpisodioidEpisodio(idEpisodio,nombres,FechaCierre){
		if(FechaCierre.trim()==""){
			document.getElementById('EpisodioHistorico').value=idEpisodio;
			document.getElementById('lblEpisodioElegido').innerHTML =nombres;
		}else{
			document.getElementById('EpisodioHistorico').value=idEpisodio;
			alert("Sólo podrá elegir EPISODIOS sin FECHA CIERRE.");
			document.getElementById('lblEpisodioElegido').innerHTML ='';
		}
		var recipiente = document.getElementById('divEpisodioHistorico');
		recipiente.style.display="none";
	}

	function borrartabla(){
		var recipiente = document.getElementById('divEpisodioHistorico');
		recipiente.style.display="none";
	}

$().ready(function() {
	
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();	
	//Buscar EstablecimientoDestino
	//var nombres=document.getElementById('EstablecimientoDestino').value;
	$("#EstablecimientoDestino").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Listado_Establecimientos", {
		width: 500, 
		matchContains: false,
		minLength: 2 /*,autoFocus: false,
		selectFirst: false */
		 
	  });
	
	/*$("#EstablecimientoDestino").result(function(event, data, formatted){
		//$("#EstablecimientoDestino").val(data[1]);
	 	//$("#IdEstablecimientoDestino").val(data[2]);
		//$("#CodigoEstablecimientoDestino").val(data[3]);		
	});*/
 	
	
	/*$("#EpisodioHistorico").click(function (e) {
	var recipiente = document.getElementById('EpisodioHistorico');
	var g_ajaxPagina = new AW.HTTP.Request;  
	g_ajaxPagina.setURL("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Listado_EpisodioClinico&IdPaciente='<?php echo $IdPaciente ?>'");
	g_ajaxPagina.setRequestMethod("POST");
	//g_ajaxPagina.setParameter("action", "BuscaPersona");
	g_ajaxPagina.setParameter("input", 'EpisodioHistorico');
	//g_ajaxPagina.setParameter("nombres", nombres);
	g_ajaxPagina.response = function(xform){
		recipiente.innerHTML = xform;
	};
	g_ajaxPagina.request();
    	//alert('hola');
						$.ajax({
							type: "POST",
							url: "../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Listado_EpisodioClinico&IdPaciente='<?php echo $IdPaciente ?>'",
							success: function(response)
							{
								$('#EpisodioHistorico').html(response).fadeIn();
							}
						});
	});*/

 
	
	//BuscarMedicoEgreso
	$("#BuscarMedicoEgreso").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Medicos&IdEspecialidad='<?php echo $IdEspecialidadServicioEgreso ?>'", {
		width: 500, 
		matchContains: true,
		minLength: 2	
		 
	  });
	
	$("#BuscarMedicoEgreso").result(function(event, data, formatted){
		$("#BuscarMedicoEgreso").val(data[1]);
	 	$("#IdMedicoEgreso").val(data[2]);
		$("#CodigoPlanillaMedicoEgreso").val(data[3]);
		//agregarDiagnosticoEgreso(); 
	});
	
	//DIAGNOSTICOS EGRESO
	$("#BuscarCodigoCIE10").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=1", {
		width: 500, 
		matchContains: true,			 
		//selectFirst: false	 
	  }); 	
	  
		
	$("#BuscarCodigoCIE10").result(function(event, data, formatted) {
		$("#BuscarCodigoCIE10").val(data[2]);
	 	$("#CodigoCIE10").val(data[2]);
		$("#descripcionCIE10").val(data[3]);
		$("#IdDiagnosticoEgreso").val(data[1]);
		agregarDiagnosticoEgreso(); 
	});
	
	/****Buscar Por Nombres **/
	 $("#descripcionCIE10").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=2", {
		width: 500, 
		matchContains: true,	
	 
	  });   
		
	$("#descripcionCIE10").result(function(event, data, formatted) {
		$("#BuscarCodigoCIE10").val(data[2]);
	 	$("#CodigoCIE10").val(data[2]);
		$("#descripcionCIE10").val(data[3]);
		$("#IdDiagnosticoEgreso").val(data[1]);
		agregarDiagnosticoEgreso();
	});	
	
	  //DIAGNOSTICOS COMPLICACIONES
  	  $("#BuscarCIE10").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=1", {
		width: 500, 
		matchContains: true,			 
		//selectFirst: false	 
	  }); 	
	  
		
	$("#BuscarCIE10").result(function(event, data, formatted) {
		$("#BuscarCIE10").val(data[2]);
	 	$("#CodigoCIE2004").val(data[2]);
		$("#descripcion").val(data[3]);
		$("#IdDiagnostico").val(data[1]);
		agregarCausaExterna(); 
	});
	
	/****Buscar Por Nombres **/
	 $("#descripcion").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=2", {
		width: 500, 
		matchContains: true,	
	 
	  });   
		
	$("#descripcion").result(function(event, data, formatted) {
		$("#BuscarCIE10").val(data[2]);
	 	$("#CodigoCIE2004").val(data[2]);
		$("#descripcion").val(data[3]);
		$("#IdDiagnostico").val(data[1]);
		agregarCausaExterna();
	});
	
	//DIAGNOSTICOS MORTALIDAD
	$("#BuscarCodigoCIE10Mort").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=1", {
		width: 500, 
		matchContains: true,			 
		//selectFirst: false	 
	  }); 	
	  
		
	$("#BuscarCodigoCIE10Mort").result(function(event, data, formatted) {
		$("#BuscarCodigoCIE10Mort").val(data[2]);
	 	$("#CodigoCIE10Mort").val(data[2]);
		$("#descripcionCIE10Mort").val(data[3]);
		$("#IdDiagnosticoEgresoMort").val(data[1]);
		agregarDiagnosticoMortalidad(); 
	});
	
	/****Buscar Por Nombres **/
	 $("#descripcionCIE10Mort").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=2", {
		width: 500, 
		matchContains: true,	
	 
	  });   
		
	$("#descripcionCIE10Mort").result(function(event, data, formatted) {
		$("#BuscarCodigoCIE10Mort").val(data[2]);
	 	$("#CodigoCIE10Mort").val(data[2]);
		$("#descripcionCIE10Mort").val(data[3]);
		$("#IdDiagnosticoEgresoMort").val(data[1]);
		agregarDiagnosticoMortalidad();
	});
	
	//DIAGNOSTICOS MORTALIDAD FETAL
	$("#BuscarCodigoCIE10MortFetal").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=1", {
		width: 500, 
		matchContains: true,			 
		//selectFirst: false	 
	  }); 	
	  
		
	$("#BuscarCodigoCIE10MortFetal").result(function(event, data, formatted) {
		$("#BuscarCodigoCIE10MortFetal").val(data[2]);
	 	$("#CodigoCIE10MortFetal").val(data[2]);
		$("#descripcionCIE10MortFetal").val(data[3]);
		$("#IdDiagnosticoEgresoMortFetal").val(data[1]);
		agregarDiagnosticoMortalidadFetal(); 
	});
	
	/****Buscar Por Nombres **/
	 $("#descripcionCIE10MortFetal").autocomplete("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Diagnosticos&TipoBusqueda=2", {
		width: 500, 
		matchContains: true,	
	 
	  });   
		
	$("#descripcionCIE10MortFetal").result(function(event, data, formatted) {
		$("#BuscarCodigoCIE10MortFetal").val(data[2]);
	 	$("#CodigoCIE10MortFetal").val(data[2]);
		$("#descripcionCIE10MortFetal").val(data[3]);
		$("#IdDiagnosticoEgresoMortFetal").val(data[1]);
		agregarDiagnosticoMortalidadFetal();
	});
	
 
});
 
</script>  
	
<?php 	
/***************LISTADOS DE DIAGNOSTICOS REGISTRADOS*************/
	//diagnosticos atención Egreso 3
	$ListaDiagnostico_idatencionEgreso=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'3');
						
    //diagnosticos atención MORTALIDAD 4
	$ListaDiagnostico_idatencionMortalidad=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'4');
						
	//diagnosticos atención NACIMIENTO 5
	$ListaDiagnostico_idatencionNacimiento=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'5');
						
    //diagnosticos atención COMPLICACIONES 6
	$ListaDiagnostico_idatencionComplicaciones=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'6');
	
	
?> 

<script language="javascript" type="text/javascript"> 	 

//var posicionCampoJ=<?php //echo $CanFilasJ;?>;	
var posicionCampoJ=1;	
function agregarDiagnosticoEgreso(){//POR MAHALI
	 
	 var IdSubclasificacionDx=document.getElementById('IdSubclasificacionDx').value;		
	
	//var SubclasificacionDx = $('#IdSubclasificacionDx').text();
	var comboselec = document.getElementById("IdSubclasificacionDx");
    var SubclasificacionDx = comboselec.options[comboselec.selectedIndex].text;	
	
	var CodigoCIE10=document.getElementById('CodigoCIE10').value;
	var descripcionCIE10=document.getElementById('descripcionCIE10').value;
	var IdDiagnosticoEgreso=document.getElementById('IdDiagnosticoEgreso').value;
	
		//VALIDAR QUE EL DIAGNOSTICO DE EGRESO NO SE REPITA
	    /*var theTable = document.getElementById('tablaDiagnosticosEgreso');
		cantFilas = theTable.rows.length;
		if(cantFilas>3){//a partir de cantFilas==4 la tabla esta llena
			for(b=1;b<(cantFilas-1);b++){
				if(IdDiagnosticoEgreso==document.getElementById("IdDiagnosticoEgreso"+b).value){					
					mensje = "El diagnóstico ya fue agregado al listado.";
					alert(mensje);
					return 0;
				}				
			}
		}//end if*/
	
		var maxJ=document.getElementById('contadorJ').value;
		for(b=1;b<=maxJ;b++){
			if(IdDiagnosticoEgreso==document.getElementById("IdDiagnosticoEgreso"+b).value){					
				mensje = "El diagnóstico ya fue agregado al listado.";
				alert(mensje);
				return 0;
			}				
		}
		//FIN VALIDAR QUE EL DIAGNOSTICO DE EGRESO NO SE REPITA
	
	if(IdDiagnosticoEgreso!=""){	 
	
	posicionCampoJ=parseInt(document.getElementById('contadorJ').value)+1;
	document.getElementById('contadorJ').value=posicionCampoJ;	
		
	nuevaFila = document.getElementById("tablaDiagnosticosEgreso").insertRow(-1);
	nuevaFila.id='Egr'+posicionCampoJ;

	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+SubclasificacionDx+"<input name='IdSubclasificacionDx"+posicionCampoJ+"' type='hidden' id='IdSubclasificacionDx"+posicionCampoJ+"'  value="+IdSubclasificacionDx+" size='20'></td>";	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input name='BuscarCodigoCIE10"+posicionCampoJ+"' type='text' disabled id='BuscarCodigoCIE10"+posicionCampoJ+"'  value="+CodigoCIE10+" size='20'></td>";
		
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='648' align='left'>"+descripcionCIE10+" <input type='hidden' name='IdDiagnosticoEgreso"+posicionCampoJ+"' id='IdDiagnosticoEgreso"+posicionCampoJ+"' value="+IdDiagnosticoEgreso+" ><input type='hidden' name='IdAtencionDiagnosticoEgreso"+posicionCampoJ+"' id='IdAtencionDiagnosticoEgreso"+posicionCampoJ+"'></td>";
		
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='58' align='left'><img src='../../MVC_Complemento/img/user_logout.png' width='16' height='16' onclick='eliminarRowDiagnosticosEgreso(this)'></td>";	
	
	posicionCampoJ++;	
		
	document.getElementById('BuscarCodigoCIE10').value="";
	document.getElementById('CodigoCIE10').value="";
	document.getElementById('descripcionCIE10').value="";
	document.getElementById('IdDiagnosticoEgreso').value="";
    document.getElementById('BuscarCodigoCIE10').focus();
	}else{
		alert("Ingrese un Diagnostico Valido.");
		document.getElementById('BuscarCodigoCIE10').focus();		
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
	 
//var posicionCampoCE=<?php //echo $CanFilasCE;?>;
var posicionCampoCE=1;	
function agregarCausaExterna(){	
	var CodigoCIE2004=document.getElementById('CodigoCIE2004').value;
	var descripcion=document.getElementById('descripcion').value;
	var IdDiagnostico=document.getElementById('IdDiagnostico').value;
	
	//VALIDAR QUE EL DIAGNOSTICO DE COMPLICACIONES NO SE REPITA	    
		var maxCE=document.getElementById('contadorCE').value;
		for(b=1;b<=maxCE;b++){
			if(IdDiagnostico==document.getElementById("IdDiagnostico"+b).value){					
				mensje = "El diagnóstico ya fue agregado al listado.";
				alert(mensje);
				return 0;
			}				
		}
		//FIN VALIDAR QUE EL DIAGNOSTICO DE COMPLICACIONES NO SE REPITA	
	
	if(IdDiagnostico!=""){	 
		
	posicionCampoCE=parseInt(document.getElementById('contadorCE').value)+1;
	document.getElementById('contadorCE').value=posicionCampoCE;	
		
	nuevaFila = document.getElementById("tablaCausaExterna").insertRow(-1);
	nuevaFila.id='Com'+posicionCampoCE;
	
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input name='BuscarCIE10"+posicionCampoCE+"' type='text' disabled id='BuscarCIE10"+posicionCampoCE+"'  value="+CodigoCIE2004+" size='20'></td>";
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='648' align='left'>"+descripcion+" <input type='hidden' name='IdDiagnostico"+posicionCampoCE+"' id='IdDiagnostico"+posicionCampoCE+"' value="+IdDiagnostico+" ><input type='hidden' name='IdAtencionDiagnostico"+posicionCampoCE+"' id='IdAtencionDiagnostico"+posicionCampoCE+"'></td>";
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='58' align='left'><img src='../../MVC_Complemento/img/user_logout.png' width='16' height='16' onclick='eliminarRowDiagnosticosComplicaciones(this)'></td>";
	
	
	 posicionCampoCE++;
	 
	document.getElementById('BuscarCIE10').value="";
	document.getElementById('CodigoCIE2004').value="";
	document.getElementById('descripcion').value="";
	document.getElementById('IdDiagnostico').value="";
    document.getElementById('BuscarCIE10').focus();
	}else{
		alert("Ingrese un Diagnostico Valido.");
		document.getElementById('BuscarCIE10').focus();
		
		}
    }
	
/////////////////////////////////////////////////////////////////////////////////////////////
	
//var posicionCampoK=<?php //echo $CanFilasCE;?>;	
var posicionCampoK=1;	
function agregarDiagnosticoMortalidad(){//POR MAHALI
	 
	 var IdSubclasificacionDxMort=document.getElementById('IdSubclasificacionDxMort').value;		
	
	//var SubclasificacionDxMort = $('#IdSubclasificacionDxMort').text();
	var comboselec = document.getElementById("IdSubclasificacionDxMort");
    var SubclasificacionDxMort = comboselec.options[comboselec.selectedIndex].text;	
	
	var CodigoCIE10Mort=document.getElementById('CodigoCIE10Mort').value;
	var descripcionCIE10Mort=document.getElementById('descripcionCIE10Mort').value;
	var IdDiagnosticoEgresoMort=document.getElementById('IdDiagnosticoEgresoMort').value;
	
		//VALIDAR QUE EL DIAGNOSTICO DE EGRESO NO SE REPITA
	    /*var theTable = document.getElementById('tablaDiagnosticosEgresoMort');
		cantFilas = theTable.rows.length;
		if(cantFilas>3){//a partir de cantFilas==4 la tabla esta llena
			for(b=1;b<(cantFilas-2);b++){
				if(IdDiagnosticoEgresoMort==document.getElementById("IdDiagnosticoEgresoMort"+b).value){				
					mensje = "El diagnóstico ya fue agregado al listado.";
					alert(mensje);
					return 0;
				}				
			}
		}//end if*/
		var maxK=document.getElementById('contadorK').value;
		for(b=1;b<=maxK;b++){
			if(IdDiagnosticoEgresoMort==document.getElementById("IdDiagnosticoEgresoMort"+b).value){					
				mensje = "El diagnóstico ya fue agregado al listado.";
				alert(mensje);
				return 0;
			}				
		}
		//FIN VALIDAR QUE EL DIAGNOSTICO DE EGRESO NO SE REPITA	
	
	if(IdDiagnosticoEgresoMort!=""){
		
	posicionCampoK=parseInt(document.getElementById('contadorK').value)+1;
	document.getElementById('contadorK').value=posicionCampoK;
		
	nuevaFila = document.getElementById("tablaDiagnosticosEgresoMort").insertRow(-1);
	nuevaFila.id='Mor'+posicionCampoK;

	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+SubclasificacionDxMort+"<input name='IdSubclasificacionDxMort"+posicionCampoK+"' type='hidden' id='IdSubclasificacionDxMort"+posicionCampoK+"'  value="+IdSubclasificacionDxMort+" size='20'></td>";	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input name='BuscarCodigoCIE10Mort"+posicionCampoK+"' type='text' disabled id='BuscarCodigoCIE10Mort"+posicionCampoK+"'  value="+CodigoCIE10Mort+" size='20'></td>";
		
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='648' align='left'>"+descripcionCIE10Mort+" <input type='hidden' name='IdDiagnosticoEgresoMort"+posicionCampoK+"' id='IdDiagnosticoEgresoMort"+posicionCampoK+"' value="+IdDiagnosticoEgresoMort+" ><input type='hidden' name='IdAtencionDiagnosticoEgresoMort"+posicionCampoK+"' id='IdAtencionDiagnosticoEgresoMort"+posicionCampoK+"'></td>";
		
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='58' align='left'><img src='../../MVC_Complemento/img/user_logout.png' width='16' height='16' onclick='eliminarRowDiagnosticosMort(this)'></td>";	
	
	posicionCampoK++;	
		
	document.getElementById('BuscarCodigoCIE10Mort').value="";
	document.getElementById('CodigoCIE10Mort').value="";
	document.getElementById('descripcionCIE10Mort').value="";
	document.getElementById('IdDiagnosticoEgresoMort').value="";
    document.getElementById('BuscarCodigoCIE10Mort').focus();
	}else{
		alert("Ingrese un Diagnostico Valido.");
		document.getElementById('BuscarCodigoCIE10Mort').focus();		
	}
}
	
/////////////////////////////////////////////////////////////////////////////////////////////
	
var posicionCampoN=1;	
function agregarDiagnosticoMortalidadFetal(){//POR MAHALI
	 
	 var IdSubclasificacionDxMortFetal=document.getElementById('IdSubclasificacionDxMortFetal').value;	
	
	var comboselec = document.getElementById("IdSubclasificacionDxMortFetal");
    var SubclasificacionDxMortFetal = comboselec.options[comboselec.selectedIndex].text;	
	
	var CodigoCIE10Mort=document.getElementById('CodigoCIE10MortFetal').value;
	var descripcionCIE10Mort=document.getElementById('descripcionCIE10MortFetal').value;
	var IdDiagnosticoEgresoMort=document.getElementById('IdDiagnosticoEgresoMortFetal').value;
	
		//VALIDAR QUE EL DIAGNOSTICO DE EGRESO NO SE REPITA
	    /*var theTable = document.getElementById('tablaDiagnosticosEgresoMortFetal');
		cantFilas = theTable.rows.length;
		if(cantFilas>3){//a partir de cantFilas==4 la tabla esta llena
			for(b=1;b<(cantFilas-2);b++){
				if(IdDiagnosticoEgresoMort==document.getElementById("IdDiagnosticoEgresoMortFetal"+b).value){				
					mensje = "El diagnóstico ya fue agregado al listado.";
					alert(mensje);
					return 0;
				}				
			}
		}//end if*/
		var maxN=document.getElementById('contadorN').value;		
		for(b=1;b<=maxN;b++){
			if(IdDiagnosticoEgresoMort==document.getElementById("IdDiagnosticoEgresoMortFetal"+b).value){					
				mensje = "El diagnóstico ya fue agregado al listado.";
				alert(mensje);
				return 0;
			}				
		}
		//FIN VALIDAR QUE EL DIAGNOSTICO DE EGRESO NO SE REPITA	
	
	if(IdDiagnosticoEgresoMort!=""){	 
	
	posicionCampoN=parseInt(document.getElementById('contadorN').value)+1;
	document.getElementById('contadorN').value=posicionCampoN;
		
	nuevaFila = document.getElementById("tablaDiagnosticosEgresoMortFetal").insertRow(-1);
	nuevaFila.id='Nac'+posicionCampoN;

	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+SubclasificacionDxMortFetal+"<input name='IdSubclasificacionDxMortFetal"+posicionCampoN+"' type='hidden' id='IdSubclasificacionDxMortFetal"+posicionCampoN+"'  value="+IdSubclasificacionDxMortFetal+" size='20'></td>";	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input name='BuscarCodigoCIE10MortFetal"+posicionCampoN+"' type='text' disabled id='BuscarCodigoCIE10MortFetal"+posicionCampoN+"'  value="+CodigoCIE10Mort+" size='20'></td>";
		
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='648' align='left'>"+descripcionCIE10Mort+" <input type='hidden' name='IdDiagnosticoEgresoMortFetal"+posicionCampoN+"' id='IdDiagnosticoEgresoMortFetal"+posicionCampoN+"' value="+IdDiagnosticoEgresoMort+" ><input type='hidden' name='IdAtencionDiagnosticoEgresoMortFetal"+posicionCampoN+"' id='IdAtencionDiagnosticoEgresoMortFetal"+posicionCampoN+"'></td>";
		
    nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='58' align='left'><img src='../../MVC_Complemento/img/user_logout.png' width='16' height='16' onclick='eliminarRowDiagnosticosMortFetal(this)'></td>";	//eliminarUsuario(this)
	
	posicionCampoN++;	
		
	document.getElementById('BuscarCodigoCIE10MortFetal').value="";
	document.getElementById('CodigoCIE10MortFetal').value="";
	document.getElementById('descripcionCIE10MortFetal').value="";
	document.getElementById('IdDiagnosticoEgresoMortFetal').value="";
    document.getElementById('BuscarCodigoCIE10MortFetal').focus();
	}else{
		alert("Ingrese un Diagnostico Valido.");
		document.getElementById('BuscarCodigoCIE10MortFetal').focus();		
	}
}	
	
/*FUNONES PARA DIAGNOSTICOS ENFERMERIA */
	function eliminarUsuario(obj){
		var oTr = obj;
		while(oTr.nodeName.toLowerCase()!='tr'){
		oTr=oTr.parentNode;
		}
		var root = oTr.parentNode;
		root.removeChild(oTr);		
    }	 

    function eliminarRowDiagnosticosEgreso(obj){
		var oTr = obj;
		while(oTr.nodeName.toLowerCase()!='tr'){
			oTr=oTr.parentNode;
		}		
		var root = oTr.parentNode;	
		var rIndex = oTr.sectionRowIndex;
		var indexDetalleTable=parseInt(rIndex)+1;		
		
		alert("Registro "+(indexDetalleTable-2)+" Eliminado"); //si elimino fila 2 obtiene 2, /si elimino fila 3 obtiene 3
		
		//reordenar	
		var theTable = document.getElementById('tablaDiagnosticosEgreso');
		cantFilas = theTable.rows.length;
		if(cantFilas!=3){
			reorderRowsDiagnosticosEgreso(indexDetalleTable);	
		}else{
			//alert('no ordenar');			
		}
		
		root.removeChild(oTr);
		//disminuir 1 al maximo numero de item
		var contadorJ=document.getElementById('contadorJ').value;
		document.getElementById('contadorJ').value=contadorJ-1;		
    }
	
	function reorderRowsDiagnosticosEgreso(startingIndex)  //startingIndex=2,3,4 
	{
		var theTable = document.getElementById('tablaDiagnosticosEgreso');
		cantFilas = (theTable.rows.length)-1;	
		var count = startingIndex-1 ; //1,2,3,4	
		for (var k=count; k<cantFilas; k++) {
			//alert(k+'cantFilas'+cantFilas);//$("#"+ k).html(k-1);
			$("#Egr"+ k).attr("id","Egr"+(k-1));	

			document.getElementById("IdSubclasificacionDx"+ k).name="IdSubclasificacionDx"+ (k-1);	
			$("#IdSubclasificacionDx"+ k).attr("id","IdSubclasificacionDx"+(k-1));

			document.getElementById("BuscarCodigoCIE10"+ k).name="BuscarCodigoCIE10"+ (k-1);	
			$("#BuscarCodigoCIE10"+ k).attr("id","BuscarCodigoCIE10"+(k-1));

			document.getElementById("IdDiagnosticoEgreso"+ k).name="IdDiagnosticoEgreso"+ (k-1);	
			$("#IdDiagnosticoEgreso"+ k).attr("id","IdDiagnosticoEgreso"+(k-1));
		}		
	}	
	/////////////////////////////////////////////
	function eliminarRowDiagnosticosComplicaciones(obj){
		var oTr = obj;
		while(oTr.nodeName.toLowerCase()!='tr'){
			oTr=oTr.parentNode;
		}		
		var root = oTr.parentNode;	
		var rIndex = oTr.sectionRowIndex;
		var indexDetalleTable=parseInt(rIndex)+1;		
		
		alert("Registro "+(indexDetalleTable-2)+" Eliminado"); //si elimino fila 2 obtiene 2, /si elimino fila 3 obtiene 3
		
		//reordenar	
		var theTable = document.getElementById('tablaCausaExterna');
		cantFilas = theTable.rows.length;
		if(cantFilas!=3){
			reorderRowsDiagnosticosComplicaciones(indexDetalleTable);	
		}else{
			//alert('no ordenar');			
		}
		
		root.removeChild(oTr);
		//disminuir 1 al maximo numero de item
		var contadorCE=document.getElementById('contadorCE').value;
		document.getElementById('contadorCE').value=contadorCE-1;				
    }	
	
	function reorderRowsDiagnosticosComplicaciones(startingIndex)  //startingIndex=2,3,4 
	{
		var theTable = document.getElementById('tablaCausaExterna');
		cantFilas = (theTable.rows.length)-1;	
		var count = startingIndex-1 ; //1,2,3,4	
		for (var n=count; n<cantFilas; n++) {
			//alert(k+'cantFilas'+cantFilas);//$("#"+ k).html(k-1);
			$("#Com"+ n).attr("id","Com"+(n-1));
			
			document.getElementById("BuscarCIE10"+ n).name="BuscarCIE10"+ (n-1);	
			$("#BuscarCIE10"+ n).attr("id","BuscarCIE10"+(n-1));

			document.getElementById("IdDiagnostico"+ n).name="IdDiagnostico"+ (n-1);	
			$("#IdDiagnostico"+ n).attr("id","IdDiagnostico"+(n-1));
		}		
	}	
	/////////////////////////////////////////////
	
	function eliminarRowDiagnosticosMortFetal(obj){
		var oTr = obj;
		while(oTr.nodeName.toLowerCase()!='tr'){
			oTr=oTr.parentNode;
		}		
		var root = oTr.parentNode;	
		var rIndex = oTr.sectionRowIndex;
		var indexDetalleTable=parseInt(rIndex)+1;		
		
		alert("Registro "+(indexDetalleTable-2)+" Eliminado"); //si elimino fila 2 obtiene 2, /si elimino fila 3 obtiene 3
		
		//reordenar	
		var theTable = document.getElementById('tablaDiagnosticosEgresoMortFetal');
		cantFilas = theTable.rows.length;
		if(cantFilas!=3){
			reorderRowsDiagnosticosMortFetal(indexDetalleTable);	
		}else{
			//alert('no ordenar');			
		}
		
		root.removeChild(oTr);
		//disminuir 1 al maximo numero de item
		var contadorN=document.getElementById('contadorN').value;
		document.getElementById('contadorN').value=contadorN-1;				
    }	
	
	function reorderRowsDiagnosticosMortFetal(startingIndex)  //startingIndex=2,3,4 
	{
		var theTable = document.getElementById('tablaDiagnosticosEgresoMortFetal');
		cantFilas = (theTable.rows.length)-1;	
		var count = startingIndex-1 ; //1,2,3,4	
		for (var n=count; n<cantFilas; n++) {
			//alert(k+'cantFilas'+cantFilas);//$("#"+ k).html(k-1);
			$("#Nac"+ n).attr("id","Nac"+(n-1));	

			document.getElementById("IdSubclasificacionDxMortFetal"+ n).name="IdSubclasificacionDxMortFetal"+ (n-1);	
			$("#IdSubclasificacionDxMortFetal"+ n).attr("id","IdSubclasificacionDxMortFetal"+(n-1));

			document.getElementById("BuscarCodigoCIE10MortFetal"+ n).name="BuscarCodigoCIE10MortFetal"+ (n-1);	
			$("#BuscarCodigoCIE10MortFetal"+ n).attr("id","BuscarCodigoCIE10MortFetal"+(n-1));

			document.getElementById("IdDiagnosticoEgresoMortFetal"+ n).name="IdDiagnosticoEgresoMortFetal"+ (n-1);	
			$("#IdDiagnosticoEgresoMortFetal"+ n).attr("id","IdDiagnosticoEgresoMortFetal"+(n-1));
		}		
	}	
	/////////////////////////////////////////////
	function eliminarRowDiagnosticosMort(obj){
		var oTr = obj;
		while(oTr.nodeName.toLowerCase()!='tr'){
			oTr=oTr.parentNode;
		}		
		var root = oTr.parentNode;	
		var rIndex = oTr.sectionRowIndex;
		var indexDetalleTable=parseInt(rIndex)+1;		
		
		alert("Registro "+(indexDetalleTable-2)+" Eliminado"); //si elimino fila 2 obtiene 2, /si elimino fila 3 obtiene 3
		
		//reordenar	
		var theTable = document.getElementById('tablaDiagnosticosEgresoMort');
		cantFilas = theTable.rows.length;
		if(cantFilas!=3){
			reorderRowsDiagnosticosMort(indexDetalleTable);	
		}else{
			//alert('no ordenar');			
		}
		
		root.removeChild(oTr);
		//disminuir 1 al maximo numero de item
		var contadorK=document.getElementById('contadorK').value;
		document.getElementById('contadorK').value=contadorK-1;				
    }	
	
	function reorderRowsDiagnosticosMort(startingIndex)  //startingIndex=2,3,4 
	{
		var theTable = document.getElementById('tablaDiagnosticosEgresoMort');
		cantFilas = (theTable.rows.length)-1;	
		var count = startingIndex-1 ; //1,2,3,4	
		for (var n=count; n<cantFilas; n++) {
			//alert(k+'cantFilas'+cantFilas);//$("#"+ k).html(k-1);
			$("#Mor"+ n).attr("id","Mor"+(n-1));	

			document.getElementById("IdSubclasificacionDxMort"+ n).name="IdSubclasificacionDxMort"+ (n-1);	
			$("#IdSubclasificacionDxMort"+ n).attr("id","IdSubclasificacionDxMort"+(n-1));

			document.getElementById("BuscarCodigoCIE10Mort"+ n).name="BuscarCodigoCIE10Mort"+ (n-1);	
			$("#BuscarCodigoCIE10Mort"+ n).attr("id","BuscarCodigoCIE10Mort"+(n-1));

			document.getElementById("IdDiagnosticoEgresoMort"+ n).name="IdDiagnosticoEgresoMort"+ (n-1);	
			$("#IdDiagnosticoEgresoMort"+ n).attr("id","IdDiagnosticoEgresoMort"+(n-1));
		}		
	}	
	/////////////////////////////////////////////
	
	function eliminarRowNacimientos(){
		
	}
	function reorderRowsNacimientos(startingIndex)  //startingIndex=2,3,4 
	{
		/*var theTable = document.getElementById('tablaDiagnosticosEgresoMort');
		cantFilas = (theTable.rows.length)-1;	
		var count = startingIndex-1 ; //1,2,3,4	
		for (var n=count; n<cantFilas; n++) {
			//alert(k+'cantFilas'+cantFilas);//$("#"+ k).html(k-1);
			$("#Mor"+ n).attr("id","Mor"+(n-1));	

			document.getElementById("IdSubclasificacionDxMort"+ n).name="IdSubclasificacionDxMort"+ (n-1);	
			$("#IdSubclasificacionDxMort"+ n).attr("id","IdSubclasificacionDxMort"+(n-1));

			document.getElementById("BuscarCodigoCIE10Mort"+ n).name="BuscarCodigoCIE10Mort"+ (n-1);	
			$("#BuscarCodigoCIE10Mort"+ n).attr("id","BuscarCodigoCIE10Mort"+(n-1));

			document.getElementById("IdDiagnosticoEgresoMort"+ n).name="IdDiagnosticoEgresoMort"+ (n-1);	
			$("#IdDiagnosticoEgresoMort"+ n).attr("id","IdDiagnosticoEgresoMort"+(n-1));
		}*/		
	}

</script>   
   
 <script language="javascript" type="text/javascript"> 	 

function calcularedad(){					
	//var fecha=$("#FechaNacimiento").datebox('getValue');//getText		
	var fecha=document.getElementById('FechaNacimientoRN').value;	
	if(fecha!=""){	
		//validar fecha actual					
		fnacarray= fecha.split('/');
		fnac = new Date(fnacarray[2], fnacarray[1]-1 , fnacarray[0]);					
		f=new Date();		
		factual = new Date(f.getFullYear() , (f.getMonth()) , f.getDate());							
		if(fnac>factual){ //validar fecha actual								
			//$.messager.alert('Mensaje','La Fecha de Nacimiento debe ser menor a la Fecha actual','info');
			alert('La Fecha de Nacimiento debe ser menor a la Fecha actual');
			$('#FechaNacimientoRN').next().find('input').focus();
			document.getElementById('FechaNacimientoRN').value='';
			document.getElementById('EdadSemanas').value='';								
			return 0;			
		}

		//calculo la fecha que recibo
		//La descompongo en un array
		var array_fecha = fecha.split("/");												
		//si el array no tiene tres partes, la fecha es incorrecta
		if (array_fecha.length!=3){
		   alert('La fecha es incorrecta');				   
		}else{	
			//compruebo que los ano, mes, dia son correctos								
			var ano = (array_fecha[2]);								
			var mes = (array_fecha[1]);								
			var dia = (array_fecha[0]);			
			var fechanac=ano+"-"+mes +"-"+dia;
			
			//calculo la fecha de hoy
			hoy=new Date();
			var ahora_ano = hoy.getFullYear();
			var ahora_mes = hoy.getMonth()+1;						
			var ahora_dia = hoy.getDate();			
			var fechaactual=ahora_ano+"-"+ahora_mes +"-"+ahora_dia;
		
			//var EdadSemanas=fechanac+'sep'+fechaactual;//PRUEBA
			
			var fechaInicio = new Date(fechanac).getTime();//new Date('2016-08-01').getTime();
			var fechaFin    = new Date(fechaactual).getTime();

			var diff = fechaFin - fechaInicio;
			
			var EdadDias=diff/(1000*60*60*24); //edad en dias
			var EdadSemanas= parseInt(EdadDias/7);																    
		}							
		document.getElementById('EdadSemanas').value=EdadSemanas;	
		
	}else{
		document.getElementById('EdadSemanas').value='';
		alert('Ingrese Fecha de Nacimiento');
		return 0;	
	}	
}
	 
var posicionCampoM=1;	
function AgregarNacimientos(){//POR MAHALI
	 
	var IdCondicionRN=document.getElementById('IdCondicionRN').value;	
	
	var comboselec1 = document.getElementById("IdCondicionRN");
    var CondicionRN = comboselec1.options[comboselec1.selectedIndex].text;	
	
	var IdTipoSexoRN=document.getElementById('IdTipoSexoRN').value;
	var comboselec2 = document.getElementById("IdTipoSexoRN");
    var TipoSexoRN = comboselec2.options[comboselec2.selectedIndex].text;	
	
	var PesoRN=document.getElementById('PesoRN').value;
	var TallaRN=document.getElementById('TallaRN').value;
	
	var FechaNacimientoRN=document.getElementById('FechaNacimientoRN').value;
	var EdadSemanas=document.getElementById('EdadSemanas').value;
	
	var NroOrdenHijoEnParto=document.getElementById('NroOrdenHijoEnParto').value;
	var NroOrdenHijo=document.getElementById('NroOrdenHijo').value;
	var ClamplajeFecha=document.getElementById('ClamplajeFecha').value;
	
	var IdDocIdentidadRN=document.getElementById('IdDocIdentidadRN').value;
	var comboselec3 = document.getElementById("IdDocIdentidadRN");
    var DocIdentidadRN = comboselec3.options[comboselec3.selectedIndex].text;	 
	
	var docIdentidad=document.getElementById('docIdentidad').value;
	
	var apgar_1=document.getElementById('apgar_1').value;
	var apgar_5=document.getElementById('apgar_5').value;

		//INICIO VALIDAR QUE ALGUN CAMPO NO SE REPITA
		/*var maxM=document.getElementById('contadorNac').value;		
		for(b=1;b<=maxM;b++){
			if(XXXX==document.getElementById("XXXX"+b).value){					
				mensje = "El XXXX ya fue agregado al listado.";
				alert(mensje);
				return 0;
			}				
		}*/
		//FIN VALIDAR QUE ALGUN CAMPO NO SE REPITA
	
	if(IdCondicionRN!=""){	

	posicionCampoM=parseInt(document.getElementById('contadorNac').value)+1;
	document.getElementById('contadorNac').value=posicionCampoM;

	///alert('-->'+CodigoCIE10+'->'+descripcion+'->'+IdDiagnosticoEgreso);
	nuevaFila = document.getElementById("tablaNacimientos").insertRow(-1);
	nuevaFila.id=posicionCampoM;
		
	nuevaCelda=nuevaFila.insertCell(-1);	
	nuevaCelda.innerHTML="<td>"+posicionCampoM+"</td>";
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+FechaNacimientoRN+"<input name='FechaNacimientoRN"+posicionCampoM+"' type='hidden' id='FechaNacimientoRN"+posicionCampoM+"'  value='"+FechaNacimientoRN+"' size='20'></td>";	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+EdadSemanas+"<input name='EdadSemanas"+posicionCampoM+"' type='hidden' id='EdadSemanas"+posicionCampoM+"'  value='"+EdadSemanas+"' size='5'></td>";	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input type='number' name='TallaRN"+posicionCampoM+"' id='TallaRN"+posicionCampoM+"' value='"+TallaRN+"' min='0' style='width: 5em;' /></td>";
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input type='number' name='PesoRN"+posicionCampoM+"' id='PesoRN"+posicionCampoM+"' value='"+PesoRN+"'  min='0' style='width: 5em;'></td>";	

	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+CondicionRN+"<input name='IdCondicionRN"+posicionCampoM+"' type='hidden' id='IdCondicionRN"+posicionCampoM+"'  value='"+IdCondicionRN+"' size='5' /></td>";	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+TipoSexoRN+"<input name='IdTipoSexoRN"+posicionCampoM+"' type='hidden' id='IdTipoSexoRN"+posicionCampoM+"' value='"+IdTipoSexoRN+"' size='5' /></td>"; 
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+ClamplajeFecha+"<input name='ClamplajeFecha"+posicionCampoM+"' type='hidden' id='ClamplajeFecha"+posicionCampoM+"' value='"+ClamplajeFecha+"' size='5' /></td>"; 	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input type='number' name='NroOrdenHijoEnParto"+posicionCampoM+"' id='NroOrdenHijoEnParto"+posicionCampoM+"' value='"+NroOrdenHijoEnParto+"' min='0' step='1' style='width: 4em;' /></td>";	
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td><input type='number' name='NroOrdenHijo"+posicionCampoM+"' id='NroOrdenHijo"+posicionCampoM+"' value='"+NroOrdenHijo+"' min='0' step='1' style='width: 4em;' /></td>";
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+DocIdentidadRN+"<input name='IdDocIdentidadRN"+posicionCampoM+"' type='hidden' id='IdDocIdentidadRN"+posicionCampoM+"' value='"+IdDocIdentidadRN+"' size='5' /></td>"; 
		
	nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td>"+docIdentidad+"<input name='docIdentidad"+posicionCampoM+"' type='hidden' id='docIdentidad"+posicionCampoM+"' value='"+docIdentidad+"' size='5' /><input type='hidden' name='apgar_1"+posicionCampoM+"' id='apgar_1"+posicionCampoM+"' value='"+apgar_1+"'  size='5'> <input type='hidden' name='apgar_5"+posicionCampoM+"' id='apgar_5"+posicionCampoM+"' value='"+apgar_5+"'  size='5'></td>";
		
    /*nuevaCelda=nuevaFila.insertCell(-1);
	nuevaCelda.innerHTML="<td width='58' align='left'><img src='../../MVC_Complemento/img/user_logout.png' width='16' height='16' onclick='eliminarRowNacimientos(this)'></td>";*/	
	
	posicionCampoM++;	
		
	/*
	document.getElementById('TallaRN').value='';
	document.getElementById('PesoRN').value='';
	
	*/
	
	}else{
		alert("Seleccione una condicion RN Valido.");
		document.getElementById('IdCondicionRN').focus();		
	}
}
	
/////////////////////////////////////////////////////////////////////////////////////////////
 
function Guardar(){	
		//ValidarReglas
		if(document.getElementById('FechaEgresoAdministrativo').value!=''){
		   alert("La cuenta se encuentra cerrado automáticamente.");	
		  return 0;
		}
	
		var fechaEgresoBD='<?php echo $fechaEgresoBD ?>';
	
		if(fechaEgresoBD!=''){
		   alert("El paciente ya fue dado de alta.");	
		  return 0;
		}
	
		var FechaEgreso=document.getElementById('FechaEgreso').value;
		var HoraEgreso=document.getElementById('HoraEgreso').value;		
		
		var array_fecha = FechaEgreso.split("/");
		var array_hora = HoraEgreso.split(":");										

		if(FechaEgreso!=""){
			if (array_fecha.length!=3 || (!/^([0-9])*$/.test(parseInt(array_fecha[0]+array_fecha[1]+array_fecha[2])))){
				   alert("El formato Fecha Alta es incorrecta.");
				   document.getElementById('HoraEgreso').focus();
					return 0;
			}

			if (HoraEgreso==''){
				   alert("Ingrese Hora Alta.");
				   document.getElementById('HoraEgreso').focus();
					return 0;
			}
			
			if (array_hora.length!=2 || (!/^([0-9])*$/.test(parseInt(array_hora[0]))) || (!/^([0-9])*$/.test(parseInt(array_hora[1]))) ){
				   alert("El formato Hora Alta es incorrecta.");
				   document.getElementById('HoraEgreso').focus();
					return 0;
			}

			var IdMedicoEgreso=document.getElementById('IdMedicoEgreso').value;	
			if (IdMedicoEgreso==''){
				   alert("Por favor debe registrar el Médico del Alta.");
				   document.getElementById('BuscarMedicoEgreso').focus();
					return 0;
			}
			//validar fecha egreso
			var HoraMaxEmerg='<?php echo $HoraMaxEmerg ?>';	
			var FechaIngreso='<?php echo $FechaIngreso ?>';	
			var HoraIngreso='<?php echo $HoraIngreso ?>';			

			  var arrayFechaIngreso=FechaIngreso.split("/"); //02/12/2018
			  var arrayFechaEgreso=FechaEgreso.split("/"); //02/12/2018

			  var arrayHoraIngreso=HoraIngreso.split(":");
			  var arrayHoraEgreso=HoraEgreso.split(":");
			  			    
			  var a = new Date(arrayFechaIngreso[2],arrayFechaIngreso[1]-1,arrayFechaIngreso[0],arrayHoraIngreso[0],arrayHoraIngreso[1]);//var a = new Date("February 12, 2014 03:29:01");
															 //var a = new Date(2018, 11, 24, 10, 33);
			  var b = new Date(arrayFechaEgreso[2],arrayFechaEgreso[1]-1,arrayFechaEgreso[0],arrayHoraEgreso[0],arrayHoraEgreso[1]);

			  var c = ((b-a)/1000);//segundos //La diferencia se da en milisegundos así que debes dividir entre 1000
			  var DifHoras=(c/3600);//horas			  

			  //alert(a+b+'Horas'+DifHoras);
			
			if( parseInt(DifHoras) > HoraMaxEmerg ) {
            	alert("En Emergencia, la FECHA DE EGRESO  no puede pasar de " + HoraMaxEmerg + " horas");
            	return 0;
            }

            if(DifHoras<0) {
            	alert("La fecha de EGRESO MEDICO no puede ser menor que la fecha de INGRESO");
            	return 0;
            }
		
			if (document.getElementById('IdDestinoAtencion').selectedIndex==0){
				   alert("Seleccionar Destino Egreso.");
				   document.getElementById('IdDestinoAtencion').focus();
				   return 0;
			}

			if (document.getElementById('IdTipoAlta').selectedIndex==0){
				alert("Seleccionar Tipo Alta.");
				document.getElementById('IdTipoAlta').focus();
				return 0;
			}	

			if (document.getElementById('IdCondicionAlta').selectedIndex==0){
				alert("Por favor debe elegir la CONDICION DE ALTA.");
				document.getElementById('IdCondicionAlta').focus();
				return 0;
			}	

			var IdCondicionAlta=document.getElementById('IdCondicionAlta').value;	
			
			if(IdCondicionAlta==4){ //'Paciente Fallecido
				//diagnostico mortalidad
				var theTable2 = document.getElementById('tablaDiagnosticosEgresoMort');
				cantFilasDXMort = theTable2.rows.length;
				if(cantFilasDXMort==2){ //'Paciente Fallecido
					alert("La condición del paciente indica PACIENTE FALLECIDO, Por favor ingreso de DIAGNOSTICO -> CAUSA FINAL (ficha 3.4)");
					document.getElementById('descripcionCIE10Mort').focus();
				    return 0;	
			    }	
			}else{
				//diagnostico egreso
				var theTable = document.getElementById('tablaDiagnosticosEgreso');
				cantFilasDXEgreso = theTable.rows.length;
				if(cantFilasDXEgreso==2){
					alert("Por favor debe llenar DIAGNOSTICO -> PRINCIPAL");
					document.getElementById('descripcionCIE10').focus();
			    	return 0;		
				}
			}

			//MsgBox "La paciente tubo Dx de PARTO, por favor debe registrar cada Nacimiento (Ficha 3.3)(según DESTINO ATENCION)"
			//'Valida la infeccion intrahospitalaria

			//'referencia EGRESO
			var IdDestinoAtencion=document.getElementById('IdDestinoAtencion').value;	
		    		        
	        if(IdDestinoAtencion.trim()=='23' || IdDestinoAtencion.trim()=='24'){ //23 referencia //24 contrareferencia	
		          var IdEstablecimientoDestino=document.getElementById('IdEstablecimientoDestino').value;    
		          if(IdEstablecimientoDestino==""){
		           	//MsgBox "Debe elejir el ESTABLEC.REFERIDO (Destino)(ficha 3.1)"
		           	alert("Debe elejir el ESTABLEC.REFERIDO (Destino)(ficha 3.1)");
		           	document.getElementById('EstablecimientoDestino').focus();
			    	return 0;
		          }

		          var NroReferencia=document.getElementById('NroReferencia').value;    
		          if(NroReferencia==""){
		           	//MsgBox "Debe registrar el N°REFERENCIA (Destino)(ficha 3.1)",
		           	alert("Debe registrar el N°REFERENCIA (Destino)(ficha 3.1)");
		           	document.getElementById('NroReferencia').focus();
			    	return 0;
		          }		          
	           
	        }//End If	

	        //Reglas UcEpisodioClinico1
	        /*If Me.UcEpisodioClinico1.ValidaReglas(" (Ficha 3.1)") = False Then
	          Me.TabEgresos.Tab = 0
	          Exit Function
	        End If*/  
			var lcMensaje = "";			
			
			var lblEpisodioElegido=document.getElementById("lblEpisodioElegido").innerHTML;
			var lcFicha="(Ficha 3.1)";
			if(document.getElementById("chkEpisodioCerrar").checked==false && document.getElementById("chkEpisodioNew").checked==false && lblEpisodioElegido==""){
				lcMensaje = lcMensaje + "Falta elejir datos para el EPISODIO CLINICO " + lcFicha;
			    alert(lcMensaje);	
				return 0;
			}
			
			if (document.getElementById("chkEpisodioCerrar").checked==true && document.getElementById("chkEpisodioNew").checked==false && lblEpisodioElegido==""){
			   lcMensaje = lcMensaje + "Falta elejir el EPISODIO HISTORICO para poder CERRAR el EPISODIO CLINICO " + lcFicha;
			   alert(lcMensaje);
			   return 0;
			}	
		
			var r = confirm("¿Está seguro de dar de Alta Médica?");
			if (r == true) {
				document.formElem.submit();
				parent.BuscarPacientesEmergencia();
				parent.googlebox.hide();
			}
			
		}else{
			/*var r = confirm("¿Está seguro de modificar los datos de la cuenta?");
			if (r == true) {
				document.formElem.submit();
				parent.BuscarPacientesEmergencia();
				parent.googlebox.hide();
			}*/
			alert("Ingrese Fecha de Alta.");
			
		}		
	    
 }	
 
function NuevoEpisodio(){		
	if(document.getElementById("chkEpisodioNew").checked==true){		
		document.getElementById("lblEpisodioElegido").innerHTML="";
		document.getElementById("EpisodioHistorico").value="";
	}
}


function Cerrar(){
	//parent.BuscarPacientesEmergencia();
	parent.googlebox.hide();
}

function fechaActual(){
	var hoy = new Date();
	var dd = hoy.getDate();
	var mm = hoy.getMonth()+1; //hoy es 0!
	var yyyy = hoy.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	var hoy = dd+'/'+mm+'/'+yyyy;
	return hoy;
}

function horaActual(){
	var hoy = new Date();
	var hor=hoy.getHours();
	var min=hoy.getMinutes();

	if(hor<10) {
	    hor='0'+hor;
	} 

	if(min<10) {
	    min='0'+min;
	} 

	var horahoy = hor+':'+min;
	return horahoy;
}

	
function CambiarDestinoAtencion(){
	var IdDestinoAtencion=document.getElementById('IdDestinoAtencion').value;
	//alert(IdDestinoAtencion);
	if(IdDestinoAtencion.trim()==''){
		//Limpiar
		document.getElementById('FechaEgreso').value='';
		document.getElementById('HoraEgreso').value='';
		
		document.getElementById('IdTipoAlta').value='0';
		document.getElementById('IdCondicionAlta').value='0';		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;
					
	}else{
		var factual=fechaActual();
		var hactual=horaActual();
		document.getElementById('FechaEgreso').value=factual;
		document.getElementById('HoraEgreso').value=hactual;
	}	
	
	if(IdDestinoAtencion.trim()=='20'){//20 domicilio alta
		document.getElementById('IdTipoAlta').value='1';
		document.getElementById('IdCondicionAlta').value='2';		
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;		
		
	}else if(IdDestinoAtencion.trim()=='21'){ //21 hospitalizacion
		document.getElementById('IdTipoAlta').value='6';	
		document.getElementById('IdCondicionAlta').value='2';
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;
		
	}else if(IdDestinoAtencion.trim()=='22'){ //22 hospitalizacion
		document.getElementById('IdTipoAlta').value='5';
		document.getElementById('IdCondicionAlta').value='0';
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;
		
	}else if(IdDestinoAtencion.trim()=='23' || IdDestinoAtencion.trim()=='24'){ //23 referencia //24 contrareferencia
		document.getElementById('IdTipoAlta').value='4';
		document.getElementById('IdCondicionAlta').value='0';
		
		document.getElementById('TipoReferencia').disabled=false;
		document.getElementById('EstablecimientoDestino').readOnly=false;
		document.getElementById('NroReferencia').readOnly=false;		
		
	}else if(IdDestinoAtencion.trim()=='25'){ //25 morgue
		document.getElementById('IdTipoAlta').value='0';
		document.getElementById('IdCondicionAlta').value='4';
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;		
		
	}else if(IdDestinoAtencion.trim()=='26'){ //26 T = Otros
		document.getElementById('IdTipoAlta').value='0';
		document.getElementById('IdCondicionAlta').value='0';
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;
		
	}else if(IdDestinoAtencion.trim()=='55'){ //55 N = Ref. Apoyo al Dx
		document.getElementById('IdTipoAlta').value='0';
		document.getElementById('IdCondicionAlta').value='0';
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;
		
	}else if(IdDestinoAtencion.trim()=='56'){ //56 F = Fuga
		document.getElementById('IdTipoAlta').value='3';
		document.getElementById('IdCondicionAlta').value='0';
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;
		
	}else if(IdDestinoAtencion.trim()=='65'){ //65 I = Citado (Consulta Externa)
		document.getElementById('IdTipoAlta').value='0';
		document.getElementById('IdCondicionAlta').value='0';
		
		document.getElementById('TipoReferencia').value='0';
		document.getElementById('TipoReferencia').disabled=true;
		document.getElementById('EstablecimientoDestino').value='';
		document.getElementById('IdEstablecimientoDestino').value='';
		document.getElementById('EstablecimientoDestino').readOnly=true;
		document.getElementById('NroReferencia').value='';
		document.getElementById('NroReferencia').readOnly=true;
	}
	cambiarCondicionAlta();
}
</script> 
	
    <script language="javascript"> 
  function NumCheck(e, field) {
    key = e.keyCode ? e.keyCode : e.which
    if (key == 8) return true
    if (key > 47 && key < 58) {
      if (field.value == "") return true
      regexp = /.[0-9]{5}$/
      return !(regexp.test(field.value))
    }
    if (key == 46) {
      if (field.value == "") return false
      regexp = /^[0-9]+$/
      return regexp.test(field.value)
    }
    return false
  }
		
  function cambiarCondicionAlta(){
	 var IdCondicionAlta= document.getElementById('IdCondicionAlta').value;
       if(IdCondicionAlta== "4"){//Si el paciente es "fallecido"  
		   //no debe tener DIAGNOSTICO EGRESO
          //solo Dx de MORTALIDAD
          //ucDiagnosticosEgreso.LimpiarDatos;		
		  	$('#tablaDiagnosticosEgreso tr:not(:first-child)').slice(1).remove();
		    document.getElementById('contadorJ').value=0;
		   document.getElementById('BuscarCodigoCIE10Mort').readOnly=false;
		    document.getElementById('BuscarCodigoCIE10').readOnly=true;
			
		   
		   
		}else{//Si el paciente NO "fallece"  
			  //se debe tener DIAGNOSTICO EGRESO
			  //eliminar los de MORTALIDAD
			  //ucDiagnosticosMortalidad.LimpiarDatos;
			 $('#tablaDiagnosticosEgresoMort tr:not(:first-child)').slice(1).remove();
			document.getElementById('contadorK').value=0;
			document.getElementById('BuscarCodigoCIE10').readOnly=false;
			document.getElementById('BuscarCodigoCIE10Mort').readOnly=true;
		}	
  }		
		
</script> 
   <script type="text/javascript" >
       /* function cambiar() {
            $('#buttonHola').removeAttr('onclick').click(function () { alert("Hola Amigo, Que la pases bien el los foros"); });
            alert("se cambio el saludo!");
         }*/
    </script>
  </head>

 
 
 <body> 	

 <form onSubmit="return false;" id="formElem" name="formElem"  method="post" action="../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=DarAlta_Emergencia">

<!--<input type="button" id="buttonHola" onclick="alert('holamundo')" value = "saludar" />
    <input type="button" id="button1" onclick="cambiar()" value = "Cambiar Saludo" /> -->
    <table width="848" border="0" align="" cellpadding="0" cellspacing="0">
      <tr>
    	<th scope="row">			
			<img src="../../MVC_Complemento/img/botoncancelar.jpg" width="85" height="22" onClick="Cerrar();"/>
			<img src="../../MVC_Complemento/img/botonaceptar.jpg" width="85" height="22" onClick="Guardar();" />			
		</th>
  	  </tr>
	</table>


<div class="tab">
  <button class="tablinks" onClick="openTabs(event, 'Egreso')" id="defaultOpen">3.1 Egreso</button>
  <button class="tablinks" onClick="openTabs(event, 'Complicaciones')">3.2 Complicaciones</button>
  <button class="tablinks" onClick="openTabs(event, 'Nacimientos')">3.3 Nacimientos</button>
  <button class="tablinks" onClick="openTabs(event, 'Mortalidad')">3.4 Mortalidad</button>
</div>

<div id="Egreso" class="tabcontent">
	
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DATOS DEL PACIENTE</strong></legend>
	  <table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
            <td height="19" width="15%"><strong>Paciente:</strong></td>
            <td colspan="3" ><?php echo $ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre;?></td>
          </tr>
          <tr>
            <td height="19"><strong>Nro Historia</strong></td>
            <td width="10%"><?php echo $NroHistoriaClinica; ?>
              <input name="NroHistoriaClinica" type="hidden" id="NroHistoriaClinica" value="<?php echo $NroHistoriaClinica; ?>"></td>
			<td height="19" width="10%"><strong>Sexo</strong></td>
            <td><?php echo strtoupper($Sexo);?></td> 
			  
          </tr>         
          <tr>
            <td height="19"><strong>Fecha Nacimiento</strong></td>
            <td><?php echo vfechahora(substr($FechaNacimiento,0,10));?></td>
			<td height="19"><strong>Edad</strong></td>
            <td><?php
				
		   if($FechaNacimiento!=NULL){
		   $fecha_nacimiento = vfechahora(substr($FechaNacimiento,0,10));
           $fecha_control = vfechahora(substr($FechaServidor,0,10));

		   $tiempo = tiempo_transcurrido($fecha_nacimiento, $fecha_control);	   
		   $texto = "$tiempo[0] Años con $tiempo[1] Me. y $tiempo[2] Dí.";
		   print "".$texto."";
		   }else{
			 $fecha_nacimiento = '';  
		   }

			
			  ?>
			  <input name="TiempoNac" type="hidden" id="TiempoNac" value="<?php echo $tiempo[0].'|'.$tiempo[1].'|'.$tiempo[2]; ?>">
			  </td> 
          </tr>        
          
          <tr>
            <td height="15" ><strong>Fte.Financiam/IAFA</strong></td>
            <td><?php echo $FuenteFinanciamiento;?><span style="background:#E0ECF8">
            <input name="idAtencion_Diagnostico" type="hidden" id="idAtencion_Diagnostico" value="<?php echo $IdAtencion;?>" size="15" readOnly>
			<input name="IdEmpleado" type="hidden" id="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'];?>" size="10" />

            <input name="IdPaciente" type="hidden" id="IdPaciente" value="<?php echo $IdPaciente;?>" />
			<input name="IdAtencion" type="hidden" id="IdAtencion" value="<?php echo $IdAtencion;?>" />	
			<input name="IdTipoServicio" type="hidden" id="IdTipoServicio" value="<?php echo $TipoServicio;?>" />	
				
			<input name="IdEspecialidadServicioEgreso" type="hidden" id="IdEspecialidadServicioEgreso" value="<?php echo $IdEspecialidadServicioEgreso;?>" />	
			<input name="Paciente" id="Paciente" type="hidden" value="<?php echo $ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre;?>" />
			<input name="FuenteFinanciamiento" type="hidden" id="FuenteFinanciamiento" value="<?php echo $FuenteFinanciamiento;?>" />

			<input name="FechaIngreso" type="hidden" id="FechaIngreso" value="<?php echo $FechaIngreso;?>" />	
			<input name="HoraIngreso" type="hidden" id="HoraIngreso" value="<?php echo $HoraIngreso;?>" />	
				
            </span></td>
			  <td height="15" ><strong>Nº Cuenta </strong></td>
            <td><?php echo $IdCuentaAtencion; ?>
              <input name="IdCuentaAtencion" type="hidden" id="IdCuentaAtencion" value="<?php echo $IdCuentaAtencion; ?>"></td>           
             </td>  
			  
          </tr>
          
          <tr>
           
          </tr>
        </table>
  </fieldset>
  	 
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DATOS DEL EGRESO</strong></legend>
    <table width="848" border="0">
		
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          
				<tr>
				<td height="16" ><strong>Destino</strong></td>
				<td><select name="IdDestinoAtencion" id="IdDestinoAtencion"  style="width: 275px; height:27" class="Combos" onChange="CambiarDestinoAtencion()">
				  <option value=""> </option>
				  <?php
				   $TiposDestino=TiposDestinoAtencionEmergencia_M();
				   if($TiposDestino != NULL) { 
					 foreach($TiposDestino as $item4){?>
				  <option value="<?php echo $item4["IdDestinoAtencion"]?>" <?php if($item4["IdDestinoAtencion"]==$IdDestinoAtencion){?>selected<?php }?> ><?php echo $item4["DescripcionLarga"]?></option>				
				  <?php } }
				?>
				</select></td>
			  </tr>

			  <tr>
				<td height="16" ><strong>Servicio Egreso</strong></td>
				<td>
				<input type="text" class="texto" disabled value="<?php echo $CodigoServicioEgreso;?>" style="width: 80px;"  /> 
				<input type="text" class="texto" disabled  value="<?php echo $NombreServicioEgreso;?>" style="width: 193px;" />
					
				<input name="IdServicioEgreso" type="hidden" id="IdServicioEgreso" value="<?php echo $IdServicioEgreso;?>" /> 
				</td>
			  </tr>

			  <tr >
				<td height="19" ><strong>Fecha Alta&nbsp</strong></td>
				<td>
					<input name="FechaEgreso" type="text" id="FechaEgreso" value="<?php echo $fechaEgresoBD ?>" style="width: 120px;" class="texto" maxlength="10" />

					<input name="ldFechaEgresoMedicoAnterior" type="hidden" id="ldFechaEgresoMedicoAnterior" value="<?php echo $fechaEgresoBD ?>" style="width: 120px;" class="texto" maxlength="10" />

					<img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio" /><script type="text/javascript"> 
					Calendar.setup({ 
						inputField     :    "FechaEgreso",     // id del campo de texto 
						 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
						 button     :    "lanzador1"     // el id del botón que lanzará el calendario 
					}); 
					</script>				
					&nbsp&nbsp&nbsp<strong>Hora</strong>&nbsp <input name="HoraEgreso" type="text" id="HoraEgreso" value="" style="width: 60px;" class="texto" maxlength="5" />

					<input name="FechaEgresoAdministrativo" type="hidden" id="FechaEgresoAdministrativo" value="<?php echo $FechaEgresoAdministrativo;?>" />

				</td>
			  </tr>
			  <tr >
				<td height="19" ><strong>Médico Egreso</td>
				<td>
					<input name="BuscarMedicoEgreso" type="text" id="BuscarMedicoEgreso" value="" class="texto" placeholder="Apellidos y Nombres" style="width: 190px;"  />
					<input name="CodigoPlanillaMedicoEgreso" type="text" id="CodigoPlanillaMedicoEgreso" value="" class="texto" style="width: 83px;" readOnly  />
					<input name="IdMedicoEgreso" type="hidden" id="IdMedicoEgreso"  />
				</td>
			  </tr>	  
         
        </table></td>
        <td width="376"  >
           <table border="0" cellpadding="0"   >
				<tr>
				<td height="19" ><strong>Tipo Alta</td>
				<td>		
					<select name="IdTipoAlta" id="IdTipoAlta"  style="width: 240px; height:27" class="Combos">
					  <option value="0" > </option>
					  <?php 
					$TiposAltaSeleccionarTodos=TiposAltaSeleccionarTodos_M();	
					if($TiposAltaSeleccionarTodos != NULL)  { 
					foreach($TiposAltaSeleccionarTodos as $item2){?>
					  <option value="<?php echo $item2["IdTipoAlta"]?>" <?php if($item2["IdTipoAlta"]==$IdTipoAlta){?>selected<?php }?> ><?php echo $item2["DescripcionLarga"]?></option>
					  <?php }}?>
					</select>
				</td>
			    </tr>
			   
				<tr>
				<td height="16" ><strong>Cama Egreso</strong></td>
				<td>				
				 <input name="IdCamaEgreso" type="text" class="texto" id="IdCamaEgreso" value="<?php echo $CodigoCamaEgreso;?>" style="width: 100px;" readOnly  /> 
				</td>
			   </tr>
			   
				<tr>
				<td height="19" ><strong>Condición Alta</td>
				<td>				
					<!--<input name="IdCondicionAlta" type="text" id="IdCondicionAlta" value="" class="texto"  />-->
					<select name="IdCondicionAlta" id="IdCondicionAlta"  style="width: 240px; height:27" class="Combos" onChange="cambiarCondicionAlta()">
					  <option value="0" > </option>
					  <?php 
					$TiposCondicionAltaSeleccionarTodos=TiposCondicionAltaSeleccionarTodos_M();	
					if($TiposCondicionAltaSeleccionarTodos != NULL)  { 
					foreach($TiposCondicionAltaSeleccionarTodos as $item2){?>
					  <option value="<?php echo $item2["IdCondicionAlta"]?>" <?php if($item2["IdCondicionAlta"]==$IdCondicionAlta){?>selected<?php }?> ><?php echo $item2["DescripcionLarga"]?></option>
					  <?php }}?>
					</select>
				</td>
			  </tr>
         		
			  <!--<tr>
				<td height="19" ><strong>Fecha Alta Administ.</td>
				<td>				
					<input name="FechaEgresoAdministrativoReg" type="text" id="FechaEgresoAdministrativoReg" value="" class="texto" style="width: 120px;" />	
					<img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Inicio" />
					<script type="text/javascript"> 
					Calendar.setup({ 
						inputField     :    "FechaEgresoAdministrativoReg",     // id del campo de texto 
						 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
						 button     :    "lanzador2"     // el id del botón que lanzará el calendario 
					}); 
					</script>
					<input name="HoraEgresoAdministrativoReg" type="text" id="HoraEgresoAdministrativoReg" value="" class="texto" style="width: 60px;" maxlength="5"  />	
				</td>
			  </tr>-->
        </table>
        </td>
      </tr>
    </table>
  
  </fieldset>
	 
	 
	 <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>REFER.DESTINO</strong></legend>
    <table width="848" border="0">
		
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          
				<tr>
				<td height="16" ><strong>Tipo Referencia</strong></td>
				<td><select name="TipoReferencia" id="TipoReferencia"  style="width: 275px; height:27" class="Combos" disabled>
				  <option value="0"> </option>
				  <?php
				   $TiposDestino=TiposReferenciaSeleccionarTodos_M();
				   if($TiposDestino != NULL) { 
					 foreach($TiposDestino as $item4){?>
				  <option value="<?php echo $item4["IdDestinoAtencion"]?>"><?php echo $item4["DescripcionLarga"]?></option>
				  <?php } }
				?>
				</select></td>
			  </tr>

			  <tr>
				<td height="16" ><strong>Establecimiento</strong></td>
				<td>
				<input type="text" id="EstablecimientoDestino" name="EstablecimientoDestino" class="texto"  style="width: 193px;" placeholder="Nombre" readOnly />	
				<input type="hidden" id="CodigoEstablecimientoDestino" name="CodigoEstablecimientoDestino" class="texto"  style="width: 80px;" readOnly  /> 	
					
				<input name="IdEstablecimientoDestino" type="hidden" id="IdEstablecimientoDestino" value="" /> 
				</td>
			  </tr>

			  <tr>
				<td height="16" ><strong>Nro Referencia</strong></td>
				<td>
				<input type="text" id="NroReferencia" name="NroReferencia" class="texto" value="" style="width: 193px;" readOnly />
				</td>
			  </tr>
         
        </table></td>
        <td width="376"  >
           <table border="0" cellpadding="0"   >
				<tr>
				<td height="19" ><strong>Episodio Histórico</td>
				<td colspan="3">
					<input type="text" id="EpisodioHistorico" name="EpisodioHistorico" class="texto"  onClick="buscarEpisodioHistorico(event,'divEpisodioHistorico')" size="10" /><!--onmouseout="borrartabla();"-->
					<div id="divEpisodioHistorico" class="autocompletar"  style="display:none" onClick="borrartabla();"></div>					
					<!--<select name="EpisodioHistorico" id="EpisodioHistorico"  style="" class="Combos">
					  <option value="0" > </option>
					  <option value="0" > </option>
					</select>
					<div id="divregistrosPersona" class="autocompletar">hola</div>-->
					<div style="color:red;" id="lblEpisodioElegido"></div>
				</td>
			    </tr>
			   
				<tr>
				<td height="19" ><strong>Nuevo Episodio</td>
				<td>		
					<input type="checkbox" id="chkEpisodioNew" name="chkEpisodioNew" value="1" onClick="NuevoEpisodio()" style="" />
				</td>
				<td height="19" ><strong>Cierre Episodio</td>
				<td>		
					<input type="checkbox" id="chkEpisodioCerrar" name="chkEpisodioCerrar" value="1" style="" />
				</td>	
			    </tr>
         
        </table>
        </td>
      </tr>
    </table>
  
  </fieldset> 
	 
	 
	 
	 
	
	<fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DIAGNÓSTICOS DE EGRESO (Todos Dx)</strong></legend>
    <table width="848" id="tablaDiagnosticosEgreso"><br>      
      <tr>
		<td><strong>Tipo</strong></td>
        <td><strong>Código</strong></td>
        <td colspan="2"><strong>Descripción</strong></td>
      </tr>
      <tr>
		<td>
		  	<select name="IdSubclasificacionDx" id="IdSubclasificacionDx"  style="width: 155px" class="Combos" >              
              <?php 
				$SubclasificacionDiagnosticos=SubclasificacionDiagnosticosSeleccionarDxHospEgreso_M();
				if($SubclasificacionDiagnosticos != NULL)  { 
				foreach($SubclasificacionDiagnosticos as $item2){?>
				  <option value="<?php echo $item2["IdSubclasificacionDx"]?>"><?php echo $item2["DescripcionLarga"]?>
				  </option>              
				<?php } }?>
            </select>		  
		 </td>  
        <td width="126"><input name="BuscarCodigoCIE10" type="text"  id="BuscarCodigoCIE10"   size="20"  class="texto"    /></td>
        <td colspan="2"><input name="descripcionCIE10" type="text" id="descripcionCIE10" size="60" class="texto" />
          <input name="CodigoCIE10" type="hidden"  id="CodigoCIE10" size="10" readOnly  />
          <input type="hidden" name="IdDiagnosticoEgreso" id="IdDiagnosticoEgreso">
        </td>
      </tr>
		
        <?php 
					//$ListaDiagnostico_idatencionEgreso=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'3');
					$j=0;
                    if($ListaDiagnostico_idatencionEgreso != NULL)
                    { 						
						foreach($ListaDiagnostico_idatencionEgreso as $item4){   
						$j++;
                    ?>                    
 
					  <tr id="Egr<?php echo $j;?>">
						<td>
						  <?php echo $item4["DescripcionTipoDx"];?>
						  <input name="IdSubclasificacionDx<?php echo $j;?>" type="hidden" id="IdSubclasificacionDx<?php echo $j;?>" value="<?php echo $item4["IdTipoDiagnostico"];?>" size="20">			 		
						 </td> 

						<td><input name="BuscarCodigoCIE10<?php echo $j;?>" type="text" disabled id="BuscarCodigoCIE10<?php echo $j;?>" value="<?php echo $item4["CodigoCIE2004"];?>" size="20"></td>
						<td width="648" align="left">
							<?php echo $item4["Descripcion"];?>
							<input type="hidden" name="IdDiagnosticoEgreso<?php echo $j;?>" id="IdDiagnosticoEgreso<?php echo $j;?>" value="<?php echo $item4["IdDiagnostico"];?>">			
							<input type="hidden" name="IdAtencionDiagnosticoEgreso<?php echo $j;?>" id="IdAtencionDiagnosticoEgreso<?php echo $j;?>" value="<?php echo $item4["idAtencionDiagnostico"]?>">		  
						</td>
						<td width="58" align="left"><img src="../../MVC_Complemento/img/user_logout.png" width="16" height="16" onclick='eliminarRowDiagnosticosEgreso(this)'></td>
					  </tr>
      
                <?php  } }?>
		
				<input type='hidden' name='contadorJ' id='contadorJ' value='<?php echo $j;?>'>
		
    </table>    
</fieldset>	
</div>

<div id="Complicaciones" class="tabcontent">
  <h3>3.2 Complicaciones (Todos Dx)</h3>
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>COMPLICACIONES</strong></legend>
    <table width="848" id="tablaCausaExterna"><br>      
      <tr>
        <td><strong>Código</strong></td>
        <td colspan="2"><strong>Descripción</strong></td>
      </tr>
      <tr>
        <td width="126"><input name="BuscarCIE10" type="text"  id="BuscarCIE10"   size="20"  class="texto"    /></td>
        <td colspan="2"><input name="descripcion" type="text" id="descripcion" size="60" class="texto" />
          <input name="CodigoCIE2004" type="hidden"  id="CodigoCIE2004" size="10" readOnly  />
          <input type="hidden" name="IdDiagnostico" id="IdDiagnostico">
        </td>
      </tr>		
		
        <?php 
					//$ListaDiagnostico_idatencionComplicaciones=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'6');
					$i=0;
                    if($ListaDiagnostico_idatencionComplicaciones != NULL)
                    { 						
						foreach($ListaDiagnostico_idatencionComplicaciones as $item4){   
						$i++;
                    ?>                    
 
					  <tr id="Com<?php echo $i;?>">
						<td><input name="BuscarCIE10<?php echo $i;?>" type="text" disabled id="BuscarCIE10<?php echo $i;?>" value="<?php echo $item4["CodigoCIE2004"];?>" size="20"></td>
						<td width="648" align="left"> 
						<?php echo $item4["Descripcion"];?>
						<input type="hidden" name="IdDiagnostico<?php echo $i;?>" id="IdDiagnostico<?php echo $i;?>" value="<?php echo $item4["IdDiagnostico"];?>">						
						<input type="hidden" name="IdAtencionDiagnostico<?php echo $i;?>" id="IdAtencionDiagnostico<?php echo $i;?>" value="<?php echo $item4["idAtencionDiagnostico"]?>"></td>
						<td width="58" align="left"><img src="../../MVC_Complemento/img/user_logout.png" width="16" height="16" onclick='eliminarRowDiagnosticosComplicaciones(this)'></td>
					  </tr>
      
                <?php  } }?>
		
				<input type='hidden' name='contadorCE' id='contadorCE' value='<?php echo $i;?>'>
		
    </table>
    
</fieldset>	
</div>

<div id="Nacimientos" class="tabcontent">
  <h3>3.3 Nacimientos</h3>
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>NACIMIENTOS</strong></legend>
    <table width="848">
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
		<td><strong>Condición</strong></td>
        <td><strong>Sexo</strong></td>
		<td><strong>Talla (cm.)</strong></td>  
        <td><strong>Peso (gr.)</strong></td>		
		<td><strong>F.Nacimiento</strong></td>  
		<td><strong>Edad (sem.)</strong></td>		
      </tr>
      <tr>
		<td>
		  	<select name="IdCondicionRN" id="IdCondicionRN"  style="width: 155px" class="Combos" >              
              <?php 
				$TiposCondicionRN=TiposCondicionRecienNacido_M();
				if($TiposCondicionRN != NULL)  { 
				foreach($TiposCondicionRN as $item2){?>
				  <option value="<?php echo $item2["IdCondicionRN"]?>"><?php echo $item2["Descripcion"]?>
				  </option>              
				<?php } }?>
            </select>		  
		 </td> 
		 <td>
		  	<select name="IdTipoSexoRN" id="IdTipoSexoRN"  style="width: 155px" class="Combos" >              
              <?php 
				$TiposSexo=TiposSexo_M();
				if($TiposSexo != NULL)  { 
				foreach($TiposSexo as $item2){?>
				  <option value="<?php echo $item2["IdTipoSexo"]?>"><?php echo $item2["Descripcion"]?>
				  </option>              
				<?php } }?>
            </select>		  
		 </td>  
        <td><input name="TallaRN" type="number"  id="TallaRN" class="texto" min="0" style="width: 7em;" /></td>
        <td><input name="PesoRN" type="number" id="PesoRN" class="texto" min="0" style="width: 7em;" /></td>
		<td>
			<input name="FechaNacimientoRN" type="text" id="FechaNacimientoRN" value="<?php echo date('d/m/Y');?>" style="width: 100px;" class="texto" onChange="calcularedad()" />

			<img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Nacimiento" /><script type="text/javascript"> 
			Calendar.setup({ 
				inputField     :    "FechaNacimientoRN",     // id del campo de texto 
				 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
				 button     :    "lanzador2"     // el id del botón que lanzará el calendario 
			}); 
			</script>  
		  
		</td>
		<td><input name="EdadSemanas" type="number" id="EdadSemanas" class="texto" style="width: 50px;" value="0" /></td> 
      </tr>
		
	  <tr>
		<td><strong>NºOrden Parto</strong></td>
        <td><strong>Nº Hijo</strong></td>
        <td><strong>F.Clampaje</strong></td>
		<td><strong>Tipo Documento Identidad</strong></td>
		<td><strong>Nº Documento</strong></td>  
		<td><strong> </strong></td>		
      </tr>
      <tr>
		 <td><input name="NroOrdenHijoEnParto" type="number"  id="NroOrdenHijoEnParto" class="texto" min="0"  style="width: 7em;" /></td>
		 <td><input name="NroOrdenHijo" type="number"  id="NroOrdenHijo" class="texto" min="0" style="width: 7em;" /></td>
		 <td>
			<input name="ClamplajeFecha" type="text" id="ClamplajeFecha" value="<?php echo date('d/m/Y');?>" style="width: 100px;" class="texto" />

			<img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador3" width="16" height="15" border="0" id="lanzador3" title="Fecha Clamplaje" /><script type="text/javascript"> 
			Calendar.setup({ 
				inputField     :    "ClamplajeFecha",     // id del campo de texto 
				 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
				 button     :    "lanzador3"     // el id del botón que lanzará el calendario 
			}); 
			</script>  
		  
		</td>
		 <td>
		  	<select name="IdDocIdentidadRN" id="IdDocIdentidadRN"  style="width: 155px" class="Combos" >              
              <?php 
				$TiposDocIdentidad=TiposDocIdentidad_M();
				if($TiposDocIdentidad != NULL)  { 
				foreach($TiposDocIdentidad as $item2){?>
				  <option value="<?php echo $item2["IdDocIdentidad"]?>"><?php echo $item2["Descripcion"]?>
				  </option>              
				<?php } }?>
            </select>		  
		 </td>  
        
        <td><input name="docIdentidad" type="text" id="docIdentidad" class="texto" style="width: 100px;" /></td>
		
		<td><input name="apgar_1" type="text" id="apgar_1" class="texto" style="width: 35px;" />
			<input name="apgar_5" type="text" id="apgar_5" class="texto" style="width: 35px;" />		
			<a href="#" onClick="AgregarNacimientos();"><img width="25" height="25" src="../../MVC_Complemento/img/agregar_diagnostico.png" /></a> 
		</td> 
      </tr>
       
      <tr>
		<td colspan="6">&nbsp;</td> 
      </tr>              
 	  <tr>
		<td colspan="6">
		  <table id="tablaNacimientos">
			<tr>
				<td><strong>Nro</strong></td>
				<td><strong>Fecha Nac.</strong></td> 		  
				<td><strong>Edad</strong></td>
				<td><strong>Talla</strong></td> 		  
				<td><strong>Peso</strong></td>
				<td><strong>Condicion</strong></td> 
				<td><strong>Sexo</strong></td>		  
				<td><strong>ClamplajeFecha</strong></td>
				<td><strong>NºOrdHijoParto</strong></td> 
				<td><strong><strong>NºOrdHijo</strong></td>
				<td><strong>Tipo Documento</strong></td> 
				<td><strong>NºDocumento</td> 
				<!-- <td width="58" align="left"><strong>Eliminar</strong></td> -->
			  </tr>
			  
			  <?php
					$AtencionesNacimientos=AtencionesNacimientosSeleccionarPorAtencion_M($IdAtencion);
					$nac=0;
                    if($AtencionesNacimientos != NULL)
                    {						
						foreach($AtencionesNacimientos as $item4){   
						$nac++;
                    ?>                    
 
					  <tr id="Nacim<?php echo $nac;?>">
					  	<td><?php echo $nac;?></td>
						<td>
						  <?php echo vfechahora(substr($item4["FechaNacimiento"],0,10));?>
						  <input name="FechaNacimientoRN<?php echo $nac;?>" type="hidden" id="FechaNacimientoRN<?php echo $nac;?>" value="<?php echo vfechahora(substr($item4["FechaNacimiento"],0,10));?>" />			 		
						 </td> 

						<td>
							<?php echo $item4["EdadSemanas"];?>	
							<input name="EdadSemanas<?php echo $nac;?>" type="hidden" id="EdadSemanas<?php echo $nac;?>" value="<?php echo $item4["EdadSemanas"];?>" />
						</td>

						<td>							
							<input type="number" name="TallaRN<?php echo $nac;?>" id="TallaRN<?php echo $nac;?>" value="<?php echo $item4["Talla"];?>" min="0" style="width: 5em;" />							  
						</td>

						<td>							
							<input type="number" name="PesoRN<?php echo $nac;?>" id="PesoRN<?php echo $nac;?>" value="<?php echo $item4["Peso"];?>" min="0" style="width: 5em;" />									  
						</td>

						<td>
							<?php echo $item4["IdCondicionRN"];?>								
							<input type="hidden" name="IdCondicionRN<?php echo $nac;?>" id="IdCondicionRN<?php echo $nac;?>" value="<?php echo $item4["IdCondicionRN"];?>">									  
						</td>

						<td>
							<?php echo $item4["Sexo"];?>								
							<input type="hidden" name="IdTipoSexoRN<?php echo $nac;?>" id="IdTipoSexoRN<?php echo $nac;?>" value="<?php echo $item4["IdTipoSexo"];?>">									  
						</td>

						<td>							
							<?php echo vfechahora(substr($item4["ClamplajeFecha"],0,10));?>	
							<input name="ClamplajeFecha<?php echo $nac;?>" type="hidden" id="ClamplajeFecha<?php echo $nac;?>" value="<?php echo vfechahora(substr($item4["ClamplajeFecha"],0,10));?>" />								  
						</td>

						<td>							
							<input name="NroOrdenHijoEnParto<?php echo $nac;?>" type="number" id="NroOrdenHijoEnParto<?php echo $nac;?>" value="<?php echo $item4["NroOrdenHijoEnParto"];?>" min="0" step="1" style="width: 4em;" />								  
						</td>

						<td>				
							<input name="NroOrdenHijo<?php echo $nac;?>" type="number" id="NroOrdenHijo<?php echo $nac;?>" value="<?php echo $item4["NroOrdenHijo"];?>" min="0" step="1" style="width: 4em;" />									  
						</td>

						<td>							
							<?php echo $item4["idDocIdentidad"];?>
							<input name="IdDocIdentidadRN<?php echo $nac;?>" type="hidden" id="IdDocIdentidadRN<?php echo $nac;?>" value="<?php echo $item4["idDocIdentidad"];?>" />										  
						</td>

						<td>							
							<?php echo $item4["docIdentidad"];?>
							<input name="docIdentidad<?php echo $nac;?>" type="hidden" id="docIdentidad<?php echo $nac;?>" value="<?php echo $item4["docIdentidad"];?>" />	
							<input name="apgar_1<?php echo $nac;?>" type="hidden" id="apgar_1<?php echo $nac;?>" value="<?php echo $item4["apgar_1"];?>" />	
							<input name="apgar_5<?php echo $nac;?>" type="hidden" id="apgar_5<?php echo $nac;?>" value="<?php echo $item4["apgar_5"];?>" />										  
						</td>
												
					  </tr>
      
                <?php  } }?>

				<input type='hidden' name='contadorNac' id='contadorNac' value='<?php echo $nac;?>'>			  
			  
		  </table>
		</td> 
      </tr>	      			
     
    </table> 
	
</fieldset>
	
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DIAGNÓSTICOS DE MUERTE FETAL (Todos Dx)</strong></legend>
    <table width="848" id="tablaDiagnosticosEgresoMortFetal"><br>      
      <tr>
		<td><strong>Tipo</strong></td>
        <td><strong>Código</strong></td>
        <td colspan="2"><strong>Descripción</strong></td>
      </tr>
      <tr>
		<td>
		  	<select name="IdSubclasificacionDxMortFetal" id="IdSubclasificacionDxMortFetal"  style="width: 155px" class="Combos" >              
              <?php 
				$DxHospMortalidad=SubclasificacionDiagnosticosSeleccionarDxHospMuerteFetal_M();
				if($DxHospMortalidad != NULL)  { 
				foreach($DxHospMortalidad as $item2){?>
				  <option value="<?php echo $item2["IdSubclasificacionDx"]?>"><?php echo $item2["DescripcionLarga"]?>
				  </option>              
				<?php } }?>
            </select>		  
		 </td>  
        <td width="126"><input name="BuscarCodigoCIE10MortFetal" type="text"  id="BuscarCodigoCIE10MortFetal"   size="20"  class="texto"    /></td>
        <td colspan="2"><input name="descripcionCIE10MortFetal" type="text" id="descripcionCIE10MortFetal" size="60" class="texto" />
          <input name="CodigoCIE10MortFetal" type="hidden"  id="CodigoCIE10MortFetal" size="10" readOnly  />
          <input type="hidden" name="IdDiagnosticoEgresoMortFetal" id="IdDiagnosticoEgresoMortFetal">
        </td>
      </tr> 
		
					<?php
					//$ListaDiagnostico_idatencionNacimiento=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'5');
					$n=0;
                    if($ListaDiagnostico_idatencionNacimiento != NULL)
                    {						
						foreach($ListaDiagnostico_idatencionNacimiento as $item4){   
						$n++;
                    ?>                    
 
					  <tr id="Nac<?php echo $n;?>">
						<td>
						  <?php echo $item4["DescripcionTipoDx"];?>
						  <input name="IdSubclasificacionDxMortFetal<?php echo $n;?>" type="hidden" id="IdSubclasificacionDxMortFetal<?php echo $n;?>" value="<?php echo $item4["IdTipoDiagnostico"];?>" size="20">			 		
						 </td> 

						<td><input name="BuscarCodigoCIE10MortFetal<?php echo $n;?>" type="text" disabled id="BuscarCodigoCIE10MortFetal<?php echo $n;?>" value="<?php echo $item4["CodigoCIE2004"];?>" size="20"></td>
						<td width="648" align="left">
							<?php echo $item4["Descripcion"];?>
							<input type="hidden" name="IdDiagnosticoEgresoMortFetal<?php echo $n;?>" id="IdDiagnosticoEgresoMortFetal<?php echo $n;?>" value="<?php echo $item4["IdDiagnostico"];?>">									  
						</td>
						<td width="58" align="left"><img src="../../MVC_Complemento/img/user_logout.png" width="16" height="16" onclick='eliminarRowDiagnosticosMortFetal(this)'></td>
					  </tr>
      
                <?php  } }?>
	 
	 		   <input type='hidden' name='contadorN' id='contadorN' value='<?php echo $n;?>'>
 
    </table>    
</fieldset>		

</div>
	 
<div id="Mortalidad" class="tabcontent">
  <h3>3.4 Mortalidad</h3>
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DIAGNÓSTICOS DE MORTALIDAD (Todos Dx)</strong></legend>
    <table width="848" id="tablaDiagnosticosEgresoMort"><br>      
      <tr>
		<td><strong>Tipo</strong></td>
        <td><strong>Código</strong></td>
        <td colspan="2"><strong>Descripción</strong></td>
      </tr>
      <tr>
		<td>
		  	<select name="IdSubclasificacionDxMort" id="IdSubclasificacionDxMort"  style="width: 155px" class="Combos" >              
              <?php 
				$DxHospMortalidad=SubclasificacionDiagnosticosSeleccionarDxHospMortalidad_M();
				if($DxHospMortalidad != NULL)  { 
				foreach($DxHospMortalidad as $item2){?>
				  <option value="<?php echo $item2["IdSubclasificacionDx"]?>"><?php echo $item2["DescripcionLarga"]?>
				  </option>              
				<?php } }?>
            </select>		  
		 </td>  
        <td width="126"><input name="BuscarCodigoCIE10Mort" type="text"  id="BuscarCodigoCIE10Mort"   size="20"  class="texto"    /></td>
        <td colspan="2"><input name="descripcionCIE10Mort" type="text" id="descripcionCIE10Mort" size="60" class="texto" />
          <input name="CodigoCIE10Mort" type="hidden"  id="CodigoCIE10Mort" size="10" readOnly  />
          <input type="hidden" name="IdDiagnosticoEgresoMort" id="IdDiagnosticoEgresoMort">
        </td>
      </tr>		
		
        			<?php
					//$ListaDiagnostico_idatencionMortalidad=AtencionesDiagnosticosSeleccionarTodosPorIdAtencionYClase_M($IdAtencion,'4');
					$k=0;
                    if($ListaDiagnostico_idatencionMortalidad != NULL)
                    { 						
						foreach($ListaDiagnostico_idatencionMortalidad as $item4){   
						$k++;
                    ?>                    
 
					  <tr id="Mor<?php echo $k;?>">
						<td>
						  <?php echo $item4["DescripcionTipoDx"];?>
						  <input name="IdSubclasificacionDxMort<?php echo $k;?>" type="hidden" id="IdSubclasificacionDxMort<?php echo $k;?>" value="<?php echo $item4["IdTipoDiagnostico"];?>" size="20">			 		
						 </td> 

						<td><input name="BuscarCodigoCIE10Mort<?php echo $k;?>" type="text" disabled id="BuscarCodigoCIE10Mort<?php echo $k;?>" value="<?php echo $item4["CodigoCIE2004"];?>" size="20"></td>
						<td width="648" align="left">
							<?php echo $item4["Descripcion"];?>
							<input type="hidden" name="IdDiagnosticoEgresoMort<?php echo $k;?>" id="IdDiagnosticoEgresoMort<?php echo $k;?>" value="<?php echo $item4["IdDiagnostico"];?>">									  
						</td>
						<td width="58" align="left"><img src="../../MVC_Complemento/img/user_logout.png" width="16" height="16" onclick='eliminarRowDiagnosticosMort(this)'></td>
					  </tr>
      
                <?php  } }?>
		
			   <input type='hidden' name='contadorK' id='contadorK' value='<?php echo $k;?>'> 
	 		   
    </table>    
</fieldset>	
	
</div>	 


 </form>

<!--<div id="DetalleOrden">
  <iframe id="Panet_Datos" name="Panet_Datos" width="0" height="0" frameborder="0">
  	<ilayer width="0" height="0" id="Panet_Datos" name="Panet_Datos"></ilayer>
	</iframe>
</div>-->
          
</body>         