<html>
<head>
	<meta charset="UTF-8">	
	<!-- <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/blue/screen.css"/>

	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	<script src="../../MVC_Vista/Emergencia/ConsultarAntimicrobiano.js"></script>  -->

	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.numeric.js"></script>
	<script src="../../MVC_Complemento/tinymce/tinymce.min.js"></script>
	<script src="../../MVC_Complemento/tinymce/langs/es.js"></script>

	<script src="../../MVC_Vista/Emergencia/ConsultarAntimicrobiano.js"></script>

	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
	<link rel="stylesheet" href="../../MVC_Complemento/css/antimicrobianos_estilo.css">
	<!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/blue/screen.css"/>--> 

	<script language="javascript">
		// Script que se ejecutaran al cargar la Pagina  
		$(function(){
			$('#Contenedor_Consultar').window('close'); 
			$('#Detalle_Orden').window('close'); 
		});

		function Cerrar_Ver_SolicAntim() {
		 	parent.googlebox.hide();
		}

	</script>

	<style>
		html,body { 
			padding: 0px;
			margin: 0px;
			height: 100%;
			width: 100%;
			font-family: 'Helvetica'; 			
		}
		.mayus>input{
			text-transform: capitalize;
		}	
		
		.tabledata 
		{
			text-align: left;
			margin: auto;
			padding: auto;
		}
	
	</style>

</head>

<body>										
										
		
		 <?php 
	      //echo $NroHistoriaClinica;
		   //$IdPaciente='1399582';
	        $Mostrar_Lista_Solicitudes=SigesaAtencionesDatosAtencion($NroHistoriaClinica);
		     if($Mostrar_Lista_Solicitudes != NULL){ 		     
		    			     
	     ?>	
         
<h3><?php echo "Paciente: ".$Mostrar_Lista_Solicitudes[0]["ApellidoPaterno"];?> <?php echo $Mostrar_Lista_Solicitudes[0]["ApellidoMaterno"];?>, <?php echo $Mostrar_Lista_Solicitudes[0]["PrimerNombre"];?></h3>

	     	<table  border="1" cellpadding="0" cellspacing="0" class="tabledata">
		     <tr>
               <th >Nro</th>  
		       <th >Cuenta Atencion</th>              
		       <th > Fecha Ingreso </th>
		       <th >Tipo Servicio</th>
		       <th >Servicio Ingreso</th>
		       <th >Fuente</th>
		       <!-- <th >Fec.Nacito</th> -->
		       <th >Examenes de Lab.</th>
		     </tr>
		       <?php 	      
		       	    $i=0;
					foreach($Mostrar_Lista_Solicitudes as $item)   {
					$i+=1;

					//$TieneExamenes=SigesaFactOrdenServicioDatosAtencion($NroHistoriaClinica);		
		     		$FactOrdenServicio=SigesaFactOrdenServicio($item["IdCuentaAtencion"]); 
		       ?>
		     <tr>
               <td><?php echo $i;?></td>
		       <td><b><?php echo $item["IdCuentaAtencion"];?></b></td>
		       <td><?php echo vfecha(substr($item["FechaIngreso"],0,10)).' '.$item["HoraIngreso"];?></td>
		       <td><?php echo $item["TipoServicio"];?></td>
		       <td><?php echo strtoupper($item["ServicioIngreso"]);?></td>
		       <td><?php echo $item["FuentesFinanciamiento"];?></td>
		     
		       <td>
		       		<?php 
		       			if($FactOrdenServicio!=NULL){
		       		?>
		        	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true"  onclick="Consultar('<?php echo $item["IdCabeceraAntimicrobiano"];?>','<?php echo $item["IdCuentaAtencion"];?>','<?php echo $item["NroHistoriaClinica"];?>')"  id="Consultar">CONSULTAR</a>
		        	<?php 
		       			}
		       		?>
		        </td>

		     </tr>
		       <?php } ?>

		      <tr align="center"><td colspan="7"><span style="text-align: center"><img src="../../MVC_Complemento/img/botoncerrar.jpg" width="85" height="22" onClick="Cerrar_Ver_SolicAntim();"/></span></td></tr>
		    </table>

			 <?php 

		  }else{
		  	echo "EL PACIENTE <b>".$_REQUEST["Paciente"]. "</b> NO TIENE EXAMENES DE LABORATORIO";
		  }

		?>          
            		  
		   <div id="Contenedor_Consultar" class="easyui-window" title="Historial de Atenciones del Paciente" data-options="iconCls:'icon-save'" style="width:90%;height:auto;margin:0;" >	           		
				<?php   //include'../../MVC_Vista/Emergencia/Consultar_Registro.php';  ?>
                <!--<div title="Examenes de Laboratorio" style="padding:17px 25px 25px 25px;background:#FAFAFA;">-->
                    <table id="Examenes_Antimicrobianos_Visualizar" style="height:auto;"></table>
                <!--</div>-->
		   </div>	
           
            <!-- Contenedor de Resultados por Examen CPT  -->
			<div id="Detalle_Orden" class="easyui-window" data-options="modal:true,iconCls:'icon-man',footer:'#Pie_Detalle'" style="width:650px;height:405px;margin:0;">
				<div class="easyui-panel" style="width:620px;height:360px;margin:0;">
						<div class="easyui-layout" data-options="fit:true">
						<div data-options="region:'center',split:true" >
							<div id="tabla_resultados_kk">
							</div>
						</div>
						</div>
				</div>
			</div>			
</body>
</html>