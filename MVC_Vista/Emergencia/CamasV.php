    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<script src="../../MVC_Vista/Emergencia/ConsultarAntimicrobiano.js"></script>
		<link rel="stylesheet" href="../../MVC_Complemento/css/antimicrobianos_estilo.css">
		<link rel="stylesheet" href="../../MVC_Complemento/css/boton.css">
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		
		label[for=message]
		{
		font-weight:bold;
		}
		
		@keyframes click-wave {
		  0% {
			height: 25px;
			width: 25px;
			opacity: 0.35;
			position: relative;
		  }
		  100% {
			height: 200px;
			width: 200px;
			margin-left: -60px;
			margin-top: -70px;
			opacity: 0;
		  }
		}

		.option-input {
		  -webkit-appearance: none;
		  -moz-appearance: none;
		  -ms-appearance: none;
		  -o-appearance: none;
		  appearance: none;
		  top: 13.33333px;
		  right: 0;
		  bottom: 0;
		  left: 0;
		  height: 25px;
		  width: 25px;
		  transition: all 0.15s ease-out 0s;
		  background: #cbd1d8;
		  border: none;
		  color: #fff;
		  cursor: pointer;
		  display: inline-block;
		  margin-right: 0.5rem;
		  outline: none;
		}
		.option-input:hover {
		  background: #9faab7;
		}
		.option-input:checked {
		  background: #40e0d0;
		}
		.option-input:checked::before {
		  height: 25px;
		  width: 25px;
		  content: '✔';
		  display: inline-block;
		  font-size: 14.66667px;
		  text-align: center;
		  line-height: 24px;
		}
		.option-input:checked::after {
		  -webkit-animation: click-wave 0.65s;
		  -moz-animation: click-wave 0.65s;
		  animation: click-wave 0.65s;
		  background: #40e0d0;
		  content: '';
		  
		}
		.option-input.radio {
		  border-radius: 50%;
		}
		.option-input.radio::after {
		  border-radius: 50%;
		}
		
		</style>

		<script>
			// Script que se ejecutaran al cargar la Pagina  
			$(function(){
				$('#Contenedor_Consultar').window('close'); 
				$('#Detalle_Orden').window('close'); 
				$('#dlg_Rotacion').dialog('close');
			});

			$(document).ready(function(){

				$("#btnRotacion").click(function(event) {
						var row = $('#Lista_Pacientes').datagrid('getSelected');
						var IdServicio=$( "#Servicio option:selected" ).val();
						if (row){
						$('#dlg_Rotacion').dialog('open');
						Rotacion_Lista_Disponibles(row.IdCama,row.IdAtencion,row.IdPaciente,IdServicio);
						}
				});



				$("#btnLimpiarCamas").click(function(event) {
						var IdServicio=$( "#Servicio option:selected" ).val();
						$.messager.confirm('Sigesa', 'Estas completamente seguro?', function(r){
			                if (r){
			                    $.post("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Limpiar_Camas", { IdServicio: IdServicio }, function(data){
                				}); 
			                }
			            });
				});


            
				function Rotacion_Lista_Disponibles(IdCama,IdAtencion,IdPaciente,IdServicio){
					var dg = $('#Lista_Rotacion_Pacientes').datagrid({
						filterBtnIconCls:'icon-filter',
						singleSelect:true,
						rowtexts:true,
						pagination: true,
						pageSize:20,
						url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Rotacion_Lista_Disponibles&IdPaciente='+IdPaciente+'&IdServicio='+IdServicio,
						columns:[[
						{field:'Cama',title:'Cama',width:55,sortable:true,align:'center'},
						{field:'NroHistoriaClinica',title:'H. Clinica',width:80,sortable:true,align:'center'},
						{field:'ApellidoPaterno',title:'Apellido Paterno',width:140},
						{field:'ApellidoMaterno',title:'Apellido Materno',width:140},
						{field:'PrimerNombre',title:'Primer Nombre',width:140}
						]],
						onSelect:function(index,row){
							$.messager.confirm('Confirmacion','Esta seguro de Realizar la Rotacion?',function(r){
							if (r){
							//Primera Rotacion
							Modificacion_Rotacion_Camas(row.IdCama,IdAtencion,IdPaciente);
							//Segunda Rotacion
							Modificacion_Rotacion_Camas_II(IdCama,row.IdAtencion,row.IdPaciente);
							//Cerrar Ventana de Rotacion
							$('#dlg_Rotacion').dialog('close');	
							$('#Lista_Pacientes').datagrid('reload');   
							}
							});
						}
						});
				}


				function Modificacion_Rotacion_Camas(IdCama,IdAtencion,IdPaciente){	
                $.post("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Modificacion_Rotacion_Camas", { IdCama: IdCama ,IdAtencion: IdAtencion,IdPaciente: IdPaciente }, function(data){
                });  
                }  

				function Modificacion_Rotacion_Camas_II(IdCama,IdAtencion,IdPaciente){
                $.post("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Modificacion_Rotacion_Camas", { IdCama: IdCama ,IdAtencion: IdAtencion,IdPaciente: IdPaciente }, function(data){
                });  
                } 





				$("#btnExportExcel").click(function(e) {
					var htmltable= document.getElementById('Contenido_Excel');
					var html = htmltable.outerHTML;
					//add more symbols if needed...
					while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
					while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
					while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
					while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
					while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
					while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
					window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
				});


				$("#btnExportPDF").click(function(event) {
					var Servicio=$( "#Servicio option:selected" ).val();
					var Servicio_Nombre= $("#Servicio option:selected").html();
					location="../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=PDF_CamasEmergencia&IdServicio="+Servicio+"&NombreServicio="+Servicio_Nombre;
				});


			});	


			$(document).ready(function() {
				$('.Contenedor_Reporte_I').hide();
				$('#dlg').dialog('close');
				
				/* Da Formato correcto al easyui  */
				
				$('.easyui-datebox').datebox({
					formatter : function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
					},
					parser : function(s){

						if (!s) return new Date();
						var ss = s.split('-');
						var y = parseInt(ss[2],10);
						var m = parseInt(ss[1],10);
						var d = parseInt(ss[0],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d)
						} else {
							return new Date();
						}
					}

				});				
									
				function ImprimirListaPacientesEmergencia()
				{
					var Servicio=$( "#Servicio option:selected" ).val();
					var Tipo = $("input[name='Tipo']:checked").val();
					var newWin = window.open();
					$.ajax({
						url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Imprimir_Lista_Pacientes_Emergencia',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte,
							Servicio:Servicio,
							Tipo:Tipo,
							Turno:Turno
						},
						success:function(impresion)
						{
							newWin.document.write(impresion);
							newWin.document.close();
							newWin.focus();
							newWin.print();
							newWin.close();
						}
					});
					
				}
											
				function Lista_Pacientes(Servicio){
									var dg = $('#Lista_Pacientes').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Lista_Camas_Pacientes_Emergencia&IdServicio='+Servicio,
									columns:[[
											{field:'Codigo',title:'Nro de <br>Cama',width:60,sortable:true,align:'center'},
											{field:'HistoriaClinica',title:'H. Clinica',width:65,sortable:true},
											{field:'ApellidoPaterno',title:'Apellido Paterno',width:110,sortable:true},
											{field:'ApellidoMaterno',title:'Apellido Materno',width:110,sortable:true},
											{field:'PrimerNombre',title:'Primer Nombre',width:90,sortable:true},	
											{field:'Edad',title:'Edad',width:50,sortable:true,align:'center'},	
										    {field:'FechaOcupacion',title:'Fecha de <br>Ingreso',width:75,sortable:true},	
										    {field:'Estancia',title:'Estancia',width:60,sortable:true,align:'center',formatter:formatPrice}								
										]],
									onSelect:function(index,row){
										getSelected(row.IdAtencion);
										Cargar_Servicios_Visualizar(row.IdCuentaAtencion);
										Movimientos_Pacientes(row.IdPaciente);
										Antimicrobianos_Pacientes(row.IdPaciente);
										cargar_grilla_medicamentos_ambulatorio(row.IdCuentaAtencion);
										cargar_grilla_Imagenologia(row.IdCuentaAtencion);
									}
									});
				}	


 				function Camas_Disponibles(Servicio){
                $.post("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Camas_Disponibles_Observacion", { Servicio: Servicio }, function(data){
				$("#Camas_Disponibles").html(data);
                });  
                }  

				function Camas_Ocupadas(Servicio){
                $.post("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Camas_Ocupadas_Observacion", { Servicio: Servicio }, function(data){
				$("#Camas_Ocupadas").html(data);
                });  
                }  
   		


				function formatPrice(val,row){
				    if (val <= 1){
				        return '<span style="color:#31A018;font-weight:bold;font-size:14px">'+val+'</span>';
				    } 
  					if (val== 2){
				        return '<span style="color:#FFC300;font-weight:bold;font-size:14px">'+val+'</span>';
				    } 
				    else {
				       return '<span style="color:#FC2020;font-weight:bold;font-size:14px">'+val+'</span>';
				    }
				}
				
				$("#Generar_Reporte_I").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Emergencia');
					$('.Contenedor_Reporte_I').show();
					var Servicio=$( "#Servicio option:selected" ).val();
					var Servicio_Nombre= $("#Servicio option:selected").html();
					$('#contenido_de_titulo').html(Servicio_Nombre);
					Lista_Pacientes(Servicio);
					Camas_Disponibles(Servicio);
					Camas_Ocupadas(Servicio);
				});
				
								
				
				function Exportar_Reporte_Excel(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Servicio,Tipo,Turno)
				{
					$.ajax({
						url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Exportar_Reporte_Excel',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte,
							Servicio:Servicio,
							Tipo:Tipo,
							Turno:Turno
						},

						success:function(impresion)
						{
						 window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
						 e.preventDefault();
						 alert(impresion);
						}
					});

				}
				
				
				$("#Generar_Reporte_Matricial").click(function(event) {
					ImprimirListaPacientesEmergencia();
				});
				
				$("#Generar_Reporte_Excel").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var Servicio=$( "#Servicio option:selected" ).val();
					var Tipo = $("input[name='Tipo']:checked").val();
					var Turno=$( "#Turno option:selected" ).val();
					Exportar_Reporte_Excel(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Servicio,Tipo,Turno);
				});
				
			
				
			});
		</script>
		
		
		
		
		<script type="text/javascript">
			function getSelected(IdAtencion){
				var dg = $('#Lista_Servicios_por_Paciente').datagrid({
					filterBtnIconCls:'icon-filter',
					singleSelect:true,
					rowtexts:true,
					pagination: true,
					pageSize:20,
					url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Lista_Servicios_por_Cama_Paciente_Emergencia&IdAtencion='+IdAtencion,
					columns:[[
					{field:'Secuencia',title:'Id',width:20,sortable:true},
					{field:'FechaOcupacion',title:'Fecha<br>Ingreso',width:180,sortable:true},
					{field:'HoraOcupacion',title:'Hora<br>Ingreso',width:50,sortable:true},
					{field:'Codigo',title:'Cama',width:42,sortable:true,align:'center'},
					{field:'Estancia',title:'Estancia',width:60,sortable:true,align:'center'},
					{field:'Nombre',title:'Nombre de Servicio',width:200,sortable:true,align:'center'}	
					]]
					});
			}
			

			function Movimientos_Pacientes(IdPaciente){
				var dg = $('#Movimientos_Pacientes').datagrid({
					filterBtnIconCls:'icon-filter',
					singleSelect:true,
					rowtexts:true,
					pagination: true,
					pageSize:20,
					url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Lista_Movimientos_Paciente&IdPaciente='+IdPaciente,
					columns:[[
					{field:'FechaIngreso',title:'Fecha de <br>Ingreso',width:90,sortable:true},
					{field:'HoraIngreso',title:'Hora de <br>Ingreso',width:60,sortable:true},
					{field:'Nombre',title:'Nombre <br>de Servicio',width:210,sortable:true},
					{field:'Descripcion',title:'Tipo de <br>Servicio',width:160,sortable:true}
					]],
					onSelect:function(index,row){
						$('#dlg').dialog('open');
						Diagnosticos_Atencion(row.IdAtencion);
					}
					});
			}
			





			function Antimicrobianos_Pacientes(IdPaciente)
			{											
					var dg =$('#Antimicrobianos_Pacientes').datagrid({
					iconCls:'icon-edit',
					singleSelect:true,
					rownumbers:true,
					url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Lista_Antimicrobianos_Cargados_Paciente&IdPaciente='+IdPaciente,
					columns:[[
					{field:'FechaRegistro',title:'Fecha de<br> Registro',width:90},
					{field:'Nombre',title:'Nombre',width:220},
					{field:'Dosis',title:'Dosis',width:50},
					{field:'Frecuencia',title:'Frecuencia',width:70,align:'center'},
					{field:'Dias',title:'Dias',width:50},
					{field:'Estado',title:'Estado',width:100}		
					]]											
					});	

			}


            
			function Diagnosticos_Atencion(IdAtencion){
				var dg = $('#Diagnosticos_Atencion').datagrid({
					filterBtnIconCls:'icon-filter',
					singleSelect:true,
					rowtexts:true,
					pagination: true,
					pageSize:20,
					url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Diagnosticos_Atencion&IdAtencion='+IdAtencion,
					columns:[[
					{field:'CodigoCIE10',title:'Codigo',width:55,sortable:true},
					{field:'Descripcion',title:'Diagnostico ',width:455,sortable:true},
					{field:'Clasificacion',title:'Tipo de Diagnostico ',width:148,sortable:true}
					]]
					});
			}


			function cargar_grilla_medicamentos_ambulatorio(IdCuentaAtencion)
			{											
					var dg =$('#consumo_medicamentos_cuenta').datagrid({
					iconCls:'icon-edit',
					singleSelect:true,
					rownumbers:true,
					pagination: true,
					pageSize:10,
					url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=FarmaciaEstadoCuenta&IdCuentaAtencion='+IdCuentaAtencion,
					columns:[[
					{field:'FechaHoraPrescribe',title:'Fecha Carga',width:90},
					{field:'Codigo',title:'Codigo',width:75},
					{field:'Nombre',title:'Medicamento',width:300},
					{field:'cantidad',title:'Cant',width:50,align:'center'},
					{field:'precio',title:'Pr. Unit.',width:55},			
					{field:'total',title:'Total',width:85}	
					]]											
					});	


			}




			function cargar_grilla_Imagenologia(IdCuentaAtencion)
			{			
					// Cargar Ris								
					var dg =$('#Ris_Imagenes_cuenta').datagrid({
					iconCls:'icon-edit',
					singleSelect:true,
					rownumbers:true,
					url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Imagenologia&IdCuentaAtencion='+IdCuentaAtencion,
					columns:[[
					{field:'codigo',title:'Codigo',width:75},
					{field:'Nombre',title:'Medicamento',width:500}
					]],
					onSelect:function(index,row){
						Mostrar_Url_Imagen(row.ResultInformeFormato_RIS);
					}
													
					});	

					// Cargar Pac
					var dg =$('#Pac_Imagenes_cuenta').datagrid({
					iconCls:'icon-edit',
					singleSelect:true,
					rownumbers:true,
					url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Imagenologia&IdCuentaAtencion='+IdCuentaAtencion,
					columns:[[
					{field:'codigo',title:'Codigo',width:75},
					{field:'Nombre',title:'Medicamento',width:500}
					]],
					onSelect:function(index,row){
						Mostrar_Pac_Imagen(row.ResultInformeImagen_PACS);
					}								
					});	
			}



			function Mostrar_Url_Imagen(url){
				window.open(url, 'download_window', 'toolbar=0,location=no,directories=0,status=0,scrollbars=0,resizeable=0,width=800,height=900,top=300,left=500');
				window.focus();	
			}


			function Mostrar_Pac_Imagen(url){
				window.open(url,  '_blank');
				window.focus();	
			}


			//Se le dara Estilo al campo de Exoneraciones	
			function Style_Exoneracion()
			{
			return 'background-color:#F9E5B0;font-weight:bold';
			}
		</script>
		
    </head>
    <body>
		<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
		<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
		

		
		<div id="dlg" class="easyui-dialog" title="Diagnosticos de Atencion" data-options="iconCls:'icon-save'" style="width:750px;height:230px;padding:12px">
					<div  style="width:95%;height:160px;">
					<table id="Diagnosticos_Atencion"></table>
					</div>
		</div>


		<div id="dlg_Rotacion" class="easyui-dialog" title="Lista de Pacientes " data-options="iconCls:'icon-save'" style="width:790px;height:450px;padding:12px">
			<div  style="width:95%;height:160px;">
				<table id="Lista_Rotacion_Pacientes"></table>
			</div>
		</div>


		<!-- Contenedor de Resultados por Examen CPT  -->
		<div id="Detalle_Orden" class="easyui-window" data-options="modal:true,iconCls:'icon-man',footer:'#Pie_Detalle'" style="width:650px;height:405px;margin:0;">
				<div class="easyui-panel" style="width:620px;height:360px;margin:0;">
						<div class="easyui-layout" data-options="fit:true">
						<div data-options="region:'center',split:true" >
							<div id="tabla_resultados_kk">
							</div>
						</div>
						</div>
				</div>
		</div>	
		
		<div class="easyui-layout" style="width:100%;height:100%;">
				<!-- Derecha  -->
				<div data-options="region:'center',iconCls:'icon-ok'"  style="width:84%;">
					<div class="easyui-tabs" data-options="tabWidth:100,tabHeight:60" style="width:100%;height:100%;">
						<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/lista.png' width='20' height='20'/>
						<br>Listado de<br> Pacientes</span>" style="padding:10px;height:850px">
							    <div class="easyui-layout" style="width:100%;height:100%;">
							        <div data-options="region:'east',split:true"  style="width:54%;height:100%">
											<div class="easyui-layout" style="width:100%;height:100%">
													<div data-options="region:'north'" title="Movimientos"  style="width:100%;height:25%">
														<table id="Lista_Servicios_por_Paciente"></table>		
													</div>
													<div data-options="region:'south',split:true" title="Datos Adicionales" style="width:100%;height:75%;">
															<div class="easyui-tabs" data-options="tabWidth:100,tabHeight:60" style="width:100%;height:100%;">
																	<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/Emergencia/laboratorio.png' width='35' height='35'/>
																				<br>Laboratorio</span>" style="padding:10px">
																		<table id="Examenes_Antimicrobianos_Visualizar" style="height:1000px;"></table>	
																	</div>
																	<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/Emergencia/medicamentos.png' width='35' height='35'/>
																				<br>Medicamentos</span>" 
																				style="padding:10px">
																			<table id="consumo_medicamentos_cuenta" style="height:1000px;"></table>	
																	</div>
																	<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/Emergencia/hospital.png' width='35' height='35'/>
																				<br>Historico</span>" 
																				style="padding:10px">
																			<table id="Movimientos_Pacientes" style="height:1000px;"></table>	
																	</div>
																	<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/Emergencia/antimicrobianos.png' width='35' height='35'/>
																				<br>Antimicrobiano</span>" 
																				style="padding:10px">
																			<table id="Antimicrobianos_Pacientes" style="height:1000px;"></table>
																	</div>
																	<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/Emergencia/imagenes_ris.png' width='35' height='35'/>
																				<br>RIS</span>" 
																				style="padding:10px">
																	<table id="Ris_Imagenes_cuenta" style="height:1000px;"></table>	
																	</div>
																	<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/Emergencia/imagenes_pac.png' width='35' height='35'/>
																				<br>PAC</span>" 
																				style="padding:10px">
																	<table id="Pac_Imagenes_cuenta" style="height:1000px;"></table>	
																	</div>
															</div>
													</div>
											</div>
							        </div>
							        <div data-options="region:'center',title:'Relacion de Pacientes',iconCls:'icon-ok'" style="width:46%;height:100%">	
							        		<div  id="Contenido_Excel">
							        		<div style="margin: 10px">
							        		<strong>Servicio :  <span id="contenido_de_titulo"> Aquí mi contenido</span></strong>
							        		</div>							
											<table id="Lista_Pacientes"></table>
											</div>				
							        </div>
							    </div>
						</div>
						<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/grafico.png' width='35' height='35'/>
						<br>Grafico</span>" 
						style="padding:10px">
							<div id="Contenedor_Reporte_I" class="Contenedor_Reporte_I" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
						</div>
					</div>
					<style scoped="scoped">
						.tt-inner{
							display:inline-block;
							line-height:12px;
							padding-top:5px;
						}
						.tt-inner img{
							border:0;
						}
					</style>
				</div>
				<!--   Izquierda   -->
				<div data-options="region:'west',split:true"  style="width:16%;">
						<div class="easyui-layout" style="width:100%;height:100%;">
						        <div data-options="region:'north'"  title="Observacion de Emergencia" style="height:280px">
									<div style="margin:5px 0px 0px 10px">
										<br>
										<span style="font-weight:bold; color:#414141">Servicio<br> de Emergencia :</span>
										<br>
										<select  name="Servicio" style="width:230px;margin:10px 0px 0px 0px" id="Servicio">
										<!--<option value="">Todos</option>-->
										<?php 
										$resultados=MostrarServiciosObservacion();								 
										if($resultados!=NULL){
										for ($i=0; $i < count($resultados); $i++) {	
										 ?>
										 <option value="<?php echo $resultados[$i]["IdServicio"] ?>" <?php if($_REQUEST["IdServicio"]==$resultados[$i]["IdServicio"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
										<?php 
										}}
										?>                   
										</select>
									</div>
									<div style="margin-bottom:10px;clear:both;margin-top:10px;">
										<div style="padding:10px;float:left;margin:5px">
											<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'"  style="width:72px;margin:0 1px 4px 0" id="Generar_Reporte_I">Vista Previa<br></a>
										    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:72px;margin:0 1px 4px 0" id="btnExportExcel">Generar Excel</a>
										    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:72px;margin:0 1px 7px 0" id="btnExportPDF">Generar PDF</a>
										    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:72px;margin:0 1px 4px 0" id="btnRotacion">Rotar Cama</a>
										    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:72px;margin:0 1px 4px 0" id="btnLimpiarCamas">Limpiar Camas</a>
										</div>
									</div>	
						        </div>
						        <div data-options="region:'west'" title="Camas Disponibles" style="width:50%;height:100%; ">
						        <br>
						        <center><div  id="Camas_Disponibles"></div></center>
						    	</div>
						        <div data-options="region:'center',title:'Camas Ocupadas'" style="width:50%;height:100%;">
						         <br>
						        <center><div  id="Camas_Ocupadas"></div> </center>
						        </div>
						</div>
				</div>
		</div>
    </body>
    </html>



