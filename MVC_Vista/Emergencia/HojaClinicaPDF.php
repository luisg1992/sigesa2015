<?php
require('../../MVC_Complemento/fpdf/fpdf.php');
//--include('../../MVC_Complemento/fpdf/php-barcode.php');
$IdAtencion=$_REQUEST['IdAtencion'];

$data=SIGESA_Emergencia_ImprimeHojaClinica_LCO($IdAtencion);
foreach($data as $item){
			$IdAtencion =$item['IdAtencion'];
			$IdCuentaAtencion =$item['IdCuentaAtencion'];
			$ApellidoPaterno =$item['ApellidoPaterno'];
			$ApellidoMaterno =$item['ApellidoMaterno'];
			$PrimerNombre =$item['PrimerNombre'];
			$NroHistoriaClinica =$item['NroHistoriaClinica'];
			$FechaNacimiento =$item['FechaNacimiento'];
			$FechaIngreso =$item['FechaIngreso'];
			$HoraIngreso =$item['HoraIngreso'];
			$Nombre =$item['Nombre'];
			$FuentesFinanciamiento =$item['FuentesFinanciamiento'];
			$IdServicio =$item['IdServicio'];
			$IdTipoServicio =$item['IdTipoServicio'];
			$IdOrigenAtencion =$item['IdOrigenAtencion'];
			$IdTipoGravedad =$item['IdTipoGravedad'];
			$Descripcion =$item['Descripcion'];
			$TipoGravedad =$item['TipoGravedad'];
			$NomDist =$item['NomDist'];
			$NomProv =$item['NomProv'];
			$NomDep =$item['NomDep'];
			$NombreAcompaniante =$item['acomp'];
			$DireccionDomicilio =$item['domiciliopaciente'];
			//$DiagnosticoIngreso=$item['DiagnosticoIngreso'];
			$Edad=$item['Edad'];
			
			$tipoAgresion=$item['IdTipoEvento'];
			$relacionaAgresion=$item['IdRelacionAgresorVictima'];
			$claseaccidente=$item['claseaccidente'];
			$lugaraccidente=$item['lugaraccidente'];
			/*
			1=pareja
2=padre
3=familiar
6=desconocido

8=fisico
6=arma fuego
3=arma blanca
diferenteanterior=otros
			*/
			
$dataDiagIngreso=SIGESA_Emergencia_DiagnosticosImprimeHojaClinica_LCO($IdAtencion,2);
foreach ($dataDiagIngreso as $itemDiagIng){
	$DiagnosticoIngreso=$itemDiagIng['Descripcion'].' ';
	}			
			
			
			
			if($tipoAgresion=='8'){
				$fisico='X';
				$armafuego=' ';
				$armablanca=' ';
				$otros=' ';
			}elseif($tipoAgresion=='6'){
				$fisico=' ';
				$armafuego='X';
				$armablanca=' ';
				$otros=' ';				
			}elseif($tipoAgresion=='3'){
				$fisico=' ';
				$armafuego=' ';
				$armablanca='X';
				$otros=' ';				
			}elseif($tipoAgresion!='3' && $tipoAgresion!='6' && $tipoAgresion!='8'){
				$fisico=' ';
				$armafuego=' ';
				$armablanca=' ';
				$otros='X';				
			}
			
			if($relacionaAgresion=='1'){
				$pareja='X';
				$padre=' ';
				$familiar=' ';
				$desconocido=' ';
				}elseif($relacionaAgresion=='2'){
				$pareja=' ';
				$padre='X';
				$familiar=' ';
				$desconocido=' ';
				}elseif($relacionaAgresion=='3'){
				$pareja=' ';
				$padre=' ';
				$familiar='X';
				$desconocido=' ';
				}elseif($relacionaAgresion=='6'){
				$pareja=' ';
				$padre=' ';
				$familiar=' ';
				$desconocido='X';
				}
				
			
			
			
			
			
			
			
		
	}
	
	
	
	
$dataTriaje=SIGESA_Emergencia_TriajeImprimeHojaClinica_LCO($IdAtencion);
foreach($dataTriaje as $itemT){
			$TriajeEdad =$itemT['TriajeEdad'];
			$TriajePresion =$itemT['TriajePresion'];
			$TriajeTalla =$itemT['TriajeTalla'];
			$TriajeTemperatura =$itemT['TriajeTemperatura'];
			$TriajePeso =$itemT['TriajePeso'];
			$TriajeFecha =$itemT['TriajeFecha'];
			$TriajeIdUsuario =$itemT['TriajeIdUsuario'];
			$TriajePulso =$itemT['TriajePulso'];
			$TriajeFrecRespiratoria =$itemT['TriajeFrecRespiratoria'];
			$TriajeFrecCardiaca=$itemT['TriajeFrecCardiaca'];
}






class PDF extends FPDF{
	function Header(){	
	
	
	$datafecha=RetornaFechaServidorSQL();

$fechahora=$datafecha[0]['FechaHoraSQL'];
$xfecha=substr($fechahora,0,10);
$fecha=vfecha($xfecha);
$hora=substr($fechahora,10,9);
	
		$this->SetY(5);
		$this->SetFont('Arial','',7);
		$this->Cell(24,6,' ',0,0,'C');
		$this->Image('../../MVC_Complemento/img/hndac.jpg',10,3,18);	
		//$this->Image('img/region_callao.jpg',40,8,18,20);
		$this->SetFont('Arial','B',8);
		$this->Cell(142,5,utf8_decode('HOSPITAL NACIONAL DANIEL ALCIDES CARRIÓN'),0,0,'C');
		$this->SetFont('Arial','B',7);
		$this->Cell(30,5,utf8_decode('Fecha: '.$fecha),0,0,'C');
		$this->ln(2.5);
		$this->Cell(361,5,utf8_decode('Hora:'.$hora),0,0,'C');
		$this->ln(0.5);
		$this->Cell(360,10,'Pagina: '.$this->PageNo().' de {nb}',0,0,'C');
		//$this->Image('img/hndac.jpg',151,8,18,20);	
		$this->SetFont('Arial','',5);
		$this->SetX(170.5);
		//$this->Ln(7.5);
		$this->Ln(3);

		$this->SetFont('Arial','',12);
		$this->Ln(1);
		$this->SetX(70);
		$this->Cell(70,10,utf8_decode('HOJA CLINICA DE EMERGENCIA'),0,0,'C');
		$this->Ln(6);

	}
	
function TextWithRotation($x, $y, $txt, $txt_angle, $font_angle=0)
    {
        $font_angle+=90+$txt_angle;
        $txt_angle*=M_PI/180;
        $font_angle*=M_PI/180;
    
        $txt_dx=cos($txt_angle);
        $txt_dy=sin($txt_angle);
        $font_dx=cos($font_angle);
        $font_dy=sin($font_angle);
    
        $s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',$txt_dx,$txt_dy,$font_dx,$font_dy,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
        if ($this->ColorFlag)
            $s='q '.$this->TextColor.' '.$s.' Q';
        $this->_out($s);
    }
	
	function Footer(){	
		/*$this->Ln(2);	
		$this->SetLineWidth(0.1);
		 $this->Cell(17,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		 $this->Cell(15,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		 $this->Cell(15,13,'',0,0,'C');
		$this->Cell(44,13,'',1,0,'C');
		
		$this->Ln(13);
		$this->Cell(17,5,'',0,0,'C');
		$this->Cell(44,5,'PRESIDENTE DEL CMCI',1,0,'C');
		 $this->Cell(15,5,'',0,0,'C');
		$this->Cell(44,5,'MIEMBRO DEL CMCI',1,0,'C');
		 $this->Cell(15,5,'',0,0,'C');
		$this->Cell(44,5,'MIEMBRO DEL CMCI',1,0,'C');	
		
		*/
		

			}
	
}


// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();


//$pdf->Cell(180,3,'BENEFICIOS',1,1,'C','true'); 


	$pdf->SetFont('Arial','B',8);
	
$pdf->Cell(190,10,utf8_decode('1.-DATOS DEL PACIENTE'),0,0,'L');
$pdf->Ln(7);


$pdf->SetDrawColor(24,53,93);
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor(24,53,93);

$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,0);
$pdf->cell(40,5,'Nro Historia',0,0,'L');
$pdf->cell(80,5,':'.$NroHistoriaClinica.' - automatica',0,0,'L');
$pdf->cell(20,5,'Nro Cuenta',0,0,'L');
$pdf->SetFont('Arial','B',10);
$pdf->cell(60,5,':'.$IdCuentaAtencion.':'.$FuentesFinanciamiento,0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(35,5,'',0,0,'L');
$pdf->Ln(); 
$pdf->cell(40,5,'Apellidos Y Nombres',0,0,'L');
$pdf->SetFont('Arial','B',10);
$pdf->cell(90,5,':'.$ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre,0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(25,5,'',0,0,'L');
$pdf->cell(35,5,'',0,0,'L');
$pdf->ln(5);
$pdf->cell(40,5,'Fecha Ingreso',0,0,'L');
$pdf->cell(80,5,':'.$FechaIngreso,0,0,'L');
$pdf->cell(20,5,'Edad',0,0,'L');
$pdf->cell(60,5,utf8_decode(':'.$Edad.' AÑOS'),0,0,'L');
$pdf->cell(35,5,'',0,0,'L');

$pdf->ln(5);
$pdf->cell(40,5,'Hora Ingreso',0,0,'L');
$pdf->cell(80,5,':'.$HoraIngreso,0,0,'L');

//$pdf->cell(80,5,':'.$DiagnosticoIngreso,0,0,'L');
$pdf->cell(20,5,'Fecha Nac.',0,0,'L');

$pdf->cell(60,5,':'.$FechaNacimiento,0,0,'L');

$pdf->cell(35,5,'',0,0,'L');

$pdf->ln(5);
$pdf->cell(40,5,utf8_decode('Consultorio médico'),0,0,'L');
$pdf->cell(80,5,':'.utf8_decode($Nombre),0,0,'L');
$pdf->cell(20,5,'Sexo',0,0,'L');
$pdf->cell(60,5,':'.$Descripcion,0,0,'L');
$pdf->cell(35,5,'',0,0,'L');

$pdf->ln(5);
$pdf->cell(40,5,utf8_decode('Dirección'),0,0,'L');
$pdf->cell(90,5,':'.utf8_decode($DireccionDomicilio),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(25,5,'',0,0,'L');
$pdf->cell(35,5,'',0,0,'L');

$pdf->ln(5);
$pdf->cell(40,5,'Departamento',0,0,'L');
$pdf->cell(80,5,':'.utf8_decode($NomDep),0,0,'L');
$pdf->cell(20,5,'Provincia',0,0,'L');
$pdf->cell(60,5,utf8_decode(':'.$NomProv),0,0,'L');
$pdf->cell(35,5,'',0,0,'L');


$pdf->ln(5);
$pdf->cell(40,5,'Distrito',0,0,'L');
$pdf->cell(80,5,':'.utf8_decode($NomDist),0,0,'L');
$pdf->cell(20,5,'Centro Poblado',0,0,'L');
$pdf->cell(60,5,utf8_decode(':'),0,0,'L');
$pdf->cell(35,5,'',0,0,'L');

$pdf->ln(5);
$pdf->cell(40,5,utf8_decode('Acompañante'),0,0,'L');
$pdf->cell(80,5,':'.utf8_decode($NombreAcompaniante),0,0,'L');
$pdf->cell(20,5,'Parentesco',0,0,'L');
$pdf->cell(60,5,utf8_decode(':'),0,0,'L');
$pdf->cell(35,5,'',0,0,'L');
$pdf->ln(6);
	$pdf->SetFont('Arial','B',8);
	
$pdf->Cell(60,5,utf8_decode('2.-MOTIVO DE EMERGENCIA'),0,0,'L');
//$pdf->Ln(7);
$pdf->SetFont('Arial','',10);
$pdf->cell(7,5,utf8_decode('P.A'),0,0,'L');
$pdf->cell(15,5,':'.$TriajePresion,0,0,'L');
$pdf->cell(7,5,'F.C',0,0,'L');
$pdf->cell(15,5,':'.$TriajeFrecCardiaca,0,0,'L');
$pdf->cell(7,5,'F.R',0,0,'L');
$pdf->cell(15,5,':'.$TriajeFrecRespiratoria,0,0,'L');
$pdf->cell(4,5,utf8_decode('T°'),0,0,'L');
$pdf->cell(15,5,':'.$TriajeTemperatura,0,0,'L');
$pdf->ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(25,5,utf8_decode(' ANTECEDENTES'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(163,4,utf8_decode(''),'B',0,'L');
$pdf->ln(8);
/*$pdf->cell(188,4,utf8_decode(' '),'B',0,'L');
$pdf->ln(8);*/
$pdf->cell(188,4,utf8_decode(' '),'B',0,'L');
$pdf->ln(8);
$pdf->cell(188,4,utf8_decode(' '),'B',0,'L');



$pdf->ln(8);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(29,5,utf8_decode('3.-CAUSA EXTERNA'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(159,4,utf8_decode(''),'B',0,'L');
$pdf->ln(5);
$pdf->SetFont('Arial','',8);
$pdf->cell(159,4,utf8_decode('EXPLORACIÓN FISICA:'),0,0,'L');
$pdf->ln(4);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(40,5,utf8_decode('   LUGAR DEL ACCIDENTE'),0,0,'L');
$pdf->Cell(90,5,utf8_decode('   '.$lugaraccidente),0,0,'L');
$pdf->ln(4);
$pdf->Cell(40,5,utf8_decode('   DISTRITO'),0,0,'L');
$pdf->Cell(40,5,utf8_decode('   '),0,0,'L');

$pdf->Cell(25,5,utf8_decode('   PROVINCIA'),0,0,'L');
$pdf->Cell(30,5,utf8_decode('   '),0,0,'L');

$pdf->Cell(20,5,utf8_decode('   DISPOSITIVO'),0,0,'L');
$pdf->Cell(30,5,utf8_decode('   '),0,0,'L');

$pdf->ln(4);
$pdf->Cell(40,5,utf8_decode('   CLASE DE ACCIDENTE'),0,0,'L');
$pdf->Cell(40,5,utf8_decode('   '.$claseaccidente),0,0,'L');


$pdf->Cell(25,5,utf8_decode('   ACCIDENTADO'),0,0,'L');
$pdf->Cell(30,5,utf8_decode('   '),0,0,'L');

$pdf->Cell(20,5,utf8_decode('   SEGURIDAD'),0,0,'L');
$pdf->Cell(30,5,utf8_decode('   '),0,0,'L');
$pdf->ln(2);
$pdf->cell(188,4,utf8_decode(' '),'B',0,'L');


$pdf->ln(5);


$pdf->SetFont('Arial','B',8);
$pdf->cell(20,5,utf8_decode('AGRESION'),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,'FECHA',0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,'HORA',0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,utf8_decode(''),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,utf8_decode(''),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,utf8_decode(''),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->cell(15,5,utf8_decode('TIPO'),0,0,'L');
$pdf->cell(10,5,'Fisico',0,0,'L');
$pdf->cell(10,5,'('.$fisico.')',0,0,'L');
$pdf->cell(23,5,utf8_decode('Arma de Fuego'),0,0,'L');
$pdf->cell(10,5,'('.$armafuego.')',0,0,'L');
$pdf->cell(20,5,'Arma Blanca',0,0,'L');
$pdf->cell(10,5,'('.$armablanca.')',0,0,'L');
$pdf->cell(10,5,'Otros',0,0,'L');
$pdf->cell(10,5,'('.$otros.')',0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,utf8_decode(''),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->cell(15,5,utf8_decode('LUGAR'),0,0,'L');
$pdf->cell(10,5,':',0,0,'L');
$pdf->cell(10,5,'',0,0,'L');
$pdf->cell(20,5,utf8_decode('DISTRITO'),0,0,'L');
$pdf->cell(25,5,':',0,0,'L');
$pdf->cell(25,5,'PROVINCIA',0,0,'L');
$pdf->cell(10,5,utf8_decode(':'),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(10,5,utf8_decode(''),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,'',0,0,'L');
$pdf->cell(15,5,utf8_decode(''),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');


$pdf->ln(5);

$pdf->cell(20,5,utf8_decode('RELACION'),0,0,'L');
$pdf->cell(20,5,'AGRESOR',0,0,'L');
$pdf->cell(5,5,'',0,0,'L');
$pdf->cell(11,5,utf8_decode('Pareja'),0,0,'L');
$pdf->cell(10,5,'('.' '.$pareja.' '.')',0,0,'L');
$pdf->cell(10,5,'Padre',0,0,'L');
$pdf->cell(10,5,'('.' '.$padre.' '.')',0,0,'L');
$pdf->cell(13,5,'Familiar',0,0,'L');
$pdf->cell(10,5,'('.' '.$familiar.' '.')',0,0,'L');
$pdf->cell(21,5,'Desconocido',0,0,'L');
$pdf->cell(15,5,'('.' '.$desconocido.' '.')',0,0,'L');
$pdf->cell(15,5,utf8_decode(''),0,0,'L');
$pdf->cell(15,5,'',0,0,'L');

$pdf->SetXY(160,124);
$pdf->SetFont('Arial','',14);
$pdf->cell(38,10,utf8_decode('PRIORIDAD'),1,0,'C');
$pdf->SetXY(160,134);
$pdf->SetFont('Arial','',8);
$pdf->cell(38,10,utf8_decode($TipoGravedad),1,0,'C');


$pdf->ln(11);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(40,5,utf8_decode('4.-FUNCIONES BIOLOGICAS'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(148,4,utf8_decode(''),'B',0,'L');
$pdf->ln(8);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(45,5,utf8_decode('5.-DIAGNOSTICOS DE INGRESO'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(143,4,utf8_decode($DiagnosticoIngreso),'B',0,'L');
$pdf->ln(8);
$pdf->cell(188,4,utf8_decode(''),'B',0,'L');
$pdf->ln(8);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(29,5,utf8_decode('6.-TRATATAMIENTO'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(159,4,utf8_decode(''),'B',0,'L');
$pdf->ln(8);
$pdf->cell(188,4,utf8_decode(''),'B',0,'L');

$pdf->ln(8);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(39,5,utf8_decode('7.-EXAMENES AUXILIARES'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(149,4,utf8_decode(''),'B',0,'L');
$pdf->ln(8);
$pdf->cell(188,4,utf8_decode(''),'B',0,'L');


$pdf->ln(8);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(22,5,utf8_decode('8.-EVOLUCIÓN'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->cell(166,4,utf8_decode(''),'B',0,'L');
$pdf->ln(8);
$pdf->cell(188,4,utf8_decode(''),'B',0,'L');

$pdf->ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(22,5,utf8_decode('9.-DIAGNOSTICOS (ALTA)'),0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->ln(5);

$pdf->SetFont('Arial','',10);
$pdf->cell(133,5,utf8_decode('Descripción'),1,0,'C');
$pdf->cell(10,5,'P',1,0,'L');
$pdf->cell(10,5,'D',1,0,'L');
$pdf->cell(10,5,'R',1,0,'L');
$pdf->cell(25,5,utf8_decode('Código'),1,0,'C');
$pdf->ln(5);


$dataDiagEgreso=SIGESA_Emergencia_DiagnosticosImprimeHojaClinica_LCO($IdAtencion,3);
if($dataDiagEgreso!=NULL){
	$cantdiag=0;
	$j=0;
				for ($i=0; $i < 3; $i++) {	
				
					if($dataDiagEgreso[$i]["Descripcion"]!=''){
						$descripciondiagnostico=$dataDiagEgreso[$i]["Descripcion"];
						$codigoCie10=$dataDiagEgreso[$i]["CodigoCIE10"];
					$cantdiag=$cantdiag+1;
				
					$j=$j+1;
				
$pdf->cell(133,5,''.utf8_decode($descripciondiagnostico),1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(25,5,$codigoCie10,1,0,'C');
$pdf->ln(5);
				}
				
				}
}
if($cantdiag<=3){
	for($z=$cantdiag;$z < 3;$z++){

$pdf->cell(133,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(25,5,'',1,0,'C');
$pdf->ln(5);
	}
}
/*
$pdf->cell(133,5,'DX2:',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(25,5,'',1,0,'C');
$pdf->ln(5);
$pdf->cell(133,5,'DX1:',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(10,5,'',1,0,'L');
$pdf->cell(25,5,'',1,0,'C');
$pdf->ln(5);
*/					
	

$pdf->ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(60,5,utf8_decode('10.-EGRESOS'),0,0,'L');
$pdf->SetFont('Arial','',8);
$pdf->cell(15,5,utf8_decode('FECHA'),0,0,'L');
$pdf->cell(25,5,'',0,0,'L');
$pdf->cell(15,5,utf8_decode('HORA'),0,0,'L');
$pdf->cell(5,5,'',0,0,'L');
$pdf->ln(5);



$pdf->SetFont('Arial','B',8);
$pdf->cell(45,5,utf8_decode('CONDICIÓN DE EGRESADO:'),0,0,'L');
$pdf->SetFont('Arial','B',8);

$pdf->cell(4,5,utf8_decode('( )'),0,0,'L');
$pdf->cell(20,5,'Citado',0,0,'L');

$pdf->cell(4,5,'( )',0,0,'L');
$pdf->cell(20,5,utf8_decode('A su casa'),0,0,'L');

$pdf->cell(4,5,'( )',0,0,'L');
$pdf->cell(22,5,utf8_decode('Observacion'),0,0,'L');

$pdf->cell(4,5,'( )',0,0,'L');
$pdf->cell(25,5,'Hospitalizacion',0,0,'L');

$pdf->cell(4,5,utf8_decode('( )'),0,0,'L');
$pdf->cell(22,5,'Fallecido',0,0,'L');
$pdf->ln(5);

$pdf->cell(4,5,utf8_decode('( )'),0,0,'L');
$pdf->cell(36,5,'Sala de Operaciones',0,0,'L');

$pdf->cell(4,5,'( )',0,0,'L');
$pdf->cell(28,5,utf8_decode('Transferencia'),0,0,'L');

$pdf->cell(4,5,'( )',0,0,'L');
$pdf->cell(22,5,utf8_decode('Fuga'),0,0,'L');

$pdf->cell(4,5,'( )',0,0,'L');
$pdf->cell(33,5,'Retiro Voluntario',0,0,'L');

$pdf->cell(4,5,utf8_decode('( )'),0,0,'L');
$pdf->cell(22,5,'Llego Cadaver',0,0,'L');

$pdf->ln(6);

$pdf->SetFont('Arial','B',8);
$pdf->cell(10,5,utf8_decode(''),0,0,'L');
$pdf->cell(38,5,utf8_decode(''),'B',0,'L');
$pdf->cell(45,5,utf8_decode(''),'B',0,'L');
$pdf->ln(5);
$pdf->cell(45,5,utf8_decode('             Firma y Sello del Médico Responsable Atencion/Emergencia'),0,0,'L');



$pdf->SetXY(125,266.9);
$pdf->SetFont('Arial','B',10);
$pdf->cell(38,10,utf8_decode($ApellidoPaterno.' '.$ApellidoMaterno.' '.$PrimerNombre),0,0,'C');


$pdf->SetXY(20,266.9);
$pdf->SetFont('Arial','B',10);
$pdf->cell(38,10,$NroHistoriaClinica.' - Automatica',0,0,'C');


 /*
	$pdf->Image('img/hndac.jpg',40,8,18,20);
	$pdf->SetFont('Arial','B',10);
	$pdf->Ln(15);
	$pdf->Cell(190,10,utf8_decode('CERTIFICADO MÉDICO - DS - N° 166 - 2005 - EF'),0,0,'C');
	$pdf->Ln(7);
	$pdf->Cell(153,7,' ',0,0,'c');
	$pdf->SetFont('Arial','',9);
	$pdf->Cell(9,7,utf8_decode('Dia'),0,0,'C');
	$pdf->Cell(9,7,utf8_decode('Mes'),0,0,'C');
	$pdf->Cell(9,7,utf8_decode('Año'),0,0,'C');
	*/
/*	$pdf->SetFont('Arial','',9);
	$pdf->Ln(5);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(140,8,utf8_decode('    N° de Certficado Médico 0305-2015'),'B',0,'L');
	$pdf->Cell(13,8,'Fecha','B',0,'L');
	$pdf->Cell(9,8,'10','B',0,'C');
	$pdf->Cell(9,8,'6','B',0,'C');
	$pdf->Cell(19,8,'2015','B',0,'L');
	$pdf->Ln(6.8);
	$pdf->Cell(10,8,'I.- CENTRO ASISTENCIAL (Hospital/Institutos)',0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(190,5.5,utf8_decode('HOSPITAL NACIONAL "DANIEL ALCIDES CARRIÓN"'),'B',0,'C');
	$pdf->Ln(4.3);
	$pdf->Cell(10,8,'II.- DATOS PERSONALES DEL EVALUADO',0,0,'L');
	$pdf->Ln(7);
	$pdf->SetLineWidth(0.1);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(78,4,'Apellido Paterno','B',0,'L');
	 $pdf->Cell(3,4,'',0,0,'L');
	$pdf->Cell(45,4,'Apellido Materno','B',0,'L');
	 $pdf->Cell(3,4,'',0,0,'L');
	$pdf->Cell(49,4,'Nombres','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(81,4,'ESCALANTE',0,0,'L'); //CAMPO APELLIDO PATERNO
	$pdf->Cell(48,4,'HIDALGO',0,0,'L'); //CAMPO APELLIDO MATERNO
	$pdf->Cell(47,4,'ADEMIR',0,0,'L'); // CAMPO NOMBRES
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,utf8_decode('N° de DNI'),'B',0,'L');
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(38,4,'Sexo','B',0,'L');
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(15,4,'Edad','B',0,'L');
	 $pdf->Cell(24,4,'',0,0,'L');
	$pdf->Cell(35,4,'Fecha de Nacimiento','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(54,4,'05242040',0,0,'L'); //CAMPO DNI
	$pdf->Cell(53,4,'Masculino',0,0,'L'); //CAMPO SEXO
	$pdf->Cell(39,4,'63',0,0,'L'); // CAMPO EDAD
	$pdf->Cell(47,4,'6 12 1951',0,0,'L'); // CAMPO FECHA NACIMIENTO
	$pdf->Ln(6);
	$pdf->SetLineWidth(0.1);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(78,4,'Direccion Actual',0,0,'L');
	$pdf->Ln(4);
	 $pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(90,4,utf8_decode('Calle/ Jirón/ Avenida'),'B',0,'L');
	 $pdf->Cell(7,4,'',0,0,'L');
	$pdf->Cell(81,4,utf8_decode('Block/ Manzana/ Urbanización'),'B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(97,4,'TUMBES 285 URB ALTA MAR',0,0,'L'); //CAMPO DIRECCION
	$pdf->Cell(81,4,' ',0,0,'L'); //CAMPO BLOQUE MANZANA URBANIZACION
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(58,4,'Distrito','B',0,'L');
	 $pdf->Cell(2,4,'',0,0,'L');
	$pdf->Cell(50,4,'Provincia','B',0,'L');
	 $pdf->Cell(2,4,'',0,0,'L');
	$pdf->Cell(66,4,'Departamento','B',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(60,4,'LA PERLA',0,0,'L'); //CAMPO DISTRITO
	$pdf->Cell(52,4,'CALLAO',0,0,'L'); //CAMPO PROVINCIA
	$pdf->Cell(61,4,'CALLAO(PROVICINCIA CONSTITUCIONAL)',0,0,'L'); // CAMPO DEPARTAMENTO
	$pdf->Ln(6);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(190,5,utf8_decode('III.- La Comisión Médica Calificadora de la Incapacidad - CMCI, de acuerdo a sus facultades certifica lo siguiente'),'T',0,'L');
	$pdf->SetLineWidth(0.1);
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,4,'a.- Diagnostico','B',0,'L');
	$pdf->Cell(12,4,'CIE - 10','B',0,'R');
	$pdf->Ln(5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,20,'DEMENCIA EN LA ENFERMEDAD DE ALZHEIMER (G30.-+)',0,0,'L'); //CAMPO DIAGNOSTICO
	$pdf->Cell(12,20,'F00*',0,0,'L'); //CAMPO CIE
	$pdf->Ln(20);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(178,6,'b.- Caracteristica de la incapacidad','T',0,'L');
	$pdf->Ln(6);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,'Naturaleza de la Incapacidad',0,0,'L');
		$pdf->Ln(5);	
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(28,5,'Temporal',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(26,5,'',0,0,'L');
	$pdf->Cell(28,5,'Permanente',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(21,5,'',0,0,'L');
	$pdf->Cell(30,5,'No Incapacidad',1,0,'L');
	$pdf->Cell(12,5,'',1,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(42,4,'Grado de la Incapacidad',0,0,'L');
		$pdf->Ln(5);	
	 $pdf->Cell(12,4,'',0,0,'L');
	$pdf->Cell(28,5,'Parcial',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(26,5,'',0,0,'L');
	$pdf->Cell(28,5,'Total',1,0,'L');
	 $pdf->Cell(12,5,'',1,0,'L');
	  $pdf->Cell(21,5,'',0,0,'L');
	$pdf->Cell(30,5,'Gran Incapacidad',1,0,'L');
	$pdf->Cell(12,5,'',1,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,4,'c.- Menoscabo',0,0,'L');
	$pdf->Ln(3.5);
	$pdf->Cell(130,5,'',0,0,'L');
	$pdf->Cell(20,5,'Porcentaje',1,0,'L');
	$pdf->Ln(5);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->Cell(38,5,'','L'.'T',0,'L');
	$pdf->Cell(55,5,'Menoscabo Combinado','T',0,'C');
	$pdf->Cell(20,5.4,'60','B'.'R',0,'C'); // PORCENTAJE MENOSCABO COMBINADO
		$pdf->Ln(5.4);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->MultiCell(35,7.05,'Factores Complementarios',1,'C');
		$pdf->SetXY(82,187);
		$pdf->Cell(62,14.1,'','L'.'T'.'B',0,'L');
	$pdf->SetXY(82,187);
	$pdf->Cell(62,4,'Tipos de Actividad',0,0,'L');
	$pdf->SetXY(149,187);
	$pdf->Cell(62,4,'0',0,0,'L'); // CAMPO PORCENTAJE TIPO ACTIVIDAD
	$pdf->SetXY(82,191);
	$pdf->Cell(62,4,utf8_decode('Posibilidad de reubicaión laboral'),0,0,'L');
	$pdf->SetXY(149,191);
	$pdf->Cell(62,4,'0',0,0,'L'); // CAMPO PORCENTAJE REUBICACION
	$pdf->SetXY(82,195.5);
	$pdf->Cell(62,4,'Edad',0,0,'L');
	$pdf->SetXY(149,195.5);
	$pdf->Cell(62,4,'0',0,0,'L'); // CAMPO PORCENTAJE EDAD	
		$pdf->SetXY(140,187);
		$pdf->Cell(20,14.1,'','R'.'B',0,'L');
	$pdf->Ln(14.1);
	$pdf->Cell(37,5,'',0,0,'L');
	$pdf->Cell(38,5,'','B'.'L'.'T',0,'L');
	$pdf->Cell(55,5,'MENOSCABO GLOBAL','B'.'T',0,'C'); 
	$pdf->Cell(20,5,'60','B'.'T'.'R',0,'C'); // CAMPO MOENOSCABO GLOBAL PORCENTAJE TOTAL
	

	$pdf->Ln(8);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(166,4,'d-. Fecha de inicio de Incapacidad',0,0,'L');
	$pdf->Ln(4);
	$pdf->Cell(63,5,'',0,0,'L');
	$pdf->Cell(10,5,'Dia',0,0,'C');
	$pdf->Cell(10,5,'Mes',0,0,'C');
	$pdf->Cell(10,5,utf8_decode('Año'),0,0,'C');
	$pdf->Cell(35,5,'No es precisable',0,0,'C');
	
	$pdf->Ln(5);
	$pdf->Cell(63,5,'',0,0,'C'); 
	$pdf->Cell(10,5,'28',0,0,'C'); // CAMPO DIA INCAPACIDAD
	$pdf->Cell(10,5,'3',0,0,'C'); // CAMPO MES INCAPACIDAD
	$pdf->Cell(10,5,'2015',0,0,'C'); //  CAMPO AÑO INCAPACIDAD
	$pdf->Cell(4,5,'',0,0,'C');
	$pdf->Cell(30,5,'',1,0,'C'); // CAMPO IMPRECISABLE
	
	$pdf->Ln(7);
	$pdf->SetLineWidth(0.5);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,9,'IV.- OBSERVACIONES','T',0,'L');
	$pdf->Ln(5);
	$pdf->Cell(7,4,'',0,0,'L');
	$pdf->Cell(166,23,'CAPITULO XI SNC: ASIGNA 60% MGP. EVALUADO POR NEUROLOGIA Y NEUROPSICOLOGIA',0,0,'L');
	$pdf->Ln(23);
	$pdf->Cell(4,4,'',0,0,'L');
	$pdf->Cell(186,9,'V.- FIRMA Y SELLO','T',0,'L');
	$pdf->Ln(7);

  $fontSize = 10;
  $marge    = 10;   // POSICIONAMIENTO DE BARRA
  $x        = 8;  
  $y        = 21;  
  $height   = 10;   
  $width    = 0.4;    
  $angle    = 90;   
  
  $code     = '123456789012'; // CODIGO DE BARRAS!!
  $type     = 'ean13';
  $black    = '000000'; // COLOR DE BARRA

//$data = Barcode::fpdf($pdf, $black, $x, $y, $angle, $type, array('code'=>$code), $width, $height);
//$len = $pdf->GetStringWidth($data['hri']);$len = $pdf->GetStringWidth($data['hri']);
// Barcode::rotate(-$len / 2, ($data['height'] / 2) + $fontSize + $marge, $angle, $xt, $yt);
$pdf->TextWithRotation($x + 8, $y + 13, $data['hri'], $angle);
*/
$pdf->Output();

?>
