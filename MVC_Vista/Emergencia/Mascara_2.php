<?php 
    include_once("../../MVC_Modelo/EmergenciaM.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
<meta name="viewport" content="width=device-width">
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" sizes="192x192" href="/icon.png">
    <title>Lista de Verificacion Quirurgica</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/mobile.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
    <style>
        html, body { height: 100%;font-family: Helvetica; font-size: 20px;}
    </style>
       <style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css') 

.funkyradio div {
  clear: both;
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 2.5em;
  text-indent: 3.25em;
  margin-top: 2em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #777;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}

.funkyradio-danger input[type="radio"]:checked ~ label:before,
.funkyradio-danger input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #d9534f;
}

.funkyradio-warning input[type="radio"]:checked ~ label:before,
.funkyradio-warning input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #f0ad4e;
}

.funkyradio-info input[type="radio"]:checked ~ label:before,
.funkyradio-info input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5bc0de;
}
 

    </style>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.mobile.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.datagrid.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/filtro/datagrid-filter.js"></script>
    <script>
    $(document).ready(function() {
    //CERRAR SESION
    $("#Cerrar_Sesion").click(function(event) {
        window.location.href = '../../MVC_Vista/Emergencia/LoginEmergencia.php';
    });



    //MOSTRAR MIEMBROS DEL EQUIPO
    function MostrarMiembrosEquipo(codigo_documento) {
        $("#dtf").datagrid({url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Lista_Miembros_LV_C&codigo_documento='+codigo_documento});
    }  
      
    //BUSCAR DATOS DE PACIENTE POR HISTORIA CLINICA 
    $("#his_paciente").searchbox('textbox').keyup(function(e){
        if(e.keyCode == 13)
        { 
           var numhis = $("#his_paciente").val();

            //ENVIANDO NUMERO DE HISTORIA CLINICA
            $.ajax({
                url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscar_Pacientes_LV_C',
                type: 'POST',
                dataType: 'json',
                data: {
                    numhis: numhis
                },
                success: function(data){
                    if (data.dato == 'bad') {
                        $("#texto_mensaje_error").html(' ');
                        $("#texto_mensaje_error").text("No se encontro ningun paciente asociado a este numero de historia clinica.");
                        $('#dlg1').dialog('open').dialog('center');
                        $("#his_paciente").focus();
                        return 0;
                    }
                    else {
                        $("#datos_paciente").html(' ');
                        $("#datos_paciente").append(data.PATERNO+' '+data.MATERNO+' '+data.NOMBRES);  
                        $("#id_paciente").append(data.IDPACIENTE); 
                        return false;
                    }
                }
            });
    
            //DERIVANDO TIPO DE SALA
            $("input[name='tipo_sala']").change(function(event) {
                var tipo_sala_cabecera_tem = $("input[name='tipo_sala']:checked").val();      
                
                //RECOGIENDO DATOS
                var url = '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Listar_Salas_LV_C&idtiposala='+tipo_sala_cabecera_tem;
                $("#combo_tipos_salas").combobox('reload', url);      
            });
        }
    });


    //PRIMERA ETAPA
    $("#continuar_apertura").click(function(event) {

        //RECOGIENDO VALORES
        var especialidad_id = $("#especialidades_lv").combobox('getValue');
        var tipo_sala_id    = $("#combo_tipos_salas").combobox('getValue');
        var numhis          = $("#his_paciente").val();
        
        //VALIDANDO SELECCION DE TIPO DE SALA
        if(tipo_sala_id < 1) {
            $("#texto_mensaje_error").html(' ');
            $("#texto_mensaje_error").text("Seleccione un tipo de sala");
            $('#dlg1').dialog('open').dialog('center');
            return false;
        }

        if (especialidad_id < 1) {
            $("#texto_mensaje_error").html(' ');
            $("#texto_mensaje_error").text("Ingrese una especialidad");
            $('#dlg1').dialog('open').dialog('center');
            return false;
        }

        //MANDANDO DATOS PARA REGISTRAR
        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Insertando_Inicio_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                especialidad_id: especialidad_id,
                tipo_sala_id: tipo_sala_id,
                numhis: numhis
            },
            success: function(data) {
                if (data.respuesta == "M_1") {
                    $.getJSON('../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Ultimo_LV_C', {idpaciente:numhis, especialidad:especialidad_id, sala:tipo_sala_id}, function(data) {
                        $("#codigo_documento").html(" ");
                        $("#codigo_documento").val(data.CODIGO);
                        $("#historia_entrada").html(" ");
                        $("#historia_entrada").text(data.NUMHIS);
                        $("#servicio_entrada").html(" ");
                        $("#servicio_entrada").text(data.ESPECIALIDAD);
                        $("#tipo_sala_entrada").html(" ");
                        $("#tipo_sala_entrada").text(data.SALA);
                        $("#historia_pausa").html(" ");
                        $("#historia_pausa").text(data.NUMHIS);
                        $("#servicio_pausa").html(" ");
                        $("#servicio_pausa").text(data.ESPECIALIDAD);
                        $("#tipo_sala_pausa").html(" ");
                        $("#tipo_sala_pausa").text(data.SALA);
                        $("#historia_salida").html(" ");
                        $("#historia_salida").text(data.NUMHIS);
                        $("#servicio_salida").html(" ");
                        $("#servicio_salida").text(data.ESPECIALIDAD);
                        $("#tipo_sala_salida").html(" ");
                        $("#tipo_sala_salida").text(data.SALA);
                        $("#historia_validacion").html(" ");
                        $("#historia_validacion").text(data.NUMHIS);
                        $("#servicio_validacion").html(" ");
                        $("#servicio_validacion").text(data.ESPECIALIDAD);
                        $("#tipo_sala_validacion").html(" ");
                        $("#tipo_sala_validacion").text(data.SALA);
                    });

                    $.mobile.go("#entrada");
                    return false;              
                }
                else {
                    $("#texto_mensaje_error").html(' ');
                    $("#texto_mensaje_error").text("Hubo un error de sistema, comuniquese con el administrador.");
                    $('#dlg1').dialog('open').dialog('center');
                    return false;
                }
            }
        });
    
    });
    

    //OBSERVACIONES: VIA AREA DIFICIL / RIESGO DE ASPIRACION
    $("#alternativa13").change(function(event) {
        $('#dlg2').dialog('open').dialog('center');
        return false;
    });

    //OBSERVACIONES: RIESGO DE HEMORRAGIA > DE 500 ML (7 ML/KG EN NIÑOS)
    $("#alternativa15").change(function(event) {
        $('#dlg3').dialog('open').dialog('center');
        return false;
    });

    //INGRESO DE MIEMBROS DEL EQUIPO
    $("#alternativa16").change(function(event) {
        $('#dlg4').dialog('open').dialog('center');
        $("#cirujano_medico_id").combobox('reload','../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Medicos_LV_C');
        $("#anestesiologo_medico_id").combobox('reload','../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Medicos_LV_C');
        $("#instrumentista_id").combobox('reload','../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Empleado_LV_C');
        $("#asistente_uno_id").combobox('reload','../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Empleado_LV_C');
        $("#asistente_dos_id").combobox('reload','../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Empleado_LV_C');
        $("#asistente_tres_id").combobox('reload','../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Empleado_LV_C');
        $("#enf_circulantes_id").combobox('reload','../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Buscando_Empleado_LV_C');
        return false;
    });
    
    //SEGUNDA ETAPA
    $("#continuar_pausa").click(function(event) {
        var codigo_documento = $("#codigo_documento").val();
        var array_combinaciones_entrada = $('input[name="alternativas_entrada[]"]').serializeArray();
        
        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Insertando_Entrada_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                array_combinaciones_entrada: array_combinaciones_entrada,
                codigo_documento:codigo_documento
            },
            success: function(data){
                if (data.respuesta == "M_1") {
                   $.mobile.go("#pausa");   
                    return false;                    
                }

                if (data.respuesta == "M_2") {
                   $.mobile.go("#pausa");   
                    return false;                    
                }

                else {
                    $("#texto_mensaje_error").html(' ');
                    $("#texto_mensaje_error").text("Hubo un error de sistema, comuniquese con el administrador.");
                    $('#dlg1').dialog('open').dialog('center');
                    return false;
                }
            }
        });  
    });

    //TERCERA ETAPA
    $("#continuar_salida").click(function(event) {
        var codigo_documento = $("#codigo_documento").val();
        var array_combinaciones_pausa = $('input[name="alternativas_pausa[]"]').serializeArray();

        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Insertando_Pausa_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                array_combinaciones_pausa: array_combinaciones_pausa,
                codigo_documento:codigo_documento
            },
            success: function(data){
                if (data.respuesta == "M_1") {
                    $.mobile.go("#salida");
                    return false;     
                }

                if (data.respuesta == "M_2") {
                   $.mobile.go("#salida");   
                    return false;                    
                }

                else {
                    $("#texto_mensaje_error").html(' ');
                    $("#texto_mensaje_error").text("Hubo un error de sistema, comuniquese con el administrador.");
                    $('#dlg1').dialog('open').dialog('center');
                    return false;
                }
            }
        });               
    });

    //CUARTA ETAPA
    $("#continuar_validaciones_observaciones").click(function(event) {
        var codigo_documento = $("#codigo_documento").val();
        var array_combinaciones_salida = $('input[name="alternativas_salida[]"]').serializeArray();

        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Insertando_Salida_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                array_combinaciones_salida: array_combinaciones_salida,
                codigo_documento:codigo_documento
            },
            success: function(data){
                if (data.respuesta == "M_1") {
                    $.mobile.go("#validacion");
                    $.ajax({
                        url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Mostrar_Observaciones_LV_C',
                        type: 'POST',
                        dataType: 'json',
                        data: {codigo_documento: codigo_documento},
                        success:function(dato){
                            $("#observaciones_general").textbox('setValue',dato.texto_mostrar);
                            console.log(dato.texto_mostrar);
                            return false;
                        }
                    });        
                    MostrarMiembrosEquipo(codigo_documento);                                                         
                    return false;
                }
                
                if (data.respuesta == "M_2") {
                    $.mobile.go("#validacion");
                    $.ajax({
                        url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Mostrar_Observaciones_LV_C',
                        type: 'POST',
                        dataType: 'json',
                        data: {codigo_documento: codigo_documento},
                        success:function(dato){
                            $("#observaciones_general").textbox('setValue',dato.texto_mostrar);
                            console.log(dato.texto_mostrar);
                            return false;
                        }
                    }); 
                    MostrarMiembrosEquipo(codigo_documento);
                    return false;                    
                }

                else {
                    $("#texto_mensaje_error").html(' ');
                    $("#texto_mensaje_error").text("Hubo un error de sistema, comuniquese con el administrador.");
                    $('#dlg1').dialog('open').dialog('center');
                    return false;
                }
            }
        });
    });
    
    //FINALIZACION DE PROCESO
    $("#finalizar_etapa").click(function(event) {
        var observaciones_general_fin = $('#observaciones_general').val();
        var codigo_documento = $('#codigo_documento').val();

        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Actualizando_Observaciones_Final_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                observaciones: observaciones_general_fin,
                codigo_documento:codigo_documento
            },
            success: function(data){
                if (data.respuesta == 'M_1') {
                    $.mobile.go("#arranque");
                    $('#dg').datagrid('reload');
                    return false;
                }
            }
        });
    });
    
    //PARA REGRESAR AL INICIO - BOTONES DE INICIO
    $("#Regresar").click(function(event) {
        $('#dg').datagrid('loadData', {"CODIGO":0,"rows":[]});
        $.mobile.go("#arranque");
        return false;
    });

    $("#Regresar_i").click(function(event) {
        $('#dg').datagrid('loadData', {"CODIGO":0,"rows":[]});
        $.mobile.go("#arranque");
        return false;
    });

    $("#Regresar_x").click(function(event) {
        $('#dg').datagrid('loadData', {"CODIGO":0,"rows":[]});
        $.mobile.go("#arranque");
        return false;
    });

    $("#Regresar_e").click(function(event) {
        $('#dg').datagrid('loadData', {"CODIGO":0,"rows":[]});
        $.mobile.go("#arranque");
        return false;
    });
    
    $("#Regresar_p").click(function(event) {
        $('#dg').datagrid('loadData', {"CODIGO":0,"rows":[]});
        $.mobile.go("#arranque");
        return false;
    });

    $("#Regresar_s").click(function(event) {
        $('#dg').datagrid('loadData', {"CODIGO":0,"rows":[]});
        $.mobile.go("#arranque");
        return false;
    });

    $("#Regresar_f").click(function(event) {
        $('#dg').datagrid('loadData', {"CODIGO":0,"rows":[]});
        $.mobile.go("#arranque");
        return false;
    });

   
    //FIN DEL SCRIPT - ///////////////////////////////
    });
    </script>

    <script>
        function Iniciar_Proceso(){
            $("#his_paciente").textbox('clear');
            $("#datos_paciente").html('  ');
            $('input[name="tipo_sala"]').attr('checked', false);
            //$("#").prop('checked', false);
            $('#combo_tipos_salas').combobox('clear');
            $('#especialidades_lv').combobox('clear');
            $.mobile.go("#inicio");
            return false;
        }

        function Ver_Generados()
        {   
            var traba = $("#traba_tabla").val();

            if (traba == 0) {
                
                $("#dg").datagrid({url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Mostrar_Listas_General_LV_C'});                  
                //$("#dg").datagrid('enableFilter'); 
                $("#traba_tabla").val(1);
            }
             
            if (traba == 1) {
                $("#dg").datagrid('reload');
            } 


            $("#Modificar_btn").linkbutton({
                    disabled: false
                });
            $.mobile.go("#listas_generadas");
            return false;
        }
        function Modificar(){
            var row = $('#dg').datagrid('getSelected');
            if (row){
                var codigo_lv = row.CODIGO;
                //ENVIANDO CODIGO PARA DERIVAR A LA ETAPA FALTANTE
                $.getJSON('../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Mostrar_Estado_LV_C', {codigo_documento: codigo_lv}, function(data) {                     
                    $("#codigo_documento").val(data.codigo);
                    if (data.estado == 1) 
                    {                            
                        $("#historia_entrada").html(" ");
                        $("#historia_entrada").text(data.numhis);
                        $("#servicio_entrada").html(" ");
                        $("#servicio_entrada").text(data.especialidad);
                        $("#tipo_sala_entrada").html(" ");
                        $("#tipo_sala_entrada").text(data.sala);
                        $("#historia_pausa").html(" ");
                        $("#historia_pausa").text(data.numhis);
                        $("#servicio_pausa").html(" ");
                        $("#servicio_pausa").text(data.especialidad);
                        $("#tipo_sala_pausa").html(" ");
                        $("#tipo_sala_pausa").text(data.sala);
                        $("#historia_salida").html(" ");
                        $("#historia_salida").text(data.numhis);
                        $("#servicio_salida").html(" ");
                        $("#servicio_salida").text(data.especialidad);
                        $("#tipo_sala_salida").html(" ");
                        $("#tipo_sala_salida").text(data.sala);
                        $("#historia_validacion").html(" ");
                        $("#historia_validacion").text(data.numhis);
                        $("#servicio_validacion").html(" ");
                        $("#servicio_validacion").text(data.especialidad);
                        $("#tipo_sala_validacion").html(" ");
                        $("#tipo_sala_validacion").text(data.sala);
                        $.mobile.go("#entrada"); 
                        return false;
                    }
                    if (data.estado == 2) 
                    {
                        $("#historia_entrada").html(" ");
                        $("#historia_entrada").text(data.numhis);
                        $("#servicio_entrada").html(" ");
                        $("#servicio_entrada").text(data.especialidad);
                        $("#tipo_sala_entrada").html(" ");
                        $("#tipo_sala_entrada").text(data.sala);
                        $("#historia_pausa").html(" ");
                        $("#historia_pausa").text(data.numhis);
                        $("#servicio_pausa").html(" ");
                        $("#servicio_pausa").text(data.especialidad);
                        $("#tipo_sala_pausa").html(" ");
                        $("#tipo_sala_pausa").text(data.sala);
                        $("#historia_salida").html(" ");
                        $("#historia_salida").text(data.numhis);
                        $("#servicio_salida").html(" ");
                        $("#servicio_salida").text(data.especialidad);
                        $("#tipo_sala_salida").html(" ");
                        $("#tipo_sala_salida").text(data.sala);
                        $("#historia_validacion").html(" ");
                        $("#historia_validacion").text(data.numhis);
                        $("#servicio_validacion").html(" ");
                        $("#servicio_validacion").text(data.especialidad);
                        $("#tipo_sala_validacion").html(" ");
                        $("#tipo_sala_validacion").text(data.sala);
                        $.mobile.go("#pausa"); 
                        return false;
                    }
                    if (data.estado == 3) 
                    {
                        $("#historia_entrada").html(" ");
                        $("#historia_entrada").text(data.numhis);
                        $("#servicio_entrada").html(" ");
                        $("#servicio_entrada").text(data.especialidad);
                        $("#tipo_sala_entrada").html(" ");
                        $("#tipo_sala_entrada").text(data.sala);
                        $("#historia_pausa").html(" ");
                        $("#historia_pausa").text(data.numhis);
                        $("#servicio_pausa").html(" ");
                        $("#servicio_pausa").text(data.especialidad);
                        $("#tipo_sala_pausa").html(" ");
                        $("#tipo_sala_pausa").text(data.sala);
                        $("#historia_salida").html(" ");
                        $("#historia_salida").text(data.numhis);
                        $("#servicio_salida").html(" ");
                        $("#servicio_salida").text(data.especialidad);
                        $("#tipo_sala_salida").html(" ");
                        $("#tipo_sala_salida").text(data.sala);
                        $("#historia_validacion").html(" ");
                        $("#historia_validacion").text(data.numhis);
                        $("#servicio_validacion").html(" ");
                        $("#servicio_validacion").text(data.especialidad);
                        $("#tipo_sala_validacion").html(" ");
                        $("#tipo_sala_validacion").text(data.sala);
                        $.mobile.go("#salida"); 
                        return false;
                    }
                    if (data.estado == 4) 
                    {
                        $("#historia_entrada").html(" ");
                        $("#historia_entrada").text(data.numhis);
                        $("#servicio_entrada").html(" ");
                        $("#servicio_entrada").text(data.especialidad);
                        $("#tipo_sala_entrada").html(" ");
                        $("#tipo_sala_entrada").text(data.sala);
                        $("#historia_pausa").html(" ");
                        $("#historia_pausa").text(data.numhis);
                        $("#servicio_pausa").html(" ");
                        $("#servicio_pausa").text(data.especialidad);
                        $("#tipo_sala_pausa").html(" ");
                        $("#tipo_sala_pausa").text(data.sala);
                        $("#historia_salida").html(" ");
                        $("#historia_salida").text(data.numhis);
                        $("#servicio_salida").html(" ");
                        $("#servicio_salida").text(data.especialidad);
                        $("#tipo_sala_salida").html(" ");
                        $("#tipo_sala_salida").text(data.sala);
                        $("#historia_validacion").html(" ");
                        $("#historia_validacion").text(data.numhis);
                        $("#servicio_validacion").html(" ");
                        $("#servicio_validacion").text(data.especialidad);
                        $("#tipo_sala_validacion").html(" ");
                        $("#tipo_sala_validacion").text(data.sala);
                        $.ajax({
                            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Mostrar_Observaciones_LV_C',
                            type: 'POST',
                            dataType: 'json',
                            data: {codigo_documento: data.codigo},
                            success:function(dato){
                                $("#observaciones_general").textbox('setValue',dato.texto_mostrar);
                                console.log(dato.texto_mostrar);
                                return false;
                            }
                        });        
                        MostrarMiembrosEquipo(data.codigo);
                        $.mobile.go("#validacion"); 
                        return false;
                        }
                        
                    });                        
            }
        }

        function Exportar_PDF_Generadas()
        {
            var row = $('#dg').datagrid('getSelected');
            if (row) 
                {
                    var codigo_exp = row.CODIGO;
                    window.open('../../MVC_Vista/Emergencia/Mascara_2_pdf.php?codigo='+codigo_exp,'_blank');
                };
        }

        function Ver_Finalizados(){
            $("#dg").datagrid({url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Mostrar_Listas_Validado_LV_C'});  
            $("#Modificar_btn").linkbutton({
                disabled: true
            });
            $("#reload_tabla").hide();
            $.mobile.go("#listas_generadas");
            $("#traba_tabla").val(0);
            return false;
        }

        function Volver(){
            $("#dg").datagrid({url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Mostrar_Listas_General_LV_C'});
            $("#Modificar_btn").linkbutton({
                disabled: false
            });
        }

        function MostrarMiembrosEquipo(codigo_documento){
            $("#dtf").datagrid({url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Lista_Miembros_LV_C&codigo_documento='+codigo_documento});
        }

    </script>

</head>
<body>

<div class="easyui-navpanel" id="arranque">
    <header>
        <div class="m-toolbar">
            <span class="m-title">LISTAS DE VERIFICACION</span>
            <div class="m-right">
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="iconCls:'icon-more',menu:'#mm',menuAlign:'right',hasDownArrow:false"></a>
            </div>
        </div>
    </header>
     <!-- MENU -->
    <div id="mm" class="easyui-menu" style="width:150px;">
        <div data-options="iconCls:'icon-man'" id="Cerrar_Sesion">Cerrar Sesion</div>
    </div>
    
    <div style="width:50%; padding:5%;text-align: center;margin-left:20%;margin-top:8%;">
        <a class="easyui-linkbutton" onclick="Iniciar_Proceso()" data-options="iconCls:'icon-add'" style="width:50%;height:50px;">NUEVO REGISTRO</a>
        <br><br><br><br>
        <a class="easyui-linkbutton" onclick="Ver_Finalizados()" data-options="iconCls:'icon-ok'" style="width:50%;height:50px;">LISTAS VALIDADAS</a>
        <br><br><br><br>
        <a class="easyui-linkbutton" onclick="Ver_Generados()" data-options="iconCls:'icon-search'" style="width:50%;height:50px;">BUSCAR</a>
        <br><br><br><br>
        <a class="easyui-linkbutton" id="Cerrar_Sesion" data-options="iconCls:'icon-man'" style="width:50%;height:50px;">CERRAR SESION</a>
    </div>   
</div>

<div class="easyui-navpanel" id="listas_generadas">
    <header>
        <div class="m-toolbar">
            <span class="m-title">LISTAS DE VERIFICACION</span>
            <div class="m-right">
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="iconCls:'icon-more',menu:'#mm',menuAlign:'right',hasDownArrow:false"></a>
            </div>
        </div>
    </header>
    <!-- MENU -->
    <div id="mm" class="easyui-menu" style="width:150px;">
        <div data-options="iconCls:'icon-man'" id="Cerrar_Sesion">Cerrar Sesion</div>
    </div>

    <div style="margin:15px 40px;">
        <a class="easyui-linkbutton" onclick="Volver()" data-options="iconCls:'icon-reload'" id="reload_tabla"></a>
        <a class="easyui-linkbutton" onclick="Modificar()" data-options="iconCls:'icon-edit'" id="Modificar_btn">Modificar</a>
        <a class="easyui-linkbutton" onclick="Exportar_PDF_Generadas()" ><img src="../../MVC_Complemento/img/pdf.png" height="15" width="15" alt="">&nbsp;Exportar</a>
        <a class="easyui-linkbutton" id="Regresar_x" data-options="iconCls:'icon-back'">INICIO</a>
        <input type="hidden" id="traba_tabla" value="0"> 
    </div>
    
    <div style="padding:0 40px">
        <table id="dg" title="" data-options="
            rownumbers:true,
            singleSelect:true,
            pagination:true,
            pageSize:10">
            <thead>
                <tr>
                    <th data-options="field:'CODIGO',hidden:true">CODIGO</th>
                    <th data-options="field:'IDPACIENTE',hidden:true">IDPACIENTE</th>
                    <th data-options="field:'NUMHIS',resizable:true">N° Historia</th>
                    <th data-options="field:'PACIENTES',resizable:true">Pacientes</th>
                    <th data-options="field:'INICIO',resizable:true,align:'center'">Inicio</th>
                    <th data-options="field:'ENTRADA',resizable:true,align:'center'">Entrada</th>
                    <th data-options="field:'PAUSA',resizable:true,align:'center'">Pausa</th>
                    <th data-options="field:'SALIDA',resizable:true,align:'center'">Salida</th>
                    <th data-options="field:'VALIDAR',resizable:true,align:'center'">Validado</th>
                </tr>
            </thead>
        </table>
    </div>

</div>

<!-- INICIO -->
<div class="easyui-navpanel" id="inicio">
    <header>
        <div class="m-toolbar">
            <span class="m-title">LISTA DE VERIFICACION</span>
            <input type="hidden" id="codigo_documento">
            <div class="m-right">
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="iconCls:'icon-more',menu:'#mm',menuAlign:'right',hasDownArrow:false"></a>
            </div>
        </div>
    </header>

    <!-- MENU -->
    <div id="mm" class="easyui-menu" style="width:150px;">
        <div data-options="iconCls:'icon-undo'" id="Regresar">Salir</div>
    </div>

    <div style="padding:0 40px">
        <div style="margin-bottom:10px; margin-top:20px">
            <input class="easyui-textbox" type="text" data-options="prompt:' N° Historia'" style="width:15%;height:30px" id="his_paciente">&nbsp;&nbsp;&nbsp;&nbsp;PACIENTE:&nbsp;&nbsp;&nbsp;<label id="datos_paciente" style="text-shadow: 2px 2px 5px #0C3680;color:#2172FF;"></label><input type="hidden" id="id_paciente">
         </div>
        <div style="margin-bottom:12px" id="dato_5">
            TIPO DE SALA: 
<!--
            <input type="Radio"  class="easyui-validatebox" required value="1" name="tipo_sala"  ><label for="SalaElectiva">Electiva</label>
            <input type="Radio"  class="easyui-validatebox" required value="2" name="tipo_sala"  ><label for="SalaEmergencia" >Emergencia</label>&nbsp;&nbsp;&nbsp;
--> 
            <input type="Radio"  class="easyui-validatebox" required value="1" name="tipo_sala" id="SalaElectiva"><label for="SalaElectiva"><strong>Electiva</strong></label>
            <input type="Radio"  class="easyui-validatebox" required value="2" name="tipo_sala" id="SalaEmergencia"><label for="SalaEmergencia" ><strong>Emergencia</strong></label>
            &nbsp;&nbsp;&nbsp;
           
          <input id="combo_tipos_salas" class="easyui-combobox" style="width:15%;" data-options="valueField:'id',textField:'text'">
        </div>

        <div id="dato_6">
            <label>SERVICIO:</label><br>
            <select class="easyui-combobox" style="width:100%;" id="especialidades_lv">
                <option value="0">  </option>
                <?php 
                    $data = Buscar_Especialidad_lv_M('','');
                    foreach ($data as $datos) {
                        echo "<option value=".$datos[0].">".$datos[1]."</option>";
                    }
                ?>
            </select>
        </div>            
        <div style="text-align:center;margin-top:30px;margin-bottom:20px;">
            <a class="easyui-linkbutton" style="width:100px;height:35px" id="continuar_apertura" data-options="iconCls:'icon-ok'">CONTINUAR</a> 
            <a class="easyui-linkbutton" style="width:100px;height:35px" id="Regresar_i" data-options="iconCls:'icon-back'">INICIO</a> 
        </div>
    </div>
</div>

<!-- CUADRO DE ENTRADA -->
<div class="easyui-navpanel" id="entrada">
    <header>
        <div class="m-toolbar">
            <div class="m-title">ENTRADA</div>
            <div class="m-right">
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="iconCls:'icon-more',menu:'#mm',menuAlign:'right',hasDownArrow:false"></a>
            </div>
        </div>
    </header>
    <!-- MENU -->
    <div id="mm" class="easyui-menu" style="width:150px;">
        <div data-options="iconCls:'icon-undo'" id="Regresar">Salir</div>
    </div>
    
    <div style="padding:0 40px">
        <ul class="m-list">
            <li><label>(Con el enfermero y el anestesiologo, como m&iacute;nimo)</label></li>                     
            <?php 
                //MOSTRANDO PREGUNTAS
                $preguntas = Listar_Preguntas_LV_M(1);          
                foreach ($preguntas as $pregunta) {
                    echo "<li>";
                    echo "<input type='hidden' value=".$pregunta['IdPregunta'].">";
                    $pregunta_recogida = strtoupper(utf8_encode($pregunta['Nombre']));
                    echo "<label><strong>".utf8_decode($pregunta_recogida)."</strong></label><br>";
                
                    //TRAYENDO ALTERNATIVAS
                    $alternativas = Listar_Alternativas_LV_M($pregunta['IdPregunta']);
                    foreach ($alternativas as $alternativa) {
                        $alternativa_minuscula = strtolower(utf8_encode($alternativa['Nombre']));
						 ?>
                         <div class="col-md-6">
						 <div class="funkyradio">
                <div class="funkyradio-success">
						 <?php
						
                        echo "<input type='checkbox' name='alternativas_entrada[]' value='1,".$pregunta['IdPregunta'].",".$alternativa['IdAlternativas']."' id='alternativa".$alternativa['IdAlternativas']."'><label for='alternativa".$alternativa['IdAlternativas']."' >".ucfirst($alternativa_minuscula)."</label>";
						
						
						 ?>   </div>
       						</div></div>
						 <?php
						
						
                    }
                    echo "</li>";
                }
                echo "</ul>";
            ?>
            
            
            
            
            
            
            
       
                
        
       
            
            
    </div>
    <div style="text-align:center;margin-top:30px;margin-bottom:20px;">
        <a href="#" class="easyui-linkbutton" style="width:100px;height:35px" id="continuar_pausa" data-options="iconCls:'icon-ok'">CONTINUAR</a> 
        <a class="easyui-linkbutton" style="width:100px;height:35px" id="Regresar_e" data-options="iconCls:'icon-back'">INICIO</a> 
    </div>      
    <footer style="padding:8px;">
        <div style="font-size:14px;text-align:right;color:#999999;">
            Historia Clinica: <label id="historia_entrada"></label>&nbsp;&nbsp;|&nbsp;&nbsp;
            <!-- Paciente: <label id="datos_paciente_entrada" style="text-transform: capitalize;"></label>&nbsp;&nbsp;|&nbsp;&nbsp;  -->
            Servicio: <label id="servicio_entrada"></label>&nbsp;&nbsp;|&nbsp;&nbsp;           
            Tipo de Sala: <label id="tipo_sala_entrada"></label>&nbsp;&nbsp;
        </div>
    </footer>
</div>

<!-- CUADRO DE PAUSA QUIRURGICA -->
<div class="easyui-navpanel" id="pausa">
    <header>
        <div class="m-toolbar">
            <div class="m-title">PAUSA QUIRURGICA</div>
            <div class="m-right">
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="iconCls:'icon-more',menu:'#mm',menuAlign:'right',hasDownArrow:false"></a>
            </div>
        </div>
    </header>
    
    <!-- MENU -->
    <div id="mm" class="easyui-menu" style="width:150px;">
        <div data-options="iconCls:'icon-undo'" id="Regresar">Salir</div>
    </div>
    
    <div style="padding:0 40px">
        <ul class="m-list">
            <li><label>(Con el enfermero y el anestesiologo, y cirujano)</label></li>                     
            <?php 
            //MOSTRANDO PREGUNTAS
            $preguntas = Listar_Preguntas_LV_M(2);          
            foreach ($preguntas as $pregunta) {
                echo "<li >";
                echo "<input type='hidden' value=".$pregunta['IdPregunta'].">";
                $pregunta_recogida = strtoupper(utf8_encode($pregunta['Nombre']));
                echo "<label><strong>".utf8_decode($pregunta_recogida)."</strong></label><br>";
                
                //TRAYENDO ALTERNATIVAS
                $alternativas = Listar_Alternativas_LV_M($pregunta['IdPregunta']);
                foreach ($alternativas as $alternativa) {
                    //CONVIRTIENDO A MINUSCULAS
                    $alternativa_minuscula = strtolower(utf8_encode($alternativa['Nombre']));
					 ?>
                         <div class="col-md-6">
						 <div class="funkyradio">
                <div class="funkyradio-info">
						 <?php
                    echo "<input type='checkbox' name='alternativas_pausa[]' value=2,".$pregunta['IdPregunta'].",".$alternativa['IdAlternativas']." id='alternativa".$alternativa['IdAlternativas']."'><label for='alternativa".$alternativa['IdAlternativas']."' >".ucfirst($alternativa_minuscula)."</label>";
               
			    ?>   </div>
       						</div></div>
						 <?php
			    }
				
                echo "</li>";
            }
            echo "</ul>";
            ?>
    </div>
    <div style="text-align:center;margin-top:30px;margin-bottom:20px;">
        <a href="#" class="easyui-linkbutton" style="width:100px;height:35px" id="continuar_salida" data-options="iconCls:'icon-ok'">CONTINUAR</a>
        <a class="easyui-linkbutton" style="width:100px;height:35px" id="Regresar_p" data-options="iconCls:'icon-back'">INICIO</a>  
    </div>      
    <footer style="padding:8px;">
        <div style="font-size:14px;text-align:right;color:#999999;">
            Historia Clinica: <label id="historia_pausa"></label>&nbsp;&nbsp;|&nbsp;&nbsp;
            <!-- Paciente: <label id="datos_paciente_pausa" style="text-transform: capitalize;"></label>&nbsp;&nbsp;|&nbsp;&nbsp;  -->
            Servicio: <label id="servicio_pausa"></label>&nbsp;&nbsp;|&nbsp;&nbsp;           
            Tipo de Sala: <label id="tipo_sala_pausa"></label>&nbsp;&nbsp;
        </div>
    </footer>
</div>

<!-- SALIDA -->
<div class="easyui-navpanel" id="salida">
    <header>
        <div class="m-toolbar">
            <div class="m-title">SALIDA</div>
            <div class="m-right">
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="iconCls:'icon-more',menu:'#mm',menuAlign:'right',hasDownArrow:false"></a>
            </div>
        </div>
    </header>
    
    <!-- MENU -->
    <div id="mm" class="easyui-menu" style="width:150px;">
        <div data-options="iconCls:'icon-undo'" id="Regresar">Salir</div>
    </div>
    <div style="padding:0 40px">
        <ul class="m-list">
            <li><label>(Con el enfermero y el anestesiologo, y cirujano)</label></li>                     
            <?php 
            //MOSTRANDO PREGUNTAS
            $preguntas = Listar_Preguntas_LV_M(3);          
            foreach ($preguntas as $pregunta) {
                echo "<li>";
                echo "<input type='hidden' value=".$pregunta['IdPregunta'].">";
                $pregunta_recogida = strtoupper(utf8_encode($pregunta['Nombre']));
                echo "<label><strong>".utf8_decode($pregunta_recogida)."</strong></label><br>";
                
                //TRAYENDO ALTERNATIVAS
                $alternativas = Listar_Alternativas_LV_M($pregunta['IdPregunta']);
                foreach ($alternativas as $alternativa) {
                    $alternativa_minuscula = strtolower(utf8_encode($alternativa['Nombre']));
					
					?>
                         <div class="col-md-6">
						 <div class="funkyradio">
                <div class="funkyradio-primary">
						 <?php
                    echo "<input type='checkbox' name='alternativas_salida[]' value=3,".$pregunta['IdPregunta'].",".$alternativa['IdAlternativas']." id='alternativa".$alternativa['IdAlternativas']."'><label for='alternativa".$alternativa['IdAlternativas']."'>".ucfirst($alternativa_minuscula)."</label>";
					
					 ?>   </div>
       						</div></div>
						 <?php
                }
                echo "</li>";
            }
            echo "</ul>";
            ?>
    </div>
    <div style="text-align:center;margin-top:30px;margin-bottom:20px;">
        <a href="#" class="easyui-linkbutton" style="width:100px;height:35px" id="continuar_validaciones_observaciones" data-options="iconCls:'icon-ok'">CONTINUAR</a>
        <a class="easyui-linkbutton" style="width:100px;height:35px" id="Regresar_s" data-options="iconCls:'icon-back'">INICIO</a>  
    </div>      
    <footer style="padding:8px;">
        <div style="font-size:14px;text-align:right;color:#999999;">
            Historia Clinica: <label id="historia_salida"></label>&nbsp;&nbsp;|&nbsp;&nbsp;
            <!-- Paciente: <label id="datos_paciente_salida" style="text-transform: capitalize;"></label>&nbsp;&nbsp;|&nbsp;&nbsp;  -->
            Servicio: <label id="servicio_salida"></label>&nbsp;&nbsp;|&nbsp;&nbsp;           
            Tipo de Sala: <label id="tipo_sala_salida"></label>&nbsp;&nbsp;
        </div>
    </footer>
</div>

<!-- VALIDACIONES Y OBSERVACIONES -->
<div class="easyui-navpanel" id="validacion">
    <header>
        <div class="m-toolbar">
            <div class="m-title">VALIDACIONES Y OBSERVACIONES</div>
            <div class="m-right">
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="iconCls:'icon-more',menu:'#mm',menuAlign:'right',hasDownArrow:false"></a>
            </div>
        </div>
    </header>
    
    <!-- MENU -->
    <div id="mm" class="easyui-menu" style="width:150px;">
        <div data-options="iconCls:'icon-undo'" id="Regresar">Salir</div>
    </div>
    
    <div style="padding:0 40px">
        <ul class="m-list">
            <li>
                <label>Observaciones</label><br>    
                <input class="easyui-textbox" data-options="multiline:true" style="width:100%;height:100%;" id="observaciones_general"><br>
            </li>
            <li>
                <label>Validacion:</label><br>
                <div id="cuadro_validacion">
                    <table class="easyui-datagrid" title="MIEMBROS DEL EQUIPO" style="width:600px;height:350px" data-options="rownumbers:true,singleSelect:true,collapsible:true" id="dtf">
                        <thead>
                            <tr>
                                <th data-options="field:'ID_EMPLEADO',width:0,hidden:true">ID EMPLEADO</th>
                                <th data-options="field:'CARGO',width:150">CARGO</th>
                                <th data-options="field:'NOMBRE',width:350">NOMBRE</th>
                                <th data-options="field:'VALIDADO',width:70">ESTADO</th>
                            </tr>
                        </thead>
                    </table>
                    <br><a class="easyui-linkbutton" onclick="getSelected()">Validar</a>
                    <script type="text/javascript">
                        function getSelected(){
                            var codigo_documento = $("#codigo_documento").val();
                            var row = $('#dtf').datagrid('getSelected');
                            var rowIndex = $("#dtf").datagrid("getRowIndex", row);                                
                            if (row){
                                var id_empleado_validacion = row.ID_EMPLEADO;
                                var cargo_empleado_validacion = row.CARGO;
                                $.messager.prompt('Validacion', 'Ingrese su contraseña:', function(r){                                        
                                    if (r){
                                        var contrasena_validacion = r;
                                        //VALIDANDO
                                        $.getJSON('../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Validar_MIembros_LV_C', {idempleado: id_empleado_validacion,contrasena:contrasena_validacion,cargo_empleado_validacion:cargo_empleado_validacion,codigo_documento:codigo_documento}, function(data) {
                                                if (data.respuesta == "M_1") {
                                                    $("#dtf").datagrid('reload');
                                                    $("#dg").datagrid('reload');
                                                    return false;
                                                }                                                
                                                if (data.respuesta == "M_2") {
                                                    $.messager.alert('Warning','Contraseña Invalida o Incorrecta.');
                                                }
                                        });
                                    }
                                });
                                $('.messager-input').get(0).type = 'password';
                            }
                        }                            
                    </script>
                </div>
            </li>
        </ul>
    </div>
    <div style="text-align:center;margin-top:30px;margin-bottom:20px;">
        <a href="#" class="easyui-linkbutton" style="width:100px;height:35px" id="finalizar_etapa" data-options="iconCls:'icon-ok'">FINALIZAR</a> 
        <a class="easyui-linkbutton" style="width:100px;height:35px" id="Regresar_f" data-options="iconCls:'icon-back'">INICIO</a> 
    </div>
    <footer style="padding:8px;">
        <div style="font-size:14px;text-align:right;color:#999999;">
            Historia Clinica: <label id="historia_validacion"></label>&nbsp;&nbsp;|&nbsp;&nbsp;
            <!-- Paciente: <label id="datos_paciente_validacion" style="text-transform: capitalize;"></label>&nbsp;&nbsp;|&nbsp;&nbsp;  -->
            Servicio: <label id="servicio_validacion"></label>&nbsp;&nbsp;|&nbsp;&nbsp;           
            Tipo de Sala: <label id="tipo_sala_validacion"></label>&nbsp;&nbsp;
        </div>
    </footer>
</div>



<!--ALERTA -->
<div id="dlg1" class="easyui-dialog" style="padding:20px;width:35%;" data-options="inline:true,modal:true,closed:true,title:'Warning'">
    <p id="texto_mensaje_error"></p>
    <div class="dialog-button">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:100%;height:35px" onclick="$('#dlg1').dialog('close')">OK</a>
    </div>
</div>

<!--ALERTA -->
<div id="dlg10" class="easyui-dialog" style="padding:20px;width:35%;" data-options="inline:true,modal:true,closed:true,title:'Warning'">
    <p id="texto_mensaje_error"></p>
    <div class="dialog-button">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:100%;height:35px" onclick="$('#dlg1').dialog('close')">OK</a>
    </div>
</div>

<!-- INGRESAR OBSERVACION DE ALTERNATICA 13-->
<div id="dlg2" class="easyui-dialog" title="Observaciones" style="width:400px;height:200px;padding:10px"
    data-options="
    closed:true,
    buttons: [{
    text:'Ok',
    iconCls:'icon-ok',
    handler:function(){
        var observaciones_uno = $('#observaciones_uno').val();
        var codigo_documento = $('#codigo_documento').val();
        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Actualizando_Observaciones_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                observaciones: observaciones_uno,
                codigo_documento:codigo_documento
            },
            success: function(data){
                if (data.respuesta == 'M_1') {
                    $('#dlg2').dialog('close');
                    return false;
                }
            }
        });
    }
    },{
    text:'Cancel',
    handler:function(){
        $('#dlg2').dialog('close');
        $('#alternativa13').prop('checked', false); 
    }
    }]
    ">
    <input class="easyui-textbox" data-options="multiline:true" style="width:100%;height:100%;" id="observaciones_uno">
</div>



<!-- INGRESAR OBSERVACION DE ALTERNATIVA 15-->
<div id="dlg3" class="easyui-dialog" title="Observaciones" style="width:400px;height:200px;padding:10px"
data-options="
    closed:true,
    buttons: [{
    text:'Ok',
    iconCls:'icon-ok',
    handler:function(){
        var observaciones_dos = $('#observaciones_dos').val();
        var codigo_documento = $('#codigo_documento').val();
        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Actualizando_Observaciones_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                observaciones: observaciones_dos,
                codigo_documento:codigo_documento
            },
            success: function(data){
                if (data.respuesta == 'M_1') {
                    $('#dlg3').dialog('close');
                    return false;
                }
            }
        });
    }
    },{
    text:'Cancel',
    handler:function(){
        $('#dlg3').dialog('close');
        $('#alternativa15').prop('checked', false); 
    }
    }]
    ">
    <input class="easyui-textbox" data-options="multiline:true" style="width:100%;height:100%;" id="observaciones_dos">
</div>

<!-- INGRESO DE MIEBROS DEL EQUIPO -->
<div id="dlg4" class="easyui-dialog" title="Miembros del Equipo" style="width:400px;height:560px;padding:10px"
data-options="
    closed:true,
    buttons: [{
    text:'Ok',
    iconCls:'icon-ok',
    handler:function(){
        var codigo_documento = $('#codigo_documento').val();
        var cirujano_medico_id = $('#cirujano_medico_id').combobox('getValue');
        var anestesiologo_medico_id = $('#anestesiologo_medico_id').combobox('getValue');
        var instrumentista_id = $('#instrumentista_id').combobox('getValue');
        var asistente_uno_id = $('#asistente_uno_id').combobox('getValue');
        var asistente_dos_id = $('#asistente_dos_id').combobox('getValue');
        var asistente_tres_id = $('#asistente_tres_id').combobox('getValue');
        var enf_circulantes_id = $('#enf_circulantes_id').combobox('getValue');
        $.ajax({
            url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Ingresar_Miembros_Equipos_LV_C',
            type: 'POST',
            dataType: 'json',
            data: {
                codigo_documento: codigo_documento,
                cirujano_medico_id: cirujano_medico_id,
                anestesiologo_medico_id: anestesiologo_medico_id,
                instrumentista_id: instrumentista_id,
                asistente_uno_id: asistente_uno_id,
                asistente_dos_id: asistente_dos_id,
                asistente_tres_id: asistente_tres_id,
                enf_circulantes_id: enf_circulantes_id
            },
            success: function(data){
                if (data.respuesta == 'M_1') {
                    $('#dlg4').dialog('close');
                    return false;
                }
            }
        });
    }
    },{
    text:'Cancel',
    handler:function(){
        $('#dlg4').dialog('close');
        $('#alternativa16').prop('checked', false); 
    }
    }]
">
    Medico Cirujano:<br>
    <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:100%;" id="cirujano_medico_id"><br><br>
    Medico Anestesiologo:<br>
    <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:100%;" id="anestesiologo_medico_id"><br><br>
    Enfermero Instrumentista:<br>
    <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:100%;" id="instrumentista_id"><br><br>
    Asistente 1:<br>
    <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:100%;" id="asistente_uno_id"><br><br>
    Asistente 2:<br>
    <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:100%;" id="asistente_dos_id"><br><br>
    Asistente 3:<br>
    <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:100%;" id="asistente_tres_id"><br><br>
    Enfermera Circulante:<br>
    <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:100%;" id="enf_circulantes_id"><br><br>
</div>

</body>   
</html>   