<?php 
ini_set('memory_limit', '1024M'); 
require('../../MVC_Modelo/bscM.php');
require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Modelo/EmergenciaM.php');
require('../../MVC_Complemento/fpdf/fpdf.php');
require('../../MVC_Complemento/librerias/Funciones.php');

#OBTENIENDO VALORES


class PDF extends FPDF
{	
	function Header(){	
		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
		$fechainicio = $anio."/".$mes."/".$dia;

		list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
		$fechafin = $anio."/".$mes."/".$dia;

		$this->SetFont('Helvetica','',9);
		$this->Cell(60);
		$this->Image('../../MVC_Complemento/img/grcallo.jpg',10,5,10,10);
		$this->Cell(40);
		$this->setfont('Helvetica','b',11);
		$this->Cell(70,2,'LISTA DE VERIFICACION '.utf8_decode('QUIRÚRGICA'),0,0,'C');
		$this->Image('../../MVC_Complemento/img/hndac.jpg',280,5,10,10);
		
		#OBTENIENDO DATOS
		$respuesta = Documento_Detallado_LV_M($_REQUEST['codigo']);

		$this->Ln(5);
		#SERVICIO
		$this->SetFont('Helvetica','B',8);
		$this->Cell(10);
		$this->Cell(100,5,'SERVICIO: ' . utf8_decode($respuesta[0]['SERVICIO']),0,0,'L'); 
		$this->Ln(5);
		#HISTORIA CLINICA
		$this->SetFont('Helvetica','B',8);
		$this->Cell(10);
		$this->Cell(100,5,'HISTORIA CLINICA: ' . $respuesta[0]['NUMEROHISTORIA'],0,0,'L'); 
		#TIPO DE SALA
		$this->Cell(100);
		$this->Cell(100,5,'TIPO SALA: ' . $respuesta[0]['TIPOSALA'] . ' - ' . $respuesta[0]['SALA'],0,0,'L'); 
		$this->Ln(1);	

	}

	function Footer()
	{
		$this->SetY(-6);
		$this->SetFont('Helvetica','',6);
		$this->Cell(235);
		$this->Cell(100,5,"HNDAC / OESY / DESARROLLO DE SISTEMAS",0,0,'L'); 		 				
	}

}

	$pdf = new PDF("L","mm","A4");
	$pdf->SetTitle('Lista Verificacion');
	$pdf->SetAuthor('Rodolfo Crisanto Rosas');
	$pdf->AliasNbPages();
	$pdf->AddPage();

	#GENERANDO DATOS
	$respuesta = Documento_Detallado_LV_M($_REQUEST['codigo']);

	
	#OBTENIENDO LAS ALTERNATIVAS MARCADAS
	$preguntas_contestadas = explode("-", $respuesta[0]['COMBINACIONES']);	
	$preguntas_contestadas = array_filter($preguntas_contestadas);

		for ($l=0; $l < count($preguntas_contestadas); $l++) { 
			$respuestas[] = explode(",", $preguntas_contestadas[$l]);
		}
	
		for ($o=0; $o < count($respuestas); $o++) { 
			$alternativas_contestadas[] = $respuestas[$o][2];
		}


	#LLENANDO EL DOCUMENTO	
	
		#OBTENIENDO MODULOS
		$modulos = Listar_Modulos_LV_M();

		$pdf->SetFont('Helvetica','B',8);
		$pdf->Ln(5);
		for ($i=0; $i < count($modulos); $i++) { 
			$pdf->Cell(91,5,$modulos[$i]['Nombre'],1,0,'C'); 		
		}
		
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',7);
		$pdf->Cell(91,5,'(Con el enfermero y el anestesiologo, como minimo)','LRB',0,'C');
		$pdf->Cell(91,5,'(Con el enfermero y el anestesiologo y cirujano)','LRB',0,'C');
		$pdf->Cell(91,5,'(Con el enfermero y el anestesiologo y cirujano)','LRB',0,'C');
		$pdf->Ln(5);

		#OBTENIENDO PREGUNTAS
		
		$preguntas = Listar_Preguntas_Sin_Modulo_LV_M();
		$pdf->SetFont('Helvetica','',8);
		
		#MODULO ENTRADA (PREGUNTAS DEL 1 AL 7)
		#=====================================
		
		#PREGUNTA 1:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[1]['Nombre'])),'LR');
		$alternativas = Listar_Alternativas_LV_M(1);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 2:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[2]['Nombre'])),'LRT');
		$alternativas = Listar_Alternativas_LV_M(2);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 3:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[3]['Nombre'])),'LRT');
		$alternativas = Listar_Alternativas_LV_M(3);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 4:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[4]['Nombre'])),'LRT');
		$alternativas = Listar_Alternativas_LV_M(4);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 5:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[5]['Nombre'])),'LRT');
		$alternativas = Listar_Alternativas_LV_M(5);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}
		$pdf->MultiCell(91,5,"\n",'LR');

		#PREGUNTA 6:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[6]['Nombre'])),'LR');
		$alternativas = Listar_Alternativas_LV_M(6);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}
		$pdf->MultiCell(91,5,"\n",'LR');

		#PREGUNTA 7:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('Riesgo de hemorragia > de 500 ml (7 ml/Kg en niños)'),'LR');
		$alternativas = Listar_Alternativas_LV_M(7);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}
		$pdf->MultiCell(91,5,' ','LR');
		$pdf->MultiCell(91,5,' ','LR');
		$pdf->MultiCell(91,5,' ','LR');
		$pdf->MultiCell(91,5,' ','LRB');
		
		#MODULO DE PAUSA (PREGUNTAS DEL 8 AL 15)
		#=======================================
		
		#SETEANDO PUNTERO
		$pdf->SetXY(101,36);
		
		#PREGUNTA 8:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('Confirmar que todos los miembros del equipo se haya presentado por su nombre y función'),'R');
		$alternativas = Listar_Alternativas_LV_M(8);
		$y = 45;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 9:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[9]['Nombre'])),'TR');
		$alternativas = Listar_Alternativas_LV_M(9);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 10:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('¿Se ha administrado profilaxis antibiótica en los últimos 60 minutos?'),'TR');
		$alternativas = Listar_Alternativas_LV_M(10);
		$y = $y + 10;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}
		
		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 11:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[11]['Nombre'])),'TR');
		$alternativas = Listar_Alternativas_LV_M(11);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 12:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[12]['Nombre'])),'R');
		$alternativas = Listar_Alternativas_LV_M(12);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 13:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[13]['Nombre'])),'R');
		$alternativas = Listar_Alternativas_LV_M(13);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[ ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}
		
		#SETEANDO PUNTERO
		$y = $y + 5;
		$pdf->SetXY(101,$y);

		#PREGUNTA 14:
		
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[14]['Nombre'])),'R');
		$alternativas = Listar_Alternativas_LV_M(14);
		$y = $y + 10;
		

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[ ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre'])),'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 15:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('¿Pueden visualizarse las imágenes diagnosticas esenciales?'),'TR');
		$alternativas = Listar_Alternativas_LV_M(15);
		$y = $y + 5;
		$cadenita = " ";
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$cadenita .= $tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."     ";
			#$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			
		}
			$pdf->MultiCell(91,6,$cadenita,'RB');


		#MODULO DE SALIDA (PREGUNTAS 16 17)
		#==================================
		
		#SETEANDO PUNTERO
		$pdf->SetXY(192,36);

		#PREGUNTA 16:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[16]['Nombre'])),'TR');
		$alternativas = Listar_Alternativas_LV_M(16);
		$y = 36+5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre'])),'R');

			$y = $y + 10;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(192,$y);

		#PREGUNTA 17:
		
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[17]['Nombre'])),'TR');
		$alternativas = Listar_Alternativas_LV_M(17);
		$y = $y + 5;
		

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre'])),'R');
			$y = $y + 5;
		}
		
		#OBSERVACIONES
		#=============
		
		#SETEANDO PUNTERO
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,'Observaciones','TR');
		
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',8);
		$pdf->MultiCell(91,8,str_replace("-*-", ", ", ucfirst(strtolower($respuesta[0]['OBSERVACIONES']))),'R','J');

						
		#VALIDACIONES
		$y = $y + 15;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,10,'Firma y Sello','TR','C');

			#CIRUJANO
			$y = $y + 10;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',7);
			$pdf->MultiCell(91,5,strtoupper($respuesta[0]['CIRUJANO']),'R','C');
			$y = $y + 3;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','b',6);
			$pdf->MultiCell(91,5,'MEDICO CIRUJANO','R','C');

			#ANESTESIOLGO
			$y = $y + 5;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',7);
			$pdf->MultiCell(91,5,strtoupper($respuesta[0]['ANESTESIOLOGO']),'R','C');
			$y = $y + 3;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','b',6);
			$pdf->MultiCell(91,5,'MEDICO ANESTESIOLOGO','R','C');

			#ENFERMERO INSTRUMENTISTA
			$y = $y + 5;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',7);
			$pdf->MultiCell(91,5,strtoupper($respuesta[0]['CIRCULANTE']),'R','C');
			$y = $y + 3;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','b',6);
			$pdf->MultiCell(91,6,'ENFERMERA CIRCULANTE','R','C');

			#ENFERMERO CIRCULANTE
			$y = $y + 6;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,10,' ','BR');

		#FECHA Y HORA
		#=============
		
		$y = $y + 10;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',7);
		$pdf->MultiCell(91,10,'FECHA: '.date('d/m/Y'),'R', 'C');
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',7);
		$pdf->MultiCell(91,10,'HORA: '.date('h:m:s a'),'RB', 'C');

		
	
	$pdf->Output();
?>

