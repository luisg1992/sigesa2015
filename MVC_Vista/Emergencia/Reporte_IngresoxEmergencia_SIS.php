<?php
 ini_set('memory_limit', '1024M'); 
header("Content-Type: text/html; charset=UTF-8",true);
include("../../MVC_Modelo/EmergenciaM.php");
include("../../MVC_Complemento/librerias/Funciones.php");


require_once '../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->
	getProperties()
		->setCreator("WWW.HNDAC.GOB.PE")
		->setLastModifiedBy("HNDAC")
		->setTitle("HOSPITAL NAVIONAL DANIEL ALCIDES CARRION")
		->setSubject("SISTEMA DE REPORTES - SIGESA ")
		->setDescription("DOCUMENTO GENERADO LA UNIDAD E INFORMATICA ")
		->setKeywords("R.A.B.- DESARROLLO DE SOFTWARE")
		->setCategory("REPORTE");

      
	  
	  
	 		
//$ListarReporte=ReporteIngreso_emergencia01_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["IdServicio"]);
$ListarReporte=Sigesa_ReporteIngresoxEmergencia_SIS_SOAT_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));

 if($ListarReporte != NULL)	{ 
 $i=2;
 foreach($ListarReporte as $item){
	 

if($item["TriajeTalla"]!=NULL){$TriajeTalla='Tal: '.$item["TriajeTalla"];}
if($item["TriajePeso"]!=NULL){$TriajePeso=' Pe: '.$item["TriajePeso"];}
if($item["TriajePresion"]!=NULL){$TriajePresion=' Pr: '.$item["TriajePresion"];}
if($item["TriajeTemperatura"]!=NULL){$TriajeTemperatura=' Tm: '.$item["TriajeTemperatura"];}
if($item["TriajePulso"]!=NULL){$TriajePulso=' Pul: '.$item["TriajePulso"];}
if($item["TriajeFrecRespiratoria"]!=NULL){$TriajeFrecRespiratoria=' F.Rs: '.$item["TriajeFrecRespiratoria"];}
if($item["TriajeFrecCardiaca"]!=NULL){$TriajeFrecCardiaca=' F.Cr: '.$item["TriajeFrecCardiaca"];}

 
 
  


	 
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Nº Cuen')
            ->setCellValue('B1', 'Apellidos y Nombres')
            ->setCellValue('C1', 'HC')
			->setCellValue('D1', 'DNI')
			->setCellValue('E1', 'Edad')
			->setCellValue('F1', 'Sexo')
			->setCellValue('G1', 'Topico')
			->setCellValue('H1', 'Fecha Hora Ingreso')
			->setCellValue('I1', 'Fuente Financiamiento')
			->setCellValue('J1', 'Gravedad')
			->setCellValue('K1', 'CIE-10')
			->setCellValue('L1', 'Signos Vitales.')
			->setCellValue('M1', 'NUM FUA')
			//->setCellValue('H1', 'HC')
			
			->setCellValue('A'.$i, $item["IdCuentaAtencion"] )
            ->setCellValue('B'.$i, utf8_encode($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["PrimerNombre"]))
			->setCellValue('C'.$i, $item["NroHistoriaClinica"])
			->setCellValue('D'.$i, $item["NroDocumento"])
			//->setCellValue('E'.$i, $item["FechaNacimiento"])
			->setCellValue('E'.$i, $item["EdadPci"])
			->setCellValue('F'.$i, $item["SexoPaci"])
			->setCellValue('G'.$i, $item["NombreServicio"])
			->setCellValue('H'.$i, $item["FechaIngreso"].' '.$item["HoraIngreso"])
			->setCellValue('I'.$i, $item["NombreFuenteFinanciam"])
			->setCellValue('J'.$i, $item["TipoGravedad"])
			->setCellValue('K'.$i, $item["NombeDiagnos"])
			->setCellValue('L'.$i, $TriajeTalla.$TriajePeso.$TriajePresion.$TriajeTemperatura.$TriajePulso.$TriajeFrecRespiratoria.$TriajeFrecCardiaca )
			->setCellValue('M'.$i, $item["NUMFUA"])
			
			 ;
$i++;
 }
 }
 					 		

 
$objPHPExcel->getActiveSheet()->setTitle('Ingreso_Emergencia');
$objPHPExcel->setActiveSheetIndex(0);



header('Content-Type: application/vnd.ms-excel;charset=UTF-8');
header('Content-Disposition: attachment;filename="ISNGRESOS_SIS.xls"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

 
  

 
?>