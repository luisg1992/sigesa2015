    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Emergencia</title>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

		
		.letras_peque�as{
			font-size:9px;
			font-weight:bold;
			color:red;
		}
		
		.letras_peque�as_2{
			font-size:8px;
			font-weight:bold;
			color:red;
		}

		label[for=message]
		{
		font-weight:bold;
		}
		</style>
		
		
		<script>
						
			$(document).ready(function() {
/*
				$('.easyui-datebox').datebox({
					formatter : function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
					},
					parser : function(s){

						if (!s) return new Date();
						var ss = s.split('-');
						var y = parseInt(ss[2],10);
						var m = parseInt(ss[1],10);
						var d = parseInt(ss[0],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d)
						} else {
							return new Date();
						}
					}

				});				
						

				function Lista_Pacientes_Emergencia(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Servicio){
									var dg = $('#Lista_Pacientes_Emergencia').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Ingreso_Emergencia_Modificar&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final+'&Servicio='+Servicio,
									columns:[[
											{field:'IdCuentaAtencion',title:'Nombre de Cajero',width:190,sortable:true},
											{field:'ApellidoMaterno',title:'Nombres y Apellidos del Paciente',width:250,sortable:true},
											{field:'ApellidoPaterno',title:'Usuario Realizo<br>Extorno',width:150,sortable:true},
											{field:'NroHistoriaClinica',title:'Usuario Realizo<br>Extorno',width:150,sortable:true}
										]]
									});
									dg.datagrid('enableFilter');

				}
				
/*
				
				$("#Generar_Reporte_I").click(function(event) {
					$('#Titulo_Reporte').text('Pacientes de Emergencia por Servicios');
					$('.Contenedor_Reporte_I').show();			
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var Servicio=$( "#Servicio option:selected" ).val();
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Pacientes_Emergencia(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Servicio);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				
				
				$("#Generar_Excel_II").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var Servicio=$( "#Servicio option:selected" ).val();
					Exportar_Reporte_Extornos(Fecha_Inicial_Reporte,Fecha_Final_Reporte);
				});
				
				
				$("#Generar_Pdf_IV").click(function(event) {
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var Servicio=$( "#Servicio option:selected" ).val();
					location="../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=PDF_Aseguradoras&FechaInicio="+Fecha_Inicial_Reporte+"&FechaFinal="+Fecha_Final_Reporte+"&IdFuenteFinanciamiento="+IdFuenteFinanciamiento+"&IdTipoServicio="+IdTipoServicio;
				});
				
*/
				
			});
		</script>
    </head>
    <body>
		<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
		<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
		<div class="easyui-layout" style="width:100%;height:800px;">
				<div data-options="region:'center',iconCls:'icon-ok'"  style="width:80%;">
					<div class="easyui-tabs" data-options="tabWidth:100,tabHeight:60" style="width:100%;height:100%;">
						<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/lista.png' width='35' height='35'/>
						<br>Lista</span>" style="padding:10px;height:850px">
								<div style="margin:0px 0px 16px 0px; background:red; height:80px;width:600px"> 
									<div style="float:left;margin:5px 20px 0 20px">									  
									  <div style="float:left;margin:5px 20px 0 20px">
									  <input type="radio" name="ordenar_consulta" value="0" checked><strong>Servicio y Apellidos</strong><br>
									  <input type="radio" name="ordenar_consulta" value="1"><strong>Servicio,Fecha</strong><br>
									  </div>
									  <div style="float:left;margin:5px 20px 0 20px">
									  <input type="radio" name="ordenar_consulta" value="3"><strong>Apellidos</strong><br>
									  <input type="radio" name="ordenar_consulta" value="4"><strong>Hora,Fecha</strong><br>
									  </div>
									</div>
									<div style="padding:5px 0;float:left;margin:5px 20px 0 20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_MATRICIAL_I">Generar Matricial</a>
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_PDF_I" >Generar PDF</a>
									</div>
								</div>
								<span id="Titulo_Reporte" style="padding-top:19;padding-left:4px;padding-bottom:60px;font-weight:bold;font-size:18px;Color:#585858"></span>
								<div  style="width:100%;height: 650px"  class="Contenedor_Reporte_I" ; >
								zz
								<table id="Lista_Pacientes_Emergencia"></table>
								</div>
						</div>	
					</div>
					<style scoped="scoped">
						.tt-inner{
							display:inline-block;
							line-height:12px;
							padding-top:5px;
						}
						.tt-inner img{
							border:0;
						}
					</style>
					
					
				</div>
				<div data-options="region:'west',split:true"  style="width:20%;">
				<div class="easyui-tabs" style="width:100%;">
					<div title="Reportes de Emergencia">
						<div title="Reporte de Emergencia por Servicios" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de Emergencia por Servicios</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_I" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_I" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Servicio<br> de Emergencia :</span>
									<br>
									<select  name="Servicio" style="width:270px" id="Servicio" >
									<option value="">Todos</option>
									<?php 
									$resultados=ServiciosEmergencia_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["IdServicio"] ?>" <?php if($_REQUEST["IdServicio"]==$resultados[$i]["IdServicio"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Servicio"]) ?></option>
									<?php 
									}}
									?>                   
									</select>
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:90px" id="Generar_Reporte_I">Aceptar</a>
								</div>			
						</div>					
					</div>	
					</div>	
				</div>
			</div>
		</div>
    </body>
    </html>



