<?php
 ini_set('memory_limit', '1024M'); 
 ini_set('max_execution_time', 300);
header("Content-Type: text/html; charset=UTF-8",true);
include("../../../MVC_Modelo/EpidemiologiaM.php");
include("../../../MVC_Complemento/librerias/Funciones.php");


require_once '../../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->
	getProperties()
		->setCreator("WWW.HNDAC.GOB.PE")
		->setLastModifiedBy("HNDAC")
		->setTitle("HOSPITAL NAVIONAL DANIEL ALCIDES CARRION")
		->setSubject("SISTEMA DE REPORTES - SIGESA ")
		->setDescription("DOCUMENTO GENERADO LA UNIDAD E INFORMATICA ")
		->setKeywords("R.A.B.- DESARROLLO DE SOFTWARE")
		->setCategory("REPORTE");

      
	  
	  
	 		
$ListarReporte=Reporte_Epidemiologia_Ingresos_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["IdServicio"]);

 if($ListarReporte != NULL)	{ 
 $i=2;
 foreach($ListarReporte as $item){
	 

if($item["TriajeTalla"]!=NULL){$TriajeTalla='Tal: '.$item["TriajeTalla"];}
if($item["TriajePeso"]!=NULL){$TriajePeso=' Pe: '.$item["TriajePeso"];}
if($item["TriajePresion"]!=NULL){$TriajePresion=' Pr: '.$item["TriajePresion"];}
if($item["TriajeTemperatura"]!=NULL){$TriajeTemperatura=' Tm: '.$item["TriajeTemperatura"];}
if($item["TriajePulso"]!=NULL){$TriajePulso=' Pul: '.$item["TriajePulso"];}
if($item["TriajeFrecRespiratoria"]!=NULL){$TriajeFrecRespiratoria=' F.Rs: '.$item["TriajeFrecRespiratoria"];}
if($item["TriajeFrecCardiaca"]!=NULL){$TriajeFrecCardiaca=' F.Cr: '.$item["TriajeFrecCardiaca"];}



	 
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Nº Cuen')
            ->setCellValue('B1', 'Apellidos y Nombres')
            ->setCellValue('C1', 'HC')
			->setCellValue('D1', 'DNI')
			->setCellValue('E1', 'Edad')
			->setCellValue('F1', 'Sexo')
			->setCellValue('G1', 'Topico')
			->setCellValue('H1', 'Fecha Ingreso')
			->setCellValue('I1', 'Hora Ingreso')
			->setCellValue('J1', 'Turno')
			->setCellValue('K1', 'Fuente Financiamiento')
			->setCellValue('L1', 'Gravedad')
			->setCellValue('M1', 'CIE-10')
			->setCellValue('N1', 'Diagnostico')
			->setCellValue('O1', 'Signos Vitales.')
			->setCellValue('P1', 'NUMFUA')

			//->setCellValue('H1', 'HC')
			
			->setCellValue('A'.$i, $item["IdCuentaAtencion"] )
            ->setCellValue('B'.$i, utf8_encode($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["PrimerNombre"]))
			->setCellValue('C'.$i, $item["NroHistoriaClinica"])
			->setCellValue('D'.$i, $item["NroDocumento"])
			//->setCellValue('E'.$i, $item["FechaNacimiento"])
			->setCellValue('E'.$i, $item["EdadPci"])
			->setCellValue('F'.$i, $item["SexoPaci"])
			->setCellValue('G'.$i, $item["NombreServicio"])
			->setCellValue('H'.$i, $item["FechaIngreso"])
			->setCellValue('I'.$i, $item["HoraIngreso"])
			->setCellValue('J'.$i, $item["TipoTurno"])
			->setCellValue('K'.$i, $item["NombreFuenteFinanciam"])
			->setCellValue('L'.$i, $item["TipoGravedad"])
			->setCellValue('M'.$i, $item["CIE10"])
			->setCellValue('N'.$i, $item["NombreDiagnos"])
			->setCellValue('O'.$i, $TriajeTalla.$TriajePeso.$TriajePresion.$TriajeTemperatura.$TriajePulso.$TriajeFrecRespiratoria.$TriajeFrecCardiaca )
			->setCellValue('P'.$i, $item["NUMFUA"])
			 ;
$i++;
 }
 }
 					 		

 
$objPHPExcel->getActiveSheet()->setTitle('Ingreso_Emergencia');
$objPHPExcel->setActiveSheetIndex(0);



header('Content-Type: application/vnd.ms-excel;charset=UTF-8');
header('Content-Disposition: attachment;filename="Reporte_Epidemiologia.xls"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

 	
  

 
?>