<?php
ini_set('memory_limit', '1024M');
require('../../MVC_Modelo/EstadisticasM.php');
require('../../MVC_Modelo/SistemaM.php');
 
require('../../MVC_Complemento/librerias/Funciones.php');
 
 ?><!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Highcharts Example</title>
 
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <style type="text/css">
${demo.css}
        </style>
        <script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Citas Vendidas '
        },
        subtitle: {
            text: 'Por Fuente de Financiamiento'
        },
        xAxis: {
            categories: [
                'Ene',
                'Feb',
                'Mar',
                'Abr',
                'May',
                'Jun',
                'Jul',
                'Ago',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: false
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Num. Citas'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [<?php $ListarCitasxFuenteFinanciaminto=CitasVendidasxFuenteFinaciamiento_M();
            if($ListarCitasxFuenteFinanciaminto != NULL){         
            $a=0;
            foreach($ListarCitasxFuenteFinanciaminto as $item){
             if($a==0){echo "{";}else{echo ",{";}
            ?>
            name: '<?php echo $item['Descripcion']; ?>',
            data: [
            <?php if($item['1']==NULL){echo "0";}else{echo $item['1'];} ?>,
            <?php if($item['2']==NULL){echo "0";}else{echo $item['2'];} ?>,
            <?php if($item['3']==NULL){echo "0";}else{echo $item['3'];} ?>,
            <?php if($item['4']==NULL){echo "0";}else{echo $item['4'];} ?>,
            <?php if($item['5']==NULL){echo "0";}else{echo $item['5'];} ?>,
            <?php if($item['6']==NULL){echo "0";}else{echo $item['6'];} ?>,
            <?php if($item['7']==NULL){echo "0";}else{echo $item['7'];} ?>,
            <?php if($item['8']==NULL){echo "0";}else{echo $item['8'];} ?>,
            <?php if($item['9']==NULL){echo "0";}else{echo $item['9'];} ?>,
            <?php if($item['10']==NULL){echo "0";}else{echo $item['10'];} ?>,
            <?php if($item['11']==NULL){echo "0";}else{echo $item['11'];} ?>,
            <?php if($item['12']==NULL){echo "0";}else{echo $item['12'];} ?>]
            <?php echo "}"; $a++;}}?>]
    });
});
        </script>
    </head>
    <body>
<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
             <?php $ListarCitasxFuenteFinanciaminto=CitasVendidasxFuenteFinaciamiento_M();
            if($ListarCitasxFuenteFinanciaminto != NULL){         
            $a=0;
            foreach($ListarCitasxFuenteFinanciaminto as $item){
             if($a==0){echo "{";}else{echo ",{";}
            ?>
            name: '<?php echo $item['Descripcion']; ?>',
            data: [
            <?php if($item['1']==NULL){echo "0";}else{echo $item['1'];} ?>,
            <?php if($item['2']==NULL){echo "0";}else{echo $item['2'];} ?>,
            <?php if($item['3']==NULL){echo "0";}else{echo $item['3'];} ?>,
            <?php if($item['4']==NULL){echo "0";}else{echo $item['4'];} ?>,
            <?php if($item['5']==NULL){echo "0";}else{echo $item['5'];} ?>,
            <?php if($item['6']==NULL){echo "0";}else{echo $item['6'];} ?>,
            <?php if($item['7']==NULL){echo "0";}else{echo $item['7'];} ?>,
            <?php if($item['8']==NULL){echo "0";}else{echo $item['8'];} ?>,
            <?php if($item['9']==NULL){echo "0";}else{echo $item['9'];} ?>,
            <?php if($item['10']==NULL){echo "0";}else{echo $item['10'];} ?>,
            <?php if($item['11']==NULL){echo "0";}else{echo $item['11'];} ?>,
            <?php if($item['12']==NULL){echo "0";}else{echo $item['12'];} ?>]
            <?php echo "}"; $a++;}}?>
    </body>
</html>