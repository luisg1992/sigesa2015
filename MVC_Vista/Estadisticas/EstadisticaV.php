<?php
ini_set('memory_limit', '1024M');
require('../../MVC_Modelo/EstadisticasM.php');
//require('../../MVC_Modelo/SistemaM.php');
require('../../MVC_Complemento/librerias/Funciones.php');
 
 ?><!DOCTYPE HTML>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Highcharts Example</title>
		<!-- CSS -->
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
	<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }	
		.highcharts-title{
			font-size:14px !important;
		}

    </style>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript">

		
		$(document).ready(function() {
			
				 /* Cargara por Defecto este Highchart */
				 Highchart_Lineas_por_Mes();	
			
			
				$("#exportar_excel_btn").click(function(event) {
					Exportar_Excel();	
				});

				
				$("#Lineas_por_Mes").click(function(event) {
					$('#container').show();
					$('#Contenido_Excel').hide();
					Highchart_Lineas_por_Mes();	
				});
				
				$("#Columnas_por_Mes").click(function(event) {
					$('#container').show();
					$('#Contenido_Excel').hide();
					Highchart_Columnas_por_Mes();	
				});
				

				$("#Spline_por_Mes").click(function(event) {
					$('#container').show();
					$('#Contenido_Excel').hide();
					Highchart_Spline_por_Mes();	
				});
				
				
				$("#Barras_por_Mes").click(function(event) {
					$('#container').show();
					$('#Contenido_Excel').hide();
					Highchart_Barras_por_Mes();	
				});
				
				
				$("#Pilas_Columnas_por_Mes").click(function(event) {
					$('#container').show();
					$('#Contenido_Excel').hide();
					Highchart_Pilas_Columnas_por_Mes();	
				});
				
				
				$("#Pilas_Barras_por_Mes").click(function(event) {
					$('#container').show();
					Highchart_Pilas_Barras_por_Mes();	
				});
				
					
				$("#Generar_Reporte_por_Periodo").click(function(event) {
		
		
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte').datebox('getValue');
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte < Fecha_Final_Reporte)
								{
								Highchart_Columnas_por_Periodo(Fecha_Inicial_Reporte,Fecha_Final_Reporte);	
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				

				


				/*  Reporte de Citas vs Citas   */
	
				
				$("#Lineas_Citas_Vs_Citas").click(function(event) {
					$('#container').show();
					Highchart_Lineas_Citas_Vs_Citas();	
				});
				
				$("#Columnas_Citas_Vs_Citas").click(function(event) {
					$('#container').show();
					Highchart_Columnas_Citas_Vs_Citas();	
				});
				
				$("#Barras_por_Citas_Vs_Citas").click(function(event) {
					$('#container').show();
					Highchart_Barras_Citas_Vs_Citas();	
				});
				
				$("#Spline_Citas_Vs_Citas").click(function(event) {
					$('#container').show();
					Highchart_Spline_Citas_Vs_Citas();	
				});
				
				$("#Pilas_Columnas_Citas_Vs_Citas").click(function(event) {
					$('#container').show();
					Highchart_Pilas_Columnas_Citas_Vs_Citas();	
				});
				
				$("#Pilas_Barras_Citas_Vs_Citas").click(function(event) {
					$('#container').show();
					Highchart_Pilas_Barras_Citas_Vs_Citas();	
				});
				
				
				
				$("#Generar_Hora_Medico").click(function(event) {
					$('#container').show();
					Highchart_Generar_Hora_Medico();	
				});


				$("#Generar_Grilla_Especialidades").click(function(event) {
					$('#container').hide();
					$('#Contenido_Excel').show();
					Grilla_Especialidades();	
				});

				
				$("#Generar_Excel_Especialidades").click(function(event) {
					Exportar_Excel_Grilla_Especialidades();	
				});

				
				
				

				/* Fin de RePorte de Citas vs Citas */
				
				
				//Funcion que Carga la Grilla de Servicios Ambulatorios
				function Grilla_Especialidades()
				{		

					var IdDepartamento=document.getElementById('IdDepartamento_4').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad_4').value;
					var Anio=document.getElementById('Anio_4').value;
					var Mes=document.getElementById('Mes_4').value;
					var Especialidad_Contenido= $("#IdEspecialidad_4 option:selected").html();
					var Mes_Contenido = $("#Mes_4 option:selected").html();
					var Anio_Contenido= $("#Anio_4 option:selected").html();
					var Contenido_1="HOSPITAL NACIONAL DANIEL ALCIDES CARRION <BR>";
					var Contenido_2="ESPECIALIDAD : "+Especialidad_Contenido+"<BR>";
					var Contenido_3="PERIODO  : A "+Mes_Contenido+' '+Anio_Contenido+"<BR>";
					$("#Contenido_Cabecera").html(Contenido_1+Contenido_2+Contenido_3.toUpperCase());
					var dg =$('#Grilla_Especialidades').datagrid({
					iconCls:'icon-edit',
					collapsible:true,
					url:'../../MVC_Controlador/Reportes/ReportesC.php?acc=Reporte_Grilla_Especialidades&IdDepartamento='+IdDepartamento+'&IdEspecialidad='+IdEspecialidad+'&Anio='+Anio+'&Mes='+Mes,
					columns:[[
					{field:'MES',title:'Mes ',width:110,align:'center'},	
					{field:'CITA_OFERTADA',title:'Citas Ofertadas',width:120,align:'center'},		
					{field:'CITA_VENDIDA',title:'Citas Vendidas',width:120,align:'center'},
					{field:'CITA_ATENDIDA',title:'Citas Atendidas',width:120,sortable:true,align:'center'},
					{field:'PORCENTAJE',title:'%',width:100,sortable:true,align:'center'}
				]]		
				});	
				}	
				
				/* Da Formato correcto al easyui  */
				
				$('.easyui-datebox').datebox({
					formatter : function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
					},
					parser : function(s){

						if (!s) return new Date();
						var ss = s.split('-');
						var y = parseInt(ss[2],10);
						var m = parseInt(ss[1],10);
						var d = parseInt(ss[0],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d)
						} else {
							return new Date();
						}
					}

				});	
				
				/* Fin Formato correcto al easyui  */
				
				
				
				
				$("#IdDepartamento").change(function () {
				   $("#IdDepartamento option:selected").each(function () {
						elegido=$(this).val();
						$.post("../../MVC_Controlador/Reportes/ReportesC.php?acc=Selected_IdDepartamento", { elegido: elegido }, function(data){
						$("#IdEspecialidad").html(data);
						});            
					});
				})
	
				
				
				$("#IdDepartamento_2").change(function () {
				   $("#IdDepartamento_2 option:selected").each(function () {
						elegido=$(this).val();
						$.post("../../MVC_Controlador/Reportes/ReportesC.php?acc=Selected_IdDepartamento", { elegido: elegido }, function(data){
						$("#IdEspecialidad_2").html(data);
						});            
					});
				})	

				
				$("#IdDepartamento_3").change(function () {
				   $("#IdDepartamento_3 option:selected").each(function () {
						elegido=$(this).val();
						$.post("../../MVC_Controlador/Reportes/ReportesC.php?acc=Selected_IdDepartamento", { elegido: elegido }, function(data){
						$("#IdEspecialidad_3").html(data);
						});            
					});
				})
				
				


				$("#IdDepartamento_4").change(function () {
				   $("#IdDepartamento_4 option:selected").each(function () {
						elegido=$(this).val();
						$.post("../../MVC_Controlador/Reportes/ReportesC.php?acc=Selected_IdDepartamento", { elegido: elegido }, function(data){
						$("#IdEspecialidad_4").html(data);
						});            
					});
				})
				




				/************** Rendimiento Hora / Medico  *********************/
				
				
				function Highchart_Generar_Hora_Medico()
				{
					var IdEspecialidad=document.getElementById('IdEspecialidad_3').value;
					var Anio=document.getElementById('Anio_3').value;
					var Mes=document.getElementById('Mes_3').value;
					if ($('#Ofertadas').is(":checked"))
					{
					 var Tipo_Busqueda='2';
					}
					else
					{
					 var Tipo_Busqueda='1';	
					}
					
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Generar_Hora_Medico',
						type: 'POST',
						dataType: 'json',
						data: {
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes,
							Tipo_Busqueda:Tipo_Busqueda
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);						
							$('#container').highcharts({			
							        chart: {
										type: 'column'
									},
									title: {
										text: '<strong>Departamento </strong>: '+$("#IdDepartamento_3 option:selected").html()+
											  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad_3 option:selected").html()+
											   '<br><strong>Año  </strong>: '+$("#Anio_3 option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes_3 option:selected").html()+'<br>',
									},
									xAxis: {
										type: 'category',
										labels: {
											style: {
												fontSize: '12px',
												fontFamily: 'Verdana, sans-serif'
											}
										}
									},
									yAxis: {
										min: 0,
										title: {
											text: 'Rendimiento Hora / Médico'
										}
									},
									legend: {
										enabled: false
									},
									tooltip: {
										pointFormat: 'Médico'
									},
									credits: {
									enabled: false
									},
									plotOptions: {
											column: {
												dataLabels: 
												{
													enabled: true,
													color: '#FFFFFF',
													align: 'right',
													format: '{point.y:.1f}', // one decimal
													y: 10, // 10 pixels down from the top
													style: {
														fontSize: '13px',
														fontFamily: 'Verdana, sans-serif'
													}
												}
											}
									},
									series: [{ data: objeto }]	
						});
						}
						});	
				}						
				
						
				/********  Reportes Rango de Fechas de  Citas vs Citas  *************/
				function Highchart_Columnas_por_Periodo(Fecha_Inicial_Reporte,Fecha_Final_Reporte)
				{

					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Citas_Vendidas_Citas_Ofertadas_Periodo',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial_Reporte:Fecha_Inicial_Reporte,
							Fecha_Final_Reporte:Fecha_Final_Reporte
						},

						success:function(res)
						{					
						objeto=jQuery.parseJSON(res);
							$('#container').highcharts({			
							    chart: {
									type: 'column'
								},
								title: {
									text: '<strong>CITAS POR DEPARTAMENTO</strong><br><strong>PERIODO DEL : '+Fecha_Inicial_Reporte+' AL '+Fecha_Final_Reporte+' </strong><br>'
								},
								subtitle: {
									text: '<strong>Diagrama de Columnas</strong><br>Citas Ofertadas vs Citas Vendidas vs Citas Atendidas'
								},
								xAxis: {
									categories: [
										'Medicina',
										'Pediatria',
										'Ginecología y Obstetricia',
										'Cirugía',
										'Servicios No Medicos',
										'Oncologia',
										'Anestesiología',
										'Medicina de Rehabilitacion',
										'Atencion Ambulatoria y Hospitalizacion',
										'Medicina'
									],
									crosshair: true
								},
								credits: {
								enabled: false
								},
								yAxis: {
									min: 0,
									title: {
										text: 'Num. Citas'
									}
								},
								tooltip: {
									headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
									pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
										'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
									footerFormat: '</table>',
									shared: true,
									useHTML: true
								},
								legend: {
											align: 'right',
											x: -30,
											verticalAlign: 'top',
											y: 25,
											floating: true,
											backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
											borderColor: '#CCC',
											borderWidth: 1,
											shadow: false
								},
								plotOptions: {
									column: {
										dataLabels: {
										enabled: true
										},
										pointPadding: 0.2,
										borderWidth: 0
									}
								},
								series: objeto

						});
						}
						});	
				}						
				
				
				/******** Fin  Reportes Rango de Fechas de  Citas vs Citas  *************/
				
				
				
				
				
				
				
				
				
				
				/********  Reportes de Citas vs Citas  */
				
				
				function Highchart_Lineas_Citas_Vs_Citas()
				{
					var IdDepartamento=document.getElementById('IdDepartamento_2').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad_2').value;
					var Anio=document.getElementById('Anio_2').value;
					var Mes=document.getElementById('Mes_2').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_VS_Citas',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({

						
										chart: {
										type: 'line'
										},
										title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento_2 option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad_2 option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio_2 option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes_2 option:selected").html()+'<br>',
											align: 'left',
											x: 110
										},
										subtitle: {
											text: '<strong>Diagrama de Líneas</strong><br>Citas Ofertadas vs Citas Vendidas vs Citas Atendidas'
										},
										xAxis: {
											categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
										},
										yAxis: {
											title: {
												text: 'Num. Citas'
											}
										},
										legend: {
											align: 'right',
											x: -30,
											verticalAlign: 'top',
											y: 25,
											floating: true,
											backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
											borderColor: '#CCC',
											borderWidth: 1,
											shadow: false
										},
										credits: {
										enabled: false
										},
										plotOptions: {
											line: {
												dataLabels: {
													enabled: true
												},
												enableMouseTracking: false
											}
										},
										
										series: objeto

							});
						}
					});

					
					
				}
				

				function Highchart_Columnas_Citas_Vs_Citas()
				{
					
					var IdDepartamento=document.getElementById('IdDepartamento_2').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad_2').value;
					var Anio=document.getElementById('Anio_2').value;
					var Mes=document.getElementById('Mes_2').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_VS_Citas',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{	
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({			
						
						 chart: {
								type: 'column'
							},
							title: {
								text: '<strong>Departamento </strong>: '+$("#IdDepartamento_2 option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad_2 option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio_2 option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes_2 option:selected").html()+'<br>',
								align: 'left',
								x: 110
							},
							subtitle: {
								text: '<strong>Diagrama de Columnas</strong><br>Citas Ofertadas vs Citas Vendidas vs Citas Atendidas'
							},
							xAxis: {
								categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
								crosshair: true
							},
							yAxis: {
								min: 0,
								title: {
									text: 'Num. Citas'
								}
							},
							
							legend: {
								align: 'right',
								x: -30,
								verticalAlign: 'top',
								y: 25,
								floating: true,
								backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
								borderColor: '#CCC',
								borderWidth: 1,
								shadow: false
							},
							tooltip: {
								headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
									'<td style="padding:0"><b>{point.y:.1f} Citas</b></td></tr>',
								footerFormat: '</table>',
								shared: true,
								useHTML: true
							},
							credits: {
								enabled: false
							},
							plotOptions: {
								column: {
									pointPadding: 0.2,
									borderWidth: 0,
									dataLabels: {
									enabled: true
									}
								}
							},
							series: objeto

						});

						}
					});
					
				}
				

				
				function Highchart_Barras_Citas_Vs_Citas()
				
				{
					var IdDepartamento=document.getElementById('IdDepartamento_2').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad_2').value;
					var Anio=document.getElementById('Anio_2').value;
					var Mes=document.getElementById('Mes_2').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_VS_Citas',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({
									chart: {
										type: 'bar'
									},
									title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento_2 option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad_2 option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio_2 option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes_2 option:selected").html()+'<br>',
											align: 'left',
											x: 110
									},
									subtitle: {
											text: '<strong>Diagrama de Barras</strong><br>Citas Ofertadas vs Citas Vendidas vs Citas Atendidas'
									},
									xAxis: {
										categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
										title: {
											text: null
										}
									},
									yAxis: {
										min: 0,
										title: {
											text: 'Num. Citas',
											align: 'high'
										},
										labels: {
											overflow: 'justify'
										}
									},
									plotOptions: {
										bar: {
											dataLabels: {
												enabled: true
											}
										}
									},
									legend: {
										align: 'right',
										x: -30,
										verticalAlign: 'top',
										y: 25,
										floating: true,
										backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
										borderColor: '#CCC',
										borderWidth: 1,
										shadow: false
									},
									credits: {
										enabled: false
									},
									series: objeto
									
							});

						}
					});
					
				}
			
				
				
				function Highchart_Spline_Citas_Vs_Citas()
				{
					var IdDepartamento=document.getElementById('IdDepartamento_2').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad_2').value;
					var Anio=document.getElementById('Anio_2').value;
					var Mes=document.getElementById('Mes_2').value;
					
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_VS_Citas',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);	
						$('#container').highcharts({
							    chart: {
										type: 'areaspline'
									},
									title: {
										text: '<strong>Departamento </strong>: '+$("#IdDepartamento_2 option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad_2 option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio_2 option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes_2 option:selected").html()+'<br>',
										align: 'left',
										x: 110
									},
									subtitle: {
										text: '<strong>Diagrama de Area Curva</strong><br>Citas Ofertadas vs Citas Vendidas vs Citas Atendidas'
									},

									xAxis: {
										categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'
										],
										plotBands: [{ // visualize the weekend
											from: 4.5,
											to: 6.5,
											color: 'rgba(68, 170, 213, .2)'
										}]
									},
									yAxis: {
										title: {
											text: 'Num. Citas'
										}
									},
									tooltip: {
										shared: true,
										valueSuffix: ' Citas'
									},
									credits: {
										enabled: false
									},
									legend: {
										align: 'right',
										x: -30,
										verticalAlign: 'top',
										y: 25,
										floating: true,
										backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
										borderColor: '#CCC',
										borderWidth: 1,
										shadow: false
									},
									plotOptions: {
										areaspline: {
											fillOpacity: 0.5,
											dataLabels: {
											enabled: true
											}
										}
									},
									series: objeto	
									
						});
						}
					});
					
				}

								
				function Highchart_Pilas_Columnas_Citas_Vs_Citas()
				{
					
					var IdDepartamento=document.getElementById('IdDepartamento_2').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad_2').value;
					var Anio=document.getElementById('Anio_2').value;
					var Mes=document.getElementById('Mes_2').value;
					
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_VS_Citas',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({

									 chart: {
											type: 'column'
										},
										title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento_2 option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad_2 option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio_2 option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes_2 option:selected").html()+'<br>',
											align: 'left',
											x: 110
										},
										subtitle: {
											text: '<strong>Diagrama de Pilas Columnas</strong><br>Citas Ofertadas vs Citas Vendidas vs Citas Atendidas'
										},
										xAxis: {
											categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
										},
										yAxis: {
											min: 0,
											title: {
												text: 'Num. Citas'
											},
											stackLabels: {
												enabled: true,
												style: {
													fontWeight: 'bold',
													color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
												}
											}
										},
										legend: {
											align: 'right',
											x: -30,
											verticalAlign: 'top',
											y: 25,
											floating: true,
											backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
											borderColor: '#CCC',
											borderWidth: 1,
											shadow: false
										},
										tooltip: {
											headerFormat: '<b>{point.x}</b><br/>',
											pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
										},
										credits: {
										enabled: false
										},
										plotOptions: {
											column: {
												stacking: 'normal',
												dataLabels: {
													enabled: true,
													color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
												}
											}
										},
										series: objeto

							});
						}
					});

					
					
				}

				
				function Highchart_Pilas_Barras_Citas_Vs_Citas()
				{
					var IdDepartamento=document.getElementById('IdDepartamento_2').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad_2').value;
					var Anio=document.getElementById('Anio_2').value;
					var Mes=document.getElementById('Mes_2').value;
					
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_VS_Citas',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({					
									 chart: {
											type: 'bar'
										},
										title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento_2 option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad_2 option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio_2 option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes_2 option:selected").html()+'<br>',
											align: 'left',
											x: 110
										},
										subtitle: {
											text: '<strong>Diagrama de Pilas Barras</strong><br>Citas Ofertadas vs Citas Vendidas vs Citas Atendidas'
										},
										xAxis: {
											categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
										},
										yAxis: {
											min: 0,
											title: {
												text: 'Num. Citas'
											},
											stackLabels: {
												enabled: true,
												style: {
													fontWeight: 'bold',
													color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
												}
											}
										},
										legend: {
											align: 'right',
											x: -30,
											verticalAlign: 'top',
											y: 25,
											floating: true,
											backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
											borderColor: '#CCC',
											borderWidth: 1,
											shadow: false
										},
										tooltip: {
											headerFormat: '<b>{point.y}</b><br/>',
											pointFormat: '{series.name}: {point.x}<br/>Total: {point.stackTotal}'
										},
										credits: {
										enabled: false
										},
										plotOptions: {
											series: {
												stacking: 'normal',
												dataLabels: {
													enabled: true,
													color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
												}
											}
										},
										series: objeto

							});
						}
					});

					
					
				}

				/******** Fin  Reportes de Citas vs Citas  *************/
				
				
				
				
				

				
				
				
				
				
				
				/******** Reportes Rango de Citas  *************/
				
				
				
				function Highchart_Pilas_Columnas_por_Mes()
				{
					var IdDepartamento=document.getElementById('IdDepartamento').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad').value;
					var Anio=document.getElementById('Anio').value;
					var Mes=document.getElementById('Mes').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Mes',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({

									 chart: {
											type: 'column'
										},
										title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes option:selected").html()+'<br>',
											align: 'left',
											x: 110
										},
										subtitle: {
											text: '<strong>Diagrama de Pilas Columnas</strong><br>Citas Vendidas por Fuente de Financimiento'
										},
										xAxis: {
											categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
										},
										yAxis: {
											min: 0,
											title: {
												text: 'Num. Citas'
											},
											stackLabels: {
												enabled: true,
												style: {
													fontWeight: 'bold',
													color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
												}
											}
										},
										legend: {
											align: 'right',
											x: -30,
											verticalAlign: 'top',
											y: 25,
											floating: true,
											backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
											borderColor: '#CCC',
											borderWidth: 1,
											shadow: false
										},
										tooltip: {
											headerFormat: '<b>{point.x}</b><br/>',
											pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
										},
										credits: {
										enabled: false
										},
										plotOptions: {
											column: {
												stacking: 'normal',
												dataLabels: {
													enabled: true,
													color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
												}
											}
										},
										series: objeto

							});
						}
					});

					
					
				}

				
				function Highchart_Pilas_Barras_por_Mes()
				{
					var IdDepartamento=document.getElementById('IdDepartamento').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad').value;
					var Anio=document.getElementById('Anio').value;
					var Mes=document.getElementById('Mes').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Mes',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({					
									 chart: {
											type: 'bar'
										},
										title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes option:selected").html()+'<br>',
											align: 'left',
											x: 110
										},
										subtitle: {
											text: '<strong>Diagrama de Pilas Barras</strong><br>Citas Vendidas por Fuente de Financimiento'
										},
										xAxis: {
											categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
										},
										yAxis: {
											min: 0,
											title: {
												text: 'Num. Citas'
											},
											stackLabels: {
												enabled: true,
												style: {
													fontWeight: 'bold',
													color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
												}
											}
										},
										legend: {
											align: 'right',
											x: -30,
											verticalAlign: 'top',
											y: 25,
											floating: true,
											backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
											borderColor: '#CCC',
											borderWidth: 1,
											shadow: false
										},
										tooltip: {
											headerFormat: '<b>{point.y}</b><br/>',
											pointFormat: '{series.name}: {point.x}<br/>Total: {point.stackTotal}'
										},
										credits: {
										enabled: false
										},
										plotOptions: {
											series: {
												stacking: 'normal',
												dataLabels: {
													enabled: true,
													color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
												}
											}
										},
										series: objeto

							});
						}
					});

					
					
				}



					
				function Highchart_Lineas_por_Mes()
				{
					var IdDepartamento=document.getElementById('IdDepartamento').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad').value;
					var Anio=document.getElementById('Anio').value;
					var Mes=document.getElementById('Mes').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Mes',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({

						
										chart: {
										type: 'line'
										},
										title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes option:selected").html()+'<br>',
											align: 'left',
											x: 110
										},
										subtitle: {
											text: '<strong>Diagrama de Lineas</strong><br>Citas Vendidas por Fuente de Financimiento'
										},
										xAxis: {
											categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
										},
										yAxis: {
											title: {
												text: 'Num. Citas'
											}
										},
										legend: {
											align: 'right',
											x: -30,
											verticalAlign: 'top',
											y: 25,
											floating: true,
											backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
											borderColor: '#CCC',
											borderWidth: 1,
											shadow: false
										},
										credits: {
										enabled: false
										},
										plotOptions: {
											line: {
												dataLabels: {
													enabled: true
												},
												enableMouseTracking: false
											}
										},
										
										series: objeto

							});
						}
					});

					
					
				}
				

						
				function Highchart_Barras_por_Mes()
				{
					var IdDepartamento=document.getElementById('IdDepartamento').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad').value;
					var Anio=document.getElementById('Anio').value;
					var Mes=document.getElementById('Mes').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Mes',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({
									chart: {
										type: 'bar'
									},
									title: {
											text: '<strong>Departamento </strong>: '+$("#IdDepartamento option:selected").html()+
														  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad option:selected").html()+
														  '<br><strong>Año  </strong>: '+$("#Anio option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes option:selected").html()+'<br>',
											align: 'left',
											x: 110
									},
									subtitle: {
											text: '<strong>Diagrama de Barras</strong><br>Citas Vendidas por Fuente de Financimiento'
									},
									xAxis: {
										categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
										title: {
											text: null
										}
									},
									yAxis: {
										min: 0,
										title: {
											text: 'Num. Citas',
											align: 'high'
										},
										labels: {
											overflow: 'justify'
										}
									},
									plotOptions: {
										bar: {
											dataLabels: {
												enabled: true
											}
										}
									},
									legend: {
										align: 'right',
										x: -30,
										verticalAlign: 'top',
										y: 25,
										floating: true,
										backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
										borderColor: '#CCC',
										borderWidth: 1,
										shadow: false
									},
									credits: {
										enabled: false
									},
									series: objeto
									
							});

						}
					});
					
				}
			
			
				function Highchart_Columnas_por_Mes()
				{
					var IdDepartamento=document.getElementById('IdDepartamento').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad').value;
					var Anio=document.getElementById('Anio').value;
					var Mes=document.getElementById('Mes').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Mes',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{	
						objeto=jQuery.parseJSON(res);
						$('#container').highcharts({			
						
						 chart: {
								type: 'column'
							},
							title: {
								text: '<strong>Departamento </strong>: '+$("#IdDepartamento option:selected").html()+
											  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad option:selected").html()+
											  '<br><strong>Año  </strong>: '+$("#Anio option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes option:selected").html()+'<br>',
								align: 'left',
								x: 110
							},
							subtitle: {
								text: '<strong>Diagrama de Columnas</strong><br> Citas Vendidas por Fuente de Financimiento'
							},
							xAxis: {
								categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
								crosshair: true
							},
							yAxis: {
								min: 0,
								title: {
									text: 'Num. Citas'
								}
							},
							
							legend: {
								align: 'right',
								x: -30,
								verticalAlign: 'top',
								y: 25,
								floating: true,
								backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
								borderColor: '#CCC',
								borderWidth: 1,
								shadow: false
							},
							tooltip: {
								headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
									'<td style="padding:0"><b>{point.y:.1f} Citas</b></td></tr>',
								footerFormat: '</table>',
								shared: true,
								useHTML: true
							},
							credits: {
								enabled: false
							},
							plotOptions: {
								column: {
									pointPadding: 0.2,
									borderWidth: 0,
									dataLabels: {
									enabled: true
									}
								}
							},
							series: objeto

						});

						}
					});
					
				}
				
				

				
				
				function Highchart_Spline_por_Mes()
				{

					var IdDepartamento=document.getElementById('IdDepartamento').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad').value;
					var Anio=document.getElementById('Anio').value;
					var Mes=document.getElementById('Mes').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Highchart_Mes',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(res)
						{
						objeto=jQuery.parseJSON(res);	
						$('#container').highcharts({
							    chart: {
										type: 'areaspline'
									},
									title: {
										text: '<strong>Departamento </strong>: '+$("#IdDepartamento option:selected").html()+
											  '<br><strong>Especialidad </strong>: '+$("#IdEspecialidad option:selected").html()+
											  '<br><strong>Año  </strong>: '+$("#Anio option:selected").html()+'   <strong> /   Mes  </strong>: '+$("#Mes option:selected").html()+'<br>',
										align: 'left',
										x: 110
									},
									subtitle: {
									text: '<strong>Diagrama de Area Curva</strong><br>Citas Vendidas por Fuente de Financimiento'
									},

									xAxis: {
										categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'
										],
										plotBands: [{ // visualize the weekend
											from: 4.5,
											to: 6.5,
											color: 'rgba(68, 170, 213, .2)'
										}]
									},
									yAxis: {
										title: {
											text: 'Num. Citas'
										}
									},
									tooltip: {
										shared: true,
										valueSuffix: ' Citas'
									},
									credits: {
										enabled: false
									},
									legend: {
										align: 'right',
										x: -30,
										verticalAlign: 'top',
										y: 25,
										floating: true,
										backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
										borderColor: '#CCC',
										borderWidth: 1,
										shadow: false
									},
									plotOptions: {
										areaspline: {
											fillOpacity: 0.5,
											dataLabels: {
											enabled: true
											}
										}
									},
									series: objeto	
									
						});
						}
					});
					
				}
				
				
				
				function Exportar_Excel()
				{
					var IdDepartamento=document.getElementById('IdDepartamento').value;
					var IdEspecialidad=document.getElementById('IdEspecialidad').value;
					var Anio=document.getElementById('Anio').value;
					var Mes=document.getElementById('Mes').value;
					$.ajax({
						url: '../../MVC_Controlador/Reportes/ReportesC.php?acc=Exportar_CitasVendidasxFuenteFinaciamiento',
						type: 'POST',
						dataType: 'json',
						data: {
							IdDepartamento:IdDepartamento,
							IdEspecialidad:IdEspecialidad,
							Anio:Anio,
							Mes:Mes
						},

						success:function(impresion)
						{
						 window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
						 e.preventDefault();
						}
					});

				}
				
				function Exportar_Excel_Grilla_Especialidades()
				{
					var htmltable= document.getElementById('Contenido_Excel');
					var html = htmltable.outerHTML;
					window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
				}				
		});
		

		
		
		function Resetear_Valores(){
		window.location.href='../../MVC_Controlador/Sistema/SistemaC.php?acc=Estadistica001';
		}
		
		/******** Fin Reportes Rango de Citas  *************/
		
        </script>

    </head>
	
    <body>
	
		<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
		<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
			<div class="easyui-layout" style="width:100%;height:800px;">

				<div data-options="region:'center',title:'Estadisticas',iconCls:'icon-ok'"  style="width:80%;">
						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto; padding-top:80px"></div>
						<div  id="Contenido_Excel">
						<div style="font-weight:bold;padding:6px;font-size:11px;margin-bottom:10px;margin-top:5px" id="Contenido_Cabecera">
						</div>
						<table id="Grilla_Especialidades"></table>
						</div>
				</div>
				
				
				<div data-options="region:'west',split:true" title="Opciones" style="width:20%;">
				<div class="easyui-tabs">
					<div title="Citas Vendidas" style="padding:10px">
							<p style="padding:10px"><strong>Reporte de Citas Vendidas</strong></p>
							<hr>
							<br>
							<span style="font-weight:bold; color:#414141">Departamento :</span>
							<br>
							<br>
															<select  name="IdDepartamento" style="width:300px" id="IdDepartamento" >
																		<option value="">Todos</option> <!--Cirugía 4to. A-->
																		 <?php 
																			$resultados=ListarDepartamentosHospitalM();								 
																			if($resultados!=NULL){
																			for ($i=0; $i < count($resultados); $i++) {	
																		 ?>
																		 <option value="<?php echo $resultados[$i]["IdDepartamento"] ?>" <?php if($_REQUEST["IdDepartamento"]==$resultados[$i]["IdDepartamento"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
																		 <?php 
																			}}
																		 ?>                   
															</select>
							<br>
							<br>
							<span style="font-weight:bold; color:#414141">Especialidades :</span>												
							<br>
							<br>
															<select  name="IdEspecialidad" style="width:300px" id="IdEspecialidad"  >
															<option value="">Todos</option>
															</select> 
							<br>
							<br>
							<span style="font-weight:bold; color:#414141">Año :</span>
							<br>
							<br>
															<select   name="Anio" style="width:300px" id="Anio"  >
																		<option value="">Todos</option>
																		 <?php 
																			$resultados=ListarAnioM();								 
																			if($resultados!=NULL){
																			for ($i=0; $i < count($resultados); $i++) {	
																		 ?>
																		 <option value="<?php echo $resultados[$i]["Anio"] ?>" <?php if($_REQUEST["Anio"]==$resultados[$i]["Anio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["Anio"]; ?></option>
																		 <?php 
																			}}
																		 ?>
															</select>
							<br>
							<br>
							<span style="font-weight:bold; color:#414141">Mes :</span>
							<br>
							<br>
															<select name="Mes"  style="width:300px" id="Mes"   >
															<option value="">Todos</option>
															<option value="1" >Enero</option>
															<option value="2"<?php if($_REQUEST["Mes"]=='2'){?> selected <?php } ?> >Febrero</option>
															<option value="3"<?php if($_REQUEST["Mes"]=='3'){?> selected <?php } ?> >Marzo</option>
															<option value="4"<?php if($_REQUEST["Mes"]=='4'){?> selected <?php } ?> >Abril</option>
															<option value="5"<?php if($_REQUEST["Mes"]=='5'){?> selected <?php } ?> >Mayo</option>
															<option value="6"<?php if($_REQUEST["Mes"]=='6'){?> selected <?php } ?> >Junio</option>
															<option value="7"<?php if($_REQUEST["Mes"]=='7'){?> selected <?php } ?> >Julio</option>
															<option value="8"<?php if($_REQUEST["Mes"]=='8'){?> selected <?php } ?> >Agosto</option>
															<option value="9"<?php if($_REQUEST["Mes"]=='9'){?> selected <?php } ?> >Septiembre</option>
															<option value="10"<?php if($_REQUEST["Mes"]=='10'){?> selected <?php } ?> >Octubre</option>
															<option value="11"<?php if($_REQUEST["Mes"]=='11'){?> selected <?php } ?> >Noviembre</option>
															<option value="12"<?php if($_REQUEST["Mes"]=='12'){?> selected <?php } ?> >Diciembre</option>
															</select>
							<br>
							<br>
							<br>
	
							<fieldset>
								<legend><strong style="font-weight:bold; color:#414141">Tipo de Gráfico:</strong></legend>
										<br>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Lineas_por_Mes">Linea</a>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:90px" id="Columnas_por_Mes">Columnas</a>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Barras_por_Mes">Barra</a>
										<br>
										<br>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Spline_por_Mes">Area<br>Curva</a>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:90px" id="Pilas_Columnas_por_Mes">Pila <br> Columna</a>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Pilas_Barras_por_Mes">Pila Barra</a>
							</fieldset>
							
							
							<br>
							<br>
							<br>
	
							<fieldset>
								<legend><strong style="font-weight:bold; color:#414141">Opciones:</strong></legend>
										<br>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" onclick="Resetear_Valores()">Resetear</a>
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" style="width:110px" id="exportar_excel_btn">Exportar</a>
							</fieldset>
						
		
				
					</div>


					<div title="Otros ">
					<div class="easyui-accordion">
					

					
					
					
						<div title="Por Rango de Fechas" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte de Citas Ofertadas vs Vendidas vs Atendidas</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;">
								</div>
								<div style="margin-bottom:20px">
									<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:90px" id="Generar_Reporte_por_Periodo">Generar Grafico de Columnas</a>
								</div>									
						</div>
						
					    <div title="Reporte Avanzado" data-options="iconCls:'icon-ok'" style="padding:10px;">
								<p style="padding:10px"><strong>Reporte de Citas Ofertadas vs Vendidas vs Atendidas</strong></p>
								<hr>
								<br>
								<span style="font-weight:bold; color:#414141">Departamento :</span>
								<br>
								<br>
								<select  name="IdDepartamento_2" style="width:300px" id="IdDepartamento_2" >
										<option value="">Todos</option> 
										<?php 
											$resultados=ListarDepartamentosHospitalM();								 
											if($resultados!=NULL){
											for ($i=0; $i < count($resultados); $i++) {	
											 ?>
											 <option value="<?php echo $resultados[$i]["IdDepartamento"] ?>" <?php if($_REQUEST["IdDepartamento"]==$resultados[$i]["IdDepartamento"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
											<?php 
											}}
										?>                   
								</select>
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Especialidades :</span>												
								<br>
								<br>
																<select  name="IdEspecialidad_2" style="width:300px" id="IdEspecialidad_2"  >
																<option value="">Todos</option>
																</select> 
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Año :</span>
								<br>
								<br>
																<select   name="Anio" style="width:300px" id="Anio_2">
																			<option value="">Todos</option>
																			 <?php 
																				$resultados=ListarAnioM();								 
																				if($resultados!=NULL){
																				for ($i=0; $i < count($resultados); $i++) {	
																			 ?>
																			 <option value="<?php echo $resultados[$i]["Anio"] ?>" <?php if($_REQUEST["Anio"]==$resultados[$i]["Anio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["Anio"]; ?></option>
																			 <?php 
																				}}
																			 ?>
																</select>
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Mes :</span>
								<br>
								<br>
																<select name="Mes"  style="width:300px" id="Mes_2">
																<option value="">Todos</option>
																<option value="1" >Enero</option>
																<option value="2"<?php if($_REQUEST["Mes"]=='2'){?> selected <?php } ?> >Febrero</option>
																<option value="3"<?php if($_REQUEST["Mes"]=='3'){?> selected <?php } ?> >Marzo</option>
																<option value="4"<?php if($_REQUEST["Mes"]=='4'){?> selected <?php } ?> >Abril</option>
																<option value="5"<?php if($_REQUEST["Mes"]=='5'){?> selected <?php } ?> >Mayo</option>
																<option value="6"<?php if($_REQUEST["Mes"]=='6'){?> selected <?php } ?> >Junio</option>
																<option value="7"<?php if($_REQUEST["Mes"]=='7'){?> selected <?php } ?> >Julio</option>
																<option value="8"<?php if($_REQUEST["Mes"]=='8'){?> selected <?php } ?> >Agosto</option>
																<option value="9"<?php if($_REQUEST["Mes"]=='9'){?> selected <?php } ?> >Septiembre</option>
																<option value="10"<?php if($_REQUEST["Mes"]=='10'){?> selected <?php } ?> >Octubre</option>
																<option value="11"<?php if($_REQUEST["Mes"]=='11'){?> selected <?php } ?> >Noviembre</option>
																<option value="12"<?php if($_REQUEST["Mes"]=='12'){?> selected <?php } ?> >Diciembre</option>
																</select>
								<br>
								<br>
								<br>
							
								<fieldset>
									<legend><strong style="font-weight:bold; color:#414141">Tipo de Gráfico:</strong></legend>
											<br>
											<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Lineas_Citas_Vs_Citas">Linea</a>
											<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:90px" id="Columnas_Citas_Vs_Citas">Columnas</a>
											<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Barras_por_Citas_Vs_Citas">Barra</a>
											<br>
											<br>
											<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Spline_Citas_Vs_Citas">Area<br>Curva</a>
											<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:90px" id="Pilas_Columnas_Citas_Vs_Citas">Pila <br> Columna</a>
											<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Pilas_Barras_Citas_Vs_Citas">Pila Barra</a>
								</fieldset>
						</div>
					     
						 
						<div title="Rendimiento Hora / Medico " data-options="iconCls:'icon-ok'" style="padding:10px;">
						   <p style="padding:10px"><strong>Rendimiento Hora / Medico</strong></p>
								<hr>
								<br>
								<span style="font-weight:bold; color:#414141">Departamento :</span>
								<br>
								<br>
								<select  name="IdDepartamento_3" style="width:300px" id="IdDepartamento_3" >
										<option value="">Todos</option> 
										<?php 
											$resultados=ListarDepartamentosHospitalM();								 
											if($resultados!=NULL){
											for ($i=0; $i < count($resultados); $i++) {	
											 ?>
											 <option value="<?php echo $resultados[$i]["IdDepartamento"] ?>" <?php if($_REQUEST["IdDepartamento"]==$resultados[$i]["IdDepartamento"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
											<?php 
											}}
										?>                   
								</select>
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Especialidades :</span>												
								<br>
								<br>
																<select  name="IdEspecialidad_3" style="width:300px" id="IdEspecialidad_3"  >
																<option value="">Todos</option>
																</select> 
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Año :</span>
								<br>
								<br>
																<select   name="Anio" style="width:300px" id="Anio_3">
																			<option value="">Todos</option>
																			 <?php 
																				$resultados=ListarAnioM();								 
																				if($resultados!=NULL){
																				for ($i=0; $i < count($resultados); $i++) {	
																			 ?>
																			 <option value="<?php echo $resultados[$i]["Anio"] ?>" <?php if($_REQUEST["Anio"]==$resultados[$i]["Anio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["Anio"]; ?></option>
																			 <?php 
																				}}
																			 ?>
																</select>
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Mes :</span>
								<br>
								<br>
																<select name="Mes"  style="width:300px" id="Mes_3">
																<option value="">Todos</option>
																<option value="1" >Enero</option>
																<option value="2"<?php if($_REQUEST["Mes"]=='2'){?> selected <?php } ?> >Febrero</option>
																<option value="3"<?php if($_REQUEST["Mes"]=='3'){?> selected <?php } ?> >Marzo</option>
																<option value="4"<?php if($_REQUEST["Mes"]=='4'){?> selected <?php } ?> >Abril</option>
																<option value="5"<?php if($_REQUEST["Mes"]=='5'){?> selected <?php } ?> >Mayo</option>
																<option value="6"<?php if($_REQUEST["Mes"]=='6'){?> selected <?php } ?> >Junio</option>
																<option value="7"<?php if($_REQUEST["Mes"]=='7'){?> selected <?php } ?> >Julio</option>
																<option value="8"<?php if($_REQUEST["Mes"]=='8'){?> selected <?php } ?> >Agosto</option>
																<option value="9"<?php if($_REQUEST["Mes"]=='9'){?> selected <?php } ?> >Septiembre</option>
																<option value="10"<?php if($_REQUEST["Mes"]=='10'){?> selected <?php } ?> >Octubre</option>
																<option value="11"<?php if($_REQUEST["Mes"]=='11'){?> selected <?php } ?> >Noviembre</option>
																<option value="12"<?php if($_REQUEST["Mes"]=='12'){?> selected <?php } ?> >Diciembre</option>
																</select>
								<br>
								<br>
								<br>
																<p><input type="checkbox" id="Ofertadas"><strong>¿Cargar Citas Vendidas?</strong></p>
								<br>
								<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Generar_Hora_Medico">Generar Grafico</a>
				
						</div>
						
						
							 
						<div title="Reporte por Especialidades " data-options="iconCls:'icon-ok'" style="padding:10px;">
						   <p style="padding:10px"><strong>Reporte por Especialidades</strong></p>
								<hr>
								<br>
								<span style="font-weight:bold; color:#414141">Departamento :</span>
								<br>
								<br>
								<select  name="IdDepartamento_4" style="width:300px" id="IdDepartamento_4" >
										<option value="">Todos</option> 
										<?php 
											$resultados=ListarDepartamentosHospitalM();								 
											if($resultados!=NULL){
											for ($i=0; $i < count($resultados); $i++) {	
											 ?>
											 <option value="<?php echo $resultados[$i]["IdDepartamento"] ?>" <?php if($_REQUEST["IdDepartamento"]==$resultados[$i]["IdDepartamento"]){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
											<?php 
											}}
										?>                   
								</select>
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Especialidades :</span>												
								<br>
								<br>
																<select  name="IdEspecialidad_4" style="width:300px" id="IdEspecialidad_4"  >
																<option value="">Todos</option>
																</select> 
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Año :</span>
								<br>
								<br>
																<select   name="Anio_4" style="width:300px" id="Anio_4">
																			<option value="">Todos</option>
																			 <?php 
																				$resultados=ListarAnioM();								 
																				if($resultados!=NULL){
																				for ($i=0; $i < count($resultados); $i++) {	
																			 ?>
																			 <option value="<?php echo $resultados[$i]["Anio"] ?>" <?php if($_REQUEST["Anio"]==$resultados[$i]["Anio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["Anio"]; ?></option>
																			 <?php 
																				}}
																			 ?>
																</select>
								<br>
								<br>
								<span style="font-weight:bold; color:#414141">Mes :</span>
								<br>
								<br>
																<select name="Mes_4"  style="width:300px" id="Mes_4">
																<option value="">Todos</option>
																<option value="1" >Enero</option>
																<option value="2"<?php if($_REQUEST["Mes"]=='2'){?> selected <?php } ?> >Febrero</option>
																<option value="3"<?php if($_REQUEST["Mes"]=='3'){?> selected <?php } ?> >Marzo</option>
																<option value="4"<?php if($_REQUEST["Mes"]=='4'){?> selected <?php } ?> >Abril</option>
																<option value="5"<?php if($_REQUEST["Mes"]=='5'){?> selected <?php } ?> >Mayo</option>
																<option value="6"<?php if($_REQUEST["Mes"]=='6'){?> selected <?php } ?> >Junio</option>
																<option value="7"<?php if($_REQUEST["Mes"]=='7'){?> selected <?php } ?> >Julio</option>
																<option value="8"<?php if($_REQUEST["Mes"]=='8'){?> selected <?php } ?> >Agosto</option>
																<option value="9"<?php if($_REQUEST["Mes"]=='9'){?> selected <?php } ?> >Septiembre</option>
																<option value="10"<?php if($_REQUEST["Mes"]=='10'){?> selected <?php } ?> >Octubre</option>
																<option value="11"<?php if($_REQUEST["Mes"]=='11'){?> selected <?php } ?> >Noviembre</option>
																<option value="12"<?php if($_REQUEST["Mes"]=='12'){?> selected <?php } ?> >Diciembre</option>
																</select>
								<br>
								<br>
								<br>
								<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Generar_Grilla_Especialidades">Generar Vista</a>
								<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px" id="Generar_Excel_Especialidades">Generar Excel</a>
						</div>
						
						
						
					</div>	
					</div>

					
					
					
					

					
					
				</div>
			</div>

    </body>
    </html>



