<head>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/demo.css">
 
 
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.autocomplete.css" />
    
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/funciones2.js"></script>
 
 
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery_enter.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src="../../MVC_Complemento/js/jquery.autocomplete.js"></script> 

<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">

<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>


 <script type="text/javascript" src="../../MVC_Complemento/js/jsalert/jquery.alerts.js"></script>
 <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/js/jsalert/jquery.alerts.css"/>
 
<style type="text/css">
<!--
#tblSample td, th { padding: 0.2em; }
.classy0 { background-color: #234567; color: #89abcd; }
.classy1 { background-color: #89abcd; color: #234567; }

#ocultarTexto {
  display: none;
}




 dt,  table, tbody, tfoot, thead, tr, th, td {
	margin:0;
	padding:0;
	border:0;
	font-weight:inherit;
	font-style:inherit;
	 
	font-family:inherit;
	vertical-align:baseline;
}


 
.cuerpo1 {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
color: #000;
 
}

form
{width: auto; background-color: #ffffff; }

textarea.Formulario {
padding: 5px;
border: 1px solid #D4D4D4;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
width:100%;
}

input.Formulario {
border: 1px solid #D4D4D4;
padding: 5px;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
}

.Combos {
     font: small-caption cursive ; 

     background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    width: 150px;
    float:left;
	
text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;

}

 
.texto {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;
  
    /*float:left;*/

}
.textodesable {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#ECECEC;
  
    /*float:left;*/

}


.buttons a, .buttons button{
    
    display:block;
    float:left;
    margin:0 17px 0 0;
    background-color:#f5f5f5;
    border:1px solid #dedede;
    border-top:1px solid #eee;
    border-left:1px solid #eee;

    font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
    font-size: 15px;
    line-height:130%;
    text-decoration:none;
    font-weight:bold;
    color:#565656;
    cursor:pointer;
    padding:5px 10px 6px 7px; /* Links */
}
.buttons button{
    width:auto;
    overflow:visible;
    padding:4px 10px 3px 7px; /* IE6 */
}
.buttons button[type]{
    padding:5px 10px 5px 7px; /* Firefox */
    line-height:17px; /* Safari */
}
*:first-child+html button[type]{
    padding:4px 10px 3px 7px; /* IE7 */
}
.buttons button img, .buttons a img{
    margin:0 3px -3px 0 !important;
    padding:0;
    border:none;
    width:16px;
    height:16px;
}

/* STANDARD */

button:hover, .buttons a:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

/* POSITIVE */

button.positive, .buttons a.positive{
    color:#529214;
}
.buttons a.positive:hover, button.positive:hover{
    background-color:#E6EFC2;
    border:1px solid #C6D880;
    color:#529214;
}
.buttons a.positive:active{
    background-color:#529214;
    border:1px solid #529214;
    color:#fff;
}

/* NEGATIVE */

.buttons a.negative, button.negative{
    color:#d12f19;
}
.buttons a.negative:hover, button.negative:hover{
    background:#fbe3e4;
    border:1px solid #fbc2c4;
    color:#d12f19;
}
.buttons a.negative:active{
    background-color:#d12f19;
    border:1px solid #d12f19;
    color:#fff;
}

/* REGULAR */

button.regular, .buttons a.regular{
    color:#336699;
}
.buttons a.regular:hover, button.regular:hover{
    background-color:#dff4ff;
    border:1px solid #c2e1ef;
    color:#336699;
}
.buttons a.regular:active{
    background-color:#6299c5;
    border:1px solid #6299c5;
    color:#fff;
}

 /*-----*/
 fieldset { border:1px solid green }

.legend {
  border: 1px solid #BDD7FF;
width: 95%;
background: #F7F7F7;
padding: 3px;

/*  text-align:right;*/
  }
.texto1 {	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;
}
  
  
   
-->
</style>  

<script   language="javascript">
 	 
 function Exportar(Exportar,IdReporte){
		//alert("Hola");
	 
	// var IdServicio=document.getElementById("IdServicio").value;
	 var FechaInicio=document.getElementById("FechaInicio").value;
	 var FechaFinal=document.getElementById("FechaFinal").value;
	 
	 var idUsuariocajero=document.getElementById("idUsuariocajero").value;
	
	 if (document.formElem.FechaInicio.value.length==0&&document.formElem.FechaFinal.value.length==0){
		alert("Ingresar Fechas");
		document.formElem.FechaInicio.focus();
		return 0;
	 }else{
	 
		 if(Exportar=="pdf"){ 
			location="../../MVC_Vista/Reportes/ECONOMIA/<?php echo $IdReporte;?>_pdf.php?FechaInicio="+FechaInicio+"&FechaFinal="+FechaFinal+"&Usuario=<?php echo $Usuario;?>&idUsuariocajero="+idUsuariocajero;
			return 0;
			 }
		 if(Exportar=="excel"){
			location="../../MVC_Vista/Emergencia/Reporte_IngresoxEmergencia.php?FechaInicio="+FechaInicio+"&FechaFinal="+FechaFinal+"&Usuario=<?php echo $Usuario;?>";
			return 0;
			 }
		 if(Exportar=="word"){
			location="../../MVC_Vista/Emergencia/Reporte_IngresoxEmergencia.php?FechaInicio="+FechaInicio+"&FechaFinal="+FechaFinal+"&Usuario=<?php echo $Usuario;?>";
			return 0;
		  }
		
	 
	}	
	 
	}	
		
</script>

 <script language="javascript" type="text/javascript">  
 
  
   
   
function Guardar(){
	
	  /*
	
     	if (document.getElementById('NumOrdden').value.length==0){
			         alert("Ingrese N° Orden ");
		             document.getElementById('IdOrden').focus();
		             return 0;
				} 
		if (document.getElementById('OrdenaPrueba').value.length==0){
			         alert("Ingrese Nombre del Medico ");
		             document.getElementById('OrdenaPrueba').focus();
		             return 0;
				} 
      
	   if (document.getElementById('idempleadoregistro').value.selectedIndex==0){
			         alert("Ingrese Nombre del Medico ");
		             document.getElementById('idempleadoregistro').focus();
		             return 0;
				} 
		*/
		
		if (document.getElementById('IdTipoGravedad').selectedIndex==1){
		       alert("Seleccionar Prioridad.");
    		   document.getElementById('IdTipoGravedad').focus();
				return 0;
	    }
				if (document.getElementById('IdCausaExternaMorbilidad').selectedIndex==0){
		       alert("Seleccionar Causa Morvilidad.");
    		   document.getElementById('IdCausaExternaMorbilidad').focus();
				return 0;
	    }				
			
			
						
				
	    document.formElem.submit();
		parent.BuscarPacientesEmergencia();
		 parent.googlebox.hide();
 	}	


function Cerrar(){
	
	 parent.googlebox.hide();
	}
    </script> 
    <script language="javascript"> 
  function NumCheck(e, field) {
    key = e.keyCode ? e.keyCode : e.which
    if (key == 8) return true
    if (key > 47 && key < 58) {
      if (field.value == "") return true
      regexp = /.[0-9]{5}$/
      return !(regexp.test(field.value))
    }
    if (key == 46) {
      if (field.value == "") return false
      regexp = /^[0-9]+$/
      return regexp.test(field.value)
    }
    return false
  }
</script> 
  </head>

 
 
 <body   >
  
		    

 <form id="formElem" name="formElem"  method="post" action="../../MVC_Controlador/Reportes/ReportesC.php?acc=<?php echo $IdReporte.'Exporta';?>">
 
    <?php //echo $IdReporte;?>
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong><?php echo  $NombreReporte;  ?></strong></legend>  
     <?php 
//  echo $IdReporte;
   if($IdReporte=="ID_ParteDiario"){?> 
    <table width="600" border="0" align="center">
      <tr>
        <td width="562" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Fecha Inicio</strong></td>
            <td width="260"><input name="FechaInicio" type="text" class="texto" id="FechaInicio" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 00:00'?>"   size="20"  autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  /></td>
            </tr>
          <tr>
            <td height="16" ><strong>Fecha Final</strong></td>
            <td><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
              </script>
              <input name="FechaFinal" type="text" class="texto1" id="FechaFinal" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 23:59'?>"  size="20"   autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Final"  />
              <script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
              </script></td>
            </tr>
          <tr>
            <td height="16" ><strong>Formato</strong></td>
            <td><a  href="#" onClick="Exportar('pdf')" > <img src="../../MVC_Complemento/img/pdf.png"  /></a>   <a href="#" onClick="Exportar('excel')" ><?php /*?><img src="../../MVC_Complemento/img/Excel.png" alt="" /> <?php */?>
              <?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?>
            </a>   <input type="hidden" name="idUsuariocajero" id="idUsuariocajero"></td>
          </tr>
          
        </table></td>
      </tr>
    </table>
  <?php }?> 
  
  <?php 
//  echo $IdReporte;
   if($IdReporte=="Id_DetallePartida"){?> 
    <table width="600" border="0" align="center">
      <tr>
        <td width="562" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Fecha Inicio</strong></td>
            <td width="260"><input name="FechaInicio" type="text" class="texto" id="FechaInicio" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 00:00'?>"   size="20"  autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  /></td>
            </tr>
          <tr>
            <td height="16" ><strong>Fecha Final</strong></td>
            <td><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
              </script>
              <input name="FechaFinal" type="text" class="texto1" id="FechaFinal" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 23:59'?>"  size="20"   autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Final"  />
              <script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
              </script></td>
            </tr>
          <tr>
            <td height="16" ><strong>Cajero</strong></td>
            <td><select name="idUsuariocajero" id="idUsuariocajero" class="combo" >
            <option value="%" >[.:Todo:.]</option>
            <?php 
            if($ListarCajeros != NULL)  { 
            foreach($ListarCajeros as $item2){?>
            <option value="<?php echo $item2["IdEmpleado"]?>"><?php echo $item2["ApellidoPaterno"].' '.$item2["ApellidoMaterno"].' '.$item2["Nombres"]?></option>
            <?php }}?>
        </select></td>
        
        			

          </tr>
          <tr>
            <td height="16" ><strong>Formato</strong></td>
            <td><a  href="#" onClick="Exportar('pdf','<?php echo $IdReporte;?>')" > <img src="../../MVC_Complemento/img/pdf.png"  /></a>   <a href="#" onClick="Exportar('excel')" ><?php /*?><img src="../../MVC_Complemento/img/Excel.png" alt="" /> <?php */?>
              <?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?>
            </a>   </td>
          </tr>
          
        </table></td>
      </tr>
    </table>
  <?php }?> 
  
  
  <?php 
   
   if($IdReporte=="Id_ResumenPartida"){?> 
    <table width="600" border="0" align="center">
      <tr>
        <td width="562" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Fecha Inicio</strong></td>
            <td width="260"><input name="FechaInicio" type="text" class="texto" id="FechaInicio" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 00:00'?>"   size="20"  autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  /></td>
            </tr>
          <tr>
            <td height="16" ><strong>Fecha Final</strong></td>
            <td><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
              </script>
              <input name="FechaFinal" type="text" class="texto1" id="FechaFinal" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 23:59'?>"  size="20"   autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Final"  />
              <script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
              </script></td>
            </tr>
          <tr>
            <td height="16" ><strong>Cajero</strong></td>
            <td><select name="idUsuariocajero" id="idUsuariocajero" class="combo" >
            <option value="%" >[.:Todo:.]</option>
            <?php 
            if($ListarCajeros != NULL)  { 
            foreach($ListarCajeros as $item2){?>
            <option value="<?php echo $item2["IdEmpleado"]?>"><?php echo $item2["ApellidoPaterno"].' '.$item2["ApellidoMaterno"].' '.$item2["Nombres"]?></option>
            <?php }}?>
        </select></td>
        
        			

          </tr>
          <tr>
            <td height="16" ><strong>Formato</strong></td>
            <td><a  href="#" onClick="Exportar('pdf','<?php echo $IdReporte;?>')" > <img src="../../MVC_Complemento/img/pdf.png"  /></a>   <a href="#" onClick="Exportar('excel')" ><?php /*?><img src="../../MVC_Complemento/img/Excel.png" alt="" /> <?php */?>
              <?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?>
            </a>   </td>
          </tr>
          
        </table></td>
      </tr>
    </table>
  <?php }?> 
  
  
  

  <?php 
   
   if($IdReporte=="ID_ConsolidadoVentas"){?> 
    <table width="600" border="0" align="center">
      <tr>
        <td width="562" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Fecha Inicio</strong></td>
            <td width="260"><input name="FechaInicio" type="text" class="texto" id="FechaInicio" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 00:00'?>"   size="20"  autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  /></td>
            </tr>
          <tr>
            <td height="16" ><strong>Fecha Final</strong></td>
            <td><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
              </script>
              <input name="FechaFinal" type="text" class="texto1" id="FechaFinal" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 23:59'?>"  size="20"   autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Final"  />
              <script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
              </script></td>
            </tr>
          <tr>
            <td height="16" ><strong>Formato</strong></td>
            <td><a  href="#" onClick="Exportar('pdf','<?php echo $IdReporte;?>')" > <img src="../../MVC_Complemento/img/pdf.png"  /></a>   <a href="#" onClick="Exportar('excel')" ><?php /*?><img src="../../MVC_Complemento/img/Excel.png" alt="" /> <?php */?>
              <?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?>
              </a>   <input type="hidden" name="idUsuariocajero" id="idUsuariocajero"></td>
          </tr>
          
        </table></td>
      </tr>
    </table>
  <?php }?> 



  <?php 
   
   if($IdReporte=="ID_CajaDevoluciones"){?> 
    <table width="600" border="0" align="center">
      <tr>
        <td width="562" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Fecha Inicio</strong></td>
            <td width="260"><input name="FechaInicio" type="text" class="texto" id="FechaInicio" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 00:00'?>"   size="20"  autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  /></td>
            </tr>
          <tr>
            <td height="16" ><strong>Fecha Final</strong></td>
            <td><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
              </script>
              <input name="FechaFinal" type="text" class="texto1" id="FechaFinal" value="<?php echo vfecha(substr($FechaServidor,0,10)).' 23:59'?>"  size="20"   autocomplete=OFF  />
              <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Final"  />
              <script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
              </script></td>
            </tr>
          <tr>
            <td height="16" ><strong>Cajero</strong></td>
            <td><select name="idUsuariocajero" id="idUsuariocajero" class="combo" >
            <option value="%" >[.:Todo:.]</option>
            <?php 
            if($ListarCajeros != NULL)  { 
            foreach($ListarCajeros as $item2){?>
            <option value="<?php echo $item2["IdEmpleado"]?>"><?php echo $item2["ApellidoPaterno"].' '.$item2["ApellidoMaterno"].' '.$item2["Nombres"]?></option>
            <?php }}?>
        </select></td>
        
        			

          </tr>
          <tr>
            <td height="16" ><strong>Formato</strong></td>
            <td><a  href="#" onClick="Exportar('pdf','<?php echo $IdReporte;?>')" > <img src="../../MVC_Complemento/img/pdf.png"  /></a>   <a href="#" onClick="Exportar('excel')" ><?php /*?><img src="../../MVC_Complemento/img/Excel.png" alt="" /> <?php */?>
              <?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?>
            </a>   </td>
          </tr>
          
        </table></td>
      </tr>
    </table>
  <?php }?> 
  
  </fieldset>
         
  
  

  
  
  <BR>

<fieldset class="fieldset legend">
    <legend style="color:#03C"></legend>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
    <th scope="row"><span style="text-align: center"> </span> <span style="text-align: center"><span style="color:#03C"> </span><img src="../../MVC_Complemento/img/botoncancelar.jpg" width="85" height="22" onClick="Cerrar();"/><?php /*?><img src="../../MVC_Complemento/img/botonaceptar.jpg" width="85" height="22" onClick="Guardar();" /><?php */?></span></th>
  </tr>
</table>
</fieldset>

 </form>
 </body>         