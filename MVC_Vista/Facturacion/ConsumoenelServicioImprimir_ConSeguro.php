<?php
require_once("../../MVC_Complemento/fpdf/Imprimir_Directo.php");
$holaMunis='SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS';
class PDF extends FPDF
{			
	function Header()
	{
		$this->SetXY(0,1);
		$this->Cell(13,15,' ',1,0,'C');
		$this->SetFont('Courier','',10);
		$this->SetXY(0,16);
		$this->Cell(15,5,'Historia Clinica',0,0,'L');	
		$this->SetXY(0,21);
		$this->Cell(40,5,'Paciente',0,0,'L');	
		$this->SetXY(0,26);
		$this->Cell(20,5,'# Cuenta',0,0,'L');
		$this->SetXY(0,31);
		$this->Cell(20,5,'Fecha y Hora',0,0,'L');	
		$this->SetXY(0,36);
		$this->Cell(35,5,'Servicio',0,0,'L');
		$this->Cell(20,5,'Especialidad',0,0,'L');
		$this->ln(7);
		$this->SetFont('Courier','',9);
		$this->Cell(4,4,'Codigo',0,0,'R');
		$this->Cell(45,4,' Descripcion',0,0,'R');
		$this->Cell(43,4,'Cant.',0,0,'R');
		$this->Cell(23,4,'Importe',0,0,'R');	
	}
	function Footer()
	{
		global $holaMunis;
		
		$this->SetXY(18,-6);	
		$this->SetFont('Courier','',12);
		$this->Cell(20,5,'NroHistoriaClinica'.$holaMunis,0,0,'L');
		$this->Cell(50,5,utf8_decode('Paciente'),0,0,'R');
		$this->SetXY(10,90);
		$this->SetFont('Courier','',9);
		$this->Cell(30,4,'TERMINAL: SIGEMINSA',0,0,'R');
		$this->Cell(44,4,'TOTAL S/. ' ,0,0,'R');
		$this->Cell(42,4,'1000.00',0,0,'R');	
	}
}
$pdf = new PDF('L','mm',array(132,101));
$pdf->AddPage();
$pdf->SetXY(10,49);
$y = $pdf->GetY();
	for($i=0;$i<9;$i++)
		{
			if($i%6==0&&$i!=0)
			{
				$pdf->AddPage();
				$pdf->SetXY(10,49);
			}
		$pdf->Cell(1,5.5,'96106',0,0,'R');
		//$pdf->Cell(45,5.5,'HEMOGRAMA COMPLETO (1521) UREA ',0,0,'L');
		 
		//$pdf->MultiCell(45,2,utf8_decode('es estudiante regular de esta institución en la especialidad de: PNF EN INFORMATICA, cursando actualmente el '),1,1,'R',1);
		$pdf->MultiCell(90,4,utf8_decode('es estudiante regular de esta institución en la especialidad de  '),0,'L'); 
		
		$pdf->Cell(30,5.5,'1',0,0,'R');
		$pdf->Cell(30,5.5,'10.00',0,0,'R');
		//$pdf->Cell(25,5.5,'10.00',0,0,'R');
		$pdf->ln(5);
				
		}$pdf->SetXY(70,$y);
				
$pdf->Output();
?>