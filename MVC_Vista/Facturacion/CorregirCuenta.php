﻿<!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<script>					
				$(document).ready(function(){	
					$("#Guardar").click(function(){
					var NroHistoriaClinica = $("#NroHistoriaClinica").val();
					Mostrar_Datos_por_Paciente_NroHistoria_Clinica(NroHistoriaClinica);
					cargar_atenciones_paciente(NroHistoriaClinica);
					});
				});				
				//Funcion que Carga la Grilla de Servicios Ambulatorios
				//function cargar_atenciones_paciente(IdPaciente,Descuento)
				function cargar_atenciones_paciente(NroHistoriaClinica)
				{	
				//Al llamar la Funcion de Cargar Cuentas se Habilitara el Selected 
				$( "#lista_atenciones_paciente" ).prop( "disabled", false );	
				var dg =$('#lista_atenciones_paciente').datagrid({
				title:'Lista de Cuentas',
				iconCls:'icon-edit',
				singleSelect:true,
				rownumbers:true,
				remoteSort:false,
				multiSort:true,
				collapsible:true,
				url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=MostrarLista_Atenciones_de_Pacientes&NroHistoriaClinica='+NroHistoriaClinica,
					columns:[[	
					{field:'IDCUENTAATENCION',title:'Nº de Cuenta',width:100,sortable:true,formatter:formatPrice},
					{field:'NOMBRE',title:'Servicio de Atencion',width:200},
					{field:'FECHAINGRESO',title:'Fecha de Ingreso',width:200},
					{field:'DESCRIPCION',title:'Tipo de Servicio',width:190,sortable:true}
					
				]],
				onSelect:function(index,row){
				var IdAtencion=row.IDATENCION;
				var Nombre=row.NOMBRE;
				var FechaIngreso=row.FECHAINGRESO;
				var IdCuentaAtencion=row.IDCUENTAATENCION;
				var NroDocumento=row.NRODOCUMENTO;
				Actualizar_Datos_Adicionales_Atencion(IdAtencion,NroDocumento);
				}		
				});			
				}

				function formatPrice(val,row)
				{
			            return '<span style="color:red;font-weight:bold">'+val+'</span>';
			    }
				function Actualizar_Datos_Adicionales_Atencion(IdAtencion,NroDocumento){
            	$.messager.confirm('SIGES', 'Estas Seguro de Realizar la Operacion?', function(r){
                	if (r){
                   		 Mostrar_Datos_SisFiliaciones_por_Paciente(NroDocumento,IdAtencion);
						 carga_progresiva();
                		}
            		});
        		}	

		        function carga_progresiva(){
		            $.messager.alert('SIGES','Modificacion Realizada Exitosamente');
		        }	
		</script>
		<script type="text/javascript">			
			function Modificar_Datos_Adicionales_Atenciones(IdSiaSis,SisCodigo,IdEstablecimientoOrigen,IdAtencion)							
			{	
				$.ajax({
				url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Modificar_Datos_Adicionales_Atenciones',
				type: 'POST',
				dataType: 'json',
				data: 
				{
				IdSiaSis: IdSiaSis,
				SisCodigo: SisCodigo,
				IdEstablecimientoOrigen:IdEstablecimientoOrigen,
				IdAtencion:IdAtencion
				}
				});											
			}
			function Mostrar_Datos_SisFiliaciones_por_Paciente(NroDocumento,IdAtencion)							
			{
			    $.post("../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Mostrar_Datos_SisFiliaciones_por_Paciente&NroDocumento="+NroDocumento, function(data, status){
			    var myArray=JSON.parse(data);
			    var IdSiaSis=myArray.IDSIASIS;
			    var SisCodigo=myArray.CODIGO;
			    var IdEstablecimientoOrigen=myArray.CODIGOESTABLASCRIPCION;
				Modificar_Datos_Adicionales_Atenciones(IdSiaSis,SisCodigo,IdEstablecimientoOrigen,IdAtencion);
    			});		
    		}

    		function Mostrar_Datos_por_Paciente_NroHistoria_Clinica(NroHistoriaClinica)							
			{

			    $.post("../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Mostrar_Datos_de_Paciente&NroHistoriaClinica="+NroHistoriaClinica, function(data, status){
			    var myArray=JSON.parse(data);
			    var ApellidoPaterno=myArray.APELLIDOPATERNO;
			    var ApellidoMaterno=myArray.APELLIDOMATERNO;
			    var PrimerNombre=myArray.PRIMERNOMBRE;
			    var SegundoNombre=myArray.SEGUNDONOMBRE;
			    var NroDocumento=myArray.NRODOCUMENTO;
			    var NroHistoriaClinica=myArray.NROHISTORIACLINICA;
			    $("#ApellidoPaterno").val(ApellidoPaterno);
			    $("#ApellidoMaterno").val(ApellidoMaterno);
			    $("#PrimerNombre").val(PrimerNombre);
			    $("#SegundoNombre").val(SegundoNombre);
			    $("#NroDocumento").val(NroDocumento);
    			});		
    		}							
		</script>


		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
		}
		.mayus>input{
		text-transform: capitalize;
		}		
		</style>
		</head>
<body>
<div class="easyui-layout" style="width:100%;height:100%;">
        <div data-options="region:'north'" title="Regenerar Hoja FUA" style="height:100px;padding:20px">
        	<table style="border:0">
        		<tr>
        			<td><span style="font-weight: bold">Historia Clinica:</span>
        			</td>
        			<td><input text=""  id="NroHistoriaClinica" size="20">
        			</td>
        			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:120px;height: 30px;"  onclick="Generar()" id="Guardar">BUSCAR</a></td>
        		</tr>
        	</table>
        </div>
        <div data-options="region:'west',split:true" title="Datos de Paciente" style="width:20%;padding:20px">
        	<table>
        		<tr>
        			<td><strong>Primer Nombre</strong></td>
        		</tr>
        		<tr>
        			<td><input text=""  id="PrimerNombre" size="30"></td>
        		</tr>
        		<tr>
        			<td><strong>Segundo Nombre</strong></td>
        		</tr>
        		<tr>
        			<td><input text=""  id="SegundoNombre" size="30"></td>
        		</tr>
        		<tr>
        			<td><strong>Apellido Paterno</strong></td>
        		</tr>
        		<tr>
        			<td><input text=""  id="ApellidoPaterno" size="30"></td>
        		</tr>
        		<tr>
        			<td><strong>Apellido Materno</strong></td>
        		</tr>
        		<tr>
        			<td><input text=""  id="ApellidoMaterno" size="30"></td>
        		</tr>
        		<tr>
        			<td><strong>Nro Documento</strong></td>
        		</tr>
        		<tr>
        			<td><input text=""  id="NroDocumento" size="30"></td>
        		</tr>
        	</table>
        </div>
        <div data-options="region:'center',iconCls:'icon-ok'" style="width:80%;">									
			<div style="clear:both">											
			<!-- Muestra la lista de Cuentas por IdPaciente  -->
			<table id="lista_atenciones_paciente"></table>
			</div>
        </div>
</div>