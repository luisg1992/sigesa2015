<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ordenes para analisis de laboratorio</title>

	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<style>
		html,body { 
        	height: 100%;
        	font-family: Helvetica; 

        }
		
		#laboratorio_icono { 
		 background:#B40404; 
		}
		
		#hospitalizacion_icono { 
		 background:#DF7401; 
		}
		
		#imagenes_icono { 
		background:#088A29; 
		}
		#otros_icono { 
		background:#0404B4; 
		}
		
		#medicamentos_icono { 
		background:#0404B4; 
		}
		
		#insumos_icono { 
		background:#0B610B; 
		}
		
		.iconos
		{
		margin-right:6px;
		width: 14px;
		height: 14px;
		}
		
		.titulo_icono{
		float:left;
		margin-right:2px;
		margin-left:8px;
		}		

	</style>

	<!-- JS -->
	<!--<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>-->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
    <script>
    $(document).ready(function() {
    	//CARGANDO LAS CONFIGURACIONES POR DEFECTO
		$('#busqueda_numero_cuenta').textbox('clear').textbox('textbox').focus();
		//BUSCAR POR NUMERO DE CUENTA
		$("#busqueda_numero_cuenta").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numero_cuenta = $("#busqueda_numero_cuenta").textbox('getText');
				$.ajax({
					url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
					type: 'POST',
					dataType: 'json',
					data: {
						tipobusuqeda:1,
						dato:numero_cuenta
					},
					success: function(data){
						var hc=data.HC;
						$('#tabla_resultados_xhc').datagrid('loadData', {"CUENTA":0,"rows":[]});
						$("#num_cuenta_d_paciente_mostrar").textbox('setText',numero_cuenta);
						$("#num_dni_d_paciente_mostrar").textbox('setText',data.DNI);
						$("#nombre_d_paciente_mostrar").textbox('setText',data.PACIENTE);
						$("#historia_d_paciente_mostrar").textbox('setText',data.HC);
						$("#fingreso_d_paciente_mostrar").textbox('setText',data.FECHAINGRESO);
						$("#fapertura_d_paciente_mostrar").textbox('setText',data.FAPERTURA);
						$("#estadocuenta_d_paciente_mostrar").textbox('setText',data.CUENTA);
						$("#paciente_fisal_mostrar").textbox('setText',data.PACIENTEFISSAL);					
						$("#faltamedica_d_paciente_mostrar").textbox('setText',data.FECHAALTAMEDICA);
						$("#fegreadmin_d_paciente_mostrar").textbox('setText',data.FEGRESOADMIN);
						$("#servicio_d_paciente_mostrar").textbox('setText',data.SERVICIO);
						$("#domicilio_d_paciente_mostrar").textbox('setText',data.DOMICILIO);
						$("#diagnostico_d_paciente_mostrar").textbox('setText',data.DIAGNOSTICO);
						$("#cie10_d_paciente_mostrar").textbox('setText',data.CIE10);
						$('#tabla_servicios').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ServiciosEstadoCuenta&dato='+numero_cuenta});
						$('#tabla_farmacia').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=FarmaciaEstadoCuenta&dato='+numero_cuenta});
						$('#tabla_CentroCosto').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=CentroCostoEstadoCuenta&dato='+numero_cuenta});	
						$.ajax({
						url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Buscar_Registro_Fallecimiento',
						type: 'POST',
						dataType: 'json',
						data: 
							{
								IdCuentaAtencion: hc
							},
						success: function(data)
						{
							if (data == 'M_1'){	
							$("#contenido").load('../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Mensaje_Fallecimiento');
							}
						}
						});	
					}
				});
				
			}

		});

		$("#buqueda_numero_historia").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numero_cuenta = $("#buqueda_numero_historia").textbox('getText');
				$.ajax({
					url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
					type: 'POST',
					dataType: 'json',
					data: {
						tipobusuqeda:2,
						dato:numero_cuenta
					},
					success: function(resultado){
						$('#tabla_resultados_xhc').datagrid('loadData', {"CUENTA":0,"rows":[]});
						$("#tabla_resultados_xhc").datagrid({data:resultado});
					}
				});
				$.ajax({
						url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Buscar_Registro_Fallecimiento',
						type: 'POST',
						dataType: 'json',
						data: 
							{
								IdCuentaAtencion: numero_cuenta
							},
						success: function(data)
						{
							if (data == 'M_1'){	
							$("#contenido").load('../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Mensaje_Fallecimiento');
							}
						}
						});	
				
			}

		});
		
		
		$("#buqueda_numero_dni").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numero_dni = $("#buqueda_numero_dni").textbox('getText');
				$.ajax({
					url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
					type: 'POST',
					dataType: 'json',
					data: {
						tipobusuqeda:3,
						dato:numero_dni
					},
					success: function(resultado){
						$('#tabla_resultados_xhc').datagrid('loadData', {"CUENTA":0,"rows":[]});
						$("#tabla_resultados_xhc").datagrid({data:resultado});
					}
				});
				$.ajax({
						url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Buscar_Registro_Fallecimiento_DNI',
						type: 'POST',
						dataType: 'json',
						data: 
							{
								IdCuentaAtencion: numero_dni
							},
						success: function(data)
						{
							if (data == 'M_1'){	
							$("#contenido").load('../../MVC_Controlador/Facturacion/FacturacionC.php?acc=Mensaje_Fallecimiento');
							}
						}
				});	
				
				
			}

		});

		
		
		
		
		

		function ImprimirEstadoCuentaConsolidado()
		{
			var num_cuenta_d_paciente_mostrar 	= $("#num_cuenta_d_paciente_mostrar").textbox('getText');
			var num_dni_d_paciente_mostrar 		= $("#num_dni_d_paciente_mostrar").textbox('getText');
			var nombre_d_paciente_mostrar 		= $("#nombre_d_paciente_mostrar").textbox('getText');
			var historia_d_paciente_mostrar 	= $("#historia_d_paciente_mostrar").textbox('getText');
			var fingreso_d_paciente_mostrar 	= $("#fingreso_d_paciente_mostrar").textbox('getText');
			var fapertura_d_paciente_mostrar 	= $("#fapertura_d_paciente_mostrar").textbox('getText');
			var estadocuenta_d_paciente_mostrar = $("#estadocuenta_d_paciente_mostrar").textbox('getText');			
			var paciente_fisal_mostrar          = $("#paciente_fisal_mostrar").textbox('getText');			
			var faltamedica_d_paciente_mostrar 	= $("#faltamedica_d_paciente_mostrar").textbox('getText');
			var idUsuarioparaSigesaLI 			= $("#idUsuarioparaSigesaLI").val();
			var newWin = window.open();
			$.ajax({
				url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ImprimirEstadoCuenta',
				type: 'POST',
				dataType: 'json',
				data: {
					num_cuenta_d_paciente_mostrar:num_cuenta_d_paciente_mostrar,
					num_dni_d_paciente_mostrar:num_dni_d_paciente_mostrar,
					nombre_d_paciente_mostrar:nombre_d_paciente_mostrar,
					historia_d_paciente_mostrar:historia_d_paciente_mostrar,
					fingreso_d_paciente_mostrar:fingreso_d_paciente_mostrar,
					fapertura_d_paciente_mostrar:fapertura_d_paciente_mostrar,
					estadocuenta_d_paciente_mostrar:estadocuenta_d_paciente_mostrar,
					faltamedica_d_paciente_mostrar:faltamedica_d_paciente_mostrar,
					idUsuarioparaSigesaLI  : idUsuarioparaSigesaLI,
					paciente_fisal : paciente_fisal_mostrar
				},
				success:function(impresion)
				{
					newWin.document.write(impresion);
				    newWin.document.close();
				    newWin.focus();
				    newWin.print();
				    newWin.close();
				}
			});
			
		}



function ImprimirCuentaConsolidado()
		{
			var num_cuenta_d_paciente_mostrar 	= $("#num_cuenta_d_paciente_mostrar").textbox('getText');
			var num_dni_d_paciente_mostrar 		= $("#num_dni_d_paciente_mostrar").textbox('getText');
			var nombre_d_paciente_mostrar 		= $("#nombre_d_paciente_mostrar").textbox('getText');
			var historia_d_paciente_mostrar 	= $("#historia_d_paciente_mostrar").textbox('getText');
			var fingreso_d_paciente_mostrar 	= $("#fingreso_d_paciente_mostrar").textbox('getText');
			var fapertura_d_paciente_mostrar 	= $("#fapertura_d_paciente_mostrar").textbox('getText');
			var estadocuenta_d_paciente_mostrar = $("#estadocuenta_d_paciente_mostrar").textbox('getText');
			var paciente_fisal_mostrar          = $("#paciente_fisal_mostrar").textbox('getText');
			var faltamedica_d_paciente_mostrar 	= $("#faltamedica_d_paciente_mostrar").textbox('getText');
			var idUsuarioparaSigesaLI 			= $("#idUsuarioparaSigesaLI").val();
			var newWin = window.open();
			$.ajax({
				url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ImprimirEstadoCuentaConsolidado',
				type: 'POST',
				dataType: 'json',
				data: {
					num_cuenta_d_paciente_mostrar:num_cuenta_d_paciente_mostrar,
					num_dni_d_paciente_mostrar:num_dni_d_paciente_mostrar,
					nombre_d_paciente_mostrar:nombre_d_paciente_mostrar,
					historia_d_paciente_mostrar:historia_d_paciente_mostrar,
					fingreso_d_paciente_mostrar:fingreso_d_paciente_mostrar,
					fapertura_d_paciente_mostrar:fapertura_d_paciente_mostrar,
					estadocuenta_d_paciente_mostrar:estadocuenta_d_paciente_mostrar,
					paciente_fisal:paciente_fisal_mostrar,
					faltamedica_d_paciente_mostrar:faltamedica_d_paciente_mostrar,
					idUsuarioparaSigesaLI  : idUsuarioparaSigesaLI
				},
				success:function(impresion)
				{
					newWin.document.write(impresion);
				    newWin.document.close();
				    newWin.focus();
				    newWin.print();
				    newWin.close();
				}
			});
			
		}
		


function ImprimirCuentaMultiples()
		{
			var num_cuenta_d_paciente_mostrar 	= $("#num_cuenta_d_paciente_mostrar").textbox('getText');
			var num_dni_d_paciente_mostrar 		= $("#num_dni_d_paciente_mostrar").textbox('getText');
			var nombre_d_paciente_mostrar 		= $("#nombre_d_paciente_mostrar").textbox('getText');
			var historia_d_paciente_mostrar 	= $("#historia_d_paciente_mostrar").textbox('getText');
			var fingreso_d_paciente_mostrar 	= $("#fingreso_d_paciente_mostrar").textbox('getText');
			var fapertura_d_paciente_mostrar 	= $("#fapertura_d_paciente_mostrar").textbox('getText');
			var estadocuenta_d_paciente_mostrar = $("#estadocuenta_d_paciente_mostrar").textbox('getText');
			var paciente_fisal_mostrar          = $("#paciente_fisal_mostrar").textbox('getText');
			var faltamedica_d_paciente_mostrar 	= $("#faltamedica_d_paciente_mostrar").textbox('getText');
			var idUsuarioparaSigesaLI 			= $("#idUsuarioparaSigesaLI").val();
			var newWin = window.open();
			
			///----
			var ss = [];
            var rows = $('#tabla_resultados_xhc').datagrid('getSelections');
            if (rows){
           
		   				var Cuentas	=0;
						var coma=0;
                        for(var i=0; i<rows.length; i++){
                            var row = rows[i];
							if(i>0){
								  coma=',';
							}else{coma='';}
                            ss.push(row.CUENTA);                            
 						    Cuentas	+=coma+row.CUENTA;
							}
							
							
			}
			///---
			
			$.ajax({
				url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ImprimirEstadoCuentaConsolidadoCuentasMultiples',
				type: 'POST',
				dataType: 'json',
				data: {
					num_cuenta_d_paciente_mostrar:Cuentas,					
					num_dni_d_paciente_mostrar:num_dni_d_paciente_mostrar,
					nombre_d_paciente_mostrar:nombre_d_paciente_mostrar,
					historia_d_paciente_mostrar:historia_d_paciente_mostrar,
					fingreso_d_paciente_mostrar:fingreso_d_paciente_mostrar,
					fapertura_d_paciente_mostrar:fapertura_d_paciente_mostrar,
					estadocuenta_d_paciente_mostrar:estadocuenta_d_paciente_mostrar,
					paciente_fisal:paciente_fisal_mostrar,
					faltamedica_d_paciente_mostrar:faltamedica_d_paciente_mostrar,
					idUsuarioparaSigesaLI  : idUsuarioparaSigesaLI
				},
				success:function(impresion)
				{
					newWin.document.write(impresion);
				    newWin.document.close();
				    newWin.focus();
				    newWin.print();
				    newWin.close();
				}
			});
			
		}
		
/*Exportar a Exel */	


function ExportarCuentaMultiples()
		{
			var num_cuenta_d_paciente_mostrar 	= $("#num_cuenta_d_paciente_mostrar").textbox('getText');
			var num_dni_d_paciente_mostrar 		= $("#num_dni_d_paciente_mostrar").textbox('getText');
			var nombre_d_paciente_mostrar 		= $("#nombre_d_paciente_mostrar").textbox('getText');
			var historia_d_paciente_mostrar 	= $("#historia_d_paciente_mostrar").textbox('getText');
			var fingreso_d_paciente_mostrar 	= $("#fingreso_d_paciente_mostrar").textbox('getText');
			var fapertura_d_paciente_mostrar 	= $("#fapertura_d_paciente_mostrar").textbox('getText');
			var estadocuenta_d_paciente_mostrar = $("#estadocuenta_d_paciente_mostrar").textbox('getText');
			var paciente_fisal_mostrar          = $("#paciente_fisal_mostrar").textbox('getText');
			var faltamedica_d_paciente_mostrar 	= $("#faltamedica_d_paciente_mostrar").textbox('getText');
			var idUsuarioparaSigesaLI 			= $("#idUsuarioparaSigesaLI").val();
		//	var newWin = window.open();
			
			///----
			var ss = [];
            var rows = $('#tabla_resultados_xhc').datagrid('getSelections');
            if (rows){
           
		   				var Cuentas	=0;
						var coma=0;
                        for(var i=0; i<rows.length; i++){
                            var row = rows[i];
							if(i>0){
								  coma=',';
							}else{coma='';}
                            ss.push(row.CUENTA);                            
 						    Cuentas	+=coma+row.CUENTA;
							}
							
							
			}
			///---
			
			$.ajax({
				url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ExportarEstadoCuentaConsolidadoCuentasMultiples',
				type: 'POST',
				dataType: 'json',
				data: {
					num_cuenta_d_paciente_mostrar:Cuentas,
					num_dni_d_paciente_mostrar:num_dni_d_paciente_mostrar,
					nombre_d_paciente_mostrar:nombre_d_paciente_mostrar,
					historia_d_paciente_mostrar:historia_d_paciente_mostrar,
					fingreso_d_paciente_mostrar:fingreso_d_paciente_mostrar,
					fapertura_d_paciente_mostrar:fapertura_d_paciente_mostrar,
					estadocuenta_d_paciente_mostrar:estadocuenta_d_paciente_mostrar,
					paciente_fisal:paciente_fisal_mostrar,
					faltamedica_d_paciente_mostrar:faltamedica_d_paciente_mostrar,
					idUsuarioparaSigesaLI  : idUsuarioparaSigesaLI
				},
				
				
				
				success:function(impresion)
				{
					
						
					  $('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible');	
					  window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion)); 
						 
					 //newWin.document.write(impresion);
					 //newWin.window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
                      impresion.preventDefault();
	
					//
					//var htmltable= document.getElementById('my-table-id');
       				//var html = htmltable.outerHTML;
	       			//window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
					//window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#dvData').html()));
                    //impresion.preventDefault();
		
					//newWin.document.write(impresion);
					
					//javascript:window.location='getFile.php?p='+data;
					
				   // newWin.document.close();
				    //newWin.focus();
				    //newWin.print();
				    //newWin.close();
				}
			});
			
		}	
		
		//BOTON IMPRIMIR
		$("#imprimirResultadoConsolidado_btn").click(function(event) {
			
			ImprimirEstadoCuentaConsolidado();
		});
		
		$("#imprimirCuentaConsolidado_btn").click(function(event) {
		
			ImprimirCuentaConsolidado();
		});
		$("#imprimirCuentaMultiples_btn").click(function(event) {
		
			ImprimirCuentaMultiples();
		});
				
		
		
		$("#exportarCuentaMultiples_btn").click(function(event) {
		
		 
			ExportarCuentaMultiples();
			
			
		});
		

		//FIN
    });

	function cellStyler(value,row,index){
        return 'background-color:#ffee00;color:red;';
    }
    </script>
</head>
<body>
	<div class="easyui-panel"  style="width:100%;height:100%;" title="Estado de Cuenta del Paciente">
		<div class="easyui-layout" data-options="fit:true">
			<!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:true" style="height:25%;">
            	<div class="easyui-layout" data-options="fit:true">
            		<input type="hidden" id="idUsuarioparaSigesaLI" value="<?php echo $_REQUEST['IdEmpleado']?>">
					<!-- CONTENEDOR: BUSQUEDAS -->
            		<div data-options="region:'west'" title="" style="width:20%;height:100%;padding:1%;" >
            			<table>
            				<tr>
            					<td><u><strong>BUSQUEDAS:</strong></u></td>
            					<td></td>
            				</tr>
            				<tr>
            					<td>N° CUENTA</td>
            					<td><input type="text" class="easyui-textbox" id="busqueda_numero_cuenta" data-options="prompt:'N°CUENTA'"></td>
            				</tr>
            				<tr>
            					<td>N° H.C.</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N° H.C.'" id="buqueda_numero_historia"></td>
            				</tr>
            				<tr>
            					<td>N° DNI</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N° DNI'"  id="buqueda_numero_dni"></td>
            				</tr>
            				
            				<!--<tr>
            					<td>N° ORD. PAGO SERV.</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N°ORD. PAGO SERV.'"></td>
            				</tr>
            				<tr>
            					<td>N° DCTO. EXO. FARM.</td>
            					<td><input type="text" class="easyui-textbox" data-options="prompt:'N° DCTO. EXO. FARM.'"></td>
            				</tr>-->
            			</table>
            		</div>
					 <!-- DATOS PACIENTES-->
            <div data-options="region:'center',split:true" style="height:20%;padding:1%;">
            	<u><strong>DATOS PACIENTE:</strong></u>
            	<table style="width:100%;">
            		<tr>
            			<td>NOMBRE</td>
            			<td>
            				<input type="text" class="easyui-textbox" id="num_cuenta_d_paciente_mostrar" data-options="prompt:'N°CUENTA'" style="width:60px;">&nbsp;
            				<input type="text" class="easyui-textbox" id="nombre_d_paciente_mostrar"     data-options="prompt:'PACIENTE'" style="width:200px;">&nbsp;
							HC  &nbsp;
            				<input type="text" class="easyui-textbox" id="historia_d_paciente_mostrar"   data-options="prompt:'N° H.C.'" style="width:60px;">
							DNI
							<input type="text" class="easyui-textbox" id="num_dni_d_paciente_mostrar"    data-options="prompt:'N° DNI'" style="width:70px;">
            			</td>
            			<td>F. INGRESO</td>
            			<td><input type="text" class="easyui-textbox" id="fingreso_d_paciente_mostrar" data-options="prompt:'FECHA INGRESO'"></td>
            			<td>F. APER. CTA.</td>
            			<td><input type="text" class="easyui-textbox" id="fapertura_d_paciente_mostrar" data-options="prompt:'FECHA APERTURA'"></td>
            		</tr>
            		<tr>
            			<td>CUENTA</td>
            			<td><input type="text" class="easyui-textbox" id="estadocuenta_d_paciente_mostrar" data-options="prompt:''" style="width:350px;color:red;">
						<input type="text" class="easyui-textbox" id="paciente_fisal_mostrar" data-options="prompt:''" style="width:105px;color:red;">
						</td>
            			<td>F. ALTA MED.</td>
            			<td><input type="text" class="easyui-textbox" id="faltamedica_d_paciente_mostrar" data-options="prompt:'FECHA ALTA'"></td>
            			<td>F. EGRE. ADM.</td>
            			<td><input type="text" class="easyui-textbox" id="fegreadmin_d_paciente_mostrar" data-options="prompt:'FECHA EGRESO'"></td>
            		</tr>
            		<tr>
            		  <td>DX. EGR</td>
            		  <td><input type="text" class="easyui-textbox" id="diagnostico_d_paciente_mostrar" data-options="prompt:'DX. EGRE'" style="width:457px;"></td>
            		  <td>CIE 10</td>
            		  <td colspan="3"><input type="text" class="easyui-textbox" id="cie10_d_paciente_mostrar" data-options="prompt:'CIE 10'"></td>
          		  </tr>
            		<tr>
            			<td>DOMICILIO</td>
            			<td ><input type="text" class="easyui-textbox" id="domicilio_d_paciente_mostrar" data-options="prompt:'DOMICILIO'" style="width:457px;"></td>
            			<td>SERV. EG.</td>
            			<td colspan="3"><input type="text" class="easyui-textbox" id="servicio_d_paciente_mostrar" data-options="prompt:'SERVICIO'" style="width:440px;"></td>
            		</tr>
					<tr>
            			<td><div id="contenido"  style="color:red;font-size:16px;font-weight:bold;"></div></td>
            		</tr>
            	</table>
            </div>
            	</div>
            </div>
            	<!-- CUENTAS A SELECCIONAR -->
            		<div data-options="region:'center'" title="" style="width:70%;height:100%;padding:1%;" >
            			<u><strong>LISTA DE CUENTAS:</strong></u>
						<br>
						<br>
            			<table class="easyui-datagrid" style="width:100%;height:auto;"  id="tabla_resultados_xhc" data-options="
							onClickRow:function(row){
								var ss = [];
								var rows = $('#tabla_resultados_xhc').datagrid('getSelections');
								for(var i=0; i<rows.length; i++){
									var row = rows[i];
									ss.push(','+row.CUENTA);
								}
								
								var Idcuenta=ss.join('').substring(1);
								$.ajax({
								url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
								type: 'POST',
								dataType: 'json',
								data: {
									tipobusuqeda:1,
									dato:Idcuenta
								},
								success: function(data){
									$('#num_cuenta_d_paciente_mostrar').textbox('setText',row.CUENTA);
									$('#nombre_d_paciente_mostrar').textbox('setText',data.PACIENTE);
									$('#historia_d_paciente_mostrar').textbox('setText',data.HC);
									$('#num_dni_d_paciente_mostrar').textbox('setText',row.DNI);
									$('#fingreso_d_paciente_mostrar').textbox('setText',data.FECHAINGRESO);
									$('#fapertura_d_paciente_mostrar').textbox('setText',data.FAPERTURA);
									$('#estadocuenta_d_paciente_mostrar').textbox('setText',data.CUENTA);
                                    $('#paciente_fisal_mostrar').textbox('setText',data.PACIENTEFISSAL);                                    
									$('#faltamedica_d_paciente_mostrar').textbox('setText',data.FECHAALTAMEDICA);
									$('#fegreadmin_d_paciente_mostrar').textbox('setText',data.FEGRESOADMIN);
									$('#servicio_d_paciente_mostrar').textbox('setText',data.SERVICIO);
									$('#domicilio_d_paciente_mostrar').textbox('setText',data.DOMICILIO);
									$('#diagnostico_d_paciente_mostrar').textbox('setText',data.DIAGNOSTICO);
									$('#cie10_d_paciente_mostrar').textbox('setText',data.CIE10);
					                $('#tabla_servicios').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ServiciosEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_farmacia').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=FarmaciaEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_CentroCosto').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=CentroCostoEstadoCuenta&dato='+Idcuenta});  
								},
								error: function(data) {
					                $('#tabla_servicios').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ServiciosEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_farmacia').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=FarmaciaEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_CentroCosto').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=CentroCostoEstadoCuenta&dato='+Idcuenta});  
								}
							});
							}"> 
						    <script type="text/javascript">
							function getSelections()
							{
								var ss = [];
								var rows = $('#tabla_resultados_xhc').datagrid('getSelections');
								for(var i=0; i<rows.length; i++){
									var row = rows[i];
									ss.push(','+row.CUENTA);
								}
								
								var Idcuenta=ss.join('').substring(1);
								$.ajax({
								url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
								type: 'POST',
								dataType: 'json',
								data: {
									tipobusuqeda:1,
									dato:Idcuenta
								},
								success: function(data){
									$('#num_cuenta_d_paciente_mostrar').textbox('setText',row.CUENTA);
									$('#num_dni_d_paciente_mostrar').textbox('setText',row.DNI);
									$('#nombre_d_paciente_mostrar').textbox('setText',data.PACIENTE);
									$('#historia_d_paciente_mostrar').textbox('setText',data.HC);
									$('#fingreso_d_paciente_mostrar').textbox('setText',data.FECHAINGRESO);
									$('#fapertura_d_paciente_mostrar').textbox('setText',data.FAPERTURA);
									$('#estadocuenta_d_paciente_mostrar').textbox('setText',data.CUENTA);
                                    $('#paciente_fisal_mostrar').textbox('setText',data.PACIENTEFISSAL);                                    
									$('#faltamedica_d_paciente_mostrar').textbox('setText',data.FECHAALTAMEDICA);
									$('#fegreadmin_d_paciente_mostrar').textbox('setText',data.FEGRESOADMIN);
									$('#servicio_d_paciente_mostrar').textbox('setText',data.SERVICIO);
									$('#domicilio_d_paciente_mostrar').textbox('setText',data.DOMICILIO);
									$('#diagnostico_d_paciente_mostrar').textbox('setText',data.DIAGNOSTICO);
									$('#cie10_d_paciente_mostrar').textbox('setText',data.CIE10);
					                $('#tabla_servicios').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ServiciosEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_farmacia').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=FarmaciaEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_CentroCosto').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=CentroCostoEstadoCuenta&dato='+row.CUENTA});  
								},
								error: function(data) {
					                $('#tabla_servicios').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=ServiciosEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_farmacia').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=FarmaciaEstadoCuenta&dato='+Idcuenta});
                                    $('#tabla_CentroCosto').datagrid({url:'../../MVC_Controlador/Facturacion/FacturacionC.php?acc=CentroCostoEstadoCuenta&dato='+row.CUENTA});  
								}
							});
							}
							</script>
						
						
				        <thead>
				            <tr>
							<th field="IdCuentasAtenciones" checkbox="true"></th>
				            	<th data-options="field:'HC',rezisable:true,align:'center'">H.C.</th>	
								<th data-options="field:'DNI',rezisable:true,align:'center'">DNI</th>	
				            	<th data-options="field:'CUENTA',rezisable:true,align:'center', styler:cellStyler">CUENTA</th>	
				            	<th data-options="field:'PACIENTE',rezisable:true,align:'center'">PACIENTE</th>	
				            	<th data-options="field:'TIPOATENCION',rezisable:true,align:'center'">TIPOATENCION</th>
				            	<th data-options="field:'SERVICIO',rezisable:true,align:'CENTER'">SERVICIO</th>
				            	<th data-options="field:'CODPRES',rezisable:true,align:'center'">C. PREST.</th>		
								<th data-options="field:'FUA',rezisable:true,align:'center'">NUM. FUA</th>			                
								<th data-options="field:'FAPERTURA',rezisable:true,align:'center'">F. APERTURA</th>	
								<th data-options="field:'FLIQUIDACION',rezisable:true,align:'center'">F. FLIQUIDACION</th>	
								<th data-options="field:'FINANCIAMIENTO',rezisable:true,align:'center'">FUENTE FINAN.</th>
								<th data-options="field:'ESTADOCUENTA',rezisable:true,align:'center'">ESTADO</th>

				            </tr>
				        </thead>
				    </table>
            		</div>



           

            <!-- DATOS CUERPO-->
            <div data-options="region:'south',split:true" style="height:55%;padding:1%;">
                <div class="easyui-tabs" style="width:100%;height:80%;">
			        <div title="Servicios" style="padding:10px">
					<div style=" width: 550px;
				    height: 25px;">

					<!-- Hospitalización  -->
					<div  class='titulo_icono'>
					<strong>Hospitalización</strong>
					</div>
					<div  id="hospitalizacion_icono" style="float:left"  class="iconos"></div>
					
					<!-- Imagenes  -->
					<div  class='titulo_icono' >
					<strong>Imagenes</strong>
					</div>
					<div  id="imagenes_icono" style="float:left"  class="iconos"></div>
					
				
					<!-- Laboratorio  -->
					<div  class='titulo_icono'>
					<strong>Laboratorio</strong>
					</div>
					<div  id="laboratorio_icono" style="float:left" class="iconos"></div>
					
					
					<!-- Otros  -->
					<div  class='titulo_icono'>
					<strong>Otros</strong>
					</div>
					<div  id="otros_icono" style="float:left"  class="iconos"></div>
					
					</div>
					
					
					
			            <table class="easyui-datagrid" style="width:95%;height:300px;" data-options="collapsible: true,
							onClickRow:function(row){	
								var row = $('#tabla_servicios').datagrid('getSelected');			    			
								
								},
								rowStyler: function(index,row){
												if (row.PARTIDACODIGO==133421){
													return 'color:#DF0101;';
												}
												if (row.PARTIDACODIGO==133416){
													return 'color:#DF7401;';
												}
												if (row.PARTIDACODIGO==133424){
													return 'color:#0B610B;';
												}
                                                
                                                if (row.PARTIDACODIGO== ''){
													return 'color:#FFFF00;background:red;font-weight:bold';
												}
                                                
												else{
													return 'color:#0404B4;';
												}	
								}
            			" id="tabla_servicios">
				        <thead>
				            <tr>
							
				            	<th data-options="field:'NUMERO',rezisable:true,hidden:true">F. Atencion</th>							
				                <th data-options="field:'FATENCION',rezisable:true,align:'left'">F. Atencion</th>
								<th data-options="field:'IDCUENTA',rezisable:true,align:'left'">Cuenta</th>			
								<th data-options="field:'PUNTOCARGA',rezisable:true,align:'left'">Puntos de Carga</th>	
								<th data-options="field:'CODIGO',rezisable:true,align:'center'">Codigo</th>	
								<th data-options="field:'DESCRIPCION',rezisable:true,align:'left'">Descripcion</th>	
								<th data-options="field:'CANTIDAD',rezisable:true,align:'center'">Cantidad</th>								
								<th data-options="field:'PRUNIT',rezisable:true,align:'center'">Pr. Unitario</th>
								<th data-options="field:'TOTALPAGAR',rezisable:true,align:'center'">Total</th>
				            </tr>
				        </thead>
				    </table>
			        </div>
			        <div title="Farmacia" style="padding:10px">
					<div style=" width: 250px;
				   height: 25px;">
					<div class='titulo_icono' >
					<strong>Medicamentos</strong>
					</div>
					<div  id="medicamentos_icono" style="float:left" class="iconos"></div>
					<div  class='titulo_icono' >
					<strong>Insumos</strong>
					</div>
					<div  id="insumos_icono" style="float:left"  class="iconos"></div>
					</div>
		
			
			            <table class="easyui-datagrid" style="width:95%;height:300px;" data-options="collapsible: true,
							onClickRow:function(row){	
								var row = $('#tabla_farmacia').datagrid('getSelected');			    			
								
								},
								rowStyler: function(index,row){
												if (row.TipoProducto!= 0){
													return 'color:#0B610B;';
												}
												else{
													return 'color:#0404B4;';
												}
								}
            			" id="tabla_farmacia">
				        <thead>
				            <tr>
				            	<th data-options="field:'FechaHoraPrescribe',rezisable:true">Fecha</th>			                
				                <th data-options="field:'DocumentoNumero',rezisable:true,align:'left'">Numero-Doc</th>
								<th data-options="field:'Cuenta',rezisable:true,align:'left'">Cuenta</th>		
								<th data-options="field:'Codigo',rezisable:true,align:'left'">Codigo</th>	
								<th data-options="field:'Nombre',rezisable:true,align:'left'">Descripcion</th>	
								<th data-options="field:'cantidad',rezisable:true,align:'left'">Cantidad</th>	
								<th data-options="field:'precio',rezisable:true,align:'center'">Precio</th>								
								<th data-options="field:'total',rezisable:true,align:'center'">Total</th>
								<th data-options="field:'descripcion',rezisable:true,align:'center'">Farmacia</th>
				            </tr>
				        </thead>
				    </table>
			        </div>
			        <div title="Consolidado Centro de Costo" style="padding:10px">
			             <table class="easyui-datagrid" style="width:95%;height:300px;" data-options="collapsible: true,
							onClickRow:function(row){	
								var row = $('#tabla_CentroCosto').datagrid('getSelected');			    			
								
								}
            			" id="tabla_CentroCosto">
				        <thead>
				            <tr>
				            	 	
								<th data-options="field:'Descripcion',rezisable:true,align:'left'">Centro de Costo</th>	
								<th data-options="field:'Cantidad',rezisable:true,align:'left'">Cantidad</th>	
							 						
								<th data-options="field:'Total',rezisable:true,align:'center'">Total</th>
							 
				            </tr>
				        </thead>
				    </table>
			        </div>
			    </div><br>
			    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:110px" id="imprimirCuentaConsolidado_btn">Imprimir Consolidado</a>
			    &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:110px" id="imprimirResultadoConsolidado_btn">Imprimir Estado de Cuenta</a>
                 <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:110px" id="imprimirCuentaMultiples_btn">Imprimir  Cuentas Consolidadas </a>
                 <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" style="width:110px" id="exportarCuentaMultiples_btn">Exportar Cuentas Consolidadas </a>
                
			    
            </div>
		</div>
	</div>
</body>
</html>