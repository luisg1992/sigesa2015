    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Antimicrobianos</title>
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

		</style>
		</head>
		<body>
		<script type="text/javascript">	
		$(document).ready(function(){
		$('#Contenedor_Editor').dialog('close');
		});
		
		function Cancelar_Actualizacion(){
		$('#Contenedor_Editor').dialog('close');	
		}

		function Ejecutar_Actualizacion(){	
		$('#Contenedor_Editor').dialog('close');
		var IdProducto=$("#IdProducto").val();  		
		var TiposProducto=$( "#TiposProducto option:selected" ).val();
		var parametros =
			{
			"IdProducto" : IdProducto,
			"TipoProductoSismed" : TiposProducto
			};												
			$.ajax({
				data:  parametros,
				url:   '../../MVC_Controlador/Fact - Config/Fact - ConfigC.php?acc=ModificarMedicamento',
				type:  'post'
			});
			$.messager.alert('SIGESA','Se modifico Medicamento Satisfactoriamente');	
			$('#Medicamentos').datagrid('reload');
		}
		

		function Estilo_Datagrid(value,row,index){
			return 'color:#8A0808;font-weight:bold';
		}
		</script>

		
		<script type="text/javascript">	
		$(function(){
		/* Aqui te Permite Seleccionar mas de un Item Selected   */
		$('#Medicamentos').datagrid({singleSelect:(1)});
		/* Habilita el Grid para poder buscar por filtro   */
		var Medicamentos = $('#Medicamentos').datagrid();
		 Medicamentos.datagrid('enableFilter');
		});

		function Modificar(){
		var row = $('#Medicamentos').datagrid('getSelected');
		if (row){
			$('#Contenedor_Editor').window('open');
			$('#IdProducto').val(row.IdProducto);
			$('#Codigo').val(row.Codigo);
			$('#Nombre').val(row.Nombre);
			$("#TiposProducto").val(row.TipoProductoSismed);
		}
		}
		</script>
		<div class="easyui-layout" style="width:100%;height:100%;">
					<div data-options="region:'west',split:true,iconCls:'icon-ok'" title="Lista de Medicamentos" style="width:80%;">
					<div style="margin-left:10px;float:left; width:450px">
					<p>Lista de Medicamentos disponibles</p>
					</div>
					<?php
						  if($ListarPermisos != NULL)
						  {
							  foreach($ListarPermisos as $Permiso)
							  { 
							  $Agregar=$Permiso["Agregar"];
							  $Modificar=$Permiso["Modificar"];
							  $Eliminar=$Permiso["Eliminar"];
							  $Consultar=$Permiso["Consultar"];
							  }
						?>
						<?php						
						if($Consultar==0){?>
						<script>$(function(){ $('#Consultar').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php if($Modificar==0){?>
						<script>$(function(){ $('#Modificar').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php if($Eliminar==0){?>
						<script>$(function(){ $('#Eliminar').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php if($Agregar==0){?>
						<script>$(function(){ $('#Agregar').linkbutton({disabled:true});});</script>
						<?php } ?>
						<?php
						}						
						?>
					
						<div  id="opciones">
						<p>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true"  onclick="Consultar()"  id="Consultar">CONSULTAR</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" 	onclick="Agregar()"    id="Agregar">AGREGAR</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true"	onclick="Modificar()"  id="Modificar" >MODIFICAR</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" 	onclick="Eliminar()"   id="Eliminar">ELIMINAR</a>
						</p>
						</div>
					
					
					<div style="clear:both">
					</div>
				    <table id="Medicamentos" class="easyui-datagrid"  style="width:100%;height:610px"
					data-options="rownumbers:true,singleSelect:true,pageSize:20,pagination: true,url:'../../MVC_Controlador/Fact - Config/Fact - ConfigC.php?acc=MostrarMedicamentosGrilla',method:'get',toolbar: '#opciones'">
					<thead>
						<tr>
							<th data-options="field:'Codigo',width:80">Codigo</th>
							<th data-options="field:'Nombre',width:550">Nombre de Producto</th>
							<th data-options="field:'TipoProducto',width:205,styler:Estilo_Datagrid,">Tipo de Medicamento</th>
						</tr>
					</thead>
					</table>
					</div>
					<div id="Contenedor_Editor" class="easyui-window" title="Editar  Medicamento" data-options="iconCls:'icon-save'" style="width:500px;height:180px;padding:10px;">
						<div style="float:left;margin-left:15px;">
							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px;margin-top:1px">						
							<label for="message">Codigo: </label>
							</div>
							<div style="float:left;margin-bottom:7px;margin-left:12px">
							<input type="hidden"  size="6" id="IdProducto"   readonly="readonly">
							<input type="text"  size="6" id="Codigo"   readonly="readonly">
							</div>
							</div>

							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px">						
							<label for="message">Descripción: </label>
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px">
							<input type="text"  size="37" id="Nombre"   readonly="readonly">
							</div>
							</div>	
							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px">						
							<label for="message">Tipo Producto: </label>
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px;padding:10px 0px 0px 0px">
								<select  name="TiposProducto" style="width:250px" id="TiposProducto" >
									<?php 
									$resultados=Listado_TiposProducto_M();								 
									if($resultados!=NULL){
									for ($i=0; $i < count($resultados); $i++) {	
									 ?>
									 <option value="<?php echo $resultados[$i]["TipoProductoSismed"] ?>"><?php echo mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
									<?php 
									}}
									?>                   
								</select>
							</div>
							</div>
							<div style="clear:both">
							<div style="float:left;width:80px;margin-left:12px">						
							&nbsp;&nbsp;
							</div>
							<div style="float:left;margin-bottom:2px;margin-left:12px;padding:10px 0px 0px 0px">
								<a href="#" class="easyui-linkbutton"   onclick="Ejecutar_Actualizacion()" data-options="iconCls:'icon-save'">Aceptar</a>
								<a href="#" class="easyui-linkbutton"  onclick="Cancelar_Actualizacion()"  data-options="iconCls:'icon-cancel'">Cancelar</a>
							</div>
							</div>								
						</div>
					</div>
					<div style="clear:both">
					</div>
        </div>
    </body>
    </html>