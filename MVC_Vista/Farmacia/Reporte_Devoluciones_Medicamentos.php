<?php
 ini_set('memory_limit', '1024M'); 
header("Content-Type: text/html; charset=UTF-8",true);
include("../../MVC_Modelo/FarmaciaM.php");
include("../../MVC_Complemento/librerias/Funciones.php");


require_once '../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->
	getProperties()
		->setCreator("WWW.HNDAC.GOB.PE")
		->setLastModifiedBy("HNDAC")
		->setTitle("HOSPITAL NAVIONAL DANIEL ALCIDES CARRION")
		->setSubject("SISTEMA DE REPORTES - SIGESA ")
		->setDescription("DOCUMENTO GENERADO LA UNIDAD E INFORMATICA ")
		->setKeywords("R.A.B.- DESARROLLO DE SOFTWARE")
		->setCategory("REPORTE");
	
		$ListarReporte=Sigesa_Reporte_Devoluciones_Medicamentos_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));
		 if($ListarReporte != NULL){ 
				$i=2;
				foreach($ListarReporte as $item){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Fecha de Registro')
					->setCellValue('B1', 'Codigo')
					->setCellValue('C1', 'Nombre')
					->setCellValue('D1', 'Tipo')
					->setCellValue('E1', 'Cantidad')
					->setCellValue('F1', 'Precio')
					->setCellValue('G1', 'Total')
					->setCellValue('H1', 'Observaciones')
					->setCellValue('I1', 'Nombre de Servicio')
					
					//Lista de Elementos
					->setCellValue('A'.$i, $item["fechaCreacion"])
					->setCellValue('B'.$i, $item["Codigo"])
					->setCellValue('C'.$i, $item["Nombre"])
					->setCellValue('D'.$i, $item["TipoProducto"])
					->setCellValue('E'.$i, $item["Cantidad"])
					->setCellValue('F'.$i, $item["Precio"])
					->setCellValue('G'.$i, $item["Total"])
					->setCellValue('H'.$i, $item["Observaciones"])
					->setCellValue('I'.$i, $item["Servicio"])
					 ;
					$i++;
				}
			}
 	
            ;


 
$objPHPExcel->getActiveSheet()->setTitle('Venta_Diaria');
$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: application/vnd.ms-excel;charset=UTF-8');
header('Content-Disposition: attachment;filename="FARMACIA_LISTA_DEVOLUCIONES_MEDICAMENTOS_INSUMOS.xls"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>