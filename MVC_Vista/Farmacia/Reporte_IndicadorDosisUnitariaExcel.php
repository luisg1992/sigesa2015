<?php 
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0); 
include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();
// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "CODIGO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "MEDICAMENTO");	 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "CANTIDAD");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "SERVICIO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "PRECIO VENTA");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "TIPO PRODUCTO");			
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "ORIGEN");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "DESTINO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "FECHA PRESCRIBE");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "TOTAL");
				
			//$resultados=SIGESA_Farmacia_ReporteIndicadorDosisUnitaria_M($FechaInicio,$FechaFinal,$IdServicio,$IdAlmacenFarmacia,$IdTipoServicio);
			$totfilas=2;
			if($resultados!=NULL){
				$totCantidad=0;
				$totPrecio=0;
				$totTotal=0;
				for ($i=0; $i < count($resultados); $i++) {	
					$Total=$resultados[$i]["cantidad"]*$resultados[$i]["PrecioVenta"];					
					$totCantidad=$totCantidad+$resultados[$i]["cantidad"];
					$totPrecio=$totPrecio+$resultados[$i]["PrecioVenta"];
					$totTotal= $totTotal+$Total;					  
				 
			    	$FechaPrescribe=vfecha(substr($resultados[$i]["FechaHoraPrescribe"],0,10));	
					$HoraPrescribe=substr($resultados[$i]["FechaHoraPrescribe"],11,8);	
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), $resultados[$i]["Codigo"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $resultados[$i]["Nombre"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $resultados[$i]["cantidad"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $resultados[$i]["NombreServicio"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $resultados[$i]["PrecioVenta"]);							
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $resultados[$i]["TipoProducto"]); 				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $resultados[$i]["Origen"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $resultados[$i]["Destino"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $FechaPrescribe.' '.$HoraPrescribe);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $Total);			
		 		}
				 $totfilas=$i+2;
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($totfilas), $totCantidad);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($totfilas), $totPrecio);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($totfilas), $totTotal);	
	   }
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('DosisUnitarias'); 		
		 $objActSheet->getColumnDimension('B')->setWidth(30);	
		 $objActSheet->getColumnDimension('D')->setWidth(30);
	
	
//Formato General
//El ancho de columna
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
		 
    //Formato primera Fila 
    $objStyleAZ = $objActSheet ->getStyle('A1:Z1');
    //$objStyleAZ ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontAZ = $objStyleAZ->getFont(); 
    $objFontAZ->setName('Arial'); 
    $objFontAZ->setSize(11); 
    $objFontAZ->setBold(true); 
    //$objFontA->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    //$objFontA ->getColor()->setARGB('FFFF0000') ;
    //$objFontA ->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE); 
	//Establecer la alineación 
    $objAlignAZ = $objStyleAZ->getAlignment(); 
    $objAlignAZ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignAZ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	//Formato Columna A
    $objStyleA = $objActSheet ->getStyle('A2:A'.$totfilas);		
	//Establecer la alineación 
    $objAlignA = $objStyleA->getAlignment(); 
    $objAlignA->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//Formato Columna F
    $objStyleF = $objActSheet ->getStyle('F2:F'.$totfilas);	
	//Establecer la alineación 
    $objAlignF = $objStyleF->getAlignment(); 
    $objAlignF->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//Formato Columna I
    $objStyleI = $objActSheet ->getStyle('I2:I'.$totfilas);	   
	//Establecer la alineación 
    $objAlignI = $objStyleI->getAlignment(); 
    $objAlignI->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//Formato Columna E
    $objStyleE = $objActSheet ->getStyle('E2:E'.$totfilas);	 
	$objStyleE->getNumberFormat()->setFormatCode('###0.00');

	//Formato Columna J
    $objStyleJ = $objActSheet ->getStyle('J2:J'.$totfilas);	 
	$objStyleJ->getNumberFormat()->setFormatCode('###0.00');

	//Formato Totales
    $objStyleFJ = $objActSheet ->getStyle('F'.$totfilas.':'.'J'.$totfilas); 
	$objStyleFJ->getNumberFormat()->setFormatCode('###0.00');

	//Formato Totales
    $objStyleT = $objActSheet ->getStyle('C'.$totfilas.':'.'J'.$totfilas); 	
	//Configuración de tipos de letra 
    $objStyleT = $objStyleT->getFont(); 
    $objStyleT->setName('Arial'); 
    $objStyleT->setSize(11); 
    $objStyleT->setBold(true); 
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="DosisUnitarias.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>
			
  