<?php
 ini_set('memory_limit', '1024M'); 
header("Content-Type: text/html; charset=UTF-8",true);
include("../../MVC_Modelo/FarmaciaM.php");
include("../../MVC_Complemento/librerias/Funciones.php");


require_once '../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->
	getProperties()
		->setCreator("WWW.HNDAC.GOB.PE")
		->setLastModifiedBy("HNDAC")
		->setTitle("HOSPITAL NAVIONAL DANIEL ALCIDES CARRION")
		->setSubject("SISTEMA DE REPORTES - SIGESA ")
		->setDescription("DOCUMENTO GENERADO LA UNIDAD E INFORMATICA ")
		->setKeywords("L.G.N.- DESARROLLO DE SOFTWARE")
		->setCategory("REPORTE");

      
	  
	  
	 		
//$ListarReporte=ReporteIngreso_emergencia01_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["IdServicio"]);
$ListarReporte=Sigesa_IngresoSalidaFarmacia_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["Variable"]); 

 if($ListarReporte != NULL)	{ 
 $i=2;
 foreach($ListarReporte as $item){
	 
/*
if($item["TriajeTalla"]!=NULL){$TriajeTalla='Tal: '.$item["TriajeTalla"];}
if($item["TriajePeso"]!=NULL){$TriajePeso=' Pe: '.$item["TriajePeso"];}
if($item["TriajePresion"]!=NULL){$TriajePresion=' Pr: '.$item["TriajePresion"];}
if($item["TriajeTemperatura"]!=NULL){$TriajeTemperatura=' Tm: '.$item["TriajeTemperatura"];}
if($item["TriajePulso"]!=NULL){$TriajePulso=' Pul: '.$item["TriajePulso"];}
if($item["TriajeFrecRespiratoria"]!=NULL){$TriajeFrecRespiratoria=' F.Rs: '.$item["TriajeFrecRespiratoria"];}
if($item["TriajeFrecCardiaca"]!=NULL){$TriajeFrecCardiaca=' F.Cr: '.$item["TriajeFrecCardiaca"];}

 
 
  */
 
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'MOVIMIENTO')
            ->setCellValue('B1', 'TIPO MOVIMIENTO')
            ->setCellValue('C1', 'FARMACIA ORIGEN')
			->setCellValue('D1', 'FARMACIA DESTINO')
			->setCellValue('E1', 'IDPRODUCTO')
			->setCellValue('F1', 'PRODUCTO')
			->setCellValue('G1', 'CANTIDAD')
			->setCellValue('H1', 'DOCUMENTO')
			->setCellValue('I1', 'FECHA CREACION')
			->setCellValue('J1', 'CONCEPTO')

			
			//->setCellValue('H1', 'HC')
			
			->setCellValue('A'.$i, $item["MovNumero"] )
            ->setCellValue('B'.$i, utf8_encode($item["MovTipo"]))
			->setCellValue('C'.$i, $item["fOrigen"])
			->setCellValue('D'.$i, $item["FDestino"])
			//->setCellValue('E'.$i, $item["FechaNacimiento"])
			->setCellValue('E'.$i, $item["idProducto"])
			->setCellValue('F'.$i, $item["Nombre"])
			->setCellValue('G'.$i, $item["Cantidad"])
			->setCellValue('H'.$i, $item["DocumentoNumero"])
			->setCellValue('I'.$i, $item["fechaCreacion"])
			->setCellValue('J'.$i, $item["Concepto"])
	

			 ;
$i++;
 }
 }
 					 		

 
$objPHPExcel->getActiveSheet()->setTitle('Venta_Diaria');
$objPHPExcel->setActiveSheetIndex(0);



header('Content-Type: application/vnd.ms-excel;charset=UTF-8');
header('Content-Disposition: attachment;filename="FARMACIA_INGRESOS_SALIDA.xls"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>