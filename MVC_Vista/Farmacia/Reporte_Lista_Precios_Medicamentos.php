<?php
 ini_set('memory_limit', '1024M'); 
header("Content-Type: text/html; charset=UTF-8",true);
include("../../MVC_Modelo/FarmaciaM.php");
include("../../MVC_Complemento/librerias/Funciones.php");


require_once '../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->
	getProperties()
		->setCreator("WWW.HNDAC.GOB.PE")
		->setLastModifiedBy("HNDAC")
		->setTitle("HOSPITAL NAVIONAL DANIEL ALCIDES CARRION")
		->setSubject("SISTEMA DE REPORTES - SIGESA ")
		->setDescription("DOCUMENTO GENERADO LA UNIDAD E INFORMATICA ")
		->setKeywords("R.A.B.- DESARROLLO DE SOFTWARE")
		->setCategory("REPORTE");
	
		$ListarReporte=Sigesa_Reporte_Lista_Precio_Medicamentos_M();
		 if($ListarReporte != NULL){ 
				$i=2;
				foreach($ListarReporte as $item){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Codigo')
					->setCellValue('B1', 'Nombre')
					->setCellValue('C1', 'Fecha De Vencimiento')
					->setCellValue('D1', 'Precio')
					->setCellValue('E1', 'Precio de Compra')
					->setCellValue('F1', 'Precio de Distribucion')
					->setCellValue('G1', 'Precio de Donacion')
					->setCellValue('H1', 'Precio de Ult. Compra')
					->setCellValue('I1', 'Registro Sanitario')
					->setCellValue('J1', 'Lote')
					->setCellValue('K1', 'Razon Social')
					->setCellValue('L1', 'Ruc')
					//Lista de Elementos
					->setCellValue('A'.$i, $item["Codigo"])
					->setCellValue('B'.$i, $item["Nombre"])
					->setCellValue('C'.$i, $item["FechaVencimiento"])
					->setCellValue('D'.$i, $item["Precio"])
					->setCellValue('E'.$i, $item["PrecioCompra"])
					->setCellValue('F'.$i, $item["PrecioDistribucion"])
					->setCellValue('G'.$i, $item["PrecioDonacion"])
					->setCellValue('H'.$i, $item["PrecioUltCompra"])
					->setCellValue('I'.$i, $item["RegistroSanitario"])
					->setCellValue('J'.$i, $item["Lote"])
					->setCellValue('K'.$i, $item["RazonSocial"])
					->setCellValue('L'.$i, $item["Ruc"])
					 ;
					$i++;
				}
			}
 	
            ;


 
$objPHPExcel->getActiveSheet()->setTitle('Venta_Diaria');
$objPHPExcel->setActiveSheetIndex(0);



header('Content-Type: application/vnd.ms-excel;charset=UTF-8');
header('Content-Disposition: attachment;filename="FARMACIA_LISTA_PRECIO_MEDICAMENTOS.xls"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;



 
  

 
?>