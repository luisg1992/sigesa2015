<?php 
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0); 
include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Codigo");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Nombre");	 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "Almacen Origen");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "Almacen Destino");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Documento Numero");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Observaciones");			
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Lote");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "Fecha Registro");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "Fecha Vencimiento");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "Cantidad");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', "Precio");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', "Total");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', "Concepto");
				
			//$resultados=SIGESA_Farmacia_ReporteSaldosAlmacenLogistica_M($FechaInicio,$FechaFinal,$idTipoConcepto);
			$totfilas=2;
			if($resultados!=NULL){
				//$totCantidad=0;
				//$totPrecio=0;
				//$totTotal=0;
				for ($i=0; $i < count($resultados); $i++) {	
					//$Total=$resultados[$i]["cantidad"]*$resultados[$i]["PrecioVenta"];					
					//$totCantidad=$totCantidad+$resultados[$i]["cantidad"];
					//$totPrecio=$totPrecio+$resultados[$i]["PrecioVenta"];
					//$totTotal= $totTotal+$Total;					  
				 	$fechaCreacion=vfecha(substr($resultados[$i]["fechaCreacion"],0,10));	
			    	$FechaVencimiento=vfecha(substr($resultados[$i]["FechaVencimiento"],0,10));	
					//$HoraFechaVencimiento=substr($resultados[$i]["FechaVencimiento"],11,8); //00:00:00
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), $resultados[$i]["Codigo"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $resultados[$i]["Nombre"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $resultados[$i]["AlmacenOrige"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $resultados[$i]["AlmacenDestino"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $resultados[$i]["DocumentoNumero"]);							
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $resultados[$i]["Observaciones"]); 	

				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $resultados[$i]["Lote"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $fechaCreacion);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $FechaVencimiento);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $resultados[$i]["Cantidad"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+2), $resultados[$i]["Precio"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+2), $resultados[$i]["Total"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+2), $resultados[$i]["Concepto"]);		
		 		}
				 $totfilas=$i+2;
				 //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($totfilas), $totCantidad);
				 //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($totfilas), $totPrecio);
				 //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($totfilas), $totTotal);	
	   }
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('SaldosAlmacenLogistica'); 		
		 $objActSheet->getColumnDimension('B')->setWidth(40);	
		 $objActSheet->getColumnDimension('C')->setWidth(30);

		 $objActSheet->getColumnDimension('F')->setWidth(55);
		 $objActSheet->getColumnDimension('M')->setWidth(30);
	
	
//Formato General
//El ancho de columna
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
		 
    //Formato primera Fila 
    $objStyleAZ = $objActSheet ->getStyle('A1:Z1');
    //$objStyleAZ ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontAZ = $objStyleAZ->getFont(); 
    $objFontAZ->setName('Arial'); 
    $objFontAZ->setSize(11); 
    $objFontAZ->setBold(true); 
    //$objFontA->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    //$objFontA ->getColor()->setARGB('FFFF0000') ;
    //$objFontA ->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE); 
	//Establecer la alineación 
    $objAlignAZ = $objStyleAZ->getAlignment(); 
    $objAlignAZ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignAZ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	//Formato Columna A
    $objStyleA = $objActSheet ->getStyle('A2:A'.$totfilas);		
	//Establecer la alineación 
    $objAlignA = $objStyleA->getAlignment(); 
    $objAlignA->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
	
	//Formato Columna J y K
    $objStyleJ = $objActSheet ->getStyle('K2:L'.$totfilas);	 
	$objStyleJ->getNumberFormat()->setFormatCode('###0.00');

	//Formato Totales
    /*$objStyleFJ = $objActSheet ->getStyle('F'.$totfilas.':'.'J'.$totfilas); 
	$objStyleFJ->getNumberFormat()->setFormatCode('###0.00');

	//Formato Totales
    $objStyleT = $objActSheet ->getStyle('C'.$totfilas.':'.'J'.$totfilas); 	
	//Configuración de tipos de letra 
    $objStyleT = $objStyleT->getFont(); 
    $objStyleT->setName('Arial'); 
    $objStyleT->setSize(11); 
    $objStyleT->setBold(true); */
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="SaldosAlmacenLogistica.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>
			
  