<?php
 ini_set('memory_limit', '1024M'); 
header("Content-Type: text/html; charset=UTF-8",true);
include("../../MVC_Modelo/FarmaciaM.php");
include("../../MVC_Complemento/librerias/Funciones.php");


require_once '../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->
	getProperties()
		->setCreator("WWW.HNDAC.GOB.PE")
		->setLastModifiedBy("HNDAC")
		->setTitle("HOSPITAL NAVIONAL DANIEL ALCIDES CARRION")
		->setSubject("SISTEMA DE REPORTES - SIGESA ")
		->setDescription("DOCUMENTO GENERADO LA UNIDAD E INFORMATICA ")
		->setKeywords("R.A.B.- DESARROLLO DE SOFTWARE")
		->setCategory("REPORTE");

      
	  
	  
	 		
//$ListarReporte=ReporteIngreso_emergencia01_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["IdServicio"]);
$ListarReporte=Sigesa_Reporte_Saldos_Almacen_M($_REQUEST["MesReporte1"],$_REQUEST["AnioReporte1"],$_REQUEST["IdAlmacen"],$_REQUEST["Variable"]);

 if($ListarReporte != NULL)	{ 
 $i=2;
 foreach($ListarReporte as $item){
	 
/*
if($item["TriajeTalla"]!=NULL){$TriajeTalla='Tal: '.$item["TriajeTalla"];}
if($item["TriajePeso"]!=NULL){$TriajePeso=' Pe: '.$item["TriajePeso"];}
if($item["TriajePresion"]!=NULL){$TriajePresion=' Pr: '.$item["TriajePresion"];}
if($item["TriajeTemperatura"]!=NULL){$TriajeTemperatura=' Tm: '.$item["TriajeTemperatura"];}
if($item["TriajePulso"]!=NULL){$TriajePulso=' Pul: '.$item["TriajePulso"];}
if($item["TriajeFrecRespiratoria"]!=NULL){$TriajeFrecRespiratoria=' F.Rs: '.$item["TriajeFrecRespiratoria"];}
if($item["TriajeFrecCardiaca"]!=NULL){$TriajeFrecCardiaca=' F.Cr: '.$item["TriajeFrecCardiaca"];}

 	([<<NINGUNO>>],
		[Almacén Logística],
		[Almacen Especializado - SISMED],
		[ALMACEN - CASOS ESPECIALES],
		[Farmacia Central],
		[Farmacia Central Estrategias],
		[Farmacia SOP3],
		[Farmacia SOP3 - Donaciones],
		[Farmacia Emergencia - Sismed],
		[Farmacia Emergencia - Donaciones],
		[Otros Servicios del Hospital],
		[DISA],
		[Farmacia Casos Especiales],
		[Farmacia Casos Especiales Donaciones],
		[Farmacia  Estrategias],
		[Farmacia  Estrategias Ventas],
		[ALMACEN - ESTRATEGIAS],
		[ALMACEN - FARMACIA],
		[Farmacia SOP4],
		[Farmacia SOP4 - Donaciones],
		[Farmacia Hospitalizacion])) 
 
  */
		if($item["Almacén Logística"]==NULL){$item["Almacén Logística"]=0;}
		if($item["Almacen Especializado - SISMED"]==NULL){$item["Almacen Especializado - SISMED"]=0;}
		if($item["ALMACEN - CASOS ESPECIALES"]==NULL){$item["ALMACEN - CASOS ESPECIALES"]=0;}
		if($item["Farmacia Central"]==NULL){$item["Farmacia Central"]=0;}
		if($item["Farmacia Central Estrategias"]==NULL){$item["Farmacia Central Estrategias"]=0;}
		if($item["Farmacia SOP3"]==NULL){$item["Farmacia SOP3"]=0;}
		if($item["Farmacia SOP3 - Donaciones"]==NULL){$item["Farmacia SOP3 - Donaciones"]=0;}
		if($item["Farmacia Emergencia - Sismed"]==NULL){$item["Farmacia Emergencia - Sismed"]=0;}
		if($item["Farmacia Emergencia - Donaciones"]==NULL){$item["Farmacia Emergencia - Donaciones"]=0;}
		if($item["DISA"]==NULL){$item["DISA"]=0;}
		if($item["Farmacia Casos Especiales"]==NULL){$item["Farmacia Casos Especiales"]=0;}
		if($item["Farmacia Casos Especiales Donaciones"]==NULL){$item["Farmacia Casos Especiales Donaciones"]=0;}
		if($item["Farmacia Estrategias"]==NULL){$item["Farmacia Estrategias"]=0;}
		if($item["Farmacia Estrategias Ventas"]==NULL){$item["Farmacia Estrategias Ventas"]=0;}
		if($item["ALMACEN - ESTRATEGIAS"]==NULL){$item["ALMACEN - ESTRATEGIAS"]=0;}
		if($item["ALMACEN - FARMACIA"]==NULL){$item["ALMACEN - FARMACIA"]=0;}
		if($item["Farmacia SOP4"]==NULL){$item["Farmacia SOP4"]=0;}
		if($item["Farmacia SOP4 - Donaciones"]==NULL){$item["Farmacia SOP4 - Donaciones"]=0;}
		if($item["Farmacia Hospitalizacion"]==NULL){$item["Farmacia Hospitalizacion"]=0;}


	 
$objPHPExcel->setActiveSheetIndex(0)
    //        ->setCellValue('A1', 'Descripcion')
            ->setCellValue('A1', 'Tipo')
            ->setCellValue('B1', 'Codigo')
	//		->setCellValue('D1', 'Id Producto')
			->setCellValue('C1', 'Nombre')
			->setCellValue('D1', 'Concentracion')
			->setCellValue('E1', 'Forma Farmaceutica')
			->setCellValue('F1', 'Alm. Logística')
			->setCellValue('G1', 'Alm. SISMED')
			->setCellValue('H1', 'Alm. Casos Esp.')
			->setCellValue('I1', 'Farm. Central')
			->setCellValue('J1', 'Farm. Central Estrategias')
			->setCellValue('K1', 'Farm. SOE')
			->setCellValue('L1', 'Farm. SOP3')
			->setCellValue('M1', 'Farm. SOP3 - Donaciones')
			->setCellValue('N1', 'Farm. Dosis Unitaria')
			->setCellValue('O1', 'Farm. Emerg. - Donaciones')
			->setCellValue('P1', 'Farm. Emerg. - Sismed')
			//->setCellValue('O1', 'Otros Servicios del Hospital')
			//->setCellValue('P1', 'DISA')
			->setCellValue('Q1', 'Farm. Casos Esp.')
			->setCellValue('R1', 'Farm. Casos Esp. Donaciones')
			->setCellValue('S1', 'Farm. Emerg-Donaciones')
			->setCellValue('T1', 'Farm. Emerg-Sismed')
			//->setCellValue('O1', 'Otros Servicios del Hospital')
			->setCellValue('U1', 'Alm. - Estrategias')
			->setCellValue('V1', 'Alm- Farmacia')
			->setCellValue('W1', 'Farm. SOP4')
			->setCellValue('X1', 'Farm. SOP4 - Donaciones')
			->setCellValue('Y1', 'Farm. Hospitalizacion'),
			->setCellValue('Z1', 'Farm. Oxigeno'),

			//->setCellValue('H1', 'HC')
			
			//->setCellValue('A'.$i, $item["descripcion"] )
            ->setCellValue('A'.$i, utf8_encode($item["TipoProducto"]))
			->setCellValue('B'.$i, $item["Codigo"])
//			->setCellValue('D'.$i, $item["idProducto"])
			//->setCellValue('E'.$i, $item["FechaNacimiento"])
			->setCellValue('C'.$i, $item["Nombre"])
			->setCellValue('D'.$i, $item["Concentracion"])
			->setCellValue('E'.$i, $item["FormaFarmaceutica"])
			->setCellValue('F'.$i, $item["Almacén Logística"])
			->setCellValue('G'.$i, $item["Almacen Especializado - SISMED"])
			->setCellValue('H'.$i, $item["ALMACEN - CASOS ESPECIALES"])
			->setCellValue('I'.$i, $item["Farmacia Central"])
			->setCellValue('J'.$i, $item["Farmacia Central Estrategias"])
			->setCellValue('K'.$i, $item["Farmacia SOE"])
			->setCellValue('L'.$i, $item["Farmacia SOP3"])
			->setCellValue('M'.$i, $item["Farmacia SOP3 - Donaciones"])
			->setCellValue('N'.$i, $item["Farmacia Dosis Unitaria"])
			->setCellValue('O'.$i, $item["Farmacia Emergencia - Donaciones"])
			->setCellValue('P'.$i, $item["Farmacia Emergencia - Sismed"])
			->setCellValue('Q'.$i, $item["Farmacia Casos Especiales"])
			->setCellValue('R'.$i, $item["Farmacia Casos Especiales Donaciones"])
			->setCellValue('S'.$i, $item["Farmacia Estrategias"])
			->setCellValue('T'.$i, $item["Farmacia Estrategias Ventas"])
			->setCellValue('U'.$i, $item["ALMACEN - ESTRATEGIAS"])
			->setCellValue('V'.$i, $item["ALMACEN - FARMACIA]"])
			->setCellValue('W'.$i, $item["Farmacia SOP4"])
			->setCellValue('X'.$i, $item["Farmacia SOP4 - Donaciones"])
			->setCellValue('Y'.$i, $item["Farmacia Hospitalizacion"])
			->setCellValue('Z'.$i, $item["Farmacia Oxigeno"])

			
			 ;
$i++;
 }
 }
 	
            ;


 
$objPHPExcel->getActiveSheet()->setTitle('Venta_Diaria');
$objPHPExcel->setActiveSheetIndex(0);



header('Content-Type: application/vnd.ms-excel;charset=UTF-8');
header('Content-Disposition: attachment;filename="FARMACIA_SALDOS_ALMACEN.xls"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;



 
  

 
?>