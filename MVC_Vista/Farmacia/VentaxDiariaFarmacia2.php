<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ordenes para analisis de laboratorio</title>

	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
	<style>
		html,body { 
        	height: 100%;
        	font-family: Helvetica; 

        }
	</style>

	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>

    <script>
    $(document).ready(function() {
    	//CARGANDO LAS CONFIGURACIONES POR DEFECTO
		$('#busqueda_numero_cuenta').textbox('clear').textbox('textbox').focus();
		$('#tabla_servicios').datagrid({
			rowStyler:function(index,row){
				if (row.NUMERO % 2 == 0){
					return 'background-color:#F2F2F2;';
				}
			}
		});

		//BUSCAR POR NUMERO DE CUENTA
		$("#busqueda_numero_cuenta").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numero_cuenta = $("#busqueda_numero_cuenta").textbox('getText');

				$.ajax({
					url: '../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=Medicamentos',
					type: 'POST',
					dataType: 'json',
					data: {
						tipobusuqeda:1,
						dato:numero_cuenta
					},
					success: function(data){
						$("#num_cuenta_d_paciente_mostrar").textbox('setText',data.NOMBRE);
					}
				});
				
			}

		});
/*
		$("#buqueda_numero_historia").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numero_cuenta = $("#buqueda_numero_historia").textbox('getText');
				$.ajax({
					url: '../../MVC_Controlador/Facturacion/FacturacionC.php?acc=EstadoCuentaGeneral',
					type: 'POST',
					dataType: 'json',
					data: {
						tipobusuqeda:2,
						dato:numero_cuenta
					},
					success: function(resultado){
						$('#tabla_resultados_xhc').datagrid('loadData', {"CUENTA":0,"rows":[]});
						$("#tabla_resultados_xhc").datagrid({data:resultado});
					}
				});
				
			}

		});*/
		//FIN
    });

	function cellStyler(value,row,index){
        return 'background-color:#ffee00;color:red;';
    }

     function Exportar_Kardex(Exportar){
      
      var FechaInicio=document.getElementById("FechaInicio").value;
      var FechaFinal=document.getElementById("FechaFinal").value;
      var IdAlmacen=document.getElementById("IdAlmacen").value;
      var CodigoProducto=document.getElementById("busqueda_numero_cuenta").value;

         if (CodigoProducto=="") {
          alert("Ingresar un Codigo");
        return 0;
        }       
    
     if(Exportar=="excel"){
       
       location="../../MVC_Vista/Farmacia/Reporte_Kardex_Excel.php?FechaInicio="+FechaInicio+"&FechaFinal="+FechaFinal+"&IdAlmacen="+IdAlmacen+"&CodigoProducto="+CodigoProducto;
  } } 
    </script>
</head>
<body>
	<div class="easyui-panel"  style="width:100%;height:100%;" title="KARDEX">
		<div class="easyui-layout" data-options="fit:true">
			<!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:true" style="height:100%;">
            	<div class="easyui-layout" data-options="fit:true">
            		<input type="hidden" id="idUsuarioparaSigesaLI" value="<?php echo $_REQUEST['IdEmpleado']?>">
					<!-- CONTENEDOR: BUSQUEDAS -->
            		<div data-options="region:'west'" title="" style="width:100%;height:100%;padding:1%;" >
            			<table>
            				<tr>
            					<td><u><strong>BUSQUEDAS:</strong></u></td>
            					<td></td>
            				</tr>
            				
            				<tr>
            				<td>FECHA INICIO: </td>

            				<td><input name="FechaInicio" type="text" id="FechaInicio" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00:00"   size="10"  autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
            </script>
            </td>
            <TR>
            <td>FECHA FINAL: </td>
            <td>
            <input name="FechaFinal" type="text" id="FechaFinal" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59:59"  size="10"   autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Final"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
            </script>
            </td>
            </TR>
            <tr>
            <td>ALMACEN</td>
            <td>
                  <select  name="IdAlmacen" id="IdAlmacen" >
            <?php 
            if($AlmacenesFarmacia != NULL){ 
            foreach($AlmacenesFarmacia as $item2){?>
            <option value="<?php echo $item2["IDALMACEN"]?>"<?php if($item2["IDALMACEN"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["NOMBRE"]?></option>
            <?php }}?>
        </select>
            	</td>	
            	</tr>		
           	<tr>
            	<td>CODIGO: </td>
            	<td><input type="text" class="easyui-textbox" id="busqueda_numero_cuenta" data-options="prompt:'Codigo CPT'"></td>
            </tr>
            <tr>
            	<td>MEDICAMENTO: </td>
            	<td><input type="text" class="easyui-textbox" id="num_cuenta_d_paciente_mostrar" data-options="prompt:'Medicamento'" style="width:300px;">&nbsp;</td>
            </tr>	

            <tr>
            	<td width="20"><a onclick="Exportar_Kardex('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
            </tr>
            			</table>
            		</div>
            	</div>
            </div>
 
		</div>
	</div>
</body>
</html>