<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
  <link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
  <link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
  <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>
<script   language="javascript">
   
 function Exportar(Exportar){

     var MesReporte1=document.getElementById("MesReporte1").value;
   var AnioReporte1=document.getElementById("AnioReporte1").value;
   var IdAlmacen1=document.getElementById("IdAlmacen1").value;
   var Variable=1;

     if(Exportar=="excel"){
       
      location="../../MVC_Vista/Farmacia/Reporte_Diario_Farmacia_Excel.php?MesReporte1="+MesReporte1+"&AnioReporte1="+AnioReporte1+"&IdAlmacen="+IdAlmacen1+"&Variable="+Variable;
      return 0;
       }
   } 
    
function Exportar_Total(Exportar){

     var FechaInicio2=document.getElementById("FechaInicio2").value;
   var FechaFinal2=document.getElementById("FechaFinal2").value;
   var IdAlmacen2=document.getElementById("IdAlmacen2").value;
   var Variable=2;
   //miPopup = window.open("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Reportes_Exel","Panet_Datos","width=700,height=80,scrollbars=yes");
     //window.locationf="../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Reportes_Exel";
   if (document.formElem.FechaInicio2.value.length==0&&document.formElem.FechaFinal2.value.length==0){
    alert("Ingresar Fechas");
    document.formElem.FechaInicio.focus();
    return 0;
   }else{
   
    
     if(Exportar=="excel"){
       
      location="../../MVC_Vista/Farmacia/Reporte_Ventas_Dia_Excel.php?FechaInicio="+FechaInicio2+"&FechaFinal="+FechaFinal2+"&IdAlmacen="+IdAlmacen2+"&Variable="+Variable;
      return 0;
       }
   
  } } 

 function Exportar_Narcoticos(Exportar){

     var MesReporte3=document.getElementById("MesReporte3").value;
   var AnioReporte3=document.getElementById("AnioReporte3").value;
   var IdAlmacen3=document.getElementById("IdAlmacen3").value;
   var Variable=3;
     
     if(Exportar=="excel"){
       
      location="../../MVC_Vista/Farmacia/Reporte_Diario_Farmacia_Excel.php?MesReporte1="+MesReporte3+"&AnioReporte1="+AnioReporte3+"&IdAlmacen="+IdAlmacen3+"&Variable="+Variable;
      return 0;
       }
      } 

    function Exportar_Total_Narcoticos(Exportar){

     var FechaInicio4=document.getElementById("FechaInicio4").value;
   var FechaFinal4=document.getElementById("FechaFinal4").value;
   var IdAlmacen4=document.getElementById("IdAlmacen4").value;
   var Variable=4;
   //miPopup = window.open("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Reportes_Exel","Panet_Datos","width=700,height=80,scrollbars=yes");
     //window.locationf="../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Reportes_Exel";
   if (document.formElem.FechaInicio2.value.length==0&&document.formElem.FechaFinal2.value.length==0){
    alert("Ingresar Fechas");
    document.formElem.FechaInicio.focus();
    return 0;
   }else{
   
     if(Exportar=="excel"){
       
      location="../../MVC_Vista/Farmacia/Reporte_Ventas_Dia_Excel.php?FechaInicio="+FechaInicio4+"&FechaFinal="+FechaFinal4+"&IdAlmacen="+IdAlmacen4+"&Variable="+Variable;
      return 0;
       }
      }} 

     function Exportar_IngresosSalidasFarmacia(Exportar){
      
      var FechaInicio5=document.getElementById("FechaInicio5").value;
      var FechaFinal5=document.getElementById("FechaFinal5").value;
      var Variable=5;
     if(Exportar=="excel"){
       
       location="../../MVC_Vista/Farmacia/Reporte_IngresoSalidoFarmacia_Excel.php?FechaInicio="+FechaInicio5+"&FechaFinal="+FechaFinal5+"&Variable="+Variable;
      return 0;  
  } } 


     function Exportar_IngresosSalidasFarmaciaEspecial(Exportar){
      
      var FechaInicio6=document.getElementById("FechaInicio6").value;
      var FechaFinal6=document.getElementById("FechaFinal6").value;
      var Variable=6;
     if(Exportar=="excel"){
       
       location="../../MVC_Vista/Farmacia/Reporte_IngresoSalidoFarmacia_Excel.php?FechaInicio="+FechaInicio6+"&FechaFinal="+FechaFinal6+"&Variable="+Variable;
      return 0;  
  } } 
  
    function Exportar_SaldosXAlmacen(Exportar){
      
      var IdAlmacen7=document.getElementById("IdAlmacen7").value;
      var Variable=7;
     if(Exportar=="excel"){
        location="../../MVC_Vista/Farmacia/Reporte_SaldosXAlmacen_Excel.php?IdAlmacen="+IdAlmacen7+"&Variable="+Variable;
      return 0;  
  } } 

    function Exportar_Ici(Exportar){
      
      var FechaInicio8=document.getElementById("FechaInicio8").value;
      var FechaFinal8=document.getElementById("FechaFinal8").value;
      var IdAlmacen8=document.getElementById("IdAlmacen8").value;
      var Variable=8;
     if(Exportar=="excel"){
       
       location="../../MVC_Vista/Farmacia/Reporte_Ici_Excel1.php?FechaInicio="+FechaInicio8+"&FechaFinal="+FechaFinal8+"&IdAlmacen="+IdAlmacen8+"&Variable="+Variable;
  } } 
 function Exportar_Saldos_Almacenes(Exportar){
      
     if(Exportar=="excel"){
       
       location="../../MVC_Vista/Farmacia/Reporte_Saldos_Almacenes_Excel.php";
  } } 

function Exportar_CosumoPorServciios(Exportar){

  /* var MesReporte3=document.getElementById("MesReporte3").value;
   var AnioReporte3=document.getElementById("AnioReporte3").value;
   var IdAlmacen3=document.getElementById("IdAlmacen3").value;*/


   

   var TipoServicioFarmacia = $("#tiposserviciofarmacia").combobox('getValue');
   var ServiciosFarmacia = $("#serviciosfarmacia").combobox('getValue');
   var FechaInicio=document.getElementById("FechaInicioFar1").value;
   var FechaFinal=document.getElementById("FechaFinalFar1").value;
	 var AlmacenFarmacia=$("#almacenfarmacia").combobox('getValue');
    
    if (TipoServicioFarmacia=='') {
            alert("Ingrese Tipo de Servicio");
            return 0;
          }
    if (ServiciosFarmacia=='') {
            ServiciosFarmacia=0;           
          }
    if (AlmacenFarmacia=='') {
           AlmacenFarmacia=0;           
      }
		 
   var Variable=3;
     if(Exportar=="pdf"){
      location="../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=ReporteConsumoxServicioFarmacia&FechaInicio="+FechaInicio+"&FechaFinal="+FechaFinal+'&TiposServicioFarmacia='+TipoServicioFarmacia+'&ServiciosFarmacia='+ServiciosFarmacia+"&AlmacenFarmacia="+AlmacenFarmacia;
      return 0;
       }
    
       }
	
function Exportar_IndicadorDosisUnitaria(Exportar){
   var TipoServicioFarmacia = $("#tiposserviciofarmaciaIn").combobox('getValue');
   var ServiciosFarmacia = $("#serviciosfarmaciaIn").combobox('getValue');
   var FechaInicio=document.getElementById("FechaInicioFar1In").value;
   var FechaFinal=document.getElementById("FechaFinalFar1In").value;
   var AlmacenFarmacia=$("#almacenfarmaciaIn").combobox('getValue');
    
    if (TipoServicioFarmacia=='') {
            alert("Ingrese Tipo de Servicio");
            return 0;
          }
    if (ServiciosFarmacia=='') {
            ServiciosFarmacia=0;           
          }
    if (AlmacenFarmacia=='') {
           AlmacenFarmacia=0;           
      }    
	
	if(Exportar=="excel"){	
		
	  location="../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=ReporteIndicadorDosisUnitaria&FechaInicio="+FechaInicio+"&FechaFinal="+FechaFinal+'&TiposServicioFarmacia='+TipoServicioFarmacia+'&ServiciosFarmacia='+ServiciosFarmacia+"&AlmacenFarmacia="+AlmacenFarmacia;return 0;   
	  
    }	
	
}

function Exportar_SaldosAlmacenLogistica(Exportar){  
   var idTipoConcepto = $("#idTipoConcepto").combobox('getValue');
   var FechaInicio=document.getElementById("FechaInicioMov").value;
   var FechaFinal=document.getElementById("FechaFinalMov").value;     
  
  if(Exportar=="excel"){  
    
    location="../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=ReporteSaldosAlmacenLogistica&FechaInicio="+FechaInicio+"&FechaFinal="+FechaFinal+'&idTipoConcepto='+idTipoConcepto;return 0;   
    
    } 
}
    
</script>
 
</head>
<body>

<form id="formElem" name="formElem" method="post" action="">
  <table width="982" border="1" cellpadding="1" cellspacing="1">
    <tr>
      <th>    
      <th>&nbsp;</th>
      <th  >&nbsp;</th>
      <th colspan="3">&nbsp;</th>
    </tr>
    <tr bgcolor="#99CCFF">
      <th width="9">Nº</td>
      <th width="199">Reporte</th>
      <th width="670"  >Opciones </th>
      <th colspan="3">Formatos</th>
    </tr>
    <tr>
      <td>1</td>
      <td><strong>VENTAS POR MES</strong></td>
      <td>
            <select name="MesReporte1" id="MesReporte1" >
              <option value='01'>Enero</option>
                <option value='02'>Febrero</option>
                <option value='03'>Marzo</option>
                <option value='04'>Abril</option>
                <option value='05'>Mayo</option>
                <option value='06'>Junio</option>
                <option value='07'>Julio</option>
                <option value='08'>Agosto</option>
                <option value='09'>Septiembre</option>
                <option value='10'>Octubre</option>
                <option value='11'>Noviembre</option>
                <option value='12'>Diciembre</option>
        </select>
            <select name="AnioReporte1" id="AnioReporte1" >
              <?php 
            if($AniosFarmacia != NULL){ 
            foreach($AniosFarmacia as $item2){?>
            <option value="<?php echo $item2["Anio"]?>"<?php if($item2["Anio"]==$Anio){?>selected<?php }?>><?php echo $item2["Anio"]?></option>
            <?php }}?>
        </select>
                  <select name="IdAlmacen1" id="IdAlmacen1" >
            <?php 
            if($AlmacenesFarmacia != NULL){ 
            foreach($AlmacenesFarmacia as $item2){?>
            <option value="<?php echo $item2["IDALMACEN"]?>"<?php if($item2["IDALMACEN"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["NOMBRE"]?></option>
            <?php }}?>
        </select>

                
       
                                  </td><td width="32">&nbsp;</td>
      <td width="20"><a onclick=" Exportar('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>
    <tr>
      <td>2</td>
      <td><strong>VENTAS TOTAL DIA</strong></td>
      <td><input name="FechaInicio2" type="text" class="texto" id="FechaInicio2" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00:00"   size="10"  autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio2",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
            </script>
            
            <input name="FechaFinal2" type="text" class="texto" id="FechaFinal2" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59:59"  size="10"   autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Final"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal2",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
            </script>
                  <select name="IdAlmacen2" id="IdAlmacen2" >
            <?php 
            if($AlmacenesFarmacia != NULL){ 
            foreach($AlmacenesFarmacia as $item2){?>
            <option value="<?php echo $item2["IDALMACEN"]?>"<?php if($item2["IDALMACEN"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["NOMBRE"]?></option>
            <?php }}?>
        </select>

                
       
                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_Total('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>
<tr>
      <td>3</td>
      <td><strong>VENTAS POR MES NARCOTICOS</strong></td>
      <td>
            <select name="MesReporte3" id="MesReporte3" >
              <option value='01'>Enero</option>
                <option value='02'>Febrero</option>
                <option value='03'>Marzo</option>
                <option value='04'>Abril</option>
                <option value='05'>Mayo</option>
                <option value='06'>Junio</option>
                <option value='07'>Julio</option>
                <option value='08'>Agosto</option>
                <option value='09'>Septiembre</option>
                <option value='10'>Octubre</option>
                <option value='11'>Noviembre</option>
                <option value='12'>Diciembre</option>
        </select>
            <select name="AnioReporte3" id="AnioReporte3" >
              <?php 
            if($AniosFarmacia != NULL){ 
            foreach($AniosFarmacia as $item2){?>
            <option value="<?php echo $item2["Anio"]?>"<?php if($item2["Anio"]==$Anio){?>selected<?php }?>><?php echo $item2["Anio"]?></option>
            <?php }}?>
        </select>
                  <select name="IdAlmacen3" id="IdAlmacen3" >
            <?php 
            if($AlmacenesFarmacia != NULL){ 
            foreach($AlmacenesFarmacia as $item2){?>
            <option value="<?php echo $item2["IDALMACEN"]?>"<?php if($item2["IDALMACEN"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["NOMBRE"]?></option>
            <?php }}?>
        </select>

                
       
                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_Narcoticos('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>
 <tr>
      <td>4</td>
      <td><strong>VENTAS TOTAL NARCOTICOS</strong></td>
      <td><input name="FechaInicio4" type="text" class="texto" id="FechaInicio4" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00:00"   size="10"  autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador3" width="16" height="15" border="0" id="lanzador3" title="Fecha Inicio"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio4",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador3"     // el id del botón que lanzará el calendario 
}); 
            </script>
            
            <input name="FechaFinal4" type="text" class="texto" id="FechaFinal4" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59:59"  size="10"   autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador4" width="16" height="15" border="0" id="lanzador4" title="Fecha Final"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal4",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador4"     // el id del botón que lanzará el calendario 
}); 
            </script>
                  <select name="IdAlmacen4" id="IdAlmacen4" >
            <?php 
            if($AlmacenesFarmacia != NULL){ 
            foreach($AlmacenesFarmacia as $item2){?>
            <option value="<?php echo $item2["IDALMACEN"]?>"<?php if($item2["IDALMACEN"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["NOMBRE"]?></option>
            <?php }}?>
        </select>

                
       
                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_Total_Narcoticos('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>
     <tr>
      <td>5</td>
      <td><strong>INGRESOS Y SALIDAS FARMACIA</strong></td>
      <td><input name="FechaInicio5" type="text" class="texto" id="FechaInicio5" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00:00"   size="10"  autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador5" width="16" height="15" border="0" id="lanzador5" title="Fecha Inicio"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio5",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador5"     // el id del botón que lanzará el calendario 
}); 
            </script>
            
            <input name="FechaFinal5" type="text" class="texto" id="FechaFinal5" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59:59"  size="10"   autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador6" width="16" height="15" border="0" id="lanzador6" title="Fecha Final"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal5",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador6"     // el id del botón que lanzará el calendario 
}); 
            </script>

                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_IngresosSalidasFarmacia('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>

    <tr>
      <td>6</td>
      <td><strong>INGRESOS Y SALIDAS FARMACIA ESPECIAL</strong></td>
      <td><input name="FechaInicio6" type="text" class="texto" id="FechaInicio6" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00:00"   size="10"  autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador7" width="16" height="15" border="0" id="lanzador7" title="Fecha Inicio"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio6",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador7"     // el id del botón que lanzará el calendario 
}); 
            </script>
            
            <input name="FechaFinal6" type="text" class="texto" id="FechaFinal6" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59:59"  size="10"   autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador8" width="16" height="15" border="0" id="lanzador8" title="Fecha Final"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal6",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador8"     // el id del botón que lanzará el calendario 
}); 
            </script>

                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_IngresosSalidasFarmaciaEspecial('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>

<tr>
      <td>7</td>
      <td><strong>SALDOS POR ALMACEN</strong></td>
      <td><select name="IdAlmacen7" id="IdAlmacen7" >
            <?php 
            if($AlmacenesFarmacia != NULL){ 
            foreach($AlmacenesFarmacia as $item2){?>
            <option value="<?php echo $item2["IDALMACEN"]?>"<?php if($item2["IDALMACEN"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["NOMBRE"]?></option>
            <?php }}?>
        </select>

                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_SaldosXAlmacen('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>


<tr>
      <td>8</td>
      <td><strong>FORMATO ICI</strong></td>
      <td><input name="FechaInicio8" type="text" class="texto" id="FechaInicio8" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00:00"   size="10"  autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador9" width="16" height="15" border="0" id="lanzador9" title="Fecha Inicio"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaInicio8",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 00:00:00",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador9"     // el id del botón que lanzará el calendario 
}); 
            </script>
            
            <input name="FechaFinal8" type="text" class="texto" id="FechaFinal8" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59:59"  size="10"   autocomplete=OFF  />
                                  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador10" width="16" height="15" border="0" id="lanzador10" title="Fecha Final"  /><script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "FechaFinal8",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y 23:59:59",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador10"     // el id del botón que lanzará el calendario 
}); 
            </script>

                                  
      <select name="IdAlmacen8" id="IdAlmacen8" >
            <?php 
            if($AlmacenesFarmacia != NULL){ 
            foreach($AlmacenesFarmacia as $item2){?>
            <option value="<?php echo $item2["IDALMACEN"]?>"<?php if($item2["IDALMACEN"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["NOMBRE"]?></option>
            <?php }}?>
        </select>

                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_Ici('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>

<tr>
      <td>9</td>
      <td><strong>SALDOS ALMACENES</strong></td>
     <td></td>

                                  </td><td>&nbsp;</td>
      <td width="20"><a onclick="Exportar_Saldos_Almacenes('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
      <td width="19"><?php /*?><img src="../../MVC_Complemento/img/DOC.png" alt="" width="24" height="24"  /><?php */?> </td>
    </tr>
	
	<tr>
   <td><strong>10</strong></td>
   <td><strong>Consumo por Servicios- Emergencia / Hospitalización </strong></td>
   <td>
                    
                      <input id="tiposserviciofarmacia" class="easyui-combobox" data-options="prompt:'DEPARTAMENTO',
                          valueField: 'id',
                          textField: 'text',
                          url: '../../MVC_Controlador/Farmacia/Farmaciac.php?acc=MostrarTiposServiciosCombo',
                          onSelect: function(rec){
                              var url = '../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=MostrarServiciosCombo&idDepartamento='+rec.id;
                              $('#serviciosfarmacia').combobox('reload', url);
                          }">
                    

                    
                    
                      <input id="serviciosfarmacia" class="easyui-combobox" data-options="prompt:'SERVICIO',
                      valueField: 'id',
                          textField: 'text',
                          
                      ">
        
        
        

        <input id="almacenfarmacia" class="easyui-combobox" data-options="prompt:'FARMACIA',
                      valueField: 'id',
                          textField: 'text',
                           url: '../../MVC_Controlador/Farmacia/Farmaciac.php?acc=MostrarFarmaciasCombo'
                      ">



        <input name="FechaInicioFar1" type="text" class="texto" id="FechaInicioFar1" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00"   size="20"  autocomplete=OFF  />                                         
        <input name="FechaFinalFar1" type="text" class="texto" id="FechaFinalFar1" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59"  size="20"   autocomplete=OFF  />
                          
            
            
        </td>
   <td>&nbsp;</td>
   <td><a onclick="Exportar_CosumoPorServciios('pdf')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
   <td>
   
   
   </td>
 </tr>
	
	<tr>
   <td><strong>11</strong></td>
   <td><strong>Indicador de Dosis Unitaria- Emergencia / Hospitalización </strong></td>
   <td>
                    
                      <input id="tiposserviciofarmaciaIn" class="easyui-combobox" data-options="prompt:'DEPARTAMENTO',
                          valueField: 'id',
                          textField: 'text',
                          url: '../../MVC_Controlador/Farmacia/Farmaciac.php?acc=MostrarTiposServiciosCombo',
                          onSelect: function(rec){
                              var url = '../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=MostrarServiciosCombo&idDepartamento='+rec.id;
                              $('#serviciosfarmaciaIn').combobox('reload', url);
                          }">
                    

                    
                    
                      <input id="serviciosfarmaciaIn" class="easyui-combobox" data-options="prompt:'SERVICIO',
                      valueField: 'id',
                          textField: 'text',
                          
                      ">
        
        
        

        <input id="almacenfarmaciaIn" class="easyui-combobox" data-options="prompt:'FARMACIA',
                      valueField: 'id',
                          textField: 'text',
                           url: '../../MVC_Controlador/Farmacia/Farmaciac.php?acc=MostrarFarmaciasCombo'
                      ">



        <input name="FechaInicioFar1In" type="text" class="texto" id="FechaInicioFar1In" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00"   size="20"  autocomplete=OFF  />                                         
        <input name="FechaFinalFar1In" type="text" class="texto" id="FechaFinalFar1In" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59"  size="20"   autocomplete=OFF  />
                          
            
            
        </td>
   <td></td>
   <td><a onclick="Exportar_IndicadorDosisUnitaria('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
   <td>
   
   
   </td>
 </tr>
 
  <tr>
   <td><strong>12</strong></td>
   <td><strong>Devolución Saldos Almacen Logistica </strong></td>
   <td>  

       <select name="idTipoConcepto" id="idTipoConcepto" class="easyui-combobox" style="width: 145px;">
          <option value="0">TODOS</option>
          <?php 
          $farmTipoConceptos=farmTipoConceptosM('DEVOLUCION X');          
          if($farmTipoConceptos != NULL){ 
            foreach($farmTipoConceptos as $item){?>  
         
          ?>  
            <option value="<?php echo $item["idTipoConcepto"]?>"><?php echo $item["Concepto"]?></option>

          <?php }} ?>
       </select>    

       <input name="FechaInicioMov" type="text" class="texto" id="FechaInicioMov" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00"   size="20"  autocomplete=OFF  />                                         
        <input name="FechaFinalMov" type="text" class="texto" id="FechaFinalMov" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59"  size="20"   autocomplete=OFF  />
                          
            
            
        </td>
   <td></td>
   <td><a onclick="Exportar_SaldosAlmacenLogistica('excel')" ><img src="../../MVC_Complemento/img/Excel.png" alt="" width="24" height="24" /></a></td>
   <td>
   
   
   </td>
 </tr>
  
  </table>
</form>


</body>
</html>