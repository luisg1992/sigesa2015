<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
        <title>Informe Operacional</title>
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">    
        <link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">         
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
<style>
        html,body { 
            padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 	       
        }      
        table{
            border-collapse:collapse;
        }
    </style> 
<script type="text/javascript">
    $(document).ready(function() {



		$("input[type=text]").keyup(function(){
		  $(this).val( $(this).val().toUpperCase() );
		});



        $('#pestanas').tabs({
                pill : true
        });

        

        $('#nuevaespecialidad').click(function(e){                
                $.messager.prompt('Nueva Especialidad', 'Ingrese nueva especialidad:', function(r){
                if (r){
                	var piso =  $("#sala_informe_operacional").combobox('getValue');
                	$.ajax({
                		url: '../../MVC_Controlador/Farmacia/CarC.php?acc=IngresarEspecialidad',
                		type: 'POST',
                		dataType: 'json',
                		data: {
                			nombreEspecialidad: r,
                			piso: piso
                		},
                		success: function(data)
                		{
                			if (data == 'M_1') {
                				$.messager.alert('SIGESA','Registro Generado');
                				$("#especialidad_informe_operacional").combobox('reload', '../../MVC_Controlador/Farmacia/CarC.php?acc=ListarEspecialidadesCombo&piso='+piso);
                				return false;
                			}
                			if (data == 'M_0') {
                				$.messager.alert('SIGESA','ERROR!!. Comuniquese con el administrador.','warning');
                				return false;
                			}
                		}
                	});
                	
                } 
                else{
                	$.messager.alert('SIGESA','Debe ingresar un nombre para la nueva Especialidad','error');
                }              
            });        
        });  

        //INGRESAR 
        $("#Ingresar_reporte_operaciones_programadas").click(function(event) {
        	var piso_operaciones_programadas = $("#piso_operaciones_programadas").combobox('getValue');
			var mes_operaciones_programadas = $("#mes_operaciones_programadas").combobox('getValue');
			var anio_operaciones_programadas = $("#anio_operaciones_programadas").combobox('getValue');
			var ope_programadas = $("#ope_programadas").numberbox('getValue');
			var ope_suspendidas = $("#ope_suspendidas").numberbox('getValue');
			var ope_programadas_realizadas = $("#ope_programadas_realizadas").numberbox('getValue');
			var cobertura_programadas = $("#cobertura_programadas").numberbox('getValue');
			var ope_aniadidas = $("#ope_aniadidas").numberbox('getValue');
			var ope_reoperadas = $("#ope_reoperadas").numberbox('getValue');
			var total_ope_realizadas = $("#total_ope_realizadas").numberbox('getValue');
			var horas_ofer_sala = $("#horas_ofer_sala").numberbox('getValue');
			var horas_ope_programadas = $("#horas_ope_programadas").numberbox('getValue');
			var horas_operatorias = $("#horas_operatorias").numberbox('getValue');
			var horas_anestesicas = $("#horas_anestesicas").numberbox('getValue');
			var horas_efectivas = $("#horas_efectivas").numberbox('getValue');
			
            $.ajax({
                url: '../../MVC_Controlador/Farmacia/CarC.php?acc=ingOperacionesProgramadas',
                type: 'POST',
                dataType: 'json',
                data: 
                {
                    OpeProgramadas: ope_programadas,
                    OpeSuspendidas: ope_suspendidas,
                    OpeProgRealizadas: ope_programadas_realizadas,
                    CoberturaProgramada: cobertura_programadas,
                    Aniadidas: ope_aniadidas,
                    Reoperados: ope_reoperadas,
                    TOR: total_ope_realizadas,
                    HorasOferSala: horas_ofer_sala,
                    HorasOperProgra: horas_ope_programadas,
                    HorasOpera: horas_operatorias,
                    HorasAnestesicas: horas_anestesicas,
                    HorasEfectivas: horas_efectivas,
                    NroMes: mes_operaciones_programadas,
                    anio: anio_operaciones_programadas,
                    Piso: piso_operaciones_programadas
                },
                success: function(data)
                {
                    if (data == 'M_1') {
                        $.messager.alert('SIGESA','Registro Generado');
                        $("#ope_programadas").numberbox('clear');
                        $("#ope_suspendidas").numberbox('clear');
                        $("#ope_programadas_realizadas").numberbox('clear');
                        $("#cobertura_programadas").numberbox('clear');
                        $("#ope_aniadidas").numberbox('clear');
                        $("#ope_reoperadas").numberbox('clear');
                        $("#total_ope_realizadas").numberbox('clear');
                        $("#horas_ofer_sala").numberbox('clear');
                        $("#horas_ope_programadas").numberbox('clear');
                        $("#horas_operatorias").numberbox('clear');
                        $("#horas_anestesicas").numberbox('clear');
                        $("#horas_efectivas").numberbox('clear');
                        return false;
                    }
                    if (data == 'M_0') {
                        $.messager.alert('SIGESA','ERROR!!. Comuniquese con el administrador.','warning');
                        return false;
                    }
                }
            });
            return false;

		});

        $("#IngresarInformeOperacional").click(function(event) {
        	var mes_informe_operacional  = $("#mes_informe_operacional").combobox('getValue');
			var anio_informa_operacional  = $("#anio_informa_operacional").combobox('getValue');
			var especialidad_informe_operacional  = $("#especialidad_informe_operacional").combobox('getValue');
			var diagnostico_informe_operacional  = $("#diagnostico_informe_operacional").textbox('getText');
			var procedimiento_informe_operacional  = $("#procedimiento_informe_operacional").textbox('getText');
			var tiempo_informe_o  = $("#tiempo_informe_o").textbox('getText');
			var numero_informe_operacional  = $("#numero_informe_operacional").textbox('getText');

			$.ajax({
				url: '../../MVC_Controlador/Farmacia/CarC.php?acc=IngresarNuevoInformeOperacional',
               	type: 'POST',
                dataType: 'json',
                data:
                {
                	diagnostico: diagnostico_informe_operacional,
					procedimiento: procedimiento_informe_operacional,
					tiempo_promedio: tiempo_informe_o,
					numero_cirugias: numero_informe_operacional,
					idEspecialidad: especialidad_informe_operacional,
					mes: mes_informe_operacional,
					anio: anio_informa_operacional
                },
                success: function(data)
                {
                	if (data == 'M_1') {
                		$.messager.alert('SIGESA','Registro Generado');
                		$('#tabla_registro_io').datagrid('loadData', {"DIAGNOSTICO":' ',"rows":[]});
                		$("#tabla_registro_io").datagrid({url: '../../MVC_Controlador/Farmacia/CarC.php?acc=TablaInfrOperacional&IdEspecialidad='+especialidad_informe_operacional+'&mes='+mes_informe_operacional+'&anio='+anio_informa_operacional});
                		return false;
                	}
                	if (data == 'M_0') {
                		$.messager.alert('SIGESA','ERROR!!. Comuniquese con el administrador.','warning');
                		return false;
                	}
                }
			});
			
        });

        //TIPO ANESTESIA
        $("#gen_balanceada_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                SumarAnestesia();
                $('#gen_inhalatoria_anestesia').textbox('textbox').focus();                
                return false;
            }

        });
        $("#gen_inhalatoria_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                SumarAnestesia();
                $('#gen_endovenosa_anestesia').textbox('textbox').focus();
                return false;
            }

        });
        $("#gen_endovenosa_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                SumarAnestesia();
                $('#per_continua_anestesia').textbox('textbox').focus();
                return false;
            }

        });
        $("#per_continua_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                SumarAnestesia();
                $('#per_simple_anestesia').textbox('textbox').focus();
                return false;
            }

        });
        $("#per_simple_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                SumarAnestesia();
                $('#raquidea_anestesia').textbox('textbox').focus();
                return false;
            }

        });
        $("#raquidea_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                SumarAnestesia();
                $('#local_anestesia').textbox('textbox').focus();
                return false;
            }

        });
        $("#local_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                SumarAnestesia();
                $('#sedacion_anestesia').textbox('textbox').focus();
                return false;
            }

        });con_uno_atenciones
        $("#sedacion_anestesia").textbox('textbox').bind('keydown', function(e){
            if (e.keyCode == 9){
                 SumarAnestesia();
                
                return false;
            }

        });

        //sumar anestesia
        function SumarAnestesia()
        {
            var gen_balanceada_anestesia = $("#gen_balanceada_anestesia").textbox('getText');
            var gen_inhalatoria_anestesia = $("#gen_inhalatoria_anestesia").textbox('getText');
            var gen_endovenosa_anestesia = $("#gen_endovenosa_anestesia").textbox('getText');
            var per_continua_anestesia = $("#per_continua_anestesia").textbox('getText');
            var per_simple_anestesia = $("#per_simple_anestesia").textbox('getText');
            var raquidea_anestesia = $("#raquidea_anestesia").textbox('getText');
            var local_anestesia = $("#local_anestesia").textbox('getText');
            var sedacion_anestesia = $("#sedacion_anestesia").textbox('getText');
            var total = $("#total_anestesia").text();
            var nuevo_total = (parseFloat(gen_balanceada_anestesia) || 0 ) + (parseFloat(gen_inhalatoria_anestesia) || 0 ) + (parseFloat(gen_endovenosa_anestesia) || 0 ) + (parseFloat(per_continua_anestesia) || 0 ) + (parseFloat(raquidea_anestesia) || 0 ) + (parseFloat(local_anestesia) || 0 ) + (parseFloat(sedacion_anestesia) || 0 ) + (parseFloat(per_simple_anestesia) || 0);
            $("#total_anestesia").text(nuevo_total);
            // PORCENTAJES
            var porc_gen_balanceada_anestesia = ((parseFloat(gen_balanceada_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));
            var porc_gen_inhalatoria_anestesia = ((parseFloat(gen_inhalatoria_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));
            var porc_gen_endovenosa_anestesia = ((parseFloat(gen_endovenosa_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));
            var porc_per_continua_anestesia = ((parseFloat(per_continua_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));
            var porc_per_simple_anestesia = ((parseFloat(per_simple_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));
            var porc_raquidea_anestesia = ((parseFloat(raquidea_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));
            var porc_local_anestesia = ((parseFloat(local_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));
            var porc_sedacion_anestesia = ((parseFloat(sedacion_anestesia) || 0 ) * 100) / (parseFloat(nuevo_total));

            $("#porc_gen_balanceada_anestesia").text((porc_gen_balanceada_anestesia.toFixed(1))+'%');
            $("#porc_gen_inhalatoria_anestesia").text((porc_gen_inhalatoria_anestesia.toFixed(1))+'%');
            $("#porc_gen_endovenosa_anestesia").text((porc_gen_endovenosa_anestesia.toFixed(1))+'%');
            $("#porc_per_continua_anestesia").text((porc_per_continua_anestesia.toFixed(1))+'%');
            $("#porc_per_simple_anestesia").text((porc_per_simple_anestesia.toFixed(1))+'%');
            $("#porc_raquidea_anestesia").text((porc_raquidea_anestesia.toFixed(1))+'%');
            $("#porc_local_anestesia").text((porc_local_anestesia.toFixed(1))+'%');
            $("#porc_sedacion_anestesia").text((porc_sedacion_anestesia.toFixed(1))+'%');
        }
        

        //INSERTAR ANESTESIA
        $("#agregar_anestesia").click(function(event) {

            var piso_anestesia = $("#piso_anestesia").combobox('getValue');
            var mes_anestesia = $("#mes_anestesia").combobox('getValue');
            var anio_anestesia = $("#anio_anestesia").combobox('getValue');
            var total =  $("#total_anestesia").text();
            var gen_balanceada_anestesia = $("#gen_balanceada_anestesia").numberbox('getValue');
            var porc_gen_balanceada_anestesia = $("#porc_gen_balanceada_anestesia").text();
            var gen_inhalatoria_anestesia = $("#gen_inhalatoria_anestesia").numberbox('getValue');
            var porc_gen_inhalatoria_anestesia = $("#porc_gen_inhalatoria_anestesia").text();
            var gen_endovenosa_anestesia = $("#gen_endovenosa_anestesia").numberbox('getValue');
            var porc_gen_endovenosa_anestesia = $("#porc_gen_endovenosa_anestesia").text();
            var per_continua_anestesia = $("#per_continua_anestesia").numberbox('getValue');
            var porc_per_continua_anestesia = $("#porc_per_continua_anestesia").text();
            var per_simple_anestesia = $("#per_simple_anestesia").numberbox('getValue');
            var porc_per_simple_anestesia = $("#porc_per_simple_anestesia").text();
            var raquidea_anestesia = $("#raquidea_anestesia").numberbox('getValue');
            var porc_raquidea_anestesia = $("#porc_raquidea_anestesia").text();
            var local_anestesia = $("#local_anestesia").numberbox('getValue');
            var porc_local_anestesia = $("#porc_local_anestesia").text();
            var sedacion_anestesia = $("#sedacion_anestesia").numberbox('getValue');
            var porc_sedacion_anestesia = $("#porc_sedacion_anestesia").text();

            $.ajax({
                url: '../../MVC_Controlador/Farmacia/CarC.php?acc=InsertarTIpoAnestesiaMes',
                type: 'POST',
                dataType: 'json',
                data: {
                    AgeneralBalanceada: gen_balanceada_anestesia,
                    AgeneralInhalatoria: gen_inhalatoria_anestesia,
                    AgeneralEndovenosa: gen_endovenosa_anestesia,
                    AperiduralContinua: per_continua_anestesia,
                    AperiduralSimple: per_simple_anestesia,
                    Araquidea: raquidea_anestesia,
                    Alocal: local_anestesia,
                    sedacion: sedacion_anestesia,
                    total: total,
                    NroMes: mes_anestesia,
                    Anio: anio_anestesia,
                    Piso: piso_anestesia,
                    porc_AgeneralBalanceada: porc_gen_balanceada_anestesia,
                    porc_AgeneralInhalatoria: porc_gen_inhalatoria_anestesia,
                    porc_AgeneralEndovenosa: porc_gen_endovenosa_anestesia,
                    porc_AperdurialContinua: porc_per_continua_anestesia,
                    porc_AperdurialSimple: porc_per_simple_anestesia,
                    porc_Araquidea: porc_raquidea_anestesia,
                    porc_Alocal: porc_local_anestesia,
                    porc_Sedacion: porc_sedacion_anestesia
                },
                success: function(data)
                {
                    if (data == 'M_1') {
                        $.messager.alert('SIGESA','Registro Generado');
                        $("#porc_gen_balanceada_anestesia").text(' ');
                        $("#porc_gen_inhalatoria_anestesia").text(' ');
                        $("#porc_gen_endovenosa_anestesia").text(' ');
                        $("#porc_per_continua_anestesia").text(' ');
                        $("#porc_per_simple_anestesia").text(' ');
                        $("#porc_raquidea_anestesia").text(' ');
                        $("#porc_local_anestesia").text(' ');
                        $("#porc_sedacion_anestesia").text(' ');
                        $("#gen_balanceada_anestesia").textbox('clear');
                        $("#gen_inhalatoria_anestesia").textbox('clear');
                        $("#gen_endovenosa_anestesia").textbox('clear');
                        $("#per_continua_anestesia").textbox('clear');
                        $("#per_simple_anestesia").textbox('clear');
                        $("#raquidea_anestesia").textbox('clear');
                        $("#local_anestesia").textbox('clear');
                        $("#sedacion_anestesia").textbox('clear');
                        $("#total_anestesia").text('0');
                        return false;
                    }
                    if (data == 'M_0') {
                        $.messager.alert('SIGESA','ERROR!!. Comuniquese con el administrador.','warning');
                        return false;
                    }
                }
            });
            

            return false;
        });
        
        //PROCEDIMIENTOS INVASIVOS
        $("#IngresarApoyoProcedimientosInvasivos").click(function(event) {
            var sala_operaciones_procedimientos_invasivos = $("#sala_operaciones_procedimientos_invasivos").combobox('getValue');
            var mes_procedimientos_invasivos = $("#mes_procedimientos_invasivos").combobox('getValue');
            var anio_procedimientos_invasivos = $("#anio_procedimientos_invasivos").combobox('getValue');
            var appi_programadas_procedimientos_invasivos = $("#appi_programadas_procedimientos_invasivos").numberbox('getValue');
            var appi_realizadas_procedimientos_invasivos = $("#appi_realizadas_procedimientos_invasivos").numberbox('getValue');
            var appi_programadas_procedimientos_invasivos = $("#appi_programadas_procedimientos_invasivos").numberbox('getValue');
            var appi_anaididas_procedimientos_invasivos = $("#appi_anaididas_procedimientos_invasivos").numberbox('getValue');
            var appi_suspendidas_procedimientos_invasivos = $("#appi_suspendidas_procedimientos_invasivos").numberbox('getValue');
            var horas_procedimientos_invasivos = $("#horas_procedimientos_invasivos").numberbox('getValue');
            var horas_anestesia_procedimientos_invasivos = $("#horas_anestesia_procedimientos_invasivos").numberbox('getValue');
            var horas_efectivas_procedimientos_invasivos = $("#horas_efectivas_procedimientos_invasivos").numberbox('getValue');

            /*console.log(
                'sala_operaciones_procedimientos_invasivos:' + sala_operaciones_procedimientos_invasivos + '\n' + 
                'mes_procedimientos_invasivos:' + mes_procedimientos_invasivos + '\n' + 
                'anio_procedimientos_invasivos:' + anio_procedimientos_invasivos + '\n' + 
                'appi_programadas_procedimientos_invasivos:' + appi_programadas_procedimientos_invasivos + '\n' + 
                'appi_realizadas_procedimientos_invasivos:' + appi_realizadas_procedimientos_invasivos + '\n' + 
                'appi_programadas_procedimientos_invasivos:' + appi_programadas_procedimientos_invasivos + '\n' + 
                'appi_anaididas_procedimientos_invasivos:' + appi_anaididas_procedimientos_invasivos + '\n' + 
                'appi_suspendidas_procedimientos_invasivos:' + appi_suspendidas_procedimientos_invasivos + '\n' + 
                'horas_procedimientos_invasivos:' + horas_procedimientos_invasivos + '\n' + 
                'horas_anestesia_procedimientos_invasivos:' + horas_anestesia_procedimientos_invasivos + '\n' + 
                'horas_efectivas_procedimientos_invasivos:' + horas_efectivas_procedimientos_invasivos + '\n');*/

            $.ajax({
                    url: '../../MVC_Controlador/Farmacia/CarC.php?acc=InsertarProcedimientosInvasivos',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        totalAppiProgramadas: appi_programadas_procedimientos_invasivos,
                        totalAppiRealizada: appi_realizadas_procedimientos_invasivos,
                        coberturaAppiProgramada: appi_programadas_procedimientos_invasivos,
                        totalAppiAñadida: appi_anaididas_procedimientos_invasivos,
                        totalAppiSuspendida: appi_suspendidas_procedimientos_invasivos,
                        totalHorasProcedimiento: horas_procedimientos_invasivos,
                        totalHorasAnestesia: horas_anestesia_procedimientos_invasivos,
                        totalHorasEfectivas: horas_efectivas_procedimientos_invasivos,
                        NroMes: mes_procedimientos_invasivos,
                        Anio: anio_procedimientos_invasivos,
                        Piso: sala_operaciones_procedimientos_invasivos
                    },
                    success: function(data)
                    {
                        if (data == 'M_1') {
                            $.messager.alert('SIGESA','Registro Generado');
                            $("#appi_programadas_procedimientos_invasivos").numberbox('clear');
                            $("#appi_realizadas_procedimientos_invasivos").numberbox('clear');
                            $("#appi_programadas_procedimientos_invasivos").numberbox('clear');
                            $("#appi_anaididas_procedimientos_invasivos").numberbox('clear');
                            $("#appi_suspendidas_procedimientos_invasivos").numberbox('clear');
                            $("#horas_procedimientos_invasivos").numberbox('clear');
                            $("#horas_anestesia_procedimientos_invasivos").numberbox('clear');
                            $("#horas_efectivas_procedimientos_invasivos").numberbox('clear');
                            return false;
                        }
                        if (data == 'M_0') {
                            $.messager.alert('SIGESA','ERROR!!. Comuniquese con el administrador.','warning');
                            return false;
                        }
                    }
                });
                    


            return false;
        });
    
    //ATENCIONES
    $("#con_uno_atenciones").textbox('textbox').bind('keydown', function(e){
        if (e.keyCode == 9){
            SumarAtenciones();
            $('#con_dos_atenciones').textbox('textbox').focus();
            return false;
        }

        });
     $("#con_dos_atenciones").textbox('textbox').bind('keydown', function(e){
        if (e.keyCode == 9){
            SumarAtenciones();
            $('#inter_con_atenciones').textbox('textbox').focus();
            return false;
        }

        });
     $("#inter_con_atenciones").textbox('textbox').bind('keydown', function(e){
        if (e.keyCode == 9){
            SumarAtenciones();
            return false;
        }

    });

     function SumarAtenciones()
     {
        var con_uno_atenciones = $("#con_uno_atenciones").textbox('getText');
        var con_dos_atenciones = $("#con_dos_atenciones").textbox('getText');
        var inter_con_atenciones = $("#inter_con_atenciones").textbox('getText');
        var total_atenciones_m = $("#total_atenciones").text();
        var nuevo_total_atenciones = (parseFloat(con_uno_atenciones) || 0 ) + (parseFloat(con_dos_atenciones) || 0 ) + (parseFloat(inter_con_atenciones) || 0 );
        $("#total_atenciones").text();
        $("#total_atenciones").text(nuevo_total_atenciones);
     }

     $("#IngresarAtenciones").click(function(event) {
        var con_uno_atenciones = $("#con_uno_atenciones").textbox('getText');
        var con_dos_atenciones = $("#con_dos_atenciones").textbox('getText');
        var inter_con_atenciones = $("#inter_con_atenciones").textbox('getText');
        var sala_atenciones = $("#sala_atenciones").combobox('getValue');
        var mes_atenciones = $("#mes_atenciones").combobox('getValue');
        var anio_atenciones = $("#anio_atenciones").combobox('getValue');
        var total_atenciones_melodia = $("#total_atenciones").text();
        $.ajax({
            url: '../../MVC_Controlador/Farmacia/CarC.php?acc=InsertarConsultoriosAtenciones',
            type: 'POST',
            dataType: 'json',
            data: {
                consultasConsul1: con_uno_atenciones,
                consultalConsul2: con_dos_atenciones,
                interconsultas: inter_con_atenciones,
                total: total_atenciones_melodia,
                NroMes: mes_atenciones,
                Anio: anio_atenciones,
                Piso: sala_atenciones
            },
            success: function(data)
            {
                if (data == 'M_1') {
                    $.messager.alert('SIGESA','Registro Generado');
                    $("#con_uno_atenciones").textbox('clear');
                    $("#con_dos_atenciones").textbox('clear');
                    $("#inter_con_atenciones").textbox('clear');
                    $("#total_atenciones").text(' ');
                    return false;
                }
                if (data == 'M_0') {
                    $.messager.alert('SIGESA','ERROR!!. Comuniquese con el administrador.','warning');
                    return false;
                }
            }
        });
        

        return false;
     });

     //COMPLIOCAICONES
    $("#IngresarComplicaciones").click(function(event) {
        var sala_complicaciones = $("#sala_complicaciones").combobox('getValue');
        var mes_complicaciones = $("#mes_complicaciones").combobox('getValue');
        var anio_complicaciones = $("#anio_complicaciones").combobox('getValue');
        var complicaciones_anestesicas = $("#complicaciones_anestesicas").numberbox('getValue');
        var muertes_complicaciones = $("#muertes_complicaciones").numberbox('getValue');
        var urpa_complicaciones = $("#urpa_complicaciones").numberbox('getValue');

        $.ajax({
            url: '../../MVC_Controlador/Farmacia/CarC.php?acc=InsertarComplicacionesMuertes',
            type: 'POST',
            dataType: 'json',
            data: {
                ComplicacionesAnestesicas: complicaciones_anestesicas,
                MuertesSalasOperaciones: muertes_complicaciones,
                MuertesUrpa: urpa_complicaciones,
                NroMes: mes_complicaciones,
                Anio: anio_complicaciones,
                Piso: sala_complicaciones
            },
            success: function(data)
            {
                if (data == 'M_1') {
                    $.messager.alert('SIGESA','Registro Generado');
                    $("#complicaciones_anestesicas").numberbox('clear');
                    $("#muertes_complicaciones").numberbox('clear');
                    $("#urpa_complicaciones").numberbox('clear');
                    return false;
                }
                if (data == 'M_0') {
                    $.messager.alert('SIGESA','ERROR!!. Comuniquese con el administrador.','warning');
                    return false;
                }
            }
        });


        return false;
    });

    // end
    });
    </script> 
</head>    

    <div class="easyui-tabs" id="pestanas" style="width:100%;height:900px">

<!--1 PESTAÑA-->
        <div title="Informe Operacional" style="padding:5%">
                <table width="100%">
                    <tr>
                        <td width="50%">
                        <select class="easyui-combobox" id="sala_informe_operacional" style="width:250px;" data-options="
                        	onSelect: function(rec){
                        		var piso =  $('#sala_informe_operacional').combobox('getValue');
            					$('#especialidad_informe_operacional').combobox('reload', '../../MVC_Controlador/Farmacia/CarC.php?acc=ListarEspecialidadesCombo&piso='+piso);
        				}">
                                <option value="3">3° PISO SALA DE OPERACIONES</option>                                                               
                                <option value="4">4° PISO SALA DE OPERACIONES</option>                                    
                        </select></td>
                        <td align="right"><a href="#" class="easyui-linkbutton c6" id="nuevaespecialidad" style="width:150px;height:30px;" data-options="iconCls:'icon-add'" style="width:150px">Nueva Especialidad</a></td>
                    </tr>
                    <tr>
                        <td>
                        <select class="easyui-combobox" id="mes_informe_operacional" style="width:150px;">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>                                                        
                        </select>
                        <select class="easyui-combobox" id="anio_informa_operacional" style="width:100px;" >
                            <?php 
                                for ($i=date('Y'); $i>=1950; $i--) {
                                    echo "<option value='$i'>$i</option>";
                                } 
                            ?>
                        </select>
                        <td>
                    </tr>
                </table>
                <br>
                <div class="easyui-panel"  style="width:100%;height:90%;">
                    <div class="easyui-layout" data-options="fit:true">
                        <div data-options="region:'west',split:false" style="width:50%;padding: 0%;">
                            <table style="width: 100%;">
                            	<tr>
                            		<th colspan="2" style="padding: 3%;background-color: #d7d7d7;" align="center">
                            			INGRESO DE DATOS
                            		</th>
                            	</tr>
                                <tr>
                                    <th style="padding: 3%;border: 1px solid #d7d7d7;">Especialidad</th>
                                    <td style="border: 1px solid #d7d7d7;" align="center">
                                        <input type="text" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Farmacia/CarC.php?acc=ListarEspecialidadesCombo&piso='+$('#sala_informe_operacional').combobox('getValue'),
                                        														onSelect: function(rec){
                                        															var mes_informe_operacional  = $('#mes_informe_operacional').combobox('getValue');
																									var anio_informa_operacional  = $('#anio_informa_operacional').combobox('getValue');
																									var especialidad_informe_operacional  = $('#especialidad_informe_operacional').combobox('getValue');
																									
																									$.ajax({
																											url: '../../MVC_Controlador/Farmacia/CarC.php?acc=TablaInfrOperacional',
																							               	type: 'POST',
																							                dataType: 'json',
																							                data:
																							                {
																							                	IdEspecialidad: rec.id,
																												mes: mes_informe_operacional,
																												anio: anio_informa_operacional
																							                },
																							                success: function(tabla_data)
																							                {
																							                	$('#tabla_registro_io').datagrid({data:tabla_data});
																							                }
																										});
                                        														}" id="especialidad_informe_operacional" style="width: 70%;">
                                    </td>
                                </tr>
                                <tr>
                                    <th style="padding: 3%;border: 1px solid #d7d7d7;">Diagnostico</th>
                                    <td style="border: 1px solid #d7d7d7;" align="center">
                                        <input type="text" class="easyui-textbox" id="diagnostico_informe_operacional" style="width: 70%;text-transform: uppercase;" >
                                    </td>
                                </tr>
                                <tr>
                                    <th style="padding: 3%;border: 1px solid #d7d7d7;">Procedimiento</th>
                                    <td style="border: 1px solid #d7d7d7;" align="center">
                                        <input type="text" class="easyui-textbox" id="procedimiento_informe_operacional" style="width: 70%;text-transform: uppercase;" >
                                    </td>
                                </tr>
                                <tr>
                                    <th style="padding: 3%;border: 1px solid #d7d7d7;">Tiempo Promedio de Sala (MIN)</th>
                                    <td style="border: 1px solid #d7d7d7;" align="center">
                                        <input class="easyui-numberbox" precision="2" id="tiempo_informe_o" style="width: 70%;text-transform: uppercase;" >
                                    </td>
                                </tr>
                                <tr>
                                    <th style="padding: 3%;border: 1px solid #d7d7d7;">Numero de Cirugias</th>
                                    <td style="border: 1px solid #d7d7d7;" align="center">
                                        <input class="easyui-numberbox" id="numero_informe_operacional" style="width: 70%;text-transform: uppercase;" >
                                    </td>
                                </tr>                     
                            </table>
                            <br>
                            <table cellspacing="10" style="margin-left: 30%;">
                            <tr>                            
                            <td>
                            <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="">LIMPIAR</a>
                            </td>
                            <td>
                            <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:96px;height: 50px;" id="IngresarInformeOperacional">AGREGAR</a>
                            </td>
                            </tr>                            
                            </table>
                        </div>
                        <div data-options="region:'center'" style="width:50%;">
                                <table class="easyui-datagrid" title="Especialidad" style="width:99%;height:400px;" data-options="collapsible:false" id="tabla_registro_io">
                                    <thead>
                                        <tr>                                
                                            <th data-options="field:'DIAGNOSTICO',rezisable:true">Diagnostico</th>
                                            <th data-options="field:'PROCEDIMIENTO',rezisable:true">Procedimiento</th>
                                            <th data-options="field:'TPSALA',rezisable:true">Tiempo Promedio de Sala (Min.)</th>
                                            <th data-options="field:'NCIRUGIAS',rezisable:true">Numero de Cirugias</th>                             
                                        </tr>
                                    </thead>
                                </table>
                                <br>
                                
                        </div>
                    </div>
            </div>
        </div>

<!--2 PESTAÑA-->
        <div title="Operaciones Programadas" style="padding:5%">           
            <table>
                    <tr>
                        <td>
                        	<select class="easyui-combobox" style="width:250px;" id="piso_operaciones_programadas">
                                <option value="3">3° PISO SALA DE OPERACIONES</option>                                                               
                                <option value="4">4° PISO SALA DE OPERACIONES</option>                                    
                        	</select>
                        </td>                        
                    </tr>
                </table>
            <br>
            <table style="padding:2%;width:80%;border-collapse: collapse;">

                <tr>
                    <thead style="background-color: #d7d7d7;">
                    <td align="center" colspan="2"><strong>OPERACIONES PROGRAMADAS, GRADO DE CUMPLIMIENTO, UTILIZACION DE SALA</strong></td>
                    </thead>
                </tr>
                              
                <tr>
                    <td align="center" colspan="2" style="background-color: #d7d7d7;">
                    	 <select class="easyui-combobox" style="width:150px;" id="mes_operaciones_programadas">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>                                                        
                        </select>
                        <select class="easyui-combobox" id="anio_operaciones_programadas" style="width:100px;" >
                            <?php 
                                for ($i=date('Y'); $i>=1950; $i--) {
                                    echo "<option value='$i'>$i</option>";
                                } 
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">OPERACIONES PROGRAMADAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="ope_programadas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">SUSPENDIDAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="ope_suspendidas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">OPER. PROGRAMADAS REALIZADAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="ope_programadas_realizadas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">COBERTURA PROGRAMADAS (%):</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" precision="2" id="cobertura_programadas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">AÑADIDAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="ope_aniadidas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">REOPERADOS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="ope_reoperadas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL OPERACIONES REALIZADAS (PROG + AÑAD + REOP):</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="total_ope_realizadas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">HORAS OFERTADAS DE SALA:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" precision="2" id="horas_ofer_sala"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">HORAS OPERATORIAS PROGRAMADAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" precision="2" id="horas_ope_programadas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">HORAS OPERATORIAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" precision="2" id="horas_operatorias"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">HORAS ANESTESICAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" precision="2" id="horas_anestesicas"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">HORAS EFECTIVAS:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" precision="2" id="horas_efectivas"></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>                            
                    <td align="right">
                    <a href="" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="">LIMPIAR</a>
                    </td>
                    <td>
                    <a href="" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:96px;height: 50px;" id="Ingresar_reporte_operaciones_programadas">AGREGAR</a>
                    </td>                           
                </tr>
            </table>           
        </div>

<!--3 PESTAÑA-->
        <div title="Tipo Anestesia" data-options="" style="padding:5%">
            <table>
                    <tr>
                        <td>
                        	<select class="easyui-combobox" style="width:250px;" id="piso_anestesia">
                                <option value="3">3° PISO SALA DE OPERACIONES</option>                                                               
                                <option value="4">4° PISO SALA DE OPERACIONES</option>                                    
                        	</select>
                        </td>                        
                    </tr>
            </table>
            <br>
            <table style="padding:2%;width:80%;border-collapse: collapse;" >                
                <tr>
                    <thead style="background-color: #d7d7d7;">
                    <td align="center" colspan="3"><strong>TIPO ANESTESIA DPTO HNDAC</strong></td>
                    </thead>
                </tr>
                               
                <tr>
                    <td align="center" colspan="3" style="background-color: #d7d7d7;">
                    <select class="easyui-combobox" style="width:150px;" id="mes_anestesia">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>                                                        
                        </select>
                        <select class="easyui-combobox" id="anio_anestesia" style="width:100px;" >
                            <?php 
                                for ($i=date('Y'); $i>=1950; $i--) {
                                    echo "<option value='$i'>$i</option>";
                                } 
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">A.GENERAL BALANCEADA:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="gen_balanceada_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_gen_balanceada_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">A.GENERAL INHALATORIA:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="gen_inhalatoria_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_gen_inhalatoria_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">A.GENERAL ENDOVENOSA:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="gen_endovenosa_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_gen_endovenosa_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">A.PERIDURAL CONTINUA:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" precision="2" id="per_continua_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_per_continua_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">A.PERIDURAL SIMPLE:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="per_simple_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_per_simple_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">A.RAQUIDEA:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="raquidea_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_raquidea_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">A.LOCAL:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="local_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_local_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">SEDACION:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="sedacion_anestesia"></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="porc_sedacion_anestesia"></span></td>
                </tr>
                <tr>
                    <th align="left" style="border: 1px dotted #d7d7d7;">TOTAL:</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><span id="total_anestesia">0</span></td>
                    <td align="center" style="border: 1px dotted #d7d7d7;">100%</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>                
                <tr>                            
                    <td align="right">
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="">LIMPIAR</a>
                    </td>
                    <td>
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:96px;height: 50px;" id="agregar_anestesia">AGREGAR</a>
                    </td>                           
                </tr>
            </table>
        </div>

<!--4 PESTAÑA-->
        <div title="Apoyo para Procedimientos Invasivos" data-options="" style="padding:5%">
            <table>
                    <tr>
                        <td><select class="easyui-combobox" id="sala_operaciones_procedimientos_invasivos" style="width:250px;">
                                <option value="3">3° PISO SALA DE OPERACIONES</option>                                                               
                                <option value="4">4° PISO SALA DE OPERACIONES</option>                                   
                        </select></td>                        
                    </tr>
            </table>
            <br>
            <table style="padding:2%;width:80%;border-collapse: collapse;">                
                <tr>
                    <thead style="background-color: #d7d7d7;">
                    <td align="center" colspan="2" ><strong>APOYO PARA PROCEDIMIENTOS INVASIVOS (APPI)</strong></td>
                    </thead>
                </tr>
                                
                <tr>
                    <td align="center" colspan="2" style="background-color: #d7d7d7;">
                    	<select class="easyui-combobox" style="width:150px;" id="mes_procedimientos_invasivos">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>                                                        
                        </select>
                        <select class="easyui-combobox" id="anio_procedimientos_invasivos" style="width:100px;" >
                            <?php 
                                for ($i=date('Y'); $i>=1950; $i--) {
                                    echo "<option value='$i'>$i</option>";
                                } 
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="background-color: #d7d7d7;"></td>
                </tr>
                <tr>
                    <td colspan="2" style="background-color: #d7d7d7;"></td>
                </tr>
                <tr>
                    <td></td>
                    <th align="center">CANTIDAD</th>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL DE APPI PROGRAMADAS</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="appi_programadas_procedimientos_invasivos"></td>                    
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL DE APPI REALIZADAS (Programadas y añadidas)</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="appi_realizadas_procedimientos_invasivos"></td>                    
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">COBERTURA DE APPI PROGRAMADAS</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="appi_programadas_procedimientos_invasivos"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL APPI AÑADIDAS</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="appi_anaididas_procedimientos_invasivos"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL DE APPI SUSPENDIDAS</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="appi_suspendidas_procedimientos_invasivos"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL DE HORAS DE PROCEDIMIENTO (APPI)</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="horas_procedimientos_invasivos"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL DE HORAS DE ANESTESIA (APPI)</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="horas_anestesia_procedimientos_invasivos"></td>
                </tr>
                <tr>
                    <th width="50%" align="left" style="border: 1px dotted #d7d7d7;">TOTAL DE HORAS EFECTIVAS (APPI)</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="horas_efectivas_procedimientos_invasivos"></td>
                </tr>
                <tr>
                    <th colspan="2" align="center" style="background-color: #d7d7d7;">APPI RESTRINGIDO POR FALTA DE UNIDAD ANESTESICA OPERATIVA</th>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>                
                <tr>                            
                    <td align="right">
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="">LIMPIAR</a>
                    </td>
                    <td>
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:96px;height: 50px;" id="IngresarApoyoProcedimientosInvasivos">AGREGAR</a>
                    </td>                           
                </tr>
            </table>
        </div>

<!--5 PESTAÑA-->
        <div title="Atencion en Consultorio de Anestesiologia" data-options="" style="padding:5%">
            <table>
                    <tr>
                        <td><select class="easyui-combobox" id="sala_atenciones" style="width:250px;">
                                <option value="3">3° PISO SALA DE OPERACIONES</option>                                                               
                                <option value="4">4° PISO SALA DE OPERACIONES</option>                                    
                        </select></td>                        
                    </tr>
            </table>
            <br>
            <table style="padding:2%;width:60%;border-collapse: collapse;">                
                <tr>
                    <thead style="background-color: #d7d7d7;">
                    <td align="center" colspan="4"><strong>ATENCION EN CONSULTORIO DE ANESTESIOLOGIA</strong></td>
                    </thead>
                </tr>
                                
                <tr>
                    <td align="center" colspan="4" style="background-color: #d7d7d7;">
                    	<select class="easyui-combobox" style="width:150px;" id="mes_atenciones">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>                                                        
                        </select>
                        <select class="easyui-combobox" id="anio_atenciones" style="width:100px;" >
                            <?php 
                                for ($i=date('Y'); $i>=1950; $i--) {
                                    echo "<option value='$i'>$i</option>";
                                } 
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <th align="center" colspan="2" style="border: 1px solid #d7d7d7;padding: 2%;">CONSULTAS</th>
                    <th align="center" rowspan="2" style="border: 1px solid #d7d7d7;padding: 2%;">INTERCONSULTAS</th>
                    <th align="center" rowspan="2" style="border: 1px solid #d7d7d7;padding: 2%;">TOTAL</th>                                      
                </tr>
                <tr>
                    <th align="center" style="border: 1px solid #d7d7d7;padding: 2%;">CONSULTORIO 1</th>
                    <th align="center" style="border: 1px solid #d7d7d7;padding: 2%;">CONSULTORIO 2</th>                                      
                </tr>
                <tr>
                    <td align="center" style="border: 1px solid #d7d7d7;padding: 2%;"><input class="easyui-numberbox" id="con_uno_atenciones"></td>
                    <td align="center" style="border: 1px solid #d7d7d7;padding: 2%;"><input class="easyui-numberbox" id="con_dos_atenciones"></td>
                    <td align="center" style="border: 1px solid #d7d7d7;padding: 2%;"><input class="easyui-numberbox" id="inter_con_atenciones"></td>
                    <td align="center" style="border: 1px solid #d7d7d7;padding: 2%;"><span id="total_atenciones">0</span></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>                                                                 
                <tr>                            
                    <td colspan="4" align="center">
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="">LIMPIAR</a>
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:96px;height: 50px;" id="IngresarAtenciones">AGREGAR</a>
                    </td>                                              
                </tr>
            </table>
        </div>

<!--6 PESTAÑA-->
        <div title="Complicaciones y Muertes en Centro Quirurgico" data-options="" style="padding:5%">
            <table>
                    <tr>
                        <td><select class="easyui-combobox" id="sala_complicaciones" style="width:250px;">
                                <option value="3">3° PISO SALA DE OPERACIONES</option>                                                               
                                <option value="4">4° PISO SALA DE OPERACIONES</option>                                     
                        </select></td>                        
                    </tr>
            </table>
            <br>
            <table style="padding:2%;width:60%;border-collapse: collapse;">                
                <tr>
                    <thead style="background-color: #d7d7d7;">
                    <td align="center" colspan="2"><strong>COMPLICACIONES Y MUERTES EN CENTRO QUIRURGICO HDNAC</strong></td>
                    </thead>
                </tr>
                                
                <tr>
                    <td align="center" colspan="2" style="background-color: #d7d7d7;">
                    	<select class="easyui-combobox" style="width:150px;" id="mes_complicaciones">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>                                                        
                        </select>
                        <select class="easyui-combobox" id="anio_complicaciones" style="width:100px;" >
                            <?php 
                                for ($i=date('Y'); $i>=1950; $i--) {
                                    echo "<option value='$i'>$i</option>";
                                } 
                            ?>
                        </select>
                    </td>
                </tr>
                               
                <tr>
                    <th width="50%" align="center" style="border: 1px dotted #d7d7d7;">COMPLICACIONES ANESTESICAS</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="complicaciones_anestesicas"></td>                    
                </tr>
                <tr>
                    <th width="50%" align="center" style="border: 1px dotted #d7d7d7;">MUERTES EN SALA DE OPERACIONES</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="muertes_complicaciones"></td>                    
                </tr>
                <tr>
                    <th width="50%" align="center" style="border: 1px dotted #d7d7d7;">MUERTES EN URPA</th>
                    <td align="center" style="border: 1px dotted #d7d7d7;"><input class="easyui-numberbox" id="urpa_complicaciones"></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>                            
                <tr>                            
                    <td align="right">
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="">LIMPIAR</a>
                    </td>
                    <td>
                    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:96px;height: 50px;" id="IngresarComplicaciones">AGREGAR</a>
                    </td>                           
                </tr>
            </table>
        </div>

    </div>
</html>


       
    
        
   