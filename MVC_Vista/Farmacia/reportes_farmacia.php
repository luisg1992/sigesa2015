<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
<!DOCTYPE html>
<html lang="en">
<head>
<title>SIGESA - <?php echo $NOMBRE ;?></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
    <link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
    <style>
        html,body { 
          padding: 0px;
          margin: 0px;
          height: 100%;
          font-family: 'Helvetica';       
        }

        .mayus>input{
      text-transform: capitalize;
        }   
    </style>
     <script language="javascript">
		function BuscarPacientesFarmacia(){
			
			var FechaInicio=document.getElementById("FechaInicio").value;
			var FechaFin=document.getElementById("FechaFin").value;
			var HistoriaClinica=document.getElementById("HistoriaClinica").value;
      		var Dni=document.getElementById("Dni").value;
			var ApellidoPaterno=document.getElementById("ApellidoPaterno").value;
			var IdEmpleado=document.getElementById("IdEmpleado").value;
			
			miPopup = window.open("../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=Buscar_Farmacia_Pacientes&FechaInicio="+FechaInicio+"&FechaFin="+FechaFin+"&HistoriaClinica="+HistoriaClinica+"&Dni="+Dni+"&ApellidoPaterno="+ApellidoPaterno+"&IdEmpleado="+IdEmpleado,"frame_rejilla","width=700,height=80,scrollbars=yes");
		}
		

		function DetalleFarmaciaPacientes(IdCuentaAtencion){
			var googlewin=dhtmlwindow.open("googlebox", "iframe", "../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=DetalleFarmaciaPacientes&IdCuentaAtencion="+IdCuentaAtencion,"Modifica Admision Emergencia ", "width=990px,height=690px,resize=0,scrolling=1,center=1", "recal")
			//Esta funcion se ejecuta para poder cerrar popup 
      FocusOnInput();
		}
		
		
	
		function MostrarDiagnosticos(IdAtencion) { 
		Diagnosticos=window.open("../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=VerificarDiagnostico&IdAtencion="+IdAtencion,'Mostrar Diagnosticos','top=300,left=700,width=580,height=300,scrollbars=yes') ; 
		} 


 /*REPORTES*/
 function Abrir_Reportes(){
			var googlewin=dhtmlwindow.open("googlebox", "iframe", "../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=Reportes","Reportes Farmacia ", "width=990px,height=690px,resize=0,scrolling=1,center=1", "recal")
			//Esta funcion se ejecuta para poder cerrar popup 
     // FocusOnInput();
		}

function Abrir_Kardex(){
      var googlewin=dhtmlwindow.open("googlebox", "iframe", "../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=Kardex","Kardex Farmacia ", "width=990px,height=690px,resize=0,scrolling=1,center=1", "recal")
      //Esta funcion se ejecuta para poder cerrar popup 
     // FocusOnInput();
    }

 function Limpiar(){
	 var FechaInicio=document.getElementById("FechaInicio").value="<?php echo vfecha(substr($FechaServidor,0,10))?>";
			document.getElementById("FechaFin").value="<?php echo vfecha(substr($FechaServidor,0,10))?>";
			document.getElementById("HistoriaClinica").value="";
			document.getElementById("IdCuenta").value="";
      		document.getElementById("Dni").value="";
      		document.getElementById("IdServicio").selectedIndex=0;
			document.getElementById("ApellidoPaterno").value="";
			document.getElementById("HistoriaClinica").texto.focus();
			
	 }
   function Abrir_Buscador_Documento(){
      var googlewin=dhtmlwindow.open("googlebox","iframe","../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=BuscadorxDocumento","Buscador", "width=1350px,height=690px,resize=0,scrolling=1,center=1", "recal" )
}
    </script>

<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">

<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
<link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" />
<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">    
<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 





  <div class="easyui-layout" style="width:100%;height:100%;">
        <div data-options="region:'north'" style="height:60px;padding: 10px">
            <table  border="0">
              <tr>
                  <td width="211" align="center"><table width="211" border="0">
                  <tr>
                  <td width="69"> <img src="../../MVC_Complemento/img/Reportes.png" width="24" height="24" onClick="Abrir_Reportes()" onMouseOver="style.cursor=cursor"> REPORTES<label for="reporte_farmacia"></td>
                  <td width="69"> <img src="../../MVC_Complemento/img/Reportes.png" width="24" height="24" onClick="Abrir_Buscador_Documento()" onMouseOver="style.cursor=cursor"> BUSCADOR<label for="reporte_farmacia"></td>
                  </tr> 
                  </table></td>
              </tr>
            </table>
     
        </div>
        <div data-options="region:'west',split:true,iconCls:'icon-ok'" title="Lista de Medicamentos e Insumos" style="width:47%;">
          <table id="Medicamentos" class="easyui-datagrid"  style="width:780px;height:585px"
          data-options="rownumbers:true,singleSelect:true,pageSize:20,pagination: true,url:'../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=MostrarMedicamentosGrillaconStock',method:'get',
          onSelect:function(index,row){
          Cargar_Stock_Medicamentos(row.Codigo);
          Cargar_Grafico_Lineas(row.Codigo);
          }">
          <thead>
              <tr>
                  <th data-options="field:'Codigo',width:80">Codigo</th>
                  <th data-options="field:'Nombre',width:550">Nombre de Producto</th>
                  <th data-options="field:'SaldoTotal',width:115,formatter:formatPrice">Saldo Total</th>
              </tr>
          </thead>
          </table>
       </div>
        <div data-options="region:'center',iconCls:'icon-ok'" style="width:63%">

                <div class="easyui-layout" style="width:100%;height:100%;">
                    <div data-options="region:'south',split:true,iconCls:'icon-ok'" title="Stock por Almacen"  style="height:350px;">
                          <!-- Carga el Stock Total por Almacen -->
                           <table id="Stock_por_Medicamento"></table>
                    </div>
                    <div data-options="region:'center',split:true,iconCls:'icon-ok'" title="Grafica Estadistica" style="width:100%;height:400px">
                      <div id="container" style="width: 700px; height: 400px; margin: 0 auto"></div>
                   </div>
                </div>

        </div>
    </div>
 
</body>

    
        <script type="text/javascript">

      function Cargar_Stock_Medicamentos(Codigo){
          var dg =$('#Stock_por_Medicamento').datagrid({
            singleSelect:true,
            rownumbers:true,
            remoteSort:false,
            multiSort:true,
            collapsible:true,
            url:'../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=MostrarStockporCodigo&Codigo='+Codigo,
            columns:[[
            {field:'Descripcion',title:'Almacen',width:250},      
            {field:'cantidad',title:'Cantidad',width:80,align:'center'},
            {field:'precio',title:'Precio',width:100}
          ]]  
        });
      } 


      function formatPrice(val,row){
          if (val == 0){
              return '<span style="color:red;font-weight: bold;">'+val+'</span>';
          } else {
              return val;
          }
      }

      $(function(){
        var Medicamentos = $('#Medicamentos').datagrid();
        Medicamentos.datagrid('enableFilter');
       });  
      </script>



      <script type="text/javascript">
            function Cargar_Grafico_Lineas(Codigo){
                  $.ajax({
                    url: '../../MVC_Controlador/Farmacia/FarmaciaC.php?acc=Highchart_Farmacia_Almacenes',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                      Codigo:Codigo
                    },

                    success:function(res)
                    {
                    objeto=jQuery.parseJSON(res);   
                      $('#container').highcharts({      
                      chart: {
                          plotBackgroundColor: null,
                          plotBorderWidth: null,
                          plotShadow: false,
                          type: 'pie'
                      },
                      title: {
                          text: 'Almacenes de Farmacia'
                      },
                      tooltip: {
                          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                      },
                      plotOptions: {
                          pie: {
                              allowPointSelect: true,
                              cursor: 'pointer',
                              dataLabels: {
                                  enabled: false
                              },
                              showInLegend: true
                          }
                      },
                      credits: {
                      enabled: false
                      },
                      series: [{ data: objeto }]  
                    });
                    }
                    }); 
      }       
    </script>
</html>

