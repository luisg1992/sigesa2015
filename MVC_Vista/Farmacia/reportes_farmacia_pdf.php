<?php 
ini_set('memory_limit', '1024M'); 

require('../../MVC_Complemento/fpdf/fpdf.php');
require('../../MVC_Modelo/LaboratorioM.php');
require('../../MVC_Modelo/FarmaciaM.php');

#FUNCIONES
function array_unique2($array) {
	$container = array();
	$i = 0;
	
	foreach ($array as $a=>$b)
		if (!in_array($b,$container)){
			$container[$i]=$b;
			$i++;
		}
			
	return $container;
} 	

class PDF extends FPDF
{	
	function Header(){	
		#RANGO DE FECHAS
		$FechaInicio_temp = substr($_REQUEST["finicio"], 0,10);
		$FechaFinal_temp = substr($_REQUEST["ffin"], 0,10);

		list($mes,$dia,$anio) = explode("/",$FechaInicio_temp);
		$FechaInicio = $dia."/".$mes."/".$anio;


		list($mes,$dia,$anio) = explode("/",$FechaFinal_temp);
		$FechaFinal = $dia."/".$mes."/".$anio;

		#IMAGEN INSTITUCIONAL
		$this->Cell(60);
		$this->Image('../../MVC_Complemento/img/hndac.jpg',10,5,10,10);
		$this->Cell(40);

		#TITULO PRINCIPAL
		$this->setfont('Helvetica','',9);
		$this->Cell(1,2,'CONSUMO DE MEDICAMENTOS (DETALLADO POR TARIFAS)',0,0,'C');

		#FECHA DE IMPRESION
		$this->setfont('Helvetica','',7);
		$this->Cell(90,2,date('h:m:s a'),0,0,'R');
		
		#RANGO DE FECHA
		$this->Ln(3);
		$this->setfont('Helvetica','',7);
		$this->Cell(0,4,'DESDE: '.$FechaInicio.'   HASTA: '.$FechaFinal,'',0,'C');
		$this->Cell(0,4,'Pagina: '.$this->PageNo(),'',0,'R');

		#TIPO DE FARMACIA
		$this->Ln(10);
		$this->setfont('Helvetica','',8);
		$this->Cell(0,4,'FARMACIA: ' . $_REQUEST["FARMACIA"],'B',0,'L');
		
		#SUBTITULO
		$this->Ln(5);
		$this->setfont('Helvetica','',8);
		$this->Cell(0,5,'CONSUMO TOTAL','B',0,'C');
		$this->Ln(2);

	}

	function Footer()
	{	
		#OBTENIEDO USUARIO QUE GENERA LA IMPRESION
		$USUARIO = $_REQUEST["USUARIO"];
		$nombreusuario = SigesaLaboratorioUsuarioMostrarImprimir_M($USUARIO);

		$this->SetY(-8);
		$this->setfont('Helvetica','',6);

		#USUARIO
		$this->Cell(0,4,'USUARIO: ' . $nombreusuario[0]["USUARIO"],0,0,'L');	

		#AREA DE SISTEMAS
		$this->Cell(0,4,'HNDAC / OESI / DESARROLLO DE SISTEMAS',0,0,'R');					 				
	}

}

	$pdf = new PDF("P","mm","A4");
	$pdf->SetTitle('Lista Verificacion');
	$pdf->SetAuthor('Rodolfo Crisanto Rosas');
	$pdf->AliasNbPages();
	$pdf->AddPage();

	#INGRESANDO VALORES
	$finicio = $_REQUEST["finicio"];
	$ffin = $_REQUEST["ffin"];
	$tipo_farmacia = $_REQUEST["tipo_farmacia"];

	$medicamentos_temp 		= ConsumoNormalMedicamentos_M($finicio,$ffin,$tipo_farmacia);
	$insumos_temp 		= ConsumoNormalInsumos_M($finicio,$ffin,$tipo_farmacia);

	#GENERANDO LISTA DE MEDICAMENTOS
	for ($i=0; $i < count($medicamentos_temp); $i++) { 
		$lista_medicamentos[$i] = array(
			'CODIGO'	=> 	$medicamentos_temp[$i]['CODIGO'],
			'NOMBRE'	=> 	$medicamentos_temp[$i]['NOMBRE'],
			'UNIDAD'	=> 	$medicamentos_temp[$i]['UNIDAD']
			);
	}

	$lista_medicamentos = array_unique2($lista_medicamentos);

	#GENERANDO LISTAS DE INSUMOS
	for ($i=0; $i < count($insumos_temp); $i++) { 
		$lista_insumos[$i] = array(
			'CODIGO'	=> 	$insumos_temp[$i]['CODIGO'],
			'NOMBRE'	=> 	$insumos_temp[$i]['NOMBRE'],
			'UNIDAD'	=> 	$insumos_temp[$i]['UNIDAD']
			);
	}

	$lista_insumos = array_unique2($lista_insumos);

	#GENERANDO STOCK PARA MEDICAMENTOS
	for ($i=0; $i < count($lista_medicamentos); $i++) { 
		for ($j=0; $j < count($medicamentos_temp); $j++) { 
			if ($lista_medicamentos[$i]["CODIGO"] == $medicamentos_temp[$j]["CODIGO"]) {
				$stock_temporal = $medicamentos_temp[$j]["STOCK"];
			}
		}

		$listas_stocks_medicamentos[$i] = round($stock_temporal);
	}

	#GENERANDO STOCK PARA INSUMOS
	for ($i=0; $i < count($lista_insumos); $i++) { 
		for ($j=0; $j < count($insumos_temp); $j++) { 
			if ($lista_insumos[$i]["CODIGO"] == $insumos_temp[$j]["CODIGO"]) {
				$stock_temporal = $insumos_temp[$j]["STOCK"];
			}
		}

		$listas_stocks_insumos[$i] = round($stock_temporal);
	}

	#GENERANDO CANTIDADES PARA INSUMOS
	$cantidad_temporal = 0;
	for ($i=0; $i < count($lista_insumos); $i++) { 
		for ($j=0; $j < count($insumos_temp); $j++) { 
			if ($lista_insumos[$i]["CODIGO"] == $insumos_temp[$j]["CODIGO"]) {
				$cantidad_temporal = $insumos_temp[$j]["CANTIDAD"] + $cantidad_temporal;
			}
		}

		$listas_cantidades_insumos[$i] = $cantidad_temporal;
		$cantidad_temporal = 0;
	}

	#GENERANDO CANTIDADES PARA MEDICAMENTOS
	$cantidad_temporal = 0;
	for ($i=0; $i < count($lista_medicamentos); $i++) { 
		for ($j=0; $j < count($medicamentos_temp); $j++) { 
			if ($lista_medicamentos[$i]["CODIGO"] == $medicamentos_temp[$j]["CODIGO"]) {
				$cantidad_temporal = $medicamentos_temp[$j]["CANTIDAD"] + $cantidad_temporal;
			}
		}

		$listas_cantidades_medicamentos[$i] = $cantidad_temporal;
		$cantidad_temporal = 0;
	}

	#echo '<pre>';
	#print_r($listas_cantidades_medicamentos);
	#echo '</pre>';
	#exit();

	
	$h=0;
	#MEDICAMENTOS
	$pdf->Ln(7);
	$pdf->setfont('Helvetica','U',8);
	$pdf->Cell(0,5,'MEDICAMENTOS:',0,0,'L');

	#CABECERAS TABLA
	$pdf->Ln(8);
	$pdf->setfont('Helvetica','b',7);
	$pdf->Cell(15,5,'CODIGO','B',0,'L');
	$pdf->Cell(110,5,'DESCRIPCION','B',0,'L');
	$pdf->Cell(15,5,'U.MED','B',0,'C');
	$pdf->Cell(20,5,'CANT.CONSU.','B',0,'C');
	$pdf->Cell(20,5,'STOCK.ATC.','B',0,'C');
	$pdf->Ln(1);

	$cantidadtotalmedicamentos = 0;
	$stocktotalmedicamentos = 0;

	
	for ($i=0; $i < count($lista_medicamentos); $i++) { 
		$pdf->Ln(5);
		$pdf->setfont('Helvetica','',7);
		$pdf->Cell(15,2,$lista_medicamentos[$i]['CODIGO'],0,0,'L');
		$pdf->Cell(110,2,$lista_medicamentos[$i]['NOMBRE'],0,0,'L');
		$pdf->Cell(15,2,$lista_medicamentos[$i]['UNIDAD'],0,0,'R');
		$pdf->Cell(20,2,$listas_cantidades_medicamentos[$i],0,0,'R');
		$pdf->Cell(20,2,$listas_stocks_medicamentos[$i],0,0,'R');
		$cantidadtotalmedicamentos = $cantidadtotalmedicamentos + $listas_cantidades_medicamentos[$i];
		$stocktotalmedicamentos = $stocktotalmedicamentos + $listas_stocks_medicamentos[$i];
		$h=$h+1;
		
		if($h>=40)
		{
			$pdf->AddPage();
			$h=0;
			$pdf->Ln(7);
			$pdf->setfont('Helvetica','U',8);
			$pdf->Cell(0,5,'MEDICAMENTOS:',0,0,'L');
			#CABECERAS TABLA
			$pdf->Ln(8);
			$pdf->setfont('Helvetica','b',7);
			$pdf->Cell(15,5,'CODIGO','B',0,'L');
			$pdf->Cell(110,5,'DESCRIPCION','B',0,'L');
			$pdf->Cell(15,5,'U.MED','B',0,'C');
			$pdf->Cell(20,5,'CANT.CONSU.','B',0,'C');
			$pdf->Cell(20,5,'STOCK.ATC.','B',0,'C');
			$pdf->Ln(1);
		}
		
	}

	#TOTALES MEDICAMENTOS
	$pdf->Ln(5);
	$pdf->setfont('Helvetica','',7);
	$pdf->Cell(15,5,'TOTALES','T',0,'L');
	$pdf->Cell(110,5,' ','T',0,'L');
	$pdf->Cell(15,5,' ','T',0,'R');
	$pdf->Cell(20,5,$cantidadtotalmedicamentos,'T',0,'R');
	$pdf->Cell(20,5,$stocktotalmedicamentos,'T',0,'R');

	

	#INSUMOS
	$pdf->Ln(10);
	$pdf->setfont('Helvetica','U',8);
	$pdf->Cell(0,5,'INSUMOS:',0,0,'L');

	#CABECERAS TABLA
	$pdf->Ln(8);
	$pdf->setfont('Helvetica','b',7);
	$pdf->Cell(15,5,'CODIGO','B',0,'L');
	$pdf->Cell(110,5,'DESCRIPCION','B',0,'L');
	$pdf->Cell(15,5,'U.MED','B',0,'C');
	$pdf->Cell(20,5,'CANT.CONSU.','B',0,'C');
	$pdf->Cell(20,5,'STOCK.ATC.','B',0,'C');
	$pdf->Ln(1);

	

	
	for ($i=0; $i < count($lista_insumos); $i++) { 
		$pdf->Ln(5);
		$pdf->setfont('Helvetica','',7);
		$pdf->Cell(15,2,$lista_insumos[$i]['CODIGO'],0,0,'L');
		$pdf->Cell(110,2,$lista_insumos[$i]['NOMBRE'],0,0,'L');
		$pdf->Cell(15,2,$lista_insumos[$i]['UNIDAD'],0,0,'R');
		$pdf->Cell(20,2,$listas_cantidades_insumos[$i],0,0,'R');
		$pdf->Cell(20,2,$listas_stocks_insumos[$i],0,0,'R');

		$cantidadtotalinsumos = $cantidadtotalinsumos + $listas_cantidades_insumos[$i];
		$stocktotalinsumos = $stocktotalinsumos + $listas_stocks_insumos[$i];
		if($h>=40)
		{
			$pdf->AddPage();
			$h=0;
			$pdf->Ln(7);
			$pdf->setfont('Helvetica','U',8);
			$pdf->Cell(0,5,'INSUMOS:',0,0,'L');
			#CABECERAS TABLA
			$pdf->Ln(8);
			$pdf->setfont('Helvetica','b',7);
			$pdf->Cell(15,5,'CODIGO','B',0,'L');
			$pdf->Cell(110,5,'DESCRIPCION','B',0,'L');
			$pdf->Cell(15,5,'U.MED','B',0,'C');
			$pdf->Cell(20,5,'CANT.CONSU.','B',0,'C');
			$pdf->Cell(20,5,'STOCK.ATC.','B',0,'C');
			$pdf->Ln(1);
		}
		
	}

	#TOTALES INSUMOS
	$pdf->Ln(5);
	$pdf->setfont('Helvetica','',7);
	$pdf->Cell(15,5,'TOTALES','T',0,'L');
	$pdf->Cell(110,5,' ','T',0,'L');
	$pdf->Cell(15,5,' ','T',0,'R');
	$pdf->Cell(20,5,$cantidadtotalinsumos,'T',0,'R');
	$pdf->Cell(20,5,$stocktotalinsumos,'T',0,'R');

	

	$pdf->Output();
 ?>