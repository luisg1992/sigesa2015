<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
 
<title>Buscar Formatos</title>
 
<meta name="description" content="">
<meta name="keywords" content="">

  
<link href="../../MVC_Complemento/css/screen.css" rel="stylesheet" type="text/css" media="all">
<link href="../../MVC_Complemento/css/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="../../MVC_Complemento/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="../../MVC_Complemento/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="../../MVC_Complemento/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="../../MVC_Complemento/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="../../MVC_Complemento/css/tipsy.css" rel="stylesheet" type="text/css" media="all">

 
 

<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
<link href="../../MVC_Complemento/css/estilos.css" type="text/css" rel="stylesheet">



<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/FechaMasck.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax.js"></script>


<script type="text/javascript" src="../../MVC_Complemento/js/jquery-ui.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.img.preload.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/hint.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/visualize/jquery.visualize.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/fancybox/jquery.fancybox-1.3.0.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.tipsy.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/custom_blue.js"></script>
<script type="text/javascript" src="jquery-1.3.2.min.js"></script>

<script>
$(document).ready(function(){ 
	$("select").change(function(){
		// Vector para saber cuál es el siguiente combo a llenar

		var combos = new Array();
		combos['cbxespecialida'] = "MED_CODIGO";
		combos['MED_CODIGO'] = "DIASPRO";
 	   combos['DIASPRO'] = "TURNOPRO";		
		 
		// Tomo el nombre del combo al que se le a dado el clic por ejemplo: país
		posicion = $(this).attr("name");
		posicion1 =  $(this).attr("name");
		// Tomo el valor de la opción seleccionada 
		valor = $(this).val()		
		// Evaluó  que si es país y el valor es 0, vacié los combos de submenu y ciudad
		if(posicion == 'cbxespecialida' && valor==0 ){
			$("#MED_CODIGO").html('<option value="%" selected="selected">[MEDICO]</option>')
			$("#DIASPRO").html('<option value="%" selected="selected">[DIAS]</option>')
			$("#TURNOPRO").html('<option value="%" selected="selected">[TURNO]</option>')
//			$("#ciudad").html('	<option value="0" selected="selected">[-Seleccione Valor-]</option>')
		}else{
		/* En caso contrario agregado el letreo de cargando a el combo siguiente
		Ejemplo: Si seleccione país voy a tener que el siguiente según mi vector combos es: submenu  por qué  combos [país] = submenu
			*/
			$("#"+combos[posicion]).html('<option selected="selected" value="%">Cargando...</option>')
			/* Verificamos si el valor seleccionado es diferente de 0 y si el combo es diferente de ciudad, esto porque no tendría caso hacer la consulta a ciudad porque no existe un combo dependiente de este */
			if(valor!="%" || posicion !='MED_CODIGO'){
			// Llamamos a pagina de combos.php donde ejecuto las consultas para llenar los combos
			
			 if(posicion != 'TURNOPRO'  ){
			$("#TURNOPRO").html('<option value="%" selected="selected">[TURNO]</option>')
			
			 }
				$.post("../../MVC_Controlador/HIS/HISC.php?acc=bp3",{
									combo:$(this).attr("name"), // Nombre del combo
									id:$(this).val() // Valor seleccionado
									},function(data){
									$("#"+combos[posicion]).html(data);	
									//Tomo el resultado de pagina e inserto los datos en el combo indicado																				
													})												
			}
		}
	})		
})
</script>


<script language="javascript">
$(document).ready(function() {
	$(".botonExcel").click(function(event) {
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
});
});
</script>
<script type="text/javascript">
function gestionar() {
		 
		if (document.getElementById("op").checked==true){
			document.getElementById("txtfin").value='1'
			
			
		}if (document.getElementById("op").checked==false){
		
			document.getElementById("txtfin").value='0'
			
									}
		
		} 
function validaBusqueda()
{
	if(document.FormularoPrincipal.t1.value!="")
		document.FormularoPrincipal.submit();
	else
		alert("Ingrese dato a buscar.");
	
	if(document.FormularoPrincipal.t2.value!="")
		document.FormularoPrincipal.submit();
	else
		alert("Ingrese dato a buscar.");
}


function anadir(obj) {
  if (obj.checked)
    obj.form1.txtfin.value += obj.value;
  else {
    texto = obj.form1.txtfin.value;
    texto = texto.split(obj.value).join('');
    obj.form1.txtfin.value = texto;
  }  
}
</script>

<style type="text/css">
    #elSel {
		font-size: 20px;
        font-family: Arial, Helvetica, sans-serif; 
        padding-left: 45px;
        background-repeat: no-repeat;
        background-position: 3px 50&#37;;
    }
    #elSel option {
        padding-left: 35px;
        background-repeat: no-repeat;
        background-position: 3px 50%;
    }
    #opcion1 { 
        background-image: url(../../MVC_Complemento/img/excel.gif);
    }
    #opcion2 {
        background-image: url(../../MVC_Complemento/img/word.gif);
    }
    #opcion3 {
        background-image: url(../../MVC_Complemento/img/pdf.gif);
    }
    #opcion4 {
        background-image: url(../../MVC_Complemento/img/icono-explorer.gif);
    }
     
    </style>
    
    <script type="text/javascript">

function getReglas() {
    return document.styleSheets[0].cssRules;
}

function getRegla(selector) {
    var reglas = getReglas();
    for(var i=0, l=reglas.length; i<l; i++) {
        if( (reglas[i].selectorText) && (reglas[i].selectorText==selector) ) {
            return reglas[i];
        }
    }
    return false;
}


var elSel = document.getElementById("elSel");
function actualizaImgSelect() {
    var i = elSel.selectedIndex + 1;
    var laRegla = getRegla("#opcion"+i);    
    if( laRegla ) {
        elSel.style.backgroundImage = laRegla.style.backgroundImage;
    }
}

window.onload = actualizaImgSelect;
elSel.onchange = actualizaImgSelect;




</script>

<script type="text/javascript">
function marcar(source)
{
checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
{
if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
{
checkboxes[i].checked=source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
}
}
}
</script>
</head> 

<body class="control"> 
<form name="FormularoPrincipal" id="FormularoPrincipal" method="post" action="../../MVC_Controlador/HIS/HISC.php?acc=fuas002">
<div id="content1">

	<!-- Begin one column tab content window -->
			<div class="onecolumn">
				<div class="header">
					<span>  Buscar Medìco Programado - HIS</span>
					<div class="switch" style="width:500px">
						 
					</div>
				</div>
				<br class="clear"/>
				<div class="content">
					<div id="tab1_content" class="tab_content hide">
						
					   
				  </div>
<div id="tab2_content" class="tab_content hide"><br class="clear"/>
				  </div>
					<div id="tab3_content" class="tab_content">
						 
						  
<table width="490" height="176" border="1">
						    <tr>
						      <td width="109">Fecha Inicio:</td>
						      <td width="402"><input name="t1" type="text" id="t1" value="<?php echo date('d/m/Y');?>" size="10" />
					        <img src="../../MVC_Complemento/img/calendario.jpg" alt="" name="lanzador" width="16" height="16" border="0" id="lanzador" title="Fecha de Emoción"  />
				          <script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "t1",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador"     // el id del bot&oacute;n que lanzar&aacute; el calendario 
}); 
                          </script> Fecha Final: <input name="t2" type="text" id="t2" value="<?php echo date('d/m/Y');?>" size="10" />
					        <img src="../../MVC_Complemento/img/calendario.jpg" alt="" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha de Emoción"  />
				          <script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "t2",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del bot&oacute;n que lanzar&aacute; el calendario 
}); 
                          </script>
				          <input name="udni" type="hidden" id="udni" value="<?php echo $_REQUEST["udni"];?>"></td>
				        </tr>
						    <tr>
						      <td>Departamento:</td>
						      <td><select name="cbxespecialida" id="cbxespecialida">
				              <option value="%" selected>[.: Ver Todos :.]</option>
						          <?php 

//0,11,12,13,14,15,16,18,19,20,50,51,52,54,6
                                $consulta="select * from DepartamentosHospital where IdDepartamento in (1,2,3,4,5,6,7,8,9,11,17,52,53,55,56,57)
";
			$stmt = $dbh->prepare($consulta);
			$stmt->execute();
			$resultado = $stmt->fetchAll();
			foreach ($resultado as $row2)
			{?>
						          <option value="<?php echo $row2["IdDepartamento"];?>"><?php echo $row2["Nombre"];?></option>
						          <?php }?>
				              </select>
						        <label for="verimpreso"></label></td>
				        </tr>
						    <tr>
						      <td  >Especialidad:</td>
						      <td  ><select name="MED_CODIGO" id="MED_CODIGO" class="Combos texto" >
                    <option value="%" disabled>[Especialidad]</option>
                  </select></td>
	        </tr>
						    <tr>
						      <td  >Medico:</td>
						      <td  ><select name="DIASPRO" class="Combos texto alinear" id="DIASPRO" >
						        <option value="%" disabled>[Medico]</option>
					          </select></td>
	        </tr>
						    <tr>
						      <td  >Turno:</td>
						      <td  > <label for="TURNOS"></label>
						        <select name="TURNOS" id="TURNOS">
						          <option value="%" selected>TODOS</option>
						          <option value="%MAÑA%">MAÑANA</option>
						          <option value="%TARDE%">TARDE</option>
</select>
					          <input type="hidden" name="verimpreso" id="verimpreso"></td>
	        </tr>
						    <?php /*?><tr>
						      <td colspan="2"  >&nbsp;</td>
	        </tr>
						    <tr>
						      <td colspan="2"  >Ver Impresos: &nbsp;&nbsp;&nbsp;&nbsp;
                              <input name="verimpreso" type="checkbox" id="verimpreso"> </td>
	        </tr><?php */?>
						    <tr>
						      <td colspan="2" align="center"><input type="button" name="button" id="button" value="    Buscar   " onClick="validaBusqueda()"></td>
					        </tr>
				      </table>
	
                 
                  </div>
  </div>
                
                
			</div>
			<!-- End one column tab content window -->
            
                <!-- Begin one column window --><!-- End one column window -->
            
  </div> 	
   </form>    
   
   
   					 
         <form name="GuardarFuas"  id="GuardarFuas" method="post" action="../../MVC_Controlador/HIS/HISC.php?acc=fuas003">
         
         <?php 
		 
		 	 
 
if($resultado1!=NULL){			 
            ?>
         <input type="submit" name="button2" id="button2" value="Guardar - Genrar HIS">
          
       
<br>
                         <table border="1"   cellpadding="0" cellspacing="0" class="data">
                <thead>
                  <tr class="header">
                    <th  style="width:2%"> Nº                    </th>
                     
                     <th  style="width:15%">Apellidos y Nombre</th>
                     <th   style="width:5%">DNI </th>
                      
                    <th   style="width:8%">Fecha.. </th>
                     
                    <th   style="width:3%">Departamento</th>
                    <th  style="width:5%">Especialidad</th>
                    <th  style="width:5%">Consultorio</th>
                    <th  style="width:5%">Horario </th>
                    <th  style="width:5%">Imp.HIS <input type="checkbox" onClick="marcar(this);" /></th>
                   
                    <th  style="width:5%">Estado </th>
                  </tr>
                </thead>
            
                <tbody>
                    <?php 
            
			$i=1;
			foreach ($resultado1 as $item){
            ?>


 


                  <tr>
                    <td><?php echo $i; //$item["IdCuentaAtencion"];?>  <input name="IdProgramacion<?php echo $i;?>" type="hidden" id="IdProgramacion<?php echo $i;?>" value="<?php echo $item["IdProgramacion"]; ?>"></td>
                    
                   <td  ><?php echo $item["Apepatmedico"];?> <?php echo $item["Apematmedico"];?> <?php echo $item["NombreMedico"];?> </td>
                   <td><?php echo $item["DNI"];   ?></td>
                    
                   <td align="left" bgcolor="#CCFFFF"><strong><?php echo vfecha(substr($item["Fecha"],0,10));?> </strong></td>
                   <td align="left"  ><?php echo $item["NombreDepartamen"];?> </td>
                   <td align="left"><?php echo $item["NombreEspecialidad"];?> </td>
                   <td align="left"><?php echo $item["NombreServcio"];?></td>
                   <td align="center"><?php echo $item["HoraInicio"];?>-<?php echo $item["HoraFin"];?></td>
                   <td align="center"><input type="checkbox" name="estado<?php echo $i;?>" id="estado<?php echo $i;?>" />
                    <input name="contador" type="hidden" id="contador" value="<?php echo $i;?>"></td>
                    <td align="center">   <a href="../../MVC_Controlador/HIS/HISC.php?acc=fuas004&IdProgramacion=<?php echo $item["IdProgramacion"]; ?>" target="_blank"><img src="../../MVC_Complemento/img/home.gif" width="16" height="16"></a>                      <?php  if($item["Usuario"]=='1'){ echo "<font color='#66FF00'>Impreso</font>";}else{ echo "No Impreso";}?></td>
                  </tr>
           
           
              <?php
                 $i++;   
             }
            ?>
                  
                </tbody>
</table>
          <input type="submit" name="button2" id="button2" value="Guardar - Genrar HIS">   
         <br>
            
              <?php
              
             }else {
				 
			
            ?>
            
            
            No existe Informacion 
            <?php
             
				 }
            ?>  </form>   
</body>

</html> 

