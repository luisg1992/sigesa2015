    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<link rel="stylesheet" href="../../MVC_Complemento/css/boton.css">
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }
		</style>
		<script>
						
			$(document).ready(function() {
				$('.Contenedor_Reporte_I').hide();
				
				/* Da Formato correcto al easyui  */
				
				$('.easyui-datebox').datebox({
					formatter : function(date){
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
					},
					parser : function(s){

						if (!s) return new Date();
						var ss = s.split('-');
						var y = parseInt(ss[2],10);
						var m = parseInt(ss[1],10);
						var d = parseInt(ss[0],10);
						if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
							return new Date(y,m-1,d)
						} else {
							return new Date();
						}
					}

				});			

				
									
				function ImprimirListaPacientesEmergencia()
				{
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var Turno=$( "#Turno option:selected" ).val();
					var newWin = window.open();
					$.ajax({
						url: '../../MVC_Controlador/HIS/HISC.php?acc=Imprimir_Lista_Pacientes_Emergencia',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte,
							Turno:Turno
						},
						success:function(impresion)
						{
							newWin.document.write(impresion);
							newWin.document.close();
							newWin.focus();
							newWin.print();
							newWin.close();
						}
					});
					
				}
											
				function Lista_Pacientes(Fecha_Inicial,Fecha_Final,Servicio,Tipo,Turno){
									var dg = $('#Lista_Pacientes').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pagination: true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									multiSort:true,
								    url:'../../MVC_Controlador/HIS/HISC.php?acc=Lista_Pacientes_Emergencia&Fecha_Inicial='+Fecha_Inicial+'&Fecha_Final='+Fecha_Final+'&Turno='+Turno,
									columns:[[
											{field:'IdCuentaAtencion',title:'Nro de Cuenta',width:100,sortable:true},
											{field:'NroHistoriaClinica',title:'Historia Clinica',width:100,sortable:true},
											{field:'ApellidoPaterno',title:'Apellido<br> Paterno',width:150,sortable:true},
											{field:'ApellidoMaterno',title:'Apellido<br> Materno',width:150,sortable:true},
											{field:'PrimerNombre',title:'Nombres',width:150,sortable:true},
											{field:'Nombre',title:'Servicio',width:150,sortable:true},
											{field:'TipoGravedad',title:'Prioridad',width:100,sortable:true},
											{field:'Turno',title:'Turno',width:70,sortable:true},
											{field:'FechaTurno',title:'Fecha de <br>Turno',width:80,sortable:true},
											{field:'FechaIngreso',title:'Fecha de <br>Ingreso',width:80,sortable:true},	
											{field:'HoraIngreso',title:'Hora de <br>Ingreso',width:60,sortable:true},
											{field:'NombreDiagnos',title:'Diag.<br> Ingreso',width:80,sortable:true}												
										]]
									});
									dg.datagrid('enableFilter');
				}		
				
				$("#Generar_Reporte_I").click(function(event) {
					$('#Titulo_Reporte').text('Reporte de Emergencia');
					$('.Contenedor_Reporte_I').show();

					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var Turno=$( "#Turno option:selected" ).val();
						if (Fecha_Inicial_Reporte.length == 0  ||  Fecha_Final_Reporte.length == 0)
						{
							$.messager.alert('SIGESA - Datos Invalido',' Se Ingreso Datos Erroneos o Vacios');
							return false;
						}
						else{
							
								if (Fecha_Inicial_Reporte <= Fecha_Final_Reporte)
								{
								Lista_Pacientes(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Turno);
								}
								else
								{
								$.messager.alert('SIGESA - Rango de Fechas Invalido',' Fecha Final : '+Fecha_Final_Reporte+'  debe ser mayor a Fecha Inicial : '+Fecha_Inicial_Reporte);
								}
						}	
				});
				
								
				
				function Exportar_Reporte_Excel(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Turno)
				{
					$.ajax({
						url: '../../MVC_Controlador/HIS/HISC.php?acc=Exportar_Reporte_Excel',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Inicial:Fecha_Inicial_Reporte,
							Fecha_Final:Fecha_Final_Reporte,
							Servicio:Servicio,
							Tipo:Tipo,
							Turno:Turno
						},

						success:function(impresion)
						{
						 window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
						 e.preventDefault();
						 alert(impresion);
						}
					});

				}
								
				$("#Generar_Reporte_Matricial").click(function(event) {
					ImprimirListaPacientesEmergencia();
				});
				
				$("#Generar_Reporte_Excel").click(function(event) {
					
					var Fecha_Inicial_Reporte = $('#Fecha_Inicial_Reporte_I').datebox('getValue');
					var Fecha_Final_Reporte= $('#Fecha_Final_Reporte_I').datebox('getValue');
					var Turno=$( "#Turno option:selected" ).val();
					Exportar_Reporte_Excel(Fecha_Inicial_Reporte,Fecha_Final_Reporte,Turno);
				});

				
			});
		</script>
		
    </head>
    <body>
		<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
		<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
		<div class="easyui-layout" style="width:100%;height:800px;">
				<div data-options="region:'center',iconCls:'icon-ok'"  style="width:80%;">
					<div class="easyui-tabs" data-options="tabWidth:100,tabHeight:60" style="width:100%;height:100%;">
						<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/lista.png' width='20' height='20'/>
						<br>Listado de<br> Pacientes</span>" style="padding:10px;height:850px">
								<div  style="width:100%;height: 650px;"  class="Contenedor_Reporte_I">
									<BR>
									<table id="Lista_Pacientes"></table>
								</div>
						</div>
						<div title="<span class='tt-inner'><img src='../../MVC_Complemento/img/grafico.png' width='35' height='35'/>
						<br>Grafico</span>" 
						style="padding:10px">
							<div id="Contenedor_Reporte_I" class="Contenedor_Reporte_I" style="width: 900px; height: 650px;">
							<br>
							<p  style="margin:10px"><STRONG>No Aplica esta Función</STRONG></p>
							</div>
						</div>
					</div>
					<style scoped="scoped">
						.tt-inner{
							display:inline-block;
							line-height:12px;
							padding-top:5px;
						}
						.tt-inner img{
							border:0;
						}
					</style>
				</div>
				<div data-options="region:'west',split:true"  style="width:20%;">
				<div class="easyui-tabs" style="width:100%;">
					<div title="Reportes His">
						<div title="Reporte de Emergencia" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:15px;">
								<p style="padding:10px"><strong>Reporte His</strong></p>
								<hr>
								<br>
								<div style="margin-bottom:20px;">
									<span style="font-weight:bold; color:#414141">Fecha Inicio :</span>
									<br>
									<input id="Fecha_Inicial_Reporte_I" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;" value="<?php echo  date("d-m-Y");?>">
								</div>
								<div style="margin-bottom:20px">
									<span style="font-weight:bold; color:#414141">Fecha Final :</span>
									<br>
									<input id="Fecha_Final_Reporte_I" class="easyui-datebox" label="End Date:" labelPosition="top" style="width:100%;" value="<?php echo  date("d-m-Y");?>">
								</div>
								
								
	
								<div style="margin-bottom:10px;margin-top:3px;clear:both">	
									<div style="float:left;margin:5px 0 0 0 ;">
									<strong>Turno</strong>
									</div>
								</div>
								
								<div style="margin-bottom:20px;margin-top:40px;clear:both">	
									<div style="float:left;margin:5px 0 10px 0;">
									<select name="Turno" id="Turno">
									  <option value="1">Mañana</option>
									  <option value="2">Tarde</option>
									</select>
									</div>
								</div>
								
								
						
								<div style="margin-bottom:20px;clear:both;margin-top:50px;margin-left:10px">
									<div style="padding:5px 0;float:left;margin:5px 20px 0 0">
										<!--
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:80px" id="Generar_Reporte_I">Vista Previa<br></a>
										-->
									    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Reporte_Matricial">Generar Reporte</a>
										<!--
									    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="Generar_Reporte_Excel">Generar Excel</a>
									</div>
								</div>								
						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
    </html>



