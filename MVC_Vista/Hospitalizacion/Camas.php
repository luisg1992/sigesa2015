<?php 
	date_default_timezone_set('America/Lima');
	include_once('../../MVC_Modelo/Servicio_SocialM.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ficha Social</title>
	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
	<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

    </style>
	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui2.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>
	<script type="text/javascript">
									$(document).ready(function(){
										// Cargar Las camas Disponibles de los Servicios
										Cargar_Servicios_Camas_Disponibles_y_Ocupadas();

										// Cargar Las camas Disponibles de los Departamentos
										Cargar_Departamentos_Camas_Disponibles_y_Ocupadas();
									$("#btnExportServicios").click(function(e) {
														var htmltable= document.getElementById('Contenido_Excel_Servicios');
														var html = htmltable.outerHTML;
														//add more symbols if needed...
														  while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
														  while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
														  while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
														  while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
														  while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
														  while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
														window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
									});

									$("#btnExportDepartamentos").click(function(e) {

														var htmltable= document.getElementById('Contenido_Excel_Departamentos');
														var html = htmltable.outerHTML;
														//add more symbols if needed...
														  while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
														  while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
														  while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
														  while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
														  while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
														  while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
														window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
									});
									});	

									//Funcion que Cargar Servicios por Camas Disponibles y Ocupadas
									function Cargar_Servicios_Camas_Disponibles_y_Ocupadas()
									{						
										var dg =$('#ServiciosCamasDisponiblesyOcupadas').datagrid({
											/*title:'Camas por Servicios',*/
											iconCls:'icon-edit',
											url:'../../MVC_Controlador/Hospitalizacion/HospitalizacionC.php?acc=MostrarServiciosCamasDisponiblesyOcupadas',
											columns:[[
												{field:'NOMBRE',title:'Servicio / Area',width:400},
												{field:'LIBRES',title:'Camas Libres',width:110},
												{field:'OCUPADAS',title:'Camas Ocupadas',width:110}	
											]]
										});
									}	


									//Funcion que Cargar Departamentos por Camas Disponibles y Ocupadas
									function Cargar_Departamentos_Camas_Disponibles_y_Ocupadas()
									{						
										var dg =$('#DepartamentosCamasDisponiblesyOcupadas').datagrid({
											/*title:'Camas por Departamento',*/
											iconCls:'icon-edit',
											url:'../../MVC_Controlador/Hospitalizacion/HospitalizacionC.php?acc=MostrarDepartamentosCamasDisponiblesyOcupadas',
											columns:[[
												{field:'NOMBRE',title:'Servicio / Area',width:400},
												{field:'LIBRES',title:'Camas Libres',width:110},
												{field:'OCUPADAS',title:'Camas Ocupadas',width:110}	
											]]
										});
									}
	</script>
</head>
<body>
    <h2>Camas de Hospitalizacion</h2>
    <div style="margin:20px 0;"></div>
    <div class="easyui-layout" style="width:100%;height:100%;">
        <div data-options="region:'north'" style="height:60px">
        </div>
        <div data-options="region:'center',split:true" title="Servicios",iconCls:'icon-ok'" style="width:50%;">
        	<div style="padding:5px;float:left;margin:10px 10px 10px 10px">
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" style="width:150px" id="btnExportServicios">Generar Excel<br></a>
			</div>
			<div  style="clear:both"></div>
        	<div  id="Contenido_Excel_Servicios">
        	<table id="ServiciosCamasDisponiblesyOcupadas"></table>
        	</div>
        </div>

		 <div data-options="region:'west',title:'Departamentos',iconCls:'icon-ok'" style="width:50%;">
		 	 <div style="padding:5px;float:left;margin:10px 10px 10px 10px">
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" style="width:150px" id="btnExportDepartamentos">Generar Excel<br></a>
			</div>
			<div  style="clear:both"></div>
			<div  id="Contenido_Excel_Departamentos">
			<table id="DepartamentosCamasDisponiblesyOcupadas"></table>
			</div>
		</div>
    </div>
 
</body>
</html>
	