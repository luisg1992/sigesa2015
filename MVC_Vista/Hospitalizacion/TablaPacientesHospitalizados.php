<?php

include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');
$objPHPExcel = new PHPExcel();

// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
$resultadosEspe=ListarServiciosHospitalizacionM();	
	   							 
 if($resultadosEspe!=NULL){	
	 $hoja=0;
	 $nservicio=0;	
	 for ($ies=0; $ies < count($resultadosEspe); $ies++) {
		 //echo $resultadosEspe[$ies]["Nombre"];
	
	/*if($hoja>0){
		$nueva_hoja = $objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex($hoja); // marcar como activa la nueva hoja
	}*/


$resultados = ListarPacientesHospitalizadosM($resultadosEspe[$ies]["IdServicio"]);	
if($resultados[0]["ServicioActual"]!=NULL){
//PRIMER SERVICIO	
	if($resultados!=NULL){			
		for ($i=0; $i < count($resultados); $i++) {					

			$objPHPExcel->setActiveSheetIndex($hoja)->mergeCells('A'.($nservicio+1).':'.'H'.($nservicio+1));
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($nservicio+1),  $resultados[0]["ServicioActual"]);

			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('A'.($nservicio+2), "Nro");
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('B'.($nservicio+2), "Cuenta");
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('C'.($nservicio+2), "PACIENTE");	 
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('D'.($nservicio+2), "H.C.");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('E'.($nservicio+2), "F.Ing.");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F'.($nservicio+2), "F.EgrAdm");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('G'.($nservicio+2), "Plan");	
			$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('H'.($nservicio+2), "Cama");				
			//$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('I1', "Servicio Actual");	

			 $Plan=substr($resultados[$i]["Plan"], 0 ,5);

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('A'.($nservicio+$i+3), ($i+1));	 

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('B'.($nservicio+$i+3), $resultados[$i]["IdCuentaAtencion"]);

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('C'.($nservicio+$i+3), mb_strtoupper($resultados[$i]["Paciente"]));		

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('D'.($nservicio+$i+3), $resultados[$i]["NroHistoriaClinica"]);

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('E'.($nservicio+$i+3), $resultados[$i]["FechaIngreso"]);

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F'.($nservicio+$i+3), $resultados[$i]["FechaEgresoAdministrativo"]);	

			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('G'.($nservicio+$i+3), $Plan);							
			 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('H'.($nservicio+$i+3), $resultados[$i]["cama"]);	

			  						
			 //$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('I'.($i+2), $resultados[$i]["ServicioActual"]);
		}		 
	}	

	//Establecer la anchura 				  
	//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
	 $objPHPExcel->setActiveSheetIndex($hoja); 
	 $objActSheet = $objPHPExcel->getActiveSheet(); 

	 //El nombre de la hoja actual de las actividades
	 /*$NombreServicio=trim(substr($resultadosEspe[$ies]["Nombre"],0,30)); 
	 $objActSheet->setTitle($NombreServicio); */
	 $objActSheet->setTitle('Servicios'); //Maximum 31 characters allowed in sheet title 

	 //Formato primera Fila 
	$objStyleA1 = $objActSheet ->getStyle('A'.($nservicio+1).':'.'H'.($nservicio+1));
	//Configuración de tipos de letra 
	$objFontA1 = $objStyleA1->getFont(); 
	$objFontA1->setName('Arial'); 
	$objFontA1->setSize(10); 
	$objFontA1->setBold(true);
	//Establecer la alineación 
	$objAlignA1 = $objStyleA1->getAlignment(); 
	$objAlignA1->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	$objAlignA1->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objAlignA1->setWrapText(true); //Ajustar Texto 

	//Formato segunda Fila 
	$objStyleA5 = $objActSheet ->getStyle('A'.($nservicio+2).':'.'H'.($nservicio+2));
	//$objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
	$objFontA5 = $objStyleA5->getFont(); 
	$objFontA5->setName('Arial'); 
	$objFontA5->setSize(10); 
	$objFontA5->setBold(true);
	//Establecer la alineación 
	$objAlignA5 = $objStyleA5->getAlignment(); 
	$objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	$objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objAlignA5->setWrapText(true); //Ajustar Texto 

	//Formato Columna Paciente
	$objStyleC = $objActSheet ->getStyle('C1:'.'C'.($nservicio+2));
	//Configuración de tipos de letra 
	$objFontC = $objStyleC->getFont();
	$objFontC->setName('Arial');
	$objFontC->setSize(8);
	//Establecer la alineación 
	//$objAlignC = $objStyleC->getAlignment(); 
	//$objAlignC->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
	//$objAlignC->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	//$objAlignC->setWrapText(true); //Ajustar Texto 
	$objActSheet->getColumnDimension('A')->setWidth(4);
	$objActSheet->getColumnDimension('C')->setWidth(29);
	$objActSheet->getColumnDimension('E')->setWidth(10);
	$objActSheet->getColumnDimension('F')->setWidth(10);	
		
//FIN PRIMER SERVICIO	-------------------------------------------------------------------------------------------
		
	//$hoja++;
	$nservicio=$nservicio+$i+3;

} 
	    
}}	
	 

//Formato General	
//$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true); 

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
 
header('Content-Type: application/vnd.ms-excel');
$DiaActual=date('dmY');
$NombreExcel='IngresosEgresosHospitalizacion'.$DiaActual;
header('Content-Disposition: attachment;filename='.$NombreExcel.'.xlsx');
//header('Content-Disposition: attachment;filename="IngresosEgresosHospitalizacion.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>



						
  