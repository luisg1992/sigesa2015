<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ordenes para analisis de laboratorio</title>

	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<style>
        html,body { 
        	height: 100%;
        	font-family: Helvetica; 

        }

        #tabla_resultados_kk table input[type="text"]:disabled {
        	background: #dddddd;
        }

		#tabla_resultados_kk table{
            width: 100%;
            border-collapse: collapse;
            text-align: center;
            margin: 0px;
            padding: 0px;
            font-family: Helvetica;
            font-size:13px;
        }
        
        #tabla_resultados_kk table tr{
            margin: 0px;
            padding: 0px;
        }    

        #tabla_resultados_kk table th {
            border:1px dotted #d3d3d3;
            background-color:#eee;
            padding: 3px;
            font-size:11px;
        }

        #tabla_resultados_kk table td {
            border-left:1px dotted #d3d3d3;
            margin: 0px;
            padding: 0px;
        }

        #tabla_resultados_kk  input[type="text"] {
            width: 100% !important;
            outline: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            appearance: none;
            border:none;
            border:1px solid #d3d3d3;
            padding: 2px;
        }
    </style>
	
    
	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>

	<!-- JS: OPERACIONES -->
	<script>
	
   $.extend($("#buscar_fi").datebox.defaults,{
			formatter:function(date){
				var y = date.getFullYear();
				var m = date.getMonth()+1;
				var d = date.getDate();
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
			},
			parser:function(s){
				if (!s) return new Date();
				var ss = s.split('/');
				var d = parseInt(ss[0],10);
				var m = parseInt(ss[1],10);
				var y = parseInt(ss[2],10);
				if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
					return new Date(y,m-1,d);
				} else {
					return new Date();
				}
			}
		});		
	
		$.extend($("#buscar_fi").datetimebox.defaults.rules, { 
			validDate: {  
				validator: function(value, element){ 
					var fechaX=value.split(' ');				 	
					var fecha=fechaX[0];
					
					var date = $.fn.datebox.defaults.parser(fecha);
					var s = $.fn.datebox.defaults.formatter(date);					
					
					//var horaX=fechaX[1].split(':'); // && horaX.length==3	
					
					if(s==fecha && fechaX.length==2){
						return true;
					}else{						
						return false;
					}
				},  
				message: 'Porfavor Seleccione una fecha valida.'  
			}
		});	
		
	$(document).ready(function() {
		
		//EVENTOS
		
		$("#Limpiar_btn").click(function(event) {
			Limpiar();
		});
		
		$("#Buscar_btn").click(function(event) {
			BuscarPorFecha();
		});

		/*$("#GrabarResultados_btn").click(function(event) {
			GrabarDatosResultado();
		});

		$("#imprimirResultado_btn").click(function(event) {
			ImprimirDatosResultado();
		});*/

		$(document).keydown(function(tecla){
            //	TECLA F8
            if (tecla.keyCode == 119) {
                Limpiar();
            }
        });

		//CARGANDO LAS CONFIGURACIONES POR DEFECTO
		$('#busqueda_nhis').textbox('clear').textbox('textbox').focus();
		$("#Detalle_Orden").window('close');
		$('#tabla_orden_detallada_resultados').datagrid('enableCellEditing').datagrid('gotoCell', {
			index: 0,
			field: 'VALORTEXTO',

		});

		$('#tabla_orden_detallada').datagrid({
			rowStyler:function(index,row){
				if (row.NUMERO % 2 == 0){
					return 'background-color:#F2F2F2;';
				}
			}
		});



				
		//BUSCAR POR NUMERO DE HISTORIA
		$("#busqueda_nhis").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numerohistoria 	= $("#busqueda_nhis").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=BusquedaxNumeroHistoriaCLinica&numerohistoria='+numerohistoria+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin+'&IdListItem=<?php echo $_REQUEST["IdListItem"] ?>'});
								
			}

		});

		//BUSCAR POR NUMERO DE CUENTA
		$("#buscar_cuenta").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numerocuenta	= $("#buscar_cuenta").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=BusquedaxNumeroCuenta&numerocuenta='+numerocuenta+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin+'&IdListItem=<?php echo $_REQUEST["IdListItem"] ?>'});
								
			}

		});

		//BUSCAR POR NUMERO DE ORDEN
		$("#buscar_movimi").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numeromovimi	= $("#buscar_movimi").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=BusquedaxNumeroMovimiento&numeromovimi='+numeromovimi+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin+'&IdListItem=<?php echo $_REQUEST["IdListItem"] ?>'});
								
			}

		});

		//BUSCAR POR NOMBRE PACIENTE
		$("#buscar_nombres").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var paciente		= $("#buscar_nombres").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');
				
				
				if(fecha_inicio=='' || fecha_fin==''){
					$.messager.alert('Laboratorio Clinico HNDAC','Ingrese Fecha Inicio y Final para buscar por Apellido');
				}else{
					$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
					$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
					$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=BusquedaxNombrePaciente&paciente='+paciente+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin+'&IdListItem=<?php echo $_REQUEST["IdListItem"] ?>'});
				}

				
								
			}

		});

		/*$("#realiza_detalle").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				$('*[data-value="1"]').focus().select();
			}
		});*/

		//NAVEGACION CON EL TECLADO
            $("#tabla_resultados_kk").on("keydown",".btnClick",function(e) {
              
                //ENTER
                if(e.keyCode == 13) {                
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField + 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //ABAJO
                else if(e.keyCode == 40) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField + 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //ARRIBA
                else if(e.keyCode == 38) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField - 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //IZQUIERDA
                else if(e.keyCode == 37) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField - 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //DERECHA
                else if(e.keyCode == 39) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField + 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }
            });

		//FUNCIONES
		function tabDatagriIngresoResultados()
		{

			
		}
		
		function Limpiar()
		{			
			$("#busqueda_nhis").textbox('clear');
			$("#buscar_movimi").textbox('clear');
			$("#buscar_cuenta").textbox('clear');
			$("#buscar_nombres").textbox('clear');
			$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
			$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
			
			$("#tabla_resultados").datagrid({url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=MostrarMascaraBody&buscar_fi='+ $('#buscar_fi').datetimebox('getValue')+'&buscar_ff='+$('#buscar_ff').datetimebox('getValue')+'&IdListItem=<?php echo  $_REQUEST["IdListItem"] ?>'});		
			
		}
		
		function BuscarPorFecha()
		{		
			$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
			$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
			
			$("#tabla_resultados").datagrid({url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=MostrarMascaraBody&buscar_fi='+ $('#buscar_fi').datetimebox('getValue')+'&buscar_ff='+$('#buscar_ff').datetimebox('getValue')+'&IdListItem=<?php echo  $_REQUEST["IdListItem"] ?>'});		
			
		}
			

	});

	function IngresarResultadosKK(orden,iprod)
		{
			$.ajax({
                url: '../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=ResultadosDetallados',
                type: 'POST',
                dataType: 'json',
                data: {
                    idorden: orden,
                    idproductocpt:iprod
                },
                success:function(data)
                {   
                	$("#tabla_resultados_kk").html(' ');
                	var valor = 1;
                    var content = "<table><tr><th style='width:1%;'>Grupo</th><th style='width:15%;'>Item</th><th style='width:5%;'>Valor Numero</th><th style='width:35%;'>Valor Texto</th><th style='width:11%;'>Valor Combo</th><th style='width:11%;'>Valor Check</th><th style='width:11%;'>Valor Referencial</th><th style='width:11%;'>Metodo</th><th style='display:none;''>idproducto</th><th style='display:none;'>ordenxresultado</th> </tr><tbody>";
                    
                    $.each(data, function(index, val) {
                        
                        if(data[index].GRUPO == null){data[index].GRUPO = " ";}
                        if(data[index].ITEM == null){data[index].ITEM = " ";}
                        if(data[index].VALORNUMERO == null){data[index].VALORNUMERO = " ";}
                        if(data[index].VALORTEXTO == null){data[index].VALORTEXTO = " ";}
                        if(data[index].VALORCOMBO == null){data[index].VALORCOMBO = " ";}
                        if(data[index].VALORCHECK == null){data[index].VALORCHECK = " ";}
                        if(data[index].VALORREFERENCIAL == null){data[index].VALORREFERENCIAL = " ";}
                        if(data[index].METODO == null){data[index].METODO = " ";}
                        
                        content += '<tr>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].GRUPO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].ITEM+'" disabled/></td>';
                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLONUMERO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }

                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLOTEXTO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }

                       
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCOMBO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCHECK+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORREFERENCIAL+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].METODO+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].IDPRODCPT+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].ORDENXRESULTADO+'" disabled/></td>';

                        content += '</tr>';
                    }); 
                    content += '</tbody></table>';

                    $("#tabla_resultados_kk").append(content); 
                }
            });
		}

		function ActualizarResultadosKK(codigo_sr)
		{
			$.ajax({
                url: '../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=OrdenesSinResultado',
                type: 'POST',
                dataType: 'json',
                data: {
                    codigo_sr:codigo_sr
                },
                success:function(data)
                {   
                	$("#tabla_resultados_kk").html(' ');
                	var valor = 1;
                    var content = "<table><tr><th style='width:1%;'>Grupo</th><th style='width:15%;'>Item</th><th style='width:5%;'>Valor Numero</th><th style='width:35%;'>Valor Texto</th><th style='width:11%;'>Valor Combo</th><th style='width:11%;'>Valor Check</th><th style='width:11%;'>Valor Referencial</th><th style='width:11%;'>Metodo</th><th style='display:none;''>idproducto</th><th style='display:none;'>ordenxresultado</th> </tr><tbody>";
                    
                    $.each(data, function(index, val) {
                        
                        if(data[index].GRUPO == null){data[index].GRUPO = " ";}
                        if(data[index].ITEM == null){data[index].ITEM = " ";}
                        if(data[index].VALORNUMERO == null){data[index].VALORNUMERO = " ";}
                        if(data[index].VALORTEXTO == null){data[index].VALORTEXTO = " ";}
                        if(data[index].VALORCOMBO == null){data[index].VALORCOMBO = " ";}
                        if(data[index].VALORCHECK == null){data[index].VALORCHECK = " ";}
                        if(data[index].VALORREFERENCIAL == null){data[index].VALORREFERENCIAL = " ";}
                        if(data[index].METODO == null){data[index].METODO = " ";}
                        
                        content += '<tr>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].GRUPO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].ITEM+'" disabled/></td>';
                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLONUMERO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORNUMERO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }

                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLOTEXTO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" data-value="'+valor+'" disabled/></td>';
                            valor = valor + 1;
                        }

                       
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCOMBO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCHECK+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORREFERENCIAL+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].METODO+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].IDPRODCPT+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].ORDENXRESULTADO+'" disabled/></td>';

                        content += '</tr>';
                    }); 
                    content += '</tbody></table>';

                    $("#tabla_resultados_kk").append(content);
                }
            });
		}

	</script>
<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>    
    
</head>
<body>
	<div class="easyui-panel"  style="width:100%;height:100%;">
		<!-- CONTENEDORES -->
		<div class="easyui-layout" data-options="fit:true">
            
			<!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:true" style="height:10%;">
            	<div class="easyui-layout" data-options="fit:true">
                
                <?php 
				$fecha = date('Y-m-j');
				$nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
				$ayer = date ( 'd/m/Y' , $nuevafecha );				
				?>
            		
					<!-- CONTENEDOR: BUSQUEDAS -->
            		<div data-options="region:'west'" title="Busqueda  -  Consulta de Resultados <?php echo $DescripcionPC ?>" style="width:100%;height:30%;padding:1%;" >
            			<input type="text" class="easyui-textbox" data-options="prompt:'N° H.C.'" id="busqueda_nhis" style="width:70px;">&nbsp;&nbsp;
            			<input type="text" class="easyui-textbox" data-options="prompt:'N° Orden'" style="width:70px;" id="buscar_movimi">&nbsp;&nbsp;
            			<input type="text" class="easyui-textbox" data-options="prompt:'N° Cuenta'" style="width:70px;" id="buscar_cuenta">&nbsp;&nbsp;
            			<input type="text" class="easyui-textbox" data-options="prompt:'Apellidos Paterno'" style="width:130px;" id="buscar_nombres">&nbsp;&nbsp;
            			<input type="text" class="easyui-datetimebox" data-options="prompt:'Fecha y Hora Inicio'" value="<?php echo $ayer ?>" id="buscar_fi" name="buscar_fi" validType="validDate" />&nbsp;&nbsp;
            			<input type="text" class="easyui-datetimebox" data-options="prompt:'Fecha y Hora Final'" value="<?php echo date("d/m/Y h:m:s") ?>" id="buscar_ff" validType="validDate" />&nbsp;&nbsp;
            			<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:100px" id="Buscar_btn">B.PorFechas</a>&nbsp;&nbsp;
            			<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px" id="Limpiar_btn">Limpiar(F8)</a>&nbsp;&nbsp;
                       <!--<!-- <a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" onClick="Imprimir()" style="width:180px">Generar e Imprimir Codigo Barra de Nro Donación</a>-->
            		</div>

            		
            	</div>
            </div>
        	
        	<!-- CONTENEDOR DE RESULTADOS -->
            <div data-options="region:'center'" style="padding:5px;height:40%;">
            	
            	<!-- CONTENEDOR: RESULTADO DE CONSULTA-->
            	<div id="resultados-body" style="height:100%;">
            		<table class="easyui-datagrid" title="Resultados" style="width:100%;height:330px;" data-options="singleSelect:true,collapsible: true, 
            		url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=MostrarMascaraBody&buscar_fi='+ $('#buscar_fi').datetimebox('getValue')+'&buscar_ff='+$('#buscar_ff').datetimebox('getValue')+'&IdListItem=<?php echo  $_REQUEST["IdListItem"] ?>' ,
					onClickRow:function(row){	
						var row = $('#tabla_resultados').datagrid('getSelected');
		    			$('#paciente_d_o').textbox('setText',row.PACIENTE);
						$('#idpaciente_d_o').textbox('setText',row.IDPACIENTE);
						$('#nhpaciente_d_o').textbox('setText',row.NHISCLI);
						$('#tabla_orden_detallada').datagrid({url:'../../MVC_Controlador/Imagenologia/ImagenologiaC.php?acc=DetalleOrdenxIdOrden&idorden='+row.ORDEN+'&IdListItem=<?php echo $_REQUEST["IdListItem"] ?>'});

		 			}
            		" id="tabla_resultados">
				        <thead>
				            <tr>			                
				                <th data-options="field:'IDMOVIMIENTO',rezisable:true,align:'center'">N° Movimiento</th>
								<th data-options="field:'NCUENTA',rezisable:true,align:'center'">N° Cuenta</th>
								<th data-options="field:'ORDEN',rezisable:true,align:'center'">N° Orden</th>
								<th data-options="field:'NHISCLI',rezisable:true,align:'center'">N° H.C.</th>
								<th data-options="field:'CAMA',rezisable:true,align:'center'">Cama</th>
								<th data-options="field:'PACIENTE',rezisable:true">Paciente</th>
								<th data-options="field:'FREGISTRO',rezisable:true">Fecha Registro</th>
								<th data-options="field:'ORDENPUEBRA',rezisable:true">Orden Prueba</th>
								<th data-options="field:'FNACIMIENTO',rezisable:true">Fecha Nacimiento</th>
								<th data-options="field:'SEXO',rezisable:true,align:'center'">Sexo</th>
								<th data-options="field:'EDAD',rezisable:true,align:'center'">Edad</th>
								
								<th data-options="field:'IDPACIENTE',rezisable:true,align:'center',hidden:true">ID</th>

				            </tr>
				        </thead>
				    </table>
            	</div>
            </div>
			<input type="hidden" id="idUsuarioparaSigesaLI" value="<?php echo $_REQUEST['IdEmpleado']?>">
            <!-- CONTENEDOR: DETALLES DE LA ORDEN -->            
            <div data-options="region:'south'" title="Detalles de la Orden" style="height:50%;padding:1%;">
            	<td>Paciente: </td>
            	<td><input type="text" class="easyui-textbox" id="paciente_d_o" style="width:300px;" disabled>&nbsp;&nbsp;&nbsp;</td>
            	<td>IdPaciente:	</td>
            	<td><input type="text" class="easyui-textbox" id="idpaciente_d_o" style="width:60px;" disabled>&nbsp;&nbsp;&nbsp;</td>
            	<td>Numero Historia:</td>
            	<td><input type="text" class="easyui-textbox" id="nhpaciente_d_o" style="width:60px;" disabled>&nbsp;&nbsp;&nbsp;</td>
            	<br><br>
            	<table class="easyui-datagrid" title="Resultados" style="width:100%;height:300px;" data-options="singleSelect:true" id="tabla_orden_detallada">
				        <thead>
				            <tr>			                
				                <th data-options="field:'NUMERO',rezisable:true">N°</th>
								<th data-options="field:'CODIGO',rezisable:true">Codigo</th>
								<th data-options="field:'PUNTOCARGA',rezisable:true,align:'center'">Centro Prod.</th>
								<th data-options="field:'NOMBRE',rezisable:true">Nombre Prueba</th>
								<th data-options="field:'CANTIDAD',rezisable:true,align:'center'">Cantidad</th>
								<th data-options="field:'PRECIO',rezisable:true,align:'right'">Precio</th>
								<th data-options="field:'TOTAL',rezisable:true,align:'right'">Total</th>
								<th data-options="field:'RESULTAD',rezisable:true,align:'center'">Resultado</th>
								<th data-options="field:'ORDEN',rezisable:true,hidden:true">Orden</th>
								<th data-options="field:'IDPRODCPT',rezisable:true,hidden:true">cpt</th>
                                
                                 <th data-options="field:'Informe_Risc',rezisable:true,align:'center',formatter:VerInforme_Risc">Informe_Risc</th>
                                <th data-options="field:'Imagen_Pacs',rezisable:true,align:'center',formatter:VerImagen_Pacs">Imagen_Pacs</th>
								
				            </tr>
				        </thead>
				    </table>
            </div>

        </div>

	</div>
     <script type="text/javascript">
	 
	function VerInforme_Risc(val,row,index){   
	  if(val=='NO'){ //NO HayImagen
		 return '';		 
	   
	  }else{ 	  
		 return '<a onclick=\'VerInforme('+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true">'+val+'</a>'; 
       	
	  }
    }
	
	function VerInforme(index){							
			var ResultInformeFormato_RIS=$('#tabla_orden_detallada').datagrid('getRows')[index].ResultInformeFormato_RIS;
			var CODIGO=$('#tabla_orden_detallada').datagrid('getRows')[index].CODIGO;
			var NOMBRE=$('#tabla_orden_detallada').datagrid('getRows')[index].NOMBRE;	
			
			$('#Detalle_Orden').window('open');
			$('#Detalle_Orden').window('setTitle',CODIGO+' - '+NOMBRE); 
            $("#typeList").panel({
                onBeforeOpen: function () {
                    var url = ResultInformeFormato_RIS;
                    $("#typeIframe").attr("src", url);
                }
            });	
			
			//var googlewin=dhtmlwindow.open("googlebox", "iframe", ResultInformeFormato_RIS, CODIGO+" - "+NOMBRE , "width="+(screen.width*0.70)+",height="+(screen.height*0.50)+",resize=1,scrolling=1,center=1,top=0px", "recal");
	}	
	
	 
	function VerImagen_Pacs(val,row,index){   
	  if(val=='NO'){ //NO HayImagen
		 return '';		 
	   
	  }else{ 	  
		 return '<a onclick=\'VerImagen('+index+');\' class="easyui-linkbutton" iconCls="icon-search" plain="true">'+val+'</a>'; 
       	
	  }
    }
	
	function VerImagen(index){			
			var ResultInformeImagen_PACS=$('#tabla_orden_detallada').datagrid('getRows')[index].ResultInformeImagen_PACS;
			var CODIGO=$('#tabla_orden_detallada').datagrid('getRows')[index].CODIGO;
			var NOMBRE=$('#tabla_orden_detallada').datagrid('getRows')[index].NOMBRE;		
			
			$('#Detalle_Orden').window('open');
			$('#Detalle_Orden').window('setTitle',CODIGO+' - '+NOMBRE); 
            $("#typeList").panel({
                onBeforeOpen: function () {
                    var url = ResultInformeImagen_PACS;
                    $("#typeIframe").attr("src", url);
                }
            });				
			
			//var googlewin=dhtmlwindow.open("googlebox", "iframe", ResultInformeImagen_PACS, CODIGO+" - "+NOMBRE , "width="+(screen.width-55)+",height="+(screen.height*0.50)+",resize=1,scrolling=1,center=1,top=0px", "recal");
	}	
	
	</script>   


	<div id="Detalle_Orden" class="easyui-window" data-options="modal:true,iconCls:'icon-man',footer:'#ft'" style="width:95%;height:95%;">      
        <div title="" id="typeList" data-options="" style="overflow:hidden;height:100%">
            <iframe scrolling="yes" id="typeIframe" frameborder="0"   style="width:100%;height:100%;"></iframe>
        </div>
    </div>   
   
        
    <div id="ft" style="padding:5px;">		
    	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cancel	'" style="width:80px" onclick="$('#tabla_orden_detallada').datagrid('reload');$('#Detalle_Orden').window('close');">Cerrar</a>
        <!--<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:80px" id="GrabarResultados_btn">Guardar</a>
    	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="imprimirResultado_btn">Imprimir</a>-->
    </div>
    
    
    
    
</body>

	
</html>