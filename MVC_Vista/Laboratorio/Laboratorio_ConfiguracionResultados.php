<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Configuracion de Resultados de Laboratorio</title>

	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<style>
        html,body { 
        	height: 100%;
        	font-family: Helvetica; 
        }
    </style>

	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>
    
	<!-- OPERACIONES JS -->
    <script>

    $(document).ready(function() {
    	//FUNCION LIMPIAR
    	function LimpiarTexto()
    	{
    		$("#codigo_resultado").textbox('clear');
			$("#nombre_resultado").textbox('clear');
			$('#tabla_resultados').datagrid('loadData', {"idProductoCpt":0,"rows":[]});
			return false;
    	}

    	//BUSQUEDA DE RESULTADOS
    	function BusquedaResultados()
    	{	
    		//BUSQUEDA POR NOMBRES
    		if ($("#codigo_resultado").textbox('getText').length < 1) {
    			var nombre_resultado = $("#nombre_resultado").textbox('getText');
    			GenerarDataResultado(nombre_resultado,2);    			
    		}

    		//BUSQUEDA POR RESULTADO
    		if ($("#nombre_resultado").textbox('getText').length < 1) {
    			var codigo_resultado = $("#codigo_resultado").textbox('getText');
    			GenerarDataResultado(codigo_resultado,1);    			
    		}
    	}

    	//GENERAR RESULTADOS 
    	function GenerarDataResultado(busqueda,tipo)
    	{	
    		$.ajax({
    			url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=Configuracion_Resultado_Laboratorio',
    			type: 'POST',
    			dataType: 'json',
    			data: {
    				busqueda:busqueda,
					tipo	:tipo
    			},
    			success: function(data)
    			{
    				$('#tabla_resultados').datagrid({
        				data: data
    				});
    				return false;
    			}
    		});
    		
    	}

    	//MODIFICAR RESULTADOS - 1
    	function SeleccionandoResultados()
    	{	
            var row = $('#tabla_resultados').datagrid('getSelected');

            if (row){
            	$("#OrdenXresultado_nuevos").textbox('setText',row.ordenXresultado);
				$("#Codigo_nuevos").textbox('setText',row.Codigo);
				$("#Nombre_nuevos").textbox('setText',row.nombre);
				$("#IdProductoCpt_nuevos").textbox('setText',row.idProductoCpt);
				$("#IdItem_nuevos").textbox('setText',row.idItem);
				$("#Item_nuevos").textbox('setText',row.Item);
				$("#ValorReferencial_nuevos").textbox('setText',row.ValorReferencial);
				$("#Metodo_nuevos").textbox('setText',row.Metodo);
				
				//
				if (row.SoloNumero == 1) {
					$("input[name=SoloNumero_nuevos]:checkbox").prop("checked", true);
				}
				else
				{	
					$("input[name=SoloNumero_nuevos]:checkbox").prop("checked", false);
				}
				if (row.SoloTexto == 1) {
					$("input[name=SoloTexto_nuevos]:checkbox").prop("checked", true);
				}
				else
				{	
					$("input[name=SoloTexto_nuevos]:checkbox").prop("checked", false);
				}
				
                $('#w').window('open');
            }
        }

        //AGREGAR RESULTADO
        function AgregarResultado()
        {
        	var row = $('#tabla_resultados').datagrid('getSelected');
        	if (row){
        		var index = parseFloat(row.ordenXresultado) + parseFloat(1) ;
        		$("#OrdenXresultado_agregar").textbox('setText',index);
        		$("#Codigo_agregar").textbox('setText',row.Codigo);
				$("#Nombre_agregar").textbox('setText',row.nombre);
				$("#IdProductoCpt_agregar").textbox('setText',row.idProductoCpt);
				$("#IdGrupo_agregar").textbox('setText',row.idGrupo);
            	$('#modal_agregar_resultado').window('open');
            	//return false;
            }
            else
            {
            	$.messager.alert('Error','Seleccione un resultado anterior para agregar','error');
            }
        }

        //MODIFICAR RESULTADOS - 2
        function ModificarResultadosXBX()
        {
        	var OrdenXresultado_modificar 	= $("#OrdenXresultado_nuevos").textbox('getText');
			var Codigo_modificar 			= $("#Codigo_nuevos").textbox('getText');
			var Nombre_modificar 			= $("#Nombre_nuevos").textbox('getText');
			var IdProductoCpt_modificar 	= $("#IdProductoCpt_nuevos").textbox('getText');
			var IdItem_modificar 			= $("#IdItem_nuevos").textbox('getText');
			var Item_modificar 				= $("#Item_nuevos").textbox('getText');
			var ValorReferencial_modificar 	= $("#ValorReferencial_nuevos").textbox('getText');
			var Metodo_modificar 			= $("#Metodo_nuevos").textbox('getText');
			var SoloNumero_modificar 		= $("input[name=SoloNumero_nuevos]:checkbox").prop("checked");
			var SoloTexto_modificar 		= $("input[name=SoloTexto_nuevos]:checkbox").prop("checked");

			$.ajax({
				url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=Modificar_Resultado_Laboratorio',
				type: 'POST',
				dataType: 'json',
				data: {
					ordenXresultado: OrdenXresultado_modificar,
					idProductoCpt: IdProductoCpt_modificar,
					idItem: IdItem_modificar,
					ValorReferencial: ValorReferencial_modificar,
					Metodo: Metodo_modificar,
					SoloNumero: SoloNumero_modificar,
					SoloTexto: SoloTexto_modificar
				},
				success: function(data)
				{
					if (data == 'R_1') {
						$('#w').window('close');
						$.messager.alert('Exito','Modificacion Exitosa!');
						BusquedaResultados();
					}
					else
					{
						$.messager.alert('Error','Comuniquese con el administrador','error');
					}
				}
			});
			
        }

        //AGREGAR RESULTADOS - 2
        function AgregarResultadoXBX()
        {	
        	var ordenXresultado_bus			= parseFloat($("#OrdenXresultado_agregar").textbox('getText')) - parseFloat(1);
        	var OrdenXresultado_agregar 	= $("#OrdenXresultado_agregar").textbox('getText');
			var Codigo_agregar 				= $("#Codigo_agregar").textbox('getText');
			var Nombre_agregar 				= $("#Nombre_agregar").textbox('getText');
			var IdProductoCpt_agregar 		= $("#IdProductoCpt_agregar").textbox('getText');
			var IdItem_agregar 				= $("#IdItem_agregar").textbox('getText');
			var ValorReferencial_agregar 	= $("#ValorReferencial_agregar").textbox('getText');
			var Metodo_agregar 				= $("#Metodo_agregar").textbox('getText');
			var Item_agregar 				= $("#Item_agregar").combobox('getText');
			var IdGrupo_agregar				= $("#IdGrupo_agregar").textbox('getText');
			var SoloNumero_agregar 			= $("input[name=SoloNumero_agregar]:checkbox").prop("checked");
			var SoloTexto_agregar 			= $("input[name=SoloTexto_agregar]:checkbox").prop("checked");

			$.ajax({
				url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=AgregarResultados',
				type: 'POST',
				dataType: 'json',
				data: {
					ordenXresultado_bus			: ordenXresultado_bus,
					OrdenXresultado_agregar 	: OrdenXresultado_agregar,
					Codigo_agregar 				: Codigo_agregar,
					Nombre_agregar 				: Nombre_agregar,
					IdProductoCpt_agregar 		: IdProductoCpt_agregar,
					IdItem_agregar 				: IdItem_agregar,
					ValorReferencial_agregar 	: ValorReferencial_agregar,
					Metodo_agregar 				: Metodo_agregar,
					Item_agregar 				: Item_agregar,
					IdGrupo_agregar 			: IdGrupo_agregar,
					SoloNumero_agregar 			: SoloNumero_agregar,
					SoloTexto_agregar 			: SoloTexto_agregar
				},
				success: function(data)
				{
					if (data == 'R_1') {
						$('#modal_agregar_resultado').window('close');
						$.messager.alert('Exito','Resultado agregado!');
						BusquedaResultados();
					}
					else
					{
						$.messager.alert('Error','Comuniquese con el administrador','error');
					}
				}
			});
			


        }

        function EliminarRegistros()
        {
        	var row = $('#tabla_resultados').datagrid('getSelected');
        	if (row){
        		var ordenXresultado_elimimar 	= row.ordenXresultado;
				var idProductoCpt_elimimar 		= row.idProductoCpt;

				$.ajax({
					url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=EliimarResultados',
					type: 'POST',
					dataType: 'json',
					data: {
						ordenXresultado_elimimar: ordenXresultado_elimimar,
						idProductoCpt_elimimar	: idProductoCpt_elimimar
					},
					success: function(data)
					{
						if (data == 'R_1') {
							$.messager.alert('Exito','Eliminacion Exitosa!');
							BusquedaResultados();
						}
						else
						{
							$.messager.alert('Error','Comuniquese con el administrador','error');
						}
					}
				});
				
        	}
        	else
            {
            	$.messager.alert('Error','Seleccione un resultado ha eliminar','error');
            }
        }

        //EVENTOS
        
    	$("#Limpiar").click(function(event) {
    		LimpiarTexto();
    	});

    	$("#BuscarGeneral").click(function(event) {
    		BusquedaResultados();
    	});

    	$("#EditarRow").click(function(event) {
    		
			SeleccionandoResultados();
    	});

    	$("#InsertarRow").click(function(event) {
    		AgregarResultado();
    	});

    	$("#AgregarResultadoBtn").click(function(event) {
    		AgregarResultadoXBX();
    	});

    	$("#codigo_resultado").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				BusquedaResultados();
			}
		});

		$("#nombre_resultado").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				BusquedaResultados();
			}
		});

		$("#ModificarResultadoBtn").click(function(event) {
			ModificarResultadosXBX();
		});

		$("#EliminarRow").click(function(event) {
			EliminarRegistros();
		});
    	
    });    	
    </script>
</head>
<body>
	<!-- PANEL GENERAL -->
	<div class="easyui-panel" title="Configuracion de Resultados de Laboratorio" style="width:100%;height:100%;">
		
		<!-- CONTENEDORES -->
		<div class="easyui-layout" data-options="fit:true">
            
			<!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:true" style="height:8%;padding:1%;">
            	<td>Codigo:</td>
            	<td><input type="text" class="easyui-textbox" style="width:70px;" id="codigo_resultado"></td>
            	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            	<td>Nombre:</td>
            	<td><input type="text" class="easyui-textbox" style="width:350px;" id="nombre_resultado"></td>
            	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            	<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px" id="BuscarGeneral">Buscar</a></td>
            	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            	<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:80px" id="Limpiar">Limpiar</a></td>
            	
            </div>
        	
        	<!-- CONTENEDOR DE RESULTADOS -->
            <div data-options="region:'center'">
            	<div id="resultados-body" style="margin: 0 auto;padding:2%;">
            		<table class="easyui-datagrid" title="Resultados" style="width:100%;height:auto;" data-options="singleSelect:true,footer:'#ft'" id="tabla_resultados">
				        <thead>
				            <tr>			                
				                <th data-options="field:'ordenXresultado',rezisable:true">ordenXresultado</th>
								<th data-options="field:'Codigo',rezisable:true">Codigo</th>
								<th data-options="field:'nombre',rezisable:true">nombre</th>
								<th data-options="field:'idProductoCpt',rezisable:true">idProductoCpt</th>
								<th data-options="field:'idItem',rezisable:true">idItem</th>
								<th data-options="field:'Item',rezisable:true">Item</th>
								<th data-options="field:'ValorReferencial',rezisable:true">ValorReferencial</th>
								<th data-options="field:'Metodo',rezisable:true">Metodo</th>
								<th data-options="field:'SoloNumero',rezisable:true,align:'center'">SoloNumero</th>
								<th data-options="field:'SoloTexto',rezisable:true,align:'center'">SoloTexto</th>
								<th data-options="field:'idGrupo', rezisable:true, hidden:true">Idgrupo</th>
				            </tr>
				        </thead>
				    </table>
				    <div id="ft" style="padding:2px 5px;">
				        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" id="InsertarRow">Agregar</a>
				        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" id="EditarRow">Editar</a>
				        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" id="EliminarRow">Eliminar</a>
				    </div>
            	</div>
            </div>
        </div>
	</div>
	
	<!-- MODAL MODIFICAR RESULTADO -->
	<div id="w" class="easyui-window" title="Operaciones" data-options="modal:true,closed:true" style="width:500px;height:auto;padding:10px;">
        <table cellpadding="5"><tr>
        	<td>OrdenXresultado: 	</td>
        	<td><input type="text" class="easyui-textbox" id="OrdenXresultado_nuevos" style="width:200px;"></td>
        </tr>
        <tr>
        	<td>Codigo: 			</td>
        	<td><input type="text" class="easyui-textbox" id="Codigo_nuevos" style="width:200px;" disabled></td>
        </tr>
        <tr>
        	<td>Nombre: 			</td>
        	<td><input type="text" class="easyui-textbox" id="Nombre_nuevos" style="width:200px;" disabled></td>
        </tr>
        <tr>
        	<td>IdProductoCpt: 		</td>
        	<td><input type="text" class="easyui-textbox" id="IdProductoCpt_nuevos" style="width:200px;" disabled></td>
        </tr>
        <tr>
        	<td>IdItem: 			</td>
        	<td><input type="text" class="easyui-textbox" id="IdItem_nuevos" style="width:200px;"></td>
        </tr>
        <tr>
        	<td>Item: 				</td>
        	<td>
				<input id="Item_nuevos" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=MostrarLabItemsC',
				        onSelect: function(rec){
        				    $('#IdItem_nuevos').textbox('setText',rec.id);
        				}
				" style="width:200px;">
        	</td>
        </tr>
        <tr>
        	<td>ValorReferencial: 	</td>
        	<td><input type="text" class="easyui-textbox" id="ValorReferencial_nuevos" style="width:200px;"></td>
        </tr>
        <tr>
        	<td>Metodo: 			</td>
        	<td><input type="text" class="easyui-textbox" id="Metodo_nuevos" style="width:200px;"></td>
        </tr>
        <tr>
        	<td>SoloNumero: 		</td>
        	<td><input type="checkbox" name="SoloNumero_nuevos" ></td>
        </tr>
        <tr>
        	<td>SoloTexto: 			</td>
        	<td><input type="checkbox" name="SoloTexto_nuevos" ></td>
        </tr></table>

        <div style="text-align:right;">
        	<a class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:80px" id="ModificarResultadoBtn">Grabar</a>
        	<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#w').window('close');" style="width:80px">Cancel</a>
        </div>
    </div>

    <!-- MODAL AGREGAR RESULTADO -->
    <div id="modal_agregar_resultado" class="easyui-window" title="Nuevo Registro" data-options="modal:true,closed:true" style="width:500px;height:auto;padding:10px;">
    	<table cellpadding="5">
    		<tr>
	        	<td>OrdenXresultado: 	</td>
	        	<td><input type="text" class="easyui-textbox" id="OrdenXresultado_agregar" style="width:200px;" disabled="true"></td>
	        </tr>
	        <tr>
	        	<td>Codigo: 			</td>
	        	<td><input type="text" class="easyui-textbox" id="Codigo_agregar" style="width:200px;" disabled></td>
	        </tr>
	        <tr>
	        	<td>Nombre: 			</td>
	        	<td><input type="text" class="easyui-textbox" id="Nombre_agregar" style="width:200px;" disabled></td>
	        </tr>
	        <tr>
	        	<td>IdProductoCpt: 		</td>
	        	<td><input type="text" class="easyui-textbox" id="IdProductoCpt_agregar" style="width:200px;" disabled></td>
	        </tr>
	        <tr style="display:none;">
	        	<td>IdGrupo: 			</td>
	        	<td><input type="text" class="easyui-textbox" id="IdGrupo_agregar" style="width:200px;"></td>
	        </tr>
	        <tr>
	        	<td>Item: 			</td>
	        	<td><input type="text" class="easyui-textbox" id="IdItem_agregar" style="width:200px;"></td>
	        </tr>
	        <tr>
	        	<td>Nombre Item: 				</td>
	        	<td>
	        		<input id="Item_agregar" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=MostrarLabItemsC',
				        onSelect: function(rec){
        				    $('#IdItem_agregar').textbox('setText',rec.id);
        				}
				" style="width:200px;">
	        	</td>
	        </tr>
	        <tr>
	        	<td>ValorReferencial: 	</td>
	        	<td><input type="text" class="easyui-textbox" id="ValorReferencial_agregar" style="width:200px;"></td>
	        </tr>
	        <tr>
	        	<td>Metodo: 			</td>
	        	<td><input type="text" class="easyui-textbox" id="Metodo_agregar" style="width:200px;"></td>
	        </tr>
	        <tr>
	        	<td>SoloNumero: 		</td>
	        	<td><input type="checkbox" name="SoloNumero_agregar" ></td>
	        </tr>
	        <tr>
	        	<td>SoloTexto: 			</td>
	        	<td><input type="checkbox" name="SoloTexto_agregar" ></td>
	        </tr>
    	</table>
    	<div style="text-align:right;">
        	<a class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:80px" id="AgregarResultadoBtn">Agregar</a>
        	<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#modal_agregar_resultado').window('close');" style="width:80px">Cancel</a>
        </div>
    </div>
</body>
</html>