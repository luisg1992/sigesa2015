﻿<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
	<title>Muestras Anatomia Patologica</title>
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/metro/easyui.css">	    
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">              
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>         
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
</head>
<style>
    html,body { 
        padding: 0px;
        margin: 0px;
        height: 100%;
        font-family: Helvetica;            
    }

    textarea {
        resize: none;
    }

    #new {
        display: inline-block;
    }

    #edit {
        display: inline-block;
    }

    #imprimir {
        display: inline-block;
    }
</style> 

<script>
 $(document).ready(function() {

    //Parametros de Inicio 

    function LimpiarRegistros(){ 
        $('#codigobusqueda').textbox('clear').textbox('textbox').focus();    
        $("#ncbusqueda").textbox('clear');
        $("#npbusqueda").textbox('clear');
        $("#hcbusqueda").textbox('clear');
    }

    function LimpiarRegistrosVentana(){ 
        $('#fnacimiento').textbox('readonly',false);
        $('#codigoap').textbox('readonly',false);
        $('#paciente').textbox('readonly',false);
        $('#numeroc').textbox('readonly',false);
        $('#direccion').textbox('readonly',false);
        $("input[type=radio]").attr('disabled', false);                    
        $("#particular").linkbutton('enable');           
        $('#codigoap').textbox('clear').textbox('textbox').focus();    
        $("#numeroc").textbox('clear');
        $("#historia").textbox('clear');
        $("#sexo").textbox('clear');
        $("#paciente").textbox('clear');
        $("#edad").textbox('clear');
        $("#direccion").textbox('clear');
        $("#fnacimiento").textbox('clear');
        $("#direccion").textbox('enable');
        $("#fnacimiento").textbox('enable');
        $("#numeroc").textbox('enable');
        $("#historia").textbox('enable');
        $('#especimen').textbox('clear');
        $('#salapiso').combobox('setValue', '');
        $('#medicos1').combobox('setValue', '');
        $('#medicos2').combobox('setValue', '');
        $('input:radio[value=Citologico]').prop('checked',true);        
        $("#motivo").val('');
        $("#otroscito").val(''); 
        $("#diagcito").val(''); 
        $("#desccito").val(''); 
        $("#diagqui").val('');
        $("#descqui").val('');
        $('input[name="fc"]').text('');
        $('input[name="mte"]').text('');
        $('#citologicoopciones').show();
        $('#calidad').combobox('enable');
        $('#endocervicales').combobox('enable');                                       
        $('#categorizacion').combobox('enable');
        $('#tipoespecifico').combobox('enable');
        $('input[type="checkbox"]').prop('checked',false);
        $('#divmotivo').hide();
        $('#citologico2').hide();
        $('#quirurgico').hide();
        $('#citologico1').show();
        $('#otros').show();
        $('#tipoespecifico').combobox('setValue', '1');
        $('#endocervicales').combobox('setValue', '1');
        $('#categorizacion').combobox('setValue', '1');
        $('#calidad').combobox('setValue', '1');
        $('label[id*="idpac"]').text('');
        //$('#citologico1').show();
        //$('#tipoespecifico').combobox('enable');
        //$("#anormalidad").show();
        /*$('#medicos1').combobox('setValue', '');
        $('#medicos2').combobox('setValue', '');
        /*$('#calidad').combobox('enable');
        $('#tipoespecifico').combobox('enable');
        $('#tipoespecifico').combobox('setValue', '1');
        $('#endocervicales').combobox('setValue', '1');
        $('#categorizacion').combobox('setValue', '1');*/
        
        /*if($('input:radio[value=Citologico]').is(':checked')==true){
            $("#quirurgico").hide();
            $("#citologicocompleto").show();
            $('#calidad').combobox('enable');
            $('#endocervicales').combobox('enable');                                       
            $('#categorizacion').combobox('enable');
        }
        /*$('input[type="checkbox"]').prop('checked',false);        
        $('#citologico1').show();
        $('#otros').show();
        $('#divmotivo').hide();
        $('#citologico2').hide();
        $('#endocervicales').combobox('enable');                                        
        $('#categorizacion').combobox('enable');
        $("#motivo").val('');
        $("#otroscito").val(''); 
        $("#diagcito").val(''); 
        $("#desccito").val(''); 
        $("#diagqui").val('');
        $("#descqui").val('');
        $('input[name="mte"]').text('');*/      
    }
    
    function ListarRegistros(){
        $.ajax({
            url: '../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ListarRegistro',                    
            type: 'POST',
            dataType: 'json',                
            success:function(data){                
                $("#busqueda").datagrid({
                    data
                });                       
            }
        });                
    }

    LimpiarRegistros();
    LimpiarRegistrosVentana();
    ListarRegistros();

    var fh_temp  =   new Date();
    var fh = fh_temp.toISOString().slice(0,10).replace(/-/g,"-");
    $('#fregistro').datebox('setValue', fh);

 	$("#citologico2").hide();
 	$("#quirurgico").hide();
    $("#divmotivo").hide();    
 	$('#calidad').combobox('setValue', '1');
    $('#tipoespecifico').combobox('setValue', '1');
 	$('#endocervicales').combobox('setValue', '1');
 	$('#categorizacion').combobox('setValue', '1');
 	$('#inflacionestado').hide();
   	

 	$('input[type="radio"]').click(function(){
        if($(this).attr("value")=="Citologico"){
            $("#quirurgico").hide();
$("#citologico2").hide();
$('#tipoespecifico').combobox('setValue', '1');
            $("#citologicoopciones").show();
            $("#citologico1").show();
            $("#otros").show();
            $('#calidad').combobox('enable');
            $('#endocervicales').combobox('enable');                                       
            $('#categorizacion').combobox('enable');
        }
        if($(this).attr("value")=="Quirurgico"){
        	$("#quirurgico").show(); 
            $("#citologico1").hide();
$("#quirurgico").hide();
    $("#divmotivo").hide();   
            $("#citologicoopciones").hide();
            $("#otros").hide();
            $('#calidad').combobox('disable');
            $('#endocervicales').combobox('disable');                                       
            $('#categorizacion').combobox('disable');                    
        }
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    function MostrarDatosxHistoria(HC)   
        {   
            var hc = $("#historia").textbox('getText');         
            $.ajax({
                url: '../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=DatosxHC',                    
                type: 'POST',
                dataType: 'json',
                data: {
                    HC:hc                   
                },
                success:function(data){
                    if (data.dato == 'bad') {
                        $.messager.alert('Error','No se encontro ningun paciente asociado a este numero de historia clinica!','Error');
                        $("#historia").textbox('clear');
                        $("#historia").focus();
                        return 0;
                    }
                    else{
                    $("#paciente").textbox('setValue',data[0].NOMBREPACIENTE);                    
                    $("#direccion").textbox('setValue',data[0].DIRECCION);
                    $("#edad").textbox('setValue',data[0].EDAD);
                    $("#sexo").textbox('setValue',data[0].DESCRIPCION);
                    $("#fnacimiento").textbox('setValue',data[0].FECHANACIMIENTO);
                    $("#numeroc").textbox('setValue',data[0].IDCUENTAATENCION);
                    $("#idpac").text(data[0].IDPACIENTE);
                    }                                                                                                            
                }
        });                
    }

    function MostrarDatosxCuenta()   
        {   
            var cuenta = $("#numeroc").textbox('getValue');         
            $.ajax({
                url: '../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=DatosxCuenta',                    
                type: 'POST',
                dataType: 'json',
                data: {
                    Cuenta:cuenta                   
                },
                success:function(data){
                if (data.dato == 'bad') {
                        $.messager.alert('Error','No se encontro ningun paciente asociado a este numero de cuenta!','Error');
                        $("#numeroc").textbox('clear');
                        $("#numeroc").focus();
                        return 0;
                    }
                else{
                    $("#paciente").textbox('setValue',data[0].NOMBREPACIENTE);
                    $("#direccion").textbox('setValue',data[0].DIRECCION);
                    $("#edad").textbox('setValue',data[0].EDAD);
                    $("#sexo").textbox('setValue',data[0].DESCRIPCION);
                    $("#fnacimiento").textbox('setValue',data[0].FECHANACIMIENTO);
                    $("#historia").textbox('setValue',data[0].NROHISTORIACLINICA);
                    $("#idpac").text(data[0].IDPACIENTE);                                                                                                                                    
                    }
                }
        });                
    }    
  
    //Registrar
    
	function RegistrarMuestra(){
                    
        var Codigoap                = $('#codigoap').textbox('getText');
        var FechaRegistro           = $('#fregistro').datebox('getText');            
        var NombrePaciente          = $('#paciente').textbox('getText'); 
        var Sexo                    = $('#sexo').textbox('getText'); 
        var Edad                    = $('#edad').textbox('getText'); 
        var Especimen               = $('#especimen').textbox('getText'); 
        var SalaPiso                = $('#salapiso').combobox('getValue');       
        var Medico1                 = $('#medicos1').combobox('getValue');
        var Medico2                 = $('#medicos2').combobox('getValue');         
        var Endocervicales          = $("#endocervicales").combobox('getValue');
        var Categorizacion          = $("#categorizacion").combobox('getValue');
        var CalidadMuestra          = $("#calidad").combobox('getValue'); 
        var Combinaciones = []
        $("input[name='Combinaciones[]']:checked").each(function ()
            {
                Combinaciones.push($(this).val());
            });
        var OtrosCitologico         = $("#otroscito").val(); 
        var DiagnosticoCitologico   = $("#diagcito").val(); 
        var DescripcionCitologico   = $("#desccito").val(); 
        var DiagnosticoQuirurgico   = $("#diagqui").val();
        var DescripcionQuirurgico   = $("#descqui").val();
        var Motivo                  = $("#motivo").val();
        var IdPaciente              = $('#idpac').text();
        var NumCuenta               = $('#numeroc').textbox('getText');
        var HC                      = $('#historia').textbox('getText');                                 
        
        //RegistrarFechadeDatos
        if($("#motivo").val().length>1 || $("#otroscito").val().length>1 || $("#diagcito").val().length>1 || $("#desccito").val().length>1 || $("#diagqui").val().length>1 || $("#descqui").val().length>1 || ("input[name='Combinaciones[]']:checked")){            
         
        var r1 = new Date();
        var FechaCompletado = r1.toISOString().slice(0,10).replace(/-/g,"-");
                  
        }
        //Validar
        var lcodigoap = $('#codigoap').val().length;
        var lpaciente = $('#paciente').val().length;
        var lsexo     = $('#sexo').val().length;
        var ledad     = $('#edad').val().length; 

        if(lcodigoap < 1) {
            $.messager.alert('Error','Ingrese Codigo de la Muestra','Error');            
            $("#codigoap").focus();
            return false;
        }
        if(lpaciente < 1) {
            $.messager.alert('Error','Ingrese el Nombre del Paciente','Error');            
            $("#codigoap").focus();
            return false;
        }
        if(lsexo < 1) {
            $.messager.alert('Error','Ingrese el Sexo del Paciente','Error');            
            $("#codigoap").focus();
            return false;
        } 
        if(ledad < 1) {
            $.messager.alert('Error','Ingrese la Edad del Paciente','Error');            
            $("#codigoap").focus();
            return false;
        }                    
        
        //Registrar Muesta y TipoE segun sea el caso

        var verespecifico = $("#tipoespecifico").combobox('getValue');
        var vercalidad = $("#calidad").combobox('getValue');

        //Valor Especifico Citologico Quirurgico

        if ($('input[name=tipomuestra]:checked').val() == "Citologico" && verespecifico==2) {
        
            var TipoMuestra = 1;        
            var TipoEspecifico = 3;
            var Combinaciones = "NULL";
        }

        //Valor Especifico Citologico Positivo

        else if ($('input[name=tipomuestra]:checked').val() == "Citologico" && vercalidad==1) {  
            var TipoMuestra = 1;
            var TipoEspecifico = 1;
        } 

        //Valor Especifico Citologico Negativo

        else if ($('input[name=tipomuestra]:checked').val() == "Citologico" && vercalidad==2) {  
            var TipoMuestra = 1;
            var TipoEspecifico = 2;
        }

            //Valor Especifico Citologico Negativo 2
        else if ($('input[name=tipomuestra]:checked').val() == "Citologico" && vercalidad==3) {  
            var TipoMuestra = 1;
            var TipoEspecifico = 2;
        }     
            
            //Valor Especifico Quirurgico
            
        else if ($('input[name=tipomuestra]:checked').val() == "Quirurgico") {
            
            var TipoMuestra = 2;            
            var TipoEspecifico  = 4;
            var Combinaciones = "NULL";       
        }    

        $.ajax({
            url: '../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=RegistrarMuestra',
            type: 'POST',
            dataType: 'json',
            data: {                
                TipoMuestra:TipoMuestra,
                TipoEspecifico:TipoEspecifico,
                NumCuenta:NumCuenta,           
                CodigoAP:Codigoap,
                FechaRegistro:FechaRegistro,
                FechaCompletado:FechaCompletado,
                IdPaciente:IdPaciente,
                NombrePaciente:NombrePaciente,
                Sexo:Sexo,
                Edad:Edad,
                HC:HC,
                Especimen:Especimen,                
                IdServicios:SalaPiso,
                IdMedico1:Medico1,
                IdMedico2:Medico2,
                Categorizacion:Categorizacion,
                CalidadMuestra:CalidadMuestra,
                Endocervicales:Endocervicales,
                Motivo:Motivo,
                Combinaciones:Combinaciones,
                OtrosCitologico:OtrosCitologico,
                DiagnosticoCitologico:DiagnosticoCitologico,
                DescripcionCitologico:DescripcionCitologico,
                DiagnosticoQuirurgico:DiagnosticoQuirurgico,
                DescripcionQuirurgico:DescripcionQuirurgico
            },
            success:function(data){
            if (data != null ) {
                $.messager.alert('Error','Error el codigo de la muestra ya existe!','Error');                
                $("#codigoap").focus();    
                return 0;
                }
            else{                              
            $("#texto_mensaje_error").html(' ');
            $("#texto_mensaje_error").text("Registro Exitoso!");
            $('#dlg1').dialog('open').dialog('center');                          
            $("#codigobusqueda").focus();
            ListarRegistros(); 
            return 0;                              
                }
            }
        });
    }  

    function BusquedaResultados(){   
        
        //BUSQUEDA POR HC
        if ($("#hcbusqueda").textbox('getText').length > 1) {
            var Historia = $("#hcbusqueda").textbox('getText');
            BuscarxTipo(Historia,4);               
        }

        //BUSQUEDA POR CODIGO
        if ($("#codigobusqueda").textbox('getText').length > 1) {
            var Codigo = $("#codigobusqueda").textbox('getText');
            BuscarxTipo(Codigo,3);               
        }

        //BUSQUEDA POR NUMCUENTA
        if ($("#ncbusqueda").textbox('getText').length > 1) {
            var Cuenta = $("#ncbusqueda").textbox('getText');
            BuscarxTipo(Cuenta,2);               
        }

        //BUSQUEDA POR NOMBRE
        if ($("#npbusqueda").textbox('getText').length > 1) {
            var Paciente = $("#npbusqueda").textbox('getText');
            BuscarxTipo(Paciente,1);               
        }
    }

    function BuscarxTipo(busqueda,tipo){ 

        $.ajax({
            url: '../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=BuscarxTipo',                    
            type: 'POST',
            dataType: 'json',
            data: {
                busqueda:busqueda,
                tipo    :tipo
            },
            success:function(data){
            if (data.dato == 'bad') {
                $.messager.alert('Error','No se encontro ningun registro!','Error');
                $("#codigobusqueda").focus();
                return 0;
            }
            else{         
                $("#busqueda").datagrid({data:data});return false;                                  
                }
            }
        });                
    }

    function SeleccionarRegistro(){ 

        var row = $('#busqueda').datagrid('getSelected');            

        if (row){
            $("#codigoap").textbox('setValue',row.CODIGOAP);
            $("#salapiso").combobox('setText',row.SERVICIO);                
            $("#especimen").textbox('setText',row.ESPECIMENES);
            $("#paciente").textbox('setText',row.NOMBREPACIENTE);
            $("#edad").textbox('setText',row.EDAD);
            $("#sexo").textbox('setText',row.SEXO);
            $("#historia").textbox('setText',row.NROHISTORIACLINICA);
            $("#MuestraTipoEspecifico").text(row.TIPOE); 
            $("#fcompletado").text(row.FECHACOMPLETADO);                
            $("#medicos1").combobox('setText',row.M1);
            $("#medicos2").combobox('setText',row.M2);                              
            $("#fregistro").textbox('setText',row.FECHAREGISTRO); 
            $("#Comb").text(row.COMBINACIONES);
            $("#medicomicro").text(row.M1);
            $("#medicomacro").text(row.M2);
            $("#sph").text(row.SERVICIO);

            if(row.SEXO=='NULL'){
                $("#sexo").textbox('clear');  
            }
            else{
                $("#sexo").textbox('setText',row.SEXO);  
            }

            if(row.MOTIVO=='NULL'){
                $("#motivo").val('');    
            }
            else{
                $("#motivo").val(row.MOTIVO);  
            }               
                                             
            if(row.OTROS=='NULL'){
                $("#otroscito").val('');    
            }
            else{
                $("#otroscito").val(row.OTROS);  
            }

            if(row.DIC=='NULL'){
                $("#diagcito").val('');    
            }
            else{
                $("#diagcito").val(row.DIC);  
            }

            if(row.DEC=='NULL'){
                $("#desccito").val('');    
            }
            else{
                $("#desccito").val(row.DEC);  
            }

            if(row.DIQ=='NULL'){
                $("#diagqui").val('');    
            }
            else{
                $("#diagqui").val(row.DIQ);  
            }

            if(row.DEQ=='NULL'){
                $("#descqui").val('');   
            }
            else{
                $("#descqui").val(row.DEQ);  
            }

            if(row.COMBINACIONES=='NULL'){                    
            }
            else{
            var ac = row.COMBINACIONES;
            var array = ac.split("-");
            substring1 = "Candidaspp";
            substring2 = "Herpesvirus";
            substring3 = "Trichomonavaginalis";
            substring4 = "Cambiosenlaflora";
            substring5 = "Actinomycesspp";
            substring6 = "Radiacion";
            substring7 = "Atrofia";
            substring8 = "Inflacion";
            substring9 = "Leve";
            substring10 = "Moderado";
            substring11 = "Severo";
            substring12 = "Diu";
            substring13 = "Statuscelulasglandulares";
            substring14 = "Celulasescamosasatipicasascus";
            substring15 = "celulasescamosasatipicasasch";
            substring16 = "Infeccionporpvh";
            substring17 = "Displasialeve";
            substring18 = "Displasiamorderada";
            substring19 = "Displasiasevera";
            substring20 = "Carcinomadecelulasescamosas";
            substring21 = "Celulasglandularesatipicas";
            substring22 = "Adenocarcinomacervicalinsitu";
            substring23 = "Adenocarcinoma";
            if(array.indexOf(substring1)>-1){
                $("input[name='Combinaciones[]'][value='Candidaspp'").prop("checked", true);
                }

            if(array.indexOf(substring2)>-1){
                $("input[name='Combinaciones[]'][value='Herpesvirus'").prop("checked", true);
                }

            if(array.indexOf(substring3)>-1){
                $("input[name='Combinaciones[]'][value='Trichomonavaginalis'").prop("checked", true);
                }

            if(array.indexOf(substring4)>-1){
                $("input[name='Combinaciones[]'][value='Cambiosenlaflora'").prop("checked", true);
                }

            if(array.indexOf(substring5)>-1){
                $("input[name='Combinaciones[]'][value='Actinomycesspp'").prop("checked", true);
                }

            if(array.indexOf(substring6)>-1){
                $("input[name='Combinaciones[]'][value='Radiacion'").prop("checked", true);
                }

            if(array.indexOf(substring7)>-1){
                $("input[name='Combinaciones[]'][value='Atrofia'").prop("checked", true);
                }

            if(array.indexOf(substring8)>-1){                    
                $("input[name='Combinaciones[]'][value='Inflacion'").prop("checked", true);
                $('#inflacionestado').show();
                }

            if(array.indexOf(substring9)>-1){
                $("input[name='Combinaciones[]'][value='Leve'").prop("checked", true);
                }

            if(array.indexOf(substring10)>-1){
                $("input[name='Combinaciones[]'][value='Moderado'").prop("checked", true);
                }

            if(array.indexOf(substring11)>-1){
                $("input[name='Combinaciones[]'][value='Severo'").prop("checked", true);
                }

            if(array.indexOf(substring12)>-1){
                $("input[name='Combinaciones[]'][value='Diu'").prop("checked", true);
                }

            if(array.indexOf(substring13)>-1){
                $("input[name='Combinaciones[]'][value='Statuscelulasglandulares'").prop("checked", true);
                }

            if(array.indexOf(substring14)>-1){
                $("input[name='Combinaciones[]'][value='Celulasescamosasatipicasascus'").prop("checked", true);
                }

            if(array.indexOf(substring15)>-1){
                $("input[name='Combinaciones[]'][value='celulasescamosasatipicasasch'").prop("checked", true);
                }

            if(array.indexOf(substring16)>-1){
                $("input[name='Combinaciones[]'][value='Infeccionporpvh'").prop("checked", true);
                }

            if(array.indexOf(substring17)>-1){
                $("input[name='Combinaciones[]'][value='Displasialeve'").prop("checked", true);
                }

            if(array.indexOf(substring18)>-1){
                $("input[name='Combinaciones[]'][value='Displasiamorderada'").prop("checked", true);
                }

            if(array.indexOf(substring19)>-1){
                $("input[name='Combinaciones[]'][value='Displasiasevera'").prop("checked", true);
                }

            if(array.indexOf(substring20)>-1){
                $("input[name='Combinaciones[]'][value='Carcinomadecelulasescamosas'").prop("checked", true);
                }

            if(array.indexOf(substring21)>-1){
                $("input[name='Combinaciones[]'][value='Celulasglandularesatipicas'").prop("checked", true);
                }
            
            if(array.indexOf(substring22)>-1){
                $("input[name='Combinaciones[]'][value='Adenocarcinomacervicalinsitu'").prop("checked", true);
                }

            if(array.indexOf(substring23)>-1){
                $("input[name='Combinaciones[]'][value='Adenocarcinoma'").prop("checked", true);
                }
            }

            if(row.NROHISTORIACLINICA=='Particular'){
                $("#numeroc").textbox('disable');
                $("#direccion").textbox('disable');
                $("#fnacimiento").textbox('disable');
                $("#hc").textbox('disable');
            }                

            if(row.NROHISTORIACLINICA!='NULL'){
                var hc = row.NROHISTORIACLINICA;                    
                MostrarDatosxHistoria(hc);
            }

            if (row.TIPO=='Quirurgico') {
                $('#rbq').prop('checked',true);
                $("#quirurgico").show();
                $("#citologicoopciones").hide();                 
                $("#citologico1").hide();
                $('#endocervicales').combobox('disable');                                       
                $('#categorizacion').combobox('disable');
                $("#otros").hide();
                $('#tipoespecifico').combobox('disable');
                $('#calidad').combobox('disable');
                $("#codigoap").textbox('readonly');
                $("#fregistro").textbox('readonly');
                $("#numeroc").textbox('readonly');
                //$("#paciente").textbox('readonly');
                $("#direccion").textbox('readonly');
                $("#fnacimiento").textbox('readonly');
                $("input[type=radio]").attr('disabled', true);                    
                $("#particular").linkbutton('disable');                    
                }

            if (row.TIPOE=='Citologico Positivo') {                    
                $('#tipoespecifico').combobox('disable');
                $('#calidad').combobox('disable');
                $("#codigoap").textbox('readonly');
                $("#fregistro").textbox('readonly');
                //$("#numeroc").textbox('readonly');
                //$("#paciente").textbox('readonly');
                $("#direccion").textbox('readonly');
                $("#fnacimiento").textbox('readonly'); 
                $("input[type=radio]").attr('disabled', true);
                $("#particular").linkbutton('disable');                                     
            }

            if (row.TIPOE=='Citologico Negativo') {
                if(row.CM == '3'){
                    $("#calidad").combobox('setValue','3');    
                }
                else{
                    $("#calidad").combobox('setValue','2');
                }
                $('#citologico1').hide();
                $('#otros').hide();
                $('#calidad').combobox('disable');                 
                $('#divmotivo').show();
                $('#endocervicales').combobox('disable');                                       
                $('#categorizacion').combobox('disable');
                $('#tipoespecifico').combobox('disable');
                $("#codigoap").textbox('readonly');
                $("#fregistro").textbox('readonly');
                $("#direccion").textbox('readonly');
                $("#fnacimiento").textbox('readonly');
                $("input[type=radio]").attr('disabled', true); 
                $("#particular").linkbutton('disable');                                                                             
            }

            if (row.TIPOE=='Citologico Quirurgico') {                    
                $('#citologico1').hide();
                $('#otros').hide();
                $('#divmotivo').hide();
                $('#citologico2').fadeIn(200);
                $('#calidad').combobox('disable');
                $('#endocervicales').combobox('disable');                                       
                $('#categorizacion').combobox('disable');
                $('#tipoespecifico').combobox('setValue','2');
                $('#tipoespecifico').combobox('disable');
                $("#codigoap").textbox('readonly');
                $("#fregistro").textbox('readonly');
                //$("#numeroc").textbox('readonly');
                //$("#paciente").textbox('readonly');
                $("#direccion").textbox('readonly');
                $("#fnacimiento").textbox('readonly');
                $("input[type=radio]").attr('disabled', true);
                $("#particular").linkbutton('disable');
            }
            $('#ventana').dialog('open');
        }
    }

    

    function ModificarMuestra(){
            
        var Codigo                  = $('#codigoap').textbox('getValue');             
        var Especimen               = $('#especimen').textbox('getText');          
        var SalaPiso                = $('#salapiso').combobox('getValue');       
        var Medico1                 = $('#medicos1').combobox('getValue');
        var Medico2                 = $('#medicos2').combobox('getValue');             
        var Categorizacion          = $("#categorizacion").combobox('getValue');
        var CalidadMuestra          = $("#calidad").combobox('getValue'); 
        var Endocervicales          = $("#endocervicales").combobox('getValue');
        var Combinaciones = []
        $("input[name='Combinaciones[]']:checked").each(function ()
            {
                Combinaciones.push($(this).val());
            });
        var OtrosCitologico         = $("#otroscito").val();
        var Motivo                  = $("#motivo").val(); 
        var DiagnosticoCitologico   = $("#diagcito").val(); 
        var DescripcionCitologico   = $("#desccito").val(); 
        var DiagnosticoQuirurgico   = $("#diagqui").val();
        var DescripcionQuirurgico   = $("#descqui").val();
        var IdPaciente              = $('#idpac').text();
        var NumCuenta               = $('#numeroc').textbox('getText');
        var NombrePaciente          = $('#paciente').textbox('getText'); 
        var Sexo                    = $('#sexo').textbox('getText'); 
        var Edad                    = $('#edad').textbox('getText');
        var HC                      = $('#historia').textbox('getText');

        if(HC=='Particular'){
            var HC = 'NULL';
        }

        if(IdPaciente=='' || IdPaciente=='NULL'){
            var IdPaciente = 'NULL';
        } 

        if($("#motivo").val().length>1 || $("#otroscito").val().length>1 || $("#diagcito").val().length>1 || $("#desccito").val().length>1 || $("#diagqui").val().length>1 || $("#descqui").val().length>1 || ("input[name='Combinaciones[]']:checked")){            
            
        var r1 = new Date();
        var FechaCompletado = r1.toISOString().slice(0,10).replace(/-/g,"-");
                      
        }           
    
        $.ajax({
            url: '../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ModificarMuestra',
            type: 'POST',
            dataType: 'json',
            data: {  
                Codigo:Codigo,
                IdPaciente:IdPaciente,
                NumCuenta:NumCuenta,
                NombrePaciente:NombrePaciente,
                HC:HC,
                Sexo:Sexo,
                Edad:Edad,
                Especimen:Especimen,
                IdServicios:SalaPiso,
                IdMedico1:Medico1,
                IdMedico2:Medico2, 
                Motivo:Motivo,             
                Categorizacion:Categorizacion,
                CalidadMuestra:CalidadMuestra,
                Endocervicales:Endocervicales,
                Combinaciones:Combinaciones,
                OtrosCitologico:OtrosCitologico,
                DiagnosticoCitologico:DiagnosticoCitologico,
                DescripcionCitologico:DescripcionCitologico,
                DiagnosticoQuirurgico:DiagnosticoQuirurgico,
                DescripcionQuirurgico:DescripcionQuirurgico,
                FechaCompletado:FechaCompletado
            },
            success:function(data){
            if (data == 'bad') {
            $("#texto_mensaje_error").html(' ');
            $("#texto_mensaje_error").text("Error.");
            $('#dlg1').dialog('open').dialog('center'); 
            $("#codigobusqueda").focus();            
            ListarRegistros();

            return 0;
                }
            else{ 
            $("#texto_mensaje_error").html(' ');
            $("#texto_mensaje_error").text("Modificacion Exitosa!");
            $('#dlg1').dialog('open').dialog('center');
            $("#codigobusqueda").focus();
            ListarRegistros();
            return 0;                              
                }
            }
        });                
    }
      
    //Editar Muestras
    
    $('#busqueda').datagrid({        
        onDblClickRow:function(){
            $('#new').hide(); 
            $('#edit').show();
            $('#imprimir').show();                         
            SeleccionarRegistro();                                       
            $("#editar").click(function(event) {
                ModificarMuestra();
            });
        }          
    }); 

    $('input.inf').on('change', function() {
        $('input.inf').not(this).prop('checked', false);  
    });     

    //Mostrar Checkboxes Inflacion

    $('#inflacion').click(function() {
    	var $this = $(this);

    	if ($this.is(':checked')) {
        	$('#inflacionestado').show(); 
    	} else {
        	$('#inflacionestado').hide();
    	}
	});

    //Buscar Registros x Codigo, Apellido, NHC

    var t = $('#codigobusqueda');
    t.textbox('textbox').bind('keydown', function(e){
       if (e.keyCode == 13){
            BusquedaResultados();
            LimpiarRegistros();            
       }
    });

    var t = $('#npbusqueda');
    t.textbox('textbox').bind('keydown', function(e){
       if (e.keyCode == 13){
            BusquedaResultados();
            LimpiarRegistros();            
       }
    });

    var t = $('#hcbusqueda');
    t.textbox('textbox').bind('keydown', function(e){
       if (e.keyCode == 13){
            BusquedaResultados();
            LimpiarRegistros();            
       }
    });

    //Buscar Datos X Historia o Cuenta

 	var t = $('#historia');
	t.textbox('textbox').bind('keydown', function(e){
	   if (e.keyCode == 13){
	   		MostrarDatosxHistoria();	   		
	   }
	});	

	var t = $('#numeroc');
	t.textbox('textbox').bind('keydown', function(e){
	   if (e.keyCode == 13){
	   		MostrarDatosxCuenta();	   		
	   }
	});	

    var t = $('#muestraeliminar');
    t.textbox('textbox').bind('keydown', function(e){
       if (e.keyCode == 13){
            if($("#muestraeliminar").val().length<1){
            $.messager.alert('Error','Ingrese muestra a eliminar!','Error');    
            }
            else{     
            $.messager.confirm('Mensaje','Esta seguro de eliminar esta muestra?',function(r){  
                if (r){              
                        $('#wd').window('close');
                        //ListarRegistros();
                        EliminarMuestra();                  
                    }            
                });
            }          
       }
    }); 
	
	$("#guardar").click(function(event) {
		RegistrarMuestra();                  		
	});

    $("#Nuevo").click(function(event) {
        $('#ventana').window('open');
        $('#new').show(); 
        $('#edit').hide(); 
        $('#imprimir').hide();                        
    });

    $("#Buscar").click(function(event) {
        BusquedaResultados();
        LimpiarRegistros();
    });

    $("#Limpiar").click(function(event) {       
        LimpiarRegistros();
        ListarRegistros();       
    });  

    $('#ventana').window({        
        onClose:function(){
            LimpiarRegistrosVentana();         
            
        }
    });
   

    //EsconderColumnas
    $(function(){
        $("#busqueda").datagrid({
            onLoadSuccess:function(){
                $('#busqueda').datagrid("hideColumn","EDAD");
                $('#busqueda').datagrid("hideColumn","SEXO");
                $('#busqueda').datagrid("hideColumn","M1");
                $('#busqueda').datagrid("hideColumn","M2");
                $('#busqueda').datagrid("hideColumn","OTROS");
                $('#busqueda').datagrid("hideColumn","DIC");
                $('#busqueda').datagrid("hideColumn","DEC");
                $('#busqueda').datagrid("hideColumn","DIQ");
                $('#busqueda').datagrid("hideColumn","DEQ"); 
                $('#busqueda').datagrid("hideColumn","COMBINACIONES");
                $('#busqueda').datagrid("hideColumn","ENDO");
                $('#busqueda').datagrid("hideColumn","CM");
                $('#busqueda').datagrid("hideColumn","CG");
                $('#busqueda').datagrid("hideColumn","MOTIVO");                  
                }            
            });           
    }); 

    function ExportarExcel(){
        window.open('../../MVC_Vista/Laboratorio/Muestras_Excel.php');
    }

    function EliminarMuestra(){    
    var ME = $('#muestraeliminar').textbox('getText'); 
      
        $.ajax({
                url: '../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=EliminarMuestra',                    
                type: 'POST',
                dataType: 'json',
                data: {
                    IdMuestra:ME                   
                }, 
                success:function(data){
                if (data.dato == 'bad') { 
                $.messager.alert('Error','No se encontro ninguna muestra asociado al codigo ingresado!','Error');                           
                ListarRegistros();
                return 0;
                    }
                else{
                $.messager.alert('Mensaje','Muestra eliminada correctamente!','Mensaje');                                 
                ListarRegistros(); 
                //return 0;                              
                    }
                }

        })
    }
    $("#eliminarok").click(function(event) {
        if($("#muestraeliminar").val().length<1){
            $.messager.alert('Error','Ingrese muestra a eliminar!','Error');    
        }
        else{     
        $.messager.confirm('Mensaje','Esta seguro de eliminar esta muestra?',function(r){  
            if (r){              
                    $('#wd').window('close');
                    //ListarRegistros();
                    EliminarMuestra();                   
                }
            else{
                    $('#wd').window('close');
            }            
            });
        }
    });

    $("#ok").click(function(event) {
        $.messager.confirm('Mensaje','Desea Imprimir?',function(r){  
            if (r){              
                    printPage();  
                    $('#dlg1').dialog('close');
                    $('#ventana').window('close');
                    //$("#botones #cancel").click();                     
                } else { 
                    $('#dlg1').dialog('close'); 
                    $('#ventana').window('close'); 
                    //$("#botones #cancel").click();                  
                }             
            });
    });

    $("#Excel").click(function(event){  
        ExportarExcel(); 
    });

    $("#Eliminar").click(function(event){    
         $('#wd').window('open');
         $('#muestraeliminar').textbox('clear');       
    });

	$("#particular").click(function(event) {  
		$("#historia").textbox('disable');  
		$("#numeroc").textbox('disable');	
		$("#direccion").textbox('disable');   
		$("#fnacimiento").textbox('disable'); return false;	
	}); 
}); 
</script>

<script>

function printPage()
{    
    var Codigoap                = $('#codigoap').textbox('getText');
    var NombrePaciente          = $("#paciente").textbox('getText');
    if ($("#historia").textbox('getText').length > 1) {
        var HC                  = $("#historia").textbox('getText');
    }
    else {
        var HC                  = 'PARTICULAR';
    }    
    var Edad                    = $("#edad").textbox('getText');
    var Sexo                    = $("#sexo").textbox('getText');
    var Dir                     = $("#direccion").textbox('getText');
    var Fn                      = $("#fnacimiento").textbox('getText');
    var Sp                      = $("#salapiso").combobox('getText');
    var Especimen               = $("#especimen").textbox('getText');
    var FechaR                  = $('#fregistro').textbox('getText');
    var FC                      = $('input[name=fc]').text();           
    var Med                     = $("#medicos1").combobox('getText');
    var Med2                    = $("#medicos2").combobox('getText');
    var Categorizacion          = $("#categorizacion").combobox('getText');
    var CalidadMuestra          = $("#calidad").combobox('getText'); 
    var Endocervicales          = $("#endocervicales").combobox('getText');    
    var OtrosCitologico         = $("#otroscito").val(); 
    var DiagnosticoCitologico   = $("#diagcito").val(); 
    var DescripcionCitologico   = $("#desccito").val();
    var DescripcionQuirurgico   = $("#descqui").val();    
    var DiagnosticoQuirurgico   = $("#diagqui").val();
    var Motivo                  = $("#motivo").val();    
    var MTE                     = $("input[name=mte]").text();
    var verespecifico           = $("#tipoespecifico").combobox('getValue');
    var vercalidad              = $("#calidad").combobox('getValue');

    //alert(MTE);return false; 
      
    //alert(verespecifico);return false;

    var diagqt = DiagnosticoQuirurgico; 
    var descqt = DescripcionQuirurgico;
    var diagct = DiagnosticoCitologico;
    var descct = DescripcionCitologico;
    var motivot = Motivo;
    var otrost = OtrosCitologico;

    diagq1 = diagqt.replace(/(?:\r\n|\r|\n)/g, '<br />');
    descq1 = descqt.replace(/(?:\r\n|\r|\n)/g, '<br />');
    DescripcionCitologico = diagct.replace(/(?:\r\n|\r|\n)/g, '<br />');
    DiagnosticoCitologico = descct.replace(/(?:\r\n|\r|\n)/g, '<br />');
    Motivo = motivot.replace(/(?:\r\n|\r|\n)/g, '<br />');
    OtrosCitologico = otrost.replace(/(?:\r\n|\r|\n)/g, '<br />');
    
    /*var rows = document.querySelector('textarea#descqui').value.split("\n").length;
    if(rows>40){
        mySplitResult = DescripcionQuirurgico.split('\n');
        for(i = 0; i < mySplitResult.length; i++){
            if (i < 40) {
            var diagq1 = mySplitResult[i];
            }                 
        }
    }*/

    //alert(rows);return false;

    /*var lines = DescripcionQuirurgico.split("\n");
    var count = lines.length;
    alert(count);return false;*/

    /*var lines = DiagnosticoQuirurgico.split("\n");
    //alert(lines);return false;
    var count = lines.length;
    if(count > 20){
        var diagq1  = DiagnosticoQuirurgico.split("\n",5)  ;
        //var diagq2  =   ;
        
    }
    alert(diagq1);return false*/
    //alert(DescripcionQuirurgico)
    /*mySplitResult = DescripcionQuirurgico.split('\n');    

    for(i = 0; i < mySplitResult.length; i++){        
        
        var diagq1 = mySplitResult[0] + '<br>' + mySplitResult[1] + '<br>' + mySplitResult[2] + '<br>' + mySplitResult[3] + '<br>' + mySplitResult[4] + '<br>' + mySplitResult[5] + '<br>' + mySplitResult[6] + '<br>' + mySplitResult[7] + '<br>' + mySplitResult[8] + '<br>' + mySplitResult[9] + '<br>' + mySplitResult[10] + '<br>' + mySplitResult[11] + '<br>' + mySplitResult[12] + '<br>' + mySplitResult[13] + '<br>' + mySplitResult[14] + '<br>' + mySplitResult[15] + '<br>' + mySplitResult[16] + '<br>' + mySplitResult[17] + '<br>' + mySplitResult[18] + '<br>' + mySplitResult[19] + '<br>' + mySplitResult[20] + '<br>' + mySplitResult[21] + '<br>' + mySplitResult[22] + '<br>' + mySplitResult[23] + '<br>' + mySplitResult[24]+ '<br>' + mySplitResult[25]+ '<br>' + mySplitResult[26]+ '<br>' + mySplitResult[27]+ '<br>' + mySplitResult[28]+ '<br>' + mySplitResult[29] + '<br>' + mySplitResult[30] + '<br>' + mySplitResult[31] + '<br>' + mySplitResult[32] + '<br>' + mySplitResult[33] + '<br>' + mySplitResult[34] + '<br>' + mySplitResult[35]+ '<br>' + mySplitResult[36] + '<br>' + mySplitResult[37] + '<br>' + mySplitResult[38] + '<br>' + mySplitResult[39] +'<br>'+'<br>'+'<br>'+'<br>'+'<br>'+'<br>';
        var diagq2 = mySplitResult[40] + '<br>' + mySplitResult[41] + '<br>' + mySplitResult[42];
        //alert(diagq1);return false;
    //alert("<br /> Element " + i + " of the array is: " + mySplitResult[i]);
    } 

    var descq1                  = "HOLA";
    var descq2                  = "HOLA";*/
    
    /*if($("#diagqui").val().length>4700){
    var diagq1                  = DiagnosticoQuirurgico.substring(0,4700);
    var diagq2                  = DiagnosticoQuirurgico.substring(4700);
    }
    else{
    var diagq1                  = $("#diagqui").val();    
    }
    if($("#descqui").val().length>4700){
    var descq1                  = DescripcionQuirurgico.substring(0,4700);
    var descq2                  = DescripcionQuirurgico.substring(4700);
    }
    else{
    var diagq1                  = $("#descqui").val();    
    }
    //var descq2                  = DescripcionQuirurgico.substring(500);*/
        
    if (document.getElementById('cspp').checked){
        var Cb1 = "<input type='checkbox' name='Combinaciones[]' checked>Candida spp";    
    }
    else{
        var Cb1 = "<input type='checkbox' name='Combinaciones[]'>Candida spp";
    }

    if (document.getElementById('hv').checked){
        var Cb2 = "<input type='checkbox' name='Combinaciones[]' checked>Herpes Virus";    
    } 
    else{
        var Cb2 = "<input type='checkbox' name='Combinaciones[]'>Herpes Virus";
    }

    if (document.getElementById('tv').checked){
        var Cb3 = "<input type='checkbox' name='Combinaciones[]' checked>Trichomona Vaginalis";    
    }
    else{
        var Cb3 = "<input type='checkbox' name='Combinaciones[]'>Trichomona Vaginalis";
    }

    if (document.getElementById('cfs').checked){
        var Cb4 = "<input type='checkbox' name='Combinaciones[]' checked>Cambios en la flora sugestivos de vaginosis bacteriana";    
    }
    else{
        var Cb4 = "<input type='checkbox' name='Combinaciones[]'>Cambios en la flora sugestivos de vaginosis bacteriana";
    }

    if (document.getElementById('aspp').checked){
        var Cb5 = "<input type='checkbox' name='Combinaciones[]' checked>Actinomyces spp";    
    }
    else{
        var Cb5 = "<input type='checkbox' name='Combinaciones[]'>Actinomyces spp";
    }

    if (document.getElementById('rad').checked){
        var Cb6 = "<input type='checkbox' name='Combinaciones[]' checked>Radiacion";    
    }
    else{
        var Cb6 = "<input type='checkbox' name='Combinaciones[]'>Radiacion";
    }

    if (document.getElementById('atr').checked){
        var Cb7 = "<input type='checkbox' name='Combinaciones[]' checked>Atrofia";    
    }
    else{
        var Cb7 = "<input type='checkbox' name='Combinaciones[]'>Atrofia";
    }
    

    if (document.getElementById('inflacion').checked){
        var Cb8 = "<input type='checkbox' name='Combinaciones[]' checked>Inflamacion";    
    }
    else{
        var Cb8 = "<input type='checkbox' name='Combinaciones[]'>Inflamacion";
    }

    if (document.getElementById('infl').checked){
        var Cb9 = "<input type='checkbox' name='Combinaciones[]' checked>Leve&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";    
    }
    else{
        var Cb9 = "<input type='checkbox' name='Combinaciones[]'>Leve&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
    }

    if (document.getElementById('infm').checked){
        var Cb10 = "<input type='checkbox' name='Combinaciones[]' checked>Moderado&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";    
    }
    else{
        var Cb10 = "<input type='checkbox' name='Combinaciones[]'>Moderado&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
    }

    if (document.getElementById('infse').checked){
        var Cb11 = "<input type='checkbox' name='Combinaciones[]' checked>Severo";    
    }
    else{
        var Cb11 = "<input type='checkbox' name='Combinaciones[]'>Severo";
    }

    if (document.getElementById('diu').checked){
        var Cb12 = "<input type='checkbox' name='Combinaciones[]' checked>DIU";    
    }
    else{
        var Cb12 = "<input type='checkbox' name='Combinaciones[]'>DIU";
    }

    if (document.getElementById('scg').checked){
        var Cb13 = "<input type='checkbox' name='Combinaciones[]' checked>Status de celulas glandulares";    
    }
    else{
        var Cb13 = "<input type='checkbox' name='Combinaciones[]'>Status de celulas glandulares";
    }

    if (document.getElementById('ascus').checked){
        var Cb14 = "<input type='checkbox' name='Combinaciones[]' checked>Células escamosas atípicas de significado indetermidao (ASC - US)";    
    }
    else{
        var Cb14 = "<input type='checkbox' name='Combinaciones[]'>Células escamosas atípicas de significado indetermidao (ASC - US)";
    }

    if (document.getElementById('asch').checked){
        var Cb15 = "<input type='checkbox' name='Combinaciones[]' checked>Células escamosas atípicas de significado, no se puede excluir una lesión de alto grado (ASC - H)";    
    }
    else{
        var Cb15 = "<input type='checkbox' name='Combinaciones[]'>Células escamosas atípicas de significado, no se puede excluir una lesión de alto grado (ASC - H)";
    }

    if (document.getElementById('ipvh').checked){
        var Cb16 = "<input type='checkbox' name='Combinaciones[]' checked>Lesión escamosa intraepitelial de Bajo Grado: Infección por PVH";    
    }
    else{
        var Cb16 = "<input type='checkbox' name='Combinaciones[]'>Lesión escamosa intraepitelial de Bajo Grado: Infección por PVH";
    }

    if (document.getElementById('nici').checked){
        var Cb17 = "<input type='checkbox' name='Combinaciones[]' checked>Lesión escamosa intraepitelial de Bajo Grado: Displasia Leve (NIC - I)";    
    }
    else{
        var Cb17 = "<input type='checkbox' name='Combinaciones[]'>Lesión escamosa intraepitelial de Bajo Grado: Displasia Leve (NIC - I)";
    }

    if (document.getElementById('nicii').checked){
        var Cb18 = "<input type='checkbox' name='Combinaciones[]' checked>Lesión escamosa intraepitelial de Alto Grado: Displasia Moderada (NIC - II)";    
    }
    else{
        var Cb18 = "<input type='checkbox' name='Combinaciones[]'>Lesión escamosa intraepitelial de Alto Grado: Displasia Moderada (NIC - II)";
    }

    if (document.getElementById('niciii').checked){
        var Cb19 = "<input type='checkbox' name='Combinaciones[]' checked>Lesión escamosa intraepitelial de Alto Grado: Displasia Severa (NIC - III)";    
    }
    else{
        var Cb19 = "<input type='checkbox' name='Combinaciones[]'>Lesión escamosa intraepitelial de Alto Grado: Displasia Severa (NIC - III)";
    }

    if (document.getElementById('cce').checked){
        var Cb20 = "<input type='checkbox' name='Combinaciones[]' checked>Carcinoma de células escamosas";    
    }
    else{
        var Cb20 = "<input type='checkbox' name='Combinaciones[]'>Carcinoma de células escamosas";
    }

    if (document.getElementById('agc').checked){
        var Cb21 = "<input type='checkbox' name='Combinaciones[]' checked>Células glandulares atípicas (AGC)";    
    }
    else{
        var Cb21 = "<input type='checkbox' name='Combinaciones[]'>Células glandulares atípicas (AGC)";
    }

    if (document.getElementById('acs').checked){
        var Cb22 = "<input type='checkbox' name='Combinaciones[]' checked>Adenocarcinoma cervical in situ";    
    }
    else{
        var Cb22 = "<input type='checkbox' name='Combinaciones[]'>Adenocarcinoma cervical in situ";
    }

    if (document.getElementById('ader').checked){
        var Cb23 = "<input type='checkbox' name='Combinaciones[]' checked>Adenocarcinoma";    
    }
    else{
        var Cb23 = "<input type='checkbox' name='Combinaciones[]'>Adenocarcinoma";
    }            

    //TipodeFormato      

    if(MTE == 'Citologico Positivo' || ($('input[name=tipomuestra]:checked').val() == "Citologico" && vercalidad==1)){
        var html="<html>";
        html+= "<head>";
        html+= "<style type='text/css'>#alfondo{position:absolute;bottom:0;width:100%;height:80px;border-top:1px solid #000;}</style>";
        html+= "</head>";
        html+= "<body>";
        html+= "<table width='100%' style='padding:0.5%;'>";
        html+= "<tr>";
        html+= "<td rowspan='4' width='5%'><img src='../../MVC_Complemento/img/hndac.jpg' height='100' width='104'></td>";
        html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'>SERVICIO DE ANATOMIA PATOLOGICA</strong></font></td>";
        html+= "</tr>";
        html+= "<tr align='center'>";
        html+= "<td><strong><font size='3.5' face='Microsoft New Tai Lue'>Dr Leoncio Vega Rizo - Patron</strong></font></td>";
        html+= "</tr>";
        html+= "<tr align='center'>";
        html+= "<td><strong><font size='3.5' face='Microsoft New Tai Lue'><u><i>DIAGNÓSTICO CITOLÓGICO CERVICO VAGINAL</u></i></font></strong></td>";
        html+= "</tr>";
        html+= "<tr align='center'>";
        html+= "<td><font size='3.5' face='Microsoft New Tai Lue'><strong><u><i>Sistema Bethesda 2001</u></i></strong></font></td>";
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;border-bottom: 0px solid' Rules=none Frame=Box>";
        html+= "<tr>";
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Registro: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>"; 
            html+= Codigoap;
            html+= "</font></strong></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>HC: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= HC;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Nombre: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>"; 
            html+= NombrePaciente;
            html+= "</font></strong></td>";
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;border-top: 0px;border-bottom: 0px;' Rules=none Frame=Box>";
        html+= "<tr>";
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Edad: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Edad;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Sexo: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Sexo;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Domicilio: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Dir;
            html+= "</font></td>";         
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;border-top: 0px; border-bottom: 0.5px;' Rules=none Frame=Box>";
        html+= "<tr>";                
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Sala/Piso: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Sp;
            html+= "</font></td>";         
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Fecha de Nac.: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Fn;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' border='1' style='padding:0.5%;' Rules=none Frame=Box>";
        html+= "<tr>";
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Especimen: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Especimen;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Fecha: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= FechaR;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='2'><font size='2.5' face='Microsoft New Tai Lue'><strong>Calidad de la Muestra: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= CalidadMuestra;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Endocervicales: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Endocervicales;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Categorizacion Gral.: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Categorizacion;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;'>";
        html+= "<tr>";
            html+= "<td colspan='6' align='center'><font size='2' face='Microsoft New Tai Lue'><u><strong><i>Interpretación/Resultado</i></u></strong></font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='6'><font size='2' face='Microsoft New Tai Lue'><strong><u>Negativo para lesion intraepitelial o malignidad</strong></u></font></td>";
        html+= "</tr>";      
        html+= "<tr>";
            html+= "<td colspan='6'><font size='2' face='Microsoft New Tai Lue'><u>Organismos<u></font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td style='width:20%;'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb1;
            html+= "</font></td>";
            html+= "<td style='width:20%;'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb2;
            html+= "</font></td>";
            html+= "<td style='width:25%;'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb3;
            html+= "</font></td>";  
            
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='3'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb4;
            html+= "</font></td>";           
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb5;
            html+= "</font></td>";                           
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='6'><font size='2' face='Microsoft New Tai Lue'><u>Cambios celulares reactivos asociados con:</u></font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb6;
            html+= "</font></td>";
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb7;
            html+= "</font></td>";
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb8;
            html+= "</font></td>";                                      
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb9;
            html+= Cb10;
            html+= Cb11;
            html+= "</font></td>";            
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb12; 
            html+= "</font></td>";       
            html+= "<td  colspan='2'><div id='inflacion'><font size='2' face='Microsoft New Tai Lue'>";            
            html+= Cb13;
            html+= "</font></td>";                  
        html+= "</tr>";                                      
        html+= "</table>";    
        html+= "<table style='width:100%;padding:0.5%;'>";
        html+= "<tr>";
            html+= "<td colspan='4'><font size='2' face='Microsoft New Tai Lue'><strong><u>Anormalidad de celulas epiteliales</strong></u></font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb14;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb15;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb16;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";    
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb17;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";    
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb18;
            html+= "</font></td>";
        html+= "</tr>"; 
        html+= "<tr>";   
            html+= "<td colspan='2'><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb19;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb20;
            html+= "</font></td>";
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb21;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";    
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb22;
            html+= "</font></td>";
        
            html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
            html+= Cb23;
            html+= "</font></td>";
        html+= "</tr><br>";
        html+= "</table>";
        html+= "<table width='100%'>";        
        html+= "<tr><font size='2' face='Microsoft New Tai Lue'>Otros: <br>";                                                                                        
        html+= OtrosCitologico;                                        
        html+= "</font></tr>";
        html+= "</table>";       
        html+= "<div id='alfondo'>";
        html+= "<table width='100%'>";
        html+= "<tr>";
            html+= "<td style='width:71.5%;'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Fecha de Resultado: ";
            html+= "</strong></font></td>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Firma y Sello: ";
            html+= "</strong></font></td>";
            html+= "</tr>";
            html+= "<tr>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Callao, ";
                html+= FC;
            html+= "</font></td>";
            html+= "<td align='right'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "______________________________";
            html+= "</strong></td>";
            html+= "</tr>";        
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= Med;
            html+= "</font></td>";
            html+= "</tr>";
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Av.Guardia Chalaca 2170 - Bellavista, Callao";
            html+= "</font></td>";
            html+= "</tr>";    
            html+= "</table>";
            html+= "</div>";
            html+= "</body>";
            html+= "</html>";
    }

    if (MTE == 'Citologico Negativo' || ($('input[name=tipomuestra]:checked').val() == "Citologico" && vercalidad==2 || vercalidad==3)) {
        var html="<html>";
        html+= "<head>";
        html+= "<style type='text/css'>#alfondo{position:absolute;bottom:0;width:100%;height:80px;border-top:1px solid #000;}</style>";
        html+= "</head>";
        html+= "<body>";        
        html+= "<table width='100%' style='padding:0.5%;'>";
        html+= "<tr>";
        html+= "<td rowspan='4' width='5%'><img src='../../MVC_Complemento/img/hndac.jpg' height='100' width='104'></td>";
        html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'>SERVICIO DE ANATOMIA PATOLOGICA</strong></font></td>";
        html+= "</tr>";
        html+= "<tr align='center'>";
        html+= "<td><strong><font size='3.5' face='Microsoft New Tai Lue'>Dr Leoncio Vega Rizo - Patron</strong></font></td>";
        html+= "</tr>";
        html+= "<tr align='center'>";
        html+= "<td><strong><font size='3.5' face='Microsoft New Tai Lue'><u><i>DIAGNÓSTICO CITOLÓGICO CERVICO VAGINAL</u></i></font></strong></td>";
        html+= "</tr>";
        html+= "<tr align='center'>";
        html+= "<td><font size='3.5' face='Microsoft New Tai Lue'><strong><u><i>Sistema Bethesda 2001</u></i></strong></font></td>";
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;border-bottom: 0px solid' Rules=none Frame=Box>";
        html+= "<tr>";
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Registro: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>"; 
            html+= Codigoap;
            html+= "</font></strong></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>HC: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= HC;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Nombre: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>"; 
            html+= NombrePaciente;
            html+= "</font></strong></td>";
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;border-top: 0px;border-bottom: 0px;' Rules=none Frame=Box>";
        html+= "<tr>";
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Edad: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Edad;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Sexo: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Sexo;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Domicilio: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Dir;
            html+= "</font></td>";         
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;border-top: 0px; border-bottom: 0.5px;' Rules=none Frame=Box>";
        html+= "<tr>";                
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Sala/Piso: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Sp;
            html+= "</font></td>";         
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Fecha de Nac.: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Fn;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "</table>";
        html+= "<table width='100%' border='1' style='padding:0.5%;' Rules=none Frame=Box>";
        html+= "<tr>";
            html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>Especimen: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= Especimen;
            html+= "</font></td>";
            html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>Fecha: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= FechaR;
            html+= "</font></td>";
        html+= "</tr>";
        html+= "<tr>";
            html+= "<td colspan='2'><font size='2.5' face='Microsoft New Tai Lue'><strong>Calidad de la Muestra: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
            html+= CalidadMuestra;
            html+= "</font></td>";
        html+= "</tr>";        
        html+= "</table>";
        html+= "<table width='100%' style='padding:0.5%;'>";
        html+= "<tr>";
    html+= "<td align='center'><strong><font size='2.5' face='Microsoft New Tai Lue'>";
        html+= "<u><i>Motivo</i></u>";
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
        html+= Motivo;
    html+= "</font></td>";
    html+= "</tr>";
        html+= "</table>";        
        html+= "<div id='alfondo'>";
        html+= "<table width='100%'>";
        html+= "<tr>";
            html+= "<td style='width:71.5%;'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Fecha de Resultado: ";
            html+= "</strong></font></td>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Firma y Sello: ";
            html+= "</strong></font></td>";
            html+= "</tr>";
            html+= "<tr>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Callao, ";
                html+= FC;
            html+= "</font></td>";
            html+= "<td align='right'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "______________________________";
            html+= "</strong></td>";
            html+= "</tr>";        
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= Med;
            html+= "</font></td>";
            html+= "</tr>";
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Av.Guardia Chalaca 2170 - Bellavista, Callao";
            html+= "</font></td>";
            html+= "</tr>";    
            html+= "</table>"; 
        html+= "</div>";   
        html+= "</body>";
        html+= "</html>";
    }

    if (MTE == 'Citologico Quirurgico' || ($('input[name=tipomuestra]:checked').val() == "Citologico" && verespecifico==2)){
        var html="<html>";
    html+= "<head>";    
    html+= "<style type='text/css'>#alfondo{position:absolute;bottom:0;width:100%;height:80px;border-top:1px solid #000;}</style>";
    html+= "</head>";
    html+= "<body>";
    html+= "<table width='100%' style='padding:0.5%;'>";
    html+= "<tr>";
        html+= "<td rowspan='3' width='5%'><img src='../../MVC_Complemento/img/hndac.jpg' height='87' width='95'>";
    html+= "</td>";
    html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'>";
        html+= "SERVICIO DE ANATOMIA PATOLOGICA";
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'>";
        html+= "Dr Leoncio Vega Rizo - Patron";
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'><u><i>";
        html+= "INFORME CITOLÓGICO";
    html+= "</u></i></strong></font></td>";
    html+= "</tr>";
    html+= "</table>";
    html+= "<table width='100%' border='1' style='padding:0.5%;' Rules=none Frame=Box>";
    html+= "<tr>";
    html+= "<td colspan='2'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Nombre: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>";
        html+= NombrePaciente;
    html+= "</font></strong></td>";
    html+= "<td colspan='2' align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Registro: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>"; 
        html+= Codigoap;
    html+= "</font></strong></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Edad: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Edad;
    html+= "</font></td>";
    html+= "<td align='center'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Sexo: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Sexo;
    html+= "</font></td>";
    html+= "<td align='center'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "HC: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= HC;
    html+= "</font></td>";
    html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Sala/Piso: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Sp;
    html+= "</font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td colspan='2' ><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Especimen: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Especimen;
    html+= "</font></td>";
    html+= "<td colspan='2' align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Fecha: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= FechaR;
    html+= "</td>";
    html+= "</tr>";
    html+= "</table>";
    html+= "<table width='100%' style='padding:1%;'>";
    html+= "<tr>";
    html+= "<td align='center'><strong><font size='2.5' face='Microsoft New Tai Lue'>";
        html+= "<u><i>Diagnóstico</i></u>";
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td><font size='2' face='Microsoft New Tai Lue'>";
        html+= DiagnosticoCitologico;
    html+= "</font></td>";
    html+= "</tr>";
    html+= "</table>";
    html+= "<table  width='100%' style='padding:1%;'>";
    html+= "<tr>";
    html+= "<td align='left'><strong><font size='2.5' face='Microsoft New Tai Lue'>";
        html+= "<u><i>Descripción Macroscopica</i></u>";
    html+= "</strong></font></td>";
    html+= "<td><strong><font size='1.5' face='Microsoft New Tai Lue'>Dr. ";
    html+= Med;
    html+= "</strong></font></td>";
    html+= "<td><strong><font size='1.5' face='Microsoft New Tai Lue'>Dr. ";
    html+= Med2;
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td colspan='3'><font size='2' face='Microsoft New Tai Lue'>";
        html+= DescripcionCitologico;
    html+= "</font></td>";    
    html+= "</tr>";
    html+= "</table>";
    html+= "<div id='alfondo'>";
    html+= "<table width='100%'>";
        html+= "<tr>";
            html+= "<td style='width:71.5%;'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Fecha de Resultado: ";
            html+= "</strong></font></td>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Firma y Sello: ";
            html+= "</strong></font></td>";
            html+= "</tr>";
            html+= "<tr>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Callao, ";
                html+= FC;
            html+= "</font></td>";
            html+= "<td align='right'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "______________________________";
            html+= "</strong></td>";
            html+= "</tr>";        
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= Med;
            html+= "</font></td>";
            html+= "</tr>";
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Av.Guardia Chalaca 2170 - Bellavista, Callao";
            html+= "</font></td>";
            html+= "</tr>";    
            html+= "</table>";
            html+= "</div>";
            html+= "</body>";
            html+= "</html>";
    }


    if (MTE == 'Quirurgico' || ($('input[name=tipomuestra]:checked').val() == "Quirurgico")){    
    var html="<html>";
    html+= "<head>";    
    html+= "<style type='text/css'>#alfondo{position:relative;bottom:0;width:100%;height:80px;border-top:1px solid #000;}</style>";    
    html+= "</head>";    
    html+= "<body>";
    html+= "<div id='header'>";
    html+= "<table width='100%' style='padding:0.5%;'>";
    html+= "<tr>";
        html+= "<td rowspan='3' width='5%'><img src='../../MVC_Complemento/img/hndac.jpg' height='90' width='94'>";
    html+= "</td>";
    html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'>";
        html+= "SERVICIO DE ANATOMIA PATOLOGICA";
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'>";
        html+= "Dr Leoncio Vega Rizo - Patron";
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td align='center'><strong><font size='3.5' face='Microsoft New Tai Lue'><u><i>";
        html+= "INFORME ANATOMOPATOLÓGICO";
    html+= "</u></i></strong></font></td>";
    html+= "</tr>";
    html+= "</table>";
    html+= "<table width='100%' border='1' style='padding:0.5%;' Rules=none Frame=Box>";
    html+= "<tr>";
    html+= "<td colspan='2'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Nombre: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>";
        html+= NombrePaciente;
    html+= "</strong></font></td>";
    html+= "<td colspan='2' align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Registro: </strong></font><font size='3' face='Microsoft New Tai Lue'><strong>"; 
        html+= Codigoap;
    html+= "</font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Edad: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Edad;
    html+= "</font></td>";
    html+= "<td align='center'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Sexo: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Sexo;
    html+= "</font></td>";
    html+= "<td align='center'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "HC: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= HC;
    html+= "</font></td>";
    html+= "<td align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Sala/Piso: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Sp;
    html+= "</font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td colspan='2' ><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Especimen: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= Especimen;
    html+= "</font></td>";
    html+= "<td colspan='2' align='right'><font size='2.5' face='Microsoft New Tai Lue'><strong>";
        html+= "Fecha: </strong></font><font size='2.5' face='Microsoft New Tai Lue'>"; 
        html+= FechaR;
    html+= "</td>";
    html+= "</tr>";
    html+= "</table>";
    html+= "</div>";

    html+= "<table width='100%' style='padding:1%;' >";
    html+= "<tr>";
    html+= "<td align='center'><strong><font size='2.5' face='Microsoft New Tai Lue'>";
        html+= "<u><i>Diagnóstico</i></u>";
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>"; 
    html+= "<td>";   
    html+= "<font size='2' face='Microsoft New Tai Lue'>";
        html+= diagq1;
    html+= "</font>";
    html+= "</td>";
    html+= "</tr>";   
    html+= "</table>";
    
    html+= "<table  width='100%' style='padding:1%;'>";
    html+= "<tr>";
    html+= "<td align='left'><strong><font size='2.5' face='Microsoft New Tai Lue'>";
        html+= "<u><i>Descripción Macroscopica</i></u>";
    html+= "</strong></font></td>";
    html+= "<td><strong><font size='1.5' face='Microsoft New Tai Lue'>Dr. ";
    html+= Med;
    html+= "</strong></font></td>";
    html+= "<td><strong><font size='1.5' face='Microsoft New Tai Lue'>Dr. ";
    html+= Med2;
    html+= "</strong></font></td>";
    html+= "</tr>";
    html+= "<tr>";
    html+= "<td colspan='3'><font size='2' face='Microsoft New Tai Lue'>";
        html+= descq1;
    html+= "</font></td>";    
    html+= "</tr>";    
    html+= "</table>";
    html+= "<div id='separar'><br><div>";
    html+= "<div id='alfondo' class='h'>";
    html+= "<table width='100%'>";
        html+= "<tr>";
            html+= "<td style='width:71.5%;'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Fecha de Resultado: ";
            html+= "</strong></font></td>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "Firma y Sello: ";
            html+= "</strong></font></td>";
            html+= "</tr>";
            html+= "<tr>";
            html+= "<td><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Callao, ";
                html+= FC;
            html+= "</font></td>";
            html+= "<td align='right'><font size='1.5' face='Microsoft New Tai Lue'><strong>";
                html+= "________________________________";
            html+= "</strong></td>";
            html+= "</tr>";        
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= Med;
            html+= "</font></td>";
            html+= "</tr>";
            html+= "<tr>";        
            html+= "<td colspan='2' align='right'><font size='1.5' face='Microsoft New Tai Lue'>";
                html+= "Av.Guardia Chalaca 2170 - Bellavista, Callao";
            html+= "</font></td>";
            html+= "</tr>";    
            html+= "</table>";
            html+= "</div>";  
    html+= "</body>";
    html+= "</html>";
    }

    //window.onload = function() { window.print(); }
    var printWin = window.open('','','left=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
    printWin.resizeTo(2200,1000); 
    printWin.document.write(html);   
    printWin.document.close();
    printWin.focus();
    printWin.print();   
    printWin.close();
}
</script>
<body>
<div class="easyui-layout" style="width:1900px;height:900px;">
<!--Principal-->
    <div data-options="region:'north', title:'Consulta Historia de Especimenes'" style="height:12%;padding:1%;">
        <table>
        	<tr>
        		<td><input class="easyui-textbox" data-options="prompt:'Codigo'" id="codigobusqueda" style="width:100%;height:30px"></td>
                <td><input class="easyui-textbox" data-options="prompt:'Apellido'" id="npbusqueda" style="width:100%;height:30px"></td>
                <td><input class="easyui-textbox" data-options="prompt:'N° Historia Clinica'" id="hcbusqueda" style="width:100%;height:30px"></td> 
        		<td><input class="easyui-textbox" data-options="prompt:'N° Cuenta'" id="ncbusqueda" style="width:100%;height:30px"></td>      			
        		<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:100px;height:30px;" id="Buscar">Buscar</a></td>
        		<td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:100px;height:30px;" id="Limpiar">Limpiar</a></td> 
                <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:100px;height:30px;" id="Nuevo" >Nuevo</a></td>
                <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove'" style="width:100px;height:30px;" id="Eliminar">Eliminar</a></td>            	               
                <td style="width:60%;" align="right"><a class="easyui-linkbutton" style="width:150px;height:30px;" id="Excel"><img src="../../MVC_Complemento/img/excel-icon.png" height="10" width="10"> Exportar a Excel</a></td>
        	</tr>
        </table>
    </div>
         
    <div data-options="region:'center', title:'Resultados'">
        <table class="easyui-datagrid" title="Resultados de Busqueda" style="width:99%;height:auto;" data-options="singleSelect:true,collapsible:false" id="busqueda">
	        <thead>
                <tr>
		            <th data-options="field:'CODIGOAP',width:'5%'"><span style="font-weight:bold">Codigo</th>
		            <th data-options="field:'NROHISTORIACLINICA',width:'5%'"><span style="font-weight:bold">N. H.C</th>                        
		            <th data-options="field:'NOMBREPACIENTE',width:'17%'"><span style="font-weight:bold">Paciente</th>
                    <th data-options="field:'EDAD',width:'15%'"><span style="font-weight:bold">Edad</th>
                    <th data-options="field:'SEXO',width:'15%'"><span style="font-weight:bold">Sexo</th>
                    <th data-options="field:'TIPO',width:'5%'"><span style="font-weight:bold">Tipo</th>
                    <th data-options="field:'TIPOE',width:'10%'" ><span style="font-weight:bold">Tipo Especifico</th>
		            <th data-options="field:'ESPECIMENES',width:'10%'"><span style="font-weight:bold">Especimen</th>
		            <th data-options="field:'FECHAREGISTRO',width:'10%',type:'datetimebox'"><span style="font-weight:bold">Fecha Registro</th>
		            <th data-options="field:'FECHACOMPLETADO',width:'10%'"><span style="font-weight:bold">Fecha Diagnostico</th>
		            <th data-options="field:'SERVICIO',width:'15%'"><span style="font-weight:bold">Sala/Piso</th>
		            <th data-options="field:'IDPACIENTE',width:'10%'"><span style="font-weight:bold">ID.PAC</th>
                    <th data-options="field:'M1',width:'10%'">M1</th>
                    <th data-options="field:'M2',width:'10%'">M2</th>
                    <th data-options="field:'ENDO',width:'10%'">E</th>
                    <th data-options="field:'CG',width:'10%'">CG</th>
                    <th data-options="field:'CM',width:'10%'">CM</th>
                    <th data-options="field:'COMBINACIONES',width:'10%'">COMBINACIONES</th>
                    <th data-options="field:'OTROS',width:'10%'">Otros</th>
                    <th data-options="field:'DIC',width:'10%'">DiC</th>
                    <th data-options="field:'DEC',width:'10%'">DeC</th>
                    <th data-options="field:'DIQ',width:'10%'">DiQ</th>
                    <th data-options="field:'DEQ',width:'10%'">DeQ</th>
                    <th data-options="field:'MOTIVO',width:'10%'">Motivo</th>
		        </tr>
	        </thead>
    	</table>
    </div>
</div>

<!--Ventana-->
<div id="ventana" class="easyui-window" title="Nueva Muestra" data-options="collapsible:false,draggable:false,resizable:false,inline:true,border:'thin',closed:true" style="width:97%;height:95%;padding:10px;">
   	<div class="easyui-layout" data-options="fit:true">
   		<div data-options="region:'north'" style="height:22%;padding:3px;">
            <table style="width:100%;">
                <tr>	
                	<td style="width:7%">Tipo de Muestra:</td>
                	<td><input type="hidden" id="tipocito"></td>                 		             		
                	<td><Input type = 'Radio' Name ='tipomuestra' id='rbc' value= 'Citologico' CHECKED>Citologico
					<Input type = 'Radio' Name ='tipomuestra' id='rbq' value= 'Quirurgico'>Quirurgico
  					</td>                		
                </tr>                	
            </table>
            <table style="width:97%;">
                <tr>
                    <td><input type='hidden' id='MuestraTipoEspecifico' name='mte'></td> 
                    <td><input type="hidden" id='fcompletado' name='fc'></td>                       
                	<td style="width:80.5%"></td>
                	<td>Codigo:</td>
                	<td><input class="easyui-textbox" data-options="prompt:'Codigo'" id="codigoap" required="required" style="width:100%;height:30px"></td>
                    <td>F.Registro:</td>
                	<td><input class="easyui-datebox" id="fregistro" required="required" style="width:100%;height:30px"></input></td>
                </tr>
            </table>
            <table style="width:97%;">
                <tr>	
                	<td>H.C:<input class="easyui-textbox"  data-options="prompt:'H.C'" id="historia" style="width:70px;height:30px;"></td>           			
                	<td>N.C:<input class="easyui-textbox" data-options="prompt:'N.C'" id="numeroc" style="width:70px;height:30px;"></td>
                	<td></td>   
                	<td></td>              		
                	<td>Paciente</td>
                	<td><input class="easyui-textbox" id="paciente" data-options="prompt: 'Paciente'" required="required" style="width:100%;height:30px"></td>
                	<td>Edad</td>
                	<td><input class="easyui-textbox" id="edad" data-options="prompt: 'Edad'" required="required" style="width:80px;height:30px"></td>
                	<td>Sexo</td>
                	<td><input class="easyui-textbox" id="sexo" data-options="prompt: 'Sexo'" required="required" style="width:80px;height:30px"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><a href="javascript:void(0)" class="easyui-linkbutton c5" id="particular" style="width:190px;">Particular</a></td>
                    <td></td>   
                    <td></td>  
                    <td>Direccion</td>
                    <td><input class="easyui-textbox" id="direccion"  data-options="prompt: 'Direccion'" style="width:100%;height:30px"></td>
                    <td>F.Nacimiento</td>
                    <td><input class="easyui-textbox" id="fnacimiento" data-options="prompt: 'F.N'" style="width:80px;height:30px"></td>
                </tr>
            </table>      
        </div>     

        <div data-options="region:'center'" style="padding:5px;">
            <table style="width:100%;padding:0.5%;">
                <tr>
                    <input type="hidden" id='medicomicro'><input type="hidden" id='medicomacro'><input type="hidden" id='sph'>
                	<td style="width:5%;">Especimen</td>                        
                	<td><input class="easyui-textbox" id="especimen" data-options="prompt: 'Especimen'" style="width:300px;height:30px">
                	Sala/Piso&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input id="salapiso" class="easyui-combobox" data-options="valueField:'IDSALAPISO',textField:'NOMBRESALAPISO',url:'../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ListarSalaPiso'" style="width:300px;">
                	</td>             		
                	<td colspan="2" align="right"><font color="00334d"><strong><label id="idpac"></label></strong></font></td>
                </tr>
                <tr>
                	<td>Medico Micro</td>
                	<td><input id="medicos1" class="easyui-combobox" data-options="valueField:'IDMEDICO',textField:'NOMBREMEDICO',url:'../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ListarMedicos'" style="width:300px;">&nbspMedico Macro&nbsp&nbsp&nbsp                		
                	<input id="medicos2" class="easyui-combobox" data-options="valueField:'IDMEDICO',textField:'NOMBREMEDICO',url:'../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ListarMedicos'" style="width:300px;"></td>
                </tr>
            </table>
            <table style="width:100%;padding:0.5%;" >
                <tr>
                	<td align="center">Diagnostico y Descripcion</td>
                </tr>
            </table>

            
            	<div id="citologicoopciones">
	                <table style="width:100%;padding:0.5%;" >                	
		                <tr>
		                	<td colspan="2">Registro</td>                                
		                	<td colspan="2" align="right"><select id="tipoespecifico" class="easyui-combobox" style="width:300px;" data-options="
		                	onSelect:function(rec){	               		
		                		var tipo_especifico = $('#tipoespecifico').combobox('getValue');

		                		if(tipo_especifico == 1)
		                		{
		                			$('#citologico1').fadeIn(200);
                                    $('#otros').fadeIn(200);
		                			$('#citologico2').hide();
							$('#calidad').combobox('setValue','1');
		                			$('#calidad').combobox('enable');
		                			$('#endocervicales').combobox('enable');		                				
		                			$('#categorizacion').combobox('enable');		                				
		                			return false;
		                		}
		                		if(tipo_especifico == 2)
		                		{	
		                			$('#citologico1').hide();
                                    $('#otros').hide();
                                    $('#divmotivo').hide();
		                			$('#citologico2').fadeIn(200);
		                			$('#calidad').combobox('disable');
		                			$('#endocervicales').combobox('disable');		                				
		                			$('#categorizacion').combobox('disable');
		                			return false;
		                		}
		                	}">
		                	<option value="1">Postivo/Negativo/Rechazado</option>                                                               
		                    <option value="2">Descripcion/Diagnostico</option>                                     
		                    </select>
		                	</td>
		                </tr>
		                <tr>
		                	<td style="width:10%">Calidad de la Muestra</td>
		                	<td style="width:15%">
		                	<select class="easyui-combobox" id="calidad" style="width:300px;" data-options="valueField:'IDCALIDAD',textField:'NOMBRECALIDAD',url:'../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ListarCalidad',
		                	onSelect:function(rec){
		                	var id_calidad = $('#calidad').combobox('getValue');
		                			
		                	if(id_calidad == 1)
		                	{	
		                		$('#citologico1').fadeIn(200);
                                $('#otros').fadeIn(200);
                                $('#divmotivo').hide();
		                		$('#endocervicales').combobox('enable');		                				
		                		$('#categorizacion').combobox('enable');		                				
		                		return false;
		                	}
		                	if(id_calidad == 2)
		                	{
		                		$('#citologico1').hide();
                                $('#otros').hide();                				               				
		                		$('#divmotivo').fadeIn(200);
		                		$('#endocervicales').combobox('disable');		                				
		                		$('#categorizacion').combobox('disable');
		                		return false;
		                	}
                            if(id_calidad == 3)
                            {
                                $('#citologico1').hide();
                                $('#otros').hide();                                                         
                                $('#divmotivo').fadeIn(200);
                                $('#endocervicales').combobox('disable');                                       
                                $('#categorizacion').combobox('disable');
                                return false;
                            }
		                	}">
		                    </select>
		                	</td>
		                	<td style="width:10%"></td>
		                	<td></td>                 		
		                </tr>
		                <tr>
		                	<td>Endocervicales</td>
		                	<td><input id="endocervicales" class="easyui-combobox" data-options="valueField:'IDENDO',textField:'NOMBREENDO',url:'../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ListarEndo'" style="width:300px;">
		                	<td>&nbsp&nbsp&nbsp&nbspCategorizacion General</td>
		                	<td><input id="categorizacion" class="easyui-combobox" data-options="valueField:'IDCATE',textField:'NOMBRECATE',url:'../../MVC_Controlador/Laboratorio/MuestrasC.php?acc=ListarCate'" style="width:300px;">                		
		                </tr>
		            </table>
	               </div>
	            <!--1-->
			        <div id="citologico1">
			        
			        <table style="width:100%;padding:0.5%;" >
			            <tr>
			                <td colspan="6"><strong><u>Negativo para lesión intraepitelial o malignidad:</strong></u></td>
			            </tr>
			            <tr>
			                <td colspan="6">Organismos</td>
			            </tr>
			            <tr>
			                <td><input type="checkbox" name="Combinaciones[]" value="Candidaspp" id="cspp">Candida spp</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Herpesvirus" id="hv">Herpes Virus</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Trichomonavaginalis" id="tv">Trichomona Vaginalis</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Cambiosenlaflora" id="cfs">Cambios en la flora sugestivos de vaginosis bacteriana</td>
			                <td style="width:22%"><input type="checkbox" name="Combinaciones[]" value="Actinomycesspp" id="aspp">Actinomyces spp</td>                			
			            </tr>
			            <tr>
			                <td colspan="6">Cambios celulares reactivos asociados con:</td>
			            </tr>
			            <tr>
			                <td><input type="checkbox" name="Combinaciones[]" value="Radiacion" id="rad">Radiacion</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Atrofia" id="atr">Atrofia</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Inflacion" id="inflacion">Inflamacion</td>
			                <td><div id="inflacionestado"><input type="checkbox" class="inf" name="Combinaciones[]" value="Leve" id="infl">Leve
                            <input type="checkbox" class="inf"  name="Combinaciones[]" value="Moderado" id="infm">Moderado
                            <input type="checkbox" class="inf"  name="Combinaciones[]" value="Severo" id="infse">Severo</div></td>
                            <td><input type="checkbox" name="Combinaciones[]" value="Diu" id="diu">DIU</td>			                			
			                <td><input type="checkbox" name="Combinaciones[]" value="Statuscelulasglandulares" id="scg">Status de celulas glandulares</td>                			
			            </tr>
			        </table> 

			        
			        <table style="width:100%;padding:0.5%;">
			            <tr>
			                <td colspan="3"><strong><u>Anormalidad de celulas epiteliales</strong></u></td>                				
                        </tr>
			            <tr>
                            <td><input type="checkbox" name="Combinaciones[]" value="Celulasescamosasatipicasascus" id="ascus">Células escamosas atípicas de significado indetermidao (ASC - US)</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="celulasescamosasatipicasasch" id="asch">Células escamosas atípicas de significado, no se puede excluir una lesión de alto grado (ASC - H)</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Infeccionporpvh" id="ipvh">Lesión escamosa intraepitelial de Bajo Grado: Infección por PVH</td>
			            </tr>
			            <tr>
                            <td><input type="checkbox" name="Combinaciones[]" value="Displasialeve" id="nici">Lesión escamosa intraepitelial de Bajo Grado: Displasia Leve (NIC - I)</td>
                            <td><input type="checkbox" name="Combinaciones[]" value="Displasiamorderada" id="nicii">Lesión escamosa intraepitelial de Alto Grado: Displasia Moderada (NIC - II)</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Displasiasevera" id="niciii">Lesión escamosa intraepitelial de Alto Grado: Displasia Severa (NIC - III)</td>
			            </tr>
			            <tr>
			                <td><input type="checkbox" name="Combinaciones[]" value="Carcinomadecelulasescamosas" id="cce">Carcinoma de células escamosas</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Celulasglandularesatipicas" id="agc">Células glandulares atípicas (AGC)</td>
			                <td><input type="checkbox" name="Combinaciones[]" value="Adenocarcinomacervicalinsitu" id="acs">Adenocarcinoma cervical in situ</td>
			            </tr>
                        <tr>			                				
                            <td><input type="checkbox" name="Combinaciones[]" value="Adenocarcinoma" id="ader">Adenocarcinoma</td>
			            </tr>			                			
			        </table>
                    </div>
                
                <div id="otros">
                    <table>
                        <tr>
                            <td><strong>Otros</strong></td>
                        </tr>
                        <tr>                                                                                        
                            <td colspan="3"><textarea id="otroscito" style="font-family:Calibri;font-size:18px;"  rows="3" cols="192"></textarea></td>                                        
                        </tr>
                    </table>
                </div>

                <div id="divmotivo">
                    <table>
                        <tr>
                            <td><strong><u>Motivo</u></strong></td>
                        </tr>
                        <tr>                                                                                        
                            <td colspan="3"><textarea id="motivo" style="font-family:Calibri;font-size:18px;"  rows="14" cols="192"></textarea></td>                                        
                        </tr>
                    </table>
                </div>

			    <div id="citologico2">
			        <table style="width:100%;padding:0.5%;">
			            <tr>
			                <td><strong><u>Descripcion Citologico:</u></strong></td>
			            </tr>
			            <tr>
			                			<td><textarea id="desccito" style="font-family:Calibri;font-size:18px;" rows="6" cols="192"></textarea></td>                			
			            </tr>
			            <tr></tr>
			            <tr>
			                			<td><strong><u>Diagnostico Citologico:</u></strong></td>
			            </tr>
			            <tr>
			                			<td><textarea id="diagcito" style="font-family:Calibri;font-size:18px;" rows="6" cols="192"></textarea></td>                			
			            </tr>
			        </table>
			    </div>
       
        		<div id="quirurgico">
                	<table style="width:100%;padding:0.5%;">
                		<tr>
                			<td><strong>Descripcion Quirurgico:</strong></td>
                		</tr>
                		<tr>
                			<td><textarea id="descqui" style="font-family:Calibri;font-size:18px;"  rows="8" cols="192"></textarea></td>                			
                		</tr>
                		<tr></tr>
                		<tr>
                			<td><strong>Diagnostico Quirurgico:</strong></td>
                		</tr>
                		<tr>
                			<td><textarea id="diagqui" style="font-family:Calibri;font-size:18px;"  rows="8" cols="192"></textarea></td>                			
                		</tr>
                	</table>
                </div>

        <div data-options="region:'south',border:false" id="botones" style="text-align:center;padding:5px 0 0;">
            <div id="edit"><a class="easyui-linkbutton" data-options="iconCls:'icon-edit'" id="editar" href="javascript:void(0)" style="width:80px">Editar</a></div><div id="new"><a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" id="guardar" href="javascript:void(0)" style="width:80px">Guardar</a></div>                
            <a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" id="cancel" href="" style="width:80px">Cancelar</a>
            <div id="imprimir"><a class="easyui-linkbutton" data-options="iconCls:'icon-print'" id="imprimir" href="javascript:void(0)" onclick="printPage();$('#dlg1').dialog('close');$('#ventana').window('close');" style="width:80px">Imprimir</a></div>
        </div>
        </div>
    </div>
</div>

<div id="wd" class="easyui-window" title="Eliminar Muestra" data-options="closed:true" style="width:500px;height:138px;padding:5px;">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'center'" style="padding:2px;">
            <table width="100%">
            <tr>
                <td><input class="easyui-textbox" data-options="prompt:'Codigo a Eliminar'" id="muestraeliminar" style="width:460px;height:30px"></td>               
            </tr>
            </table>
            </div>
            <div data-options="region:'south',border:false" style="text-align:right;padding:5px 0 0;">
                <a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" id="eliminarok" style="width:80px">Ok</a>
                <a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" href="" style="width:80px">Cancel</a>
            </div>
        </div>
    </div>

<!--ALERTA -->
<div id="dlg1" class="easyui-dialog" style="padding:5%;width:35%;" data-options="inline:true,modal:true,closed:true,title:'Mensaje'">
<p id="texto_mensaje_error"></p>
<div class="dialog-button">
<a href="javascript:void(0)" class="easyui-linkbutton" style="width:80px;" id="ok">OK</a>
</div> 
</body>
</html>