<?php 
ini_set('memory_limit', '1024M'); 

require('../../MVC_Modelo/AnatomiaPatologicaM.php');
require('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

	#ESTILOS
	$estiloCentrado = array(
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ));

    $estiloEncabezados = array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4682B4'),
            
    	),
        'font' => array(
	            'bold' => true,
	            'color' => array('rgb' => 'ffffff'),
	            'size' => 11,
	            'name' => 'Calibri',
    		)
    );
    

	// Establecer propiedades
	$objPHPExcel->getProperties()
	->setTitle("Muestras")
	->setSubject("Anatomia Patologica");

	// CABECERA
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'CODIGO')
	->setCellValue('B1', 'N. HISTORIA CLINICA')
	->setCellValue('C1', 'PACIENTE')
	->setCellValue('D1', 'SEXO')
	->setCellValue('E1', 'TIPO')
	->setCellValue('F1', 'T.ESPECIFICO')
	->setCellValue('G1', 'ESPECIMEN')
	->setCellValue('H1', 'FECHA REGISTRO')
	->setCellValue('I1', 'FECHA COMPLETADO')
	->setCellValue('J1', 'SALA/PISO')
	->setCellValue('K1', 'IDPACIENTE')
	->setCellValue('L1', 'MEDICO MICRO')
	->setCellValue('M1', 'MEDICO MACRO');

	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($estiloEncabezados);
	$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($estiloEncabezados);

	$data_muestras = ListarRegistroExcel_M();

	if (!$data_muestras) {
		echo 'sin datos';exit();
	}

	$contador = count($data_muestras);
	$j = 2;
	for ($i=0; $i < $contador; $i++) {
		$id_paciente = 'NULL';
		$nhc = 'NULL';
		$temp_paciente = $data_muestras[$i]["IDPACIENTE"];
		$temp_nhc = $data_muestras[$i]["N. HISTORIA CLINICA"];
		if ($temp_paciente == NULL or $temp_paciente == 'NULL'){
		$id_paciente = 'PARTICULAR';
		} 
		else{
		$id_paciente = $data_muestras[$i]["IDPACIENTE"];
		}
		if ($temp_nhc == NULL or $temp_nhc == 'NULL'){
		$nhc = 'PARTICULAR';
		} 
		else{
		$nhc = $data_muestras[$i]["N. HISTORIA CLINICA"];
		}  
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $data_muestras[$i]['CODIGO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $nhc);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $data_muestras[$i]['PACIENTE']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $data_muestras[$i]['SEXO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $data_muestras[$i]['TIPO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $data_muestras[$i]['T.ESPECIFICO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $data_muestras[$i]['ESPECIMEN']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $data_muestras[$i]['FECHA REGISTRO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $data_muestras[$i]['FECHA COMPLETADO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $data_muestras[$i]['SERVICIO']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $id_paciente);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, $data_muestras[$i]['MEDICO 1']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$j, $data_muestras[$i]['MEDICO 2']);
		$j = $j + 1;
	}

	// TITULO DEL DOCUMENTO
	$objPHPExcel->getActiveSheet()->setTitle('MUESTRAS');

	#AJUSTANDO CELDAS
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

    #FILTROS
    $objPHPExcel->getActiveSheet()->setAutoFilter("A1:M".$j); 

	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);

	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="AnatomiaPatologica_Muestras.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit();
?>