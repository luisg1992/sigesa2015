<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ordenes para analisis de laboratorio</title>

	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<style>
        html,body { 
        	height: 100%;
        	font-family: Helvetica; 

        }
		#tabla_resultados_kk table{
            width: 100%;
            border-collapse: collapse;
            text-align: center;
            margin: 0px;
            padding: 0px;
            font-family: Helvetica;
            font-size:13px;
        }
        
        #tabla_resultados_kk table tr{
            margin: 0px;
            padding: 0px;
        }    

        #tabla_resultados_kk table th {
            border:1px dotted #d3d3d3;
            background-color:#eee;
            padding: 3px;
            font-size:11px;
        }

        #tabla_resultados_kk table td {
            border-left:1px dotted #d3d3d3;
            margin: 0px;
            padding: 0px;
        }

        #tabla_resultados_kk  input[type="text"] {
            width: 100% !important;
            outline: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            appearance: none;
            border:none;
            border:1px solid #d3d3d3;
            padding: 2px;
        }
    </style>
	
    
	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>

	<!-- JS: OPERACIONES -->
	<script>
	function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " 0123456789.";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
	$(document).ready(function() {
		
		

		//CARGANDO LAS CONFIGURACIONES POR DEFECTO
		$('#busqueda_nhis').textbox('clear').textbox('textbox').focus();
		$("#Detalle_Orden").window('close');
		$('#tabla_orden_detallada_resultados').datagrid('enableCellEditing').datagrid('gotoCell', {
			index: 0,
			field: 'VALORTEXTO',

		});

		$('#tabla_orden_detallada').datagrid({
			rowStyler:function(index,row){
				if (row.NUMERO % 2 == 0){
					return 'background-color:#F2F2F2;';
				}
			}
		});



				
		//BUSCAR POR NUMERO DE HISTORIA
		$("#busqueda_nhis").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numerohistoria 	= $("#busqueda_nhis").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=BusquedaxNumeroHistoriaCLinica&numerohistoria='+numerohistoria+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin});
								
			}

		});

		//BUSCAR POR NUMERO DE CUENTA
		$("#buscar_cuenta").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numerocuenta	= $("#buscar_cuenta").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=BusquedaxNumeroCuenta&numerocuenta='+numerocuenta+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin});
								
			}

		});

		//BUSCAR POR NUMERO DE ORDEN
		$("#buscar_movimi").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numeromovimi	= $("#buscar_movimi").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=BusquedaxNumeroMovimiento&numeromovimi='+numeromovimi+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin});
								
			}

		});

		//BUSCAR POR NOMBRE PACIENTE
		$("#buscar_nombres").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var paciente		= $("#buscar_nombres").textbox('getText');
				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=BusquedaxNombrePaciente&paciente='+paciente+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin});
								
			}

		});
		
		//BUSCAR POR NUMERO DE BOLETA *************************
		
				
		$("#busqueda_Serie").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				
				$('#buscar_Boleta').textbox('clear').textbox('textbox').focus();

											
			}

		});
		$("#buscar_Boleta").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var NroSerie		= $("#busqueda_Serie").textbox('getText');
				var NroDocumento		= $("#buscar_Boleta").textbox('getText');
					

				var fecha_inicio  	= $("#buscar_fi").datetimebox('getValue');
				var fecha_fin		= $("#buscar_ff").datetimebox('getValue');

				$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
				$('#tabla_resultados').datagrid({url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=BusquedaxNumBoleta&NroSerie='+NroSerie+'&NroDocumento='+NroDocumento+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin});
								
			}

		});
		
		
		
		
		//BUSCAR POR NUMERO DE BOLETA 
		
		

		$("#realiza_detalle").textbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				$('*[data-value="1"]').focus().select();
			}
		});

		//NAVEGACION CON EL TECLADO
            $("#tabla_resultados_kk").on("keydown",".btnClick",function(e) {
              
                //ENTER
                if(e.keyCode == 13) {                
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField + 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //ABAJO
                else if(e.keyCode == 40) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField + 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //ARRIBA
                else if(e.keyCode == 38) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField - 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //IZQUIERDA
                else if(e.keyCode == 37) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField - 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }

                //DERECHA
                else if(e.keyCode == 39) { 
                    var currentField = $(this).data("value");
                    var currentTD = $(this).parent();
                    var nextField = currentField + 1;
                    var prevTD = $('*[data-value="'+nextField+'"]').parent();
                    $('*[data-value="'+nextField+'"]').focus().select();
                }
            });

		//FUNCIONES
		function tabDatagriIngresoResultados()
		{

			
		}
		
		function Limpiar()
		{
			$("#busqueda_nhis").textbox('clear');
			$("#buscar_movimi").textbox('clear');
			$("#buscar_cuenta").textbox('clear');
			$("#buscar_nombres").textbox('clear');
			$("#busqueda_Serie").textbox('clear');
			$("#buscar_Boleta").textbox('clear');
			
			
			$('#tabla_orden_detallada').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
			$('#tabla_resultados').datagrid('loadData', {"IDMOVIMIENTO":0,"rows":[]});
			$("#tabla_resultados").datagrid({url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=MostrarMascaraBody'});
		}

		function GrabarDatosResultado()
		{	
			var idUsuarioLabSigesa 	= $("#idUsuarioparaSigesaLI").val();							//IDUSUARIO
			var fecha_detalle 		= $("#fecha_detalle").textbox('getText');						//FECHA
			//var dataResultado 		= $("#tabla_orden_detallada_resultados").datagrid('getData');	//DATA
			var row 				= $('#tabla_orden_detallada').datagrid('getSelected');  		
			var idorden 			= row.ORDEN;
			var resulta  = row.RESULTAD;													//IDORDEN
			var idUsuarioRealiza 	= $('#realiza_detalle').combobox('getValue');					//REALIZAANALISIS

			//obteniendo datos de tabla
			var valores = [];

            $('.btnClick').each(function() {
                valores.push($(this).val());
            });

            var rows = [];
            for (var i = 0; i < valores.length; i++) {
                rows.push({ grupo: valores[i],
                            item: valores[i+1],
                            valornumero: valores[i+2],
                            valortexto: valores[i+3],
                            valorcombo: valores[i+4],
                            valorcheck: valores[i+5],
                            valorreferencial: valores[i+6],
                            metodo: valores[i+7],
                            idproductocpt: valores[i+8],
                            ordenxresultado: valores[i+9]});
                i = i + 9;
            };

            //ACTUALIZANDO RESULTADOS
            if (resulta == "SI") {
				$.ajax({
					url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ActualizarResultadosDetalle',
					type: 'POST',
					dataType: 'json',
					data: {
						idUsuarioLabSigesa:idUsuarioLabSigesa,
						fecha_detalle:fecha_detalle,
						dataResultado:rows,
						idorden:idorden,
						idUsuarioRealiza:idUsuarioRealiza
					},
					success:function(data)
					{	
						if (data == "R_1") {
							$.messager.confirm('HNDAC', 'Resultado Grabado', function(r){
								if (r){
									$.messager.confirm('HNDAC', 'Desea Imprimir?', function(r){
										if (r){
											ImprimirDatosResultado();
											$('#Detalle_Orden').window('close');
											$('#tabla_orden_detallada').datagrid('reload');
										}
										else{
											$('#Detalle_Orden').window('close');
											$('#tabla_orden_detallada').datagrid('reload');
										}
									});
								}
								else{
									$('#Detalle_Orden').window('close');
									$('#tabla_orden_detallada').datagrid('reload');
								}
							});
						}
						
					}
				});
				
				return false;
			}

			//GRABANDO RESULTADOS
			if (resulta == "NO") {
				$.ajax({
					url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=GrabarResultadosDetalle',
					type: 'POST',
					dataType: 'json',
					data: {
						idUsuarioLabSigesa:idUsuarioLabSigesa,
						fecha_detalle:fecha_detalle,
						dataResultado:rows,
						idorden:idorden,
						idUsuarioRealiza:idUsuarioRealiza
					},
					success:function(data)
					{	
						if (data == "R_1") {
							$.messager.confirm('HNDAC', 'Resultado Grabado', function(r){
								if (r){
									$.messager.confirm('HNDAC', 'Desea Imprimir?', function(r){
										if (r){
											ImprimirDatosResultado();
											$('#Detalle_Orden').window('close');
											$('#tabla_orden_detallada').datagrid('reload');
										}
										else{
											$('#Detalle_Orden').window('close');
											$('#tabla_orden_detallada').datagrid('reload');
										}
									});
								}
								else{
									$('#Detalle_Orden').window('close');
									$('#tabla_orden_detallada').datagrid('reload');
								}
							});
						}
						
					}
				});
				
				return false;
			}

			
			

		}

		function ImprimirDatosResultado()
		{	
			var idUsuarioLabSigesa 	= $("#idUsuarioparaSigesaLI").val();
			var paciente = $("#paciente_detalle").textbox('getText');
			var historia = $("#historia_detalle").textbox('getText');
			var edad 	 = $("#edad_detalle").textbox('getText');
			var row 	 = $('#tabla_orden_detallada').datagrid('getSelected');  			
			var resulta  = row.RESULTAD;
			var codigo 	 = row.CODIGO;
			var nombpru  = row.NOMBRE;
			var puntocarga  = row.PUNTOCARGA;
			var pera = $('#tabla_resultados').datagrid('getSelected');
			var idorden  = pera.ORDEN;
			var fechareg = pera.FREGISTRO;
			var ordprue  = pera.ORDENPUEBRA;
			var cuenta   = pera.NCUENTA;
			var cama 	 = pera.CAMA;
			var fecresul = $("#fecha_detalle").textbox('getText');
			//var dataResultado 		= $("#tabla_orden_detallada_resultados").datagrid('getData');
			var valores = [];

            $('.btnClick').each(function() {
                valores.push($(this).val());
            });

            var rows = [];
            for (var i = 0; i < valores.length; i++) {
                rows.push({ grupo: valores[i],
                            item: valores[i+1],
                            valornumero: valores[i+2],
                            valortexto: valores[i+3],
                            valorcombo: valores[i+4],
                            valorcheck: valores[i+5],
                            valorreferencial: valores[i+6],
                            metodo: valores[i+7],
                            idproductocpt: valores[i+8],
                            ordenxresultado: valores[i+9]});
                i = i + 9;
            };
			
			//alert(cuenta);return false;
			//if (resulta == 'NO') {
				//$.messager.alert('Laboratorio Clinico HNDAC','No existen resultados para imprimir.');
			//}
			//else{
				var newWin = window.open();
			
				$.ajax({
					url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ImprimirResultados',
					type: 'POST',
					dataType: 'json',
					data: {
						PACIENTE:paciente,
						HISTORIA:historia,
						FECHAREREGISTRO:fechareg,
						EDAD:edad,
						ORDENAPRUEBA:ordprue,
						FECHARESULTADO:fecresul,
						NORDEN: idorden,
						CODIGO: codigo,
						PRUEBA: nombpru,
						PUNTOCARGA: puntocarga,
						RESULTADOS : rows,
						CAMA : cama,
						xCUENTA: cuenta,
						USUARIO: idUsuarioLabSigesa
					},
					success: function(impresion)
					{	
						newWin.document.write(impresion);
				        newWin.document.close();
				        newWin.focus();
				        newWin.print();
				        newWin.close();
					}
				});
			//}
		}

		
		
	//Imprimir Codigo de barra
	
	$("#imprimirResultado_btn").click(function(event) {
		var NroOrdenBar=    $("#idNumOrden").textbox('getText');
		
		if(NroOrdenBar!=''){
		  
			
			location.href="../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=OrdenesLaboratorio_ImprimirOrden&IdOrden="+NroOrdenBar+"&retorno=1" ;
			
			}else{
				//$.messager.alert('Laboratorio Clinico HNDAC','no existe datos.');
				alert('Selecionar Orden ');
				}
			 
			//alert('no existe numero'+NroOrdenBar);
		});
	
		//EVENTOS
		
		$("#Limpiar_btn").click(function(event) {
			Limpiar();
		});

		$("#GrabarResultados_btn").click(function(event) {
			GrabarDatosResultado();
		});

		$("#imprimirResultado_btn").click(function(event) {
			ImprimirDatosResultado();
		});

		$(document).keydown(function(tecla){
            //	TECLA F8
            if (tecla.keyCode == 119) {
                Limpiar();
            }
        });

		
		

	});

	function IngresarResultadosKK(orden,iprod)
		{
			$.ajax({
                url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=ResultadosDetallados',
                type: 'POST',
                dataType: 'json',
                data: {
                    idorden: orden,
                    idproductocpt:iprod
                },
                success:function(data)
                {   
                	$("#tabla_resultados_kk").html(' ');
                	var valor = 1;
                    var content = "<table><tr><th style='width:1%;'>Grupo</th><th style='width:15%;'>Item</th><th style='width:5%;'>Valor Numero</th><th style='width:35%;'>Valor Texto</th><th style='width:11%;'>Valor Combo</th><th style='width:11%;'>Valor Check</th><th style='width:11%;'>Valor Referencial</th><th style='width:11%;'>Metodo</th><th style='display:none;''>idproducto</th><th style='display:none;'>ordenxresultado</th> </tr><tbody>";
                    
                    $.each(data, function(index, val) {
                        
                        if(data[index].GRUPO == null){data[index].GRUPO = " ";}
                        if(data[index].ITEM == null){data[index].ITEM = " ";}
                        if(data[index].VALORNUMERO == null){data[index].VALORNUMERO = " ";}
                        if(data[index].VALORTEXTO == null){data[index].VALORTEXTO = " ";}
                        if(data[index].VALORCOMBO == null){data[index].VALORCOMBO = " ";}
                        if(data[index].VALORCHECK == null){data[index].VALORCHECK = " ";}
                        if(data[index].VALORREFERENCIAL == null){data[index].VALORREFERENCIAL = " ";}
                        if(data[index].METODO == null){data[index].METODO = " ";}
                        
                        content += '<tr>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].GRUPO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].ITEM+'" disabled/></td>';
                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLONUMERO != 1) {
                            content += '<td><input type="text" class="btnClick" onkeypress="return soloLetras(event)" value="'+data[index].VALORNUMERO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" onkeypress="return soloLetras(event)" value="'+data[index].VALORNUMERO+'" data-value="'+valor+'"/></td>';
                            valor = valor + 1;
                        }

                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLOTEXTO != 1) {
                            content += '<td><input type="text" class="btnClick"  value="'+data[index].VALORTEXTO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" data-value="'+valor+'"/></td>';
                            valor = valor + 1;
                        }

                       
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCOMBO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCHECK+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORREFERENCIAL+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].METODO+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].IDPRODCPT+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].ORDENXRESULTADO+'" disabled/></td>';

                        content += '</tr>';
                    }); 
                    content += '</tbody></table>';

                    $("#tabla_resultados_kk").append(content); 
                }
            });
		}

		function ActualizarResultadosKK(codigo_sr)
		{
			$.ajax({
                url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=OrdenesSinResultado',
                type: 'POST',
                dataType: 'json',
                data: {
                    codigo_sr:codigo_sr
                },
                success:function(data)
                {   
                	$("#tabla_resultados_kk").html(' ');
                	var valor = 1;
                    var content = "<table><tr><th style='width:1%;'>Grupo</th><th style='width:15%;'>Item</th><th style='width:5%;'>Valor Numero</th><th style='width:35%;'>Valor Texto</th><th style='width:11%;'>Valor Combo</th><th style='width:11%;'>Valor Check</th><th style='width:11%;'>Valor Referencial</th><th style='width:11%;'>Metodo</th><th style='display:none;''>idproducto</th><th style='display:none;'>ordenxresultado</th> </tr><tbody>";
                    
                    $.each(data, function(index, val) {
                        
                        if(data[index].GRUPO == null){data[index].GRUPO = " ";}
                        if(data[index].ITEM == null){data[index].ITEM = " ";}
                        if(data[index].VALORNUMERO == null){data[index].VALORNUMERO = " ";}
                        if(data[index].VALORTEXTO == null){data[index].VALORTEXTO = " ";}
                        if(data[index].VALORCOMBO == null){data[index].VALORCOMBO = " ";}
                        if(data[index].VALORCHECK == null){data[index].VALORCHECK = " ";}
                        if(data[index].VALORREFERENCIAL == null){data[index].VALORREFERENCIAL = " ";}
                        if(data[index].METODO == null){data[index].METODO = " ";}
                        
                        content += '<tr>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].GRUPO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].ITEM+'" disabled/></td>';
                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLONUMERO != 1) {
                            content += '<td><input type="text" class="btnClick" onkeypress="return soloLetras(event)" value="'+data[index].VALORNUMERO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" onkeypress="return soloLetras(event)" value="'+data[index].VALORNUMERO+'" data-value="'+valor+'"/></td>';
                            valor = valor + 1;
                        }

                        //VERIFICANDO SI SE LLENA NUMERO
                        if (data[index].SOLOTEXTO != 1) {
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" disabled/></td>';
                        }

                        else{
                            content += '<td><input type="text" class="btnClick" value="'+data[index].VALORTEXTO+'" data-value="'+valor+'"/></td>';
                            valor = valor + 1;
                        }

                       
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCOMBO+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORCHECK+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].VALORREFERENCIAL+'" disabled/></td>';
                        content += '<td><input type="text" class="btnClick" value="'+data[index].METODO+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].IDPRODCPT+'" disabled/></td>';
                        content += '<td style="display:none;"><input type="text" class="btnClick" value="'+data[index].ORDENXRESULTADO+'" disabled/></td>';

                        content += '</tr>';
                    }); 
                    content += '</tbody></table>';

                    $("#tabla_resultados_kk").append(content);
                }
            });
		}

	</script>
</head>
<body>
	<div class="easyui-panel"  style="width:100%;height:100%;">
		<!-- CONTENEDORES -->
		<div class="easyui-layout" data-options="fit:true">
            
			<!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:true" style="height:10%;">
            	<div class="easyui-layout" data-options="fit:true">
            		
					<!-- CONTENEDOR: BUSQUEDAS -->
            		<div data-options="region:'west'" title="Busqueda  -  Ingreso de Resultados" style="width:100%;height:30%;padding:1%;" >
            			<input type="text" class="easyui-textbox" data-options="prompt:'N° H.C.'" id="busqueda_nhis" style="width:70px;">&nbsp;&nbsp;
            			<input type="text" class="easyui-textbox" data-options="prompt:'N° Orden'" style="width:70px;" id="buscar_movimi">&nbsp;&nbsp;
            			<input type="text" class="easyui-textbox" data-options="prompt:'N° Cuenta'" style="width:70px;" id="buscar_cuenta">&nbsp;&nbsp;
            			<input type="text" class="easyui-textbox" data-options="prompt:'Apellidos Paterno'" style="width:130px;" id="buscar_nombres">&nbsp;&nbsp;
            			<input type="text" class="easyui-datetimebox" data-options="prompt:'Fecha y Hora Inicio'" value="<?php echo "12/01/2015";?>" id="buscar_fi">&nbsp;&nbsp;
            			<input type="text" class="easyui-datetimebox" data-options="prompt:'Fecha y Hora Final'" value="<?php echo date("m/d/Y");?>" id="buscar_ff">&nbsp;&nbsp;
                        
                        <input type="text" class="easyui-textbox" data-options="prompt:'N° Serie'"  id="busqueda_Serie" style="width:50px;">&nbsp;&nbsp;
            			<input type="text" class="easyui-textbox" data-options="prompt:'N° Boleta'" id="buscar_Boleta" style="width:70px;">&nbsp;&nbsp;
                        
                        
            			<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px">Buscar</a>&nbsp;&nbsp;
            			<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px" id="Limpiar_btn">Limpiar(F8)</a>&nbsp;&nbsp;
            		</div>

            		
            	</div>
            </div>
        	
        	<!-- CONTENEDOR DE RESULTADOS -->
            <div data-options="region:'center'" style="padding:5px;height:40%;">
            	
            	<!-- CONTENEDOR: RESULTADO DE CONSULTA-->
            	<div id="resultados-body" style="height:100%;">
            		<table class="easyui-datagrid" title="Resultados" style="width:100%;height:330px;" data-options="singleSelect:true,collapsible: true,
            		url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=MostrarMascaraBody',
					onClickRow:function(row){	
						var row = $('#tabla_resultados').datagrid('getSelected');
		    			$('#paciente_d_o').textbox('setText',row.PACIENTE);
						$('#idpaciente_d_o').textbox('setText',row.IDPACIENTE);
						$('#nhpaciente_d_o').textbox('setText',row.NHISCLI);
                        $('#idnummovimento').textbox('setText',row.IDMOVIMIENTO);
                        $('#idNumOrden').textbox('setText',row.ORDEN);
                        
                        
						$('#tabla_orden_detallada').datagrid({url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=DetalleOrdenxIdOrden&idorden='+row.ORDEN});

		 			}
            		" id="tabla_resultados">
				        <thead>
				            <tr>			                
				                <th data-options="field:'IDMOVIMIENTO',rezisable:true,align:'center'">N° Movimiento</th>
								<th data-options="field:'NCUENTA',rezisable:true,align:'center'">N° Cuenta</th>
								<th data-options="field:'ORDEN',rezisable:true,align:'center'">N° Orden</th>
								<th data-options="field:'NHISCLI',rezisable:true,align:'center'">N° H.C.</th>
								<th data-options="field:'CAMA',rezisable:true,align:'center'">Cama</th>
								<th data-options="field:'PACIENTE',rezisable:true">Paciente</th>
								<th data-options="field:'FREGISTRO',rezisable:true">Fecha Registro</th>
								<th data-options="field:'ORDENPUEBRA',rezisable:true">Orden Prueba</th>
								<th data-options="field:'FNACIMIENTO',rezisable:true">Fecha Nacimiento</th>
								<th data-options="field:'SEXO',rezisable:true,align:'center'">Sexo</th>
								<th data-options="field:'EDAD',rezisable:true,align:'center'">Edad</th>
								<th data-options="field:'IDPACIENTE',rezisable:true,align:'center',hidden:true">ID</th>
				            </tr>
				        </thead>
				    </table>
            	</div>
            </div>
			<input type="hidden" id="idUsuarioparaSigesaLI" value="<?php echo $_REQUEST['IdEmpleado']?>">
            <!-- CONTENEDOR: DETALLES DE LA ORDEN -->            
            <div data-options="region:'south'" title="Detalles de la Orden" style="height:50%;padding:1%;">
            	<td>Paciente: </td>
            	<td><input type="text" class="easyui-textbox" id="paciente_d_o" style="width:300px;" disabled>&nbsp;&nbsp;&nbsp;</td>
            	<td>IdPaciente:	</td>
            	<td><input type="text" class="easyui-textbox" id="idpaciente_d_o" style="width:60px;" disabled>&nbsp;&nbsp;&nbsp;</td>
            	<td>Numero Historia:</td>
            	<td><input type="text" class="easyui-textbox" id="nhpaciente_d_o" style="width:60px;" disabled>&nbsp;&nbsp;&nbsp;</td>
                <td>Nro.Movi: <input type="text" class="easyui-textbox" id="idnummovimento" style="width:60px;" disabled></td>
                 <td>Nro.Orden: <input type="text" class="easyui-textbox" id="idNumOrden" style="width:60px;" disabled></td>
                  <td> <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:120px" id="imprimirResultado_btn"> Cod.Bar</a></td>
                
                
            	
            	<br><br>
            	<table class="easyui-datagrid" title="Resultados" style="width:100%;height:300px;" data-options="singleSelect:true,
            	onClickRow:function(row){
            		var row = $('#tabla_orden_detallada').datagrid('getSelected');            		
            		var pera = $('#tabla_resultados').datagrid('getSelected');
            		$('#Detalle_Orden').window('setTitle',row.CODIGO+' - '+row.NOMBRE);
            		
            		if(row.RESULTAD == 'SI')
            		{
            			
			            IngresarResultadosKK(row.ORDEN,row.IDPRODCPT);
            			
            			var idorden = row.ORDEN;
						var idproductocpt = row.IDPRODCPT;
						$.ajax({
							url: '../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=datosCabecerasDetalle',
							type: 'POST',
							dataType: 'json',
							data: {
								idorden:idorden,
								idproductocpt:idproductocpt
								},
							success: function(dato)
							{
								$('#realiza_detalle').combobox('setText', dato.REALIZAP);
								$('#fecha_detalle').textbox('setText',dato.FECHA);
							}
						});
            		}
            		if(row.RESULTAD == 'NO'){
            			
            			ActualizarResultadosKK(row.CODIGO);
            			
            			$('#realiza_detalle').combobox('setText', ' ');
						$('#fecha_detalle').textbox('setText','<?php echo date("Y-m-d h:i:s A"); ?>');
            		}
            		
            		
					$('#Detalle_Orden').window('open');
					$('#realiza_detalle').next().find('input').focus();
					var nombre_paciente_d = $('#paciente_d_o').textbox('getText');
					var histori_paciente_d = $('#nhpaciente_d_o').textbox('getText');
					$('#paciente_detalle').textbox('setText',nombre_paciente_d);
					$('#historia_detalle').textbox('setText',histori_paciente_d);
					$('#nacimiento_detalle').textbox('setText',pera.FNACIMIENTO);
					if(pera.SEXO == 'F'){var sexo_l = 'Femenino';}
					if(pera.SEXO == 'M'){var sexo_l = 'Masculino';}
					$('#sexo_detalle').textbox('setText',sexo_l);
					$('#edad_detalle').textbox('setText',pera.EDAD);
					$('#solicitante_detalle').textbox('setText',pera.ORDENPUEBRA);
					

		 			}" id="tabla_orden_detallada">
				        <thead>
				            <tr>			                
				                <th data-options="field:'NUMERO',rezisable:true">N°</th>
								<th data-options="field:'CODIGO',rezisable:true">Codigo</th>
								<th data-options="field:'PUNTOCARGA',rezisable:true">Centro Prod.</th>
								<th data-options="field:'NOMBRE',rezisable:true">Nombre Prueba</th>
								<th data-options="field:'CANTIDAD',rezisable:true,align:'center'">Cantidad</th>
								<th data-options="field:'PRECIO',rezisable:true,align:'right'">Precio</th>
								<th data-options="field:'TOTAL',rezisable:true,align:'right'">Total</th>
								<th data-options="field:'RESULTAD',rezisable:true,align:'center'">Resultado</th>
								<th data-options="field:'ORDEN',rezisable:true,hidden:true">Orden</th>
								<th data-options="field:'IDPRODCPT',rezisable:true,hidden:true">cpt</th>
				            </tr>
				        </thead>
				    </table>
            </div>

        </div>

	</div>

	<div id="Detalle_Orden" class="easyui-window" data-options="modal:true,iconCls:'icon-man',footer:'#ft'" style="width:1100px;height:600px;">
        <div class="easyui-panel" style="width:100%;height:100%;">
        	<div class="easyui-layout" data-options="fit:true">
        		<div data-options="region:'west',split:true" style="width:35%;height:100%;">	
        			<div class="easyui-layout" data-options="fit:true">
						<div data-options="region:'north'" style="width:100%;height:40%;padding:2%;">
            				<table width="100%">
            				<tr>
	            				<td style="width:120px;">Realiza la prueba:</td>
								<td style="width:350px;">
									    <input id="realiza_detalle" class="easyui-combobox" name="dept" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Laboratorio/LaboratorioC.php?acc=MostrarEmpleadoRealizaPrueba'" style="width:270px;" >
								</td>
            				</tr>
            				<tr>
	            				<td style="width:120px;">Fecha Prueba:</td>
	            				<td style="width:350px;"><input type="text" class="easyui-textbox" id="fecha_detalle" style="width:270px;" disabled></td>
            				</tr>
            				<tr>
	            				<td style="width:120px;">Medico Solicitante</td>
	            				<td style="width:350px;"><input type="text" class="easyui-textbox" id="solicitante_detalle" disabled style="width:270px;"></td>
	            			</tr>
	            			</table>
            			</div>

            		
            			<div data-options="region:'center'" style="width:100%;height:60%;padding:2%;" title="Datos Paciente">
            				<table>
            					<tr>
            						<td>Numero Historia:</td>
            						<td><input type="text" class="easyui-textbox" id="historia_detalle" style="width:240px;" disabled></td>
            					</tr>
            					<tr>
            						<td>Paciente:</td>
            						<td><input type="text" class="easyui-textbox" id="paciente_detalle" style="width:240px;" disabled></td>
            					</tr>
            					<tr>
            						<td>Fecha Nacimiento:</td>
            						<td><input type="text" class="easyui-textbox" id="nacimiento_detalle" style="width:240px;" disabled></td>
            					</tr>
            					<tr>
            						<td>Sexo:</td>
            						<td><input type="text" class="easyui-textbox" id="sexo_detalle" style="width:240px;" disabled></td>
            					</tr>
            					<tr>
            						<td>Edad:</td>
            						<td><input type="text" class="easyui-textbox" id="edad_detalle" style="width:240px;" disabled></td>
            					</tr>
            				</table>
            			</div>
        			</div>
        		</div>
        		<div data-options="region:'center',split:true" style="width:65%;">
					<div id="tabla_resultados_kk">
						
					</div>
        		</div>
        	</div>
        	
        </div>
    </div>
    <div id="ft" style="padding:5px;">
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:80px" id="GrabarResultados_btn">Guardar</a>
    	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cancel	'" style="width:80px" onclick="$('#tabla_orden_detallada').datagrid('reload');$('#Detalle_Orden').window('close');">Cerrar</a>
    	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:80px" id="imprimirResultado_btn">Imprimir</a>
    </div>
</body>

	
</html>