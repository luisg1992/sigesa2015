<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - Lista De Verificación Quirúrgica</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript">
           
		   function Nuevo(IdEmpleado){  	
			 window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Nuevo&IdEmpleado="+IdEmpleado;
			//$(location).attr('href','../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Nuevo');
		   }
		   
		   function Modificar(IdListVerAtenciones,IdEmpleado){
				 window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Modificar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;
		   }
		   
		   function Imprimir(CodLista,IdEmpleado){           
			//window.location.href = "../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+CodLista;
		   		window.open("../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+CodLista+"&IdEmpleado="+IdEmpleado,'_blank');
			
		   }
		   function Eliminar(IdListVerAtenciones,IdEmpleado,CodigoListVerAtenciones){
			     /*eliminar=confirm("¿Seguro de eliminar la Verificación Quirúrgica "+CodigoListVerAtenciones+"?");
				   if (eliminar)
				   //Redireccionamos si das a aceptar
					 window.location.href = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Eliminar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;    //página web a la que te redirecciona si confirmas la eliminación
				//else
				  //Y aquí pon cualquier cosa que quieras que salga si le diste al boton de cancelar
					//alert('No se ha eliminado la Verificación Quirúrgica...')	*/			
					alertify.confirm("¿Seguro de eliminar la Verificación Quirúrgica "+CodigoListVerAtenciones+"?", function (e) {
						if (e) {
							//alertify.success("Verificación Quirúrgica eliminada correctamente");
							//Redireccionamos si das a aceptar
					 		window.location.href = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Eliminar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;    //página web a la que te redirecciona si confirmas la eliminación
						} else {
							//alertify.alert("Successful AJAX after Cancel");
							alertify.error('Cancelado')
						}
					});			
		   }
		   
   </script> 

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio">SIGESA - Lista De Verificación Quirúrgica</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul> 
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">LISTA DE VERIFICACIÓN QUIRÚRGICA</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Búsqueda de Pacientes por Nro. Historia Clínica, DNI, Apellidos y Nombres 
                        </div>
                        <div  >
                        <br>
                        <a href="#" class="btn btn-primary btn-block" onClick="Nuevo('<?php echo $IdEmpleado ?>');" > <span class="glyphicon glyphicon-plus"></span> Nuevo  </a>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>NºVerif.</th>
                                            <th>N°Hist.Clínica</th>
                                            <th>DNI</th>
                                            <th>Paciente</th>
                                            <th>Fec.Ingreso</th>
                                            <th>Sala</th>                                             
                                            <!--<th>Estado</th>-->
                                            <th align="center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
									  		$i=0; 
									  		foreach($Listarpacientes as $Pacientes){
												$i++; 
									  			$estado=$Pacientes["Estado"];
												if($estado=='1'){
													$DesEstado='<font color="#0000FF">ACTIVO</font>';
												}else{
													$DesEstado='<font color="#FF0000">INACTIVO</font>';
												}
									  ?>                                 						
 
                                        <tr class="even">
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $Pacientes["CodigoListVerAtenciones"];?></td>
                                            <td><?php echo $Pacientes["NroHistoriaClinica"];?></td>
                                            <td><?php echo $Pacientes["NroDocumento"];?></td>
                                            <td><?php echo $Pacientes["Paciente"];?></td>
                                            <td class="center"><?php echo vfecha(substr($Pacientes["FechaCreacion"],0,10));?></td>
                                            <td class="center"><?php echo $Pacientes["Sala"];?></td>
                                            <?php /*?> <td class="center"><?php echo $DesEstado;?></td><?php */?>
                                            <td class="text-center">                        
                                                     <a href="#" class="btn btn-default btn-sm" onClick="Imprimir('<?php echo $Pacientes["IdListVerAtenciones"]; ?>','<?php echo $IdEmpleado; ?>');"><span class="glyphicon glyphicon-print" ></span> Imprimir</a>
                                                   <?php  if($Pacientes["Estado"]!='0'){?>
                                                   	 <a href="#" class="btn btn-info btn-sm" onClick="Modificar('<?php echo $Pacientes["IdListVerAtenciones"]; ?>','<?php echo $IdEmpleado; ?>');"><span class="glyphicon glyphicon-ok"></span> Editar</a>
                                                     <a href="#" class="btn btn-danger btn-sm" onClick="Eliminar('<?php echo $Pacientes["IdListVerAtenciones"]; ?>','<?php echo $IdEmpleado; ?>','<?php echo $Pacientes["CodigoListVerAtenciones"]; ?>')" ><span class="glyphicon glyphicon-remove" ></span> Eliminar</a>                    
                                         <?php  }?>
                                         </td>
                                         <?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">
                            
                            <!--<div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="success">
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr class="info">
                                            <td>2</td>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr class="warning">
                                            <td>3</td>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                        <tr class="danger">
                                            <td>4</td>
                                            <td>John</td>
                                            <td>Smith</td>
                                            <td>@jsmith</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>-->
                                <h4>COMO REALIZAR BÚSQUEDAS DIRECTAS </h4>
                                <p>Búsqueda de pacientes por Nro. Historia clínica, DNI, Apellidos y Nombres </p><p>
<strong>Eje: Nro. Historia. 162578,  Eje: DNI. 43623262, Eje: Apellidos y Nombres. Ariza Bravo</strong>
</p>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>
    
    <!-- Custom Theme JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
    
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				"language": {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
        });
    });
    </script>

</body>

</html>
