<?php 
ini_set('memory_limit', '1024M'); 
date_default_timezone_set('America/Bogota');
require('../../MVC_Modelo/bscM.php');

require('../../MVC_Modelo/SistemaM.php');
//require('../../MVC_Modelo/EmergenciaM.php');
include('../../MVC_Modelo/Lista_VerificacionM.php');
require('../../MVC_Complemento/fpdf/fpdf.php');
require('../../MVC_Complemento/librerias/Funciones.php');

#OBTENIENDO VALORES


class PDF extends FPDF
{	
	function Header(){	
		//list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
		//$fechainicio = $anio."/".$mes."/".$dia;

		//list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
		//$fechafin = $anio."/".$mes."/".$dia;
		#OBTENIENDO DATOS
		$respuesta = SIGESA_ListVer_ImpresionM($_REQUEST['codigo']);

		$this->SetFont('Helvetica','',9);
		$this->Cell(60);
		$this->Image('../../MVC_Complemento/img/grcallo.jpg',10,5,10,10);
		$this->Cell(40);
		$this->setfont('Helvetica','b',11);
		$this->Cell(70,2,'LISTA DE VERIFICACION '.utf8_decode('QUIRÚRGICA N° ').$respuesta[0]['CODIGO'],0,0,'C');
		$this->Image('../../MVC_Complemento/img/hndac.jpg',280,5,10,10);
		$this->Ln(5);
		
		#SERVICIO
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(85,5,'Servicio: ' . utf8_decode($respuesta[0]['SERVICIO']),0,0,'L'); 		
		#TIPO DE SALA
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(85,5,'Tipo Sala: ' . $respuesta[0]['TIPOSALA'] . ' - ' . $respuesta[0]['SALA'],0,0,'L');		
		#FECHA Y HORA
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(85,5,'Fecha y Hora: ' . vfecha(substr($respuesta[0]['FechaRegistro'],0,10)) . '  ' . $respuesta[0]['horario'],0,0,'L');
		
		$this->Ln(5);
		
		#HISTORIA CLINICA
		$this->SetFont('Helvetica','B',8);		
		$this->Cell(40,5,utf8_decode('Historia clínica: '). $respuesta[0]['NUMEROHISTORIA'],0,0,'L'); 		
		#DNI
		$this->SetFont('Helvetica','B',8);
		$this->Cell(10);
		$this->Cell(30,5,'DNI: ' . $respuesta[0]['NroDocumento'],0,0,'L');
		#SEXO
		$this->SetFont('Helvetica','B',8);
		$this->Cell(10);
		if($respuesta[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($respuesta[0]["IdTipoSexo"]==2){$Sexo='Femenino'; }
		$this->Cell(25,5,'Sexo: ' . $Sexo ,0,0,'L');
		#EDAD		
		$this->SetFont('Helvetica','B',8);
		$this->Cell(20);
		$FechaNacx = time() - strtotime($respuesta[0]["FechaNacimiento"]);$edad = floor((($FechaNacx / 3600) / 24) / 360);
		$this->Cell(25,5,'Edad: ' . $edad ,0,0,'L');		
		#PACIENTE
		$this->SetFont('Helvetica','B',9);
		$this->Cell(10);
		$this->Cell(100,5,'Paciente: ' .utf8_decode($respuesta[0]['PACIENTE']),0,0,'L'); 
		$this->Ln(2);	

	}

	function Footer()
	{
		$ip = $_SERVER['REMOTE_ADDR'];$hostname = gethostbyaddr($ip);
		$usuario=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
		$this->SetY(-6);		
		$this->SetFont('Helvetica','',6);
		$this->Cell(60,5,'TERMINAL: ' .$hostname ,0,0,'L');	
		$this->Cell(60,5,'USUARIO: ' .utf8_decode(strtoupper($usuario[0]['Usuario'])),0,0,'L');	
		$this->Cell(60,5,utf8_decode('FECHA Y HORA DE IMPRESIÓN: ') .date('d/m/Y H:i:s'),0,0,'L');
		$this->Cell(40);
		$this->Cell(100,5,"HNDAC / OESY / DESARROLLO DE SISTEMAS",0,0,'L'); 		 				
	}

}

	$pdf = new PDF("L","mm","A4");
	$pdf->SetTitle('Lista Verificacion');
	$pdf->SetAuthor('Rodolfo Crisanto Rosas');
	$pdf->AliasNbPages();
	$pdf->AddPage();

	#GENERANDO DATOS
	$respuesta = SIGESA_ListVer_ImpresionM($_REQUEST['codigo']);
	
	#OBTENIENDO LAS ALTERNATIVAS MARCADAS
	
		/*$preguntas_contestadas = explode("-", $respuesta[0]['COMBINACIONES']);	
		$preguntas_contestadas = array_filter($preguntas_contestadas);
		
		if( !empty($preguntas_contestadas) ){
			for ($l=0; $l < count($preguntas_contestadas); $l++) { 
				$respuestas[] = explode(",", $preguntas_contestadas[$l]);
			}
		}
		
		if( !empty($respuestas) ){
			for ($o=0; $o < count($respuestas); $o++) { 
				$alternativas_contestadas[] = $respuestas[$o][2];
			}
		}*/
		
		$respuestas = SIGESA_ObtenerAlternativasSeleccionadasM($_REQUEST['codigo']);
			if( !empty($respuestas) ){
				for ($o=0; $o < count($respuestas); $o++) { 
					$alternativas_contestadas[] = $respuestas[$o]['IdAlternativas'];
				}
			}
	#LLENANDO EL DOCUMENTO	
	
		#OBTENIENDO MODULOS
		$modulos = SIGESA_ListVerModulosM();

		$pdf->SetFont('Helvetica','B',8);
		$pdf->Ln(5);
		for ($i=0; $i < count($modulos); $i++) { 
			$pdf->Cell(91,5,$modulos[$i]['Descripcion'],1,0,'C'); 		
		}
		
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',7);
		$pdf->Cell(91,5,'(Con el enfermero y el anestesiologo, como minimo)','LRB',0,'C');
		$pdf->Cell(91,5,'(Con el enfermero y el anestesiologo y cirujano)','LRB',0,'C');
		$pdf->Cell(91,5,'(Con el enfermero y el anestesiologo y cirujano)','LRB',0,'C');
		$pdf->Ln(5);

		#OBTENIENDO PREGUNTAS
		
		$preguntas = Sigesa_ListarPreguntasM();
		$pdf->SetFont('Helvetica','',8);
		
		#MODULO ENTRADA (PREGUNTAS DEL 1 AL 7)
		#=====================================
		
		#PREGUNTA 1:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[1]['Nombre'])),'LR');
		$alternativas = Sigesa_ListarAlternativasM(1);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			if( !empty($alternativas_contestadas) ){
				for ($j=0; $j < count($alternativas_contestadas); $j++) { 
					if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
						$tip = '[ X ]';
					}
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 2:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[2]['Nombre'])),'LRT');
		$alternativas = Sigesa_ListarAlternativasM(2);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 3:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[3]['Nombre'])),'LRT');
		$alternativas = Sigesa_ListarAlternativasM(3);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 4:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[4]['Nombre'])),'LRT');
		$alternativas = Sigesa_ListarAlternativasM(4);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}

		#PREGUNTA 5:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[5]['Nombre'])),'LRT');
		$alternativas = Sigesa_ListarAlternativasM(5);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}
		//$pdf->MultiCell(91,5,"\n",'LR');

		#PREGUNTA 6:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[6]['Nombre'])),'LR');
		$alternativas = Sigesa_ListarAlternativasM(6);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}
		//$pdf->MultiCell(91,5,"\n",'LR');

		#PREGUNTA 7:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(strtolower($preguntas[7]['Nombre']))),'LR');
		$alternativas = Sigesa_ListarAlternativasM(7);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'LR');
		}
		$pdf->MultiCell(91,5,' ','LR');
		$pdf->MultiCell(91,5,' ','LR');
		$pdf->MultiCell(91,5,' ','LR');
		$pdf->MultiCell(91,5,' ','LRB');//PRIMERA COLUMNA
		
		#MODULO DE PAUSA (PREGUNTAS DEL 8 AL 15)
		#=======================================
		
		#SETEANDO PUNTERO
		$pdf->SetXY(101,36);
		
		#PREGUNTA 8:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(strtolower($preguntas[8]['Nombre']))),'R');
		$alternativas = Sigesa_ListarAlternativasM(8);
		$y = 45;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 9:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[9]['Nombre'])),'TR');
		$alternativas = Sigesa_ListarAlternativasM(9);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 10:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('¿Se ha administrado profilaxis antibiótica en los últimos 60 minutos?'),'TR');
		$alternativas = Sigesa_ListarAlternativasM(10);
		$y = $y + 10;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}
		
		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 11:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[11]['Nombre'])),'TR');
		$alternativas = Sigesa_ListarAlternativasM(11);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 12:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[12]['Nombre'])),'R');
		$alternativas = Sigesa_ListarAlternativasM(12);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 13:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[13]['Nombre'])),'R');
		$alternativas = Sigesa_ListarAlternativasM(13);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[ ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			$y = $y + 5;
		}
		
		#SETEANDO PUNTERO
		$y = $y + 5;
		$pdf->SetXY(101,$y);

		#PREGUNTA 14:
		
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[14]['Nombre'])),'R');
		$alternativas = Sigesa_ListarAlternativasM(14);
		$y = $y + 10;
		

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[ ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre'])),'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 15:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('¿Pueden visualizarse las imágenes diagnosticas esenciales?'),'TR');
		$alternativas = Sigesa_ListarAlternativasM(15);
		$y = $y + 5;
		$cadenita = " ";
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$cadenita .= $tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."     ";
			#$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre']))."\n",'R');
			
		}
			$pdf->MultiCell(91,7,$cadenita,'RB');//SEGUNDA COLUMNA 


		#MODULO DE SALIDA (PREGUNTAS 16 17)
		#==================================
		
		#SETEANDO PUNTERO
		$pdf->SetXY(192,37);

		#PREGUNTA 16:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[16]['Nombre'])),'TR');
		$alternativas = Sigesa_ListarAlternativasM(16);
		$y = 36+5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre'])),'R');
			$pdf->SetXY(192,$y+5);
			$pdf->Cell(91,5,'','R',1);
			$y = $y + 10;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(192,$y);

		#PREGUNTA 17:
		
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,ucfirst(strtolower($preguntas[16]['Nombre'])),'TR');
		$alternativas = Sigesa_ListarAlternativasM(17);
		$y = $y + 5;
		

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.ucfirst(strtolower($alternativas[$i]['Nombre'])),'R');
			$y = $y + 5;
		}
		
		#OBSERVACIONES
		#=============
		
		#SETEANDO PUNTERO
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,'Observaciones','TR');
		
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',8);
		$pdf->MultiCell(91,8,str_replace("-*-", ", ", ucfirst(strtolower($respuesta[0]['OBSERVACIONES']))),'R','J');
		$pdf->SetXY(192,$y+5);
		$pdf->Cell(91,10,'','R',1);

						
		#VALIDACIONES
		$y = $y + 15;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,10,'Firma y Sello','TR','L');
		
		$ListVerResponsable1=Sigesa_ListVerResponsablesM($_REQUEST['codigo'],'1');	//CIRUJANO
		$ListVerResponsable2=Sigesa_ListVerResponsablesM($_REQUEST['codigo'],'2');	//ANESTESIOLGO	
		$ListVerResponsable7=Sigesa_ListVerResponsablesM($_REQUEST['codigo'],'7');	//ENFERMERA CIRCULANTE
			#CIRUJANO
			$y = $y + 10;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',7);
			$pdf->MultiCell(91,10,ltrim(utf8_decode(mb_strtoupper($ListVerResponsable1[0]['Responsable']))),'R','C');
			$y = $y + 3;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','b',6);
			$pdf->MultiCell(91,10,'MEDICO CIRUJANO','R','C');

			#ANESTESIOLGO
			$y = $y + 10;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',7);
			if($ListVerResponsable2[0]['Responsable']!=NULL){
				$ANESTESIOLOGO=$ListVerResponsable2[0]['Responsable'];
			}else{$ANESTESIOLOGO='-';}
			$pdf->MultiCell(91,15,ltrim(utf8_decode(mb_strtoupper($ANESTESIOLOGO))),'R','C');
			$y = $y + 3;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','b',6);
			$pdf->MultiCell(91,15,'MEDICO ANESTESIOLOGO','R','C');

			#ENFERMERO CIRCULANTE 
			$y = $y + 11;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',7);
			if($ListVerResponsable7[0]['Responsable']!=NULL){
				$INSTRU=$ListVerResponsable7[0]['Responsable'];
			}else{$INSTRU='-';}
			$pdf->MultiCell(91,20,ltrim(utf8_decode(mb_strtoupper($INSTRU))),'R','C');
			$y = $y + 3;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','b',6);
			$pdf->MultiCell(91,20,'ENFERMERA CIRCULANTE','R','C');

			#ENFERMERO INSTRUMENTISTA
			$y = $y + 11;
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,10,' ','BR');

		#FECHA Y HORA
		#=============
		
		/*$y = $y + 10;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',7);
		$pdf->MultiCell(91,10,'FECHA: '.vfecha(substr($respuesta[0]['FechaRegistro'],0,10)),'R', 'C');
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',7);
		$pdf->MultiCell(91,10,'HORA: '.$respuesta[0]['horario'],'RB', 'C');*/

		
	
	$pdf->Output();
?>

