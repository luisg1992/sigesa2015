<!DOCTYPE html>
 
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>SIGESA - Lista de Verificacion </title>

        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/ListaVerificacion.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
		<style>
          p {
            background: yellow;
          }
        </style>
        
        <script type="text/javascript">
			/*function Limpiar(){	    
				$('#NroDocumento').val('');
				$('#NroHistoriaClinica').val('');	
				$('#ApellidosNombrespaciente').val('');			
				$('#IdPaciente').val('');	
				$('#dnival').val('');					
				$('#DatosPaciente').hide();	
			}*/
			
			function Regresar(){				
				window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
			}
			
			function Imprimir(){
				var codigo_exp = document.getElementById('IdListVerAtenciones4').value; 
				var IdEmpleado = document.getElementById('IdEmpleado').value; 
				//window.location.href = "../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+codigo_exp;
				window.open('../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo='+codigo_exp+"&IdEmpleado="+IdEmpleado,'_blank');
				
			}
			
			//INICIO
			function llenachkIdAlternativas() {
				var validarcheckbox = document.getElementsByClassName('validarcheckboxI');
				var IdAlternativasChk=''; var ischecked_chk = false;
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxI' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxI' + i).value;
							IdAlternativasChk=IdAlternativasChk+chkreg+'|';
							document.getElementById('IdAlternativasChk').value=IdAlternativasChk;
							ischecked_chk = true;
						} 
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdAlternativasChk').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }
             }
			 
			 function llenaradioIdAlternativas(){
				var validarradio = document.getElementsByClassName('validarradio');
				var IdAlternativasRad='';
				for ( var i = 1; i <= validarradio.length; i++) {						
						if (document.getElementById('radio' + i).checked == true) {	
								var chkreg = document.getElementById('radio' + i).value;
								IdAlternativasRad=IdAlternativasRad+chkreg+'|';
								document.getElementById('IdAlternativasRad').value=IdAlternativasRad;
								//alert(chkreg);
						}//end if	
				}//end for*/
             }
			 
			 //PAUSA
			 function llenachkIdAlternativasP() {
				var validarcheckbox = document.getElementsByClassName('validarcheckboxP');
				var IdAlternativasChk=''; var ischecked_chk = false;
				//alert(validarcheckbox.length);
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxP' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxP' + i).value;
							IdAlternativasChk=IdAlternativasChk+chkreg+'|';
							document.getElementById('IdAlternativasChkP').value=IdAlternativasChk;
							ischecked_chk = true;
						} 
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdAlternativasChkP').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }
             }
			 
			 function llenaradioIdAlternativasP(){
				var validarradio = document.getElementsByClassName('validarradioP');
				var IdAlternativasRad='';
				for ( var i = 1; i <= validarradio.length; i++) {						
						if (document.getElementById('radioP' + i).checked == true) {	
								var chkreg = document.getElementById('radioP' + i).value;
								IdAlternativasRad=IdAlternativasRad+chkreg+'|';
								document.getElementById('IdAlternativasRadP').value=IdAlternativasRad;
								//alert(chkreg);
						}//end if	
				}//end for*/
             }
			 
			 //SALIDA
			 function llenachkIdAlternativasS() {
				var validarcheckbox = document.getElementsByClassName('validarcheckboxS');
				var IdAlternativasChk=''; var ischecked_chk = false;
				//alert(validarcheckbox.length);
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxS' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxS' + i).value;
							IdAlternativasChk=IdAlternativasChk+chkreg+'|';
							document.getElementById('IdAlternativasChkS').value=IdAlternativasChk;
							ischecked_chk = true;
						} 
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdAlternativasChkS').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }
             }
			 
			 function llenaradioIdAlternativasS(){
				var validarradio = document.getElementsByClassName('validarradioS');
				var IdAlternativasRad='';
				for ( var i = 1; i <= validarradio.length; i++) {						
						if (document.getElementById('radioS' + i).checked == true) {	
								var chkreg = document.getElementById('radioS' + i).value;
								IdAlternativasRad=IdAlternativasRad+chkreg+'|';
								document.getElementById('IdAlternativasRadS').value=IdAlternativasRad;
								//alert(chkreg);
						}//end if	
				}//end for*/
             }
				 
			 
		</script>
    </head>
    <body>
    
 
    
<div class="container-fluid">
   

   <div class="col-md-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading"  >
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab">Identificar</a></li>
                            <li class = "desabilitar"><a href="#tab2primary" data-toggle="tab">Inicio</a></li>
                            <li class = "desabilitar"><a href="#tab3primary" data-toggle="tab">Pausa</a></li>
                            <li class = "desabilitar"><a href="#tab4primary" data-toggle="tab">Salida</a></li>
                           
                            
                        </ul>
                </div>
                <div class="panel-body">
                  <div class="tab-content">
                    <!--Inicio Identificacion-->
                        <div class="tab-pane fade in active" id="tab1primary">
                        <!--Contenido-->
                        
                        
                           <div class="container-fluid">
                             <div class="panel panel-default">
  								<div class="panel-body">
                             <div class="row">
                                 <div class="col-md-12">
                                  <form class="form-inline" id="error-paciente"  >
                                      <div class="form-group">
                                        <label for="NroHistoriaClinica">Nro.HistClinica:</label>
                                        <input type="text" class="form-control" id="NroHistoriaClinica" name="NroHistoriaClinica" placeholder="Hist.Clinica" disabled />
                                      </div>
                                      <div class="form-group">
                                        <label for="NroDocumento">Nro. DNI:</label>
                                        <input class="form-control" id="NroDocumento" name="NroDocumento" placeholder="DNI" disabled />
                                        <input type="hidden" id="IdPaciente" name="IdPaciente" value="<?php echo $ListVerModi[0]["IdPaciente"]; ?>" />
                                       
                                      </div>
                                      <div class="form-group">
                                        <label for="ApellidosNombrespaciente">Apellidos y Nombres:</label>
                                        <input class="form-control" id="ApellidosNombrespaciente" name="ApellidosNombrespaciente" placeholder="Apellidos y Nombres" disabled />
                                      </div>
                                      <!--<input type="button" value="Limpiar" onClick="Limpiar()" class="btn btn-group-sm btn-primary"  >-->
                                    </form>
                                </div>
  								  </div>
  								  
                              </div>
                              </div>
                              
                              <div class="panel panel-default" id="DatosPaciente" style="display: block;">
  								<div class="panel-body">
                              <div class="row">
                                 <div class="col-md-12">
                                 		<div class="row">
                                         <div class="col-md-2"><strong>DNI:</strong></div>
                                         <div class="col-md-2" id="DNI"><?php echo $ListVerModi[0]["NroDocumento"]; ?></div>
                                         <div class="col-md-2"><strong>Nro.His.Cli:</strong></div>
                                         <div class="col-md-2" id="nrohiscli"><?php echo $ListVerModi[0]["NroHistoriaClinica"]; ?></div>
                                         <div class="col-md-2"><strong>Fecha Atencion:</strong></div>
                                         <div class="col-md-2"><?php echo vfecha(substr($ListVerModi[0]["FechaRegistro"],0,10)) ?></div>
                                      </div>                                
                                 </div>  								  
                              </div>
                              
                              <div class="row">
                                 <div class="col-md-12">
                                 		<div class="row">
                                         <div class="col-md-2"><strong>Paciente:</strong></div>
                                         <div class="col-md-2" id="Paciente"><?php echo utf8_decode($ListVerModi[0]["PACIENTE"]); ?></div>
                                         <div class="col-md-2"><strong>Fecha Nacimiento:</strong></div>
                                         <div class="col-md-2" id="FechaNac"><?php echo vfecha(substr($ListVerModi[0]["FechaNacimiento"],0,10)) ?></div>
                                         <?php 
										 	$FechaNacx = time() - strtotime($ListVerModi[0]["FechaNacimiento"]);
											$edad = floor((($FechaNacx / 3600) / 24) / 360);
											if($ListVerModi[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($ListVerModi[0]["IdTipoSexo"]==2){$Sexo='Femenino';}				
										 ?>
                                         <div class="col-md-2"><strong>Edad:</strong></div>
                                         <div class="col-md-2" id="Edad"><?php echo $edad ?></div>
                                      </div>                                
                                 </div>  								  
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                 	 <div class="row">
                                         <div class="col-md-2"><strong>Sexo:</strong></div>
                                         <div class="col-md-2" id="Sexo"><?php echo $Sexo ?></div>
                                         <div class="col-md-2"><strong>Domicilio:</strong></div>
                                         <div class="col-md-2" id="Domicilio"><?php echo $ListVerModi[0]["departamentodomicilio"].' - '.$ListVerModi[0]["provinciadomicilio"].' - '.$ListVerModi[0]["distritodomicilio"] ?></div>
                                         <div class="col-md-2"><strong>Lugar Nacim.</strong></div>
                                         <div class="col-md-2" id="LugarNacim"><?php echo $ListVerModi[0]["departamentonac"].' - '.$ListVerModi[0]["provincianac"].' - '.$ListVerModi[0]["distritonac"] ?></div>                                         
                                      </div>                                
                                 </div>  								  
                              </div>
                               </div>  								  
                              </div>
							  
							  <form id="FormLisVer" name="FormLisVer"  method="POST"> 
                              <div class="panel panel-default">
  								<div class="panel-body">                 
                              
                                <div class="row">
                                 <div class="col-md-12">                                 	  
                                     
                                       <input type="hidden" id="dnival" name="dnival" value="<?php echo $ListVerModi[0]["IdPaciente"] ?>" >                                       
                                       <input type="hidden" id="accion1" name="accion1" value="A" >
                                       <input type="hidden" id="IdListVerAtenciones" name="IdListVerAtenciones" value="<?php echo $IdListVerAtenciones ?>" >                                       
                                       <input type="hidden" id="IdEmpleado" name="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'] ?>" >
                                       
                                        <div id="TipoSala-group" class="form-group row" >                                        
                                          <label class="col-sm-2" for="TipoSala">Tipo Sala:</label>
                                          <div class="col-sm-2">                                           
                                              <label class="form-check-label">
                                                <input  type="radio" name="TipoSala" id="TipoSala" <?php if($ListVerModi[0]["IdTipoSala"]=='1'){ ?> checked <?php } ?> value="1"  >
                                                Electiva
                                              </label>
                                            </div>                                            
                                            <div class="col-sm-2">
                                              <label class="form-check-label">
                                                <input  type="radio" name="TipoSala" id="TipoSala" value="2" <?php if($ListVerModi[0]["IdTipoSala"]=='2'){ ?> checked <?php } ?> >
                                                Emergencia
                                              </label>
                                            </div>                                    
                                        </div>
                                       
                                        <div id="Sala-group" class="form-group row" >
                                          <label class="col-sm-2" for="Sala">Sala:</label>
                                          <div class="col-sm-4">
                                             <select id="Sala" name="Sala" class="form-control" style="width:300px;">                                         	
                                             	<option value="Select">SELECCIONE</option>
                                                <?php 
													$listasalas=SIGESA_ListVer_BuscarSalaXTipo_M($ListVerModi[0]["IdTipoSala"]);
													for ($i=0; $i < count($listasalas); $i++) { 												
												?>
                                                <option value="<?php echo  $listasalas[$i]["IdSala"] ?>" <?php if($ListVerModi[0]["IdSala"]==$listasalas[$i]["IdSala"]){ ?> selected <?php } ?> ><?php echo  $listasalas[$i]["Descripcion"] ?></option>
                                                <?php } ?>
                                             </select>
                                          </div>                                          
                                        </div>
                                        
                                        <div id="Servicio-group" class="form-group row">
                                          <label class="col-sm-2"  for="Servicio">Servicio:</label>
                                          <div class="col-sm-4">                                              
                                             <select id="Servicio" name="Servicio" class="form-control" style="width:300px;">
                                                <option value="">SELECCIONE</option>
                                                     <?php 
                                                        $resultados=ListarServiciosVerificacionQuirurgicaM();								 
                                                        if($resultados!=NULL){
                                                        for ($i=0; $i < count($resultados); $i++) {	
                                                     ?>
                                                 <option value="<?php echo $resultados[$i]["IdServicio"] ?>" <?php if($ListVerModi[0]["IdServicio"]==$resultados[$i]["IdServicio"]){ ?> selected <?php } ?>  ><?php echo $resultados[$i]["IdServicio"].' | '.mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
                                                 <?php 
                                                    }}
                                                 ?>                   
                                            </select>                                        
                                          </div>
                                         
                                        </div> 
                                          
                                          
                                          
                                                                    
                                     
                                    </div>                                  
  								  
                              </div>
                               </div>  								  
                              </div>
                              <div class="panel panel-default">
  								<div class="panel-body">
                              <div class="row">                      
                              	  <div class="col-md-12">                                	 
                                      <div class="col-sm-1"></div>
                                      <div class="col-sm-2"></div>
                                      <div class="col-sm-1"><strong>DNI</strong></div>
                                      <div class="col-sm-3"><strong>Profesional</strong></div>
                                      <div class="col-sm-3"><strong>Especialidad</strong></div>                                      
                                      <div class="col-sm-1"><strong>Colegiatura</strong></div>
                                      <div class="col-sm-1"><strong>RNE</strong></div>                               
                                  </div>  								  
                              </div>
                              
                              	<?php 
									$ListVerResponsables1=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'1');
									$ListVerResponsables2=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'2');
									$ListVerResponsables3=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'3');
									$ListVerResponsables4=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'4');
									$ListVerResponsables5=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'5');
									$ListVerResponsables6=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'6');
									$ListVerResponsables7=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'7');
								?>
                              
                               <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div id="IdCirujano-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Cirujano:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Cirujano"  placeholder="Cirujano">
                                        <input type="hidden" id="IdCirujano" name="IdCirujano" value="<?php echo $ListVerResponsables1[0]["IdMedico"] ?>">
                                      </div>                                     
                                      
                                      <div class="col-sm-1" id="CirujanoDNI"><?php echo $ListVerResponsables1[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="CirujanoMedico"><?php echo $ListVerResponsables1[0]["Responsable"] ?></div>
                                      <div class="col-sm-3" id="CirujanoTiposEmpleado"><?php echo $ListVerResponsables1[0]["Descripcion"] ?></div>  
                                      <div class="col-sm-1" id="CirujanoColegiatura"><?php echo $ListVerResponsables1[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Cirujanorne" ><?php echo $ListVerResponsables1[0]["rne"] ?></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div id="IdAnestesiologo-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Anestesiologo:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Anestesiologo" placeholder="Anestesiologo">
                                        <input type="hidden" id="IdAnestesiologo" name="IdAnestesiologo" value="<?php echo $ListVerResponsables2[0]["IdMedico"] ?>">
                                      </div>
                                      <div class="col-sm-1" id="AnestesiologoDNI"><?php echo $ListVerResponsables2[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="AnestesiologoMedico"><?php echo $ListVerResponsables2[0]["Responsable"] ?></div>
                                      <div class="col-sm-3" id="AnestesiologoTiposEmpleado"><?php echo $ListVerResponsables2[0]["Descripcion"] ?></div>                         			  <div class="col-sm-1" id="AnestesiologoColegiatura"><?php echo $ListVerResponsables2[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Anestesiologorne" ><?php echo $ListVerResponsables2[0]["rne"] ?></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Instrumentista:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Instrumentista" placeholder="Instrumentista">
                                        <input type="hidden" id="IdInstrumentista" name="IdInstrumentista" value="<?php echo $ListVerResponsables3[0]["IdMedico"] ?>">
                                      </div>
                                      <div class="col-sm-1" id="InstrumentistaDNI"><?php echo $ListVerResponsables3[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="InstrumentistaMedico"><?php echo $ListVerResponsables3[0]["Responsable"] ?></div>
                                      <div class="col-sm-3" id="InstrumentistaTiposEmpleado"><?php echo $ListVerResponsables3[0]["Descripcion"] ?></div>      
                                      <div class="col-sm-1" id="InstrumentistaColegiatura"><?php echo $ListVerResponsables3[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Instrumentistarne" ><?php echo $ListVerResponsables3[0]["rne"] ?></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente1:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Asistente1" placeholder="Asistente1">
                                        <input type="hidden" id="IdAsistente1" name="IdAsistente1" value="<?php echo $ListVerResponsables4[0]["IdMedico"] ?>">
                                      </div>
                                      <div class="col-sm-1" id="Asistente1DNI"><?php echo $ListVerResponsables4[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="Asistente1Medico"><?php echo $ListVerResponsables4[0]["Responsable"] ?></div>
                                      <div class="col-sm-3" id="Asistente1TiposEmpleado"><?php echo $ListVerResponsables4[0]["Descripcion"] ?></div>                                      <div class="col-sm-1" id="Asistente1Colegiatura"><?php echo $ListVerResponsables4[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Asistente1rne" ><?php echo $ListVerResponsables4[0]["rne"] ?></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente2:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Asistente2" placeholder="Asistente2">
                                        <input type="hidden" id="IdAsistente2" name="IdAsistente2" value="<?php echo $ListVerResponsables5[0]["IdMedico"] ?>">
                                      </div>
                                      <div class="col-sm-1" id="Asistente2DNI"><?php echo $ListVerResponsables5[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="Asistente2Medico"><?php echo $ListVerResponsables5[0]["Responsable"] ?></div>
                                      <div class="col-sm-3" id="Asistente2TiposEmpleado"><?php echo $ListVerResponsables5[0]["Descripcion"] ?></div>                                      <div class="col-sm-1" id="Asistente2Colegiatura"><?php echo $ListVerResponsables5[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Asistente2rne" ><?php echo $ListVerResponsables5[0]["rne"] ?></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente3:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Asistente3" placeholder="Asistente3">
                                        <input type="hidden" id="IdAsistente3" name="IdAsistente3" value="<?php echo $ListVerResponsables6[0]["IdMedico"] ?>">
                                      </div>
                                      <div class="col-sm-1" id="Asistente3DNI"><?php echo $ListVerResponsables6[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="Asistente3Medico"><?php echo $ListVerResponsables6[0]["Responsable"] ?></div>
                                      <div class="col-sm-3" id="Asistente3TiposEmpleado"><?php echo $ListVerResponsables6[0]["Descripcion"] ?></div>                                      <div class="col-sm-1" id="Asistente3Colegiatura"><?php echo $ListVerResponsables6[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Asistente3rne" ><?php echo $ListVerResponsables6[0]["rne"] ?></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">              
                              	<div class="col-md-12"> 
                                 	<div id="IdEnfermeraCirculante1-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Enf.Circulante:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="EnfermeraCirculante1" placeholder="EnfermeraCirculante">
                                        <input type="hidden" id="IdEnfermeraCirculante1" name="IdEnfermeraCirculante1" value="<?php echo $ListVerResponsables7[0]["IdMedico"] ?>">
                                      </div>
                                      <div class="col-sm-1" id="EnfermeraCirculante1DNI"><?php echo $ListVerResponsables7[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante1Medico"><?php echo $ListVerResponsables7[0]["Responsable"] ?></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante1TiposEmpleado"><?php echo $ListVerResponsables7[0]["Descripcion"] ?></div>                                      <div class="col-sm-1" id="EnfermeraCirculante1Colegiatura"><?php echo $ListVerResponsables7[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="EnfermeraCirculante1rne" ><?php echo $ListVerResponsables7[0]["rne"] ?></div>  
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <!--<div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">EnfermeraCirculante2:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="EnfermeraCirculante2" placeholder="EnfermeraCirculante2">
                                        <input type="text" id="IdEnfermeraCirculante2" name="IdEnfermeraCirculante2">
                                      </div>
                                      <div class="col-sm-1" id="EnfermeraCirculante2DNI"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante2Medico"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante2TiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="EnfermeraCirculante2Colegiatura"></div>
                                      <div class="col-sm-1" id="EnfermeraCirculante2rne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>-->
                              
                              
                              
                              </div>  								  
                              </div>
                              
                              
                              <div class="panel panel-default">
  								<div class="panel-body">
                                
                              <div class="row">
                                 <div class="col-md-9">
                                 <?php /*?><a href="#tab2primary" data-toggle="tab" class="btn btn-block btn-success">Guardar</a>
                                 <input type="button" value="Guardar" class="btn btn-default" onClick="$('.nav-tabs a[href='#tab4primary']').tab('show');"/><?php */?>
                                 <!--<input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar1" />-->
                                 <input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar1" />
                                 
                                  
                                 </div>
  								 <div class="col-md-3 text-right"><input type="button" class="btn   btn-danger" value="Regresar" onClick="Regresar()" /></div>
                              </div>
                              
                              </div>  								  
                              </div>
                              
                              </form>
                            </div>
                        <!--Contenido-->
                      </div>
                    <!--Primary 1-->
                        <div class="tab-pane fade" id="tab2primary">
                        <!------ Inicio Inicio--------->
                        <!--<form action="Guardar" method="get">-->                      
                                               
                        <form id="FormInicio" name="FormInicio"  method="POST">                     
                        		 <?php 
								 	$FechaInicioEntrada=$ListVerModi[0]["FechaInicioEntrada"];
									if($FechaInicioEntrada==NULL){
										$FechaInicioEntrada=date('Y-m-d H:i:s');
										$validartab='1';
									}else{
										$validartab='2';
									}
									$FechaInicioPausa=$ListVerModi[0]["FechaInicioPausa"];
									if($FechaInicioPausa==NULL){
										$FechaInicioPausa=date('Y-m-d H:i:s');
									}else{
										$validartab='3';
									}
									$FechaInicioSalida=$ListVerModi[0]["FechaInicioSalida"];
									if($FechaInicioSalida==NULL){
										$FechaInicioSalida=date('Y-m-d H:i:s');
									}else{
										$validartab='4';
									}
								 ?>	
                                 <input type="hidden" name="FechaInicioEntrada" id="FechaInicioEntrada" value="<?php echo $FechaInicioEntrada; ?>"  /> 
                                 <input type="hidden" name="IdListVerAtenciones2" id="IdListVerAtenciones2" value="<?php echo $IdListVerAtenciones ?>" >
                                 <input type="hidden" name="validartab" id="validartab" value="<?php echo $validartab ?>" >
                                 <?php 									
									$respuestasChk=Sigesa_ListVerRespuestasXtipoM($IdListVerAtenciones,'I','V');									
									$rptaConcatChk='';
									for ($i=0; $i < count($respuestasChk); $i++) { 	
										$rptaConcatChk=$rptaConcatChk.$respuestasChk[$i]["IdAlternativas"].'|';								
									}
									
									$respuestasRad=Sigesa_ListVerRespuestasXtipoM($IdListVerAtenciones,'I','U');
									$rptaConcatRad='';
									for ($i=0; $i < count($respuestasRad); $i++) { 	
										$rptaConcatRad=$rptaConcatRad.$respuestasRad[$i]["IdAlternativas"].'|';								
									}										
								?>	
                                <input type="hidden" name="IdAlternativasChk" id="IdAlternativasChk" value="<?php echo $rptaConcatChk ?>" >
                                <input type="hidden" name="IdAlternativasRad" id="IdAlternativasRad" value="<?php echo $rptaConcatRad ?>" > 
                                                              
                                 <?php  foreach($ListarEntrada as $itemEntr){ ?>                                 
                                        <div class="col-md-4">
                                            <h4><?php echo $itemEntr["IdPreguntas"]?>: <?php echo $itemEntr["Nombre"]?></h4>
						<?php  
                            $ListarAternativas=SIGESA_ListVer_ListarAlternativas_C($itemEntr["IdPreguntas"]);
                            $respuestasI=Sigesa_ListVerRespuestasM($IdListVerAtenciones,'I',$itemEntr["IdPreguntas"]);								
                        ?>	
                                            <div class="funkyradio">                                   
                                             <?php 	
											 											 	 
											 	if($itemEntr["Tipo"]=='V'){
													$opcV=$opcV; $cant=0;													
											    	foreach($ListarAternativas as $itemEntrAlter){ 													
													$opcV++;																									
											  ?>                                              
                                                   <div class="funkyradio-primary">
                                        	<input type="checkbox" name="checkboxI<?php echo $opcV ?>" id="checkboxI<?php echo $opcV?>" value="<?php echo $itemEntrAlter["IdAlternativas"]?>" class="validarcheckboxI" onclick="llenachkIdAlternativas()" 
											<?php if($respuestasI[0]["IdAlternativas"]==$itemEntrAlter["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasI[1]["IdAlternativas"]==$itemEntrAlter["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasI[2]["IdAlternativas"]==$itemEntrAlter["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasI[3]["IdAlternativas"]==$itemEntrAlter["IdAlternativas"]){ ?> checked
                                            <?php }else if($respuestasI[4]["IdAlternativas"]==$itemEntrAlter["IdAlternativas"]){ ?> checked <?php } ?> />
                                       		<label for="checkboxI<?php echo $opcV?>" class="text-center"><?php echo $itemEntrAlter["Nombre"]?></label>
                                                   </div>                                                
													<?php  $cant++;	} ?>                                               
                                                <?php
												 }else{  
													$opcO=$opcO;
													foreach($ListarAternativas as $itemEntrAlter){	
													$opcO++; 
											   ?>
                                               <div class="funkyradio-primary">
                                                <input type="radio" name="radioI<?php echo $itemEntrAlter["IdPreguntas"]?>" id="radio<?php echo $opcO?>" value="<?php echo $itemEntrAlter["IdAlternativas"]?>" class="validarradio" onclick="llenaradioIdAlternativas()" <?php if($respuestasI[0]["IdAlternativas"]==$itemEntrAlter["IdAlternativas"]){ ?> checked <?php } ?> />
                                                <label for="radio<?php echo $opcO?>" class="text-center"><?php echo $itemEntrAlter["Nombre"]?></label>                       						   </div>
                                               <?php } ?>
                                               
                                          <?php  } ?>                                                 
                                              
<div class="clearfix visible-xs-block"></div>
                                            </div>
                                            <div class="clearfix visible-xs-block"></div>
                                        </div>
                                     <?php  } ?>                                    
                                    
                                    
                          <div class="panel panel-default">
  							<div class="panel-body">                                
                              <div class="row">
                                 <div class="col-md-9">                                 
                                 	<!--<a   data-toggle="tab" class="btn btn-block btn-success" onClick="Siguentepagina1()">Guardar</a>-->
                                 	<!--<input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar2" />-->
                                 	<input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar2" />
                                 </div>
  								 <div class="col-md-3 text-right"><input type="button" class="btn   btn-danger" value="Regresar" onClick="Regresar()" /></div>
                              </div>                              
                            </div>  								  
                          </div>
                          </form>
                        <!-------Finar Inicio--------->                        
                        </div>
                        
                        
                        <div class="tab-pane fade" id="tab3primary">
                        
                        	<!--<form action="Guardar" method="get">-->
                            <form id="FormPausa" name="FormPausa"  method="POST">  
                                                 
                                 <input type="hidden" name="FechaInicioPausa" id="FechaInicioPausa" value="<?php echo $FechaInicioPausa; ?>"  />
                                 <input type="hidden" name="IdListVerAtenciones3" id="IdListVerAtenciones3" value="<?php echo $IdListVerAtenciones ?>" >
                                 <?php 									
									$respuestasChk=Sigesa_ListVerRespuestasXtipoM($IdListVerAtenciones,'P','V');									
									$rptaConcatChk='';
									for ($i=0; $i < count($respuestasChk); $i++) { 	
										$rptaConcatChk=$rptaConcatChk.$respuestasChk[$i]["IdAlternativas"].'|';								
									}
									
									$respuestasRad=Sigesa_ListVerRespuestasXtipoM($IdListVerAtenciones,'P','U');
									$rptaConcatRad='';
									for ($i=0; $i < count($respuestasRad); $i++) { 	
										$rptaConcatRad=$rptaConcatRad.$respuestasRad[$i]["IdAlternativas"].'|';								
									}										
								?>
                                 <input type="hidden" name="IdAlternativasChkP" id="IdAlternativasChkP" value="<?php echo $rptaConcatChk ?>" >
                                 <input type="hidden" name="IdAlternativasRadP" id="IdAlternativasRadP" value="<?php echo $rptaConcatRad ?>" >
                                                                
                                 <?php  foreach($ListarPausa as $itemPausa){ ?>                                 
                                        <div class="col-md-4">
                                            <h4><?php echo $itemPausa["IdPreguntas"]?>: <?php echo $itemPausa["Nombre"]?></h4>
						<?php  
                            $ListarAternativas=SIGESA_ListVer_ListarAlternativas_C($itemPausa["IdPreguntas"]);
							$respuestasP=Sigesa_ListVerRespuestasM($IdListVerAtenciones,'P',$itemPausa["IdPreguntas"]);	
                        ?>	
                                            <div class="funkyradio">
                                              
												 <?php 
												 	if($itemPausa["Tipo"]=='V'){
													   $opcVP=$opcVP;
                                                       foreach($ListarAternativas as $itemPausa){
                                                       $opcVP++;   
                                                ?>
                                                <div class="funkyradio-success">
                                       				<input type="checkbox" name="checkboxP<?php echo $opcVP?>" id="checkboxP<?php echo $opcVP?>" value="<?php echo $itemPausa["IdAlternativas"]?>" class="validarcheckboxP" onclick="llenachkIdAlternativasP()" 
											<?php if($respuestasP[0]["IdAlternativas"]==$itemPausa["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasP[1]["IdAlternativas"]==$itemPausa["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasP[2]["IdAlternativas"]==$itemPausa["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasP[3]["IdAlternativas"]==$itemPausa["IdAlternativas"]){ ?> checked
                                            <?php }else if($respuestasP[4]["IdAlternativas"]==$itemPausa["IdAlternativas"]){ ?> checked <?php } ?>	/>	                                               	   <label for="checkboxP<?php echo $opcVP?>" class="text-center"><?php echo $itemPausa["Nombre"]?></label>
                                                </div>
                                                <?php  } ?>
                                            <?php  }else{ ?>
                                            <?php  
												$opcOP=$opcOP;
												foreach($ListarAternativas as $itemPausa){ 
												$opcOP++;
											?>
                                              <div class="funkyradio-success">
                                                <input type="radio" name="radioP<?php echo $itemPausa["IdPreguntas"]?>" id="radioP<?php echo $opcOP?>" value="<?php echo $itemPausa["IdAlternativas"]?>" class="validarradioP" onclick="llenaradioIdAlternativasP()" <?php if($respuestasP[0]["IdAlternativas"]==$itemPausa["IdAlternativas"]){ ?> checked <?php } ?> />
                                                <label for="radioP<?php echo $opcOP?>" class="text-center"><?php echo $itemPausa["Nombre"]?></label>
                                            </div>
                                            <?php } ?>
                                           <?php } ?>                                                 
                                                
<div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                     <?php  } ?>                                        
                                    <!--</form>-->
                                <div class="panel panel-default">
  									<div class="panel-body">                                
                                      <div class="row">
                                         <div class="col-md-9">
                                            <!--<a  data-toggle="tab" class="btn btn-block btn-success" onClick="Siguentepagina2()">Guardar</a>-->
                                            <!--<input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar3" />-->
                                            <input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar3" />
                                         </div>
                                         <div class="col-md-3 text-right"><input type="button" class="btn   btn-danger" value="Regresar" onClick="Regresar();" /></div>
                                      </div>                              
                            	  </div>  								  
                              </div>
                               </form>
                        <!-------Finar Pausa--------->                               
                        </div> 
                                             
                        <div class="tab-pane fade" id="tab4primary"> 
                        
                        	<!--<form action="Guardar" method="get">-->
                                 <form id="FormSalida" name="FormSalida"  method="POST">  
                                                 
                                 <input type="hidden" name="FechaInicioSalida" id="FechaInicioSalida" value="<?php echo $FechaInicioSalida; ?>"  />
                                 <input type="hidden" name="IdListVerAtenciones4" id="IdListVerAtenciones4" value="<?php echo $IdListVerAtenciones ?>" >
                                 <?php 									
									$respuestasChk=Sigesa_ListVerRespuestasXtipoM($IdListVerAtenciones,'S','V');									
									$rptaConcatChk='';
									for ($i=0; $i < count($respuestasChk); $i++) { 	
										$rptaConcatChk=$rptaConcatChk.$respuestasChk[$i]["IdAlternativas"].'|';								
									}
									
									$respuestasRad=Sigesa_ListVerRespuestasXtipoM($IdListVerAtenciones,'S','U');
									$rptaConcatRad='';
									for ($i=0; $i < count($respuestasRad); $i++) { 	
										$rptaConcatRad=$rptaConcatRad.$respuestasRad[$i]["IdAlternativas"].'|';								
									}										
								?>
                                 <input type="hidden" name="IdAlternativasChkS" id="IdAlternativasChkS" value="<?php echo $rptaConcatChk ?>" >
                                 <input type="hidden" name="IdAlternativasRadS" id="IdAlternativasRadS" value="<?php echo $rptaConcatRad ?>" >
                                                                
                                 <?php  foreach($ListarSalida as $itemSalida){ ?>
                                 
                                        <div class="col-md-4">
                                            <h4><?php echo $itemSalida["IdPreguntas"]?>: <?php echo $itemSalida["Nombre"]?></h4>
						<?php  
                            $ListarAternativas=SIGESA_ListVer_ListarAlternativas_C($itemSalida["IdPreguntas"]);
							$respuestasS=Sigesa_ListVerRespuestasM($IdListVerAtenciones,'S',$itemSalida["IdPreguntas"]);	 
                        ?>	
                                            <div class="funkyradio">
                                              
                                             <?php if($itemSalida["Tipo"]=='V'){?>
											
											<?php  
												$opcVS=$opcVS;
												foreach($ListarAternativas as $itemVSalida){													 
												$opcVS++;
											?>
                                                <div class="funkyradio-success">
                                        		  <input type="checkbox" name="checkboxS<?php echo $opcVS?>" id="checkboxS<?php echo $opcVS?>" value="<?php echo $itemVSalida["IdAlternativas"]?>" class="validarcheckboxS" onClick="llenachkIdAlternativasS()" 
										    <?php if($respuestasS[0]["IdAlternativas"]==$itemVSalida["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasS[1]["IdAlternativas"]==$itemVSalida["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasS[2]["IdAlternativas"]==$itemVSalida["IdAlternativas"]){ ?> checked 
											<?php }else if($respuestasS[3]["IdAlternativas"]==$itemVSalida["IdAlternativas"]){ ?> checked
                                            <?php }else if($respuestasS[4]["IdAlternativas"]==$itemVSalida["IdAlternativas"]){ ?> checked <?php } ?>	/>
                                                  <label for="checkboxS<?php echo $opcVS?>" class="text-center"><?php echo $itemVSalida["Nombre"]?></label>
                                                </div>
                                                <?php  } ?>
                                            <?php  }else{ ?>
                                            <?php 
													$opcOS=$opcOS; 																							
													foreach($ListarAternativas as $itemVSalida){ 
													$opcOS++;
													
													
											?>
                                             <div class="funkyradio-success">
                                                <input type="radio" name="radioS<?php echo $itemVSalida["IdPreguntas"]?>" id="radioS<?php echo $opcOS?>" value="<?php echo $itemVSalida["IdAlternativas"]?>" class="validarradio3" onClick="llenaradioIdAlternativasS()" <?php if($respuestasS[0]["IdAlternativas"]==$itemVSalida["IdAlternativas"]){ ?> checked <?php } ?> />
                                                <label for="radioS<?php echo $opcOS?>" class="text-center"><?php echo $itemVSalida["Nombre"]?></label>
                                            </div>
                                            	<?php } ?>
                                            <?php  } ?>                        

                                            </div>
                                        </div>
                                     <?php  } ?>

                                         <div class="col-md-6">
                                         	 <label for="textarea" for="Observacion">Observacion</label>
 											 <textarea name="Observacion" id="Observacion" cols="45" rows="5" class="form-control"><?php echo $ListVerModi[0]["Observacion"]; ?></textarea>
                                         </div>
                                    <!--</form>-->
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-body">                                
                                              <div class="row">
                                                 <div class="col-md-9">
                                                        <!--<a   data-toggle="tab" class="btn btn-block btn-success" onClick="Siguentepagina3()">Guardar</a>-->
                                                        <!--<input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar4" />-->
                                                        <input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar4" />	
                                                 </div>
                                                 <div class="col-md-3 text-right">
                                                 	   <input type="button" class="btn  btn-danger" value="Regresar" onClick="Regresar();" />
                                                       <input type="button" class="btn  btn-default" value="Imprimir" onClick="Imprimir();" />
                                                 </div>
                                              </div>                                      
                                        </div>  								  
                              		</div>
                                    </form>
                              </div>
                    </div>
                </div>
            </div>
        </div>
</div>
	
	
	
	
	
	
	    <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/ListaVerificacionMod.js"></script> 
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
     
    </body>
</html>
