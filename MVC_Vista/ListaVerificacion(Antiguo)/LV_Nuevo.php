<!DOCTYPE html>
 
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>SIGESA - Lista de Verificacion </title>

        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/ListaVerificacion.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
		<style>
          p {
            background: yellow;
          }
        </style>
        
        <script type="text/javascript">
			function Limpiar(){	    
				$('#NroDocumento').val('');
				$('#NroHistoriaClinica').val('');	
				$('#ApellidosNombrespaciente').val('');			
				$('#IdPaciente').val('');	
				$('#dnival').val('');					
				$('#DatosPaciente').hide();	
			}
			
			function Regresar(){				
				window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
			}
			
			function Imprimir(){
				var codigo_exp = document.getElementById('IdListVerAtenciones4').value; 
				var IdEmpleado = document.getElementById('IdEmpleado').value; 
				//window.location.href = "../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+codigo_exp;
				window.open('../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo='+codigo_exp+"&IdEmpleado="+IdEmpleado,'_blank');
				
			}
			
			//INICIO
			function llenachkIdAlternativas() {
				var validarcheckbox = document.getElementsByClassName('validarcheckboxI');
				var IdAlternativasChk=''; var ischecked_chk = false;
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxI' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxI' + i).value;
							IdAlternativasChk=IdAlternativasChk+chkreg+'|';
							document.getElementById('IdAlternativasChk').value=IdAlternativasChk;
							ischecked_chk = true;
						} 
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdAlternativasChk').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }
             }
			 
			 function llenaradioIdAlternativas(){
				var validarradio = document.getElementsByClassName('validarradio');
				var IdAlternativasRad='';
				for ( var i = 1; i <= validarradio.length; i++) {						
						if (document.getElementById('radio' + i).checked == true) {	
								var chkreg = document.getElementById('radio' + i).value;
								IdAlternativasRad=IdAlternativasRad+chkreg+'|';
								document.getElementById('IdAlternativasRad').value=IdAlternativasRad;
								//alert(chkreg);
						}//end if	
				}//end for*/
             }
			 
			 //PAUSA
			 function llenachkIdAlternativasP() {
				var validarcheckbox = document.getElementsByClassName('validarcheckboxP');
				var IdAlternativasChk=''; var ischecked_chk = false;
				//alert(validarcheckbox.length);
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxP' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxP' + i).value;
							IdAlternativasChk=IdAlternativasChk+chkreg+'|';
							document.getElementById('IdAlternativasChkP').value=IdAlternativasChk;
							ischecked_chk = true;
						} 
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdAlternativasChkP').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }
             }
			 
			 function llenaradioIdAlternativasP(){
				var validarradio = document.getElementsByClassName('validarradioP');
				var IdAlternativasRad='';
				for ( var i = 1; i <= validarradio.length; i++) {						
						if (document.getElementById('radioP' + i).checked == true) {	
								var chkreg = document.getElementById('radioP' + i).value;
								IdAlternativasRad=IdAlternativasRad+chkreg+'|';
								document.getElementById('IdAlternativasRadP').value=IdAlternativasRad;
								//alert(chkreg);
						}//end if	
				}//end for*/
             }
			 
			 //SALIDA
			 function llenachkIdAlternativasS() {
				var validarcheckbox = document.getElementsByClassName('validarcheckboxS');
				var IdAlternativasChk=''; var ischecked_chk = false;
				//alert(validarcheckbox.length);
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxS' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxS' + i).value;
							IdAlternativasChk=IdAlternativasChk+chkreg+'|';
							document.getElementById('IdAlternativasChkS').value=IdAlternativasChk;
							ischecked_chk = true;
						} 
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdAlternativasChkS').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }
             }
			 
			 function llenaradioIdAlternativasS(){
				var validarradio = document.getElementsByClassName('validarradioS');
				var IdAlternativasRad='';
				for ( var i = 1; i <= validarradio.length; i++) {						
						if (document.getElementById('radioS' + i).checked == true) {	
								var chkreg = document.getElementById('radioS' + i).value;
								IdAlternativasRad=IdAlternativasRad+chkreg+'|';
								document.getElementById('IdAlternativasRadS').value=IdAlternativasRad;
								//alert(chkreg);
						}//end if	
				}//end for*/
             }
				 
			 
		</script>
    </head>
    <body>
    
 
    
<div class="container-fluid">
   

   <div class="col-md-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading"  >
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab">Identificar</a></li>
                            <li class = "desabilitar"><a href="#tab2primary" data-toggle="tab">Inicio</a></li>
                            <li class = "desabilitar"><a href="#tab3primary" data-toggle="tab">Pausa</a></li>
                            <li class = "desabilitar"><a href="#tab4primary" data-toggle="tab">Salida</a></li>
                           
                            
                        </ul>
                </div>
                <div class="panel-body">
                  <div class="tab-content">
                    <!--Inicio Identificacion-->
                        <div class="tab-pane fade in active" id="tab1primary">
                        <!--Contenido-->
                        
                        
                           <div class="container-fluid">
                             <div class="panel panel-default">
  								<div class="panel-body">
                             <div class="row">
                                 <div class="col-md-12">
                                  <form class="form-inline" id="error-paciente"  >
                                      <div class="form-group">
                                        <label for="NroHistoriaClinica">Nro.HistClinica:</label>
                                        <input type="text" class="form-control" id="NroHistoriaClinica" name="NroHistoriaClinica" placeholder="Hist.Clinica">
                                      </div>
                                      <div class="form-group">
                                        <label for="NroDocumento">Nro. DNI:</label>
                                        <input class="form-control" id="NroDocumento" name="NroDocumento" placeholder="DNI" />
                                        <input type="hidden" id="IdPaciente" name="IdPaciente" />
                                       
                                      </div>
                                      <div class="form-group">
                                        <label for="ApellidosNombrespaciente">Apellidos y Nombres:</label>
                                        <input class="form-control" id="ApellidosNombrespaciente" name="ApellidosNombrespaciente" placeholder="Apellidos y Nombres" />
                                      </div>
                                      <input type="button" value="Limpiar" onClick="Limpiar()" class="btn btn-group-sm btn-primary" />
                                    </form>
                                </div>
  								  </div>
  								  
                              </div>
                              </div>
                              
                              <div class="panel panel-default" id="DatosPaciente" style="display: none;">
  								<div class="panel-body">
                              <div class="row">
                                 <div class="col-md-12">
                                 		<div class="row">
                                         <div class="col-md-2"><strong>DNI:</strong></div>
                                         <div class="col-md-2" id="DNI"></div>
                                         <div class="col-md-2"><strong>Nro.His.Cli:</strong></div>
                                         <div class="col-md-2" id="nrohiscli"></div>
                                         <div class="col-md-2"><strong>Fecha Atencion:</strong></div>
                                         <div class="col-md-2"><?php echo date('d/m/Y H:i:s'); ?></div>
                                      </div>                                
                                 </div>  								  
                              </div>
                              
                              <div class="row">
                                 <div class="col-md-12">
                                 		<div class="row">
                                         <div class="col-md-2"><strong>Paciente:</strong></div>
                                         <div class="col-md-2" id="Paciente">  </div>
                                         <div class="col-md-2"><strong>Fecha Nacimiento:</strong></div>
                                         <div class="col-md-2" id="FechaNac"></div>
                                         <div class="col-md-2"><strong>Edad:</strong></div>
                                         <div class="col-md-2" id="Edad"> </div>
                                      </div>                                
                                 </div>  								  
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                 		<div class="row">
                                         <div class="col-md-2"><strong>Sexo:</strong></div>
                                         <div class="col-md-2" id="Sexo"></div>
                                         <div class="col-md-2"><strong>Domicilio:</strong></div>
                                         <div class="col-md-2" id="Domicilio"></div>
                                         <div class="col-md-2"><strong>Lugar Nacim.</strong></div>
                                         <div class="col-md-2" id="LugarNacim"></div>
                                      </div>                                
                                 </div>  								  
                              </div>
                               </div>  								  
                              </div>
							  
							  <form id="FormLisVer" name="FormLisVer"  method="POST"> 
                              <div class="panel panel-default">
  								<div class="panel-body">                 
                              
                                <div class="row">
                                 <div class="col-md-12">                                 	  
                                     
                                     <input type="hidden" id="dnival" name="dnival" >                                       
                                       <input type="hidden" id="accion1" name="accion1" value="R" >
                                       <input type="hidden" id="IdListVerAtenciones" name="IdListVerAtenciones" value="0" >                                       <input type="hidden" id="IdEmpleado" name="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'] ?>" >
                                       
                                        <div id="TipoSala-group" class="form-group row" >                                        
                                          <label class="col-sm-2" for="TipoSala">Tipo Sala:</label>
                                          <div class="col-sm-2">                                           
                                              <label class="form-check-label">
                                                <input  type="radio" name="TipoSala" id="TipoSala" value="1" >
                                                Electiva
                                              </label>
                                            </div>                                            
                                            <div class="col-sm-2">
                                              <label class="form-check-label">
                                                <input  type="radio" name="TipoSala" id="TipoSala" value="2">
                                                Emergencia
                                              </label>
                                            </div>                                    
                                        </div>
                                       
                                        <div id="Sala-group" class="form-group row" >
                                          <label class="col-sm-2" for="Sala">Sala:</label>
                                          <div class="col-sm-4">
                                             <select id="Sala" name="Sala" class="form-control" style="width:300px;">
                                             	<option value="Select">SELECCIONE</option>
                                             </select>
                                          </div>                                          
                                        </div>
                                        
                                        <div id="Servicio-group" class="form-group row">
                                          <label class="col-sm-2"  for="Servicio">Servicio:</label>
                                          <div class="col-sm-4">                                              
                                             <select id="Servicio" name="Servicio" class="form-control" style="width:300px;">
                                                <option value="">SELECCIONE</option>
                                                     <?php 
                                                        $resultados=ListarServiciosVerificacionQuirurgicaM();								 
                                                        if($resultados!=NULL){
                                                        for ($i=0; $i < count($resultados); $i++) {	
                                                     ?>
                                                 <option value="<?php echo $resultados[$i]["IdServicio"] ?>"  ><?php echo $resultados[$i]["IdServicio"].' | '.mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
                                                 <?php 
                                                    }}
                                                 ?>                   
                                            </select>                                        
                                          </div>
                                         
                                        </div> 
                                          
                                          
                                          
                                                                    
                                     
                                    </div>                                  
  								  
                              </div>
                               </div>  								  
                              </div>
                              <div class="panel panel-default">
  								<div class="panel-body">
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	 
                                      
                                      <div class="col-sm-1"></div>
                                      <div class="col-sm-2"></div>
                                      <div class="col-sm-1"><strong>DNI</strong></div>
                                      <div class="col-sm-3"><strong>Profesional</strong></div>
                                      <div class="col-sm-3"><strong>Especialidad</strong></div>                                      
                                      <div class="col-sm-1"><strong>Colegiatura</strong></div>
                                      <div class="col-sm-1"><strong>RNE</strong></div>
                                      
                                      
                                    
                                 </div>  								  
                              </div>
                               <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div id="IdCirujano-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Cirujano:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Cirujano"  placeholder="Cirujano">
                                        <input type="hidden" id="IdCirujano" name="IdCirujano">
                                      </div>
                                      <div class="col-sm-1" id="CirujanoDNI"></div>
                                      <div class="col-sm-3" id="CirujanoMedico"></div>
                                      <div class="col-sm-3" id="CirujanoTiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="CirujanoColegiatura"></div>
                                      <div class="col-sm-1" id="Cirujanorne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div id="IdAnestesiologo-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Anestesiologo:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Anestesiologo" placeholder="Anestesiologo">
                                        <input type="hidden" id="IdAnestesiologo" name="IdAnestesiologo">
                                      </div>
                                      <div class="col-sm-1" id="AnestesiologoDNI"></div>
                                      <div class="col-sm-3" id="AnestesiologoMedico"></div>
                                      <div class="col-sm-3" id="AnestesiologoTiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="AnestesiologoColegiatura"></div>
                                      <div class="col-sm-1" id="Anestesiologorne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Instrumentista:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Instrumentista" placeholder="Instrumentista">
                                        <input type="hidden" id="IdInstrumentista" name="IdInstrumentista">
                                      </div>
                                      <div class="col-sm-1" id="InstrumentistaDNI"></div>
                                      <div class="col-sm-3" id="InstrumentistaMedico"></div>
                                      <div class="col-sm-3" id="InstrumentistaTiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="InstrumentistaColegiatura"></div>
                                      <div class="col-sm-1" id="Instrumentistarne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente1:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Asistente1" placeholder="Asistente1">
                                        <input type="hidden" id="IdAsistente1" name="IdAsistente1">
                                      </div>
                                      <div class="col-sm-1" id="Asistente1DNI"></div>
                                      <div class="col-sm-3" id="Asistente1Medico"></div>
                                      <div class="col-sm-3" id="Asistente1TiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="Asistente1Colegiatura"></div>
                                      <div class="col-sm-1" id="Asistente1rne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente2:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Asistente2" placeholder="Asistente2">
                                        <input type="hidden" id="IdAsistente2" name="IdAsistente2">
                                      </div>
                                      <div class="col-sm-1" id="Asistente2DNI"></div>
                                      <div class="col-sm-3" id="Asistente2Medico"></div>
                                      <div class="col-sm-3" id="Asistente2TiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="Asistente2Colegiatura"></div>
                                      <div class="col-sm-1" id="Asistente2rne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente3:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="Asistente3" placeholder="Asistente3">
                                        <input type="hidden" id="IdAsistente3" name="IdAsistente3">
                                      </div>
                                      <div class="col-sm-1" id="Asistente3DNI"></div>
                                      <div class="col-sm-3" id="Asistente3Medico"></div>
                                      <div class="col-sm-3" id="Asistente3TiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="Asistente3Colegiatura"></div>
                                      <div class="col-sm-1" id="Asistente3rne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">              
                              	<div class="col-md-12"> 
                                 	<div id="IdEnfermeraCirculante1-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Enf.Circulante:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="EnfermeraCirculante1" placeholder="EnfermeraCirculante">
                                        <input type="hidden" id="IdEnfermeraCirculante1" name="IdEnfermeraCirculante1">
                                      </div>
                                      <div class="col-sm-1" id="EnfermeraCirculante1DNI"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante1Medico"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante1TiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="EnfermeraCirculante1Colegiatura"></div>
                                      <div class="col-sm-1" id="EnfermeraCirculante1rne" ></div>  
                                    </div>
                                 </div>  								  
                              </div>
                              
                              <!--<div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">EnfermeraCirculante2:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="EnfermeraCirculante2" placeholder="EnfermeraCirculante2">
                                        <input type="text" id="IdEnfermeraCirculante2" name="IdEnfermeraCirculante2">
                                      </div>
                                      <div class="col-sm-1" id="EnfermeraCirculante2DNI"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante2Medico"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante2TiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="EnfermeraCirculante2Colegiatura"></div>
                                      <div class="col-sm-1" id="EnfermeraCirculante2rne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>-->
                              
                              
                              
                              </div>  								  
                              </div>
                              
                              
                              <div class="panel panel-default">
  								<div class="panel-body">
                                
                              <div class="row">
                                 <div class="col-md-9">
                                 <?php /*?><a href="#tab2primary" data-toggle="tab" class="btn btn-block btn-success">Guardar</a>
                                 <input type="button" value="Guardar" class="btn btn-default" onClick="$('.nav-tabs a[href='#tab4primary']').tab('show');"/><?php */?>
                                 <input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar1" />
                                 <input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar1" />
                                 
                                  
                                 </div>
  								 <div class="col-md-3 text-right"><input type="button" class="btn   btn-danger" value="Regresar" onClick="Regresar()" /></div>
                              </div>
                              
                              </div>  								  
                              </div>
                              
                              </form>
                            </div>
                        <!--Contenido-->
                      </div>
                    <!--Primary 1-->
                        <div class="tab-pane fade" id="tab2primary">
                        <!------ Inicio Inicio--------->
                        <!--<form action="Guardar" method="get">-->
                        <form id="FormInicio" name="FormInicio"  method="POST">
                        
                                 <input type="hidden" name="FechaInicioEntrada" id="FechaInicioEntrada" value="<?php echo date('Y-m-d H:i:s'); ?>"  /> 
                                 <input type="hidden" name="IdListVerAtenciones2" id="IdListVerAtenciones2" >                                 
                                 <input type="hidden" name="IdAlternativasChk" id="IdAlternativasChk" >
                                 <input type="hidden" name="IdAlternativasRad" id="IdAlternativasRad" >
                                                                
                                 <?php  foreach($ListarEntrada as $itemEntr){ ?>
                                 
                                        <div class="col-md-4">
                                            <h4><?php echo $itemEntr["IdPreguntas"]?>: <?php echo $itemEntr["Nombre"]?></h4>
<?php  $ListarAternativas=SIGESA_ListVer_ListarAlternativas_C($itemEntr["IdPreguntas"]) ?>	
                                            <div class="funkyradio">
                                              
                                             <?php 	
											 											 	 
											 	if($itemEntr["Tipo"]=='V'){
													$opcV=$opcV;	
											    	foreach($ListarAternativas as $itemEntrAlter){ 	
													$opcV++; 												
											  ?>                                              
                                                   <div class="funkyradio-primary">
                                        	<input type="checkbox" name="checkboxI<?php echo $opcV ?>" id="checkboxI<?php echo $opcV?>" value="<?php echo $itemEntrAlter["IdAlternativas"]?>" class="validarcheckboxI" onclick="llenachkIdAlternativas()"  />
                                       		<label for="checkboxI<?php echo $opcV?>" class="text-center"><?php echo $itemEntrAlter["Nombre"]?></label>
                                                   </div>                                                
													<?php  }  ?>                                               
                                                <?php
												 }else{  
													$opcO=$opcO;
													foreach($ListarAternativas as $itemEntrAlter){
													$opcO++; 
											   ?>
                                               <div class="funkyradio-primary">
                                                <input type="radio" name="radioI<?php echo $itemEntrAlter["IdPreguntas"]?>" id="radio<?php echo $opcO?>" value="<?php echo $itemEntrAlter["IdAlternativas"]?>" class="validarradio" onclick="llenaradioIdAlternativas()" />
                                                <label for="radio<?php echo $opcO?>" class="text-center"><?php echo $itemEntrAlter["Nombre"]?></label>                       						   </div>
                                               <?php } ?>
                                               
                                          <?php  } ?>                                                 
                                              
<div class="clearfix visible-xs-block"></div>
                                            </div>
                                            <div class="clearfix visible-xs-block"></div>
                                        </div>
                                     <?php  } ?>                                    
                                    
                                    
                          <div class="panel panel-default">
  							<div class="panel-body">                                
                              <div class="row">
                                 <div class="col-md-9">                                 
                                 	<!--<a   data-toggle="tab" class="btn btn-block btn-success" onClick="Siguentepagina1()">Guardar</a>-->
                                 	<input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar2" />
                                 	<input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar2" />
                                 </div>
  								 <div class="col-md-3 text-right"><input type="button" class="btn   btn-danger" value="Regresar" onClick="Regresar()" /></div>
                              </div>                              
                            </div>  								  
                          </div>
                          </form>
                        <!-------Finar Inicio--------->                        
                        </div>
                        
                        
                        <div class="tab-pane fade" id="tab3primary">
                        
                        	<!--<form action="Guardar" method="get">-->
                            <form id="FormPausa" name="FormPausa"  method="POST">  
                                                 
                                 <input type="hidden" name="FechaInicioPausa" id="FechaInicioPausa" value="<?php echo date('Y-m-d H:i:s'); ?>"  />
                                 <input type="hidden" name="IdListVerAtenciones3" id="IdListVerAtenciones3" >
                                 <input type="hidden" name="IdAlternativasChkP" id="IdAlternativasChkP" >
                                 <input type="hidden" name="IdAlternativasRadP" id="IdAlternativasRadP" >
                                                                
                                 <?php  foreach($ListarPausa as $itemPausa){ ?>                                 
                                        <div class="col-md-4">
                                            <h4><?php echo $itemPausa["IdPreguntas"]?>: <?php echo $itemPausa["Nombre"]?></h4>
<?php  $ListarAternativas=SIGESA_ListVer_ListarAlternativas_C($itemPausa["IdPreguntas"]) ?>	
                                            <div class="funkyradio">
                                              
												 <?php 
												 	if($itemPausa["Tipo"]=='V'){
													   $opcVP=$opcVP;
                                                       foreach($ListarAternativas as $itemPausa){ 
                                                       $opcVP++;   
                                                ?>
                                                <div class="funkyradio-success">
                                       				<input type="checkbox" name="checkboxP<?php echo $opcVP?>" id="checkboxP<?php echo $opcVP?>" value="<?php echo $itemPausa["IdAlternativas"]?>" class="validarcheckboxP" onclick="llenachkIdAlternativasP()"   />
                                                    <label for="checkboxP<?php echo $opcVP?>" class="text-center"><?php echo $itemPausa["Nombre"]?></label>
                                                </div>
                                                <?php  } ?>
                                            <?php  }else{ ?>
                                            <?php  
												$opcOP=$opcOP;
												foreach($ListarAternativas as $itemPausa){ 
												$opcOP++;
											?>
                                              <div class="funkyradio-success">
                                                <input type="radio" name="radioP<?php echo $itemPausa["IdPreguntas"]?>" id="radioP<?php echo $opcOP?>" value="<?php echo $itemPausa["IdAlternativas"]?>" class="validarradioP" onclick="llenaradioIdAlternativasP()" />
                                                <label for="radioP<?php echo $opcOP?>" class="text-center"><?php echo $itemPausa["Nombre"]?></label>
                                            </div>
                                            <?php  } ?>
                                            <?php  } ?>  
                                                
                                                
<div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                     <?php  } ?>                                        
                                    <!--</form>-->
                                <div class="panel panel-default">
  									<div class="panel-body">                                
                                      <div class="row">
                                         <div class="col-md-9">
                                            <!--<a  data-toggle="tab" class="btn btn-block btn-success" onClick="Siguentepagina2()">Guardar</a>-->
                                            <input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar3" />
                                            <input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar3" />
                                         </div>
                                         <div class="col-md-3 text-right"><input type="button" class="btn   btn-danger" value="Regresar" onClick="Regresar();" /></div>
                                      </div>                              
                            	  </div>  								  
                              </div>
                               </form>
                        <!-------Finar Pausa--------->                               
                        </div> 
                                             
                        <div class="tab-pane fade" id="tab4primary"> 
                        
                        	<!--<form action="Guardar" method="get">-->
                                 <form id="FormSalida" name="FormSalida"  method="POST">  
                                                 
                                 <input type="hidden" name="FechaInicioSalida" id="FechaInicioSalida" value="<?php echo date('Y-m-d H:i:s'); ?>"  />
                                 <input type="hidden" name="IdListVerAtenciones4" id="IdListVerAtenciones4" >
                                 <input type="hidden" name="IdAlternativasChkS" id="IdAlternativasChkS" >
                                 <input type="hidden" name="IdAlternativasRadS" id="IdAlternativasRadS" >
                                                                
                                 <?php  foreach($ListarSalida as $itemSalida){ ?>
                                 
                                        <div class="col-md-4">
                                            <h4><?php echo $itemSalida["IdPreguntas"]?>: <?php echo $itemSalida["Nombre"]?></h4>
<?php  $ListarAternativas=SIGESA_ListVer_ListarAlternativas_C($itemSalida["IdPreguntas"]) ?>	
                                            <div class="funkyradio">
                                              
                                             <?php if($itemSalida["Tipo"]=='V'){?>
											
											<?php  
												$opcVS=$opcVS;
												foreach($ListarAternativas as $itemAlterPausa){ 
												$opcVS++;
											?>
                                                <div class="funkyradio-success">
                                        		  <input type="checkbox" name="checkboxS<?php echo $opcVS?>" id="checkboxS<?php echo $opcVS?>" value="<?php echo $itemAlterPausa["IdAlternativas"]?>" class="validarcheckboxS" onClick="llenachkIdAlternativasS()"  />
                                                  <label for="checkboxS<?php echo $opcVS?>" class="text-center"><?php echo $itemAlterPausa["Nombre"]?></label>
                                                </div>
                                                <?php  } ?>
                                            <?php  }else{ ?>
                                            <?php 
													$opcOS=$opcOS; 
													foreach($ListarAternativas as $itemAlterPausa){ 
													$opcOS++;
											?>
                                             <div class="funkyradio-success">
                                                <input type="radio" name="radioS<?php echo $itemAlterPausa["IdPreguntas"]?>" id="radioS<?php echo $opcOS?>" value="<?php echo $itemAlterPausa["IdAlternativas"]?>" class="validarradio3" onClick="llenaradioIdAlternativasS()" />
                                                <label for="radioS<?php echo $opcOS?>" class="text-center"><?php echo $itemAlterPausa["Nombre"]?></label>
                                            </div>
                                            	<?php } ?>
                                            <?php  } ?>                        

                                            </div>
                                        </div>
                                     <?php  } ?>

                                         <div class="col-md-6">
                                         	 <label for="textarea" for="Observacion">Observacion</label>
 											 <textarea name="Observacion" id="Observacion" cols="45" rows="5" class="form-control"></textarea>
                                         </div>
                                    <!--</form>-->
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-body">                                
                                              <div class="row">
                                                 <div class="col-md-9">
                                                        <!--<a   data-toggle="tab" class="btn btn-block btn-success" onClick="Siguentepagina3()">Guardar</a>-->
                                                        <input type="submit" value="Guardar" class="btn btn-block btn-success"  id="Guardar4" />
                                                        <input type="submit" value="Actualizar" class="btn btn-block btn-primary"  id="Actualizar4" />	
                                                 </div>
                                                 <div class="col-md-3 text-right">
                                                 	   <input type="button" class="btn  btn-danger" value="Regresar" onClick="Regresar();" />
                                                       <input type="button" class="btn  btn-default" value="Imprimir" onClick="Imprimir();" />
                                                 </div>
                                              </div>                                      
                                        </div>  								  
                              		</div>
                                    </form>
                              </div>
                    </div>
                </div>
            </div>
        </div>
</div>
	
	
	
	
	
	
	    <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/ListaVerificacion.js"></script> 
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
     
    </body>
</html>
