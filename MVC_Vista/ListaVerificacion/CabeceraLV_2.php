<?php 
if($_GET['IdEmpleado']==''){
	$IdEmpleado=$_SESSION['IdEmpleado'];		
		
	$ApellidoPaterno = $_SESSION['ApellidoPaterno'];	
	$ApellidoMaterno = $_SESSION['ApellidoMaterno'];	
	$Nombres = $_SESSION['Nombres'];	
	$DNI = $_SESSION['DNI'];
	$loginPC = $_SESSION['loginPC'];
	
}else{
	$IdEmpleado=$_GET['IdEmpleado'];
	
	$ListarUsuarioxIdempleado=ListarUsuarioxIdempleado_M($IdEmpleado);	
	$ApellidoPaterno = $ListarUsuarioxIdempleado[0]['ApellidoPaterno'];	
	$ApellidoMaterno = $ListarUsuarioxIdempleado[0]['ApellidoMaterno'];	
	$Nombres = $ListarUsuarioxIdempleado[0]['Nombres'];	
	$DNI = $ListarUsuarioxIdempleado[0]['DNI'];
	$loginPC = $ListarUsuarioxIdempleado[0]['loginPC'];
	
}

//Validar empleado tiene permiso: Modificar Clave Responsables LV
$RolesPermisosXidEmpleado=RolesPermisosXidEmpleado_M($IdEmpleado);
$l=0; 
do {	
	if ($RolesPermisosXidEmpleado[$l]['IdPermiso']=='500') {
		$IdPermiso='1';		
	}	
	$l++;
} while ( $l < count($RolesPermisosXidEmpleado) );
//echo 'pp'.$IdPermiso;

?>

<!--modal de CambiarClave-->
<div class="modal fade" id="my_modalCambiarClave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">      
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       		<h4 class="modal-title" id="exampleModalLabel">Cambiar Clave Usuario</h4>        
        </div>
      	<div class="modal-body">
        	<div id="tablaCambiarClave">            
        		<!--Contenido se encuentra en CambiarClave.php-->           
           </div>
        </div>
      </div><!--FIN modal-content-->
    </div>          
</div><!--fin modal de CambiarClave-->
	
    			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio">SIGESA - Lista De Verificación Quirúrgica</a>
					</div>
					<!-- /.navbar-header -->
					
					<table border="0" style="float:left">
					<tr>
						<td width="6" height="89">&nbsp;</td>
						<td width="88" align="center" valign="middle" ><img src="../../MVC_Complemento/img/Usuarios/<?php echo trim($DNI)?>.jpg" width="88" height="87" /></td>
						<td width="6">&nbsp;</td>
						<td colspan="2" align="left" valign="middle">
							<font color="#000000"><strong>
								DNI: <?php echo trim($DNI);?><br />
								Nombre: <?php echo  $ApellidoPaterno." ".$ApellidoMaterno.", ".$Nombres;?><br>
								PC: <?php echo $loginPC;?>
							</strong></font>
						</td>
						<td width="39" align="left" valign="middle">&nbsp;</td>
						<td width="427" align="left" valign="middle"><font color="#000000"><strong>IP: <font color="#000000" size="+1">              
						  <?php echo  $ip = $_SERVER['REMOTE_ADDR'];
						  //$_SERVER['SERVER_NAME'];?></font><BR>
						  SERVIDOR: <font color="#FF0000" size="+1"><?php echo $hostname = gethostbyaddr($ip); ?></font><BR>
						  <?php //echo $_SERVER['HTTP_USER_AGENT'];?> 
						  </strong></font>
					  </td>
					</tr>
					</table>
					
					<ul class="nav navbar-top-links navbar-right">
						
						<!-- /.dropdown -->
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
							</a>
                            <!--dropdown-user -->
							<ul class="dropdown-menu dropdown-user">
								<li><a onClick="CambiarClave()"><i class="fa fa-user fa-fw"></i> Modificar Clave</a></li>								
								<li class="divider"></li>
                                <?php /*?><?php if(trim($IdPermiso)=='1'){ ?>
                                <li><a onClick="CambiarClaveResponsablesLV()"><i class="fa fa-user fa-fw"></i>Modificar Clave Responsables LV</a></li>								
								<li class="divider"></li>
                                <?php } ?><?php */?>
								<li><a href="../../MVC_Controlador/Sistema/SistemaC.php?acc=LoginLV"  target="_parent"><i class="fa fa-sign-out fa-fw"></i> Salir</a></li>
							</ul>
							<!-- /.dropdown-user -->
						</li>
						<!-- /.dropdown -->
						<li class="dropdown">
						   <a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
							</a>
                            <!--dropdown-alerts -->
							<ul class="dropdown-menu dropdown-alerts">
								<li>
									<a href="../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio&IdEmpleado=<?php echo $IdEmpleado;?>">
										<div>
											<i class="fa fa-fw"></i> Lista de Verificación Quirúrgica
											<!--<span class="pull-right text-muted small">4 minutes ago</span>-->
										</div>
									</a>
								</li>
								<li class="divider"></li> 
								
								<li>
									<a onClick="Nuevo('<?php echo $IdEmpleado ?>');">
										<div>
											<i class="fa fa-fw"></i> Nueva Verificación Quirúrgica
											<!--<span class="pull-right text-muted small">4 minutes ago</span>-->
										</div>
									</a>
								</li>
								<li class="divider"></li> 
								
								<li>
									<a href="../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=Reportes&IdEmpleado=<?php echo $IdEmpleado;?>">
										<div>
											<i class="fa fa-fw"></i> Reportes
											<!--<span class="pull-right text-muted small">4 minutes ago</span>-->
										</div>
									</a>
								</li>  
								<li class="divider"></li>                  
								
							</ul>
							<!-- /.dropdown-alerts -->
						</li>
                        
					</ul> 
				</nav>
                
	<script type="text/javascript">
	 	function CambiarClave(){	   
			$('#my_modalCambiarClave').modal('show');	
			$('#tablaCambiarClave').load("../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=CambiarClave&IdEmpleado=<?php echo $IdEmpleado;?>");	
		}
	
	 	function CambiarClaveResponsablesLV(){
			location.href="../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=CambiarClaveResponsablesLV&IdEmpleado=<?php echo $IdEmpleado ?>";
		}			
				 
	 </script>