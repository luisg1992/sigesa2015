<?php 
	/*session_start();
	error_reporting(E_ALL^E_NOTICE);*/
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - Lista De Verificación Quirúrgica</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
    
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>

    <script type="text/javascript">
	$(function(){
			<!--   Funcion para eliminar un Empleado de la Lista  -->
			$(".Eliminar_Empleado").click(function()
			{
				var id = $(this).attr("id");
				var IdEmpleado = id;
				
				var name = $(this).attr("name");
				var DNIEmpleado = name;
				var parent = $(this).parent("td").parent("tr");
				
				alertify.confirm("¿ Seguro de eliminar el Usuario "+DNIEmpleado+" ?", function (e) {
					if (e) {					
						$.post('../../MVC_Controlador/Lista_Verificacion/SeguridadLVC.php?acc=Eliminar_Empleado', {'IdEmpleadoEliminar':IdEmpleado}, function(data)
						{						
							parent.fadeOut('slow');
							alertify.success("<b>Usuario Eliminado </b> <h1></h1>"); 
						});	
						return true;
						
					} else {
						//alertify.alert("Successful AJAX after Cancel");
						alertify.error('Cancelado')
					}
				});	
					
			});
			
			<!--   Funcion para Resetear un Empleado de la Lista  -->
			$(".Resetear_Empleado").click(function()
			{
				var id = $(this).attr("id");
				var IdEmpleado = id;	
				
				var name = $(this).attr("name");
				var DNIEmpleado = name;
				var parent = $(this).parent("td").parent("tr");
							
					alertify.confirm("¿ Seguro de resetear Contraseña de Usuario "+DNIEmpleado+" ?", function (e) {
						if (e) {
							//alertify.success("Verificación Quirúrgica eliminada correctamente");
							//Redireccionamos si das a aceptar
							//window.location.href = "../Lista_Verificacion/SeguridadLVC.php?acc=Resetear_Empleado&IdEmpleadoResetear="+IdEmpleado;    //página web a la que te redirecciona si confirmas la eliminación
							$.post('../../MVC_Controlador/Lista_Verificacion/SeguridadLVC.php?acc=Resetear_Empleado', {'IdEmpleadoResetear':IdEmpleado}, function(data)
							{
								parent.css( "background-color:black" );
								alertify.success("<b>Contraseña Reseteada </b> <h1>123456</h1>"); 
							});	
							return true;
							
						} else {
							//alertify.alert("Successful AJAX after Cancel");
							alertify.error('Cancelado')
						}
					});	
			});
	
	
				/* Get Edit ID  */
			$(".Modificar_Empleado").click(function()
			{
				var IdEmpleado = $(this).attr("id");	
				
				$('#my_modalContenedor').modal('show');	
				$('#tablaContenedor').load("../../MVC_Controlador/Lista_Verificacion/SeguridadLVC.php?acc=EditarEmpleado&IdEmpleado="+IdEmpleado);	
				
			});		
		
		
			$("#Agregar_Empleado").click(function ()
			{
				 //$("#Lista_de_Empleados").hide();
				 //$("#contenedor").load('../../MVC_Vista/Seguridad/Agregar_Empleado.php');
				$('#my_modalContenedor').modal('show');	
				var IdEmpleado='<?php echo $IdEmpleado ?>';
				$('#tablaContenedor').load("../../MVC_Vista/ListaVerificacion/Seguridad_Agregar_Empleado.php?IdEmpleado="+IdEmpleado);
			});
		
	});
	</script>
    
    
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
<!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script><script type="text/javascript" src="../../MVC_Complemento/tables/site.js">
</script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async>
</script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<script type="text/javascript" class="init">
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            
       
            buttons: [ 'pageLength'				
				
				
            ]
        });
    });
</script>     


</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">
				<!-- Navigation -->
				<?php  include('../../MVC_Vista/ListaVerificacion/CabeceraLV.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">USUARIOS RESPONSABLES LV</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#" class="btn btn-primary btn-block" id="Agregar_Empleado" > <span class="glyphicon glyphicon-plus"></span> Agregar  </a>
                        </div>
                        <div  >
                        <br>
                        
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            
                                <table width="100%" class="table table-bordered table-hover" id="dataTables-example"> <!--table-hover-->
                                    <thead>
                                        <tr class="text-center">
                                           <th class="text-center">N&deg;</th>
                                           <th class="text-center">Nº DNI</th>
                                           <th class="text-center">Tipo Empleado</th>
                                           <th class="text-center">Apellido Paterno</th>
                                           <th class="text-center">Apellido Materno</th>
                                           <th class="text-center">Nombres</th>   
                                           <th class="text-center">Usuario</th> 	   
                                           <th >Editar</th>
                                           <th >Resetear</th>
                                           <th >Eliminar</th>
                                         </tr>   
      
                                    </thead>
                                    <tbody>
                                      <?php 									  
										include('../../MVC_Modelo/SeguridadLVM.php');
										if($_REQUEST["NroDocumento"]!=NULL){$NroDocumento=$_REQUEST["NroDocumento"];}else{$NroDocumento='%';}
										if($_REQUEST["Nombres"]!=NULL){$Nombres=$_REQUEST["Nombres"];}else{$Nombres='%';}
										if($_REQUEST["ApellidoPaterno"]!=NULL){$ApellidoPaterno=$_REQUEST["ApellidoPaterno"];}else{$ApellidoPaterno='%';}
										if($_REQUEST["ApellidoMaterno"]!=NULL){$ApellidoMaterno=$_REQUEST["ApellidoMaterno"];}else{$ApellidoMaterno='%';}
										
										
										$ListarEmpleados=Listar_Empleado_M($NroDocumento,$ApellidoPaterno,$ApellidoMaterno);
										if($ListarEmpleados != NULL){ 
										foreach($ListarEmpleados as $item){
										  
										?>
										 <tr align="center"> 
										   <td><a><?php echo  $item["Correlativo"];?></a></td>
										   <td><?php echo  strtoupper($item["DNI"]);?></td>
										   <td><?php echo  strtoupper($item["Descripcion"]);?></td>
										   <td><?php echo  strtoupper($item["ApellidoPaterno"]);?></td>
										   <td><?php echo  strtoupper($item["ApellidoMaterno"]);?></td>
										   <td><?php echo  strtoupper($item["Nombres"]);?></td>
										   <td style="color:red !important; font-weight:bold"><?php echo  strtoupper($item["Usuario"]);?></td>
										   <td><a href="#"  id="<?php echo $item['IdEmpleado']; ?>" class="Modificar_Empleado"><img src="../../MVC_Complemento/img/modiciar.jpg" width="16" height="16" /></a></td>
										   <td><a href="#"  id="<?php echo $item['IdEmpleado']; ?>" name="<?php echo $item['DNI']; ?>" class="Resetear_Empleado"><img src="../../MVC_Complemento/img/Refresh.png" width="16" height="16" /></a></td>	
										   <td><a href="#"  id="<?php echo $item['IdUsuario']; ?>" name="<?php echo $item['DNI']; ?>" class="Eliminar_Empleado"><img src="../../MVC_Complemento/img/close.png" width="16" height="16" /></a></td>	   
										   </tr>
										   <?php } }else{ ?>
										   <tr>
										   <td colspan="8" align="center" bgcolor="#FFFFFF" class="alert_error">NO SE ENCONTRÓ NINGÚN REGISTRO </td>
										   </tr>
									  <?php 
										}  
									  ?> 
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">                            
                            
                                <h4>COMO REALIZAR BÚSQUEDAS DIRECTAS </h4>
                                <p> Búsqueda de Personal Médico por DNI, Tipo Empleado, Apellido Paterno, Apellido Materno y Nombres </p><p>
<strong>Eje: DNI. 43623262, Eje: Apellido Paterno. Ariza</strong></p>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
  <!--Inicio de Contenedor  ---->   
<div class="modal fade" id="my_modalContenedor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">    	
        <div class="modal-header">      
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       		<!--<h4 class="modal-title" id="exampleModalLabel">Modificar Clave de Empleado</h4>-->
        </div>
      	<div class="modal-body">
        	<div id="tablaContenedor">            
        		<!--Contenido se encuentra en CambiarClave.php-->           
           </div>
        </div>
      </div><!--FIN modal-content-->
    </div>          
</div>
  <!--Fin de Contenedor  ---->  
  

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>-->

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>

</body>

</html>
