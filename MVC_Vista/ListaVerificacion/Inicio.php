<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - Lista De Verificación Quirúrgica</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script><script type="text/javascript" src="../../MVC_Complemento/tables/site.js">
</script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async>
</script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<script type="text/javascript" class="init">
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            dom: 'Bfrtip',
			 lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar Todo' ]
        ],
       
            buttons: [ 'pageLength',
				{
					text: 'Ocultar/Mostrar Acciones',
					action: function ( e, dt, node, config ) {
						dt.column( -1 ).visible( ! dt.column( -1 ).visible() );
					}
                }, //'colvis'
                <!--'copyHtml5','csvHtml5','excelHtml5',-->
				<!--'copy', 'csv', 'excel','print',-->	
				{ //1
					extend: 'copyHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //2
					extend: 'csvHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //3
					extend: 'excelHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //4
					extend: 'print',
					exportOptions: {columns: ':visible'}
				},
								
                {//valores por defecto orientation:'portrait' and pageSize:'A4',
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'A4',
					exportOptions: {columns: ':visible'}
                } //,				
				
				
            ]
        });
    });
</script> 
    
    <script type="text/javascript">
           
		   function Nuevo(IdEmpleado){  	
			 window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Nuevo&IdEmpleado="+IdEmpleado;
			//$(location).attr('href','../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Nuevo');
		   }
		   
		   function Validar(IdListVerAtenciones,IdEmpleado){
				 window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Validar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;
		   }
		   
		   function Modificar(IdListVerAtenciones,IdEmpleado){
				 window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Modificar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;
		   }
		   
		   function Imprimir(CodLista,IdEmpleado){           
			//window.location.href = "../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+CodLista;
		   		window.open("../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+CodLista+"&IdEmpleado="+IdEmpleado,'_blank');
			
		   }
		   function Eliminar(IdListVerAtenciones,IdEmpleado,CodigoListVerAtenciones){			     			
					alertify.confirm("¿Seguro de eliminar la Verificación Quirúrgica "+CodigoListVerAtenciones+"?", function (e) {
						if (e) {
							//alertify.success("Verificación Quirúrgica eliminada correctamente");
							//Redireccionamos si das a aceptar
					 		window.location.href = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Eliminar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;    //página web a la que te redirecciona si confirmas la eliminación
						} else {
							//alertify.alert("Successful AJAX after Cancel");
							alertify.error('Cancelado')
						}
					});			
		   }
		   
   </script> 

</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">
				<!-- Navigation -->
				<?php  include('../../MVC_Vista/ListaVerificacion/CabeceraLV.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">LISTA DE VERIFICACIÓN QUIRÚRGICA</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Búsqueda de Pacientes por Nro. Historia Clínica, DNI, Apellidos y Nombres 
                        </div>
                        <div  >
                        <br>
                        <a href="#" class="btn btn-primary btn-block" onClick="Nuevo('<?php echo $IdEmpleado ?>');" > <span class="glyphicon glyphicon-plus"></span> Nuevo  </a>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            
                                <table width="100%" class="table table-bordered table-hover" id="dataTables-example"> <!--table-hover-->
                                    <thead>
                                        <tr>
                                            <th width="3%">&nbsp;</th>
                                            <th width="6%">NºVerif.</th>
                                            <th width="8%">N°Hist.Clín.</th>
                                            <th width="5%">DNI</th>
                                            <th width="15%">Paciente</th>
                                            <th width="9%">Fec.Registro</th>
                                            <th width="7%">Tipo Sala</th>
                                            <th width="7%" align="center">Identificar</th>
                                            <th width="4%" align="center">Inicio</th>
                                            <th width="5%" align="center">Pausa</th>
                                            <th width="5%" align="center">Salida</th>
                                            <th width="7%" class="text-center">Validar</th>                                             
                                            <!--<th>Estado</th>-->
                                            <th width="19%" class="text-center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
									  		$i=0; 
									  		foreach($Listarpacientes as $Pacientes){
												$i++; 
									  			$estado=$Pacientes["Estado"];
												if($estado=='1'){
													$DesEstado='<font color="#00FF00">ACTIVO</font>';
													$colorestado="#FFF";
												}else if($estado=='0'){
													$DesEstado='<font color="#FF0000">INACTIVO</font>';
													$colorestado="#FFAAAA";
												}else if($estado=='2'){
													$DesEstado='<font color="#0000FF">CERRADO</font>';
													$colorestado="#d3f1f8";
												}
												$IdListVerAtenciones=$Pacientes["IdListVerAtenciones"];
									  ?>
                                      
                                      <?php 
									  if($estado!='0'){
									  	$ListVerModi=Sigesa_ListVer_ModificarM($IdListVerAtenciones);
										$FechaInicioEntrada=$ListVerModi[0]["FechaInicioEntrada"];
										$FechaInicioPausa=$ListVerModi[0]["FechaInicioPausa"];
										$FechaInicioSalida=$ListVerModi[0]["FechaInicioSalida"];
										
										if($FechaInicioSalida!=NULL){
											$validartab='3';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val2="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val3="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
										}else if($FechaInicioPausa!=NULL){
											$validartab='2';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val2="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val3="";
										}else if($FechaInicioEntrada!=NULL){
											$validartab='1';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val2="";
											$val3="";
										}else{
											$validartab='0';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="";
											$val2="";
											$val3="";
										}
									  }else{ //$estado=0
										  	$val0="";
											$val1="";
											$val2="";
											$val3="";										  
									  }
									 ?>	                                 						
 
                                        <tr  bgcolor="<?php echo $colorestado; ?>">
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $Pacientes["CodigoListVerAtenciones"];?></td>
                                            <td><?php echo $Pacientes["NroHistoriaClinica"];?></td>
                                            <td><?php echo $Pacientes["NroDocumento"];?></td>
                                            <td><?php echo $Pacientes["Paciente"];?></td>
                                            <td><?php echo vfecha(substr($Pacientes["FechaRegistro"],0,10));?></td>
                                            <td><?php echo $Pacientes["TipoSala"];?></td>
                                            <td> <?php echo $val0; ?> </td>
                                            <td> <?php echo $val1; ?> </td>
                                            <td> <?php echo $val2; ?> </td>
                                            <td> <?php echo $val3; ?> </td>
                                            <td class="text-center">
                                            	<?php  if($Pacientes["Estado"]=='1' && $validartab=='3'){?>
                                                <a href="#" class="btn btn-default btn-sm" onClick="Validar('<?php echo $Pacientes["IdListVerAtenciones"]; ?>','<?php echo $IdEmpleado; ?>');"><span class="glyphicon glyphicon-ok"></span> Validar</a>
                                                <?php  }else if($Pacientes["Estado"]=='0'){ echo "<font color='#FF0000'>ELIMINADO</font>"; ?> 
                                                
                                                <?php  }else if($Pacientes["Estado"]=='2'){ echo "<font color='#0000FF'>VALIDADO</font>";    ?> 
                                                
                                                <?php  }else{ echo "<strong>FALTA INFORMACIÓN</strong>";   }?>                                           
                                            </td>
                                            
                                            <?php /*?> <td class="center"><?php echo $DesEstado;?></td><?php */?>
                                            
                                            <td class="text-center">               
                                          
                                         	<?php  if($Pacientes["Estado"]=='1'){?>                                         			 
                                                   	 <a href="#" class="btn btn-info btn-sm" onClick="Modificar('<?php echo $Pacientes["IdListVerAtenciones"]; ?>','<?php echo $IdEmpleado; ?>');"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                                     <a href="#" class="btn btn-danger btn-sm" onClick="Eliminar('<?php echo $Pacientes["IdListVerAtenciones"]; ?>','<?php echo $IdEmpleado; ?>','<?php echo $Pacientes["CodigoListVerAtenciones"]; ?>')" ><span class="glyphicon glyphicon-remove" ></span> Eliminar</a>   
                                                                      
                                        	<?php  }else if ($Pacientes["Estado"]=='2'){?>
                                         			 <a href="#" class="btn btn-default btn-sm" onClick="Imprimir('<?php echo $Pacientes["IdListVerAtenciones"]; ?>','<?php echo $IdEmpleado; ?>');"><span class="glyphicon glyphicon-print" ></span> Imprimir</a>                                          
                                          <?php } ?>                                                            
                                         
                                         </td>
                                         <?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">                            
                            
                                <h4>COMO REALIZAR BÚSQUEDAS DIRECTAS </h4>
                                <p>Búsqueda de pacientes por Nro. Historia clínica, DNI, Apellidos y Nombres </p><p>
<strong>Eje: Nro. Historia. 162578,  Eje: DNI. 43623262, Eje: Apellidos y Nombres. Ariza Bravo</strong></p>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>-->

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>

</body>

</html>
