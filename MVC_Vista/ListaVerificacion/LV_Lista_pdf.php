<?php 
ini_set('error_reporting',0);//para xamp
ini_set('memory_limit', '1024M'); 
date_default_timezone_set('America/Bogota');
//require('../../MVC_Modelo/bscM.php');

require('../../MVC_Modelo/SistemaM.php');
//require('../../MVC_Modelo/EmergenciaM.php');
include('../../MVC_Modelo/Lista_VerificacionM.php');
require('../../MVC_Complemento/fpdf/fpdfLV.php');
require('../../MVC_Complemento/librerias/Funciones.php');

#OBTENIENDO VALORES


class PDF extends FPDF
{	
	//INICIO funciones para auto imprimir
	protected $javascript;
	protected $n_js;

	function IncludeJS($script, $isUTF8=false) {
		if(!$isUTF8)
			$script=utf8_encode($script);
		$this->javascript=$script;
	}

	function _putjavascript() {
		$this->_newobj();
		$this->n_js=$this->n;
		$this->_put('<<');
		$this->_put('/Names [(EmbeddedJS) '.($this->n+1).' 0 R]');
		$this->_put('>>');
		$this->_put('endobj');
		$this->_newobj();
		$this->_put('<<');
		$this->_put('/S /JavaScript');
		$this->_put('/JS '.$this->_textstring($this->javascript));
		$this->_put('>>');
		$this->_put('endobj');
	}

	function _putresources() {
		parent::_putresources();
		if (!empty($this->javascript)) {
			$this->_putjavascript();
		}
	}

	function _putcatalog() {
		parent::_putcatalog();
		if (!empty($this->javascript)) {
			$this->_put('/Names <</JavaScript '.($this->n_js).' 0 R>>');
		}
	}
	
	function AutoPrint($printer='')
	{
		// Open the print dialog
		if($printer)
		{
			$printer = str_replace('\\', '\\\\', $printer);
			$script = "var pp = getPrintParams();";
			$script .= "pp.interactive = pp.constants.interactionLevel.full;";
			$script .= "pp.printerName = '$printer'";
			$script .= "print(pp);";
		}
		else
			$script = 'print(true);';
		$this->IncludeJS($script);
	}
	
	//FIN funciones para auto imprimir
	
		
	function Header(){	
		//list($mes,$dia,$anio) = explode("/",$_REQUEST["fechaini"]);
		//$fechainicio = $anio."/".$mes."/".$dia;

		//list($mes,$dia,$anio) = explode("/",$_REQUEST["fechafin"]);
		//$fechafin = $anio."/".$mes."/".$dia;
		#OBTENIENDO DATOS
		$respuesta = SIGESA_ListVer_ImpresionM($_REQUEST['codigo']);

		$this->SetFont('Helvetica','',9);
		$this->Cell(60);
		$this->Image('../../MVC_Complemento/img/grcallo.jpg',10,5,10,10);
		$this->Cell(40);
		$this->setfont('Helvetica','b',11);
		$this->Cell(70,2,utf8_decode('LISTA DE VERIFICACIÓN DE SEGURIDAD DE LA CIRUGÍA       N° ').$respuesta[0]['CODIGO'],0,0,'C');
		$this->Image('../../MVC_Complemento/img/hndac.jpg',280,5,10,10);
		$this->Ln(5);
		
		#SERVICIO
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(90,5,'Especialidad: ' . utf8_decode($respuesta[0]['SERVICIO']),0,0,'L'); 		
		#TIPO DE SALA
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(80,5,'Tipo Sala: ' . $respuesta[0]['TIPOSALA'] . ' - ' . $respuesta[0]['SALA'],0,0,'L');		
		#FECHA Y HORA
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(85,5,'Fecha y Hora: ' . vfecha(substr($respuesta[0]['FechaRegistro'],0,10)) . '  ' . $respuesta[0]['horario'],0,0,'L');
				
		$this->Ln(5);
		
		#PACIENTE
		$this->SetFont('Helvetica','B',9);		
		$this->Cell(90,5,'Paciente: ' .utf8_decode($respuesta[0]['PACIENTE']),0,0,'L'); 		
		#HISTORIA CLINICA
		$this->SetFont('Helvetica','B',8);		
		$this->Cell(50,5,utf8_decode('Historia clínica: '). $respuesta[0]['NUMEROHISTORIA'],0,0,'L'); 						
		#DNI		
		$this->SetFont('Helvetica','B',8);		
		//$this->Cell(30,5,'DNI: ' . $respuesta[0]['NroDocumento'],0,0,'L');
		$this->Cell(30);
		#SEXO
		$this->SetFont('Helvetica','B',8);
		if($respuesta[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($respuesta[0]["IdTipoSexo"]==2){$Sexo='Femenino'; }
		$this->Cell(45,5,'Sexo: ' . $Sexo ,0,0,'L');
		#EDAD		
		$this->SetFont('Helvetica','B',8);		
		$FechaNacx = time() - strtotime($respuesta[0]["FechaNacimiento"]);$edad = floor((($FechaNacx / 3600) / 24) / 360);
		$this->Cell(25,5,'Edad: ' . $edad ,0,0,'L');
		
		$this->Ln(2);
	}

	function Footer()
	{
		$ip = $_SERVER['REMOTE_ADDR'];$hostname = gethostbyaddr($ip);
		$usuario=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);			
		$this->SetFont('Helvetica','',6);
		
		$this->Cell(60,5,'TERMINAL: ' .$hostname ,0,0,'L');	$this->Ln(3);	
		$this->Cell(60,5,'USUARIO: ' .utf8_decode(strtoupper($usuario[0]['Usuario'])),0,0,'L');	$this->Ln(3);	
		$this->Cell(60,5,utf8_decode('FECHA Y HORA DE IMPRESIÓN: ') .date('d/m/Y H:i:s'),0,0,'L');$this->Ln(3);			
		$this->Cell(60,5,"HNDAC / OESY / DESARROLLO DE SISTEMAS",0,0,'L'); $this->Ln(5);	
		
		$ListVerResponsable1=Sigesa_ListVerResponsablesM($_REQUEST['codigo'],'1');	//CIRUJANO
		$ListVerResponsable2=Sigesa_ListVerResponsablesM($_REQUEST['codigo'],'2');	//ANESTESIOLGO
		$ListVerResponsable3=Sigesa_ListVerResponsablesM($_REQUEST['codigo'],'3');	//INSTRUMENTISTA	
		$ListVerResponsable7=Sigesa_ListVerResponsablesM($_REQUEST['codigo'],'7');	//ENFERMERA CIRCULANTE		
		
		
		$this->Cell(50,5,'Firma y Sello:',0,0,'C'); $this->Cell(5); //DEJAR UN ESPACIO A LA IZQUIERDA		
				
		if($ListVerResponsable1[0]['Validacion']=='1'){ //cirujano 
			$this->Cell(45,-2,"Validado DNI :".$ListVerResponsable1[0]['DNI'],0,0,'C'); $this->Cell(10);
			//$this->Cell(55);			
		}else{
			$this->Cell(45,-2,"",0,0,'C'); $this->Cell(10);
			//$this->Cell(55);			
		}		
		
		if($ListVerResponsable2[0]['Validacion']=='1'){ //anestesiologo	
			$this->Cell(50,-2,"Validado DNI :".$ListVerResponsable2[0]['DNI'],0,0,'C');	$this->Cell(10);
			//$this->Cell(110);//55 +45+10	
			
		}else{
			$this->Cell(50,-2,"",0,0,'C'); $this->Cell(10);
			//$this->Cell(110);			
		}	
		
		if($ListVerResponsable7[0]['Validacion']=='1'){ //enfermera circulante	
			$this->Cell(45,-2,"Validado DNI :".$ListVerResponsable7[0]['DNI'],0,0,'C'); $this->Cell(10);
			//$this->Cell(170);//55+45+10 +50+10
			
		}else{
			$this->Cell(45,-2,"",0,0,'C'); $this->Cell(10);
			//$this->Cell(170);
		}	
			
		if($ListVerResponsable3[0]['Validacion']=='1'){ //enfermera instrumentista	
			$this->Cell(45,-2,"Validado DNI :".$ListVerResponsable3[0]['DNI'],0,0,'C');	
			//$this->Cell(225);//55+45+10+50+10 +45+10			
		}else{
			$this->Cell(45,-2,"",0,0,'C');	
			//$this->Cell(225);//55+45+10+50+10 +45+10	
		}
			
		$this->Ln(1);	$this->Cell(55); 
		$this->Cell(45,0,"",0,0,'LR','C');	$this->Cell(10);	
		$this->Cell(50,0,"",0,0,'LR','C');  $this->Cell(10);		
		$this->Cell(45,0,"",0,0,'LR','C');	$this->Cell(10);		
		$this->Cell(45,0,"",0,0,'LR','C');					
		
		$this->Ln(0.1);
		$this->Cell(50);			 
			#CIRUJANO
			$this->SetFont('Helvetica','',7);						
			$this->Cell(55,5,ltrim(utf8_decode(mb_strtoupper($ListVerResponsable1[0]['Responsable']))),0,0,'C');		
			
			#ANESTESIOLGO			
			$this->SetFont('Helvetica','',7);
			if($ListVerResponsable2[0]['Responsable']!=NULL){
				$ANESTESIOLOGO=$ListVerResponsable2[0]['Responsable'];
			}else{$ANESTESIOLOGO='';}			
			$this->Cell(60,5,ltrim(utf8_decode(mb_strtoupper($ANESTESIOLOGO))),0,0,'C');
			
			#ENFERMERO CIRCULANTE 			
			$this->SetFont('Helvetica','',7);
			if($ListVerResponsable7[0]['Responsable']!=NULL){
				$INSTRU=$ListVerResponsable7[0]['Responsable'];
			}else{$INSTRU='';}
			$this->Cell(55,5,ltrim(utf8_decode(mb_strtoupper($INSTRU))),0,0,'C');
			
			#ENFERMERO INSTRUMENTISTA			
			$this->SetFont('Helvetica','',7);
			if($ListVerResponsable3[0]['Responsable']!=NULL){
				$EINSTRU=$ListVerResponsable3[0]['Responsable'];
			}else{$EINSTRU='';}
			$this->Cell(55,5,ltrim(utf8_decode(mb_strtoupper($EINSTRU))),0,0,'C');$this->Ln(2);			
			
			$this->Cell(50); //DEJAR UN ESPACIO A LA IZQUIERDA
			$this->SetFont('Helvetica','b',6);
			$this->Cell(55,5,'MEDICO CIRUJANO',0,0,'C'); 		
			$this->SetFont('Helvetica','b',6);
			$this->Cell(60,5,'MEDICO ANESTESIOLOGO',0,0,'C'); 
			$this->SetFont('Helvetica','b',6);
			$this->Cell(55,5,'ENFERMERA CIRCULANTE',0,0,'C'); 
			$this->SetFont('Helvetica','b',6);
			$this->Cell(55,5,'INSTRUMENTISTA',0,0,'C');	
			 				
	}

}

	$pdf = new PDF("L","mm","A4");
	$pdf->SetTitle('Lista Verificacion');
	$pdf->SetAuthor('Rodolfo Crisanto Rosas');
	$pdf->AliasNbPages();
	$pdf->AddPage();

	#GENERANDO DATOS
	$respuesta = SIGESA_ListVer_ImpresionM($_REQUEST['codigo']);
	
	#OBTENIENDO LAS ALTERNATIVAS MARCADAS
	
		/*$preguntas_contestadas = explode("-", $respuesta[0]['COMBINACIONES']);	
		$preguntas_contestadas = array_filter($preguntas_contestadas);
		
		if( !empty($preguntas_contestadas) ){
			for ($l=0; $l < count($preguntas_contestadas); $l++) { 
				$respuestas[] = explode(",", $preguntas_contestadas[$l]);
			}
		}
		
		if( !empty($respuestas) ){
			for ($o=0; $o < count($respuestas); $o++) { 
				$alternativas_contestadas[] = $respuestas[$o][2];
			}
		}*/
		
		$respuestas = SIGESA_ObtenerAlternativasSeleccionadasM($_REQUEST['codigo']);
			if( !empty($respuestas) ){
				for ($o=0; $o < count($respuestas); $o++) { 
					$alternativas_contestadas[] = $respuestas[$o]['IdAlternativas'];
				}
			}else{
				$alternativas_contestadas[] ='';
			}
	#LLENANDO EL DOCUMENTO	
	
		#OBTENIENDO MODULOS
		$modulos = SIGESA_ListVerModulosM();

		$pdf->SetFont('Helvetica','B',8);
		$pdf->Ln(5);
		for ($i=0; $i < count($modulos); $i++) { 
			$pdf->Cell(91,5,$modulos[$i]['Descripcion'],1,0,'C'); 		
		}
		
		$pdf->Ln(5);
		$pdf->SetFont('Helvetica','',7);
		$pdf->Cell(91,5,utf8_decode('(Con el enfermero y el anestesiólogo, como mínimo)'),'LRB',0,'C');
		$pdf->Cell(91,5,utf8_decode('(Con el enfermero, el anestesiólogo y el cirujano)'),'LRB',0,'C');
		$pdf->Cell(91,5,utf8_decode('(Con el enfermero, el anestesiólogo y el cirujano)'),'LRB',0,'C');
		$pdf->Ln(5);

		#OBTENIENDO PREGUNTAS
		
		$preguntas = Sigesa_ListarPreguntasM();
		$pdf->SetFont('Helvetica','',8);
		
		#MODULO ENTRADA (PREGUNTAS DEL 1 AL 7)
		#=====================================
		
		#PREGUNTA 1:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[1]['Nombre']))),'LR');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(1);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			if( !empty($alternativas_contestadas) ){
				for ($j=0; $j < count($alternativas_contestadas); $j++) { 
					if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
						$tip = '[ X ]';
					}
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR');
		}

		#PREGUNTA 2:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[2]['Nombre']))),'LRT');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(2);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR');
		}

		#PREGUNTA 3:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[3]['Nombre']))),'LRT');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(3);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR');
		}

		#PREGUNTA 4:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[4]['Nombre']))),'LRT');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(4);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR');
		}

		#PREGUNTA 5:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[5]['Nombre']))),'LRT');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(5);

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR');
		}
		//$pdf->MultiCell(91,5,"\n",'LR');

		#PREGUNTA 6:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[6]['Nombre']))),'LRT');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(6);
		//alternativas No y Si
		for ($i=0; $i < 2; $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}				
			}
			$pdf->SetFont('Helvetica','',8);
			//$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR');
			$cadenita6 .= $tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."     ";
		}
			$pdf->MultiCell(91,5,$cadenita6,'LR');	
			
		//subalternativas
		for ($i=2; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}				
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,'                '.$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR','L');
		}
			

		#PREGUNTA 7:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[7]['Nombre']))),'LRT');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(7);
		//alternativas No y Si
		for ($i=0; $i < 2; $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetFont('Helvetica','',8);
			//$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR');
			  $cadenita7 .= $tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."     ";
		}
			$pdf->MultiCell(91,5,$cadenita7,'LR');
				
		//subalternativas
		for ($i=2; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}				
			}
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,'                '.$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'LR','L');
		}
		//$pdf->MultiCell(91,5,' ','LR');
		//$pdf->MultiCell(91,5,' ','LR');
		//$pdf->MultiCell(91,5,' ','LR');
		$pdf->MultiCell(91,15,' ','LRB');//PRIMERA COLUMNA
		
		#MODULO DE PAUSA (PREGUNTAS DEL 8 AL 15)
		#=======================================
		
		#SETEANDO PUNTERO
		$pdf->SetXY(101,36);
		
		#PREGUNTA 8:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[8]['Nombre']))),'R');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(8);
		$y = 45;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 9:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[9]['Nombre']))),'TR');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(9);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 10:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('¿'.ucfirst(mb_strtolower($preguntas[10]['Nombre']))),'TR');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(10);
		$y = $y + 10;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'R');
			$y = $y + 5;
		}
		
		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 11:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('Previsión de eventos críticos: ')."\n".utf8_decode(ucfirst(mb_strtolower($preguntas[11]['Nombre']))),'TR');		
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(11);
		$y = $y + 10;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 12:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[12]['Nombre']))),'R');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(12);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'R');
			$y = $y + 5;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 13:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[13]['Nombre']))),'R');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(13);
		$y = $y + 5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'R');
			$y = $y + 5;
		}
		
		#SETEANDO PUNTERO
		$y = $y + 5;
		$pdf->SetXY(101,$y);

		#PREGUNTA 14:
		
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode('¿'.ucfirst(mb_strtolower($preguntas[14]['Nombre']))),'TR');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(14);
		$y = $y + 5;		
		$cadenitas = " ";
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			//$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre']))),'R');
			$cadenitas .= $tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."     ";
			//$y = $y + 5;
		}
			$pdf->MultiCell(91,5,$cadenitas,'RB');//SEGUNDA COLUMNA 
		
		$y = $y + 5;
		#SETEANDO PUNTERO
		$pdf->SetXY(101,$y);

		#PREGUNTA 15:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[15]['Nombre']))),'TR');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(15);
		$y = $y + 5;
		$cadenita = " ";
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(101,$y);
			$pdf->SetFont('Helvetica','',8);
			$cadenita .= $tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."     ";
			#$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre'])))."\n",'R');
			
		}
			$pdf->MultiCell(91,15,$cadenita,'R');//SEGUNDA COLUMNA 
			
			$y = $y + 7; //ESPACIOS
			$pdf->SetXY(101,$y);			
			$pdf->MultiCell(91,25,' ','BR');	
			

		#MODULO DE SALIDA (PREGUNTAS 16 17)
		#==================================
		
		#SETEANDO PUNTERO
		$pdf->SetXY(192,37);

		#PREGUNTA 16:
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[16]['Nombre']))),'TR');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(16);
		$y = 36+5;
		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[   ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre']))),'R');
			$pdf->SetXY(192,$y+5);
			$pdf->Cell(91,5,'','R',1);
			$y = $y + 10;
		}

		#SETEANDO PUNTERO
		$pdf->SetXY(192,$y);

		#PREGUNTA 17:
		
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,utf8_decode(ucfirst(mb_strtolower($preguntas[17]['Nombre']))),'TR');
		$alternativas = SIGESA_ListVer_ListarAlternativas_M(17);
		$y = $y + 5;
		

		for ($i=0; $i < count($alternativas); $i++) { 
			$tip = '[  ]';
			for ($j=0; $j < count($alternativas_contestadas); $j++) { 
				if ($alternativas[$i]['IdAlternativas'] == $alternativas_contestadas[$j]) {
					$tip = '[ X ]';
				}
			}
			$pdf->SetXY(192,$y);
			$pdf->SetFont('Helvetica','',8);
			$pdf->MultiCell(91,5,$tip.' '.utf8_decode(ucfirst(mb_strtolower($alternativas[$i]['Nombre']))),'R');
			$y = $y + 5;
		}
		
		#OBSERVACIONES
		#=============
		
		#SETEANDO PUNTERO
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','B',8);
		$pdf->MultiCell(91,5,'Observaciones','TR');
		
		$y = $y + 5;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',8);
		$pdf->MultiCell(91,5,str_replace("-*-", ", ", utf8_decode(ucfirst(mb_strtolower(ltrim($respuesta[0]['OBSERVACIONES']))))),'R');
		
		$y = $y + 46;
		$pdf->SetXY(192,$y);
		$pdf->Cell(91,35,'','R','L');						
		
		
		//$pdf->MultiCell(91,10,'Firma y Sello','TR','L');	
		
		/*$ip = $_SERVER['REMOTE_ADDR'];$hostname = gethostbyaddr($ip);
		$usuario=ListarUsuarioxIdempleado_M($_GET['IdEmpleado']);
		
		$y = $y + 10;
		$pdf->SetXY(192,$y);						
		$pdf->SetFont('Helvetica','',6);
		$pdf->MultiCell(91,10,ltrim(utf8_decode(mb_strtoupper('TERMINAL: ' .$hostname))),'R','L');
		$y = $y + 3;
		$pdf->SetXY(192,$y);
		$pdf->MultiCell(91,10,'USUARIO: ' .utf8_decode(strtoupper($usuario[0]['Usuario'])),'R','L');
		$y = $y + 3;
		$pdf->SetXY(192,$y);
		$pdf->MultiCell(91,10,utf8_decode('FECHA Y HORA DE IMPRESIÓN: ') .date('d/m/Y H:i:s'),'R','L');
		$y = $y + 3;
		$pdf->SetXY(192,$y);
		$pdf->MultiCell(91,10,"HNDAC / OESY / DESARROLLO DE SISTEMAS",'R','L');	*/
					
		$y = $y +30;
		$pdf->SetXY(192,$y);
		$pdf->SetFont('Helvetica','',8);
		$pdf->MultiCell(91,10,' ','BR');
				
	$pdf->AutoPrint();//para auto imprimir
	$pdf->Output();
?>

