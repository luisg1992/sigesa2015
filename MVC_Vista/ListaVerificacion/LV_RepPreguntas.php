
<?php	
		$todosIdLV=$_REQUEST['todosIdLV'];	       
        $resultado1=SIGESA_ListVer_ContarAtenciones_M($todosIdLV);
		
		$IdEstaoPreg=$_REQUEST['IdEstaoPreg'];
		if($IdEstaoPreg=='1'){
			$parametro='FechaInicioEntrada';
			$texto='Inicio';
		}else if($IdEstaoPreg=='2'){
			$parametro='FechaInicioPausa';
			$texto='Pausa';
		}else if($IdEstaoPreg=='3'){
			$parametro='FechaInicioSalida';
			$texto='Salida';
		}
		
		
		$resultado2=SIGESA_ListVer_ContarAtencionesParametro_M($parametro,$todosIdLV);//$parametro=FechaInicioEntrada,FechaInicioPausa,FechaInicioSalida
        
 ?>	
<table id="datatable" class="table table-hover" style="font-size:12px;">           
    <thead>  
    	      	 
       <tr>
          <td colspan="11">
            <input id="buscar" name="buscar" value="" type="text" class="form-control" placeholder="Ingrese Un Valor de Busqueda" />
          </td>
        </tr>
        <tr>
          <td>Total Reg.LV &nbsp;</td>          
          <td><?php echo $resultado1[0]['cantidad']; ?></td>
          <td>Cantidad Reg. <?php echo $texto; ?> <?php echo $resultado2[0]['cantidad']; ?></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
            <th>Pregunta</th>
            <th colspan="2">Nombre</th>
            <th>Tipo Pregunta</th>
            <th>Numero Alternativas</th>
            <th>&nbsp;</th> 
        </tr>        
    
    </thead>
    <tbody>
    
        <?php		
        $IdEstaoPreg=$_REQUEST['IdEstaoPreg'];
        $resultadoP=SIGESA_ListVer_ListarPreguntas_M($IdEstaoPreg);
        	foreach ($resultadoP as $resultado) {
				$resultadoA=SIGESA_ListVer_ContarAlternativasPregunta_M($resultado['IdPreguntas']);
				$cantAlternativas=$resultadoA[0]['cantidad'];
				$Tipo=$resultado['Tipo'];
				if($Tipo=='U'){
					$TipoP='UNICA';
				}else if($Tipo=='V'){
					$TipoP='TODOS';
				}
        ?>	
    
    
            <tr bgcolor="#CCFFFF">
                <td><?php echo $resultado['IdPreguntas']; ?></td>
                <td colspan="2"><?php echo trim($resultado['Nombre']); ?></td>
                <td><?php echo $TipoP; ?></td>
                <td><?php echo $cantAlternativas ?></td>
                <td>&nbsp;</td>
            </tr> 
            <?php		
       		if($Tipo=='U'){
				$alternativas = SIGESA_ListVer_ListarAlternativas_M($resultado['IdPreguntas']);
				for ($j=0; $j < count($alternativas); $j++) { 
				$IdAlternativas=$alternativas[$j]['IdAlternativas'];
				
				$respTotAltContest=SIGESA_ListVer_ContarAlternativasContestadas_M($IdAlternativas,$todosIdLV);
				$CantTotAltContest=$respTotAltContest[0]['cantidad'];
				
				if(($resultado['IdPreguntas']=='6' && ($IdAlternativas=='45' || $IdAlternativas=='47')) || ($resultado['IdPreguntas']=='7' && ($IdAlternativas=='46' || $IdAlternativas=='48'))){ //45 47 y 46 48
					$textoidalter="";
				}else{
					$textoidalter='Alternativa '.($j+1);
				}				
        ?>	
            <tr>
                <td><?php echo $textoidalter; ?></td>
                <td><?php echo trim(ucfirst(mb_strtolower($alternativas[$j]['Nombre']))); ?></td>
                <td>Cantidad</td>
                <td><?php echo $CantTotAltContest; ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>          
         <?php }} ?>   
            
        <?php } ?>    	
    
    </tbody>
 </table>


<script type="text/javascript">


        document.querySelector("#buscar").onkeyup = function () {
            $TableFilter("#datatable", this.value);
        }

        $TableFilter = function (id, value) {
            var rows = document.querySelectorAll(id + ' tbody tr');

            for (var i = 0; i < rows.length; i++) {
                var showRow = false;

                var row = rows[i];
                row.style.display = 'none';

                for (var x = 0; x < row.childElementCount; x++) {
                    if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                        showRow = true;
                        break;
                    }
                }

                if (showRow) {
                    row.style.display = null;
                }
            }
        }
    
</script>
