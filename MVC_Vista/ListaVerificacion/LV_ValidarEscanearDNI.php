<!DOCTYPE html>
 
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>SIGESA - Lista de Verificacion </title>

        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/ListaVerificacion.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
        <script type="text/javascript">				
			function inhabilitar(){
				alert ("Esta función está inhabilitada.\n\n Disculpen las molestias.")
				return false
			}			
			document.oncontextmenu=inhabilitar;				
			/*document.oncontextmenu = function(){return false}*/
		</script> 
        
        <script type="text/javascript">
			/*function stopRKey(evt) {
			   var evt = (evt) ? evt : ((event) ? event : null);
			   var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
			   if ((evt.keyCode == 13) && (node.type=="password")) {return false;}
			}
			document.onkeypress = stopRKey;*/
		</script>
        
		<style>
          p {
            background: yellow;
          }		  
		  /*#ocultar{
			visibility:hidden;	
			display:none;		
		  }*/
        </style>
        
        <script type="text/javascript">	
		
			function ocultar(){
				//$('#ocultar6').hide();//muestro mediante clase   show
				//$('#ocultar7').hide();//muestro mediante clase   show
									
					///////
					if(document.getElementById('IdCirujano').value==""){
						$('#Validar1').hide();
					}else{
						$('#Validar1').show();
					}
					
					if(document.getElementById('IdAnestesiologo').value==""){
						$('#Validar2').hide();
					}else{
						$('#Validar2').show();
					}
					
					if(document.getElementById('IdInstrumentista').value==""){
						$('#Validar3').hide();
					}else{
						$('#Validar3').show();
					}
					
					if(document.getElementById('IdAsistente1').value==""){
						$('#Validar4').hide();
					}else{
						$('#Validar4').show();
					}
					
					if(document.getElementById('IdAsistente2').value==""){
						$('#Validar5').hide();
					}else{
						$('#Validar5').show();
					}
					
					if(document.getElementById('IdAsistente3').value==""){
						$('#Validar6').hide();
					}else{
						$('#Validar6').show();
					}
					
					if(document.getElementById('IdEnfermeraCirculante1').value==""){
						$('#Validar7').hide();
					}else{
						$('#Validar7').show();
					}
			}			
			
			function Regresar(){				
				window.location.href = "../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";
			}			
				 
		   //LIMPIAR 
		   function ValidarCirujano(){			   
			   var CirujanoDNI = document.getElementById("CirujanoDNI").innerHTML;			   
			   //var md5CirujanoDNI = document.getElementById("md5CirujanoDNI").value;
			   var Cirujano = document.getElementById("Cirujano").value;
			   
			   if(CirujanoDNI.trim()==Cirujano){	  			   
				   document.getElementById('ValidaCirujano').value='VALIDADO';
				   document.getElementById('Cirujano').readOnly=true;
				   
				   document.getElementById('Anestesiologo').focus();											
				   $('#Validar1').hide();
				   
			   }else{
				   alertify.error("<b> Mensaje del Sistema: </b> <h1>El codigo del Cirujano NO COINCIDE</h1>");
				   document.getElementById("Cirujano").value="";
				   document.getElementById("Cirujano").focus();
			   }
			}
			
			function ValidarAnestesiologo(){
			   var AnestesiologoDNI = document.getElementById("AnestesiologoDNI").innerHTML;			   		   
			   //var md5AnestesiologoDNI = document.getElementById("md5AnestesiologoDNI").value;
			   var Anestesiologo = document.getElementById("Anestesiologo").value;
			   
			   //alert(AnestesiologoDNI.trim()+'separa'+Anestesiologo);
			   if(AnestesiologoDNI.trim()==Anestesiologo){
			   
				   document.getElementById('ValidaAnestesiologo').value='VALIDADO';
				   document.getElementById('Anestesiologo').readOnly=true;
				   
				   if(document.getElementById('InstrumentistaDNI').value!=""){
						document.getElementById('Instrumentista').focus();
				   }else if(document.getElementById('Asistente1DNI').value!=""){
						document.getElementById('Asistente1').focus();
				   }else if(document.getElementById('Asistente2DNI').value!=""){
						document.getElementById('Asistente2').focus();
				   }else if(document.getElementById('Asistente3DNI').value!=""){
						document.getElementById('Asistente3').focus();
				   }else if(document.getElementById('EnfermeraCirculante1DNI').value!=""){
						document.getElementById('EnfermeraCirculante1').focus();
				   }												
				   $('#Validar2').hide();
				   	
				}else{
				   alertify.error("<b> Mensaje del Sistema: </b> <h1>El codigo del Anestesiologo NO COINCIDE</h1>");
				   document.getElementById("Anestesiologo").value="";
				   document.getElementById("Anestesiologo").focus();
			    }
			}
			
			function ValidarInstrumentista(){
			   var InstrumentistaDNI = document.getElementById("InstrumentistaDNI").innerHTML;
			   //var md5InstrumentistaDNI = document.getElementById("md5InstrumentistaDNI").value;
			   var Instrumentista = document.getElementById("Instrumentista").value;
			   
			   if(InstrumentistaDNI.trim()==Instrumentista){
				   
				   document.getElementById('ValidaInstrumentista').value='VALIDADO';
				   document.getElementById('Instrumentista').readOnly=true;
				   
				   if(document.getElementById('IdAsistente1').value!=""){
						document.getElementById('Asistente1').focus();
				   }else if(document.getElementById('IdAsistente2').value!=""){
						document.getElementById('Asistente2').focus();
				   }else if(document.getElementById('IdAsistente3').value!=""){
						document.getElementById('Asistente3').focus();
				   }else if(document.getElementById('IdEnfermeraCirculante1').value!=""){
						document.getElementById('EnfermeraCirculante1').focus();
				   }											
				   $('#Validar3').hide();
						
			   }else{
				   alertify.error("<b> Mensaje del Sistema: </b> <h1>El codigo del Instrumentista NO COINCIDE</h1>");
				   document.getElementById("Instrumentista").value="";
				   document.getElementById("Instrumentista").focus();
			   }
			}
			
			function ValidarAsistente1(){
			   var Asistente1DNI = document.getElementById("Asistente1DNI").innerHTML;
			   //var md5Asistente1DNI = document.getElementById("md5Asistente1DNI").value;
			   var Asistente1 = document.getElementById("Asistente1").value;
			   
			   if(Asistente1DNI.trim()==Asistente1){
				   
				   document.getElementById('ValidaAsistente1').value='VALIDADO';
				   document.getElementById('Asistente1').readOnly=true;
				   
				   if(document.getElementById('IdAsistente2').value!=""){
						document.getElementById('Asistente2').focus();
				   }else if(document.getElementById('IdAsistente3').value!=""){
						document.getElementById('Asistente3').focus();
				   }else if(document.getElementById('IdEnfermeraCirculante1').value!=""){
						document.getElementById('EnfermeraCirculante1').focus();
				   }											
				   $('#Validar4').hide();	
			   
			   }else{
				   alertify.error("<b> Mensaje del Sistema: </b> <h1>El codigo del Asistente1 NO COINCIDE</h1>");
				   document.getElementById("Asistente1").value="";
				   document.getElementById("Asistente1").focus();
			   }
			}
			
			function ValidarAsistente2(){
			   var Asistente2DNI = document.getElementById("Asistente2DNI").innerHTML;
			   //var md5Asistente2DNI = document.getElementById("md5Asistente2DNI").value;
			   var Asistente2 = document.getElementById("Asistente2").value;
			   
			   if(Asistente2DNI.trim()==Asistente2){
				   
				   document.getElementById('ValidaAsistente2').value='VALIDADO';
				   document.getElementById('Asistente2').readOnly=true;
				   
				   if(document.getElementById('IdAsistente3').value!=""){
						document.getElementById('Asistente3').focus();
				   }else if(document.getElementById('IdEnfermeraCirculante1').value!=""){
						document.getElementById('EnfermeraCirculante1').focus();
				   }										
				   $('#Validar5').hide();
			   
			   }else{
				   alertify.error("<b> Mensaje del Sistema: </b> <h1>El codigo del Asistente2 NO COINCIDE</h1>");
				   document.getElementById("Asistente2").value="";
				   document.getElementById("Asistente2").focus();
			   }	
			}
			
			function ValidarAsistente3(){
			   var Asistente3DNI = document.getElementById("Asistente3DNI").innerHTML;
			   //var md5Asistente3DNI = document.getElementById("md5Asistente3DNI").value;
			   var Asistente3 = document.getElementById("Asistente3").value;
			   
			   if(Asistente3DNI.trim()==Asistente3){
				   
				   document.getElementById('ValidaAsistente3').value='VALIDADO';
				   document.getElementById('Asistente3').readOnly=true;			   
				  
				   document.getElementById('EnfermeraCirculante1').focus();							
				   $('#Validar6').hide();
			   
			   }else{
				   alertify.error("<b> Mensaje del Sistema: </b> <h1>El codigo del Asistente3 NO COINCIDE</h1>");
				   document.getElementById("Asistente3").value="";
				   document.getElementById("Asistente3").focus();
			   }	
			}
			
			function ValidarEnfermeraCirculante1(){
			   var EnfermeraCirculante1DNI = document.getElementById("EnfermeraCirculante1DNI").innerHTML;
			   //var md5EnfermeraCirculante1DNI = document.getElementById("md5EnfermeraCirculante1DNI").value;
			   var EnfermeraCirculante1 = document.getElementById("EnfermeraCirculante1").value;
			   
			   if(EnfermeraCirculante1DNI.trim()==EnfermeraCirculante1){
				   
				   document.getElementById('ValidaEnfermeraCirculante1').value='VALIDADO';
				   document.getElementById('EnfermeraCirculante1').readOnly=true;												
				   $('#Validar7').hide();
				   
			   }else{
				   alertify.error("<b> Mensaje del Sistema: </b> <h1>El codigo de la Enfermera Circulante NO COINCIDE</h1>");
				   document.getElementById("EnfermeraCirculante1").value="";
				   document.getElementById("EnfermeraCirculante1").focus();
			   }	
			}	
	 
		</script>
    </head>
    
    
<body onLoad="ocultar();">    
 
    
<div class="container-fluid">
   

   <div class="col-md-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading"  >
                       VALIDACIÓN DE RESPONSABLES
                </div>
                <div class="panel-body">
                  <div class="tab-content">
                    <!--Inicio Identificacion-->
                        <div class="tab-pane fade in active" id="tab1primary">
                        <!--Contenido-->
                        
                        
                           <div class="container-fluid">
                             
                              
                              <div class="panel panel-default" id="DatosPaciente" style="display: block;">
  								<div class="panel-body">
                              <div class="row">
                                 <div class="col-md-12">
                                 		<div class="row">
                                         <div class="col-md-2"><strong>DNI:</strong></div>
                                         <div class="col-md-2" id="DNI"><?php echo $ListVerModi[0]["NroDocumento"]; ?></div>
                                         <div class="col-md-2"><strong>Nro.His.Cli:</strong></div>
                                         <div class="col-md-2" id="nrohiscli"><?php echo $ListVerModi[0]["NroHistoriaClinica"]; ?></div>
                                         <div class="col-md-2"><strong>Fecha Atencion:</strong></div>
                                         <div class="col-md-2"><?php echo vfecha(substr($ListVerModi[0]["FechaRegistro"],0,10)) ?></div>
                                      </div>                                
                                 </div>  								  
                              </div>
                              
                              <div class="row">
                                 <div class="col-md-12">
                                 		<div class="row">
                                         <div class="col-md-2"><strong>Paciente:</strong></div>
                                         <div class="col-md-2" id="Paciente"><?php echo utf8_decode($ListVerModi[0]["PACIENTE"]); ?></div>
                                         <div class="col-md-2"><strong>Fecha Nacimiento:</strong></div>
                                         <div class="col-md-2" id="FechaNac"><?php echo vfecha(substr($ListVerModi[0]["FechaNacimiento"],0,10)) ?></div>
                                         <?php 
										 	$FechaNacx = time() - strtotime($ListVerModi[0]["FechaNacimiento"]);
											$edad = floor((($FechaNacx / 3600) / 24) / 360);
											if($ListVerModi[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($ListVerModi[0]["IdTipoSexo"]==2){$Sexo='Femenino';}				
										 ?>
                                         <div class="col-md-2"><strong>Edad:</strong></div>
                                         <div class="col-md-2" id="Edad"><?php echo $edad ?></div>
                                      </div>                                
                                 </div>  								  
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                 	 <div class="row">
                                         <div class="col-md-2"><strong>Sexo:</strong></div>
                                         <div class="col-md-2" id="Sexo"><?php echo $Sexo ?></div>
                                         <div class="col-md-2"><strong>Domicilio:</strong></div>
                                         <div class="col-md-2" id="Domicilio"><?php echo $ListVerModi[0]["departamentodomicilio"].' - '.$ListVerModi[0]["provinciadomicilio"].' - '.$ListVerModi[0]["distritodomicilio"] ?></div>
                                         <div class="col-md-2"><strong>Lugar Nacim.</strong></div>
                                         <div class="col-md-2" id="LugarNacim"><?php echo $ListVerModi[0]["departamentonac"].' - '.$ListVerModi[0]["provincianac"].' - '.$ListVerModi[0]["distritonac"] ?></div>                                         
                                      </div>                                
                                 </div>  								  
                              </div>
                               </div>  								  
                              </div>
							  
							  <form id="FormLisVerValidar" name="FormLisVerValidar"  method="POST" autocomplete="off"> 
                              <div class="panel panel-default">
  								<div class="panel-body">                 
                              
                                <div class="row">
                                 <div class="col-md-12">                                 	  
                                     
                                       <input type="hidden" id="dnival" name="dnival" value="<?php echo $ListVerModi[0]["IdPaciente"] ?>" >                                       
                                       <input type="hidden" id="accion1" name="accion1" value="A" >
                                       <input type="hidden" id="IdListVerAtenciones" name="IdListVerAtenciones" value="<?php echo $IdListVerAtenciones ?>" >                                       
                                       <input type="hidden" id="IdEmpleado" name="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'] ?>" >
                                       
                                        <div id="TipoSala-group" class="form-group row" >                                        
                                          <label class="col-sm-2" for="TipoSala">Tipo Sala:</label>
                                          <div class="col-sm-4">                                           
                                              <?php echo $ListVerModi[0]["TIPOSALA"]; ?>
                                          </div>                                          
                                                                              
                                        </div>
                                       
                                        <div id="Sala-group" class="form-group row" >
                                          <label class="col-sm-2" for="Sala">Sala:</label>
                                          <div class="col-sm-4">
                                             <?php echo $ListVerModi[0]["SALA"]; ?>
                                          </div>                                          
                                        </div>
                                        
                                        <div id="Servicio-group" class="form-group row">
                                          <label class="col-sm-2"  for="Servicio">Especialidad:</label>
                                          <div class="col-sm-4">                                              
                                             <?php echo $ListVerModi[0]["SERVICIO"]; ?>                                                                                   
                                          </div>                                         
                                        </div>                                       
                                         
                                    </div>                                  
  								  
                              </div>
                               </div>  								  
                              </div>
                              <div class="panel panel-default">
  								<div class="panel-body">
                              <div class="row">                      
                              	  <div class="col-md-12">                                	 
                                      <div class="col-sm-1"></div>                                      
                                      <div class="col-sm-1"><strong>DNI</strong></div>
                                      <div class="col-sm-3"><strong>Profesional</strong></div>
                                      <div class="col-sm-2"><strong>Especialidad</strong></div>                                      
                                      <div class="col-sm-1"><strong>Colegiatura</strong></div>
                                      <div class="col-sm-1"><strong>RNE</strong></div> 
                                      <div class="col-sm-2"><strong>Código Validación</strong></div>                              
                                  </div>  								  
                              </div>
                              
                              	<?php 
									$ListVerResponsables1=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'1');
									$ListVerResponsables2=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'2');
									$ListVerResponsables3=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'3');
									$ListVerResponsables4=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'4');
									$ListVerResponsables5=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'5');
									$ListVerResponsables6=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'6');
									$ListVerResponsables7=Sigesa_ListVerResponsablesM($IdListVerAtenciones,'7');
								?>
                              
                               <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div id="IdCirujano-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Cirujano: <font color="#FF0000">(*)</font></label>                                
                                      <div class="col-sm-1" id="CirujanoDNI"><?php echo $ListVerResponsables1[0]["DNI"] ?></div>                                      
                                      <div class="col-sm-3" id="CirujanoMedico"><?php echo $ListVerResponsables1[0]["Responsable"] ?></div>
                                      <div class="col-sm-2" id="CirujanoTiposEmpleado"><?php echo $ListVerResponsables1[0]["Descripcion"] ?></div>  
                                      <div class="col-sm-1" id="CirujanoColegiatura"><?php echo $ListVerResponsables1[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Cirujanorne" ><?php echo $ListVerResponsables1[0]["rne"] ?></div>   
                                      
                                       <?php if($ListVerResponsables1[0]["Validacion"]=='0'){ ?>
                                       <div class="col-sm-2">                                      	
                                        <input type="password" class="form-control" id="Cirujano" name="Cirujano"  placeholder="Cirujano" onpaste="return false" /> 
                                        <input type="hidden" id="ValidaCirujano" name="ValidaCirujano" /> 
                                       </div>
                                       <!--<input type="button" value="Validar" onClick="ValidarCirujano()" class="btn btn-group-sm btn-primary" id="Validar1" />-->   
                                                                 
                                        <?php }else if($ListVerResponsables1[0]["Validacion"]=='1'){ ?>
                                        <div class="col-sm-2">
                                        <input type="text" class="form-control" value="VALIDADO" readonly>  
                                        </div>                                      
                                        <?php } ?>                                                                         
                                      
                                      <input type="hidden" id="IdCirujano" name="IdCirujano" value="<?php echo $ListVerResponsables1[0]["IdMedico"] ?>">                                      
                                      <input type="hidden" id="md5CirujanoDNI" name="md5CirujanoDNI" value="<?php echo md5($ListVerResponsables1[0]["DNI"]) ?>"> 
                                                                             
                                   </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div id="IdAnestesiologo-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Anestesiologo: <font color="#FF0000">(*)</font></label>                                      
                                      <div class="col-sm-1" id="AnestesiologoDNI"><?php echo $ListVerResponsables2[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="AnestesiologoMedico"><?php echo $ListVerResponsables2[0]["Responsable"] ?></div>
                                      <div class="col-sm-2" id="AnestesiologoTiposEmpleado"><?php echo $ListVerResponsables2[0]["Descripcion"] ?></div>                         			  
                                      <div class="col-sm-1" id="AnestesiologoColegiatura"><?php echo $ListVerResponsables2[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Anestesiologorne" ><?php echo $ListVerResponsables2[0]["rne"] ?></div>                                      
                                      
                                       <?php if($ListVerResponsables2[0]["Validacion"]=='0'){ ?>
                                       <div class="col-sm-2">                                      	
                                        <input type="password" class="form-control" id="Anestesiologo" name="Anestesiologo" placeholder="Anestesiologo" onpaste="return false" /> 
                                        <input type="hidden" id="ValidaAnestesiologo" name="ValidaAnestesiologo" />  
                                       </div>
                                       <!--<input type="button" value="Validar" onClick="ValidarAnestesiologo()" class="btn btn-group-sm btn-primary" id="Validar2" />-->    
                                                                 
                                        <?php }else if($ListVerResponsables2[0]["Validacion"]=='1'){ ?>
                                        <div class="col-sm-2">
                                        <input type="text" class="form-control" value="VALIDADO" readonly>  
                                        </div>                                      
                                        <?php } ?>                                         
                                      	 
                                      <input type="hidden" id="IdAnestesiologo" name="IdAnestesiologo" value="<?php echo $ListVerResponsables2[0]["IdMedico"] ?>">           
                                      <input type="hidden" id="md5AnestesiologoDNI" name="md5AnestesiologoDNI" value="<?php echo md5($ListVerResponsables2[0]["DNI"]) ?>">                                       
                                   </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Enf.Instrumentista:</label>                                      
                                      <div class="col-sm-1" id="InstrumentistaDNI"><?php echo $ListVerResponsables3[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="InstrumentistaMedico"><?php echo $ListVerResponsables3[0]["Responsable"] ?></div>
                                      <div class="col-sm-2" id="InstrumentistaTiposEmpleado"><?php echo $ListVerResponsables3[0]["Descripcion"] ?></div>      
                                      <div class="col-sm-1" id="InstrumentistaColegiatura"><?php echo $ListVerResponsables3[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Instrumentistarne" ><?php echo $ListVerResponsables3[0]["rne"] ?></div>  
                                      
                                       <?php if($ListVerResponsables3[0]["Validacion"]=='0'){ ?>
                                       <div class="col-sm-2">                                      	
                                        <input type="password" class="form-control" id="Instrumentista" name="Instrumentista" placeholder="Enf.Instrumentista" onpaste="return false" />  
                                        <input type="hidden" id="ValidaInstrumentista" name="ValidaInstrumentista" /> 
                                       </div>
                                       <!--<input type="button" value="Validar" onClick="ValidarInstrumentista()" class="btn btn-group-sm btn-primary" id="Validar3" />-->    
                                                                 
                                        <?php }else if($ListVerResponsables3[0]["Validacion"]=='1'){ ?>
                                        <div class="col-sm-2">
                                        <input type="text" class="form-control" value="VALIDADO" readonly>  
                                        </div>                                      
                                        <?php } ?>                                                                             
                                       
                                      <input type="hidden" id="IdInstrumentista" name="IdInstrumentista" value="<?php echo $ListVerResponsables3[0]["IdMedico"] ?>"> 
                                      <input type="hidden" id="md5InstrumentistaDNI" name="md5InstrumentistaDNI" value="<?php echo md5($ListVerResponsables3[0]["DNI"]) ?>">                             
                                                                            
                                   </div>
                                 </div>  								  
                              </div>
                              
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente1:</label>                                      
                                      <div class="col-sm-1" id="Asistente1DNI"><?php echo $ListVerResponsables4[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="Asistente1Medico"><?php echo $ListVerResponsables4[0]["Responsable"] ?></div>
                                      <div class="col-sm-2" id="Asistente1TiposEmpleado"><?php echo $ListVerResponsables4[0]["Descripcion"] ?></div>                                      
                                      <div class="col-sm-1" id="Asistente1Colegiatura"><?php echo $ListVerResponsables4[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Asistente1rne" ><?php echo $ListVerResponsables4[0]["rne"] ?></div>  
                                      
                                       <?php if($ListVerResponsables4[0]["Validacion"]=='0'){ ?>
                                       <div class="col-sm-2">                                      	
                                        <input type="password" class="form-control" id="Asistente1" name="Asistente1" placeholder="Asistente1" onpaste="return false" /> 
                                        <input type="hidden" id="ValidaAsistente1" name="ValidaAsistente1" /> 
                                       </div>
                                       <!--<input type="button" value="Validar" onClick="ValidarAsistente1()" class="btn btn-group-sm btn-primary" id="Validar4" /> -->  
                                                                 
                                        <?php }else if($ListVerResponsables4[0]["Validacion"]=='1'){ ?>
                                        <div class="col-sm-2">
                                        <input type="text" class="form-control" value="VALIDADO" readonly>  
                                        </div>                                      
                                        <?php } ?>                                                                             
                                       
                                      <input type="hidden" id="IdAsistente1" name="IdAsistente1" value="<?php echo $ListVerResponsables4[0]["IdMedico"] ?>">                                   
                                      <input type="hidden" id="md5Asistente1DNI" name="md5Asistente1DNI" value="<?php echo md5($ListVerResponsables4[0]["DNI"]) ?>">                                       
                                   </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente2:</label>                                      
                                      <div class="col-sm-1" id="Asistente2DNI"><?php echo $ListVerResponsables5[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="Asistente2Medico"><?php echo $ListVerResponsables5[0]["Responsable"] ?></div>
                                      <div class="col-sm-2" id="Asistente2TiposEmpleado"><?php echo $ListVerResponsables5[0]["Descripcion"] ?></div>                                      
                                      <div class="col-sm-1" id="Asistente2Colegiatura"><?php echo $ListVerResponsables5[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Asistente2rne" ><?php echo $ListVerResponsables5[0]["rne"] ?></div> 
                                      
                                       <?php if($ListVerResponsables5[0]["Validacion"]=='0'){ ?>
                                       <div class="col-sm-2">                                      	
                                        <input type="password" class="form-control" id="Asistente2" name="Asistente2" placeholder="Asistente2" onpaste="return false" /> 
                                        <input type="hidden" id="ValidaAsistente2" name="ValidaAsistente2" /> 
                                       </div>
                                       <!--<input type="button" value="Validar" onClick="ValidarAsistente2()" class="btn btn-group-sm btn-primary" id="Validar5" />-->     
                                                                 
                                        <?php }else if($ListVerResponsables5[0]["Validacion"]=='1'){ ?>
                                        <div class="col-sm-2">
                                        <input type="text" class="form-control" value="VALIDADO" readonly>  
                                        </div>                                      
                                        <?php } ?>                                                                             
                                       
                                      <input type="hidden" id="IdAsistente2" name="IdAsistente2" value="<?php echo $ListVerResponsables5[0]["IdMedico"] ?>">                                  
                                      <input type="hidden" id="md5Asistente2DNI" name="md5Asistente2DNI" value="<?php echo md5($ListVerResponsables5[0]["DNI"]) ?>">                                      
                                   </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Asistente3:</label>                                      
                                      <div class="col-sm-1" id="Asistente3DNI"><?php echo $ListVerResponsables6[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="Asistente3Medico"><?php echo $ListVerResponsables6[0]["Responsable"] ?></div>
                                      <div class="col-sm-2" id="Asistente3TiposEmpleado"><?php echo $ListVerResponsables6[0]["Descripcion"] ?></div>                                      
                                      <div class="col-sm-1" id="Asistente3Colegiatura"><?php echo $ListVerResponsables6[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="Asistente3rne" ><?php echo $ListVerResponsables6[0]["rne"] ?></div>  
                                      
                                       <?php if($ListVerResponsables6[0]["Validacion"]=='0'){ ?>
                                       <div class="col-sm-2">                                      	
                                        <input type="password" class="form-control" id="Asistente3" name="Asistente3" placeholder="Asistente3" onpaste="return false" />
                                        <input type="hidden" id="ValidaAsistente3" name="ValidaAsistente3" />   
                                       </div>
                                       <!--<input type="button" value="Validar" onClick="ValidarAsistente3()" class="btn btn-group-sm btn-primary" id="Validar6" />-->    
                                                                 
                                        <?php }else if($ListVerResponsables6[0]["Validacion"]=='1'){ ?>
                                        <div class="col-sm-2">
                                        <input type="text" class="form-control" value="VALIDADO" readonly>  
                                        </div>                                      
                                        <?php } ?>                                                                             
                                     
                                      <input type="hidden" id="IdAsistente3" name="IdAsistente3" value="<?php echo $ListVerResponsables6[0]["IdMedico"] ?>">
                                      <input type="hidden" id="md5Asistente3DNI" name="md5Asistente3DNI" value="<?php echo md5($ListVerResponsables6[0]["DNI"]) ?>">                                      
                                   </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">              
                              	<div class="col-md-12"> 
                                 	<div id="IdEnfermeraCirculante1-group" class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">Enf.Circulante: <font color="#FF0000">(*)</font></label>                                      
                                      <div class="col-sm-1" id="EnfermeraCirculante1DNI"><?php echo $ListVerResponsables7[0]["DNI"] ?></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante1Medico"><?php echo $ListVerResponsables7[0]["Responsable"] ?></div>
                                      <div class="col-sm-2" id="EnfermeraCirculante1TiposEmpleado"><?php echo $ListVerResponsables7[0]["Descripcion"] ?></div>   
									  <div class="col-sm-1" id="EnfermeraCirculante1Colegiatura"><?php echo $ListVerResponsables7[0]["Colegiatura"] ?></div>
                                      <div class="col-sm-1" id="EnfermeraCirculante1rne" ><?php echo $ListVerResponsables7[0]["rne"] ?></div> 
                                       
                                       <?php if($ListVerResponsables7[0]["Validacion"]=='0'){ ?>
                                       <div class="col-sm-2">                                      	
                                        <input type="password" class="form-control" id="EnfermeraCirculante1" name="EnfermeraCirculante1" placeholder="EnfermeraCirculante" onpaste="return false" />
                                        <input type="hidden" id="ValidaEnfermeraCirculante1" name="ValidaEnfermeraCirculante1" />
                                       </div>
                                       <!--<input type="button" value="Validar" onClick="ValidarEnfermeraCirculante1()" class="btn btn-group-sm btn-primary" id="Validar7" />-->   
                                                                 
                                        <?php }else if($ListVerResponsables7[0]["Validacion"]=='1'){ ?>
                                        <div class="col-sm-2">
                                        <input type="text" class="form-control" value="VALIDADO" readonly>  
                                        </div>                                      
                                        <?php } ?>
                                      
                                      <?php /*?><div class="col-sm-2">
                                      	<?php if($ListVerResponsables7[0]["Validacion"]=='0'){ ?>
                                        <input type="text" class="form-control" id="EnfermeraCirculante1" name="EnfermeraCirculante1" placeholder="EnfermeraCirculante">
                                        <input type="button" value="Validar" onClick="ValidarEnfermeraCirculante1()" class="btn btn-group-sm btn-primary" id="Validar7" />                            
                                        <?php }else if($ListVerResponsables1[0]["Validacion"]=='1'){ ?>
                                        <input type="text" class="form-control" value="VALIDADO" readonly>                                        
                                        <?php } ?>
                                      </div><?php */?>
                                                                            
                                      <input type="hidden" id="IdEnfermeraCirculante1" name="IdEnfermeraCirculante1" value="<?php echo $ListVerResponsables7[0]["IdMedico"] ?>">
                                      <input type="hidden" id="md5EnfermeraCirculante1DNI" name="md5EnfermeraCirculante1DNI" value="<?php echo md5($ListVerResponsables7[0]["DNI"]) ?>">  
                                                                      
                                  </div>
                                 </div>  								  
                              </div>
                              
                              <div class="row">              
                              	<font color="#FF0000"> (*): Campos Obligatorios</font>                               								  
                              </div>
                              
                              <!--<div class="row">                         
                              
                                 <div class="col-md-12"> 
                                 	<div class="form-group row">
                                      <label for="inputEmail3" class="col-sm-1 col-form-label">EnfermeraCirculante2:</label>
                                      <div class="col-sm-2">
                                        <input type="text" class="form-control" id="EnfermeraCirculante2" placeholder="EnfermeraCirculante2">
                                        <input type="text" id="IdEnfermeraCirculante2" name="IdEnfermeraCirculante2">
                                      </div>
                                      <div class="col-sm-1" id="EnfermeraCirculante2DNI"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante2Medico"></div>
                                      <div class="col-sm-3" id="EnfermeraCirculante2TiposEmpleado"></div>                                      
                                      <div class="col-sm-1" id="EnfermeraCirculante2Colegiatura"></div>
                                      <div class="col-sm-1" id="EnfermeraCirculante2rne" ></div>
                                      
                                      
                                    </div>
                                 </div>  								  
                              </div>-->
                              
                              
                              
                              </div>  								  
                              </div>
                              
                              
                              <div class="panel panel-default">
  								<div class="panel-body">
                                
                              <div class="row">
                                 <div class="col-md-9">                      
                                 
                                 	<input type="submit" value="Guardar Validaciones" class="btn btn-block btn-success" />
                                	<!--<input type="button" name="Enviar" value="Guardar Validaciones" class="btn btn-block btn-success" onclick="FormLisVerValidar.submit()">-->                                  
                                 </div>
  								 <div class="col-md-3 text-right"><input type="button" class="btn   btn-danger" value="Regresar" onClick="Regresar()" /></div>
                              </div>
                              
                              </div>  								  
                              </div>
                              
                              </form>
                            </div>
                        <!--Contenido-->
                      </div>
                   
                        
                        
                    </div>
                </div>
            </div>
        </div>
</div>
	
	
	    <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        <!--<script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/ListaVerificacionMod.js"></script> -->
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
        
        <script type="text/javascript">
			$.fn.delayPasteKeyUp = function(fn, ms) {
				 var timer = 0;
				 $(this).on("propertychange input", function(){
					 clearTimeout(timer);
					 timer = setTimeout(fn, ms);
				 });
			};
 
			//la utilizamos
			$(document).ready(function(){
				
				$("#Cirujano").delayPasteKeyUp(function(){
					//$("#Cirujano").append("Producto registrado: "+ $("#ingreso").val() +"<br>");
					$("#Cirujano").val("");
				}, 200);				
				
				$("#Anestesiologo").delayPasteKeyUp(function(){					
					$("#Anestesiologo").val("");
				}, 200);
				
				$("#Instrumentista").delayPasteKeyUp(function(){					
					$("#Instrumentista").val("");
				}, 200);
				
				$("#Asistente1").delayPasteKeyUp(function(){					
					$("#Asistente1").val("");
				}, 200);
				
				$("#Asistente2").delayPasteKeyUp(function(){					
					$("#Asistente2").val("");
				}, 200);
				
				$("#Asistente3").delayPasteKeyUp(function(){					
					$("#Asistente3").val("");
				}, 200);
				
				$("#EnfermeraCirculante1").delayPasteKeyUp(function(){					
					$("#EnfermeraCirculante1").val("");
				}, 200);
				
			});
		
		 
			$(document).ready(function() {	
			
				$('#Cirujano').on('keypress', function (e) {						
					if (e.which === 13) {         		
						ValidarCirujano();	
						return false;
					}					
				});
				
				$('#Anestesiologo').on('keypress', function (e) {							
					if (e.which === 13) {         		
						ValidarAnestesiologo();	
						return false;
					}
				});
				
				$('#Instrumentista').on('keypress', function (e) {							
					if (e.which === 13) {         		
						ValidarInstrumentista();	
						return false;
					}
				});
				
				$('#Asistente1').on('keypress', function (e) {							
					if (e.which === 13) {         		
						ValidarAsistente1();	
						return false;
					}
				});
				
				$('#Asistente2').on('keypress', function (e) {							
					if (e.which === 13) {         		
						ValidarAsistente2();	
						return false;
					}
				});
				
				$('#Asistente3').on('keypress', function (e) {							
					if (e.which === 13) {         		
						ValidarAsistente3();	
						return false;
					}
				});
				
				$('#EnfermeraCirculante1').on('keypress', function (e) {							
					if (e.which === 13) {         		
						ValidarEnfermeraCirculante1();	
						return false;
					}
				});

			 		
			//guardar GUARDAR VALIDACIONES
			$('#FormLisVerValidar').submit(function(event) {   				
			
				//RECUPERAR PARA GUARDAR
				//var IdPaciente = $("#IdPaciente").val();  
				var IdEmpleado = $("#IdEmpleado").val();  
				var IdCirujano = $("#IdCirujano").val(); 				
				var IdAnestesiologo = $("#IdAnestesiologo").val();  
				var IdInstrumentista = $("#IdInstrumentista").val();  
				var IdAsistente1 = $("#IdAsistente1").val();  
				var IdAsistente2 = $("#IdAsistente2").val();  
				var IdAsistente3 = $("#IdAsistente3").val();  
				var IdEnfermeraCirculante1 = $("#IdEnfermeraCirculante1").val(); 		
				
				var Cirujano = $("#ValidaCirujano").val(); 				
				var Anestesiologo = $("#ValidaAnestesiologo").val();  
				var Instrumentista = $("#ValidaInstrumentista").val();  
				var Asistente1 = $("#ValidaAsistente1").val();  
				var Asistente2 = $("#ValidaAsistente2").val();  
				var Asistente3 = $("#ValidaAsistente3").val();  
				var EnfermeraCirculante1 = $("#ValidaEnfermeraCirculante1").val(); 
				
				//var accion1 = $("#accion1").val();
				var IdListVerAtenciones = $("#IdListVerAtenciones").val();  
				 
				var formData = 'IdEmpleado='+ IdEmpleado + '&IdCirujano='+ IdCirujano + '&IdAnestesiologo='+ IdAnestesiologo + '&IdInstrumentista='+ IdInstrumentista + '&IdAsistente1='+ IdAsistente1 + '&IdAsistente2='+ IdAsistente2 + '&IdAsistente3='+ IdAsistente3 + '&IdEnfermeraCirculante1='+ IdEnfermeraCirculante1 + '&Cirujano='+ Cirujano + '&Anestesiologo='+ Anestesiologo + '&Instrumentista='+ Instrumentista + '&Asistente1='+ Asistente1 + '&Asistente2='+ Asistente2 + '&Asistente3='+ Asistente3 + '&EnfermeraCirculante1='+ EnfermeraCirculante1 +'&IdListVerAtenciones='+IdListVerAtenciones;
			   
				$.ajax({
					type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
					url         : '../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_GuardarValidar', // the url where we want to POST
					data        :  formData, // our data object
					dataType    : 'json', // what type of data do we expect back from the server
					encode      :  true,
					
				})
					// using the done promise callback
					.done(function(data) {                
					   console.log(data);			
											
					if (!data.success) { 					 
						/*if (data.errors.IdCirujano) {  
					   		alertify.error("<b> Mensaje del Sistema: </b> <h1>"+data.errors.IdCirujano+"</h1>");											   
			      		}	*/			
					}else{
						alertify.success("<b>Validaciones Guardadas </b> <h1>VALIDAR TODOS LOS RESPONSABLES PARA CERRAR</h1>"); 					
						return false;
					}
							
				});
				// stop the form from submitting the normal way and refreshing the page
				event.preventDefault();		
			});
		
		});	
		//fin GUARDAR VALIDACIONES
		</script>
     
    </body>
</html>
