<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - Lista De Verificación Quirúrgica</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Fecha --> 
    <link rel="stylesheet" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css" />     
    
<!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../MVC_Complemento/tables/ga.js" async type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/tables/site.js"></script>
<!--<script type="text/javascript" src="../../MVC_Complemento/tables/dynamic.php" async></script>-->
<!--<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../MVC_Complemento/tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<script type="text/javascript" class="init">
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            dom: 'Bfrtip',
			 lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar Todo' ]
        ],
       
            buttons: [ 'pageLength',
				'colvis',
                <!--'copyHtml5','csvHtml5','excelHtml5',-->
				<!--'copy', 'csv', 'excel','print',-->	
				{ //1
					extend: 'copyHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //2
					extend: 'csvHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //3
					extend: 'excelHtml5',
					exportOptions: {columns: ':visible'}
				},
				
				{ //4
					extend: 'print',
					exportOptions: {columns: ':visible'}
				},
								
                {//valores por defecto orientation:'portrait' and pageSize:'A4',
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'A4',
					exportOptions: {columns: ':visible'}
                } //,				
				
				
            ]
        });
    });
</script> 
    
    <script type="text/javascript">
	
		   /*function verPreg(){
			alert('rfg');   
		   }*/
	
		   function buscar(){
			   var FechaReg_Inicio=document.getElementById('FechaReg_Inicio').value;
			   var FechaReg_Final=document.getElementById('FechaReg_Final').value;
			   if((FechaReg_Inicio=="" && FechaReg_Final!="") || (FechaReg_Inicio!="" && FechaReg_Final=="")){
				   alertify.error('Debe ingresar ambas fechas o Ninguna. Si selecciona ninguna se filtraran todas');
			   }else{
				   $('#form1').submit();	
			   }
			}
			
			function cancelar(){
				window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=Reportes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>";
			}
           
		   function Nuevo(IdEmpleado){  	
			 window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Nuevo&IdEmpleado="+IdEmpleado;
			//$(location).attr('href','../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Nuevo');
		   }
		   
		   function Modificar(IdListVerAtenciones,IdEmpleado){
				 window.location = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Modificar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;
		   }
		   
		   function Imprimir(CodLista,IdEmpleado){           
			//window.location.href = "../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+CodLista;
		   		window.open("../../MVC_Vista/ListaVerificacion/LV_Lista_pdf.php?codigo="+CodLista+"&IdEmpleado="+IdEmpleado,'_blank');
			
		   }
		   function Eliminar(IdListVerAtenciones,IdEmpleado,CodigoListVerAtenciones){
			     /*eliminar=confirm("¿Seguro de eliminar la Verificación Quirúrgica "+CodigoListVerAtenciones+"?");
				   if (eliminar)
				   //Redireccionamos si das a aceptar
					 window.location.href = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Eliminar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;    //página web a la que te redirecciona si confirmas la eliminación
				//else
				  //Y aquí pon cualquier cosa que quieras que salga si le diste al boton de cancelar
					//alert('No se ha eliminado la Verificación Quirúrgica...')	*/			
					alertify.confirm("¿Seguro de eliminar la Verificación Quirúrgica "+CodigoListVerAtenciones+"?", function (e) {
						if (e) {
							//alertify.success("Verificación Quirúrgica eliminada correctamente");
							//Redireccionamos si das a aceptar
					 		window.location.href = "../Lista_Verificacion/Lista_VerificacionC.php?acc=LV_Eliminar&IdListVerAtenciones="+IdListVerAtenciones+"&IdEmpleado="+IdEmpleado;    //página web a la que te redirecciona si confirmas la eliminación
						} else {
							//alertify.alert("Successful AJAX after Cancel");
							alertify.error('Cancelado')
						}
					});			
		   }
		   
   </script> 
   
   <script>
    $(function () {

//Array para dar formato en español
        $.datepicker.regional['es'] =
                {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        $.datepicker.setDefaults($.datepicker.regional['es']);

//miDate: fecha de comienzo D=días | M=mes | Y=año
//maxDate: fecha tope D=días | M=mes | Y=año
        //var f = new Date();
        //diaactual=f.getDate();
        // $( "#Ipd_frecibido").datepicker({ minDate: "-"+diaactual+"D", maxDate: "+1M +10D" });    
        //$( "#NT_FGUI" ).datepicker({ minDate: "-1M", maxDate: "+1M +10D" }); 
        $("#FechaReg_Inicio").datepicker();
        $("#FechaReg_Final").datepicker();

    });
	
	function verPreg(IdEstaoPreg,todosIdLV){	   
		$('#my_modalProd').modal('show');	
		$('#tablaProd').load("../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=VerPreguntas&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>",{IdEstaoPreg:IdEstaoPreg,todosIdLV:todosIdLV});	
	}	
	
</script>

</head>

<body>

<!--modal de ver Resumen de Preguntas y Respuestas-->
<div class="modal fade" id="my_modalProd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <form class="form-horizontal" id="frmproducto" name="frmproducto" >
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">      
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Resumen de Preguntas y Respuestas</h4>        
      </div>
      	<div class="modal-body">
        	<div id="tablaProd">
        		<!--Contenido se encuentra en LV_RepPreguntas.php-->
           </div>
        </div>
      </div><!--FIN modal-content-->
    </div>
    </form>
</div><!--fin modal de ver Resumen de Preguntas y Respuestas-->

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">
				<!-- Navigation -->
				<?php  include('../../MVC_Vista/ListaVerificacion/CabeceraLV.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">REPORTES DE VERIFICACIÓN QUIRÚRGICA</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary"> <!--panel-default-->
                        <div class="panel-heading"> Búsqueda de Pacientes por Nro. Historia Clínica, DNI, Apellidos y Nombres </div>
                        <div class="panel-body">
                        <form action="../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=Reportes&IdEmpleado=<?php echo $_REQUEST['IdEmpleado'];?>" name="form1" id="form1" method="post">                        
                        	 <div class="" align="right">
                                <!--<a class="btn btn-success" onClick="validarguardar();" href="#">Registrar</a>-->
                                <input class="btn btn-success" type="button" onclick="buscar()" value="Buscar"/>
                                &nbsp;<a class="btn btn-danger" onClick="cancelar();">Cancelar</a>
                                &nbsp;<a class="btn btn-warning" onClick="location.reload();" >Refrescar</a>&nbsp;
                                <!--&nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                              </div>
                                                 
                            <!--fila 1-->
                            <div class="form-group row">
                                <label class="control-label col-xs-1"></label>
                            </div>                            
                            <div class="form-group row">                                               
                                <label class="control-label col-xs-2">Fecha Reg.Inicio</label>
                                <div class="col-xs-2">
                                    <input type="text" id="FechaReg_Inicio" name="FechaReg_Inicio" class="form-control datepicker input-sm" placeholder="Fecha Registro Inicial" value="<?php echo $_REQUEST["FechaReg_Inicio"] ?>" />    
                                </div>
                                <label class="control-label col-xs-1">Fecha Reg. Fin</label>
                                <div class="col-xs-2">
                                    <input type="text" id="FechaReg_Final" name="FechaReg_Final" class="form-control datepicker input-sm" placeholder="Fecha Registro Final" value="<?php echo $_REQUEST["FechaReg_Final"] ?>" />    
                                </div>
                                <label class="control-label col-xs-1">Especialidad</label>
                                <div class="col-xs-2">                                              
                                 <select id="Servicio" name="Servicio" class="form-control input-sm" onChange="buscar();">
                                    <option value="">Todos</option> <!--Cirugía 4to. A-->
                                         <?php 
                                            $resultados=ListarServiciosVerificacionQuirurgicaM();								 
                                            if($resultados!=NULL){
                                            for ($i=0; $i < count($resultados); $i++) {	
                                         ?>
                                     <option value="<?php echo $resultados[$i]["IdServicio"] ?>" <?php if($_REQUEST["Servicio"]==$resultados[$i]["IdServicio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["IdServicio"].' | '.mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
                                     <?php 
                                        }}
                                     ?>                   
                                </select>                                                                                   
                              </div>
                        </div><!--fin fila 1-->
                        
                        <!--fila 2--> 
                        <div class="form-group row">
                            <label class="control-label col-xs-1">Tipo Sala</label>
                            	<div class="col-xs-1">                                           
                                  <label class="form-check-label">
                                    <input  type="radio" name="TipoSala" id="TipoSala" value="">
                                    Todos
                                  </label>
                                </div>
                                <div class="col-xs-1">                                           
                                  <label class="form-check-label">
                                    <input  type="radio" name="TipoSala" id="TipoSala" value="1" <?php if($_REQUEST["TipoSala"]==1){?> checked <?php } ?> >
                                    Electiva
                                  </label>
                                </div>                                            
                                <div class="col-xs-1">
                                  <label class="form-check-label">
                                    <input  type="radio" name="TipoSala" id="TipoSala" value="2" <?php if($_REQUEST["TipoSala"]==2){?> checked <?php } ?> >
                                    Emerg.
                                  </label>
                                </div>
                                <label class="control-label col-xs-1">Sala</label>
                                <div class="col-xs-2">
                                    <select id="Sala" name="Sala" class="form-control input-sm" onChange="buscar();">
                                        <option value="Select">Todos</option>
                                     </select>
                                </div>
                                <!--<label class="control-label col-xs-1">Estado</label>
                                <div class="col-xs-2">
                                    <select id="EstadoReg" name="EstadoReg" class="form-control input-sm">
                                    	<option value="">Todos</option>
                                        <option value="0">Identificar</option>
                                        <option value="1">Inicio</option>
                                        <option value="2">Pausa</option>
                                        <option value="3">Salida</option>
                                     </select>
                                </div>-->
                        </div><!--fin fila 2-->                    
                        </form>
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            
                               <?php  							
									$FechaReg_Inicio=$_REQUEST["FechaReg_Inicio"];
									if($FechaReg_Inicio!=NULL){
										$FechaReg_Inicio=gfecha($_REQUEST["FechaReg_Inicio"]);
									}
									$FechaReg_Final=$_REQUEST["FechaReg_Final"];
									if($FechaReg_Final!=NULL){
										$FechaReg_Final=gfecha($_REQUEST["FechaReg_Final"]);
									}	
									
									$Servicio=$_REQUEST["Servicio"];
									$TipoSala=$_REQUEST["TipoSala"];
									$Sala=$_REQUEST["Sala"];
									if($Sala==""){
										$Sala="Select";
									}else{
										$Sala=$_REQUEST["Sala"];
									}
									
									//$EstadoReg=$_REQUEST["EstadoReg"];	 
								?>
                            
                                <table width="100%" class="table table-bordered" id="dataTables-example"> <!--table-hover-->
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>N°Verif.</th>
                                            <th>N°Hist.Clín.</th>
                                            <th>DNI</th>
                                            <th>Paciente</th>
                                            <th>Fec.Registro</th>
                                            <th>Tipo Sala</th>
                                            <th align="center">Sala</th>
                                            <th align="center">Especialidad</th>
                                            <th align="center">Identificar</th>
                                            <th align="center">Inicio </th>
                                            <th align="center">Pausa </th>
                                            <th align="center">Salida </th>                                             
                                            <!--<th>Estado</th>-->     
                                         </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
									  		$i=0;								
											$numactivos=0; $numentrada=0;$numpausa=0;$numsalida=0; $todosId="";
											//$Listarpacientes=SIGESA_ListVer_PacientesAtendidos_M();																					
											$Listarpacientes=SIGESA_ListVer_FiltroPacientesAtendidos_M($FechaReg_Inicio,$FechaReg_Final,$Servicio,$TipoSala,$Sala);											
											
											if($Listarpacientes!=NULL){
									  		foreach($Listarpacientes as $Pacientes){
												$i++; 
									  			$estado=$Pacientes["Estado"];
												if($estado=='1'){
													$DesEstado='<font color="#00FF00">ACTIVO</font>';
													$colorestado="#FFF";
												}else if($estado=='0'){
													$DesEstado='<font color="#FF0000">INACTIVO</font>';
													$colorestado="#FFAAAA";
												}else if($estado=='2'){
													$DesEstado='<font color="#0000FF">CERRADO</font>';
													$colorestado="#d3f1f8";
												}
												$IdListVerAtenciones=$Pacientes["IdListVerAtenciones"];
												
									  ?>
                                      
                                      <?php 
									  if($estado!='0'){ $numactivos++;
									  	
									  	$todosId=$todosId.$IdListVerAtenciones.',';
										$todosIdLV=$todosId.'0';
																			  
									  	$ListVerModi=Sigesa_ListVer_ModificarM($IdListVerAtenciones);
										$FechaInicioEntrada=$ListVerModi[0]["FechaInicioEntrada"];
										$FechaInicioPausa=$ListVerModi[0]["FechaInicioPausa"];
										$FechaInicioSalida=$ListVerModi[0]["FechaInicioSalida"];
										
										 if($FechaInicioEntrada!=NULL){ $numentrada++;}
										 if($FechaInicioPausa!=NULL){ $numpausa++;}
										 if($FechaInicioSalida!=NULL){ $numsalida++;} 
										
										if($FechaInicioSalida!=NULL){ 
											$validartab='3';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val2="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val3="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
										}else if($FechaInicioPausa!=NULL){ 
											$validartab='2';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val2="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val3="";
										}else if($FechaInicioEntrada!=NULL){ 
											$validartab='1';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val2="";
											$val3="";
										}else{
											$validartab='0';
											$val0="<img src='../../MVC_Complemento/easyui/themes/icons/ok.png'/> <font color='#FFFFFF'>X</font>";
											$val1="";
											$val2="";
											$val3="";
										}
									  }else{
										  	$val0="";
											$val1="";
											$val2="";
											$val3="";										  
									  }
									 ?>	                                 						
 
                                        <tr  bgcolor="<?php echo $colorestado; ?>">
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $Pacientes["CodigoListVerAtenciones"];?></td>
                                            <td><?php echo $Pacientes["NroHistoriaClinica"];?></td>
                                            <td><?php echo $Pacientes["NroDocumento"];?></td>
                                            <td><?php echo $Pacientes["Paciente"];?></td>
                                            <td><?php echo vfecha(substr($Pacientes["FechaRegistro"],0,10));?></td>
                                            <td><?php echo $Pacientes["TipoSala"];?></td>
                                            <td><?php echo $Pacientes["SALA"];?></td>
                                            <td><?php echo $Pacientes["Servicio"];?></td>
                                            <td><?php echo $val0; ?> </td>
                                            <td><?php echo $val1; ?> </td>
                                            <td><?php echo $val2; ?> </td>
                                            <td><?php echo $val3; ?> </td>
                                            <?php /*?> <td class="center"><?php echo $DesEstado;?></td><?php */?>                                          
                                        </tr> 
                                        <?php 
											}
										 } 
										?>                                       
                                    </tbody>
                                    <tr  bgcolor="<?php echo $colorestado; ?>">
                                      <td colspan="9" align="center">TOTALES ACTIVOS</td>
                                      <td><?php echo $numactivos; ?></td>
                                      <td><?php echo $numentrada; ?>
                                      <input class="btn btn-success" type="button" onclick="verPreg(1,'<?php echo $todosIdLV ?>')" value="Ver"/></td>
                                      <td><?php echo $numpausa; ?>
                                      <input class="btn btn-success" type="button" onclick="verPreg(2,'<?php echo $todosIdLV ?>')" value="Ver"/></td>
                                      <td><?php echo $numsalida; ?>
                                      <input class="btn btn-success" type="button" onclick="verPreg(3,'<?php echo $todosIdLV ?>')" value="Ver"/></td>
                                  </tr>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">
                                <h4>COMO REALIZAR BÚSQUEDAS DIRECTAS </h4>
                                <p>Búsqueda de pacientes por Nro. Historia clínica, DNI, Apellidos y Nombres </p><p>
<strong>Eje: Nro. Historia. 162578,  Eje: DNI. 43623262, Eje: Apellidos y Nombres. Ariza Bravo</strong>
</p>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/jquery/dist/jquery.min.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <!--<script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../../MVC_Complemento/LibBosstrap/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>-->
    
    <!-- Custom Theme JavaScript -->
    <script src="../../MVC_Complemento/LibBosstrap/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - alertify - Use for reference -->    
    <script src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
    
    <!-- Fecha -->     
    <script src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>   
    
<script type="text/javascript">
    $(document).ready(function() {
		//TipoSala/*
		
		//cuando refrescamos la pagina y hemos seleccionado un radio TipoSala
				//obtener el valor del radio TipoSala seleccionado	
				var resultado="ninguno";   
				var TipoSala=document.getElementsByName("TipoSala");
				for(var i=0;i<TipoSala.length;i++)
				{
					if(TipoSala[i].checked)
						resultado=TipoSala[i].value;
				} 	
				//buscamos las salas del TipoSala seleccionado	 	
				$.getJSON("../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarSalas&TipoSala="+resultado, function(json){
						$('#Sala').empty();
						//$('#Sala').append($('<option>').text("Todos"));
						$('#Sala').append($('<option>').text("Todos").attr('value', "Select"));
						$.each(json, function(i, obj){
								$('#Sala').append($('<option>').text(obj.text).attr('value', obj.val));
						});
				}); 
				
		//cuando cambiamos de radio TipoSala
		$('input:radio[name="TipoSala"]').change(function(){   
			var Sala=$(this).val();	 	
			 $.getJSON("../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=LV_BuscarSalas&TipoSala="+Sala, function(json){
					$('#Sala').empty();
					//$('#Sala').append($('<option>').text("Todos"));
					$('#Sala').append($('<option>').text("Todos").attr('value', "Select"));
					$.each(json, function(i, obj){
							$('#Sala').append($('<option>').text(obj.text).attr('value', obj.val));
					});
			});	
		});
	
	});
</script>	

</body>

</html>
