<html>
    <head>
		<link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
	
		<script type="text/javascript">
		$(function(){								
			$('#btn_cargar_datos').click(function(){
					$.post('../../MVC_Controlador/Lista_Verificacion/SeguridadLVC.php', 
						{ 
						 nro_documento: $('#nro_documento').val(),
						acc: 'Mostrar_Empleado'
						},
						function(res)
						{
							//alert(res);							
							if(res!=''){	
								var parsedRes = $.parseJSON(res); 								
								$( '#Apellido_Paterno' ).val( parsedRes.ApellidoPaterno);
								$( '#Apellido_Materno' ).val( parsedRes.ApellidoMaterno);
								$( '#Nombre' ).val( parsedRes.Nombres);	
								$( '#Empleado_Id' ).val( parsedRes.IdEmpleado);	
								//alert('HOLAAA111');
							}else{
								$( '#Apellido_Paterno' ).val('');
								$( '#Apellido_Materno' ).val('');
								$( '#Nombre' ).val('');	
								$( '#Empleado_Id' ).val('');	
								alert('No existe Usuario Responsable con el DNI ingresado');
							}
						}										
						);	
			});
			
		
			$("#btn_Agregar_Empleado").click(function(){
				

							var Clave=$("#Clave").val();
							var Id_Empleado=$("#Empleado_Id").val();
														
							if ($("#Clave").val() == "") {  
								$("#Clave").focus().after('<span class="error">Ingrese Contraseña</span>');  
								return false;  
							}
								
		
							var parametros =
							{
							"Clave" : Clave,
							"Id_Empleado" : Id_Empleado,
							"acc" : 'Agregar_Empleado'
							};
			
							$.ajax({
											data:  parametros,
											url:   '../../MVC_Controlador/Lista_Verificacion/SeguridadLVC.php',
											type:  'post',
											success:  function (response) {
													$("#resultado").html(response);
													var IdEmpleado='<?php echo $_GET['IdEmpleado'] ?>';
													location.href="../../MVC_Controlador/Lista_Verificacion/Lista_VerificacionC.php?acc=CambiarClaveResponsablesLV&IdEmpleado="+IdEmpleado;
											}
							
							
							}); 
							$('#my_modalContenedor').modal('toggle');
							//$('#dataTables-example tr:last').after('<tr><td>John</td><td>455</td></tr>');
							
			});
			
			
			$("#btn_limpiar").click(function(){
				$('#Clave').val("");
			});
			
			$("#Cancelar").click(function(){
				$('#my_modalContenedor').modal('toggle');
			});

			
		});
		</script>
		</head>
		<body>
		<div>
		<!--------Aqui apareceran los Alert de Jquery------------>
		<div id="resultado">
		</div>
		<!-------------------->
		<h3>Agregar Usuario</h3>	
			 	
			<table>
			<tr>
				<td>DNI</td>
				<td>
				<input type='text' name='nro_documento' id='nro_documento'  required>
				</td>
				<td>&nbsp;
				 
				</td>
				<td>
				<input type="button" class="button" value="Cargar Datos"   name="btn_cargar_datos" id="btn_cargar_datos" />
				</td>
				<td>&nbsp;
				 
				</td>
				<td>
				<input type="button" class="button" value="Limpiar"   name="btn_limpiar" id="btn_limpiar" />
				</td>
			</tr>
			<tr>
				<td>Apellido Paterno</td>
				<td colspan="2">
				<input type='text' name='Apellido_Paterno' id='Apellido_Paterno'   readonly='readonly'    required>
				</td>
			</tr>
			<tr>
				<td>Apellido Materno</td>
				<td colspan="2" ><input type='text' name='Apellido_Materno' id='Apellido_Materno'  readonly='readonly'  required></td>
			</tr>
			<tr>
				<td>Nombres</td>
				<td><input type='text' name='Nombres' id='Nombre'  readonly='readonly' autocomplete="off"  required></td>
			</tr>
			<tr>
				<td>Clave</td>
				<td><input type='password' name='Clave' id='Clave' autocomplete="off"  required></td>
			</tr>
			<tr style="display:none">
				<td>Id Empleado</td>
				<td><input type='text' name='Empleado_Id' id='Empleado_Id'  readonly='readonly'  required></td>	
			</tr>
            <tr>
			  <td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
				<input type="button" class="buttonS" value="Guardar"   name="btn_Agregar_Empleado" id="btn_Agregar_Empleado" />	
                <input type="button" class="buttonS" value="Cancelar"  id="Cancelar" />			
				</td>
			</tr>
		</table>
		</div>
    </body>
</html>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	