<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Farmacia Registro Venta</title>

<!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
			.caja {

font-family: sans-serif;

font-size: 18px;

margin:0 0 5px;
overflow:hidden;
padding:5px;
background-color:#FF0;
border:1px solid #FF0000;
-webkit-border-radius: 8px;
border-radius: 8px;

}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.edatagrid.js"></script>
                  

<script type="text/javascript">
//$(document).ready(function(){

	
								
function Cargar(){

	//	$("#IdMed").textbox('textbox').bind('keydown', function(e){
		//	if (e.keyCode == 13){
				//alert('hola');
				var IdMedico=$('#nombre').combogrid('getValue');
//alert(IdMedico);

				//$('#TablaMedicos').datagrid('loadData', {"IdMedico":0,"rows":[]});
				
				$('#TablaMedicos').datagrid({url:'../../MVC_Controlador/Programacion/ProgramacionC.php?acc=BusquedaMedicosEspecialidad&IdMedico='+IdMedico});
		}
		//	}

	//	});

//});	
	</script>
<script>
		$(function(){
			var lastIndex;
			$('#TablaMedicos').datagrid({
				onClickRow:function(rowIndex){
					if (lastIndex != rowIndex){
						$(this).datagrid('endEdit', lastIndex);
						$(this).datagrid('beginEdit', rowIndex);
					}
					lastIndex = rowIndex;
				},
				onBeginEdit:function(rowIndex){
				}
			});
		});
		
function accept(){
$('#TablaMedicos').datagrid('acceptChanges');
return true;
}


function GrabarDatos() {
	accept();
	 var dataResultado = $("#TablaMedicos").datagrid('getData'); //DATA ITEMS DE FACTURACIÒN   
	 var nrow = $('#TablaMedicos').datagrid('getRows').length; 
	 
	                 $.ajax({
                    url: '../../MVC_Controlador/Programacion/ProgramacionC.php?acc=GuardarActualizaEspecialidades',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        dataResultado: dataResultado
                    }
                }).done(function (data) {
                    console.log(data);
                    if (!data.success) {
                        if (data.errors.IdCertificadoMed) {
							$.messager.alert('SIGESA','Error la Grabar ' ,'error');
                        }
                    } else {
						var mensaje='';
						mensaje+= "Datos Actualizados Correctamente </br>";
						$.messager.confirm('Mensaje', mensaje, function(r){
						if (r){
							NuevoRegistro();		
							}
						});
                    }
                });
	 
	 
}
function NuevoRegistro()
{
 	
	
	location.href="../../MVC_Controlador/Programacion/ProgramacionC.php?acc=Medico";

}

function Procesar(){
			$.ajax({
                    url: '../../MVC_Controlador/Programacion/ProgramacionC.php?acc=ActualizarEspecialidadesCE',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        dataResultado: 1
                    }
                }).done(function (data) {
                    console.log(data);
                    if (!data.success) {
                        if (data.errors.IdCertificadoMed) {
							$.messager.alert('SIGESA','Error la Grabar ' ,'error');
                        }
                    } else {
						var mensaje='';
						mensaje+= "Procesamiento de especialidades ejecutado correctamente </br>";
						$.messager.confirm('Mensaje', mensaje, function(r){
						if (r){
							NuevoRegistro();		
							}
						});
                    }
                });
	}

</script>
</head>

<body>

<div class="easyui-panel" title="Actualizar Especialidades Médicos" style="width:1045px">
    <div id="dlg-buttons" class="easyui-panel" style="padding:0px;">   
    	
		<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'" onClick="GrabarDatos();">Actualizar</a>
	  <!--	<a href="javascript:location.reload()"  class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'">Refrescar</a>   -->    
        <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-cancel'" onclick="NuevoRegistro()">Cancelar</a>        
<!--		<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">Search</a>
		<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-print'">Print</a>-->
    <!--    <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'" onclick="Procesar()">Procesar</a>-->
		<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-help'">Ayuda</a>		
		<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-back'" onClick=" NuevoRegistro()">Salir</a>
	</div>
    
	<div class="easyui-panel" style="padding:5px;">
    
    <select id="nombre" class="easyui-combogrid" name="nombre" style="width:250px;height:30px;"
                                            data-options="
                                            prompt:'Buscar Medico.',
                                            panelWidth:500,
                                            //readonly:'true',
                                            url: '../../MVC_Controlador/Programacion/ProgramacionC.php?acc=BuscarMedicos',                                                           
                                            type: 'POST',
                                            dataType: 'json',                                                            
                                            idField:'IdMedico',
                                            textField:'Medico',
                                            mode:'remote',
                                            columns:[[
                                            {field:'IdMedico',title:'N°Id',width:70},
                                            {field:'Medico',title:'Medico',width:350}                                          
                                            ]],
                                            onSelect:function(index,row){              
                                            
                                         
                                         	// $('#IdMed').textbox('textbox').focus();
                                        	// $('#IdMed').textbox('setText',row.IdMedico);
                                           
                                             var tb = $('#nombre').combogrid('textbox');
                                            tb.bind('keydown',function(event){
                                            var keycode = (event.keyCode ? event.keyCode : event.which);
                                            if(keycode == '13'){                              
          										Cargar();
                                            }              
                                            });                 

                                            },
                                            fitColumns:true

                                            "></select>
    
    
      <?php /*?><input class="easyui-textbox" style="width:120px;height:27px" maxlength="8" data-options="prompt:'Buscar por N°DNI'" id="IdMed" name="IdMed" 
           ><?php */?>
      
             <!--<input  id="ApellidoPaterno" name="ApellidoPaterno" >-->
 
          <strong style="color:#039"> &nbsp;&nbsp;Presione la tecla enter para buscar </strong>
          &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; <strong style="color:#090">  </strong>
          
</div>
		</div>		
 
<div class="easyui-tabs" style="width:1045px;height:280px" title="Detalle Especialidades">
  <div title="Detalle Especialidades" style="padding:0px">
        
         <table class="easyui-datagrid" style="width:100%;height:auto"
                            id="TablaMedicos"  singleSelect="true" >  
                            <thead>
                                <tr>

                                   
                                   <th field="xId" width="50" align="center" >Item</th>
                                   <th field="xMedico" width="400" align="center" >Medico</th>
                                   <th field="xDepartamento" width="213" align="center" >Departamento</th>
                                   <th field="xEspecialidad" width="213" align="center" >Especialidad</th>
                                   <th field="xEstado" width="120" align="center" editor="{type:'checkbox',options:{on:'1',off:'0'}}">Tipo</th>
                                    
                                </tr>
                            </thead>  
                        </table>
        
        <div align="right">
      <strong style="text-align:right;color:#039">  Leyenda:Columna Tipo<br /> Valor 1 = Especialidades Principales <br />Valor 0 = Especialidades Alternativas </strong>
      </div>
        </div>
        
</div>

</body>


</html>