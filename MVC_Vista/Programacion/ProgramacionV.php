<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>PROGRAMACIÓN MÉDICA</title>
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
    <style>
        html,body { 
            padding: 0px;
            margin: 0px;
            height: 100%;
            font-family: Helvetica; 
        }
        #fm{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
        .fitem input{
            width:160px;
        }       
    </style>

        <!-- JS -->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-cellediting.js"></script>

        <!-- JS: OPERACIONES -->
        <script>
        $(document).ready(function() {
            //FUNCIONES

            function MostrarDataGeneral(fecha, Servicio)   
            {
                var fecha = $("#fecha").datetimebox('getValue');
                var Servicio = $("#Servicio").combobox('getValue');

                $.ajax({
                    url:'../../MVC_Controlador/Programacion/ProgramacionC.php?acc=mostrarProgramacion',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        fecha:fecha,
                        Servicio:Servicio
                    },
                    success:function(data)
                    {
                        $("#tabla_programacion").datagrid({
                            data:data
                        });
                        return false;
                    }
                });
            }


            function SeleccionarRegistro()
            {   
                var row = $('#tabla_programacion').datagrid('getSelected');

                if (row){
                    $("#idprog").attr('value',row.IDPROGRAMACION);
                    $("#HORAI").timespinner('setValue',row.HORAINICIO);
                    $("#HORAF").timespinner('setValue',row.HORAFIN);
                    $('#dlg').dialog('open');
                }
            }

            function ActualizarDatos(IDPROGRAMACION, HORAINICIO, HORAFIN)
            {
                if (($("#HORAF").timespinner('getValue').length < 1) ) {
                    $("#texto_mensaje_error").html(' ');
                    $("#texto_mensaje_error").text("Ingrese la hora final.");
                    $('#alerta').dialog('open').dialog('center');
                    return false;
                }

                if (($("#HORAI").timespinner('getValue').length < 1)) {
                    $("#texto_mensaje_error").html(' ');
                    $("#texto_mensaje_error").text("Ingrese la hora de inicio.");
                    $('#alerta').dialog('open').dialog('center');
                   return false;
                }
                if (confirm('¿Esta seguro de guardar los cambios?')) {
                var IDPROGRAMACION = $("#idprog").val();
                var HORAINICIO = $("#HORAI").timespinner('getValue');
                var HORAFIN = $("#HORAF").timespinner('getValue');

                $.ajax({
                    url:'../../MVC_Controlador/Programacion/ProgramacionC.php?acc=actualizarProgramacion',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        IDPROGRAMACION:IDPROGRAMACION,
                        HORAINICIO:HORAINICIO,
                        HORAFIN:HORAFIN
                    },
                    success:function(data)
                    {
                        $('#dlg').dialog('close');
                    }
                });
                }
            }

        /*  $('.easyui-datebox').datebox.defaults.formatter = function(date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
            };

            $('.easyui-datebox').datebox.defaults.parser = function(s) {
            if (s) {
            var a = s.split('/');
            var d = new Number(a[0]);
            var m = new Number(a[1]);
            var y = new Number(a[2]);
            var dd = new Date(y, m-1, d);
            return dd;
            } else {
            return new Date();
            }
            }; */


            //EVENTOS   
            $("#consultar").click(function(event) {
                MostrarDataGeneral();
            });

            $('#tabla_programacion').datagrid({
                onDblClickRow:function(){            
                    SeleccionarRegistro();    
                }
            });

            $('#guardar').click(function(event){
                ActualizarDatos();
                MostrarDataGeneral();
            });
        });
    </script>
</head>
<body>
     <div class="easyui-panel"  style="width:100%;height:110%;">

        <!-- CONTENEDORES -->
        <div class="easyui-layout" data-options="fit:true">
            
            <!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:false" style="height:18%;padding:2%;" title="PROGRAMACIÓN MÉDICA">
                <div style="float:left;">
                    <table>
                        <tr>
                            <td> <strong><u>BÚSQUEDA</u></strong> </td>
                            <td style="width:350px;"></td>
                        </tr>
                        <tr>
                            <td>FECHA DE PROGRAMACIÓN</td>
                            <td style="width:350px;"><input id="fecha" class="easyui-datebox" style="width:150px;" value="<?php echo date('dd/mm')?>"></td>
                        </tr>
                        <tr>
                            <td>NOMBRE DEL CONSULTORIO:</td>
                            <td style="width:350px;">
                                <input class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Programacion/ProgramacionC.php?acc=mostrarServicios'" style="width:350px;" id="Servicio">
                            </td>                                                                             
                        </tr>
                    </table>
                </div>

                <div style="float:left; padding:2%;">
                    <!--  <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" id="buscar_general">BUSCAR</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                    <a class="easyui-linkbutton" id="consultar"><img src="../../MVC_Complemento/easyui/themes/icons/search.png" height="15" width="15" alt="">CONSULTAR</a>
                </div>
                <div style="float:left; padding:2%;">
                    <!--  <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" id="buscar_general">BUSCAR</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                   <!--<a class="easyui-linkbutton" id="guardar"><img src="../../MVC_Complemento/img/guardar.jpg" height="15" width="15" alt="">GUARDAR</a>-->
                </div>
            </div>
            VISUALIZACIONES 
            <div data-options="region:'center',split:false" style="height:82%;" title="RESULTADOS">
                <div style="float:left;width:45%;padding:1%;">
                 
                   <table id="tabla_programacion" title="PROGRAMACION MEDICA" class="easyui-datagrid" style="width:1060px;" data-options="singleSelect:true,collapsible:true"> 
                        <thead>
                            <tr>
                              <th data-options="field:'IDPROGRAMACION',rezisable:true,width:130" >IDPROGRAMACION</th>       
                              <th data-options="field:'CONSULTORIO',rezisable:true,width:200">CONSULTORIO</th>
                              <th data-options="field:'MEDICO',rezisable:true,width:270">MEDICO</th>
                              <th data-options="field:'HORAINICIO',rezisable:true,width:80" >HORAINICIO</th>
                              <th data-options="field:'HORAFIN',rezisable:true,width:80" >HORAFIN</th>
                              <th data-options="field:'PrimerPaciente',rezisable:true,width:90" >Hor.Pri.Paci</th>
                              <th data-options="field:'UltimoPaciente',rezisable:true,width:90" >Hor.Ult.Paci</th>
                              <th data-options="field:'TotPaciente',rezisable:true,width:117" ># Cupos Asignados</th>
                              
                             <!-- <th data-options="field:'IDSERVICIO',rezisable:true,width:100">IDSERVICIO</th>-->
                            </tr>
                        </thead>
                    </table>
                    </div>
                <div id="toolbar">
                     <!-- <a id="modificar" class="easyui-linkbutton" iconCls="icon-edit" plain="true" >Modificar</a>

                        <input class="easyui-combobox" data-options="valueField:'id',textField:'text'" style="width:350px;" id="idprograma">-->

                    </div>
                <div id="dlg" class="easyui-dialog" style="width:330px;height:230px;padding:20px 20px" closed="true" buttons="#dlg-buttons">
                        <div class="ftitle">HORAS PROGRAMADAS</div>
                        <form id="frmhora" method="post" novalidate>
                            <input id="idprog" type="hidden">
                            <div class="fitem">
                                <label>HORA INICIO:</label>
                                <input id="HORAI" name="HORAI" class="easyui-timespinner" data-options="min:'03:00',max:'18:00'" style="width:80px;" required>
                            </div>
                            <div class="fitem">
                                <label>HORA FIN:</label>
                                <input id="HORAF" name="HORAF" class="easyui-timespinner" data-options="min:'03:00',max:'18:00'" style="width:80px;" required>
                            </div>
                      </form>
                     
                    <div id="dlg-buttons">
                        <a id="guardar" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px">Guardar</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancelar</a>
                    </div>
                </div>
            </div>

            <!--ALERTA -->
            <div id="alerta" class="easyui-dialog" style="padding:20px;width:35%;" data-options="inline:false,modal:true,closed:true,title:'Error'">
                <p id="texto_mensaje_error"></p>
                <div class="dialog-button">
                <a href="javascript:void(0)" class="easyui-linkbutton" style="width:100%;height:35px" onclick="$('#alerta').dialog('close')">ACEPTAR</a>
            </div>
</div>
</body>
</html>