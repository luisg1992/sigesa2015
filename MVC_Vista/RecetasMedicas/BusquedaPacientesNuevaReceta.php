<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	//echo $_SESSION['IdMedico'].'resr'.$FechaServidor;
	$ValidarProgramacionMedicos=ListarServiciosProgramacionMedicos_M($_SESSION['IdMedico'],$FechaServidor);
	if($ValidarProgramacionMedicos!=NULL){
		$TipoServicio='1';
	}else{
		$TipoServicio='';
	}
?>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - RECETAS MÉDICAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">   

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--Alerts -->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!--NUEVO-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">-->
    
    <!--datepicker y autocompletado-->
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.css">
  		
	<script type="text/javascript">
	 
		function cambiarFecha(){
			var FechaIngreso=document.getElementById('FechaIngreso').value;
			var IdMedico='<?php echo $_SESSION['IdMedico'] ?>';
			
			$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarServiciosProgramacionMedicos&FechaIngreso="+FechaIngreso+"&IdMedico="+IdMedico, function(json){
					$('#Servicio').empty();
					$('#Servicio').append($('<option>').text("SELECCIONE"));
					if(json!=""){						
						$.each(json, function(i, obj){							
							$('#TipoServicio').val('1');
							$('#Servicio').append($('<option>').text(obj.text).attr('value', obj.val));
							//limpiar
							$('#DescripcionServicio').val('');	
							$('#Paciente').empty(); 
							$('#Paciente').append($('<option>').text("SELECCIONE"));
								
						});
						
					}else{
						//limpiar							
						$('#TipoServicio').val('');								
						$('#Servicio').empty(); 
						$('#Servicio').append($('<option>').text("SELECCIONE"));
						$('#DescripcionServicio').val('');
						$('#Paciente').empty(); 
						$('#Paciente').append($('<option>').text("SELECCIONE"));
					}
			});			 
		}
		
		function cambiarTipoServicio(){
			var TipoServicio=document.getElementById('TipoServicio').value;
			  if(TipoServicio=='1'){	
					//llamar servicios consulta externa 
				 	var FechaIngreso=document.getElementById('FechaIngreso').value;
					var IdMedico='<?php echo $_SESSION['IdMedico'] ?>';
					
					$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarServiciosProgramacionMedicos&FechaIngreso="+FechaIngreso+"&IdMedico="+IdMedico, function(json){
							$('#Servicio').empty();
							$('#Servicio').append($('<option>').text("SELECCIONE"));
							if(json!=""){													
								$.each(json, function(i, obj){										
									$('#Servicio').append($('<option>').text(obj.text).attr('value', obj.val));
									//limpiar
									$('#DescripcionServicio').val('');	
									$('#Paciente').empty(); 
									$('#Paciente').append($('<option>').text("SELECCIONE"));										
								});	
							}else{
								//limpiar							
								$('#DescripcionServicio').val('');
								$('#Paciente').empty(); 
								$('#Paciente').append($('<option>').text("SELECCIONE"));
							}			
					});			
					
			  }else if(TipoServicio=='2'){	
				 //llamar servicios emergencia 		
				 $.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarServiciosEmergencia", function(json){
						$('#Servicio').empty();
						$('#Servicio').append($('<option>').text("SELECCIONE"));
						$.each(json, function(i, obj){
								$('#Servicio').append($('<option>').text(obj.text).attr('value', obj.val));	
								//limpiar								
								$('#DescripcionServicio').val('');	
								$('#Paciente').empty(); 
								$('#Paciente').append($('<option>').text("SELECCIONE"));						
						});
				 });	
			  }else if(TipoServicio=='3'){
				  //llamar servicios hospitalizacion
				  $.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarServiciosHospitalizacion", function(json){
						$('#Servicio').empty();
						$('#Servicio').append($('<option>').text("SELECCIONE"));
						$.each(json, function(i, obj){
								$('#Servicio').append($('<option>').text(obj.text).attr('value', obj.val));									
								//limpiar								
								$('#DescripcionServicio').val('');	
								$('#Paciente').empty(); 
								$('#Paciente').append($('<option>').text("SELECCIONE"));						
						});
				});	
			  }else{
				//limpiar										
				$('#Servicio').empty(); 
				$('#Servicio').append($('<option>').text("SELECCIONE"));											
				$('#DescripcionServicio').val('');	
				$('#Paciente').empty(); 
				$('#Paciente').append($('<option>').text("SELECCIONE"));
			  }
			
		}
		
		function cambiarServicio(){
			
				var FechaIngreso=document.getElementById('FechaIngreso').value;
				var IdMedico='<?php echo $_SESSION['IdMedico'] ?>';
				var Servicio=document.getElementById('Servicio').value;
				
				if(Servicio.trim()!="SELECCIONE"){
					var TipoServicio=document.getElementById('TipoServicio').value;
					if(TipoServicio=='1'){ //ListarPacientesProgramacionMedicos_M
					  $.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarPacientesProgramacionMedicos&FechaIngreso="+FechaIngreso+"&IdMedico="+IdMedico+"&IdServicio="+Servicio, function(json){
								$('#Paciente').empty();
								$('#Paciente').append($('<option>').text("SELECCIONE"));
								$.each(json, function(i, obj){
										$('#Paciente').append($('<option>').text(obj.text).attr('value', obj.val));									
										$('#DescripcionServicio').val(obj.DescripcionServicio);							
								});
						});	
					}else if(TipoServicio=='2'){ //ListarPacientesEmergencia_M
						$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarPacientesEmergencia&FechaIngreso="+FechaIngreso+"&IdServicio="+Servicio, function(json){
								$('#Paciente').empty();
								$('#Paciente').append($('<option>').text("SELECCIONE"));
								
								if(json!=""){																			
									$.each(json, function(i, obj){
											$('#Paciente').append($('<option>').text(obj.text).attr('value', obj.val));									
											$('#DescripcionServicio').val(obj.DescripcionServicio);					
									});	
								}else{
									//alert('No existen Pacientes en el Servicio Seleccionado');
									alertify.error("<b> Mensaje del Sistema: </b> <h1>No existen Pacientes en el Servicio Seleccionado</h1>");
									//limpiar										
									$('#DescripcionServicio').val('');	
								}							
								
						});	
					}else if(TipoServicio=='3'){ //ListarPacientesHospitalizacion_M
						$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarPacientesHospitalizacion&FechaIngreso="+FechaIngreso+"&IdServicio="+Servicio, function(json){
								$('#Paciente').empty();
								$('#Paciente').append($('<option>').text("SELECCIONE"));
								
								if(json!=""){																			
									$.each(json, function(i, obj){
										$('#Paciente').append($('<option>').text(obj.text).attr('value', obj.val));									
										$('#DescripcionServicio').val(obj.DescripcionServicio);							
									});
								}else{
									//alert('No existen Pacientes en el Servicio Seleccionado');
									alertify.error("<b> Mensaje del Sistema: </b> <h1>No existen Pacientes en el Servicio Seleccionado</h1>");
									//limpiar										
									$('#DescripcionServicio').val('');	
								}
								
						});	
					}	
								
				}else{
					$('#Paciente').empty(); 
					$('#Paciente').append($('<option>').text("SELECCIONE"));	
					$('#DescripcionServicio').val('');							
				}			
		}	
		
		function llenarDatosPaciente(){
			Paciente=document.getElementById('Paciente').value;
			var res = Paciente.split("|");
			//alert(res[0]);
			document.getElementById('IdPaciente').value=res[0];
			document.getElementById('IdCuentaAtencion').value=res[1];
			document.getElementById('idFuenteFinanciamiento').value=res[2];
			document.getElementById('IdEstado').value=res[3];
			document.getElementById('estadoCta').value=res[4];
		}
		
		
	
	</script>
   
</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">				
				<?php  include('../../MVC_Vista/RecetasMedicas/Cabecera.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">REGISTRAR RECETAS MÉDICAS</h3>
                </div>                
            </div>
            <!-- /.row -->
            
            <div class="row">
            
                <div class="container-fluid">               
                   <div class="col-md-12">
                            <div class="panel panel-primary"> <!--with-nav-tabs--> 
                            	<div class="panel-heading">BÚSQUEDA DE PACIENTES PARA REGISTRAR NUEVA RECETA</div>
                                <!--<div class="panel-heading"  >
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab1primary" data-toggle="tab">Identificar</a></li>
                                            <li class = "desabilitar"><a href="#tab2primary" data-toggle="tab">Inicio</a></li>
                                            <li class = "desabilitar"><a href="#tab3primary" data-toggle="tab">Pausa</a></li>
                                            <li class = "desabilitar"><a href="#tab4primary" data-toggle="tab">Salida</a></li>          
                                        </ul>
                                </div>-->
                                <div class="panel-body">
                                  <div class="tab-content">
                                    <!--Inicio Identificacion-->
                                        <div class="tab-pane fade in active" id="tab1primary">
                                            
                                                     
                                               <form class="form-horizontal" id="form1" name="form1" method="post" action="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=AceptarBusquedaPacientes&IdEmpleado=<?php echo $IdEmpleado ?>"  >
                                                
                                                    <!--fila 1-->
                                                    <div class="form-group">
                                                    	<label class="control-label col-xs-1">Fecha Ingreso:</label>
                                                        <div class="col-xs-2">
                                                            <input type="text" id="FechaIngreso" name="FechaIngreso" class="form-control datepicker input-sm" value="<?php echo date("d/m/Y") ?>" placeholder="Fecha Ingreso" onChange="cambiarFecha()" />  							                                                        
                                                        </div>
                                                        <label class="control-label col-xs-1">Tipo Servicio:</label>
                                                        <div class="col-xs-2">  
                                                            <select id="TipoServicio" name="TipoServicio" class="form-control input-sm" onChange="cambiarTipoServicio()" >
                                                                <option value="">SELECCIONE</option> 
                                                                <option value="1" <?php if($TipoServicio=='1'){ ?>selected<?php } ?>>Consulta Externa</option> 
                                                                <option value="2">Emergencia</option>
                                                                <option value="3">Hospitalizacion</option>                
                                                             </select>                                                                   	
                                                        </div>                     
                                                        
                                                        <label class="control-label col-xs-1">Servicio:</label>
                                                        <div class="col-xs-2">
                                                        	<select id="Servicio" name="Servicio" class="form-control input-sm" onChange="cambiarServicio()" >
                                                                <option value="">SELECCIONE</option> 
                                                                     <?php 
                                                                        $resultados=ListarServiciosProgramacionMedicos_M($_SESSION['IdMedico'],$FechaServidor);								 
                                                                        if($resultados!=NULL){
                                                                        for ($i=0; $i < count($resultados); $i++) {	
                                                                     ?>
                                                                 <option value="<?php echo $resultados[$i]["IdServicio"] ?>"  ><?php echo $resultados[$i]["IdServicio"].' | '.mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
                                                                 <?php 
                                                                    }}
                                                                 ?>                   
                                                            </select> 
                                                            <input name="DescripcionServicio" id="DescripcionServicio" type="hidden" />                                                            
                                                            <input name="IdMedico" id="IdMedico" type="hidden" value="<?php echo $_SESSION['IdMedico']; ?>" /> 
                                                            
                                                        </div>
                                                        
                                                          
                                                    </div> 
                                                    <!--FIN fila 1--> 
                                                      
                                                    <!--fila 2--> 
                                                     <div class="form-group">
                                                        <label class="control-label col-xs-1">Paciente</label>
                                                        <div class="col-xs-2">  
                                                            <select id="Paciente" name="Paciente" class="form-control input-sm" onChange="llenarDatosPaciente()"  >
                                                                 <option value="SELECCIONE">SELECCIONE</option>         
                                                            </select>    
                                                            <input name="IdPaciente" id="IdPaciente" type="hidden" /> 
                                                            <input name="idFuenteFinanciamiento" id="idFuenteFinanciamiento" type="hidden" />                                                            <input name="IdEstado" id="IdEstado" type="hidden" /> 
                                                            <input name="estadoCta" id="estadoCta" type="hidden" />  	
                                                        </div> 
                                                        <label class="control-label col-xs-1">Cuenta</label>
                                                        <div class="col-xs-1">       
                                                            <input name="IdCuentaAtencion" id="IdCuentaAtencion" type="text" class="form-control input-sm" />                                                        </div>                                                         
                                                            
                                                        <div class="col-xs-1 text-right">  
                                                            <input type="button" value="Aceptar" onClick="Aceptar()" class="btn btn-group-sm btn-primary" />                                                        </div>                                                         
                                                      </div> 
                                                      <!--FIN fila 2-->                                                   
                                                  
                                                 </form>
                                                                                      
                                        
                                        </div>
                                   		<!--tab1primary-->
                                        
                                        <div class="tab-pane fade" id="tab2primary">                                    
                                        </div>
                                        <!--tab2primary-->
                                        
                                        <div class="tab-pane fade" id="tab3primary">                                                                    
                                        </div> 
                                        <!--tab3primary-->
                                                              
                                        <div class="tab-pane fade" id="tab4primary"> 
                                        </div>
                                        <!--tab4primary-->
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                </div>
                                
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    
     	<script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script>         
        
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
        
       <!-- datepicker y autocompletado-->     
       <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        
 <script type="text/javascript">
    $(function () {
	
		//Array para dar formato en español
        $.datepicker.regional['es'] =
        {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        			$.datepicker.setDefaults($.datepicker.regional['es']);

       				$("#FechaIngreso").datepicker();
       });
   

   function Aceptar(){
	   
	   var Paciente=document.getElementById('Paciente').value;
	   
	   if(Paciente.trim=='' || Paciente.trim()=='SELECCIONE'){
		   alertify.error("<b> Mensaje del Sistema: </b> <h1>Falta Buscar Paciente</h1>");
		   return 0;
	   }
	   
	   var IdEstado=document.getElementById('IdEstado').value;
	   var estadoCta=document.getElementById('estadoCta').value;
	   
	   if(IdEstado.trim()!=1){
		   alertify.error("<b> Mensaje del Sistema: </b> <h1>La cuenta se encuentra "+estadoCta+"</h1>");
		   return 0;
	   }
	   
	   //document.getElementById('form1').submit();
	   alertify.confirm("Seguro de Registrar Receta al Paciente seleccionado", function (e) {
			if (e) {							
				document.getElementById('form1').submit();
			} 
		});	
	   
   }

</script>  
</body>

</html>
