<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/imprimir.css"/>
<title>IMPRESIÓN RECETAS</title>
 
<style type="text/css">
.botonExcel{cursor:pointer;}

@media all {
   div.saltopagina{
      display: none;
   }
}
   
@media print{
   div.saltopagina{
      display:block;
      page-break-before:always;
   }
} 
</style>
<style type="text/css" media="print">
.nover {display:none}
</style>
</head>
<body> <!--onLoad="window.print();"-->

<?php

//session_start();
//include('../../MVC_Modelo/SistemaM.php');
//include('../../MVC_Modelo/DatosInstitucionesM.php');
 ini_set('error_reporting',0);//para xamp
 date_default_timezone_set('America/Bogota');
 include('../../MVC_Modelo/RecetasMedicasM.php');
 include('../../MVC_Modelo/SistemaM.php');
 include('../../MVC_Complemento/librerias/Funciones.php');

//$porciones = explode("|", $IdRecetas);//$IdReceta='43|32|44|'
$porciones = explode("|", $_GET["IdRecetas"]);//$IdRecetas='43|32|44|'
$cantidad=count($porciones);//4

for ($g=0;$g<$cantidad-1;$g++){ //0,1,2
	$IdReceta=$porciones[$g];
		
?>

<ul class="pro15 nover">
    <li><a href="#nogo" onClick="window.print();" class="nover"><em class="home nover"></em><b>Imprimir</b></a></li>
     
    <li><a href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ModificarReceta&IdReceta=<?php echo $IdReceta; ?>&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"   class="nover" >
    <em class="find nover"></em><b><< Seguir Editando </b></a>
    </li>
    <li><a href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado']; ?>"   class="nover" >
    <em class="find nover"></em><b><< Salir </b></a>
    
    </li>
</ul>
 <?php
  
    $ListarRecetaCabecera=RecetaCabeceraPoridRecetaM($IdReceta);	
	
	$Paciente=$ListarRecetaCabecera[0]["ApellidoPaterno"].' '.$ListarRecetaCabecera[0]["ApellidoMaterno"].' '.$ListarRecetaCabecera[0]["PrimerNombre"];
	$NombrePuntoCarga=$ListarRecetaCabecera[0]['NombrePuntoCarga'];	
	$NroHistoriaClinica=$ListarRecetaCabecera[0]['NroHistoriaClinica']; 
	$PuntoCarga=$ListarRecetaCabecera[0]["PuntoCarga"]; 	
	$idReceta=$ListarRecetaCabecera[0]["idReceta"]; 
	
	//$FechaReceta=$ListarRecetaCabecera[0]["FechaReceta"]; 
	$FechaReceta=vfecha(substr($ListarRecetaCabecera[0]['FechaReceta'],0,10));
	$HoraReceta=substr($ListarRecetaCabecera[0]['FechaReceta'],11,8);
	
	$Servicio=$ListarRecetaCabecera[0]["Servicio"]; 
	$IdPuntoCarga=$ListarRecetaCabecera[0]["IdPuntoCarga"]; 	
	
    $idComprobantePago=$ListarRecetaCabecera[0]["idComprobantePago"];
	$IdservicioIngreso=$ListarRecetaCabecera[0]["IdservicioIngreso"];
	$IdMedicoIngreso=$ListarRecetaCabecera[0]["IdMedicoIngreso"];	
	
	//Nombre Medico (ApellidoParterno ApellidoMarterno 1Nombre)
	$NombreMedico=trim(mb_strtoupper($ListarRecetaCabecera[0]["NombreMedico"]));
	$NombreMedicox=explode(' ',$NombreMedico);
	if(count($NombreMedicox)>3){
		$NombreMedico=$NombreMedicox[0].' '.$NombreMedicox[1].' '.$NombreMedicox[2];
	}
	 		
	$NombrePaciente=mb_strtoupper($ListarRecetaCabecera[0]["NombrePaciente"]);
	
	//$FechaAtencion=$ListarRecetaCabecera[0]["FechaAtencion"]; 
	//$HoraAtencion=$ListarRecetaCabecera[0]["HoraAtencion"];	
	$NombreEspecialidad=$ListarRecetaCabecera[0]["NombreEspecialidad"]; 
	//$ValidoHasta=$ListarRecetaCabecera[0]["ValidoHasta"];
	 
	$IdFormaPago=$ListarRecetaCabecera[0]["IdFormaPago"]; 
	$Descripcion=$ListarRecetaCabecera[0]["descripcion"];	
	$IdAtencion=$ListarRecetaCabecera[0]["IdAtencion"];
	$idCuentaAtencion=$ListarRecetaCabecera[0]["idCuentaAtencion"]; 
	 
	//OBTENER AFILIACION	
	$lcAfiliacionIdSiaSis=0 ;
	$AtencionesDatosAdicionales=AtencionesDatosAdicionalesSeleccionarPorIdM($IdAtencion);	
	if($AtencionesDatosAdicionales[0]["IdSiaSis"]==0){	
		$SisFuaAtencion=SisFuaAtencionSeleccionarPorIdM($idCuentaAtencion);
		if($SisFuaAtencion!=NULL){
			$lcAfiliacionIdSiaSis=$SisFuaAtencion[0]["idSiasis"];  
			$lcAfiliacionCodigo=$SisFuaAtencion[0]["Codigo"]; 
		}
	}else{
		$lcAfiliacionIdSiaSis=$AtencionesDatosAdicionales[0]["IdSiaSis"];  
		$lcAfiliacionCodigo=$AtencionesDatosAdicionales[0]["SisCodigo"]; 
	}
	$filtro = " where idSiaSis=" . $lcAfiliacionIdSiaSis . " and codigo=''" . $lcAfiliacionCodigo . "''";
	$SisFiltra=SisFiltraPacientesAfiliadosM($filtro);
	if($SisFiltra!=NULL){
		$BuscarNumeroAfiliacion =$SisFiltra[0]["cDisa"]. " " .$SisFiltra[0]["cFormato"]. " " . $SisFiltra[0]["cNumero"];
		$Afiliacion="(Nº Afil:". $BuscarNumeroAfiliacion.')';
	}else{
		$BuscarNumeroAfiliacion="";
		$Afiliacion="";
	}
	
	
	$TipoServicio=$ListarRecetaCabecera[0]["TipoServicio"]; 
	$xTipoServicio=trim(substr($TipoServicio,0,3));
	if($xTipoServicio=='Hos'){ //Hospitalizacion		
		$TipoDiagnostico = 2;
	}else if($xTipoServicio=='Eme'){ //Emergencia			
		$TipoDiagnostico = 2;
	}else{ //consulta externa		
		$TipoDiagnostico = 1;
	}	
		
	//OBTENER DIAGNOTICOS
	$rsDiagnosticos=AtencionesDiagnosticosSeleccionarPorAtencionM($IdAtencion,$TipoDiagnostico);
	if($rsDiagnosticos!=NULL){	
    	$lcDx = "(" .trim($rsDiagnosticos[0]["CodigoCIE2004"]) . ") " .$rsDiagnosticos[0]["Descripcion"];       
	}else{
		$lcDx = "";
	}	
	
	$EdadPaciente=$ListarRecetaCabecera[0]["EdadPaciente"]; 
	$NombreEESS=$ListarRecetaCabecera[0]["NombreEESS"];  
	$DireccionEESS=$ListarRecetaCabecera[0]["DireccionEESS"]; 
	$TelefonoEESSx=$ListarRecetaCabecera[0]["TelefonoEESS"];//TELEFONO : 614-7474  
	$TelefonoEESSxx=explode(':',$TelefonoEESSx);
	$TelefonoEESS=$TelefonoEESSxx[1];
	
	$RucEESSx=$ListarRecetaCabecera[0]["RucEESS"];//RUC : 20174943924
	$RucEESSxx=explode(':',$RucEESSx);
	$RucEESS=$RucEESSxx[1];
	
	$fechaVigencia=$ListarRecetaCabecera[0]["fechaVigencia"]; 	
	

?>
<table width="330" height="434" border="0" cellpadding="0" cellspacing="0">
 <tr>
   <td colspan="4">&nbsp;</td>
 </tr>
 <tr>
   <td colspan="4" align="center"><font face="Calibri" size="+1"><strong><?php echo $NombreEESS ?></strong></font></td>
 </tr>
 <tr>
   <td colspan="4" align="center"><font face="Calibri">RUC: <?php echo $RucEESS ?><br>
	<?php echo $DireccionEESS ?><br>Telef: <?php echo $TelefonoEESS ?></font></td>
 </tr>
 <tr>
   <td colspan="4" align="center" height="35"><font face="Calibri" size="+1"><strong>ORDEN MEDICA</strong></font></td>
 </tr>
 <tr>
   <td width="82"><font face="Calibri" >Servicio: </font></td>   
   <td width="134"><font face="Calibri" ><?php echo trim(mb_strtoupper($PuntoCarga)); ?></font></td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
  </tr>
  <tr>
   <td colspan="4"><font face="Calibri" >Fecha/Hora Atencion: </font><font face="Calibri" ><?php echo $FechaReceta.' '.$HoraReceta; ?></font></td>
  </tr>
  <tr>
   <td colspan="2"><font face="Calibri" >NºOrd.Med: <?php echo $idReceta; ?></font></td>
   <td colspan="2"><font face="Calibri" >NºCta: <?php echo $idCuentaAtencion; ?></font></td>
 </tr>

 <tr>
   <td><font face="Calibri" >Consultorio: </font></td>
   <td colspan="3"><font face="Calibri" ><?php echo mb_strtoupper($Servicio); ?></font></td>
  </tr>
 </tr>
 <tr>
   <td colspan="4"><font face="Calibri" >Prof. de la Salud: </font><font face="Calibri" ><?php echo $NombreMedico; ?></font></td>
  </tr>
 <tr>
   <td><font face="Calibri" >Paciente: </font></td>
   <td colspan="3"><font face="Calibri" ><?php echo $Paciente; ?></font></td>
  </tr>
 <tr>
   <td><font face="Calibri" >Tipo Plan: </font></td>
   <td colspan="3"><font face="Calibri" ><?php echo $Descripcion.' '.$Afiliacion; ?></font></td>
  </tr>
 <tr>
   <td colspan="4"><font face="Calibri" ><?php echo $lcDx; ?> </font></td>   
 </tr>
 <tr>
   <td colspan="2">Hc:<?php echo $NroHistoriaClinica; ?></td>
   <td colspan="2">(Edad: <?php echo $EdadPaciente; ?>)</td>
 </tr> 
 
 <tr height="33">
   <td colspan="3"><font face="Calibri" size="+1" ><strong>Concepto</strong></font></td>
   <td width="87" align="right"><font size="+1" face="Calibri" id="cant"  ><strong>Cant.</strong></font></td>
  </tr>
 
<tr>
  <td colspan="4"><hr></td>
</tr> 

<?php
 
$RecetaDetalleServicioPorIdReceta=RecetaDetalleServicioPorIdRecetaM($IdReceta,$IdPuntoCarga);
if($RecetaDetalleServicioPorIdReceta != NULL){ 
$i=0;
	  foreach($RecetaDetalleServicioPorIdReceta as $item){
?>

<tr height="45">
   <td colspan="3"><?php echo $item['nombre']; ?></td>
   <td align="right"><?php echo number_format( $item['CantidadPedida'],2); ?></td>
</tr>
 
<?php  }  

}  ?>

<tr>
  <td colspan="4"><hr></td>
</tr>

<tr>
  <td colspan="4">Fecha: <?php echo date('d/m/y  H:i:s') ?></td>
</tr>

<tr>
  <td colspan="4" height="33">Terminal: <?php $nombrePC=gethostbyaddr($_SERVER['REMOTE_ADDR']); echo $nombrePC ?></td>
</tr>

<tr>
  <td colspan="4" align="center" height="33">TICKET VALIDO POR HASTA 45 HORAS</td>
</tr>

</table>

<?php  }  ?>
	   
</body> 