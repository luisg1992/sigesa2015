<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	//echo $_SESSION['IdMedico'].'resr'.$FechaServidor;	
?>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - RECETAS MÉDICAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">   

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--Alerts -->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
        
    <!--NUEVO-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">-->
    
    <!--datepicker y autocompletado-->
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.css">  		
	   
</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">				
				<?php  include('../../MVC_Vista/RecetasMedicas/Cabecera.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">LISTAR RECETAS MÉDICAS</h3>
                </div>                
            </div>
            <!-- /.row -->
            
            <div class="row">
            
                <div class="container-fluid">               
                   <div class="col-md-12">
                            <div class="panel panel-primary"> <!--with-nav-tabs--> 
                            	<div class="panel-heading">BÚSQUEDA DE RECETAS MÉDICAS</div>
                                                                
                                <div class="panel-body">
                                  <div class="tab-content">
                                                                                         
                                               <form class="form-horizontal" id="form1" name="form1" method="post" action="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $IdEmpleado ?>"  >
                                                <div class="form-control-static" align="right">
                                                 <input class="btn btn-success" type="button" onClick="Buscar()" value="Buscar"/>
                                                 &nbsp;<input class="btn btn-warning" type="button" onClick="Limpiar()" value="Limpiar"/>                                                
                                                 <!--&nbsp;<a class="btn btn-warning" onClick="location.reload();" >Refrescar</a>&nbsp;
                                                 &nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                                                </div> <br>
                                                    <!--fila 1-->
                                                    <div class="form-group">
                                                    	<label class="control-label col-xs-1">Tipo Servicio:</label>
                                                        <div class="col-xs-2">
                                                            <select id="idTipoServicio" name="idTipoServicio" class="form-control input-sm">
                                                            	<option value="">Seleccione</option>
                                                                <option value="1" <?php if($idTipoServicio=='1'){?> selected <?php } ?>>Consulta Externa</option>
                                                                <option value="2" <?php if($idTipoServicio=='2' || $idTipoServicio=='4'){?> selected <?php } ?>>Emergencia</option>
                                                                <option value="3" <?php if($idTipoServicio=='3'){?> selected <?php } ?>>Hospitalización</option>
                                                            </select>	                                                        
                                                        </div>
                                                            
                                                    	<label class="control-label col-xs-1">Nro Receta:</label>
                                                        <div class="col-xs-1">
                                                            <input type="text" id="idReceta" name="idReceta" class="form-control input-sm" placeholder="Nro Receta" value="<?php echo $_REQUEST["idReceta"] ?>" />
                                                        </div>
                                                            
                                                        <label class="control-label col-xs-1">Nro Cuenta:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" id="idCuentaAtencion" name="idCuentaAtencion" class="form-control input-sm" placeholder="Nro Cuenta" value="<?php echo $_REQUEST["idCuentaAtencion"] ?>" />
                                                        </div>                     
                                                            
                                                    </div> 
                                                    <!--FIN fila 1-->    
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">                                                    	                  
                                                        
                                                        <label class="control-label col-xs-1">Nro Documento:</label>
                                                        <div class="col-xs-2">
                                                        	<input type="text" id="NroDocumento" name="NroDocumento" class="form-control input-sm" placeholder="Nro Documento" value="<?php echo $_REQUEST["NroDocumento"] ?>" /> 
                                                        </div>
                                                        
                                                        <label class="control-label col-xs-1">Nro Historia:</label>
                                                        <div class="col-xs-1">
                                                        	<input type="text" id="NroHistoriaClinica" name="NroHistoriaClinica" class="form-control input-sm" placeholder="Nro Historia" value="<?php echo $_REQUEST["NroHistoriaClinica"] ?>" /> 
                                                        </div>
                                                        
                                                        <label class="control-label col-xs-1">Ap.Paterno:</label>
                                                        <div class="col-xs-1">
                                                        	<input type="text" id="ApellidoPaterno" name="ApellidoPaterno" class="form-control input-sm" placeholder="Ap.Paterno" value="<?php echo $_REQUEST["ApellidoPaterno"] ?>" /> 
                                                        </div>
                                                        
                                                        <label class="control-label col-xs-1">Ap.Materno:</label>
                                                        <div class="col-xs-1">
                                                        	<input type="text" id="ApellidoMaterno" name="ApellidoMaterno" class="form-control input-sm" placeholder="Ap.Materno" value="<?php echo $_REQUEST["ApellidoMaterno"] ?>" /> 
                                                        </div>
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                       
                                                                                                     
                                                  
                                                 </form>
                                                 
                                                <?php 
													$lcSql = "";
													$idTipoServicio=(isset($idTipoServicio) ? $idTipoServicio : "");											
													
													if ($idTipoServicio == 3) {
													   $lcSql = $lcSql. " dbo.Atenciones.idTipoServicio=3 and";
													}else if ($idTipoServicio == 1) {
													   $lcSql = $lcSql. " dbo.Atenciones.idTipoServicio=1 and";
													}else{
													   $lcSql = $lcSql. " (dbo.Atenciones.idTipoServicio=2 or dbo.Atenciones.idTipoServicio=4) and";
													}
	
													if ($_REQUEST['ApellidoPaterno'] <> "") {     
														  $lcSql = $lcSql . "  dbo.Pacientes.ApellidoPaterno like ''%".$_REQUEST['ApellidoPaterno']."%'' and";   //  dbo.Pacientes.ApellidoPaterno like "%MESIAS%"    
													}
													if ($_REQUEST['ApellidoMaterno'] <> "") {       
														  $lcSql = $lcSql . "  dbo.Pacientes.ApellidoMaterno like ''%".$_REQUEST['ApellidoMaterno']."%'' and";       
													}
													if ($_REQUEST['NroHistoriaClinica'] <> 0) {
													   $lcSql = $lcSql . "  dbo.Pacientes.NroHistoriaClinica= " . $_REQUEST['NroHistoriaClinica'] . " and";
													}
													if ($_REQUEST['NroDocumento'] <> "") {
													   $lcSql = $lcSql . "  dbo.Pacientes.NroDocumento= " . $_REQUEST['NroDocumento'] . " and";
													}
													if ($_REQUEST['idReceta'] > 0) {
													   $lcSql = $lcSql . "  dbo.RecetaCabecera.idReceta= " . $_REQUEST['idReceta'] . " and";
													}
													if ($_REQUEST['idCuentaAtencion'] > 0) {
													   $lcSql = $lcSql . "  dbo.RecetaCabecera.idCuentaAtencion= " . $_REQUEST['idCuentaAtencion'] . " and";
													}
												
													//$lcSql = Left($lcSql, Len($lcSql) - 4);
													$lcSql = substr($lcSql,0,-4);
   													$lcSql = $lcSql . " Order by dbo.RecetaCabecera.FechaReceta desc, dbo.Pacientes.ApellidoPaterno, dbo.Pacientes.ApellidoMaterno, dbo.Pacientes.PrimerNombre";					
																										
													$data=RecetaFiltrar($lcSql);
													
													if($idTipoServicio!=""){
														if($data!=NULL){	
												?>

                                                <hr />
                                                <!--<div class="form-control-static" align="right">
                                                 <input class="btn btn-success" type="button" onClick="Buscar()" value="Buscar"/>
                                                 &nbsp;<a class="btn btn-danger" onClick="cancelarPaquetes();">Cancelar</a>
                                                </div>-->                                              
                                                
                                                    <table id="tblSample" class="table table-hover" style="font-size:12px;">            	
                                                        <thead>        
                                                            <tr>
                                                            	<th></th>
                                                                <th>Nro Receta</th>
                                                                <th>Nro Cuenta</th> 
                                                                <th>F.Receta</th> 
                                                                <th>Nro Historia</th> 
                                                                <th>Ap.Paterno</th>                                                              
                                                                <th>Ap.Materno</th> 
                                                                <th>1er Nombre</th>
                                                                <th>Nro Documento</th>
                                                                <th>Modificar</th> 
                                                                <th>Eliminar</th>          
                                                            </tr>                                                        
                                                        </thead>
                                                        
                                                        <tbody>                                                             
                                                        <?php 																
                                                            for ($i=0; $i < count($data); $i++) { 															
																$AtencionesSelecionarPorCuenta=AtencionesSelecionarPorCuenta($data[$i]['idCuentaAtencion']);
																																
																if($AtencionesSelecionarPorCuenta!=''){ 
																	$IdEstado=$AtencionesSelecionarPorCuenta[0];
																	$estadoCta=$AtencionesSelecionarPorCuenta[1]; 
																	$descripcionEC="<font color='#FF0000'>$estadoCta</font>"; 
																}                                                              		
                                                        ?>	        
                                                            <tr>
                                                              <td><?php echo $i+1; ?></td>
                                                              <td><?php echo $data[$i]['idReceta']; ?></td>
                                                              <td><?php echo $data[$i]['idCuentaAtencion']; ?></td>
                                                              <td><?php echo vfecha(substr($data[$i]['FechaReceta'],0,10)).' '.substr($data[$i]['FechaReceta'],11,8); ?></td>              
                                                              <td><?php echo $data[$i]['NroHistoriaClinica']; ?></td> 
                                                              <td><?php echo $data[$i]['ApellidoPaterno']; ?></td>    
                                                              <td><?php echo $data[$i]['ApellidoMaterno']; ?></td>
                                                              <td><?php echo $data[$i]['PrimerNombre']; ?></td>   
                                                              <td><?php echo $data[$i]['NroDocumento']; ?></td>                                                               
                                                              <td>
                                                              	<?php 
																	if($IdEstado <> 1){ 																		
																		echo $descripcionEC;  
																?>                                                              		
                                                                <?php }else{  ?>
                                                                	<input class="btn btn-info" type="button" onClick="ModificarReceta('<?php echo $data[$i]['idReceta']; ?>')" value="Modificar"/>
                                                                <?php } ?>   
                                                              </td>   
                                                              <td>
                                                              	<?php 
																	if($IdEstado <> 1){ 																		
																		echo $descripcionEC;  
																?>                                                              		
                                                                <?php }else{  ?>
                                                              	<input class="btn btn-danger" type="button" onClick="EliminarReceta('<?php echo $data[$i]['idReceta']; ?>')" value="Eliminar"/>
                                                                 <?php }  ?>
                                                              </td>         
                                                            </tr>
                                                        <?php } ?>    	
                                                                
                                                        </tbody>
                                                    
                                                    </table>
                                                
                                                  <?php 
												  	}else{ echo "<br>NO HAY DATOS"; }
												  } 
												  ?>                                                                                    
                                      
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                </div>
                                
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    
     	<script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script>         
        
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
        
       <!-- datepicker y autocompletado-->     
       <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        
 <script type="text/javascript">
    $(function () {
	
		//Array para dar formato en español
        $.datepicker.regional['es'] =
        {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        			$.datepicker.setDefaults($.datepicker.regional['es']);

       				$("#FechaIngreso").datepicker();
       });
   

   function Buscar(){
	  var idTipoServicio=document.getElementById('idTipoServicio').value;
	 
	  var ApellidoPaterno=document.getElementById('ApellidoPaterno').value;
	  var ApellidoMaterno=document.getElementById('ApellidoMaterno').value;	  
	  
	  /*var theTable = document.getElementById('tblSample');
	  if(typeof theTable !== 'undefined' && theTable !== null) {	
		cantFilas = theTable.rows.length;//a partir de cantFilas==2 la tabla esta llena
	  }else{
		cantFilas = 0;  
	  }*/
	  
	  if(idTipoServicio==''){
		  alertify.error("<b> Mensaje del Sistema: </b> <h1>Seleccione Tipo de Servicio</h1>");
		  document.getElementById('idTipoServicio').focus();
		  return 0;	
		  	  
	  }else if(ApellidoPaterno.trim()=='' && ApellidoMaterno.trim()!=''){
		  alertify.error("<b> Mensaje del Sistema: </b> <h1>Por favor ingrese Ap. Paterno</h1>");
		  document.getElementById('ApellidoPaterno').focus();
		  return 0;	 
		    
	  }else{
		 document.getElementById('form1').submit();  
	  }	  
	  
	   
   }
   
   function Limpiar(){
	  location.href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		   
   }
   
   function ModificarReceta(IdReceta){
	  location.href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ModificarReceta&IdReceta="+IdReceta+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		  
   }
   
   function EliminarReceta(IdReceta){
		//document.getElementById('form1').submit();
		alertify.confirm("Seguro de Eliminar la Receta "+IdReceta, function (e) {
			if (e) {							
				 location.href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=EliminarReceta&IdReceta="+IdReceta+"&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
			} 
		});	
	   
   }

</script>  
</body>

</html>
