<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	
	$IdReceta=$_REQUEST["IdReceta"];   
	$ListarRecetaCabecera=ListarRecetaCabeceraSeleccionarPorIdReceta($IdReceta);	
	$IdPaciente=$ListarRecetaCabecera[0]['IdPaciente'];	
	
	//DATOS CABECERA RECETA
	$FechaReceta=vfecha(substr($ListarRecetaCabecera[0]['FechaReceta'],0,10));
	$HoraReceta=substr($ListarRecetaCabecera[0]['FechaReceta'],11,8);
	$fechaVigencia=vfecha(substr($ListarRecetaCabecera[0]['fechaVigencia'],0,10));	
	$idMedicoReceta=$ListarRecetaCabecera[0]['idMedicoReceta'];	
	
	$IdPuntoCargaReg=$ListarRecetaCabecera[0]['IdPuntoCarga'];		
	$NombrePuntoCarga=$ListarRecetaCabecera[0]['NombrePuntoCarga'];	
	
	//DATOS ATENCION
	$IdCuentaAtencion=$ListarRecetaCabecera[0]['idCuentaAtencion'];
	$idFuenteFinanciamiento=$ListarRecetaCabecera[0]['idFuenteFinanciamiento'];
	$FechaIngreso = vfecha(substr($ListarRecetaCabecera[0]['FechaIngreso'],0,10));
	$Servicio=$ListarRecetaCabecera[0]['IdServicioIngreso'];
	$DescripcionServicio=$ListarRecetaCabecera[0]['DescripcionServicio'];
	$idTipoServicio=$ListarRecetaCabecera[0]['IdTipoServicio'];
		
	//Fuente Financiamiento
    $ObtenerDatosFuenteFinanciamiento=ObtenerDatosFuenteFinanciamiento_M($idFuenteFinanciamiento);
    $DescripcionFinanciamiento=$ObtenerDatosFuenteFinanciamiento[0]["Descripcion"];
    $IdTipoFinanciamiento=$ObtenerDatosFuenteFinanciamiento[0]["IdTipoFinanciamiento"]; 
	
	//DATOS PACIENTE
	$ObtenerDatosPacientes=ObtenerDatosPacientes_M($IdPaciente);		
	$FechaNacimiento = vfecha(substr($ObtenerDatosPacientes[0]['FechaNacimiento'],0,10));
	$NroHistoriaClinica=$ObtenerDatosPacientes[0]["NroHistoriaClinica"];	
	$Paciente=$ObtenerDatosPacientes[0]["ApellidoPaterno"].' '.$ObtenerDatosPacientes[0]["ApellidoMaterno"].', '.$ObtenerDatosPacientes[0]["PrimerNombre"].' '.$ObtenerDatosPacientes[0]["SegundoNombre"];
	
	//EDAD SIN TIPO EDAD (EN AÑOS)
	/*$FechaNacimientoX = time() - strtotime($ObtenerDatosPacientes[0]['FechaNacimiento']);
	$edad = floor((($FechaNacimientoX / 3600) / 24) / 360);*/
	$ObtenerDatosPacientes[0]['FechaNacimiento'];
	$EDAD=str_replace('<BR> A',' AÑOS',CalcularEdad($ObtenerDatosPacientes[0]['FechaNacimiento'])) ;
	$EDAD=str_replace('<BR> M',' MESES',$EDAD) ;
	$EDAD=str_replace('<BR> D',' DIAS',$EDAD) ;
	$EDAD=str_replace('<BR> H',' HORAS',$EDAD) ; 
	
   if($ObtenerDatosPacientes[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($ObtenerDatosPacientes[0]["IdTipoSexo"]==2){$Sexo='Femenino';}	
   $GrupoSanguineo=$ObtenerDatosPacientes[0]['GrupoSanguineo']; 
   
   //Obtener cantidad Producto   
   $dataDetalle=ListarRecetasDevuelveDatosDelDetalle($IdReceta,5);
   if($dataDetalle!=NULL){
	   foreach($dataDetalle as $r1){
		   $cantitemsProducto++;	   
	   }
   }	
	
	//Obtener cantidad Servicio 
	if($IdPuntoCargaReg=='5'){
		$IdPuntoCargaServicio='';
	}else{
		$IdPuntoCargaServicio=$IdPuntoCargaReg;
	}
	
	$dataDetalleServicio=ListarRecetasDevuelveDatosDelDetalle($IdReceta,$IdPuntoCargaServicio);
	if($dataDetalleServicio!=NULL){
		foreach($dataDetalleServicio as $r2){
			$cantitemsServicio++;	
		}
	}

   
	
?>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - RECETAS MÉDICAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">   

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--Alerts -->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
    
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/ListaVerificacion.css">   
    <!--NUEVO-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">-->
    
    <!--datepicker y autocompletado-->
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.css">
  	
	<style type="text/css">
		.tab {
		  float: left;
		}
		.tab label {
		  background: #eee; 
		  padding: 10px; 
		  border: 1px solid #ccc; 
		  margin-left: -1px; 
		  position: relative;
		  left: 1px; 
		}
		.tab [type=radio] {
		  display: none;   
		}
		
		[type=radio]:checked ~ label {
		  background: white;
		  border-bottom: 1px solid white;
		  z-index: 2;
		}    
	</style>
    
<!--GRILLA DETALLE Medicamento/Insumo-->
<script type="text/javascript">	
	
//var valor=<?php //echo $cantitemsProducto; ?>;	
//var posicionCampo=valor+1;
	
function addRowToTable()
{	
		var elem1 = document.getElementById('cantitemsProducto');
		if(typeof elem1 != 'undefined' && elem1 != null) {
			posicionCampo=parseInt(document.getElementById('cantitemsProducto').value)+1;
		}
		nuevaFila = document.getElementById("tblSample").insertRow(-1);
		nuevaFila.id=posicionCampo;
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td>" +posicionCampo+ "</td>"; 
		nuevaCelda.id="nro"+posicionCampo;
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='c_codprd"+posicionCampo+"' type='text' id='c_codprd"+posicionCampo+ "' size='5' readonly='readonly' class='form-control input-sm'/></td>"; 
		 
		nuevaCelda=nuevaFila.insertCell(-1); 
		nuevaCelda.innerHTML="<td> <input  name='c_desprd"+posicionCampo+"' type='text' id='c_desprd"+posicionCampo+ "' size='30' readonly='readonly' class='form-control input-sm'/></td>";
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='Precio"+posicionCampo+"' type='text'  id='Precio"+posicionCampo+"'  size='5' readonly='readonly' class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='CantidadPedida"+posicionCampo+"' type='text'  id='CantidadPedida"+posicionCampo+"'  size='3'  class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='IdProducto"+posicionCampo+"' type='hidden'  id='IdProducto"+posicionCampo+"'  size='2'  class='form-control input-sm' readonly='readonly'/></td>";
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <select name='idDosisRecetada"+posicionCampo+"' type='text'  id='idDosisRecetada"+posicionCampo+"' class='form-control input-sm'></select></td>";
		//nuevaCelda.innerHTML="<td> <input name='idDosisRecetada"+posicionCampo+"' readonly='readonly' type='text'  id='idDosisRecetada"+posicionCampo+"' class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <select name='IdViaAdministracion"+posicionCampo+"' id='IdViaAdministracion"+posicionCampo+"'  class='form-control input-sm'></select></td>";		
		/*nuevaCelda.innerHTML="<td> <input name='IdViaAdministracion"+posicionCampo+"' type='text'  id='IdViaAdministracion"+posicionCampo+"'  class='form-control input-sm'/></td>";*/
				
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='HaySaldo"+posicionCampo+"' type='checkbox'  id='HaySaldo"+posicionCampo+"'  size='4'  class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='Frecuencia"+posicionCampo+"' type='text'  id='Frecuencia"+posicionCampo+"'  size='10'  class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
        nuevaCelda.innerHTML="<td bgcolor='#CCCCCC'> <input value='Delete' type='button'  class='btn btn-danger btn-sm' onclick='eliminarProducto(this)'/></td>";		

		escribirdetalle(posicionCampo);
		posicionCampo++;
	
}

function escribirdetalle(posicionCampo)
{	
	c_codprd = document.getElementById("c_codprd" + posicionCampo);
	c_codprd.value = document.form1.c_codprd.value;
	
	c_desprd = document.getElementById("c_desprd" + posicionCampo);
	c_desprd.value = document.form1.c_desprd.value;
	
	Precio = document.getElementById("Precio" + posicionCampo);
	Precio.value = document.form1.Precio.value;			
	
	CantidadPedida = document.getElementById("CantidadPedida" + posicionCampo);
	CantidadPedida.value = document.form1.CantidadPedida.value;
	
	IdProducto = document.getElementById("IdProducto" + posicionCampo);
	IdProducto.value = document.form1.IdProducto.value;
	
	//idDosisRecetada = document.getElementById("idDosisRecetada" + posicionCampo);
	//idDosisRecetada.value = document.form1.idDosisRecetada.value;	
	idDosisRecetada = document.form1.idDosisRecetada.value;	
	var sDosis=document.getElementById('idDosisRecetada'+posicionCampo);
	
	$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarRecetaDosis", function(json){													
			$.each(json, function(i, obj){										
				//txtInpg.append($('<option>').text(obj.text).attr('value', obj.val));					
				var option = document.createElement("option");
				option.value =  obj.val;
				option.text = obj.text;	
				//selected								
				if(option.value == idDosisRecetada){
					option.setAttribute('selected', idDosisRecetada);	
				}	
				sDosis.appendChild(option);				
																
			});						
	});
	
	//IdViaAdministracion = document.getElementById("IdViaAdministracion" + posicionCampo);
	//IdViaAdministracion.value = document.form1.IdViaAdministracion.value;	
	IdViaAdministracion = document.form1.IdViaAdministracion.value;	
	var sVia=document.getElementById('IdViaAdministracion'+posicionCampo);
	
	$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarRecetaViaAdministracion", function(json){												
			$.each(json, function(i, obj){										
				//txtInpg.append($('<option>').text(obj.text).attr('value', obj.val));					
				var option = document.createElement("option");
				option.value =  obj.val;
				option.text = obj.text;	
				//selected								
				if(option.value == IdViaAdministracion){
					option.setAttribute('selected', IdViaAdministracion);	
				}	
				sVia.appendChild(option);				
																
			});						
	});
		
	
	if(document.getElementById('HaySaldo').checked == true){
		document.getElementById("HaySaldo" + posicionCampo).checked=true;
	}
	
	Frecuencia = document.getElementById("Frecuencia" + posicionCampo);
	Frecuencia.value = document.form1.Frecuencia.value;	
	
	$("#nro"+ posicionCampo).html(posicionCampo);
	document.getElementById('cantitemsProducto').value=	posicionCampo;	
	//imp = document.getElementById("imp" + posicionCampo);
	//imp.value = parseFloat(document.form1.imp.value).toFixed(2);
	
}


function eliminarProducto(obj){

    var oTr = obj;
    while(oTr.nodeName.toLowerCase()!='tr'){
    oTr=oTr.parentNode;
 	}
    var root = oTr.parentNode;		
	//var tbl = oTr.parentNode.parentNode;
	var rIndex = oTr.sectionRowIndex;
	var indexDetalleTable=parseInt(rIndex)+1;	
	//reordenar			
	reorderRows(indexDetalleTable);	
	alert("Registro "+indexDetalleTable+" Eliminado"); //si elimino fila 2 obtiene 2, /si elimino fila 3 obtiene 3	
	//eliminar
	root.removeChild(oTr);
	//disminuir 1 al maximo numero de item
	cantitemsProducto=document.getElementById('cantitemsProducto').value;
	document.getElementById('cantitemsProducto').value=cantitemsProducto-1;
}

function reorderRows(startingIndex)  //startingIndex=1,2,3,4
{
	var theTable = document.getElementById('tblSample');
	cantFilas = theTable.rows.length;		
	//if (startingIndex>1) { //2,3,4
		var count = startingIndex + 1; //2,3,4	
		for (var k=count; k<cantFilas; k++) {
							
			$("#nro"+ k).html(k-1);
			$("#nro"+ k).attr("id","nro"+(k-1));	
							
			document.getElementById("c_codprd"+ k).name="c_codprd"+ (k-1);	
			$("#c_codprd"+ k).attr("id","c_codprd"+(k-1));						
			document.getElementById("c_desprd"+ k).name="c_desprd"+ (k-1);	
			$("#c_desprd"+ k).attr("id","c_desprd"+(k-1));	
			
			document.getElementById("Precio"+ k).name="Precio"+ (k-1);	
			$("#Precio"+ k).attr("id","Precio"+(k-1));				
			document.getElementById("CantidadPedida"+ k).name="CantidadPedida"+ (k-1);	
			$("#CantidadPedida"+ k).attr("id","CantidadPedida"+(k-1));				
			document.getElementById("IdProducto"+ k).name="IdProducto"+ (k-1);	
			$("#IdProducto"+ k).attr("id","IdProducto"+(k-1));			
			document.getElementById("idDosisRecetada"+ k).name="idDosisRecetada"+ (k-1);	
			$("#idDosisRecetada"+ k).attr("id","idDosisRecetada"+(k-1));			
			document.getElementById("IdViaAdministracion"+ k).name="IdViaAdministracion"+ (k-1);	
			$("#IdViaAdministracion"+ k).attr("id","IdViaAdministracion"+(k-1));			
			document.getElementById("HaySaldo"+ k).name="HaySaldo"+ (k-1);	
			$("#HaySaldo"+ k).attr("id","HaySaldo"+(k-1));			
			document.getElementById("Frecuencia"+ k).name="Frecuencia"+ (k-1);	
			$("#Frecuencia"+ k).attr("id","Frecuencia"+(k-1));
															
			//count++;				
		}
	//}//end if
	//document.getElementById("c_codprd2").name="c_codprd1";	
}

function agregar(){
	
	    var theTable = document.getElementById('tblSample');
		cantFilas = theTable.rows.length;//a partir de cantFilas==2 la tabla esta llena
		if(cantFilas>1){
			for(i=1;i<cantFilas;i++){
				if((document.form1.c_desprd.value)==document.getElementsByName("c_desprd"+i)[0].value){					
					//mensje = "Producto Seleccionado ya está en el Registro";
					mensje = "Producto "+(document.form1.c_desprdS.value)+" se repite en el Registro";
					alert(mensje);
					return 0;
				}				
			}
		}//end if
	
	    if((document.form1.c_codprd.value)==""){
			mensje = "Falta Seleccionar un Medicamento/Insumo";
			alert(mensje);
			document.getElementById("descripcion").focus();			
		}else if((document.form1.CantidadPedida.value)=="0" || (document.form1.CantidadPedida.value)==""){
			mensje = "Falta Ingresar Cantidad";
			alert(mensje);	
		}else{        
			addRowToTable();		
			limpiar();			
		}	
	}
	
function limpiar(){
	//document.form1.c_codprd.value='';
	//document.form1.c_desprd.value='';
	$("#c_codprd").val('');
	$("#c_desprd").val('');	
	$("#CantidadPedida").val('1');		
	$("#IdProducto").val('');
	$("#IdViaAdministracion").val('');
	$("#idDosisRecetada").val('');					
	$("#Precio").val('');		
	$("#descripcion").val('');
	$("#Frecuencia").val('');	
}
	
</script>
<!--FIN GRILLA Medicamento/Insumo HASTA Linea 467--> 


<!--GRILLA DETALLE Procedimiento-->

<script type="text/javascript">
		

function addRowToTableS()
{
	var elem1 = document.getElementById('cantitemsServicio');
		if(typeof elem1 != 'undefined' && elem1 != null) {
			posicionCampo=parseInt(document.getElementById('cantitemsServicio').value)+1;
		}
		nuevaFila = document.getElementById("tblSampleServicio").insertRow(-1);
		nuevaFila.id=posicionCampo;
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td>" +posicionCampo+ "</td>"; 
		nuevaCelda.id="nroS"+posicionCampo;
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='c_codprdS"+posicionCampo+"' type='text' id='c_codprdS"+posicionCampo+ "' size='5' readonly='readonly' class='form-control input-sm'/></td>"; 
		 
		nuevaCelda=nuevaFila.insertCell(-1); 
		nuevaCelda.innerHTML="<td> <input  name='c_desprdS"+posicionCampo+"' type='text' id='c_desprdS"+posicionCampo+ "' size='30' readonly='readonly' class='form-control input-sm'/></td>";
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='PrecioS"+posicionCampo+"' type='text'  id='PrecioS"+posicionCampo+"'  size='5' readonly='readonly' class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='CantidadPedidaS"+posicionCampo+"' type='text'  id='CantidadPedidaS"+posicionCampo+"'  size='3'  class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='IdProductoS"+posicionCampo+"' type='hidden'  id='IdProductoS"+posicionCampo+"'  size='2'  class='form-control input-sm' readonly='readonly'/></td>";
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='IdPuntoCarga"+posicionCampo+"' type='hidden'  id='IdPuntoCarga"+posicionCampo+"' size='2' class='form-control input-sm' /></td>";				
		
		/*nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <select name='IdViaAdministracion"+posicionCampo+"' id='IdViaAdministracion"+posicionCampo+"'  class='form-control input-sm'></select></td>";*/		
						
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='HaySaldoS"+posicionCampo+"' type='checkbox'  id='HaySaldoS"+posicionCampo+"'  size='4'  class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
		nuevaCelda.innerHTML="<td> <input name='FrecuenciaS"+posicionCampo+"' type='text'  id='FrecuenciaS"+posicionCampo+"'  size='10'  class='form-control input-sm'/></td>";		
		
		nuevaCelda=nuevaFila.insertCell(-1);
        nuevaCelda.innerHTML="<td bgcolor='#CCCCCC'> <input value='Delete' type='button'  class='btn btn-danger btn-sm' onclick='eliminarServicio(this)'/></td>";		

		escribirdetalleS(posicionCampo);
		posicionCampo++;
	
}

function escribirdetalleS(posicionCampo)
{	
	c_codprdS = document.getElementById("c_codprdS" + posicionCampo);
	c_codprdS.value = document.form1.c_codprdS.value;
	
	c_desprdS = document.getElementById("c_desprdS" + posicionCampo);
	c_desprdS.value = document.form1.c_desprdS.value;
	
	PrecioS = document.getElementById("PrecioS" + posicionCampo);
	PrecioS.value = document.form1.PrecioS.value;			
	
	CantidadPedidaS = document.getElementById("CantidadPedidaS" + posicionCampo);
	CantidadPedidaS.value = document.form1.CantidadPedidaS.value;
	
	IdProductoS = document.getElementById("IdProductoS" + posicionCampo);
	IdProductoS.value = document.form1.IdProductoS.value;
	
	/*IdPuntoCarga = document.getElementById("IdPuntoCarga" + posicionCampo);
	IdPuntoCarga.value = document.form1.IdPuntoCarga.value;*/	
	var IdPuntoCargas = document.getElementsByName('IdPuntoCargaSel');
	var len = IdPuntoCargas.length;
	for (var r=1; r<=len; r++) {
		if(document.getElementById('IdPuntoCargaSel'+r).checked){
			var IdPuntoCargaF=document.getElementById('IdPuntoCargaSel'+r).value;				
		}			
	}		
	IdPuntoCargaInput = document.getElementById("IdPuntoCarga" + posicionCampo);
	IdPuntoCargaInput.value = IdPuntoCargaF;
	
	if(document.getElementById('HaySaldoS').checked == true){
		document.getElementById("HaySaldoS" + posicionCampo).checked=true;
	}
	
	FrecuenciaS = document.getElementById("FrecuenciaS" + posicionCampo);
	FrecuenciaS.value = document.form1.observaciones.value;	
	
	$("#nroS"+ posicionCampo).html(posicionCampo);
	document.getElementById('cantitemsServicio').value=	posicionCampo;	
	//imp = document.getElementById("imp" + posicionCampo);
	//imp.value = parseFloat(document.form1.imp.value).toFixed(2);
	
}


function eliminarServicio(obj){

    var oTr = obj;
    while(oTr.nodeName.toLowerCase()!='tr'){
    oTr=oTr.parentNode;
 	}
    var root = oTr.parentNode;		
	//var tbl = oTr.parentNode.parentNode;
	var rIndex = oTr.sectionRowIndex;
	var indexDetalleTable=parseInt(rIndex)+1;	
	//reordenar			
	reorderRowsS(indexDetalleTable);	
	alert("Registro "+indexDetalleTable+" Eliminado"); //si elimino fila 2 obtiene 2, /si elimino fila 3 obtiene 3	
	//eliminar
	root.removeChild(oTr);
	//disminuir 1 al maximo numero de item
	cantitemsServicio=document.getElementById('cantitemsServicio').value;
	document.getElementById('cantitemsServicio').value=cantitemsServicio-1;
}

function reorderRowsS(startingIndex)  //startingIndex=1,2,3,4
{
	var theTable = document.getElementById('tblSampleServicio');
	cantFilas = theTable.rows.length;		
	//if (startingIndex>1) { //2,3,4
		var count = startingIndex + 1; //2,3,4	
		for (var k=count; k<cantFilas; k++) {
							
			$("#nroS"+ k).html(k-1);
			$("#nroS"+ k).attr("id","nroS"+(k-1));	
							
			document.getElementById("c_codprdS"+ k).name="c_codprdS"+ (k-1);	
			$("#c_codprdS"+ k).attr("id","c_codprdS"+(k-1));						
			document.getElementById("c_desprdS"+ k).name="c_desprdS"+ (k-1);	
			$("#c_desprdS"+ k).attr("id","c_desprdS"+(k-1));	
			
			document.getElementById("PrecioS"+ k).name="PrecioS"+ (k-1);	
			$("#PrecioS"+ k).attr("id","PrecioS"+(k-1));				
			document.getElementById("CantidadPedidaS"+ k).name="CantidadPedidaS"+ (k-1);	
			$("#CantidadPedidaS"+ k).attr("id","CantidadPedidaS"+(k-1));				
			document.getElementById("IdProductoS"+ k).name="IdProductoS"+ (k-1);	
			$("#IdProductoS"+ k).attr("id","IdProductoS"+(k-1));
									
			document.getElementById("IdPuntoCarga"+ k).name="IdPuntoCarga"+ (k-1);	
			$("#IdPuntoCarga"+ k).attr("id","IdPuntoCarga"+(k-1));						
			//document.getElementById("IdViaAdministracion"+ k).name="IdViaAdministracion"+ (k-1);	
			//$("#IdViaAdministracion"+ k).attr("id","IdViaAdministracion"+(k-1));	
					
			document.getElementById("HaySaldoS"+ k).name="HaySaldoS"+ (k-1);	
			$("#HaySaldoS"+ k).attr("id","HaySaldoS"+(k-1));			
			document.getElementById("FrecuenciaS"+ k).name="FrecuenciaS"+ (k-1);	
			$("#FrecuenciaS"+ k).attr("id","FrecuenciaS"+(k-1));
															
			//count++;				
		}
	//}//end if
	//document.getElementById("c_codprd2").name="c_codprd1";	
}


function agregarServicio(){
	
	  var theTable = document.getElementById('tblSampleServicio');
		cantFilas = theTable.rows.length;//a partir de cantFilas==2 la tabla esta llena
		if(cantFilas>1){
			for(i=1;i<cantFilas;i++){
				if((document.form1.c_desprdS.value)==document.getElementsByName("c_desprdS"+i)[0].value){					
					//mensje = "Servicio Seleccionado ya está en el Registro";
					mensje = "Servicio "+(document.form1.c_desprdS.value)+" se repite en el Registro";
					alert(mensje);
					return 0;
				}				
			}
		}//end if		
		
		var IdPuntoCargaSel = document.getElementsByName('IdPuntoCargaSel');
		var len = IdPuntoCargaSel.length;
		for (var i=1; i<=len; i++) {
			if(document.getElementById('IdPuntoCargaSel'+i).checked){
				var IdPuntoCargaSel=document.getElementById('IdPuntoCargaSel'+i).value;				
			}			
		}
		//alert(IdPuntoCargaSel);		
		
	    if(IdPuntoCargaSel=="[object NodeList]"){
			mensje = "Falta Seleccionar un Punto de Carga";
			alert(mensje);
			//document.getElementById("descripcionS").focus();			
		}else if((document.form1.c_codprdS.value)==""){
			mensje = "Falta Seleccionar un Servicio";
			alert(mensje);
			document.getElementById("descripcionS").focus();			
		}else if((document.form1.CantidadPedidaS.value)=="0" || (document.form1.CantidadPedidaS.value)==""){
			mensje = "Falta Ingresar Cantidad";
			alert(mensje);	
		}else{        
			addRowToTableS();		
			limpiarS();			
		}	
	}
	
function limpiarS(){
	//document.form1.c_codprd.value='';
	//document.form1.c_desprd.value='';
	$("#c_codprdS").val('');
	$("#c_desprdS").val('');	
	$("#CantidadPedidaS").val('1');		
	$("#IdProductoS").val('');
		
	var IdPuntoCargaSel = document.getElementsByName('IdPuntoCargaSel');
	var len = IdPuntoCargaSel.length;
	for (var i=1; i<=len; i++) {
		document.getElementById('IdPuntoCargaSel'+i).checked=false;				
	}	
	//$("#IdViaAdministracion").val('');
	//$("#idDosisRecetada").val('');					
	$("#PrecioS").val('');		
	$("#descripcionS").val('');
	$("#observaciones").val('');
		
}
	
</script>
<!--FIN GRILLA Servicio--> 

<script type="text/javascript">

function llevarViaAdministracion(){
	$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarRecetaViaAdministracion", function(json){												
				$.each(json, function(i, obj){										
					$('#IdViaAdministracion1').append($('<option>').text(obj.text).attr('value', obj.val));													
				});						
	});	
}

//validar numeros
 function validaDecimal(e){	 //solo acepta numeros y punto 
	tecla = (document.all) ? e.keyCode : e.which;//obtenemos el codigo ascii de la tecla
	if (tecla==8) return true;//backspace en ascii es 8
	patron=/[0-9\.]/; 
	te = String.fromCharCode(tecla);//convertimos el codigo ascii a string
	return patron.test(te);
} 

/*function validarnumero(){		
	var CantidadPedida=document.getElementById('CantidadPedida').value;
	var patron=/^\d+(\.\d{1,2})?$/;
		if(!patron.test(CantidadPedida)){		
		//window.alert('monto ingresado incorrecto');
		document.getElementById('CantidadPedida').value='';
		document.getElementById('CantidadPedida').focus();
		return false;
		}		
}*/

</script>
</head>

<body>

<!--modal de ver productos-->
<div class="modal fade" id="my_modalProd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<form class="form-horizontal" id="frmproducto" name="frmproducto" action="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">      
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="exampleModalLabel">Medicamentos e Insumos</h4>
            
            <div class="col-xs-6">            
                <input id="codigo" name="codigo" placeholder="Busque por Codigo de Producto" onKeyUp="abrirmodalProd2()" type="text" class="form-control input-sm" />           	
            </div> 
            
            <div class="col-xs-6">            
                <input type="text" id="criterio" name="criterio" placeholder="Busque por Descripcion de Producto" onKeyUp="abrirmodalProd2()"  class="form-control input-sm" />          	
            </div>     
        </div>
        
      	<div class="modal-body">
            <table id="tablaProd" class="table table-hover" style="font-size:12px;">
        		<!--Contenido se encuentra en verProductos.php-->
            </table> 
        </div>
      </div>
    </div>
    </form>
  </div>
 <!--fin modal de ver productos-->
 
 <!--modal de ver servicios-->
<div class="modal fade" id="my_modalServ" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<form class="form-horizontal" id="frmservicio" name="frmservicio" action="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">      
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="exampleModalLabel">Servicios</h4>
            
            <div class="col-xs-6">            
                <input id="codigo" name="codigo" placeholder="Busque por Codigo de Servicio" onKeyUp="abrirmodalServ2()" type="text" class="form-control input-sm" />           	
            </div> 
            
            <div class="col-xs-6">            
                <input type="text" id="criterio" name="criterio" placeholder="Busque por Descripcion de Servicio" onKeyUp="abrirmodalServ2()"  class="form-control input-sm" />          	
            </div>     
        </div>
        
      	<div class="modal-body">
            <table id="tablaServ" class="table table-hover" style="font-size:12px;">
        		<!--Contenido se encuentra en verServicios.php-->
            </table> 
        </div>
      </div>
    </div>
    </form>
  </div>
 <!--fin modal de ver servicios-->
 
<!--modal de ver paquetes-->
<div class="modal fade" id="my_modalPaque" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<form class="form-horizontal" id="frmpaquete" name="frmpaquete" action="#">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">      
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="exampleModalLabel">Paquetes</h4>
            
            <div class="col-xs-6">            
                <input id="codigo" name="codigo" placeholder="Busque por Codigo de Paquete" onKeyUp="abrirmodalPaque2()" type="text" class="form-control input-sm" />           	
            </div> 
            
            <div class="col-xs-6">            
                <input type="text" id="criterio" name="criterio" placeholder="Busque por Descripcion de Paquete" onKeyUp="abrirmodalPaque2()"  class="form-control input-sm" />          	
            </div>     
        </div>
        
      	<div class="modal-body">
            <div id="tablaPaque">
        		<!--Contenido se encuentra en verPaquetes.php-->
            </div> 
        </div>
      </div>
    </div>
    </form>
  </div>
 <!--fin modal de ver paquetes-->

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">				
				<?php  include('../../MVC_Vista/RecetasMedicas/Cabecera.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">MODIFICAR RECETAS MÉDICAS</h3>
                </div>                
            </div>
            <!-- /.row -->
            
            <div class="row">
            
                <div class="container-fluid">               
                   <div class="col-md-12">
                            <div class="panel panel-primary with-nav-tabs"> <!--with-nav-tabs--> 
                            	<!--<div class="panel-heading">REGISTRAR RECETA</div>-->
                                <div class="panel-heading"  >
                                        <ul class="nav nav-tabs">
                                        	<li class="active"><a href="#tabcabecera" data-toggle="tab">Cabecera</a></li>
                                            <li class="desabilitar"><a href="#tab1primary" data-toggle="tab">Farmacia</a></li>
                                            <li class = "desabilitar"><a href="#tab2primary" data-toggle="tab">Servicio</a></li>
                                            <li class = "desabilitar"><a href="#tab3primary" data-toggle="tab">Dieta</a></li>
                                            <!--<li class = "desabilitar"><a href="#tab4primary" data-toggle="tab">Salida</a></li>-->         
                                        </ul>
                                </div>
                                <div class="panel-body">
                                	<form class="form-horizontal" id="form1" name="form1" method="post" action="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=GuardarUpdReceta&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  >
                                       <div class="form-control-static" align="right">
                                         <input class="btn btn-success" type="button" onClick="validarguardar()" value="Modificar"/>
                                         &nbsp;<a class="btn btn-danger" onClick="cancelar();">Cancelar</a>
                                         <!--&nbsp;<a class="btn btn-warning" onClick="location.reload();" >Refrescar</a>&nbsp;
                                         &nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                                        </div>
                                  <div class="tab-content">                                  		
                                  <!--Inicio Identificacion-->
                                        <div class="tab-pane fade in active" id="tabcabecera">                                     
                                                <fieldset>
					  							<legend>Datos Paciente</legend>
                                                    <!--fila 1-->
                                                    <div class="form-group">
                                                    
                                                    	<input name="NombrePuntoCarga" id="NombrePuntoCarga" type="hidden" value="<?php echo $NombrePuntoCarga ?>" />
                                                        <input name="IdPuntoCargaReg" id="IdPuntoCargaReg" type="hidden" value="<?php echo $IdPuntoCargaReg ?>" />
                                                        
                                                        <input name="IdReceta" id="IdReceta" type="hidden" value="<?php echo $IdReceta ?>" />
                                                    	
                                                        <input name="Servicio" id="Servicio" type="hidden" value="<?php echo $Servicio ?>" />
                                                        <input name="idTipoServicio" id="idTipoServicio" type="hidden" value="<?php echo $idTipoServicio ?>" />                                                                                                         
                                                        <input name="IdMedico" id="IdMedico" type="hidden" value="<?php echo $idMedicoReceta; ?>" /> 
                                                        
                                                        <input name="IdPaciente" id="IdPaciente" type="hidden" value="<?php echo $IdPaciente ?>" />  
                                                        
                                                        <input name="NroHistoriaClinica" id="NroHistoriaClinica" type="hidden" value="<?php echo $NroHistoriaClinica ?>" />                                                   		<input name="Paciente" id="Paciente" type="hidden" value="<?php echo $Paciente ?>" />                                                        <input name="EDAD" id="EDAD" type="hidden" value="<?php echo $EDAD ?>" />
                                                        
                                                        <input name="nombrePC" id="nombrePC" type="hidden" value="<?php $ip = $_SERVER['REMOTE_ADDR']; echo $hostname = gethostbyaddr($ip); ?>" />
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres:</label>
                                                        <div class="col-xs-2">
                                                        	<div class="input-sm"> <?php echo $Paciente ?> </div>                                                                            				
                                                        </div>
                                                            
                                                        <label class="control-label col-xs-1">Fecha Nac.:</label>
                                                        <div class="col-xs-1">  
                                                        	<div class="input-sm"> <?php echo $FechaNacimiento ?> </div>
                                                       	</div>                     
                                                        
                                                        <label class="control-label col-xs-1">Edad:</label>
                                                        <div class="col-xs-1"> 
                                                        	<div class="input-sm"> <?php echo $EDAD;  ?> </div>
                                                        </div> 
                                                        
                                                        <label class="control-label col-xs-1">Sexo</label>
                                                        <div class="col-xs-1"> 
                                                        	<div class="input-sm"> <?php echo $Sexo ?> </div>                                                        
                                                        </div> 
                                                        <?php if($GrupoSanguineo!=NULL){ ?>
                                                        <label class="control-label col-xs-1">Grupo Sang.:</label>
                                                        <div class="col-xs-1">
                                                        	<div class="input-sm"> <?php echo $GrupoSanguineo ?> </div>
                                                        </div>                             
                                                        <?php } ?>  
                                                    </div> 
                                                    <!--FIN fila 1-->            
                                                                                                     
                                                                                                      
                                                  </fieldset>
                                                  
                                                  <fieldset>
					  							  <legend>Datos Atención Paciente</legend>                                    
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">   
                                                    	<label class="control-label col-xs-2">Nro Cuenta:</label>
                                                        <div class="col-xs-1">
                                                        	<div class="input-sm"> 
																<?php echo $IdCuentaAtencion ?> 
                                                            	<input name="idCuentaAtencion" id="idCuentaAtencion" value="<?php echo $IdCuentaAtencion ?>" type="hidden">
                                                            </div>
                                                        </div>  
                                                        <label class="control-label col-xs-1">Financiamiento:</label>
                                                        <div class="col-xs-1">
                                                        	<div class="input-sm"> <?php echo $DescripcionFinanciamiento ?> </div>
                                                        </div>                                                  	
                                                        <label class="control-label col-xs-1">Servicio:</label>
                                                        <div class="col-xs-2"> 
                                                        	<div class="input-sm"> <?php echo $DescripcionServicio ?> </div> 
                                                        </div>                                                               
                                                        <label class="control-label col-xs-1">Fecha Ingreso:</label>
                                                        <div class="col-xs-1"> 
                                                        	<div class="input-sm"> <?php echo $FechaIngreso ?> </div>  
                                                        </div>                              
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>   
                                                  
                                                  <fieldset>
					  							  <legend>Datos Cabecera Receta</legend>                                    
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">                                                    	
                                                        <label class="control-label col-xs-2">Fecha Receta:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" name="FechaReceta" id="FechaReceta" value="<?php echo $FechaReceta; ?>" />      
                                                        </div>
                                                        <div class="col-xs-1"> 
                                                          <input type="text" class="form-control input-sm" name="HoraReceta" id="HoraReceta" value="<?php echo $HoraReceta; ?>" />                                                 		</div>  
                                                             
                                                        <label class="control-label col-xs-1">Fecha Vigencia:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" name="fechaVigencia" id="fechaVigencia" value="<?php echo $fechaVigencia; ?>" />                                                      	
                                                        </div>   
                                                        <label class="control-label col-xs-1">Punto Carga:</label>
                                                        <div class="col-xs-2">  
                                                            <div class="input-sm"> <?php echo $NombrePuntoCarga ?> </div>                                                       	
                                                        </div>                            
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>   
                                                  
                                                  <!--<fieldset>
					  							  <legend>Búsqueda de Paquetes</legend>
					  							  <div class="col-xs-3">
					  							    <input type="text" class="form-control input-sm" name="NombrePaquete" id="NombrePaquete" onFocus="abrirmodalPaque();" placeholder="Nombre del Paquete" readonly />
					  							    </div>                                    
                                                    
                                                    
                                                    <div class="form-group">                                                    	
                                                        <label class="control-label col-xs-2">Lista de Paquetes:</label>
                                                        <label class="control-label col-xs-1">Codigo:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" name="CodigoPaquete" id="CodigoPaquete" readonly />                                                      		<input type="hidden" name="idFactPaquete" id="idFactPaquete" />
                                                        </div>                                     
                                                                     
                                                          
                                                    </div> 
                                                                                                     
                                                                                                      
                                                  </fieldset> -->                                             
                                                 
                                                                                      
                                        
                                        </div>
                                   		<!--tabcabecera-->
                                        
                                    <!--Inicio tab1primary-->
                                        <div class="tab-pane fade" id="tab1primary">                      
                                               
                                                	<!--fila 1-->
                                                    <div class="form-group">                                               
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres Paciente:</label>
                                                        <div class="col-xs-3">
                                                            <div class="input-sm"> <?php echo $Paciente ?> </div>                                                       
                                                        </div>                            
                                                          
                                                    </div> 
                                                    <!--FIN fila 1-->  
                                                                                                  
                                                  <fieldset>
					  							  <legend>Detalle Farmacia</legend>
                                                  
                                                     <div class="row">                                                       
                                                        <div class="col-xs-3">
                                                        <label class="control-label col-xs-3">Medicamento/Insumo(min.4Letras)</label>
                                                        </div>
                                                        <div class="col-xs-1"> 
                                                        <label class="control-label col-xs-1">Codigo</label>
                                                        </div>
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">PrecioUnit</label>
                                                        </div> 
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Cant</label>
                                                        </div>  
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">N°Dosis</label>
                                                        </div>                                                                                                              
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Via</label>
                                                        </div>
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">HaySaldo</label>
                                                        </div>
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-2">Frecuencia</label>
                                                        </div>                                                                         
                                                   </div> 
                       
                                                   <div class="row">                         
                                                       <div class="col-xs-3">                                                                                                           
                                                        <input  id="c_desprd" name="c_desprd"  type="hidden" />
                                                        <input autocomplete="off" id="descripcion" name="descripcion" class="form-control input-sm" type="text" placeholder="Nombre del producto" /> <!--readonly onFocus="abrirmodalProd();"-->
                                                    </div>
                                                    <div class="col-xs-1">
                                                    	<input id="c_codprd" name="c_codprd" type="text" class="form-control input-sm" /> <!--readonly-->
                                                        <input name="IdProducto" type="hidden" class="form-control input-sm"  id="IdProducto" />                                                         
                                                    </div>                                                    
                                                    <div class="col-xs-1">                                                       
                                                        <input id="Precio" name="Precio" type="text" class="form-control input-sm" />         	
                                                    </div> 
                                                    <div class="col-xs-1">
                                                        <input name="CantidadPedida" type="text" class="form-control input-sm"  id="CantidadPedida" value="1" onKeyPress="return validaDecimal(event)" />  <!--onChange="validarnumero()"-->
                                                      
                                                    </div>  
                                                    <div class="col-xs-1">                                                     
                                                         <select id="idDosisRecetada" name="idDosisRecetada" class="form-control input-sm" >                                                            	<?php 
                                                                    $resultados=ListarRecetaDosis_M();								 
                                                                    if($resultados!=NULL){
                                                                    for ($i=0; $i < count($resultados); $i++) {	
                                                                 ?>
                                                             <option value="<?php echo $resultados[$i]["idDosis"] ?>"  ><?php echo mb_strtoupper($resultados[$i]["NumeroDosis"]) ?></option>
                                                             <?php 
                                                                }}
                                                             ?>                   
                                                        </select>        	
                                                    </div>                                                   
                                                    <div class="col-xs-1">
                                                    	<select id="IdViaAdministracion" name="IdViaAdministracion" class="form-control input-sm" >                                                            	<?php 
                                                                    $resultados=ListarRecetaViaAdministracion_M();								 
                                                                    if($resultados!=NULL){
                                                                    for ($i=0; $i < count($resultados); $i++) {	
                                                                 ?>
                                                             <option value="<?php echo $resultados[$i]["IdViaAdministracion"] ?>"  ><?php echo $resultados[$i]["IdViaAdministracion"].' | '.mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
                                                             <?php 
                                                                }}
                                                             ?>                   
                                                        </select>                                                        
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <input name="HaySaldo" id="HaySaldo" type="checkbox" class="form-control input-sm" value="1" /> 
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input name="Frecuencia" id="Frecuencia" type="text" class="form-control input-sm" /> 
                                                    </div>
                                                                       
                                                    <div class="col-xs-1">
                                                        <button class="btn btn-success btn-sm" id="btn-agregar" 
                                                        type="button" onClick="agregar();">
                                                             <i class="glyphicon glyphicon-plus"></i>
                                                        </button>                                                        
                                                    </div>
                                                    
                                                </div> 
                                                
                                                <hr />
                                                <table  id="tblSample" class="table table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>Codigo</th>
                                                      <th>Medicamento/Insumo</th> 
                                                      <th>Precio Unit.</th>  
                                                      <th>Cantidad</th> 
                                                      <th></th>     
                                                      <th>N°Dosis</th>       
                                                      <th>Via</th> 
                                                      <th>HaySaldo</th>       
                                                      <th>Frecuencia</th>   
                                                      <th>Delete</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                  	<?php 
													$dataDetalle=ListarRecetasDevuelveDatosDelDetalle($IdReceta,5);
													for ($j=0; $j < count($dataDetalle); $j++) { 															
													?>
												<tr>
												  <td id="<?php echo "nro".($j+1) ?>"><?php echo ($j+1) ?></td>											
												  <td>												  
												  <input type="text" name="<?php echo "c_codprd".($j+1); ?>" id="<?php echo "c_codprd".($j+1); ?>" size="5" value="<?php echo $dataDetalle[$j]['Codigo'] ?>"    class="form-control input-sm" readonly /></td>
                                                  
												  <td><input type="text" name="<?php echo "c_desprd".($j+1); ?>" id="<?php echo "c_desprd".($j+1); ?>" size="30" value="<?php echo $dataDetalle[$j]['Producto'] ?>" class="form-control input-sm" readonly /></td>
                                                  
												  <td><input type="text" name="<?php echo "Precio".($j+1); ?>" id="<?php echo "Precio".($j+1); ?>" size="5" value="<?php $Precio=number_format( $dataDetalle[$j]['Precio'] , 2 , "." , " " ); echo $Precio; ?>" class="form-control input-sm" readonly /></td>
												  
                                                  <td><input type="text" name="<?php echo "CantidadPedida".($j+1); ?>" id="<?php echo "CantidadPedida".($j+1); ?>" size="3" value="<?php echo $dataDetalle[$j]['CantidadPedida'] ?>" class="form-control input-sm" /></td>
                                                  
                                                  <td><input type="hidden" name="<?php echo "IdProducto".($j+1); ?>" id="<?php echo "IdProducto".($j+1); ?>" size="2" value="<?php echo $dataDetalle[$j]['idItem'] ?>" class="form-control input-sm" readonly /></td>
                                                  
                                                  <td> <select id="<?php echo "idDosisRecetada".($j+1); ?>" name="<?php echo "idDosisRecetada".($j+1); ?>" class="form-control input-sm" >                                                            	<?php 
                                                                    $resultados=ListarRecetaDosis_M();								 
                                                                    if($resultados!=NULL){
                                                                    for ($i=0; $i < count($resultados); $i++) {	
                                                                 ?>
                                                             <option value="<?php echo $resultados[$i]["idDosis"] ?>" <?php if($resultados[$i]["idDosis"]==$dataDetalle[$j]['idDosisRecetada']){?> selected <?php } ?> ><?php echo mb_strtoupper($resultados[$i]["NumeroDosis"]) ?></option>
                                                             <?php 
                                                                }}
                                                             ?>                   
                                                        </select>  </td>
                                                  
                                                  <td>	<select id="<?php echo "IdViaAdministracion".($j+1); ?>" name="<?php echo "IdViaAdministracion".($j+1); ?>" class="form-control input-sm" >                                                            	<?php 
                                                                    $resultados=ListarRecetaViaAdministracion_M();								 
                                                                    if($resultados!=NULL){
                                                                    for ($i=0; $i < count($resultados); $i++) {	
                                                                 ?>
                                                             <option value="<?php echo $resultados[$i]["IdViaAdministracion"] ?>" <?php if($resultados[$i]["IdViaAdministracion"]==$dataDetalle[$j]['IdViaAdministracion']){?> selected <?php } ?> ><?php echo $resultados[$i]["IdViaAdministracion"].' | '.mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
                                                             <?php 
                                                                }}
                                                             ?>                   
                                                        </select>   </td>
                                                  
                                                  <td><input name="<?php echo "HaySaldo".($j+1); ?>" id="<?php echo "HaySaldo".($j+1); ?>" size="4" value="1" class="form-control input-sm" type="checkbox"></td>
                                                  
                                                  <td><input type="text" name="<?php echo "Frecuencia".($j+1); ?>" id="<?php echo "Frecuencia".($j+1); ?>" size="10" value="<?php echo $dataDetalle[$j]['observaciones'] ?>" class="form-control input-sm" /></td>                               
                                                 
												  <td><input type="button" name="button3" id="button3" value="delete"  class="btn btn-danger btn-sm" onClick="eliminarProducto(this)" /></td>
												</tr>
                                                
												<?php } ?>                                                	
                                                 
                                                  </tbody> <input name="cantitemsProducto" id="cantitemsProducto" type="hidden" value="<?php echo $j ?>">
                                                </table>                               
                                                                                                      
                                              </fieldset>
                                                                                                                                     
                                        
                                        </div>
                                   		<!--tab1primary-->
                                        
                                        <div class="tab-pane fade" id="tab2primary"> 
                                        	<!--fila 1-->
                                                    <div class="form-group">                                               
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres Paciente:</label>
                                                        <div class="col-xs-3">
                                                            <div class="input-sm"> <?php echo $Paciente ?> </div>                                                       
                                                        </div>                            
                                                          
                                                    </div> 
                                                    <!--FIN fila 1-->                                                       
   													
                                                    <label class="control-label col-xs-2">Punto de Carga del Servicio:</label>
														<?php 
                                                            $resultados=ListarPuntosCargaServicios_M();								 
                                                                if($resultados!=NULL){
                                                                for ($i=0; $i < count($resultados); $i++) {	
                                                                if(trim($resultados[$i]["Descripcion"])=='Pro.Ecografia.01 (CE)'){
                                                                    $DescripcionPuntosCarga='Ecografía Obstetrica';
                                                                }else{
                                                                    $DescripcionPuntosCarga=$resultados[$i]["Descripcion"];
                                                                }
                                                        ?>
                                                        <div class="tab">
                                                        <input type="radio" id="IdPuntoCargaSel<?php echo ($i+1) ?>" name="IdPuntoCargaSel" value="<?php echo $resultados[$i]["IdPuntoCarga"] ?>" >
                                                        <label for="IdPuntoCargaSel<?php echo ($i+1) ?>"><?php echo $DescripcionPuntosCarga ?></label> &nbsp;&nbsp;&nbsp;
                                                        </div>
                                                        <?php 
                                                            }}
                                                         ?>                                                   
                                                  <br> <br> <br> 
                                                                                          
                                                  <fieldset>
					  							  <legend>Detalle Servicios</legend>
                                                  
                                                     <div class="row">  
                                                     	<!--<div class="col-xs-2">
                                                        <label class="control-label col-xs-2">Financiamiento</label>
                                                        </div> -->                                                      
                                                        <div class="col-xs-3">
                                                        <label class="control-label col-xs-2">Servicio(min.4Letras)</label>
                                                        </div>
                                                        <div class="col-xs-1"> 
                                                        <label class="control-label col-xs-1">Codigo</label>
                                                        </div>
                                                        <div class="col-xs-2">
                                                        <label class="control-label col-xs-2">PrecioUnitario</label>
                                                        </div> 
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Cant</label>
                                                        </div>  
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Hay</label>
                                                        </div>                                                                                                              
                                                        <div class="col-xs-2">
                                                        <label class="control-label col-xs-2">Observaciones</label>
                                                        </div>
                                                                                                                                  
                                                   </div> 
                       
                                                   <div class="row"> 
                                                   	<?php /*?><div class="col-xs-2">                                                                                                         
                                                        <input type="text" class="form-control input-sm" value="<?php echo $DescripcionFinanciamiento ?>" disabled /> 
                                                    </div> <?php */?>                       
                                                    <div class="col-xs-3">                                                                                                          
                                                        <input  id="c_desprdS" name="c_desprdS"  type="hidden" />
                                                        <input autocomplete="off" id="descripcionS" name="descripcionS" class="form-control input-sm" type="text" placeholder="Nombre del Servicio" /> <!--onFocus="abrirmodalServ();" readonly-->
                                                    </div>
                                                    <div class="col-xs-1">
                                                    	<input id="c_codprdS" name="c_codprdS" type="text" class="form-control input-sm" /> <!--readonly-->
                                                        <input name="IdProductoS" type="hidden" class="form-control input-sm"  id="IdProductoS" />                                                         
                                                    </div>                                                    
                                                    <div class="col-xs-2">                                                       
                                                        <input id="PrecioS" name="PrecioS" type="text" class="form-control input-sm" />         	
                                                    </div> 
                                                    <div class="col-xs-1">
                                                        <input name="CantidadPedidaS" type="text" class="form-control input-sm"  id="CantidadPedidaS" value="1" onKeyPress="return validaDecimal(event)" />  
                                                      
                                                    </div>  
                                                    <div class="col-xs-1">
                                                        <input name="HaySaldoS" id="HaySaldoS" type="checkbox" class="form-control input-sm" value="1" /> 
                                                    </div>                                                  
                                                    <div class="col-xs-2">
                                                    	<input name="observaciones" id="observaciones" type="text" class="form-control input-sm" />                                                        
                                                    </div>
                                                                       
                                                    <div class="col-xs-1">
                                                        <button class="btn btn-success btn-sm" id="btn-agregar" 
                                                        type="button" onClick="agregarServicio();">
                                                             <i class="glyphicon glyphicon-plus"></i>
                                                        </button>                                                        
                                                    </div>
                                                    
                                                </div> 
                                                
                                                <hr />
                                                <table  id="tblSampleServicio" class="table table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>Codigo</th>
                                                      <th>Procedimiento</th> 
                                                      <th>Precio Unit.</th>  
                                                      <th>Cantidad</th> 
                                                      <th></th> 
                                                      <th></th>     
                                                      <th>Hay</th>       
                                                      <th>Observaciones</th>    
                                                      <th>Delete</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                  <?php 												  	
													$dataDetalleServicio=ListarRecetasDevuelveDatosDelDetalle($IdReceta,$IdPuntoCargaServicio);
													for ($j=0; $j < count($dataDetalleServicio); $j++) { 															
													?>
												<tr>
												  <td id="<?php echo "nroS".($j+1) ?>"><?php echo ($j+1) ?></td>											
												  <td>												  
												  <input type="text" name="<?php echo "c_codprdS".($j+1); ?>" id="<?php echo "c_codprdS".($j+1); ?>" size="5" value="<?php echo $dataDetalleServicio[$j]['Codigo'] ?>"    class="form-control input-sm" readonly /></td>
                                                  
												  <td><input type="text" name="<?php echo "c_desprdS".($j+1); ?>" id="<?php echo "c_desprdS".($j+1); ?>" size="30" value="<?php echo $dataDetalleServicio[$j]['Producto'] ?>" class="form-control input-sm" readonly /></td>
                                                  
												  <td><input type="text" name="<?php echo "PrecioS".($j+1); ?>" id="<?php echo "PrecioS".($j+1); ?>" size="5" value="<?php $Precio=number_format( $dataDetalleServicio[$j]['Precio'] , 2 , "." , " " ); echo $Precio; ?>" class="form-control input-sm" readonly /></td>
												  
                                                  <td><input type="text" name="<?php echo "CantidadPedidaS".($j+1); ?>" id="<?php echo "CantidadPedidaS".($j+1); ?>" size="3" value="<?php echo $dataDetalleServicio[$j]['CantidadPedida'] ?>" class="form-control input-sm" /></td>
                                                  
                                                  <td><input type="hidden" name="<?php echo "IdProductoS".($j+1); ?>" id="<?php echo "IdProductoS".($j+1); ?>" size="2" value="<?php echo $dataDetalleServicio[$j]['idItem'] ?>" class="form-control input-sm" readonly /></td>
                                                  
                                                  <td><input type="hidden" name="<?php echo "IdPuntoCarga".($j+1); ?>" id="<?php echo "IdPuntoCarga".($j+1); ?>" size="2" value="<?php echo $IdPuntoCarga?>" class="form-control input-sm" /></td>                                                
                                                 
                                                  <td><input name="<?php echo "HaySaldoS".($j+1); ?>" id="<?php echo "HaySaldoS".($j+1); ?>" size="4" value="1" class="form-control input-sm" type="checkbox"></td>
                                                  
                                                  <td><input type="text" name="<?php echo "FrecuenciaS".($j+1); ?>" id="<?php echo "FrecuenciaS".($j+1); ?>" size="10" value="<?php echo $dataDetalleServicio[$j]['observaciones'] ?>" class="form-control input-sm" /></td>                               
                                                 
												  <td><input type="button" name="button3" id="button3" value="delete"  class="btn btn-danger btn-sm" onClick="eliminarServicio(this)" /></td>
												</tr>
                                                
												<?php } ?>                                                  	
                                                    
                                                  </tbody> <input name="cantitemsServicio" id="cantitemsServicio" type="hidden" value="<?php echo $j ?>">
                                                </table>                               
                                                                                                      
                                              </fieldset>
                                                                           
                                        </div>
                                        <!--tab2primary-->
                                        
                                        <div class="tab-pane fade" id="tab3primary">      
                                        	<!--fila 1-->
                                            <div class="form-group">                                               
                                                
                                                <label class="control-label col-xs-2">Apellidos y Nombres Paciente:</label>
                                                <div class="col-xs-3">
                                                    <div class="input-sm"> <?php echo $Paciente ?> </div>                                                       
                                                </div>                            
                                                  
                                            </div> 
                                            <!--FIN fila 1-->                                                           
                                        </div> 
                                        <!--tab3primary-->                                                        
                                        
                                    </div>
                                    </form>
                                </div>
                                
                            </div>
                        </div>
                </div>
                                
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    
     	<script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/js/mascara/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script>         
        
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
        
       <!-- datepicker y autocompletado-->     
       <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        
 <script type="text/javascript">
 
	jQuery(function($){
		$.mask.definitions['h'] = "[0-2]";
		$.mask.definitions['i'] = "[0-9]";
		$.mask.definitions['m'] = "[0-5]"; 
		$.mask.definitions['n'] = "[0-9]";		
	   $("#HoraReceta").mask("hi:mn:mn");	   
	});

    $(function () {	
				
		//Array para dar formato en español
        $.datepicker.regional['es'] =
        {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        			$.datepicker.setDefaults($.datepicker.regional['es']);

       				$("#FechaIngreso").datepicker();
					$("#FechaReceta").datepicker();
					$("#fechaVigencia").datepicker();
       });
   

   function validarguardar(){
	   
	   var theTable = document.getElementById('tblSample');
		cantFilasProducto = theTable.rows.length;
		
		var tblSampleServicio = document.getElementById('tblSampleServicio');
		cantFilasServicio = tblSampleServicio.rows.length;
		
		if(cantFilasProducto==1 && cantFilasServicio==1){
			alertify.error("<b> Mensaje del Sistema: </b> <h1>Falta Detalle de Farmacia y Servicio</h1>");
		    return 0;		
		}
	   
	   //document.getElementById('form1').submit();
	   alertify.confirm("Seguro de Modificar la Receta", function (e) {
			if (e) {							
				document.getElementById('form1').submit();
			} 
		});
		  
   }
   
   function cancelar(){
	   location.href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
   }
   
 function abrirmodalProd(){
	document.frmproducto.criterio.value="";
	document.frmproducto.codigo.value="";
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';		
	$('#my_modalProd').modal('show');
	$('#tablaProd').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verProductos",{IdTipoFinanciamiento:IdTipoFinanciamiento});		
 }
 
 function abrirmodalProd2(){				
	var criterio=document.frmproducto.criterio.value;
	var codigo=document.frmproducto.codigo.value;	
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';				
	$('#my_modalProd').modal('show');		
	$('#tablaProd').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verProductos",{criterio:criterio,codigo:codigo,IdTipoFinanciamiento:IdTipoFinanciamiento});	
}

function abrirmodalServ(){
	document.frmservicio.criterio.value="";
	document.frmservicio.codigo.value="";
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';	
	$('#my_modalServ').modal('show');
	$('#tablaServ').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verServicios",{IdTipoFinanciamiento:IdTipoFinanciamiento});	
 }
 
 function abrirmodalServ2(){				
	var criterio=document.frmservicio.criterio.value;
	var codigo=document.frmservicio.codigo.value;	
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';			
	$('#my_modalServ').modal('show');	
	$('#tablaServ').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verServicios",{criterio:criterio,codigo:codigo,IdTipoFinanciamiento:IdTipoFinanciamiento});	
}

function abrirmodalPaque(){
	document.frmpaquete.criterio.value="";
	document.frmpaquete.codigo.value="";
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';	
	$('#my_modalPaque').modal('show');
	$('#tablaPaque').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verPaquetes",{IdTipoFinanciamiento:IdTipoFinanciamiento});	
 }
 
 function abrirmodalPaque2(){				
	var criterio=document.frmpaquete.criterio.value;
	var codigo=document.frmpaquete.codigo.value;	
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';			
	$('#my_modalPaque').modal('show');	
	$('#tablaPaque').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verPaquetes",{criterio:criterio,codigo:codigo,IdTipoFinanciamiento:IdTipoFinanciamiento});	
}

//autocomplete
$(document).ready(function() {	 
 		
	$("#descripcion").autocomplete({
		source: function ( request, response ) {
			$.ajax({
				
				url: "../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=FiltroProductos",
				dataType: "json",
				data: {
					criterio: request.term,
					codigo:'',
					IdTipoFinanciamiento:'<?php echo $IdTipoFinanciamiento ?>'
				},
				success: function (data) {
					response($.map(data,function(item){
						return {
							label        : item.Nombre,
							value        : item.Nombre,
							Codigo        : item.Codigo,
							Nombre        : item.Nombre,
							IdProducto   : item.IdProducto,
							Precio  	 : item.Precio
							};
					}));
				}
			});
		},
		minLength: 4,
		/*focus: function (event, ui) {
			$(event.target).val(ui.item.label);
			return false;
		},*/
		select: function (event, ui) {
			$(event.target).val(ui.item.label);
			document.getElementById('c_codprd').value=ui.item.Codigo;	
			document.getElementById('c_desprd').value=ui.item.Nombre;
			document.getElementById('descripcion').value=ui.item.Nombre;
			document.getElementById('Precio').value=ui.item.Precio;
			document.getElementById('IdProducto').value=ui.item.IdProducto;				
			return false;
		}		
	}); //fin descripcion Farmacia
	
	$("#c_codprd").autocomplete({
		source: function ( request, response ) {
			$.ajax({
				
				url: "../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=FiltroProductos",
				dataType: "json",
				data: {
					criterio:'',
					codigo:request.term,
					IdTipoFinanciamiento:'<?php echo $IdTipoFinanciamiento ?>'
				},
				success: function (data) {
					response($.map(data,function(item){
						return {
							label        : item.Codigo,
							value        : item.Codigo,
							Codigo        : item.Codigo,
							Nombre        : item.Nombre,
							IdProducto   : item.IdProducto,
							Precio  	 : item.Precio
							};
					}));
				}
			});
		},
		minLength: 2,
		/*focus: function (event, ui) {
			$(event.target).val(ui.item.label);
			return false;
		},*/
		select: function (event, ui) {
			$(event.target).val(ui.item.label);
			document.getElementById('c_codprd').value=ui.item.Codigo;	
			document.getElementById('c_desprd').value=ui.item.Nombre;
			document.getElementById('descripcion').value=ui.item.Nombre;
			document.getElementById('Precio').value=ui.item.Precio;
			document.getElementById('IdProducto').value=ui.item.IdProducto;				
			return false;
		}		
	});//fin codigo Farmacia
	
	
	$("#descripcionS").autocomplete({
		source: function ( request, response ) {
			$.ajax({
				
				url: "../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=FiltroServicios",
				dataType: "json",
				data: {
					criterio: request.term,
					codigo:'',
					IdTipoFinanciamiento:'<?php echo $IdTipoFinanciamiento ?>'
				},
				success: function (data) {
					response($.map(data,function(item){
						return {
							label        : item.Nombre,
							value        : item.Nombre,
							Codigo        : item.Codigo,
							Nombre        : item.Nombre,
							IdProducto   : item.IdProducto,
							Precio  	 : item.Precio
							};
					}));
				}
			});
		},
		minLength: 4,
		/*focus: function (event, ui) {
			$(event.target).val(ui.item.label);
			return false;
		},*/
		select: function (event, ui) {
			$(event.target).val(ui.item.label);				
			document.getElementById('c_codprdS').value=ui.item.Codigo;	
			document.getElementById('c_desprdS').value=ui.item.Nombre;
			document.getElementById('descripcionS').value=ui.item.Nombre;
			document.getElementById('PrecioS').value=ui.item.Precio;
			document.getElementById('IdProductoS').value=ui.item.IdProducto;	
			return false;
		}		
	}); //fin descripcion Servicio
	
	$("#c_codprdS").autocomplete({
		source: function ( request, response ) {
			$.ajax({
				
				url: "../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=FiltroServicios",
				dataType: "json",
				data: {
					criterio:'',
					codigo:request.term,
					IdTipoFinanciamiento:'<?php echo $IdTipoFinanciamiento ?>'
				},
				success: function (data) {
					response($.map(data,function(item){
						return {
							label        : item.Codigo,
							value        : item.Codigo,
							Codigo        : item.Codigo,
							Nombre        : item.Nombre,
							IdProducto   : item.IdProducto,
							Precio  	 : item.Precio
							};
					}));
				}
			});
		},
		minLength: 2,
		/*focus: function (event, ui) {
			$(event.target).val(ui.item.label);
			return false;
		},*/
		select: function (event, ui) {
			$(event.target).val(ui.item.label);
			document.getElementById('c_codprdS').value=ui.item.Codigo;	
			document.getElementById('c_desprdS').value=ui.item.Nombre;
			document.getElementById('descripcionS').value=ui.item.Nombre;
			document.getElementById('PrecioS').value=ui.item.Precio;
			document.getElementById('IdProductoS').value=ui.item.IdProducto;			
			return false;
		}		
	});//fin codigo Servicio
	
});

</script>  
</body>

</html>
