<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	
	$ObtenerDatosPacientes=ObtenerDatosPacientes_M($IdPaciente);		
	$FechaNacimiento = vfecha(substr($ObtenerDatosPacientes[0]['FechaNacimiento'],0,10));
	$FechaNacimientoX = time() - strtotime($ObtenerDatosPacientes[0]['FechaNacimiento']);
	$edad = floor((($FechaNacimientoX / 3600) / 24) / 360);
	$Paciente=$ObtenerDatosPacientes[0]["ApellidoPaterno"].' '.$ObtenerDatosPacientes[0]["ApellidoMaterno"].', '.$ObtenerDatosPacientes[0]["PrimerNombre"].' '.$ObtenerDatosPacientes[0]["SegundoNombre"];
	
   if($ObtenerDatosPacientes[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($ObtenerDatosPacientes[0]["IdTipoSexo"]==2){$Sexo='Femenino';}	
   $GrupoSanguineo=$ObtenerDatosPacientes[0]['GrupoSanguineo']
	
?>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - RECETAS MÉDICAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">   

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--Alerts -->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
    
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/ListaVerificacion.css">   
    <!--NUEVO-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">-->
    
    <!--datepicker y autocompletado-->
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.css">
  	

<!--GRILLA DETALLE ASIGNACION-->
<!--<script src="assets/js/bootbox.min.js"></script>-->

<script type="text/javascript">

function limpiar(){
	document.form1.c_codprd.value='';
	document.form1.c_desprd.value='';
	//document.form1.Precio.value='';
}
		
var INPUT_NAME_PREFIX = 'c_codprd'; // this is being set via script a
var INPUT_NAME_Precio = 'Precio'; // this is being set via script a
var INPUT_NAME_DES = 'c_desprd'; // this is being set via script b
var INPUT_NAME_CAN = 'n_canprd'; // this is being set via script c
var INPUT_NAME_OBS = 'IdViaAdministracion'; // this is being set via script e
var INPUT_NAME_ESTA = 'NT_CUND';
var INPUT_NAME_EQP = 'c_codcont'; // this is being set via script g
//var INPUT_NAME_PRODUCTO = 'c_producto'; // this is being set via script g
//var INPUT_NAME_CLASE = 'COD_CLASE'; // this is being set via script g

var TABLE_NAME = 'tblSample'; // this should be named in the HTML
var ROW_BASE = 1; // first number (for display)
var hasLoaded = false;
window.onload=fillInRows;
function fillInRows()
{
	hasLoaded = true;	
}
// CONFIG:
// myRowObject is an object for storing information about the table rows
function myRowObject(one,two,tres,cuatro,cinco,seis,siete,ocho,nueve,diez)
{
	this.one = one; // text object
	this.two = two; // input text object
	this.tres=tres;
	this.cuatro=cuatro;
	this.cinco=cinco;
	this.seis=seis;	
	this.siete=siete;
	this.ocho=ocho;
	this.nueve=nueve;
	this.diez=diez;
}

function insertRowToTable()
{
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAME);
		var rowToInsertAt = tbl.tBodies[0].rows.length;
		for (var i=0; i<tbl.tBodies[0].rows.length; i++) {
			if (tbl.tBodies[0].rows[i].myRow) {
				rowToInsertAt = i;
				break;
			}
		}
		addRowToTable(rowToInsertAt);
reorderRows(tbl, rowToInsertAt);
	}
}

function addRowToTable(num)
{
	//alert('hola');
	var codigo=document.getElementById("c_codprd").value
	var Precio=document.getElementById("Precio").value
	var des=document.getElementById("c_desprd").value	
	var can=document.getElementById("n_canprd").value
	var serie=document.getElementById("c_codcont").value	
	var obs=document.getElementById("IdViaAdministracion").value
	var NT_CUND=document.getElementById("NT_CUND").value
	//var c_producto=document.getElementById("c_producto").value
	//var COD_CLASE=document.getElementById("COD_CLASE").value
		
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAME);
		var nextRow = tbl.tBodies[0].rows.length;
		var iteration = nextRow + ROW_BASE;
		if (num == null) { 
			num = nextRow;
		} else {
			iteration = num + ROW_BASE;
		}
		
		// add the row
		var row = tbl.tBodies[0].insertRow(num);
		
		// CONFIG: requires classes named classy0 and classy1
		row.className = 'classy' + (iteration % 2);
	
		// CONFIG: This whole section can be configured
		
		// cell 0 - text
		var cell0 = row.insertCell(0);
		var textNode = document.createTextNode(iteration);
		cell0.appendChild(textNode);
		
		// cell 1 - input text
		var cell1 = row.insertCell(1);
		var txtInpa = document.createElement('input');
		txtInpa.setAttribute('type', 'hidden');
		txtInpa.setAttribute('name', INPUT_NAME_PREFIX + iteration);
		txtInpa.setAttribute('id', INPUT_NAME_PREFIX + iteration);
		txtInpa.setAttribute('size', '5');
		txtInpa.setAttribute('value', codigo); // iteration included for debug purposes
		txtInpa.setAttribute('readonly', 'readonly');
		txtInpa.setAttribute('class', 'class="form-control input-sm"'); 
		cell1.appendChild(txtInpa);	
		
		
		var cell2 = row.insertCell(2);
		var txtInpb = document.createElement('input');
		txtInpb.setAttribute('type', 'text');
		txtInpb.setAttribute('name', INPUT_NAME_DES + iteration);
		txtInpb.setAttribute('id', INPUT_NAME_DES + iteration);
		txtInpb.setAttribute('size', '40');
	    txtInpb.setAttribute('value', des); // iteration included for debug purposes 
	    txtInpb.setAttribute('readonly', 'readonly'); // iteration included for debug 
	    txtInpb.setAttribute('class', 'form-control input-sm'); 	
		cell2.appendChild(txtInpb);	
		
		var cell4 = row.insertCell(3);
		var txtInpd = document.createElement('input');
		txtInpd.setAttribute('type', 'text');
		txtInpd.setAttribute('name', INPUT_NAME_Precio  + iteration);
		txtInpd.setAttribute('id', INPUT_NAME_Precio + iteration);
		txtInpd.setAttribute('size', '5');
		txtInpd.setAttribute('value', Precio); // iteration included for debug purposes 
		//txtInpd.setAttribute('readonly', 'readonly');
		txtInpd.setAttribute('class', 'form-control input-sm'); 		
		cell4.appendChild(txtInpd);			
					
		var cell22 = row.insertCell(4);
		var txtInpb2 = document.createElement('input');
		txtInpb2.setAttribute('type', 'text');
		txtInpb2.setAttribute('name', INPUT_NAME_CAN + iteration);
		txtInpb2.setAttribute('id', INPUT_NAME_CAN + iteration);
		txtInpb2.setAttribute('size', '3');
	    txtInpb2.setAttribute('value', can); // iteration included for debug purposes 
	    txtInpb2.setAttribute('class', 'form-control input-sm'); 
		txtInpb2.setAttribute('onkeyup','validarcambiocantidad(this.name)');	
		cell22.appendChild(txtInpb2);		

		var cell3 = row.insertCell(5);
		var txtInpc = document.createElement('input');
		txtInpc.setAttribute('type', 'text');
		txtInpc.setAttribute('name', INPUT_NAME_EQP + iteration);
		txtInpc.setAttribute('id', INPUT_NAME_EQP + iteration);
		txtInpc.setAttribute('size', '10');
	    txtInpc.setAttribute('value', serie); // iteration included for debug purposes	    
	    txtInpc.setAttribute('readonly', 'readonly');
		txtInpc.setAttribute('class', 'form-control input-sm'); 
		//txtInpc.setAttribute('onFocus','abrirmodalEqp(this.name)');		
		cell3.appendChild(txtInpc);			
		
		var cell5 = row.insertCell(6);
		var txtInpde = document.createElement('input');
		txtInpde.setAttribute('type', 'text');
		txtInpde.setAttribute('name', INPUT_NAME_ESTA + iteration);
		txtInpde.setAttribute('id', INPUT_NAME_ESTA + iteration);
		txtInpde.setAttribute('size', '5');
		txtInpde.setAttribute('value', NT_CUND); // iteration included for debug purposes 
		txtInpde.setAttribute('readonly', 'readonly');
		txtInpde.setAttribute('class', 'form-control input-sm'); 		
		cell5.appendChild(txtInpde);		
		
		// cell 1 - input text
		var cell6 = row.insertCell(7);
		var txtInpf = document.createElement('input');
		txtInpf.setAttribute('type', 'text');
		txtInpf.setAttribute('name', INPUT_NAME_OBS + iteration);
		txtInpf.setAttribute('id', INPUT_NAME_OBS + iteration);
		txtInpf.setAttribute('size', '2');
		txtInpf.setAttribute('value', obs); // iteration included for debug purposes
		txtInpf.setAttribute('readonly', 'readonly');
		txtInpf.setAttribute('class', 'class="form-control input-sm"'); 
		cell6.appendChild(txtInpf);
		
		// cell 1 - input text
		var cell7 = row.insertCell(8);
		var txtInpg = document.createElement('input');
		txtInpg.setAttribute('type', 'hidden');
		txtInpg.setAttribute('name', INPUT_NAME_PRODUCTO + iteration);
		txtInpg.setAttribute('id', INPUT_NAME_PRODUCTO + iteration);
		txtInpg.setAttribute('size', '2');
		txtInpg.setAttribute('value', 'c_producto'); // iteration included for debug purposes
		txtInpg.setAttribute('readonly', 'readonly');
		txtInpg.setAttribute('class', 'class="form-control input-sm"'); 
		cell7.appendChild(txtInpg);
		
		// cell 1 - input text
		var cell8 = row.insertCell(9);
		var txtInph = document.createElement('input');
		txtInph.setAttribute('type', 'hidden');
		txtInph.setAttribute('name', INPUT_NAME_CLASE + iteration);
		txtInph.setAttribute('id', INPUT_NAME_CLASE + iteration);
		txtInph.setAttribute('size', '2');
		txtInph.setAttribute('value', 'COD_CLASE'); // iteration included for debug purposes
		txtInph.setAttribute('readonly', 'readonly');
		txtInph.setAttribute('class', 'class="form-control input-sm"'); 
		cell8.appendChild(txtInph);
		

		var cell9 = row.insertCell(10);
		var btnEl = document.createElement('input');
		btnEl.setAttribute('type', 'button');
		btnEl.setAttribute('value', 'Delete');
		btnEl.setAttribute('class', 'btn btn-danger btn-sm'); 
		btnEl.onclick = function () {deleteCurrentRow(this)};
		cell6.appendChild(btnEl);
		
		row.myRow = new myRowObject(textNode,txtInpa,txtInpb,txtInpb2,txtInpc,txtInpd,txtInpde,txtInpf,txtInpg,txtInph);
	
	}
}

function deleteCurrentRow(obj)
{	
	if (hasLoaded) {			
		var delRow = obj.parentNode.parentNode;
		var tbl = delRow.parentNode.parentNode;
		var rIndex = delRow.sectionRowIndex;		
		var rowArray = new Array(delRow);
		
		DesbloquearEquiposQuit(rIndex);//desbloqueo equipo		
		deleteRows(rowArray);		
	    reorderRows(tbl, rIndex);
		
	}
}

function DesbloquearEquiposQuit(rIndex){		
			//recuperar codigos a desbloquear
			nitem=parseInt(rIndex)+1;
			serie=document.getElementsByName("c_codcont"+nitem)[0].value;
			alert('El equipo'+serie+' con item'+nitem+' Fue quitado');
			//serie='hola';
			//alert(serie+i);
				 jQuery.ajax({
					url: '?c=not01&a=DesbloquearEquiposQuit',
					type: "post",
					dataType: "json",
					data: {
						//idequipo: idequipo, //codsel
						c_codcont:serie //codanterior
						//ncoti:ncoti
					}
				})	
}


function reorderRows(tbl, startingIndex)
{
	if (hasLoaded) {
		if (tbl.tBodies[0].rows[startingIndex]) {
			var count = startingIndex + ROW_BASE;
			for (var i=startingIndex; i<tbl.tBodies[0].rows.length; i++) {
			
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.one.data = count; // text
				
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.two.name = INPUT_NAME_PREFIX + count; // input text							
				tbl.tBodies[0].rows[i].myRow.tres.name = INPUT_NAME_DES + count;
				tbl.tBodies[0].rows[i].myRow.cuatro.name = INPUT_NAME_OBS + count;
				
				tbl.tBodies[0].rows[i].myRow.cinco.name = INPUT_NAME_CAN + count;	
				tbl.tBodies[0].rows[i].myRow.seis.name = INPUT_NAME_EQP + count;			
									
				tbl.tBodies[0].rows[i].myRow.siete.name = INPUT_NAME_ESTA + count;
				tbl.tBodies[0].rows[i].myRow.ocho.name = INPUT_NAME_Precio + count;		
				tbl.tBodies[0].rows[i].myRow.nueve.name = INPUT_NAME_PRODUCTO + count;
				tbl.tBodies[0].rows[i].myRow.diez.name = INPUT_NAME_CLASE + count;
				
				// CONFIG: next line is affected by myRowObject settings
				var tempVal = tbl.tBodies[0].rows[i].myRow.two.value.split(' '); 
			    tbl.tBodies[0].rows[i].className = 'classy' + (count % 2);
				
				count++;
				
			}
		}
	}
}

function validarcambiocantidad(obj){
	var cant=obj;//cantidadX
	var condicion=cant.substring(0, 8);//devuelve n_canprd
	//var condicion2=cant.substring(0, 6);
	
	if(condicion=='n_canprd'){
		var nc=cant.substring(8,10);//devuelve index n_canprd		
	}	
	
	var canti=document.getElementById("n_canprd"+nc).value; //cantidad de producto
	var tipopod=document.getElementById("c_producto"+nc).value;
	
}

function deleteRows(rowObjArray)
{
	if (hasLoaded) {
		for (var i=0; i<rowObjArray.length; i++) {
			var rIndex = rowObjArray[i].sectionRowIndex;
			rowObjArray[i].parentNode.deleteRow(rIndex);
		}
	}
}

//validar numeros
 function validaDecimal(e){	 //solo acepta numeros y punto 
	tecla = (document.all) ? e.keyCode : e.which;//obtenemos el codigo ascii de la tecla
	if (tecla==8) return true;//backspace en ascii es 8
	patron=/[0-9\.]/; 
	te = String.fromCharCode(tecla);//convertimos el codigo ascii a string
	return patron.test(te);
} 

function validarnumero(){		
	var n_canprd=document.getElementById('n_canprd').value;
	var patron=/^\d+(\.\d{1,2})?$/;
		if(!patron.test(n_canprd)){		
		//window.alert('monto ingresado incorrecto');
		document.getElementById('n_canprd').value='';
		document.getElementById('n_canprd').focus();
		return false;
		}
		
}

function validarcantidad(){		
		var descripcion=document.getElementById('descripcion').value;
		var c_codprd=document.getElementById('c_codprd').value;
		if(descripcion==""){
			var mensje = "Falta Ingresar un Producto ...!!!";
			alert(mensje);
			document.getElementById('n_canprd').value='0';
		}else if(c_codprd==""){
			var mensje = "Busque y Seleccione un Producto ...!!!";
			alert(mensje);
			document.getElementById('n_canprd').value='0';
		}else{		
			var n_canprd=document.getElementById('n_canprd').value;
			var patron=/^\d+(\.\d{1,2})?$/;
				if(!patron.test(n_canprd)){		
				//window.alert('monto ingresado incorrecto');
				document.getElementById('n_canprd').value='0';
				document.getElementById('n_canprd').focus();
				return false;
				}
		}		
}

function agregar(){
	
	  var theTable = document.getElementById('tblSample');
		cantFilas = theTable.rows.length;//a partir de cantFilas==2 la tabla esta llena
		if(cantFilas>1){
			for(i=1;i<cantFilas;i++){
				if((document.form1.c_desprd.value)==document.getElementsByName("c_desprd"+i)[0].value){
					//alert('hola'+i);
					mensje = "Producto Seleccionado ya está en el Registro";
					alert(mensje);
					return 0;
				}
				
			}
		}//end if
	
	    if((document.form1.c_codprd.value)==""){
			mensje = "Falta Ingresar Descripcion";
			alert(mensje);
			document.getElementById("descripcion").focus();			
		}else if((document.form1.n_canprd.value)=="0" || (document.form1.n_canprd.value)==""){
			mensje = "Falta Ingresar Cantidad";
			alert(mensje);	
		}else{        
			addRowToTable();		
			$("#c_codprd").val('');
			$("#c_desprd").val('');	
			$("#n_canprd").val('0');		
			$("#c_codcont").val('');
			$("#IdViaAdministracion").val('');
			//$("#c_producto").val('');			
			$("#Precio").val('');		
			$("#descripcion").val('');
			$("#NT_CUND").val('');
			//$("#COD_CLASE").val('');
		}	
	}
	
</script>
<!--FIN GRILLA--> 
</head>

<body>

<!--modal de ver productos-->
<div class="modal fade" id="my_modalProd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<form class="form-horizontal" id="frmproducto" name="frmproducto" action="#">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">      
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="exampleModalLabel">Medicamentos e Insumos</h4>
        <!--<input name="c_codalm" id="c_codalm" type="text"  />-->
        <!--<input type="text" id="criterio" name="criterio" class="form-control" placeholder="Busque por Descripcion y/o Codigo de Producto" onKeyUp="abrirmodalProd2()"  />
        <input type="text" id="codigo" name="codigo" class="form-control" placeholder="Busque por Codigo de Producto" onKeyUp="abrirmodalProd2()"  />-->
        <div class="col-xs-6">            
            <input type="text" id="criterio" name="criterio" placeholder="Busque por Descripcion de Producto" onKeyUp="abrirmodalProd2()"  class="form-control input-sm" />           	
         
        </div> 
        <div class="col-xs-6">            
            <input id="codigo" name="codigo" placeholder="Busque por Codigo de Producto" onKeyUp="abrirmodalProd2()" type="text" class="form-control input-sm" />           	
         
        </div> 
        
      </div>
      	<div class="modal-body">
            <table id="tablaProd" class="table table-hover" style="font-size:12px;">
        		<!--Contenido se encuentra en verEquiposDispo.php-->
            </table> 
        </div>
      </div>
    </div>
    </form>
  </div>
 <!--fin modal de ver productos-->

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">				
				<?php  include('../../MVC_Vista/RecetasMedicas/Cabecera.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">RECETAS MÉDICAS</h3>
                </div>                
            </div>
            <!-- /.row -->
            
            <div class="row">
            
                <div class="container-fluid">               
                   <div class="col-md-12">
                            <div class="panel panel-primary with-nav-tabs"> <!--with-nav-tabs--> 
                            	<!--<div class="panel-heading">REGISTRAR RECETA</div>-->
                                <div class="panel-heading"  >
                                        <ul class="nav nav-tabs">
                                        	<li class="active"><a href="#tabcabecera" data-toggle="tab">Cabecera</a></li>
                                            <li class="desabilitar"><a href="#tab1primary" data-toggle="tab">Farmacia</a></li>
                                            <li class = "desabilitar"><a href="#tab2primary" data-toggle="tab">Servicio</a></li>
                                            <li class = "desabilitar"><a href="#tab3primary" data-toggle="tab">Dieta</a></li>
                                            <!--<li class = "desabilitar"><a href="#tab4primary" data-toggle="tab">Salida</a></li>-->         
                                        </ul>
                                </div>
                                <div class="panel-body">
                                	<form class="form-horizontal" id="form1" name="form1" method="post" action="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=GuardarReceta&IdEmpleado=<?php echo $IdEmpleado ?>"  >
                                       <div class="form-control-static" align="right">
                                         <input class="btn btn-success" type="button" onClick="validarguardar()" value="Registrar"/>
                                         &nbsp;<a class="btn btn-danger" onClick="cancelar();">Cancelar</a>
                                         <!--&nbsp;<a class="btn btn-warning" onClick="location.reload();" >Refrescar</a>&nbsp;
                                         &nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                                        </div>
                                  <div class="tab-content">                                  		
                                  <!--Inicio Identificacion-->
                                        <div class="tab-pane fade in active" id="tabcabecera">                                     
                                                <fieldset>
					  							<legend>Datos Paciente</legend>
                                                    <!--fila 1-->
                                                    <div class="form-group">
                                                    	
                                                        <input name="Servicio" id="Servicio" type="hidden" value="<?php echo $Servicio ?>" />                                                 
                                                        <input name="IdMedico" id="IdMedico" type="hidden" value="<?php echo $_SESSION['IdMedico']; ?>" /> 
                                                        
                                                        <input name="IdPaciente" id="IdPaciente" type="hidden" value="<?php echo $IdPaciente ?>" />  
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres:</label>
                                                        <div class="col-xs-2">
                                                            <input type="text" class="form-control input-sm" value="<?php echo $Paciente ?>" />  							                                                        
                                                        </div>
                                                        <label class="control-label col-xs-1">Fecha Nac.:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $FechaNacimiento ?>" />                                                      	</div>                     
                                                        
                                                        <label class="control-label col-xs-1">Edad:</label>
                                                        <div class="col-xs-1">
                                                        	<input type="text" class="form-control input-sm" value="<?php echo $edad ?>" />
                                                        </div> 
                                                        
                                                        <label class="control-label col-xs-1">Sexo</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $Sexo ?>" />                                                                  	
                                                        </div>                             
                                                          
                                                    </div> 
                                                    <!--FIN fila 1--> 
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">
                                                    	<label class="control-label col-xs-2">Nro Cuenta:</label>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control input-sm" value="<?php echo $IdCuentaAtencion ?>" />  							                                                        
                                                        </div>                                                       
                                                    	<label class="control-label col-xs-2">Grupo Sanguineo:</label>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control input-sm" value="<?php echo $GrupoSanguineo ?>" />  							                                                        
                                                        </div>
                                                        <label class="control-label col-xs-1">Servicio:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $DescripcionServicio ?>" />                                                      	</div>   
                                                        <label class="control-label col-xs-1">Fecha Ingreso:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $FechaIngreso ?>" />                                                      	</div>                              
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>                                                  
                                                 </form>
                                                                                      
                                        
                                        </div>
                                   		<!--tabcabecera-->
                                        
                                    <!--Inicio tab1primary-->
                                        <div class="tab-pane fade" id="tab1primary">                                          
                                               
                                                <fieldset>
					  							<legend>Datos Paciente</legend>
                                                    <!--fila 1-->
                                                    <div class="form-group">
                                                    	
                                                        <input name="Servicio" id="Servicio" type="hidden" value="<?php echo $Servicio ?>" />                                                 
                                                        <input name="IdMedico" id="IdMedico" type="hidden" value="<?php echo $_SESSION['IdMedico']; ?>" /> 
                                                        
                                                        <input name="IdPaciente" id="IdPaciente" type="hidden" value="<?php echo $IdPaciente ?>" />  
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres:</label>
                                                        <div class="col-xs-2">
                                                            <input type="text" class="form-control input-sm" value="<?php echo $Paciente ?>" />  							                                                        
                                                        </div>
                                                        <label class="control-label col-xs-1">Fecha Nac.:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $FechaNacimiento ?>" />                                                      	</div>                     
                                                        
                                                        <label class="control-label col-xs-1">Edad:</label>
                                                        <div class="col-xs-1">
                                                        	<input type="text" class="form-control input-sm" value="<?php echo $edad ?>" />
                                                        </div> 
                                                        
                                                        <label class="control-label col-xs-1">Sexo</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $Sexo ?>" />                                                                  	
                                                        </div>                             
                                                          
                                                    </div> 
                                                    <!--FIN fila 1--> 
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">
                                                    	<label class="control-label col-xs-2">Nro Cuenta:</label>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control input-sm" value="<?php echo $IdCuentaAtencion ?>" />  							                                                        
                                                        </div>                                                       
                                                    	<label class="control-label col-xs-2">Grupo Sanguineo:</label>
                                                        <div class="col-xs-1">
                                                            <input type="text" class="form-control input-sm" value="<?php echo $GrupoSanguineo ?>" />  							                                                        
                                                        </div>
                                                        <label class="control-label col-xs-1">Servicio:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $DescripcionServicio ?>" />                                                      	</div>   
                                                        <label class="control-label col-xs-1">Fecha Ingreso:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" value="<?php echo $FechaIngreso ?>" />                                                      	</div>                              
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>
                                                  
                                                  <fieldset>
					  							  <legend>Datos Farmacia</legend>
                                                  
                                                     <div class="row">                                                       
                                                        <div class="col-xs-3">
                                                        <label class="control-label col-xs-3">Descripcion</label>
                                                        </div>
                                                        <div class="col-xs-1"> 
                                                        <label class="control-label col-xs-1">Codigo</label>
                                                        </div>
                                                        <div class="col-xs-2">
                                                        <label class="control-label col-xs-2">PrecioUnitario</label>
                                                        </div> 
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Cant</label>
                                                        </div>                                                                                                                
                                                        <div class="col-xs-2">
                                                        <label class="control-label col-xs-2">Via</label>
                                                        </div>
                                                                                                                                  
                                                   </div> 
                       
                                                   <div class="row">                         
                                                       <div class="col-xs-3">
                                                        <!--<input  id="COD_CLASE" name="COD_CLASE" type="hidden" value=1""  />-->                                                        
                                                        <!--<input id="c_producto" name="c_producto" type="hidden" value="3" />-->                                                   
                                                        <input  id="c_desprd" name="c_desprd"  type="hidden" />
                                                        <input autocomplete="off" id="descripcion" name="descripcion" class="form-control input-sm" type="text" placeholder="Nombre del producto" onFocus="abrirmodalProd();" readonly/>
                                                    </div>
                                                    <div class="col-xs-1">
                                                    	<input id="c_codprd" name="c_codprd" type="text" class="form-control input-sm" readonly /><!--NT_CART-->
                                                        <input name="c_codcont" type="hidden" class="form-control input-sm"  id="c_codcont" />                                                         
                                                    </div>                                                    
                                                    <div class="col-xs-2">                                                         
                                                        <input name="NT_CUND" type="hidden" id="NT_CUND"  /> 
                                                         <input id="Precio" name="Precio" type="text" class="form-control input-sm" /> <!--IN_COST-->            	
                                                     
                                                    </div> 
                                                    <div class="col-xs-1">
                                                        <input name="n_canprd" type="text" class="form-control input-sm"  id="n_canprd" value="0" onKeyPress="return validaDecimal(event)"  /> 
                                                      
                                                    </div>                                                    
                                                    <div class="col-xs-2">
                                                        <input name="IdViaAdministracion" type="text" class="form-control input-sm" id="IdViaAdministracion" placeholder="Observacion"  />
                                                    </div>
                                                                       
                                                    <div class="col-xs-1">
                                                        <button class="btn btn-success btn-sm" id="btn-agregar" 
                                                        type="button" onClick="agregar();">
                                                             <i class="glyphicon glyphicon-plus"></i>
                                                        </button>                                                        
                                                    </div>
                                                    
                                                </div> 
                                                
                                                <hr />
                                                <table  id="tblSample" class="table table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th></th>
                                                      <th>Descripcion</th> 
                                                      <th>Precio</th>  
                                                      <th>Cantidad</th>      
                                                      <th>Codigo Equipo</th>       
                                                      <th>Medida</th>    
                                                      <th>Delete</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                  </tbody>
                                                </table>                                  
                                                      
                                                    <!--fila 3-->                                                     
                                                     <!--<div class="form-group">                                                        
                                                        <div class="col-xs-1 text-right">  
                                                            <input type="button" value="Aceptar" onClick="Aceptar()" class="btn btn-group-sm btn-primary" />                                                        </div>                                                         
                                                      </div> -->
                                                      <!--FIN fila 3-->                                                   
                                                  </fieldset>
                                                 
                                                                                      
                                        
                                        </div>
                                   		<!--tab1primary-->
                                        
                                        <div class="tab-pane fade" id="tab2primary">                                    
                                        </div>
                                        <!--tab2primary-->
                                        
                                        <div class="tab-pane fade" id="tab3primary">                                                                    
                                        </div> 
                                        <!--tab3primary-->
                                                              
                                        <div class="tab-pane fade" id="tab4primary"> 
                                        </div>
                                        <!--tab4primary-->
                                    </div>
                                    </form>
                                </div>
                                
                            </div>
                        </div>
                </div>
                                
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    
     	<script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script>         
        
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
        
       <!-- datepicker y autocompletado-->     
       <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        
 <script type="text/javascript">
    $(function () {
	
		//Array para dar formato en español
        $.datepicker.regional['es'] =
        {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        			$.datepicker.setDefaults($.datepicker.regional['es']);

       				$("#FechaIngreso").datepicker();
       });
   

   function Aceptar(){
	   
	   Paciente=document.getElementById('Paciente').value;
	   
	   if(Paciente.trim=='' || Paciente.trim()=='SELECCIONE'){
		   alertify.error("<b> Mensaje del Sistema: </b> <h1>Falta Buscar Paciente</h1>");
		   return 0;
	   }
	   
	   //document.getElementById('form1').submit();
	   alertify.confirm("Seguro de Registrar Receta al Paciente seleccionado", function (e) {
			if (e) {							
				document.getElementById('form1').submit();
			} else {
				//alertify.alert("Successful AJAX after Cancel");
				window.location.href = "../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $IdEmpleado ?>";
			}
		});	
	   
   }
   
   function cancelar(){
	   location.href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
   }
   
 function abrirmodalProd(){
	document.frmproducto.criterio.value="";
	document.frmproducto.codigo.value="";
	//almacen=document.Frmregcoti.almacen.value;
	  /* if(almacen==''){			
			var mensje = "Falta seleccionar Almacen en Datos Destinatario ...!!!";
				$('#alertone').modal('show');
				$('#mensaje').val(mensje);
			document.getElementById("almacen").focus();
			return 0;
		}	*/
	$('#my_modalProd').modal('show');
	 $('#tablaProd').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verProductos");	
 }
 
 function abrirmodalProd2(){
	$('#my_modalProd').modal('show');				
	//var alm=document.Frmregcoti.c_codalm.value;
	var criterio=document.frmproducto.criterio.value;
	var codigo=document.frmproducto.codigo.value;				
	//document.write("c_codprd = " + c_codprd);
	 $('#tablaProd').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verProductos",{criterio:criterio,codigo:codigo});	
}

</script>  
</body>

</html>
