<?php 
	session_start();
	error_reporting(E_ALL^E_NOTICE);
	
	$ObtenerDatosPacientes=ObtenerDatosPacientes_M($IdPaciente);		
	$FechaNacimiento = vfecha(substr($ObtenerDatosPacientes[0]['FechaNacimiento'],0,10));
	
	$Paciente=$ObtenerDatosPacientes[0]["ApellidoPaterno"].' '.$ObtenerDatosPacientes[0]["ApellidoMaterno"].', '.$ObtenerDatosPacientes[0]["PrimerNombre"].' '.$ObtenerDatosPacientes[0]["SegundoNombre"];
	
	//EDAD SIN TIPO EDAD (EN AÑOS)
	/*$FechaNacimientoX = time() - strtotime($ObtenerDatosPacientes[0]['FechaNacimiento']);
	$edad = floor((($FechaNacimientoX / 3600) / 24) / 360);*/
	$EDAD=str_replace('<BR> A',' AÑOS',CalcularEdad($ObtenerDatosPacientes[0]['FechaNacimiento'])) ;
	$EDAD=str_replace('<BR> M',' MESES',$EDAD) ;
	$EDAD=str_replace('<BR> D',' DIAS',$EDAD) ;
	$EDAD=str_replace('<BR> H',' HORAS',$EDAD) ; 
	
   if($ObtenerDatosPacientes[0]["IdTipoSexo"]==1){$Sexo='Masculino';}else if($ObtenerDatosPacientes[0]["IdTipoSexo"]==2){$Sexo='Femenino';}	
   $GrupoSanguineo=$ObtenerDatosPacientes[0]['GrupoSanguineo'];
   
   $ObtenerDatosFuenteFinanciamiento=ObtenerDatosFuenteFinanciamiento_M($idFuenteFinanciamiento);
   $DescripcionFinanciamiento=$ObtenerDatosFuenteFinanciamiento[0]["Descripcion"];
   $IdTipoFinanciamiento=$ObtenerDatosFuenteFinanciamiento[0]["IdTipoFinanciamiento"]; 
	
?>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGESA - RECETAS MÉDICAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">   

    <!-- Custom CSS -->
    <link href="../../MVC_Complemento/LibBosstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../MVC_Complemento/LibBosstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--Alerts -->
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/alertify/themes/alertify.default.css">
    
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/ListaVerificacion.css">   
    <!--NUEVO-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.css">-->
    
    <!--datepicker y autocompletado-->
    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.css">
  	

<!--GRILLA DETALLE Medicamento/Insumo-->

<script type="text/javascript">
		
var INPUT_NAME_CODIGO = 'c_codprd'; // this is being set via script a
var INPUT_NAME_Precio = 'Precio'; // this is being set via script a
var INPUT_NAME_DES = 'c_desprd'; // this is being set via script b
var INPUT_NAME_CAN = 'CantidadPedida'; // this is being set via script c
var INPUT_NAME_VIA = 'IdViaAdministracion'; // this is being set via script e
var INPUT_NAME_DOSIS = 'idDosisRecetada';
var INPUT_NAME_IDPRODUCTO = 'IdProducto'; // this is being set via script g
var INPUT_NAME_HAYSALDO = 'HaySaldo'; // this is being set via script g
var INPUT_NAME_FRECUENCIA = 'Frecuencia'; // this is being set via script g

var TABLE_NAME = 'tblSample'; // this should be named in the HTML
var ROW_BASE = 1; // first number (for display)
var hasLoaded = false;
window.onload=fillInRows;
function fillInRows()
{
	hasLoaded = true;	
}
// CONFIG:
// myRowObject is an object for storing information about the table rows
function myRowObject(one,two,tres,cuatro,cinco,seis,siete,ocho,nueve,diez)
{
	this.one = one; // text object
	this.two = two; // input text object
	this.tres=tres;
	this.cuatro=cuatro;
	this.cinco=cinco;
	this.seis=seis;	
	this.siete=siete;
	this.ocho=ocho;
	this.nueve=nueve;
	this.diez=diez;
}

function insertRowToTable()
{
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAME);
		var rowToInsertAt = tbl.tBodies[0].rows.length;
		for (var i=0; i<tbl.tBodies[0].rows.length; i++) {
			if (tbl.tBodies[0].rows[i].myRow) {
				rowToInsertAt = i;
				break;
			}
		}
		addRowToTable(rowToInsertAt);
reorderRows(tbl, rowToInsertAt);
	}
}

function addRowToTable(num)
{
	//alert('hola');
	var codigo=document.getElementById("c_codprd").value
	var Precio=document.getElementById("Precio").value
	var des=document.getElementById("c_desprd").value	
	var can=document.getElementById("CantidadPedida").value
	var IdProducto=document.getElementById("IdProducto").value	
	var IdViaAdministracion=document.getElementById("IdViaAdministracion").value
	var idDosisRecetada=document.getElementById("idDosisRecetada").value
	var HaySaldo=document.getElementById("HaySaldo").value
	var Frecuencia=document.getElementById("Frecuencia").value
		
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAME);
		var nextRow = tbl.tBodies[0].rows.length;
		var iteration = nextRow + ROW_BASE;
		if (num == null) { 
			num = nextRow;
		} else {
			iteration = num + ROW_BASE;
		}
		
		// add the row
		var row = tbl.tBodies[0].insertRow(num);
		
		// CONFIG: requires classes named classy0 and classy1
		row.className = 'classy' + (iteration % 2);
	
		// CONFIG: This whole section can be configured
		
		// cell 0 - text
		var cell0 = row.insertCell(0);
		var textNode = document.createTextNode(iteration);
		cell0.appendChild(textNode);
		
		// cell 1 - input text
		var cell1 = row.insertCell(1);
		var txtInpa = document.createElement('input');
		txtInpa.setAttribute('type', 'text');
		txtInpa.setAttribute('name', INPUT_NAME_CODIGO + iteration);
		txtInpa.setAttribute('id', INPUT_NAME_CODIGO + iteration);
		txtInpa.setAttribute('size', '5');
		txtInpa.setAttribute('value', codigo); // iteration included for debug purposes
		txtInpa.setAttribute('readonly', 'readonly');
		txtInpa.setAttribute('class', 'form-control input-sm'); 
		cell1.appendChild(txtInpa);	
		
		// cell 2 - input text
		var cell2 = row.insertCell(2);
		var txtInpb = document.createElement('input');
		txtInpb.setAttribute('type', 'text');
		txtInpb.setAttribute('name', INPUT_NAME_DES + iteration);
		txtInpb.setAttribute('id', INPUT_NAME_DES + iteration);
		txtInpb.setAttribute('size', '30');
	    txtInpb.setAttribute('value', des); // iteration included for debug purposes 
	    txtInpb.setAttribute('readonly', 'readonly'); // iteration included for debug 
	    txtInpb.setAttribute('class', 'form-control input-sm'); 	
		cell2.appendChild(txtInpb);	
		
		// cell 3 - input text
		var cell3 = row.insertCell(3);
		var txtInpc = document.createElement('input');
		txtInpc.setAttribute('type', 'text');
		txtInpc.setAttribute('name', INPUT_NAME_Precio  + iteration);
		txtInpc.setAttribute('id', INPUT_NAME_Precio + iteration);
		txtInpc.setAttribute('size', '5');
		txtInpc.setAttribute('value', Precio); // iteration included for debug purposes 
		txtInpc.setAttribute('readonly', 'readonly');
		txtInpc.setAttribute('class', 'form-control input-sm'); 		
		cell3.appendChild(txtInpc);			
		
		// cell 4 - input text			
		var cell4 = row.insertCell(4);
		var txtInpd = document.createElement('input');
		txtInpd.setAttribute('type', 'text');
		txtInpd.setAttribute('name', INPUT_NAME_CAN + iteration);
		txtInpd.setAttribute('id', INPUT_NAME_CAN + iteration);
		txtInpd.setAttribute('size', '3');
	    txtInpd.setAttribute('value', can); // iteration included for debug purposes 
	    txtInpd.setAttribute('class', 'form-control input-sm'); 
		//txtInpd.setAttribute('onkeyup','validarcambiocantidad(this.name)');	
		cell4.appendChild(txtInpd);		

		// cell 5 - input text
		var cell5 = row.insertCell(5);
		var txtInpe = document.createElement('input');
		txtInpe.setAttribute('type', 'hidden');
		txtInpe.setAttribute('name', INPUT_NAME_IDPRODUCTO + iteration);
		txtInpe.setAttribute('id', INPUT_NAME_IDPRODUCTO + iteration);
		txtInpe.setAttribute('size', '2');
	    txtInpe.setAttribute('value', IdProducto); // iteration included for debug purposes	    
	    txtInpe.setAttribute('readonly', 'readonly');
		txtInpe.setAttribute('class', 'form-control input-sm'); 
		//txtInpe.setAttribute('onFocus','abrirmodalEqp(this.name)');		
		cell5.appendChild(txtInpe);			
		
		// cell 6 - select
		var cell6 = row.insertCell(6);		
		//Create and append select list
		var txtInpf = document.createElement("select");		
		//Create and append the options
		$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarRecetaDosis", function(json){												
				$.each(json, function(i, obj){		
					var option = document.createElement("option");				
					option.value =  obj.val;
					option.text = obj.text;
					//selected
					if(option.value == idDosisRecetada){
						option.setAttribute('selected', idDosisRecetada);	
					}
					txtInpf.appendChild(option);						 										
				});	// end of each				
		}); // end of getJSON	
		//var txtInpf = document.createElement('input');
		//txtInpf.setAttribute('type', 'text');
		txtInpf.setAttribute('name', INPUT_NAME_DOSIS + iteration);
		txtInpf.setAttribute('id', INPUT_NAME_DOSIS + iteration);
		//txtInpf.setAttribute('size', '5');
		//txtInpf.setAttribute('value', idDosisRecetada); // iteration included for debug purposes	
		txtInpf.setAttribute('class', 'form-control input-sm'); 		
		cell6.appendChild(txtInpf);		
		
		// cell 7 - select		
		var cell7 = row.insertCell(7);				
		//Create and append select list
		var txtInpg = document.createElement("select");		
		//Create and append the options
		$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarRecetaViaAdministracion", function(json){												
				$.each(json, function(i, obj){										
					//txtInpg.append($('<option>').text(obj.text).attr('value', obj.val));	
					var option = document.createElement("option");
					option.value =  obj.val;
					option.text = obj.text;	
					//selected				
					if(option.value == IdViaAdministracion){
						option.setAttribute('selected', IdViaAdministracion);	
					}					
					txtInpg.appendChild(option);													
				});						
		});	
		//var txtInpg = document.createElement('input');
		//txtInpg.setAttribute('type', 'text');
		txtInpg.setAttribute('name', INPUT_NAME_VIA + iteration);
		txtInpg.setAttribute('id', INPUT_NAME_VIA + iteration);
		//txtInpg.setAttribute('size', '5');
		//txtInpg.setAttribute('value', IdViaAdministracion); // iteration included for debug purposes		
		txtInpg.setAttribute('class', 'form-control input-sm'); 
		cell7.appendChild(txtInpg);	
		
		// cell 8 - input text
		var cell8 = row.insertCell(8);
		var txtInph = document.createElement('input');
		txtInph.setAttribute('type', 'checkbox');
		if(document.getElementById('HaySaldo').checked == true){
			txtInph.setAttribute('checked', 'true');	
		}
		txtInph.setAttribute('name', INPUT_NAME_HAYSALDO + iteration);
		txtInph.setAttribute('id', INPUT_NAME_HAYSALDO + iteration);
		txtInph.setAttribute('size', '4');
	    txtInph.setAttribute('value', HaySaldo); // iteration included for debug purposes	   
		txtInph.setAttribute('class', 'form-control input-sm'); 		
		cell8.appendChild(txtInph);	
		
		// cell 9 - input text
		var cell9 = row.insertCell(9);
		var txtInpi = document.createElement('input');
		txtInpi.setAttribute('type', 'text');
		txtInpi.setAttribute('name', INPUT_NAME_FRECUENCIA + iteration);
		txtInpi.setAttribute('id', INPUT_NAME_FRECUENCIA + iteration);
		txtInpi.setAttribute('size', '10');
	    txtInpi.setAttribute('value', Frecuencia); // iteration included for debug purposes	    
	    //txtInpi.setAttribute('readonly', 'readonly');
		txtInpi.setAttribute('class', 'form-control input-sm'); 		
		cell9.appendChild(txtInpi);	

		var cellE1 = row.insertCell(10);
		var btnEl = document.createElement('input');
		btnEl.setAttribute('type', 'button');
		btnEl.setAttribute('value', 'Delete');
		btnEl.setAttribute('class', 'btn btn-danger btn-sm'); 
		btnEl.onclick = function () {deleteCurrentRow(this)};
		cellE1.appendChild(btnEl);
		
		row.myRow = new myRowObject(textNode,txtInpa,txtInpb,txtInpc,txtInpd,txtInpe,txtInpf,txtInpg,txtInph,txtInpi);
	
	}
}

function deleteCurrentRow(obj)
{	
	if (hasLoaded) {			
		var delRow = obj.parentNode.parentNode;
		var tbl = delRow.parentNode.parentNode;
		var rIndex = delRow.sectionRowIndex;		
		var rowArray = new Array(delRow);
		
		//DesbloquearEquiposQuit(rIndex);//desbloqueo equipo		
		deleteRows(rowArray);		
	    reorderRows(tbl, rIndex);
		
	}
}

function reorderRows(tbl, startingIndex)
{
	if (hasLoaded) {
		if (tbl.tBodies[0].rows[startingIndex]) {
			var count = startingIndex + ROW_BASE;
			for (var i=startingIndex; i<tbl.tBodies[0].rows.length; i++) {
			
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.one.data = count; // text
				
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.two.name = INPUT_NAME_CODIGO + count; // input text							
				tbl.tBodies[0].rows[i].myRow.tres.name = INPUT_NAME_DES + count;
				tbl.tBodies[0].rows[i].myRow.cuatro.name = INPUT_NAME_Precio + count;
				
				tbl.tBodies[0].rows[i].myRow.cinco.name = INPUT_NAME_CAN + count;	
				tbl.tBodies[0].rows[i].myRow.seis.name = INPUT_NAME_IDPRODUCTO + count;			
									
				tbl.tBodies[0].rows[i].myRow.siete.name = INPUT_NAME_DOSIS + count;
				tbl.tBodies[0].rows[i].myRow.ocho.name = INPUT_NAME_VIA + count;		
				tbl.tBodies[0].rows[i].myRow.nueve.name = INPUT_NAME_HAYSALDO + count;
				tbl.tBodies[0].rows[i].myRow.diez.name = INPUT_NAME_FRECUENCIA + count;
				
				// CONFIG: next line is affected by myRowObject settings
				var tempVal = tbl.tBodies[0].rows[i].myRow.two.value.split(' '); 
			    tbl.tBodies[0].rows[i].className = 'classy' + (count % 2);
				
				count++;
				
			}
		}
	}
}

function deleteRows(rowObjArray)
{
	if (hasLoaded) {
		for (var i=0; i<rowObjArray.length; i++) {
			var rIndex = rowObjArray[i].sectionRowIndex;
			rowObjArray[i].parentNode.deleteRow(rIndex);
		}
	}
}

/*function validarcambiocantidad(obj){
	var cant=obj;//cantidadX
	var condicion=cant.substring(0, 8);//devuelve CantidadPedida
	//var condicion2=cant.substring(0, 6);
	
	if(condicion=='CantidadPedida'){
		var nc=cant.substring(8,10);//devuelve index CantidadPedida		
	}	
	
	var canti=document.getElementById("CantidadPedida"+nc).value; //cantidad de producto
	//var tipopod=document.getElementById("c_producto"+nc).value;
	
}*/

/*function validarcantidad(){		
		var descripcion=document.getElementById('descripcion').value;
		var c_codprd=document.getElementById('c_codprd').value;
		if(descripcion==""){
			var mensje = "Falta Ingresar un Producto ...!!!";
			alert(mensje);
			document.getElementById('CantidadPedida').value='0';
		}else if(c_codprd==""){
			var mensje = "Busque y Seleccione un Producto ...!!!";
			alert(mensje);
			document.getElementById('CantidadPedida').value='0';
		}else{		
			var CantidadPedida=document.getElementById('CantidadPedida').value;
			var patron=/^\d+(\.\d{1,2})?$/;
				if(!patron.test(CantidadPedida)){		
				//window.alert('monto ingresado incorrecto');
				document.getElementById('CantidadPedida').value='0';
				document.getElementById('CantidadPedida').focus();
				return false;
				}
		}		
}*/

function agregar(){
	
	  var theTable = document.getElementById('tblSample');
		cantFilas = theTable.rows.length;//a partir de cantFilas==2 la tabla esta llena
		if(cantFilas>1){
			for(i=1;i<cantFilas;i++){
				if((document.form1.c_desprd.value)==document.getElementsByName("c_desprd"+i)[0].value){					
					//mensje = "Producto Seleccionado ya está en el Registro";
					mensje = "Producto "+(document.form1.c_desprdS.value)+" se repite en el Registro";
					alert(mensje);
					return 0;
				}				
			}
		}//end if
	
	    if((document.form1.c_codprd.value)==""){
			mensje = "Falta Seleccionar un Medicamento/Insumo";
			alert(mensje);
			document.getElementById("descripcion").focus();			
		}else if((document.form1.CantidadPedida.value)=="0" || (document.form1.CantidadPedida.value)==""){
			mensje = "Falta Ingresar Cantidad";
			alert(mensje);	
		}else{        
			addRowToTable();		
			limpiar();			
		}	
	}
	
function limpiar(){
	//document.form1.c_codprd.value='';
	//document.form1.c_desprd.value='';
	$("#c_codprd").val('');
	$("#c_desprd").val('');	
	$("#CantidadPedida").val('1');		
	$("#IdProducto").val('');
	$("#IdViaAdministracion").val('');
	$("#idDosisRecetada").val('');					
	$("#Precio").val('');		
	$("#descripcion").val('');
	$("#Frecuencia").val('');	
}
	
</script>
<!--FIN GRILLA Medicamento/Insumo HASTA Linea 445--> 


<!--GRILLA DETALLE Procedimiento-->

<script type="text/javascript">
		
var INPUT_NAME_CODIGOS = 'c_codprdS'; // this is being set via script a
var INPUT_NAME_PrecioS = 'PrecioS'; // this is being set via script a
var INPUT_NAME_DESS = 'c_desprdS'; // this is being set via script b
var INPUT_NAME_CANS = 'CantidadPedidaS'; // this is being set via script c
//var INPUT_NAME_VIAS = 'IdViaAdministracionS'; // this is being set via script e
//var INPUT_NAME_DOSISS = 'idDosisRecetadaS';
var INPUT_NAME_IDPRODUCTOS = 'IdProductoS'; // this is being set via script g
var INPUT_NAME_HAYSALDOS = 'HaySaldoS'; // this is being set via script g
var INPUT_NAME_FRECUENCIAS = 'FrecuenciaS'; // this is being set via script g

var TABLE_NAMES = 'tblSampleServicio'; // this should be named in the HTML
var ROW_BASE = 1; // first number (for display)
/*var hasLoaded = false;
window.onload=fillInRows;
function fillInRows()
{
	hasLoaded = true;	
}*/
// CONFIG:
// myRowObject is an object for storing information about the table rows
function myRowObjectS(one,two,tres,cuatro,cinco,seis,siete,ocho,nueve,diez)
{
	this.one = one; // text object
	this.two = two; // input text object
	this.tres=tres;
	this.cuatro=cuatro;
	this.cinco=cinco;
	this.seis=seis;	
	this.siete=siete;
	this.ocho=ocho;
	this.nueve=nueve;
	this.diez=diez;
}

/*function insertRowToTable()
{
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAMES);
		var rowToInsertAt = tbl.tBodies[0].rows.length;
		for (var i=0; i<tbl.tBodies[0].rows.length; i++) {
			if (tbl.tBodies[0].rows[i].myRow) {
				rowToInsertAt = i;
				break;
			}
		}
		addRowToTableS(rowToInsertAt);
reorderRows(tbl, rowToInsertAt);
	}
}*/

function addRowToTableS(num)
{
	//alert('hola');
	var codigo=document.getElementById("c_codprdS").value
	var Precio=document.getElementById("PrecioS").value
	var des=document.getElementById("c_desprdS").value	
	var can=document.getElementById("CantidadPedidaS").value
	var IdProducto=document.getElementById("IdProductoS").value	
	//var IdViaAdministracion=document.getElementById("IdViaAdministracion").value
	//var idDosisRecetada=document.getElementById("idDosisRecetada").value
	var HaySaldo=document.getElementById("HaySaldoS").value
	var Frecuencia=document.getElementById("observaciones").value
		
	if (hasLoaded) {
		var tbl = document.getElementById(TABLE_NAMES);
		var nextRow = tbl.tBodies[0].rows.length;
		var iteration = nextRow + ROW_BASE;
		if (num == null) { 
			num = nextRow;
		} else {
			iteration = num + ROW_BASE;
		}
		
		// add the row
		var row = tbl.tBodies[0].insertRow(num);
		
		// CONFIG: requires classes named classy0 and classy1
		row.className = 'classy' + (iteration % 2);
	
		// CONFIG: This whole section can be configured
		
		// cell 0 - text
		var cell0 = row.insertCell(0);
		var textNode = document.createTextNode(iteration);
		cell0.appendChild(textNode);
		
		// cell 1 - input text
		var cell1 = row.insertCell(1);
		var txtInpa = document.createElement('input');
		txtInpa.setAttribute('type', 'text');
		txtInpa.setAttribute('name', INPUT_NAME_CODIGOS + iteration);
		txtInpa.setAttribute('id', INPUT_NAME_CODIGOS + iteration);
		txtInpa.setAttribute('size', '5');
		txtInpa.setAttribute('value', codigo); // iteration included for debug purposes
		txtInpa.setAttribute('readonly', 'readonly');
		txtInpa.setAttribute('class', 'form-control input-sm'); 
		cell1.appendChild(txtInpa);	
		
		// cell 2 - input text
		var cell2 = row.insertCell(2);
		var txtInpb = document.createElement('input');
		txtInpb.setAttribute('type', 'text');
		txtInpb.setAttribute('name', INPUT_NAME_DESS + iteration);
		txtInpb.setAttribute('id', INPUT_NAME_DESS + iteration);
		txtInpb.setAttribute('size', '30');
	    txtInpb.setAttribute('value', des); // iteration included for debug purposes 
	    txtInpb.setAttribute('readonly', 'readonly'); // iteration included for debug 
	    txtInpb.setAttribute('class', 'form-control input-sm'); 	
		cell2.appendChild(txtInpb);	
		
		// cell 3 - input text
		var cell3 = row.insertCell(3);
		var txtInpc = document.createElement('input');
		txtInpc.setAttribute('type', 'text');
		txtInpc.setAttribute('name', INPUT_NAME_PrecioS  + iteration);
		txtInpc.setAttribute('id', INPUT_NAME_PrecioS + iteration);
		txtInpc.setAttribute('size', '5');
		txtInpc.setAttribute('value', Precio); // iteration included for debug purposes 
		txtInpc.setAttribute('readonly', 'readonly');
		txtInpc.setAttribute('class', 'form-control input-sm'); 		
		cell3.appendChild(txtInpc);			
		
		// cell 4 - input text			
		var cell4 = row.insertCell(4);
		var txtInpd = document.createElement('input');
		txtInpd.setAttribute('type', 'text');
		txtInpd.setAttribute('name', INPUT_NAME_CANS + iteration);
		txtInpd.setAttribute('id', INPUT_NAME_CANS + iteration);
		txtInpd.setAttribute('size', '3');
	    txtInpd.setAttribute('value', can); // iteration included for debug purposes 
	    txtInpd.setAttribute('class', 'form-control input-sm'); 
		//txtInpd.setAttribute('onkeyup','validarcambiocantidad(this.name)');	
		cell4.appendChild(txtInpd);		

		// cell 5 - input text
		var cell5 = row.insertCell(5);
		var txtInpe = document.createElement('input');
		txtInpe.setAttribute('type', 'hidden');
		txtInpe.setAttribute('name', INPUT_NAME_IDPRODUCTOS + iteration);
		txtInpe.setAttribute('id', INPUT_NAME_IDPRODUCTOS + iteration);
		txtInpe.setAttribute('size', '2');
	    txtInpe.setAttribute('value', IdProducto); // iteration included for debug purposes	    
	    txtInpe.setAttribute('readonly', 'readonly');
		txtInpe.setAttribute('class', 'form-control input-sm'); 
		//txtInpe.setAttribute('onFocus','abrirmodalEqp(this.name)');		
		cell5.appendChild(txtInpe);				
		
		// cell 8 - input text
		var cell8 = row.insertCell(6);
		var txtInph = document.createElement('input');
		txtInph.setAttribute('type', 'checkbox');
		if(document.getElementById('HaySaldoS').checked == true){
			txtInph.setAttribute('checked', 'true');	
		}
		txtInph.setAttribute('name', INPUT_NAME_HAYSALDOS + iteration);
		txtInph.setAttribute('id', INPUT_NAME_HAYSALDOS + iteration);
		txtInph.setAttribute('size', '4');
	    txtInph.setAttribute('value', HaySaldo); // iteration included for debug purposes	   
		txtInph.setAttribute('class', 'form-control input-sm'); 		
		cell8.appendChild(txtInph);	
		
		// cell 9 - input text
		var cell9 = row.insertCell(7);
		var txtInpi = document.createElement('input');
		txtInpi.setAttribute('type', 'text');
		txtInpi.setAttribute('name', INPUT_NAME_FRECUENCIAS + iteration);
		txtInpi.setAttribute('id', INPUT_NAME_FRECUENCIAS + iteration);
		txtInpi.setAttribute('size', '10');
	    txtInpi.setAttribute('value', Frecuencia); // iteration included for debug purposes	    
	    //txtInpi.setAttribute('readonly', 'readonly');
		txtInpi.setAttribute('class', 'form-control input-sm'); 		
		cell9.appendChild(txtInpi);	

		var cellE1 = row.insertCell(8);
		var btnEl = document.createElement('input');
		btnEl.setAttribute('type', 'button');
		btnEl.setAttribute('value', 'Delete');
		btnEl.setAttribute('class', 'btn btn-danger btn-sm'); 
		btnEl.onclick = function () {deleteCurrentRowS(this)};
		cellE1.appendChild(btnEl);
		
		row.myRow = new myRowObjectS(textNode,txtInpa,txtInpb,txtInpc,txtInpd,txtInpe,txtInph,txtInpi); //,txtInpf,txtInpg
	
	}
}

function deleteCurrentRowS(obj)
{	
	if (hasLoaded) {			
		var delRow = obj.parentNode.parentNode;
		var tbl = delRow.parentNode.parentNode;
		var rIndex = delRow.sectionRowIndex;		
		var rowArray = new Array(delRow);
		
		//DesbloquearEquiposQuit(rIndex);//desbloqueo equipo		
		deleteRowsS(rowArray);		
	    reorderRows(tbl, rIndex);
		
	}
}

function reorderRows(tbl, startingIndex)
{
	if (hasLoaded) {
		if (tbl.tBodies[0].rows[startingIndex]) {
			var count = startingIndex + ROW_BASE;
			for (var i=startingIndex; i<tbl.tBodies[0].rows.length; i++) {
			
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.one.data = count; // text
				
				// CONFIG: next line is affected by myRowObject settings
				tbl.tBodies[0].rows[i].myRow.two.name = INPUT_NAME_CODIGO + count; // input text							
				tbl.tBodies[0].rows[i].myRow.tres.name = INPUT_NAME_DES + count;
				tbl.tBodies[0].rows[i].myRow.cuatro.name = INPUT_NAME_Precio + count;
				
				tbl.tBodies[0].rows[i].myRow.cinco.name = INPUT_NAME_CAN + count;	
				tbl.tBodies[0].rows[i].myRow.seis.name = INPUT_NAME_IDPRODUCTO + count;			
									
				//tbl.tBodies[0].rows[i].myRow.siete.name = INPUT_NAME_DOSIS + count;
				//tbl.tBodies[0].rows[i].myRow.ocho.name = INPUT_NAME_VIA + count;		
				tbl.tBodies[0].rows[i].myRow.siete.name = INPUT_NAME_HAYSALDO + count;
				tbl.tBodies[0].rows[i].myRow.ocho.name = INPUT_NAME_FRECUENCIA + count;
				
				// CONFIG: next line is affected by myRowObject settings
				var tempVal = tbl.tBodies[0].rows[i].myRow.two.value.split(' '); 
			    tbl.tBodies[0].rows[i].className = 'classy' + (count % 2);
				
				count++;
				
			}
		}
	}
}

function deleteRowsS(rowObjArray)
{
	if (hasLoaded) {
		for (var i=0; i<rowObjArray.length; i++) {
			var rIndex = rowObjArray[i].sectionRowIndex;
			rowObjArray[i].parentNode.deleteRow(rIndex);
		}
	}
}

function agregarServicio(){
	
	  var theTable = document.getElementById('tblSampleServicio');
		cantFilas = theTable.rows.length;//a partir de cantFilas==2 la tabla esta llena
		if(cantFilas>1){
			for(i=1;i<cantFilas;i++){
				if((document.form1.c_desprdS.value)==document.getElementsByName("c_desprdS"+i)[0].value){					
					//mensje = "Servicio Seleccionado ya está en el Registro";
					mensje = "Servicio "+(document.form1.c_desprdS.value)+" se repite en el Registro";
					alert(mensje);
					return 0;
				}				
			}
		}//end if
	
	    if((document.form1.c_codprdS.value)==""){
			mensje = "Falta Seleccionar un Medicamento/Insumo";
			alert(mensje);
			document.getElementById("descripcionS").focus();			
		}else if((document.form1.CantidadPedidaS.value)=="0" || (document.form1.CantidadPedidaS.value)==""){
			mensje = "Falta Ingresar Cantidad";
			alert(mensje);	
		}else{        
			addRowToTableS();		
			limpiarS();			
		}	
	}
	
function limpiarS(){
	//document.form1.c_codprd.value='';
	//document.form1.c_desprd.value='';
	$("#c_codprdS").val('');
	$("#c_desprdS").val('');	
	$("#CantidadPedidaS").val('1');		
	$("#IdProductoS").val('');
	//$("#IdViaAdministracion").val('');
	//$("#idDosisRecetada").val('');					
	$("#PrecioS").val('');		
	$("#descripcionS").val('');
	$("#observaciones").val('');
		
}
	
</script>
<!--FIN GRILLA Servicio--> 

<script type="text/javascript">

function llevarViaAdministracion(){
	$.getJSON("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=ListarRecetaViaAdministracion", function(json){												
				$.each(json, function(i, obj){										
					$('#IdViaAdministracion1').append($('<option>').text(obj.text).attr('value', obj.val));													
				});						
	});	
}

//validar numeros
 function validaDecimal(e){	 //solo acepta numeros y punto 
	tecla = (document.all) ? e.keyCode : e.which;//obtenemos el codigo ascii de la tecla
	if (tecla==8) return true;//backspace en ascii es 8
	patron=/[0-9\.]/; 
	te = String.fromCharCode(tecla);//convertimos el codigo ascii a string
	return patron.test(te);
} 

/*function validarnumero(){		
	var CantidadPedida=document.getElementById('CantidadPedida').value;
	var patron=/^\d+(\.\d{1,2})?$/;
		if(!patron.test(CantidadPedida)){		
		//window.alert('monto ingresado incorrecto');
		document.getElementById('CantidadPedida').value='';
		document.getElementById('CantidadPedida').focus();
		return false;
		}		
}*/

</script>
</head>

<body>

<!--modal de ver productos-->
<div class="modal fade" id="my_modalProd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<form class="form-horizontal" id="frmproducto" name="frmproducto" action="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">      
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="exampleModalLabel">Medicamentos e Insumos</h4>
            
            <div class="col-xs-6">            
                <input id="codigo" name="codigo" placeholder="Busque por Codigo de Producto" onKeyUp="abrirmodalProd2()" type="text" class="form-control input-sm" />           	
            </div> 
            
            <div class="col-xs-6">            
                <input type="text" id="criterio" name="criterio" placeholder="Busque por Descripcion de Producto" onKeyUp="abrirmodalProd2()"  class="form-control input-sm" />          	
            </div>     
        </div>
        
      	<div class="modal-body">
            <table id="tablaProd" class="table table-hover" style="font-size:12px;">
        		<!--Contenido se encuentra en verProductos.php-->
            </table> 
        </div>
      </div>
    </div>
    </form>
  </div>
 <!--fin modal de ver productos-->
 
 <!--modal de ver servicios-->
<div class="modal fade" id="my_modalServ" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<form class="form-horizontal" id="frmservicio" name="frmservicio" action="#">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">      
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="exampleModalLabel">Servicios</h4>
            
            <div class="col-xs-6">            
                <input id="codigo" name="codigo" placeholder="Busque por Codigo de Servicio" onKeyUp="abrirmodalServ2()" type="text" class="form-control input-sm" />           	
            </div> 
            
            <div class="col-xs-6">            
                <input type="text" id="criterio" name="criterio" placeholder="Busque por Descripcion de Servicio" onKeyUp="abrirmodalServ2()"  class="form-control input-sm" />          	
            </div>     
        </div>
        
      	<div class="modal-body">
            <table id="tablaServ" class="table table-hover" style="font-size:12px;">
        		<!--Contenido se encuentra en verServicios.php-->
            </table> 
        </div>
      </div>
    </div>
    </form>
  </div>
 <!--fin modal de ver servicios-->
 
<!--modal de ver paquetes-->
<div class="modal fade" id="my_modalPaque" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<form class="form-horizontal" id="frmpaquete" name="frmpaquete" action="#">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">      
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="exampleModalLabel">Paquetes</h4>
            
            <div class="col-xs-6">            
                <input id="codigo" name="codigo" placeholder="Busque por Codigo de Paquete" onKeyUp="abrirmodalPaque2()" type="text" class="form-control input-sm" />           	
            </div> 
            
            <div class="col-xs-6">            
                <input type="text" id="criterio" name="criterio" placeholder="Busque por Descripcion de Paquete" onKeyUp="abrirmodalPaque2()"  class="form-control input-sm" />          	
            </div>     
        </div>
        
      	<div class="modal-body">
            <div id="tablaPaque">
        		<!--Contenido se encuentra en verPaquetes.php-->
            </div> 
        </div>
      </div>
    </div>
    </form>
  </div>
 <!--fin modal de ver paquetes-->

    <div id="wrapper">
        <div id="page-wrapper">
        
        	<div class="row">				
				<?php  include('../../MVC_Vista/RecetasMedicas/Cabecera.php'); ?>
			</div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">RECETAS MÉDICAS</h3>
                </div>                
            </div>
            <!-- /.row -->
            
            <div class="row">
            
                <div class="container-fluid">               
                   <div class="col-md-12">
                            <div class="panel panel-primary with-nav-tabs"> <!--with-nav-tabs--> 
                            	<!--<div class="panel-heading">REGISTRAR RECETA</div>-->
                                <div class="panel-heading"  >
                                        <ul class="nav nav-tabs">
                                        	<li class="active"><a href="#tabcabecera" data-toggle="tab">Cabecera</a></li>
                                            <li class="desabilitar"><a href="#tab1primary" data-toggle="tab">Farmacia</a></li>
                                            <li class = "desabilitar"><a href="#tab2primary" data-toggle="tab">Servicio</a></li>
                                            <li class = "desabilitar"><a href="#tab3primary" data-toggle="tab">Dieta</a></li>
                                            <!--<li class = "desabilitar"><a href="#tab4primary" data-toggle="tab">Salida</a></li>-->         
                                        </ul>
                                </div>
                                <div class="panel-body">
                                	<form class="form-horizontal" id="form1" name="form1" method="post" action="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=GuardarReceta&IdEmpleado=<?php echo $IdEmpleado ?>"  >
                                       <div class="form-control-static" align="right">
                                         <input class="btn btn-success" type="button" onClick="validarguardar()" value="Registrar"/>
                                         &nbsp;<a class="btn btn-danger" onClick="cancelar();">Cancelar</a>
                                         <!--&nbsp;<a class="btn btn-warning" onClick="location.reload();" >Refrescar</a>&nbsp;
                                         &nbsp;<a class="btn btn-info" onClick="salir();">Salir</a>&nbsp;-->
                                        </div>
                                  <div class="tab-content">                                  		
                                  <!--Inicio Identificacion-->
                                        <div class="tab-pane fade in active" id="tabcabecera">                                     
                                                <fieldset>
					  							<legend>Datos Paciente</legend>
                                                    <!--fila 1-->
                                                    <div class="form-group">
                                                    	
                                                        <input name="Servicio" id="Servicio" type="hidden" value="<?php echo $Servicio ?>" />                                                                                                         
                                                        <input name="IdMedico" id="IdMedico" type="hidden" value="<?php echo $_SESSION['IdMedico']; ?>" /> 
                                                        
                                                        <input name="IdPaciente" id="IdPaciente" type="hidden" value="<?php echo $IdPaciente ?>" />  
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres:</label>
                                                        <div class="col-xs-2">
                                                        	<div class="input-sm"> <?php echo $Paciente ?> </div>
                                                            <?php /*?><input type="text" class="form-control input-sm" value="<?php echo $Paciente ?>" /><?php */?>                                        				
                                                        </div>
                                                            
                                                        <label class="control-label col-xs-1">Fecha Nac.:</label>
                                                        <div class="col-xs-1">  
                                                        	<div class="input-sm"> <?php echo $FechaNacimiento ?> </div>
                                                       	</div>                     
                                                        
                                                        <label class="control-label col-xs-1">Edad:</label>
                                                        <div class="col-xs-1"> 
                                                        	<div class="input-sm"> <?php echo $EDAD;  ?> </div>
                                                        </div> 
                                                        
                                                        <label class="control-label col-xs-1">Sexo</label>
                                                        <div class="col-xs-1"> 
                                                        	<div class="input-sm"> <?php echo $Sexo ?> </div>                                                        
                                                        </div> 
                                                        <?php if($GrupoSanguineo!=NULL){ ?>
                                                        <label class="control-label col-xs-1">Grupo Sang.:</label>
                                                        <div class="col-xs-1">
                                                        	<div class="input-sm"> <?php echo $GrupoSanguineo ?> </div>
                                                        </div>                             
                                                        <?php } ?>  
                                                    </div> 
                                                    <!--FIN fila 1--> 
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">
                                                    	
                                                                                                                 
                                                    	                            
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>
                                                  
                                                  <fieldset>
					  							  <legend>Datos Atención Paciente</legend>                                    
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">   
                                                    	<label class="control-label col-xs-2">Nro Cuenta:</label>
                                                        <div class="col-xs-1">
                                                        	<div class="input-sm"> <?php echo $IdCuentaAtencion ?> </div>
                                                        </div>  
                                                        <label class="control-label col-xs-1">Financiamiento:</label>
                                                        <div class="col-xs-1">
                                                        	<div class="input-sm"> <?php echo $DescripcionFinanciamiento ?> </div>
                                                        </div>                                                  	
                                                        <label class="control-label col-xs-1">Servicio:</label>
                                                        <div class="col-xs-2"> 
                                                        	<div class="input-sm"> <?php echo $DescripcionServicio ?> </div> 
                                                        </div>                                                               
                                                        <label class="control-label col-xs-1">Fecha Ingreso:</label>
                                                        <div class="col-xs-1"> 
                                                        	<div class="input-sm"> <?php echo $FechaIngreso ?> </div>  
                                                        </div>                              
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>   
                                                  
                                                  <fieldset>
					  							  <legend>Datos Cabecera Receta</legend>                                    
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">                                                    	
                                                        <label class="control-label col-xs-2">Fecha Receta:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" name="FechaReceta" id="FechaReceta" value="<?php echo vfecha(substr($FechaServidor,0,10)); ?>" />                                                      	
                                                        </div>  
                                                             
                                                        <label class="control-label col-xs-1">Fecha Vigencia:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" name="fechaVigencia" id="fechaVigencia" value="<?php $nuevafecha = strtotime('+7 day' , strtotime ($FechaServidor)); $nuevafecha = date ( 'Y-m-j',$nuevafecha ); echo vfecha(substr($nuevafecha,0,10)); ?>" />                                                      	
                                                        </div>                              
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>   
                                                  
                                                  <fieldset>
					  							  <legend>Búsqueda de Paquetes</legend>                                    
                                                    
                                                    <!--fila 2-->
                                                    <div class="form-group">                                                    	
                                                        <label class="control-label col-xs-2">Lista de Paquetes:</label>
                                                        <div class="col-xs-3">  
                                                            <input type="text" class="form-control input-sm" name="NombrePaquete" id="NombrePaquete" onFocus="abrirmodalPaque();" placeholder="Nombre del Paquete" readonly />                                
                                                        </div>                                                               
                                                        <label class="control-label col-xs-1">Codigo:</label>
                                                        <div class="col-xs-1">  
                                                            <input type="text" class="form-control input-sm" name="CodigoPaquete" id="CodigoPaquete" readonly />                                                      		<input type="hidden" name="idFactPaquete" id="idFactPaquete" />
                                                        </div>                                     
                                                                     
                                                          
                                                    </div> 
                                                    <!--FIN fila 2-->                                                  
                                                                                                      
                                                  </fieldset>                                              
                                                 
                                                                                      
                                        
                                        </div>
                                   		<!--tabcabecera-->
                                        
                                    <!--Inicio tab1primary-->
                                        <div class="tab-pane fade" id="tab1primary">                                          
                                               
                                                
                                                    <!--fila 1-->
                                                    <div class="form-group">                                               
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres Paciente:</label>
                                                        <div class="col-xs-3">
                                                            <div class="input-sm"> <?php echo $Paciente ?> </div>                                                       
                                                        </div>                            
                                                          
                                                    </div> 
                                                    <!--FIN fila 1-->  
                                                                                                  
                                                  <fieldset>
					  							  <legend>Detalle Farmacia</legend>
                                                  
                                                     <div class="row">                                                       
                                                        <div class="col-xs-3">
                                                        <label class="control-label col-xs-3">Medicamento/Insumo</label>
                                                        </div>
                                                        <div class="col-xs-1"> 
                                                        <label class="control-label col-xs-1">Codigo</label>
                                                        </div>
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">PrecioUnit</label>
                                                        </div> 
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Cant</label>
                                                        </div>  
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">N°Dosis</label>
                                                        </div>                                                                                                              
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Via</label>
                                                        </div>
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">HaySaldo</label>
                                                        </div>
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-2">Frecuencia</label>
                                                        </div>                                                                         
                                                   </div> 
                       
                                                   <div class="row">                         
                                                       <div class="col-xs-3">                                                                                                           
                                                        <input  id="c_desprd" name="c_desprd"  type="hidden" />
                                                        <input autocomplete="off" id="descripcion" name="descripcion" class="form-control input-sm" type="text" placeholder="Nombre del producto" onFocus="abrirmodalProd();" readonly/>
                                                    </div>
                                                    <div class="col-xs-1">
                                                    	<input id="c_codprd" name="c_codprd" type="text" class="form-control input-sm" readonly />
                                                        <input name="IdProducto" type="hidden" class="form-control input-sm"  id="IdProducto" />                                                         
                                                    </div>                                                    
                                                    <div class="col-xs-1">                                                       
                                                        <input id="Precio" name="Precio" type="text" class="form-control input-sm" />         	
                                                    </div> 
                                                    <div class="col-xs-1">
                                                        <input name="CantidadPedida" type="text" class="form-control input-sm"  id="CantidadPedida" value="1" onKeyPress="return validaDecimal(event)" />  <!--onChange="validarnumero()"-->
                                                      
                                                    </div>  
                                                    <div class="col-xs-1">                                                     
                                                         <select id="idDosisRecetada" name="idDosisRecetada" class="form-control input-sm" >                                                            	<?php 
                                                                    $resultados=ListarRecetaDosis_M();								 
                                                                    if($resultados!=NULL){
                                                                    for ($i=0; $i < count($resultados); $i++) {	
                                                                 ?>
                                                             <option value="<?php echo $resultados[$i]["idDosis"] ?>"  ><?php echo mb_strtoupper($resultados[$i]["NumeroDosis"]) ?></option>
                                                             <?php 
                                                                }}
                                                             ?>                   
                                                        </select>        	
                                                    </div>                                                   
                                                    <div class="col-xs-1">
                                                    	<select id="IdViaAdministracion" name="IdViaAdministracion" class="form-control input-sm" >                                                            	<?php 
                                                                    $resultados=ListarRecetaViaAdministracion_M();								 
                                                                    if($resultados!=NULL){
                                                                    for ($i=0; $i < count($resultados); $i++) {	
                                                                 ?>
                                                             <option value="<?php echo $resultados[$i]["IdViaAdministracion"] ?>"  ><?php echo $resultados[$i]["IdViaAdministracion"].' | '.mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
                                                             <?php 
                                                                }}
                                                             ?>                   
                                                        </select>                                                        
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <input name="HaySaldo" id="HaySaldo" type="checkbox" class="form-control input-sm" value="1" /> 
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input name="Frecuencia" id="Frecuencia" type="text" class="form-control input-sm" /> 
                                                    </div>
                                                                       
                                                    <div class="col-xs-1">
                                                        <button class="btn btn-success btn-sm" id="btn-agregar" 
                                                        type="button" onClick="agregar();">
                                                             <i class="glyphicon glyphicon-plus"></i>
                                                        </button>                                                        
                                                    </div>
                                                    
                                                </div> 
                                                
                                                <hr />
                                                <table  id="tblSample" class="table table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>Codigo</th>
                                                      <th>Medicamento/Insumo</th> 
                                                      <th>Precio Unit.</th>  
                                                      <th>Cantidad</th> 
                                                      <th></th>     
                                                      <th>N°Dosis</th>       
                                                      <th>Via</th> 
                                                      <th>HaySaldo</th>       
                                                      <th>Frecuencia</th>   
                                                      <th>Delete</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                  </tbody>
                                                </table>                                  
                                                      
                                                    <!--fila 3-->                                                     
                                                     <!--<div class="form-group">                                                        
                                                        <div class="col-xs-1 text-right">  
                                                            <input type="button" value="Aceptar" onClick="Aceptar()" class="btn btn-group-sm btn-primary" />                                                        </div>                                                         
                                                      </div> -->
                                                      <!--FIN fila 3-->                                                   
                                                  </fieldset>
                                                 
                                                                                      
                                        
                                        </div>
                                   		<!--tab1primary-->
                                        
                                        <div class="tab-pane fade" id="tab2primary"> 
                                        	<!--fila 1-->
                                                    <div class="form-group">                                               
                                                        
                                                    	<label class="control-label col-xs-2">Apellidos y Nombres Paciente:</label>
                                                        <div class="col-xs-3">
                                                            <div class="input-sm"> <?php echo $Paciente ?> </div>                                                       
                                                        </div>                            
                                                          
                                                    </div> 
                                                    <!--FIN fila 1-->  
                                                                                                  
                                                  <fieldset>
					  							  <legend>Detalle Servicios</legend>
                                                  
                                                     <div class="row">  
                                                     	<!--<div class="col-xs-2">
                                                        <label class="control-label col-xs-2">Financiamiento</label>
                                                        </div> -->                                                      
                                                        <div class="col-xs-2">
                                                        <label class="control-label col-xs-2">Servicio</label>
                                                        </div>
                                                        <div class="col-xs-1"> 
                                                        <label class="control-label col-xs-1">Codigo</label>
                                                        </div>
                                                        <div class="col-xs-2">
                                                        <label class="control-label col-xs-2">PrecioUnitario</label>
                                                        </div> 
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Cant</label>
                                                        </div>  
                                                        <div class="col-xs-1">
                                                        <label class="control-label col-xs-1">Hay</label>
                                                        </div>                                                                                                              
                                                        <div class="col-xs-2">
                                                        <label class="control-label col-xs-2">Observaciones</label>
                                                        </div>
                                                                                                                                  
                                                   </div> 
                       
                                                   <div class="row"> 
                                                   	<?php /*?><div class="col-xs-2">                                                                                                         
                                                        <input type="text" class="form-control input-sm" value="<?php echo $DescripcionFinanciamiento ?>" disabled /> 
                                                    </div> <?php */?>                       
                                                    <div class="col-xs-2">                                                                                                          
                                                        <input  id="c_desprdS" name="c_desprdS"  type="hidden" />
                                                        <input autocomplete="off" id="descripcionS" name="descripcionS" class="form-control input-sm" type="text" placeholder="Nombre del Servicio" onFocus="abrirmodalServ();" readonly/>
                                                    </div>
                                                    <div class="col-xs-1">
                                                    	<input id="c_codprdS" name="c_codprdS" type="text" class="form-control input-sm" readonly />
                                                        <input name="IdProductoS" type="hidden" class="form-control input-sm"  id="IdProductoS" />                                                         
                                                    </div>                                                    
                                                    <div class="col-xs-2">                                                       
                                                        <input id="PrecioS" name="PrecioS" type="text" class="form-control input-sm" />         	
                                                    </div> 
                                                    <div class="col-xs-1">
                                                        <input name="CantidadPedidaS" type="text" class="form-control input-sm"  id="CantidadPedidaS" value="1" onKeyPress="return validaDecimal(event)" />  
                                                      
                                                    </div>  
                                                    <div class="col-xs-1">
                                                        <input name="HaySaldoS" id="HaySaldoS" type="checkbox" class="form-control input-sm" value="1" /> 
                                                    </div>                                                  
                                                    <div class="col-xs-2">
                                                    	<input name="observaciones" id="observaciones" type="text" class="form-control input-sm" />                                                        
                                                    </div>
                                                                       
                                                    <div class="col-xs-1">
                                                        <button class="btn btn-success btn-sm" id="btn-agregar" 
                                                        type="button" onClick="agregarServicio();">
                                                             <i class="glyphicon glyphicon-plus"></i>
                                                        </button>                                                        
                                                    </div>
                                                    
                                                </div> 
                                                
                                                <hr />
                                                <table  id="tblSampleServicio" class="table table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>Codigo</th>
                                                      <th>Procedimiento</th> 
                                                      <th>Precio Unit.</th>  
                                                      <th>Cantidad</th> 
                                                      <th></th>     
                                                      <th>Hay</th>       
                                                      <th>Observaciones</th>    
                                                      <th>Delete</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                  </tbody>
                                                </table>                                  
                                                      
                                                    <!--fila 3-->                                                     
                                                     <!--<div class="form-group">                                                        
                                                        <div class="col-xs-1 text-right">  
                                                            <input type="button" value="Aceptar" onClick="Aceptar()" class="btn btn-group-sm btn-primary" />                                                        </div>                                                         
                                                      </div> -->
                                                      <!--FIN fila 3-->                                                   
                                                  </fieldset>
                                                                           
                                        </div>
                                        <!--tab2primary-->
                                        
                                        <div class="tab-pane fade" id="tab3primary">                                                                    
                                        </div> 
                                        <!--tab3primary-->
                                                              
                                        <div class="tab-pane fade" id="tab4primary"> 
                                        </div>
                                        <!--tab4primary-->
                                    </div>
                                    </form>
                                </div>
                                
                            </div>
                        </div>
                </div>
                                
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    
     	<script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script>         
        
        <script type="text/javascript" src="../../MVC_Complemento/bootstrap/alertify/lib/alertify.js"></script>
        
       <!-- datepicker y autocompletado-->     
       <script type="text/javascript" src="../../MVC_Complemento/bootstrap/jquery-ui-themes-1.12.0/jquery-ui-1.12.0/jquery-ui.min.js"></script>
        
 <script type="text/javascript">
    $(function () {
	
		//Array para dar formato en español
        $.datepicker.regional['es'] =
        {
                    closeText: 'Cerrar',
                    prevText: 'Previo',
                    nextText: 'Próximo',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    dateFormat: 'dd/mm/yy', firstDay: 0,
                    initStatus: 'Selecciona la fecha', isRTL: false};
        			$.datepicker.setDefaults($.datepicker.regional['es']);

       				$("#FechaIngreso").datepicker();
					$("#FechaReceta").datepicker();
					$("#fechaVigencia").datepicker();
       });
   

   function Aceptar(){
	   
	   Paciente=document.getElementById('Paciente').value;
	   
	   if(Paciente.trim=='' || Paciente.trim()=='SELECCIONE'){
		   alertify.error("<b> Mensaje del Sistema: </b> <h1>Falta Buscar Paciente</h1>");
		   return 0;
	   }
	   
	   //document.getElementById('form1').submit();
	   alertify.confirm("Seguro de Registrar Receta al Paciente seleccionado", function (e) {
			if (e) {							
				document.getElementById('form1').submit();
			} else {
				//alertify.alert("Successful AJAX after Cancel");
				window.location.href = "../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $IdEmpleado ?>";
			}
		});	
	   
   }
   
   function cancelar(){
	   location.href="../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=Inicio&IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>";		
   }
   
 function abrirmodalProd(){
	document.frmproducto.criterio.value="";
	document.frmproducto.codigo.value="";
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';		
	$('#my_modalProd').modal('show');
	$('#tablaProd').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verProductos",{IdTipoFinanciamiento:IdTipoFinanciamiento});		
 }
 
 function abrirmodalProd2(){				
	var criterio=document.frmproducto.criterio.value;
	var codigo=document.frmproducto.codigo.value;	
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';				
	$('#my_modalProd').modal('show');		
	$('#tablaProd').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verProductos",{criterio:criterio,codigo:codigo,IdTipoFinanciamiento:IdTipoFinanciamiento});	
}

function abrirmodalServ(){
	document.frmservicio.criterio.value="";
	document.frmservicio.codigo.value="";
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';	
	$('#my_modalServ').modal('show');
	$('#tablaServ').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verServicios",{IdTipoFinanciamiento:IdTipoFinanciamiento});	
 }
 
 function abrirmodalServ2(){				
	var criterio=document.frmservicio.criterio.value;
	var codigo=document.frmservicio.codigo.value;	
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';			
	$('#my_modalServ').modal('show');	
	$('#tablaServ').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verServicios",{criterio:criterio,codigo:codigo,IdTipoFinanciamiento:IdTipoFinanciamiento});	
}

function abrirmodalPaque(){
	document.frmpaquete.criterio.value="";
	document.frmpaquete.codigo.value="";
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';	
	$('#my_modalPaque').modal('show');
	$('#tablaPaque').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verPaquetes",{IdTipoFinanciamiento:IdTipoFinanciamiento});	
 }
 
 function abrirmodalPaque2(){				
	var criterio=document.frmpaquete.criterio.value;
	var codigo=document.frmpaquete.codigo.value;	
	var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';			
	$('#my_modalPaque').modal('show');	
	$('#tablaPaque').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verPaquetes",{criterio:criterio,codigo:codigo,IdTipoFinanciamiento:IdTipoFinanciamiento});	
}

</script>  
</body>

</html>
