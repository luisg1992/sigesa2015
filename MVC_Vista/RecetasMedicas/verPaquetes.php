<!--usado para buscar Paquetes-->
<table id="tablaPaque" class="table table-hover" style="font-size:12px;">            	
    <thead>        
        <tr>
        	<th>Nro</th> 
            <th>Código</th> 
            <th>Descripcion</th>                                                              
            <th>Seleccionar</th>            
        </tr>       
        
    </thead>
    
    <tbody>  
      	
    <?php 		
		$data=$PaqueteBuscar;
		for ($i=0; $i < count($data); $i++) { 				
	?>	        
		<tr>
          <td><?php echo $i+1; ?></td>
          <td><?php echo $data[$i]['Codigo']; ?></td>
          <td><?php echo $data[$i]['Descripcion']; ?></td>                                 
          <?php /*?><td><a href="javascript:pon_prefijo('<?php echo $data[$i]['Codigo']?>','<?php echo $data[$i]['Descripcion']?>','<?php echo $data[$i]['idFactPaquete']?>')"><span class="glyphicon glyphicon-check"></span></a></td> <?php */?> 
          <td><a href="javascript:abrirmodalPaque3('<?php echo $data[$i]['Codigo']?>','<?php echo $data[$i]['Descripcion']?>','<?php echo $data[$i]['idFactPaquete']?>')"><span class="glyphicon glyphicon-check"></span></a></td>          
        </tr>
    <?php } ?>    	
   
    </tbody>

</table>

<?php 
	$idFactPaquete=(isset($_REQUEST['idFactPaquete']) ? $_REQUEST['idFactPaquete'] : "");
	if($idFactPaquete!=""){
		$dataDetalle=ListarCatalogoPaquetesXpaqueteM($idFactPaquete);
?>

<hr />
<div class="form-control-static" align="right">
 <input class="btn btn-success" type="button" onClick="Aceptar()" value="Aceptar"/>
 &nbsp;<a class="btn btn-danger" onClick="cancelarPaquetes();">Cancelar</a>
</div>

<form id="formEnviarDetalle" name="formEnviarDetalle" action="#" >
    <table id="tablaDetallePaque" class="table table-hover" style="font-size:12px;">            	
        <thead>        
            <tr>
                <th>Nro</th> 
                <th>Código</th> 
                <th>Descripcion</th> 
                <th>Precio Unitario</th>                                                              
                <th>Especialidad</th>            
            </tr>       
            
        </thead>
        
        <tbody>  
            
        <?php 		
            for ($i=0; $i < count($dataDetalle); $i++) { 
                $PrecioX=$dataDetalle[$i]['Precio']; 
                $Precio=number_format( $PrecioX , 2 , "." , " " );	
                $idPuntoCarga=$dataDetalle[$i]['idPuntoCarga']; 
                if($idPuntoCarga=='5'){
                    $tipo='Medicamento/Insumo';
                }else{
                    $tipo='Servicio';
                }			
        ?>	        
            <tr>
              <td><?php echo $i+1; ?></td>
              <td><?php echo $dataDetalle[$i]['Codigo']; ?>
              <input type="hidden" name="idProducto<?php echo ($i+1) ?>" id="idProducto<?php echo ($i+1) ?>" value="<?php echo $dataDetalle[$i]['idProducto']; ?>"  />
              <input type="hidden" name="Codigo<?php echo ($i+1) ?>" id="Codigo<?php echo ($i+1) ?>" value="<?php echo $dataDetalle[$i]['Codigo']; ?>"  />
              <input type="hidden" name="idPuntoCarga<?php echo ($i+1) ?>" id="idPuntoCarga<?php echo ($i+1) ?>" value="<?php echo $dataDetalle[$i]['idPuntoCarga']; ?>"  />
              </td>
              <td><?php echo $dataDetalle[$i]['Descripcion']; ?><input type="hidden" name="Descripcion<?php echo ($i+1) ?>" id="Descripcion<?php echo ($i+1) ?>" value="<?php echo $dataDetalle[$i]['Descripcion']; ?>"  /></td>                                 
              <td align="right"><?php echo 'S/ '.$Precio; ?><input type="hidden" name="Precio<?php echo ($i+1) ?>" id="Precio<?php echo ($i+1) ?>" value="<?php echo $Precio; ?>"  /></td> 
              <td><?php echo $tipo; ?></td>       
              	       
            </tr>
        <?php } ?>    	
       			<input type="hidden" name="maxitem" id="maxitem" value="<?php echo $i; ?>"  />  
        </tbody>
    
    </table>
</form>


<?php } ?>
   
<script type="text/javascript">	
	function abrirmodalPaque3(Codigo,Descripcion,idFactPaquete){
		//llenar datos formulario receta
		document.getElementById('CodigoPaquete').value=Codigo;	
		document.getElementById('NombrePaquete').value=Descripcion;		
		document.getElementById('idFactPaquete').value=idFactPaquete;
		//llenar tabla tablaDetallePaque				
		var criterio=document.frmpaquete.criterio.value;
		var codigo=document.frmpaquete.codigo.value;	
		var IdTipoFinanciamiento='<?php echo $IdTipoFinanciamiento ?>';			
		$('#my_modalPaque').modal('show');	
		$('#tablaPaque').load("../../MVC_Controlador/RecetasMedicas/RecetasMedicasC.php?acc=verPaquetes",{criterio:criterio,codigo:codigo,IdTipoFinanciamiento:IdTipoFinanciamiento,idFactPaquete:idFactPaquete});	
	}
	
	function cancelarPaquetes(){
		$('#my_modalPaque').modal('toggle');	//cerrar	
	}
	
	function Aceptar(){

		//Eliminar Medicamento/Insumo
 		var theTableFarmacia = document.getElementById('tblSample');		
		cantFilasFarmacia = theTableFarmacia.rows.length;//a partir de cantFilas==2 la tabla esta llena
		if(cantFilasFarmacia>1){			
			for(var k=1;k<cantFilasFarmacia;k++){
				//$("#tabla tbody tr:eq('"+k+"')").remove();
				document.getElementById("tblSample").deleteRow(1);
			}
		}
		
		//Eliminar Servicio
 		var theTableServicio = document.getElementById('tblSampleServicio');		
		cantFilasServicio = theTableServicio.rows.length;//a partir de cantFilas==2 la tabla esta llena
		if(cantFilasServicio>1){			
			for(m=1;m<cantFilasServicio;m++){
				//$("#tabla tbody tr:eq('"+m+"')").remove();	
				document.getElementById("tblSampleServicio").deleteRow(1);						
			}
		}
		
		//Agregar Medicamento y/o Servicio
		//var maxitem=0;
		maxitem=document.getElementById('maxitem').value;		
		for(var n=1; n<=maxitem; n++){
			idProducto=document.getElementById('idProducto'+n).value;
			codigo=document.getElementById('Codigo'+n).value;
			Descripcion=document.getElementById('Descripcion'+n).value;
			Precio=document.getElementById('Precio'+n).value;
			idPuntoCarga=document.getElementById('idPuntoCarga'+n).value;
			
			//CHECKED IDPUNTOCARGA			
			
			var IdPuntoCargaSel = document.getElementsByName('IdPuntoCargaSel');
			var len = IdPuntoCargaSel.length;
			for (var s=1; s<=len; s++) {
				if(document.getElementById('IdPuntoCargaSel'+s).value==document.getElementById('idPuntoCarga'+n).value){
					document.getElementById('IdPuntoCargaSel'+s).checked=true;			
				}			
			}
			//FIN CHECKED IDPUNTOCARGA
			
			if(idPuntoCarga=='5'){ //Medicamento/Insumo
				document.getElementById('IdProducto').value=idProducto;
				document.getElementById('c_codprd').value=codigo;
				document.getElementById('descripcion').value=Descripcion;
				document.getElementById('c_desprd').value=Descripcion;
				document.getElementById('Precio').value=Precio;
				agregar();
			}else{ //Servicio
				document.getElementById('IdProductoS').value=idProducto;
				document.getElementById('c_codprdS').value=codigo;
				document.getElementById('descripcionS').value=Descripcion;
				document.getElementById('c_desprdS').value=Descripcion;
				document.getElementById('PrecioS').value=Precio;
				agregarServicio();
			}
		}
		
		$('#my_modalPaque').modal('toggle');
		
	}
	
	
	/*function pon_prefijo(Codigo,Nombre,idFactPaquete) {				
		document.getElementById('CodigoPaquete').value=Codigo;	
		document.getElementById('NombrePaquete').value=Nombre;		
		document.getElementById('idFactPaquete').value=idFactPaquete;		
		$('#my_modalPaque').modal('toggle');	//cerrar		
	}*/
	
</script>
