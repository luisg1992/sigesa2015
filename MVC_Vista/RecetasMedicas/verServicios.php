<!--usado para buscar Servicios-->	
    <thead>        
        <tr>
        	<th>Nro</th> 
            <th>Código</th> 
            <th>Descripcion</th>       
            <th>Precio Unitario</th>                                                       
            <th>Seleccionar</th>          
            
        </tr>        
        
    </thead>
    <tbody>
    	
        <?php 		
		
		$data=$ServicioBuscar;
		for ($i=0; $i < count($data); $i++) { 
			$PrecioX=$data[$i]['PrecioUnitario']; 
			$Precio=number_format( $PrecioX , 2 , "." , " " ); 				
		?>	        
	
        <tr>
          <td><?php echo $i+1; ?></td>
          <td><?php echo $data[$i]['Codigo']; ?></td>
          <td><?php echo $data[$i]['Nombre']; ?></td>
          <td align="right"><?php echo 'S/ '.$Precio; ?></td>                        
          <td><a href="javascript:pon_prefijo('<?php echo $data[$i]['Codigo']?>','<?php echo $data[$i]['Nombre']?>','<?php echo $Precio?>','<?php echo $data[$i]['IdProducto']?>')"><span class="glyphicon glyphicon-check"></span></a></td>          
          
        </tr>
    <?php } ?>    	
   
    </tbody>
   
<script type="text/javascript">	
	function pon_prefijo(Codigo,Nombre,Precio,IdProducto) {				
		document.getElementById('c_codprdS').value=Codigo;	
		document.getElementById('c_desprdS').value=Nombre;
		document.getElementById('descripcionS').value=Nombre;
		document.getElementById('PrecioS').value=Precio;
		document.getElementById('IdProductoS').value=IdProducto;		
		$('#my_modalServ').modal('toggle');	//cerrar		
	}
	
</script>
