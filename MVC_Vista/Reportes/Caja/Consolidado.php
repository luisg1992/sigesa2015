﻿<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/CajaM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 

	class PDF extends FPDF
	{
		function Header()
		{	
			$this->SetFont('Arial','',9);
			$this->Cell(20);
			$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
			$this->Cell(37);
			$this->setfont('arial','b',12);
			$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
			$this->Cell(70);
			$this->SetFont('Arial','',9); 
			$this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
			$this->Ln(4);
			$this->Cell(22,4,'',0,0,'L');
			$this->Cell(135);
			$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
			$this->Ln(3);
			$this->Cell(49);
			$NumPag=$this->PageNo();
			$this->Cell(80,5,strtoupper('Reporte de Consolidado por Servicio'),0,0,'C');	
			$this->Cell(25);
			$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
			$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
			$this->Ln(5);
			$this->setfont('arial','b',12);
			$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
			$this->Ln(8);
		}
		function Footer()
		{
			 $this->SetY(-10);
			$this->SetFont('Arial','',9);
			$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
			$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
			$this->Ln(5);
			$this->SetFont('Arial','',9);
			$this->Cell(150,5,'Terminal()',0,0,'L');
					
		}
	}

	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();

	$pdf->SetFont('Arial','b',9);

	$pdf->Cell(70,5,'NOMBRE DE SERVICIO',1,0,'C');  
	$pdf->Cell(30,5,'TOTAL',1,0,'C');				
	$ListarReporte=Reporte_Consolidado_por_Servicio_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
		if($ListarReporte != NULL)	{
		$i=0;
	    foreach($ListarReporte as $item){  
				$pdf->ln(5);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,5,utf8_decode(substr($item["Nombre"],0,40)),1,0,'L');  
				$pdf->Cell(30,5,utf8_decode("s/. ".number_format($item["SumaTotal"],2)),1,0,'L'); 
				$suma=$suma+$item["SumaTotal"];
				$i=$i+1;
				}
				$pdf->ln(5);
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(70,5,strtoupper('Total').' ( '.$i.' Items )',1,0,'L');  
				$pdf->Cell(30,5,"s/. ".number_format($suma,2),1,0,'L'); 		
	  }
	$pdf->Output('Consolidado.pdf','D');?>