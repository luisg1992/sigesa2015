﻿<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/CajaM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 

class PDF extends FPDF
{
function Header(){	
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',12);
		$this->Cell(150,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(1);
		$this->SetFont('Arial','',9); 
	    $this->Cell(30,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(193);
		$this->Cell(20,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		$NumPag=$this->PageNo();
		$this->Cell(161,5,'  REPORTE DE EXTORNOS  ',0,0,'C');	
		$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'L');
		$this->Image('../../../MVC_Complemento/img/grcallo.jpg',247,5,18,20);
		$this->Ln(5);
		$this->setfont('arial','b',12);
		$this->Cell(265,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
		$this->Ln(8);
	}
	function Footer()
	{
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
 		$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
		$this->Ln(5);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,'Terminal()',0,0,'L');
 				
	}
}

$pdf = new PDF('L','mm');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Arial','b',9);	
$pdf->Cell(70,5,'Nombre de Cajero',1,0,'C');
$pdf->Cell(75,5,'Nombre de Paciente',1,0,'C');			
$pdf->Cell(25,5,'Historia Clinica',1,0,'C');		
$pdf->Cell(30,5,'Serie Boleta',1,0,'C');		
$pdf->Cell(20,5,'Monto',1,0,'C');		
$pdf->Cell(40,5,'Fecha Cobranza',1,0,'C');	
$ListarReporte=Reporte_Extornos_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
 if($ListarReporte != NULL)	{ 
	  $suma=0; 
	  $c=0;
	  foreach($ListarReporte as $item){
				$pdf->ln(5);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,5,utf8_decode(substr($item["NombreCajero"],0,35)),1,0,'L');  	
				$pdf->Cell(75,5,utf8_decode(substr($item["RazonSocial"],0,35)),1,0,'L');  
				$pdf->Cell(25,5,utf8_decode($item["NroHistoriaClinica"]),1,0,'L');  
				$pdf->Cell(30,5,utf8_decode($item["NroSerie"]).' - '.utf8_decode($item["NroDocumento"]),1,0,'C');  
				$pdf->Cell(20,5,' s/. '.number_format($item["Total"], 2),1,0,'L'); 
				$pdf->Cell(40,5,substr($item['FechaCobranza'],0, 19),1,0,'C'); 
				$suma=$suma+$item["Total"];
				$c=$c+1;
				
		}
		
	  }
	            $pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(200,5,'TOTAL ( '.$c.' ) ',1,0,'C');
				$pdf->Cell(60,5,' s/. '.number_format($suma,2),1,0,'L');  				

	  



/*
$pdf->Output();*/
$pdf -> Output('Extornos.pdf', 'D');
?>