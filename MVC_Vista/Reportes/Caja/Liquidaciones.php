﻿<?php 
ini_set('memory_limit', '1024M'); 

require('../../../MVC_Modelo/CajaM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');


	class PDF extends FPDF
	{
		function Header()
		{	
			$this->SetFont('Arial','',9);
			$this->Cell(20);
			$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
			$this->Cell(37);
			$this->setfont('arial','b',12);
			$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION'.$mira,0,0,'C');
			$this->Cell(70);
			$this->SetFont('Arial','',9); 
			$this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
			$this->Ln(4);
			$this->Cell(22,4,'',0,0,'L');
			$this->Cell(135);
			$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
			$this->Ln(3);
			$this->Cell(49);
			$NumPag=$this->PageNo();
			$this->Cell(80,5,strtoupper('Reporte de Liquidaciones'),0,0,'C');	
			$this->Cell(25);
			$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
			$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
			$this->Ln(5);
			$this->setfont('arial','b',12);
			$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
			$this->Ln(8);
		}
		function Footer()
		{
			 $this->SetY(-10);
			$this->SetFont('Arial','',9);
			$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
			$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
			$this->Ln(5);
			$this->SetFont('Arial','',9);
			$this->Cell(150,5,'Terminal()',0,0,'L');
					
		}
	}

	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();

	$pdf->SetFont('Arial','b',9);

	$pdf->Cell(36,5,'NOMBRE SERVICIO',1,0,'C');  
	$pdf->Cell(50,5,'NOMBRE PACIENTE',1,0,'C');	
	$pdf->Cell(20,5,strtoupper('Cuenta'),1,0,'C');		
	$pdf->Cell(20,5,strtoupper('Hc'),1,0,'C');	
	$pdf->Cell(24,5,strtoupper('F. Ingreso'),1,0,'C');
	$ListarReporte=Reporte_Liquidaciones_M(sqlfecha_devolver($_REQUEST["FechaInicio"]),sqlfecha_devolver($_REQUEST["FechaFinal"]));
		if($ListarReporte != NULL)	{ 
		$Pagado=0;
		$DEVUELTO=0;
		$Anulado=0;
		$DebueltoDia=0;
		$ToTNumDocDevuelDia=0;
		
	  foreach($ListarReporte as $item){  
				$pdf->ln(5);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(36,5,utf8_decode(substr($item["ServicioNombre"],0,40)),1,0,'L');
				$pdf->Cell(50,5,utf8_decode(substr($item["NombrePaciente"],0,40)),1,0,'L');  
				$pdf->Cell(20,5,utf8_decode(substr($item["IdCuentaAtencion"],0,40)),1,0,'C');	
				$pdf->Cell(20,5,utf8_decode(substr($item["NroHistoriaClinica"],0,40)),1,0,'C');  
				$pdf->Cell(24,5,utf8_decode(substr($item["FechaIngreso"],0,10)),1,0,'C');  			
				/*$pdf->Cell(30,5,utf8_decode("s/. ".number_format($item["SumaTotal"],2)),1,0,'L');		*/	
		}						
	  }
	$pdf->Output('Liquidaciones.pdf','D');?>