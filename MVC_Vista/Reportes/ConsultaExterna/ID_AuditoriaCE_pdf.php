<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 
//

class PDF extends FPDF
{
function Header()
{
   
    	$this->SetFont('Arial','',9);  /*   ARIAL TIPO DE LETRA SIEMPRE; '' ; 9 TAMAÑO */
		$this->Cell(76,2,'"HOSPITAL NACIONAL DANIEL ALCIDES CARRION"',0,0,'C');
		$this->Cell(160);
	    $this->Cell(19,4,date("d/m/Y"),0,0,'L');
		$this->Ln(4);
		//$this->Cell(22,4,'Admision-Caja',0,0,'L');
		$this->Cell(237);
		$this->Cell(14,4,date('h:m:s'),0,0,'R');
		$this->Ln(5);
		$this->Cell(49);
		$this->Cell(150,5,'AUDITORIA CE',0,"B",'C');
		$this->Cell(25);
		
		$this->Ln(5);
		
		$this->Ln(7);
		//$this->Cell(43,6,'PACIENTE','B.T',0,'L');
		//$this->Cell(25,6,'TIPO','B.T',0,'L');
		$this->Cell(18,6,'FECHA','B.T',0,'L');
		$this->Cell(50,6,'CONSULTORIO','B.T',0,'L');
		//$this->Cell(35,6,'ESPECIALIDAD','B.T',0,'L');
		$this->Cell(45,6,'ESPECIALIDAD','B.T',0,'L');
		$this->Cell(50,6,'PACIENTE','B.T',0,'L');
		$this->Cell(109,6,'DIAGNOSTICO','B.T',0,'L');
		//$this->Cell(35,6,'CAJERO','B.T',0,'L');
}

function Footer() 
{ 
   	$this->SetY(-10);
	$this->SetFont('Arial','',7);
	//$nombre_usuario = ListarUsuarioxIdempleado_M($_REQUEST["idUsuariocajero"]);
	//$this->Cell(150,5,"HNDAC - ".$nombre_usuario[1],0,0,'L');
 	$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
	
   
} 
}

$pdf = new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$NroHistoria = $_REQUEST["NroHistoria"];
$NroDni = $_REQUEST["NroDni"]; 
	
	if (empty($NroHistoria)) {
		$datos_pacientes = BuscarPacientesxNroDni_M($NroDni);
	}
	if (empty($NroDni)) {
		$datos_pacientes = BuscarPacientesxNroHistoriaClinica_M($NroHistoria);
	}

	//$datos_pacientes = BuscarPacientesxNroDni_M($_REQUEST["NroDni"]);

/*	
	foreach ($datos_pacientes as $datos_pacientes_temp) {
		$pdf->Cell(35,6,utf8_decode($datos_pacientes_temp[0]),'B.T',0,'L');
		$pdf->Cell(35,6,utf8_decode($datos_pacientes_temp[1]),'B.T',0,'L');
		$pdf->Cell(35,6,number_format($datos_pacientes_temp[2]),'B.T',0,'L');
		$pdf->Cell(35,6,utf8_decode($datos_pacientes_temp[3]),'B.T',0,'L');
		$pdf->Cell(35,6,utf8_decode($datos_pacientes_temp[4]),'B.T',0,'L');
		$pdf->Cell(35,6,utf8_decode($datos_pacientes_temp[5]),'B.T',0,'L');
		$pdf->Cell(35,6,utf8_decode($datos_pacientes_temp[6]),'B.T',0,'L');
		$pdf->Cell(35,6,utf8_decode($datos_pacientes_temp[7]),'B.T',0,'L');
	}
*/

								



if($datos_pacientes != NULL)	{ 
        $h=0;
	foreach($datos_pacientes as $item3){
		
		$pdf->Ln(7);
		$pdf->SetFont('Arial','',7);
		//$pdf->Cell(38,6,$item3["DATOSPACIENTES"],0,0,'L');
		//$pdf->Cell(21,6,$item3["TIPO"],0,0,'L');
		$pdf->Cell(18,6,substr($item3["FECHA"],0,10),0,0,'L');
		$pdf->Cell(50,6,utf8_decode(strtoupper($item3["CONSULTORIO"])),0,0,'L');
		//$pdf->Cell(34,6,$item3["ESPECIALIDAD"],0,0,'L');
		$pdf->Cell(45,6,utf8_decode(strtoupper($item3["ESPECIALIDAD"])),0,0,'L');
		$pdf->Cell(50,6,utf8_decode(strtoupper($item3["DATOSPACIENTES"])),0,0,'L');
		$pdf->Cell(109,6,utf8_decode(strtoupper($item3["NOMBREDX"])),0,0,'L');
		//$pdf->Cell(35,6,$item3["NOMBRECAJERO"],0,0,'L');
		$h=$h+1;
		if($h>=21){$pdf->AddPage();$h=0;}
	}
}

$pdf->Output();


?>