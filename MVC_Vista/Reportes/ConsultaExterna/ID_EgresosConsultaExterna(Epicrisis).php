<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 
$CodigoMedico = $_REQUEST['CodigoMedico'];


$FechaInicio_temp = substr($_REQUEST["FechaInicio"], 0,10);
$FechaFinal_temp  = substr($_REQUEST["FechaFinal"], 0,10);

list($dia,$mes,$anio) = explode("/",$FechaInicio_temp);
$FechaInicio = $anio."-".$mes."-".$dia;


list($dia,$mes,$anio) = explode("/",$FechaFinal_temp);
$FechaFinal = $anio."-".$mes."-".$dia;




class PDF extends FPDF
{
function Header(){	
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',12);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		//$this->SetFont('Arial','',9); 
	    //$this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(135);
		//$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		//$this->Cell(80,5,'CITAS POR CONSULTORIO',0,0,'C');
		$this->Cell(25);
		//$this->Cell(17,4,'Pagina:  1',0,0,'R');
		$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
		$this->Ln(5);
		//$this->setfont('arial','b',12);
		//$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
		//$this->Ln(7);
		//$this->setfont('arial','b',12);
		//$this->Cell(150,6,'Apellidos y Nombres','1',0,'C');
 		//$this->Cell(39,6,'Total','1',0,'C');
		//$this->SetFont('Arial','',9);
			
		//$this->Cell(34.1,5,'ESPECIALIDAD','B.T',0,'L');		
		//$this->Cell(60.1,5,'MEDICO','B.T',0,'L');
	}
	function Footer()
	{
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(200,5,"HNDAC - ".strtoupper($_REQUEST["Usuario"]),0,0,'L');
 		$this->Cell(100,5,"OESI/UI/DS" ,0,0,'C');
		$this->Ln(5);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,Terminal(),0,0,'L');
 				
	}
}

$pdf = new PDF("P","mm","A4");
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Arial','',8);

		$inico = new DateTime($FechaInicio);
		$fin = new DateTime($FechaFinal);
		$cantidad_dias = round(($fin->format('U') - $inico->format('U')) / (60*60*24));
			


		for ($i=0; $i < $cantidad_dias+1; $i++) { 
			$nuevafecha = date($FechaInicio);
			$nuevafecha = strtotime ( '+'.$i.' day' , strtotime ( $nuevafecha ) ) ;
			$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
			$NombreMedico = RetornarIdMedicoxNombre($CodigoMedico);
			$data = CitadosAtendidosxConsultorios($nuevafecha,$CodigoMedico);
			$data_dos = AtendidosxConsultorios($nuevafecha,$CodigoMedico);
			if($data != NULL)	{ 
			        $h=0;
			        $pdf->Cell(179,5,strtoupper($NombreMedico[0]),0,0,'C');
			        $pdf->Ln(4);
			        $pdf->Cell(179,5,'CITAS POR CONSULTORIO',0,0,'C');
			        $pdf->Ln(4);
			        $pdf->Cell(180,5,'FECHA: '.$nuevafecha,0,0,'C');
			        $pdf->Ln(8);
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(22,5,'HORA INICIO','B.T',0,'L');  
					$pdf->Cell(22,5,'HORA FINAL','B.T',0,'L');  
					$pdf->Cell(38.1,5,'CONSULTORIO','B.T',0,'L'); 
					$pdf->Cell(68.1,5,'PACIENTE','B.T',0,'L');		
					$pdf->Cell(22.1,5,utf8_decode('N° HIS'),'B.T',0,'L');		
					$pdf->Cell(22.1,5,'DNI','B.T',0,'L');	
				foreach($data as $item3){
					
					$pdf->Ln(7);
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(22,5,$item3["HORAINICIO"],0,0,'L');  
					$pdf->Cell(22,5,$item3["HORAFIN"],0,0,'L');  
					$pdf->Cell(38.1,5,utf8_decode(strtoupper($item3["CONSULTORIO"])),0,0,'L');	 
					$pdf->Cell(68.1,5,utf8_decode(strtoupper($item3["DATOSPACIENTES"])),0,0,'L');		
					$pdf->Cell(22.1,5,utf8_decode(strtoupper($item3["NHIS"])),0,0,'L');		
					$pdf->Cell(22.1,5,utf8_decode(strtoupper($item3["NDOC"])),0,0,'L');
					
					$h=$h+1;
					if($h>=39){$pdf->AddPage();$h=0;}
				}
				$pdf->AddPage();
			}

			// para atenciones
			if($data_dos != NULL)	{ 
					$pdf->Cell(179,5,strtoupper($NombreMedico[0]),0,0,'C');
			        $pdf->Ln(4);
			        $h=0;
			        $pdf->Cell(179,5,'ATENCIONES POR CONSULTORIO',0,0,'C');
			        $pdf->Ln(4);
			        $pdf->Cell(180,5,'FECHA: '.$nuevafecha,0,0,'C');
			        $pdf->Ln(8);
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(10,5,'HI','B.T',0,'L');  
					//$pdf->Cell(10,5,'HF','B.T',0,'L');  
					//$pdf->Cell(30.1,5,'CONSULTORIO','B.T',0,'L'); 
					$pdf->Cell(56.1,5,'PACIENTE','B.T',0,'L');		
					$pdf->Cell(75.1,5,'DESCRIPCION','B.T',0,'L');		
					$pdf->Cell(22.1,5,'COD','B.T',0,'L');	
				foreach($data_dos as $item4){
					
					$pdf->Ln(7);
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(10,5,$item4["HORAINICIO"],0,0,'L');  
					//$pdf->Cell(10,5,$item4["HORAFIN"],0,0,'L');  
					//$pdf->Cell(30.1,5,utf8_decode(strtoupper($item4["CONSULTORIO"])),0,0,'L');	 
					$pdf->Cell(56.1,5,utf8_decode(strtoupper($item4["DATOSPACIENTES"])),0,0,'L');		
					$pdf->Cell(75.1,5,utf8_decode(strtoupper($item4["NOMBREDX"])),0,0,'L');		
					$pdf->Cell(22.1,5,utf8_decode(strtoupper($item4["DIAGCIEX"])),0,0,'L');
					
					$h=$h+1;
					if($h>=39){$pdf->AddPage();$h=0;}
				}
				$pdf->AddPage();
			}

		}


	

$pdf->Output();
?>

