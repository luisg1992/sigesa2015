<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 

class PDF extends FPDF
{
function Header(){	
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',12);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		$this->SetFont('Arial','',9); 
	    $this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(135);
		$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		$this->Cell(80,5,' DEVOLUCIONES   ',0,0,'C');
		$this->Cell(25);
		$this->Cell(17,4,'Pagina: '.$this->PageNo(),0,0,'R');
		$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
		$this->Ln(5);
		$this->setfont('arial','b',12);
		$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
		 $this->Ln(7);
		//$this->setfont('arial','b',12);
		//$this->Cell(150,6,'Apellidos y Nombres','1',0,'C');
 		//$this->Cell(39,6,'Total','1',0,'C');
		//$this->SetFont('Arial','',9);
		$this->SetFont('Arial','b',9);
		$this->Cell(23,5,'Boleta',1,0,'C');  
		$this->Cell(29,5,'Fecha',1,0,'C');		
		$this->Cell(61,5,'Paciente',1,0,'C');		
		$this->Cell(51,5,'Cajero',1,0,'C');		
		$this->Cell(26,5,'Monto',1,0,'C');	
		
		$this->Ln(5);
	}
	function Footer()
	{
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
 		$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
		$this->Ln(5);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,Terminal(),0,0,'L');
 				
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

	

$ListarDevolucion =sigesaCajaDevolucionesPorFechasUsuario_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["idUsuariocajero"]);

 if($ListarDevolucion != NULL)	{ 
	 
	    $i=0;	
		$Total=0;
	  foreach($ListarDevolucion as $item){
 
			 	$pdf->ln(5);
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(23,5,$item["NroSerie"].'-'.$item["NroDocumento"],1,0,'C'); 
				$pdf->Cell(29,5,vfechahora($item["fechaDevolucion"]),1,0,'C');		
				$pdf->Cell(61,5,utf8_decode(substr($item["RazonSocial"],0,35)),1,0,'L');
				$pdf->Cell(51,5,utf8_decode(substr($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["Nombres"],0,35)),1,0,'L');		
				$pdf->Cell(26,5,number_format($item["montoTotal"],2),1,0,'R');		
				
  
				 $i++;	
		        $Total=$Total+$item["montoTotal"];
				
		}
				
				 
				
				
	  }
	            $pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(138,5,'TOTAL',1,0,'R');  
				$pdf->Cell(26,5,$i,1,0,'R');	
				$pdf->Cell(26,5,'S/.  '.number_format($Total,2),1,0,'R');
	  
	  
 

$pdf->Output();

?>