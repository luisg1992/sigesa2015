<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 
class PDF extends FPDF
{
function Header(){	
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',11);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		$this->SetFont('Arial','',9); 
	    $this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(135);
		$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		$this->Cell(80,5,'  RECAUDACION GENERAL  ',0,0,'C');
		$this->Cell(25);
		$this->Cell(17,4,'Pagina: '.$this->PageNo(),0,0,'R');
		//$this->Cell(33,4,'Pagina: '.$this->PageNo(),0,0,'L');
		$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
		$this->Ln(5);
		$this->setfont('arial','b',11);
		$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
 		$this->SetFont('Arial','',8);
 		$this->Ln(5);	
		$this->Cell(23.75,5,'No. Boleta',1,0,'C');
		$this->Cell(23.75,5,'HC',1,0,'C');    
		$this->Cell(23.75,5,'Exonerado',1,0,'C');		
		$this->Cell(23.75,5,'Efectivo',1,0,'C');		
		$this->Cell(23.75,5,'No. Boleta',1,0,'C');
		$this->Cell(23.75,5,'HC',1,0,'C');    
		$this->Cell(23.75,5,'Exonerado',1,0,'C');		
		$this->Cell(23.75,5,'Efectivo',1,0,'C');		
	}
	function Footer()
	{
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"].'     '.Terminal(),0,0,'L');
		$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
	}
	
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

	

$ListarBoletas=SigesaCajaComprobantePagoBoletaxFecahcxCajero_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),'%');
 if($ListarBoletas!=NULL){

$Y=0;
$X=0;
$i=0;
$TotalPagado=0;
$Exoneraciones=0;
$Numdoc=0;
foreach($ListarBoletas as $item){	
//for($i=0;$i<=500;$i++){
	if($i%80==0&&$i!=0)
		{
			$Y=0;
			$X=95;
			if($i%160==0&&$i!=0)
			{
				$pdf->AddPage();
				$X=0;
			}
			
		}
	  //NroSerie	NroDocumento
	$pdf->SetXY(10+$X,32+$Y);
	$pdf->SetFont('Courier','',7);
	$pdf->Cell(23.75,5,$item["NroSerie"].'-'.$item["NroDocumento"].$i,0,0,'C');
	$pdf->Cell(23.75,5,$item["NroHistoriaClinica"],0,0,'R');    
	$pdf->Cell(23.75,5,number_format($item["Exoneraciones"],2),0,0,'R');		
	$pdf->Cell(23.75,5,number_format($item["TotalPagado"],2),0,0,'R');		
	  //$pdf->Cell(30,5,'',1,0,'C');
	$Y=$Y+3;
	$i++;
	$Numdoc++;
	$TotalPagado=$TotalPagado+$item["TotalPagado"];
	$Exoneraciones=$Exoneraciones+$item["Exoneraciones"];
	
	}
	$ListarDevolucion=SigesaCajaDevolucionesxFechayUsuario_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),'%');
	$TotalDevoluciondelDia=$ListarDevolucion[0]["DevueltodelDia"];
	$NumDocDevuelDia=$ListarDevolucion[0]["NumDoc"];
   $TotaxCajero= SigesAFactReporteCentrocostroTotalesXCajero(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),'%');	
	 
	$SubTotal=$TotaxCajero[0]["SubTotal"];	
	$Total=$TotaxCajero[0]["Total"];	
	$Exoneraciones=$TotaxCajero[0]["Exoneraciones"];	
	$Pagado=$TotaxCajero[0]["Pagado"];	
	$Anulado=$TotaxCajero[0]["Anulado"];	
	$Devolucion=$TotaxCajero[0]["Devolucion"];

	$pdf->SetXY(10+$X,35+$Y);
	
	$pdf->SetFont('Arial','B',9);	
	$pdf->Cell(47.5,5,'TOTAL NETO:',0,0,'R');
	$pdf->Cell(23.75,5,number_format($Exoneraciones,2),0,0,'R');		
	$pdf->Cell(23.75,5,number_format($TotalPagado-$Anulado,2),0,0,'R');
		
	$pdf->SetXY(10+$X,40+$Y);
	$pdf->SetFont('Arial','B',9);	
	$pdf->Cell(71.25,5,'Devolucion del Dia:',0,0,'R');
	$pdf->Cell(23.75,5,'-'.number_format($TotalDevoluciondelDia,2),0,0,'R');
	
	$pdf->SetXY(10+$X,45+$Y);
	$pdf->SetFont('Arial','B',9);	
	$pdf->Cell(71.25,5,'TOTAL RECAUDADO:',0,0,'R');
	$pdf->Cell(23.75,5,number_format($Pagado-$Devolucion-$TotalDevoluciondelDia,2),0,0,'R');
	
	
	$pdf->SetXY(10+$X,50+$Y);
	$pdf->SetFont('Arial','B',9);	
	$pdf->Cell(71.25,5,'Total de Documnetos:',0,0,'R');
	$pdf->Cell(23.75,5,$Numdoc,0,0,'R');
	 
 }
$pdf->Output();


?>
