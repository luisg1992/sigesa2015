<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 
class PDF extends FPDF
{
function Header()
{
   
       
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',12);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		$this->SetFont('Arial','',9); 
	    $this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(135);
		$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		$this->Cell(80,5,'RECAUDACION INSTITUCIONAL POR SERVICIO',0,0,'C');
		$this->Cell(25);
		$this->Ln(5);
		$this->Cell(65,5,'','C');
 
		$this->Cell(82,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
		$this->Cell(33,4,'Pagina: '.$this->PageNo(),0,0,'L');
			$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
		$this->Ln(5);
		$this->setfont('Arial','',10);
	 
		$this->Cell(32,6,'Codigo CPT','B.T',0,'L');
		$this->Cell(92,6,'Descripcion Del Servicio','B.T',0,'L');
		$this->Cell(43,6,'Cantidad','B.T',0,'R');
		$this->Cell(24,6,'Total','B.T',0,'R');
		$this->Ln(5);
		
		
}

function Footer()
	{
	 /*  
		$this->Cell(140,5,"BYLLYRON",0,0,'L');
 		$this->Cell(70,5,"rpgh0312.frx",0,0,'C');	*/	
		
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"].'     '.Terminal(),0,0,'L');
 		$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');



		
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(3);
$h=0;


$ListarCajaxCentrodecosto=SigesaCajaxFechayCentrodeCosto_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["idUsuariocajero"]);

 if($ListarCajaxCentrodecosto != NULL)	{ 
	 
				$Total=0;
				$Cantidad=0;
	  foreach($ListarCajaxCentrodecosto as $item){
				 	   
			    $pdf->SetFont('Courier','B',9);
			    $pdf->Cell(37,4,utf8_decode($item["Descripcion"]),0,0,'L');
				$h=$h+3;
	     	    $pdf->Ln(3);
					if($h>=240){$pdf->AddPage();$h=0;$pdf->Ln(2);}
					
				  $ListarCalatoloxCentroCosto= SigesaCajaxFechayCatalogodeServicios_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$item["IdCentroCosto"]);;
				  $CantidadxCat=0;
				  $TotalxCat=0;
				  foreach($ListarCalatoloxCentroCosto as $item1){	 	
 					
					$pdf->SetFont('Courier','',8);
					$pdf->cell(12,4,$item1["Codigo"],0,0,'R');
					$pdf->Cell(10);
					$pdf->cell(133,4,substr(utf8_decode($item1["Nombre"]),0,70),0,0,'l');
					$pdf->cell(12,4,$item1["Cantidad"],0,0,'R');
					$pdf->cell(25,4,number_format($item1["Total"],2),0,0,'R');
					$h=$h+3;
					$pdf->Ln(3);
					$CantidadxCat=$CantidadxCat+$item1["Cantidad"];
				    $TotalxCat=$TotalxCat+$item1["Total"];
 
					
					if($h>=240){$pdf->AddPage();$h=0;$pdf->Ln(2);}
				}	
				    $pdf->ln(4);
					$pdf->cell(22);
					$pdf->cell(133,5,'TOTAL :','T',0,'R');
					$pdf->cell(12,5,$CantidadxCat,'T',0,'R');
					$pdf->cell(25,5,number_format($TotalxCat,2),'T',0,'R');
					$pdf->Ln(7);
					$h=$h+11;
					
					if($h>=239){$pdf->AddPage();$h=0;$pdf->Ln(2);}
					
				 $Total=$Total+$TotalxCat;
				$Cantidad=$Cantidad+$CantidadxCat;
	 
		}
 		$pdf->ln(4);
		$h=$h+4;
	    if($h>=240){$pdf->AddPage();$h=0;$pdf->Ln(5);}
	 
		$pdf->cell(166,5,'','T',0,'L');
		$pdf->cell(26.5,5,'','T',0,'L');
		$pdf->LN(4);
		$h=$h+4;
		 if($h>=240){$pdf->AddPage();$h=0;$pdf->Ln(4);}
		$pdf->Cell(130);
		 $pdf->SetFont('Courier','B',9);
		$pdf->Cell(62,6,'TOTAL NETO    '.$Cantidad.'   '.number_format($Total,2),1,0,'R');// 
		
		$ListarReportexCentroCostoTotales= SigesAFactReporteCentrocostroTotales_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));
if($ListarReportexCentroCostoTotales != NULL)	{ 
         
		  foreach($ListarReportexCentroCostoTotales as $item3){
		  
		  $tSubTotal=$item3["SubTotal"];	
		  $tIGV=$item3["IGV"];	
		  $tTotal=$item3["Total"];	
		  $tDsctos=$item3["Dsctos"];	
		  $tExoneraciones=$item3["Exoneraciones"];	
		  $tAdelantos=$item3["Adelantos"];	
		  $tPagado=$item3["Pagado"];	
		  $tAnulado=$item3["Anulado"];	
		  $tDevolucion=$item3["Devolucion"];
			}
}
$ListarDevolucion=SigesaCajaDevolucionesxFechayUsuario_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["idUsuariocajero"]);
	$TotalDevoluciondelDia=$ListarDevolucion[0]["DevueltodelDia"];
	$NumDocDevuelDia=$ListarDevolucion[0]["NumDoc"];
//  SubTotal	IGV	Total	Dsctos	Exoneraciones	Adelantos	Pagado	Anulado	Devolucion
$pdf->SetFont('Arial','b',9);
 	
$pdf->ln(10);
//$pdf->Cell(140,5,'TOTAL NETO',0,0,'R');  
//$pdf->Cell(50,5,number_format($tPagado,2),0,0,'R');
//$pdf->ln(4);
//$pdf->Cell(140,5,'EXTORNOS ',0,0,'R');  
//$pdf->Cell(50,5,'-'.number_format($tAnulado,2),0,0,'R');	
//$pdf->ln(4);
$pdf->Cell(140,5,'EXONERACIONES',0,0,'R');  
$pdf->Cell(50,5,'-'.number_format($tExoneraciones,2),0,0,'R');
$pdf->ln(4);
$pdf->Cell(140,5,'DEVOLUCIONES',0,0,'R');  
$pdf->Cell(50,5,'-'.number_format($tDevolucion,2),0,0,'R');
$pdf->ln(4);
$pdf->Cell(140,5,'DEVOLUCIONES DEL DIA',0,0,'R');  
$pdf->Cell(50,5,'-'.number_format($TotalDevoluciondelDia,2),0,0,'R');
	
$pdf->ln(4);
$pdf->Cell(140,5,'TOTAL RECAUDADO',0,0,'R');  
$pdf->Cell(50,5,number_format($tPagado-$tDevolucion-$TotalDevoluciondelDia,2),0,0,'R');	
}// Fin Primer IF
$pdf->Output();


?>
