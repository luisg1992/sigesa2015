<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
class PDF extends FPDF
{
function Header(){	
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',12);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		$this->SetFont('Arial','',9); 
	    $this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(135);
		$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		$NumPag=$this->PageNo();
		if($NumPag==1){
		$this->Cell(80,5,'  DETALLE NOTAS DE CREDITO  ',0,0,'C');	
		 }else{
		$this->Cell(80,5,'  RECAUDACION POR CENTRO DE COSTO ',0,0,'C'); 
			 }
		$this->Cell(25);
		$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
		$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
		$this->Ln(5);
		$this->setfont('arial','b',12);
		$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');

		$this->Ln(8);
	}
	function Footer()
	{
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
 		$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
		$this->Ln(5);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,'Terminal()',0,0,'L');
 				
	}
}

//$pdf = new PDF(); // AQUI HORIZONTAL
$pdf=new FPDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Arial','',9);
		$pdf->Cell(20);
		//$pdf->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$pdf->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$pdf->Cell(37);
		$pdf->setfont('arial','b',12);
		$pdf->Cell(154,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$pdf->Cell(70);
		$pdf->SetFont('Arial','',9); 
	    $pdf->Cell(-40,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$pdf->Ln(4);
		$pdf->Cell(22,4,'',0,0,'L');
		$pdf->Cell(135);
		$pdf->Cell(84,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$pdf->Ln(3);
		$pdf->Cell(49);
		$NumPag=$pdf->PageNo();
		if($NumPag==1){
		$pdf->Cell(160,5,'  DETALLE NOTAS DE CREDITO  ',0,0,'C');	
		 }else{
		$pdf->Cell(160,5,'  RECAUDACION POR CENTRO DE COSTO ',0,0,'C'); 
			 }
		$pdf->Cell(25);
		$pdf->Cell(7,4,'Pagina:  '.$pdf->PageNo(),0,0,'R');
		$pdf->Image('../../../MVC_Complemento/img/grcallo.jpg',260,5,18,20);
		$pdf->Ln(5);
		$pdf->setfont('arial','b',12);
		$pdf->Cell(255,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');

		$pdf->Ln(10);



$pdf->SetFont('Arial','b',9);
$pdf->Cell(80,5,'BOLETA',1,0,'C');	
$pdf->Cell(100,5,'NOTA DE CREDITO',1,0,'C');  
$pdf->Cell(90,5,'DEVUELTO POR CAJERO',1,0,'C');	
$pdf->Ln(5);

$pdf->Cell(30,5,'Nro Docto',1,0,'C');	
$pdf->Cell(20,5,'F. Docto',1,0,'C');  
$pdf->Cell(30,5,'Monto',1,0,'C');	


$pdf->Cell(32.5,5,'Nro Nota C.',1,0,'C');	
$pdf->Cell(22.5,5,'F. Aprobacion',1,0,'C');	
$pdf->Cell(22.5,5,'Aprobado Por.',1,0,'C'); 
$pdf->Cell(22.5,5,'Monto',1,0,'C');	

$pdf->Cell(22.5,5,'Estado',1,0,'C');	
$pdf->Cell(22.5,5,'Cajero',1,0,'C');  
$pdf->Cell(22.5,5,'F. Canje',1,0,'C');	
$pdf->Cell(22.5,5,'Monto',1,0,'C');	

$cajero=0;
if($_REQUEST["idUsuariocajero"]=='0'){ $cajero='0';}else{$cajero=$_REQUEST["idUsuariocajero"];}

$ListarReporte=SigesaNotasCreditoporRangoFechas_LCO_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$cajero);
 $Aprobados=0; //1
 $Anulado=0; //2
 $Canjeado=0; //3
 $PorAprobar=0; //0
    
 $AprobadosNroDctos=0;
 $AnuladoNroDctos=0;
 $CanjeadoNroDctos=0;
 $PorAprobarNroDctos=0;
 
 $montoBoleta=0;
 $montoNC=0;
 $montocanjeado=0;
 
 $CajeroDevuelve="";
 $FechaDevuelve="";
 $MontoDevuelve="";
 if($ListarReporte != NULL)	{ 
		
	  foreach($ListarReporte as $item){
			$pdf->ln(5);
			 $montoBoleta= $montoBoleta+$item["MontoBoleta"];
			 $montoNC=$montoNC+$item["MontoNotaCredito"];
				
			$pdf->Cell(30,5,($item["NroBoleta"]),1,0,'L');	
			$pdf->Cell(20,5,($item["FechaBoleta"]),1,0,'L');  
			$pdf->Cell(30,5,number_format($item["MontoBoleta"],2),1,0,'R');
			
			$pdf->Cell(32.5,5,($item["NroNotaCredito"]),1,0,'L');	
			$pdf->Cell(22.5,5,($item["FechaNotaCredito"]),1,0,'L');  
			$pdf->Cell(22.5,5,(substr($item["UsuarioAutorizaNotaCredito"],0,10)),1,0,'L');	
			$pdf->Cell(22.5,5,number_format($item["MontoNotaCredito"],2),1,0,'R');	
			
			
				if($item["IdEstadoNota"]=='1'){
				$Aprobados=$Aprobados+$item["MontoNotaCredito"];
				$AprobadosNroDctos++;
				}elseif($item["IdEstadoNota"]=='2'){
				$Anulado=$Anulado+$item["MontoNotaCredito"];
				$AnuladoNroDctos++;					
				}elseif($item["IdEstadoNota"]=='3'){
				$Canjeado=$Canjeado+$item["MontoNotaCredito"];
				$CanjeadoNroDctos++;									
				}elseif($item["IdEstadoNota"]=='0'){
				$PorAprobar=$PorAprobar+$item["MontoNotaCredito"];
				$PorAprobarNroDctos++;					
				}
				if($item["IdEstadoNota"]=='3'){ // si el documento es canjeado debe mostrar cajero fecha de devoluciony el monto
				$obtener=SigesaNotasCreditoObtenerDevolucion_LCO_M($item["IdComprobantePago"]);
				$CajeroDevuelve	=(substr($obtener[0]["Cajero"],0,10));
				$FechaDevuelve=$obtener[0]["FechaDevolucion"];	
				$MontoDevuelve=number_format($obtener[0]["montoDevuelto"],2);
				$montocanjeado=$montocanjeado+$obtener[0]["montoDevuelto"];	
				}else{
				$CajeroDevuelve="";	
				$FechaDevuelve="";
				$MontoDevuelve="";
				}		
			$pdf->Cell(22.5,5,($item["estadoNota"]),1,0,'C');	
			$pdf->Cell(22.5,5,$CajeroDevuelve,1,0,'L');  
			$pdf->Cell(22.5,5,$FechaDevuelve,1,0,'L');	
			$pdf->Cell(22.5,5,$MontoDevuelve,1,0,'R');				
		}
 }
 $pdf->ln(5);
 	
$pdf->Cell(50,5,'Total',1,0,'R');  
$pdf->Cell(30,5,number_format($montoBoleta,2),1,0,'R');	
 
$pdf->Cell(77.5,5,'Total',1,0,'R');	
$pdf->Cell(22.5,5,number_format($montoNC,2) ,1,0,'R');	
 
$pdf->Cell(67.5,5,'Total',1,0,'R');	
$pdf->Cell(22.5,5,number_format($montocanjeado,2),1,0,'R');
 
 // MUESTRA LA SIGUIENTE PAGINA EL RESUMEN
	$pdf->AddPage();
	$pdf->Ln(10);

 
 $pdf->ln(10);
$pdf->Cell(190,5,'RESUMEN DETALLE TOTALES',1,1,'C');  
$pdf->ln(3);
 
	           // $pdf->ln(15);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(30,5,'',1,0,'C');  
				$pdf->Cell(26.1,5,'APROBADOS',1,0,'C');		
				$pdf->Cell(20.1,5,'ANULADOS',1,0,'C');	
				$pdf->Cell(26.1,5,'CANJEADOS',1,0,'C');					
				$pdf->Cell(26.1,5,'POR APROBAR',1,0,'C');	


				$pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(30.1,5,'TOTAL',1,0,'C');	
				$pdf->Cell(26.1,5,'S/. '.number_format($Aprobados,2),1,0,'R');  
				$pdf->Cell(20.1,5,'S/. '.number_format($Anulado,2),1,0,'R');
				$pdf->Cell(26.1,5,'S/. '.number_format($Canjeado,2),1,0,'R');					
				$pdf->Cell(26.1,5,'S/. '.number_format($PorAprobar,2),1,0,'R');		


				$pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(30.1,5,utf8_decode('Nº DOCUMENTOS'),1,0,'C');	
				$pdf->Cell(26.1,5,$AprobadosNroDctos,1,0,'R');  
				$pdf->Cell(20.1,5,$AnuladoNroDctos,1,0,'R');
				$pdf->Cell(26.1,5,$CanjeadoNroDctos,1,0,'R');					
				$pdf->Cell(26.1,5,$PorAprobarNroDctos,1,0,'R');		

////CONSOLIDADO 
/*
$pdf->ln(10);
$pdf->Cell(190,5,'CONSOLIDADO POR CAJERO',1,1,'C');  
$pdf->ln(3);

$pdf->Cell(40.1,5,'Usuario',1,0,'C');
$pdf->Cell(30.1,5,'Estado N. Credito',1,0,'C');	
$pdf->Cell(20.1,5,'TOTAL',1,0,'C');		

$ListarReporteConsolidado=SigesaNotasCreditoporRangoFechasConsolidado_LCO_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$cajero);
 $TOTAL=0;

 if($ListarReporteConsolidado != NULL)	{ 
	  foreach($ListarReporteConsolidado as $itemC){
				$pdf->ln(5);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(40,5,utf8_decode(($itemC["Cajero"])),1,0,'L'); 
				$pdf->Cell(30,5,utf8_decode(($itemC["EstadoNota"])),1,0,'L'); 
				$pdf->Cell(20.1,5,number_format($itemC["MontoNotaCredito"],2),1,0,'R');		
				$TOTAL=$TOTAL+$itemC["MontoNotaCredito"];
		}
	
	 }
	            $pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(40,5,'',1,0,'C');  
				$pdf->Cell(30,5,'',1,0,'C'); 
				$pdf->Cell(20.1,5,'',1,0,'C');									
	
	*/
$pdf->Output();

?>