﻿<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');
 

class PDF extends FPDF
{
function Header(){	
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',12);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		$this->SetFont('Arial','',9); 
	    $this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(135);
		$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		$NumPag=$this->PageNo();
		if($NumPag==1){
		$this->Cell(80,5,'  RECAUDACION POR CAJERO  ',0,0,'C');	
		 }else{
		$this->Cell(80,5,'  RECAUDACION POR CENTRO DE COSTO ',0,0,'C'); 
			 }
		
		$this->Cell(25);
		$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
		$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
		$this->Ln(5);
		$this->setfont('arial','b',12);
		$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
		//$this->Ln(7);
		//$this->setfont('arial','b',12);
		//$this->Cell(150,6,'Apellidos y Nombres','1',0,'C');
 		//$this->Cell(39,6,'Total','1',0,'C');
		//$this->SetFont('Arial','',9);
		$this->Ln(8);
	}
	function Footer()
	{
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
 		$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
		$this->Ln(5);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,'Terminal()',0,0,'L');
 				
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Arial','b',9);

$pdf->Cell(60,5,'NOMBRES',1,0,'C');  
$pdf->Cell(26.1,5,'EXTORNOS',1,0,'C');		
$pdf->Cell(26.1,5,'DEVOLUCIONES',1,0,'C');		
$pdf->Cell(26.1,5,'DEVO-DEL-DIA',1,0,'C');		
$pdf->Cell(26.1,5,'SUB-TOTAL',1,0,'C');		
$pdf->Cell(26.1,5,'TOTAL',1,0,'C');		

$ListarReporte=SigesaCajaComprobantePagoFiltroPorNroSerieDocumentoOporRangoFechas_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));

$ListarNroDcoumento=SigesaCajaComprobantePagONumeroDocumentoPorRangoFechas_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));


 

 if($ListarReporte != NULL)	{ 
	 
		
		$Pagado=0;
		$DEVUELTO=0;
		$Anulado=0;
		$DebueltoDia=0;
		$ToTNumDocDevuelDia=0;
		
	  foreach($ListarReporte as $item){
 
				 	   
				$ListarDevolucion=SigesaCajaDevolucionesxFechayUsuario_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$item["IdEmpleado"]);
				$TotalDevoluciondelDia=$ListarDevolucion[0]["DevueltodelDia"];
				$NumDocDevuelDia=$ListarDevolucion[0]["NumDoc"];
				$pdf->ln(5);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(60,5,utf8_decode(substr($item["CajeroNombres"],0,35)),1,0,'L');  
				$pdf->Cell(26.1,5,number_format($item["Anulado"],2),1,0,'R');		
				$pdf->Cell(26.1,5,number_format($item["DEVUELTO"],2),1,0,'R');		
				$pdf->Cell(26.1,5,number_format($TotalDevoluciondelDia,2),1,0,'R');
				$pdf->Cell(26.1,5,number_format(($item["Pagado"]-$item["DEVUELTO"]),2),1,0,'R');		
				$pdf->Cell(26.1,5,number_format(($item["Pagado"]-$item["DEVUELTO"])-$TotalDevoluciondelDia,2),1,0,'R');		
				
				$Pagado=$Pagado+$item["Pagado"];
				$DEVUELTO=$DEVUELTO+$item["DEVUELTO"];
				$Anulado=$Anulado+$item["Anulado"];
				$DebueltoDia=$DebueltoDia+$TotalDevoluciondelDia;
				$ToTNumDocDevuelDia=$ToTNumDocDevuelDia+$NumDocDevuelDia;
				
		}
				
				 
				
				
	  }
	            $pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(60,5,'TOTAL',1,0,'C');  
				$pdf->Cell(26.1,5,number_format($Anulado,2),1,0,'R');		
				$pdf->Cell(26.1,5,number_format($DEVUELTO,2),1,0,'R');	
				$pdf->Cell(26.1,5,number_format($DebueltoDia,2),1,0,'R');	
				$pdf->Cell(26.1,5,number_format(($Pagado-$DEVUELTO),2),1,0,'R');	
				$pdf->Cell(26.1,5,'S/.  '.number_format(($Pagado-$DEVUELTO)-$DebueltoDia,2),1,0,'R');
	  
	  if($ListarNroDcoumento != NULL)	{ 
		  foreach($ListarNroDcoumento as $item1){ 
	  
				$pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(60,5,utf8_decode('Nº DOCUMENTOS'),1,0,'C');  
				$pdf->Cell(26.1,5,$item1["Anulado"],1,0,'R');		
				$pdf->Cell(26.1,5,$item1["DEVUELTO"],1,0,'R');
				$pdf->Cell(26.1,5,$ToTNumDocDevuelDia,1,0,'R');		
				$pdf->Cell(26.1,5,$item1["DEVUELTO"]+$item1["Anulado"]+$item1["Pagado"],1,0,'R');
				$pdf->Cell(26.1,5,$item1["DEVUELTO"]+$item1["Anulado"]+$item1["Pagado"],1,0,'R');
		   }
	  }


		

//$pdf->ln(10);
//$pdf->Cell(190,50,'OBSERVACION:',1,1,'J');  
/*
$pdf->ln(10);
$pdf->Cell(190,8,'OBSERVACION:','L'.'T'.'R',0,'J');  
$pdf->ln(8);
$pdf->Cell(190,20,'','L'.'B'.'R',0,'J');  
*/

//$pdf->AddPage();




/*

$pdf->ln(10);
$pdf->Cell(190,5,'CONSOLIDADO POR CENTRO DE COSTO',1,1,'C');  
$pdf->ln(3);

$pdf->Cell(80,5,'CENTRO DE COSTO','L'.'R'.'T'.'B',0,'C');  
$pdf->Cell(36,5,'CANTIDAD','R'.'T'.'B',0,'C');		
$pdf->Cell(36,5,'EXONERACION','R'.'T'.'B',0,'C');		
$pdf->Cell(38,5,'TOTAL','R'.'T'.'B',0,'C');		

	
 $ListarReportexCentroCosto= SigesaFactReportexCentroCosto_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));
if($ListarReportexCentroCosto != NULL)	{ 
         $SubTotal=0;
		 $Cantidas=0;
		 $Exoneracion=0;
		  foreach($ListarReportexCentroCosto as $item2){ 
	  
				$pdf->ln(5);
				
				$pdf->SetFont('Arial','',9); 
								
				$pdf->Cell(80,5,utf8_decode($item2["Descripcion"]),'L'.'R'.'T'.'B',0,'L');  
 				$pdf->Cell(36,5, $item2["Cantidad"] ,'R'.'T'.'B',0,'R');		
				$pdf->Cell(36,5,number_format($item2["TotalFinanciado"],2),'R'.'T'.'B',0,'R');		
				$pdf->Cell(38,5,number_format($item2["Total"],2),'R'.'T'.'B',0,'R');
				
								
			$Cantidas=$Cantidas+$item2["Cantidad"];
			$SubTotal=$SubTotal+$item2["Total"];	
			$Exoneracion=$Exoneracion+$item2["TotalFinanciado"];
				 //Descripcion	Cantidad	Total
 
		   }
	  }
	  
//	  ---- Farmacia
	   $ListarReportexCentroCostoFarmacia= SigesaFactReportexCentroCosto_Farmaci_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));
     if($ListarReportexCentroCostoFarmacia != NULL)	{ 
         $SubTotalFarma=0;
		 $CantidasFarma=0;
		 $ExoneraFarmacia=0;
		  foreach($ListarReportexCentroCostoFarmacia as $item2){ 
	  
				$pdf->ln(5);
				
				$pdf->SetFont('Arial','',9); 
								
				$pdf->Cell(80,5,utf8_decode($item2["Descripcion"]),'L'.'R'.'T'.'B',0,'L');  
 				$pdf->Cell(36,5, $item2["Cantidad"] ,'R'.'T'.'B',0,'R');		
				$pdf->Cell(36,5,number_format($item2["TotalFinanciado"],2),'R'.'T'.'B',0,'R');
				$pdf->Cell(38,5,number_format($item2["Total"],2),'R'.'T'.'B',0,'R');
				
								
			$CantidasFarma=$CantidasFarma+$item2["Cantidad"];
			$SubTotalFarma=$SubTotalFarma+$item2["Total"];	
			
			$ExoneraFarmacia=$ExoneraFarmacia+$item2["TotalFinanciado"];	
				 //Descripcion	Cantidad	Total
 
		   }
	  } 
	  
	  
 


 $ListarReportexCentroCostoTotales= SigesAFactReporteCentrocostroTotales_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));
if($ListarReportexCentroCostoTotales != NULL)	{ 
         
		  foreach($ListarReportexCentroCostoTotales as $item3){
		  
		  $tSubTotal=$item3["SubTotal"];	
		  $tIGV=$item3["IGV"];	
		  $tTotal=$item3["Total"];	
		  $tDsctos=$item3["Dsctos"];	
		  $tExoneraciones=$item3["Exoneraciones"];	
		  $tAdelantos=$item3["Adelantos"];	
		  $tPagado=$item3["Pagado"];	
		  $tAnulado=$item3["Anulado"];	
		  $tDevolucion=$item3["Devolucion"];
			}
}

$RedondeoFarma=SigesaFactReportexCentroCosto_FarmaciaRedondeado_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));

$RedondeoFarmaPositico=$RedondeoFarma[0]['redondeopos'];

//  SubTotal	IGV	Total	Dsctos	Exoneraciones	Adelantos	Pagado	Anulado	Devolucion
$pdf->SetFont('Arial','b',9);
$pdf->ln(5);
$pdf->Cell(80,5,'TOTAL NETO','L'.'R'.'T'.'B',0,'R');  
$pdf->Cell(36,5,$Cantidas+$CantidasFarma,'R'.'T'.'B',0,'R');		
$pdf->Cell(36,5,number_format($Exoneracion+$ExoneraFarmacia,2),'R'.'T'.'B',0,'R');		
$pdf->Cell(38,5,number_format($SubTotal+$SubTotalFarma,2),'R'.'T'.'B',0,'R');	
//$pdf->ln(5);
//$pdf->Cell(140,5,'TOTAL NETO','L'.'R'.'T'.'B',0,'R');  
//$pdf->Cell(50,5,number_format($tPagado,2),'R'.'T'.'B',0,'R');
//$pdf->ln(5);
//$pdf->Cell(140,5,'EXTORNOS ','L'.'R'.'T'.'B',0,'R');  
//$pdf->Cell(50,5,'-'.number_format($tAnulado,2),'R'.'T'.'B',0,'R');	
$pdf->ln(5);
$pdf->Cell(152,5,'EXONERACIONES','L'.'R'.'T'.'B',0,'R');  
$pdf->Cell(38,5,'-'.number_format($Exoneracion+$ExoneraFarmacia,2),'R'.'T'.'B',0,'R');
$pdf->ln(5);
$pdf->Cell(152,5,'DEVOLUCIONES','L'.'R'.'T'.'B',0,'R');  
$pdf->Cell(38,5,'-'.number_format($tDevolucion,2),'R'.'T'.'B',0,'R');
$pdf->ln(5);
$pdf->Cell(152,5,'DEVOLUCIONES DEL DIA','L'.'R'.'T'.'B',0,'R');  
$pdf->Cell(38,5,'-'.number_format($DebueltoDia,2),'R'.'T'.'B',0,'R');

$pdf->ln(5);
$pdf->Cell(152,5,'REDONDEO','L'.'R'.'T'.'B',0,'R');  
$pdf->Cell(38,5,'-'.number_format($RedondeoFarmaPositico,2),'R'.'T'.'B',0,'R');
	
$pdf->ln(5);
$pdf->Cell(152,5,'TOTAL RECAUDADO','L'.'R'.'T'.'B',0,'R');  
$pdf->Cell(38,5,number_format($tPagado-$tDevolucion-$DebueltoDia,2),'R'.'T'.'B',0,'R');	
*/ 
$pdf->Output();

?>