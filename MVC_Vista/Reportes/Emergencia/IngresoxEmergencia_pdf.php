<?php
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/EmergenciaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');

	class PDF extends FPDF
{
function Header()
{
    	
		
		
		
	$this->SetFont('Arial','',11);
		$this->Cell(60);
			 $this->Cell(33,5,'','0',0,'C');	
		 $this->Cell(1);
		 $this->Image('../../../MVC_Complemento/img/grcallo.jpg',26,3,18,20);
		
		
		$this->Cell(100,4,'"HOSPITAL NACIONAL "DANIEL ALCIDES CARRION"',0,0,'C');
		$this->ln(4);
		$this->Cell(100);
		$this->Cell(95,5,'DEPARTAMENTO DE EMERGENCIA Y CUIDADOS CRITICOS',0,0,'C');
        $this->SetFont('Arial','B',11);
		$this->Ln(4);
		$this->Cell(100);
		$this->Cell(94,5,'REPORTE INGRESO POR EMERGENCIA',0,0,'C');
		$this->Cell(50);
		 $this->Image('../../../MVC_Complemento/img/hndac.jpg',256,3,18,20); 
		$this->Ln(8);	
    	$this->SetFont('Arial','B',7);
		$this->cell(13,4,utf8_decode('N°Cuenta'),'1',0,'C');
		$this->cell(50,4,'Apellidos Y Nombres','1',0,'C');
		$this->Cell(12,4,'HC','1',0,'C');
		$this->Cell(14,4,'DNI','1',0,'C');
		$this->Cell(9,4,'Edad','1',0,'C');
		$this->cell(30,4,'Fecha/Hora Ingreso','1',0,'C');
		
		$this->Cell(42,4,'Servicio','1',0,'C');
		$this->Cell(21,4,'Financiamiento','1',0,'C');
		$this->Cell(22,4,'Prio.','1',0,'C');
		$this->cell(19,4,'CIE-Ing','1',0,'C');
		$this->cell(48,4,'Sig-vital','1',0,'C');
	
		

		
}

function Footer()
	{
		
		$this->SetY(-10);
		
		$this->Cell(150,5,"WWW.HNDAC.GOB.PE",0,0,'L');
 		$this->Cell(240,5,"OEI/UI/ADS",0,0,'C');
		$this->LN(0);
			
		$this->Cell(0,10,'Pages'.$this->pageno().'/{nb}',0,0,'C');	
	}
}


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('L' , 'A4');
$pdf->SetFont('Arial','',8);
$salto=0;

$a=4;
$ListarReporte=ReporteIngreso_emergencia01_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]),$_REQUEST["IdServicio"]);
 if($ListarReporte != NULL)	{ 

 foreach($ListarReporte as $item){
if($item["TriajeTalla"]!=NULL){$TriajeTalla='Tal: '.$item["TriajeTalla"];}
if($item["TriajePeso"]!=NULL){$TriajePeso=' Pe: '.$item["TriajePeso"];}
if($item["TriajePresion"]!=NULL){$TriajePresion=' Pr: '.$item["TriajePresion"];}
if($item["TriajeTemperatura"]!=NULL){$TriajeTemperatura=' Tm: '.$item["TriajeTemperatura"];}
if($item["TriajePulso"]!=NULL){$TriajePulso=' Pul: '.$item["TriajePulso"];}
if($item["TriajeFrecRespiratoria"]!=NULL){$TriajeFrecRespiratoria=' F.Rs: '.$item["TriajeFrecRespiratoria"];}
if($item["TriajeFrecCardiaca"]!=NULL){$TriajeFrecCardiaca=' F.Cr: '.$item["TriajeFrecCardiaca"];}	 
	 
//for($i=0;$i<=250;$i++){

	$pdf->ln($a);
	                             

		$pdf->Cell(13,4,$item["IdCuentaAtencion"],1,0,'L');
		$pdf->Cell(50,4,utf8_decode(substr($item["ApellidoPaterno"].' '.$item["ApellidoMaterno"].' '.$item["PrimerNombre"],0,29)),1,0,'L');
		$pdf->Cell(12,4,$item["NroHistoriaClinica"],'1',0,'C');
		$pdf->Cell(14,4,$item["NroDocumento"],'1',0,'C');
		$pdf->Cell(9,4,$item["EdadPci"],'1',0,'C');
		$pdf->cell(30,4,$item["FechaIngreso"].' '.$item["HoraIngreso"],'1',0,'C');
		$pdf->Cell(42,4,utf8_decode($item["NombreServicio"]),'1',0,'C');
		$pdf->Cell(21,4,$item["NombreFuenteFinanciam"],'1',0,'C');
		$pdf->Cell(22,4,$item["TipoGravedad"],'1',0,'C');
		$pdf->cell(19,4,$item["NombeDiagnos"],'1',0,'C');
		$pdf->cell(48,4,$TriajeTalla.$TriajePeso.$TriajePresion.$TriajeTemperatura.$TriajePulso.$TriajeFrecRespiratoria.$TriajeFrecCardiaca,'1',0,'C');






//$item["SexoPaci"]







			





$salto=$salto+4;
if($salto>155){$pdf->AddPage('L','A4');$pdf->ln(0);$salto=0;}
}
}


$pdf->Output();

?>