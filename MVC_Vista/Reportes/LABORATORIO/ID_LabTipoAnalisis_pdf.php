<?php 
ini_set('memory_limit', '1024M'); 
require('../../../MVC_Modelo/ReportesM.php');
require('../../../MVC_Modelo/SistemaM.php');
require('../../../MVC_Complemento/fpdf/fpdf.php');
require('../../../MVC_Complemento/librerias/Funciones.php');


class PDF extends FPDF
{
function Header(){	
		$this->SetFont('Arial','',9);
		$this->Cell(20);
		$this->Image('../../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
		$this->Cell(37);
		$this->setfont('arial','b',12);
		$this->Cell(70,2,'HOSPITAL NACIONAL DANIEL ALCIDES CARRION',0,0,'C');
		$this->Cell(70);
		$this->SetFont('Arial','',9); 
	    $this->Cell(-25,4,'F.Imp: '.date("d/m/Y"),0,0,'R');
		$this->Ln(4);
		$this->Cell(22,4,'',0,0,'L');
		$this->Cell(135);
		$this->Cell(14,4,'H.Imp: '.date("H:i:s"),0,0,'R');
		$this->Ln(3);
		$this->Cell(49);
		$NumPag=$this->PageNo();
		if($_REQUEST["tipo_reporte"]==0)
		{
		$titulo='REPORTE POR GRUPO DE LABORATORIO';
		}
		else
		{
		$titulo='REPORTE POR EXAMENES DE LABORATORIO';
		}
		if($NumPag==1){
		$this->Cell(80,5,''.$titulo,0,0,'C');	
		 }else{
		$this->Cell(80,5,''.$titulo,0,0,'C'); 
			 }
		$this->Cell(25);
		$this->Cell(17,4,'Pagina:  '.$this->PageNo(),0,0,'R');
		$this->Image('../../../MVC_Complemento/img/grcallo.jpg',187,5,18,20);
		$this->Ln(5);
		$this->setfont('arial','b',12);
		$this->Cell(181,5,'RANGO  '.$_REQUEST["FechaInicio"].' - '.$_REQUEST["FechaFinal"],0,0,'C');
		$this->Ln(8);
	}
	function Footer()
	{
		 $this->SetY(-10);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,"HNDAC - ".$_REQUEST["Usuario"],0,0,'L');
 		$this->Cell(40,5,"OESI/UI/DS" ,0,0,'C');
		$this->Ln(5);
		$this->SetFont('Arial','',9);
		$this->Cell(150,5,'Terminal()',0,0,'L');
 				
	}
}


if($_REQUEST["tipo_reporte"]==0)
{
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','b',9);
$pdf->Cell(100,5,'NOMBRES',1,0,'C');  		
$pdf->Cell(60,5,'TOTAL',1,0,'C');		
$ListarReporte=SigesaLaboratorioporAreaPorRangoFechas_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));
 if($ListarReporte != NULL)	{ 
	  $Cantidad=0;
	  foreach($ListarReporte as $item){
				$pdf->ln(5);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(100,5,utf8_decode(substr($item["Descripcion"],0,70)),1,0,'L');  
				$pdf->Cell(60,5,$item["Cantidad"],1,0,'R');						
				$Cantidad=$Cantidad+$item["Cantidad"];	
		}	
	  }
	            $pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(100,5,'TOTAL',1,0,'C');  
				$pdf->Cell(60,5,$Cantidad,1,0,'R');	
}
else

{
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','b',9);
$pdf->Cell(150,5,'NOMBRES',1,0,'C');  		
$pdf->Cell(40,5,'TOTAL',1,0,'C');		
$ListarReporte=SigesaLaboratorioporExamenesPorRangoFechas_M(gfecha($_REQUEST["FechaInicio"]),gfecha($_REQUEST["FechaFinal"]));
 if($ListarReporte != NULL)	{ 
	  $Cantidad=0;
	  foreach($ListarReporte as $item){
				$pdf->ln(5);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(150,5,utf8_decode(substr($item["Nombre"],0,70)),1,0,'L');  
				$pdf->Cell(40,5,$item["Cantidad"],1,0,'R');						
				$Cantidad=$Cantidad+$item["Cantidad"];	
		}	
	  }
	            $pdf->ln(5);
				$pdf->SetFont('Arial','b',9);
				$pdf->Cell(150,5,'TOTAL',1,0,'C');  
				$pdf->Cell(40,5,$Cantidad,1,0,'R');	
}


$pdf->Output();
?>