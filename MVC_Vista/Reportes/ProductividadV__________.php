<?php
ini_set('memory_limit', '1024M');

 ?>
<!DOCTYPE HTML>
    <html>
    <head>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }	
		.highcharts-title{
			font-size:14px !important;
		}
		</style>
	
		<script type="text/javascript">
		
		function Atenciones_Medicas_Total(value,row,index){
                return 'background-color:#E0F2F7;font-weight:bold;color:#000;';
        }

		function Horas_Programadas_Total(value,row,index){
                return 'background-color:#F8ECE0;font-weight:bold;color:#000;';
        }
		
		function Productividad_Total(value,row,index){
                return 'background-color:#E0F8EC;font-weight:bold;color:#000;';
        }
		
        function Atenciones_Medicas(value,row,index){
                return 'background-color:#EFF8FB;color:#000;';
        }

        function Horas_Programadas(value,row,index){
                return 'background-color:#FBF5EF;color:#000;';
        }

        function Productividad(value,row,index){
			    if(value >= 4)
				{
				 return 'background-color:#EFFBF5;color:#FF0000;';	
				}
				if(value < 4)
				{
				 return 'background-color:#EFFBF5;color:#000;';	
				}
               
        }
		
		function Especialidades(value,row,index){
				if(value.substring(0, 12)=='DEPARTAMENTO'  ||  value.substring(0, 1)=='-'){
				return 'font-weight:bold;color:#000;';	
				}
                
        }

		</script>
		
		
		<script  type="text/javascript">
						
			$(document).ready(function() {
				
				$('#w').window('close');
				$("#Generar_Reporte_Servicio").click(function(event) {
									var mes = $("#Mes option:selected").html();
									var anio= $("#Anio option:selected").html();
									var anio_reporte=document.getElementById('Anio').value;
									var mes_reporte=document.getElementById('Mes').value;
									var Contenido_1="HOSPITAL NACIONAL DANIEL ALCIDES CARRION <BR>";
									var Contenido_2="HNDAC 	  : PRODUCTIVIDAD HORA MEDICO EN CONSULTA EXTERNA POR DEPARTAMENTO Y SERVICIOS<BR>";
									var Contenido_3="PERIODO  : A "+mes+' '+anio+"<BR>";
									$("#Contenido_Cabecera").html(Contenido_1+Contenido_2+Contenido_3.toUpperCase());
									var  b=true;
									switch (mes_reporte) {
										case '1':
										  var mes_arreglo = [false,true,true,true,true,true,true,true,true,true,true,true];
										  var  longitud =[200,100,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '2':
										  var mes_arreglo = [false,false,true,true,true,true,true,true,true,true,true,true];
										   var  longitud =[100,100,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '3':
										  var mes_arreglo = [false,false,false,true,true,true,true,true,true,true,true,true];
										  var  longitud =[70,70,70,36,36,36,36,36,36,36,36,36];
										  break;
										case '4':
										  var mes_arreglo = [false,false,false,false,true,true,true,true,true,true,true,true];
										  var  longitud =[50,50,50,50,36,36,36,36,36,36,36,36];
										  break;										  
										case '5':
										  var mes_arreglo = [false,false,false,false,false,true,true,true,true,true,true,true];
										  var  longitud =[45,45,45,45,45,36,36,36,36,36,36,36];
										  break; 
										case '6':
										  var mes_arreglo = [false,false,false,false,false,false,true,true,true,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '7':
										  var mes_arreglo = [false,false,false,false,false,false,false,true,true,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '8':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,true,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '9':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '10':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,false,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '11':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,false,false,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '12':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,false,false,false];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;										  
									  }
									var dg = $('#Productividad').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									singleSelect:true,
									collapsible:true,
									multiSort:true,
								    url:'../../MVC_Controlador/Reportes/ReportesC.php?acc=MostrarProductividad&Anio='+anio_reporte+'&Mes='+mes_reporte,
									columns:[
											[{colspan:40}],
											[{field:'Especialidad',title:'<strong>DEPARTAMENTOS / SERVICIOS</strong>',width:210,sortable:true,rowspan:2,styler:Especialidades},
											{title:'<strong>AÑO</strong>',colspan:1,align:'right'},
											{title:'Atenciones Medicas',colspan:mes_reporte,align:'right'},	
											{title:'<strong>AÑO</strong>',colspan:1,align:'right'},
											{title:'Horas Programadas',colspan:mes_reporte,align:'right'},
											{title:'<strong>AÑO</strong>',colspan:1,align:'right'},
											{title:'Productividad Hora Medico',colspan:mes_reporte,align:'left'}
											],
											[
											{field:'Total_Atenciones',title:'<strong>'+anio+'</strong>',width:50,sortable:true,styler:Atenciones_Medicas_Total,align:'center'},
											{field:'jan_a',title:'<strong>Ene</strong>',width:longitud[0],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[0]},
											{field:'feb_a',title:'<strong>Feb</strong>',width:longitud[1],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[1]},
											{field:'mar_a',title:'<strong>Mar</strong>',width:longitud[2],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[2]},
											{field:'apr_a',title:'<strong>Abr</strong>',width:longitud[3],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[3]},
											{field:'may_a',title:'<strong>May</strong>',width:longitud[4],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[4]},
											{field:'jun_a',title:'<strong>Jun</strong>',width:longitud[5],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[5]},
											{field:'jul_a',title:'<strong>Jul</strong>',width:longitud[6],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[6]},
											{field:'aug_a',title:'<strong>Aug</strong>',width:longitud[7],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[7]},
											{field:'sep_a',title:'<strong>Set</strong>',width:longitud[8],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[8]},
											{field:'oct_a',title:'<strong>Oct</strong>',width:longitud[9],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[9]},	
											{field:'nov_a',title:'<strong>Nov</strong>',width:longitud[10],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[10]},
											{field:'dec_a',title:'<strong>Dic</strong>',width:longitud[11],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[11]},
											{field:'Total_Horas',title:'<strong>'+anio+'</strong>',width:50,sortable:true,styler:Horas_Programadas_Total,align:'center'},
											{field:'jan_h',title:'<strong>Ene</strong>',width:longitud[0],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[0]},
											{field:'feb_h',title:'<strong>Feb</strong>',width:longitud[1],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[1]},
											{field:'mar_h',title:'<strong>Mar</strong>',width:longitud[2],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[2]},
											{field:'apr_h',title:'<strong>Abr</strong>',width:longitud[3],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[3]},
											{field:'may_h',title:'<strong>May</strong>',width:longitud[4],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[4]},
											{field:'jun_h',title:'<strong>Jun</strong>',width:longitud[5],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[5]},
											{field:'jul_h',title:'<strong>Jul</strong>',width:longitud[6],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[6]},
											{field:'aug_h',title:'<strong>Aug</strong>',width:longitud[7],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[7]},
											{field:'sep_h',title:'<strong>Set</strong>',width:longitud[8],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[8]},
											{field:'oct_h',title:'<strong>Oct</strong>',width:longitud[9],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[9]},	
											{field:'nov_h',title:'<strong>Nov</strong>',width:longitud[10],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[10]},
											{field:'dec_h',title:'<strong>Dic</strong>',width:longitud[11],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[11]},
											{field:'Total_Productividad',title:'<strong>'+anio+'</strong>',width:50,sortable:true,styler:Productividad_Total,align:'center',formatter: function(value,row,index){
												return (row.Total_Atenciones/row.Total_Horas).toFixed(2);
											}
											},
											{field:'jan_p',title:'<strong>Ene</strong>',width:longitud[0],sortable:true,styler:Productividad,hidden:mes_arreglo[0]},
											{field:'feb_p',title:'<strong>Feb</strong>',width:longitud[1],sortable:true,styler:Productividad,hidden:mes_arreglo[1]},
											{field:'mar_p',title:'<strong>Mar</strong>',width:longitud[2],sortable:true,styler:Productividad,hidden:mes_arreglo[2]},
											{field:'apr_p',title:'<strong>Abr</strong>',width:longitud[3],sortable:true,styler:Productividad,hidden:mes_arreglo[3]},
											{field:'may_p',title:'<strong>May</strong>',width:longitud[4],sortable:true,styler:Productividad,hidden:mes_arreglo[4]},
											{field:'jun_p',title:'<strong>Jun</strong>',width:longitud[5],sortable:true,styler:Productividad,hidden:mes_arreglo[5]},
											{field:'jul_p',title:'<strong>Jul</strong>',width:longitud[6],sortable:true,styler:Productividad,hidden:mes_arreglo[6]},
											{field:'aug_p',title:'<strong>Aug</strong>',width:longitud[7],sortable:true,styler:Productividad,hidden:mes_arreglo[7]},
											{field:'sep_p',title:'<strong>Set</strong>',width:longitud[8],sortable:true,styler:Productividad,hidden:mes_arreglo[8]},
											{field:'oct_p',title:'<strong>Oct</strong>',width:longitud[9],sortable:true,styler:Productividad,hidden:mes_arreglo[9]},	
											{field:'nov_p',title:'<strong>Nov</strong>',width:longitud[10],sortable:true,styler:Productividad,hidden:mes_arreglo[10]},
											{field:'dec_p',title:'<strong>Dic</strong>',width:longitud[11],sortable:true,styler:Productividad,hidden:mes_arreglo[11]}												
										]],
										onSelect: function(index,row){
											var  titulo=row.Especialidad;
											var  atenciones=[numero(row.jan_a),numero(row.feb_a),numero(row.mar_a),numero(row.apr_a),numero(row.may_a),numero(row.jun_a),numero(row.jul_a),numero(row.aug_a),numero(row.sep_a),numero(row.oct_a),numero(row.nov_a),numero(row.dec_a)]; 
											var  horas=[numero(row.jan_h),numero(row.feb_h),numero(row.mar_h),numero(row.apr_h),numero(row.may_h),numero(row.jun_h),numero(row.jul_h),numero(row.aug_h),numero(row.sep_h),numero(row.oct_h),numero(row.nov_h),numero(row.dec_h)];
											var  productividad=[numero(row.jan_p),numero(row.feb_p),numero(row.mar_p),numero(row.apr_p),numero(row.may_p),numero(row.jun_p),numero(row.jul_p),numero(row.aug_p),numero(row.sep_p),numero(row.oct_p),numero(row.nov_p),numero(row.dec_p)];
											Cargar_Grafico_Lineas(titulo,atenciones,horas,productividad,Contenido_3);
										}	
									});
				});	


				
				
				$("#Generar_Reporte_Medico").click(function(event) {
									var mes = $("#Mes_Medico option:selected").html();
									var anio= $("#Anio_Medico option:selected").html();
									var anio_reporte=document.getElementById('Anio_Medico').value;
									var mes_reporte=document.getElementById('Mes_Medico').value;
									var Contenido_1="HOSPITAL NACIONAL DANIEL ALCIDES CARRION <BR>";
									var Contenido_2="HNDAC 	  : PRODUCTIVIDAD HORA MEDICO EN CONSULTA EXTERNA POR DEPARTAMENTO Y SERVICIOS<BR>";
									var Contenido_3="PERIODO  : A "+mes+' '+anio+"<BR>";
									$("#Contenido_Cabecera").html(Contenido_1+Contenido_2+Contenido_3.toUpperCase());
									switch (mes_reporte) {
										case '1':
										  var mes_arreglo = [false,true,true,true,true,true,true,true,true,true,true,true];
										  var  longitud =[200,100,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '2':
										  var mes_arreglo = [false,false,true,true,true,true,true,true,true,true,true,true];
										   var  longitud =[100,100,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '3':
										  var mes_arreglo = [false,false,false,true,true,true,true,true,true,true,true,true];
										  var  longitud =[70,70,70,36,36,36,36,36,36,36,36,36];
										  break;
										case '4':
										  var mes_arreglo = [false,false,false,false,true,true,true,true,true,true,true,true];
										  var  longitud =[50,50,50,50,36,36,36,36,36,36,36,36];
										  break;										  
										case '5':
										  var mes_arreglo = [false,false,false,false,false,true,true,true,true,true,true,true];
										  var  longitud =[45,45,45,45,45,36,36,36,36,36,36,36];
										  break; 
										case '6':
										  var mes_arreglo = [false,false,false,false,false,false,true,true,true,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '7':
										  var mes_arreglo = [false,false,false,false,false,false,false,true,true,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '8':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,true,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '9':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,true,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '10':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,false,true,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '11':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,false,false,true];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;
										case '12':
										  var mes_arreglo = [false,false,false,false,false,false,false,false,false,false,false,false];
										  var  longitud =[36,36,36,36,36,36,36,36,36,36,36,36];
										  break;										  
									  }
									var dg = $('#Productividad').datagrid({
									filterBtnIconCls:'icon-filter',
									singleSelect:true,
									rowtexts:true,
									pageSize:20,
									remotefilter:false,
									remoteSort:false,
									singleSelect:true,
									collapsible:true,
									multiSort:true,
								    url:'../../MVC_Controlador/Reportes/ReportesC.php?acc=MostrarProductividadMedico&Anio='+anio_reporte+'&Mes='+mes_reporte,
									columns:[
											[{colspan:40}],
											[{field:'Especialidad',title:'<strong>DEPARTAMENTOS / SERVICIOS</strong>',width:210,sortable:true,rowspan:2,styler:Especialidades},
											{title:'<strong>AÑO</strong>',colspan:1,align:'right'},
											{title:'Atenciones Medicas',colspan:mes_reporte,align:'right'},	
											{title:'<strong>AÑO</strong>',colspan:1,align:'right'},
											{title:'Horas Programadas',colspan:mes_reporte,align:'right'},
											{title:'<strong>AÑO</strong>',colspan:1,align:'right'},
											{title:'Productividad Hora Medico',colspan:mes_reporte,align:'left'}
											],
											[
											{field:'Total_Atenciones',title:'<strong>'+anio+'</strong>',width:50,sortable:true,styler:Atenciones_Medicas_Total,align:'center'},
											{field:'jan_a',title:'<strong>Ene</strong>',width:longitud[0],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[0]},
											{field:'feb_a',title:'<strong>Feb</strong>',width:longitud[1],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[1]},
											{field:'mar_a',title:'<strong>Mar</strong>',width:longitud[2],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[2]},
											{field:'apr_a',title:'<strong>Abr</strong>',width:longitud[3],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[3]},
											{field:'may_a',title:'<strong>May</strong>',width:longitud[4],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[4]},
											{field:'jun_a',title:'<strong>Jun</strong>',width:longitud[5],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[5]},
											{field:'jul_a',title:'<strong>Jul</strong>',width:longitud[6],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[6]},
											{field:'aug_a',title:'<strong>Aug</strong>',width:longitud[7],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[7]},
											{field:'sep_a',title:'<strong>Set</strong>',width:longitud[8],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[8]},
											{field:'oct_a',title:'<strong>Oct</strong>',width:longitud[9],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[9]},	
											{field:'nov_a',title:'<strong>Nov</strong>',width:longitud[10],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[10]},
											{field:'dec_a',title:'<strong>Dic</strong>',width:longitud[11],sortable:true,styler:Atenciones_Medicas,hidden:mes_arreglo[11]},
											{field:'Total_Horas',title:'<strong>'+anio+'</strong>',width:50,sortable:true,styler:Horas_Programadas_Total,align:'center'},
											{field:'jan_h',title:'<strong>Ene</strong>',width:longitud[0],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[0]},
											{field:'feb_h',title:'<strong>Feb</strong>',width:longitud[1],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[1]},
											{field:'mar_h',title:'<strong>Mar</strong>',width:longitud[2],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[2]},
											{field:'apr_h',title:'<strong>Abr</strong>',width:longitud[3],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[3]},
											{field:'may_h',title:'<strong>May</strong>',width:longitud[4],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[4]},
											{field:'jun_h',title:'<strong>Jun</strong>',width:longitud[5],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[5]},
											{field:'jul_h',title:'<strong>Jul</strong>',width:longitud[6],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[6]},
											{field:'aug_h',title:'<strong>Aug</strong>',width:longitud[7],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[7]},
											{field:'sep_h',title:'<strong>Set</strong>',width:longitud[8],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[8]},
											{field:'oct_h',title:'<strong>Oct</strong>',width:longitud[9],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[9]},	
											{field:'nov_h',title:'<strong>Nov</strong>',width:longitud[10],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[10]},
											{field:'dec_h',title:'<strong>Dic</strong>',width:longitud[11],sortable:true,styler:Horas_Programadas,hidden:mes_arreglo[11]},
											{field:'Total_Productividad',title:'<strong>'+anio+'</strong>',width:50,sortable:true,styler:Productividad_Total,align:'center',formatter: function(value,row,index){
												return (row.Total_Atenciones/row.Total_Horas).toFixed(2);
											}
											},
											{field:'jan_p',title:'<strong>Ene</strong>',width:longitud[0],sortable:true,styler:Productividad,hidden:mes_arreglo[0]},
											{field:'feb_p',title:'<strong>Feb</strong>',width:longitud[1],sortable:true,styler:Productividad,hidden:mes_arreglo[1]},
											{field:'mar_p',title:'<strong>Mar</strong>',width:longitud[2],sortable:true,styler:Productividad,hidden:mes_arreglo[2]},
											{field:'apr_p',title:'<strong>Abr</strong>',width:longitud[3],sortable:true,styler:Productividad,hidden:mes_arreglo[3]},
											{field:'may_p',title:'<strong>May</strong>',width:longitud[4],sortable:true,styler:Productividad,hidden:mes_arreglo[4]},
											{field:'jun_p',title:'<strong>Jun</strong>',width:longitud[5],sortable:true,styler:Productividad,hidden:mes_arreglo[5]},
											{field:'jul_p',title:'<strong>Jul</strong>',width:longitud[6],sortable:true,styler:Productividad,hidden:mes_arreglo[6]},
											{field:'aug_p',title:'<strong>Aug</strong>',width:longitud[7],sortable:true,styler:Productividad,hidden:mes_arreglo[7]},
											{field:'sep_p',title:'<strong>Set</strong>',width:longitud[8],sortable:true,styler:Productividad,hidden:mes_arreglo[8]},
											{field:'oct_p',title:'<strong>Oct</strong>',width:longitud[9],sortable:true,styler:Productividad,hidden:mes_arreglo[9]},	
											{field:'nov_p',title:'<strong>Nov</strong>',width:longitud[10],sortable:true,styler:Productividad,hidden:mes_arreglo[10]},
											{field:'dec_p',title:'<strong>Dic</strong>',width:longitud[11],sortable:true,styler:Productividad,hidden:mes_arreglo[11]}												
										]],
										onSelect: function(index,row){
											var  titulo=row.Especialidad;
											var  atenciones=[numero(row.jan_a),numero(row.feb_a),numero(row.mar_a),numero(row.apr_a),numero(row.may_a),numero(row.jun_a),numero(row.jul_a),numero(row.aug_a),numero(row.sep_a),numero(row.oct_a),numero(row.nov_a),numero(row.dec_a)]; 
											var  horas=[numero(row.jan_h),numero(row.feb_h),numero(row.mar_h),numero(row.apr_h),numero(row.may_h),numero(row.jun_h),numero(row.jul_h),numero(row.aug_h),numero(row.sep_h),numero(row.oct_h),numero(row.nov_h),numero(row.dec_h)];
											var  productividad=[numero(row.jan_p),numero(row.feb_p),numero(row.mar_p),numero(row.apr_p),numero(row.may_p),numero(row.jun_p),numero(row.jul_p),numero(row.aug_p),numero(row.sep_p),numero(row.oct_p),numero(row.nov_p),numero(row.dec_p)];
											Cargar_Grafico_Lineas(titulo,atenciones,horas,productividad,Contenido_3);
										}	
									});
				});			
				
				
			  $("#btnExportServicio").click(function(e) {

									 var htmltable= document.getElementById('Contenido_Excel');
									var html = htmltable.outerHTML;
									window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));

				});
				
				$("#btnExportMedico").click(function(e) {

									var htmltable= document.getElementById('Contenido_Excel');
									var html = htmltable.outerHTML;
									window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));

				});
				

			});

		</script>
		
		<script type="text/javascript">
			function numero(a){
				if(a!=null){
				if(a=='0')
				{
				return null;
				}
				else
				{
				return parseFloat(a);	
				}				 
				}
				else
				{   
				return null;		
				}
			}

			function Cargar_Grafico_Lineas(titulo,atenciones,horas,pro,Contenido_3){
							$('#w').window('open');
							$('#container').highcharts({
								chart: {
									type: 'line'
								},
								title: {
									text: '<h1><strong>'+titulo+'</strong></h1>'
								},
								subtitle: {
									text: '<h2><strong>'+Contenido_3+'</strong></h2>'
								},
								xAxis: {
									categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic']
								},
								yAxis: {
									title: {
										text: 'Cantidades'
									}
								},
								credits: {
								enabled: false
								},
								plotOptions: {
									line: {
										dataLabels: {
											enabled: true
										},
										enableMouseTracking: false
									}
								},
								series: [{
									name: 'Atenciones Medicas',
									data: [atenciones[0],atenciones[1], atenciones[2], atenciones[3], atenciones[4], atenciones[5], atenciones[6], atenciones[7], atenciones[8], atenciones[9], atenciones[10], atenciones[11]]
								},
								{
									name: 'Horas Programadas',
									data: [horas[0],horas[1], horas[2], horas[3], horas[4], horas[5], horas[6], horas[7], horas[8], horas[9], horas[10], horas[11]]
								},
								{
									name: 'Productividad Hora Medica',
									data: [pro[0],pro[1], pro[2], pro[3], pro[4], pro[5], pro[6], pro[7], pro[8],pro[9], pro[10], pro[11]]
								}]
							});
		  }				
		</script>
    </head>
    <body>
		<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
		<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
		
		<div class="easyui-layout" style="width:100%;height:800px;">
						<div data-options="region:'north',split:true" title="Productividad Hora / Médico" style="height:130px">
						    <div class="easyui-tabs" style="width:100%;height:100%">
							<div title="Por Departamentos" style="padding:10px">
								<div style="padding:15px;float:left;margin:0px 20px 0 0;width:100px" >
										<STRONG>Seleccione Mes de Periodo: </STRONG>
										</div>
										<div style="padding:15px;float:left;margin:0px 20px 0 0">
										<?php 
										$mes_actual=date("m");
										?>
										<select name="Mes"  style="width:110px" id="Mes">
										<option value="1"  <?php if($mes_actual=='1'){?> selected <?php } ?> >Enero</option>
										<option value="2"  <?php if($mes_actual=='2'){?> selected <?php } ?> >Febrero</option>
										<option value="3"  <?php if($mes_actual=='3'){?> selected <?php } ?> >Marzo</option>
										<option value="4"  <?php if($mes_actual=='4'){?> selected <?php } ?> >Abril</option>
										<option value="5"  <?php if($mes_actual=='5'){?> selected <?php } ?> >Mayo</option>
										<option value="6"  <?php if($mes_actual=='6'){?> selected <?php } ?> >Junio</option>
										<option value="7"  <?php if($mes_actual=='7'){?> selected <?php } ?> >Julio</option>
										<option value="8"  <?php if($mes_actual=='8'){?> selected <?php } ?> > Agosto</option>
										<option value="9"  <?php if($mes_actual=='9'){?> selected <?php } ?> >Setiembre</option>
										<option value="10" <?php if($mes_actual=='10'){?> selected <?php } ?> >Octubre</option>
										<option value="11" <?php if($mes_actual=='11'){?> selected <?php } ?> >Noviembre</option>
										<option value="12" <?php if($mes_actual=='12'){?> selected <?php } ?> >Diciembre</option>
										</select>
										</div>
										<div style="padding:15px;float:left;margin:0px 20px 0 0;width:100px">
										<STRONG>Seleccione Año de Periodo: </STRONG>
										</div>
										<div style="padding:15px;float:left;margin:0px 20px 0 0">
										<select   name="Anio" style="width:100px" id="Anio">
										<?php 
										$resultados=ListarAnioM();	
										$anio_actual=date("Y");
										/*$anio_actual=date("Y");*/						
										if($resultados!=NULL){
										for ($i=0; $i < count($resultados); $i++) {
										
										?>
										<option value="<?php echo $resultados[$i]["Anio"] ?>" <?php if($anio_actual==$resultados[$i]["Anio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["Anio"]; ?></option>
										<?php 
										}}
										?>
										</select>
										</div>
										<div style="padding:5px 0;float:left;margin:5px 20px 0 0">
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:150px" id="Generar_Reporte_Servicio">Vista Previa<br></a>
										</div>						
										<div style="padding:5px 0;float:left;margin:5px 20px 0 0">
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" style="width:150px" id="btnExportServicio">Generar Excel<br></a>
										</div>
							</div>
							<div title="Por Médicos" style="padding:10px">
										<div style="padding:15px;float:left;margin:0px 20px 0 0;width:100px" >
										<STRONG>Seleccione Mes de Periodo: </STRONG>
										</div>
										<div style="padding:15px;float:left;margin:0px 20px 0 0">
										<?php 
										$mes_actual=date("m");
										?>
										<select name="Mes_Medico"  style="width:110px" id="Mes_Medico">
										<option value="1"  <?php if($mes_actual=='1'){?> selected <?php } ?> >Enero</option>
										<option value="2"  <?php if($mes_actual=='2'){?> selected <?php } ?> >Febrero</option>
										<option value="3"  <?php if($mes_actual=='3'){?> selected <?php } ?> >Marzo</option>
										<option value="4"  <?php if($mes_actual=='4'){?> selected <?php } ?> >Abril</option>
										<option value="5"  <?php if($mes_actual=='5'){?> selected <?php } ?> >Mayo</option>
										<option value="6"  <?php if($mes_actual=='6'){?> selected <?php } ?> >Junio</option>
										<option value="7"  <?php if($mes_actual=='7'){?> selected <?php } ?> >Julio</option>
										<option value="8"  <?php if($mes_actual=='8'){?> selected <?php } ?> > Agosto</option>
										<option value="9"  <?php if($mes_actual=='9'){?> selected <?php } ?> >Setiembre</option>
										<option value="10" <?php if($mes_actual=='10'){?> selected <?php } ?> >Octubre</option>
										<option value="11" <?php if($mes_actual=='11'){?> selected <?php } ?> >Noviembre</option>
										<option value="12" <?php if($mes_actual=='12'){?> selected <?php } ?> >Diciembre</option>
										</select>
										</div>
										<div style="padding:15px;float:left;margin:0px 20px 0 0;width:100px">
										<STRONG>Seleccione Año de Periodo: </STRONG>
										</div>
										<div style="padding:15px;float:left;margin:0px 20px 0 0">
										<select   name="Anio_Medico" style="width:100px" id="Anio_Medico">
										<?php 
										$resultados=ListarAnioM();	
										$anio_actual=date("Y");
										/*$anio_actual=date("Y");*/						
										if($resultados!=NULL){
										for ($i=0; $i < count($resultados); $i++) {
										
										?>
										<option value="<?php echo $resultados[$i]["Anio"] ?>" <?php if($anio_actual==$resultados[$i]["Anio"]){?> selected <?php } ?> ><?php echo $resultados[$i]["Anio"]; ?></option>
										<?php 
										}}
										?>
										</select>
										</div>
										<div style="padding:5px 0;float:left;margin:5px 20px 0 0">
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:150px" id="Generar_Reporte_Medico">Vista Previa<br></a>
										</div>							
										<div style="padding:5px 0;float:left;margin:5px 20px 0 0">
										<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-excel'" style="width:150px" id="btnExportMedico">Generar Excel<br></a>
										</div>
							</div>
							</div>
						</div>



						
						
					
						<div data-options="region:'center',title:'Productividad Hora Medico en Consulta Externa por Departamentos y Servicios',iconCls:'icon-ok'">
						
						
							<div id="w" class="easyui-window" title="Reporte" data-options="iconCls:'icon-save'" style="width: 810px; height: 550px;padding:10px;">
								<div id="container" style="width: 760px; height: 490px; margin: 0 auto"></div>
							</div>
						
						
							<div  id="Contenido_Excel">
								<div style="font-weight:bold;padding:6px;font-size:11px;margin-bottom:10px;margin-top:5px" id="Contenido_Cabecera">
								</div>
								<table id="Productividad"></table>
							</div>
						</div>
		</div>
		
    </body>
    </html>



