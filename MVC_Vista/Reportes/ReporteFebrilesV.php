    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Reportes Diarios - Diagnósticos</title>
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <style>
        html,body { 
            padding: 0px;
            margin: 0px;
            height: 100%;
            font-family: Helvetica; 

        }       
    </style>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>


            <script>
                    $(document).ready(function() {
                         function ExportarPDF()
                        {
                            var EdadIni = $("#edadInicio").numberspinner('getValue');
                            var EdadFin = $("#edadFin").numberspinner('getValue');
                            var Diagnostico = $("#Diagnostico").combobox('getValue');

                            //alert(EdadIni+' '+EdadFin+' '+Diagnostico);return false;

                            if(Diagnostico == ''){
                                if (Diagnostico == '' && EdadIni == '' && EdadFin == ''){
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "B";
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Pdf.php?fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&tipoReporte='+tipoReporte);
                                } else {
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "A";
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Pdf.php?fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&EdadIni='+EdadIni+'&EdadFin='+EdadFin+'&tipoReporte='+tipoReporte);
                                }
                            } else {
                                if (EdadIni == '' && EdadFin == '') {
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "C";
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Pdf.php?Diagnostico='+Diagnostico+'&fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&tipoReporte='+tipoReporte);
                                }
                                else {
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "D";
                                    //alert('../../MVC_Vista/BSC/Atenciones_febriles_Pdf.php?fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&EdadIni='+EdadIni+'&EdadFin='+EdadFin+'&Diagnostico='+Diagnostico+'&tipoReporte='+tipoReporte);
                                    //return;
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Pdf.php?Diagnostico='+Diagnostico+'&fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&EdadIni='+EdadIni+'&EdadFin='+EdadFin+'&tipoReporte='+tipoReporte);
                                }
                            }
                        }

                         function ExportarEXCEL()
                        {
                            var EdadIni = $("#edadInicio").numberspinner('getValue');
                            var EdadFin = $("#edadFin").numberspinner('getValue');
                            var Diagnostico = $("#Diagnostico").combobox('getValue');

                            //alert(EdadIni+' '+EdadFin+' '+Diagnostico);return false;

                            if(Diagnostico == ''){
                                if (Diagnostico == '' && EdadIni == '' && EdadFin == ''){
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "B";
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Excel.php?fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&tipoReporte='+tipoReporte);
                                } else {
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "A";
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Excel.php?fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&EdadIni='+EdadIni+'&EdadFin='+EdadFin+'&tipoReporte='+tipoReporte);
                                }
                            } else {
                                if (EdadIni == '' && EdadFin == '') {
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "C";
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Excel.php?Diagnostico='+Diagnostico+'&fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&tipoReporte='+tipoReporte);
                                }
                                else {
                                    var fechaIng = $("#fechaIng").datetimebox('getValue');
                                    var fechaFin = $("#fechaFin").datetimebox('getValue');
                                    var tipoReporte = "D";
                                    //alert('../../MVC_Vista/BSC/Atenciones_febriles_Pdf.php?fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&EdadIni='+EdadIni+'&EdadFin='+EdadFin+'&Diagnostico='+Diagnostico+'&tipoReporte='+tipoReporte);
                                    //return;
                                    window.open('../../MVC_Vista/BSC/Atenciones_febriles_Excel.php?Diagnostico='+Diagnostico+'&fechaIng='+fechaIng+'&fechaFin='+fechaFin+'&EdadIni='+EdadIni+'&EdadFin='+EdadFin+'&tipoReporte='+tipoReporte);
                                }
                            }
                        }

                        //EVENTOS       
                        $("#exportar_pdf").click(function(event) {
                            ExportarPDF();
                        });

                        $("#exportar_excel").click(function(event) {
                            ExportarEXCEL();
                        });
                    });

            </script>

    </head>
    <body>
        <div class="easyui-panel"  style="width:100%;height:180%;">

        <!-- CONTENEDORES -->
        <div class="easyui-layout" data-options="fit:true">
            
            <!-- CONTENEDOR DE OPERACIONES -->
            <div data-options="region:'north',split:false" style="height:18%;padding:1%;" title="Reportes Diarios - Diagnósticos">
                <div style="float:left;">
                    <table>
                    <tr>
                        <td> <strong><u>BUSQUEDAS</u></strong> </td>
                        <td style="width:350px;"></td>
                    </tr>
                    <tr>
                        <td>FECHA INICIO:</td>
                        <td style="width:350px;"><input id="fechaIng" class="easyui-datetimebox" style="width:150px;" value="<?php echo date('01/01/2016')?>"></td>
                    </tr>
                    <tr>
                        <td>FECHA FIN:</td>
                        <td style="width:350px;"><input id="fechaFin" class="easyui-datetimebox" style="width:150px;" value="<?php echo date('m/d/Y')?>"></td>
                    </tr>
                    <tr>
                        <td>EDAD EN AÑOS DESDE:</td>
                        <td style="width:350px;">
                            <input class="easyui-numberspinner" data-options="min:0,max:100,required:false" style="width:80px;" id="edadInicio"></input>
                        </td>
                    </tr>
                     <tr>
                        <td>EDAD AÑOS HASTA:</td>
                        <td style="width:350px;">
                            <input class="easyui-numberspinner" data-options="min:0,max:100,required:false" style="width:80px;" id="edadFin"></input>
                        </td>
                    </tr>
                    <tr>
                        <td>CÓDIGO DE DIAGNÓSTICO:</td>
                        <td style="width:350px;">
                            <input class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Reportes/ReportesC.php?acc=mostrarCodigos'" style="width:500px;" id="Diagnostico"></input>
                        </td>
                    </tr>
                </table>
                </div>

                <div style="float:left; padding:2%;">
                    <!--  <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" id="buscar_general">BUSCAR</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                    <a class="easyui-linkbutton" id="exportar_pdf"><img src="../../MVC_Complemento/img/pdf.png" height="15" width="15" alt="">EXPORTAR PDF</a>
                </div>
                <div style="float:left; padding:2%;">
                    <!--  <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" id="buscar_general">BUSCAR</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                    <a class="easyui-linkbutton" id="exportar_excel"><img src="../../MVC_Complemento/img/excel-icon.png" height="15" width="15" alt="">EXPORTAR EXCEL</a>
                </div>
            </div>
            <!-- VISUALIZACIONES 
            <div data-options="region:'center',split:false" style="height:82%;" title="Resultados">
                <div style="float:left;width:45%;padding:1%;">
                    <table class="easyui-datagrid" title="REPORTES" style="width:100%;height:auto;" data-options="singleSelect:true,collapsible: true," id="tabla_reportes">
                        <thead>
                            <tr>                            
                                <th data-options="field:'IDATENCION',rezisable:true,align:'center'">IDATENCIÓN</th>
                                <th data-options="field:'PACIENTE',rezisable:true,align:'left'">PACIENTE</th>
                                <th data-options="field:'DNI',rezisable:true,align:'center'">DNI</th>
                                <th data-options="field:'FNACIMIENTO',rezisable:true,align:'right'">FNACIMIENTO</th>
                                <th data-options="field:'SEXO',rezisable:true,align:'right'">SEXO</th>
                                <th data-options="field:'DISTRITO',rezisable:true,align:'left'">DISTRITO</th>
                                <th data-options="field:'EDAD',rezisable:true,align:'center'">EDAD</th>
                                <th data-options="field:'FECHAINGRESO',rezisable:true,align:'right'">FECHAINGRESO</th>
                                <th data-options="field:'CIE10',rezisable:true,align:'right'">CIE10</th>
                                <th data-options="field:'DIAGNOSTICO',rezisable:true,align:'right'">DIAGNÓSTICO</th>
                            </tr>
                        </thead>
                    </table>
                </div>-->

    </body>
    </html>