<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>REPORTE SOAT</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../../MVC_Complemento/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<script src="../../MVC_Complemento/bootstrap-3.3.7-dist/js/jquery.js"></script>
	<script src="../../MVC_Complemento/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>

	<script>


		function Exportar_Hospitalizados(Exportar){
      	
      var TiposServico = document.getElementById("tiposervicio").value;
  	  var FuenteFin = document.getElementById("fuentefin").value;
      var FechaInicio = document.getElementById("FechaInicio").value;
      var FechaFin = document.getElementById("FechaFin").value;

      elemento = document.getElementById("someSwitchOptionSuccess");
		if( elemento.checked ) { 
			var EstadoCuentas=1;
		}else{ EstadoCuentas=0;}

      if(Exportar=="excel"){
      	 location="../../MVC_Vista/Reportes/ReporteHospitalizados_Excel.php?TiposServico="+TiposServico+"&FuenteFin="+FuenteFin+"&FechaInicio="+FechaInicio+"&FechaFin="+FechaFin+"&EstadoCuentas="+EstadoCuentas;
      	 return 0;
      } } 

	</script>
	<style>
		.material-switch > input[type="checkbox"] {
    display: none;   
}

.material-switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
}

.material-switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
}
.material-switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
}
.material-switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
}
.material-switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}
	</style>
</head>
<body>
<div class="container-fluid" style="margin-left: 20px;">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-md-offset-3 col-lg-offset-0">
      
      <h3 align="center">Reporte Hospitalizados</h3>
        <form class="form-horizontal">
          <div class="form-group col-md-5 col-xs-12">
            <label for="location1" class="control-label">Tipo Servicio</label>
            <select class="form-control" name="" id="tiposervicio">
	            <?php 
	            if($TiposServicios != NULL){ 
	            foreach($TiposServicios as $item2){?>
	            <option value="<?php echo $item2["IDTIPOSERVICIO"]?>"<?php if($item2["IDTIPOSERVICIO"]==$IdAlmacen){?>selected<?php }?>><?php echo $item2["DESCRIPCION"]?></option>
	            <?php }}?>
            </select>
          </div>
          <div class="col-md-1"></div>
          <div class="form-group col-md-5 col-xs-12">
            <label for="type1" class="control-label">Fuente Financiamiento</label>
            <select class="form-control" name="" id="fuentefin">
              <?php 
	            if($FuentesFinanciamiento != NULL){ 
	            foreach($FuentesFinanciamiento as $item1){?>
	            <option value="<?php echo $item1["IdFuenteFinanciamiento"]?>"<?php if($item1["IdFuenteFinanciamiento"]==$IdAlmacen){?><?php }?>><?php echo $item1["Descripcion"]?></option>
	            <?php }}?>
            </select>
          </div>
          <div class="form-group col-md-5 col-xs-12">
          	<label for="FechaInicio" class="control-label">Fecha Inicio:</label>
			   <div class="input-group">
	          	<div class="input-group-addon" id="basic-addon1">
						<img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador" width="16" height="15" border="0" id="lanzador" title="Fecha Inicio"  />
					</div>	
	            <input name="FechaInicio" type="text" class="form-control" id="FechaInicio" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 00:00:00"   size="10"  autocomplete=OFF  />
	              <script type="text/javascript"> 
				   Calendar.setup({ 
				    inputField     :    "FechaInicio",     // id del campo de texto 
				     ifFormat     :     "%d/%m/%Y 00:00:00",     // formato de la fecha que se escriba en el campo de texto 
				     button     :    "lanzador"     // el id del botón que lanzará el calendario 
					}); 
				  </script>
              </div>
          	</div>
          <div class="col-md-1"	></div>
          <div class="form-group col-md-5 col-xs-12">
          	<label for="FechaFin" class="control-label">Fecha Fin:</label>
			   <div class="input-group">
	          	<div class="input-group-addon" id="basic-addon1">
						<img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Fin"  />
					</div>	
	            <input name="FechaFin" type="text" class="form-control" id="FechaFin" value="<?php echo vfecha(substr($FechaServidor,0,10))?> 23:59:59"   size="10"  autocomplete=OFF  />
	              <script type="text/javascript"> 
				   Calendar.setup({ 
				    inputField     :    "FechaFin",     // id del campo de texto 
				     ifFormat     :     "%d/%m/%Y 23:59:59",     // formato de la fecha que se escriba en el campo de texto 
				     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
					}); 
				  </script>
              </div>
          	</div>
          	<div class="col-md-1"	></div>
          <div class="form-group col-md-5 col-xs-12">
  
                        Cuentas Abiertas
                        <div class="material-switch pull-right">
                            <input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox" class="estadocuenta" value="1" />
                            <label for="someSwitchOptionSuccess" class="label-success"></label>
                        </div>

          	</div>
          </div>      
        </form>
    <div class="col-lg-1" style="margin-top: 110px;"><p class="text-center col-md-12"><button type="button" class="btn btn-default btn-md btn-primary" onclick="Exportar_Hospitalizados('excel')"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Exportar</button></p></div>
  </div>
  </div>
</div>
</body>
</html>