<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to Tablecloth</title>
<meta name="description" content="Tablecloth is unobtrusive way to add style and behaviour to html table elements.">
</meta>
<!-- paste this code into your webpage -->
<link href="../../MVC_Complemento/css/tablecloth.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../../MVC_Complemento/js/tablecloth.js"></script>
<!-- end -->

 <link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
 <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
 
 <script type="text/javascript">
 

function VentanaAgregar(ccparametro,NombreReporte){ 
	
	//var IdEmpleado=document.getElementById("IdEmpleado").value;
	
 var googlewin=dhtmlwindow.open("googlebox", "iframe", "../../MVC_Controlador/Reportes/ReportesC.php?acc=VistaReporte&IdReporte="+ccparametro+"&NombreReporte="+NombreReporte+"&Usuario=<?php echo $Usuario;?>",       "SIGESA: Reportes   ", "width=999px,height=600px,resize=0,scrolling=1,center=1", "recal");

 

}
 

</script>

<style>
body {
	margin:0;
	padding:0;
	background:#e5f1f4;
	font:90% Arial, Helvetica, sans-serif;
	color:#555;
	line-height:180%;
	text-align:left;
}
a {
	text-decoration:none;
	color:#057fac;
}
a:hover {
	text-decoration:none;
	color:#999;
}
h1 {
	font-size:220%;
	margin:0 20px;
	padding-top:1em;
	line-height:1em;
	color:#8bb544;
	font-weight:normal;
	font-family:Tahoma, Trebuchet MS, Arial, Helvetica, sans-serif;
}
h2 {
	font-size:170%;
	color:#8bb544;
	font-weight:normal;
	font-family:Tahoma, Trebuchet MS, Arial, Helvetica, sans-serif;
}
p.info {
	margin:0 20px;
	font-size:90%;
	color:#999;
}
p.floater{
	margin:0;
	position:absolute;
	top:2em;
	right:20px;
	float:left;
	line-height:2em;
	height:2em;
	font-size:120%;
	font-family:Tahoma, Trebuchet MS, Arial, Helvetica, sans-serif;
}
p.floater a{
	float:left;
	line-height:2em;
	height:2em;
	padding:0 20px;
	background:#8bb544;
	color:#fff;
}
p.floater a:hover{
	background:#4a98af;
	color:#fff;
}
code {
	font-size:110%;
}
pre {
	margin:1em 0;
	padding:1em 20px;
	line-height:150%;
	background:#e5f1f4;
	border-left:1px solid #a4d5e4;
}
table {
	font-size:90%;
}
#container {
	margin:0 auto;
	margin-top:2em;
	width:888px;
	background:#fff;
	padding-bottom:20px;
	text-align:left;
	position:relative;
}
#content {
	margin:0 20px;
}
p.sig {
	margin:0 auto;
	width:888px;
	padding:1em 0;
	text-align:left;
}
</style>
</head>
<body>
<div id="container">
	<h1>Reportes SIGESA- HNDAC</h1>
  <p class="info">Oficina de Estadistica e Informatica- Desarrollo de Software  </p>
	<div id="content">

	  
		<table cellspacing="0" cellpadding="0">
			 
                <?php 
				
	if($ListarReportes!=NULL){			
				
	foreach($ListarReportes as $item)   {
		?>
         <?php  
		 if(substr($item["Reporte"],0,2)=="--"){
		
		 ?>
                
			<tr>
				<th colspan="3"><?php echo $item["Reporte"]?></th>

			</tr>
             <?php }else{?>
             
             <tr> 
				<td><a href="#" onClick="VentanaAgregar('<?php echo $item["id_MenuReporte"]?>','<?php echo $item["Reporte"]?>');" class="nover"  ><?php echo $item["Reporte"]?></a></td>
				<td>-</td>
                <td><a href="#" onClick="VentanaAgregar('<?php echo $item["id_MenuReporte"]?>','<?php echo $item["Reporte"]?>');" class="nover"  ><img src="../../MVC_Complemento/img/Reportes.png" width="24" height="24" /></a><?php /*?><img src="../../MVC_Complemento/img/Estadistica.png" width="24" height="24" /><?php */?></td>
			</tr>
            
             <?php }}?>
		</table>
        
        <?php }?>
		<h2>  <?php /*?>with top headings</h2>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
		</table>
		<h2>Table with side headings</h2>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
		</table>
		<h2>Table with top and side headings</h2>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td>&nbsp;</td>
				<th>Title</th>
				<th>Title</th>
				<th>Title</th>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
			<tr>
				<th>Title</th>
				<td>Data</td>
				<td>Data</td>
				<td>Data</td>
			</tr>
		</table>
		
		<p>Any comments you might have you can post <a href="http://cssglobe.com/post.asp?id=1009">here</a><?php */?></p>
		
  </div>
</div>
 

 
