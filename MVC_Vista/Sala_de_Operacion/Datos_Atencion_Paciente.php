<html>
    <head>
	
	
		<meta charset="utf-8">
		<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
		<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
		<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
		<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
		<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
		<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
		<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
		<link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
		<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
		<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
		 <!-- Script de Validacion  --->
		 <link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
		 <script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
		<!-- Fin Script de Validacion  --->
	

		<!-- Script de Autocompletado  --->
		<link rel="stylesheet" href="../../MVC_Complemento/css/Autocompletado.css" type="text/css" />
		<!-- Fin Script de Autocompletado  --->
	
		<!-- Script de Hora  --->
		<link href="../../MVC_Complemento/css/jquerysctipttop.css" rel="stylesheet" type="text/css" media="all">
		<link href="../../MVC_Complemento/css/mtimepicker.css" type="text/css" rel="stylesheet">
		<script src="../../MVC_Complemento/js/mtimepicker.js" type="text/javascript"></script>
		<!-- Fin Script de Hora  --->


			<script type="text/javascript">
				function Autocompletar_Operacion() {
				var min_length = 0; 
				var Nombre = $('#Operacion').val();
				if (Nombre.length >= min_length) {
					$.ajax({
						url: '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						type: 'POST',
						data: {Nombre:Nombre,acc:'Seleccionar_Producto'},
						success:function(data){
							$('#Operacion_list_id').show();
							$('#Operacion_list_id').html(data);
						}
					});
				} else {
					$('#Operacion_list_id').hide();
				}
			}

			function set_item_operacion(Nombre,IdProducto) {
				$('#Operacion').val(Nombre);
				$('#Operacion_list_id').hide();
				alert(IdProducto);
			}
			</script>
			
		
			
			<script type="text/javascript">

				function Autocompletar_Diagnostico() {
				var min_length = 0; 
				var Diagnostico = $('#Diagnostico').val();
				if (Diagnostico.length >= min_length) {
					$.ajax({
						url: '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						type: 'POST',
						data: {Descripcion:Diagnostico,acc:'Seleccionar_Diagnostico'},
						success:function(data){
							$('#Diagnostico_list_id').show();
							$('#Diagnostico_list_id').html(data);
						}
					});
				} else {
					$('#Diagnostico_list_id').hide();
				}
			}

	
			function set_item_diagnostico(Nombre,IdProducto) {
				$('#Diagnostico').val(Nombre);
				$('#Diagnostico_list_id').hide();
				alert(IdProducto);
			}

			</script>
	

	
	
	
		<script type="text/javascript">
		$(function(){

/* Funcion que se ejecutara al Load  */
			$(document).ready(function(){
					CargarListaExamenes();
					Cargar_Datos_de_Pacientes(); 
			});		

			
			$('#btn_cargar_datos').click(function(){
					CargarListaExamenes();
					Cargar_Datos_de_Pacientes();

			});
			
		function Cargar_Datos_de_Pacientes() 
		{

						$.post('../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						{ 
						 nro_cuenta: $('#nro_cuenta').val(),
						acc: 'Mostrar_Paciente_por_Cuenta'
						},
						 function(res)
						{
							parsedRes = $.parseJSON(res);
							var parsedRes = $.parseJSON(res);  	
							$( '#Apellido_Paterno' ).val( parsedRes.ApellidoPaterno);
							$( '#Apellido_Materno' ).val( parsedRes.ApellidoMaterno);
							$( '#Nombres' ).val( parsedRes.PrimerNombre);	
							$( '#Sexo' ).val( parsedRes.Sexo);	
							$( '#Edad' ).val( parsedRes.Edad);	
							$( '#Servicio' ).val( parsedRes.Servicio);	
							$( '#Cama' ).val( parsedRes.Cama);	
							var  Edad=parsedRes.Edad;
							/*Calcular_Edad(Edad);*/
														
						}										
						);		
					}	
		
		function CargarListaExamenes()
		{
	
			var gridder = $('#lineaResultado');
			var UrlToPass = {
				    nro_cuenta: $('#nro_cuenta').val(),
					acc : 'CargarListaExamenes'
			};

			$.ajax({
				url : '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
				type : 'POST',
				data : UrlToPass,
				success: function(responseText) {
					gridder.html(responseText);
				}
			});
	
		}	
			
			

			function Calcular_Edad(Edad)
			{
		/*	alert('aaaa'+Edad); */
				$.post('../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php', { Edad: Edad, acc :'Combo_Examenes' },
				function(data){
				$("#Examenes").html(data);
			  });     
 			
			}
			
		
			$("#btn_Agregar_Medico").click(function(){
				
							var Apellido_Paterno=$("#Apellido_Paterno").val(); 
							var Apellido_Materno=$("#Apellido_Materno").val(); 
							var Nombre=$("#Nombre").val(); 
							var DNI=$("#DNI").val(); 
							var Colegiatura=$("#Colegiatura").val();
							var IdMedico=$("#IdMedico").val();
														
							if ($("#nro_documento").val() == "") {  
								$("#nro_documento").focus().after('<span class="error">Ingrese DNI</span>');  
								return false;  
							}
								

							
							var parametros =
							{
							"IdMedico" : IdMedico,
							"acc" : 'Agregar_Medico'
							};
			
							$.ajax({
											data:  parametros,
											url:  '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
											type:  'post',
											success:  function (response) {
													$("#resultado").html(response);
											
											}
							
							
							}); 
			});
			
			
			$("#btn_limpiar").click(function(){
				$('#nro_documento').val("");
				$('#Apellido_Paterno').val("");
				$('#Apellido_Materno').val("");
				$('#Nombre').val("");
				$('#DNI').val("");
				$('#Colegiatura').val("");
	
			});
	
		});
		</script>
		
		
		
		<script type="text/javascript" charset="utf-8">
		  $(document).ready(function() {
		   $("#IdServicio").change(function () {
			  $("#IdServicio option:selected").each(function () {
				IdServicio=$(this).val();
				$.post("../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php", { IdServicio: IdServicio  , acc :'Combo_Cama' }, function(data){
				$("#Servicios").html(data);
			  });     
			 });
		   });    
		});
		</script>

		<!-- Script de Hora  --->
		<script type="text/javascript">
		$(document).ready( function(){
			$('#my_time').mTimePicker().mTimePicker( 'setTime', '12:00' );

			$('button#set').on('click', function(event) { $('#my_time').mTimePicker( 'setTime', '15:40' ); });
			$('button#get').on('click', function(event) { alert( 'On timer is ' + $('#my_time').mTimePicker('getTime') ); });
		});
		</script>
		<!-- Fin de Script de Hora  --->


		</head>
		

		
		
		
		<body>
		<div style="margin:15px !important">
		<!--------Aqui apareceran los Alert de Jquery------------>
		<div id="resultado">
		</div>
		<!-------------------->
		<h2>Atencion de Paciente <span style="color:red"> (Cuenta: <?php echo  $IdCuentaAtencion;?>) </span></h2>	
			<hr/> 

			<fieldset>
            <legend>Datos del Paciente</legend>			
			<table width="50%" height="141">		
			<tr>
				<td width="12%" height="38">Apellido Paterno</td>
				<td colspan="2">
				<input type='text' name='Apellido_Paterno' id='Apellido_Paterno'   readonly='readonly'    required>
				</td>
				<td width="9%">Apellido Materno</td>
				<td colspan="2">
				<input type='text' name='Apellido_Materno' id='Apellido_Materno'   readonly='readonly'    required>
				</td>
                <td width="16%" height="21">Nombres</td>
				<td width="23%" colspan="2">
				<input type='text' name='Nombres' id='Nombres'   readonly='readonly'    required>
				</td>
			</tr>
            <tr style="display:none">
				<td height="26">Nro de Cuenta</td>
				<td colspan="2"><input type='text' name='nro_cuenta' id='nro_cuenta' value="<?php echo  $IdCuentaAtencion;?>"></td>
			</tr>
			<tr>
				<td>Sexo</td>
				<td colspan="2">
				<input type='text' name='Sexo' id='Sexo'   readonly='readonly'    required>
				</td>
				<td height="21">Edad</td>
				<td colspan="2">
				<input type='text' name='Edad' id='Edad'   readonly='readonly'    required>
				</td>
				<td>Tiempo Aproximado de Duracion</td>
				<td colspan="2">
					<div class="section">
					<input id="my_time" type="text" readonly>
					</div>
				</td>
			</tr>
			<tr>
				<td height="21">Servicios de Cama</td>
				<td colspan="2">
						<input type='text' name='Servicio' id='Servicio'   readonly='readonly'    required>
				</td>
				<td>Cama</td>
				<td colspan="2">
                        <input type='text' name='Cama' id='Cama'   readonly='readonly'    required>
				</td>  
			</tr>
			</table>
            <table width="544">
			<tr>
				<td width="96">Operacion</td>
				<td width="436" colspan="4">
					<div class="input_container">
					<input type="text" id="Operacion" onKeyUp="Autocompletar_Operacion()" size="40">
                    <ul id="Operacion_list_id"></ul>
					</div>
				</td>
			</tr>
			<tr>
				<td>Diagnostico</td>
				<td colspan="4">
					<div class="input_container">
					<input type="text" id="Diagnostico" onKeyUp="Autocompletar_Diagnostico()" size="40">
                    <ul id="Diagnostico_list_id"></ul>
					</div>
				</td>
			</tr>
		    </table>
			<br>
			<table width="400">
			<tr>
			  <td colspan="4">
              
             <b>Examenes Solicitados</b>
              <tr>
			  <td colspan="4">
 				<!--Inicio de Contenedor  ---->

								<div id="scroll_examenes">							
									<div id="lineaResultado">
									</div>
								</div>									
              </td>
              </tr>
			</table>
			<table>
			<tr>
				<td colspan="2">
				<input type="button" class="buttonS" value="Guardar"   name="btn_cargar_datos" id="btn_Agregar_Medico" />			
				</td>
			</tr>
		</table>
		</fieldset>
		</div>
    </body>
</html>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	