<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	 <!-- Script de Validacion  --->
	 <link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
     <script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	 <!-- Fin Script de Validacion  --->
	<link rel="stylesheet" href="../../MVC_Complemento/css/Autocompletado.css" type="text/css" />
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<!--------  Jquery Alert Dialog------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
	<!------------------------------>
	<!-------- Css Alert Dialog------------>
	<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
	<!------------------------------>
			<script type="text/javascript">
			/* Funcion para Autocompletar */
			function Autocompletar_Operacion() {
				var min_length = 0; 
				var Nombre = $('#Examen').val();
				if (Nombre.length >= min_length) {
					$.ajax({
						url: '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						type: 'POST',
						data: {Nombre:Nombre,acc:'Seleccionar_Producto'},
						success:function(data){
							$('#Operacion_list_id').show();
							$('#Operacion_list_id').html(data);
						}
					});
				} else {
					$('#Operacion_list_id').hide();
				}
			}

			function set_item_operacion(Nombre,IdProducto) {
				$('#Examen').val(Nombre);
				$('#Id_Examen').val(IdProducto);
				$('#Operacion_list_id').hide();
			}


			$(function(){
				/* Funcion que se ejecutara al Load  */
				$(document).ready(function() {
					CargarListaExamenes(); 
				});
				
				/* Funcion para Listar todos los Examenes */
				function CargarListaExamenes()
				{
					var gridder = $('#lineaResultado');
					var UrlToPass = {
							acc : 'Listar_Sala_Examenes'
					};
					$.ajax({
						url : '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						type : 'POST',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});
				
				}
				
				/* Funcion en el momento del Focus */
				$("#Examen").focus(function(){
					document.getElementById("Examen").value="";
				});
								
				
				/* Funcion para Agregar un Nuevo Examen */
				$("#btn_Agregar_Examen").click(function(){
							
					var Id_Examen=$("#Id_Examen").val(); 
					var Tipo_Edad=$('select[name=Tipo_Edad]').val();
					if ($('select[name=Tipo_Edad]').val() == "0") 
					{  
					$.alertx('Informacion Sala de Operacion','Seleccione Tipo de Sexo');
					return false;  
					}
					if ($("#Examen").val() == "") 
					{  
					$.alertx('Informacion Sala de Operacion','Ingrese un Examen');
					return false;  
					}
					var parametros =
					{
					"Tipo_Edad" : Tipo_Edad,
					"Id_Examen" : Id_Examen,
					"acc" : 'Agregar_Examen'
					};
					$.ajax
						({
						data:  parametros,
						url:  '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						type:  'post',
						success:  function (response) {
						$("#resultado").html(response);
						$.alertx('Informacion Sala de Operacion','Examen Ingresado Satisfactoriamente');
						CargarListaExamenes(); 
						}				
						}); 
				});
						
			});
			</script>			
			<script type="text/javascript">
			function Limpiar(){

						document.getElementById("ApellidoPaterno").value="";
						document.getElementById("ApellidoMaterno").value="";
						document.getElementById("Nombres").value="";
						document.getElementById("NroDocumento").value="";
						document.getElementById("NroDocumento").focus();
				}	
			</script>
 </head>
 <body>
  <!-- Lista de Examenes  ---->
  <div style="margin:15px !important">
  <br>
  <h1>Lista de Examenes</h1>
  <div  id="lista_archiveros">
  <table width="1000" border="0">
    <tr>
      <td width="994"><table width="100%">
        <tr  style="padding:15px !important">
          <td width="66%"><fieldset>
            <legend>Buscar</legend>
            <table width="100%">
                <tr>
                <td>Examen para Solicitar</td>
                <td style="display:none"><input name="Id_Examen" type="text" id="Id_Examen" size="10" ></td>
				<td>
					<div class="input_container">
					<input type="text" id="Examen" onkeyup="Autocompletar_Operacion()" size="50">
                    <ul id="Operacion_list_id"></ul>
					</div>
				</td>
				<td>Tipo de Edad</td>
				 <td>
				 	<select name="Tipo_Edad" id="Tipo_Edad" style="margin:10px 10px -9px 0px;">
                          <option value="0" >Seleccionar Edad</option>
                          <?php 
                          if($Tipos_Edad != NULL)  { 
                          foreach($Tipos_Edad as $item2){?>
                              <option value="<?php echo $item2["IdSalaEdad"]?>"<?php if($item2["IdSalaEdad"]==$IdSalaEdad){?>selected<?php }?>><?php echo $item2["Descripcion"]?></option>
                          <?php }}?>
					</select>
				 </td>
                <td>&nbsp;</td>
				<td >
				<input type="button" value="Agregar Examen" id="btn_Agregar_Examen">
				</td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
          </fieldset></td>
          </tr>
    </table>
	</tr>
  </table>
	<div style="margin:15px !important">
	<table width="970">
	<tr>
	<td width="975"><fieldset>
	<legend>Lista de Examenes</legend>
	<div id="scroll">
		<div id="lineaResultado">
		</div>
	</div>
	</fieldset></td>
	</tr>
	</table>
	</div>
  </div>
  </div>
  <!--Fin Lista de Examenes  ----> 

  
  <!--Inicio de Contenedor  ---->  
  <div style="margin:15px !important">
  <div  id="contenedor">
  </div>
  </div>
  <!--Fin de Contenedor  ----> 
  
  
  <p>&nbsp;</p>
  <p>&nbsp;</p>
 </body>
 