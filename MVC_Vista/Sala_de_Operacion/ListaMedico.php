<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>

<!--------  Jquery Alert Dialog------------>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
<!------------------------------>
<!-------- Css Alert Dialog------------>
<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
<!------------------------------>
	<script type="text/javascript">
	$(function(){
		
		$(".Eliminar_Medico").click(function()
		{
			var id = $(this).attr("id");
			var IdEmpleado = id;
			var Accion = 'Eliminar_Archivero';
			var parent = $(this).parent("td").parent("tr");		
			$.confirmx('Confirmacion','Esta Seguro que eliminara Registro?',function(dialogx)
			{
			console.log('Confirm');
				$.post('../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php', {'IdEmpleado':IdEmpleado,'acc':Accion}, function(data)
				{
					parent.fadeOut('slow');
				});	
			return true;
			},function(dialogx)
			{ console.log('Cancelar'); return true; });
			
			return false;		
		});
	});
	</script>
<body>
   <table width="940" border="1" cellpadding="0" cellspacing="0" class="data">
     <tr>
       <th width="10" align="center">N&deg; Id</th>
       <th width="140" align="center">Nº DNI</th>
       <th width="220" align="center">Profesional Medico</th>
       <th width="220" align="center">Colegiatura</th>         
       <th width="50" align="center">Opcion</th>
     </tr>   
    <?php 
	  if($Listar_Total_Medicos != NULL){ 
	  foreach($Listar_Total_Medicos as $item){
      
    ?>
     <tr> 
       <td><a><?php echo  $item["Correlativo"];?></a></td>
	   <td align="center"><?php echo  $item["DNI"];?></td>
	   <td align="center"><?php echo  $item["Medico"];?></td>
       <td align="center"><?php echo  $item["Colegiatura"];?></td>
	   <td align="center"> <a href="#"  id="<?php echo $item['IdSalaProfesional']; ?>" class="Eliminar_Archivero"><img src="../../MVC_Complemento/img/close.png" width="16" height="16" /></a></td>	   
	   </tr>
	   <?php } }else{ ?>
	   <tr>
	   <td colspan="7" align="center" bgcolor="#FFFFFF" class="alert_error">NO SE ENCONTRÓ NINGÚN REGISTRO </td>
	   </tr>
  <?php 
	}  
  ?>       
   </table>
</body>
</html>