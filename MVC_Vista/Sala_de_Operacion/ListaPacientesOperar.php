<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
<!--------  Jquery Alert Dialog------------>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
<!------------------------------>
<!-------- Css Alert Dialog------------>
<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
<!------------------------------>
	<script type="text/javascript">
	
	
	
	function popupCenter(url, title, w, h) {
	  var left = (screen.width/2)-(w/2);
	  var top = (screen.height/2)-(h/2);
	  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=YES, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
	}

	$(function(){
		
		$(".Eliminar_Archivero").click(function()
		{
			var id = $(this).attr("id");
			var IdEmpleado = id;
			var Accion = 'Eliminar_Archivero';
			var parent = $(this).parent("td").parent("tr");		
			$.confirmx('Confirmacion','Esta Seguro que eliminara Registro?',function(dialogx)
			{
			console.log('Confirm');
				$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php', {'IdEmpleado':IdEmpleado,'acc':Accion}, function(data)
				{
					parent.fadeOut('slow');
				});	
			return true;
			},function(dialogx)
			{ console.log('Cancelar'); return true; });
			
			return false;		
		});
	});
	</script>
<body>
   <table width="940" border="1" cellpadding="0" cellspacing="0" class="data">
     <tr>
       <th width="50" align="center">N&deg; Id</th>
	   <th width="150" align="center">Nombres</th>
	   <th width="200" align="center">Apellido Paterno</th>
	   <th width="200" align="center">Apellido Materno</th>
	   <th width="100" align="center">Historia Clinica</th>
       <th width="100" align="center">DNI</th>
       <th width="100" align="center">N&deg; Cuenta</th>
       <th width="10" align="center">Edad</th>
	   <th width="100" align="center">Fecha de Ingreso</th>
       <th width="420" align="center">Servicio</th>     
     </tr>   
    <?php 
	  if($Lista_Pacientes_Sala_Operacion != NULL){ 
	  foreach($Lista_Pacientes_Sala_Operacion as $item){
    ?>
     <tr> 
	   <td><a  onclick="popupCenter('../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php?acc=Mostrar_Datos_Atencion_Paciente&IdCuentaAtencion=<?php echo  $item["IdCuentaAtencion"];?>','Imprimir Boleta',800,700)"   ><?php echo  $item["Correlativo"];?></a></td> 
	   <td align="center"><?php echo  $item["PrimerNombre"];?></td>
	   <td align="center"><?php echo  $item["ApellidoPaterno"];?></td>
	   <td align="center"><?php echo  $item["ApellidoMaterno"];?></td>
       <td align="center"><a  onclick="popupCenter('../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php?acc=Mostrar_Datos_Atencion_Paciente&IdCuentaAtencion=<?php echo  $item["IdCuentaAtencion"];?>','Imprimir Boleta',800,700)"   ><?php echo  $item["NroHistoriaClinica"];?></a></td>
	   <td align="center"><?php echo  $item["NroDocumento"];?></td>
       <td align="center"><?php echo  $item["IdCuentaAtencion"];?></td>
	   <td align="center"><?php echo  $item["Edad"];?></td>
	   <td align="center"><?php echo  $item["FechaIngreso"];?></td>
	   <td align="center"><?php echo  $item["Servicio"];?></td>
	   <td align="center"> <a href="#"  id="<?php echo $item['IdAtencion']; ?>" class="Eliminar_Archivero"><img src="../../MVC_Complemento/img/close.png" width="16" height="16" /></a></td>	   
	   </tr>
	   <?php } }else{ ?>
	   <tr>
	   <td colspan="7" align="center" bgcolor="#FFFFFF" class="alert_error">NO SE ENCONTRÓ NINGÚN REGISTRO </td>
	   </tr>
  <?php 
	}  
  ?>       
   </table>
</body>
</html>