<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>

<!--------  Jquery Alert Dialog------------>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
<!------------------------------>
<!-------- Css Alert Dialog------------>
<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
<!------------------------------>

	<script type="text/javascript">
	$(function(){
		<!--   Funcion para eliminar un archivero de la Lista  -->
		$(".Eliminar_Archivero").click(function()
		{
			var id = $(this).attr("id");
			var IdEmpleado = id;
			var Accion = 'Eliminar_Archivero';
			var parent = $(this).parent("td").parent("tr");		
			$.confirmx('Confirmacion','Esta Seguro que eliminara Registro?',function(dialogx)
			{
			console.log('Confirm');
				$.post('../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php', {'IdEmpleado':IdEmpleado,'acc':Accion}, function(data)
				{
					parent.fadeOut('slow');
				});	
			return true;
			},function(dialogx)
			{ console.log('Cancelar'); return true; });
			
			return false;		
		});	
	});
	</script>
<body>
   <table width="940" border="1" cellpadding="0" cellspacing="0" class="data">
     <tr>
       <th width="30" align="center">N&deg; Id</th>
       <th width="220" align="center">Codigo</th>
       <th width="220" align="center">Descripcion</th>     
       <th width="50" align="center">Hora Inicial</th>
       <th width="50" align="center">Hora Final</th>       
       <th width="50" align="center">Opcion</th>
     </tr>   
    <?php 
	  if($ListarTurnos != NULL){ 
	  foreach($ListarTurnos as $item){
      
    ?>
     <tr> 
       <td><a><?php echo  $item["Correlativo"];?></a></td>
	   <td align="center"><?php echo  $item["Codigo"];?></td>
	   <td align="center"><?php echo  $item["Descripcion"];?></td>
       <td align="center"><?php echo  $item["HoraInicio"];?></td>
       <td align="center"><?php echo  $item["HoraFin"];?></td>
	   <td align="center"> <a href="#"  id="<?php echo $item['IdTurno']; ?>" class="Eliminar_Turno"><img src="../../MVC_Complemento/img/close.png" width="16" height="16" /></a></td>	   
	   </tr>
	   <?php } }else{ ?>
	   <tr>
	   <td colspan="7" align="center" bgcolor="#FFFFFF" class="alert_error">NO SE ENCONTRÓ NINGÚN REGISTRO </td>
	   </tr>
  <?php 
	}  
  ?>       
   </table>
</body>
</html>