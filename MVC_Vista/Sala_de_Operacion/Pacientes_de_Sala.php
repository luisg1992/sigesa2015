<?php 
 //session_start();
error_reporting(E_ALL^E_NOTICE);

?> 
	<!DOCTYPE html>
	<html lang="en">
	<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen_archivo.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	
	<!--------  Jquery  Popup_query     ------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/Popup_query.js"></script>
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/Popup_query.css">
	<!--------  Fin Jquery  Popup_query ------------>
	
	<!-------- Script de Validacion      ------>
	<script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
	<!-------- Fin Script de Validacion  ------>
	
	
	<!-------- Script  Jquery Alert Dialog        ------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
	<!-------- Fin  Script  Jquery Alert Dialog   ------------>
	
	
	
	<script type="text/javascript">
		$(function(){

				$("#Buscar_Pacientes").click(function Cargar(){
					Cargar_Lista_Pacientes(); 
				});
				

				/* Funcion que se ejecutara al Load  */
				$(document).ready(function() {
					Cargar_Lista_Pacientes(); 
				});
				
				function Cargar_Lista_Pacientes() {

					var gridder = $('#lineaResultado');
					var UrlToPass = {
							Fecha_Inicio: $('#Fecha_Inicio').val(),
							Fecha_Final: $('#Fecha_Final').val(),
							Nro_Cuenta: $('#Nro_Cuenta').val(),
							Nro_Documento: $('#Nro_Documento').val(),
							Historia_Clinica: $('#Historia_Clinica').val(),
							acc : 'Listar_Pacientes_por_Operar'
					};

					$.ajax({
						url : '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						type : 'POST',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});		
				}
		});
	</script>
	
	</head>	
	<body>
    <br>
    <H1>Lista de Pacientes por Operar</H1>
	<div style="margin:15px !important">

	
	  <table width="1040" border="0">
		<tr>
		  <td width="994">
		    <table width="100%">
			<tr>
			  <td width="66%"><fieldset>
				<legend>Reporte de HC</legend>
				<table width="950" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td colspan="8">
					<table width="100%">
					  <tr>
					    <td width="1%">&nbsp;</td>
						<td width="6%">Cuenta</td>
						<td width="6%"><input  type="text"  name="Nro_Cuenta"  id="Nro_Cuenta"   size="10"></td>
						<td width="6%">DNI</td>
						<td width="6%"><input  type="text"  name="Nro_Documento"  id="Nro_Documento"   size="10"></td>
						<td width="6%">Historia Clinica</td>
						<td width="6%"><input  type="text"  name="Historia_Clinica"  id="Historia_Clinica"   size="10"></td>
						<td width="6%">Fecha Inicio</td>
						<td width="20%"> 
						<input  type="text"  name="Fecha_Inicio"  id="Fecha_Inicio" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="10"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Inicio",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador1"     // el id del botón que lanzará el calendario 
											}); 
										</script>	
						</td>
						<td width="8%">Fecha Final</td>
						<td width="19.5%">
						<input  type="text"  name="Fecha_Final"  id="Fecha_Final" value="<?php echo vfecha(substr($FechaServidor,0,10))?>"   size="10"  autocomplete=OFF  />
									  <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Inicio"  />
									  
									    <script type="text/javascript"> 
											   Calendar.setup({ 
												inputField     :    "Fecha_Final",     // id del campo de texto 
												 ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
												 button     :    "lanzador2"     // el id del botón que lanzará el calendario 
											}); 
										</script>				
						</td>
						<td> 
						<input type="button"  id="Buscar_Pacientes" value="Buscar">
						</td>
						</tr>
					</table>
					</td>
					 <!-- Aqui estan las funciones propias de jcal para poder Cargar las Fechas de Rango-->
					  <script type="text/javascript">
					  RANGE_CAL_1 = new Calendar({
							  inputField: "Fecha_Inicio",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaInicio_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
							  RANGE_CAL_2 = new Calendar({
							  inputField: "Fecha_Final",
							  dateFormat: "%d/%m/%Y",
							  trigger: "FechaFinal_Boton",
							  bottomBar: false,
							  onSelect: function() {
									  var date = Calendar.intToDate(this.selection.get());
									  LEFT_CAL.args.min = date;
									  LEFT_CAL.redraw();
									  this.hide();
							  }
					  });
					</script>
				  </tr>
				  <tr>
				  <td>&nbsp;</td>
				  </tr>
				</table>
				</td>
				</tr>
			</table>

						<div style="margin:15px !important; clear:both;">
						<table width="970">
						<tr>
						  <td width="975">
						  <fieldset>
							<legend>Lista de Pacientes</legend>
							<!--
							<table width="950" border="1" cellpadding="0" cellspacing="0" class="data">
							     <tr>
								   <th width="50" align="center">N&deg; Id</th>
								   <th width="150" align="center">Nombres</th>
								   <th width="200" align="center">Apellido Paterno</th>
								   <th width="200" align="center">Apellido Materno</th>
								   <th width="100" align="center">Historia Clinica</th>
								   <th width="100" align="center">DNI</th>
								   <th width="100" align="center">N&deg; Cuenta</th>
								   <th width="10" align="center">Edad</th>
								   <th width="100" align="center">Fecha de Ingreso</th>
								   <th width="420" align="center">Servicio</th>     
								 </tr>
							</table>
							-->
							<!--Inicio de Contenedor  ----> 
							<div id="scroll">								
								<div id="lineaResultado">
								</div>
							</div>
							<!--Fin de Contenedor  ----> 
						  </fieldset>
						  </td>
						</tr>
					    </table>
					    </div>
	    </table>
	</div>
	</body>
	</html>

