<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	 <!-- Script de Validacion  --->
	 <link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
     <script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	 <!-- Fin Script de Validacion  --->
	 
     <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	 <script type="text/javascript">
	$(function(){
		
			$("#btn_Buscar").click(function Cargar(){
				var gridder = $('#lineaResultado');
				var UrlToPass = {
						Nombres: $('#Nombres').val(),
						acc : 'ListarServicioSala'
				};

				$.ajax({
					url : '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
					type : 'POST',
					data : UrlToPass,
					success: function(responseText) {
						gridder.html(responseText);
					}
				});

			});	
			

			$("#btn_Agregar").click(function Cargar(){
				var IdServicio=$('select[name=ServicioEmergencia]').val(); 
				var parametros =
				{
				"IdServicio" : IdServicio,
				"acc" : 'Agregar_Servicio_Sala'
				};
			
				$.ajax({
				data:  parametros,
				url : '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
				type:  'post',
				success:  function (response) 
					{
					$("#resultado").html(response);	
					$.alertx('Informacion Sala de Operacion','Servicio Ingresado Satisfactoriamente');
					CargarListaServicios(); 					
					}			
				}); 				
				
			});


			/* Funcion que se ejecutara al Load  */
			$(document).ready(function() {
					CargarListaServicios(); 
			});
				
				/* Funcion para Listar todos los Examenes */
			function CargarListaServicios()
			{
					var gridder = $('#lineaResultado');
					var UrlToPass = {
							acc : 'Listar_Total_Sala_Servicios'
					};
					$.ajax({
						url : '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
						type : 'POST',
						data : UrlToPass,
						success: function(responseText) {
							gridder.html(responseText);
						}
					});
				
			}			
	});
			
	</script>
	
 </head>
 <body>
  <!-- Lista de Servicios de Sala ---->
  <div style="margin:15px !important">
  <br>
  <h1>Lista - Servicios de Sala</h1>
  <div  id="lista_archiveros">
  <table width="960" border="0">
    <tr>
      <td width="850">
	  <table width="100%">
        <tr  style="padding:15px !important">
          <td width="50%"><fieldset>
            <legend>Agregar Servicio</legend>
            <table width="100%">
              <tr>
				<td>Servicio de Emergencia:</td>
			    <td>
					  	<select name="ServicioEmergencia" id="ServicioEmergencia" style="margin:0px 10px -5px 0px;">
                          <option value="0" >Todos los T&oacute;picos</option>
                          <?php 
                          if($Lista_Servicios_Emergencia != NULL)  { 
                          foreach($Lista_Servicios_Emergencia as $item2){?>
                              <option value="<?php echo $item2["IdServicio"]?>"<?php if($item2["IdServicio"]==$IdTipoServicio){?>selected<?php }?>><?php echo $item2["Servicio"]?></option>
                          <?php }}?>
						</select>
				</td>
				<td align="center" valign="top"><input type="button" class="button" value="Agregar"   name="btn_Agregar" id="btn_Agregar"></td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
          </fieldset></td>
		  
		    <td width="50%"><fieldset>
            <legend>Buscar</legend>
            <table width="100%">
                <tr>
				<td>Nombre de Servicio :</td>
                <td><input name="Nombres" type="text" id="Nombres" size="30" onkeyup="if(validateEnter(event) == true) { alert(this.value); }" ></td>
                <td align="center" valign="top"><input type="button" class="button" value="Buscar"   name="btn_Buscar" id="btn_Buscar" /></td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
          </fieldset></td>
          </tr>
		  </td> 
    </table>
	</tr>
  </table>
	<div style="margin:15px !important">
	<table width="970">
	<tr>
	<td width="975"><fieldset>
	<legend>Lista de Servicios</legend>
	
	<div id="scroll">
		<div id="lineaResultado">
		</div>
	</div>
	</fieldset></td>
	</tr>
	</table>
	</div>
  </div>
  </div>
  <!--Fin Lista de Archiveros  ---->  
  
  <!--Inicio de Contenedor  ---->  
  <div style="margin:15px !important">
  <div  id="contenedor">
  </div>
  </div>
  <!--Fin de Contenedor  ----> 
  
  
  
  <p>&nbsp;</p>
  <p>&nbsp;</p>
 </body>
 