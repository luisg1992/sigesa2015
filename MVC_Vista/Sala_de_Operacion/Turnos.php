<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 

	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	<!-- Script de Validacion  --->
	<link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
    <script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	<!-- Fin Script de Validacion  --->

	<!---Script de Jquery Alert Dialog------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
	<!--- Fin Script Jquery Alert Dialog  --------->
	 
     <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	 <script type="text/javascript">
	$(function(){
		$("#ListarTurnos").click(function Cargar(){
		
			var gridder = $('#lineaResultado');
			var UrlToPass = {
					Descripcion: $('#Descripcion').val(),
					acc : 'ListarTurnos'
			};

			$.ajax({
				url : '../../MVC_Controlador/Sala_de_Operacion/Sala_de_OperacionC.php',
				type : 'POST',
				data : UrlToPass,
				success: function(responseText) {
					gridder.html(responseText);
				}
			});


			
			
		});
		
		$("#Agregar_Turnos").click(function Agregar_Turnos()
		{
			 $("#lista_archiveros").hide();
			 $("#contenedor").load('../../MVC_Vista/Sala_de_Operacion/Agregar_Turnos.php');
		});
		

		
		// Show the text box on click
		$('body').delegate('.editable', 'click', function(){
			var ThisElement = $(this);
			ThisElement.find('span').hide();
			ThisElement.find('.digito_inicial').show().focus();
		});
		
		// Pass and save the textbox values on blur function
		$('body').delegate('.digito_inicial', 'blur', function(){		
			var ThisElement = $(this);
			ThisElement.hide();
			ThisElement.prev('span').show().html($(this).val()).prop('title', $(this).val());
			var UrlToPass = 'acc=Modificar_Digito_Inicial&Digito_Inicial='+ThisElement.val()+'&IdEmpleado='+ThisElement.prop('name');	
			$.ajax({
				url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
				type : 'POST',
				data : UrlToPass
			});
		});


	
		// Show the text box on click
		$('body').delegate('.editable', 'click', function(){
			var ThisElement = $(this);
			ThisElement.find('span').hide();
			ThisElement.find('.digito_final').show().focus();
		});
		
		// Pass and save the textbox values on blur function
		$('body').delegate('.digito_final', 'blur', function(){		
			var ThisElement = $(this);
			ThisElement.hide();
			ThisElement.prev('span').show().html($(this).val()).prop('title', $(this).val());
			if ($(this).val() <= 99  && $(this).val() >=0) 
			{			
				var UrlToPass = 'acc=Modificar_Digito_Final&Digito_Final='+ThisElement.val()+'&IdEmpleado='+ThisElement.prop('name');	
				$.ajax({
					url : '../../MVC_Controlador/Archivo_Clinico/Archivo_ClinicoC.php',
					type : 'POST',
					data : UrlToPass
				});			
			}
			if(isNaN($(this).val()) || $(this).val() > 99)
			{
				$.alertx('Informacion Archivo Clinico','Digito Invalido o Fuera de Rango');
			}
			
		});
	
	});
	</script>
			
	<script type="text/javascript">
	function Limpiar(){

				document.getElementById("ApellidoPaterno").value="";
				document.getElementById("ApellidoMaterno").value="";
				document.getElementById("Nombres").value="";
				document.getElementById("NroDocumento").value="";
				document.getElementById("NroDocumento").focus();
		}	
	</script>


 </head>
 <body>
  <ul class="pro15 nover">
  <li><a href="#"    id="Agregar_Turnos" class="nover" ><em class="nuevo nover"></em><b>Agregar</b></a></li>
  </ul> 
  <!-- Lista de Archiveros  ---->
  <div style="margin:15px !important">
  <br>
  <h1>Lista de Turnos</h1>
  <div  id="lista_archiveros">
  <table width="500" border="0">
    <tr>
      <td width="500"><table width="100%">
        <tr  style="padding:15px !important">
          <td width="66%"><fieldset>
            <legend>Buscar</legend>
            <table width="100%">
                <tr>
				<td>&nbsp;</td>
                <td>Descripcion</td>
                <td><input name="Descripcion" type="text" id="Descripcion" size="30" ></td>
				<td>&nbsp;</td>
			    <td width="12%" align="center"><img src="../../MVC_Complemento/img/botonbuscar.jpg" width="69" height="22" id="ListarTurnos"></td>              
                <td align="center" valign="top"><img src="../../MVC_Complemento/img/botonlimpiar.jpg" alt="" width="69" height="22" onClick="Limpiar();"></td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
          </fieldset></td>
          </tr>
    </table>
	</tr>
  </table>
	<div style="margin:15px !important">
	<table width="970">
	<tr>
	<td width="975"><fieldset>
	<legend>Lista de Turnos</legend>
	
	<div id="scroll">
		<div id="lineaResultado">
		</div>
	</div>
	</fieldset></td>
	</tr>
	</table>
	</div>
  </div>
  </div>
  <!--Fin Lista de Archiveros  ---->  
  
  <!--Inicio de Contenedor  ---->  
  <div style="margin:15px !important">
  <div  id="contenedor">
  </div>
  </div>
  <!--Fin de Contenedor  ----> 
  
  <p>&nbsp;</p>
  <p>&nbsp;</p>
 </body>
 