<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 

	<meta charset="utf-8">
	<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
	<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
	<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
	<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
    <link rel="stylesheet" href="../../MVC_Complemento/css/formulario.css" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/estilo_archivo_clinico.css">
    <script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
	<!-- Script de Validacion  --->
	<link rel="stylesheet" href="../../MVC_Complemento/css/validacion.css" type="text/css" />
    <script type="text/javascript" src="../../MVC_Complemento/js/validacion.js"></script>
	<!-- Fin Script de Validacion  --->

	<!---Script de Jquery Alert Dialog------------>
	<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
	<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
	<!--- Fin Script Jquery Alert Dialog  --------->
	 
     <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
	 <script type="text/javascript">
	$(function(){
		$("#ListarEmpleados").click(function Cargar(){
			CargarListaEmpleados(); 
		});
		
		$("#Agregar_Empleado").click(function Agregar_Archivero()
		{
			 $("#Lista_de_Empleados").hide();
			 $("#contenedor").load('../../MVC_Vista/Seguridad/Agregar_Empleado.php');
		});
		
		$("#Pendientes_Empleado").click(function ()
		{
			 $("#Lista_de_Empleados").hide();
			 $("#contenedor").load('../../MVC_Vista/Seguridad/Pendientes_Empleado.php');
		});
		

		/* Funcion que se ejecutara al Load  */
		$(document).ready(function() {
			CargarListaEmpleados(); 
		});
				
		/* Funcion para Listar todos los Empleados */
		function CargarListaEmpleados()
		{
			var gridder = $('#lineaResultado');
			var UrlToPass = {
					ApellidoPaterno: $('#ApellidoPaterno').val(),
					ApellidoMaterno: $('#ApellidoMaterno').val(),
					Nombres: $('#Nombres').val(),
					NroDocumento: $('#NroDocumento').val(),
					acc : 'ListarEmpleados'
			};

			$.ajax({
				url : '../../MVC_Controlador/Seguridad/SeguridadC.php',
				type : 'POST',
				data : UrlToPass,
				success: function(responseText) {
					gridder.html(responseText);
				}
			});	
		}		
		
	});
	</script>
			
	<script type="text/javascript">
	function Limpiar(){

				document.getElementById("ApellidoPaterno").value="";
				document.getElementById("ApellidoMaterno").value="";
				document.getElementById("Nombres").value="";
				document.getElementById("NroDocumento").value="";
				document.getElementById("NroDocumento").focus();
		}	
	</script>


 </head>
 <body>
  <ul class="pro15 nover">
  <li><a href="#"    id="Agregar_Empleado" class="nover" ><em class="nuevo nover"></em><b>Agregar</b></a></li>
  </ul> 
  <!-- Lista de Archiveros  ---->
  <div style="margin:15px !important">
  <br>
  <div  id="Lista_de_Empleados">
  <h1>Lista de Empleados</h1>
  <table width="1000" border="0">
    <tr>
      <td width="994"><table width="100%">
        <tr  style="padding:15px !important">
          <td width="66%"><fieldset>
            <legend>Buscar</legend>
            <table width="100%">
              <tr>
                <th width="9%"><strong>Nº D.N.I.</strong></th>
                <th width="18%"><strong>Apellido Paterno</strong></th>
                <th width="18%"><strong>Apellido Materno</strong></th>
				<th width="9%"><strong>Nombres</strong></th>
                <th width="34%">
                <input type="hidden" name="IdPuntoCarga" id="IdPuntoCarga" value="1">
                <input type="hidden" name="IdEmpleado" id="IdEmpleado" value="<?php echo $_REQUEST['IdEmpleado'];?>"></th>
                <th width="12%" align="center"><img src="../../MVC_Complemento/img/botonbuscar.jpg" width="69" height="22" id="ListarEmpleados" onMouseOver="style.cursor=cursor"></th>
                </tr>
                <tr>
                <td><input name="NroDocumento" type="text" id="NroDocumento" onkeyup="if(validateEnter(event) == true) { alert(this.value); }"  size="15"></td>
                <td><input name="ApellidoPaterno" type="text" id="ApellidoPaterno" size="30" onkeyup="if(validateEnter(event) == true) { alert(this.value); }" ></td>
                <td><input name="ApellidoMaterno" type="text" id="ApellidoMaterno" size="30" onkeyup="if(validateEnter(event) == true) { alert(this.value); }" ></td>
				 <td><input name="Nombres" type="text" id="Nombres" onChange="validarclientexHC();" onkeyup="if(validateEnter(event) == true) { alert(this.value); }"  size="15" ></td>
                <td>&nbsp;</td>
                <td align="center" valign="top"><img src="../../MVC_Complemento/img/botonlimpiar.jpg" alt="" width="69" height="22" onClick="Limpiar();"></td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;</td>
                </tr>
              </table>
          </fieldset></td>
          </tr>
    </table>
	</tr>
  </table>
	<div style="margin:15px !important">
	<table width="970">
	<tr>
	<td width="975"><fieldset>
	<legend>Lista de Empleados</legend>
	
	<div id="scroll">
		<div id="lineaResultado">
		</div>
	</div>
	</fieldset></td>
	</tr>
	</table>
	</div>
  </div>
  </div>
  <!--Fin Lista de Archiveros  ---->  
  
  <!--Inicio de Contenedor  ---->  
  <div style="margin:15px !important">
  <div  id="contenedor">
  </div>
  </div>
  <!--Fin de Contenedor  ----> 
  
  <p>&nbsp;</p>
  <p>&nbsp;</p>
 </body>
 