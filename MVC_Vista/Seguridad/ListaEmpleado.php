<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.11.1.min.js"></script>

<!--------  Jquery Alert Dialog------------>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery.dialog.js"></script>
<!------------------------------>
<!-------- Css Alert Dialog------------>
<link rel="stylesheet" href="../../MVC_Complemento/css/jquery.dialog.css" type="text/css" /> 
<!------------------------------>

	<script type="text/javascript">
	$(function(){
		<!--   Funcion para eliminar un Empleado de la Lista  -->
		$(".Eliminar_Empleado").click(function()
		{
			var id = $(this).attr("id");
			var IdEmpleado = id;
			var Accion = 'Eliminar_Empleado';
			var parent = $(this).parent("td").parent("tr");		
			$.confirmx('Confirmacion','Esta Seguro que eliminara Registro?',function(dialogx)
			{
			console.log('Confirm');
				$.post('../../MVC_Controlador/Seguridad/SeguridadC.php', {'IdEmpleado':IdEmpleado,'acc':Accion}, function(data)
				{
					parent.fadeOut('slow');
				});	
			return true;
			},function(dialogx)
			{ console.log('Cancelar'); return true; });
			
			return false;		
		});
		
		<!--   Funcion para Resetear un Empleado de la Lista  -->
		$(".Resetear_Empleado").click(function()
		{
			var id = $(this).attr("id");
			var IdEmpleado = id;
			var Accion = 'Resetear_Empleado';
			var parent = $(this).parent("td").parent("tr");		
			$.confirmx('Confirmacion','Esta Seguro que Reseteara Contraseña de Usuario?',function(dialogx)
			{
			console.log('Confirm');
				$.post('../../MVC_Controlador/Seguridad/SeguridadC.php', {'IdEmpleado':IdEmpleado,'acc':Accion}, function(data)
				{
					parent.css( "background-color:black" );
				});	
			return true;
			},function(dialogx)
			{ console.log('Cancelar'); return true; });
			
			return false;		
		});


			/* Get Edit ID  */
		$(".Modificar_Empleado").click(function()
		{
			var IdEmpleado = $(this).attr("id");
			$("#Lista_de_Empleados").hide();
			var gridder = $('#contenedor');
			var UrlToPass = {
					IdEmpleado:IdEmpleado,
					acc : 'EditarEmpleado'
			};

			$.ajax({
				url : '../../MVC_Controlador/Seguridad/SeguridadC.php',
				type : 'GET',
				data : UrlToPass,
				success: function(responseText) {
					gridder.html(responseText);
				}
			});
			
			
		});		
	});
	</script>
<body>
   <table width="940" border="1" cellpadding="0" cellspacing="0" class="data">
     <tr>
       <th width="10" align="center">N&deg; Id</th>
       <th width="140" align="center">Nº DNI</th>
       <th width="220" align="center">Apellido Paterno</th>
       <th width="220" align="center">Apellido Materno</th>
       <th width="220" align="center">Nombres</th>   
       <th width="220" align="center">Usuario</th> 	   
       <th width="50" align="center">Editar</th>
	   <th width="50" align="center">Resetear</th>
	   <th width="50" align="center">Eliminar</th>
     </tr>   
    <?php 
	  if($ListarEmpleados != NULL){ 
	  foreach($ListarEmpleados as $item){
      
    ?>
     <tr> 
       <td><a><?php echo  $item["Correlativo"];?></a></td>
	   <td align="left"><?php echo  strtoupper($item["DNI"]);?></td>
	   <td align="left"><?php echo  strtoupper($item["ApellidoPaterno"]);?></td>
       <td align="left"><?php echo  strtoupper($item["ApellidoMaterno"]);?></td>
       <td align="left"><?php echo  strtoupper($item["Nombres"]);?></td>
	   <td align="center" style="color:red !important; font-weight:bold"><?php echo  strtoupper($item["Usuario"]);?></td>
	   <td align="center"><a href="#"  id="<?php echo $item['IdEmpleado']; ?>" class="Modificar_Empleado"><img src="../../MVC_Complemento/img/modiciar.jpg" width="16" height="16" /></a></td>
	   <td align="center"><a href="#"  id="<?php echo $item['IdEmpleado']; ?>" class="Resetear_Empleado"><img src="../../MVC_Complemento/img/Refresh.png" width="16" height="16" /></a></td>	
	   <td align="center"><a href="#"  id="<?php echo $item['IdUsuario']; ?>" class="Eliminar_Empleado"><img src="../../MVC_Complemento/img/close.png" width="16" height="16" /></a></td>	   
	   </tr>
	   <?php } }else{ ?>
	   <tr>
	   <td colspan="7" align="center" bgcolor="#FFFFFF" class="alert_error">NO SE ENCONTRÓ NINGÚN REGISTRO </td>
	   </tr>
  <?php 
	}  
  ?>       
   </table>
</body>
</html>