<head>
<meta charset="utf-8">
<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/demo.css">
 
 
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/menu_opciones.css">
<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/js/jsalert/jquery.alerts.css"/>    
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/funciones2.js"></script>
 
 
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery_enter.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src="../../MVC_Complemento/js/jquery.autocomplete.js"></script> 
<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
 <script type="text/javascript" src="../../MVC_Complemento/js/jsalert/jquery.alerts.js"></script>
 <link rel="stylesheet" href="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.css" type="text/css" />
<script type="text/javascript" src="../../MVC_Complemento/js/js_VentanaEmergente/dhtmlwindow.js"></script>
 
 
<style type="text/css">
<!--
#tblSample td, th { padding: 0.2em; }
.classy0 { background-color: #234567; color: #89abcd; }
.classy1 { background-color: #89abcd; color: #234567; }

#ocultarTexto {
  display: none;
}




 dt,  table, tbody, tfoot, thead, tr, th, td {
  margin:0;
  padding:0;
  border:0;
  font-weight:inherit;
  font-style:inherit;
   
  font-family:inherit;
  vertical-align:baseline;
}


 
.cuerpo1 {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
color: #000;
 
}

form
{width: auto; background-color: #ffffff; }

textarea.Formulario {
padding: 5px;
border: 1px solid #D4D4D4;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
width:100%;
}

input.Formulario {
border: 1px solid #D4D4D4;
padding: 5px;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
}

.Combos {
     font: small-caption cursive ; 

     background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    width: 150px;
    /*float:left;*/
  
text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;

}

 
.texto {
    font: small-caption; 
    background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;
  
    /*float:left;*/

}
.textodesable {
  font: small-caption; 
    background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#ECECEC;
  
    /*float:left;*/

}




 /*-----*/
 fieldset { border:1px solid green }

.legend {
  border: 1px solid #BDD7FF;
width: 95%;
background: #F7F7F7;
padding: 3px;

/*  text-align:right;*/
  }

.CSSTableGenerator {
  margin:0px;padding:0px;
  width:100%;
  border:1px solid #ffffff;
  
  -moz-border-radius-bottomleft:0px;
  -webkit-border-bottom-left-radius:0px;
  border-bottom-left-radius:0px;
  
  -moz-border-radius-bottomright:0px;
  -webkit-border-bottom-right-radius:0px;
  border-bottom-right-radius:0px;
  
  -moz-border-radius-topright:0px;
  -webkit-border-top-right-radius:0px;
  border-top-right-radius:0px;
  
  -moz-border-radius-topleft:0px;
  -webkit-border-top-left-radius:0px;
  border-top-left-radius:0px;
}.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
  width:100%;
  height:100%;
  margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
  -moz-border-radius-bottomright:0px;
  -webkit-border-bottom-right-radius:0px;
  border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
  -moz-border-radius-topleft:0px;
  -webkit-border-top-left-radius:0px;
  border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
  -moz-border-radius-topright:0px;
  -webkit-border-top-right-radius:0px;
  border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
  -moz-border-radius-bottomleft:0px;
  -webkit-border-bottom-left-radius:0px;
  border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
  
}
.CSSTableGenerator tr:nth-child(odd){ background-color:#aad4ff; }
.CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }.CSSTableGenerator td{
  vertical-align:middle;
  
  
  border:1px solid #ffffff;
  border-width:0px 1px 1px 0px;
  text-align:left;
  padding:4px;
  font-size:10px;
  font-family:Helvetica;
  font-weight:normal;
  color:#000000;
}.CSSTableGenerator tr:last-child td{
  border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
  border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
  border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
    background:-o-linear-gradient(bottom, #005fbf 5%, #005fbf 100%);  background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #005fbf) );
  background:-moz-linear-gradient( center top, #005fbf 5%, #005fbf 100% );
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#005fbf");  background: -o-linear-gradient(top,#005fbf,005fbf);

  background-color:#005fbf;
  border:0px solid #ffffff;
  text-align:center;
  border-width:0px 0px 1px 1px;
  font-size:10px;
  font-family:Helvetica;
  font-weight:bold;
  color:#ffffff;
}
.CSSTableGenerator tr:first-child:hover td{
  background:-o-linear-gradient(bottom, #005fbf 5%, #005fbf 100%);  background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #005fbf) );
  background:-moz-linear-gradient( center top, #005fbf 5%, #005fbf 100% );
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#005fbf");  background: -o-linear-gradient(top,#005fbf,005fbf);

  background-color:#005fbf;
}
.CSSTableGenerator tr:first-child td:first-child{
  border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
  border-width:0px 0px 1px 1px;
}
   
-->
</style> 


<script type="text/javascript">
   
   $(document).ready(function() {
      
      //autocompletado estado civil
      $("#NombreEstadoCivil").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_EstadoCivil&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#NombreEstadoCivil").result(function(event, data, formatted) {
        $("#NombreEstadoCivil").val(data[2]);
        $("#CodigoEstadoCivil").val(data[1]);      
     });

   //autocompletado de grado de instruccion 
    $("#GradoInstruccion").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Grado_Instruccion_C&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#GradoInstruccion").result(function(event, data, formatted) {
        $("#GradoInstruccion").val(data[2]);
        $("#IdGradoInst").val(data[1]);      
     });     

     //autocompletado condicion de trabajo
     $("#CondicionTrabajo").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Condicion_trabajo_C&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#CondicionTrabajo").result(function(event, data, formatted) {
        $("#CondicionTrabajo").val(data[2]);
        $("#IdCondicionTrabajo").val(data[1]);      
     });

     //autocompletado de cargos
     $("#cargo_trabajador").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Tipos_Ocupacion_C&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#cargo_trabajador").result(function(event, data, formatted) {
        $("#cargo_trabajador").val(data[2]);
        $("#codigo_cargo_trb").val(data[1]);      
     });
          


     //departamentos del Peru
     $.ajax({
       url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Departamentos_C',
       type: 'POST',
       dataType: 'json',
       data: {param1: ' '},
       success: function(data){
          $.each(data, function (index, value) {
                    $("#combo_departamentos").append('<option value="' + value.id + '">' + value.departamentos + '</option>')
          });
       }
     });

     //provincias estandar del Peru
     $.ajax({
       url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Provincias_Estandar_C',
       type: 'POST',
       dataType: 'json',
       data: {param1: ' '},
       success: function(data){
          $.each(data, function (index, value) {
                    $("#combo_provincias").append('<option value="' + value.id + '">' + value.provincias + '</option>')
          });
       }
     });

     //distritos estandar del Peru
     $.ajax({
       url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Distritos_Estandar_C',
       type: 'POST',
       dataType: 'json',
       data: {param1: ' '},
       success: function(data){
          $.each(data, function (index, value) {
                    $("#combo_distritos").append('<option value="' + value.id + '">' + value.distritos + '</option>')
          });
       }
     });
     
     //provincias del peru segun el departamento escogido
     $("#combo_departamentos").change(function(event) {
       var id_departamentos = $("#combo_departamentos").val();
       $.ajax({
         url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Provincias_C',
         type: 'POST',
         dataType: 'json',
         data: {id_departamentos: id_departamentos},
         success: function(data){
          $("#combo_provincias").html('');
                 $("#combo_provincias").append('<option value=0> </option>');
          $.each(data, function (index, value) {
                    $("#combo_provincias").append('<option value="' + value.id + '">' + value.provincias + '</option>')
          });
       }
       });       
     });

     //distritos del peru segun el departamento y provincia escogida
     $("#combo_provincias").change(function(event) {
      var id_departamentos = $("#combo_departamentos").val();
      var id_provincias = $("#combo_provincias").val();

      $.ajax({
        url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Distrito_C',
        type: 'POST',
        dataType: 'json',
        data: { id_departamentos:id_departamentos,
                id_provincias:id_provincias},
        success: function(data){
          $("#combo_distritos").html('');
                 $("#combo_distritos").append('<option value=0> </option>');
          $.each(data, function (index, value) {
                    $("#combo_distritos").append('<option value="' + value.id + '">' + value.distritos + '</option>')
          });
        }
      });
      
     });

     //departamentos/oficinas/servicios del hospital
     $.ajax({
        url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Dpto_Hos_C',
        type: 'POST',
        dataType: 'json',
        data: {param1: ' '},
        success: function(data){
          $.each(data, function (index, value) {
                    $("#IdDptoHos").append('<option value="' + value.id + '">' + value.departamentos + '</option>')
          });
       }
     });

     //unidades de hospital
     //
     $("#IdDptoHos").change(function(event) {
       var iddpto = $("#IdDptoHos").val();
       $.ajax({
         url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Unidad_Hos_C',
         type: 'POST',
         dataType: 'json',
         data: {iddpto: iddpto},
         success: function(data){
          $("#combo_unidad_hospital").html('');
                 $("#combo_unidad_hospital").append('<option value=0> </option>');
          $.each(data, function (index, value) {
                    $("#combo_unidad_hospital").append('<option value="' + value.id + '">' + value.unidades + '</option>')
          });
       }
       });       
     });
     
     //unidades estandar
     $.ajax({
         url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Unidades_estandar',
         type: 'POST',
        dataType: 'json',
        data: {param1: ' '},
        success: function(data){
          $.each(data, function (index, value) {
                    $("#combo_unidad_hospital").append('<option value="' + value.id + '">' + value.unidades + '</option>')
          });
       }
     });
     

     //areas de hospital - 
      $("#combo_unidad_hospital").change(function(event) {
       var idunidad = $("#combo_unidad_hospital").val();
       $.ajax({
         url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_area_Hos_C',
         type: 'POST',
         dataType: 'json',
         data: {idunidad: idunidad},
         success: function(data){
          $("#combo_areas_hospital").html('');
                 $("#combo_areas_hospital").append('<option value=0> </option>');
          $.each(data, function (index, value) {
                    $("#combo_areas_hospital").append('<option value="' + value.id + '">' + value.areass + '</option>')
          });
       }
       });       
     });

    // areas estandar
    $.ajax({
        url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Areas_estandar',
         type: 'POST',
        dataType: 'json',
        data: {param1: ' '},
        success: function(data){
          $.each(data, function (index, value) {
                    $("#combo_areas_hospital").append('<option value="' + value.id + '">' + value.areass + '</option>')
          });
       }
    });
    

   });

</script>

<script type="text/javascript">
function RefreshHijos(){
  var dni_buscar = $("#dni_trabajador").val();
  $.ajax({
          url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Familiares_C',
          type: 'POST',
          dataType: 'json',
          data: {
            dni_buscar: dni_buscar
          },

          success: function(data) {

             
            if (data.dato == 'NM') {
              $("#tabla_familiares").html(" ");
              $("#tabla_familiares").append('sin familiares');
            } else {
              $("#tabla_familiares").html('<tr><td>NOMBRES</td><td>A.PATERNO</td><td>A.MATERNO</td><td>DNI</td><td>PARENTESCO</td><td>ESTADOCIVIL</td><td>GRADO DE INSTRUCCION</td><td>OCUPACION</td></tr>');
            $.each(data, function(index, val) {
               $("#tabla_familiares").append('<tr><td width="260">'+val.NOMBRES+'</td>'+
                                            '<td width="260">'+val.PATERNO+'</td>'+
                                            '<td width="260">'+val.MATERNO+'</td>'+
                                            '<td width="260">'+val.DNI+'</td>'+
                                            '<td width="260">'+val.PARENTESCO+'</td>'+
                                            '<td width="260">'+val.ESTADOCIVIL+'</td>'+
                                            '<td width="260">'+val.GRADO+'</td>'+
                                            '<td width="260">'+val.OCUPACION+'</td>'+
                                            //'<td width="260">'+val.FECHA+'</td>'+
                                            //'<td width="260">'+val.OBSERVACION+'</td>'+
                                            '</tr>')
            });
            }
          }
        });

}

  //busqueda de datos
  $(document).ready(function() {

    // funcion para limpiar cajas de texto
    var LimpiarCajasTexto = function() {
        $(":text").each(function(){ 
            $($(this)).val('');
        });
        $('input:radio[name=sexo]').attr('checked', false);
        $('input:radio[name=afiliacion]').attr('checked', false);
        $('input:radio[name=conyuge_trabaja_trabajador]').attr('checked', false);
        $('input:radio[name=otro_trabajo_trabajador]').attr('checked', false);
        $('input:radio[name=condicion_trabajo_trabajador]').attr('checked', false);
        $('input:radio[name=deudas_trabajador]').attr('checked', false);
        $('input:radio[name=sit_salud_trabajador]').attr('checked', false);
        $('input:radio[name=practica_deporte_condicional]').attr('checked', false);
        $("#observaciones_trabajador").val("  ");
        $("#diagnostico_social_trabajador").val("  ");

    }

// funcion para limpiar los select
    var LimpiarSelect = function() {
      $('select option:first-child').attr("selected", "selected");
    }
    


    //busqueda por dni
    $("#buscar_trabajador_x_dni").click(function(event) {
      
      if ($("#dni_buscar").val().length < 1) {
        alert('Ingrese un numero de DNI');
        return 0;
      }

      else {
        var dni_buscar = $("#dni_buscar").val();
        $.ajax({
          url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Busqueda_fs_dni',
          type: 'POST',
          dataType: 'json',
          data: {
            dni_buscar: dni_buscar
          }, 
          success: function(data){
            LimpiarCajasTexto();
            LimpiarSelect();
            //datos generales
            $("#dni_buscar").val(data.DNI);
            $("#dni_trabajador").val(data.DNI);
            $("#FechaNacimiento").val(data.FECHANACIMIENTO);
            $("#his_trabajador").val(data.HISTORIA);
            $("#edad_trabajador").val(data.EDAD);
            $("#apellidopat_trabajador").val(data.APELLIDOPAT);
            $("#apellidomat_trabajador").val(data.APELLIDOMAT);
            $("#nombres_trabajador").val(data.NOMBRES);
            $("#NombreEstadoCivil").val(data.ESTADOCIVIL);
            $("#CodigoEstadoCivil").val(data.IDESTCI);
            $("#GradoInstruccion").val(data.GRADOINSTRUCCION);
            $("#IdGradoInst").val(data.CODGRAD);
            $("#telefono_trabajador").val(data.TELEFONO);
            $("#celular_trabajador").val(data.CELULAR);
            $("#email_trabajador").val(data.EMAIL);
            $("#direccion_trabajador").val(data.DIRECCION);

            if (data.SEXO == "MASCULINO") {
              $("#masculino").attr('checked', 'checked');
            }
            if (data.SEXO == "FEMENINO") {
              $("#femenino").attr('checked', 'checked');
            }

            $("#combo_departamentos > option[value='"+data.DEPARTAMENTO+"']").attr('selected', 'selected');
            $("#combo_provincias > option[value='"+data.PROVINCIA+"']").attr('selected', 'selected');
            $("#combo_distritos > option[value='"+data.DISTRITO+"']").attr('selected', 'selected');

            //datos laborales
            $("#IdDptoHos").val(data.IDDPTO_OFICINA);
            $("#combo_unidad_hospital").val(data.IDUNIDAD_OFICINA);
            $("#combo_areas_hospital").val(data.IDAREA_OFICINA);
            
            $("#funcion_des_trabajador").val(data.FUNCIONDESEM);
            $("#pertenece_trabajador > option[value='"+data.PERTENECE+"']").attr('selected', 'selected');
            $("#pension_trabajador > option[value='"+data.PENSIONTIPO+"']").attr('selected', 'selected');
           
            $("#FechaIngreso").val(data.FECHAINGRESO);
            if(data.ESSALUD == "SI") {
              $("#afil_essalud_si").attr('checked', 'checked');
            }
            if(data.ESSALUD == "NO") {
              $("#afil_essalud_no").attr('checked', 'checked');
            }
            
            $("#CondicionTrabajo").val(data.CONDICION);
            $("#IdCondicionTrabajo").val(data.IDCONDINCION);
            $("#cargo_trabajador").val(data.CARGO);
            $("#codigo_cargo_trb").val(data.IDCARGO);

            //situacion de economica
            if(data.CONVIVIENTTRABAJA == "EVENTUAL") {
              $("#condicion_trabajo_eventual").attr('checked', 'checked');
            }

            if(data.CONDICICONVIVI == "SI") {
              $("#conyuge_trabaja_si").attr('checked', 'checked');
            }

            if(data.DUEDAS == "SI") {
              $("#deudas_trabaja_si").attr('checked', 'checked');
            }

            if(data.OTROTRA == "SI") {
              $("#otro_trabajo_si").attr('checked', 'checked');
            }

            if(data.CONVIVIENTTRABAJA == "ESTABLE") {
              $("#condicion_trabajo_estable").attr('checked', 'checked');
            }
            if(data.CONDICICONVIVI == "NO") {
              $("#conyuge_trabaja_no").attr('checked', 'checked');
            }
            if(data.DUEDAS == "NO") {
              $("#deudas_trabaja_no").attr('checked', 'checked');
            }
            if(data.OTROTRA == "NO") {
              $("#otro_trabajo_no").attr('checked', 'checked');
            }   
            
            //SITUACION VIVIENDA
            $('#tipo_vivienda_trabajador option:eq('+data.TIPOVV+')').attr('selected', 'selected');
            $('#material_construccion_trabajador option:eq('+data.MATERIAL+')').attr('selected', 'selected');
            $("#cant_miembros_trabajador").val(data.NUMMIENBROSH);
            $("#cant_habi_trabajador").val(data.NUMHABITAH);

            //situacion de salud
            if (data.PROBLEMASSALUD == "SI") {
              $("#sit_salud_trabajador_si").attr('checked', 'checked');
            }
            if(data.PROBLEMASSALUD == "NO") {
               $("#sit_salud_trabajador_no").attr('checked', 'checked');
            }
            $("#problema_salud_trab_des").val(data.DESCRPSSALUD);

            //actividades recreativas  
            if (data.CONDICIONALDEPORTE == "SI") {
              $("#practica_deporte_condicional_si").attr('checked', 'checked');
            }
            if (data.CONDICIONALDEPORTE == "NO") {
              $("#practica_deporte_condicional_no").attr('checked', 'checked');
            }           
            
            $("#cual_deporte_trabajador").val(data.CUALDEPORTE);
            $("#actividades_recre_traba").val(data.ACTIVIDADESDEPOR);

            //observaciones y diagnostico social
            $("#observaciones_trabajador").val(data.OBSERVACION);
            $("#diagnostico_social_trabajador").val(data.DISGNOSTSOCIAL);



            return 0;
          }
        });

        $.ajax({
          url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Familiares_C',
          type: 'POST',
          dataType: 'json',
          data: {
            dni_buscar: dni_buscar
          },

          success: function(data) {
            
            if (data.dato == 'NM') {
              $("#tabla_familiares").html("  ");
              $("#tabla_familiares").append('sin familiares');
            } else {
              $("#tabla_familiares").html("  ");
              $("#tabla_familiares").html('<tr><td>NOMBRES</td><td>A.PATERNO</td><td>A.MATERNO</td><td>DNI</td><td>PARENTESCO</td><td>ESTADOCIVIL</td><td>GRADO DE INSTRUCCION</td><td>OCUPACION</td></tr>');
            $.each(data, function(index, val) {
               $("#tabla_familiares").append('<tr><td width="260">'+val.NOMBRES+'</td>'+
                                            '<td width="260">'+val.PATERNO+'</td>'+
                                            '<td width="260">'+val.MATERNO+'</td>'+
                                            '<td width="260">'+val.DNI+'</td>'+
                                            '<td width="260">'+val.PARENTESCO+'</td>'+
                                            '<td width="260">'+val.ESTADOCIVIL+'</td>'+
                                            '<td width="260">'+val.GRADO+'</td>'+
                                            '<td width="260">'+val.OCUPACION+'</td>'+
                                            //'<td width="260">'+val.FECHA+'</td>'+
                                            //'<td width="260">'+val.OBSERVACION+'</td>'+
                                            '</tr>')
            });
            }
          }
        });
        

      }

    });
    
    $("#dni_buscar").keyup(function(e){
      if(e.keyCode == 13)
        { 
          if ($("#dni_buscar").val().length < 1) {
        alert('Ingrese un numero de DNI');
        return 0;
      }

      else {
        var dni_buscar = $("#dni_buscar").val();
        $.ajax({
          url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Busqueda_fs_dni',
          type: 'POST',
          dataType: 'json',
          data: {
            dni_buscar: dni_buscar
          }, 
          success: function(data){
            LimpiarCajasTexto();
            LimpiarSelect();
            //datos generales
            $("#dni_buscar").val(data.DNI);
            $("#dni_trabajador").val(data.DNI);
            $("#FechaNacimiento").val(data.FECHANACIMIENTO);
            $("#his_trabajador").val(data.HISTORIA);
            $("#edad_trabajador").val(data.EDAD);
            $("#apellidopat_trabajador").val(data.APELLIDOPAT);
            $("#apellidomat_trabajador").val(data.APELLIDOMAT);
            $("#nombres_trabajador").val(data.NOMBRES);
            $("#NombreEstadoCivil").val(data.ESTADOCIVIL);
            $("#CodigoEstadoCivil").val(data.IDESTCI);
            $("#GradoInstruccion").val(data.GRADOINSTRUCCION);
            $("#IdGradoInst").val(data.CODGRAD);
            $("#telefono_trabajador").val(data.TELEFONO);
            $("#celular_trabajador").val(data.CELULAR);
            $("#email_trabajador").val(data.EMAIL);
            $("#direccion_trabajador").val(data.DIRECCION);

            if (data.SEXO == "MASCULINO") {
              $("#masculino").attr('checked', 'checked');
            }
            if (data.SEXO == "FEMENINO") {
              $("#femenino").attr('checked', 'checked');
            }

            $("#combo_departamentos > option[value='"+data.DEPARTAMENTO+"']").attr('selected', 'selected');
            $("#combo_provincias > option[value='"+data.PROVINCIA+"']").attr('selected', 'selected');
            $("#combo_distritos > option[value='"+data.DISTRITO+"']").attr('selected', 'selected');

            //datos laborales
            $("#IdDptoHos").val(data.IDDPTO_OFICINA);
            $("#combo_unidad_hospital").val(data.IDUNIDAD_OFICINA);
            $("#combo_areas_hospital").val(data.IDAREA_OFICINA);
            
            $("#funcion_des_trabajador").val(data.FUNCIONDESEM);
            $("#pertenece_trabajador > option[value='"+data.PERTENECE+"']").attr('selected', 'selected');
            $("#pension_trabajador > option[value='"+data.PENSIONTIPO+"']").attr('selected', 'selected');
           
            $("#FechaIngreso").val(data.FECHAINGRESO);
            if(data.ESSALUD == "SI") {
              $("#afil_essalud_si").attr('checked', 'checked');
            }
            if(data.ESSALUD == "NO") {
              $("#afil_essalud_no").attr('checked', 'checked');
            }
            
            $("#CondicionTrabajo").val(data.CONDICION);
            $("#IdCondicionTrabajo").val(data.IDCONDINCION);
            $("#cargo_trabajador").val(data.CARGO);
            $("#codigo_cargo_trb").val(data.IDCARGO);

            //situacion de economica
            if(data.CONVIVIENTTRABAJA == "EVENTUAL") {
              $("#condicion_trabajo_eventual").attr('checked', 'checked');
            }

            if(data.CONDICICONVIVI == "SI") {
              $("#conyuge_trabaja_si").attr('checked', 'checked');
            }

            if(data.DUEDAS == "SI") {
              $("#deudas_trabaja_si").attr('checked', 'checked');
            }

            if(data.OTROTRA == "SI") {
              $("#otro_trabajo_si").attr('checked', 'checked');
            }

            if(data.CONVIVIENTTRABAJA == "ESTABLE") {
              $("#condicion_trabajo_estable").attr('checked', 'checked');
            }
            if(data.CONDICICONVIVI == "NO") {
              $("#conyuge_trabaja_no").attr('checked', 'checked');
            }
            if(data.DUEDAS == "NO") {
              $("#deudas_trabaja_no").attr('checked', 'checked');
            }
            if(data.OTROTRA == "NO") {
              $("#otro_trabajo_no").attr('checked', 'checked');
            }   
            
            //SITUACION VIVIENDA
            $('#tipo_vivienda_trabajador option:eq('+data.TIPOVV+')').attr('selected', 'selected');
            $('#material_construccion_trabajador option:eq('+data.MATERIAL+')').attr('selected', 'selected');
            $("#cant_miembros_trabajador").val(data.NUMMIENBROSH);
            $("#cant_habi_trabajador").val(data.NUMHABITAH);

            //situacion de salud
            if (data.PROBLEMASSALUD == "SI") {
              $("#sit_salud_trabajador_si").attr('checked', 'checked');
            }
            if(data.PROBLEMASSALUD == "NO") {
               $("#sit_salud_trabajador_no").attr('checked', 'checked');
            }
            $("#problema_salud_trab_des").val(data.DESCRPSSALUD);

            //actividades recreativas  
            if (data.CONDICIONALDEPORTE == "SI") {
              $("#practica_deporte_condicional_si").attr('checked', 'checked');
            }
            if (data.CONDICIONALDEPORTE == "NO") {
              $("#practica_deporte_condicional_no").attr('checked', 'checked');
            }           
            
            $("#cual_deporte_trabajador").val(data.CUALDEPORTE);
            $("#actividades_recre_traba").val(data.ACTIVIDADESDEPOR);

            //observaciones y diagnostico social
            $("#observaciones_trabajador").val(data.OBSERVACION);
            $("#diagnostico_social_trabajador").val(data.DISGNOSTSOCIAL);



            return 0;
          }
        });

        $.ajax({
          url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Familiares_C',
          type: 'POST',
          dataType: 'json',
          data: {
            dni_buscar: dni_buscar
          },

          success: function(data) {
            
            if (data.dato == 'NM') {
              $("#tabla_familiares").html("  ");
              $("#tabla_familiares").append('sin familiares');
            } else {
              $("#tabla_familiares").html("  ");
              $("#tabla_familiares").html('<tr><td>NOMBRES</td><td>A.PATERNO</td><td>A.MATERNO</td><td>DNI</td><td>PARENTESCO</td><td>ESTADOCIVIL</td><td>GRADO DE INSTRUCCION</td><td>OCUPACION</td></tr>');
            $.each(data, function(index, val) {
               $("#tabla_familiares").append('<tr><td width="260">'+val.NOMBRES+'</td>'+
                                            '<td width="260">'+val.PATERNO+'</td>'+
                                            '<td width="260">'+val.MATERNO+'</td>'+
                                            '<td width="260">'+val.DNI+'</td>'+
                                            '<td width="260">'+val.PARENTESCO+'</td>'+
                                            '<td width="260">'+val.ESTADOCIVIL+'</td>'+
                                            '<td width="260">'+val.GRADO+'</td>'+
                                            '<td width="260">'+val.OCUPACION+'</td>'+
                                            //'<td width="260">'+val.FECHA+'</td>'+
                                            //'<td width="260">'+val.OBSERVACION+'</td>'+
                                            '</tr>')
            });
            }
          }
        });
        

      }

        }
    });
  
    $("#Agregar_Parientes").click(function(event) {
      var dni_buscar = $("#dni_trabajador").val();
      var googlewin=dhtmlwindow.open("googlebox", "iframe", "../../MVC_Vista/Servicio_Social/MascaraAgregarPacientes.php?dni_titular="+dni_buscar,"Reportes Emergencia ", "width=990px,height=690px,resize=0,scrolling=1,center=1", "recal")
      //Esta funcion se ejecuta para poder cerrar popup 
     // FocusOnInput();
      });

    $("#Grabar_Nuevo_Registro").click(function(event) {
    
    if ($("#dni_buscar").val().length > 1) {
      alert("Si esta haciendo una busqueda, solo puede actualizar no registrar");
      return 0;
    }

    //validacion de datos
    //validando apellidos paternos
    if ($("#apellidopat_trabajador").val().length < 1) {
      alert("Ingrese un Apellido Paterno");
      return 0;
    }

    //validando apellidos materno
    if ($("#apellidomat_trabajador").val().length < 1) {
      alert("Ingrese un Apellido Materno");
      return 0;
    }

    //validando nombres
    if ($("#nombres_trabajador").val().length < 1) {
      alert("Ingrese un Nombre");
      return 0;
    }

    //validando fecha de nacimiento
    if ($("#FechaNacimiento").val().length < 1) {
      alert("Ingrese una Fecha de Nacimiento");
      return 0;
    }

    //validando numero de historia clinica
    if ($("#his_trabajador").val().length < 1) {
      alert("Ingrese un numero de historia clinica");
      return 0;
    }

    //validando edad del trabajador
    //if ($("#edad_trabajador").val().length < 1) {
    //  alert("Ingrese una edad");
    //  return 0;
    //}

    //validando sexo
    if(!$("input[name=sexo]:checked").val()) {
      alert('Seleccione un tipo sexo');
      return 0;
    }

    //validando estado civil
    if ($("#CodigoEstadoCivil").val().length < 1) {
      alert("Seleccion un estado civil");
      return 0;
    }

    //validando grado de instruccion
    if ($("#IdGradoInst").val().length < 1) {
      alert("Seleccion un grado de instruccion");
      return 0;
    }

    //validando departamentos
    if ($("#combo_departamentos").val().length < 1) {
      alert("Seleccion un departameto");
      return 0;
    }

    //validando provincias
    if ($("#combo_provincias").val().length < 1) {
      alert("Seleccion una provincia");
      return 0;
    }

    //validando distritos
    if ($("#combo_distritos").val().length < 1) {
      alert("Seleccion un distrito");
      return 0;
    }

    //validando direccion
    if ($("#direccion_trabajador").val().length < 1) {
      alert("Ingrese una direccion domiciliaria");
      return 0;
    }

    //validadno telefono
    //if ($("#telefono_trabajador").val().length < 1) {
    //  alert("Ingrese un numero de telefono");
    //  return 0;
    //}

    //validando numero de celular
    //if ($("#celular_trabajador").val().length < 1) {
    //  alert("Ingrese un numero de celular");
    //  return 0;
    //}

    //validando un numero de dni
    if ($("#dni_trabajador").val().length < 1) {
      alert("Ingrese un numero de dni");
      return 0;
    }

    //validando departamento de hospital
    if ($("#IdDptoHos").val().length < 1) {
      alert("Seleccione un Departamento/Servicio/Oficina");
      return 0;
    }

    

    //validando funcion del trabajador
    if ($("#funcion_des_trabajador").val().length < 1) {
      alert("Ingrese una funcion para el trabajador");
      return 0;
    }

    //validadno condicion del trabajador
    if ($("#IdCondicionTrabajo").val().length < 1) {
      alert("Ingrese una condicion de trabajo");
      return 0;
    }

    //validando pertenece trabajador
    if ($("#pertenece_trabajador").val().length < 1) {
      alert("Seleccione una pertenencia");
      return 0;
    }    

    //validando un tipo de pension
    if ($("#pension_trabajador").val().length < 1) {
      alert("Seleccione un tipo de pension");
      return 0;
    }

    //validar cargo de trabajador
    if ($("#cargo_trabajador").val().length < 1) {
      alert("Ingrese un cargo para el trabajador");
      return 0;
    }    

    //validar fecha de ingreso 
    if ($("#FechaIngreso").val().length < 1) {
      alert("Ingrese un fecha de ingreso");
      return 0;
    } 

    //validar afiliacion a ESSALUD
    if(!$("input[name=afiliacion]:checked").val()) {
      alert('Seleccione si esta o no afiliado a ESSALUD');
      return 0;
    }

    //validacion de conyuge
    if(!$("input[name=conyuge_trabaja_trabajador]:checked").val()) {
      alert('Seleccione si el conyuge trabaja o no');
      return 0;
    }

    //validando otro trabajo
    if(!$("input[name=otro_trabajo_trabajador]:checked").val()) {
      alert('Seleccione si el trabajador tiene otro trabajo');
      return 0;
    }

    //validando la condicion de trabajo del conyugue
    //if(!$("input[name=condicion_trabajo_trabajador]:checked").val()) {
    //  alert('Seleccione la condicion de trabajo del conyuge');
    //  return 0;
    //}

    //validando si existes deudas
    if(!$("input[name=deudas_trabajador]:checked").val()) {
      alert('Seleccione si existe algun tipo de deudas');
      return 0;
    }

    //validando el tipo de vivienda
    if ($("#tipo_vivienda_trabajador").val().length < 1) {
      alert("Seleccion un tipo de vivienda");
      return 0;
    } 

    //validando cantidad de miembros del hogar
    if ($("#cant_miembros_trabajador").val().length < 1) {
      alert("Ingrese la cantidad de miembros del hogar");
      return 0;
    } 

    //validando el material de construccion
    if ($("#material_construccion_trabajador").val().length < 1) {
      alert("Seleccione el tipo de material de construccion");
      return 0;
    }

    //validando cantidad de habitaciones
    if ($("#cant_habi_trabajador").val().length < 1) {
      alert("Ingrese la cantidad de habitaciones");
      return 0;
    }

    //validando si tienes problemas de salud
    if(!$("input[name=sit_salud_trabajador]:checked").val()) {
      alert('Seleccione si se tiene problemas de salud');
      return 0;
    }

    //validando practica del deporte
    if(!$("input[name=practica_deporte_condicional]:checked").val()) {
      alert('Seleccione si se practica algun deporte');
      return 0;
    }

    //datos generales
    var apellidopat_trabajador = $("#apellidopat_trabajador").val();
    var apellidomat_trabajador = $("#apellidomat_trabajador").val();
    var nombres_trabajador = $("#nombres_trabajador").val();
    var FechaNacimiento = $("#FechaNacimiento").val();
    var his_trabajador = $("#his_trabajador").val();
    var edad_trabajador = $("#edad_trabajador").val();
    var sexo = $("input[name='sexo']:checked").val();
    var CodigoEstadoCivil = $("#CodigoEstadoCivil").val();
    var IdGradoInst = $("#IdGradoInst").val();
    var combo_departamentos = $("#combo_departamentos").val();
    var combo_provincias = $("#combo_provincias").val();
    var combo_distritos = $("#combo_distritos").val();
    var direccion_trabajador =$("#direccion_trabajador").val();
    var telefono_trabajador = $("#telefono_trabajador").val();
    var celular_trabajador = $("#celular_trabajador").val();
    var email_trabajador = $("#email_trabajador").val();
    var dni_trabajador = $("#dni_trabajador").val();

    //datos laborales
    var IdDptoHos = $("#IdDptoHos").val();
    var combo_unidad_hospital = $("#combo_unidad_hospital").val();
    var combo_areas_hospital = $("#combo_areas_hospital").val();
    var funcion_des_trabajador = $("#funcion_des_trabajador").val();
    var IdCondicionTrabajo = $("#IdCondicionTrabajo").val();
    var pertenece_trabajador = $("#pertenece_trabajador").val();
    var pension_trabajador = $("#pension_trabajador").val();
    var cargo_trabajador = $("#codigo_cargo_trb").val();
    var FechaIngreso = $("#FechaIngreso").val();
    var afiliacion_cond = $("input[name='afiliacion']:checked").val(); 


    // situacion economica
    var conyuge_trabaja_trabajador = $("input[name='conyuge_trabaja_trabajador']:checked").val();
    var otro_trabajo_trabajador = $("input[name='otro_trabajo_trabajador']:checked").val();
    var condicion_trabajo_trabajador = $("input[name='condicion_trabajo_trabajador']:checked").val();
    var deudas_trabajador = $("input[name='deudas_trabajador']:checked").val();

    //situacion vivienda
    var tipo_vivienda_trabajador = $("#tipo_vivienda_trabajador").val();
    var cant_miembros_trabajador = $("#cant_miembros_trabajador").val();
    var material_construccion_trabajador = $("#material_construccion_trabajador").val();
    var cant_habi_trabajador = $("#cant_habi_trabajador").val();

    //situacion de salud
    var sit_salud_trabajador = $("input[name='sit_salud_trabajador']:checked").val();
    var problema_salud_trab_des = $("#problema_salud_trab_des").val();

    //actividades recreativas
    var practica_deporte_condicional = $("input[name='practica_deporte_condicional']:checked").val();
    var actividades_recre_traba = $("#actividades_recre_traba").val();
    var cual_deporte_trabajador = $("#cual_deporte_trabajador").val();

    //ultimos
    var observaciones_trabajador = $("#observaciones_trabajador").val();
    var diagnostico_social_trabajador = $("#diagnostico_social_trabajador").val();

    $.ajax({
      url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Agregar_Titular_C',
      type: 'POST',
      dataType: 'json',
      data: {
        apellidopat_trabajador: apellidopat_trabajador,
        apellidomat_trabajador: apellidomat_trabajador,
        nombres_trabajador: nombres_trabajador,
        FechaNacimiento: FechaNacimiento,
        his_trabajador: his_trabajador,
        edad_trabajador: edad_trabajador,
        sexo: sexo,
        CodigoEstadoCivil: CodigoEstadoCivil,
        IdGradoInst: IdGradoInst,
        combo_departamentos: combo_departamentos,
        combo_provincias: combo_provincias,
        combo_distritos: combo_distritos,
        telefono_trabajador: telefono_trabajador,
        celular_trabajador: celular_trabajador,
        email_trabajador: email_trabajador,
        dni_trabajador: dni_trabajador,
        IdDptoHos: IdDptoHos,
        combo_unidad_hospital:combo_unidad_hospital,
        combo_areas_hospital:combo_areas_hospital,
        funcion_des_trabajador: funcion_des_trabajador,
        IdCondicionTrabajo: IdCondicionTrabajo,
        pertenece_trabajador: pertenece_trabajador,
        pension_trabajador: pension_trabajador,
        cargo_trabajador: cargo_trabajador,
        FechaIngreso: FechaIngreso,
        afiliacion_cond: afiliacion_cond,
        conyuge_trabaja_trabajador: conyuge_trabaja_trabajador,
        otro_trabajo_trabajador: otro_trabajo_trabajador,
        condicion_trabajo_trabajador: condicion_trabajo_trabajador,
        deudas_trabajador: deudas_trabajador,
        tipo_vivienda_trabajador: tipo_vivienda_trabajador,
        cant_miembros_trabajador: cant_miembros_trabajador,
        material_construccion_trabajador: material_construccion_trabajador,
        cant_habi_trabajador: cant_habi_trabajador,
        sit_salud_trabajador: sit_salud_trabajador,
        problema_salud_trab_des: problema_salud_trab_des,
        practica_deporte_condicional: practica_deporte_condicional,
        actividades_recre_traba: actividades_recre_traba,
        cual_deporte_trabajador: cual_deporte_trabajador,
        observaciones_trabajador: observaciones_trabajador,
        diagnostico_social_trabajador: diagnostico_social_trabajador,
        direccion_trabajador: direccion_trabajador
      },
      success: function(data){
        if (data.dato == "M_1") {
          alert("Registro Grabado con Exito");
          LimpiarCajasTexto();
          LimpiarSelect();
          
          return 0;
        }
        if (data.dato == "M_2") {
          alert("Ya existe una persona con el mismo numero de DNI registrado");
          return 0;
        }
        else {
          alert("Intentelo de nuevo, o comuniquese con el administrador");
          return 0;
        }
      }
    });
    });
  
  //actualizar
  $("#actualizar_datos").click(function(event) {
    if ($("#dni_buscar").val().length < 1) {
        alert('Ingrese un numero de DNI para actualizar');
        return 0;
      }

    else {
        //validacion de datos
    //validando apellidos paternos
    if ($("#apellidopat_trabajador").val().length < 1) {
      alert("Ingrese un Apellido Paterno");
      return 0;
    }

    //validando apellidos materno
    if ($("#apellidomat_trabajador").val().length < 1) {
      alert("Ingrese un Apellido Materno");
      return 0;
    }

    //validando nombres
    if ($("#nombres_trabajador").val().length < 1) {
      alert("Ingrese un Nombre");
      return 0;
    }

    //validando fecha de nacimiento
    if ($("#FechaNacimiento").val().length < 1) {
      alert("Ingrese una Fecha de Nacimiento");
      return 0;
    }

    //validando numero de historia clinica
    if ($("#his_trabajador").val().length < 1) {
      alert("Ingrese un numero de historia clinica");
      return 0;
    }

    //validando edad del trabajador
    //if ($("#edad_trabajador").val().length < 1) {
    //  alert("Ingrese una edad");
    //  return 0;
    //}

    //validando sexo
    if(!$("input[name=sexo]:checked").val()) {
      alert('Seleccione un tipo sexo');
      return 0;
    }

    //validando estado civil
    if ($("#CodigoEstadoCivil").val().length < 1) {
      alert("Seleccion un estado civil");
      return 0;
    }

    //validando grado de instruccion
    if ($("#IdGradoInst").val().length < 1) {
      alert("Seleccion un grado de instruccion");
      return 0;
    }

    //validando departamentos
    if ($("#combo_departamentos").val().length < 1) {
      alert("Seleccion un departameto");
      return 0;
    }

    //validando provincias
    if ($("#combo_provincias").val().length < 1) {
      alert("Seleccion una provincia");
      return 0;
    }

    //validando distritos
    if ($("#combo_distritos").val().length < 1) {
      alert("Seleccion un distrito");
      return 0;
    }

    //validando direccion
    if ($("#direccion_trabajador").val().length < 1) {
      alert("Ingrese una direccion domiciliaria");
      return 0;
    }

    //validadno telefono
    //if ($("#telefono_trabajador").val().length < 1) {
    //  alert("Ingrese un numero de telefono");
    //  return 0;
    //}

    //validando numero de celular
    //if ($("#celular_trabajador").val().length < 1) {
    //  alert("Ingrese un numero de celular");
    //  return 0;
    //}

    //validando un numero de dni
    if ($("#dni_trabajador").val().length < 1) {
      alert("Ingrese un numero de dni");
      return 0;
    }

    //validando departamento de hospital
    if ($("#IdDptoHos").val().length < 1) {
      alert("Seleccione un Departamento/Servicio/Oficina");
      return 0;
    }

    

    //validando funcion del trabajador
    if ($("#funcion_des_trabajador").val().length < 1) {
      alert("Ingrese una funcion para el trabajador");
      return 0;
    }

    //validadno condicion del trabajador
    if ($("#IdCondicionTrabajo").val().length < 1) {
      alert("Ingrese una condicion de trabajo");
      return 0;
    }

    //validando pertenece trabajador
    if ($("#pertenece_trabajador").val().length < 1) {
      alert("Seleccione una pertenencia");
      return 0;
    }    

    //validando un tipo de pension
    if ($("#pension_trabajador").val().length < 1) {
      alert("Seleccione un tipo de pension");
      return 0;
    }

    //validar cargo de trabajador
    if ($("#cargo_trabajador").val().length < 1) {
      alert("Ingrese un cargo para el trabajador");
      return 0;
    }    

    //validar fecha de ingreso 
    if ($("#FechaIngreso").val().length < 1) {
      alert("Ingrese un fecha de ingreso");
      return 0;
    } 

    //validar afiliacion a ESSALUD
    if(!$("input[name=afiliacion]:checked").val()) {
      alert('Seleccione si esta o no afiliado a ESSALUD');
      return 0;
    }

    //validacion de conyuge
    if(!$("input[name=conyuge_trabaja_trabajador]:checked").val()) {
      alert('Seleccione si el conyuge trabaja o no');
      return 0;
    }

    //validando otro trabajo
    if(!$("input[name=otro_trabajo_trabajador]:checked").val()) {
      alert('Seleccione si el trabajador tiene otro trabajo');
      return 0;
    }

    //validando la condicion de trabajo del conyugue
    //if(!$("input[name=condicion_trabajo_trabajador]:checked").val()) {
    //  alert('Seleccione la condicion de trabajo del conyuge');
    //  return 0;
    //}

    //validando si existes deudas
    if(!$("input[name=deudas_trabajador]:checked").val()) {
      alert('Seleccione si existe algun tipo de deudas');
      return 0;
    }

    //validando el tipo de vivienda
    if ($("#tipo_vivienda_trabajador").val().length < 1) {
      alert("Seleccion un tipo de vivienda");
      return 0;
    } 

    //validando cantidad de miembros del hogar
    if ($("#cant_miembros_trabajador").val().length < 1) {
      alert("Ingrese la cantidad de miembros del hogar");
      return 0;
    } 

    //validando el material de construccion
    if ($("#material_construccion_trabajador").val().length < 1) {
      alert("Seleccione el tipo de material de construccion");
      return 0;
    }

    //validando cantidad de habitaciones
    if ($("#cant_habi_trabajador").val().length < 1) {
      alert("Ingrese la cantidad de habitaciones");
      return 0;
    }

    //validando si tienes problemas de salud
    if(!$("input[name=sit_salud_trabajador]:checked").val()) {
      alert('Seleccione si se tiene problemas de salud');
      return 0;
    }

    //validando practica del deporte
    if(!$("input[name=practica_deporte_condicional]:checked").val()) {
      alert('Seleccione si se practica algun deporte');
      return 0;
    }

      //datos a actualizar
      var dni_buscar = $("#dni_buscar").val();
      var apellidopat_trabajador = $("#apellidopat_trabajador").val();
      var apellidomat_trabajador = $("#apellidomat_trabajador").val();
      var nombres_trabajador = $("#nombres_trabajador").val();
      var FechaNacimiento = $("#FechaNacimiento").val();
      var his_trabajador = $("#his_trabajador").val();
      var edad_trabajador = $("#edad_trabajador").val();
      var sexo = $("input[name='sexo']:checked").val();
      var CodigoEstadoCivil = $("#CodigoEstadoCivil").val();
      var IdGradoInst = $("#IdGradoInst").val();
      var combo_departamentos = $("#combo_departamentos").val();
      var combo_provincias = $("#combo_provincias").val();
      var combo_distritos = $("#combo_distritos").val();
      var direccion_trabajador =$("#direccion_trabajador").val();
      var telefono_trabajador = $("#telefono_trabajador").val();
      var celular_trabajador = $("#celular_trabajador").val();
      var email_trabajador = $("#email_trabajador").val();
      var dni_trabajador = $("#dni_trabajador").val();
      var IdDptoHos = $("#IdDptoHos").val();
      var combo_unidad_hospital = $("#combo_unidad_hospital").val();
      var combo_areas_hospital = $("#combo_areas_hospital").val();
      var funcion_des_trabajador = $("#funcion_des_trabajador").val();
      var IdCondicionTrabajo = $("#IdCondicionTrabajo").val();
      var pertenece_trabajador = $("#pertenece_trabajador").val();
      var pension_trabajador = $("#pension_trabajador").val();
      var cargo_trabajador = $("#codigo_cargo_trb").val();
      var FechaIngreso = $("#FechaIngreso").val();
      var afiliacion_cond = $("input[name='afiliacion']:checked").val();
      var conyuge_trabaja_trabajador = $("input[name='conyuge_trabaja_trabajador']:checked").val();
      var otro_trabajo_trabajador = $("input[name='otro_trabajo_trabajador']:checked").val();
      var condicion_trabajo_trabajador = $("input[name='condicion_trabajo_trabajador']:checked").val();
      var deudas_trabajador = $("input[name='deudas_trabajador']:checked").val();
      var tipo_vivienda_trabajador = $("#tipo_vivienda_trabajador").val();
      var cant_miembros_trabajador = $("#cant_miembros_trabajador").val();
      var material_construccion_trabajador = $("#material_construccion_trabajador").val();
      var cant_habi_trabajador = $("#cant_habi_trabajador").val();
      var sit_salud_trabajador = $("input[name='sit_salud_trabajador']:checked").val();
      var problema_salud_trab_des = $("#problema_salud_trab_des").val();
      var practica_deporte_condicional = $("input[name='practica_deporte_condicional']:checked").val();
      var actividades_recre_traba = $("#actividades_recre_traba").val();
      var cual_deporte_trabajador = $("#cual_deporte_trabajador").val();
      var observaciones_trabajador = $("#observaciones_trabajador").val();
      var diagnostico_social_trabajador = $("#diagnostico_social_trabajador").val();

      //enviando datos a actualizar
      $.ajax({
        url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Actualizar_Titular_C',
        type: 'POST',
        dataType: 'json',
        data: {
          dni_buscar: dni_buscar,
          apellidopat_trabajador: apellidopat_trabajador,
          apellidomat_trabajador: apellidomat_trabajador,
          nombres_trabajador: nombres_trabajador,
          FechaNacimiento: FechaNacimiento,
          his_trabajador: his_trabajador,
          edad_trabajador: edad_trabajador,
          sexo: sexo,
          CodigoEstadoCivil: CodigoEstadoCivil,
          IdGradoInst: IdGradoInst,
          combo_departamentos: combo_departamentos,
          combo_provincias: combo_provincias,
          combo_distritos: combo_distritos,
          telefono_trabajador: telefono_trabajador,
          celular_trabajador: celular_trabajador,
          email_trabajador: email_trabajador,
          dni_trabajador: dni_trabajador,
          IdDptoHos: IdDptoHos,
          combo_unidad_hospital:combo_unidad_hospital,
          combo_areas_hospital:combo_areas_hospital,
          funcion_des_trabajador: funcion_des_trabajador,
          IdCondicionTrabajo: IdCondicionTrabajo,
          pertenece_trabajador: pertenece_trabajador,
          pension_trabajador: pension_trabajador,
          cargo_trabajador: cargo_trabajador,
          FechaIngreso: FechaIngreso,
          afiliacion_cond: afiliacion_cond,
          conyuge_trabaja_trabajador: conyuge_trabaja_trabajador,
          otro_trabajo_trabajador: otro_trabajo_trabajador,
          condicion_trabajo_trabajador: condicion_trabajo_trabajador,
          deudas_trabajador: deudas_trabajador,
          tipo_vivienda_trabajador: tipo_vivienda_trabajador,
          cant_miembros_trabajador: cant_miembros_trabajador,
          material_construccion_trabajador: material_construccion_trabajador,
          cant_habi_trabajador: cant_habi_trabajador,
          sit_salud_trabajador: sit_salud_trabajador,
          problema_salud_trab_des: problema_salud_trab_des,
          practica_deporte_condicional: practica_deporte_condicional,
          actividades_recre_traba: actividades_recre_traba,
          cual_deporte_trabajador: cual_deporte_trabajador,
          observaciones_trabajador: observaciones_trabajador,
          diagnostico_social_trabajador: diagnostico_social_trabajador,
          direccion_trabajador: direccion_trabajador
        },
        success: function(data) {
          if (data.dato == "M_1") {
            alert("Registro Actualizado con Exito");
            LimpiarCajasTexto();
            LimpiarSelect();
            
            return 0;
          }
          if (data.dato == "M_2") {
            alert("Ya existe una persona con el mismo numero de DNI registrado");
            return 0;
          }
          else {
            alert("Intentelo de nuevo, o comuniquese con el administrador");
            return 0;
          }
        }
      });
      
    }
  });

  //imprimir_ficha
  $("#imprimir_ficha").click(function(event) {
    if ($("#dni_buscar").val().length < 1) {
        alert('Ingrese un numero de DNI para poder imprimir');
        return 0;
      }
    else {
      var dni_buscar = $("#dni_buscar").val();
      window.open('../../MVC_Vista/Servicio_Social/MascaraImprimir.php?dni_buscar='+dni_buscar);
      return 0;  
    }
    
  });

  //eliminar_ficha
  $("#eliminar_ficha").click(function(event) {
    if ($("#dni_buscar").val().length < 1) {
        alert('Ingrese un numero de DNI para eliminar');
        return 0;
      }
    else {
      var dni_buscar = $("#dni_buscar").val();
      $.ajax({
        url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Eliminar_Registros_C',
        type: 'POST',
        dataType: 'json',
        data: {dni_buscar: dni_buscar},
        success: function(data) {
          if (data.dato == "M_1") {
            alert("Registro Eliminado con Exito");
            LimpiarCajasTexto();
            LimpiarSelect();
            $("#tabla_familiares").html("  ");            
            return 0;
          }
          if (data.dato == "M_2") {
            alert("Ya existe una persona con el mismo numero de DNI registrado");
            return 0;
          }
          else {
            alert("Intentelo de nuevo, o comuniquese con el administrador");
            return 0;
          }
        }
      });
      
    }
  });


  });
</script>



</head>
 
 <body>
 
 <ul class="pro15 nover">
  <li><a href="#" id="Grabar_Nuevo_Registro"><em class="guardar nover"></em><b>Grabar Nuevo Registro</b></a></li>
  <li><a href="#" id="actualizar_datos"><em class="actualiza nover"></em><b>Actualizar</b></a></li>
  <li><a href="#" id="imprimir_ficha"><em class="citar nover"></em><b>Imprimir</b></a></li>
  <li><a href="#" id="eliminar_ficha"><em class="cobrar nover"></em><b>Eliminar</b></a></li>
</ul> 
 <br>
  <label></label>

  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>BUSQUEDA</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>DNI</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="dni_buscar"></td>
          </tr>
          
        </table></td>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><button type="button"class="regular" id="buscar_trabajador_x_dni">Buscar</button></td>
          </tr>
        </table>

        
          
        </td>
      </tr>
    </table>
  
  </fieldset>

  <br>

  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DATOS GENERALES</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Apellido Paterno</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="apellidopat_trabajador"></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Apellido Materno</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="apellidomat_trabajador"></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Nombres</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="nombres_trabajador"></td>
          </tr>
          <tr>
           <td width="190" height="16"><strong>Fecha de Nacimiento</strong></td>
           <td width="260"><input name="FechaNacimiento" type="text" class="texto" id="FechaNacimiento" placeholder="<?php echo vfecha(substr($FechaServidor,0,10)).' 00:00'?>"   size="25"  autocomplete=OFF  />
           <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />
                    <script type="text/javascript"> 
                         Calendar.setup({ 
                          inputField     :    "FechaNacimiento",     // id del campo de texto 
                           ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
                           button     :    "lanzador1"     // el id del botón que lanzará el calendario 
                      }); 
                    </script>
            </td>
          </tr>
          <tr >
            <td width="190" height="16"><strong>N° Historia Clinica</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="his_trabajador"></td>
          </tr>
          <tr >
            <td width="190" height="16"><strong>Edad</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="edad_trabajador" disabled></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Sexo</strong></td>
            <td width="260">
              <input type="radio" name="sexo" value="1" placeholder="" size="20" id="masculino">M
              <input type="radio" name="sexo" value="2" placeholder="" size="20" id="femenino">F
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Estado Civil</strong></td>
            <td width="260"><input name="NombreEstadoCivil" type="text" class="texto" id="NombreEstadoCivil" size="25"  autocomplete=OFF class="text" /></td><input type="hidden" id="CodigoEstadoCivil" name="CodigoEstadoCivil" /></td>
          </tr>
        </table></td>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          
          <tr>
            <td width="190" height="16"><strong>Grado de Instruccion</strong></td>
            <td width="260"><input name="GradoInstruccion" type="text" class="texto" id="GradoInstruccion" size="25"  autocomplete=OFF class="text" /></td><input type="hidden" id="IdGradoInst" name="IdGradoInst" /></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Departamentos</strong></td>
            <td width="260">
              <select name="" id="combo_departamentos" class="Combos">
                <option value="">  </option>
                
              </select>
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Provincias</strong></td>
            <td width="260">
              <select name="" id="combo_provincias" class="Combos">
                <option value="">  </option>
                
              </select>
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Distrito</strong></td>
            <td width="260">
              <select name="" id="combo_distritos" class="Combos">
                <option value="">  </option>
                
              </select>
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Direccion</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="direccion_trabajador" ></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Telefono</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="telefono_trabajador"></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Celular</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="celular_trabajador"></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Email</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="email_trabajador"></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>DNI</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="dni_trabajador" ></td>
          </tr>
        </table>

        
          
        </td>
      </tr>
    </table>
  
  </fieldset>
  <BR>

<fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DATOS LABORALES</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Dpto/Servicio/Oficina</strong></td>
            <td width="260">
                <select name="" id="IdDptoHos" class="Combos">
                <option value="">  </option>
                
              </select>
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Unidad</strong></td>
            <td width="260">
                <select name="" id="combo_unidad_hospital" class="Combos">
                <option value="">  </option>
                
              </select>
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Area</strong></td>
            <td width="260">
                <select name="" id="combo_areas_hospital" class="Combos">
                <option value="0">  </option>
                
              </select>
            </td>
          </tr>
          
          <tr>
            <td width="190" height="16"><strong>Funcion que desempeña</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="funcion_des_trabajador"></td>
          </tr>
          
          <tr >
            <td width="190" height="16"><strong>Condicion</strong></td>
            <td width="260"><input name="CondicionTrabajo" type="text" class="texto" id="CondicionTrabajo" size="25"  autocomplete=OFF class="text" /></td><input type="hidden" id="IdCondicionTrabajo" name="IdCondicionTrabajo" /></td>
          </tr>
          
          
        </table></td>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Cargo</strong></td>
            <td width="260"><input type="hidden" id="codigo_cargo_trb"/><input type="text" class="texto" size="25" id="cargo_trabajador" ></td>
          </tr>
          <tr>
           <td width="190" height="16"><strong>Fecha de Ingreso</strong></td>
           <td width="260"><input name="FechaIngreso" type="text" class="texto" id="FechaIngreso" placeholder="<?php echo vfecha(substr($FechaServidor,0,10)).' 00:00'?>"   size="25"  autocomplete=OFF  />
           <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador2" width="16" height="15" border="0" id="lanzador2" title="Fecha Ingreso"  />
                    <script type="text/javascript"> 
                         Calendar.setup({ 
                          inputField     :    "FechaIngreso",     // id del campo de texto 
                           ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
                           button     :    "lanzador2"     // el id del botón que lanzará el calendario 
                      }); 
                    </script>
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Pertenece</strong></td>
            <td width="260"><select name="" id="pertenece_trabajador"   style="" class="Combos" >
              <option value="0"> </option>
              <option value="MUTUAL SANITARIA">Mutual Sanitaria</option>
              <option value="FONSERFUTS">Fonserfuts</option>
              <option value="COOPERATIVAS">Cooperativas</option>
              <option value="OTROS">Otros</option>
            </select></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Regimen de Pension</strong></td>
            <td width="260"><select name="" id="pension_trabajador"   style="" class="Combos" >
              <option value=""> </option>
              <option value="1">Ley 20530</option>
              <option value="2">Ley 19990</option>
              <option value="3">AFP</option>
              <option value="4">Services</option>
              option
            </select></td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Afiliado a ESSALUD</strong></td>
            <td width="260">
              <input type="radio" name="afiliacion" value="SI" placeholder="" size="20" id="afil_essalud_si">SI
              <input type="radio" name="afiliacion" value="NO" placeholder="" size="20" id="afil_essalud_no">NO
            </td>
          </tr>
          
        </table>
          
        </td>
      </tr>
    </table>
  
  </fieldset>        
        

<BR>
<fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>SITUACION ECONOMICA</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Conyuge/Conviviente trabaja</strong></td>
            <td width="260">
              <input type="radio" name="conyuge_trabaja_trabajador" value="SI" placeholder="" size="20" id="conyuge_trabaja_si">SI
              <input type="radio" name="conyuge_trabaja_trabajador" value="NO" placeholder="" size="20" id="conyuge_trabaja_no">NO
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>Tiene otro trabajo</strong></td>
            <td width="260">
              <input type="radio" name="otro_trabajo_trabajador" value="SI" placeholder="" size="20" id="otro_trabajo_si">SI
              <input type="radio" name="otro_trabajo_trabajador" value="NO" placeholder="" size="20" id="otro_trabajo_no">NO
            </td>
          </tr>
        </table></td>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Condincion</strong></td>
            <td width="260">
              <input type="radio" name="condicion_trabajo_trabajador" value="ESTABLE" placeholder="" size="20" id="condicion_trabajo_estable">Estable
              <input type="radio" name="condicion_trabajo_trabajador" value="EVENTUAL" placeholder="" size="20" id="condicion_trabajo_eventual">Eventual
            </td>
          </tr>
          
          <tr>
            <td width="190" height="16"><strong>Deudas Financieras</strong></td>
            <td width="260">
              <input type="radio" name="deudas_trabajador" value="SI" placeholder="" size="20" id="deudas_trabaja_si">SI
              <input type="radio" name="deudas_trabajador" value="NO" placeholder="" size="20" id="deudas_trabaja_no">NO
            </td>
          </tr>
          
        </table>
          
        </td>
      </tr>
    </table>
  
  </fieldset>       
 <BR>

 <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>SITUACION DE VIVIENDA</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Tipo de Vivienda</strong></td>
            <td width="260">
              <select name="" id="tipo_vivienda_trabajador"   style="" class="Combos" >
              <option value="0"> </option>
              <option value="1">Vivienda Propia</option>
              <option value="2">Alquilada</option>
              <option value="3">Alojada</option>
              <option value="4">Asent. Humano</option>
            </select>
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>N° Miembros</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="cant_miembros_trabajador"></td>
          </tr>
        </table></td>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>Material de construccion</strong></td>
            <td width="260">
             <select name="" id="material_construccion_trabajador"   style="" class="Combos" >
              <option value="0"> </option>
              <option value="1">Noble</option>
              <option value="2">Noble sin acabar</option>
              <option value="3">otro material</option>
            </select>
            </td>
          </tr>
          
          <tr>
            <td width="190" height="16"><strong>N° Habitaciones</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="cant_habi_trabajador"></td>
          </tr>
          
        </table>
          
        </td>
      </tr>
    </table>
  
  </fieldset> 

  <br>

  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>SITUACION DE SALUD</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="300" height="16"><strong>¿Padece de algun problema de salud cronica?</strong></td>            
          </tr>
          <tr>
            <td width="190" height="16"><strong>Cual</strong></td>            
          </tr>
        </table></td>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="260">
              <input type="radio" name="sit_salud_trabajador" value="SI" placeholder="" size="20" id="sit_salud_trabajador_si">SI
              <input type="radio" name="sit_salud_trabajador" value="NO" placeholder="" size="20" id="sit_salud_trabajador_no">NO
            </td>
          </tr>
          <tr>
             <td width="260"><input type="text" class="texto" size="25" id="problema_salud_trab_des"></td>
          </tr>
                   
        </table>
          
        </td>
      </tr>
    </table>
  
  </fieldset> 

   <br>

  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>ACTIVIDADES RECREATIVAS</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>¿Practica algun deporte?</strong></td>
            <td width="260">
              <input type="radio" name="practica_deporte_condicional" value="SI" placeholder="" size="20" id="practica_deporte_condicional_si">Si
              <input type="radio" name="practica_deporte_condicional" value="NO" placeholder="" size="20" id="practica_deporte_condicional_no">No
            </td>
          </tr>
          <tr>
            <td width="190" height="16"><strong>¿Que actividades le gustaria realizar?</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="actividades_recre_traba"></td>
          </tr>
          <tr>
           
          </tr>
        </table></td>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="190" height="16"><strong>¿Cual?</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="cual_deporte_trabajador"></td>
          </tr>
          
        </table>
          
        </td>
      </tr>
    </table>
  
  </fieldset> 

  <br>

  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>OBSERVACIONES</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="862" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="862">
              <textarea name="" class="texto" cols="142" rows="6" style="margin:1%;" id="observaciones_trabajador"></textarea>
            </td>
          </tr>          
        </table></td>
        
      </tr>
    </table>
  
  </fieldset> 

  <br>

  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DIAGNOSTICO SOCIAL</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="862" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <td width="862">
              <textarea name="" class="texto" cols="142" rows="6" style="margin:1%;" id="diagnostico_social_trabajador"></textarea>
            </td>
          </tr>          
        </table></td>
        
      </tr>
    </table>
  
  </fieldset> 
  <br>

  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>COMPOSICION FAMILIAR</strong></legend>
    <table width="1200" border="0">
      <tr>
        <td width="1200" valign="middle"  ><table border="0" cellpadding="0">
          
          <table id="tabla_familiares" class="CSSTableGenerator" style="margin:1%;">
            
          </table>
        
          <tr>
            <td>
              <button type="button" class="regular" id="Agregar_Parientes"><img src="../../MVC_Complemento/img/agregar_diagnostico.png" height="12" width="10" alt=""> Agregar Pariente</button>
            </td>
          </tr>          
        </table></td>
      </tr>

    </table>
  
  </fieldset>  
          
 </body>         