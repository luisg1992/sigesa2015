<head>
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/demo.css">
 
 
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/jquery.autocomplete.css" />
    
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/funciones2.js"></script>
 
 
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery_enter.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src="../../MVC_Complemento/js/jquery.autocomplete.js"></script> 

<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
 <script type="text/javascript" src="../../MVC_Complemento/js/jsalert/jquery.alerts.js"></script>
 <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/js/jsalert/jquery.alerts.css"/>
 
<style type="text/css">
<!--
#tblSample td, th { padding: 0.2em; }
.classy0 { background-color: #234567; color: #89abcd; }
.classy1 { background-color: #89abcd; color: #234567; }

#ocultarTexto {
  display: none;
}




 dt,  table, tbody, tfoot, thead, tr, th, td {
	margin:0;
	padding:0;
	border:0;
	font-weight:inherit;
	font-style:inherit;
	 
	font-family:inherit;
	vertical-align:baseline;
}


 
.cuerpo1 {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
color: #000;
 
}

form
{width: auto; background-color: #ffffff; }

textarea.Formulario {
padding: 5px;
border: 1px solid #D4D4D4;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
width:100%;
}

input.Formulario {
border: 1px solid #D4D4D4;
padding: 5px;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px; color: #666;
}

.Combos {
     font: small-caption cursive ; 

     background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    width: 150px;
    float:left;
	
text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;

}

 
.texto {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#FFFEEF;
  
    /*float:left;*/

}
.textodesable {
	font: small-caption; 
	  background: #ffffff;
    border: 1px solid #848284;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    padding: 4px;
    text-transform: uppercase; 
    -moz-box-shadow:0px 0px 3px #aaa;
    -webkit-box-shadow:0px 0px 3px #aaa;
    box-shadow:0px 0px 3px #aaa;
    background-color:#ECECEC;
  
    /*float:left;*/

}





 /*-----*/
 fieldset { border:1px solid green }

.legend {
  border: 1px solid #BDD7FF;
width: 95%;
background: #F7F7F7;
padding: 3px;

/*  text-align:right;*/
  }
  
  
   
-->
</style> 

<script>
  function Cerrar(){
   
    parent.RefreshHijos();
    parent.googlebox.hide();
  }
</script>

<script>
  $(document).ready(function() {
    
   //autocompletado estado civil
      $("#tipo_parentesco").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Tipos_Parentesco_C&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#tipo_parentesco").result(function(event, data, formatted) {
        $("#tipo_parentesco").val(data[2]);
        $("#id_tipo_parentesco").val(data[1]);      
     });

     //autocompletado estado civil
      $("#estado_civil").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_EstadoCivil&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#estado_civil").result(function(event, data, formatted) {
        $("#estado_civil").val(data[2]);
        $("#id_estado_civil").val(data[1]);      
     });

   //autocompletado de grado de instruccion 
    $("#grado_instruccion").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Grado_Instruccion_C&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#grado_instruccion").result(function(event, data, formatted) {
        $("#grado_instruccion").val(data[2]);
        $("#id_grado_instruccion").val(data[1]);      
     }); 

     //autocompletado tipos de ocupacion - 
     $("#tipos_ocupacion").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Tipos_Ocupacion_C&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#tipos_ocupacion").result(function(event, data, formatted) {
        $("#tipos_ocupacion").val(data[2]);
        $("#id_tipos_ocupacion").val(data[1]);      
     });

     //AUTOCOMPLETAR PARENTESCO
      $("#tipo_parentesco").autocomplete("../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Tipos_Parentesco_C&TipoBusqueda=1", {
      width: 500, 
      matchContains: true,
      });    
      
     $("#tipo_parentesco").result(function(event, data, formatted) {
        $("#tipo_parentesco").val(data[2]);
        $("#id_tipo_parentesco").val(data[1]);      
     });

  });
</script>        

<script>
  $(document).ready(function() {
    $("#Grabar_familiar").click(function(event) {
      var id_titular = $("#id_titular").val();
      var apellido_paterno_familiar = $("#apellido_paterno_familiar").val();
      var apellido_materno_familiar = $("#apellido_materno_familiar").val();
        if ($("#apellido_materno_familiar").val().length < 1) {var apellido_materno_familiar=0;} 
      var nombres_familiar = $("#nombres_familiar").val();
      var edad_familiar = $("#edad_familiar").val();
        if ($("#edad_familiar").val().length < 1) {var edad_familiar=0;} 
      var id_tipo_parentesco = $("#id_tipo_parentesco").val();
        if ($("#id_tipo_parentesco").val().length < 1) {var id_tipo_parentesco=0;}  
      var id_estado_civil = $("#id_estado_civil").val();
        if ($("#id_estado_civil").val().length < 1) {var id_estado_civil=0;} 
      var id_grado_instruccion = $("#id_grado_instruccion").val();
        if ($("#id_grado_instruccion").val().length < 1) {var id_grado_instruccion=0;} 
      var id_tipos_ocupacion = $("#id_tipos_ocupacion").val();
         if ($("#id_tipos_ocupacion").val().length < 1) {var id_tipos_ocupacion=0;} 
      var FechaNacimientoFamiliar = $("#FechaNacimientoFamiliar").val();
        if ($("#FechaNacimientoFamiliar").val().length < 1) {var FechaNacimientoFamiliar='01/01/2000';}  
      var observacion_familiar = $("#observacion_familiar").val();
        if ($("#observacion_familiar").val().length < 1) {var observacion_familiar=0;}  


      var dni_familiar = $("#dni_familiar").val();
        if ($("#dni_familiar").val().length < 1) {var dni_familiar=0;}  
      // validando campos - pendiente

      $.ajax({
        url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Agregar_Familiares_Titular_C',
        type: 'POST',
        dataType: 'json',
        data: {
          id_titular: id_titular,
          dni_familiar: dni_familiar,
          apellido_paterno_familiar: apellido_paterno_familiar,
          apellido_materno_familiar: apellido_materno_familiar,
          nombres_familiar: nombres_familiar,
          edad_familiar: edad_familiar,
          id_tipo_parentesco: id_tipo_parentesco,
          id_estado_civil: id_estado_civil,
          id_grado_instruccion: id_grado_instruccion,
          id_tipos_ocupacion: id_tipos_ocupacion,
          FechaNacimientoFamiliar: FechaNacimientoFamiliar,
          observacion_familiar: observacion_familiar
        },
        success: function(data) {

          //si la grabacion fue exitosa
          if (data.dato == "M_1") {
            parent.RefreshHijos();
            parent.googlebox.hide();
            alert("Familiar Registrado");
            return 0;
          }

          //si es que hubo un error 
          else {
            alert("Intentelo de nuevo");
            return 0;
          }
        }
      });
      
    });
  });

</script>   


  </head>

 
 
 <body   >
 

 
  <label></label>
  <fieldset class="fieldset legend">
    <legend style="color:#03C"><strong>DATOS DEL FAMILIAR</strong></legend>
    <table width="848" border="0">
      <tr>
        <td width="462" valign="middle"  ><table border="0" cellpadding="0"   >
          <tr>
            <?php 
              $DNI = $_REQUEST["dni_titular"];
              echo "<input type='hidden' id='id_titular' value='".$DNI."'>";
             ?>
            <td width="190" height="16"><strong>Apellido Paterno</strong></td>
            <td width="260"><input type="text" class="texto" size="25" id="apellido_paterno_familiar"></td>
          </tr>
          <tr>
            <td height="16" ><strong>Apellido Materno</strong></td>
            <td><input type="text" class="texto" size="25" id="apellido_materno_familiar"></td>
          </tr>
          <tr>
            <td height="16" ><strong>Nombres</strong></td>
            <td><input type="text" class="texto" size="25" id="nombres_familiar"></td>
          </tr>
          <tr>
            <td height="16" ><strong>DNI</strong></td>
            <td><input type="text" class="texto" size="25" id="dni_familiar"></td>
          </tr>
           <tr>
            <td height="16"><strong>Edad</strong></td>
            <td><input type="text" class="texto" size="25" id="edad_familiar"></td>
          </tr>
          <tr>
            <td height="19" ><strong>Parentesco</strong></td>
            <td><input type="text" class="texto" size="25" id="tipo_parentesco"><input type="hidden" name="" id="id_tipo_parentesco"></td>
          </tr>
          <tr>
            <td height="19" ><strong>Estado Civil</strong></td>
            <td><input type="text" class="texto" size="25" id="estado_civil"><input type="hidden" name="" id="id_estado_civil"></td>
          </tr>
          <tr >
            <td height="19" ><strong>Grado de Instruccion</strong></td>
            <td><input type="text" class="texto" size="25" id="grado_instruccion"><input type="hidden" name="" id="id_grado_instruccion"></td>
          </tr>
          <tr>
            <td height="19" ><strong>Ocupacion</strong></td>
            <td><input type="text" class="texto" size="25" id="tipos_ocupacion"><input type="hidden" name="" id="id_tipos_ocupacion"></td>
          </tr>
          <tr>
            <td height="19" ><strong>Fecha de Nacimiento</strong></td>
            <td><input name="FechaNacimientoFamiliar" type="text" class="texto" id="FechaNacimientoFamiliar" size="25"  autocomplete=OFF  />
            <img src="../../MVC_Complemento/img/calendario.jpg" name="lanzador1" width="16" height="15" border="0" id="lanzador1" title="Fecha Inicio"  />
            <script type="text/javascript"> 
                         Calendar.setup({ 
                          inputField     :    "FechaNacimientoFamiliar",     // id del campo de texto 
                           ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
                           button     :    "lanzador1"     // el id del botón que lanzará el calendario 
                      }); 
                    </script>
           </td>
          </tr>
          <tr>
            <td height="19" ><strong>Observacion</strong></td>
            <td><input type="text" class="texto" size="25" id="observacion_familiar"></td>
          </tr>
        </table></td>        
      </tr>
    </table>
  
  </fieldset>
  
  
        
        

<BR>


<fieldset class="fieldset legend">
    <legend style="color:#03C"> </legend>
    <table width="848" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
    <th scope="row">
      <button type="button" class="regular" id="Grabar_familiar">Grabar</button>
      <button type="button" class="regular" id="Cerrar" onClick="Cerrar()">Cerrar</button>
    </th>
  </tr>
</table>
</fieldset>



 <div id="DetalleOrden">
          <iframe id="Panet_Datos" name="Panet_Datos" width="0" height="0" frameborder="0">
  <ilayer width="0" height="0" id="Panet_Datos" name="Panet_Datos">
  </ilayer>
</iframe>
		  </div>
          
 </body>         