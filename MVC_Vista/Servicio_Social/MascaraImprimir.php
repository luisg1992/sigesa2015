<?php 
ini_set('memory_limit', '1024M'); 
require('../../MVC_Modelo/Servicio_SocialM.php');
require('../../MVC_Complemento/fpdf/fpdf.php');
require('../../MVC_Complemento/librerias/Funciones.php');
 


$pdf = new FPDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Helvetica','',8);

$data = Buscar_fs_dni_M($_REQUEST["dni_buscar"]);
	if ($data!=null) {
		foreach ($data as $item) {
			$DNI=$item["DNI"];
			$NOMBRES=strtoupper(utf8_encode($item["NOMBRES"]));
			$APELLIDOPAT=strtoupper(utf8_encode($item["APELLIDOPAT"]));
			$APELLIDOMAT=strtoupper(utf8_encode($item["APELLIDOMAT"]));
			$FECHANACIMIENTO=substr($item["FECHANACIMIENTO"],0,10);
			$HISTORIA=$item["HISTORIA"];
			$SEXO=strtoupper(utf8_decode($item["SEXO"]));
			$ESTADOCIVIL=strtoupper(utf8_decode($item["ESTADOCIVIL"]));
			$IDESTCI=$item["IDESTCI"];
			$GRADOINSTRUCCION=strtoupper(utf8_decode($item["GRADOINSTRUCCION"]));
			$CODGRAD=$item["CODGRAD"];
			$DEPARTAMENTO=strtoupper(utf8_decode($item["NOMBREDPTOP"]));
			$PROVINCIA=strtoupper(utf8_decode($item["NOMBREPRONVINCIA"]));
			$DISTRITO=strtoupper(utf8_decode($item["NOMBREDISTRITO"]));
			$DIRECCION=strtoupper(utf8_decode($item["DIRECCION"]));
			$REFERENCIADOMIC=strtoupper(utf8_decode($item["REFERENCIADOMIC"]));
			$TELEFONO=$item["TELEFONO"];
			$CELULAR=$item["CELULAR"];
			$EMAIL=$item["EMAIL"];
			
			$DPTO_OFICINA=strtoupper(utf8_decode($item["DPTOHOS"]));
			$UNIDAD_OFICINA=strtoupper(utf8_decode($item["UNIHOS"]));
			if ($UNIDAD_OFICINA == "ESTACTIC") {
				$UNIDAD_OFICINA = " ";
			}

			$AREA_OFICINA=strtoupper(utf8_decode($item["AREAHOS"]));
			if ($AREA_OFICINA == "ESTATIC") {
				$AREA_OFICINA = " ";
			}
			$IDDPTO_OFICINA=$item["IDDPTO_OFICINA"];
			$CARGO=strtoupper(utf8_decode($item["CARGO"]));
			$FUNCIONDESEM=strtoupper(utf8_decode($item["FUNCIONDESEM"]));
			$FECHAINGRESO=substr($item["FECHAINGRESO"],0,10);
			$CONDICION=strtoupper(utf8_decode($item["CONDICION"]));
			$IDCONDINCION=$item["IDCONDINCION"];
			$PERTENECE=strtoupper(utf8_decode($item["PERTENECE"]));
			$PENSIONTIPO=strtoupper(utf8_decode($item["PENSIONNOMBRE"]));
			$CONDICICONVIVI=$item["CONDICICONVIVI"];
			$CONVIVIENTTRABAJA=$item["CONVIVIENTTRABAJA"];
			$DUEDAS=$item["DUEDAS"];
			$OTROTRA=$item["OTROTRA"];
			$TIPOVIVIENDA=strtoupper(utf8_decode($item["TIPOVIVIENDA"]));
			$MATERIALCONST=strtoupper(utf8_decode($item["MATERIALCONST"]));
			$NUMMIENBROSH=$item["NUMMIENBROSH"];
			$NUMHABITAH=$item["NUMHABITAH"];
			$PROBLEMASSALUD=$item["PROBLEMASSALUD"];
			$DESCRPSSALUD=strtoupper(utf8_decode($item["DESCRPSSALUD"]));
			$OBSERVACION=strtoupper(utf8_decode($item["OBSERVACION"]));
			$DISGNOSTSOCIAL=strtoupper(utf8_decode($item["DISGNOSTSOCIAL"]));
			$ESSALUD=strtoupper(utf8_decode($item["ESSALUD"]));
			$TIPOVV=$item["TIPOVV"];
			$MATERIAL=$item["MATERIAL"];
			$CONDICIONALDEPORTE=strtoupper(utf8_decode($item["CONDICIONALDEPORTE"]));
			$CUALDEPORTE=strtoupper(utf8_decode($item["CUALDEPORTE"]));
			$ACTIVIDADESDEPOR=strtoupper(utf8_decode($item["ACTIVIDADESDEPOR"]));
		}
	}
	//calculando edad
	$fecha = time() - strtotime($FECHANACIMIENTO);
	$edad = floor((($fecha / 3600) / 24) / 360);
	
	//calculando tiempo de servicio x años
	$fecha = time() - strtotime($FECHAINGRESO);
	$servicio = floor((($fecha / 3600) / 24) / 360);
	
	//configurando fecha
	list($anio,$mes,$dia) = explode("-",$FECHANACIMIENTO);
	$FECHANACIMIENTO = $dia."/".$mes."/".$anio;
	list($anio,$mes,$dia) = explode("-",$FECHAINGRESO);
	$FECHAINGRESO = $dia."/".$mes."/".$anio;

	//limpiando condicion de trabajo de conviviente
	if ($CONVIVIENTTRABAJA == "undefined") {
		$CONVIVIENTTRABAJA = " ";
	}
//CABECERA
	$pdf->SetFont('Helvetica','',9);
	$pdf->Cell(20);
	$pdf->Image('../../MVC_Complemento/img/hndac.jpg',15,5,18,20);
	$pdf->SetFont('Helvetica','B',9);
	$pdf->Cell(145);
	$pdf->Cell(20,0,'UNIDAD BIENESTAR',0,1,'C');
	$pdf->Ln(3);
	$pdf->Cell(165);
	$pdf->Cell(20,0,'DE PERSONAL',0,1,'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','BU',12);
	$pdf->Cell(55);
	$pdf->Cell(70,2,'FICHA SOCIAL DEL TRABAJADOR',0,0,'C');	
	$pdf->Ln(5);

//DATOS GENERALES
	$pdf->Ln(15);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'I.-DATOS GENERALES:',0,1,'L');
	$pdf->SetFont('Helvetica');
	$pdf->Ln(5);
	$pdf->Cell(63,7,$APELLIDOPAT,1,0,'C');
	$pdf->Cell(63,7,$APELLIDOMAT,1,0,'C');
	$pdf->Cell(63,7,$NOMBRES,1,0,'C');
	$pdf->Ln(7);
	$pdf->Cell(63,5,'APELLIDO PATERNO','LRB',0,'C');
	$pdf->Cell(63,5,'APELLIDO MATERNO','LRB',0,'C');
	$pdf->Cell(63,5,'NOMBRES','LRB',0,'C');
	$pdf->Ln(10);
	$pdf->Cell(40,0,'FECHA DE NACIMIENTO: ',0,0,'L');
	$pdf->Cell(20,0,$FECHANACIMIENTO,0,0,'L');
	$pdf->Cell(50);
	$pdf->Cell(15,0,utf8_decode('N° DNI: '),0,0,'L');
	$pdf->Cell(20,0,$DNI,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(15,0,utf8_decode('EDAD: '),0,0,'L');
	$pdf->Cell(15,0,$edad,0,0,'L');
	$pdf->Cell(10,0,utf8_decode('SEXO: '),0,0,'L');
	$pdf->Cell(25,0,$SEXO,0,0,'L');
	$pdf->Cell(25,0,utf8_decode('ESTADO CIVIL: '),0,0,'L');
	$pdf->Cell(30,0,$ESTADOCIVIL,0,0,'L');
	$pdf->Cell(25,0,utf8_decode('N° HISTORIA: '),0,0,'L');
	$pdf->Cell(25,0,$HISTORIA,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(47,0,utf8_decode('GRADO DE INSTRUCCION: '),0,0,'L');
	$pdf->Cell(70,0,$GRADOINSTRUCCION,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(30,0,utf8_decode('DEPARTAMENTO: '),0,0,'L');
	$pdf->Cell(30,0,$DEPARTAMENTO,0,0,'L');
	$pdf->Cell(30,0,utf8_decode('PROVINCIAS: '),0,0,'L');
	$pdf->Cell(30,0,$PROVINCIA,0,0,'L');
	$pdf->Cell(30,0,utf8_decode('DISTRITOS: '),0,0,'L');
	$pdf->Cell(30,0,$DISTRITO,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(30,0,utf8_decode('DOMICILIO: '),0,0,'L');
	$pdf->Cell(120,0,$DIRECCION,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(20,0,utf8_decode('TELEFONO: '),0,0,'L');
	$pdf->Cell(30,0,$TELEFONO,0,0,'L');
	$pdf->Cell(20,0,utf8_decode('CELULAR: '),0,0,'L');
	$pdf->Cell(30,0,$CELULAR,0,0,'L');
	$pdf->Cell(20,0,utf8_decode('EMAIL: '),0,0,'L');
	$pdf->Cell(30,0,$EMAIL,0,0,'L');

//DATOS LABORALES
	$pdf->Ln(10);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'II.-DATOS LABORALES:',0,1,'L');
	$pdf->SetFont('Helvetica');
	$pdf->Ln(7);
	$pdf->Cell(90,0,utf8_decode('DEPARTAMENTO/SERVICIO/OFICINA, DONDE LABORA:'),0,0,'L');
	$pdf->Cell(70,0,$DPTO_OFICINA,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(20,0,utf8_decode('UNIDAD:'),0,0,'L');
	$pdf->Cell(60,0,$UNIDAD_OFICINA,0,0,'L');
	$pdf->Cell(20,0,utf8_decode('AREA:'),0,0,'L');
	$pdf->Cell(50,0,$AREA_OFICINA,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(15,0,utf8_decode('CARGO: '),0,0,'L');
	$pdf->Cell(55,0,$CARGO,0,0,'L');
	$pdf->Cell(50,0,utf8_decode('FUNCION QUE DESEMPEÑA: '),0,0,'L');
	$pdf->Cell(75,0,$FUNCIONDESEM,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(40,0,utf8_decode('FECHA INGRESO HNDAC:'),0,0,'L');
	$pdf->Cell(20,0,$FECHAINGRESO,0,0,'L');
	$pdf->Cell(40,0,utf8_decode('TIEMPO DE SERVICIO:'),0,0,'L');
	$pdf->Cell(20,0,$servicio,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(25,0,utf8_decode('CONDICION:'),0,0,'L');
	$pdf->Cell(28,0,$CONDICION,0,0,'L');
	$pdf->Cell(20,0,utf8_decode('AFILIADO:'),0,0,'L');
	$pdf->Cell(10,0,$ESSALUD,0,0,'L');
	$pdf->Cell(23,0,utf8_decode('PERTENECE:'),0,0,'L');
	$pdf->Cell(23,0,$PERTENECE,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(40,0,utf8_decode('REGIMEN DE PENSION:'),0,0,'L');
	$pdf->Cell(10,0,$PENSIONTIPO,0,0,'L');

//SITUACION ECONOMICA
	$pdf->Ln(10);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'III.-SITUACION ECONOMICA:',0,1,'L');
	$pdf->SetFont('Helvetica');	
	$pdf->Ln(7);
	$pdf->Cell(60,0,utf8_decode('CONYUGE/CONVIVIENTE TRABAJA:'),0,0,'L');
	$pdf->Cell(50,0,$CONDICICONVIVI,0,0,'L');
	$pdf->Cell(25,0,utf8_decode('CONDICION:'),0,0,'L');
	$pdf->Cell(10,0,$CONVIVIENTTRABAJA,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(60,0,utf8_decode('USTED TIENE OTRO TRABAJO:'),0,0,'L');
	$pdf->Cell(30,0,$OTROTRA,0,0,'L');
	$pdf->Cell(60,0,utf8_decode('ADEUDA A ENTIDADES FINANCIERAS:'),0,0,'L');
	$pdf->Cell(10,0,$DUEDAS,0,0,'L');

//SITUACION DE VIVIENDA
	$pdf->Ln(10);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'III.-SITUACION DE VIVIENDA:',0,1,'L');
	$pdf->SetFont('Helvetica');	
	$pdf->Ln(7);
	$pdf->Cell(35,0,utf8_decode('TIPO DE VIVIENDA:'),0,0,'L');
	$pdf->Cell(40,0,$TIPOVIVIENDA,0,0,'L');
	$pdf->Cell(55,0,utf8_decode('MATERIAL DE CONSTTRUCCION:'),0,0,'L');
	$pdf->Cell(40,0,$MATERIALCONST,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(55,0,utf8_decode('N° MIEMBROS DEL HOGAR:'),0,0,'L');	
	$pdf->Cell(10,0,$NUMMIENBROSH,0,0,'L');
	$pdf->Cell(55,0,utf8_decode('N° HABITACIONES:'),0,0,'L');	
	$pdf->Cell(10,0,$NUMHABITAH,0,0,'L');
	
//SITUACION DE SALUDA
	$pdf->Ln(10);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'III.-SITUACION DE SALUD:',0,1,'L');
	$pdf->SetFont('Helvetica');	
	$pdf->Ln(7);
	$pdf->Cell(100,0,utf8_decode('¿USTED PADECE DE ALGUN PROBLEMA DE SALUD CRONICO?:'),0,0,'L');
	$pdf->Cell(10,0,$PROBLEMASSALUD,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(10,0,'CUAL: ',0,0,'L');
	$pdf->Cell(170,0,$DESCRPSSALUD,0,0,'L');

//ACTIVIDADES RECREATIVAS
	$pdf->Ln(10);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'III.-ACTIVIDADES RECREATIVAS:',0,1,'L');
	$pdf->SetFont('Helvetica');	
	$pdf->Ln(7);
	$pdf->Cell(50,0,utf8_decode('PRACTICA ALGUN DEPORTE:'),0,0,'L');
	$pdf->Cell(40,0,$CONDICIONALDEPORTE,0,0,'L');
	$pdf->Cell(20,0,utf8_decode('CUAL: '),0,0,'L');
	$pdf->Cell(50,0,$CUALDEPORTE,0,0,'L');
	$pdf->Ln(7);
	$pdf->Cell(78,0,utf8_decode('¿QUE ACTIVIDADES LES GUSTARIA REALIZAR?:'),0,0,'L');
	$pdf->Cell(50,0,$ACTIVIDADESDEPOR,0,0,'L');

//AGREGANDO UNA NUEVA PAGINA
	$pdf->AddPage();
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'VII.-COMPOSICION FAMILIAR:',0,1,'L');
	$pdf->SetFont('Helvetica','',8);
	$pdf->Ln(7);
	$pdf->Cell(50,7,'APELLIDOS Y NOMBRES',1,0,'C');
	$pdf->Cell(10,7,'EDAD',1,0,'C');
	$pdf->Cell(25,7,'PARENTESCO',1,0,'C');
	$pdf->Cell(25,7,'ESTADO CIVIL',1,0,'C');
	$pdf->Cell(40,7,'G.INSTRUCCION',1,0,'C');
	$pdf->Cell(40,7,'OCUPACION',1,0,'C');

	$data_familia = Buscar_Familiares_M($_REQUEST["dni_buscar"]);

	if ($data_familia!=null) {
		foreach ($data_familia as $item) {
			$IDENTIFICADO_F = $item["IDENTIFICADO"];
			$PATERNO_F = strtoupper(utf8_decode($item["PATERNO"]));
			$MATERNO_F = strtoupper(utf8_decode($item["MATERNO"]));
			$NOMBRES_F = strtoupper(utf8_decode($item["NOMBRES"]));
			$EDAD_F = $item["EDAD"];
			$DNI_F=$item["DNI"];
			$IDPARENTESCO_F = $item["IDPARENTESCO"];
			$PARENTESCO_F = strtoupper(utf8_decode($item["PARENTESCO"]));
			$IDCIVIL_F = $item["IDCIVIL"];
			$ESTADOCIVIL_F = strtoupper(utf8_decode($item["ESTADOCIVIL"]));
			$IDGRADO_F = $item["IDGRADO"];
			$GRADO_F = strtoupper(utf8_decode($item["GRADO"]));
			$IDOCUPA_F = $item["IDOCUPA"];
			$OCUPACION_F = strtoupper(utf8_decode($item["OCUPACION"]));
			//configuracion de fecha
			list($anio,$mes,$dia) = explode("-",substr($item["FECHA"],0,10));
			$FECHA_F = $dia."/".$mes."/".$anio;				
			$OBSERVACION_F = strtoupper(utf8_decode($item["OBSERVACION"]));

			if($IDPARENTESCO_F > 4) {
				$PARENTESCO_F = 'HIJO';
			}
	
		//MOSTRANDO FAMILIARES
		$pdf->SetFont('Helvetica','',7);
		$pdf->Ln(7);
		$pdf->Cell(50,7,$PATERNO_F.' '.$MATERNO_F.' '.$NOMBRES_F,'LRB',0,'C');
		$pdf->Cell(10,7,$EDAD_F,'LRB',0,'C');
		$pdf->Cell(25,7,$PARENTESCO_F,'LRB',0,'C');
		$pdf->Cell(25,7,$ESTADOCIVIL_F,'LRB',0,'C');
		$pdf->Cell(40,7,$GRADO_F,'LRB',0,'C');
		$pdf->Cell(40,7,$OCUPACION_F,'LRB',0,'C');
		}
	}

//OBSERVACIONES
	$pdf->Ln(15);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'VII.-OBSERVACIONES:',0,1,'L');
	$pdf->SetFont('Helvetica');	
	$pdf->Ln(7);
	$pdf->Cell(190,0,$OBSERVACION,0,1,'L');
//DAGNOSTICO SOCIAL
	$pdf->Ln(15);
	$pdf->SetFont('Helvetica','b',9);
	$pdf->Cell(20,0,'IX.-DIAGNOSTICO SOCIAL:',0,1,'L');
	$pdf->SetFont('Helvetica');	
	$pdf->Ln(7);
	$pdf->Cell(190,0,$DISGNOSTSOCIAL,0,1,'L');

//FIN DEL DOCUMENTO
	$pdf->SetY(-50);
	$pdf->SetFont('Arial','b',7);
	$pdf->Cell(150,5,'Declaro bajo juramento que todos los datos referidos en el presente documento son verdaderos',0,1,'L');
	$pdf->Ln(7);
	$pdf->SetFont('Helvetica','BU',9);
 	$pdf->Cell(50,5,'                                                                     ',0,0,'L');
 	$pdf->Cell(70);
 	$pdf->Cell(50,5,'                                                                     ',0,0,'L');
 	$pdf->Ln(7);
 	$pdf->SetFont('Helvetica','',8);
 	$pdf->Cell(50,5,'FIRMA DEL TRABAJADOR',0,0,'R');
 	$pdf->Cell(70);
 	$pdf->Cell(57,5,'JEFE DE BIENESTAR DE PERSONAL',0,0,'R');

$pdf->Output();


?>