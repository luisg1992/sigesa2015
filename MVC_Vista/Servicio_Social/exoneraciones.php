<?php 
	date_default_timezone_set('America/Lima');
	include_once('../../MVC_Modelo/Servicio_SocialM.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ficha Social</title>
	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
	<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		

    </style>
	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui2.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>
	<script src="../../MVC_Complemento/Highcharts/js/highcharts.js"></script>
	<script src="../../MVC_Complemento/Highcharts/js/modules/exporting.js"></script> 
	
								<script type="text/javascript">
								
								//Variable que almacenara el Descuento del Paciente
								var Desc=0;
								//Variable que almacenara el IdPaciente del Paciente
								var IdPaciente=0;
								//Al Inicializar el HTML
								$(document).ready(function(){
										$(".ExoneracionLetra").html('<img src="../../MVC_Complemento/img/candado_cerrado.png" width="80px" height="90px">');	
										//Al Inicializar el Focus en la Caja de Texto de Historia Clinica
									    $("#HistoriaClinica").blur(); 
										//Al Inicio Ocultar la Suma de Medicamentos
										$("#Suma_Medicamentos").css("display", "none");
										//Al Inicializar se desabilitara el Selected 
										$( "#tipo_lista_Cuenta" ).prop( "disabled", true );
										//Al Inicializar el Boton de Generar Comprobante se encontrara desabilitado
										$("#GenerarComprobante").linkbutton('disable');
										//Al Inicializar el Boton de Guardar Exoneraciones se encontrara desabilitado
										$("#GuardarExoneracion").linkbutton('disable');
										//Cargar la Busqueda de los Datos de un Paciente al hacer enter	
										$("#HistoriaClinica").textbox('textbox').bind('keydown', function(e){
										if (e.keyCode == 13){
											var HistoriaClinica = $("#HistoriaClinica").textbox('getText');
															// Busqueda del IdPaciente del Paciente por la Historia Clinica
															$.ajax({
																url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=PacientesObtenerIdPacientePorHistoriaClinica',
																type: 'POST',
																dataType: 'json',
																data: 
																{
																	//Se almacena la variable de Historia
																	HistoriaClinica: HistoriaClinica
																},
																success: function(Busqueda_Paciente)
																{
																	//Si existe la Historia Clinica
																	if (Busqueda_Paciente.variable == 'M_1') 
																	{	
																		CargarDatos_Generales_de_Paciente(HistoriaClinica);
																	}
																	
																	///Si no existe la Historia Clinica mostrara el mensaje 
																	if (Busqueda_Paciente.variable == 'M_0') 
																	{	
																		$.messager.alert('SIGESA','La Historia Clinica Ingresada no Existe');	
																	}						
																}
															});
											
										}
										});								
								});			
								</script>
								
								<script>
								function probar(IdCuenta){
												$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Grafico_Exoneraciones',
												type: 'POST',
												dataType: 'json',
												data: 
												{
												//Se almacena la variable de Historia
												IdCuenta: IdCuenta
												},
												success: function(reporte)
												{
												objeto=jQuery.parseJSON(reporte);
													$('#container').highcharts({
														chart: {
															plotBackgroundColor: null,
															plotBorderWidth: null,
															plotShadow: false,
															type: 'pie'
														},
														title: {
															text: 'Browser market shares January, 2015 to May, 2015'
														},
														tooltip: {
															pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
														},
														plotOptions: {
															pie: {
																allowPointSelect: true,
																cursor: 'pointer',
																dataLabels: {
																	enabled: false
																},
																showInLegend: true
															}
														},
														series: [{
															name: 'Brands',
															colorByPoint: true,
															data: objeto
														}]
													});
												}
												});
									}
								</script>
								
								
								<script type="text/javascript">
								
								function Cargar_Datos_por_Boton(){
									var HistoriaClinica = $("#HistoriaClinica").textbox('getText');
															// Busqueda del IdPaciente del Paciente por la Historia Clinica
															$.ajax({
																url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=PacientesObtenerIdPacientePorHistoriaClinica',
																type: 'POST',
																dataType: 'json',
																data: 
																{
																	//Se almacena la variable de Historia
																	HistoriaClinica: HistoriaClinica
																},
																success: function(Busqueda_Paciente)
																{
																	//Si existe la Historia Clinica
																	if (Busqueda_Paciente.variable == 'M_1') 
																	{	
																		CargarDatos_Generales_de_Paciente(HistoriaClinica);
																	}
																	
																	///Si no existe la Historia Clinica mostrara el mensaje 
																	if (Busqueda_Paciente.variable == 'M_0') 
																	{	
																		$.messager.alert('SIGESA','La Historia Clinica Ingresada no Existe');	
																	}						
																}
															});
								}
								</script>
								
								
								
								<script type="text/javascript">
								
								
								
								//Funcion para Cargar los Datos basicos del Paciente
								
								function CargarDatos_Generales_de_Paciente(HistoriaClinica){	
									
									$.ajax({
									url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarPacientesxNumeroCuenta',
									type: 'POST',
									dataType: 'json',
									data: 
										{
											numerocuenta: HistoriaClinica
										},
									success: function(resultado_p)
									{		
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=BuscarExistenciaPacienteFichaSocial',
												type: 'POST',
												dataType: 'json',
												data: 
												{
													numerocuenta: HistoriaClinica
												},
												success: function(resultado_t)
												{
													if (resultado_t.variable == 'M_1') 
													{
														$("#numichasocial").textbox('setText',resultado_t.numfs);
														$("#seguro").textbox('setText',resultado_t.TipoFinanciamiento);
														$.messager.alert('SIGESA','Este paciente tiene una ficha social: <br><br> PUNTAJE OBTENIDO: ' + resultado_t.puntaje + '<br> CATEGORIA: ' + resultado_t.letra);			
														
														//Cargar la Exoneracion por Letra
														$(".ExoneracionLetra").html(resultado_t.letra);	
														
														//Cargar la Exoneracion por Porcentaje
														Descuento=resultado_t.PorcentajeDesc;
														//Cargar el Porcentaje de Descuento 
														$('#porcentaje').progressbar({
															value: resultado_t.PorcentajeNumero
														});
														
														//Cargar el IdPaciente
														IdPaciente=resultado_p.IDPACIENTE;
														
														//Llamar a la funcion que carga las Cuentas del Paciente por su IdPaciente
														cargar_cuentas_paciente(IdPaciente,Descuento);
														
														
													}
													
													if (resultado_t.variable == 'M_0') 
													{
														var Descuento=1;
														var IdPaciente=0;
														$.messager.confirm('Confirm', 'El Paciente no cuenta con Ficha Social, Desea Continuar ?', function(r){
															
															if (r)
															{	
															//Cargar la Exoneracion por Letra
															$(".ExoneracionLetra").html('<img src="../../MVC_Complemento/img/candado_abierto.png" width="80px" height="90px">');
															$("#numichasocial").textbox('setText','NO TIENE FICHA');	
															
															// Busqueda del IdPaciente del Paciente por la Historia Clinica
															$.ajax({
																url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=PacientesObtenerIdPacientePorHistoriaClinica',
																type: 'POST',
																dataType: 'json',
																data: 
																{
																	//Se almacena la variable de Historia
																	HistoriaClinica: HistoriaClinica
																},
																success: function(Busqueda_Paciente)
																{
																if (Busqueda_Paciente.variable == 'M_1') 
																{	
																	cargar_cuentas_paciente(Busqueda_Paciente.IdPaciente,Descuento);	
															    }					
																}
															});
															}
																								
															else
															{
															$(".ExoneracionLetra").html('<img src="../../MVC_Complemento/img/candado_cerrado.png" width="80px" height="90px">');	
															Limpiar_Grillas();
															$("#MostrarFichaNuevabtn").linkbutton('disable');
															$("#GuardarFichaSocial").linkbutton('enable');
															$("#numichasocial").textbox('setText','NO TIENE FICHA');																		
															}
														});	
													}
													
													
												}
											});

											
												$("#PACIENTE").textbox('setText',resultado_p.PACIENTE);	
															

											
									}
								});
								}
								
								
								</script>
								
								
								
								
								<script type="text/javascript">
								
								function CargarConsumoporCuenta(numerocuenta,Descuento){
									
											//LLamar a la Funcion que Cargara todo lo Consumo por el Paciente	
											// de Atencion Ambulatoria (Consulta Externa)
																
																
											//Grilla de Servicios Ambulatorios
											cargar_grilla_servicios_ambulatorio(numerocuenta,Descuento);
																											
											//Grilla de Medicamentos Ambulatorios
											cargar_grilla_medicamentos_ambulatorio(numerocuenta,Descuento);		

											
							                Generar_Resumen_Cuentas(numerocuenta);
											
											probar(numerocuenta);
											
								}
								
										function Generar_Resumen_Cuentas(numerocuenta){
											$.ajax({
														url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Generar_Resumen_Cuentas_Array',
														type: 'POST',
														dataType: 'json',
														data: 
														{
														//Se almacena la variable de Historia
														Idcuenta: numerocuenta
														},
														success: function(htmlexterno)
														{
														$("#cargaexterna").html(htmlexterno);
														}
											});
										}
								
								</script>
								
								<script type="text/javascript">
								
								// Con esta funcion se limpia todas las grillas
								  function Limpiar_Grillas(){
									  
									  $('#lista_cuentas_paciente').datagrid('loadData', {"total":0,"rows":[]}); 
									  $('#consumo_servicios_cuenta').datagrid('loadData', {"total":0,"rows":[]}); 
									  $('#consumo_medicamentos_cuenta').datagrid('loadData', {"total":0,"rows":[]});

								  }

								</script>
								
								
								
								
								<script type="text/javascript">
								
								    function Cargar_Cuentas_Seleccionadas(Descuento){
										var ss = [];
										var rows = $('#lista_cuentas_paciente').datagrid('getSelections');
										for(var i=0; i<rows.length; i++){
											var row = rows[i];
											ss.push(","+row.IDCUENTAATENCION);
										}
										var Idcuenta=ss.join('').substring(1);
										
										//Se Carga lo consumido por el Paciente
										CargarConsumoporCuenta(Idcuenta,Descuento);
										
									}
															
								
									// Se Imprime un Resumen de todo lo consumo de acuerdo 
									// a las Cuentas Seleccionadas
								    function Generar_Resumen(){
										var ss = [];
										var rows = $('#lista_cuentas_paciente').datagrid('getSelections');
										for(var i=0; i<rows.length; i++){
											var row = rows[i];
											ss.push(","+row.IDCUENTAATENCION);
										}
										var Idcuenta=ss.join('').substring(1);
										$.ajax({
											url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Generar_Resumen_Cuentas',
											type: 'POST',
											dataType: 'json',
											data: {
												Idcuenta:Idcuenta
											},
											success:function(impresion)
											{
												window.open('data:application/vnd.ms-excel,' + encodeURIComponent(impresion));
												e.preventDefault();
											},
											 error: function() {
												 alert('Envio de Datos Incorrecto');
											}
										})
										
										
										
									}
								
									//Funcion que Carga la Grilla de Servicios Ambulatorios
									function cargar_cuentas_paciente(IdPaciente,Descuento)
										{
											
										//Al llamar la Funcion de Cargar Cuentas se Habilitara el Selected 
										$( "#tipo_lista_Cuenta" ).prop( "disabled", false );	
										/*var IdPaciente = $("#HistoriaClinica").textbox('getText');*/	
										var dg =$('#lista_cuentas_paciente').datagrid({
											title:'Lista de Cuentas',
											iconCls:'icon-edit',
											singleSelect:true,
											rownumbers:true,
											remoteSort:false,
											multiSort:true,
											collapsible:true,
											url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarCuentas_de_Pacientes&IdPaciente='+IdPaciente,
											columns:[[
													{field:'IDCUENTAATENCION',title:'Nº Cuenta',width:70},			
													{field:'FINANCIAMIENTO',title:'Tipo',width:61},
													{field:'DESTADOCUENTA',title:'Estado',width:50},
													{field:'TOTAL',title:'Total Pagar',width:80,sortable:true},
													{field:'SERVICIONOMBRE',title:'Servicio',width:130,sortable:true},
													{field:'FECHAINGRESO',title:'F. Apertura',width:80},
													{field:'TIPOSSERVICIO',title:'Tipo de Servicio',width:180,sortable:true}	
											]],
											onSelect:function(index,row){
													Cargar_Cuentas_Seleccionadas(Descuento);
													$("#GuardarExoneracion").linkbutton('enable');	
											},
											rowStyler: function(index,row){
												if (row.TOTAL!= 0){
													return 'font-weight:bold;';
												}
											}		
										});
										
										// Limpiar las Grillas Anteriormente Cargadas
										 $('#consumo_servicios_cuenta').datagrid('loadData', {"total":0,"rows":[]}); 
									     $('#consumo_medicamentos_cuenta').datagrid('loadData', {"total":0,"rows":[]});
										
									}		
								</script>
								
								
								
								
								
								
								
								
								
								<script type="text/javascript">
									//Funcion que Carga la Grilla de Servicios Ambulatorios
									function cargar_grilla_servicios_ambulatorio(numerocuenta,Descuento)
										{
																
										var dg =$('#consumo_servicios_cuenta').datagrid({
											title:'Exoneracion Pacientes - Ambulatorios',
											iconCls:'icon-edit',
											singleSelect:true,
											rownumbers:true,
											url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarConsumo_Ambulatorio_Grilla_Servicios_Cuenta&IdCuenta='+numerocuenta,
											columns:[[
												{field:'IdOrden',title:'IdOrden',width:50,hidden:true},
												{field:'IdProducto',title:'IdProducto',width:50,hidden:true},
												{field:'IdEstadoFacturacion',title:'IdEstadoFacturacion',width:50,hidden:true},
												{field:'TipoServicio',title:'TipoServicio',width:50,hidden:true},
												{field:'IdCuentaAtencion',title:'Nª Cuenta',width:70},
												{field:'Servicio',title:'Centro Produccion',width:120},
												{field:'Producto',title:'Producto',width:300},
												{field:'IdOrdenPago',title:'Or. Pago',width:60},
												{field:'Cantidad',title:'Cant',width:40},
												{field:'Precio',title:'Pr. Unit.',width:55},
												{field:'Importe_Formato',title:'Importe',width:60},
												{field:'ImporteExonerado',title:'Imp.Exo.',width:65,editor:{type:'numberbox',options:{precision:2}},styler:Style_Exoneracion,formatter:formatPrice},
												{field:'FechaCarga',title:'Fecha Carga',width:85}	
											]],

											onEndEdit:function(index,row){
												var ed = $(this).datagrid('getEditor', {
													index: index,
													field: 'ImporteExonerado'
												});						
												var valor=$(ed.target).numberbox('getText');
												
												if(valor<0)
												{
													row.ImporteExonerado = '0.00';
													$.messager.alert('SIGESA','MONTO INGRESADO NO VALIDO');	
												}
												
												else
												{
													
													if(row.TipoServicio==2)
													{
													Descuento='1';	
													}
													
													if(parseFloat(valor)<=parseFloat(row.Importe)*Descuento)
													{
													row.ImporteExonerado = $(ed.target).numberbox('getText');	
													}
													else
													{
													row.ImporteExonerado = '0.00';
													$.messager.alert('SIGESA','MONTO INGRESADO NO PERMITIDO  <br>Importe   : s/.'+row.Importe+'<br>          Descuento Max. : s/.'+(parseFloat(row.Importe)*Descuento).toFixed(2)+'');
													}	
												}
													
											},
											
											onAfterEdit:function(index,row,changes){
											//Cargar Total de Servicios Ambulatorios	
											Total_Servicios_Ambulatorio();
											},
											
											onLoadSuccess(data){
											//Cargar Total de Servicios Ambulatorios
											Total_Servicios_Ambulatorio();
											},
											
											onClickRow: function(index,row){
												if(row.IdEstadoFacturacion==4)
												{
												$(this).datagrid('endEdit', index);
												var ed = $(this).datagrid('getEditor', {index:index,field:field});
												$(ed.target).focus();
												}
											},
											
											rowStyler: function(index,row){
												if (row.IdProducto==0){
													return 'color:#FFFF00;background:red;font-weight:bold';
												}
												
												if (parseFloat(row.IdEstadoFacturacion)!=1 && parseFloat(row.IdProducto)!=4691){
													return 'color:#FF0000;';
												}
												if (parseFloat(row.IdProducto)==4691){
													return 'color:#33acff;';
												}
												
											}	
												
										});
										
										dg.datagrid('disableCellEditing').datagrid('gotoCell', {
											index: 0,
											field: 'Importe'
											});
										
										dg.datagrid('enableCellEditing').datagrid('gotoCell', {
											index: 0,
											field: 'Importe'
											});
											
										
									}
									
									
									
								</script>
								
								<script type="text/javascript">
								
								
									//Funcion que Carga la Grilla de Medicamentos Ambulatorios
									
									function cargar_grilla_medicamentos_ambulatorio(numerocuenta,Descuento)
										{
											
										//Resetear la suma de lo Exonerado										
										SumarExoneracion_Resetear_Valores();

										
										var dg =$('#consumo_medicamentos_cuenta').datagrid({
											title:'Exoneracion Pacientes - Ambulatorios',
											iconCls:'icon-edit',
											singleSelect:true,
											rownumbers:true,
											url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarConsumo_Ambulatorio_Grilla_Medicamentos_Cuenta&IdCuenta='+numerocuenta,
											columns:[[
												{field:'IDCUENTAATENCION',title:'Nª Cuenta',width:70},
												{field:'CENTROPRODUCCION',title:'Centro Produccion',width:120},
												{field:'DESCRIPCIONMEDICAMENTO',title:'Producto',width:300},
												{field:'CANTIDAD',title:'Cant',width:40},
												{field:'PRECIO',title:'Pr. Unit.',width:55},			
												{field:'IMPORTE_FORMATO',title:'Importe',width:60},
												{field:'IMPORTEEXONERADO',title:'Imp.Exo.',width:65,editor:{type:'numberbox',options:{precision:2}},styler:Style_Exoneracion,formatter:formatPrice},
												{field:'FECHACARGA',title:'Fecha Carga',width:85}		
											]],
		
											onEndEdit:function(index,row){
												var ed = $(this).datagrid('getEditor', {
													index: index,
													field: 'IMPORTEEXONERADO'
												});						
												var valor=$(ed.target).numberbox('getText');
													if(parseFloat(valor)<=parseFloat(row.IMPORTE)*Descuento)
													{
													row.IMPORTEEXONERADO = $(ed.target).numberbox('getText');	
													}
													else
													{
													row.IMPORTEEXONERADO = '0.00';
													$.messager.alert('SIGESA','MONTO INGRESADO NO PERMITIDO  <br>Importe   : s/.'+row.IMPORTE+'<br>          Descuento Max. : s/.'+(parseFloat(row.IMPORTE)*Descuento).toFixed(2)+'');
													}
											},
											onAfterEdit:function(index,row,changes){
											Total_Medicamentos_Ambulatorio();
											},
											onLoadSuccess(data){
	
											//Cargar Total de Medicamentos Ambulatorios
											Total_Medicamentos_Ambulatorio();											
											
											
											//Cargar Total de Servicios Ambulatorios
											Total_Servicios_Ambulatorio();

											},
											rowStyler: function(index,row){
												if (parseFloat(row.IDESTADOFACTURACION)!=1){
													return 'color:#FF0000;';
												}
		
											}
												
										});
										
										dg.datagrid('disableCellEditing').datagrid('gotoCell', {
											index: 0,
											field: 'Importe'
											});

										dg.datagrid('enableCellEditing').datagrid('gotoCell', {
											index: 0,
											field: 'Importe'
											});
									}
								</script>
								
								
								
								

								
								
								
								<script type="text/javascript">
								
										//Se le dara Estilo al campo de Exoneraciones	
										function Style_Exoneracion()
										{
												return 'background-color:#FFE4B5;color:blue;font-weight:bold';
										}
										
										
										//Se le dara Estilo al campo Pago Pendiente 
										function Style_Pago_Pendiente()
										{
												return 'background-color:#D1F2EB;';
										}
										

										//Funcion para dar el formato en caso sea igual a 0.00.
										function formatPrice(val,row){
											if (val =='0.00'){
												return '<span style="color:black;font-weight:normal">'+val+'</span>';
											} else {
												return val;
											}
										}		
								</script>

								
								
								
								
								<script type="text/javascript">
								
									//Funcion para hacer la Suma de Todo lo que Debe el Paciente
									//Siempre que el IdEstadoFacturacion sea 1 
									
									function Total_Servicios_Ambulatorio(){
										
										
										// Variable de Total de Servicios
										
										var Total_Servicios=0;
										
										// Variable del Total de Descontar cuando es Pago a Cuenta
										
										var Total_Descontar=0;
										
										//Variable que guarda el Total de lo Exonerado
										
										var TotalExoneradoServicios=0;
										
										//Variable que guarda el Total por Exonerar
										
										var TotalporExonerarServicios=0;
										
										// Variable que guarda el Total de Pagos por Medicamentos
										
										var Total_Por_Pagar_Medicamentos = $("#Total_Por_Pagar_Medicamentos").val();
										
										
										//Variable que Guarda la Resta del Total de Servicio menos lo Exonerado
										
										var Total=0;
										
																					
										//Variable que Guarda el Valor del Pago a Cuenta para los Servicios
											
										var Pago_Cuenta_Servicios=0;
										
										var rows = $('#consumo_servicios_cuenta').datagrid('getRows');
										
										
										for(var i=0; i<rows.length; i++)
											{
												
											var row = rows[i];
											var Importe=row.Importe;	
												
												//Realiza el Calculo de todos lo que se Pagara que Figura como pendiente de Pago
												if(parseFloat(row.IdEstadoFacturacion)==1  && parseInt(row.IdOrdenPago))
												{	
												//Realiza la Suma del Total de los Servicios a Deber	
												Total_Servicios=parseFloat(Total_Servicios)+parseFloat(Importe);
												}
												
												//Realiza el Calculo de todos los Pagos a Cuenta a Restar filtrados por el IdProducto
												if(parseFloat(row.IdEstadoFacturacion)==4 && row.IdProducto==4691)
												{
												Total_Descontar=parseFloat(Total_Descontar)+parseFloat(row.Importe);	
												}
												
												
												//Realiza el Calculo del total por Exonerar
												if(parseFloat(row.IdEstadoFacturacion)==1)
												{
												TotalporExonerarServicios=parseFloat(TotalporExonerarServicios)+parseFloat(rows[i].ImporteExonerado);		
												}
												
												//Realiza la Suma del Total de Exonerado
												TotalExoneradoServicios=parseFloat(TotalExoneradoServicios)+parseFloat(rows[i].ImporteExonerado);	
												
											
											}
											


											
											
											//Actualiza el Valor de Total de Pago a Cuenta
											$(".Pago_Cuenta").html('S/. '+Total_Descontar.toFixed(2));
											$("#Pago_Cuenta").val('S/. '+Total_Descontar.toFixed(2));
											  

											//Actualiza el Valor de Total a Exonerado
											$(".TotalExonerar_Servicios").html('S/. '+TotalExoneradoServicios.toFixed(2));
											 $("#TotalExonerar_Servicios").val('S/. '+TotalExoneradoServicios.toFixed(2));
											 
																						
											//Actualiza el Valor de Total a Exonerar
											$(".Total_por_Exonerar_Servicios").html('S/. '+TotalporExonerarServicios.toFixed(2));
											 $("#Total_por_Exonerar_Servicios").val('S/. '+TotalporExonerarServicios.toFixed(2));


											//Actualiza el Valor de Pago a Cuenta de Servicios
											Pago_Cuenta_Servicios=Total_Descontar-$("#Total_Por_Pagar_Medicamentos").val();  
											  
											//Actualiza el Valor de Total de Pago a Cuenta de Servicios
											$(".Pago_Cuenta_Servicios").html('S/. '+Validar_Numero(Pago_Cuenta_Servicios).toFixed(2));
											$("#Pago_Cuenta_Servicios").val('S/. '+Validar_Numero(Pago_Cuenta_Servicios).toFixed(2)); 
											
											
											
											Total_Servicios=Total_Servicios-Validar_Numero(Pago_Cuenta_Servicios);
											
											//Actualiza el Valor de Total de Servicios a Cobrar	
											$(".Total_Servicios").html('S/. '+Validar_Numero(Total_Servicios).toFixed(2));
											$("#Total_Servicios").val('S/. '+Validar_Numero(Total_Servicios).toFixed(2));
											
											
											
											//Actualiza el Valor del Total a Pagar 
											Total=Total_Servicios-TotalporExonerarServicios;

											 $(".Total_Por_Pagar_Servicios").html('S/. '+Validar_Numero(Total).toFixed(2));
											 $("#Total_Por_Pagar_Servicios").val('S/. '+Validar_Numero(Total).toFixed(2));

									}
									
									
									
									
								</script>

								
								
								<script type="text/javascript">
								
									//Funcion para hacer la Suma de Todo lo que Debe el Paciente
									//Siempre que el IdEstadoFacturacion sea 1 
									
									function Total_Medicamentos_Ambulatorio(){
										
										var Total_Medicamentos=0;
										var Total_Descontar=0;
										var TotalExoneradoMedicamentos=0;
										var Total=0;
										var rows = $('#consumo_medicamentos_cuenta').datagrid('getRows');
										for(var i=0; i<rows.length; i++)
											{
											var row = rows[i];
											var IMPORTE=row.IMPORTE;													

											
											    //Realiza el Calculo de todos lo que se Pagara que Figura como pendiente de Pago
												if(parseFloat(row.IDESTADOFACTURACION)==1)
												{	
												//Realiza la Suma del Total de los Medicamentos a Deber												
												Total_Medicamentos=parseFloat(Total_Medicamentos)+parseFloat(IMPORTE);					
												}

												//Realiza la Suma del Total de Exonerado
												TotalExoneradoMedicamentos=parseFloat(TotalExoneradoMedicamentos)+parseFloat(rows[i].IMPORTEEXONERADO);		
											}
											
												
											//Actualiza el Valor de Total de Servicios a Cobrar	
											$(".Total_Medicamentos").html(Total_Medicamentos.toFixed(2));
											$("#Total_Medicamentos").val(Total_Medicamentos.toFixed(2));

											  
											  
											//Actualiza el Valor de Total a Exonerar  
											$(".TotalExonerar_Medicamentos").html(TotalExoneradoMedicamentos.toFixed(2));
											$("#TotalExonerar_Medicamentos").val(TotalExoneradoMedicamentos.toFixed(2));
											
											
											 //Actualiza el Valor del Total a Pagar 
											 Total=Total_Medicamentos-TotalExoneradoMedicamentos;
																 
											 $(".Total_Por_Pagar_Medicamentos").html(Total.toFixed(2));
											 $("#Total_Por_Pagar_Medicamentos").val(Total.toFixed(2));
											 
									}
									
															
									function Retornar_Pago_Cuenta_Total()
									{
										
										// Variable del Total de Descontar cuando es Pago a Cuenta								
										var Total_Descontar=0;
										
										// Variable del Total de las Filas de la tabla
										var rows = $('#consumo_servicios_cuenta').datagrid('getRows');
										
										for(var i=0; i<rows.length; i++)
											{
												
											var row = rows[i];
											var Importe=row.Importe;	
											
											//Realiza el Calculo de todos los Pagos a Cuenta a Restar filtrados por el IdProducto
												if(parseFloat(row.IdEstadoFacturacion)==4 && row.IdProducto==4691)
												{
												Total_Descontar=parseFloat(Total_Descontar)+parseFloat(row.Importe);	
												}
	
											}
											
										return 	Total_Descontar;
									}
								</script>
								
								
																	
								<script type="text/javascript">
								
									   //Funcion para resetear las Sumas de lo exonerado

										function SumarExoneracion_Resetear_Valores()
										{
										var suma=0;
										$(".TotalExonerar_Servicios").html(suma.toFixed(2));
										$("#TotalExonerar_Servicios").val(suma.toFixed(2));					
										$(".TotalExonerar_Medicamentos").html(suma.toFixed(2));
										$(".Total_Por_Pagar_Servicios").html(suma.toFixed(2));
										$("#Total_Por_Pagar_Servicios").val(suma.toFixed(2));
										Total_Servicios_Ambulatorio();
										}
										
										//Funcion para Validar el Valor del Precio que no sea Negativo
										
										function Validar_Numero(numero)
										{
													if(numero<0)
													{
													validado=0;	
													}
													else
													{
													validado=numero;			
													}	
											return validado;	
										}
										
										function Retornar_Pago_Cuenta_Total()
										{
											
											// Variable del Total de Descontar cuando es Pago a Cuenta								
											var Total_Descontar=0;
											
											// Variable del Total de las Filas de la tabla
											var rows = $('#consumo_servicios_cuenta').datagrid('getRows');
											
											for(var i=0; i<rows.length; i++)
												{
													
												var row = rows[i];
												var Importe=row.Importe;	
												
												//Realiza el Calculo de todos los Pagos a Cuenta a Restar filtrados por el IdProducto
													if(parseFloat(row.IdEstadoFacturacion)==4 && row.IdProducto==4691)
													{
													Total_Descontar=parseFloat(Total_Descontar)+parseFloat(row.Importe);	
													}
		
												}
												
											return 	Total_Descontar;
										}

								</script>
								
								
								<script type="text/javascript">
									
									//Funcion para los Tabs de Consumo
									$(function(){
											$('#Tab_consumos').tabs({
													  onSelect: function(title){
														  
															if (title == 'Servicios'){ //Al Seleccionar el Tab de los Servicios Consumos por el Paciente
														
																$("#Suma_Servicios").css("display", "block");
																$("#Suma_Medicamentos").css("display", "none");														
															}
															
															if (title == 'Medicamentos'){ //Al Seleccionar el Tab de los Medicamentos Consumos por el Paciente
												
																$("#Suma_Servicios").css("display", "none");
																$("#Suma_Medicamentos").css("display", "block");
															}
													  }
													});

									});			
								</script>
								
								
								
								<script type="text/javascript">
								function Generar(){
												$.messager.confirm('Confirm', 'Esta Seguro de Realizar la Exoneracion?', function(r){
													if (r){
														Generar_Exoneraciones_Servicio();
														Generar_Exoneraciones_Medicamentos();
														Generar_Descuento_FactOrdenServiciosPagos();
														Generar_Descuento_FactOrdenesBienes();
												          }
												});	
								}
								</script>

								
								<script type="text/javascript">
								
							
								// Funcion para Atrapar los valores posibles ser Actualizados como inserciones
								
									function Generar_Exoneraciones_Servicio(){
										var ss = [];
										var rows = $('#consumo_servicios_cuenta').datagrid('getChanges');
										for(var i=0; i<rows.length; i++){
											var row = rows[i];
											var IdOrden=row.IdOrden;
											var IdProducto=row.IdProducto;
											var CantidadFinanciada=row.Cantidad;
											var TotalFinanciado=row.ImporteExonerado;
											var PrecioFinanciado=row.ImporteExonerado;
											Buscar_Exoneraciones_Facturacion_Servicio_Financiamiento(IdOrden,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado);
											}
											
											$("#GenerarComprobante").linkbutton('enable');
											$("#GuardarExoneracion").linkbutton('disable');												
											
											//Se Muestra el Mensaje de Generacion de Exonerado fue exitoso
											$.messager.alert('SIGESA','Se realizo satisfactoriamente la Exoneracion');
									}
								</script>
								
								
								
								<script type="text/javascript">
								
							
								// Funcion para Atrapar los valores posibles ser Actualizados como inserciones
								
									function Generar_Exoneraciones_Medicamentos(){
										var ss = [];
										var rows = $('#consumo_medicamentos_cuenta').datagrid('getChanges');
										for(var i=0; i<rows.length; i++){
											var row = rows[i];
											var MovNumero=row.MOVNUMERO;
											var MovTipo=row.MOVTIPO;
											var IdProducto=row.IDPRODUCTO;
											var CantidadFinanciada=row.CANTIDAD;
											var TotalFinanciado=row.IMPORTEEXONERADO;
											var PrecioFinanciado=row.IMPORTEEXONERADO;
											Buscar_Exoneraciones_Facturacion_Bienes_Financiamiento(MovNumero,MovTipo,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado);
											}
									}
								</script>
								

								
								
								<script type="text/javascript">
											
											function Buscar_Exoneraciones_Facturacion_Bienes_Financiamiento(MovNumero,MovTipo,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado)											
											{
											
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Exoneraciones_Facturacion_Bienes_Financiamiento',
												type: 'POST',
												dataType: 'json',
												data: 
												{
													MovNumero: MovNumero,
													MovTipo: MovTipo,
													IdProducto: IdProducto
													
												},
												success: function(resultado_t)
												{
													if (resultado_t.variable == 'M_1') 
													{
													Modificar_Exoneraciones_Facturacion_Bienes_Financiamiento(MovNumero,MovTipo,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado);
													}
													if (resultado_t.variable == 'M_0') 
													{
													Agregar_Exoneraciones_Facturacion_Bienes_Financiamiento(MovNumero,MovTipo,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado);	
													}
		
												}
											});											
											}			
								
								</script>
								
								
								<script type="text/javascript">
											
											function Agregar_Exoneraciones_Facturacion_Bienes_Financiamiento(MovNumero,MovTipo,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado)											
											{
											var IdUsuario = $("#IdUsuario").val();
											var FechaIngreso = $("#FechaIngreso").val();
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Agregar_Exoneraciones_Facturacion_Bienes_Financiamiento',
												type: 'POST',
												dataType: 'json',
												data: 
												{
													MovNumero: MovNumero,
													MovTipo: MovTipo,
													IdProducto: IdProducto,
													CantidadFinanciada: CantidadFinanciada,
													IdTipoFinanciamiento: '9',
													IdFuenteFinanciamiento: '0',
													IdEstadoFacturacion: '1',
													TotalFinanciado: TotalFinanciado,
													PrecioFinanciado: PrecioFinanciado,
													IdUsuarioAutoriza:IdUsuario,
													FechaAutoriza:FechaIngreso
												}
											});											
											}			
								
								</script>
								
								
								
								<script type="text/javascript">
											
											function  Modificar_Exoneraciones_Facturacion_Bienes_Financiamiento(MovNumero,MovTipo,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado)										
											{
											var IdUsuario = $("#IdUsuario").val();
											var FechaIngreso = $("#FechaIngreso").val();

											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Modificar_Exoneraciones_Facturacion_Bienes_Financiamiento',
												type: 'POST',
												dataType: 'json',
												data: 
												{
													MovNumero: MovNumero,
													MovTipo: MovTipo,
													IdProducto: IdProducto,
													CantidadFinanciada: CantidadFinanciada,
													IdTipoFinanciamiento: '9',
													IdFuenteFinanciamiento: '0',
													IdEstadoFacturacion: '1',
													TotalFinanciado: TotalFinanciado,
													PrecioFinanciado: PrecioFinanciado,
													IdUsuarioAutoriza:IdUsuario,
													FechaAutoriza:FechaIngreso
												}
											});											
											}			
								
								</script>
								
								
								
								
								
								
								
								
								
								
								<script type="text/javascript">
											
											function Buscar_Exoneraciones_Facturacion_Servicio_Financiamiento(IdOrden,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado)											
											{				
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Buscar_Exoneraciones_Facturacion_Servicio_Financiamiento',
												type: 'POST',
												dataType: 'json',
												data: 
												{
													IdOrden: IdOrden,
													IdProducto: IdProducto
												},
												success: function(resultado_t)
												{
													if (resultado_t.variable == 'M_1') 
													{
														
													Modificar_Exoneraciones_Facturacion_Servicio_Financiamiento(IdOrden,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado);

													}
													if (resultado_t.variable == 'M_0') 
													{
													Agregar_Exoneraciones_Facturacion_Servicio_Financiamiento(IdOrden,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado);
													}
		
												}
											});											
											}			
								
								</script>

								<script type="text/javascript">
											
											function Agregar_Exoneraciones_Facturacion_Servicio_Financiamiento(IdOrden,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado)											
											{
											var IdUsuario = $("#IdUsuario").val();
											var FechaIngreso = $("#FechaIngreso").val();
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Agregar_Exoneraciones_Facturacion_Servicio_Financiamiento',
												type: 'POST',
												dataType: 'json',
												data: 
												{
													IdOrden: IdOrden,
													IdProducto: IdProducto,
													CantidadFinanciada: CantidadFinanciada,
													IdTipoFinanciamiento: '9',
													IdFuenteFinanciamiento: '0',
													IdEstadoFacturacion: '1',
													TotalFinanciado: TotalFinanciado,
													PrecioFinanciado: PrecioFinanciado,
													IdUsuarioAutoriza:IdUsuario,
													FechaAutoriza:FechaIngreso
												}
											});											
											}			
								
								</script>
								
								
								
								<script type="text/javascript">
											
											function Modificar_Exoneraciones_Facturacion_Servicio_Financiamiento(IdOrden,IdProducto,CantidadFinanciada,TotalFinanciado,PrecioFinanciado)											
											{
											var IdUsuario = $("#IdUsuario").val();
											var FechaIngreso = $("#FechaIngreso").val();
											
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Modificar_Exoneraciones_Facturacion_Servicio_Financiamiento',
												type: 'POST',
												dataType: 'json',
												data: 
												{
													IdOrden: IdOrden,
													IdProducto: IdProducto,
													CantidadFinanciada: CantidadFinanciada,
													IdTipoFinanciamiento: '9',
													IdFuenteFinanciamiento: '0',
													IdEstadoFacturacion: '1',
													TotalFinanciado: TotalFinanciado,
													PrecioFinanciado: PrecioFinanciado,
													IdUsuarioAutoriza:IdUsuario,
													FechaAutoriza:FechaIngreso
												}
											});											
											}			
								
								</script>
								
								
								
								<script type="text/javascript">
								
											function Generar_Descuento_FactOrdenServiciosPagos()
											{
											// En la Variable Filas Se Almacena el Arreglo	
											var filas = $('#consumo_servicios_cuenta').datagrid('getRows');	
											var unique = [];
												for(var i=0; i<filas.length; i++)
												{
												    if(unique.indexOf(filas[i].IdOrdenPago) === -1)
													{
														unique.push(filas[i].IdOrdenPago);        
													} 
												
												}
												
												for(i = 0; i< unique.length; i++)
												{
													
													var Total=0;
													var IdOrden=0;
													var IdUsuario=0;	
													var Rows = $('#consumo_servicios_cuenta').datagrid('getRows');			
													for(var j=0; j<Rows.length; j++)
													{
														if(Rows[j].IdOrdenPago==unique[i])
														{
														Total=parseFloat(Total)+parseFloat(Rows[j].ImporteExonerado); 
														IdOrden=Rows[j].IdOrden;
														IdUsuario=Rows[j].IdUsuario;	
														IdEstadoFacturacion=Rows[j].IdEstadoFacturacion;															
														} 
													}

													if(IdEstadoFacturacion==1)
													{
													Exoneraciones_Modificar_FactOrdenServicioPagos(Total,unique[i],IdOrden,IdUsuario);	
													}
												}
											}

											
								
								</script>
								
																
								<script type="text/javascript">
											
											function Exoneraciones_Modificar_FactOrdenServicioPagos(ImporteExonerado,IdOrdenPago,IdOrden,IdUsuario)											
											{
												
											var idUsuarioExonera = $("#IdUsuario").val();
											var FechaIngreso = $("#FechaIngreso").val();
											var ImporteExonerado = ImporteExonerado.toFixed(2);
										
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Exoneraciones_Modificar_FactOrdenServicioPagos',
												type: 'POST',
												dataType: 'json',
												data: 
												{				
													ImporteExonerado: ImporteExonerado,
													FechaCreacion: FechaIngreso,
													IdEstadoFacturacion:'1',
													idUsuarioExonera: idUsuarioExonera,
													IdOrdenPago: IdOrdenPago,
													IdOrden: IdOrden,
													IdUsuario:IdUsuario
													
												}
											});
											
											}			
								
								</script>
								
								
								
								<script type="text/javascript">
								
											function Generar_Descuento_FactOrdenesBienes()
											{
											// En la Variable Filas Se Almacena el Arreglo	
											var filas = $('#consumo_medicamentos_cuenta').datagrid('getRows');	
											var unique = [];
												for(var i=0; i<filas.length; i++)
												{
												    if(unique.indexOf(filas[i].IDORDEN) === -1)
													{
														unique.push(filas[i].IDORDEN);        
													} 
												
												}

												for(i = 0; i< unique.length; i++)
												{
													
													var Total=0;
													var IdOrden=0;
													var IdUsuario=0;	
													var Rows = $('#consumo_medicamentos_cuenta').datagrid('getRows');			
													for(var j=0; j<Rows.length; j++)
													{
														if(Rows[j].IDORDEN==unique[i])
														{
														IdOrden=Rows[j].IDORDEN;
														IdPaciente=Rows[j].IDPACIENTE;
														IdCuentaAtencion=Rows[j].IDCUENTAATENCION;	
														MovNumero=Rows[j].MOVNUMERO;
														IdUsuario=Rows[j].IDUSUARIO;
														Total=parseFloat(Total)+parseFloat(Rows[j].IMPORTEEXONERADO); 
														IDESTADOFACTURACION=Rows[j].IDESTADOFACTURACION;														
														} 
													}
													
													if(IDESTADOFACTURACION == 1)
													{
													Exoneraciones_Modificar_FactOrdenesBienes(IdOrden,IdPaciente,IdCuentaAtencion,MovNumero,IdUsuario,Total);		
													}
												}
												
	
											
											}

											
								
								</script>
								
								
								<script type="text/javascript">
											
											function Exoneraciones_Modificar_FactOrdenesBienes(IdOrden,IdPaciente,IdCuentaAtencion,MovNumero,IdUsuario,ImporteExonerado)											
											{
											var IdUsuarioExonera = $("#IdUsuario").val();
											var FechaIngreso = $("#FechaIngreso").val();
											var ImporteExonerado = ImporteExonerado.toFixed(2);
										
											$.ajax({
												url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Modificar_Exoneraciones_FactOrdenes_Bienes',
												type: 'POST',
												dataType: 'json',
												data: 
												{				
													IdOrden: IdOrden,
													IdPaciente: IdPaciente,
													IdCuentaAtencion: IdCuentaAtencion,
													MovNumero: MovNumero,
													FechaCreacion: FechaIngreso,
													IdUsuario: IdUsuario,
													ImporteExonerado: ImporteExonerado,
													idUsuarioExonera: IdUsuarioExonera
												}
											});
											}			
								
								</script>
								
								
</head>
<body>
		<!-- CONTENEDORES -->
		<div class="easyui-layout" data-options="fit:true">

		
						<div id="Contenedor_Exoneraciones" class="easyui-layout" style="width:100%;height:100%;">
						
						
						
										<!--	Cuadro de Arriba : Muestra Datos por Paciente  -->
										<div data-options="region:'north',title:'Datos del Paciente',split:true" style="height:18%;">
																<div style="float: left;border:1px; margin-left:15px">
																						<CENTER>						
																						<table>
																						<tr>
																							<td>
																							<center><div class="ExoneracionLetra" style="color: red; font-size:35px ; font-weight:bold;"></div></center>
																							</td>
																						<tr>	
																						</tr>
																							<td>
																							<div id="porcentaje" style="width:80px;"></div>
																							</td>
																						</tr>
																						</table>
																						</CENTER>												
																</div>		
																<div style="float: left;border:1px;padding:1%;">
																<table cellpadding="5">
																	<tr>
																		<td style="color: Teal;">
																			<strong>Historia Clinica:</strong>   
																		</td>
																		<td >
																			<input type="text" class="easyui-textbox" data-options="prompt:'HISTORIA CLINICA'"  id="HistoriaClinica" autofocus="autofocus">   
																		</td>
																		<td>
																			<a href="#" class="easyui-linkbutton"  style="width:80px;height: 25px;" iconCls="icon-search" onclick="Cargar_Datos_por_Boton()">Buscar</a>
																			<input type="hidden" id="cuenta">
																		</td>
																		<td>
																			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		</td>								
																	</tr>
																	<tr>
																		<td style="color: Teal;">
																			<strong>Ficha Social N°:</strong>   
																		</td>
																		<td>
																			<input type="text" disabled class="easyui-textbox" data-options="prompt:'N° FICHA SOCIAL'" style="color: red;" id="numichasocial">    									
																		</td>	
																		<td colspan="2">
																			<input type="hidden" id="idpaciente">
																			<input type="text" class="easyui-textbox" style="width: 100%;" data-options="prompt:'PACIENTE'" id="PACIENTE" disabled>
																		</td> 										
																	</tr>
																</table>
																</div>
																<div id="cargaexterna" style="float: left;padding:1%; margin-left:10px;padding-right:20px;color:#336600">
																</div>																	
										</div>
											

											
											
										<!--	Cuadro de la Derecha : Lista de Cuentas por Paciente  -->
										<div data-options="region:'west',title:'Datos Adicionales',split:true" style="width:25%;height:70%">
										
										    <div style="margin:10px; float:left">
												<span>Seleccionar Modo : </span>
												<select onchange="$('#lista_cuentas_paciente').datagrid({singleSelect:(this.value==0)})"  id="tipo_lista_Cuenta">
													<option value="0">Una Cuenta</option>
													<option value="1">Multiples Cuentas</option>
												</select>
											</div>
											
											<div style="clear:both">											
											<!-- Muestra la lista de Cuentas por IdPaciente  -->
											<table id="lista_cuentas_paciente"></table>
											</div>
										</div>
										
										
										
										
										<!-- Cuadro Principal: Muestra todo lo cargado a la Cuenta -->
										<div data-options="region:'center',title:'Exoneracion'" style="width:75%;height:55%;">	
											<!-- Tabs que muestra lo consumido por el Paciente -->
											<div id="Tab_consumos" class="easyui-tabs">
											
											
																			
													
													<!-- Tab que muestra lo consumido en Servicios -->
													<div title="Servicios">
													<br>
													<span style="color:blue;font-weight:bold; padding:10px;">Listado de Servicios que tiene cargado el Paciente</span>
													<br>
													<br>
													<!-- Carga la tabla todo lo consumido en Servicios -->
													<table id="consumo_servicios_cuenta"></table>
													</div>
													
													
													
													<!-- Tab que muestra lo consumido en Farmacia -->
													<div title="Medicamentos" >
													<br>
													<span style="color:blue;font-weight:bold; padding:10px;">Listado de Medicamentos que tiene cargado el Paciente</span>
													<br>
													<br>
													<!-- Carga la tabla todo lo consumido de Medicamentos-->
													<table id="consumo_medicamentos_cuenta"></table>
													</div>



													<!-- Tab que muestra Grafico -->
													<div title="Consolidado" >
													<div id="container" style="min-width: 500px; height: 380px; margin: 10 auto;margin-top:10px;margin-left:10px"></div>
													</div>	
													
													
													
											
											</div>
										</div>
										
										

										
										
									
										
										<!-- Cuadro de Pie de Pagina  -->
										<div data-options="region:'south',title:'.',split:true" style="height:22%;">
										
										
										
													<div style="float: left;padding:1%;margin:20px;">
														<table cellpadding="5">
															<tr>
																<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:96px;height: 50px;"  onclick="Generar()" id="GuardarExoneracion">GUARDAR</a></td>
																<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:150px;height: 50px;"  onclick="Generar_Resumen()" id="Generar_Resumen">Estado de Cuenta por Serv /Hospitalizacion</a></td>
																<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:150px;height: 50px;"  onclick="Generar_Descuento_FactOrdenesBienes()" id="GenerarComprobante">Imprimir</a></td>
																<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;"   onclick="Generar_Exoneraciones_Medicamentos()" id="SalirExoneracion">SALIR</a></td>
															</tr>
														</table>
													</div>	
													
		
													
													
													
													<!-- Contenedor que Muestra la Suma de Servicios  -->
													<div  id="Suma_Servicios"  style="float: right;border:1px dotted #999;padding:1%; margin:10px;padding-right:20px">
														<table cellpadding="5">
															<tr>
															  <td width="100"><strong style="color: black;">Total Servicios :</strong></td>
															  <td width="12" style="width: 90px;">
															  <div class="Total_Servicios" style="color: black; font-size:14px ; font-weight:bold" >0.00</div>
															  <input type="hidden" style="color: blue;" id="Total_Servicios" >
															  </td>
															  <td width="87">
																	<strong style="color: blue;">
																		Total por Exonerar :
																	</strong>
															  </td>
															  <td width="50" style="width: 50px;">
																	<div class="Total_por_Exonerar_Servicios" style="color: blue; font-size:14px ; font-weight:bold" >0.00</div>
																	<input type="hidden" style="color: blue;" id="Total_por_Exonerar_Servicios" >
															  </td>
															</tr>
															<tr>
															  
															  <td width="100">
																<!--
																	  <strong style="color:#9400D3;">
																	  Total Exonerado :
																	  </strong>
																 -->	  
															  </td>
															 
															  <td width="12" style="width: 70px;">
																<!--
																	  <div class="TotalExonerar_Servicios" style="color:#9400D3; font-size:14px ; font-weight:bold" >0.00</div>
																	  <input type="hidden" style="color: blue;" id="TotalExonerar_Servicios" >
																 -->	  
															  </td>
																<td>		
																	<strong style="color: red;">
																		Total a Pagar :
																	</strong>	
																</td>
																<td width="100"  style="width: 100px;">
																	<div class="Total_Por_Pagar_Servicios" style="color: red; font-size:14px ; font-weight:bold" >0.00</div>
																	<input type="hidden" style="color: blue;" id="Total_Por_Pagar_Servicios" >
																</td>
															</tr>
														</table>
													</div>
													
													
													
													
													<!-- Contenedor que Muestra la Suma de Medicamentos  -->
													<div id="Suma_Medicamentos" style="float: right;border:1px dotted #999;padding:1%; margin:10px;padding-right:20px;">
														<table cellpadding="5">
															<tr>
															  <td width="100"><strong style="color: black;">Total Medicamentos :</strong></td>
															  <td width="12" style="width: 70px;">
															   <div class="Total_Medicamentos" style="color: black; font-size:14px ; font-weight:bold" >0.00</div>
															   <input type="hidden" style="color: blue;" id="Total_Medicamentos" >
															  </td>
																<td width="87">
																	<strong style="color: blue;">
																		Total Exonerado :
																	</strong>
																</td>
																<td width="50" style="width: 50px;">
																	<div class="TotalExonerar_Medicamentos" style="color: blue; font-size:14px ; font-weight:bold" >0.00</div>
																	<input type="hidden" style="color: blue;" id="TotalExonerar_Medicamentos" >
																</td>
															</tr>
															<tr>
															  <td>&nbsp;</td>
															  <td>&nbsp;</td>
																<td>		
																	<strong style="color: red;">
																		Total a Pagar :
																	</strong>	
																</td>
																<td width="100"  style="width: 100px;">
																	<div class="Total_Por_Pagar_Medicamentos" style="color: red; font-size:14px ; font-weight:bold" >0.00</div>
																	<input type="hidden" style="color: blue;" id="Total_Por_Pagar_Medicamentos" >
																</td>
															</tr>
														</table>
													</div>
													
													
										

																										
													
													
													<!--
													<div  id="Pago_a_cuenta_Servicios_y_Farmacia"  style="float: right;border:1px dotted #999;padding:1%; margin:10px;padding-right:20px">
														<table cellpadding="5">
															<tr>
															  <td width="100"><strong style="color: #088A29;">Pagos a Cuenta Servicios:</strong></td>
															  <td width="12" style="width: 65px;">
															  <div class="Pago_Cuenta_Servicios" style="color: #088A29; font-size:14px ; font-weight:bold" >0.00</div>
															  <input type="hidden" style="color: blue;" id="Pago_Cuenta_Servicios" >
															  </td>
															</tr>
															<tr>
															  <td width="100"><strong style="color: #088A29;">Pagos a Cuenta:</strong></td>
															  <td width="12" style="width: 65px;">
															  <div class="Pago_Cuenta" style="color: #088A29; font-size:14px ; font-weight:bold" >0.00</div>
															  <input type="hidden" style="color: blue;" id="Pago_Cuenta" >
															  </td>
															</tr>	
														</table>
													</div>
													-->
													
													
													
													
													<!-- Fin de Contenedor que Muestra el Pago a Cuenta de Servicios y Farmacia por Separado -->
													
													
													<!-- Contenedor que Muestra el Pago a Cuenta  -->
													<!--
													<div  id="Pago_a_cuenta"  style="float: right;border:1px dotted #999;padding:1%; margin:10px;padding-right:20px">
														<table cellpadding="5">
															<tr>
															  <td width="100"><strong style="color: #088A29;">Pagos a Cuenta :</strong></td>
															  <td width="12" style="width: 90px;">
															  <div class="Pago_Cuenta" style="color: #088A29; font-size:14px ; font-weight:bold" >0.00</div>
															  <input type="hidden" style="color: blue;" id="Pago_Cuenta" >
															  </td>
															</tr>
														</table>
													</div>
													-->
													
													
													
										</div>
																			
														
										<!-- Cuadro Izquierda -->
										<!--
										<div data-options="region:'east',title:'Categoria',split:true" style="width:100px;">
										</div>
										-->

						</div>
		</div>
												<!-- IdUsuario de la Asistenta Social -->
												<input type="hidden" id="IdUsuario" value="<?php echo $_REQUEST['IdEmpleado']?>">
												
												
												<!-- Fecha de Ingreso cuando Exonero la Asistenta Social -->
												<input type="hidden" id="FechaIngreso" value="<?php echo date('Y-m-d H:i:s')?>">
</body>

	
</html>