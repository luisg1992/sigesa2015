<?php 
	date_default_timezone_set('America/Lima');
	include_once('../../MVC_Modelo/Servicio_SocialM.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="Rodolfo Esteban Crisanto Rosas" name="author" />
	<title>Ficha Social</title>
	<!-- CSS -->
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
	<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
	<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		
    </style>
	<!-- JS -->
	<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/datagrid-cellediting.js"></script>
	<script src="../../MVC_Complemento/tinymce/tinymce.min.js"></script>
	<script src="../../MVC_Complemento/tinymce/langs/es.js"></script>
	<script>tinymce.init({ selector:'textarea' ,
							   language : 'es', 
							   menubar: false,
							   plugins: [
								'advlist autolink lists  charmap print preview anchor textcolor',
								'searchreplace visualblocks code fullscreen',
								'insertdatetime media table contextmenu paste code help'],
							   branding: false});
	</script>
	<script>
	function getRowIndex(target){
			var tr = $(target).closest('tr.datagrid-row');
			return parseInt(tr.attr('datagrid-row-index'));       
		}
		function editrow(target){
			$('#tt').datagrid('beginEdit', getRowIndex(target));
		}
		function deleterow(target){
			$.messager.confirm('Confirm','Are you sure?',function(r){
				if (r){
					$('#tt').datagrid('deleteRow', getRowIndex(target));
				}
			});
		}
		function saverow(target){
			$('#tt').datagrid('endEdit', getRowIndex(target));
		}
		function cancelrow(target){
			$('#tt').datagrid('cancelEdit', getRowIndex(target));
		}
		function insert(){
			var row = $('#tt').datagrid('getSelected');
			if (row){
				var index = $('#tt').datagrid('getRowIndex', row);
			} else {
				index = 0;
			}
			$('#tt').datagrid('insertRow', {
				index: index,
				row:{
					status:'P'
				}
			});
			$('#tt').datagrid('selectRow',index);
			$('#tt').datagrid('beginEdit',index);
		}
		
		function getSelected()
			{
            var row = $('#tt').datagrid('getSelected');
					if (row)
					{
						$.messager.confirm('Confirmacion','Estas Seguro que quieres eliminar Registro?',function(r){
							if (r)
							{
							$.ajax({
							url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=Eliminar_Registros_Familiar_C',
							type: 'POST',
							dataType: 'json',
							data: 
								{
									IdFamiliar: row.IDFAMILIAR
								},
							success: function(data)
							{
								if (data == 'M_1') {
									$('#tt').datagrid('reload'); 	
								}
							}
							});
							$('#tt').datagrid('reload'); 
							}	
						});	
					}				
			}
		
	
	</script>
	
	<script>
		$(document).ready(function() {
		$('#w').window('close');
		function Mostrar_Familiares_IdPaciente()
		{
		var EstadoCivil = [
		    {EstadoCivilId:'1',Descripcion:'Casado'},
		    {EstadoCivilId:'2',Descripcion:'Soltero'},
		    {EstadoCivilId:'3',Descripcion:'Conviviente'},
		    {EstadoCivilId:'4',Descripcion:'Viudo'},
		    {EstadoCivilId:'5',Descripcion:'Divorciado'},
		    {EstadoCivilId:'10',Descripcion:'Madre Soltera'},
		    {EstadoCivilId:'11',Descripcion:'Separado'}
		];	


		var Sexo = [
		    {IDSEXO:'1',SEXO:'M'},
		    {IDSEXO:'2',SEXO:'F'}
		];	
	
	    var Instruccion = [
		    {IDGRADOINS:'11',GRADO:'Sin Instruccion'},
		    {IDGRADOINS:'2',GRADO:'Primaria Completa'},
		    {IDGRADOINS:'3',GRADO:'Primaria Incompleta'},
		    {IDGRADOINS:'4',GRADO:'Secundaria Completa'},
		    {IDGRADOINS:'5',GRADO:'Secundaria Incompleta'},
		    {IDGRADOINS:'6',GRADO:'Superior Universitaria'},
		    {IDGRADOINS:'8',GRADO:'Superior No Universitaria'}
		];	
			
		var Parentesco = [
			{IdFamiliar:'2',Descripcion:'MADRE'},
		    {IdFamiliar:'1',Descripcion:'PADRE'},
			{IdFamiliar:'3',Descripcion:'ESPOSO'},
		    {IdFamiliar:'4',Descripcion:'ESPOSA'},
			{IdFamiliar:'5',Descripcion:'HIJO'},
			{IdFamiliar:'14',Descripcion:'HERMANO'},
			{IdFamiliar:'15',Descripcion:'HERMANA'},
			{IdFamiliar:'18',Descripcion:'CONVIVIENTE'},
		    {IdFamiliar:'19',Descripcion:'PACIENTE'},
		    {IdFamiliar:'30',Descripcion:'OTRO'}
		];	
		
	    var TiposEdad = [
		    {IDTIPOEDAD:'1',Descripcion:'Años'},
		    {IDTIPOEDAD:'2',Descripcion:'Mes'},
		    {IDTIPOEDAD:'3',Descripcion:'Dias'},
		    {IDTIPOEDAD:'4',Descripcion:'Horas'}
		];	
		var idpaciente =   $("#idpaciente").text();	
		$(function(){
			$('#tt').datagrid({
				title:'Familiares',
				iconCls:'icon-edit',
				height:450,
				singleSelect:true,
				idField:'DNI',
				url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarFamiliaresGrilla&idpaciente='+idpaciente,
				columns:[[
					{field:'IDFAMILIAR',title:'IDFAMILIAR',width:50,hidden:true
					},
					{field:'FAMILIAR',title:'NOMBRES',width:180,
							editor:{
							type:'validatebox',
							options:{
							required:true
									}
							}
					},
					{field:'ESTADO CIVIL',title:'ESTADO CIVIL',width:90,
						formatter:function(value,row){
							return row.EstadoCivil || value;
						},
						editor:{
							type:'combobox',
							options:{
								valueField:'EstadoCivilId',
								textField:'Descripcion',
								data:EstadoCivil,
								required:true
							}
						}
					},
					{field:'INSTRUCCION',title:'INSTRUCCION',width:180,
						formatter:function(value,row){
							return row.Instruccion || value;
						},
						editor:{
							type:'combobox',
							options:{
								valueField:'IDGRADOINS',
								textField:'GRADO',
								data:Instruccion,
								required:true
							}
						}
					},
					{field:'EDAD',title:'EDAD',width:40,editor:'numberbox'},				
					{field:'TIPOEDAD',title:'UNIDAD',width:60,
						formatter:function(value,row){
							return row.TipoEdad || value;
						},
						editor:{
							type:'combobox',
							options:{
								valueField:'IDTIPOEDAD',
								textField:'Descripcion',
								data:TiposEdad,
								required:true
							}
						}
					},
					{field:'SEXO',title:'SEXO',width:40,
						formatter:function(value,row){
							return row.Sexo || value;
						},
						editor:{
							type:'combobox',
							options:{
								valueField:'IDSEXO',
								textField:'SEXO',
								data:Sexo,
								required:true
							}
						}
					},
					{field:'PARENTESCO',title:'PARENTESCO',width:100,
						formatter:function(value,row){
							return row.Parentesco || value;
						},
						editor:{
							type:'combobox',
							options:{
								valueField:'IdFamiliar',
								textField:'Descripcion',
								data:Parentesco,
								required:true
							}
						}
					},	
					{field:'OCUPACION',title:'OCUPACION',width:180,
							editor:{
							type:'validatebox',
							options:{
							required:true
									}
							}
					},			
					{field:'action',title:'ACCION',width:115,align:'center',
						formatter:function(value,row,index){
							if (row.editing){
								var s = '<a href="javascript:void(0)" onclick="saverow(this)">Guardar</a> ';
								var c = '<a href="javascript:void(0)" onclick="cancelrow(this)">Cancelar</a>';
								return s+c;
							} 
						}
					}
					
				]],
				onEndEdit:function(index,row){
					var ed_sexo = $(this).datagrid('getEditor', {
						index: index,
						field: 'SEXO'
					});
					row.Sexo = $(ed_sexo.target).combobox('getText');
					
					var ed_parentesco = $(this).datagrid('getEditor', {
						index: index,
						field: 'PARENTESCO'
					});
					row.Parentesco = $(ed_parentesco.target).combobox('getText');
					
					var ed_instruccion = $(this).datagrid('getEditor', {
						index: index,
						field: 'INSTRUCCION'
					});
					row.Instruccion = $(ed_instruccion.target).combobox('getText');
					
					var ed_EstadoCivil = $(this).datagrid('getEditor', {
						index: index,
						field: 'ESTADO CIVIL'
					});
					row.EstadoCivil = $(ed_EstadoCivil.target).combobox('getText');
					
					var ed_TipoEdad = $(this).datagrid('getEditor', {
						index: index,
						field: 'TIPOEDAD'
					});
					row.TipoEdad = $(ed_TipoEdad.target).combobox('getText');
				   /*Insertar los Valores  */
					numerdodnifamiliar = row.DNI;
					var apaternofamiliar 		= 	row.FAMILIAR;
					var amaternofamiliar		= 	'';
					var nombresfamiliar 		= 	'';
					var estadocivilfamiliar 	= 	$(ed_EstadoCivil.target).combobox('getValue');
					var gradoistruciionfamiliar = 	$(ed_instruccion.target).combobox('getValue');
					var edadfamiliar 			= 	row.EDAD;
					var sexofamiliar 			= 	$(ed_sexo.target).combobox('getValue');
					var parentescofamiliar 		= 	$(ed_parentesco.target).combobox('getValue');
				    var tipoedad        		= 	$(ed_TipoEdad.target).combobox('getValue');
					var ocupacionfamiliar 		= 	row.OCUPACION;
					var direccionfamiliar		=   " ";
					var idpaciente 				=   $("#idpaciente").text();
					
					$.ajax({
						url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=IngresarNuevoFamiliaresD',
						type: 'POST',
						dataType: 'json',
						data: 
							{
								numerdodnifamiliar: numerdodnifamiliar,
								apaternofamiliar: apaternofamiliar,
								amaternofamiliar: amaternofamiliar,
								nombresfamiliar: nombresfamiliar,
								estadocivilfamiliar: estadocivilfamiliar,
								gradoistruciionfamiliar: gradoistruciionfamiliar,
								edadfamiliar: edadfamiliar,
								sexofamiliar: sexofamiliar,
								parentescofamiliar: parentescofamiliar,
								ocupacionfamiliar: ocupacionfamiliar,
								tipoedad: tipoedad,
								direccionfamiliar: direccionfamiliar,
								idpaciente: idpaciente
							},
						success: function(data)
						{
							if (data == 'M_1') {
								$("#numerdodnifamiliar").textbox('clear');
								$("#apaternofamiliar").textbox('clear');
								$("#amaternofamiliar").textbox('clear');
								$("#nombresfamiliar").textbox('clear');
								$("#estadocivilfamiliar").combobox('clear');
								$("#gradoistruciionfamiliar").combobox('clear');
								$("#sexofamiliar").combobox('clear');
								$("#parentescofamiliar").combobox('clear');
								$("#ocupacionfamiliar").combobox('clear');
								$("#edadfamiliar").textbox('clear');
								$("#idpaciente").val(' ');
								$("#grillaFamiliares").datagrid('reload');
								$("#grillaFamiliares_mostrar").datagrid('reload');
							    $.messager.alert('SIGESA','Familiar Ingresado Satisfactoriamente');
								
								
							}
						}
					});
				},
				onBeforeEdit:function(index,row){
					row.editing = true;
					$(this).datagrid('refreshRow', index);
				},
				onAfterEdit:function(index,row){
					row.editing = false;
					$(this).datagrid('refreshRow', index);
				},
				onCancelEdit:function(index,row){
					row.editing = false;
					$(this).datagrid('refreshRow', index);
				}
			});
			

		});	
		}
		
		

			//CONFIGURACIONES INICIALES
			// OCULTAR DIV DE FICHA SOCIAL
			$("#ficha_social").hide();
			// OCULTAR TABLA PARA INGRESO DE COMPOSICION FAMILIAR
			$("#AgregarPariente").linkbutton('disable');
			// OCULTAR CHECKBOX DE PROBLEMAS SOCIALES
			$("input[name=problemas_sociales_check_0]").prop('disabled', true);
			$("input[name=problemas_sociales_check_1]").prop('disabled', true);
			$("input[name=problemas_sociales_check_2]").prop('disabled', true);
			$("input[name=problemas_sociales_check_3]").prop('disabled', true);
			$("input[name=problemas_sociales_check_4]").prop('disabled', true);
			$("input[name=problemas_sociales_check_5]").prop('disabled', true);
			$("input[name=problemas_sociales_check_6]").prop('disabled', true);
			$("input[name=problemas_sociales_check_7]").prop('disabled', true);
			$("input[name=problemas_sociales_check_8]").prop('disabled', true);
			$("input[name=problemas_sociales_check_9]").prop('disabled', true);
			$("input[name=problemas_sociales_check_10]").prop('disabled', true);
			// OCULTAR CHECKBOX DE PROBLEMAS DE SALU
			$("input[name=salud_familiar_check_0]").prop('disabled', true);
			$("input[name=salud_familiar_check_1]").prop('disabled', true);
			$("input[name=salud_familiar_check_2]").prop('disabled', true);
			$("input[name=salud_familiar_check_3]").prop('disabled', true);
			$("input[name=salud_familiar_check_4]").prop('disabled', true);
			$("input[name=salud_familiar_check_5]").prop('disabled', true);
			$("input[name=salud_familiar_check_6]").prop('disabled', true);

			$("#NuevoFamiliar").window('close');

			
			//MOSTRANDO FICHA SOCIAL
			$("#MostrarFichaNueva").click(function(event) {
				$("#ficha_social").fadeOut();
				$("#ficha_social").fadeIn();
				$("input[name=sexo]").removeAttr('checked');
				//Limpiar Cuadro de TextArea
				tinymce.get('diagnosticosocial_V2').setContent('');
				tinymce.get('tratamientosocial_V2').setContent('');
				$("input[type=checkbox], input[type=radio]").prop("checked", false);
				//Limpiar Cuadro de Parientes
				$('#tt').datagrid('loadData', []); 
				//Limpiar Input
				$("#otrossaludfamiliar").textbox('clear');
				$("#nombres_asistente").textbox('clear');
				$("#otrosproblemassociales").textbox('clear');
			    $("#numichasocial").textbox('clear');
			    $("#Numerocuenta").textbox('clear');
				$("#nombrepaciente").textbox('clear');
				$("#tipopaciente").textbox('clear');
				$("#tipoatencion").textbox('clear');
				$("#dnipaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#telefonopaciente_I").textbox('clear');
				$("#telefonopaciente_II").textbox('clear');
				$("#edadpaciente").textbox('clear');
				$("#seguro").textbox('clear');
				$("#puntaje_total").textbox('clear');
				$("#marca_puntaje_total").val(' ');
				$("#calificacion").textbox('clear');
				$("#distritosPacientesProcedencias").combobox('clear');
				$("#departamentoPacienteProcedencia").combobox('clear');
				$("#provinciasPacienteProcedencia").combobox('clear');
				$("#direccionPacienteProcedencia").textbox('clear');		
				$("#departamentoPaciente").textbox('clear');
				$("#provinciaPaciente").textbox('clear');
				$("#distritoPaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#personaresponsable_paterno").textbox('clear');
				$("#personaresponsable_materno").textbox('clear');
				$("#personaresponsable_nombres").textbox('clear');
				$("#parentesco_pr").combobox('clear');
				$("#direccion_pr").textbox('clear');
				$("#edadpersonares").textbox('clear');
				$("#numerodedormitorios").textbox('clear');
				$("#numerodemiembros").textbox('clear');
				$("#diagnosticosocial").textbox('clear');
				$("#tratamientosocial").textbox('clear');
				$("#diagnosticomedico").textbox('clear');
				$("#nombres_asistente").textbox('clear');
        		$('input[name=grado_instruccion]').prop('checked', false);
	        	$('input[name=composicion_familiarr]').prop('checked', false);
	        	$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=hacinamiento]').prop('checked', false);
				$('input[name=situacion_vivienda]').prop('checked', false);
				$('input[name=condicion_trabajo_pr]').prop('checked', false);
				$('input[name=ocupacion_pr]').prop('checked', false);
				$('input[name=grado_instruccion_pr]').prop('checked', false);
				$('input[name=estado_civil_pr]').prop('checked', false);
				$('input[name=condicion_trabajo]').prop('checked', false);
				$('input[name=rango_edad]').prop('checked', false);
				$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=material_contruccion]').prop('checked', false);
				$('input[name=ingreso_economico]').prop('checked', false);
				$('input[name=condicion_ingreso]').prop('checked', false);
				$('input[name=lugar_procedencia]').prop('checked', false);
				$('input[name=ocupacion]').prop('checked', false);
				$('input[name=estado_civil]').prop('checked', false);
				$('input[name=sexo]').prop('checked', false);
				$('input[name=tipo]').prop('checked', false);
				$('input[name=problemas_sociales]').prop('checked', false);
				$('input[name=salud_familiar]').prop('checked', false);
				$("#numerodemiembros").textbox('clear');
				$("#numerodedormitorios").textbox('clear');

				return false;
			});

			$("#ModificarFicha").click(function(event) {
				$("#ficha_social").fadeOut();
				$("#ficha_social").fadeIn();
				$("input[name=sexo]").removeAttr('checked');
				//Limpiar Cuadro de TextArea
				tinymce.get('diagnosticosocial_V2').setContent('');
				tinymce.get('tratamientosocial_V2').setContent('');
				$("input[type=checkbox], input[type=radio]").prop("checked", false);
				//Limpiar Cuadro de Parientes
				$('#tt').datagrid('loadData', []); 
				//Limpiar Input
				$("#otrossaludfamiliar").textbox('clear');
				$("#nombres_asistente").textbox('clear');
				$("#otrosproblemassociales").textbox('clear');
			    $("#numichasocial").textbox('clear');
			    $("#Numerocuenta").textbox('clear');
				$("#nombrepaciente").textbox('clear');
				$("#tipopaciente").textbox('clear');
				$("#tipoatencion").textbox('clear');
				$("#dnipaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#telefonopaciente_I").textbox('clear');
				$("#telefonopaciente_II").textbox('clear');
				$("#edadpaciente").textbox('clear');
				$("#seguro").textbox('clear');
				$("#puntaje_total").textbox('clear');
				$("#marca_puntaje_total").val(' ');
				$("#calificacion").textbox('clear');
				$("#distritosPacientesProcedencias").combobox('clear');
				$("#departamentoPacienteProcedencia").combobox('clear');
				$("#provinciasPacienteProcedencia").combobox('clear');
				$("#direccionPacienteProcedencia").textbox('clear');		
				$("#departamentoPaciente").textbox('clear');
				$("#provinciaPaciente").textbox('clear');
				$("#distritoPaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#personaresponsable_paterno").textbox('clear');
				$("#personaresponsable_materno").textbox('clear');
				$("#personaresponsable_nombres").textbox('clear');
				$("#parentesco_pr").combobox('clear');
				$("#direccion_pr").textbox('clear');
				$("#edadpersonares").textbox('clear');
				$("#numerodedormitorios").textbox('clear');
				$("#numerodemiembros").textbox('clear');
				$("#diagnosticosocial").textbox('clear');
				$("#tratamientosocial").textbox('clear');
				$("#diagnosticomedico").textbox('clear');
				$("#nombres_asistente").textbox('clear');
        		$('input[name=grado_instruccion]').prop('checked', false);
	        	$('input[name=composicion_familiarr]').prop('checked', false);
	        	$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=hacinamiento]').prop('checked', false);
				$('input[name=situacion_vivienda]').prop('checked', false);
				$('input[name=condicion_trabajo_pr]').prop('checked', false);
				$('input[name=ocupacion_pr]').prop('checked', false);
				$('input[name=grado_instruccion_pr]').prop('checked', false);
				$('input[name=estado_civil_pr]').prop('checked', false);
				$('input[name=condicion_trabajo]').prop('checked', false);
				$('input[name=rango_edad]').prop('checked', false);
				$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=material_contruccion]').prop('checked', false);
				$('input[name=ingreso_economico]').prop('checked', false);
				$('input[name=condicion_ingreso]').prop('checked', false);
				$('input[name=lugar_procedencia]').prop('checked', false);
				$('input[name=ocupacion]').prop('checked', false);
				$('input[name=estado_civil]').prop('checked', false);
				$('input[name=sexo]').prop('checked', false);
				$('input[name=tipo]').prop('checked', false);
				$('input[name=problemas_sociales]').prop('checked', false);
				$('input[name=salud_familiar]').prop('checked', false);
				$("#numerodemiembros").textbox('clear');
				$("#numerodedormitorios").textbox('clear');

				return false;
			});

			//OCULTANDO FICHA SOCIAL
			$("#SalirFichaSocial").click(function(event) {
				$("#ficha_social").hide();
				$("input[type=checkbox], input[type=radio]").not("input[name=sexo]").prop("checked", false);				
				return false;
			});

			// SALIR DE FICHA SOCIAL
			$("#RegresarFichaSocial").click(function(event) {
				$("#ficha_social").hide();
				$("input[type=checkbox], input[type=radio]").not("input[name=sexo]").prop("checked", false);				
				return false;
			});

			//FUNCION PARA SUMAR LOS INPUT'S RADIOS QUE SE BAN SELECCIONANDO
			function SumarRadios()
			{	
				var puntaje_total = 0;
				var tipo							= $("input[name=tipo]:checked").attr('value');
				var rango_edad						= $("input[name=rango_edad]:checked").attr('value');
				var estado_civil 					= $("input[name=estado_civil]:checked").attr('value');
				var lugar_procedencia 				= $("input[name=lugar_procedencia]:checked").attr('value');
				var grado_instruccion 				= $("input[name=grado_instruccion]:checked").attr('value');
				var ocupacion 						= $("input[name=ocupacion]:checked").attr('value');
				var condicion_trabajo 				= $("input[name=condicion_trabajo]:checked").attr('value');
				var situacion_vivienda 				= $("input[name=situacion_vivienda]:checked").attr('value');
				var hacinamiento 					= $("input[name=hacinamiento]:checked").attr('value');
				var personas_x_dormitorio 			= $("input[name=personas_x_dormitorio]:checked").attr('value');
				var material_contruccion 			= $("input[name=material_contruccion]:checked").attr('value');
				var ingreso_economico 				= $("input[name=ingreso_economico]:checked").attr('value');
				var condicion_ingreso 				= $("input[name=condicion_ingreso]:checked").attr('value');
				var composicion_familiarr 			= $("input[name=composicion_familiarr]:checked").attr('value');
				var problemas_sociales 				= $("input[name=problemas_sociales]:checked").attr('value');
				var salud_familiar 					= $("input[name=salud_familiar]:checked").attr('value');
				var servicion_basicos_Tiene_Agua 	= $("input[name=servicion_basicos_Tiene_Agua]:checked").attr('value');
				var servicion_basicos_Tiene_Desague = $("input[name=servicion_basicos_Tiene_Desague]:checked").attr('value');
				var servicion_basicos_Tiene_Luz 	= $("input[name=servicion_basicos_Tiene_Luz]:checked").attr('value');
				puntaje_total = (	(parseFloat(tipo) || 0 ) +
									(parseFloat(rango_edad) || 0 ) + 
									(parseFloat(estado_civil) || 0 ) + 
									(parseFloat(lugar_procedencia) || 0 ) + 
									(parseFloat(grado_instruccion) || 0 ) + 
									(parseFloat(ocupacion) || 0 ) + 
									(parseFloat(condicion_trabajo) || 0 ) + 
									(parseFloat(situacion_vivienda) || 0 ) + 
									(parseFloat(hacinamiento) || 0 ) + 
									(parseFloat(personas_x_dormitorio) || 0 ) + 
									(parseFloat(material_contruccion) || 0 ) + 
									(parseFloat(ingreso_economico) || 0 ) + 
									(parseFloat(condicion_ingreso) || 0 ) + 
									(parseFloat(composicion_familiarr) || 0 ) + 
									(parseFloat(problemas_sociales) || 0 ) + 
									(parseFloat(salud_familiar) || 0 ) +
									(parseFloat(servicion_basicos_Tiene_Agua) || 0 ) +
									(parseFloat(servicion_basicos_Tiene_Desague) || 0 ) +
									(parseFloat(servicion_basicos_Tiene_Luz) || 0 ) 
								);
				$("#puntaje_total").textbox('setValue',puntaje_total);
				return false;
			}

			// FUNCION PARA MOSTRAR LA EXONERACION Y CATEGORIA
			function MostrarExoneracion()
			{
				var Puntaje = $("#puntaje_total").textbox('getText');
				$.ajax({
					url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarCategoriaParaExonerar',
					type: 'POST',
					dataType: 'json',
					data: 
						{
							puntaje: Puntaje
						},
					success:function(data)
					{	
						$("#letra").val(' ');
						$("#calificacion").textbox('setValue',data.EXONERACION);
						$("#letra").val(data.LETRA);
					}
				});
				
			}

			// INGRESAR FAMILIARES
			$("#IngresarFamiliares").click(function(event) {
				var numerdodnifamiliar = null;
				if ($("#numerdodnifamiliar").textbox('getText').length < 1) {
						numerdodnifamiliar = 0
				}
				else{
					numerdodnifamiliar = $("#numerdodnifamiliar").textbox('getText');
				}
			
				var apaternofamiliar 		= 	$("#apaternofamiliar").textbox('getText');
				var amaternofamiliar		= 	$("#amaternofamiliar").textbox('getText');
				var nombresfamiliar 		= 	$("#nombresfamiliar").textbox('getText');
				var estadocivilfamiliar 	= 	$("#estadocivilfamiliar").combobox('getValue');
				var gradoistruciionfamiliar = 	$("#gradoistruciionfamiliar").combobox('getValue');
				var edadfamiliar 			= 	$("#edadfamiliar").textbox('getText');
				var sexofamiliar 			= 	$("#sexofamiliar").combobox('getValue');
				var parentescofamiliar 		= 	$("#parentescofamiliar").combobox('getValue');
				var ocupacionfamiliar 		= 	$("#ocupacionfamiliar").textbox('getText');
				var direccionfamiliar		=   " ";
				var idpaciente 				=   $("#idpaciente").text();
				$.ajax({
					url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=IngresarNuevoFamiliaresD',
					type: 'POST',
					dataType: 'json',
					data: 
						{
							numerdodnifamiliar: numerdodnifamiliar,
							apaternofamiliar: apaternofamiliar,
							amaternofamiliar: amaternofamiliar,
							nombresfamiliar: nombresfamiliar,
							estadocivilfamiliar: estadocivilfamiliar,
							gradoistruciionfamiliar: gradoistruciionfamiliar,
							edadfamiliar: edadfamiliar,
							sexofamiliar: sexofamiliar,
							parentescofamiliar: parentescofamiliar,
							ocupacionfamiliar: ocupacionfamiliar,
							direccionfamiliar: direccionfamiliar,
							idpaciente: idpaciente
						},
					success: function(data)
					{
						if (data == 'M_1') {
							$("#numerdodnifamiliar").textbox('clear');
							$("#apaternofamiliar").textbox('clear');
							$("#amaternofamiliar").textbox('clear');
							$("#nombresfamiliar").textbox('clear');
							$("#estadocivilfamiliar").combobox('clear');
							$("#gradoistruciionfamiliar").combobox('clear');
							$("#sexofamiliar").combobox('clear');
							$("#parentescofamiliar").combobox('clear');
							$("#ocupacionfamiliar").combobox('clear');
							$("#edadfamiliar").textbox('clear');
							$("#idpaciente").val(' ');
							$("#grillaFamiliares").datagrid('reload');
							$("#grillaFamiliares_mostrar").datagrid('reload');
							
							
						}
					}
				});
				
			});

			$("#LimpiarFamiliares").click(function(event) {
				$("#numerdodnifamiliar").textbox('clear');
				$("#apaternofamiliar").textbox('clear');
				$("#amaternofamiliar").textbox('clear');
				$("#nombresfamiliar").textbox('clear');
				$("#estadocivilfamiliar").textbox('clear');
				$("#gradoistruciionfamiliar").textbox('clear');
				$("#edadfamiliar").textbox('clear');
				$("#sexofamiliar").textbox('clear');
				$("#parentescofamiliar").textbox('clear');
				$("#ocupacionfamiliar").textbox('clear');
			});
			// EVENTOS
			// =======
			// 
			
			// MOSTRAR MODAL NUEVOS PARIENTES
			$("#AgregarPariente").click(function(event) {
				var idpaciente=$("#idpaciente").text();
				$("#grillaFamiliares").datagrid({url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarFamiliaresGrilla&idpaciente='+idpaciente});
				$("#NuevoFamiliar").window('open');
			});
			
			// SELECCIONAR ALGUN RADIO INPUT
			$("input[type='radio']").on('change', function(event) {
				SumarRadios();
				MostrarExoneracion();
			});
			
			//ACCIONES CHECKBOX
			$("input[name=problemas_sociales_check_0]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_1]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_2]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_3]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_4]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_5]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_6]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_7]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_8]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_9]").on('change', function(event) {SumarProblemasSociales();	});
			$("input[name=problemas_sociales_check_10]").on('change', function(event) {SumarProblemasSociales();});
			
			$("input[name=salud_familiar_check_0]").on('change', function(event) {SumarSaludFamiliar();	});
			$("input[name=salud_familiar_check_1]").on('change', function(event) {SumarSaludFamiliar();	});
			$("input[name=salud_familiar_check_2]").on('change', function(event) {SumarSaludFamiliar();	});
			$("input[name=salud_familiar_check_3]").on('change', function(event) {SumarSaludFamiliar();	});
			$("input[name=salud_familiar_check_4]").on('change', function(event) {SumarSaludFamiliar();	});
			$("input[name=salud_familiar_check_5]").on('change', function(event) {SumarSaludFamiliar();	});
			$("input[name=salud_familiar_check_6]").on('change', function(event) {SumarSaludFamiliar();	});

			
			function SumarProblemasSociales()
			{	
				var problemas_sociales_check_0=$("input[name=problemas_sociales_check_0]:checked").attr('value');
				var problemas_sociales_check_1=$("input[name=problemas_sociales_check_1]:checked").attr('value');
				var problemas_sociales_check_2=$("input[name=problemas_sociales_check_2]:checked").attr('value');
				var problemas_sociales_check_3=$("input[name=problemas_sociales_check_3]:checked").attr('value');
				var problemas_sociales_check_4=$("input[name=problemas_sociales_check_4]:checked").attr('value');
				var problemas_sociales_check_5=$("input[name=problemas_sociales_check_5]:checked").attr('value');
				var problemas_sociales_check_6=$("input[name=problemas_sociales_check_6]:checked").attr('value');
				var problemas_sociales_check_7=$("input[name=problemas_sociales_check_7]:checked").attr('value');
				var problemas_sociales_check_8=$("input[name=problemas_sociales_check_8]:checked").attr('value');
				var problemas_sociales_check_9=$("input[name=problemas_sociales_check_9]:checked").attr('value');
				var problemas_sociales_check_10=$("input[name=problemas_sociales_check_10]:checked").attr('value');	
				var PS = (parseFloat(problemas_sociales_check_0) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_1) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_2) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_3) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_4) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_5) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_6) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_7) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_8) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_9) || 0)+" ,"+
						 (parseFloat(problemas_sociales_check_10) || 0);
				//alert(PS);
				$("#ProblemasSocialestotal").textbox('setValue',PS);
			}
			
			function SumarSaludFamiliar()
			{	
				var salud_familiar_check_0=$("input[name=salud_familiar_check_0]:checked").attr('value');
				var salud_familiar_check_1=$("input[name=salud_familiar_check_1]:checked").attr('value');
				var salud_familiar_check_2=$("input[name=salud_familiar_check_2]:checked").attr('value');
				var salud_familiar_check_3=$("input[name=salud_familiar_check_3]:checked").attr('value');
				var salud_familiar_check_4=$("input[name=salud_familiar_check_4]:checked").attr('value');
				var salud_familiar_check_5=$("input[name=salud_familiar_check_5]:checked").attr('value');
				var salud_familiar_check_6=$("input[name=salud_familiar_check_6]:checked").attr('value');
				var SF = (parseFloat(salud_familiar_check_0) || 0)+" ,"+
						 (parseFloat(salud_familiar_check_1) || 0)+" ,"+
						 (parseFloat(salud_familiar_check_2) || 0)+" ,"+
						 (parseFloat(salud_familiar_check_3) || 0)+" ,"+
						 (parseFloat(salud_familiar_check_4) || 0)+" ,"+
						 (parseFloat(salud_familiar_check_5) || 0)+" ,"+
						 (parseFloat(salud_familiar_check_6) || 0);
						 
				//alert(SF);
				$("#Saludfamiliartotal").textbox('setValue',SF);
			}
			
			

			// SELECCIONAR MAS DE UN MIEMBRO DE COMPOSICION FAMILIAR
			$("input[name=composicion_familiarr]").on('change', function(event) {
				var temp_composicion_familiarr = $("input[name=composicion_familiarr]:checked").attr('value');
				if (temp_composicion_familiarr >= 6) {
					$("#AgregarPariente").linkbutton('enable');
				}
				if (temp_composicion_familiarr <= 5) {
					$("#AgregarPariente").linkbutton('disable');
				}
			});

			// SELECCIONAR MAS DE UN PROBLEMA SOCIAL
			$("input[name=problemas_sociales]").on('change', function(event) {
				var temp_problemas_sociales = $("input[name=problemas_sociales]:checked").attr('value');
				if (temp_problemas_sociales >= 6) {
					$("input[name=problemas_sociales_check_0]").prop('disabled', false);
					$("input[name=problemas_sociales_check_1]").prop('disabled', false);
					$("input[name=problemas_sociales_check_2]").prop('disabled', false);
					$("input[name=problemas_sociales_check_3]").prop('disabled', false);
					$("input[name=problemas_sociales_check_4]").prop('disabled', false);
					$("input[name=problemas_sociales_check_5]").prop('disabled', false);
					$("input[name=problemas_sociales_check_6]").prop('disabled', false);
					$("input[name=problemas_sociales_check_7]").prop('disabled', false);
					$("input[name=problemas_sociales_check_8]").prop('disabled', false);
					$("input[name=problemas_sociales_check_9]").prop('disabled', false);
					$("input[name=problemas_sociales_check_10]").prop('disabled', false);
				}
				//Sin Problemas
				if (temp_problemas_sociales <= 5) {
					//Quitar Check si Tuviera
					$("input[name=problemas_sociales_check_0]").removeAttr('checked');
					$("input[name=problemas_sociales_check_1]").removeAttr('checked');
					$("input[name=problemas_sociales_check_2]").removeAttr('checked');
					$("input[name=problemas_sociales_check_3]").removeAttr('checked');
					$("input[name=problemas_sociales_check_4]").removeAttr('checked');
					$("input[name=problemas_sociales_check_5]").removeAttr('checked');
					$("input[name=problemas_sociales_check_6]").removeAttr('checked');
					$("input[name=problemas_sociales_check_7]").removeAttr('checked');
					$("input[name=problemas_sociales_check_8]").removeAttr('checked');
					$("input[name=problemas_sociales_check_9]").removeAttr('checked');
					$("input[name=problemas_sociales_check_10]").removeAttr('checked');
					//Desahibilitar Check si Tuviera
					$("input[name=problemas_sociales_check_0]").prop('disabled', true);
					$("input[name=problemas_sociales_check_1]").prop('disabled', true);
					$("input[name=problemas_sociales_check_2]").prop('disabled', true);
					$("input[name=problemas_sociales_check_3]").prop('disabled', true);
					$("input[name=problemas_sociales_check_4]").prop('disabled', true);
					$("input[name=problemas_sociales_check_5]").prop('disabled', true);
					$("input[name=problemas_sociales_check_6]").prop('disabled', true);
					$("input[name=problemas_sociales_check_7]").prop('disabled', true);
					$("input[name=problemas_sociales_check_8]").prop('disabled', true);
					$("input[name=problemas_sociales_check_9]").prop('disabled', true);
					$("input[name=problemas_sociales_check_10]").prop('disabled', true);
					//Limpiar los Datos
					$("#ProblemasSocialestotal").textbox('setValue','');
				}
			});	

			// SELEECIONAR MAS DE UN PROBLEMA FAMILIAR
			$("input[name=salud_familiar]").on('change', function(event) {
				var temp_salud_familiar = $("input[name=salud_familiar]:checked").attr('value');
				if (temp_salud_familiar >= 6) {
					$("input[name=salud_familiar_check_0]").prop('disabled', false);
					$("input[name=salud_familiar_check_1]").prop('disabled', false);
					$("input[name=salud_familiar_check_2]").prop('disabled', false);
					$("input[name=salud_familiar_check_3]").prop('disabled', false);
					$("input[name=salud_familiar_check_4]").prop('disabled', false);
					$("input[name=salud_familiar_check_5]").prop('disabled', false);
					$("input[name=salud_familiar_check_6]").prop('disabled', false);
				}
				//Es en caso sea Salud Familiar Sin Problemas
				if (temp_salud_familiar <= 5) {
					//Quitar Check si Tuviera
					$("input[name=salud_familiar_check_0]").removeAttr('checked');
			        $("input[name=salud_familiar_check_1]").removeAttr('checked');
			        $("input[name=salud_familiar_check_2]").removeAttr('checked');
			        $("input[name=salud_familiar_check_3]").removeAttr('checked');
			        $("input[name=salud_familiar_check_4]").removeAttr('checked');
			        $("input[name=salud_familiar_check_5]").removeAttr('checked');
			        $("input[name=salud_familiar_check_6]").removeAttr('checked');
					//Desahibilitar Check si Tuviera
					$("input[name=salud_familiar_check_0]").prop('disabled', true);
					$("input[name=salud_familiar_check_1]").prop('disabled', true);
					$("input[name=salud_familiar_check_2]").prop('disabled', true);
					$("input[name=salud_familiar_check_3]").prop('disabled', true);
					$("input[name=salud_familiar_check_4]").prop('disabled', true);
					$("input[name=salud_familiar_check_5]").prop('disabled', true);
					$("input[name=salud_familiar_check_6]").prop('disabled', true);
					//Limpiar los Datos
					$("#Saludfamiliartotal").textbox('setValue','');
				}
			});

			//validando asistente
			$("#codigo_validacion_asistente").textbox('textbox').bind('keydown', function(e){
				if (e.keyCode == 13){
					//alert('hoal');return false;
					var codigo = $("#codigo_validacion_asistente").textbox('getText');

					$.ajax({
						url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=ValidarCodigoAsistenciaSocial',
						type: 'POST',
						dataType: 'json',
						data: 
							{
								contrasena: codigo
							},
						success: function(respuestassss)
						{
							if (respuestassss.idempleado == null || respuestassss.idempleado == ' ' ) {
								$.messager.alert('SIGESA','El codigo es invalido o el usuario es inexistente.');
							}
							else
							{
								$("#idempleado_asistente").text(respuestassss.idempleado);
								$("#nombres_asistente_validacion").textbox('setText',respuestassss.asistente);
							}
						}
					});
					
				}
			});

			// BUSQUEDA POR NUMERO DE CUENTA
			$("#Numerocuenta").numberbox('textbox').bind('keydown', function(e){
			if (e.keyCode == 13){
				var numerocuenta = $("#Numerocuenta").numberbox('getText');
				LimpiarCajasdeTextoFichaSocial();
				$.ajax({
					url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarPacientesxNumeroCuenta',
					type: 'POST',
					dataType: 'json',
					data: 
						{
							numerocuenta: numerocuenta
						},
					success: function(resultado_p)
					{		
							$.ajax({
								url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=BuscarExistenciaPacienteFichaSocial',
								type: 'POST',
								dataType: 'json',
								data: 
									{
									numerocuenta: numerocuenta
								},
								success: function(resultado_t)
								{
									if (resultado_t.variable == 'M_1') {
										//recategorizar
										$("#MostrarFichaNuevabtn").linkbutton('enable');
										$("#numichasocial").textbox('setText',resultado_t.numfs);
										$("#diagnosticosocial").textbox('setText',resultado_t.diagnosticosocial);
										var	Just = tinymce.get('diagnosticosocial_V2').setContent(resultado_t.diagnosticosocial);
				                        var	Eva = tinymce.get('tratamientosocial_V2').setContent(resultado_t.tratamientoSocial);
										$("#tratamientosocial").textbox('setText',resultado_t.tratamientoSocial);
										$("#diagnosticomedico").textbox('setText',resultado_t.diagnosticomedico);

										//////////////TIPO/////////////////////
										if (resultado_t.tipo == 1) {
											$("#Nuevo").prop('checked', true);
										}
										if (resultado_t.tipo == 2) {
											$("#Reingreso").prop('checked', true);
										}
										if (resultado_t.tipo == 3) {
											$("#Continuador").prop('checked', true);
										}
							/////////////ESTADO CIVL////////////////////
										if (resultado_t.estadocivil == 1) {
											$("#Soltero").prop('checked', true);
										}
										if (resultado_t.estadocivil == 2) {
											$("#Madre_Soltera").prop('checked', true);
										}
										if (resultado_t.estadocivil == 3) {
											$("#Casado").prop('checked', true);
										}
										if (resultado_t.estadocivil == 4) {
											$("#Separado").prop('checked', true);
										}
										if (resultado_t.estadocivil == 5) {
											$("#Conviviente").prop('checked', true);
										}
										if (resultado_t.estadocivil == 6) {
											$("#Divorciado").prop('checked', true);
										}
										if (resultado_t.estadocivil == 7) {
											$("#Viudo").prop('checked', true);
										}
							/////////////////LUGAR PROCEDENCIA///////////////////
										if (resultado_t.lugarprocedencia == 1) {
											$("#Rural").prop('checked', true);
										}
										if (resultado_t.lugarprocedencia == 2) {
											$("#Urbano").prop('checked', true);
										}
										if (resultado_t.lugarprocedencia == 3) {
											$("#Urbano_Marginal").prop('checked', true);
										}
							//////////////GRADO DE INSTRUCCION//////////////////////////////
										if (resultado_t.gradoinstruccion == 1) {
											$("#Sin_Instruccion").prop('checked', true);
										}
										if (resultado_t.gradoinstruccion == 2) {
											$("#Primaria").prop('checked', true);
										}
										if (resultado_t.gradoinstruccion == 3) {
											$("#Secundaria").prop('checked', true);
										}
										if (resultado_t.gradoinstruccion == 4) {
											$("#Superior_Tecnico").prop('checked', true);
										}
										if (resultado_t.gradoinstruccion == 5) {
											$("#Superior_Universidad").prop('checked', true);
										}
							////////////////////OCUPACION PRINCIPAL/////////////////////////
										if (resultado_t.ocupacion == 1) {
											$("#Profesional").prop('checked', true);
										}
										if (resultado_t.ocupacion == 2) {
											$("#Trabajador_Calificado").prop('checked', true);
										}
										if (resultado_t.ocupacion == 3) {
											$("#Trabajador_No_Calificado").prop('checked', true);
										}
										if (resultado_t.ocupacion == 4) {
											$("#Estudiante").prop('checked', true);
										}
										if (resultado_t.ocupacion == 5) {
											$("#Estudia_y_Trabaja").prop('checked', true);
										}
										if (resultado_t.ocupacion == 6) {
											$("#Sin_Ocupacion").prop('checked', true);
										}
										if (resultado_t.ocupacion == 7) {
											$("#Ama_de_Casa").prop('checked', true);
										}
										if (resultado_t.ocupacion == 8) {
											$("#Pensionista").prop('checked', true);
										}
							///////////////CONDICION DE TRABAJO///////////////////////////////
										if (resultado_t.condicion == 1) {
											$("#Estable").prop('checked', true);
										}
										if (resultado_t.condicion == 2) {
											$("#Eventual").prop('checked', true);
										}
										if (resultado_t.condicion == 3) {
											$("#Contratado").prop('checked', true);
										}
										if (resultado_t.condicion == 4) {
											$("#Desempleado").prop('checked', true);
										}
							/////////////////////PERSONA RESPONSABLEE//////////////////////////////////////////

										if (resultado_t.estadocivilpr == 1) {
											$("#Soltero_pr").prop('checked', true);
										}
										if (resultado_t.estadocivilpr == 2) {
											$("#Madre_Soltera_pr").prop('checked', true);

										}
										if (resultado_t.estadocivilpr == 3) {
											$("#Casado_pr").prop('checked', true);
										}
										if (resultado_t.estadocivilpr == 4) {
											$("#Separado_pr").prop('checked', true);
										}
										if (resultado_t.estadocivilpr == 5) {
											$("#Conviviente_pr").prop('checked', true);
										}

										if (resultado_t.estadocivilpr == 6) {
											$("#Divorciado_pr").prop('checked', true);
										}
										if (resultado_t.estadocivilpr == 7) {
											$("#Viudo_pr").prop('checked', true);
										}
							///////////////////////////////////////////////////////////////////////////////
										if (resultado_t.intruccionpr == 1) {
											$("#Sin_Instruccion_pr").prop('checked', true);
										}
										if (resultado_t.intruccionpr == 2) {
											$("#Primaria_pr").prop('checked', true);
										}
										if (resultado_t.intruccionpr == 3) {
											$("#Secundaria_pr").prop('checked', true);
										}
										if (resultado_t.intruccionpr == 4) {	
											$("#Superior_Tecnico_pr").prop('checked', true);
										}
										if (resultado_t.intruccionpr == 5) {
											$("#Superior_Universidad_pr").prop('checked', true);
										}
							////////////////////OCUPACION PRINCIPAL/////////////////////////
										if (resultado_t.ocupacionpr == 1) {
											$("#Profesional_pr").prop('checked', true);
										}
										if (resultado_t.ocupacionpr == 2) {
											$("#Trabajador_Calificado_pr").prop('checked', true);
										}
										if (resultado_t.ocupacionpr == 3) {
											$("#Trabajador_No_Calificado_pr").prop('checked', true);
										}
										if (resultado_t.ocupacionpr == 4) {
											$("#Estudiante_pr").prop('checked', true);
										}
										if (resultado_t.ocupacionpr == 5) {
											$("#Estudia_y_Trabaja_pr").prop('checked', true);
										}
										if (resultado_t.ocupacionpr == 6) {
											$("#Sin_Ocupacion_pr").prop('checked', true);
										}
										if (resultado_t.ocupacionpr == 7) {
											$("#Ama_de_Casa_pr").prop('checked', true);
										}
										if (resultado_t.ocupacionpr == 8) {
											$("#Pensionista_pr").prop('checked', true);
										}
							///////////////////CONDICION TRABAJO///////////////////////////

										if (resultado_t.condicionpr == 1) {
											$("#Estable_pr").prop('checked', true);
										}
										if (resultado_t.condicionpr == 2) {
											$("#Eventual_pr").prop('checked', true);
										}
										if (resultado_t.condicionpr == 3) {
											$("#Contratado_pr").prop('checked', true);
										}
										if (resultado_t.condicionpr == 4) {
											$("#Desempleado_pr").prop('checked', true);
										}
					
							///////////////////////SITUACION VIVIENDA///////////////////////
										if (resultado_t.situacionvivienda == 1) {
											$("#Propia").prop('checked', true);
										}
										if (resultado_t.situacionvivienda == 2) {
											$("#Alquilada").prop('checked', true);
										}
										if (resultado_t.situacionvivienda == 3) {
											$("#Alojamiento").prop('checked', true);
										}
										if (resultado_t.situacionvivienda == 4) {
											$("#Guardiania").prop('checked', true);
										}
										if (resultado_t.situacionvivienda == 5) {
											$("#Invasion").prop('checked', true);
										}
										if (resultado_t.situacionvivienda == 6) {
											$("#Via_Publica").prop('checked', true);
										}
							///////////////////HACIMIENTO/////////////////////////////////////
										if (resultado_t.hacimiento == 1) {
											$("#Unifamiliar").prop('checked', true);
										}
										if (resultado_t.hacimiento == 2) {
											$("#Multifamiliar").prop('checked', true);
										}

							//////////////////////N° MIEMBROS DEL HOGAR & DORMITORIOS//////////////
										$("#numerodemiembros").textbox('setText',resultado_t.miembroshogar);
										$("#numerodemiembros").textbox('setValue',resultado_t.miembroshogar);
										$("#numerodedormitorios").textbox('setText',resultado_t.dormitorioshogar);
										$("#numerodedormitorios").textbox('setValue',resultado_t.dormitorioshogar);

							/////////////////////PERSONAS POR DORMITORIO///////////////////////////			
										if (resultado_t.personasxdormitorio == 1) {
											$("#Menos_de_3_pers_por_dormitorio").prop('checked', true);
										}
										if (resultado_t.personasxdormitorio == 2) {
											$("#Mas_de_3_pers_por_dormitorio").prop('checked', true);
										}
							/////////////////////MATERIAL CONSTRUCCION/////////////////////////////
										if (resultado_t.materialconstruccion == 1) {
											$("#Noble_y_acabado").prop('checked', true);
										}
										if (resultado_t.materialconstruccion == 2) {
											$("#Noble_y_sin_acabar").prop('checked', true);
										}
										if (resultado_t.materialconstruccion == 3) {
											$("#Mixto").prop('checked', true);
										}
										if (resultado_t.materialconstruccion == 4) {
											$("#Rustico").prop('checked', true);
										}
										if (resultado_t.materialconstruccion == 5) {
											$("#Precario").prop('checked', true);
										}
								/////////////////INGRESO ECONOMICO///////////////////////////////
										if (resultado_t.ingresoeconomico == 1) {
											$("#Menor_a_1_SMV").prop('checked', true);
										}
										if (resultado_t.ingresoeconomico == 2) {
											$("#De_1_a_2_SMV").prop('checked', true);
										}
										if (resultado_t.ingresoeconomico == 3) {
											$("#Mas_de_3_SMV").prop('checked', true);
										}
										if (resultado_t.ingresoeconomico == 4) {
											$("#Sin_Ingreso").prop('checked', true);
										}
								/////////////////CONDICION INGRESO///////////////////////////////
										if (resultado_t.condicioningreso == 1) {
											$("#Fijo").prop('checked', true);
										}
										if (resultado_t.condicioningreso == 2) {
											$("#Eventualmente").prop('checked', true);
										}
										if (resultado_t.condicioningreso == 3) {
											$("#Ninguno").prop('checked', true);
										}
								///////////////////COMPOSICION FAMILIAR///////////////////////////
										if (resultado_t.composicionfamiliar == 1) {
											$("#Sin_carga_familiar").prop('checked', true);
										}		
										if (resultado_t.composicionfamiliar == 2) {
											$("#De_1_a_2_hijo").prop('checked', true);
										}		
										if (resultado_t.composicionfamiliar == 3) {
											$("#De_3_a_mas_hijos").prop('checked', true);
										}		
								///////////////////PROBLEMAS SOCIALES///////////////////////////////
										if (resultado_t.problemassociales == 1) {
											$("#Sin_Problemas").prop('checked', true);
										}
										if (resultado_t.problemassociales == 2) {
											$("#Tiene_1_problema").prop('checked', true);
										}
										if (resultado_t.problemassociales == 3) {
											$("#Tiene_2_problemas").prop('checked', true);
										}
										if (resultado_t.problemassociales == 4) {
											$("#Tiene_3_a_mas_problemas").prop('checked', true);
										}
								////////////////////SALUD FAMILIAR///////////////////////////////////////
										if (resultado_t.saludfamiliar == 1) {
											$("#Sin_problem").prop('checked', true);
										}	
										if (resultado_t.saludfamiliar == 2) {
											$("#Tiene_1_problem").prop('checked', true);
										}
										if (resultado_t.saludfamiliar == 3) {
											$("#Tiene_2_problem").prop('checked', true);
										}
										if (resultado_t.saludfamiliar == 4) {
											$("#Tiene_3_o_mas_problem").prop('checked', true);
										}							
										
								////////////////////AGUA/////////////////////////////
										if (resultado_t.agua == 1) {
											$("#Si_1").prop('checked', true);
											
										}	
										if (resultado_t.agua == 2) {
											$("#No_1").prop('checked', true);
										}	
								///////////////////DESAGUE////////////////////////////
										if (resultado_t.desague == 1) {
											$("#Si_2").prop('checked', true);
										}	
										if (resultado_t.desague == 2) {
											$("#No_2").prop('checked', true);
										}	
								//////////////////////LUZ///////////////////////////////		
										if (resultado_t.luz == 1) {
											$("#Si_3").prop('checked', true);
										}	
										if (resultado_t.luz == 2) {
											$("#No_3").prop('checked', true);
										}

								///////////////////////////////////////////////////////////////////////////////////////////////////////////		
										$("#telefonopaciente_I").textbox('setText',resultado_t.primertelefono);
										$("#telefonopaciente_II").textbox('setText',resultado_t.otrotelefono);
										$("#edadpersonares").textbox('setText',resultado_t.edadpr);
										$("#nombres_asistente").textbox('setText',resultado_t.asistenta);
										$("#parentesco_pr").textbox('setValue',resultado_t.parientepr); 
										$("#parentesco_pr").textbox('setText',resultado_t.parienteprnombre); 
										$("#personaresponsable_paterno").textbox('setText',resultado_t.paternofamiliar);
										$("#personaresponsable_materno").textbox('setText',resultado_t.maternofamiliar);
										$("#personaresponsable_nombres").textbox('setText',resultado_t.nombrefamiliar);
										$("#direccion_pr").textbox('setText',resultado_t.direccionresponsable);
										$("#otrosproblemassociales").textbox('setValue',resultado_t.otrops);
										$("#otrosproblemassociales").textbox('setText',resultado_t.otrops);
										$("#otrossaludfamiliar").textbox('setValue',resultado_t.otrosf);
										$("#otrossaludfamiliar").textbox('setText',resultado_t.otrosf);
								///////////////////////PROBLEMAS SOCIALES CHECK//////////////////////////
										if (resultado_t.ps1 == 1) {
											$("#Abandono_Familiar").prop('checked', true);
										}
										if (resultado_t.ps2 == 2) {
											$("#Discapacitado").prop('checked', true);
										}
										if (resultado_t.ps3 == 3) {
											$("#Menores_no_reconocidos").prop('checked', true);
										}
										if (resultado_t.ps4 == 4) {
											$("#Privacion_de_la_libertad").prop('checked', true);
										}
										if (resultado_t.ps5 == 5) {
											$("#Prostitucion").prop('checked', true);
										}
										if (resultado_t.ps6 == 6) {
											$("#Violencia_Familiar").prop('checked', true);
										}
										if (resultado_t.ps7 == 7) {
											$("#Delincuencia").prop('checked', true);
										}
										if (resultado_t.ps8 == 8) {
											$("#Menores_Fugados").prop('checked', true);
										}
										if (resultado_t.ps9 == 9) {
											$("#Alcoholismo_y_drogadiccion").prop('checked', true);
										}
										if (resultado_t.ps10 == 10) {
											$("#Ausencia_de_familiar_de_origen").prop('checked', true);
										}
										if (resultado_t.ps11== 11) {
											$("#Otros").prop('checked', true);
										}
								//////////////////////////SALUD FAMILIAR////////////////////////////////
										if (resultado_t.sf1 == 1) {
											$("#TBC").prop('checked', true);
										}
										if (resultado_t.sf2 == 2) {
											$("#ETS").prop('checked', true);
										}
										if (resultado_t.sf3 == 3) {
											$("#VIH_Sida").prop('checked', true);
										}
										if (resultado_t.sf4 == 4) {
											$("#Cancer").prop('checked', true);
										}
										if (resultado_t.sf5 == 5) {
											$("#Desnutricion").prop('checked', true);
										}
										if (resultado_t.sf6 == 6) {
											$("#Enfermedad_Mental").prop('checked', true);
										}
										if (resultado_t.sf7 == 7) {
											$("#Otros_SF").prop('checked', true);
										}
										//Cargar Sumas Automaticamente
										SumarRadios();
										MostrarExoneracion();
										SumarSaludFamiliar();
										SumarProblemasSociales();
								//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
										$.messager.alert('SIGESA','Este paciente ya tiene una ficha social, se pasara a recategorizar: <br><br> PUNTAJE OBTENIDO: ' + resultado_t.puntaje + '<br> CATEGORIA: ' + resultado_t.letra);
									}
									if (resultado_t.variable == 'M_0') {
										//nuevo registro
										$("#MostrarFichaNuevabtn").linkbutton('disable');
										$("#GuardarFichaSocial").linkbutton('enable');
										$("#numichasocial").textbox('setText',resultado_t.numfs);
									}
								}
							});
							
							//$("#").textbox('setText',resultado_p.CUENTA);
							if (resultado_p.SEXO == 'FEMENINO') {
								//$("#Femenino_sexo").attr('checked', true);
								$("#Femenino_sexo").prop('checked', true);
							}
							if (resultado_p.SEXO == 'MASCULINO') {
								//$("#Masculino_sexo").attr('checked', true);
								$("#Masculino_sexo").prop('checked', true);
							}
							
/////////////////////////////MARCAR EDAD//////////////////////////////
							if (resultado_p.EDAD >= 0  && resultado_p.EDAD <= 5) {
								$("#0-5").prop('checked', true);
							}
							if (resultado_p.EDAD >= 6  && resultado_p.EDAD <= 17) {
								$("#6-17").prop('checked', true);
							}
							if (resultado_p.EDAD >= 18  && resultado_p.EDAD <= 59) {
								$("#18-59").prop('checked', true);
							}
							if (resultado_p.EDAD >= 60) {
								$("#60_a_mas").prop('checked', true);
							}
							
///////////////////////////////////////////////////////////////////////////////////////////////////////////		

						//	$("#personaresponsable_paterno").textbox('setText',resultado_t.apellidopaterno_familiar);
						//	$("#personaresponsable_materno").textbox('setText',resultado_t.apellidomaterno_familiar);
						//	$("#personaresponsable_nombres").textbox('setText',resultado_t.nombre_familiar);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////					
							
							$("#tipopaciente").textbox('setText',resultado_p.TIPOPACIENTE);
							$("#tipoatencion").textbox('setText',resultado_p.TIPOATENCION);
							$("#nombrepaciente").textbox('setText',resultado_p.PACIENTE);
							$("#seguro").textbox('setText',resultado_p.SEGURO);
							$("#dnipaciente").textbox('setText',resultado_p.DNI);
							$("#direccionPaciente").textbox('setText',resultado_p.DOMICILIO);
							$("#telefonopaciente").textbox('setText',resultado_p.TELEFONO);
							$("#edadpaciente").textbox('setText',resultado_p.EDAD);
							$("#idpaciente").text(resultado_p.IDPACIENTE);
							$("#cuenta").text(resultado_p.CUENTA);
							$("#departamentoPaciente").textbox('setText',resultado_p.DEPARTAMENTO);
							$("#provinciaPaciente").textbox('setText',resultado_p.PROVINCIA);
							$("#distritoPaciente").textbox('setText',resultado_p.DISTRITO);
							// Carga los Familiares del Paciente
							Mostrar_Familiares_IdPaciente();							
							
					}
				});
				
			}

			});

			//RECATEGORIZAR
			$("#MostrarFichaNuevabtn").click(function(event) {
				var HistoriaClinica = $("#Numerocuenta").textbox('getValue'); //historia
				var numichasocial = $("#numichasocial").textbox('getText');
				var FechaIngreso = $("#FechaIngreso").textbox('getValue');
				var idpaciente = $("#idpaciente").text();
				var letra = $("#letra").val();
				var numerodemiembros = $("#numerodemiembros").textbox('getValue');
				var numerodedormitorios	= $("#numerodedormitorios").textbox('getValue');
				var puntaje_total = $("#puntaje_total").textbox('getValue');
				var otrosproblemassociales = $("#otrosproblemassociales").textbox('getValue');
				var otrossaludfamiliar = $("#otrossaludfamiliar").textbox('getValue');
				var diagnosticosocial = $("#diagnosticosocial").textbox('getText');
				var tratamientosocial = $("#tratamientosocial").textbox('getText');
				var diagnosticomedico = $("#diagnosticomedico").textbox('getText');
				var cuenta = $("#cuenta").text();
				var direccionActualizar = $("#direccionPacienteProcedencia").textbox('getText');
				var distritoProcedencia = $("#distritosPacientesProcedencias").combobox('getValue');
				var telefonoPacientes1 = $("#telefonopaciente_I").textbox('getText');
				var telefonoPacientes2 = $("telefonopaciente_II").textbox('getText');
				
				//DATOS PERSONA RESPONASABLE
				var personaresponsable_paterno = $("#personaresponsable_paterno").textbox('getText');
				var personaresponsable_materno = $("#personaresponsable_materno").textbox('getText');
				var personaresponsable_nombres = $("#personaresponsable_nombres").textbox('getText');
				var parentesco_pr = $("#parentesco_pr").textbox('getValue');
				var direccion_pr = $("#direccion_pr").textbox('getText');
				var edadpersonares = $("#edadpersonares").textbox('getText');
				var estado_civil_pr = $("input[name=estado_civil_pr]:checked").attr('value');
				var grado_instruccion_pr = $("input[name=grado_instruccion_pr]:checked").attr('value');
				var ocupacion_pr = $("input[name=ocupacion_pr]:checked").attr('value');
				var condicion_trabajo_pr = $("input[name=condicion_trabajo_pr]:checked").attr('value');

				//CLAVES
				var tipo							= $("input[name=tipo]:checked").attr('value');
				var rango_edad						= $("input[name=rango_edad]:checked").attr('value');
				var estado_civil 					= $("input[name=estado_civil]:checked").attr('value');
				var estado_civil_name				= $("input[name=estado_civil]:checked").attr('id'); //	Nombre Estado Civil
				var lugar_procedencia 				= $("input[name=lugar_procedencia]:checked").attr('value');
				var grado_instruccion 				= $("input[name=grado_instruccion]:checked").attr('value');
				var ocupacion 						= $("input[name=ocupacion]:checked").attr('value');
				var condicion_trabajo 				= $("input[name=condicion_trabajo]:checked").attr('value');
				var situacion_vivienda 				= $("input[name=situacion_vivienda]:checked").attr('value');
				var hacinamiento 					= $("input[name=hacinamiento]:checked").attr('value');
				var personas_x_dormitorio 			= $("input[name=personas_x_dormitorio]:checked").attr('value');
				var material_contruccion 			= $("input[name=material_contruccion]:checked").attr('value');
				var ingreso_economico 				= $("input[name=ingreso_economico]:checked").attr('value');
				var condicion_ingreso 				= $("input[name=condicion_ingreso]:checked").attr('value');
				var composicion_familiarr 			= $("input[name=composicion_familiarr]:checked").attr('value');
				var problemas_sociales 				= $("input[name=problemas_sociales]:checked").attr('value');
				var salud_familiar 					= $("input[name=salud_familiar]:checked").attr('value');
				var servicion_basicos_Tiene_Agua 	= $("input[name=servicion_basicos_Tiene_Agua]:checked").attr('value');
				var servicion_basicos_Tiene_Desague = $("input[name=servicion_basicos_Tiene_Desague]:checked").attr('value');
				var servicion_basicos_Tiene_Luz 	= $("input[name=servicion_basicos_Tiene_Luz]:checked").attr('value');
				var codAsistenta =$("#idempleado_asistente").text();

				/*console.log('HistoriaClinica: ' + HistoriaClinica + '\n' +
							'numichasocial: ' + numichasocial + '\n' +
							'FechaIngreso: ' + FechaIngreso + '\n' +
							'idpaciente: ' + idpaciente + '\n' +
							'letra: ' + letra + '\n' +
							'numerodemiembros: ' + numerodemiembros + '\n' +
							'numerodedormitorios: ' + numerodedormitorios + '\n' +
							'puntaje_total: ' + puntaje_total + '\n' +
							'otrosproblemassociales: ' + otrosproblemassociales + '\n' +
							'otrossaludfamiliar: ' + otrossaludfamiliar + '\n' +
							'diagnosticosocial: ' + diagnosticosocial + '\n' +
							'tratamientosocial: ' + tratamientosocial + '\n' +
							'cuenta: ' + cuenta + '\n' +
							'direccionActualizar: ' + direccionActualizar + '\n' +
							'distritoProcedencia: ' + distritoProcedencia + '\n' +
							'telefonoPacientes: ' + telefonoPacientes + '\n' +
							'personaresponsable_paterno: ' + personaresponsable_paterno + '\n' +
							'personaresponsable_materno: ' + personaresponsable_materno + '\n' +
							'personaresponsable_nombres: ' + personaresponsable_nombres + '\n' +
							'parentesco_pr: ' + parentesco_pr + '\n' +
							'direccion_pr: ' + direccion_pr + '\n' +
							'edadpersonares: ' + edadpersonares + '\n' +
							'estado_civil_pr: ' + estado_civil_pr + '\n' +
							'grado_instruccion_pr: ' + grado_instruccion_pr + '\n' +
							'ocupacion_pr: ' + ocupacion_pr + '\n' +
							'estado_civil: ' + estado_civil + '\n' +
							'lugar_procedencia: ' + lugar_procedencia + '\n' +
							'grado_instruccion: ' + grado_instruccion + '\n' +
							'ocupacion: ' + ocupacion + '\n' +
							'condicion_trabajo: ' + condicion_trabajo + '\n' +
							'situacion_vivienda: ' + situacion_vivienda + '\n' +
							'hacinamiento: ' + hacinamiento + '\n' +
							'personas_x_dormitorio: ' + personas_x_dormitorio + '\n' +
							'material_contruccion: ' + material_contruccion + '\n' +
							'ingreso_economico: ' + ingreso_economico + '\n' +
							'condicion_ingreso: ' + condicion_ingreso + '\n' +
							'composicion_familiarr: ' + composicion_familiarr + '\n' +
							'problemas_sociales: ' + problemas_sociales + '\n' +
							'salud_familiar: ' + salud_familiar + '\n' +
							'servicion_basicos_Tiene_Agua: ' + servicion_basicos_Tiene_Agua + '\n' +
							'servicion_basicos_Tiene_Desague: ' + servicion_basicos_Tiene_Desague + '\n' +
							'servicion_basicos_Tiene_Luz: ' + servicion_basicos_Tiene_Luz + '\n');
				return false;*/
				
				//obteniendo datos de tabla
				$.ajax({
					url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=GenerarFichaSocial',
					type: 'POST',
					dataType: 'json',
					data: {
						numichasocial : numichasocial,
						FechaIngreso : FechaIngreso,
						idpaciente : idpaciente,
						letra : letra,
						numerodemiembros : numerodemiembros,
						numerodedormitorios : numerodedormitorios,
						puntaje_total : puntaje_total,
						otrosproblemassociales : otrosproblemassociales,
						otrossaludfamiliar : otrossaludfamiliar,
						diagnosticosocial : diagnosticosocial,
						tratamientosocial : tratamientosocial,
						diagnosticomedico: diagnosticomedico,
						cuenta : cuenta,
						personaresponsable_paterno : personaresponsable_paterno,
						personaresponsable_materno : personaresponsable_materno,
						personaresponsable_nombres : personaresponsable_nombres,
						parentesco_pr : parentesco_pr,
						direccion_pr : direccion_pr,
						edadpersonares : edadpersonares,
						estado_civil_pr : estado_civil_pr,
						grado_instruccion_pr : grado_instruccion_pr,
						ocupacion_pr : ocupacion_pr,
						condicion_trabajo_pr :condicion_trabajo_pr,
						estado_civil : estado_civil,
						lugar_procedencia : lugar_procedencia,
						grado_instruccion : grado_instruccion,
						ocupacion : ocupacion,
						condicion_trabajo : condicion_trabajo,
						situacion_vivienda : situacion_vivienda,
						hacinamiento : hacinamiento,
						personas_x_dormitorio : personas_x_dormitorio,
						material_contruccion : material_contruccion,
						ingreso_economico : ingreso_economico,
						condicion_ingreso : condicion_ingreso,
						composicion_familiarr : composicion_familiarr,
						problemas_sociales : problemas_sociales,
						salud_familiar : salud_familiar,
						servicion_basicos_Tiene_Agua : servicion_basicos_Tiene_Agua,
						servicion_basicos_Tiene_Desague : servicion_basicos_Tiene_Desague,
						servicion_basicos_Tiene_Luz : servicion_basicos_Tiene_Luz,
						direccionActualizar : direccionActualizar,
						distritoProcedencia : distritoProcedencia,
						telefonoPacientes1 : telefonoPacientes1,
						telefonopacientes2 : telefonopacientes2,
						codAsistenta: codAsistenta,
						rango_edad: rango_edad,
						tipo: tipo
					},
					success: function(data)
					{
						if (data == 'M_1') {
							$.messager.alert('SIGESA','Ficha Generada con Exito');
							$("input[type=checkbox], input[type=radio]").not("input[name=sexo]").prop("checked", false);
							$("#Numerocuenta").textbox('clear');
							$("#numichasocial").textbox('clear');
							$("#nombrepaciente").textbox('clear');
							$("#tipopaciente").textbox('clear');
							$("#tipoatencion").textbox('clear');
							$("#dnipaciente").textbox('clear');
							$("#direccionPaciente").textbox('clear');
							$("#telefonopaciente_I").textbox('clear');
							$("#telefonopaciente_II").textbox('clear');
							
							$("#edadpaciente").textbox('clear');
							$("#seguro").textbox('clear');
							$("#puntaje_total").textbox('clear');
							$("#marca_puntaje_total").val(' ');
							$("#calificacion").textbox('clear');
							$("#diagnosticosocial").textbox('clear');
							$("#tratamientosocial").textbox('clear');	
							$("#diagnosticomedico").textbox('clear');
							$("#codigo_validacion_asistente").textbox('clear');
							$("#nombres_asistente_validacion").textbox('clear');						
							return false;
						}

						else
						{
							$.messager.alert('SIGESA','Error de sistema, comuniquese con los administradores','error');
						}
					}
				});
			});

			// GENERAR DOCUMENTO
			$("#GuardarFichaSocial").click(function(event) {
				
				if(!$("input[name=tipo]:checked").val()) {
					alert('Seleccione un tipo de Paciente');					 
				  return 0;
				}
				if(!$("input[name=estado_civil]:checked").val()) {
					alert('Seleccione un Estado Civil');
				  return 0;
				}
				if(!$("input[name=lugar_procedencia]:checked").val()) {
					alert('Seleccione un Lugar de Procedencia');
				  return 0;
				}
				if(!$("input[name=rango_edad]:checked").val()) {
					alert('Seleccione un Rango de Edad');
				  return 0;
				}
				if(!$("input[name=grado_instruccion]:checked").val()) {
					alert('Seleccione un Grado de Instrucción');
				  return 0;
				}
				if(!$("input[name=ocupacion]:checked").val()) {
					alert('Seleccione una Ocupación');
				  return 0;
				}
				if(!$("input[name=condicion_trabajo]:checked").val()) {
					alert('Seleccione una Condición de Trabajo');
				  return 0;
				}
			//////////////////////////////////////////////////////////////////				
				if(!$("input[name=situacion_vivienda]:checked").val()) {
					alert('Seleccione una Situacion de Vivienda');
				  return 0;
				}
				if(!$("input[name=hacinamiento]:checked").val()) {
					alert('Seleccione un Hacimiento');
				  return 0;
				}
				if(!$("input[name=personas_x_dormitorio]:checked").val()) {
					alert('Seleccione un Nº de personas por dormitorio');
				  return 0;
				}
				if(!$("input[name=material_contruccion]:checked").val()) {
					alert('Seleccione un Material de Construcción');
				  return 0;
				}
				if(!$("input[name=ingreso_economico]:checked").val()) {
					alert('Seleccione un Tipo de ingreso');
				  return 0;
				}
				if(!$("input[name=condicion_ingreso]:checked").val()) {
					alert('Seleccione una Condición de Ingreso');
				  return 0;
				}
				if(!$("input[name=servicion_basicos_Tiene_Agua]:checked").val()) {
					alert('Seleccione Si Tiene Agua');
				  return 0;
				}
				if(!$("input[name=servicion_basicos_Tiene_Desague]:checked").val()) {
					alert('Seleccione Si tiene Desague');
				  return 0;
				}
				if(!$("input[name=servicion_basicos_Tiene_Luz]:checked").val()) {
					alert('Seleccione si tiene Luz');
				  return 0;
				}
		//////////////////////////////////////////////////////////////////////////////////				
				if(!$("input[name=composicion_familiarr]:checked").val()) {
					alert('Seleccione una Composicion Familiar');
				  return 0;
				}
				
				if(!$("input[name=problemas_sociales]:checked").val()) {
					alert('Seleccione Problemas Sociales');
				  return 0;
				}
				if(!$("input[name=salud_familiar]:checked").val()) {
					alert('Seleccione Salud Familiar');
				  return 0;
				}

		////////////////////////////////////////////////////////////////////////
				if ($("#codigo_validacion_asistente").val().length < 1) {
			      alert("Ingrese codigo de asistenta");
			      return 0;
			    }

		//////////////////////////////////////////////////////////////////////	

				var HistoriaClinica = $("#Numerocuenta").textbox('getValue'); //historia
				var numichasocial = $("#numichasocial").textbox('getText');
				var FechaIngreso = $("#FechaIngreso").textbox('getValue');
				var idpaciente = $("#idpaciente").text();
				var letra = $("#letra").val();
				var numerodemiembros = $("#numerodemiembros").textbox('getValue');
				var numerodedormitorios	= $("#numerodedormitorios").textbox('getValue');
				var puntaje_total = $("#puntaje_total").textbox('getValue');
				var ProblemasSocialestotal =  $("#ProblemasSocialestotal").textbox('getValue');
				var Saludfamiliartotal = $("#Saludfamiliartotal").textbox('getValue');
				var otrosproblemassociales = $("#otrosproblemassociales").textbox('getValue');
				var otrossaludfamiliar = $("#otrossaludfamiliar").textbox('getValue');
				//var diagnosticosocial = $("#diagnosticosocial").textbox('getText');
				var diagnosticosocial =tinymce.get('diagnosticosocial_V2').getContent();
				//var tratamientosocial = $("#tratamientosocial").textbox('getText');
				var tratamientosocial = tinymce.get('tratamientosocial_V2').getContent();
				var diagnosticomedico = $("#diagnosticomedico").textbox('getText');
				var cuenta = $("#cuenta").text();
				var direccionActualizar = $("#direccionPacienteProcedencia").textbox('getText');
				var distritoProcedencia = $("#distritosPacientesProcedencias").combobox('getValue');
				var telefonoPacientes1 = $("#telefonopaciente_I").textbox('getText');
				var telefonoPacientes2 = $("#telefonopaciente_II").textbox('getText');
				var problemas_sociales_check = $('input[name=problemas_sociales_check]:checked').map(function(_, el) {
					return $(el).val();
    				}).get();
					
				//DATOS PERSONA RESPONASABLE
				
				var personaresponsable_paterno = $("#personaresponsable_paterno").textbox('getText');
				var personaresponsable_materno = $("#personaresponsable_materno").textbox('getText');
				var personaresponsable_nombres = $("#personaresponsable_nombres").textbox('getText');
				var parentesco_pr = $("#parentesco_pr").textbox('getValue');
				var direccion_pr = $("#direccion_pr").textbox('getText');
				var edadpersonares = $("#edadpersonares").textbox('getText');
				var estado_civil_pr = $("input[name=estado_civil_pr]:checked").attr('value');
				var estado_civil_name_pr	= $("input[name=estado_civil_pr]:checked").attr('id'); //	Nombre Estado Civil Persona Resonsable
				var grado_instruccion_pr = $("input[name=grado_instruccion_pr]:checked").attr('value');
				var grado_instruccion_name_pr	= $("input[name=grado_instruccion_pr]:checked").attr('id'); //	Nombre Grado Instruccion
				var ocupacion_pr = $("input[name=ocupacion_pr]:checked").attr('value');
				var ocupacion_name_pr	= $("input[name=ocupacion_pr]:checked").attr('id'); // Nombre Ocupacion
				var condicion_trabajo_pr = $("input[name=condicion_trabajo_pr]:checked").attr('value');
				

				//CLAVES
				var tipo							= $("input[name=tipo]:checked").attr('value');
				var rango_edad						= $("input[name=rango_edad]:checked").attr('value');
					var rango_edad_name					= $("input[name=rango_edad]:checked").attr('id'); // Nombre rango edad
				var estado_civil 					= $("input[name=estado_civil]:checked").attr('value');
					var estado_civil_name				= $("input[name=estado_civil]:checked").attr('id'); //	Nombre Estado Civil
				var lugar_procedencia 				= $("input[name=lugar_procedencia]:checked").attr('value');
				var grado_instruccion 				= $("input[name=grado_instruccion]:checked").attr('value');
					var grado_instruccion_name			= $("input[name=grado_instruccion]:checked").attr('id'); //	Nombre Grado Instruccion
				var ocupacion 						= $("input[name=ocupacion]:checked").attr('value');
					var ocupacion_name					= $("input[name=ocupacion]:checked").attr('id'); // Nombre Ocupacion
				var condicion_trabajo				= $("input[name=condicion_trabajo]:checked").attr('value');
					var condicion_trabajo_name			= $("input[name=condicion_trabajo]:checked").attr('id'); // Nombre Condicion Trabajo					
				var situacion_vivienda 				= $("input[name=situacion_vivienda]:checked").attr('value');
				var hacinamiento 					= $("input[name=hacinamiento]:checked").attr('value');
				var personas_x_dormitorio 			= $("input[name=personas_x_dormitorio]:checked").attr('value');
				var material_contruccion 			= $("input[name=material_contruccion]:checked").attr('value');
					var material_contruccion_name		= $("input[name=material_contruccion]:checked").attr('id'); // Nombre Material Construccion				
				var ingreso_economico 				= $("input[name=ingreso_economico]:checked").attr('value');
				var condicion_ingreso 				= $("input[name=condicion_ingreso]:checked").attr('value');
				var composicion_familiarr 			= $("input[name=composicion_familiarr]:checked").attr('value');
				var problemas_sociales 				= $("input[name=problemas_sociales]:checked").attr('value');
				var salud_familiar 					= $("input[name=salud_familiar]:checked").attr('value');
				var servicion_basicos_Tiene_Agua 	= $("input[name=servicion_basicos_Tiene_Agua]:checked").attr('value');
				var servicion_basicos_Tiene_Desague = $("input[name=servicion_basicos_Tiene_Desague]:checked").attr('value');
				var servicion_basicos_Tiene_Luz 	= $("input[name=servicion_basicos_Tiene_Luz]:checked").attr('value');
						
							
				var codAsistenta =$("#idempleado_asistente").text();

				/*console.log('HistoriaClinica: ' + HistoriaClinica + '\n' +
							'numichasocial: ' + numichasocial + '\n' +
							'FechaIngreso: ' + FechaIngreso + '\n' +
							'idpaciente: ' + idpaciente + '\n' +
							'letra: ' + letra + '\n' +
							'numerodemiembros: ' + numerodemiembros + '\n' +
							'numerodedormitorios: ' + numerodedormitorios + '\n' +
							'puntaje_total: ' + puntaje_total + '\n' +
							'otrosproblemassociales: ' + otrosproblemassociales + '\n' +
							'otrossaludfamiliar: ' + otrossaludfamiliar + '\n' +
							'diagnosticosocial: ' + diagnosticosocial + '\n' +
							'tratamientosocial: ' + tratamientosocial + '\n' +
							'cuenta: ' + cuenta + '\n' +
							'direccionActualizar: ' + direccionActualizar + '\n' +
							'distritoProcedencia: ' + distritoProcedencia + '\n' +
							'telefonoPacientes: ' + telefonoPacientes + '\n' +
							'personaresponsable_paterno: ' + personaresponsable_paterno + '\n' +
							'personaresponsable_materno: ' + personaresponsable_materno + '\n' +
							'personaresponsable_nombres: ' + personaresponsable_nombres + '\n' +
							'parentesco_pr: ' + parentesco_pr + '\n' +
							'direccion_pr: ' + direccion_pr + '\n' +
							'edadpersonares: ' + edadpersonares + '\n' +
							'estado_civil_pr: ' + estado_civil_pr + '\n' +
							'grado_instruccion_pr: ' + grado_instruccion_pr + '\n' +
							'ocupacion_pr: ' + ocupacion_pr + '\n' +
							'estado_civil: ' + estado_civil + '\n' +
							'lugar_procedencia: ' + lugar_procedencia + '\n' +
							'grado_instruccion: ' + grado_instruccion + '\n' +
							'ocupacion: ' + ocupacion + '\n' +
							'condicion_trabajo: ' + condicion_trabajo + '\n' +
							'situacion_vivienda: ' + situacion_vivienda + '\n' +
							'hacinamiento: ' + hacinamiento + '\n' +
							'personas_x_dormitorio: ' + personas_x_dormitorio + '\n' +
							'material_contruccion: ' + material_contruccion + '\n' +
							'ingreso_economico: ' + ingreso_economico + '\n' +
							'condicion_ingreso: ' + condicion_ingreso + '\n' +
							'composicion_familiarr: ' + composicion_familiarr + '\n' +
							'problemas_sociales: ' + problemas_sociales + '\n' +
							'salud_familiar: ' + salud_familiar + '\n' +
							'servicion_basicos_Tiene_Agua: ' + servicion_basicos_Tiene_Agua + '\n' +
							'servicion_basicos_Tiene_Desague: ' + servicion_basicos_Tiene_Desague + '\n' +
							'servicion_basicos_Tiene_Luz: ' + servicion_basicos_Tiene_Luz + '\n');
				return false;*/
				
				//obteniendo datos de tabla
				$.ajax({
					url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=GenerarFichaSocial',
					type: 'POST',
					dataType: 'json',
					data: {
						problemas_sociales_check: problemas_sociales_check,
						numichasocial : numichasocial,
						FechaIngreso : FechaIngreso,
						idpaciente : idpaciente,
						letra : letra,
						numerodemiembros : numerodemiembros,
						numerodedormitorios : numerodedormitorios,
						puntaje_total : puntaje_total,
						otrosproblemassociales : otrosproblemassociales,
						otrossaludfamiliar : otrossaludfamiliar,
						diagnosticosocial : diagnosticosocial,
						tratamientosocial : tratamientosocial,
						diagnosticomedico : diagnosticomedico,
						cuenta : cuenta,
						personaresponsable_paterno : personaresponsable_paterno,
						personaresponsable_materno : personaresponsable_materno,
						personaresponsable_nombres : personaresponsable_nombres,
						parentesco_pr : parentesco_pr,
						direccion_pr : direccion_pr,
						edadpersonares : edadpersonares,
						estado_civil_pr : estado_civil_pr,
						estado_civil_name_pr : estado_civil_name_pr,
						grado_instruccion_pr : grado_instruccion_pr,
						grado_instruccion_name_pr: grado_instruccion_name_pr,
						ocupacion_pr : ocupacion_pr,
						ocupacion_name_pr:ocupacion_name_pr,
						condicion_trabajo_pr : condicion_trabajo_pr,
						estado_civil : estado_civil,
						estado_civil_name : estado_civil_name,
						lugar_procedencia : lugar_procedencia,
						grado_instruccion : grado_instruccion,
						grado_instruccion_name: grado_instruccion_name,
						ocupacion : ocupacion,
						ocupacion_name:ocupacion_name,
						condicion_trabajo : condicion_trabajo,
						condicion_trabajo_name: condicion_trabajo_name,
						situacion_vivienda : situacion_vivienda,
						hacinamiento : hacinamiento,
						personas_x_dormitorio : personas_x_dormitorio,
						material_contruccion : material_contruccion,
							material_contruccion_name: material_contruccion_name,
						ingreso_economico : ingreso_economico,
						condicion_ingreso : condicion_ingreso,
						composicion_familiarr : composicion_familiarr,
						problemas_sociales : problemas_sociales,
						salud_familiar : salud_familiar,
						servicion_basicos_Tiene_Agua : servicion_basicos_Tiene_Agua,
						servicion_basicos_Tiene_Desague : servicion_basicos_Tiene_Desague,
						servicion_basicos_Tiene_Luz : servicion_basicos_Tiene_Luz,
						direccionActualizar : direccionActualizar,
						distritoProcedencia : distritoProcedencia,
						telefonoPacientes1 : telefonoPacientes1,
						telefonoPacientes2 : telefonoPacientes2,
						codAsistenta: codAsistenta,
						codAsistenta: codAsistenta,
						rango_edad: rango_edad,
							rango_edad_name: rango_edad_name,
						tipo: tipo,
						ProblemasSocialestotal: ProblemasSocialestotal,
						Saludfamiliartotal: Saludfamiliartotal
					},
					success: function(data)
					{
						if (data == 'M_1') {
							var NroHistoriaClinica = $("#Numerocuenta").val();
							Mostrar_Numero_de_Ficha(NroHistoriaClinica);
							$("input[type=checkbox], input[type=radio]").prop("checked", false);
							$("input[name=sexo]").removeAttr('checked');
							//Limpiar Cuadro de TextArea
							tinymce.get('diagnosticosocial_V2').setContent('');
							tinymce.get('tratamientosocial_V2').setContent('');
							//Limpiar Cuadro de Parientes
							$('#tt').datagrid('loadData', []); 
							//Limpiar Input
							$("#otrossaludfamiliar").textbox('clear');
							$("#nombres_asistente").textbox('clear');
							$("#otrosproblemassociales").textbox('clear');
							$("#Numerocuenta").textbox('clear');
							$("#numichasocial").textbox('clear');
							$("#nombrepaciente").textbox('clear');
							$("#tipopaciente").textbox('clear');
							$("#tipoatencion").textbox('clear');
							$("#dnipaciente").textbox('clear');
							$("#direccionPaciente").textbox('clear');
							$("#telefonopaciente_I").textbox('clear');
							$("#telefonopaciente_II").textbox('clear');
							$("#edadpaciente").textbox('clear');
							$("#seguro").textbox('clear');
							$("#puntaje_total").textbox('clear');
							$("#marca_puntaje_total").val(' ');
							$("#calificacion").textbox('clear');
							$("#diagnosticosocial").textbox('clear');
							$("#tratamientosocial").textbox('clear');
							$("#diagnosticomedico").textbox('clear');
							$("#codigo_validacion_asistente").textbox('clear');
							$("#nombres_asistente_validacion").textbox('clear');
							$("#distritosPacientesProcedencias").combobox('clear');
							$("#departamentoPacienteProcedencia").combobox('clear');
							$("#provinciasPacienteProcedencia").combobox('clear');
							$("#direccionPacienteProcedencia").textbox('clear');		
							$("#distritoPaciente").textbox('clear');
							$("#direccionPaciente").textbox('clear');
							$("#personaresponsable_paterno").textbox('clear');
							$("#personaresponsable_materno").textbox('clear');
							$("#personaresponsable_nombres").textbox('clear');
							$("#parentesco_pr").combobox('clear');
							$("#direccion_pr").textbox('clear');
							$("#edadpersonares").textbox('clear');
							$("#ProblemasSocialestotal").textbox('clear');
							$("#Saludfamiliartotal").textbox('clear');
							$("#numerodemiembros").textbox('clear');
							$("#numerodedormitorios").textbox('clear');
							return false;
						}

						else
						{
							$.messager.alert('SIGESA','Error de sistema, comuniquese con los administradores','error');
						}
					}
				});
			});
				
		
		$("#ImprimirFichaSocial").click(function(event) {
			var numerocuenta = $("#Numerocuenta").val();
			window.open('../../MVC_Vista/Servicio_Social/pdf.ficha_social.php?numerocuenta='+numerocuenta);
			return 0;
		});
				

		$("#Busqueda_Paciente_Datos").click(function(event) {
			var Apellido_Paterno_Paciente 		= 	$("#Apellido_Paterno_Paciente").textbox('getText');
			var Apellido_Materno_Paciente		= 	$("#Apellido_Materno_Paciente").textbox('getText');
			var DNI_Paciente 					= 	$("#DNI_Paciente").textbox('getText');
				var dg =$('#lista_pacientes').datagrid({
				iconCls:'icon-edit',
				singleSelect:true,
				rownumbers:true,
				remoteSort:false,
				url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarLista_de_Pacientes&ApellidoPaterno='+Apellido_Paterno_Paciente+'&ApellidoMaterno='+Apellido_Materno_Paciente+'&DNI_Paciente='+DNI_Paciente,
				columns:[[
				{field:'ApellidoPaterno',title:'Apellido Paterno',width:140},			
				{field:'ApellidoMaterno',title:'Apellido Materno',width:140},
				{field:'PrimerNombre',title:'Primer Nombre',width:120},
				{field:'SegundoNombre',title:'Segundo Nombre',width:120},
				{field:'NroHistoriaClinica',title:'Historia Clinica',width:100,sortable:true}	
				]],
				onSelect:function(index,row){
				$("#Numerocuenta").textbox('setText',row.NroHistoriaClinica);
				$('#w').window('close');
				$("#Apellido_Paterno_Paciente").textbox('clear');
				$("#Apellido_Materno_Paciente").textbox('clear');
				$("#DNI_Paciente").textbox('clear');
 				$('#lista_pacientes').datagrid('loadData', {"total":0,"rows":[]}); 
				}	
				});

		});


		$("#Limpiar_Busqueda_Paciente_Datos").click(function(event) {
				$("#Apellido_Paterno_Paciente").textbox('clear');
				$("#Apellido_Materno_Paciente").textbox('clear');
				$("#DNI_Paciente").textbox('clear');

		});
				

		$("#LimpiarCajasdeTextoFichaSocial").click(function(event) {
				$("input[name=sexo]").removeAttr('checked');
				//Limpiar Cuadro de TextArea
				tinymce.get('diagnosticosocial_V2').setContent('');
				tinymce.get('tratamientosocial_V2').setContent('');
				$("input[type=checkbox], input[type=radio]").prop("checked", false);
				//Limpiar Cuadro de Parientes
				$('#tt').datagrid('loadData', []); 
				//Limpiar Input
				$("#otrossaludfamiliar").textbox('clear');
				$("#nombres_asistente").textbox('clear');
				$("#otrosproblemassociales").textbox('clear');
			    $("#numichasocial").textbox('clear');
			    $("#Numerocuenta").textbox('clear');
				$("#nombrepaciente").textbox('clear');
				$("#tipopaciente").textbox('clear');
				$("#tipoatencion").textbox('clear');
				$("#dnipaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#telefonopaciente_I").textbox('clear');
				$("#telefonopaciente_II").textbox('clear');
				$("#edadpaciente").textbox('clear');
				$("#seguro").textbox('clear');
				$("#puntaje_total").textbox('clear');
				$("#marca_puntaje_total").val(' ');
				$("#calificacion").textbox('clear');
				$("#distritosPacientesProcedencias").combobox('clear');
				$("#departamentoPacienteProcedencia").combobox('clear');
				$("#provinciasPacienteProcedencia").combobox('clear');
				$("#direccionPacienteProcedencia").textbox('clear');		
				$("#departamentoPaciente").textbox('clear');
				$("#provinciaPaciente").textbox('clear');
				$("#distritoPaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#personaresponsable_paterno").textbox('clear');
				$("#personaresponsable_materno").textbox('clear');
				$("#personaresponsable_nombres").textbox('clear');
				$("#parentesco_pr").combobox('clear');
				$("#direccion_pr").textbox('clear');
				$("#edadpersonares").textbox('clear');
				$("#numerodedormitorios").textbox('clear');
				$("#numerodemiembros").textbox('clear');
				$("#diagnosticosocial").textbox('clear');
				$("#tratamientosocial").textbox('clear');
				$("#diagnosticomedico").textbox('clear');
				$("#nombres_asistente").textbox('clear');
        		$('input[name=grado_instruccion]').prop('checked', false);
	        	$('input[name=composicion_familiarr]').prop('checked', false);
	        	$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=hacinamiento]').prop('checked', false);
				$('input[name=situacion_vivienda]').prop('checked', false);
				$('input[name=condicion_trabajo_pr]').prop('checked', false);
				$('input[name=ocupacion_pr]').prop('checked', false);
				$('input[name=grado_instruccion_pr]').prop('checked', false);
				$('input[name=estado_civil_pr]').prop('checked', false);
				$('input[name=condicion_trabajo]').prop('checked', false);
				$('input[name=rango_edad]').prop('checked', false);
				$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=material_contruccion]').prop('checked', false);
				$('input[name=ingreso_economico]').prop('checked', false);
				$('input[name=condicion_ingreso]').prop('checked', false);
				$('input[name=lugar_procedencia]').prop('checked', false);
				$('input[name=ocupacion]').prop('checked', false);
				$('input[name=estado_civil]').prop('checked', false);
				$('input[name=sexo]').prop('checked', false);
				$('input[name=tipo]').prop('checked', false);
				$('input[name=problemas_sociales]').prop('checked', false);
				$('input[name=salud_familiar]').prop('checked', false);
				$("#numerodemiembros").textbox('clear');
				$("#numerodedormitorios").textbox('clear');

		});

         
		//Limpiar

        function LimpiarCajasdeTextoFichaSocial(){
        		$("#numichasocial").textbox('clear');
				$("#nombrepaciente").textbox('clear');
				$("#tipopaciente").textbox('clear');
				$("#tipoatencion").textbox('clear');
				$("#dnipaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#telefonopaciente_I").textbox('clear');
				$("#telefonopaciente_II").textbox('clear');
				$("#edadpaciente").textbox('clear');
				$("#seguro").textbox('clear');
				$("#puntaje_total").textbox('clear');
				$("#marca_puntaje_total").val(' ');
				$("#calificacion").textbox('clear');
				$("#distritosPacientesProcedencias").combobox('clear');
				$("#departamentoPacienteProcedencia").combobox('clear');
				$("#provinciasPacienteProcedencia").combobox('clear');
				$("#direccionPacienteProcedencia").textbox('clear');		
				$("#departamentoPaciente").textbox('clear');
				$("#provinciaPaciente").textbox('clear');
				$("#distritoPaciente").textbox('clear');
				$("#direccionPaciente").textbox('clear');
				$("#personaresponsable_paterno").textbox('clear');
				$("#personaresponsable_materno").textbox('clear');
				$("#personaresponsable_nombres").textbox('clear');
				$("#parentesco_pr").combobox('clear');
				$("#direccion_pr").textbox('clear');
				$("#edadpersonares").textbox('clear');
				$("#numerodedormitorios").textbox('clear');
				$("#numerodemiembros").textbox('clear');
				$("#diagnosticosocial").textbox('clear');
				$("#tratamientosocial").textbox('clear');
				$("#diagnosticomedico").textbox('clear');
				$("#nombres_asistente").textbox('clear');
        		$('input[name=grado_instruccion]').prop('checked', false);
	        	$('input[name=composicion_familiarr]').prop('checked', false);
	        	$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=hacinamiento]').prop('checked', false);
				$('input[name=situacion_vivienda]').prop('checked', false);
				$('input[name=condicion_trabajo_pr]').prop('checked', false);
				$('input[name=ocupacion_pr]').prop('checked', false);
				$('input[name=grado_instruccion_pr]').prop('checked', false);
				$('input[name=estado_civil_pr]').prop('checked', false);
				$('input[name=condicion_trabajo]').prop('checked', false);
				$('input[name=rango_edad]').prop('checked', false);
				$('input[name=personas_x_dormitorio]').prop('checked', false);
				$('input[name=material_contruccion]').prop('checked', false);
				$('input[name=ingreso_economico]').prop('checked', false);
				$('input[name=condicion_ingreso]').prop('checked', false);
				$('input[name=lugar_procedencia]').prop('checked', false);
				$('input[name=ocupacion]').prop('checked', false);
				$('input[name=estado_civil]').prop('checked', false);
				$('input[name=sexo]').prop('checked', false);
				$('input[name=tipo]').prop('checked', false);
				$('input[name=problemas_sociales]').prop('checked', false);
				$('input[name=salud_familiar]').prop('checked', false);
				$("#numerodemiembros").textbox('clear');
				$("#numerodedormitorios").textbox('clear');
        } 

		//Mostrar el Numero de la Ficha Social						
				function Mostrar_Numero_de_Ficha(NroHistoriaClinica)
				{		
						$.post('../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php',
						{ 
						NroHistoriaClinica:NroHistoriaClinica,
						acc: 'BuscarFichaSocial_por_HistoriaClinica'
						},
						 function(res)
						{
							parsedRes = $.parseJSON(res);
							var parsedRes = $.parseJSON(res);  		
							var NumFichaSocial=parsedRes.NumFichaSocial;	
							$.messager.alert('SIGESA','Ficha Generada con Exito  Nº  '+NumFichaSocial);
						}										
						);

				}		
				
				
		});
	</script>
</head>
<body>
	<div class="easyui-panel"  style="width:100%;height:100%;">
		<!-- CONTENEDORES -->
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'north'" style="height:15%;padding:1%;text-align: center;" title="FICHA SOCIAL  (Version 3.0)">
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:15%;height:70%;" id="ModificarFicha">MODIFICAR FICHA</a>
            	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:15%;height:70%;" id="MostrarFichaNueva">NUEVA FICHA SOCIAL</a>
            	<a class="easyui-linkbutton" id="" style="width:15%;height:70%;"><img src="../../MVC_Complemento/img/pdf.png" height="15" width="15" alt="">&nbsp;REPORTES</a> 
            </div>
			<div id="w" class="easyui-window" title="Busqueda de Pacientes" data-options="iconCls:'icon-save'" style="width:880px;height:500px;padding:10px;">
			        <table>
			        	<tr>
			        		<td>
			        			<strong>Ape. Paterno :</strong>
			        		</td>
			        		<td>
			        			<input type="text" class="easyui-textbox" data-options="prompt:'APELLIDO PATERNO'" id="Apellido_Paterno_Paciente">
			        		</td>
			        		<td>
			        			<strong>Ape. Materno :</strong>
			        		</td>
			        		<td>
			        			<input type="text" class="easyui-textbox" data-options="prompt:'APELLIDO MATERNO'" id="Apellido_Materno_Paciente">
			        		</td>
			        		<td>
			        			<strong>Nro Documento :</strong>
			        		</td>
			        		<td>
			        			<input type="text" class="easyui-textbox" data-options="prompt:'NUMERO DE DNI'" id="DNI_Paciente">
			        		</td>
			        		<td>
			        		   <a href="#" class="easyui-linkbutton"  style="width:50px;height: 25spx;" id="Busqueda_Paciente_Datos">Buscar</a>
			        		</td>
			        		<td>
			        		   <a href="#" class="easyui-linkbutton"  style="width:50px;height: 25spx;" id="Limpiar_Busqueda_Paciente_Datos">Limpiar</a>
			        		</td>
			        		
			        	</tr>
			        </table>
			        <table id="lista_pacientes"></table>
			</div>
            <div data-options="region:'center',split:false" style="height:85%; padding:5px;text-align: center;" >
            	<!-- FICHA SOCIAL -->
            	<div id="ficha_social" style="padding: 1%">
            		<div class="easyui-panel" title="FICHA SOCIAL || NUEVA HOJA" style="padding:0%;width: 100%;">
            			<table cellpadding="5">
	            			<tr>
	            				<td>
	            					<input type="text" class="easyui-numberbox" data-options="prompt:'HISTORIA CLINICA'" id="Numerocuenta">
	            					<input type="hidden" id="cuenta">
	            				</td>
	            				<td>
	            					<a href="#" class="easyui-linkbutton"   onclick="$('#w').window('open')" style="width:50px;height: 25spx;">...</a>
	            					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            				</td>
	            				<td style="color: red;">
	            					<strong>Ficha Social N°:</strong>   
	            				</td>
	            				<td >
	            					<input type="text" disabled class="easyui-textbox" data-options="prompt:'N° FICHA SOCIAL'" style="color: red;" id="numichasocial">    
	            				</td>
	            				<td style="color: blue;">
	            					<strong>Fecha Ingreso:</strong>
	            				</td>
	            				<td >
	            					<input type="text" class="easyui-textbox" value="<?php echo date('Y-m-d H:i:s')?>" style="color: blue;" id="FechaIngreso" disabled>
	            				</td>
	            				<td>
	            					<a href="#" class="easyui-linkbutton c6" id="MostrarFichaNuevabtn" data-options="iconCls:'icon-reload',disabled:true"> RECATEGORIZACION </a>
	            				</td>
	            			</tr>
	            			<tr>
	            				<td colspan="2">
	            					<input type="hidden" id="idpaciente">
	            					<input type="text" class="easyui-textbox" style="width: 100%;" data-options="prompt:'PACIENTE'" id="nombrepaciente" disabled>
	            				</td>            					
	            				<td>
	            					Tipo:
	            				</td>
	            				<td>
	            					<input type="text" class="easyui-textbox" data-options="prompt:'TIPO PACIENTE'" id="tipopaciente" disabled>
	            				</td>
	            				<td>
	            					Seguro:
	            				</td>
	            				<td>
	            					<input type="text" class="easyui-textbox" data-options="prompt:'SEGURO'" id="seguro" disabled>
	            				</td>
	            				<td>
	            					<input type="text" class="easyui-textbox" data-options="prompt:'CONSULTA EXTERNA'" id="tipoatencion" disabled>
	            				</td>
	            			</tr>
            			</table>

	    				<div class="easyui-tabs" style="width:100%;margin-top: 1%;">
					       	<div title="DATOS PERSONALES" style="padding:10px">
								<table cellpadding="5">
                                <tr>
										<?php
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(0);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(0);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='tipo' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
										?>
					    				<input type="text" class="easyui-textbox" style="width: 20%;" id="nombres_asistente" disabled="disabled">
					    			
									</tr>
									<tr>
										<td><strong>Sexo:</strong></td>
										<td colspan="2">
											<input type="radio" name="sexo" id="Masculino_sexo">
											<label for="Masculino_sexo">Masculino</label>
										
											<input type="radio" name="sexo" id="Femenino_sexo">
											<label for="Femenino_sexo">Femenino</label>
										</td>
										<td>
											<strong>N° DNI:</strong>
										</td>
										<td>
											<input type="text" disabled class="easyui-textbox"  id="dnipaciente">
										</td>
									</tr>
									<tr>
										<td><strong><u>DIRECCION</u></strong></td>
									</tr>
									<tr>
										<td><strong>Departamento:</strong></td>
										<td><input type="text" class="easyui-textbox" disabled	id="departamentoPaciente" ></td>

										<td><strong>Provincia: </strong></td>
										<td><input type="text" class="easyui-textbox" disabled	id="provinciaPaciente"></td>

										<td><strong>Distrito:  &nbsp;&nbsp;</strong><input type="text" class="easyui-textbox" disabled	id="distritoPaciente"></td>
									</tr>
									<tr>
										<td>
											<strong>Direccion:</strong>
										</td>
										<td colspan="4">
											<input type="text" class="easyui-textbox" style="width: 100%;"  id="direccionPaciente" disabled>
										</td>
									</tr>
									<tr>
										<td><strong><u>LUGAR DE PROCEDENCIA</u></strong></td>
									</tr>
									<tr>
										<td><strong>Departamento:</strong></td>
										<td>
											<input id="departamentoPacienteProcedencia" class="easyui-combobox" data-options="
									        valueField: 'id',
									        textField: 'text',
									        url: '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarDepartamentoCombo',
									        onSelect: function(rec){
									            var url = '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarProvinciasCombo&idDepartamento='+rec.id;
									            $('#provinciasPacienteProcedencia').combobox('reload', url);
									        }">
										</td>

										<td><strong>Provincia: </strong></td>
										<td>
											<input id="provinciasPacienteProcedencia" class="easyui-combobox" data-options="
											valueField: 'id',
									        textField: 'text',
									        onSelect: function(rec){
									            var url = '../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarDistritosCombo&idProvincias='+rec.id;
									            $('#distritosPacientesProcedencias').combobox('reload', url);
									        }
											">
										</td>

										<td><strong>Distrito:  &nbsp;&nbsp;</strong>
											<input class="easyui-combobox" id="distritosPacientesProcedencias" data-options="valueField: 'id',
									        textField: 'text'">
										</td>
									</tr>
									<tr>
										<td>
											<strong>Direccion:</strong>
										</td>
										<td colspan="4">
											<input type="text" class="easyui-textbox" style="width: 100%;"  id="direccionPacienteProcedencia">
										</td>
									</tr>
									<tr>
										<?php
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(1);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(1);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='estado_civil' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
										?>
									</tr>
									<tr>
										<td><strong>Telefono:</strong></td>
										<td><input type="text" class="easyui-numberbox"  id="telefonopaciente_I" ></td>
										<td><input type="text" class="easyui-numberbox"  id="telefonopaciente_II"></td>
										<td><strong>Edad:</strong></td>
										<td><input type="text" class="easyui-textbox"  id="edadpaciente"  readonly="readonly"></td>

										<?php 
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(2);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";

											echo "<td colspan='3'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(2);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='lugar_procedencia' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label><br>"; 
												
											}
											echo "</td>";
										 ?>
									</tr>
                                     <tr>
										<?php
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(16);
											echo "<td><input type='hidden' name='rango_edad' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(16);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='rango_edad' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
										?>
									</tr>
										<td><strong>Diagnostico Medico:</strong></td>
										<td colspan="4"><input type="text" class="easyui-textbox" style="width: 100%;" id="diagnosticomedico"></td>
									<tr>
										
									</tr>
									<tr>
										<?php 
											$pregunta = MostrarPreguntas_x_Id_M(3);
											echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(3);
											for ($i=0; $i < count($opciones); $i++) { 
												#echo "<td>";
												echo "<input type='radio' name='grado_instruccion' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label><br>";
												#echo "</td>";
											}
											#PREGUNTA
											echo "</td>";

											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(4);
											echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(4);
											for ($i=0; $i < count($opciones); $i++) { 
												#echo "<td>";
												echo "<input type='radio' name='ocupacion' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label><br>";
												#echo "</td>";
											}
											echo "</td>";

											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(5);
											echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(5);
											for ($i=0; $i < count($opciones); $i++) { 
												#echo "<td>";
												echo "<input type='radio' name='condicion_trabajo' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label><br>";
												#echo "</td>";
											}
											echo "</td>";
										 ?>
									</tr>
									
								</table>

								<!--BOTONES-->
									
								
					       	</div>
					       	<div title="PERSONA RESPONSABLE" style="padding:10px">
					       		<table>
					       			<tr>
					       				<td>
					       					<strong>Persona Responsable:</strong>
					       				</td>
					       				<td colspan="2">
					       					<input type="text" class="easyui-textbox" id="personaresponsable_paterno" data-options="prompt:'Apellidos Paterno'" required>
					       					<input type="text" class="easyui-textbox" id="personaresponsable_materno" data-options="prompt:'Apellidos Materno'" required>
					       					<input type="text" class="easyui-textbox" id="personaresponsable_nombres" data-options="prompt:'Nombres'" required>
					       				</td>
					       				<td>
					       					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					       				</td>
					       				<td>
					       					<strong>Parentesco:</strong>
					       				</td>
					       				<td colspan="2">
					       					<input type="text" class="easyui-combobox" style="width: 100%;" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarTipoDeParentesco'" id="parentesco_pr">
					       				</td>
					       			</tr>
					       			<tr>
					       				<td>
					       					<strong>Direccion:</strong>
					       				</td>
					       				<td colspan="3" class="mayus">
					       					<input type="text" class="easyui-textbox" style="width: 100%;" id="direccion_pr" required data-options="prompt:'Direccion'">
					       				</td>
					       				<td>
					       					<strong>Edad:</strong>
					       				</td>
					       				<td>
					       					<input type="number" class="easyui-textbox" style="width: 100%;" id="edadpersonares">
					       				</td>
					       			</tr>
					       			<tr>

					       				<?php
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(1);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(1);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='estado_civil_pr' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."_pr'>";
												echo "<label for='".$opciones[$i]['OPCION']."_pr'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
										?>
					       			</tr>
					       			<tr>
					       				<?php 
					       			
					       					$pregunta = MostrarPreguntas_x_Id_M(3);
											echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(3);
											for ($i=0; $i < count($opciones); $i++) { 
												#echo "<td>";
												echo "<input type='radio' name='grado_instruccion_pr' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."_pr'>";
												echo "<label for='".$opciones[$i]['ID']."_pr'>".$opciones[$i]['OPCION']."</label><br>";
												#echo "</td>";
											}

											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(4);
											echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(4);
											for ($i=0; $i < count($opciones); $i++) { 
												#echo "<td>";
												echo "<input type='radio' name='ocupacion_pr' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."_pr'>";
												echo "<label for='".$opciones[$i]['ID']."_pr'>".$opciones[$i]['OPCION']."</label><br>";
												#echo "</td>";
											}
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(5);
											echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(5);
											for ($i=0; $i < count($opciones); $i++) { 
												#echo "<td>";
												echo "<input type='radio' name='condicion_trabajo_pr' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."_pr'>";
												echo "<label for='".$opciones[$i]['ID']."_pr'>".$opciones[$i]['OPCION']."</label><br>";
												#echo "</td>";
											}
											echo "</td>";
					       			 	?>
					       			</tr>
					       		</table>				            
					       	</div>
					       	<div title="SITUACION VIVIENDA" style="padding:10px">	
					       		<div style="width: 40%;display: inline-block;vertical-align: top;">
					       			<table cellpadding="5">
					       				<tr>
					       				<?php
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(6);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(6);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='situacion_vivienda' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
										?>
					       				</tr>
					       				<tr>
					       					<?php
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(7);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(7);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='hacinamiento' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
										?>
					       				</tr>
					       				<tr>
					       					<td>
					       						<strong>N° de miembros del hogar:</strong>
					       					</td>
					       					<td>
					       						<input class="easyui-numberspinner" data-options="min:0" style="width:80px;" id="numerodemiembros"></input>
					       					</td>
					       				</tr>
					       				<tr>
					       					<td>
					       						<strong>N° de dormitorios:</strong>
					       					</td>
					       					<td>
					       						<input class="easyui-numberspinner" data-options="min:0" style="width:80px;" id="numerodedormitorios"></input>
					       					</td>
					       				</tr>
					       				<tr>
					       					<?php
											#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(15);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(15);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='personas_x_dormitorio' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
										?>
					       				</tr>
					       				<tr>
					       					<?php 
					       						#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(8);
											echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(8);
											for ($i=0; $i < count($opciones); $i++) { 
												#echo "<td>";
												echo "<input type='radio' name='material_contruccion' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label><br>";
												#echo "</td>";
											}
											echo "</td>";
					       					 ?>
					       				</tr>
					       			</table>
					       		</div>
					       		<div style="width: 50%;display: inline-block;vertical-align: top;">
					       			<table cellpadding="5">
					       				<tr>
					       					<?php 
					       					#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(9);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(9);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='ingreso_economico' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
											 ?>
					       				</tr>
					       				<tr>
					       					<?php 
					       					#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(10);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(10);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='condicion_ingreso' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
											 ?>
					       				</tr>
										<script>
													function checkTodos()
													{
														$(".Si").each(function(){
															$(this).prop('checked',true);
														});
													}
										</script> 
					       				<tr>
					       					<?php 
					       						#PREGUNTA
												$pregunta = MostrarPreguntas_x_Id_M(11);
												echo "<td colspan='2' style='border:1px dotted #999;padding:1%;'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><a href='#' onclick='checkTodos()' style='color:red;text-decoration:none '>(***)</a><br>";

												#OPCIONES
												$opciones = MostrarOpciones_x_IdPregunta_M(11);
												for ($i=0; $i < count($opciones); $i++) { 
													#echo "<td>";
													#echo "<input type='radio' name='condicion_trabajo' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
													echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";

													#ALTERNATIVAS
													$alternativas = MostrarAlternativas_x_Pregunta_M(11,$opciones[$i]['IDPCION']);
													for ($j=0; $j < count($alternativas); $j++) { 
														echo "<input type='radio' class='".substr($alternativas[$j]['ID'], 0, 2)."' name='servicion_basicos_".$opciones[$i]['OPCION']."' value='".$alternativas[$j]['VALOR']."' id='".$alternativas[$j]['ID']."'>";
														echo "<label for='".$alternativas[$j]['ID']."'>".$alternativas[$j]['ALTERNATIVA']."</label>";
													}
													echo "<br>";
												}
												echo "</td>";
					       					 ?>
					       				</tr>
					       			</table>
					       		</div>			            
					       	</div>
					       	<div title="COMPOSICION FAMILIAR" style="padding:10px">	
					       		<table align="center" cellpadding="5">
					       			<tr>
					       				<?php 
					       					#PREGUNTA
											$pregunta = MostrarPreguntas_x_Id_M(12);
											echo "<td><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong></td>";
											echo "<td colspan='4'>";
											#OPCIONES
											$opciones = MostrarOpciones_x_IdPregunta_M(12);
											for ($i=0; $i < count($opciones); $i++) { 
												
												echo "<input type='radio' name='composicion_familiarr' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
												echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>";
												
											}
											echo "</td>";
											 ?>
					       			</tr>
					       		</table>

								<!-- Tabla para mostrar los Parientes del Paciente  -->
								<br>
								<div style="margin:10px 0">
										<a href="javascript:void(0)" class="easyui-linkbutton" onclick="insert()">Agregar Pariente</a>
										<a href="#" class="easyui-linkbutton" onclick="getSelected()">Eliminar Pariente</a>
								</div>
								<table id="tt"></table>
					       	</div>
					       	<div title="PROBLEMAS SOCIALES" style="padding:10px">
								<table cellpadding="5">
									<tr>
										<?php 
					       						#PREGUNTA
												$pregunta = MostrarPreguntas_x_Id_M(13);
												echo "<td colspan='2'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";
												#OPCIONES
												$opciones = MostrarOpciones_x_IdPregunta_M(13);
												for ($i=0; $i < count($opciones); $i++) { 
													#echo "<td>";
													echo "<input type='radio' name='problemas_sociales' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
													echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>&nbsp;";

													#ALTERNATIVAS		
													
												}
												
		
													echo "</td>";
					       					 ?>
									</tr>				
										<?php 
										$alternativas = MostrarAlternativas_x_Pregunta_M(13,57);
													echo "<tr>";
													$x = 0;
												//	for ($j=0; $j < count($alternativas); $j++) { 
														
														echo "<td><input type='checkbox' name='problemas_sociales_check_0' value='".$alternativas[0]['VALOR']."' id='".$alternativas[0]['ID']."'>";
														echo "<label for='".$alternativas[0]['ID']."'>".$alternativas[0]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_1' value='".$alternativas[1]['VALOR']."' id='".$alternativas[1]['ID']."'>";
														echo "<label for='".$alternativas[1]['ID']."'>".$alternativas[1]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_2' value='".$alternativas[2]['VALOR']."' id='".$alternativas[2]['ID']."'>";
														echo "<label for='".$alternativas[2]['ID']."'>".$alternativas[2]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_3' value='".$alternativas[3]['VALOR']."' id='".$alternativas[3]['ID']."'>";
														echo "<label for='".$alternativas[3]['ID']."'>".$alternativas[3]['ALTERNATIVA']."</label></td>";echo "</tr><tr>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_4' value='".$alternativas[4]['VALOR']."' id='".$alternativas[4]['ALTERNATIVA']."'>";
														echo "<label for='".$alternativas[4]['ID']."'>".$alternativas[4]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_5' value='".$alternativas[5]['VALOR']."' id='".$alternativas[5]['ID']."'>";
														echo "<label for='".$alternativas[5]['ID']."'>".$alternativas[5]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_6' value='".$alternativas[6]['VALOR']."' id='".$alternativas[6]['ID']."'>";
														echo "<label for='".$alternativas[6]['ID']."'>".$alternativas[6]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_7' value='".$alternativas[7]['VALOR']."' id='".$alternativas[7]['ID']."'>";
														echo "<label for='".$alternativas[7]['ID']."'>".$alternativas[7]['ALTERNATIVA']."</label></td>";echo "</tr><tr>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_8' value='".$alternativas[8]['VALOR']."' id='".$alternativas[8]['ID']."'>";
														echo "<label for='".$alternativas[8]['ID']."'>".$alternativas[8]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_9' value='".$alternativas[9]['VALOR']."' id='".$alternativas[9]['ID']."'>";
														echo "<label for='".$alternativas[9]['ID']."'>".$alternativas[9]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='problemas_sociales_check_10' value='".$alternativas[10]['VALOR']."' id='".$alternativas[10]['ID']."'>";
														echo "<label for='".$alternativas[10]['ID']."'>".$alternativas[10]['ALTERNATIVA']."</label></td>";
														$x++;
														if ($x == 3) {
															echo "</tr><tr>";
															$x = 0;
													//	}
													} 
										?>
										<td>
											<input type="text" class="easyui-textbox" style="width: 100%;" id="otrosproblemassociales">
                                            <input type="hidden" class="easyui-textbox" id="ProblemasSocialestotal" >
										</td>
										<tr>
										<?php 
					       						#PREGUNTA
												$pregunta = MostrarPreguntas_x_Id_M(14);
												echo "<td colspan='2'><input type='hidden' value='".$pregunta[0]['IDPREGUNTA']."'><strong>".$pregunta[0]['PREGUNTA'].":</strong><br>";

												#OPCIONES
												$opciones = MostrarOpciones_x_IdPregunta_M(14);
												for ($i=0; $i < count($opciones); $i++) { 
													#echo "<td>";
													echo "<input type='radio' name='salud_familiar' value='".$opciones[$i]['VALOR']."' id='".$opciones[$i]['ID']."'>";
													echo "<label for='".$opciones[$i]['ID']."'>".$opciones[$i]['OPCION']."</label>&nbsp;";

													#ALTERNATIVAS
													
												}
												

													echo "</td>";
					       					 ?>
									</tr>
										<?php 
										$alternativas = MostrarAlternativas_x_Pregunta_M(14,61);
													echo "<tr>";
													$x = 0;
											//		for ($j=0; $j < count($alternativas); $j++) { 
														
														echo "<td><input type='checkbox' name='salud_familiar_check_0' value='".$alternativas[0]['VALOR']."' id='".$alternativas[0]['ID']."'>";
														echo "<label for='".$alternativas[0]['ID']."'>".$alternativas[0]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='salud_familiar_check_1' value='".$alternativas[1]['VALOR']."' id='".$alternativas[1]['ID']."'>";
														echo "<label for='".$alternativas[1]['ID']."'>".$alternativas[1]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='salud_familiar_check_2' value='".$alternativas[2]['VALOR']."' id='".$alternativas[2]['ID']."'>";
														echo "<label for='".$alternativas[2]['ID']."'>".$alternativas[2]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='salud_familiar_check_3' value='".$alternativas[3]['VALOR']."' id='".$alternativas[3]['ID']."'>";
														echo "<label for='".$alternativas[3]['ID']."'>".$alternativas[3]['ALTERNATIVA']."</label></td>";echo"</tr><tr>";
														echo "<td><input type='checkbox' name='salud_familiar_check_4' value='".$alternativas[4]['VALOR']."' id='".$alternativas[4]['ID']."'>";
														echo "<label for='".$alternativas[4]['ID']."'>".$alternativas[4]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='salud_familiar_check_5' value='".$alternativas[5]['VALOR']."' id='".$alternativas[5]['ID']."'>";
														echo "<label for='".$alternativas[5]['ID']."'>".$alternativas[5]['ALTERNATIVA']."</label></td>";
														echo "<td><input type='checkbox' name='salud_familiar_check_6' value='".$alternativas[6]['VALOR']."' id='".$alternativas[6]['ID']."'>";
														echo "<label for='".$alternativas[6]['ID']."'>".$alternativas[6]['ALTERNATIVA']."</label></td>";"</tr><tr>";
														$x++;
														if ($x == 3) {
															echo "</tr><tr>";
															$x = 0;
												//		}
													} 
										?>
										<td>
											<input type="text" class="easyui-textbox" style="width: 100%;" id="otrossaludfamiliar">
                                            <input type="hidden" class="easyui-textbox" id="Saludfamiliartotal" >
										</td>
								</table>
					       	</div>
					       	<div title="DIAGNOSTICO SOCIAL" style="padding:10px">
					       		<table cellspacing="5">
					       			<tr>
					       				<td>
					       					<strong>Diagnostico social:</strong>
					       				</td>
					       			</tr>
					       			<tr>
					       				<td>
					       				<!--<input class="easyui-textbox" data-options="multiline:true" style="width:900px;height:200px" id="diagnosticosocial">-->
					       							<textarea rows="5" cols="80" name="diagnosticosocial_V2"  style="width: 700px; height: 210px;">
													</textarea> 
					       				</td>
					       			</tr>
					       		</table>				            
					       	</div>
					       	<div title="TRATAMIENTO SOCIAL" style="padding:10px">	
					       	<table cellspacing="5">
					       			<tr>
					       				<td>
					       					<strong>Tratamiento Social:</strong>
					       				</td>
					       			</tr>
					       			<tr>
					       				<td>
					       					<!--<input class="easyui-textbox" data-options="multiline:true" style="width:900px;height:200px" id="tratamientosocial">-->
					       					        <textarea rows="5" cols="80" name="tratamientosocial_V2"  style="width: 700px; height: 210px;">
													</textarea> 
					       				</td>
					       			</tr>
					       		</table>			            
						    </div>				        
					    </div>
					    <br>
					    <div style="float: left;border:1px dotted #999;padding:1%;">
					    	<table cellpadding="5">
					    		<tr>
					    			<td><strong>Asistente:</strong></td>
					    			<td>
					    				<input type="password" class="easyui-textbox" style="width: 95%;" id="codigo_validacion_asistente">
					    				<input type="hidden" id="idempleado_asistente">
					    			</td>
					    			<td colspan="2">
					    				<input type="text" class="easyui-textbox" style="width: 100%;" id="nombres_asistente_validacion">
					    			</td>
					    		</tr>
					    		
					    		<tr>
					    			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:96px;height: 50px;" id="RegresarFichaSocial">REGRESAR</a></td>
					    			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:96px;height: 50px;" id="GuardarFichaSocial">GUARDAR</a></td>
					    			<td><a href="" class="easyui-linkbutton" data-options="iconCls:'icon-print'" style="width:96px;height: 50px;" id="ImprimirFichaSocial">IMPRIMIR</a></td>
					    			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="LimpiarCajasdeTextoFichaSocial">LIMPIAR</a></td>
					    			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="SalirFichaSocial">SALIR</a></td>
					    			
					    		</tr>
					    	</table>
					    </div>
					    <div style="float: left;border:1px dotted #999;padding:1%; margin-left: 5%;">
					    	<table cellpadding="5">
					    		<tr>
					    			<td>
					    				<strong style="color: red;">Puntaje Total:</strong>
					    			</td>
					    			<td style="width: 200px;">
					    				<input type="text" class="easyui-textbox" style="color: red;text-align: right;" id="puntaje_total" >
					    				<input type="hidden" id="marca_puntaje_total">
					    			</td>
					    		</tr>
					    		<tr>
					    			<td>
					    				<strong style="color: blue;">
					    					Calificacion:
					    				</strong>
					    			</td>
					    			<td style="width: 200px;">
					    				<input type="text" class="easyui-textbox" style="color: blue;width: 100%;" id="calificacion" >
					    				<input type="hidden" id="letra">
					    			</td>
					    		</tr>
					    	</table>
					    </div>
            		</div>
            	</div>
            </div>

		</div>
	</div>
	<!-- REALIZA IMPRESION -->
	<input type="hidden" id="idUsuarioparaSigesaLI" value="<?php echo $_REQUEST['IdEmpleado']?>">

	<!-- INGRESAR PARIENTES -->
	<div id="NuevoFamiliar" class="easyui-window" title="Nuevo Familiar" data-options="modal:true,iconCls:'icon-man'" style="width:1000px;height:500px;padding:10px;">
    	<div class="easyui-layout" data-options="fit:true">
        	<div data-options="region:'west',split:true" style="width:50%;padding: 2%;">
        		<table cellspacing="10" style="margin-left: 5%;">
        			<tr>
        				<th>N° DNI</th>
        				<td>
        					<input type="text" class="easyui-textbox" id="numerdodnifamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Apellido Paterno:</th>
        				<td>
        					<input type="text" class="easyui-textbox" id="apaternofamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Apellido Materno</th>
        				<td>
        					<input type="text" class="easyui-textbox" id="amaternofamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Nombres:</th>
        				<td>
        					<input type="text" class="easyui-textbox" id="nombresfamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Estado Civil:</th>
        				<td>
        					<input type="text" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarTipoDeEstadoCivil'" id="estadocivilfamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Grado Instruccion:</th>
        				<td>
        					<input type="text" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarTipoDeGradoInstruccion'" id="gradoistruciionfamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Edad:</th>
        				<td>
        					<input type="number" class="easyui-textbox" id="edadfamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Sexo:</th>
        				<td>
        					<input type="text" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarTipoDeSexo'" id="sexofamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Parentesco:</th>
        				<td>
        					<input type="text" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarTipoDeParentesco'" id="parentescofamiliar">
        				</td>
        			</tr>
        			<tr>
        				<th>Ocupacion:</th>
        				<td>
        					<input type="text" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'../../MVC_Controlador/Servicio_Social/Servicio_SocialC.php?acc=MostrarTipoDeOcupacion'" id="ocupacionfamiliar">
        				</td>
                        <td>
        					<input type="hidden"  type="text" value=" " id="direccionfamiliar">
        				</td>
        			</tr>
        		</table>
        		<br>
        		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="width:96px;height: 50px;" id="IngresarFamiliares">AGREGAR</a>&nbsp;
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" style="width:96px;height: 50px;" id="LimpiarFamiliares">LIMPIAR</a>&nbsp;
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:96px;height: 50px;" onclick="$('#NuevoFamiliar').window('close');">SALIR</a>

        	</div>
            <div data-options="region:'center'" style="width:50%;">
            	    <table class="easyui-datagrid" title="Familiares" style="width:100%;height:100%;" data-options="collapsible:true" id="grillaFamiliares">
				        <thead>
				            <tr>
				                <th data-options="field:'FAMILIAR',rezisable:true">NOMBRES</th>
                                <th data-options="field:'ESTADO CIVIL',rezisable:true">ESTADO CIVIL</th>
                                <th data-options="field:'INSTRUCCION',rezisable:true">INSTRUCCION</th>
                                <th data-options="field:'EDAD',rezisable:true">EDAD</th>
                                <th data-options="field:'SEXO',rezisable:true">SEXO</th>
                                <th data-options="field:'PARENTESCO',rezisable:true">PARENTESCO</th>
                                <th data-options="field:'OCUPACION',rezisable:true">OCUPACION</th>
				            </tr>
				        </thead>
				    </table>
            </div>
        </div>
    </div>
</body>
</html>