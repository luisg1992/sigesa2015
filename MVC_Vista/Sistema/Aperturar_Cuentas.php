    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/icon.css">
		<link rel="stylesheet" href="../../MVC_Complemento/easyui/themes/color.css">
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
		<link rel="stylesheet" href="../../MVC_Complemento/css/boton.css">
		<style>
        html,body { 
        	padding: 0px;
        	margin: 0px;
        	height: 100%;
        	font-family: 'Helvetica'; 			
        }

        .mayus>input{
			text-transform: capitalize;
        }		
		label[for=message]
		{
		font-weight:bold;
		}
		
		@keyframes click-wave {
		  0% {
			height: 25px;
			width: 25px;
			opacity: 0.35;
			position: relative;
		  }
		  100% {
			height: 200px;
			width: 200px;
			margin-left: -60px;
			margin-top: -70px;
			opacity: 0;
		  }
		}

		.option-input {
		  -webkit-appearance: none;
		  -moz-appearance: none;
		  -ms-appearance: none;
		  -o-appearance: none;
		  appearance: none;
		  top: 13.33333px;
		  right: 0;
		  bottom: 0;
		  left: 0;
		  height: 25px;
		  width: 25px;
		  transition: all 0.15s ease-out 0s;
		  background: #cbd1d8;
		  border: none;
		  color: #fff;
		  cursor: pointer;
		  display: inline-block;
		  margin-right: 0.5rem;
		  outline: none;
		}
		.option-input:hover {
		  background: #9faab7;
		}
		.option-input:checked {
		  background: #40e0d0;
		}
		.option-input:checked::before {
		  height: 25px;
		  width: 25px;
		  content: '?';
		  display: inline-block;
		  font-size: 14.66667px;
		  text-align: center;
		  line-height: 24px;
		}
		.option-input:checked::after {
		  -webkit-animation: click-wave 0.65s;
		  -moz-animation: click-wave 0.65s;
		  animation: click-wave 0.65s;
		  background: #40e0d0;
		  content: '';
		  
		}
		.option-input.radio {
		  border-radius: 50%;
		}
		.option-input.radio::after {
		  border-radius: 50%;
		}
		
		</style>
		
		<script>			
	    $(document).ready(function() {
					$('.easyui-datebox').datebox({
						formatter : function(date){
							var y = date.getFullYear();
							var m = date.getMonth()+1;
							var d = date.getDate();
							return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
						},
						parser : function(s){

							if (!s) return new Date();
							var ss = s.split('-');
							var y = parseInt(ss[2],10);
							var m = parseInt(ss[1],10);
							var d = parseInt(ss[0],10);
							if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
								return new Date(y,m-1,d)
							} else {
								return new Date();
							}
						}

					});	
			
					$("#Aperturar_Cuenta").click(function(event) {
					var Fecha_Apertura = $('#Fecha_Apertura').datebox('getValue');
					$.messager.alert('Cuentas','Se Aperturaron las Cuentas del Dia'+Fecha_Apertura);
					$.ajax({
						url: '../../MVC_Controlador/Emergencia/EmergenciaC.php?acc=Aperturar_Cuentas_Emergencia',
						type: 'POST',
						dataType: 'json',
						data: {
							Fecha_Apertura:Fecha_Apertura
						}
						});
					});
			
		});
		</script>	
	</head>	
	<body>
	<div style="margin:20px;">
	<h2>Apertura Cuentas de Emergencia</h2>
    <p>Seleccione el dia que aperturara la cuenta.</p>
	</div>
    <div style="margin:20px 0;"></div>
    <div class="easyui-layout" style="width:100%;height:100%;">
        <div data-options="region:'north'" style="height:100px">
			<div style="width:200px;padding:30px 30px 30px 30px;float:left">
				<strong>Seleccione fecha :  </strong>
			</div>
			<div style="width:200px;padding:30px 30px 30px 0;float:left">
				<input id="Fecha_Apertura" class="easyui-datebox" label="Start Date:" labelPosition="top" style="width:100%;" value="<?php echo  date("d-m-Y");?>">
			</div>
			
			<div style="width:200px;padding:30px 30px 30px 0;float:left">
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:80px" id="Aperturar_Cuenta">Aperturar Cuentas<br></a>
			</div>
		</div>
        <div data-options="region:'south',split:true" style="height:50px;">
		
		</div>
        <div data-options="region:'east',split:true" title="East" style="width:100px;"></div>
        <div data-options="region:'west',split:true" title="West" style="width:100px;"></div>
        <div data-options="region:'center',title:'Main Title',iconCls:'icon-ok'">
            <table class="easyui-datagrid"
                    data-options="url:'datagrid_data1.json',method:'get',border:false,singleSelect:true,fit:true,fitColumns:true">
                <thead>
                    <tr>
                        <th data-options="field:'itemid'" width="80">Item ID</th>
                        <th data-options="field:'productid'" width="100">Product ID</th>
                        <th data-options="field:'listprice',align:'right'" width="80">List Price</th>
                        <th data-options="field:'unitcost',align:'right'" width="80">Unit Cost</th>
                        <th data-options="field:'attr1'" width="150">Attribute</th>
                        <th data-options="field:'status',align:'center'" width="60">Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</body>
</html>