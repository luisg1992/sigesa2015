 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Fancy Sliding Form with jQuery" />
<meta name="keywords" content="jquery, form, sliding, usability, css3, validation, javascript"/>
<meta name="description" content="">
<meta name="keywords" content="">
 
<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/formulario.css">
<link href="../../MVC_Complemento/css/calendario.css" type="text/css" rel="stylesheet">
 
<link href="../../MVC_Complemento/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">

 
<script type="text/javascript" src="../../MVC_Complemento/js/funciones.js"></script>
 
<script src="../../MVC_Complemento/js/calendar.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-es.js" type="text/javascript"></script>
<script src="../../MVC_Complemento/js/calendar-setup.js" type="text/javascript"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/FechaMasck.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../../MVC_Complemento/js/classAjax_Listar.js"></script>

<script src="../../MVC_Complemento/js/jquery_enter.js"></script>

 

<script language="JavaScript">
$(function(){
	$("input[type=text]").keydown(function(e){
		if (e.keyCode == 13) {
				var  tabIndex = parseFloat($(this).attr("tabindex")) + 1;
				$("input[tabindex=" + tabIndex + "]").focus();
				$("password[tabindex=" + tabIndex + "]").focus();
				
				return false;
		}
	})
})
</script>

<script type="text/javascript">
  
   
function valida_envia(){
	//valido el PAC_DNI

	if (document.formElem.USER_DNI.value.length==0){
		alert("Ingresar D.N.I.");
		document.formElem.USER_DNI.focus();
		return 0;
	}
	 
	 if (document.formElem.CONTRA_NUEVA.value.length==0){
		alert("Ingresar Contraseña");
		document.formElem.CONTRA_NUEVA.focus();
		return 0;
	}
	 
	 
 
	
   if (document.formElem.CONTRA_NUEVA.value != document.formElem.CONTRA_REPITA.value) {
            alert('LA CONTRASEÑA NO COINCIDE DIGITE NUEVAMENTE');
			document.formElem.CONTRA_NUEVA.focus();
            return 0;
            }

	 
	
	//el formulario se envia
	//alert("Se está registrando Datos del Paciente");
	document.formElem.submit();
}

    function repitecontras(){
		
			if (document.formElem.CONTRA_NUEVA.value != document.formElem.CONTRA_REPITA.value) {
            alert('Comprobación no coincide con la contraseña');
            return false;
            }
             return true;
		}
  
 
 


window.onload=function() {
setCursor(document.getElementById('USER_DNI'),13,13)
}
function setCursor(el,st,end) { 
if(el.setSelectionRange) { 
el.focus(); 
el.setSelectionRange(st,end); 
} 
else { 
if(el.createTextRange) { 
range=el.createTextRange(); 
range.collapse(true); 
range.moveEnd('character',end); 
range.moveStart('character',st); 
range.select();
} 
} 
}


function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
return false;
 
return true;
}
 
 
  
</script>
 


 
<!--validad usuario final -->
</head>
<body class="control">
<div class="cuerpo1"> 
<form id="formElem" name="formElem" method="post" action="../../MVC_Controlador/Sistema/SistemaC.php?acc=CambiarContrasennaActualizar">
     
   
    <table width="415" border="0">
  <tr>
    <td width="409"><div id="DivDestino"></div></td>
  </tr>
  <tr>
    <td><fieldset  class="fieldset legend">
            <legend><span class="alert_info"> Datos  del Usuario</span></legend>
                    
                    <table width="371" border="0">
                      <tr>
                        <td width="164">DNI:</td>
                        <td width="197"><input name="USER_DNI" type="text" class="texto" id="USER_DNI" tabindex="1" onkeypress="return isNumberKey(event)" onkeyup="compoDNI(event);" value="<?php echo $DNI; ?>" size="16" autocomplete="OFF" />
                        <input type="hidden" name="IdEmpleado" id="IdEmpleado"  value="<?php echo $_REQUEST["IdEmpleado"]?>"/></td>
                      </tr>
                      <tr>
                        <td>Usuario:</td>
                        <td><?php echo $Usuario; ?> - <?php echo $ApellidoPaterno; ?> <?php echo $ApellidoPaterno; ?>, <?php echo $Nombres; ?></td>
                      </tr>
                      <tr>
                        <td>Contraseña Nueva</td>
                        <td> 
                        <input type="password" name="CONTRA_NUEVA" id="CONTRA_NUEVA" tabindex="9"/></td>
                      </tr>
                      <tr>
                        <td>Repita Contraseña Nueva </td>
                        <td> 
                        <input type="password" name="CONTRA_REPITA" id="CONTRA_REPITA" tabindex="10"   /></td>
                      </tr>
            </table>
          </fieldset></td>
    </tr>
  <tr>
    <td>&nbsp; 
       
       </td>
  </tr>
  <tr>
    <td><table border="0" align="center">
                    <tr>
                        <td>
                            <div class="buttons" align="center">
                            <button type="button" class="positive" name="save" onclick="valida_envia();">
                            Guardar</button>
                            <a href="../MVC_Controlador/UsuarioC.php?acc=busca001&udni=<?php echo $udni;?>" class="negative"> Cancelar</a>
                            </div>
                        </td>
                    </tr>
                </table></td>
    </tr>
    </table>

    
    <!--
        <div id="pop">    
        <div id="cerrar">X</div>    
        <img src="../img/Advertencia.JPG" height="507" width="600" border="0"/>  
    </div>
    -->
</form>

<iframe id="Panet_Datos" name="Panet_Datos" width="0" height="0" frameborder="0">
  <ilayer width="0" height="0" id="Panet_Datos" name="Panet_Datos">
  </ilayer>
  </iframe>
</div>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
</body>
</html>