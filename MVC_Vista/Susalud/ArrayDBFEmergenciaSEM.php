<?php
//
// "Definición" de la base de datos
$def = array(
  /*array("date",     "D"),
  array("name",     "C",  50),
  array("age",      "N",   3, 0),
  array("email",    "C", 128),
  array("ismember", "L")*/

  array("RENIPRESS",  "C", 10),//1
  array("E_UBIG",  "C", 6),//2
  array("COD_DISA",  "C", 2),//3
  array("COD_RED",  "C", 2),//4
  array("COD_MRED",  "C", 2),
  array("FECATE",  "D"),
  array("HORATE",  "C", 5),
  array("NUMHC",  "C", 15),
  array("DOC_IDEN",  "C", 11),
  array("ETNIA",  "C", 2),
  array("FINANCIA",  "C", 2),
  array("SEXO",  "C", 1),
  array("EDAD",  "N", 3, 0),
  array("TIPOEDAD",  "C", 1),
  array("NOMB",  "C", 50),
  array("APELL",  "C", 50),
  array("DIRECC",  "C", 100),
  array("UBIG_RESHA",  "C", 6),
  array("UBIG_PROCE",  "C", 6),
  array("ACOMPANA",  "C", 50),
  array("ADOC_IDEN",  "C", 11),
  array("MOTATEN",  "C", 2),
  array("SITOCURREN",  "C", 6),
  array("UPS",  "C", 6),
  array("CODDIAG1",  "C", 5),
  array("TIPDIAG1",  "C", 1),
  array("CODDIAG2",  "C", 5),
  array("TIPDIAG2",  "C", 1),
  array("CODDIAG3",  "C", 5),
  array("TIPDIAG3",  "C", 1),
  array("CODDIAG4",  "C", 5),
  array("TIPDIAG4",  "C", 1),
  array("CODCPT1",  "C", 5),
  array("CODCPT2",  "C", 5),
  array("CODCPT3",  "C", 5),
  array("CODCPT4",  "C", 5),
  array("CONDICION",  "C", 1),
  array("FECEGR",  "D"),
  array("HOREGR",  "C", 5),
  array("DESTINO",  "C", 2),
  array("DES_EESS",  "C", 10),
  array("DES_UPS",  "C", 6),
  array("CODPSAL",  "C", 11),
  array("OBSERV",  "N", 1, 0),
  array("OFECING",  "D"),
  array("OHORING",  "C", 5),
  array("OFECEGR",  "D"),
  array("OHOREGR",  "C", 5),
  array("TOTALEST",  "N", 3, 0),
  array("OCAMA",  "C", 10),
  array("OCODPSAL",  "C", 11),
  array("OCODDIAG1",  "C", 5),
  array("OCODDIAG2",  "C", 5),
  array("FECHAREG",  "D"),
  array("ESTADO",  "N", 1, 0),
  
  array("PRIORIDAD",  "C", 1),
  array("FORM_ING",  "C", 1)

);

// creación
$nombrearchivo=$Anio.'_'.$Mes.'_'.'EmergenciaSEM.dbf';
if (!dbase_create($nombrearchivo, $def)) {
  echo "Error, no se puede crear la base de datos\n";
}

$db = dbase_open($nombrearchivo, 2);

if ($db) {
  			$CodIPress=$_REQUEST["CodIpress"];
			$CodUgiPress=$_REQUEST["CodUgiPres"];
			$CodIgss=$_REQUEST["CodIgss"];
			$CodRed=$_REQUEST["CodRed"];
			$CodMicrored=$_REQUEST["CodMicrored"];
				
			//ElimTemp_IngresosEgresosEmergenciaM();
			//RegTemp_IngresosEgresosEmergenciaM($Anio, $Mes);	
			$resultados = ListarIngresosEgresosEmergenciaM($Anio, $Mes); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {														
					//$AnioMes = $resultados[$i]["AnioMes"];
					//$CodIPress = $resultados[$i]["CodIPress"];
					//$CodUgiPress = $resultados[$i]["CodUgiPress"];					
					$FECHAING = $resultados[$i]["FECHAING"];//d/m/Y
					
					//SOLO PARA EXPORTAR A DBF
					$porcionesFI = explode("/", $FECHAING);
					$FECHAING=$porcionesFI[2].$porcionesFI[1].$porcionesFI[0];//date('Ymd')
					//FIN PARA EXPORTAR A DBF
					
					$HORAING = $resultados[$i]["HORAING"];
					$HISTORIACLINICA = $resultados[$i]["HISTORIACLINICA"];
					
					$tipodoc=$resultados[$i]["TIPODOC"];
					$doc = $resultados[$i]["NDOC"];
					if(trim($doc)!="" && (int)$doc!=0){
					$NDOC = $tipodoc.$doc;
					}else{
					$NDOC = "0";	
					}				
					
					$ETNIA = $resultados[$i]["ETNIA"];//10				
					$SEGURO = $resultados[$i]["SEGURO"];//11
					$SEXO = $resultados[$i]["SEXO"];//12
					$EDAD = $resultados[$i]["EDAD"];//13
					$TIPOEDAD = $resultados[$i]["TIPOEDAD"];//14					
										
					$nopermitidos = array( "'" , '"', "," , ";" , "&" , "+" );
									
					$NOMBRESPACIENTE = trim($resultados[$i]["NOMBRESPACIENTE"]);
					$NOMBRESPACIENTE = str_replace($nopermitidos, "", $NOMBRESPACIENTE);//15				
					$APELLIDOSPACIENTE = trim($resultados[$i]["APELLIDOSPACIENTE"]);
					$APELLIDOSPACIENTE = str_replace($nopermitidos, "", $APELLIDOSPACIENTE);//16
					
					$DIRECCION=trim($resultados[$i]["DIRECCION"]);
					$DIRECCION = str_replace($nopermitidos, "", $DIRECCION);//17
					
					$IDDISTRITODOMICILIO=$resultados[$i]["IDDISTRITODOMICILIO"];//18					
					if($IDDISTRITODOMICILIO!=""){
						$IDDISTRITODOMICILIO=str_pad((string)$resultados[$i]["IDDISTRITODOMICILIO"], 6, "0", STR_PAD_LEFT); 	
					}else{
						$IDDISTRITODOMICILIO="";
					}
					
					$IDDISTRITOPROCEDENCIA=$resultados[$i]["IDDISTRITOPROCEDENCIA"];//19
					if($IDDISTRITOPROCEDENCIA!=""){
						$IDDISTRITOPROCEDENCIA=str_pad((string)$resultados[$i]["IDDISTRITOPROCEDENCIA"], 6, "0", STR_PAD_LEFT); 	
					}else{
						$IDDISTRITOPROCEDENCIA="";
					}
					
					$NOMBREACOMPA=$resultados[$i]["NOMBREACOMPA"];
					$NOMBREACOMPA = str_replace($nopermitidos, "", $NOMBREACOMPA);//20
					
					$DNIACOMPA=$resultados[$i]["DNIACOMPA"];//21
					$MOTIVOATCEMERGENCIA=$resultados[$i]["MOTIVOATCEMERGENCIA"];//22
					$UBIGEOSITIOOCURRENCIA=$resultados[$i]["UBIGEOSITIOOCURRENCIA"];//23
					
					$SERVICIOATENCION=$resultados[$i]["SERVICIOATENCION"];
					$SERVICIOATENCION = str_replace($nopermitidos, "", $SERVICIOATENCION);//24
					
					$DX_ING1=$resultados[$i]["DX_ING1"];//25					
					$TIPODIAG1=$resultados[$i]["TIPODIAG1"];//26
					if($resultados[$i]["TIPODIAG1"]==NULL && $resultados[$i]["DX_ING1"]!=NULL){	
						$TIPODIAG1='D';				
					}else{
						$TIPODIAG1=$resultados[$i]["TIPODIAG1"];
					}
										
					$DX_ING2=$resultados[$i]["DX_ING2"];//27
					$TIPODIAG2=$resultados[$i]["TIPODIAG2"];//28
					if($resultados[$i]["TIPODIAG2"]==NULL && $resultados[$i]["DX_ING2"]!=NULL){	
						$TIPODIAG2='D';				
					}else{
						$TIPODIAG2=$resultados[$i]["TIPODIAG2"];
					}	
									
					$DX_ING3=$resultados[$i]["DX_ING3"];//29
					$TIPODIAG3=$resultados[$i]["TIPODIAG3"];//30
					if($resultados[$i]["TIPODIAG3"]==NULL && $resultados[$i]["DX_ING3"]!=NULL){	
						$TIPODIAG3='D';				
					}else{
						$TIPODIAG3=$resultados[$i]["TIPODIAG3"];
					}
					
					$DX_ING4=$resultados[$i]["DX_ING4"];//31
					$TIPODIAG4=$resultados[$i]["TIPODIAG4"];//32
					if($resultados[$i]["TIPODIAG4"]==NULL && $resultados[$i]["DX_ING4"]!=NULL){	
						$TIPODIAG4='D';				
					}else{
						$TIPODIAG4=$resultados[$i]["TIPODIAG4"];
					}
					
					$OTROPROCE_CPT1=$resultados[$i]["OTROPROCE_CPT1"];//33
					$OTROPROCE_CPT2=$resultados[$i]["OTROPROCE_CPT2"];//34
					$OTROPROCE_CPT3=$resultados[$i]["OTROPROCE_CPT3"];//35
					$OTROPROCE_CPT4=$resultados[$i]["OTROPROCE_CPT4"];//36					
					$CONDICIONSALIDA=$resultados[$i]["CONDICIONSALIDA"];//37
						if($CONDICIONSALIDA==""){
							$CONDICIONSALIDA="2";
						}else{
							$CONDICIONSALIDA=$CONDICIONSALIDA;
						}
					$FECHAEGRESO=$resultados[$i]["FECHAEGRESO"];//38					
						if($FECHAEGRESO==""){
							$FECHAEGRESO=$resultados[$i]["FECHAEGRESOADMIN"];
						}else{
							$FECHAEGRESO=$FECHAEGRESO;
						}
					
					//SOLO PARA EXPORTAR A DBF
					if($FECHAEGRESO!=NULL){
						$porcionesFE = explode("/", $FECHAEGRESO);
						$FECHAEGRESO=$porcionesFE[2].$porcionesFE[1].$porcionesFE[0];//date('Ymd')
					}else{
						$FECHAEGRESO='';
					}
					//FIN PARA EXPORTAR A DBF
					
					$HORAEGRESO=$resultados[$i]["HORAEGRESO"];//39
						if($HORAEGRESO==""){
							$HORAEGRESO=$resultados[$i]["HORAEGRESOADMIN"];
						}else{
							$HORAEGRESO=$HORAEGRESO;
						}
						
					$DESTINOATENCION=$resultados[$i]["DESTINOATENCION"];//40	
						if($DESTINOATENCION==""){
							$DESTINOATENCION='01';
						}else{
							$DESTINOATENCION=$DESTINOATENCION;
						}
										
					$CODEESSDESTINO="";//41
					$UPSSIESHOSPITALIZADO="";//42					
					$MEDICO=$resultados[$i]["MEDICO"];//CODPSAL 43
					$PASOOBSERVACION="";//44
					$FECHAINOBS="";//45
					$HORAINOBS="";//46					
					$FECHAOUTOBS="";//47
					$HORAOUTOBS="";//48
					$TOTALESTAENOBS="";//49
					$NCAMAENOBS="";//50
					
					$MEDICOOBS="";//51
					$CODIGO1OBS="";//52					
					$CODIGO2OBS="";//53
					$FECHAREGISTRO="";//54
					$ESTADOREGISTRO="";//55
					
					$IdTipoGravedad=$resultados[$i]["IdTipoGravedad"];//56
					
			    dbase_add_record($db, array(
					$CodIPress,//1 
					$CodUgiPress, //2
					$CodIgss, //3
					$CodRed,//4
					$CodMicrored,//5
					$FECHAING, //6
					$HORAING, //7
					$HISTORIACLINICA, //8
					$NDOC,//9
					$ETNIA,//10
					$SEGURO ,//11
					$SEXO,//12
					$EDAD ,//13
					$TIPOEDAD ,//14
					$NOMBRESPACIENTE ,//15
					$APELLIDOSPACIENTE,//16
					$DIRECCION,//17
					$IDDISTRITODOMICILIO,//18
					$IDDISTRITOPROCEDENCIA,//19
					$NOMBREACOMPA,//20
					$DNIACOMPA,//21
					$MOTIVOATCEMERGENCIA,//22
					$UBIGEOSITIOOCURRENCIA,//23					
					$SERVICIOATENCION,//24
					$DX_ING1,//25
					$TIPODIAG1,//26
					$DX_ING2,//27
					$TIPODIAG2,//28
					$DX_ING3,//29
					$TIPODIAG3,//30
					
					$DX_ING4,//31
					$TIPODIAG4,//32
					$OTROPROCE_CPT1,//33
					$OTROPROCE_CPT2,//34
					$OTROPROCE_CPT3,//35
					$OTROPROCE_CPT4,//36
					$CONDICIONSALIDA,//37
					$FECHAEGRESO,//38
					$HORAEGRESO,//39
					$DESTINOATENCION,//40
					$CODEESSDESTINO,//41
					$UPSSIESHOSPITALIZADO,//42
					$MEDICO,//43
					$PASOOBSERVACION,//44
					$FECHAINOBS,//45
					$HORAINOBS,//46
					$FECHAOUTOBS,//47
					$HORAOUTOBS,//48
					$TOTALESTAENOBS,//49
					$NCAMAENOBS,//50
					
					$MEDICOOBS,//51
					$CODIGO1OBS,//52
					$CODIGO2OBS,//53
					$FECHAREGISTRO,//54
					$ESTADOREGISTRO,//55
					
					$IdTipoGravedad,//56
					'1' //57



				)); 
	  }
	 }
  dbase_close($db);
  
}
?> 
						
  