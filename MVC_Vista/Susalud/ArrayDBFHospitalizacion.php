<?php
//
// "Definición" de la base de datos
$def = array(
  /*array("date",     "D"), //DATE (fecha)
  array("name",     "C",  50), //CARACTER (texto)
  array("age",      "N",   3, 0), //NUMBER (numero)
  array("email",    "C", 128),
  array("ismember", "L")*/ 
  array("renipress",  "C", 10),//1
  array("e_ubig",  "C", 6),//2
  array("e_cdpto",  "C", 2),//3
  array("e_cprov",  "C", 2),//4
  array("e_cdist",  "C", 2),//5
  array("cod_disa",  "C", 2),//6
  array("cod_red",  "C", 2),//7
  array("cod_mred",  "C", 2),//8  
  array("numhc",  "C", 15), 
  array("nomb",  "C", 50),
	
  array("apell",  "C", 50),   
  array("doc_iden",  "C", 11),  
  array("etnia",  "C", 2),   
  array("sexo",  "C", 1),
  array("edad",  "N", 3, 0),
  array("tipoedad",  "C", 1),  
  array("ubigeo",  "C", 6),//UbigeoProc
  array("cdpto",  "C", 2),
  array("cprov",  "C", 2),
  array("cdist",  "C", 2),
  
  //array("FechaNac",  "D"), 
  array("fecing",  "D"),
 // array("HoraIng.",  "C", 10),
  //array("Espec.Ing",  "C", 50),
  array("fecegr",  "D"),
  array("totalest",  "N", 5, 0),  
  array("ups",  "C", 6), //UPS SERVICIO SALIDA
  array("SERVEGRESO",  "C", 50), //DESCRIPCION SERVICIO 
	
  array("SERINGRESO",  "C", 50), //DESCRIPCION SERVICIO 
	
  array("condicion",  "C", 1), //Cond.Egr
  array("financia",  "C", 2), //TipoSeguro  
  //array("Serv.Egr",  "C", 50),
  //array("Espec.Egr",  "C", 50),  
  /*array("Diag.Ing.1",  "C", 20),
  array("Diag.Ing.2",  "C", 20),
  array("Diag.Ing.3",  "C", 20),
  array("Diag.Ing.4",  "C", 20),*/
  array("coddiag1",  "C", 5),//Diag.Alta1
  array("coddiag2",  "C", 5),//Diag.Alta2
  array("coddiag3",  "C", 5),//Diag.Alta3
  array("coddiag4",  "C", 5),//Diag.Alta4
	
  array("cemorb1",  "C", 5),//Diag.Mort1
  array("cemorb2",  "C", 5),//Diag.Mort2
  //array("Diag.Mort3",  "C", 20),//Diag.Mort3
  //array("Diag.Mort4",  "C", 20)//Diag.Mort4  
  array("codcpt1",  "C", 5),
  array("codcpt2",  "C", 5),
  array("codcpt3",  "C", 5),
  array("codcpt4",  "C", 5),    
  array("estadio",  "C", 1),
  array("valor_t",  "C", 3),
  array("valor_n",  "C", 3),	
  array("valor_m",  "C", 3),
	
  array("tratamien",  "C", 3),  
  array("prof_parto",  "C", 2),
  array("fecparto",  "D"),  
  array("rnvivo",  "N", 1, 0),    
  array("rnmuerto","N", 1, 0),
  array("codpsal",  "C", 11),
  array("fechareg",  "D"), //fechaegresoadministrativo
  array("estado",  "C", 1)
  
  

);

// creación
$nombrearchivo=$Anio.'_'.$Mes.'_'.'Hospitalizacion.dbf';
if (!dbase_create($nombrearchivo, $def)) {
  echo "Error, no se puede crear la base de datos\n";
}

$db = dbase_open($nombrearchivo, 2);

if ($db) {
  			//RegTemp_IngresosEgresosHospitalizacionM($Anio, $Mes);
            $resultados=ListarIngresosEgresosHospitalizacionM($Anio, $Mes);						 
            if($resultados!=NULL){			
                for ($i=0; $i < count($resultados); $i++) {
                
                  $NroHistoriaClinica = $resultados[$i]["NroHistoriaClinica"];
                  $NroDocumento = $resultados[$i]["NroDocumento"];
                  $APELLIDOSPACIENTE = $resultados[$i]["APELLIDOSPACIENTE"];
                  $NOMBRESPACIENTE = $resultados[$i]["NOMBRESPACIENTE"];		 
                  $IdTipoSexo = $resultados[$i]["IdTipoSexo"];
                  if($IdTipoSexo=='1'){
                    $sexo='MASCULINO';  
                  }else{
                      $sexo='FEMENINO';  
                  }
                  $EDAD = $resultados[$i]["EDAD"];
				  $TIPOEDAD = ($resultados[$i]["TIPOEDAD"]);
				  $IdTipoEdad = ($resultados[$i]["IdTipoEdad"]);			
					
				   	$xIDDISTRITOPROCEDENCIA=$resultados[$i]["IDDISTRITOPROCEDENCIA"];	
					$IdDistritoDomicilio=$resultados[$i]["IdDistritoDomicilio"];
					
					if($IdDistritoDomicilio!="" && $IdDistritoDomicilio!=0){
						$IDDISTRITOPROCEDENCIA=$IdDistritoDomicilio;
						
					}else if($xIDDISTRITOPROCEDENCIA!="" && $xIDDISTRITOPROCEDENCIA!=0){
						$IDDISTRITOPROCEDENCIA = $xIDDISTRITOPROCEDENCIA;	
						
					}else{
						$IDDISTRITOPROCEDENCIA="";
					}
					
					if($IDDISTRITOPROCEDENCIA!=""){
						$IDDISTRITOPROCEDENCIA=str_pad((string)$IDDISTRITOPROCEDENCIA, 6, "0", STR_PAD_LEFT);
						$CDPTO=substr($IDDISTRITOPROCEDENCIA,0,2);
						$CPROV=substr($IDDISTRITOPROCEDENCIA,2,2);
						$CDIST=substr($IDDISTRITOPROCEDENCIA,4,2);
 	
					}else{
						$IDDISTRITOPROCEDENCIA="070101";
						$CDPTO='07';$CPROV='01';$CDIST='01';
					}
				  
                  /*$FechaNacimiento = $resultados[$i]["FechaNacimiento"];
                  if($FechaNacimiento!=NULL){
                      $FechaNacimiento=vfecha(substr($FechaNacimiento,0,10));
                  }else{
                      $FechaNacimiento="";
                  }
				  //SOLO PARA EXPORTAR A DBF
					if($FechaNacimiento!=NULL){
						$porcionesFN = explode("/", $FechaNacimiento);
						$FechaNacimiento=$porcionesFN[2].$porcionesFN[1].$porcionesFN[0];//date('Ymd')
					}else{
						$FechaNacimiento='';
					}*/
				  //FIN PARA EXPORTAR A DBF
                  $NroDocumento = $resultados[$i]["NroDocumento"];
                  
                  $FechaIngreso = $resultados[$i]["FechaIngreso"];
                  if($FechaIngreso!=NULL){
                      $FechaIngreso=vfecha(substr($FechaIngreso,0,10));
                  }else{
                      $FechaIngreso="";
                  }
				  //SOLO PARA EXPORTAR A DBF
					if($FechaIngreso!=NULL){
						$porcionesFI = explode("/", $FechaIngreso);
						$FechaIngreso=$porcionesFI[2].$porcionesFI[1].$porcionesFI[0];//date('Ymd')
					}else{
						$FechaIngreso="";
					}
				  //FIN PARA EXPORTAR A DBF
				  
                  $FechaEgreso = $resultados[$i]["FechaEgreso"];
                  if($FechaEgreso!=NULL){
                      $FechaEgreso=vfecha(substr($FechaEgreso,0,10));
                  }else{
                      $FechaEgreso="";
                  }					  
				  //SOLO PARA EXPORTAR A DBF
					if($FechaEgreso!=NULL){
						$porcionesFE = explode("/", $FechaEgreso);
						$FechaEgreso=$porcionesFE[2].$porcionesFE[1].$porcionesFE[0];//date('Ymd')
					}else{
						$FechaEgreso='';
					}
				  //FIN PARA EXPORTAR A DBF
				  
				  $UPSSERVICIOSALIDA = $resultados[$i]["UPSSERVICIOSALIDA"];
				  
				  if($FechaIngreso!="" && $FechaEgreso!=""){
                      $TOTALEST=compararFechas(vfecha(substr($resultados[$i]["FechaEgreso"],0,10)), vfecha(substr($resultados[$i]["FechaIngreso"],0,10)));
                  }else{
                      $TOTALEST="";
                  }
				  
                  $especialidadIngreso = $resultados[$i]["especialidadIngreso"];
                  $especialidadEgreso = $resultados[$i]["especialidadEgreso"];
                  
				  //INICIO DIAGNOSTICOS
                  $DX_ING1 = $resultados[$i]["DX_ING1"];
                  $DX_ING2 = $resultados[$i]["DX_ING2"];
                  $DX_ING3 = $resultados[$i]["DX_ING3"];
                  $DX_ING4 = $resultados[$i]["DX_ING4"];
                  
                  $DX_ALTA1 = $resultados[$i]["DX_ALTA1"];
                  $DX_ALTA2 = $resultados[$i]["DX_ALTA2"];
                  $DX_ALTA3 = $resultados[$i]["DX_ALTA3"];
                  $DX_ALTA4 = $resultados[$i]["DX_ALTA4"];
					
				  if($resultados[$i]["DX_ALTA1"]==""){
                     $DX_ALTA1=$DX_ING1;
                  }else{
					 $DX_ALTA1=$DX_ALTA1; 
				  }
				  
				  if($resultados[$i]["DX_ALTA2"]==""){
                     $DX_ALTA2=$DX_ING2;
                  }else{
					 $DX_ALTA2=$DX_ALTA2; 
				  }
					
				  if($resultados[$i]["DX_ALTA3"]==""){
                     $DX_ALTA3=$DX_ING3;
                  }else{
					 $DX_ALTA3=$DX_ALTA3; 
				  }
					
				  if($resultados[$i]["DX_ALTA4"]==""){
                     $DX_ALTA4=$DX_ING4;
                  }else{
					 $DX_ALTA4=$DX_ALTA4; 
				  }
				  //FIN DIAGNOSTICOS	
                             
                  $DX_Mortalidad1 = $resultados[$i]["DX_Mortalidad1"];
                  $DX_Mortalidad2 = $resultados[$i]["DX_Mortalidad2"];
                  $DX_Mortalidad3 = $resultados[$i]["DX_Mortalidad3"];
                  $DX_Mortalidad4 = $resultados[$i]["DX_Mortalidad4"];
                  
                  /*$DX_Complicacion1 = $resultados[$i]["DX_Complicacion1"];
                  $DX_Complicacion2 = $resultados[$i]["DX_Complicacion2"];
                  $DX_Complicacion3 = $resultados[$i]["DX_Complicacion3"];
                  $DX_Complicacion4 = $resultados[$i]["DX_Complicacion4"];	*/				 
					
				  $XSEGURO = $resultados[$i]["IDSEGURO"];
				  $SEGURO=str_pad((string)$XSEGURO, 2, "0", STR_PAD_LEFT);
				  	 
				  $HoraIngreso = $resultados[$i]["HoraIngreso"];
				  $SERVICIOEGRESO = $resultados[$i]["SERVICIOEGRESO"];	
				  $SERVICIOINGRESO = $resultados[$i]["SERVICIOINGRESO"];	
					
				  $CONDICIONSALIDA = $resultados[$i]["IDCONDICIONSALIDASEM"];	
				  
				  $ETNIA = $resultados[$i]["ETNIA"];//IdEtnia
				  
				  $PROF_PARTO = '';
				  
				  $FechaNacimientoRN = $resultados[$i]["FechaNacimientoRN"];
                  if($FechaNacimientoRN!=NULL){
                      $FechaNacimientoRN=vfecha(substr($FechaNacimientoRN,0,10));
					  //SOLO PARA EXPORTAR A DBF
					  $porcionesFE = explode("/", $FechaNacimientoRN);
					  $FECPARTO=$porcionesFE[2].$porcionesFE[1].$porcionesFE[0];//date('Ymd')
                  }else{
                      $FECPARTO="";
                  }					  
				 //RNVIVO,RNMUERTO
			     $IdCondicionRN = $resultados[$i]["IdCondicionRN"];
				 $RNVIVO = $resultados[$i]["RNVIVO"]; //CANTIDAD RN VIVOS
				 $RNMUERTO = $resultados[$i]["RNMUERTO"]; //CANTIDAD RN MUERTOS
				  
				 $CODPSAL = $resultados[$i]["CODPSAL"];
				 $FECHAREG=$FechaIngreso; $ESTADO='1';
					
				 $codcpt1=$resultados[$i]["codcpt1"]; 
					
					
			    dbase_add_record($db, array(
					$RENIPRESS, $E_UBIG, $E_CDPTO, $E_CPROV, $E_CDIST, $COD_DISA, $COD_RED, $COD_MRED, //1-8
					$NroHistoriaClinica,//9					
					utf8_decode($NOMBRESPACIENTE),//10
					utf8_decode($APELLIDOSPACIENTE), //11
					$NroDocumento, //12
					$ETNIA,//13
					$IdTipoSexo, //14 sexo
					$EDAD, //15
					$IdTipoEdad, //utf8_decode($TIPOEDAD),	//16				  
					$IDDISTRITOPROCEDENCIA,$CDPTO,$CPROV,$CDIST, //17-20
					
					//$FechaNacimiento, 				
					$FechaIngreso , //21
					//$HoraIngreso,//
					//utf8_decode($especialidadIngreso) ,//
					$FechaEgreso,//22
					$TOTALEST,//23
					$UPSSERVICIOSALIDA, utf8_decode(mb_strtoupper($SERVICIOEGRESO)),utf8_decode(mb_strtoupper($SERVICIOINGRESO)),	//24
					utf8_decode($CONDICIONSALIDA),//25
					utf8_decode($SEGURO),//26				
									
					//utf8_decode($SERVICIOEGRESO) ,//15
					//utf8_decode($especialidadEgreso),//16
					
					/*$DX_ING1,//18
					$DX_ING2,//19
					$DX_ING3,//20
					$DX_ING4,//21*/
					$DX_ALTA1,//27
					$DX_ALTA2,//28					
					$DX_ALTA3,//29
					$DX_ALTA4,//30
					
					$DX_Mortalidad1,//31
					$DX_Mortalidad2,//32					
					$codcpt1,'','','','','','','','',// 9 CAMPOS VACIOS //33-41
					$PROF_PARTO, //42
					$FECPARTO,$RNVIVO,$RNMUERTO, //43-45
					$CODPSAL,$FECHAREG, //46-47
					$ESTADO //48
					
					
				)); 
	  }
	 }
  dbase_close($db);
  
}
?> 
						
  