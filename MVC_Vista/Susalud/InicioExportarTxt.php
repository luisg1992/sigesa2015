<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
     	
        <script>
			function buscar(){ 
				var tiporep=$('#tiporep').combobox('getValue');
				if(tiporep==""){
					$.messager.alert('Mensaje','Seleccione Tipo de Reporte','info');
					$('#tiporep').next().find('input').focus();
					return 0;			
				}				 
	 
				document.formElem.submit();
			}
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:80%;height:200px;padding:10px;"
        title="DATOS GENERALES DE TODOS LOS REPORTES" iconCls="icon-search" collapsible="true" align="center">
        
        <form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VerReporte&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem">
              <table width="438" border="0">
                <tr>
                    <td><label>Tipo Reporte:</label></td>
                    <td>
                        <select class="easyui-combobox" name="tiporep" id="tiporep" style="width:auto;height:auto">
                          <option value="">Seleccione</option>
                          <!--<option value="A0">A0:Recursos de Salud</option>
                          <option value="B1">B1:Consolidado de Producción Asistencial en Consulta Ambulatoria</option>
                          <option value="B2">B2:Consolidado de Morbilidad en Consulta Ambulatoria</option>
                          <option value="C1">C1:Consolidado de Producción Asistencial de Emergencia</option>
                          <option value="C2">C2:Consolidado de Morbilidad en Emergencia</option>
                          <option value="D1">D1:Consolidado de Producción Asistencial en Hospitalización</option>
                          <option value="D2">D2:Consolidado de Morbilidad en Hospitalización</option>
                          <option value="E0">E0:Consolidado de Partos</option> --> 
							
                          <!--<option value="F0">F0:Consolidado de Eventos bajo Vigilancia Institucional</option>							
                          <option value="G0">G0:Consolidado de Producción Asistencial de Procedimientos</option>
                          <option value="H0">H0:Consolidado de Producción Asistencial de Intervenciones Quirurgicas</option>
                          <option value="I0">I0:Referencias</option>
                          <option value="J0">J0:Consolidado de Programación Asistencial</option> <br> -->  
							
                          <option value="OTRO1">1.1. Ingresos y Egresos Emergencia</option>
                          <!--<option value="EMERGENCIAMENORES29DIAS">1.2. Ingresos y Egresos Emergencia Menores 29 dias</option>--><hr>                          
                          <option value="DIAGHOSP">2.1. Ingresos y Egresos Hospitalización con Diagnosticos</option> 
                          <option value="OTRO2">2.2. Totales Ingresos y Egresos Hospitalización</option>
						  <option value="HOSPIFINDEMES">2.3. Hospitalizados Fin de mes</option><hr> 							
                          <!--<option value="CONSUMOSERVICIOS">Listar Consumo de Servicios</option>-->          
                          <option value="HORAMEDICO">3. Programacion Hora Medico</option>  
                          <option value="CITAS">4. Citas por Consulta Externa</option>						  
						  <option value="IngresosHospitalizadosUCIPediatricos">5. Ingresos Hospitalizados UCI Pediatricos</option>
                          <option value="ACCIDENTESTRANSITO">6. Totales Accidentes de tránsito</option>
                        </select>
                    </td>
                  </tr>
                  <tr>
                    <td><label>Mes:</label></td>
                    <td><select class="easyui-combobox" id="Mes" name="Mes"  style="width:100px;" >
                      <!--<option value="0"> </option>-->
                      <option value="01">ENERO</option>
                      <option value="02">FEBRERO</option>
                      <option value="03">MARZO</option>
                      <option value="04">ABRIL</option>
                      <option value="05">MAYO</option>
                      <option value="06">JUNIO</option>
                      <option value="07">JULIO</option>
                      <option value="08">AGOSTO</option>
                      <option value="09">SETIEMBRE</option>
                      <option value="10">OCTUBRE</option>
                      <option value="11">NOVIEMBRE</option>
                      <option value="12">DICIEMBRE</option>
                    </select></td>
                  </tr>
                  <tr>
                    <td>Periodo:</td>
                    <td><select class="easyui-combobox" id="Anio" name="Anio"  style="width:100px;">
                      <?php 
                            for ($i=date('Y'); $i>=1950; $i--) {
                                echo "<option value='$i'>$i</option>";
                            } 
                         ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>                
                </table>  
                 <div class="buttons" align="center">
                            <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-search'"  style="width:20%"  onclick="buscar();">Buscar</button>                            
                            <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:20%" data-options="iconCls:'icon-reload'">Refrescar</a> 
          </div>           
                    
            </form>
		</div>	
		 </div>	
      
    </body>
</html>
