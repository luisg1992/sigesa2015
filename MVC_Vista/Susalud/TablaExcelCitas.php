<?php 
include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();
// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Nro HistoriaClinica");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Nro Documento");	 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "Apellido Paterno");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "Apellido Materno");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Primer Nombre");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Segundo Nombre");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Fecha Nacimiento");

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "Edad");		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "Tipos Edad");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "Sexo");

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', "Especialidade");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', "Fecha Solicitud");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', "Fecha Atencion");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', "Año Atencion");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', "Mes Atencion");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1', "Dias");				
				
			$resultados = ListarCitasM($fecha_inicio, $fecha_fin); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {	
				 
				 $FechaNacimiento=vfecha(substr($resultados[$i]["FechaNacimiento"],0,10));
				 $FechaSolicitud=vfecha(substr($resultados[$i]["FechaSolicitud"],0,10));
				 $FechaAtencion=vfecha(substr($resultados[$i]["FechaAtencion"],0,10));
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), trim($resultados[$i]["NroHistoriaClinica"]));													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $resultados[$i]["NroDocumento"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $resultados[$i]["ApellidoPaterno"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $resultados[$i]["ApellidoMaterno"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $resultados[$i]["PrimerNombre"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $resultados[$i]["SegundoNombre"]);				 													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $FechaNacimiento);				 						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $resultados[$i]["Edad"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $resultados[$i]["TiposEdad"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $resultados[$i]["TiposSexo"]);		

				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+2), $resultados[$i]["Especialidade"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+2), $FechaSolicitud);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+2), $FechaAtencion);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.($i+2), $resultados[$i]["AnoAtencion"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.($i+2), $resultados[$i]["MesAtencion"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.($i+2), $resultados[$i]["Dias"]);
				 			 

		 }
	   }
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('CITAS'); 		
		 /*$objActSheet->getColumnDimension('I')->setWidth(30);	
		 $objActSheet->getColumnDimension('J')->setWidth(30);		
		 $objActSheet->getColumnDimension('K')->setWidth(30);		
		
		 $objActSheet->getColumnDimension('S')->setWidth(30);
		 $objActSheet->getColumnDimension('T')->setWidth(30);  
		 $objActSheet->getColumnDimension('U')->setWidth(30); 
		 $objActSheet->getColumnDimension('V')->setWidth(30);*/
	
	
//Formato General
//El ancho de columna
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
		 
    //Formato primera Fila 
    $objStyleA5 = $objActSheet ->getStyle('A1:Z1');
    //$objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontA5 = $objStyleA5->getFont(); 
    $objFontA5->setName('Arial'); 
    $objFontA5->setSize(11); 
    $objFontA5->setBold(true); 
    //$objFontA5->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    //$objFontA5 ->getColor()->setARGB('FFFF0000') ;
    //$objFontA5 ->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE); 
	//Establecer la alineación 
    $objAlignA5 = $objStyleA5->getAlignment(); 
    $objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);	 
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Citas.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>
			
  