<?php 
include('/../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();
// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "PUNTOS DE CARGA");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "SERVICIOS");	 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "ESTADO FACTURACION");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "FUENTE FINANCIAMIENTO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "TIPOS FINANCIAMIENTO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "FECHA ATENCIÓN");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "EDAD");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "TIPO EDAD");	
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "APELLIDO PATERNO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "APELLIDO MATERNO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', "PRIMER NOMBRE");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', "DNI");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', "HISTORIA CLINICA");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', "FECHA NACIMIENTO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', "CANTIDAD");
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1', "PRECIO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1', "TOTAL");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1', "CODIGO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S1', "NOMBRE");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T1', "DESCRIPCION");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U1', "TIPO SERVICIO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V1', "SERVICIO INGRESO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W1', "SERVICIO EGRESO");				
				
			$resultados = ListarConsumoServiciosM($fecha_inicio, $fecha_fin, $IdServicio, $IdCentroCosto, $codigoCPT); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {	
				
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), trim($resultados[$i]["PuntosCarga"]));													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $resultados[$i]["ServicioPaciente"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $resultados[$i]["EstadosFacturacion"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $resultados[$i]["FuentesFinanciamiento"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $resultados[$i]["TiposFinanciamiento"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $resultados[$i]["FechaIngreso"]);				 													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $resultados[$i]["Edad"]);				 						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $resultados[$i]["Tipoedad"]);
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $resultados[$i]["ApellidoPaterno"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $resultados[$i]["ApellidoMaterno"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+2), $resultados[$i]["PrimerNombre"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+2), $resultados[$i]["NroDocumento"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+2), $resultados[$i]["NroHistoriaClinica"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.($i+2), $resultados[$i]["FechaNacimiento"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.($i+2), $resultados[$i]["Cantidad"]);
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.($i+2), $resultados[$i]["Precio"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.($i+2), $resultados[$i]["Total"]);   				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.($i+2), $resultados[$i]["Codigo"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.($i+2), $resultados[$i]["Nombre"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.($i+2), $resultados[$i]["Descripcion"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.($i+2), $resultados[$i]["TiposServicio"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.($i+2), $resultados[$i]["ServicioIngreso"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.($i+2), $resultados[$i]["ServicioEgreso"]);				 

		 }
	   }
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('Consumo Servicios'); 		
		 $objActSheet->getColumnDimension('I')->setWidth(30);	
		 $objActSheet->getColumnDimension('J')->setWidth(30);		
		 $objActSheet->getColumnDimension('K')->setWidth(30);		
		
		 $objActSheet->getColumnDimension('S')->setWidth(30);
		 $objActSheet->getColumnDimension('T')->setWidth(30);  
		 $objActSheet->getColumnDimension('U')->setWidth(30); 
		 $objActSheet->getColumnDimension('V')->setWidth(30);
	
	
//Formato General
//El ancho de columna
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
		 
    //Formato primera Fila 
    $objStyleA5 = $objActSheet ->getStyle('A1:Z1');
    //$objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontA5 = $objStyleA5->getFont(); 
    $objFontA5->setName('Arial'); 
    $objFontA5->setSize(11); 
    $objFontA5->setBold(true); 
    //$objFontA5->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    //$objFontA5 ->getColor()->setARGB('FFFF0000') ;
    //$objFontA5 ->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE); 
	//Establecer la alineación 
    $objAlignA5 = $objStyleA5->getAlignment(); 
    $objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);	 
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="ConsumoServicios.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>
			
  