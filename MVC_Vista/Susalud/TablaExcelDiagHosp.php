<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">  -->      
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <!--<script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <!--<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>-->
     	

    
    </head>
    
    <body> 
    
    <!--inicia nuevo filter, exportar pdf,csv,excel y copy, paginacion-->
<script src="../../tables/ga.js" async type="text/javascript"></script><script type="text/javascript" src="../../tables/site.js">
</script>
<!--<script type="text/javascript" src="../../tables/dynamic.php" async>
</script>-->
<!--<script type="text/javascript" language="javascript" src="../../tables/jquery-1.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/jquery.js">
</script>-->
<script type="text/javascript" language="javascript" src="../../tables/dataTables.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/dataTables_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/buttons_002.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/jszip.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/pdfmake.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/buttons_003.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/buttons_004.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/buttons.js">
</script>
<script type="text/javascript" language="javascript" src="../../tables/demo.js">
</script>
<link rel="stylesheet" type="text/css" href="../../tables/dataTables.css"><!--STILO A LA TABLA-->
<link rel="stylesheet" type="text/css" href="../../tables/buttons.css"> <!--MENSAJE DEL BOTON COPIAR-->
<!--fin nuevo fiter, exportar pdf,csv,excel y copy, paginacion-->

<script type="text/javascript" class="init">
    $(document).ready(function () {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                {//valores por defecto orientation:'portrait' and pageSize:'A4',
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'A4'
                }
            ]
        });
    });
</script>   

        <table id="example" width="2626" border="1" cellspacing="0" style="font-size:12px">
         <tr>
            <th width="100" rowspan="2">Nro</th>
            <th colspan="10" >DATOS DEL PACIENTE</th>
            <th colspan="8" >DATOS HOSPITALIZACION</th>
            <th colspan="12" >DIAGNOSTICOS</th>
          </tr>
          <tr>
            <th width="100">Hist.Clin.</th>
            <th width="100">DNI</th>
            <th width="110">Apellidos</th>
            <th width="110">Nombres</th>
            <th width="110">Sexo</th>
            <th width="80">Edad</th>
            <th width="90">Tipo Edad</th>
            <th width="100">Fecha Nac. </th>
            <th width="100">Ubigeo (Distrito Proced.)</th>               
            <th width="100">Tipo Seguro</th>  
            
            <th width="100">Fecha Ing.</th>
            <th width="100">Hora Ing.</th>
            <th width="100">Especialidad Ing.</th>  
            <th width="100">Fecha Egr.</th>
            <th width="100">Servicio Egr.</th>
            <th width="100">Especialidad Egr.</th>
            <th width="100">Condicion Egr.</th>              
            <th width="100">Origen</th>
            <th width="50">Diag. Ing. 1</th>
            <th width="50">Diag. Ing. 2</th>
            <th width="50">Diag. Ing. 3</th>
            <th width="50">Diag. Ing. 4</th>
            <th width="50">Diag. Alta 1</th>
            <th width="50">Diag. Alta 2</th>
            <th width="50">Diag. Alta 3</th>
            <th width="50">Diag. Alta 4</th>
            
            <th width="50">Diag. Mort. 1</th>
            <th width="50">Diag. Mort. 2</th>
            <th width="50">Diag. Mort. 3</th>
            <th width="50">Diag. Mort. 4</th>
            <th width="50">DNI Med.Egreso</th>
            <th width="120">Nombres Med.Egreso</th>
          </tr>
            <?php 
            //RegTemp_IngresosEgresosHospitalizacionM($Anio, $Mes);
            $resultados=ListarIngresosEgresosHospitalizacionM($Anio, $Mes);						 
            if($resultados!=NULL){			
                for ($i=0; $i < count($resultados); $i++) {
                
                  $NroHistoriaClinica = $resultados[$i]["NroHistoriaClinica"];
                  $NroDocumento = $resultados[$i]["NroDocumento"];
                  $APELLIDOSPACIENTE = $resultados[$i]["APELLIDOSPACIENTE"];
                  $NOMBRESPACIENTE = $resultados[$i]["NOMBRESPACIENTE"];		 
                  $IdTipoSexo = $resultados[$i]["IdTipoSexo"];
                  if($IdTipoSexo=='1'){
                    $sexo='MASCULINO';  
                  }else{
                      $sexo='FEMENINO';  
                  }
                  $EDAD = $resultados[$i]["EDAD"];
				  $TIPOEDAD = ($resultados[$i]["TIPOEDAD"]);
				  
                  $FechaNacimiento = $resultados[$i]["FechaNacimiento"];
                  if($FechaNacimiento!=NULL){
                      $FechaNacimiento=vfecha(substr($FechaNacimiento,0,10));
                  }else{
                      $FechaNacimiento="";
                  }
                  $NroDocumento = $resultados[$i]["NroDocumento"];
                  
                  $FechaIngreso = $resultados[$i]["FechaIngreso"];
                  if($FechaIngreso!=NULL){
                      $FechaIngreso=vfecha(substr($FechaIngreso,0,10));
                  }else{
                      $FechaIngreso="";
                  }
                  $FechaEgreso = $resultados[$i]["FechaEgreso"];
                  if($FechaEgreso!=NULL){
                      $FechaEgreso=vfecha(substr($FechaEgreso,0,10));
                  }else{
                      $FechaEgreso="";
                  }
                  $especialidadIngreso = $resultados[$i]["especialidadIngreso"];
                  $especialidadEgreso = $resultados[$i]["especialidadEgreso"];
                  
                  $DX_ING1 = $resultados[$i]["DX_ING1"];
                  $DX_ING2 = $resultados[$i]["DX_ING2"];
                  $DX_ING3 = $resultados[$i]["DX_ING3"];
                  $DX_ING4 = $resultados[$i]["DX_ING4"];
                  
                  $DX_ALTA1 = $resultados[$i]["DX_ALTA1"];
                  $DX_ALTA2 = $resultados[$i]["DX_ALTA2"];
                  $DX_ALTA3 = $resultados[$i]["DX_ALTA3"];
                  $DX_ALTA4 = $resultados[$i]["DX_ALTA4"];
                             
                  $DX_Mortalidad1 = $resultados[$i]["DX_Mortalidad1"];
                  $DX_Mortalidad2 = $resultados[$i]["DX_Mortalidad2"];
                  $DX_Mortalidad3 = $resultados[$i]["DX_Mortalidad3"];
                  $DX_Mortalidad4 = $resultados[$i]["DX_Mortalidad4"];
                  
                  /*$DX_Complicacion1 = $resultados[$i]["DX_Complicacion1"];
                  $DX_Complicacion2 = $resultados[$i]["DX_Complicacion2"];
                  $DX_Complicacion3 = $resultados[$i]["DX_Complicacion3"];
                  $DX_Complicacion4 = $resultados[$i]["DX_Complicacion4"];	*/
				  
				  $IDDISTRITOPROCEDENCIA = $resultados[$i]["IDDISTRITOPROCEDENCIA"];
				  $SEGURO = $resultados[$i]["SEGURO"];	 
				  $HoraIngreso = $resultados[$i]["HoraIngreso"];
				  $SERVICIOEGRESO = $resultados[$i]["SERVICIOEGRESO"];	
				  $CONDICIONSALIDA = $resultados[$i]["CONDICIONSALIDA"];
				  $DescripcionTOA = $resultados[$i]["DescripcionTOA"];//Descripcion TiposOrigenAtencion	

          $DNIMedicoEgreso = $resultados[$i]["DNIMedicoEgreso"];
          $NombreMedicoEgreso = $resultados[$i]["NombreMedicoEgreso"];
                             
            ?>  
        
                  <tr>
                    <td bgcolor="#FFFF99"><?php echo $i+1; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $NroHistoriaClinica; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $NroDocumento; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo ($APELLIDOSPACIENTE); ?></td>
                    <td bgcolor="#CEFFFF"><?php echo ($NOMBRESPACIENTE); ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $sexo; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $EDAD; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $TIPOEDAD; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $FechaNacimiento; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $IDDISTRITOPROCEDENCIA; ?></td>
                    <td bgcolor="#CEFFFF"><?php echo $SEGURO; ?></td>
                    <td><?php echo $FechaIngreso; ?></td>
                    <td><?php echo $HoraIngreso; ?></td>
                    <td><?php echo ($especialidadIngreso); ?></td>
                    <td><?php echo $FechaEgreso; ?></td>
                    <td><?php echo $SERVICIOEGRESO; ?></td>
                    <td><?php echo ($especialidadEgreso); ?></td>
                    <td><?php echo $CONDICIONSALIDA; ?></td>
                    <td><?php echo $DescripcionTOA; ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ING1); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ING2); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ING3); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ING4); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ALTA1); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ALTA2); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ALTA3); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_ALTA4); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_Mortalidad1); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_Mortalidad2); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_Mortalidad3); ?></td>
                    <td bgcolor="#FFFF99"><?php echo ($DX_Mortalidad4); ?></td>
                    <td><?php echo $DNIMedicoEgreso; ?></td>
                    <td><?php echo $NombreMedicoEgreso; ?></td>
                  </tr>
          <?php	
                 }
               }            
         ?>
        </table>	
      
    </body>
</html>




						
  