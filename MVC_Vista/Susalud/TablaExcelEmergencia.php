<table width="200" border="1">
			<tr>
				<th field="CodIPress" >renipress</th>
				<th field="CodUgiPress" >e_ubig</th>
				<th field="CodIgss" >cod_disa</th>
				<th field="CodRed" >cod_red</th>               
				<th field="CodMicrored" >cod_mred</th>                
                <th field="FECHAING" >fecate</th> 
                <th field="HORAING" width="70">horate</th> 
                <th field="HISTORIACLINICA" >numhc</th> 
                <th field="NDOC" >doc_iden</th> 
                <th field="ETNIA" >etnia</th>                 
                <th field="SEGURO" >financia</th> 
                <th field="SEXO" >sexo</th> 
                <th field="EDAD" >edad</th> 
                <th field="TIPOEDAD" >tipoedad</th>                   
                <th field="NOMBRESPACIENTE" >nomb</th> 
                <th field="APELLIDOSPACIENTE" >apell</th> 
                <th field="DIRECCION" >direcc</th>  
                <th field="IDDISTRITODOMICILIO" >ubig_resha</th> 
                <th field="IDDISTRITOPROCEDENCIA" >ubig_proce</th>                    
                <th field="NOMBREACOMPA" >acompana</th>
                <th field="DNIACOMPA" >adoc_iden</th>
                <th field="MOTIVOATCEMERGENCIA" >motaten</th>
                <th field="UBIGEOSITIOOCURRENCIA" >sitocurren</th>
                <th field="SERVICIOATENCION" >ups</th> 
                
                <th field="DX_ING1" >coddiag1</th> 
                <th field="TIPODIAG1" >tipdiag1</th>                 
                <th field="DX_ING2" >coddiag2</th> 
                <th field="TIPODIAG2" >tipdiag2</th>                 
                <th field="DX_ING3" >coddiag3</th> 
                <th field="TIPODIAG3" >tipdiag3</th> 
                <th field="DX_ING4" >coddiag4</th> 
                <th field="TIPODIAG4" >tipdiag4</th>
                
                <th field="OTROPROCE_CPT1" >codcpt1</th> 
                <th field="OTROPROCE_CPT2" >codcpt2</th> 
                <th field="OTROPROCE_CPT3" >codcpt3</th> 
                <th field="OTROPROCE_CPT4" >codcpt4</th>                
                <th field="CONDICIONSALIDA" >condicion</th>
                <th field="FECHAEGRESO" >fecegr</th> 
                <th field="HORAEGRESO" >horegr</th> 
                <th field="DESTINOATENCION" >destino</th>                  
                 
                <th field="CODEESSDESTINO" >des_eess</th> 
                <th field="UPSSIESHOSPITALIZADO" >des_ups</th>                
                <th field="MEDICO" >codpsal</th> 
                <th field="PASOOBSERVACION" >observ</th> 
                <th field="FECHAINOBS" >ofecing</th> 
                <th field="HORAINOBS" >ohoring</th>                
                <th field="FECHAOUTOBS" >ofecegr</th>
                <th field="HORAOUTOBS" >ohoregr</th> 
                <th field="TOTALESTAENOBS" >totalest</th> 
                <th field="NCAMAENOBS" >ocama</th> 
                
                <th field="MEDICOOBS" >ocodpsal</th> 
                <th field="CODIGO1OBS" >ocoddiag1</th>                
                <th field="CODIGO2OBS" >ocoddiag2</th> 
                <th field="FECHAREGISTRO" >fechareg</th> 
                <th field="ESTADOREGISTRO" >estado</th> 
                <th field="IdTipoGravedad" >prioridad</th> 
                <th field="form_ing" >form_ing</th> 
			</tr>
	<?php 
			$CodIPress=$_REQUEST["CodIpress"];
			$CodUgiPress=$_REQUEST["CodUgiPres"];
			$CodIgss=$_REQUEST["CodIgss"];
			$CodRed=$_REQUEST["CodRed"];
			$CodMicrored=$_REQUEST["CodMicrored"];
				
			//ElimTemp_IngresosEgresosEmergenciaM();
			//RegTemp_IngresosEgresosEmergenciaM($Anio, $Mes);	
			$resultados = ListarIngresosEgresosEmergenciaM($Anio, $Mes); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {														
					//$AnioMes = $resultados[$i]["AnioMes"];
					//$CodIPress = $resultados[$i]["CodIPress"];
					//$CodUgiPress = $resultados[$i]["CodUgiPress"];					
					$FECHAING = $resultados[$i]["FECHAING"];
					$HORAING = $resultados[$i]["HORAING"];
					$HISTORIACLINICA = $resultados[$i]["HISTORIACLINICA"];
					
					$tipodoc=$resultados[$i]["TIPODOC"];
					$doc = $resultados[$i]["NDOC"];
					if(trim($doc)!="" && (int)$doc!=0){
					$NDOC = $tipodoc.$doc;
					}else{
					$NDOC = "0";	
					}				
					
					$ETNIA = $resultados[$i]["ETNIA"];//10				
					$SEGURO = $resultados[$i]["SEGURO"];//11
					$SEXO = $resultados[$i]["SEXO"];//12
					$EDAD = $resultados[$i]["EDAD"];//13
					$TIPOEDAD = $resultados[$i]["TIPOEDAD"];//14					
										
					$nopermitidos = array( "'" , '"', "," , ";" , "&" , "+" );
									
					$NOMBRESPACIENTE = trim($resultados[$i]["NOMBRESPACIENTE"]);
					$NOMBRESPACIENTE = str_replace($nopermitidos, "", $NOMBRESPACIENTE);//15				
					$APELLIDOSPACIENTE = trim($resultados[$i]["APELLIDOSPACIENTE"]);
					$APELLIDOSPACIENTE = str_replace($nopermitidos, "", $APELLIDOSPACIENTE);//16
					
					$DIRECCION=trim($resultados[$i]["DIRECCION"]);
					$DIRECCION = str_replace($nopermitidos, "", $DIRECCION);//17
					
					$IDDISTRITODOMICILIO=$resultados[$i]["IDDISTRITODOMICILIO"];//18
					$IDDISTRITOPROCEDENCIA=$resultados[$i]["IDDISTRITOPROCEDENCIA"];//19
					
					$NOMBREACOMPA=$resultados[$i]["NOMBREACOMPA"];
					$NOMBREACOMPA = str_replace($nopermitidos, "", $NOMBREACOMPA);//20
					
					$DNIACOMPA=$resultados[$i]["DNIACOMPA"];//21
					$MOTIVOATCEMERGENCIA=$resultados[$i]["MOTIVOATCEMERGENCIA"];//22
					$UBIGEOSITIOOCURRENCIA=$resultados[$i]["UBIGEOSITIOOCURRENCIA"];//23
					
					$SERVICIOATENCION=$resultados[$i]["SERVICIOATENCION"];
					$SERVICIOATENCION = str_replace($nopermitidos, "", $SERVICIOATENCION);//24
					
					$DX_ING1=$resultados[$i]["DX_ING1"];//25					
					$TIPODIAG1=$resultados[$i]["TIPODIAG1"];//26
					if($resultados[$i]["TIPODIAG1"]==NULL && $resultados[$i]["DX_ING1"]!=NULL){	
						$TIPODIAG1='D';				
					}else{
						$TIPODIAG1=$resultados[$i]["TIPODIAG1"];
					}
										
					$DX_ING2=$resultados[$i]["DX_ING2"];//27
					$TIPODIAG2=$resultados[$i]["TIPODIAG2"];//28
					if($resultados[$i]["TIPODIAG2"]==NULL && $resultados[$i]["DX_ING2"]!=NULL){	
						$TIPODIAG2='D';				
					}else{
						$TIPODIAG2=$resultados[$i]["TIPODIAG2"];
					}	
									
					$DX_ING3=$resultados[$i]["DX_ING3"];//29
					$TIPODIAG3=$resultados[$i]["TIPODIAG3"];//30
					if($resultados[$i]["TIPODIAG3"]==NULL && $resultados[$i]["DX_ING3"]!=NULL){	
						$TIPODIAG3='D';				
					}else{
						$TIPODIAG3=$resultados[$i]["TIPODIAG3"];
					}
					
					$DX_ING4=$resultados[$i]["DX_ING4"];//31
					$TIPODIAG4=$resultados[$i]["TIPODIAG4"];//32
					if($resultados[$i]["TIPODIAG4"]==NULL && $resultados[$i]["DX_ING4"]!=NULL){	
						$TIPODIAG4='D';				
					}else{
						$TIPODIAG4=$resultados[$i]["TIPODIAG4"];
					}
					
					$OTROPROCE_CPT1=$resultados[$i]["OTROPROCE_CPT1"];//33
					$OTROPROCE_CPT2=$resultados[$i]["OTROPROCE_CPT2"];//34
					$OTROPROCE_CPT3=$resultados[$i]["OTROPROCE_CPT3"];//35
					$OTROPROCE_CPT4=$resultados[$i]["OTROPROCE_CPT4"];//36					
					$CONDICIONSALIDA=$resultados[$i]["CONDICIONSALIDA"];//37
					$FECHAEGRESO=$resultados[$i]["FECHAEGRESO"];//38
					$HORAEGRESO=$resultados[$i]["HORAEGRESO"];//39
					$DESTINOATENCION=$resultados[$i]["DESTINOATENCION"];//40
					
					$CODEESSDESTINO="";//41
					$UPSSIESHOSPITALIZADO="";//42					
					$MEDICO=$resultados[$i]["MEDICO"];//CODPSAL 43
					$PASOOBSERVACION="";//44
					$FECHAINOBS="";//45
					$HORAINOBS="";//46					
					$FECHAOUTOBS="";//47
					$HORAOUTOBS="";//48
					$TOTALESTAENOBS="";//49
					$NCAMAENOBS="";//50
					
					$MEDICOOBS="";//51
					$CODIGO1OBS="";//52					
					$CODIGO2OBS="";//53
					$FECHAREGISTRO="";//54
					$ESTADOREGISTRO="";//55
					
					$IdTipoGravedad=$resultados[$i]["IdTipoGravedad"];//56
			  		 
	?>  

          <tr>
				<td field="CodIPress" ><?php echo $CodIPress ?></td>
				<td field="CodUgiPress" ><?php echo $CodUgiPress ?></td>
				<td field="CodIgss" ><?php echo $CodIgss ?></td>
				<td field="CodRed" ><?php echo $CodRed ?></td>               
				<td field="CodMicrored" ><?php echo $CodMicrored ?></td>                
                <td field="FECHAING" ><?php echo $FECHAING ?></td> 
                <td field="HORAING"><?php echo $HORAING ?></td> 
                <td field="HISTORIACLINICA" ><?php echo $HISTORIACLINICA ?></td> 
                <td field="NDOC" ><?php echo $NDOC ?></td> 
                <td field="ETNIA" ><?php echo $ETNIA ?></td>                 
                <td field="SEGURO" ><?php echo $SEGURO ?></td> 
                <td field="SEXO" ><?php echo $SEXO ?></td> 
                <td field="EDAD" ><?php echo $EDAD ?></td> 
                <td field="TIPOEDAD" ><?php echo $TIPOEDAD ?></td>                   
                <td field="NOMBRESPACIENTE" ><?php echo $NOMBRESPACIENTE ?></td> 
                <td field="APELLIDOSPACIENTE" ><?php echo $APELLIDOSPACIENTE ?></td> 
                <td field="DIRECCION" ><?php echo $DIRECCION ?></td>  
                <td field="IDDISTRITODOMICILIO" ><?php echo $IDDISTRITODOMICILIO ?></td> 
                <td field="IDDISTRITOPROCEDENCIA" ><?php echo $IDDISTRITOPROCEDENCIA ?></td>                    
                <td field="NOMBREACOMPA" ><?php echo $NOMBREACOMPA ?></td>
                <td field="DNIACOMPA" ><?php echo $DNIACOMPA ?></td>
                <td field="MOTIVOATCEMERGENCIA" ><?php echo $MOTIVOATCEMERGENCIA ?></td>
                <td field="UBIGEOSITIOOCURRENCIA" ><?php echo $UBIGEOSITIOOCURRENCIA ?></td>
                <td field="SERVICIOATENCION" ><?php echo $SERVICIOATENCION ?></td> 
                
                <td field="DX_ING1" ><?php echo $DX_ING1 ?></td> 
                <td field="TIPODIAG1" ><?php echo $TIPODIAG1 ?></td>                 
                <td field="DX_ING2" ><?php echo $DX_ING2 ?></td> 
                <td field="TIPODIAG2" ><?php echo $TIPODIAG2 ?></td>                 
                <td field="DX_ING3" ><?php echo $DX_ING3 ?></td> 
                <td field="TIPODIAG3" ><?php echo $TIPODIAG3 ?></td> 
                <td field="DX_ING4" ><?php echo $DX_ING4 ?></td> 
                <td field="TIPODIAG4" ><?php echo $TIPODIAG4 ?></td>
                
                <td field="OTROPROCE_CPT1" ><?php echo $OTROPROCE_CPT1 ?></td> 
                <td field="OTROPROCE_CPT2" ><?php echo $OTROPROCE_CPT2 ?></td> 
                <td field="OTROPROCE_CPT3" ><?php echo $OTROPROCE_CPT3 ?></td> 
                <td field="OTROPROCE_CPT4" ><?php echo $OTROPROCE_CPT4 ?></td>                
                <td field="CONDICIONSALIDA" ><?php echo $CONDICIONSALIDA ?></td>
                <td field="FECHAEGRESO" ><?php echo $FECHAEGRESO ?></td> 
                <td field="HORAEGRESO" ><?php echo $HORAEGRESO ?></td> 
                <td field="DESTINOATENCION" ><?php echo $DESTINOATENCION ?></td>                  
                 
                <td field="CODEESSDESTINO" ><?php echo $CODEESSDESTINO ?></td> 
                <td field="UPSSIESHOSPITALIZADO" ><?php echo $UPSSIESHOSPITALIZADO ?></td>                
                <td field="MEDICO" ><?php echo $MEDICO ?></td> 
                <td field="PASOOBSERVACION" ><?php echo $PASOOBSERVACION ?></td> 
                <td field="FECHAINOBS" ><?php echo $FECHAINOBS ?> </td> 
                <td field="HORAINOBS" ><?php echo $HORAINOBS ?></td>                
                <td field="FECHAOUTOBS" ><?php echo $FECHAOUTOBS ?></td>
                <td field="HORAOUTOBS" ><?php echo $HORAOUTOBS ?></td> 
                <td field="TOTALESTAENOBS" ><?php echo $TOTALESTAENOBS ?></td> 
                <td field="NCAMAENOBS" ><?php echo $NCAMAENOBS ?></td> 
                
                <td field="MEDICOOBS" ><?php echo $MEDICOOBS ?></td> 
                <td field="CODIGO1OBS" ><?php echo $CODIGO1OBS ?></td>                
                <td field="CODIGO2OBS" ><?php echo $CODIGO2OBS ?></td> 
                <td field="FECHAREGISTRO" ><?php echo $FECHAREGISTRO ?></td> 
                <td field="ESTADOREGISTRO" ><?php echo $ESTADOREGISTRO ?></td> 
                
                <td field="IdTipoGravedad" ><?php echo $IdTipoGravedad ?></td> 
                <td field="form_ing" ><?php echo '1' ?></td> 
		 </tr>
  <?php	
		 }
	   }
	//}
 ?>
</table>

						
  