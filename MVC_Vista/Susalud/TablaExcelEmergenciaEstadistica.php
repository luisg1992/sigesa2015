<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <!--<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">  -->      
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>
    
    </head>
    
    <body> 
    
<table id="example" width="200" border="1">
			<tr>
            	<th field="CodIPress" >renipress</th>
				<th field="CodUgiPress" >e_ubig</th>
				<th field="CodIgss" >cod_disa</th>
				<th field="CodRed" >cod_red</th>               
				<th field="CodMicrored" >cod_mred</th>
				<th field="FECHAING" >FECHA DE INGRESO</th> 
                <th field="HORAING" width="70">HORA DE INGRESO</th> 
                <th field="HISTORIACLINICA" >HISTORIA CLINICA</th> 
                <th field="NDOC" >DNI</th> 
                <th field="ETNIA" >etnia</th>                 
                <th field="SEGURO" >financia</th>
                <th field="SEXO" >TIPO DE PACIENTE ( SIS, PARTICULAR Y SOAT)</th> 
                <th field="SEXO" >SEXO</th>
                <th field="EDAD" >DESCRIPCION SEXO</th> 
                <th field="EDAD" >EDAD</th> 
                <th field="TIPOEDAD" >TIPO EDAD</th>
                <th field="NOMBRESPACIENTE" >DESCRIPCION TIPO EDAD</th>                   
                <th field="NOMBRESPACIENTE" >nomb</th> 
                <th field="APELLIDOSPACIENTE" >apell</th> 
                <th field="DIRECCION" >direcc</th>  
                <th field="IDDISTRITODOMICILIO" >ubig_resha</th>
                <th field="IDDISTRITOPROCEDENCIA" >DESCRIPCION UBIGEO DOMICILIO</th> 
                <th field="IDDISTRITOPROCEDENCIA" >ubig_proce</th>
                <th field="NOMBREACOMPA" >DESCRIPCION UBIGEO PROCEDENCIA</th>                    
                <th field="NOMBREACOMPA" >acompana</th>
                <th field="DNIACOMPA" >adoc_iden</th>
                <th field="MOTIVOATCEMERGENCIA" >motaten</th>
                <th field="UBIGEOSITIOOCURRENCIA" >sitocurren</th>
                <th field="SERVICIOATENCION" >ups (CODIGO DE SERVICIO DE INGRESO)</th> 
                
                <th field="DX_ING1" >coddiag1 INGRESO 1</th> 
                <th field="DX_ING2" >DESCRIPCION CIE10 1</th>                 
                <th field="DX_ING2" >coddiag2 INGRESO 2</th> 
                <th field="DX_ING3" >DESCRIPCION CIE10 2</th>                 
                <th field="DX_ING3" >coddiag3 INGRESO 3</th> 
                <th field="DX_ING4" >DESCRIPCION CIE10 3</th> 
                <th field="DX_ING4" >coddiag4 INGRESO 4</th> 
                <th field="OTROPROCE_CPT1" >DESCRIPCION CIE10 4</th>
                
                <th field="OTROPROCE_CPT1" >coddiag1 ALTA 1</th>
                <th field="OTROPROCE_CPT2" >DESCRIPCION CIE10 ALTA1</th> 
                <th field="OTROPROCE_CPT2" >coddiag1 ALTA 2</th>
                <th field="OTROPROCE_CPT3" >DESCRIPCION CIE10 ALTA2</th> 
                <th field="OTROPROCE_CPT3" >coddiag1 ALTA 3</th>
                <th field="OTROPROCE_CPT4" >DESCRIPCION CIE10 ALTA3</th> 
                <th field="OTROPROCE_CPT4" >coddiag1 ALTA 4</th>
                <th field="CONDICIONSALIDA" >DESCRIPCION CIE10 ALTA4</th>                
                <th field="CONDICIONSALIDA" >condicion Salida</th>
                <th field="FECHAEGRESO" >Descripcion Condicion Salida</th>
                <th field="FECHAEGRESO" >fecegr</th> 
                <th field="HORAEGRESO" >HORA DE EGRESO</th> 
                <th field="DESTINOATENCION" >destino</th>
                <th field="CODEESSDESTINO" >DESTINO DE EGRESOS  ( CITADO, A SU CASA, OBS. , HOSP. Y FALLECIDO)</th>                  
                 
                <th field="CODEESSDESTINO" >des_eess</th> 
                <th field="UPSSIESHOSPITALIZADO" >des_ups</th>                
                <th field="MEDICO" >codpsal</th> 
                <th field="PASOOBSERVACION" >observ</th> 
                <th field="FECHAINOBS" >ofecing</th> 
                <th field="HORAINOBS" >ohoring</th>                
                <th field="FECHAOUTOBS" >ofecegr</th>
                <th field="HORAOUTOBS" >ohoregr</th> 
                <th field="TOTALESTAENOBS" >totalest</th> 
                <th field="NCAMAENOBS" >ocama</th> 
                
                <th field="MEDICOOBS" >ocodpsal</th> 
                <th field="CODIGO1OBS" >ocoddiag1</th>                
                <th field="CODIGO2OBS" >ocoddiag2</th> 
                <th field="FECHAREGISTRO" >fechareg</th> 
                <th field="ESTADOREGISTRO" >estado</th> 
                <th field="IdTipoGravedad" >prioridad</th>
                <th field="form_ing" >TIPO DE PRIORIDADES ( I,II, III Y IV)</th>
                <th field="form_ing" >TOPICO (NOMBRE DEL SERVICIO)</th>
                <th field="form_ing" >CODIGO CAUSA EXTERNA</th>
                <th field="form_ing" >DESCRIPCION CAUSA EXTERNA </th>
                <th field="form_ing" >TIPO EVENTO</th>
                <th field="form_ing" >DESCRIPCION TIPO EVENTO</th>
            </tr>
	<?php 
			$CodIPress=$_REQUEST["CodIpress"];
			$CodUgiPress=$_REQUEST["CodUgiPres"];
			$CodIgss=$_REQUEST["CodIgss"];
			$CodRed=$_REQUEST["CodRed"];
			$CodMicrored=$_REQUEST["CodMicrored"];
				
			//ElimTemp_IngresosEgresosEmergenciaM();
			//RegTemp_IngresosEgresosEmergenciaM($Anio, $Mes);	
			$resultados = ListarIngresosEgresosEmergenciaM($Anio, $Mes); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {														
					//$AnioMes = $resultados[$i]["AnioMes"];
					//$CodIPress = $resultados[$i]["CodIPress"];
					//$CodUgiPress = $resultados[$i]["CodUgiPress"];					
					$FECHAING = $resultados[$i]["FECHAING"];
					$HORAING = $resultados[$i]["HORAING"];
					$HISTORIACLINICA = $resultados[$i]["HISTORIACLINICA"];
					
					$tipodoc=$resultados[$i]["TIPODOC"];
					$doc = $resultados[$i]["NDOC"];
					if(trim($doc)!="" && (int)$doc!=0){
					$NDOC = $doc;
					}else{
					$NDOC = "0";	
					}				
					
					$ETNIA = $resultados[$i]["ETNIA"];//10				
					$SEGURO = $resultados[$i]["SEGURO"];//11
					$SEXO = $resultados[$i]["SEXO"];//12
					$EDAD = $resultados[$i]["EDAD"];//13
					$TIPOEDAD = $resultados[$i]["TIPOEDAD"];//14					
										
					$nopermitidos = array( "'" , '"', "," , ";" , "&" , "+" );
									
					$NOMBRESPACIENTE = trim($resultados[$i]["NOMBRESPACIENTE"]);
					$NOMBRESPACIENTE = str_replace($nopermitidos, "", $NOMBRESPACIENTE);//15				
					$APELLIDOSPACIENTE = trim($resultados[$i]["APELLIDOSPACIENTE"]);
					$APELLIDOSPACIENTE = str_replace($nopermitidos, "", $APELLIDOSPACIENTE);//16
					
					$DIRECCION=trim($resultados[$i]["DIRECCION"]);
					$DIRECCION = str_replace($nopermitidos, "", $DIRECCION);//17
					
					$XIDDISTRITODOMICILIO=(string) (trim($resultados[$i]["IDDISTRITODOMICILIO"]));//18
					if(strlen($XIDDISTRITODOMICILIO)==5){
						$IDDISTRITODOMICILIO="0".$XIDDISTRITODOMICILIO; //el 0 adelante no funciona porque en excel lo esta reconociento como un numero
					}else{
						$IDDISTRITODOMICILIO=$XIDDISTRITODOMICILIO;
					}
					//$IDDISTRITODOMICILIO=str_pad((string)$resultados[$i]["IDDISTRITODOMICILIO"], 6, "0", STR_PAD_LEFT);									
					
					$XIDDISTRITOPROCEDENCIA=(string) (trim($resultados[$i]["IDDISTRITOPROCEDENCIA"]));//19
					if(strlen($XIDDISTRITOPROCEDENCIA)==5){
						$IDDISTRITOPROCEDENCIA="0".$XIDDISTRITOPROCEDENCIA; //el 0 adelante no funciona porque en excel lo esta reconociento como un numero
					}else{
						$IDDISTRITOPROCEDENCIA=$XIDDISTRITOPROCEDENCIA;
					}
					//$IDDISTRITOPROCEDENCIA=str_pad((string)$resultados[$i]["IDDISTRITOPROCEDENCIA"], 6, "0", STR_PAD_LEFT); 								
					
					$NOMBREACOMPA=$resultados[$i]["NOMBREACOMPA"];
					$NOMBREACOMPA = str_replace($nopermitidos, "", $NOMBREACOMPA);//20
					
					$DNIACOMPA=$resultados[$i]["DNIACOMPA"];//21
					$MOTIVOATCEMERGENCIA=$resultados[$i]["MOTIVOATCEMERGENCIA"];//22
					$UBIGEOSITIOOCURRENCIA=$resultados[$i]["UBIGEOSITIOOCURRENCIA"];//23
					
					$SERVICIOATENCION=$resultados[$i]["SERVICIOATENCION"];
					$SERVICIOATENCION = str_replace($nopermitidos, "", $SERVICIOATENCION);//24
					
					$DX_ING1=$resultados[$i]["DX_ING1"];//25					
					$TIPODIAG1=$resultados[$i]["TIPODIAG1"];//26
					if($resultados[$i]["TIPODIAG1"]==NULL && $resultados[$i]["DX_ING1"]!=NULL){	
						$TIPODIAG1='D';				
					}else{
						$TIPODIAG1=$resultados[$i]["TIPODIAG1"];
					}
										
					$DX_ING2=$resultados[$i]["DX_ING2"];//27
					$TIPODIAG2=$resultados[$i]["TIPODIAG2"];//28
					if($resultados[$i]["TIPODIAG2"]==NULL && $resultados[$i]["DX_ING2"]!=NULL){	
						$TIPODIAG2='D';				
					}else{
						$TIPODIAG2=$resultados[$i]["TIPODIAG2"];
					}	
									
					$DX_ING3=$resultados[$i]["DX_ING3"];//29
					$TIPODIAG3=$resultados[$i]["TIPODIAG3"];//30
					if($resultados[$i]["TIPODIAG3"]==NULL && $resultados[$i]["DX_ING3"]!=NULL){	
						$TIPODIAG3='D';				
					}else{
						$TIPODIAG3=$resultados[$i]["TIPODIAG3"];
					}
					
					$DX_ING4=$resultados[$i]["DX_ING4"];//31
					$TIPODIAG4=$resultados[$i]["TIPODIAG4"];//32
					if($resultados[$i]["TIPODIAG4"]==NULL && $resultados[$i]["DX_ING4"]!=NULL){	
						$TIPODIAG4='D';				
					}else{
						$TIPODIAG4=$resultados[$i]["TIPODIAG4"];
					}
					
					$OTROPROCE_CPT1=$resultados[$i]["OTROPROCE_CPT1"];//33
					$OTROPROCE_CPT2=$resultados[$i]["OTROPROCE_CPT2"];//34
					$OTROPROCE_CPT3=$resultados[$i]["OTROPROCE_CPT3"];//35
					$OTROPROCE_CPT4=$resultados[$i]["OTROPROCE_CPT4"];//36					
					$CONDICIONSALIDA=$resultados[$i]["CONDICIONSALIDA"];//37
					$FECHAEGRESO=$resultados[$i]["FECHAEGRESO"];//38
					$HORAEGRESO=$resultados[$i]["HORAEGRESO"];//39
					$DESTINOATENCION=$resultados[$i]["DESTINOATENCION"];//40
					
					$CODEESSDESTINO="";//41
					$UPSSIESHOSPITALIZADO="";//42					
					$MEDICO=$resultados[$i]["MEDICO"];//CODPSAL 43
					$PASOOBSERVACION="";//44
					$FECHAINOBS="";//45
					$HORAINOBS="";//46					
					$FECHAOUTOBS="";//47
					$HORAOUTOBS="";//48
					$TOTALESTAENOBS="";//49
					$NCAMAENOBS="";//50
					
					$MEDICOOBS="";//51
					$CODIGO1OBS="";//52					
					$CODIGO2OBS="";//53
					$FECHAREGISTRO="";//54
					$ESTADOREGISTRO="";//55
					
					$IdTipoGravedad=$resultados[$i]["IdTipoGravedad"];//56
					
					$DESCRIPCIONSEGURO=$resultados[$i]["DESCRIPCIONSEGURO"];
					$DESCRIPCIONDISTRITODOMICILIO=$resultados[$i]["DESCRIPCIONDISTRITODOMICILIO"];
					$DESCRIPCIONDISTRITOPROCEDENCIA=$resultados[$i]["DESCRIPCIONDISTRITOPROCEDENCIA"];
					$DESCRIPCIONDESTINOATENCION=$resultados[$i]["DESCRIPCIONDESTINOATENCION"];
					$DESCRIPCIONGRAVEDAD=$resultados[$i]["DESCRIPCIONGRAVEDAD"];
					
					$DX_INGDES1=$resultados[$i]["DX_INGDES1"];
					$DX_INGDES2=$resultados[$i]["DX_INGDES2"];
					$DX_INGDES3=$resultados[$i]["DX_INGDES3"];
					$DX_INGDES4=$resultados[$i]["DX_INGDES4"];
					
					$DX_ALTA1=$resultados[$i]["DX_ALTA1"];
					$DX_ALTADES1=$resultados[$i]["DX_ALTADES1"];
					
					$DX_ALTA2=$resultados[$i]["DX_ALTA2"];
					$DX_ALTADES2=$resultados[$i]["DX_ALTADES2"];
					
					$DX_ALTA3=$resultados[$i]["DX_ALTA3"];
					$DX_ALTADES3=$resultados[$i]["DX_ALTADES3"];
					
					$DX_ALTA4=$resultados[$i]["DX_ALTA4"];
					$DX_ALTADES4=$resultados[$i]["DX_ALTADES4"];
					
					$NombreServicio=$resultados[$i]["NombreServicio"];
					
					$codigoce=$resultados[$i]["codigoce"];
					$CausaExternaMorbilidad=$resultados[$i]["CausaExternaMorbilidad"];
					
					$DESCRIPCIONSEXO=$resultados[$i]["DESCRIPCIONSEXO"];
					$DESCRIPCIONTIPOEDAD=$resultados[$i]["DESCRIPCIONTIPOEDAD"];
					
					$IdTipoEvento=$resultados[$i]["IdTipoEvento"];
					$Descripciontipoevento=$resultados[$i]["Descripciontipoevento"];
			  		 
					$DESCRIPCIONCONDICIONSALIDA=$resultados[$i]["DESCRIPCIONCONDICIONSALIDA"];
	?>  

          <tr>
          		<td field="CodIPress" ><?php echo $CodIPress ?></td>
				<td field="CodUgiPress" ><?php echo $CodUgiPress ?></td>
				<td field="CodIgss" ><?php echo $CodIgss ?></td>
				<td field="CodRed" ><?php echo $CodRed ?></td>               
				<td field="CodMicrored" ><?php echo $CodMicrored ?></td>
				<td field="FECHAING" ><?php echo $FECHAING ?></td> 
                <td field="HORAING"><?php echo $HORAING ?></td> 
                <td field="HISTORIACLINICA" ><?php echo $HISTORIACLINICA ?></td> 
                <td field="NDOC" ><?php echo $NDOC ?></td> 
                <td field="ETNIA" ><?php echo $ETNIA ?></td>                 
                <td field="SEGURO" ><?php echo $SEGURO ?></td>
                <td field="SEXO" ><?php echo $DESCRIPCIONSEGURO ?></td> 
                <td field="SEXO" ><?php echo $SEXO ?></td>
                <td field="EDAD" ><?php echo $DESCRIPCIONSEXO ?></td> 
                <td field="EDAD" ><?php echo $EDAD ?></td> 
                <td field="TIPOEDAD" ><?php echo $TIPOEDAD ?></td>
                <td field="NOMBRESPACIENTE" ><?php echo $DESCRIPCIONTIPOEDAD ?></td>                   
                <td field="NOMBRESPACIENTE" ><?php echo $NOMBRESPACIENTE ?></td> 
                <td field="APELLIDOSPACIENTE" ><?php echo $APELLIDOSPACIENTE ?></td> 
                <td field="DIRECCION" ><?php echo $DIRECCION ?></td>  
                <td field="IDDISTRITODOMICILIO" ><?php echo $IDDISTRITODOMICILIO ?></td>
                <td field="IDDISTRITOPROCEDENCIA" ><?php echo $DESCRIPCIONDISTRITODOMICILIO ?></td> 
                <td field="IDDISTRITOPROCEDENCIA" ><?php echo $IDDISTRITOPROCEDENCIA ?></td>
                <td field="NOMBREACOMPA" ><?php echo $DESCRIPCIONDISTRITOPROCEDENCIA ?></td>                    
                <td field="NOMBREACOMPA" ><?php echo $NOMBREACOMPA ?></td>
                <td field="DNIACOMPA" ><?php echo $DNIACOMPA ?></td>
                <td field="MOTIVOATCEMERGENCIA" ><?php echo $MOTIVOATCEMERGENCIA ?></td>
                <td field="UBIGEOSITIOOCURRENCIA" ><?php echo $UBIGEOSITIOOCURRENCIA ?></td>
                <td field="SERVICIOATENCION" ><?php echo $SERVICIOATENCION ?></td> 
                
                <td field="DX_ING1" ><?php echo $DX_ING1 ?></td> 
                <td field="DX_ING2" ><?php echo $DX_INGDES1 ?></td>                 
                <td field="DX_ING2" ><?php echo $DX_ING2 ?></td> 
                <td field="DX_ING3" ><?php echo $DX_INGDES2 ?></td>                 
                <td field="DX_ING3" ><?php echo $DX_ING3 ?></td> 
                <td field="DX_ING4" ><?php echo $DX_INGDES3 ?></td> 
                <td field="DX_ING4" ><?php echo $DX_ING4 ?></td> 
                <td field="OTROPROCE_CPT1" ><?php echo $DX_INGDES4 ?></td>
                
                <td field="" ><?php echo $DX_ALTA1 ?></td>
                <td field="" ><?php echo $DX_ALTADES1 ?></td> 
                <td field="" ><?php echo $DX_ALTA2 ?></td>
                <td field="" ><?php echo $DX_ALTADES2 ?></td> 
                <td field="" ><?php echo $DX_ALTA3 ?></td>
                <td field="" ><?php echo $DX_ALTADES3 ?></td> 
                <td field="" ><?php echo $DX_ALTA4 ?></td>
                <td field="" ><?php echo $DX_ALTADES4 ?></td>                 
                <td field="CONDICIONSALIDA" ><?php echo $CONDICIONSALIDA ?></td>
                <td field="FECHAEGRESO" ><?php echo $DESCRIPCIONCONDICIONSALIDA ?></td>
                <td field="FECHAEGRESO" ><?php echo $FECHAEGRESO ?></td> 
                <td field="HORAEGRESO" ><?php echo $HORAEGRESO ?></td> 
                <td field="DESTINOATENCION" ><?php echo $DESTINOATENCION ?></td>
                <td field="CODEESSDESTINO" ><?php echo $DESCRIPCIONDESTINOATENCION ?></td>                  
                 
                <td field="CODEESSDESTINO" ><?php echo $CODEESSDESTINO ?></td> 
                <td field="UPSSIESHOSPITALIZADO" ><?php echo $UPSSIESHOSPITALIZADO ?></td>                
                <td field="MEDICO" ><?php echo $MEDICO ?></td> 
                <td field="PASOOBSERVACION" ><?php echo $PASOOBSERVACION ?></td> 
                <td field="FECHAINOBS" ><?php echo $FECHAINOBS ?> </td> 
                <td field="HORAINOBS" ><?php echo $HORAINOBS ?></td>                
                <td field="FECHAOUTOBS" ><?php echo $FECHAOUTOBS ?></td>
                <td field="HORAOUTOBS" ><?php echo $HORAOUTOBS ?></td> 
                <td field="TOTALESTAENOBS" ><?php echo $TOTALESTAENOBS ?></td> 
                <td field="NCAMAENOBS" ><?php echo $NCAMAENOBS ?></td> 
                
                <td field="MEDICOOBS" ><?php echo $MEDICOOBS ?></td> 
                <td field="CODIGO1OBS" ><?php echo $CODIGO1OBS ?></td>                
                <td field="CODIGO2OBS" ><?php echo $CODIGO2OBS ?></td> 
                <td field="FECHAREGISTRO" ><?php echo $FECHAREGISTRO ?></td> 
                <td field="ESTADOREGISTRO" ><?php echo $ESTADOREGISTRO ?></td> 
                
                <td field="IdTipoGravedad" ><?php echo $IdTipoGravedad ?></td>
                <td field="" ><?php echo $DESCRIPCIONGRAVEDAD ?></td>
                <td field="" ><?php echo $NombreServicio ?></td>
                <td field="" ><?php echo $codigoce ?></td>
                <td field="" ><?php echo $CausaExternaMorbilidad ?></td>
                <td field="" ><?php echo $IdTipoEvento ?></td>
                <td field="" ><?php echo $Descripciontipoevento ?></td>
         </tr>
  <?php	
		 }
	   }
	//}
 ?>
</table>

</body>
</html>
						
  