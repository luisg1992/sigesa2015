<?php 
$CodIPress=$_REQUEST["CodIpress"];
$CodUgiPress=$_REQUEST["CodUgiPres"];
include('/../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

		$objPHPExcel = new PHPExcel();

// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "NRO HISTORIA CLINICA");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "FECHA ATENCION");	 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "NOMBRES PACIENTE");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "APELLIDOS PACIENTE");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "EDAD");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "TIPO EDAD");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "SEXO");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "coddiag1 INGRESO 1");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "DESCRIPCION CIE10 1");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "coddiag2 INGRESO 2");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', "DESCRIPCION CIE10 2");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', "coddiag3 INGRESO 3");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', "DESCRIPCION CIE10 3");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', "coddiag4 INGRESO 4");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', "DESCRIPCION CIE10 4");
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1', "coddiag1 ALTA 1");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1', "DESCRIPCION CIE10 ALTA1");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1', "coddiag2 ALTA 2");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S1', "DESCRIPCION CIE10 ALTA2");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T1', "coddiag3 ALTA 3");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U1', "DESCRIPCION CIE10 ALTA3");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V1', "coddiag4 ALTA 4");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W1', "DESCRIPCION CIE10 ALTA4");			
				
			//ElimTemp_IngresosEgresosEmergenciaM();
			//RegTemp_IngresosEgresosEmergenciaM($Anio, $Mes);	
			$resultados = ListarIngresosEgresosEmergenciaMenores29DiasM($Anio, $Mes); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {	
				
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), $resultados[$i]["HISTORIACLINICA"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $resultados[$i]["FECHAING"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $resultados[$i]["NOMBRESPACIENTE"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $resultados[$i]["APELLIDOSPACIENTE"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $resultados[$i]["EDAD"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $resultados[$i]["DESCRIPCIONTIPOEDAD"]);				 													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $resultados[$i]["DESCRIPCIONSEXO"]);
				 						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $resultados[$i]["DX_ING1"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $resultados[$i]["DX_INGDES1"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $resultados[$i]["DX_ING2"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+2), $resultados[$i]["DX_INGDES2"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+2), $resultados[$i]["DX_ING3"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+2), $resultados[$i]["DX_INGDES3"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.($i+2), $resultados[$i]["DX_ING4"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.($i+2), $resultados[$i]["DX_INGDES4"]);
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.($i+2), $resultados[$i]["DX_ALTA1"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.($i+2), $resultados[$i]["DX_ALTADES1"]);   				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.($i+2), $resultados[$i]["DX_ALTA2"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.($i+2), $resultados[$i]["DX_ALTADES2"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.($i+2), $resultados[$i]["DX_ALTA3"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.($i+2), $resultados[$i]["DX_ALTADES3"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.($i+2), $resultados[$i]["DX_ALTA4"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.($i+2), $resultados[$i]["DX_ALTADES4"]);
		 }
	   }
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('Emergencia Menores'); 
		 //$objActSheet->getColumnDimension('I')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('I')->setWidth(30);
		 //$objActSheet->getColumnDimension('K')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('K')->setWidth(30);				 
		 //$objActSheet->getColumnDimension('M')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('M')->setWidth(30);				 
		 //$objActSheet->getColumnDimension('O')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('O')->setWidth(30);
		 
		 //$objActSheet->getColumnDimension('Q')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('Q')->setWidth(30); 				 
		 //$objActSheet->getColumnDimension('S')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('S')->setWidth(30);				 
		 //$objActSheet->getColumnDimension('U')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('U')->setWidth(30);	
		 //$objActSheet->getColumnDimension('W')->setAutoSize(true); 
		 $objActSheet->getColumnDimension('W')->setWidth(30);
		 
//Formato primera Fila 
    $objStyleA5 = $objActSheet ->getStyle('A1:W1');
    $objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontA5 = $objStyleA5->getFont(); 
    $objFontA5->setName('Courier New'); 
    $objFontA5->setSize(12); 
    $objFontA5->setBold(true); 
    //$objFontA5->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    //$objFontA5 ->getColor()->setARGB('FFFF0000') ;
    //$objFontA5 ->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE); 
	//Establecer la alineación 
    $objAlignA5 = $objStyleA5->getAlignment(); 
    $objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); 
    $objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);	   

//Formato General
//El ancho de columna
//$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);   
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="EmergenciaMenores29Dias.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>
			
  