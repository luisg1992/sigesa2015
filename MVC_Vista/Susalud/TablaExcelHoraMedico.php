<table width="1200" border="1">
 <tr>
    <th width="115">Departamento</th>
    <th width="115">Especialidad</th>
    <th width="120">Servicios</th>
    <th width="76">Fecha</th>
    <th width="190">Medico</th>
    <th width="75">DNI</th>
    <th width="105">Tipos Empleado</th>
    <th width="150">Turnos</th>
    <th width="100">TipoTurno</th>
    <th width="90">Total Horas Pro</th>
  </tr>
	<?php 
			
	$resultados = ListarProgramacionHoraMedico($fecha_inicio, $fecha_fin, $IdDepartamentoChk, $IdEspecialidadChk, $IdServicioChk);//todos
	if($resultados!=NULL){
		for ($i=0; $i < count($resultados); $i++){
			$Departamento = $resultados[$i]["Departamento"];
			$Especialidad = $resultados[$i]["Especialidad"];
			$Servicios = $resultados[$i]["Servicios"];
			$Fecha =  $resultados[$i]["Fecha"];	
			
			$Medico = $resultados[$i]["Medico"];
			$DNI = $resultados[$i]["DNI"];
			$TiposEmpleado = $resultados[$i]["TiposEmpleado"];
			$Turnos =  $resultados[$i]["Turnos"];	
			$TipoTurno = $resultados[$i]["TipoTurno"];
			$TotalHorasPro = $resultados[$i]["TotalHorasPro"];	 
	?>  

          <tr>
            <td><?php echo ltrim(utf8_decode(mb_strtoupper($Departamento))); ?></td>
            <td><?php echo ltrim(utf8_decode(mb_strtoupper($Especialidad))); ?></td>
            <td><?php echo ltrim(utf8_decode($Servicios)); ?></td>
            <td><?php echo $Fecha; ?></td>
            <td><?php echo ltrim(utf8_decode($Medico)); ?></td>
            <td><?php echo $DNI; ?></td>
            <td><?php echo ltrim(utf8_decode($TiposEmpleado)); ?></td>
            <td><?php echo ltrim(utf8_decode($Turnos)); ?></td>
            <td><?php echo ltrim(utf8_decode($TipoTurno)); ?></td>
            <td><?php echo $TotalHorasPro; ?></td>
          </tr>
  <?php	
		}
	}
 ?>
</table>

						
  