<?php 
include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();
// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "FechaIngresoHospital");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "FechaEgresoHospital");	 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "Cuenta Atencion");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "NroHistoriaClinica");

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Apellido Paterno");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Apellido Materno");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Primer Nombre");
	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "Secuencia");			
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "FechaOcupacion");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "HoraOcupacion");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', "FechaDesocupacion");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', "HoraDesocupacion");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', "Servicio");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', "CodigoCama");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', "DiasEstancia");				
				
			$resultados = ListarIngresosEgresosHospitalizacionFindeMesM($fecha_fin); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {	
				 
				 $FechaIngresoHospital=vfecha(substr($resultados[$i]["FechaIngresoHospital"],0,10));
					
				 if($resultados[$i]["FechaEgresoHospital"]!=NULL){	
				 	$FechaEgresoHospital=vfecha(substr($resultados[$i]["FechaEgresoHospital"],0,10));
				 }else{
					$FechaEgresoHospital="";
				 }
					
				 $FechaOcupacion=vfecha(substr($resultados[$i]["FechaOcupacion"],0,10));	
				
				 if($resultados[$i]["FechaDesocupacion"]!=NULL){	
				 	$FechaDesocupacion=vfecha(substr($resultados[$i]["FechaDesocupacion"],0,10));
				 }else{
					$FechaDesocupacion="";
				 }	
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), $FechaIngresoHospital);										
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $FechaEgresoHospital);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $resultados[$i]["IdCuentaAtencion"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $resultados[$i]["NroHistoriaClinica"]);	
					
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $resultados[$i]["ApellidoPaterno"]);	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $resultados[$i]["ApellidoMaterno"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $resultados[$i]["PrimerNombre"]);										
					 						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $resultados[$i]["Secuencia"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $FechaOcupacion);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $resultados[$i]["HoraOcupacion"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+2), $FechaDesocupacion);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+2), $resultados[$i]["HoraDesocupacion"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+2), $resultados[$i]["Servicio"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.($i+2), $resultados[$i]["CodigoCama"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.($i+2), $resultados[$i]["DiasEstancia"]);
				 			 

		 }
	   }
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('FinDeMes'); 		
		 $objActSheet->getColumnDimension('M')->setWidth(40);	
		 /*$objActSheet->getColumnDimension('J')->setWidth(30);		
		 $objActSheet->getColumnDimension('K')->setWidth(30);		
		
		 $objActSheet->getColumnDimension('S')->setWidth(30);
		 $objActSheet->getColumnDimension('T')->setWidth(30);  
		 $objActSheet->getColumnDimension('U')->setWidth(30); 
		 $objActSheet->getColumnDimension('V')->setWidth(30);*/
	
	
//Formato General
//El ancho de columna
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
		 
    //Formato primera Fila 
    $objStyleA5 = $objActSheet ->getStyle('A1:Z1');
    //$objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontA5 = $objStyleA5->getFont(); 
    $objFontA5->setName('Arial'); 
    $objFontA5->setSize(11); 
    $objFontA5->setBold(true); 
    //$objFontA5->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    //$objFontA5 ->getColor()->setARGB('FFFF0000') ;
    //$objFontA5 ->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE); 
	//Establecer la alineación 
    $objAlignA5 = $objStyleA5->getAlignment(); 
    $objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);	 
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="HospitalizadosFinDeMes.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>
			
  