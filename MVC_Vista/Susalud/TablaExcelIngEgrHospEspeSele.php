<table width="200" border="1">
 <tr>
    <th width="50" rowspan="2" field="Dia">Dia</th>
    <th width="100" rowspan="2" field="Saldoant">Saldo Anterior</th>
    <th colspan="3" field="Ingreso">INGRESOS</th>
    <th colspan="4" field="Alta">EGRESOS</th>
    <th width="100" rowspan="2" field="SaldoDiaSgte">Saldo dia Sgte</th>
    <th width="100" rowspan="2" field="Camas">Total Camas</th>
  </tr>
  <tr>
    <th field="Ingreso" width="100">Ingresos</th>
    <th field="TotalTransfIng" width="100">Transf.</th>               
    <th field="TotalIng" width="110">Total</th>  
    
    <th field="Alta" width="100">Altas</th>
    <th field="TransfEgr" width="100">Transf.</th>  
    <th field="Defuncion" width="100">Defuncion</th>              
    <th field="TotalEgr" width="100">Total</th>               
  </tr>
	<?php 
		
	//SALDO MES ANTERIOR
	/*$PrimerDiaMes=$Anio.'-'.$Mes.'-01';	//primer dia del mes
	$FechaAntMes = strtotime ( '-1 day' , strtotime ( $PrimerDiaMes ) ) ; //dia anterior
	$FechaAntMes = date ( 'Y-m-j' , $FechaAntMes );
	
	$resulSaldoAnt = ObtenerCamasOcupadasDisponiblesAntM($FechaAntMes, $IdServicio);					 
	 if($resulSaldoAnt!=NULL){
		for ($s=0; $s < count($resulSaldoAnt); $s++) {												
			 $OcupadasAnt = $resulSaldoAnt[$s]["Ocupadas"];
		}
	 }else{
			 $OcupadasAnt = 0;
		}*/
	//FIN SALDO MES ANTERIOR
	
	$numero = cal_days_in_month(CAL_GREGORIAN, $Mes, $Anio); // 31					
	$resultados = ListarDiasHospitalizadosOcu_EspeM($Anio, $Mes, $CodIpress, $CodUgiPres, $IdEspecialidad);	
	/*if($resultados==NULL){
		$resultados = ListarDiasHospitalizadosDesocuM($Anio, $Mes, $CodIpress, $CodUgiPres, $IdServicio);
	}*/
					 
	if($resultados!=NULL){			
		for ($i=0; $i < count($resultados); $i++) {
		  $SaldoAnt=$OcupadasAnt;
		  for($Dia=1;$Dia<=$numero;$Dia++){
			$Camas = $resultados[$i]["Totcamas"]; //TOTAL DEL CAMAS DEL SERVICIO  		  
		  	$Fecha=$Anio.'-'.$Mes.'-'.$Dia;	
			
			/*//SALDO ANTERIOR
			$FechaAnt = strtotime ( '-1 day' , strtotime ( $Fecha ) ) ;
			$FechaAnt = date ( 'Y-m-j' , $FechaAnt );			 
			
			//SALDO ANTERIOR INGRESO
			$resulIngresosAnt = ListarIngresosHospitalizados_EspeM($FechaAnt, $IdServicio);					 
			 if($resulIngresosAnt!=NULL){
				$TotalIngAnt=0; 
				for ($j=0; $j < count($resulIngresosAnt); $j++) {												
					$Ingresos = $resulIngresosAnt[$j]["Ingresos"];
					$TotalIngAnt = $TotalIngAnt+$Ingresos;
				}
			  }else{
				$TotalIngAnt=0; 
			  }
			  
			  //SALDO ANTERIOR EGRESO
			$resulIngresosAnt = ListarEgresosHospitalizados_EspeM($FechaAnt, $IdServicio);					 
			 if($resulEgresosAnt!=NULL){
				$TotalEgrAnt=0; 
				for ($j=0; $j < count($resulEgresosAnt); $j++) {												
					$Egresos = $resulEgresosAnt[$j]["Egresos"];
					$TotalEgrAnt = $TotalEgrAnt+$Ingresos;
				}
			  }else{
				$TotalEgrAnt=0; 
			  }			  
			 $SaldoAnt=$OcupadasAnt+$TotalIngAnt-$TotalEgrAnt;*/
			 
			 $SaldoAnt=$SaldoAnt+$TotalIng-$TotalEgr;
			
			//INGRESO ACTUAL				
			$Fecha=$Anio.'-'.$Mes.'-'.$Dia;
			$resulIngresos = ListarIngresosHospitalizados_EspeM($Fecha, $IdEspecialidad);					 
			 if($resulIngresos!=NULL){
				$TotalIng=0; $TotalTransfIng=0; $Ingreso=0;
				for ($j=0; $j < count($resulIngresos); $j++) {												
					$TransfIng = $resulIngresos[$j]["TransfIng"];
					$TotalTransfIng = $TotalTransfIng+$TransfIng;//transferencia							
						
					$Ingresos = $resulIngresos[$j]["Ingresos"];	
					$TotalIng = $TotalIng+$Ingresos; //totalingreso (suma de los ingresos)
										
					$Ingreso = $TotalIng-$TotalTransfIng; //ingresos menos la transferencia
				}
			  }else{
				$TotalIng="<font color='#FF0000'>0</font>"; $TotalTransfIng="0"; $Ingreso="0";
			  }	
			  
			//EGRESO ACTUAL				
			$Fecha=$Anio.'-'.$Mes.'-'.$Dia;
			$resulEgresos = ListarEgresosHospitalizados_EspeM($Fecha, $IdEspecialidad);					 
			 if($resulEgresos!=NULL){
				$TotalEgr=0; $TotalTransfEgr=0; $Egreso=0; $Defuncion=0;
				for ($j=0; $j < count($resulEgresos); $j++) {												
					$TransfEgr = $resulEgresos[$j]["TransfEgr"];
					$TotalTransfEgr = $TotalTransfEgr+$TransfEgr;//transferencia
					
					$Fallecidos = $resulEgresos[$j]["Fallecidos"];
					$Defuncion = $Defuncion+$Fallecidos;							
						
					/*$Egresos = $resulEgresos[$j]["Egresos"];//Alta					
					$TotalEgr = $TotalEgr+$Egresos; //totalingreso (suma de los egresos)					
					$Egreso = $TotalEgr-$TotalTransfEgr-$Defuncion; //egresos menos la transferencia*/
					
					$Alta = $resulEgresos[$j]["Alta"];//Alta					
					$Egreso = $Egreso+$Alta; 				
					$TotalEgr = $TotalTransfEgr+$Defuncion+$Egreso;//totalingreso (suma de los egresos)				 
				}
			  }else{
				$TotalEgr="<font color='#FF0000'>0</font>"; $TotalTransfEgr="0"; $Egreso="0"; $Defuncion="0";
			  }	
			  
			  //$SaldoDiaSgte = $SaldoAnt+$TotalIng-$TotalEgr;
			  		 
	?>  

          <tr>
            <td><?php echo $Dia; ?></td>
            <td bgcolor="#FFFF99"><?php //echo $SaldoAnt; ?></td>
            <td><?php echo $Ingreso; ?></td>
            <td><?php echo $TotalTransfIng; ?></td>
            <td bgcolor="#CEFFFF"><?php echo $TotalIng; ?></td>
            <td><?php echo $Egreso; ?></td>
            <td><?php echo $TotalTransfEgr; ?></td>
            <td><?php echo $Defuncion; ?></td>
            <td bgcolor="#CEFFFF"><?php echo $TotalEgr; ?></td>
            <td bgcolor="#FFFF99"><?php //echo $SaldoDiaSgte; ?></td>
            <td><?php echo $Camas; ?></td>
          </tr>
  <?php	
		 }
	   }
	}
 ?>
</table>

						
  