<?php

include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');
$objPHPExcel = new PHPExcel();

// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
	$resultadosEspe=ListarServiciosHospitalizacionM();	
	   							 
		 if($resultadosEspe!=NULL){	
			 $hoja=0;	
			 for ($ies=0; $ies < count($resultadosEspe); $ies++) {
				 //echo $resultadosEspe[$ies]["Nombre"];
	
	if($hoja>0){
	$nueva_hoja = $objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex($hoja); // marcar como activa la nueva hoja
	}
	//PRIMERA HOJA
		
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('A1', "Dia");
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('B1', "Saldo Anterior");	 
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('C1', "Ingresos");	
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('D1', "Transf. Ing.");	
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('E1', "Total Ing.");	
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F1', "Altas Egr.");	
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('G1', "Transf. Egr.");	
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('H1', "Defuncion Egr.");	
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('I1', "Total Egr.");
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('J1', "Saldo dia Sgte");	
	$objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('K1', "Total Camas");			
				
	$numero = cal_days_in_month(CAL_GREGORIAN, $Mes, $Anio); // 31					
	$resultados = ListarDiasHospitalizadosOcuM($Anio, $Mes, $CodIpress, $CodUgiPres, $resultadosEspe[$ies]["IdServicio"]);	
	if($resultados==NULL){
		$resultados = ListarDiasHospitalizadosDesocuM($Anio, $Mes, $CodIpress, $CodUgiPres, $resultadosEspe[$ies]["IdServicio"]);
	}
					 
	if($resultados!=NULL){			
		for ($i=0; $i < count($resultados); $i++) {
		 
		  for($Dia=1;$Dia<=$numero;$Dia++){
			$Camas = $resultados[$i]["Totcamas"]; //TOTAL DEL CAMAS DEL SERVICIO  		  
		  	$Fecha=$Anio.'-'.$Mes.'-'.$Dia;				
			
			//INGRESO ACTUAL				
			$Fecha=$Anio.'-'.$Mes.'-'.$Dia;
			$resulIngresos = ListarIngresosHospitalizadosM($Fecha, $resultadosEspe[$ies]["IdServicio"]);					 
			 if($resulIngresos!=NULL){
				$TotalIng=0; $TotalTransfIng=0; $Ingreso=0;
				for ($j=0; $j < count($resulIngresos); $j++) {												
					$TransfIng = $resulIngresos[$j]["TransfIng"];
					$TotalTransfIng = $TotalTransfIng+$TransfIng;//transferencia							
						
					$Ingresos = $resulIngresos[$j]["Ingresos"];	
					$TotalIng = $TotalIng+$Ingresos; //totalingreso (suma de los ingresos)
										
					$Ingreso = $TotalIng-$TotalTransfIng; //ingresos menos la transferencia
				}
			  }else{
				$TotalIng="0"; $TotalTransfIng="0"; $Ingreso="0";
			  }	
			  
			//EGRESO ACTUAL				
			$Fecha=$Anio.'-'.$Mes.'-'.$Dia;	
					$TotalTransfEgr=0;
					$Egreso=0;//$Alta=0;
					$Defuncion=0;
					$TotalEgr=0;
					
					$ListarAtencionesHospServicioDesocupacion=ListarAtencionesHospServicioDesocupacionM($Fecha,$resultadosEspe[$ies]["IdServicio"]);
					if($ListarAtencionesHospServicioDesocupacion!=NULL){
					foreach($ListarAtencionesHospServicioDesocupacion as $AtencionesHospServicioD){//Atenciones inner join AtencionesEstanciaHospitalaria where FechaDesocupacion and IdServicio
						$IdAtencion=$AtencionesHospServicioD['IdAtencion'];
						$SecuenciaDesoc=$AtencionesHospServicioD['Secuencia'];//secuencia de la Fecha Desocupacion de la Atencion	
						
						$IdCondicionAlta=$AtencionesHospServicioD['IdCondicionAlta'];
						$FechaDesocupacion=$AtencionesHospServicioD['FechaDesocupacion'];
						$FechaEgreso=$AtencionesHospServicioD['FechaEgreso'];
						
						$MaximoSecuencia=ListarAtencionesHospMaximoSecuenciaM($IdAtencion);
						$MaxSecuencia=$MaximoSecuencia[0]['maxSecuencia'];
						
						if($MaxSecuencia>1){ // si tiene mas de una secuencia															
							if($SecuenciaDesoc<$MaxSecuencia){
								$TotalTransfEgr++;
							}else{
								if($IdCondicionAlta==4){
									$Defuncion++; 
								}else{
									$Egreso++; 
								}
							}										
								
						}//si solo tiene una secuencia colocar otra condicion
						else{
							if($IdCondicionAlta==4){
								$Defuncion++; 
							}else{
								$Egreso++; 
							}													
						}	
						$TotalEgr = $TotalTransfEgr+$Defuncion+$Egreso;			
					}
					}			
			
			/*$resulEgresos = ListarEgresosHospitalizadosM($Fecha, $resultadosEspe[$ies]["IdServicio"]);					 
			 if($resulEgresos!=NULL){
				$TotalEgr=0; $TotalTransfEgr=0; $Egreso=0; $Defuncion=0;
				for ($j=0; $j < count($resulEgresos); $j++) {												
					$TransfEgr = $resulEgresos[$j]["TransfEgr"];
					$TotalTransfEgr = $TotalTransfEgr+$TransfEgr;//transferencia
					
					$Fallecidos = $resulEgresos[$j]["Fallecidos"];
					$Defuncion = $Defuncion+$Fallecidos;							
						
					
					$Alta = $resulEgresos[$j]["Alta"];//Alta					
					$Egreso = $Egreso+$Alta; 				
					$TotalEgr = $TotalTransfEgr+$Defuncion+$Egreso;//totalingreso (suma de los egresos)				 
				}
			  }else{
				$TotalEgr="0"; $TotalTransfEgr="0"; $Egreso="0"; $Defuncion="0";
			  }*/	
				
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('A'.($Dia+1), $Dia);													
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('B'.($Dia+1), '');						
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('C'.($Dia+1), $Ingreso);
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('D'.($Dia+1), $TotalTransfIng);
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('E'.($Dia+1), $TotalIng);				 
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('F'.($Dia+1), $Egreso);				 													
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('G'.($Dia+1), $TotalTransfEgr);
				 						
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('H'.($Dia+1), $Defuncion);
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('I'.($Dia+1), $TotalEgr);				 
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('J'.($Dia+1), '');				 
				 $objPHPExcel->setActiveSheetIndex($hoja)->setCellValue('K'.($Dia+1), $Camas);
		 }
	    }	   
	} 
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex($hoja); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades
		 //$NombreServicio='CENTRO DE EXCELENCIA';//Maximum 31 characters allowed in sheet title
		 $NombreServicio=trim(substr($resultadosEspe[$ies]["Nombre"],0,30)); 
		 $objActSheet->setTitle($NombreServicio); 
		 		  
		 //$objActSheet->getColumnDimension('C')->setWidth(20);
		 
	//Formato primera Fila 
    $objStyleA5 = $objActSheet ->getStyle('A1:K1');
    $objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontA5 = $objStyleA5->getFont(); 
    $objFontA5->setName('Arial'); 
    $objFontA5->setSize(12); 
    $objFontA5->setBold(true);
	//Establecer la alineación 
    $objAlignA5 = $objStyleA5->getAlignment(); 
    $objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//HORIZONTAL_RIGHT
    $objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	
	$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20); 
	
//FIN PRIMERA HOJA	-------------------------------------------------------------------------------------------
	
	$hoja++;
}}	
	 
/*   */

//Formato General	
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
 
header('Content-Type: application/vnd.ms-excel');
$NombreExcel='IngresosEgresosHospitalizacion'.$Mes.'-'.$Anio;
header('Content-Disposition: attachment;filename='.$NombreExcel.'.xlsx');
//header('Content-Disposition: attachment;filename="IngresosEgresosHospitalizacion.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>



						
  