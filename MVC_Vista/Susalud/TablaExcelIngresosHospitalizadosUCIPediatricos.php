<?php 
include('../../MVC_Complemento/PHPExcel/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();
// Seleccionando la fuente a utilizar
/*$objPHPExcel->getDefaultStyle()->getFont()->setName("Arial");
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);*/
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Nro");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Nro HistoriaClinica");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "Nro Documento");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "Nro Cuenta");
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Apellidos Paciente");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Nombres Paciente");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Sexo");		
	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "Edad");		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "Tipo Edad");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "Fecha Nacimiento");			
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', "Codigo Distrito Procedencia");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', "Codigo Distrito Domicilio");
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', "Seguro");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', "Condicion Salida");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', "Origen Atencion");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1', "Fecha Ingreso");	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1', "Hora Ingreso");

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1', "Servicio Ingreso");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S1', "Fecha Egreso");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T1', "Servicio Egreso");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U1', "SERVICIO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V1', "Fecha Ocupacion");
				
			$resultados = ListarIngresosHospitalizadosUCIPediatricosM($Anio, $Mes); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {	
				 
				 $FechaNacimiento=vfecha(substr($resultados[$i]["FechaNacimiento"],0,10));
				 $FechaIngreso=vfecha(substr($resultados[$i]["FechaIngreso"],0,10));
				 
				 $FechaEgreso= isset($resultados[$i]["FechaEgreso"]) ? vfecha(substr($resultados[$i]["FechaEgreso"],0,10)) : '';				 
				 $FechaOcupacion=vfecha(substr($resultados[$i]["FechaOcupacion"],0,10));
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), ($i+1));	
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), trim($resultados[$i]["NroHistoriaClinica"]));													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $resultados[$i]["NroDocumento"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $resultados[$i]["IdCuentaAtencion"]);

				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $resultados[$i]["APELLIDOSPACIENTE"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $resultados[$i]["NOMBRESPACIENTE"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $resultados[$i]["Sexo"]);	
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $resultados[$i]["EDAD"]);				 													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $resultados[$i]["TIPOEDAD"]);				 						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $FechaNacimiento);
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+2), $resultados[$i]["IDDISTRITOPROCEDENCIA"]);				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.($i+2), $resultados[$i]["IdDistritoDomicilio"]);
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.($i+2), $resultados[$i]["SEGURO"]);													
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.($i+2), $resultados[$i]["CONDICIONSALIDA"]);						
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.($i+2), $resultados[$i]["DescripcionOrigenAtencion"]);
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.($i+2), $FechaIngreso);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.($i+2), $resultados[$i]["HoraIngreso"]);
				 
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.($i+2), $resultados[$i]["SERVICIOINGRESO"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.($i+2), $FechaEgreso);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.($i+2), $resultados[$i]["SERVICIOEGRESO"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.($i+2), $resultados[$i]["SERVICIO"]);
				 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.($i+2), $FechaOcupacion);
				 			 

		 }
	   }
	   
	    //Establecer la anchura 				  
		//De forma predeterminada, PHPExcel crea automáticamente la primera hoja está SheetIndex = 0 
		 $objPHPExcel->setActiveSheetIndex(0); 
		 $objActSheet = $objPHPExcel->getActiveSheet(); 
	
		 //El nombre de la hoja actual de las actividades 
		 $objActSheet->setTitle('HospitalizadosUCIPediatricos');

		 $objActSheet->getColumnDimension('A')->setWidth(10);
		 $objActSheet->getColumnDimension('E')->setWidth(30);	
		
		 $objActSheet->getColumnDimension('R')->setWidth(35);
		 $objActSheet->getColumnDimension('T')->setWidth(35);  
		 $objActSheet->getColumnDimension('U')->setWidth(35); 
	
	
//Formato General
//El ancho de columna
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
//El ancho de la línea
//$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);	   
//Worksheet estilo predeterminado 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment(); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setBold(true);
		 
    //Formato primera Fila 
    $objStyleA5 = $objActSheet ->getStyle('A1:Z1');
    //$objStyleA5 ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER); 
	//Configuración de tipos de letra 
    $objFontA5 = $objStyleA5->getFont(); 
    $objFontA5->setName('Arial'); 
    $objFontA5->setSize(11); 
    $objFontA5->setBold(true); 
    //$objFontA5->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
    //$objFontA5 ->getColor()->setARGB('FFFF0000') ;
    //$objFontA5 ->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE); 
	//Establecer la alineación 
    $objAlignA5 = $objStyleA5->getAlignment(); 
    $objAlignA5->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $objAlignA5->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);	 
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="IngresosHospitalizadosUCIPediatricos.xlsx"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
	
 ?>
			
  