<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
     	
        <script>
			$.extend( $( "#fecha_inicio" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#fecha_inicio" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							$("#fecha_inicio" ).datebox('setValue', '');
							//$("#EdadPaciente").textbox('setValue','');
							//$("#IdTipoEdad").combobox('setValue','0');
							return false;
						}
					},  
					message: 'Porfavor Selecione una fecha valida.'  
				}
		    });
			
			$.extend($( "#fecha_fin" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							$("#fecha_fin" ).datebox('setValue', '');
							//$("#EdadPaciente").textbox('setValue','');
							//$("#IdTipoEdad").combobox('setValue','0');
							return false;
						}
					},  
					message: 'Porfavor Selecione una fecha valida.'  
				}
		    });
		
			function validarDatos(){
								
				var IdServicio=$('#IdServicio').combobox('getValue');
				if(IdServicio==""){
					$.messager.alert('Mensaje','Seleccione un Servicio','info');
					$('#IdServicio').next().find('input').focus();
					return 0;			
				}
			}
							
			function vistaprevia(){	
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='vistaprevia';	
					document.formElem.submit();
				}				
			}
			
			/*function exportartxt(){		
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportartxt';
					document.formElem.submit();	
				}
				
			}*/  				
					
			function exportarexcel(){	
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportarexcel';
					document.formElem.submit();	
				}
			}
				
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:80%;height:auto;padding:10px;"
        title="CONSUMO DE SERVICIOS DEL <?php echo $_REQUEST['Mes'].'/'.$_REQUEST['Anio'] ?>" iconCls="icon-ok" collapsible="true" align="center">
        
        <form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem">
        	<table width="709" border="0">             	                           
              <tr>
                <td>Fecha Inicio</td>
                <td width="217"><input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
                <input type="text" id="fecha_inicio" name="fecha_inicio" class="easyui-datebox" data-options="prompt:'Fecha Inicio'" validType="validDate" value="<?php echo '01/'.$Mes.'/'.$Anio; ?>" style="width:150px;" /> </td>
                <td width="106">Fecha Fin</td>
                <td width="284"><input type="text" id="fecha_fin" name="fecha_fin" class="easyui-datebox" data-options="prompt:'Fecha Fin'" validType="validDate" value="<?php echo getUltimoDiaMes($Anio,$Mes).'/'.$Mes.'/'.$Anio; ?>" style="width:150px;" /> </td>
              </tr>
              
              <tr>
                <td>Servicio</td>
                <td>
                    <select class="easyui-combobox" id="IdServicio" name="IdServicio"  style="width:200px;">
                    <option value="0">SELECCIONE</option>
                   		 <?php 
							$resultados=ListarServiciosM();								 
							if($resultados!=NULL){
							for ($i=0; $i < count($resultados); $i++) {	
                         ?>
                         <option value="<?php echo $resultados[$i]["IdServicio"] ?>"  ><?php echo $resultados[$i]["IdServicio"].' | '.mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
                         <?php 
							}}
                         ?>                   
                    </select>
                 </td>
                <td>Centro de Costo</td>
                <td>
                	<select class="easyui-combobox" id="IdCentroCosto" name="IdCentroCosto"  style="width:200px;">
                    <option value="0">SELECCIONE</option>
                   		 <?php 
							$resultados=ListarCentroCostosM();								 
							if($resultados!=NULL){
							for ($i=0; $i < count($resultados); $i++) {	
                         ?>
                         <option value="<?php echo $resultados[$i]["IdCentroCosto"] ?>"  ><?php echo $resultados[$i]["IdCentroCosto"].' | '.mb_strtoupper($resultados[$i]["Descripcion"]) ?></option>
                         <?php 
							}}
                         ?>                   
                    </select>
                </td>
              </tr>
              <tr>
                <td width="84">Codigo CPT</td>
                <td colspan="3">
                	<select class="easyui-combobox" id="codigoCPT" name="codigoCPT"  style="width:530px;">
                    <option value="0">SELECCIONE</option>
                   		 <?php 
							$resultados=ListarCatalogoServiciosM();								 
							if($resultados!=NULL){
							for ($i=0; $i < count($resultados); $i++) {	
                         ?>
                         <option value="<?php echo $resultados[$i]["Codigo"] ?>"  ><?php echo $resultados[$i]["Codigo"].' | '.mb_strtoupper($resultados[$i]["Nombre"]) ?></option>
                         <?php 
							}}
                         ?>                   
                    </select>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input name="tipolink" id="tipolink" type="hidden" value=""> </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
                              
                 <div class="buttons" align="center">
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-print'"  style="width:20%"  onclick="vistaprevia();">Vista Previa</button>-->
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>-->
                      <button type="button" name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" style="width:20%" onclick="exportarexcel()" value="Exportar Excel" >Exportar Excel</button>                      
                      <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:20%">Cancelar</a>
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:20%" data-options="iconCls:'icon-reload'">Refrescar</a> 
                  </div>
              </form>
		</div>	
		 </div>	
      
    </body>
</html>
