<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
     	
        <script>
			function validarDatos(){
								
				var CodIpress=$('#CodIpress').textbox('getValue');
				if(CodIpress==""){
					$.messager.alert('Mensaje','Ingrese Código de IPRESS','info');
					$('#CodIpress').next().find('input').focus();
					return 0;			
				}
				
				var CodUgiPres=$('#CodUgiPres').textbox('getValue');
				if(CodUgiPres==""){
					$.messager.alert('Mensaje','Ingrese Código de UGIPRESS','info');
					$('#CodUgiPres').next().find('input').focus();
					return 0;			
				}				
				
			}
							
			function vistaprevia(){	
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='vistaprevia';	
					document.formElem.submit();
				}				
			}
			
			function exportartxt(){		
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportartxt';
					document.formElem.submit();	
				}
				
			}  				
					
			$(function() {
				$("#CodIpress").textbox('textbox').attr('maxlength',8);
				$("#CodUgiPres").textbox('textbox').attr('maxlength',8);
  
			})//function
				
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:80%;height:auto;padding:10px;"
        title="Reporte de Consolidado de Morbilidad en Hospitalización del <?php echo $_REQUEST['Mes'].'/'.$_REQUEST['Anio'] ?>" iconCls="icon-ok" collapsible="true" align="center">
        
        <form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem">
        	<table width="600" border="0">             	                           
              <tr>
                <td width="192"><label>Año:</label></td>
                <td width="100">
                	<input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
               		<input value="<?php echo $_REQUEST['Anio'] ?>" class="easyui-textbox" name="Anio" id="Anio" style="width:100px;height:32px" readonly>
                </td>
                <td width="180"><label>Mes:</label></td>               
                <td width="100">
                	<input name="Mes" id="Mes" type="hidden" value="<?php echo $_REQUEST['Mes'] ?>">
                	<input value="<?php echo mb_strtoupper(nombremes($_REQUEST['Mes'])); ?>" class="easyui-textbox" name="nombremes" id="nombremes" style="width:100px;height:32px" readonly>
                </td>
              </tr>
              <tr>
                <td>Código de IPRESS</td>
                <td><input class="easyui-textbox" name="CodIpress" id="CodIpress" style="width:100px;height:32px" value="00000001" maxlength="8"></td>
                <td>Código de UGIPRESS</td>
                <td><input class="easyui-textbox" name="CodUgiPres" id="CodUgiPres" style="width:100px;height:32px" value="00000002" maxlength="8"></td>
              </tr>
              <tr>
                <td width="192">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="180">&nbsp;</td>
                <td width="100">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input name="tipolink" id="tipolink" type="hidden" value=""> </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
                              
                 <div class="buttons" align="center">
                   <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-print'"  style="width:20%"  onclick="vistaprevia();">Vista Previa</button>
                   <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>
                      <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:20%">Cancelar</a>
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:20%" data-options="iconCls:'icon-reload'">Refrescar</a> 
                  </div>
              </form>
		</div>	
		 </div>	
      
    </body>
</html>
