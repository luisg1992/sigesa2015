<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
     	
        <script>
			function validarDatos(){
								
				/*var IdServicio=$('#IdServicio').combobox('getValue');
				if(IdServicio==""){
					$.messager.alert('Mensaje','Seleccione un Servicio','info');
					$('#IdServicio').next().find('input').focus();
					return 0;			
				}*/
			}
							
			function vistaprevia(){	
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='vistaprevia';	
					document.formElem.submit();
				}				
			}
			
			/*function exportartxt(){		
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportartxt';
					document.formElem.submit();	
				}
				
			}*/  				
					
			function exportarexcel(){	
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportarexcel';
					document.formElem.submit();	
				}
			}
			
			function exportarDBF(){
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportarDBF';
					document.formElem.submit();	
				}
				
			}
				
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:80%;height:auto;padding:10px;"
        title="Ingresos y Egresos Hospitalización con Diagnosticos de <?php echo $_REQUEST['Mes'].'/'.$_REQUEST['Anio'] ?>" iconCls="icon-ok" collapsible="true" align="center">
        
        <?php 
		//COPIAR DATOS TABLA TEMPORAL		
		RegTemp_IngresosEgresosHospitalizacionM($_REQUEST['Anio'], $_REQUEST['Mes']);	
		?>
        
        <form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem">
        	<table width="600" border="0">             	                           
              <tr>
                <td width="100"><label>Año:</label></td>
                <td width="196">
                	<input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
               		<input value="<?php echo $_REQUEST['Anio'] ?>" class="easyui-textbox" name="Anio" id="Anio" style="width:100px;height:32px" readonly>
                </td>
                <td width="100"><label>Mes:</label></td>               
                <td width="186">
                	<input name="Mes" id="Mes" type="hidden" value="<?php echo $_REQUEST['Mes'] ?>">
                	<input value="<?php echo mb_strtoupper(nombremes($_REQUEST['Mes'])); ?>" class="easyui-textbox" name="nombremes" id="nombremes" style="width:100px;height:32px" readonly>
                </td>
              </tr>
              
              <tr>
                <td>Código de IPRESS</td>
                <td><input class="easyui-textbox" name="CodIpress" id="CodIpress" style="width:100px;height:32px" value="00000001" maxlength="8"></td>
                <td>Código de UGIPRESS</td>
                <td><input class="easyui-textbox" name="CodUgiPres" id="CodUgiPres" style="width:100px;height:32px" value="00000002" maxlength="8"></td>
              </tr>
                            
              <tr>
                <td colspan="4">
                	<input name="tipolink" id="tipolink" type="hidden" value="">
                	<?php 
						$ListarParametro1=ListarParametrosHospitalM('UBIGEO_HOSP');
						$UBIGEO_HOSP=$ListarParametro1[0]["ValorTexto"];
						$E_UBIG=str_pad((string)$UBIGEO_HOSP, 6, "0", STR_PAD_LEFT); 
						
						$ListarParametro2=ListarParametrosHospitalM('CODIGO_DISA');
						//$CODIGO_DISA=$ListarParametro2[0]["ValorTexto"];
						$CODIGO_DISA='08';
						
						$ListarParametro3=ListarParametrosHospitalM('CODIGO_RED');
						$CODIGO_RED=$ListarParametro3[0]["ValorTexto"];
						
						$ListarParametro4=ListarParametrosHospitalM('CODIGO_MRED');
						$CODIGO_MRED=$ListarParametro4[0]["ValorTexto"];
							
					?>
                </td>
              </tr>
              <tr>
                <td>Código RENIPRESS</td>
                <td><input class="easyui-textbox" name="RENIPRESS" id="RENIPRESS" style="width:100px;height:32px" value="0000006218" maxlength="10"></td>
                <td>Ubigeo Establec.</td>
                <td><input class="easyui-textbox" name="E_UBIG" id="E_UBIG" style="width:100px;height:32px" value="<?php echo $E_UBIG; ?>" ></td>
              </tr>
              <tr>
                <td>Código de la DISA</td>
                <td><input class="easyui-textbox" name="COD_DISA" id="COD_DISA" style="width:100px;height:32px" value="<?php echo $CODIGO_DISA; ?>" ></td>
                <td>Código de la RED</td>
                <td><input class="easyui-textbox" name="COD_RED" id="COD_RED" style="width:100px;height:32px" value="<?php echo $CODIGO_RED; ?>" ></td>
              </tr>
              <tr>
                <td>Código de la MicroRED</td>
                <td><input class="easyui-textbox" name="COD_MRED" id="COD_MRED" style="width:100px;height:32px" value="<?php echo $CODIGO_MRED; ?>" ></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
                              
                 <div class="buttons" align="center">
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-print'"  style="width:20%"  onclick="vistaprevia();">Vista Previa</button>-->
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>-->
                  	  <button type="button" name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" style="width:20%" onclick="exportarexcel()" value="Exportar Excel" >Exportar Excel</button>   
                      <button type="button"  name="save" class="easyui-linkbutton" style="width:20%"  onclick="exportarDBF();">Exportar DBF SEM</button>    
                  </div> <br>
                   
                   <div class="buttons" align="center">                                     
                      <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:15%">Cancelar</a>
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:15%" data-options="iconCls:'icon-reload'">Refrescar</a> 
                  </div>
              </form>
		</div>	
		 </div>	
      
    </body>
</html>
