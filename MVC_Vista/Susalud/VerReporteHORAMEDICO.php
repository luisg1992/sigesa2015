<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
     	
        <script>		
					 
			function llenarIdDepartamentoChk(){
				var validarcheckbox = document.getElementsByClassName('classDep');
				var IdDepartamentoChk=''; var ischecked_chk = false; 
				//alert(validarcheckbox.length);
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxD' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxD' + i).value;
							IdDepartamentoChk=IdDepartamentoChk+chkreg+',';
							document.getElementById('IdDepartamentoChk').value=IdDepartamentoChk+'0';					
							ischecked_chk = true;
						}
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdDepartamentoChk').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }				
			}
			
			function llenarIdEspecialidadChk(){
				var validarcheckbox = document.getElementsByClassName('classEsp');
				var IdEspecialidadChk=''; var ischecked_chk = false; 
				//alert(validarcheckbox.length);
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxE' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxE' + i).value;
							IdEspecialidadChk=IdEspecialidadChk+chkreg+',';
							document.getElementById('IdEspecialidadChk').value=IdEspecialidadChk+'0';					
							ischecked_chk = true;
						}
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdEspecialidadChk').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }				
			}
			
			function llenarIdServicioChk(){
				var validarcheckbox = document.getElementsByClassName('classSer');
				var IdServicioChk=''; var ischecked_chk = false; 
				//alert(validarcheckbox.length);
				for ( var i = 1; i <= validarcheckbox.length; i++) {						
						if (document.getElementById('checkboxS' + i).checked == true) {	
							var chkreg = document.getElementById('checkboxS' + i).value;
							IdServicioChk=IdServicioChk+chkreg+',';
							document.getElementById('IdServicioChk').value=IdServicioChk+'0';					
							ischecked_chk = true;
						}
				}//end for
				
				 if(!ischecked_chk && validarcheckbox.length>0)   { //payment method button is not checked
				 		document.getElementById('IdServicioChk').value='';
						//alert("Porfavor Marcar por lo menos una opcion de cada Pregunta");
				 }	
				
			}
			
			$.extend( $( "#fecha_inicio" ).datebox.defaults,{
				formatter:function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					var d = date.getDate();
					return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
				},
				parser:function(s){
					if (!s) return new Date();
					var ss = s.split('/');
					var d = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var y = parseInt(ss[2],10);
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			
			$.extend($( "#fecha_inicio" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							$("#fecha_inicio" ).datebox('setValue', '');
							//$("#EdadPaciente").textbox('setValue','');
							//$("#IdTipoEdad").combobox('setValue','0');
							return false;
						}
					},  
					message: 'Porfavor Selecione una fecha valida.'  
				}
		    });
			
			$.extend($( "#fecha_fin" ).datebox.defaults.rules, { 
				validDate: {  
					validator: function(value, element){  
						var date = $.fn.datebox.defaults.parser(value);
						var s = $.fn.datebox.defaults.formatter(date);	
						
						if(s==value){
							return true;
						}else{								
							$("#fecha_fin" ).datebox('setValue', '');
							//$("#EdadPaciente").textbox('setValue','');
							//$("#IdTipoEdad").combobox('setValue','0');
							return false;
						}
					},  
					message: 'Porfavor Selecione una fecha valida.'  
				}
		    });
		
			/*function validarDatos(){
								
				var IdServicio=$('#IdServicio').combobox('getValue');
				if(IdServicio==""){
					$.messager.alert('Mensaje','Seleccione un Servicio','info');
					$('#IdServicio').next().find('input').focus();
					return 0;			
				}
			}*/
							
			function vistaprevia(){	
				//var val=validarDatos();	
				//if(val!=0){		
					document.getElementById('tipolink').value='vistaprevia';	
					document.formElem.submit();
				//}				
			}
			
			/*function exportartxt(){		
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportartxt';
					document.formElem.submit();	
				}
				
			}*/  				
					
			function exportarexcel(){	
				//var val=validarDatos();	
				//if(val!=0){		
					document.getElementById('tipolink').value='exportarexcel';
					document.formElem.submit();	
				//}
			}
				
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:auto;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:80%;height:auto;padding:10px;"
        title="LISTAR PROGRAMACION HORA MEDICO DE <?php echo $_REQUEST['Mes'].'/'.$_REQUEST['Anio'] ?>" iconCls="icon-ok" collapsible="true" align="center">
        
        <form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem">
        	<table width="1000" border="0">             	                           
              <tr>
                <td width="100">Fecha Inicio</td>
                <td width="190"><input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
                <input type="text" id="fecha_inicio" name="fecha_inicio" class="easyui-datebox" data-options="prompt:'Fecha Inicio'" validType="validDate" value="<?php echo '01/'.$Mes.'/'.$Anio; ?>" style="width:150px;" /> </td>
                <td width="100">Fecha Fin</td>
                <td width="250"><input type="text" id="fecha_fin" name="fecha_fin" class="easyui-datebox" data-options="prompt:'Fecha Fin'" validType="validDate" value="<?php echo getUltimoDiaMes($Anio,$Mes).'/'.$Mes.'/'.$Anio; ?>" style="width:150px;" /></td>
                <td width="100">&nbsp;</td>
                <td width="250">&nbsp;</td>
              </tr>
              
              <tr>
                <td valign="top">Departamentos Hospital</td>
                <td valign="top">
                  <?php 	
				  $resultados=ListarDepartamentosHospitalM();
				  	  						 
					  if($resultados!=NULL){
						//for ($i=0; $i < count($resultados); $i++) {
						$opcD=0;															
						foreach($resultados as $itemDepa){ 													
						$opcD++; 																									
				  ?>
                  <div class="">
					<input type="checkbox" name="checkboxD<?php echo $opcD ?>" id="checkboxD<?php echo $opcD?>" value="<?php echo $itemDepa["IdDepartamento"]?>"  
					<?php if($itemDepa["IdDepartamento"]=='55'){ ?> checked 
					<?php }else if($itemDepa["IdDepartamento"]=='57'){ ?> checked 
					<?php }else if($itemDepa["IdDepartamento"]=='4'){ ?> checked 
					<?php }else if($itemDepa["IdDepartamento"]=='14'){ ?> checked
					<?php }else if($itemDepa["IdDepartamento"]=='11'){ ?> checked 
					<?php }else if($itemDepa["IdDepartamento"]=='3'){ ?> checked 
					<?php }else if($itemDepa["IdDepartamento"]=='1'){ ?> checked 
					<?php }else if($itemDepa["IdDepartamento"]=='56'){ ?> checked
					<?php }else if($itemDepa["IdDepartamento"]=='53'){ ?> checked
					<?php }else if($itemDepa["IdDepartamento"]=='2'){ ?> checked 
					<?php }else if($itemDepa["IdDepartamento"]=='5'){ ?> checked <?php } ?> onClick="llenarIdDepartamentoChk();" class="classDep" />
					<label for="checkboxD<?php echo $opcD?>" class=""><?php echo $itemDepa["Nombre"]?></label>
				   </div>                                                
				 <?php 															
                    }//end  foreach
				 }
                 ?>                  
                <input name="IdDepartamentoChk" id="IdDepartamentoChk" type="hidden" value="55,57,4,14,11,3,1,56,53,2,5">                 
                </td>
                
                <td valign="top">Especialidades</td>
                <td valign="top"><?php 	
				  $resultadosEspecialidad=ListarEspecialidadM();
				  	  $opcE=0;					 
					  if($resultadosEspecialidad!=NULL){
						//for ($i=0; $i < count($resultados); $i++) {																					
						foreach($resultadosEspecialidad as $itemEspecialidad){ 													
						$opcE++; 																									
				  ?>
                  <div class="">
                    <input type="checkbox" name="checkboxE<?php echo $opcE ?>" id="checkboxE<?php echo $opcE?>" value="<?php echo $itemEspecialidad["IdEspecialidad"]?>"  
					<?php if($itemEspecialidad["IdEspecialidad"]=='7'){ ?> checked 
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='131'){ ?> checked 
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='1'){ ?> checked 
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='34'){ ?> checked
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='35'){ ?> checked 
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='38'){ ?> checked 
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='129'){ ?> checked 
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='128'){ ?> checked
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='39'){ ?> checked
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='127'){ ?> checked 
					<?php }else if($itemEspecialidad["IdEspecialidad"]=='73'){ ?> checked <?php } ?> onClick="llenarIdEspecialidadChk();" class="classEsp" />
                    <label for="checkboxE<?php echo $opcE?>" class=""><?php echo $itemEspecialidad["Nombre"]?></label>
                  </div>
                  <?php 															
                    }//end  foreach
				 }
                 ?>
                <input name="IdEspecialidadChk" id="IdEspecialidadChk" type="hidden" value="7,131,1,34,35,38,129,128,39,127,73,42,3,4,5,123,30,8,9,51,140,
10,12,23,13,43,14,25,52,31,53,44,15,46,27,220,134,133,54,16,17,48,86,49"></td>

                <td valign="top">Servicios</td>
                <td valign="top"><?php 	
				  $resultadosServicio=ListarServiciosEspecialidadM();
				  	  $opcS=0;					 
					  if($resultadosServicio!=NULL){
						//for ($i=0; $i < count($resultados); $i++) {																					
						foreach($resultadosServicio as $itemServicio){ 													
						$opcS++; 																									
				  ?>                                              
					<div class="">
					<input type="checkbox" name="checkboxS<?php echo $opcS ?>" id="checkboxS<?php echo $opcS?>" value="<?php echo $itemServicio["IdServicio"]?>"  
					<?php if($itemServicio["IdServicio"]=='306'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='307'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='273'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='274'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='777'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='779'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='776'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='781'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='41'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='300'){ ?> checked
                    
                    <?php }else if($itemServicio["IdServicio"]=='301'){ ?> checked  
					<?php }else if($itemServicio["IdServicio"]=='37'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='124'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='125'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='126'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='133'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='57'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='144'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='156'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='154'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='155'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='134'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='135'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='157'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='158'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='159'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='160'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='161'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='177'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='178'){ ?> checked
                    
                    <?php }else if($itemServicio["IdServicio"]=='61'){ ?> checked  
					<?php }else if($itemServicio["IdServicio"]=='62'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='94'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='181'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='183'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='185'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='187'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='38'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='191'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='192'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='97'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='40'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='213'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='214'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='306'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='307'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='387'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='227'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='229'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='230'){ ?> checked 
					
					<?php }else if($itemServicio["IdServicio"]=='232'){ ?> checked  
					<?php }else if($itemServicio["IdServicio"]=='432'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='68'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='270'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='271'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='272'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='273'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='274'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='31'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='32'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='172'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='173'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='426'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='427'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='238'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='236'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='237'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='239'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='240'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='241'){ ?> checked
					
					<?php }else if($itemServicio["IdServicio"]=='242'){ ?> checked  
					<?php }else if($itemServicio["IdServicio"]=='50'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='36'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='35'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='282'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='284'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='288'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='289'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='297'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='298'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='299'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='76'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='257'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='263'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='250'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='251'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='252'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='256'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='258'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='79'){ ?> checked
					
					<?php }else if($itemServicio["IdServicio"]=='80'){ ?> checked  
					<?php }else if($itemServicio["IdServicio"]=='243'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='248'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='249'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='428'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='278'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='279'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='66'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='259'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='260'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='261'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='262'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='264'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='265'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='266'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='267'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='268'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='269'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='275'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='65'){ ?> checked
					
					<?php }else if($itemServicio["IdServicio"]=='216'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='216'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='219'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='222'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='224'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='226'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='220'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='389'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='98'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='113'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='114'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='221'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='223'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='225'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='228'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='369'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='370'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='409'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='418'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='372'){ ?> checked
                    
                    <?php }else if($itemServicio["IdServicio"]=='42'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='43'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='253'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='254'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='99'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='245'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='381'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='255'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='218'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='305'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='302'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='304'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='303'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='81'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='82'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='83'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='136'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='137'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='138'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='139'){ ?> checked
                    
                    <?php }else if($itemServicio["IdServicio"]=='140'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='423'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='411'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='410'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='93'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='162'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='164'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='39'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='174'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='176'){ ?> checked
                    <?php }else if($itemServicio["IdServicio"]=='313'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='310'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='311'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='312'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='67'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='182'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='184'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='64'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='190'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='193'){ ?> checked 
					
					<?php }else if($itemServicio["IdServicio"]=='198'){ ?> checked  
					<?php }else if($itemServicio["IdServicio"]=='200'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='202'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='37'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='124'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='160'){ ?> checked 
					<?php }else if($itemServicio["IdServicio"]=='777'){ ?> checked
					<?php }else if($itemServicio["IdServicio"]=='3'){ ?> checked<?php } ?> onClick="llenarIdServicioChk();" class="classSer" />
					<label for="checkboxS<?php echo $opcS?>" class=""><?php echo $itemServicio["Nombre"]?></label>
				   </div>                                                
				 <?php 															
                    }//end  foreach
				 }
                 ?>                  
                <input name="IdServicioChk" id="IdServicioChk" type="hidden" value="306,307,273,274,777,779,776,781,41,300,
                301,37,124,125,126,133,57,144,156,154,155,134,135,157,158,159,160,161,177,178,
                61,62,94,181,183,185,187,38,191,192,97,40,213,214,306,307,387,227,229,230,
                232,432,68,270,271,272,273,274,31,32,172,173,426,427,238,236,237,239,240,241,
                242,50,36,35,282,284,288,289,297,298,299,76,257,263,250,251,252,256,258,79,
                80,243,248,249,428,278,279,66,259,260,261,262,264,265,266,267,268,269,275,65,
                216,217,219,222,224,226,220,389,98,113,114,221,223,225,228,369,370,409,418,372,
                42,43,253,254,99,245,381,255,218,305,302,304,303,81,82,83,136,137,138,139,
                140,423,411,410,93,162,164,39,174,176,313,310,311,312,67,182,184,64,190,193,
                198,200,202,37,124,160,777,31"> 
                
                </td>
              </tr>
              <tr>
                <td><input name="tipolink" id="tipolink" type="hidden" value=""> </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
                              
                 <div class="buttons" align="center">
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-print'"  style="width:20%"  onclick="vistaprevia();">Vista Previa</button>-->
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>-->
                   <button type="button" name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" style="width:20%" onclick="exportarexcel()" value="Exportar Excel" >Exportar Excel</button>                      
                      <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:20%">Cancelar</a>
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:20%" data-options="iconCls:'icon-reload'">Refrescar</a> 
                  </div>
              </form>
		</div>	
		 </div>	
      
    </body>
</html>
