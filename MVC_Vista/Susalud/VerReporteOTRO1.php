<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>
     	
        <script>
			function validarDatos(){
								
				var CodIpress=$('#CodIpress').textbox('getValue');
				if(CodIpress==""){
					$.messager.alert('Mensaje','Ingrese Código de IPRESS','info');
					$('#CodIpress').next().find('input').focus();
					return 0;			
				}
				
				var CodUgiPres=$('#CodUgiPres').textbox('getValue');
				if(CodUgiPres==""){
					$.messager.alert('Mensaje','Ingrese Código de UGIPRESS','info');
					$('#CodUgiPres').next().find('input').focus();
					return 0;			
				}				
				
			}
							
			function vistaprevia(){	
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='vistaprevia';	
					document.formElem.submit();
				}				
			}
			
			function exportartxt(){		
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportartxt';
					document.formElem.submit();	
				}				
			}
			
			function exportarexcel(){	
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportarexcel';
					document.formElem.submit();	
				}
			}
			
			function exportarexcelEstadistica(){
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportarexcelEstadistica';
					document.formElem.submit();	
				}				
			}  
			
			function exportarDBF(){
				var val=validarDatos();	
				if(val!=0){		
					document.getElementById('tipolink').value='exportarDBF';
					document.formElem.submit();	
				}
				
			}				
					
			$(function() {
				$("#CodIpress").textbox('textbox').attr('maxlength',10);
				$("#CodUgiPres").textbox('textbox').attr('maxlength',6);
				$("#CodIgss").textbox('textbox').attr('maxlength',2);
				$("#CodRed").textbox('textbox').attr('maxlength',2);
				$("#CodMicrored").textbox('textbox').attr('maxlength',2);
  
			})//function
				
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:80%;height:auto;padding:10px;"
        title="Reporte de Ingresos y Egresos Emergencia del <?php echo $_REQUEST['Mes'].'/'.$_REQUEST['Anio'] ?>" iconCls="icon-ok" collapsible="true" align="center">
        
        <?php 
		//COPIAR DATOS TABLA TEMPORAL
		ElimTemp_IngresosEgresosEmergenciaM();
		RegTemp_IngresosEgresosEmergenciaM($_REQUEST['Anio'], $_REQUEST['Mes']);		
		?>
        
        <form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem">
        	<table width="600" border="0">             	                           
              <tr>
                <td width="192"><label>Año:</label></td>
                <td width="100">
                	<input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
               		<input value="<?php echo $_REQUEST['Anio'] ?>" class="easyui-textbox" name="Anio" id="Anio" style="width:100px;height:32px" readonly >
                </td>
                <td width="180"><label>Mes:</label></td>               
                <td width="100">
                	<input name="Mes" id="Mes" type="hidden" value="<?php echo $_REQUEST['Mes'] ?>">
                	<input value="<?php echo mb_strtoupper(nombremes($_REQUEST['Mes'])); ?>" class="easyui-textbox" name="nombremes" id="nombremes" style="width:100px;height:32px" readonly>
                </td>
              </tr>
              <tr>
                <td>Código Establecimiento</td>
                <td><input class="easyui-textbox" name="CodIpress" id="CodIpress" style="width:100px;height:32px" value="0000006218" maxlength="10"></td>
                <td>Ubigeo Establecimiento</td>
                <td><input class="easyui-textbox" name="CodUgiPres" id="CodUgiPres" style="width:100px;height:32px" value="070102" maxlength="6"></td>
              </tr>
              <tr>
                <td width="192">Código de la DIRESA /IGSS/DISA</td>
                <td width="100"><input class="easyui-textbox" name="CodIgss" id="CodIgss" style="width:100px;height:32px" value="08" maxlength="2"></td>
                <td width="180">Código de la RED</td>
                <td width="100"><input class="easyui-textbox" name="CodRed" id="CodRed" style="width:100px;height:32px" value="00" maxlength="2"></td>
              </tr>
              <tr>
                <td>Código de la MICRORED</td>
                <td><input class="easyui-textbox" name="CodMicrored" id="CodMicrored" style="width:100px;height:32px" value="00" maxlength="2"></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input name="tipolink" id="tipolink" type="hidden" value=""> </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
                              
                 <div class="buttons" align="center">
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-print'"  style="width:20%"  onclick="vistaprevia();">Vista Previa</button>-->
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>-->
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-excel'"  style="width:20%"  onclick="exportarexcel();">Exportar Excel SEM</button>-->
                    
                  	  <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-excel'"  style="width:20%"  onclick="exportarexcelEstadistica();">Exportar Excel Estadistica</button>                   
                 	  <button type="button"  name="save" class="easyui-linkbutton" style="width:20%"  onclick="exportarDBF();">Exportar DBF SEM</button>  
                   </div> <br>
                   
                   <div class="buttons" align="center">                 
                    
                      <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:15%">Cancelar</a>                      
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:15%" data-options="iconCls:'icon-reload'">Refrescar</a> 
                      
                  </div>
              </form>
		</div>	
		 </div>	
      
    </body>
</html>
