<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <!--<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
     	
        <script>
			function vistaprevia(){ 	 
				document.formElem.submit();
			}
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:100%;height:auto;padding:10px;"
        title="Reporte de Consolidado de Partos" iconCls="icon-ok" collapsible="true" align="center">      
        
        	 <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Datos Totales Sistema SIGH" style="width:100%;height:400px" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
				<th field="AnioMes"  width="60">Periodo</th>
				<th field="CodIPress" width="100">Codigo IPRESS</th>
				<th field="CodUgiPress" width="100">Codigo UGIPRESS</th>
				<th field="codigoz" width="100">Tipo de Parto</th>
				<th field="TotalIng" width="125">Complicación del Parto</th>               
				<th field="TotalEgr" width="110">Partos</th>                
                <th field="estancia" width="90">Total de Nacim.</th> 
                <th field="totpaciente" width="90">Nacim. Vivos</th> 
                <th field="totfacellidos" width="50">Nacim. Muertos</th>              
			</tr>
		</thead>
	</table>
    
    	<form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem" >
         <table width="600" border="0" style="visibility:hidden;display:none;" > 
            <tr>
                <td width="192"><label>Año:</label></td>
                <td width="100">
                	<input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
               		<input value="<?php echo $_REQUEST['Anio'] ?>" class="easyui-textbox" name="Anio" id="Anio" style="width:100px;height:32px" readonly>
                </td>
                <td width="180"><label>Mes:</label></td>               
                <td width="100">
                	<input name="Mes" id="Mes" type="hidden" value="<?php echo $_REQUEST['Mes'] ?>">
                	<input value="<?php echo mb_strtoupper(nombremes($_REQUEST['Mes'])); ?>" class="easyui-textbox" name="nombremes" id="nombremes" style="width:100px;height:32px" readonly>
                </td>
              </tr>
              <tr>
                <td>Código de IPRESS</td>
                <td><input class="easyui-textbox" name="CodIpress" id="CodIpress" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodIpress'] ?>"></td>
                <td>Código de UGIPRESS</td>
                <td><input class="easyui-textbox" name="CodUgiPres" id="CodUgiPres" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodUgiPres'] ?>"></td>
              </tr>
                           
              <tr>
                <td><input name="tipolink" id="tipolink" type="hidden" value=""> </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>	
          </table>
       </form>                      
                 <div class="buttons" align="center">
                  <!-- <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-print'"  style="width:20%"  onclick="vistaprevia();">Vista Previa</button>-->
                   <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>
                      <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:20%">Cancelar</a>
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:20%" data-options="iconCls:'icon-reload'">Refrescar</a>
                  </div>
              
		</div>	
	</div>	
         
         <script>
		 		
		function exportartxt(){										
				document.getElementById('tipolink').value='exportartxt';
				document.formElem.submit();				
		} 
		
			
		function getData(){
			var rows = [];
			
		<?php 			
			$resultados = ListarConsolidado_HospitalizacionM($Anio, $Mes, $CodIpress, $CodUgiPres);			 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {										
					$AnioMes = $resultados[$i]["AnioMes"];
					$CodIPress = $resultados[$i]["CodIPress"];
					$CodUgiPress = $resultados[$i]["CodUgiPress"];
					$codigoz = $resultados[$i]["codigoz"];
					$TotalIng = $resultados[$i]["Ing"];
					$TotalEgr = $resultados[$i]["Egr"];	
					$estancia = $resultados[$i]["estancia"];
					if($estancia!=NULL){
					  $estancia = $resultados[$i]["estancia"];
					}else{
					  $estancia = '0';	
					}
					$totpaciente = $resultados[$i]["totpaciente"];
					$totfacellidos = $resultados[$i]["totfacellidos"];	 							
		?>			
				
				rows.push({					
					AnioMes: '<?php echo $AnioMes; ?>',
					CodIPress: '<?php echo $CodIPress; ?>',
					CodUgiPress: '<?php echo $CodUgiPress; ?>',	
					codigoz: '<?php echo $codigoz; ?>',	
					TotalIng: '<?php echo $TotalIng; ?>',
					TotalEgr: '<?php echo $TotalEgr; ?>',
					estancia: '<?php echo $estancia; ?>',
					totpaciente: '<?php echo $totpaciente; ?>',
					totfacellidos: '<?php echo $totfacellidos; ?>'					
					
				});
			
			<?php  //$i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});
		
		/*$(function(){
			$('#dg').datagrid({data:getData()}).datagrid('clientPaging');
		});*/
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});	
	</script>
      
    </body>
</html>
