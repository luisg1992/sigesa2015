<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <!--<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
     	
        <script>
			function vistaprevia(){ 	 
				document.formElem.submit();
			}
		</script>
    
    
    </head>
    <body class="login_page">
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:100%;height:auto;padding:10px;"
        title="Reporte de Ingresos y Egresos Emergencia" iconCls="icon-ok" collapsible="true" align="center">      
        
        	 <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Datos Totales Sistema SIGH" style="width:100%;height:600px" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
				<th field="CodIPress" width="100">Cód. establecimiento</th>
				<th field="CodUgiPress" width="100">Ubigeo establecimiento</th>
				<th field="CodIgss" width="50">Cód.DIRESA/IGSS/DISA</th>
				<th field="CodRed" width="50">Cód.RED</th>               
				<th field="CodMicrored" width="50">Cód.MICRORED</th>                
                <th field="FECHAING" width="90">Fecha Atención</th> 
                <th field="HORAING" width="70">Hora Atención</th> 
                <th field="HISTORIACLINICA" width="90">N° historia clínica paciente</th> 
                <th field="NDOC" width="90">Tipo y Numero Documento paciente</th> 
                <th field="ETNIA" width="90">Etnia paciente</th>                 
                <th field="SEGURO" width="50">Financiador</th> 
                <th field="SEXO" width="50">Sexo paciente</th> 
                <th field="EDAD" width="50">Edad paciente</th> 
                <th field="TIPOEDAD" width="50">Tipo edad</th>                   
                <th field="NOMBRESPACIENTE" width="100">Nombres paciente</th> 
                <th field="APELLIDOSPACIENTE" width="100">Apellidos paciente</th> 
                <th field="DIRECCION" width="120">domicilio del paciente</th>  
                <th field="IDDISTRITODOMICILIO" width="50">Ubigeo residencia habitual del paciente</th> 
                <th field="IDDISTRITOPROCEDENCIA" width="50">Ubigeo procedencia del paciente</th>                    
                <th field="NOMBREACOMPA" width="50">acompañante paciente</th>
                <th field="DNIACOMPA" width="50">Doc.identidad acompañante</th>
                <th field="MOTIVOATCEMERGENCIA" width="50">Motivo de atención</th>
                <th field="UBIGEOSITIOOCURRENCIA" width="50">Ubigeo ocurrencia accidente</th>
                <th field="SERVICIOATENCION" width="50">Servicio que atendió al paciente</th> 
                
                <th field="DX_ING1" width="50">Código del diagnóstico 1</th> 
                <th field="TIPODIAG1" width="50">Tipo de diagnóstico del código de diagnóstico 1</th>                 
                <th field="DX_ING2" width="50">Código del diagnóstico 2</th> 
                <th field="TIPODIAG2" width="50">Tipo de diagnóstico del código de diagnóstico 2</th>                 
                <th field="DX_ING3" width="50">Código del diagnóstico 3</th> 
                <th field="TIPODIAG3" width="50">Tipo de diagnóstico del código de diagnóstico 3</th> 
                <th field="DX_ING4" width="50">Código del diagnóstico 4</th> 
                <th field="TIPODIAG4" width="50">Tipo de diagnóstico del código de diagnóstico 4</th>
                
                <th field="OTROPROCE_CPT1" width="50">Código de otros procedimientos médicos 1</th> 
                <th field="OTROPROCE_CPT2" width="50">Código de otros procedimientos médicos 2</th> 
                <th field="OTROPROCE_CPT3" width="50">Código de otros procedimientos médicos 3</th> 
                <th field="OTROPROCE_CPT4" width="50">Código de otros procedimientos médicos 4</th>                
                <th field="CONDICIONSALIDA" width="50">Condición de salida</th>
                <th field="FECHAEGRESO" width="50">Fecha de Salida (egreso)</th> 
                <th field="HORAEGRESO" width="50">Hora de Salida (egreso)</th> 
                <th field="DESTINOATENCION" width="50">Destino del paciente</th>                  
                 
                <th field="CODEESSDESTINO" width="50">Codigo del establecimiento</th> 
                <th field="UPSSIESHOSPITALIZADO" width="50">Codigo del servicio</th>                
                <th field="MEDICO" width="50">Código del Profesional</th> 
                <th field="PASOOBSERVACION" width="50">Si paso a observacion</th> 
                <th field="FECHAINOBS" width="50">Fecha de ingreso a observación </th> 
                <th field="HORAINOBS" width="50">Hora de ingreso a observación </th>                
                <th field="FECHAOUTOBS" width="50">Fecha de egreso de observación</th>
                <th field="HORAOUTOBS" width="50">Hora de egreso de observación</th> 
                <th field="TOTALESTAENOBS" width="50">Total estancia en día</th> 
                <th field="NCAMAENOBS" width="50">Número de cama</th> 
                
                <th field="MEDICOOBS" width="50">Código del profesional que observa</th> 
                <th field="CODIGO1OBS" width="50">Código del diagnóstico 1</th>                
                <th field="CODIGO2OBS" width="50">Código del diagnóstico 2</th> 
                <th field="FECHAREGISTRO" width="50">Fecha y hora de registro</th> 
                <th field="ESTADOREGISTRO" width="50">Estado de registro</th> 
			</tr>
		</thead>
	</table>
    
    	<form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem" >
         <table width="600" border="0" style="visibility:hidden;display:none;" > 
            <tr>
                <td width="192"><label>Año:</label></td>
                <td width="100">
                	<input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
               		<input value="<?php echo $_REQUEST['Anio'] ?>" class="easyui-textbox" name="Anio" id="Anio" style="width:100px;height:32px" readonly>
                </td>
                <td width="180"><label>Mes:</label></td>               
                <td width="100">
                	<input name="Mes" id="Mes" type="hidden" value="<?php echo $_REQUEST['Mes'] ?>">
                	<input value="<?php echo mb_strtoupper(nombremes($_REQUEST['Mes'])); ?>" class="easyui-textbox" name="nombremes" id="nombremes" style="width:100px;height:32px" readonly>
                </td>
              </tr>             
              <tr>
              	<td>Código Establecimiento</td>
                <td><input class="easyui-textbox" name="CodIpress" id="CodIpress" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodIpress'] ?>" maxlength="10"></td>
                <td>Ubigeo Establecimiento</td>
                <td><input class="easyui-textbox" name="CodUgiPres" id="CodUgiPres" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodUgiPres'] ?>" maxlength="6"></td>
              </tr>
              <tr>
                <td width="192">Código de la DIRESA /IGSS/DISA</td>
                <td width="100"><input class="easyui-textbox" name="CodIgss" id="CodIgss" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodIgss'] ?>" maxlength="2"></td>
                <td width="180">Código de la RED</td>
                <td width="100"><input class="easyui-textbox" name="CodRed" id="CodRed" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodRed'] ?>" maxlength="2"></td>
              </tr>
              <tr>
                <td>Código de la MICRORED</td>
                <td><input class="easyui-textbox" name="CodMicrored" id="CodMicrored" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodMicrored'] ?>" maxlength="2"></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>             
              <tr>
                <td><input name="tipolink" id="tipolink" type="hidden" value=""> </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>	
          </table>
       </form>                      
                 <div class="buttons" align="center">
                  <!-- <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-print'"  style="width:20%"  onclick="vistaprevia();">Vista Previa</button>-->
                   <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>
                   <button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-excel'"  style="width:20%"  onclick="exportarexcel();">Exportar Excel SEM</button>
                      <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:20%">Cancelar</a>
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:20%" data-options="iconCls:'icon-reload'">Refrescar</a>
                  </div>
              
		</div>	
	</div>	
         
         <script>
		 		
		function exportartxt(){										
				document.getElementById('tipolink').value='exportartxt';
				document.formElem.submit();				
		} 
		
		function exportarexcel(){		
				document.getElementById('tipolink').value='exportarexcel';
				document.formElem.submit();					
		}
			
		function getData(){
			var rows = [];
			
		<?php 			
			
			$CodIPress=$_REQUEST["CodIpress"];
			$CodUgiPress=$_REQUEST["CodUgiPres"];
			$CodIgss=$_REQUEST["CodIgss"];
			$CodRed=$_REQUEST["CodRed"];
			$CodMicrored=$_REQUEST["CodMicrored"];				
			
			//ElimTemp_IngresosEgresosEmergenciaM();
			//RegTemp_IngresosEgresosEmergenciaM($Anio, $Mes);		
			$resultados = ListarIngresosEgresosEmergenciaM($Anio, $Mes); 	 
			if($resultados!=NULL){
				for ($i=0; $i < count($resultados); $i++) {														
					//$AnioMes = $resultados[$i]["AnioMes"];
					//$CodIPress = $resultados[$i]["CodIPress"];
					//$CodUgiPress = $resultados[$i]["CodUgiPress"];					
					$FECHAING = $resultados[$i]["FECHAING"];
					$HORAING = $resultados[$i]["HORAING"];
					$HISTORIACLINICA = $resultados[$i]["HISTORIACLINICA"];
					
					$tipodoc=$resultados[$i]["TIPODOC"];
					$doc = $resultados[$i]["NDOC"];
					if(trim($doc)!="" && (int)$doc!=0){
					$NDOC = $tipodoc.$doc;
					}else{
					$NDOC = "0";	
					}				
					
					$ETNIA = $resultados[$i]["ETNIA"];//10				
					$SEGURO = $resultados[$i]["SEGURO"];//11
					$SEXO = $resultados[$i]["SEXO"];//12
					$EDAD = $resultados[$i]["EDAD"];//13
					$TIPOEDAD = $resultados[$i]["TIPOEDAD"];//14					
										
					$nopermitidos = array( "'" , '"', "," , ";" , "&" , "+" );
									
					$NOMBRESPACIENTE = trim($resultados[$i]["NOMBRESPACIENTE"]);
					$NOMBRESPACIENTE = str_replace($nopermitidos, "", $NOMBRESPACIENTE);//15				
					$APELLIDOSPACIENTE = trim($resultados[$i]["APELLIDOSPACIENTE"]);
					$APELLIDOSPACIENTE = str_replace($nopermitidos, "", $APELLIDOSPACIENTE);//16
					
					$DIRECCION=trim($resultados[$i]["DIRECCION"]);
					$DIRECCION = str_replace($nopermitidos, "", $DIRECCION);//17
					
					$IDDISTRITODOMICILIO=$resultados[$i]["IDDISTRITODOMICILIO"];//18
					$IDDISTRITOPROCEDENCIA=$resultados[$i]["IDDISTRITOPROCEDENCIA"];//19
					
					$NOMBREACOMPA=$resultados[$i]["NOMBREACOMPA"];
					$NOMBREACOMPA = str_replace($nopermitidos, "", $NOMBREACOMPA);//20
					
					$DNIACOMPA=$resultados[$i]["DNIACOMPA"];//21
					$MOTIVOATCEMERGENCIA=$resultados[$i]["MOTIVOATCEMERGENCIA"];//22
					$UBIGEOSITIOOCURRENCIA=$resultados[$i]["UBIGEOSITIOOCURRENCIA"];//23
					
					$SERVICIOATENCION=$resultados[$i]["SERVICIOATENCION"];
					$SERVICIOATENCION = str_replace($nopermitidos, "", $SERVICIOATENCION);//24
					
					$DX_ING1=$resultados[$i]["DX_ING1"];//25					
					$TIPODIAG1=$resultados[$i]["TIPODIAG1"];//26
					if($resultados[$i]["TIPODIAG1"]==NULL && $resultados[$i]["DX_ING1"]!=NULL){	
						$TIPODIAG1='D';				
					}else{
						$TIPODIAG1=$resultados[$i]["TIPODIAG1"];
					}
										
					$DX_ING2=$resultados[$i]["DX_ING2"];//27
					$TIPODIAG2=$resultados[$i]["TIPODIAG2"];//28
					if($resultados[$i]["TIPODIAG2"]==NULL && $resultados[$i]["DX_ING2"]!=NULL){	
						$TIPODIAG2='D';				
					}else{
						$TIPODIAG2=$resultados[$i]["TIPODIAG2"];
					}	
									
					$DX_ING3=$resultados[$i]["DX_ING3"];//29
					$TIPODIAG3=$resultados[$i]["TIPODIAG3"];//30
					if($resultados[$i]["TIPODIAG3"]==NULL && $resultados[$i]["DX_ING3"]!=NULL){	
						$TIPODIAG3='D';				
					}else{
						$TIPODIAG3=$resultados[$i]["TIPODIAG3"];
					}
					
					$DX_ING4=$resultados[$i]["DX_ING4"];//31
					$TIPODIAG4=$resultados[$i]["TIPODIAG4"];//32
					if($resultados[$i]["TIPODIAG4"]==NULL && $resultados[$i]["DX_ING4"]!=NULL){	
						$TIPODIAG4='D';				
					}else{
						$TIPODIAG4=$resultados[$i]["TIPODIAG4"];
					}
					
					$OTROPROCE_CPT1=$resultados[$i]["OTROPROCE_CPT1"];//33
					$OTROPROCE_CPT2=$resultados[$i]["OTROPROCE_CPT2"];//34
					$OTROPROCE_CPT3=$resultados[$i]["OTROPROCE_CPT3"];//35
					$OTROPROCE_CPT4=$resultados[$i]["OTROPROCE_CPT4"];//36					
					$CONDICIONSALIDA=$resultados[$i]["CONDICIONSALIDA"];//37
					$FECHAEGRESO=$resultados[$i]["FECHAEGRESO"];//38
					$HORAEGRESO=$resultados[$i]["HORAEGRESO"];//39
					$DESTINOATENCION=$resultados[$i]["DESTINOATENCION"];//40
					
					$CODEESSDESTINO='';//41
					$UPSSIESHOSPITALIZADO='';//42					
					$MEDICO='';//43
					$PASOOBSERVACION='';//44
					$FECHAINOBS='';//45
					$HORAINOBS='';//46					
					$FECHAOUTOBS='';//47
					$HORAOUTOBS='';//48
					$TOTALESTAENOBS='';//49
					$NCAMAENOBS='';//50
					
					$MEDICOOBS='';//51
					$CODIGO1OBS='';//52					
					$CODIGO2OBS='';//53
					$FECHAREGISTRO='';//54
					$ESTADOREGISTRO='';//55
												
		?>			
				
				rows.push({					
					CodIPress: '<?php echo $CodIPress; ?>',
					CodUgiPress: '<?php echo $CodUgiPress; ?>',	
					CodIgss: '<?php echo $CodIgss; ?>',	
					CodRed: '<?php echo $CodRed; ?>',
					CodMicrored: '<?php echo $CodMicrored; ?>',
					FECHAING: '<?php echo $FECHAING; ?>',
					HORAING: '<?php echo $HORAING; ?>',
					HISTORIACLINICA: '<?php echo $HISTORIACLINICA; ?>',
					NDOC: '<?php echo $NDOC; ?>',
					ETNIA: '<?php echo $ETNIA; ?>',
					SEGURO: '<?php echo $SEGURO; ?>',
					SEXO: '<?php echo $SEXO; ?>',
					EDAD: '<?php echo $EDAD; ?>',
					TIPOEDAD: '<?php echo $TIPOEDAD; ?>',
					NOMBRESPACIENTE: '<?php echo $NOMBRESPACIENTE; ?>',
					APELLIDOSPACIENTE: '<?php echo $APELLIDOSPACIENTE; ?>',
					DIRECCION: '<?php echo $DIRECCION; ?>',
					IDDISTRITODOMICILIO: '<?php echo $IDDISTRITODOMICILIO; ?>',
				    IDDISTRITOPROCEDENCIA: '<?php echo $IDDISTRITOPROCEDENCIA; ?>',					
					NOMBREACOMPA: '<?php echo $NOMBREACOMPA; ?>',
				    DNIACOMPA: '<?php echo $DNIACOMPA; ?>',
					MOTIVOATCEMERGENCIA: '<?php echo $MOTIVOATCEMERGENCIA; ?>',
				    UBIGEOSITIOOCURRENCIA: '<?php echo $UBIGEOSITIOOCURRENCIA; ?>',					
					SERVICIOATENCION: '<?php echo $SERVICIOATENCION; ?>',					
				    DX_ING1: '<?php echo $DX_ING1; ?>',
					TIPODIAG1: '<?php echo $TIPODIAG1; ?>',
				    DX_ING2: '<?php echo $DX_ING2; ?>',
					TIPODIAG2: '<?php echo $TIPODIAG2; ?>',
				    DX_ING3: '<?php echo $DX_ING3; ?>',
					TIPODIAG3: '<?php echo $TIPODIAG3; ?>',
				    DX_ING4: '<?php echo $DX_ING4; ?>',
				    TIPODIAG4: '<?php echo $TIPODIAG4; ?>',
					
					OTROPROCE_CPT1: '<?php echo $OTROPROCE_CPT1; ?>',
					OTROPROCE_CPT2: '<?php echo $OTROPROCE_CPT2; ?>',
				    OTROPROCE_CPT3: '<?php echo $OTROPROCE_CPT3; ?>',
					OTROPROCE_CPT4: '<?php echo $OTROPROCE_CPT4; ?>',
				    CONDICIONSALIDA: '<?php echo $CONDICIONSALIDA; ?>',
					FECHAEGRESO: '<?php echo $FECHAEGRESO; ?>',
				    HORAEGRESO: '<?php echo $HORAEGRESO; ?>',
				    DESTINOATENCION: '<?php echo $DESTINOATENCION; ?>',
					
				    CODEESSDESTINO: '<?php echo $CODEESSDESTINO; ?>',
				    UPSSIESHOSPITALIZADO: '<?php echo $UPSSIESHOSPITALIZADO; ?>',					
					MEDICO: '<?php echo $MEDICO; ?>',
					PASOOBSERVACION: '<?php echo $PASOOBSERVACION; ?>',
				    FECHAINOBS: '<?php echo $FECHAINOBS; ?>',
					HORAINOBS: '<?php echo $HORAINOBS; ?>',
				    FECHAOUTOBS: '<?php echo $FECHAOUTOBS; ?>',
					HORAOUTOBS: '<?php echo $HORAOUTOBS; ?>',
				    TOTALESTAENOBS: '<?php echo $TOTALESTAENOBS; ?>',
				    NCAMAENOBS: '<?php echo $NCAMAENOBS; ?>',
					
					MEDICOOBS: '<?php echo $MEDICOOBS; ?>',
				    CODIGO1OBS: '<?php echo $CODIGO1OBS; ?>',					
					CODIGO2OBS: '<?php echo $CODIGO2OBS; ?>',
					FECHAREGISTRO: '<?php echo $FECHAREGISTRO; ?>',
				    ESTADOREGISTRO: '<?php echo $ESTADOREGISTRO; ?>'		
					
				});
			
			<?php  //$i += 1;	
		}
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});
		
		/*$(function(){
			$('#dg').datagrid({data:getData()}).datagrid('clientPaging');
		});*/
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});	
	</script>
      
    </body>
</html>
