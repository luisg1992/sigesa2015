<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SIGESA - SUSALUD</title>
        <!--CSS-->
	    <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="../../MVC_Complemento/easyui/themes/color.css">        
        <style>
            html, body { height: 100%;font-family: Helvetica;}
        </style>

         <!--JS-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="../../MVC_Complemento/easyui/jquery.easyui.min.js"></script>
        <!--<script type="text/javascript" src="../../MVC_Complemento/easyui/plugins/jquery.messager.js"></script>-->
        <script type="text/javascript" src="../../MVC_Complemento/easyui/datagrid-filter.js"></script>
     	
        <script>
			function vistaprevia(){ 	 
				document.formElem.submit();
			}
			
			/*function exportartxt(){										
				document.getElementById('tipolink').value='exportartxt';
				document.formElem.submit();				
			 }*/
			
			function exportarexcel(){
				document.getElementById('tipolink').value='exportarexcel';
				document.formElem.submit();			
			}			
			
		</script>
    
    
    </head>
    <body class="login_page">   
		
		 <div class="easyui-layout" style="width:100%;height:100%;">  
         <!--DIV ENCABEZADO MENU-->      
          <div id="p" class="easyui-panel" style="width:100%;height:auto;padding:10px;"
        title="Ingresos y Egresos Hospitalización" iconCls="icon-ok" collapsible="true" align="center">      
        
        	 <table  class="easyui-datagrid" toolbar="#tb" id="dg" title="Datos Totales Sistema SIGH" style="width:100%;height:400px" data-options="				
                rownumbers:true,
                method:'get',
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10">
		<thead>
			<tr>
				<!--<th field="AnioMes"  width="60">Periodo</th>-->
				<th field="Dia" width="50">Dia</th>
				<th field="SaldoAnt" width="100">Saldo Anterior</th>
				<th field="Ingreso" width="100" formatter="FormatColorIng">Ingresos</th>
				<th field="TotalTransfIng" width="100" formatter="FormatColorIng">Transf.</th>               
				<th field="TotalIng" width="110" formatter="FormatColorIng">Total</th>  
                
                <th field="Egreso" width="100" formatter="FormatColorEgr">Altas</th>
				<th field="TotalTransfEgr" width="100" formatter="FormatColorEgr">Transf.</th>  
                <th field="Defuncion" width="100" formatter="FormatColorEgr">Defuncion</th>              
				<th field="TotalEgr" width="100" formatter="FormatColorEgr">Total</th>               
                <th field="SaldoDiaSgte" width="100">Saldo dia Sgte</th> 
                <th field="Camas" width="100">Total Camas</th>                
			</tr>
		</thead>
	</table>
    
    	<form action="../../MVC_Controlador/Susalud/SusaludC.php?acc=VistaPrevia&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>" method="post" name="formElem" id="formElem" >       
        <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" /> 
         <table width="600" border="0" style="visibility:hidden;display:none;" > 
            <tr>
                <td width="192"><label>Año:</label></td>
                <td width="100">
                	<input name="tiporep" id="tiporep" type="hidden" value="<?php echo $_REQUEST['tiporep'] ?>"> 
               		<input value="<?php echo $_REQUEST['Anio'] ?>" class="easyui-textbox" name="Anio" id="Anio" style="width:100px;height:32px" readonly>
                </td>
                <td width="180"><label>Mes:</label></td>               
                <td width="100">
                	<input name="Mes" id="Mes" type="hidden" value="<?php echo $_REQUEST['Mes'] ?>">
                	<input value="<?php echo mb_strtoupper(nombremes($_REQUEST['Mes'])); ?>" class="easyui-textbox" name="nombremes" id="nombremes" style="width:100px;height:32px" readonly>
                </td>
              </tr>
              <tr>
                <td>Código de IPRESS</td>
                <td><input class="easyui-textbox" name="CodIpress" id="CodIpress" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodIpress'] ?>"></td>
                <td>Código de UGIPRESS</td>
                <td><input class="easyui-textbox" name="CodUgiPres" id="CodUgiPres" style="width:100px;height:32px" value="<?php echo $_REQUEST['CodUgiPres'] ?>"></td>
              </tr>
                           
              <tr>
                <td><input name="tipolink" id="tipolink" type="hidden" value=""> <input name="IdServicio" id="IdServicio" type="hidden" value="<?php echo $_REQUEST['IdServicio'] ?>"> </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>	
          </table>
       </form>                      
                 <div class="buttons" align="center">                 
                   <!--<button type="button"  name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  style="width:20%"  onclick="exportartxt();">Exportar Txt</button>-->
                   <button type="button" name="save" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" style="width:20%" onclick="exportarexcel()" value="Exportar Excel" >Exportar Excel</button>
                   <a href="../../MVC_Controlador/Susalud/SusaludC.php?acc=Exportar_txt&amp;IdEmpleado=<?php echo $_GET['IdEmpleado'] ?>"  class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:20%">Cancelar</a>
                      <a href="javascript:location.reload()"  class="easyui-linkbutton" style="width:20%" data-options="iconCls:'icon-reload'">Refrescar</a>
                  </div>
              
		</div>	
	</div>	
         
         <script>	
		 
    function FormatColorIng(val,row){        
            return '<span style="color:red;">'+val+'</span>';      
    }
	
	function FormatColorEgr(val,row){        
            return '<span style="color:blue;">'+val+'</span>';      
    }	
	
			
		function getData(){
			var rows = [];
			
	<?php 
		
	//SALDO MES ANTERIOR
	/*$PrimerDiaMes=$Anio.'-'.$Mes.'-01';	//primer dia del mes
	$FechaAntMes = strtotime ( '-1 day' , strtotime ( $PrimerDiaMes ) ) ; //dia anterior
	$FechaAntMes = date ( 'Y-m-j' , $FechaAntMes );
	
	$resulSaldoAnt = ObtenerCamasOcupadasDisponiblesAntM($FechaAntMes, $IdServicio);					 
	 if($resulSaldoAnt!=NULL){
		for ($s=0; $s < count($resulSaldoAnt); $s++) {												
			 $OcupadasAnt = $resulSaldoAnt[$s]["Ocupadas"];
		}
	 }else{
			 $OcupadasAnt = 0;
		}*/
	//FIN SALDO MES ANTERIOR
	
	$numero = cal_days_in_month(CAL_GREGORIAN, $Mes, $Anio); // 31					
	$resultados = ListarDiasHospitalizadosOcuM($Anio, $Mes, $CodIpress, $CodUgiPres, $IdServicio);	
	if($resultados==NULL){
		$resultados = ListarDiasHospitalizadosDesocuM($Anio, $Mes, $CodIpress, $CodUgiPres, $IdServicio);
	}	
				 
	if($resultados!=NULL){			
		for ($i=0; $i < count($resultados); $i++) {
		  $SaldoAnt=$OcupadasAnt;
		  for($Dia=1;$Dia<=$numero;$Dia++){
			$Camas = $resultados[$i]["Totcamas"]; //TOTAL DEL CAMAS DEL SERVICIO  		  
		  	$Fecha=$Anio.'-'.$Mes.'-'.$Dia;	
						 
			$SaldoAnt=$SaldoAnt+$TotalIng-$TotalEgr;
			
			//INGRESO ACTUAL				
			$Fecha=$Anio.'-'.$Mes.'-'.$Dia;
			$resulIngresos = ListarIngresosHospitalizadosM($Fecha, $IdServicio);					 
			 if($resulIngresos!=NULL){
				$TotalIng=0; $TotalTransfIng=0; $Ingreso=0;
				for ($j=0; $j < count($resulIngresos); $j++) {												
					$TransfIng = $resulIngresos[$j]["TransfIng"];
					$TotalTransfIng = $TotalTransfIng+$TransfIng;//transferencia							
						
					$Ingresos = $resulIngresos[$j]["Ingresos"];	
					$TotalIng = $TotalIng+$Ingresos; //totalingreso (suma de los ingresos)
										
					$Ingreso = $TotalIng-$TotalTransfIng; //ingresos menos la transferencia
				}
			  }else{
				$TotalIng="0"; $TotalTransfIng="0"; $Ingreso="0";
			  }	
			  
			//EGRESO ACTUAL				
			$Fecha=$Anio.'-'.$Mes.'-'.$Dia;
			$resulEgresos = ListarEgresosHospitalizadosM($Fecha, $IdServicio);					 
			 if($resulEgresos!=NULL){
				$TotalEgr=0; $TotalTransfEgr=0; $Egreso=0; $Defuncion=0;
				for ($j=0; $j < count($resulEgresos); $j++) {												
					$TransfEgr = $resulEgresos[$j]["TransfEgr"];
					$TotalTransfEgr = $TotalTransfEgr+$TransfEgr;//transferencia
					
					$Fallecidos = $resulEgresos[$j]["Fallecidos"];
					$Defuncion = $Defuncion+$Fallecidos;							
						
					/*$Egresos = $resulEgresos[$j]["Egresos"];//Alta					
					$TotalEgr = $TotalEgr+$Egresos; //totalingreso (suma de los egresos)
					$Egreso = $TotalEgr-$TotalTransfEgr-$Defuncion; //egresos menos la transferencia*/
					
					$Alta = $resulEgresos[$j]["Alta"];//Alta					
					$Egreso = $Egreso+$Alta; 				
					$TotalEgr = $TotalTransfEgr+$Defuncion+$Egreso;//totalingreso (suma de los egresos)			 
				}
			  }else{
				$TotalEgr="0"; $TotalTransfEgr="0"; $Egreso="0"; $Defuncion="0";
			  }	
			  
			  //$SaldoDiaSgte = $SaldoAnt+$TotalIng-$TotalEgr;								
												
		?>			
				
				rows.push({	
					Dia: '<?php echo $Dia; ?>',
					<?php /*?>SaldoAnt: '<?php echo $SaldoAnt; ?>',<?php */?>
					
					Ingreso: '<?php echo $Ingreso; ?>',
					TotalTransfIng: '<?php echo $TotalTransfIng; ?>',					
					TotalIng: '<?php echo $TotalIng; ?>',
										
					Egreso: '<?php echo $Egreso; ?>',					
					TotalTransfEgr: '<?php echo $TotalTransfEgr; ?>',
					Defuncion: '<?php echo $Defuncion; ?>',
					TotalEgr: '<?php echo $TotalEgr; ?>',
										
					<?php /*?>SaldoDiaSgte: '<?php echo $SaldoDiaSgte; ?>',<?php */?>
					Camas: '<?php echo $Camas; ?>'
											
					
				});
			
			<?php  //$i += 1;	
		}
	  }
	}
?>
		return rows;
		}
		
		$('#dg').datagrid({
		  //data:getData(),
		  pagination:true,
		  pageSize:10,
		  remoteFilter:false
		});
		
		/*$(function(){
			$('#dg').datagrid({data:getData()}).datagrid('clientPaging');
		});*/
		
		$(function(){			
			var dg =$('#dg').datagrid({data:getData()}).datagrid({
				filterBtnIconCls:'icon-filter'
			});
			
			dg.datagrid('enableFilter');
		});	
	</script>
      
    </body>
</html>
