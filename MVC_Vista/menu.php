<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Gebo Admin Panel</title>
    
        <!-- Bootstrap framework -->
            <link rel="stylesheet" href="../MVC_Complemento/bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="../MVC_Complemento/bootstrap/css/bootstrap-responsive.min.css" />
        <!-- gebo blue theme-->
      
        <!-- breadcrumbs-->
           
        <!-- tooltips-->
   
        <!-- notifications -->
           
        <!-- notifications -->
          
        <!-- splashy icons -->
   
		<!-- colorbox -->
            

        <!-- main styles -->
            <link rel="stylesheet" href="../MVC_Complemento/css/style.css" />
			
           <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans" />
	<link rel="stylesheet" type="text/css" href="../../MVC_Complemento/css/sigesa_font.css">-->
        <!-- Favicon -->
            <link rel="shortcut icon" href="favicon.ico" />
		
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="../MVC_Complemento/css/ie.css" />
            <script src="../MVC_Complemento/js/ie/html5.js"></script>
			<script src="../MVC_Complemento/js/ie/respond.min.js"></script>
        <![endif]-->
		
		<script>
			//* hide all elements & show preloader
			document.documentElement.className += 'js';
		</script>
    <!-- Shared on MafiaShare.net  --><!-- Shared on MafiaShare.net  --></head>
    <body>
		  <div class="sidebar">
				
				       
					  
					 
                                            
                                        <div id="side_accordion" class="accordion">
                                                    
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                                                <i class="icon-folder-close"></i> Content
                                                            </a>
                                                        </div>
                                                        <div class="accordion-body collapse" id="collapseOne">
                                                            <div class="accordion-inner">
                                                                <ul class="nav nav-list">
                                                                    <li><a href="javascript:void(0)">Articles</a></li>
                                                                    <li><a href="javascript:void(0)">News</a></li>
                                                                    <li><a href="javascript:void(0)">Newsletters</a></li>
                                                                    <li><a href="javascript:void(0)">Comments</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a href="#collapseTwo" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                                                <i class="icon-th"></i> Modules
                                                            </a>
                                                        </div>
                                                        <div class="accordion-body collapse" id="collapseTwo">
                                                            <div class="accordion-inner">
                                                                <ul class="nav nav-list">
                                                                    <li><a href="javascript:void(0)">Content blocks</a></li>
                                                                    <li><a href="javascript:void(0)">Tags</a></li>
                                                                    <li><a href="javascript:void(0)">Blog</a></li>
                                                                    <li><a href="javascript:void(0)">FAQ</a></li>
                                                                    <li><a href="javascript:void(0)">Formbuilder</a></li>
                                                                    <li><a href="javascript:void(0)">Location</a></li>
                                                                    <li><a href="javascript:void(0)">Profiles</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a href="#collapseThree" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                                                <i class="icon-user"></i> Account manager
                                                            </a>
                                                        </div>
                                                        <div class="accordion-body collapse" id="collapseThree">
                                                            <div class="accordion-inner">
                                                                <ul class="nav nav-list">
                                                                    <li><a href="javascript:void(0)">Members</a></li>
                                                                    <li><a href="javascript:void(0)">Members groups</a></li>
                                                                    <li><a href="javascript:void(0)">Users</a></li>
                                                                    <li><a href="javascript:void(0)">Users groups</a></li>
                                                                </ul>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                           </div>
                                   
         
			
			 
            
            <script src="../MVC_Complemento/js/jquery.min.js"></script>
			<!-- smart resize event -->
			<script src="../MVC_Complemento/js/jquery.debouncedresize.min.js"></script>
			<!-- hidden elements width/height -->
			<script src="../MVC_Complemento/js/jquery.actual.min.js"></script>
			<!-- js cookie plugin -->
			<script src="../MVC_Complemento/js/jquery.cookie.min.js"></script>
			<!-- main bootstrap js -->
			<script src="../MVC_Complemento/bootstrap/js/bootstrap.min.js"></script>
			<!-- tooltips -->
			<script src="../MVC_Complemento/lib/qtip2/jquery.qtip.min.js"></script>
			<!-- jBreadcrumbs -->
			<script src="../MVC_Complemento/lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
			<!-- sticky messages -->
            <script src="../MVC_Complemento/lib/sticky/sticky.min.js"></script>
			<!-- fix for ios orientation change -->
			<script src="../MVC_Complemento/js/ios-orientationchange-fix.js"></script>
			<!-- scrollbar -->
			<script src="../MVC_Complemento/lib/antiscroll/antiscroll.js"></script>
			<script src="../MVC_Complemento/lib/antiscroll/jquery-mousewheel.js"></script>
             <!-- common functions -->
			<script src="../MVC_Complemento/js/gebo_common.js"></script>
	
            
			<script>
				$(document).ready(function() {
					//* show all elements & remove preloader
					setTimeout('$("html").removeClass("js")',100);
				});
			</script>
		
		
	</body>
</html>