//FUNCION QUE PERMITE AGREGAR LOS DIAGNOSTICOS DEL PACIENTE PARA EL CERTIFICADO MEDICO
function agregar(){

			var Descripcion=$('#Descripcion').combogrid('getText');

            if (Descripcion == "") {
			$.messager.alert('SIGESA','No hay diagnostico ingresado' ,'error');

            } else {

                var zCodigoCIE10 = $('#Descripcion').combogrid('getValue');
                var zDescripcion = $('#Descripcion').combogrid('getText');
				var Accion = '<a href="javascript:void(0)" onclick="CIE10Eliminar(this)">Eliminar</a>';
                var CIE10Duplic  = CIE10duplicado(zCodigoCIE10);
                if (zCodigoCIE10 != CIE10Duplic) {
					
                    $('#tblDiagnosticos').datagrid('insertRow', {
                        index: 0,
                        row: {
							
							x1CodigoCIE10: zCodigoCIE10,
                            x1Descripcion: zDescripcion,
                            action: Accion
                        }
                    });
					$('#Descripcion').combogrid('clear');
					$('#Descripcion').combogrid('textbox').focus();

					var nrow = $('#tblDiagnosticos').datagrid('getRows').length;
                } else {
					$.messager.alert

('SIGESA','Diagnostico:'+zCodigoCIE10+'-'+zDescripcion+ ' ya está registrado' ,'error');
					$('#Descripcion').combogrid('clear');
					$('#Descripcion').combogrid('textbox').focus();
                }

        }
	}

function HabilitarGradoActividad(){
	

	
	
var valor=$("input[name=optNI]:checked").val();
//alert(valor);
	if(valor==2){
		

		$("#optGIA").attr('disabled', false);
		$("#optGIB").attr('disabled', false);
		$("#optGIC").attr('disabled', false);
		

		
		}else{
		
		
		$("#optGIA").prop('checked', false);		  
		$("#optGIB").prop('checked', false);
		$("#optGIC").prop('checked', false);
			
		$("#optGIA").attr('disabled', true);
		$("#optGIB").attr('disabled', true);
		$("#optGIC").attr('disabled', true);
		
		
			
	}
	
}
	
//FUNCION QUE PERMITE LA BUSQUEDA DE DIAGNOSTICO REPETIDO EN EL GRID tblDiagnostico.	
function CIE10duplicado(valor) {

    var dg = $('#tblDiagnosticos');
    var data = dg.datagrid('getData');
    dg.datagrid('loadData', data);
    var rows = dg.datagrid('getRows');
    //alert(rows[0][0]);
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].x1CodigoCIE10 == valor) {

            return rows[i].x1CodigoCIE10;
            break;
        }
    }

}
//FUNCION QUE PERMITE ELIMINAR UN DIAGNOSTICO EN EL GRID tblDiagnostico
function CIE10Eliminar(target) {

    $.messager.confirm('Confirm', 'Esta seguro que desea Quitar este diagnostico?', function (r) {
        if (r) {
            var selectedrow = $('#tblDiagnosticos').datagrid('getSelected');
            var rowIndex = $('#tblDiagnosticos').datagrid('getRowIndex', selectedrow);
            //$('#dg_id').datagrid('endEdit', rowIndex);


            $('#tblDiagnosticos').datagrid('deleteRow', rowIndex);
           // CPTRecalcularTotal();
           // $('#CPTCodigo').textbox('clear').textbox('textbox').focus();
        }
    });
}
//FUNCION QUE HABILITA SI PRECISA FECHA DE INICIO DE INCAPACIDAD
function HabilitaFechaPrecisable(){
	if ($('#chkprecisable').is(":checked"))
	{
		$('#FechaIniIncapacidad').textbox('readonly', true);
		$('#FechaIniIncapacidad').textbox('clear');
	}else{
		$('#FechaIniIncapacidad').textbox('readonly', false);
		$('#FechaIniIncapacidad').textbox('clear').textbox('textbox').focus();
		}
	}
	
//FUNCION QUE REALIZA LA SUMA DE LOS MENOSCABOS SUMA LOS NUMBERSPINER		
function SumarMenoscabo(){
	
	var valor_combinado=$("#valor_combinado").numberspinner('getValue');
	var valor_actividad=$("#valor_actividad").numberspinner('getValue');
	var valor_posibilidad=$("#valor_posibilidad").numberspinner('getValue');
	var valor_edad=$("#valor_edad").numberspinner('getValue');

	var sumaglobal=
	 parseFloat(valor_combinado) + parseFloat(valor_actividad) + parseFloat(valor_posibilidad) + parseFloat(valor_edad);	
	$('#valor_global').textbox('setValue',sumaglobal);
	}
//FUNCION QUE CANCELA EL LA OPERACION REGRESANDO AL FORMULARIO DE REGISTRO	
function cancelar(){
	location.href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=Registro";
	}	
//FUNCION QUE PERMITE GRABAR ENVIANDO LOS DATOS AL CONTROLADOR.

function ActualizaDatos() {
	 var dataResultado = $("#tblDiagnosticos").datagrid('getData'); //DATA ITEMS DE FACTURACIÒN   
	 var nrow = $('#tblDiagnosticos').datagrid('getRows').length; 
	   
	  var ok = true;
	 var msg = "Falta completar los siguientes datos:</br>";
	 

	 
	  if ($("#ApellidoPaterno").textbox('getText') == "")
            {
                msg += "- Falta apellido paterno </br>";
                ok = false;
       }
	  if ($("#ApellidoMaterno").textbox('getText') == "")
            {
                msg += "- Falta apellido materno </br>";
                ok = false;
       }

	  if ($('#edadpaciente').textbox('getText') == "")
            {
                msg += "- Falta edad del paciente </br>";
                ok = false;
       }
	   
	  if ($('#NomDep').textbox('getText') == "")
            {
                msg += "- Falta departamento </br>";
                ok = false;
     }
	  if ($('#NomProv').textbox('getText') == "")
            {
                msg += "- Falta provincia </br>";
                ok = false;
     }	   
	  if ($('#NomDist').textbox('getText') == "")
            {
                msg += "- Falta distrito </br>";
                ok = false;
     }	   
	  if ($('#DireccionDomicilio').textbox('getText') == "")
            {
                msg += "- Falta dirección </br>";
                ok = false;
     }
	 if ($('#NroDocumento').textbox('getText').length < 8)
        {
                msg += "- DNI debe Tener 8 Digitos </br>";
                ok = false;
       }
	// if ($('input:radio[name=optNI]:checked').val()== ""){
	  if(!$("input[name=optNI]:checked").val()) {
            
                msg += "- Falta Naturaleza de la Incapacidad </br>";
                ok = false;
     }
/*	  if(!$("input[name=optGI]:checked").val()) {
                msg += "- Falta Grado de la Incapacidad </br>";
                ok = false;
     }*/
	 if ($('#valor_global').numberspinner('getText') == "")
            {
                msg += "- Falta Valores Menoscabo </br>";
                ok = false;
     }
	 if ((!$('#chkprecisable').is(":checked")) && $('#FechaIniIncapacidad').textbox('getText')=="" )
            {
                msg += "- Falta Feccha inicio Incapacidad </br>";
                ok = false;
     }

 	/* 	 	
	 if(nrow==0){
		 msg += "- Falta diagnóstico del paciente </br>";
         ok = false;
		 }
	*/
	
	            if (dataResultado['total'] <= 0)
			
            {
                msg += "- Falta diagnóstico del paciente "+dataResultado['total']+"   </br>";
                ok = false;
            }
	
	
		 var IdEmpleado=$('#IdEmpleado').val();
	  if (ok != false) {
		 // $.messager.confirm('SIGESA', 'Seguro de guardar la información: ', function(r){
			//if (r){
				
                $.ajax({
                    url: '../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=ActualizaCertificadoMedicos',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        
                        IdAtencionCertMed :  $('#IdAtencionCertMed').val(),
						 IdEmpleado :$('#IdEmpleado').val(),
                        IdPaciente :  $('#IdPaciente').val(),
						 
						EdadPaciente  : $('#edadpaciente').textbox('getText'),  
						FechaEmision   :   $('#FechaEmision').textbox('getText'), 
						DireccionDomicilio : $('#DireccionDomicilio').textbox('getText'),
						NaturalezaIncapacidad:$("input[name=optNI]:checked").val(), 
						GradoIncapacidad:$("input[name=optGI]:checked").val(),   
						MenoscaboCombinado:   $('#valor_combinado').val(),   
						TipoActividad  :    $('#valor_actividad').val(),
						PosibilidadReubicaLaboral : $('#valor_posibilidad').val(),  
						Edad     : $('#valor_edad').val(),
						MenoscaboGlobal :    $('#valor_global').val(), 
						SwPrecisable:$('input:checkbox[name=chkprecisable]:checked').val(), 
						FechaInicioIncapacidad   : $('#FechaIniIncapacidad').textbox('getText'), 
						Observaciones   :   $('#observaciones').textbox('getText'),
						IdDistritoDomicilio :$('#IdDistritoDomicilio').val(),
                        dataResultado: dataResultado

                    }
					
				
                }).done(function (data) {
                    console.log(data);
                    //alert(data);		

                    if (!data.success) {

                        if (data.errors.IdCertificadoMed) {
							
							$.messager.alert('SIGESA','Error al Actualizar' ,'error');
                        }


                    } else {
						var NroCertificado=data.NroCertificado;
						//$.messager.alert('SIGESA','Documento Grabado Correctamente ' ,'info');
						var mensaje='';
						mensaje+= "Documento Actualizado Correctamente </br>";
						mensaje+= "Genera Vista Previa ? </br>";
						
						$.messager.confirm('Mensaje', mensaje, function(r){
						if (r){
							 ImprimirDatosResultado(NroCertificado,IdEmpleado);	
										
							}
						});

                    }

                });
		//	}
		//});	
                // stop the form from submitting the normal way and refreshing the page
                event.preventDefault();
            }else{
				//alert(msg);
				$.messager.alert('SIGESA',msg ,'error');
			}   
	   	   	
	}
	
function ImprimirDatosResultado(valor,IdEmpleado)
{
 	var Ncertificado=valor;
	var empleado=IdEmpleado;
	
	location.href="../../MVC_Controlador/AtencionesCertificados/AtencionesCertificadosC.php?acc=ImprimeCertificadoMedico&NroDocumentoCertificado="+Ncertificado+"&IdEmpleado="+empleado;

}